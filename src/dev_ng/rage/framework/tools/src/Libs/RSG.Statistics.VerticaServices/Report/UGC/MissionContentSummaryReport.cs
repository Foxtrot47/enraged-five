﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Linq;
using System.Text;
using RSG.Base.Extensions;
using RSG.Statistics.Common;
using RSG.Statistics.Common.Model.UGC;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using Vertica.Data.VerticaClient;
using RSG.Statistics.VerticaServices.Util;
using RSG.Statistics.VerticaServices.DataAccess;
using RSG.Statistics.Common.Model;
using RSG.Base.Math;

namespace RSG.Statistics.VerticaServices.Report.UGC
{
    /// <summary>
    /// Report that provides UGC mission summary information.
    /// </summary>
    [Export(typeof(IVerticaReport))]
    internal class MissionContentSummaryReport : VerticaReportBase<MissionContentSummary, MissionContentParams>
    {
        #region Intermediate Result Classes
        /// <summary>
        /// 
        /// </summary>
        private class SubTypeStat
        {
            public MatchType Type { get; set; }
            public int SubType { get; set; }
            public ulong Count { get; set; }
        } // SubTypeStat

        /// <summary>
        /// 
        /// </summary>
        private class MaxPlayersStat
        {
            public MatchType Type { get; set; }
            public int MaxPlayers { get; set; }
            public ulong Count { get; set; }
        } // MaxPlayersStat

        /// <summary>
        /// 
        /// </summary>
        private class RadioStationStat
        {
            public MatchType Type { get; set; }
            public int RadioStationHash { get; set; }
            public ulong Count { get; set; }
        } // RadioStationStat


        /// <summary>
        /// More details about the breakdowns, queried seperately.
        /// </summary>
        private class FinerDetailStat
        {
            public float StartX { get; set; }
            public float StartY { get; set; }
            public float StartZ { get; set; }
            public MatchType Type { get; set; }
            public int SubType { get; set; }
            public int MaxPlayers { get; set; }
            public int RadioStationHash { get; set; }
            public int Contact { get; set; }
            public uint LaunchTimes { get; set; }
            public int RaceClass { get; set; }
            public int VehicleModels { get; set; }
            public uint Rank { get; set; }
        } // FinerDetailStat

        #endregion // Intermediate Result Classes

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public MissionContentSummaryReport()
            : base(ReportNames.MissionContentSummaryReport)
        {
        }
        #endregion // Constructor(s)

        #region VerticaReportBase Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        protected override MissionContentSummary Generate(ISession session, MissionContentParams parameters)
        {
            // Initialise the return object.
            MissionContentSummary summary = new MissionContentSummary();
            summary.MissionTypes.AddRange(session.RetrieveAll<MissionTypeStat>(new StringQuery(CreatePerTypeQuery(parameters))));
            summary.ContentPerPlayerCounts.AddRange(session.RetrieveAll<KeyValueStat<int, ulong>>(new StringQuery(CreatePerMaxPlayerQuery(parameters))));

            // Create a lookup for quick access to the data.
            IDictionary<MatchType, MissionTypeStat> typeLookup = summary.MissionTypes.ToDictionary(item => item.Type);

            // Retrieve all start coordinates ( finer detail ) for all content
            // Important : ToList() is required otherwise the enumerable will be evaluated one at a time upon access!
            IList<FinerDetailStat> details = session.RetrieveAll<FinerDetailStat>(new StringQuery(CreateFinerDetailQuery(parameters))).ToList();

            // Get the totals per match sub-type.
            foreach (SubTypeStat subStat in session.RetrieveAll<SubTypeStat>(new StringQuery(CreatePerSubTypeQuery(parameters))))
            {
                MissionTypeStat stat;
                if (typeLookup.TryGetValue(subStat.Type, out stat))
                {
                    // Add the sub-type stat to the breakdown.
                    if (!stat.Breakdowns.ContainsKey("Sub-Type"))
                    {
                        stat.Breakdowns["Sub-Type"] = new List<MissionTypeStatBreakDownData>();
                    }

                    // Create start coords for this sub type
                    IList<MissionInstance> instances =
                        details.Where(item => (item.Type == subStat.Type && item.SubType == subStat.SubType))
                               .Select(item => new MissionInstance(new Vector3f(item.StartX, item.StartY, item.StartZ), item.Rank))
                               .ToList();

                    String name = GetSubTypeName(subStat.Type, subStat.SubType);
                    MissionTypeStatBreakDownData breakdownData = new MissionTypeStatBreakDownData(name, subStat.Count, instances);                 

                    stat.Breakdowns["Sub-Type"].Add(breakdownData);                   
                }
            }

            // Get the max players per match type.
            foreach (MaxPlayersStat playersStat in session.RetrieveAll<MaxPlayersStat>(new StringQuery(CreatePerTypeMaxPlayersQuery(parameters))))
            {
                MissionTypeStat stat;
                if (typeLookup.TryGetValue(playersStat.Type, out stat))
                {
                    // Add the sub-type stat to the breakdown.
                    if (!stat.Breakdowns.ContainsKey("Max Player Count"))
                    {
                        stat.Breakdowns["Max Player Count"] = new List<MissionTypeStatBreakDownData>();
                    }
                    String playersString = String.Format("{0} Players", playersStat.MaxPlayers);
                    if (playersStat.MaxPlayers == -1)
                    {
                        playersString = "Max Players";
                    }

                    IList<MissionInstance> instances =
                        details.Where(item => (item.Type == playersStat.Type && item.MaxPlayers == playersStat.MaxPlayers))
                               .Select(item => new MissionInstance(new Vector3f(item.StartX, item.StartY, item.StartZ), item.Rank))
                               .ToList();

                    MissionTypeStatBreakDownData breakdownData = new MissionTypeStatBreakDownData(playersString, playersStat.Count, instances);
                    stat.Breakdowns["Max Player Count"].Add(breakdownData);
                }
            }

            // Get the default radio station per match type.
            foreach (RadioStationStat radioStat in session.RetrieveAll<RadioStationStat>(new StringQuery(CreateDefaultRadioStationQuery(parameters))))
            {
                MissionTypeStat stat;
                if (typeLookup.TryGetValue(radioStat.Type, out stat))
                {
                    // Add the sub-type stat to the breakdown.
                    if (!stat.Breakdowns.ContainsKey("Default Radio Station"))
                    {
                        stat.Breakdowns["Default Radio Station"] = new List<MissionTypeStatBreakDownData>();
                    }

                    String friendlyName;
                    if (!parameters.RadioStationLookup.TryGetValue(radioStat.RadioStationHash, out friendlyName))
                    {
                        friendlyName = String.Format("Unknown Radio Station (Hash: {0})", radioStat.RadioStationHash);
                    }

                    IList<MissionInstance> instances =
                        details.Where(item => (item.Type == radioStat.Type && item.RadioStationHash == radioStat.RadioStationHash))
                               .Select(item => new MissionInstance(new Vector3f(item.StartX, item.StartY, item.StartZ), item.Rank))
                               .ToList();

                    MissionTypeStatBreakDownData breakdownData = new MissionTypeStatBreakDownData(friendlyName, radioStat.Count, instances);
                    stat.Breakdowns["Default Radio Station"].Add(breakdownData);
                }
            }

            // Special case contact based breakdown for missions.
            MissionTypeStat missionStat;
            if (typeLookup.TryGetValue(MatchType.Mission, out missionStat))
            {
                AddMissionContactBreakdown(session, parameters, missionStat, details);
            }
            
            // Special case gang attack time of day breakdown.
            MissionTypeStat gangAttackStat;
            if (typeLookup.TryGetValue(MatchType.GangAttack, out gangAttackStat))
            {
                AddGangAttackTimeOfDayBreakdown(session, parameters, gangAttackStat, details);
            }
            
            // Special case vehicle class breakdown for races.
            MissionTypeStat raceStat;
            if (typeLookup.TryGetValue(MatchType.Race, out raceStat))
            {
                AddRaceVehicleClassBreakdown(session, parameters, raceStat, details);
            }
            
            // Special case vehicle type breakdown for deathmatches.
            MissionTypeStat deathmatchStat;
            if (typeLookup.TryGetValue(MatchType.Deathmatch, out deathmatchStat))
            {
                AddRaceVehicleTypeBreakdown(session, parameters, deathmatchStat, details);
            }

            return summary;
        }
        #endregion // IStatsReport Implementation

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private String CreatePerTypeQuery(MissionContentParams parameters)
        {
            // We want to replicate a query along the following lines:
            //
            // SELECT
            //     Data_Gen_Type AS Type, COUNT(*) AS PiecesOfContent
            // FROM
            // (
            //     SELECT *, ROW_NUMBER() OVER(PARTITION BY RootContentId ORDER BY UpdatedDate DESC) AS HistoryIndex
            //     FROM ugc.gta5mission_metadata
            // ) AS AllMissions
            // WHERE HistoryIndex=1 AND Data_Gen_Type IS NO NULL AND CategoryId=1 AND DeletedDate IS NULL AND LatestVersionContentId = ContentId
            // GROUP BY Data_Gen_Type;

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("SELECT ");
            sb.AppendFormat("Data_Gen_Type AS Type, COUNT(*) AS PiecesOfContent ");
            sb.AppendFormat("FROM (");
            sb.AppendFormat("SELECT *, ROW_NUMBER() OVER(PARTITION BY {0} ORDER BY UpdatedDate DESC) AS HistoryIndex ",
                (parameters.LatestVersionsOnly == true ? "RootContentId" : "ContentId"));
            sb.AppendFormat("FROM ugc.gta5mission_metadata ");
            sb.AppendFormat(") AS AllMissions ");
            sb.Append("WHERE AllMissions.HistoryIndex=1 AND Data_Gen_Type IS NOT NULL ");

            // Add the list of restrictions.
            IEnumerable<String> restrictions = GetRestrictions(parameters);
            if (restrictions.Any())
            {
                sb.AppendFormat("AND {0} ", String.Join(" AND ", restrictions));
            }
            sb.AppendFormat("GROUP BY Data_Gen_Type;");
            return sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private String CreateFinerDetailQuery(MissionContentParams parameters)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("SELECT ");
            sb.AppendFormat("StartX, StartY, StartZ, Data_Gen_Type AS Type, Data_Gen_Subtype AS SubType, Data_Gen_MaxPlayers AS MaxPlayers, ");
            sb.AppendFormat("Data_Gen_Music AS RadioStationHash, Data_Gen_CharacterContact AS Contact, Data_Gen_LaunchTimes AS LaunchTimes, ");
            sb.AppendFormat("Data_Race_RaceClass AS RaceClass, Data_Gen_VehicleModels AS VehicleModels, Data_Gen_Rank AS Rank ");
            sb.AppendFormat("FROM (");
            sb.AppendFormat("SELECT *, ROW_NUMBER() OVER(PARTITION BY {0} ORDER BY UpdatedDate DESC) AS HistoryIndex, ",
                (parameters.LatestVersionsOnly == true ? "RootContentId" : "ContentId"));
            sb.AppendFormat(" IFNULL(Data_Gen_StartX, 0) AS StartX, IFNULL(Data_Gen_StartY, 0) AS StartY, IFNULL(Data_Gen_StartZ, 0) AS StartZ ");
            sb.AppendFormat("FROM ugc.gta5mission_metadata ");
            sb.AppendFormat(") AS AllMissions ");
            sb.Append("WHERE AllMissions.HistoryIndex=1 AND Data_Gen_Type IS NOT NULL ");

            // Add the list of restrictions.
            IEnumerable<String> restrictions = GetRestrictions(parameters);
            if (restrictions.Any())
            {
                sb.AppendFormat("AND {0} ", String.Join(" AND ", restrictions));
            }
            return sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        private String CreatePerMaxPlayerQuery(MissionContentParams parameters)
        {
            // We want to replicate a query along the following lines:
            //
            // SELECT
            //     Data_Gen_MaxPlayers AS Key, COUNT(*) AS Value
            // FROM
            // (
            //     SELECT *, ROW_NUMBER() OVER(PARTITION BY RootContentId ORDER BY UpdatedDate DESC) AS HistoryIndex
            //     FROM ugc.gta5mission_metadata
            // ) AS AllMissions
            // WHERE HistoryIndex=1 AND Data_Gen_MaxPlayers IS NOT NULL AND CategoryId=1 AND DeletedDate IS NULL AND LatestVersionContentId = ContentId
            // GROUP BY Data_Gen_MaxPlayers;

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("SELECT ");
            sb.AppendFormat("Data_Gen_MaxPlayers AS Key, COUNT(*) AS Value ");
            sb.AppendFormat("FROM (");
            sb.AppendFormat("SELECT *, ROW_NUMBER() OVER(PARTITION BY {0} ORDER BY UpdatedDate DESC) AS HistoryIndex ",
                (parameters.LatestVersionsOnly == true ? "RootContentId" : "ContentId"));
            sb.AppendFormat("FROM ugc.gta5mission_metadata ");
            sb.AppendFormat(") AS AllMissions ");
            sb.Append("WHERE AllMissions.HistoryIndex=1 AND Data_Gen_Type IS NOT NULL AND Data_Gen_MaxPlayers IS NOT NULL ");

            // Add the list of restrictions.
            IEnumerable<String> restrictions = GetRestrictions(parameters);
            if (restrictions.Any())
            {
                sb.AppendFormat("AND {0} ", String.Join(" AND ", restrictions));
            }
            sb.AppendFormat("GROUP BY Data_Gen_MaxPlayers;");
            return sb.ToString();
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private String CreatePerSubTypeQuery(MissionContentParams parameters)
        {
            // We want to replicate a query along the following lines:
            //
            // SELECT
            //     Data_Gen_Type AS Type, Data_Gen_Subtype AS SubType, COUNT(*) AS Count
            // FROM
            // (
            //     SELECT *, ROW_NUMBER() OVER(PARTITION BY RootContentId ORDER BY UpdatedDate DESC) AS HistoryIndex
            //     FROM ugc.gta5mission_metadata
            // ) AS AllMissions
            // WHERE HistoryIndex=1 AND Data_Gen_Type IS NOT NULL AND Data_Gen_Subtype IS NOT NULL AND CategoryId=1 AND DeletedDate IS NULL AND LatestVersionContentId = ContentId
            // GROUP BY Data_Gen_Type, Data_Gen_Subtype;

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("SELECT ");
            sb.AppendFormat("Data_Gen_Type AS Type, Data_Gen_Subtype AS SubType, COUNT(*) AS Count ");
            sb.AppendFormat("FROM (");
            sb.AppendFormat("SELECT *, ROW_NUMBER() OVER(PARTITION BY {0} ORDER BY UpdatedDate DESC) AS HistoryIndex ",
                (parameters.LatestVersionsOnly == true ? "RootContentId" : "ContentId"));
            sb.AppendFormat("FROM ugc.gta5mission_metadata ");
            sb.AppendFormat(") AS AllMissions ");
            sb.AppendFormat("WHERE AllMissions.HistoryIndex=1 AND Data_Gen_Type IN ({0}, {1}, {2}) AND Data_Gen_Subtype IS NOT NULL ",
                (int)MatchType.Mission, (int)MatchType.Deathmatch, (int)MatchType.Race);

            // Add the list of restrictions.
            IEnumerable<String> restrictions = GetRestrictions(parameters);
            if (restrictions.Any())
            {
                sb.AppendFormat("AND {0} ", String.Join(" AND ", restrictions));
            }
            sb.AppendFormat("GROUP BY Data_Gen_Type, Data_Gen_Subtype;");
            return sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private String CreatePerTypeMaxPlayersQuery(MissionContentParams parameters)
        {
            // We want to replicate a query along the following lines:
            //
            // SELECT
            //     Data_Gen_Type AS Type, Data_Gen_MaxPlayers AS MaxPlayers, COUNT(*) AS Count
            // FROM
            // (
            //     SELECT *, ROW_NUMBER() OVER(PARTITION BY RootContentId ORDER BY UpdatedDate DESC) AS HistoryIndex
            //     FROM ugc.gta5mission_metadata
            // ) AS AllMissions
            // WHERE HistoryIndex=1 AND Data_Gen_Type IS NOT NULL AND Data_Gen_MaxPlayers IS NOT NULL AND CategoryId=1 AND DeletedDate IS NULL AND LatestVersionContentId = ContentId
            // GROUP BY Data_Gen_Type, Data_Gen_MaxPlayers;

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("SELECT ");
            sb.AppendFormat("Data_Gen_Type AS Type, Data_Gen_MaxPlayers AS MaxPlayers, COUNT(*) AS Count ");
            sb.AppendFormat("FROM (");
            sb.AppendFormat("SELECT *, ROW_NUMBER() OVER(PARTITION BY {0} ORDER BY UpdatedDate DESC) AS HistoryIndex ",
                (parameters.LatestVersionsOnly == true ? "RootContentId" : "ContentId"));
            sb.AppendFormat("FROM ugc.gta5mission_metadata ");
            sb.AppendFormat(") AS AllMissions ");
            sb.AppendFormat("WHERE AllMissions.HistoryIndex=1 AND Data_Gen_Type IN ({0}, {1}, {2}, {3}) AND Data_Gen_MaxPlayers IS NOT NULL ",
                (int)MatchType.Mission, (int)MatchType.Deathmatch, (int)MatchType.Race, (int)MatchType.BaseJump);

            // Add the list of restrictions.
            IEnumerable<String> restrictions = GetRestrictions(parameters);
            if (restrictions.Any())
            {
                sb.AppendFormat("AND {0} ", String.Join(" AND ", restrictions));
            }
            sb.AppendFormat("GROUP BY Data_Gen_Type, Data_Gen_MaxPlayers;");
            return sb.ToString();
        }

        /// <summary>
        /// Creates a query for retrieving the default radio station per match type.
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private String CreateDefaultRadioStationQuery(MissionContentParams parameters)
        {

            // We want to replicate a query along the following lines:
            //
            // SELECT
            //     Data_Gen_Type AS Type, Data_Gen_MaxPlayers AS MaxPlayers, COUNT(*) AS Count
            // FROM
            // (
            //     SELECT *, ROW_NUMBER() OVER(PARTITION BY RootContentId ORDER BY UpdatedDate DESC) AS HistoryIndex
            //     FROM ugc.gta5mission_metadata
            // ) AS AllMissions
            // WHERE HistoryIndex=1 AND Data_Gen_Type IS NOT NULL AND Data_Gen_MaxPlayers IS NOT NULL AND CategoryId=1 AND DeletedDate IS NULL AND LatestVersionContentId = ContentId
            // GROUP BY Data_Gen_Type, Data_Gen_MaxPlayers;

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("SELECT ");
            sb.AppendFormat("Data_Gen_Type AS Type, Data_Gen_Music AS RadioStationHash, COUNT(*) AS Count ");
            sb.AppendFormat("FROM (");
            sb.AppendFormat("SELECT *, ROW_NUMBER() OVER(PARTITION BY {0} ORDER BY UpdatedDate DESC) AS HistoryIndex ",
                (parameters.LatestVersionsOnly == true ? "RootContentId" : "ContentId"));
            sb.AppendFormat("FROM ugc.gta5mission_metadata ");
            sb.AppendFormat(") AS AllMissions ");
            sb.AppendFormat("WHERE AllMissions.HistoryIndex=1 AND Data_Gen_Type IN ({0}, {1}, {2}, {3}) AND Data_Gen_Music IS NOT NULL AND Data_Gen_Music != 0",
                (int)MatchType.Deathmatch, (int)MatchType.Race, (int)MatchType.Survival, (int)MatchType.BaseJump);

            // Add the list of restrictions.
            IEnumerable<String> restrictions = GetRestrictions(parameters);
            if (restrictions.Any())
            {
                sb.AppendFormat("AND {0} ", String.Join(" AND ", restrictions));
            }
            sb.AppendFormat("GROUP BY Data_Gen_Type, Data_Gen_Music;");
            return sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        private void AddMissionContactBreakdown(ISession session, MissionContentParams parameters, MissionTypeStat missionStat, IList<FinerDetailStat> details)
        {
            missionStat.Breakdowns["Contact"] = new List<MissionTypeStatBreakDownData>();

            foreach (KeyValueStat<int, ulong> subStat in
                session.RetrieveAll<KeyValueStat<int, ulong>>(new StringQuery(CreateMissionContactQuery(parameters))))
            {
                IList<MissionInstance> instances =
                    details.Where(item => (item.Type == MatchType.Mission && item.Contact == subStat.Key))
                           .Select(item => new MissionInstance(new Vector3f(item.StartX, item.StartY, item.StartZ), item.Rank))
                           .ToList();

                missionStat.Breakdowns["Contact"].Add(new MissionTypeStatBreakDownData(Conversion.ConvertCharacterContact(subStat.Key), subStat.Value, instances));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private String CreateMissionContactQuery(MissionContentParams parameters)
        {
            // We want to replicate a query along the following lines:
            //
            // SELECT
            //     Data_Gen_CharacterContact AS Key, COUNT(*) AS Value
            // FROM
            // (
            //     SELECT *, ROW_NUMBER() OVER(PARTITION BY RootContentId ORDER BY UpdatedDate DESC) AS HistoryIndex
            //     FROM ugc.gta5mission_metadata
            // ) AS AllMissions
            // WHERE HistoryIndex=1 AND Data_Gen_Type=0 AND Data_Gen_CharacterContact IS NOT NULL AND Data_Gen_CharacterContact != 0 AND
            //     CategoryId=1 AND DeletedDate IS NULL AND LatestVersionContentId = ContentId
            // GROUP BY Data_Gen_CharacterContact;

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("SELECT ");
            sb.AppendFormat("Data_Gen_CharacterContact AS Key, COUNT(*) AS Value ");
            sb.AppendFormat("FROM (");
            sb.AppendFormat("SELECT *, ROW_NUMBER() OVER(PARTITION BY {0} ORDER BY UpdatedDate DESC) AS HistoryIndex ",
                (parameters.LatestVersionsOnly == true ? "RootContentId" : "ContentId"));
            sb.AppendFormat("FROM ugc.gta5mission_metadata ");
            sb.AppendFormat(") AS AllMissions ");
            sb.AppendFormat("WHERE AllMissions.HistoryIndex=1 AND Data_Gen_Type = {0} AND Data_Gen_CharacterContact IS NOT NULL AND Data_Gen_CharacterContact != 0 ",
                (int)MatchType.Mission);

            // Add the list of restrictions.
            IEnumerable<String> restrictions = GetRestrictions(parameters);
            if (restrictions.Any())
            {
                sb.AppendFormat("AND {0} ", String.Join(" AND ", restrictions));
            }
            sb.AppendFormat("GROUP BY Data_Gen_CharacterContact;");
            return sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        private void AddGangAttackTimeOfDayBreakdown(ISession session, MissionContentParams parameters, MissionTypeStat gangAttackStat, IList<FinerDetailStat> details)
        {
            gangAttackStat.Breakdowns["Time of Day"] = new List<MissionTypeStatBreakDownData>();

            foreach (KeyValueStat<uint, ulong> subStat in
                session.RetrieveAll<KeyValueStat<uint, ulong>>(new StringQuery(CreateGangAttackTimeOfDayQuery(parameters))))
            {
                IList<MissionInstance> instances =
                    details.Where(item => (item.Type == MatchType.GangAttack && item.LaunchTimes == subStat.Key))
                           .Select(item => new MissionInstance(new Vector3f(item.StartX, item.StartY, item.StartZ), item.Rank))
                           .ToList();

                String friendlyTimeOfDay = TimeOfDayUtils.ConvertBitsetToTimeOfDay(subStat.Key).GetFriendlyNameValue();
                gangAttackStat.Breakdowns["Time of Day"].Add(new MissionTypeStatBreakDownData(friendlyTimeOfDay, subStat.Value, instances));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private String CreateGangAttackTimeOfDayQuery(MissionContentParams parameters)
        {
            // We want to replicate a query along the following lines:
            //
            // SELECT
            //     Data_Gen_LaunchTimes AS Key, COUNT(*) AS Value
            // FROM
            // (
            //     SELECT *, ROW_NUMBER() OVER(PARTITION BY RootContentId ORDER BY UpdatedDate DESC) AS HistoryIndex
            //     FROM ugc.gta5mission_metadata
            // ) AS AllMissions
            // WHERE HistoryIndex=1 AND Data_Gen_Type=6 AND Data_Rule_TimeOfDay IS NOT NULL AND
            //     CategoryId=1 AND DeletedDate IS NULL AND LatestVersionContentId = ContentId
            // GROUP BY Data_Gen_LaunchTimes;

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("SELECT ");
            sb.AppendFormat("Data_Gen_LaunchTimes AS Key, COUNT(*) AS Value ");
            sb.AppendFormat("FROM (");
            sb.AppendFormat("SELECT *, ROW_NUMBER() OVER(PARTITION BY {0} ORDER BY UpdatedDate DESC) AS HistoryIndex ",
                (parameters.LatestVersionsOnly == true ? "RootContentId" : "ContentId"));
            sb.AppendFormat("FROM ugc.gta5mission_metadata ");
            sb.AppendFormat(") AS AllMissions ");
            sb.AppendFormat("WHERE AllMissions.HistoryIndex=1 AND Data_Gen_Type = {0} AND Data_Rule_TimeOfDay IS NOT NULL ",
                (int)MatchType.GangAttack);

            // Add the list of restrictions.
            IEnumerable<String> restrictions = GetRestrictions(parameters);
            if (restrictions.Any())
            {
                sb.AppendFormat("AND {0} ", String.Join(" AND ", restrictions));
            }
            sb.AppendFormat("GROUP BY Data_Gen_LaunchTimes;");
            return sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        private void AddRaceVehicleClassBreakdown(ISession session, MissionContentParams parameters, MissionTypeStat raceStat, IList<FinerDetailStat> details)
        {
            raceStat.Breakdowns["Vehicle Class"] = new List<MissionTypeStatBreakDownData>();

            foreach (KeyValueStat<int, ulong> subStat in
                session.RetrieveAll<KeyValueStat<int, ulong>>(new StringQuery(CreateRaceVehicleClassQuery(parameters))))
            {
                IList<MissionInstance> instances =
                    details.Where(item => (item.Type == MatchType.Race && item.RaceClass == subStat.Key))
                           .Select(item => new MissionInstance(new Vector3f(item.StartX, item.StartY, item.StartZ), item.Rank))
                           .ToList();
            
                String friendlyVehicleClass = VehicleClassUtils.ConvertToVehicleClass(subStat.Key).GetFriendlyNameValue();
                raceStat.Breakdowns["Vehicle Class"].Add(new MissionTypeStatBreakDownData(friendlyVehicleClass, subStat.Value, instances));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private String CreateRaceVehicleClassQuery(MissionContentParams parameters)
        {
            // We want to replicate a query along the following lines:
            //
            // SELECT
            //     Data_Race_RaceClass AS Key, COUNT(*) AS Value
            // FROM
            // (
            //     SELECT *, ROW_NUMBER() OVER(PARTITION BY RootContentId ORDER BY UpdatedDate DESC) AS HistoryIndex
            //     FROM ugc.gta5mission_metadata
            // ) AS AllMissions
            // WHERE HistoryIndex=1 AND Data_Gen_Type=2 AND Data_Race_RaceClass IS NOT NULL AND
            //     CategoryId=1 AND DeletedDate IS NULL AND LatestVersionContentId = ContentId
            // GROUP BY Data_Race_RaceClass;

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("SELECT ");
            sb.AppendFormat("Data_Race_RaceClass AS Key, COUNT(*) AS Value ");
            sb.AppendFormat("FROM (");
            sb.AppendFormat("SELECT *, ROW_NUMBER() OVER(PARTITION BY {0} ORDER BY UpdatedDate DESC) AS HistoryIndex ",
                (parameters.LatestVersionsOnly == true ? "RootContentId" : "ContentId"));
            sb.AppendFormat("FROM ugc.gta5mission_metadata ");
            sb.AppendFormat(") AS AllMissions ");
            sb.AppendFormat("WHERE AllMissions.HistoryIndex=1 AND Data_Gen_Type = {0} AND Data_Race_RaceClass IS NOT NULL ",
                (int)MatchType.Race);

            // Add the list of restrictions.
            IEnumerable<String> restrictions = GetRestrictions(parameters);
            if (restrictions.Any())
            {
                sb.AppendFormat("AND {0} ", String.Join(" AND ", restrictions));
            }
            sb.AppendFormat("GROUP BY Data_Race_RaceClass;");
            return sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        private void AddRaceVehicleTypeBreakdown(ISession session, MissionContentParams parameters, MissionTypeStat deathmatchStat, IList<FinerDetailStat> details)
        {
            deathmatchStat.Breakdowns["Vehicle Type"] = new List<MissionTypeStatBreakDownData>();

            foreach (KeyValueStat<int, ulong> subStat in
                session.RetrieveAll<KeyValueStat<int, ulong>>(new StringQuery(CreateDeathmatchVehicleTypeQuery(parameters))))
            {
                IList<MissionInstance> instances =
                    details.Where(item => (item.Type == MatchType.Race && item.VehicleModels == subStat.Key))
                           .Select(item => new MissionInstance(new Vector3f(item.StartX, item.StartY, item.StartZ), item.Rank))
                           .ToList();
            
                String friendlyVehicleType = DeathmatchVehicleTypeUtils.ConvertToDeathmatchVehicleType(subStat.Key).GetFriendlyNameValue();
                deathmatchStat.Breakdowns["Vehicle Type"].Add(new MissionTypeStatBreakDownData(friendlyVehicleType, subStat.Value, instances));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private String CreateDeathmatchVehicleTypeQuery(MissionContentParams parameters)
        {
            // We want to replicate a query along the following lines:
            //
            // SELECT
            //     Data_Gen_VehicleModels AS Key, COUNT(*) AS Value
            // FROM
            // (
            //     SELECT *, ROW_NUMBER() OVER(PARTITION BY RootContentId ORDER BY UpdatedDate DESC) AS HistoryIndex
            //     FROM ugc.gta5mission_metadata
            // ) AS AllMissions
            // WHERE HistoryIndex=1 AND Data_Gen_Type=1 AND Data_Gen_VehicleModels IS NOT NULL AND
            //     CategoryId=1 AND DeletedDate IS NULL AND LatestVersionContentId = ContentId
            // GROUP BY Data_Gen_VehicleModels;

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("SELECT ");
            sb.AppendFormat("Data_Gen_VehicleModels AS Key, COUNT(*) AS Value ");
            sb.AppendFormat("FROM (");
            sb.AppendFormat("SELECT *, ROW_NUMBER() OVER(PARTITION BY {0} ORDER BY UpdatedDate DESC) AS HistoryIndex ",
                (parameters.LatestVersionsOnly == true ? "RootContentId" : "ContentId"));
            sb.AppendFormat("FROM ugc.gta5mission_metadata ");
            sb.AppendFormat(") AS AllMissions ");
            sb.AppendFormat("WHERE AllMissions.HistoryIndex=1 AND Data_Gen_Type = {0} AND Data_Gen_VehicleModels IS NOT NULL ",
                (int)MatchType.Deathmatch);

            // Add the list of restrictions.
            IEnumerable<String> restrictions = GetRestrictions(parameters);
            if (restrictions.Any())
            {
                sb.AppendFormat("AND {0} ", String.Join(" AND ", restrictions));
            }
            sb.AppendFormat("GROUP BY Data_Gen_VehicleModels;");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the list of restrictions to apply to the query.
        /// </summary>
        private IEnumerable<String> GetRestrictions(MissionContentParams parameters)
        {
            if (parameters.MissionCategories.Any())
            {
                yield return String.Format("AllMissions.CategoryId IN ({0})", String.Join(", ", parameters.MissionCategories.Select(item => (int)item)));
            }
            if (parameters.MissionCreatedStart.HasValue && parameters.MissionCreatedEnd.HasValue)
            {
                yield return String.Format("AllMissions.CreatedDate BETWEEN '{0}' AND '{1}'",
                    parameters.MissionCreatedStart.Value.ToString("u"), parameters.MissionCreatedEnd.Value.ToString("u"));
            }
            else if (parameters.MissionCreatedStart.HasValue)
            {
                yield return String.Format("AllMissions.CreatedDate >= '{0}'", parameters.MissionCreatedStart.Value.ToString("u"));
            }
            else if (parameters.MissionCreatedEnd.HasValue)
            {
                yield return String.Format("AllMissions.CreatedDate <= '{0}'", parameters.MissionCreatedEnd.Value.ToString("u"));
            }
            if (parameters.MissionPublishedOnly.HasValue)
            {
                yield return String.Format("AllMissions.PublishedDate {0}", parameters.MissionPublishedOnly.Value ? "IS NOT NULL" : "IS NULL");
            }
            if (parameters.MissionCreatorUserIdPlatformPairs.Any())
            {
                yield return String.Format("AllMissions.UserId IN ('{0}')",
                    String.Join("', '", parameters.MissionCreatorUserIdPlatformPairs.Select(item => item.Item1)));
            }
            if (parameters.LatestVersionsOnly.HasValue)
            {
                yield return String.Format("AllMissions.LatestVersionContentId {0} ContentId", (parameters.LatestVersionsOnly.Value ? "=" : "!="));
            }
            if (parameters.IncludeDeleted.HasValue)
            {
                yield return String.Format("AllMissions.DeletedDate {0}", (parameters.IncludeDeleted.Value ? "IS NOT NULL" : "IS NULL"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private String GetSubTypeName(MatchType type, int subType)
        {
            switch (type)
            {
                case MatchType.Mission:
                    return Conversion.ConvertToMissionSubType(subType);
                case MatchType.Race:
                    return Conversion.ConvertToRaceSubType(subType);
                case MatchType.Deathmatch:
                    return Conversion.ConvertToDeathmatchSubType(subType);
                default:
                    return "Unknown";
            }
        }
        #endregion // Private Methods
    } // MissionContentSummaryReport
}
