﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Linq;
using System.Text;
using RSG.ROS;
using RSG.Statistics.Common;
using RSG.Statistics.Common.Model.Telemetry;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Dto.ReportResults;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.VerticaServices.DataAccess;
using RSG.Statistics.VerticaServices.Util;

namespace RSG.Statistics.VerticaServices.Report.Telemetry.PlayerStats
{
    /// <summary>
    /// Report that provides weapon mod purchase information.
    /// </summary>
    [Export(typeof(IVerticaReport))]
    internal class WeaponModPurchaseReport : PlayerStatsReportBase<List<WeaponModPurchaseResult>, WeaponModPurchaseParams>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public WeaponModPurchaseReport()
            : base(ReportNames.WeaponModPurchaseReport)
        {
        }
        #endregion // Constructor(s)
        
        #region VerticaReportBase Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        protected override List<WeaponModPurchaseResult> Generate(ISession session, WeaponModPurchaseParams parameters)
        {
            return session.RetrieveAll<WeaponModPurchaseResult>(new StringQuery(CreateQueryString(parameters))).ToList();
        }
        #endregion // IStatsReport Implementation

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private String CreateQueryString(WeaponModPurchaseParams parameters)
        {
            // We want to replicate a query along the following lines:
            //
            // SELECT
            //     s.id AS WeaponModHash,
            //     s.ExtraIdentifier AS WeaponHash,
            //     COUNT(s.SubmissionId) AS TimesPurchased,
            //     COUNT(DISTINCT h.AccountId) AS NumGamers,
            //     SUM(s.as) AS TotalSpent
            // FROM dev_gta5_11_ps3.Telemetry_Gta5_SHOP s
            // INNER JOIN dev_gta5_11_ps3.Telemetry_Gta5_header h on h.SubmissionId=s.SubmissionId
            // WHERE h.ver=5504 AND s.id
            // GROUP BY s.id, s.ExtraIdentifier;

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("SELECT ");
            sb.AppendFormat("WeaponModHash, ");
            sb.AppendFormat("WeaponHash, ");
            sb.AppendFormat("SUM(TimesPurchased) AS TimesPurchased, ");
            sb.AppendFormat("SUM(UniqueGamers) AS UniqueGamers, ");
            sb.AppendFormat("SUM(TotalSpent) AS TotalSpent ");
            sb.AppendFormat("FROM (");
            {
                String schema = Conversion.Schema();
                sb.Append("SELECT ");
                sb.Append("WeaponModHash, ");
                sb.Append("WeaponHash, ");
                sb.Append("COUNT(*) AS TimesPurchased, ");
                sb.Append("COUNT(DISTINCT Header_AccountId) AS UniqueGamers, ");
                sb.Append("SUM(Amount) AS TotalSpent ");
                sb.Append("FROM (");
                bool requiresUnion = false;
                if (parameters.GameTypeRestriction == null || parameters.GameTypeRestriction == GameType.Singleplayer)
                {
                    sb.Append("SELECT ");
                    sb.Append("s.id AS WeaponModHash, ");
                    sb.Append("s.ExtraItemHash AS WeaponHash, ");
                    sb.Append("s.as AS Amount, ");
                    sb.Append("s.Header_AccountId ");
                    sb.AppendFormat("FROM {0}.Telemetry_Gta5_SHOP s ", schema);
                    AddEmbeddedRestrictions(sb, parameters, "s",
                        new String[] { String.Format("s.id IN ({0})", String.Join(", ", parameters.WeaponModHashes)),
                            "IFNULL(s.ExtraItemHash, 0) != 0"});

                    requiresUnion = true;
                }
                if (parameters.GameTypeRestriction == null || parameters.GameTypeRestriction == GameType.Multiplayer)
                {
                    if (requiresUnion)
                    {
                        sb.Append("UNION ALL ");
                    }

                    sb.Append("SELECT ");
                    sb.Append("s.ItemLabelHash AS WeaponModHash, ");
                    sb.Append("s.Item AS WeaponHash, ");
                    sb.Append("s.Amount, ");
                    sb.Append("s.Header_AccountId ");
                    sb.AppendFormat("FROM {0}.Telemetry_Gta5Online_SPEND2 s ", schema);
                    AddEmbeddedRestrictions(sb, parameters, "s", new String[] {
                        String.Format("s.ItemLabelHash IN ({0})", String.Join(", ", parameters.WeaponModHashes)) });
                }
                sb.Append(") AS AllShopping ");
                sb.AppendFormat("GROUP BY WeaponHash, WeaponModHash ");
            }
            sb.Append(") AS AllStats ");
            sb.Append("GROUP BY WeaponModHash, WeaponHash;");
            return sb.ToString();
        }
        #endregion // Private Methods
    } // WeaponModPurchaseReport
}
