﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common.Async;
using RSG.Statistics.VerticaServices.DataAccess;
using Vertica.Data.VerticaClient;

namespace RSG.Statistics.VerticaServices.TransManager
{
    /// <summary>
    /// Interface for objects that execute transactions.
    /// </summary>
    internal interface ITransManager : IDisposable
    {
        #region Properties
        /// <summary>
        /// Results associated with an asynchronous task.
        /// </summary>
        IAsyncCommandResult AsyncTaskResults { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Execute an action that doesn't require a return value.
        /// </summary>
        /// <param name="command"></param>
        void ExecuteCommand(Action<ISession> command);

        /// <summary>
        /// Execute an action that returns a result.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="command"></param>
        /// <returns></returns>
        T ExecuteCommand<T>(Func<ISession, T> command);

        /// <summary>
        /// Execute an action asynchronously.
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        IAsyncCommandResult ExecuteAsyncCommand<T>(Func<ISession, IAsyncCommandResult, T> command);
        #endregion // Methods
    } // ITransManager
}
