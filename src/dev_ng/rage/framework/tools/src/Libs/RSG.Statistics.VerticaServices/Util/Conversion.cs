﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using RSG.Base.Configuration.ROS;
using RSG.Base.Extensions;
using RSG.ROS;
using RSG.Base.Configuration;
using RSG.Statistics.Common;
using RSG.Statistics.Common.Config;

namespace RSG.Statistics.VerticaServices.Util
{
    /// <summary>
    /// Contains common conversion methods.
    /// </summary>
    internal static class Conversion
    {
        /// <summary>
        /// Server config object.
        /// </summary>
        internal static IVerticaServer ServerConfig { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        internal static String Schema()
        {
            return ServerConfig.SchemaPrefix;
        }

        /// <summary>
        /// Retrieves the shared R* social club schema name.
        /// </summary>
        internal static String SocialClubSchema()
        {
            return "SocialClub";
        }

        /// <summary>
        /// Retrieves the UGC schema name.
        /// </summary>
        /// <returns></returns>
        internal static String UGCSchema()
        {
            return "ugc";
        }

        /// <summary>
        /// Convert the grouping mode enum to a Vertica SQL datepart t be used with the TIMESTAMPADD and DATEDIFF functions.
        /// </summary>
        internal static String GetGroupModeFunction(DateTimeGroupMode groupMode)
        {
            switch (groupMode)
            {
                case DateTimeGroupMode.FiveMinutely:
                    return "MINUTE";
                case DateTimeGroupMode.QuarterHourly:
                    return "MINUTE";
                case DateTimeGroupMode.HalfHourly:
                    return "MINUTE";
                case DateTimeGroupMode.Hourly:
                    return "HOUR";
                case DateTimeGroupMode.Daily:
                    return "DAY";
                case DateTimeGroupMode.Weekly:
                    return "WEEK";
                case DateTimeGroupMode.Monthly:
                    return "MONTH";
                case DateTimeGroupMode.Quarterly:
                    return "QUARTER";
                case DateTimeGroupMode.Yearly:
                    return "YEAR";
                default:
                    return "DAY";
            }
        }

        /// <summary>
        /// Converts a character contact hash into a friendly string.
        /// </summary>
        /// <param name="contactHash"></param>
        /// <returns></returns>
        internal static String ConvertCharacterContact(int contactHash)
        {
            switch (contactHash)
            {
                case 0:
                    return null;
                case 131908481:
                    return "Gerald";
                case -366822323:
                    return "Lamar";
                case -1917614010:
                    return "Lester";
                case -328739832:
                    return "Martin";
                case -1984782235:
                    return "Ron";
                case -2105450473:
                    return "Simeon";
                case 657970604:
                    return "Trevor";
                default:
                    return String.Format("Unknown Contact (Hash: {0})", contactHash);
            }
        }


        /// <summary>
        /// Converts a race sub type integer to an enum value.
        /// </summary>
        internal static String ConvertToRaceSubType(int value)
        {
            RaceSubType type;
            if (Enum.IsDefined(typeof(RaceSubType), value))
            {
                type = (RaceSubType)value;
            }
            else
            {
                type = RaceSubType.Unknown;
            }
            return type.GetFriendlyNameValue();
        }

        /// <summary>
        /// Converts a deathmatch sub type integer to an enum value.
        /// </summary>
        internal static String ConvertToDeathmatchSubType(int value)
        {
            DeathmatchSubType type;
            if (Enum.IsDefined(typeof(DeathmatchSubType), value))
            {
                type = (DeathmatchSubType)value;
            }
            else
            {
                type = DeathmatchSubType.Unknown;
            }
            return type.GetFriendlyNameValue();
        }

        /// <summary>
        /// Converts a mission sub type integer to an enum value.
        /// </summary>
        internal static String ConvertToMissionSubType(int value)
        {
            MissionSubType type;
            if (Enum.IsDefined(typeof(MissionSubType), value))
            {
                type = (MissionSubType)value;
            }
            else
            {
                type = MissionSubType.Unknown;
            }
            return type.GetFriendlyNameValue();
        }
    } // Conversion
}
