﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.ROS;
using RSG.Statistics.Common.Dto.GameAssetStats;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.VerticaServices.DataAccess;
using RSG.Statistics.VerticaServices.Util;
using Vertica.Data.VerticaClient;

namespace RSG.Statistics.VerticaServices.Report.Telemetry.PlayerStats
{
    /// <summary>
    /// Report that provides vehicle distance driven statistics.
    /// </summary>
    [Export(typeof(IVerticaReport))]
    internal class VehicleDistanceDrivenReport : PlayerStatsReportBase<List<VehicleCategoryUsageStat>, VehicleTelemetryParams>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public VehicleDistanceDrivenReport()
            : base(ReportNames.VehicleDistanceDrivenReport)
        {
        }
        #endregion // Constructor(s)

        #region VerticaReportBase Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        protected override List<VehicleCategoryUsageStat> Generate(ISession session, VehicleTelemetryParams parameters)
        {
            // Get the stats we are after
            List<VehicleCategoryUsageStat> categoryStats =
                session.RetrieveAll<VehicleCategoryUsageStat>(new StringQuery(CreateQuery(parameters, false))).ToList();
            IDictionary<VehicleCategory, VehicleCategoryUsageStat> categoryStatLookup =
                categoryStats.ToDictionary(item => item.Category);

            // Add the per vehicle stats to the categories
            IEnumerable<VehicleUsageStat> vehicleStats =
                session.RetrieveAll<VehicleUsageStat>(new StringQuery(CreateQuery(parameters, true)));
            foreach (VehicleUsageStat vehicleStat in vehicleStats)
            {
                VehicleCategory category;
                if (!parameters.Vehicles.TryGetValue(vehicleStat.Hash, out category))
                {
                    category = VehicleCategory.Unknown;
                }
                VehicleCategoryUsageStat categoryStat = categoryStatLookup[category];
                categoryStat.PerVehicleStats.Add(vehicleStat);
            }

            return categoryStats;
        }
        #endregion // IStatsReport Implementation

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private String CreateQuery(VehicleTelemetryParams parameters, bool perVehicle)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("SELECT ");
            if (perVehicle)
            {
                sb.Append("VehicleHash AS Hash, ");
            }
            else
            {
                sb.AppendFormat("VehicleCategory AS Category, ");
            }
            sb.AppendFormat("SUM(DistanceDriven) AS DistanceDriven, ");
            sb.AppendFormat("SUM(TimesDriven) AS TimesDriven, ");
            sb.AppendFormat("SUM(UniqueGamers) AS UniqueGamers ");
            sb.AppendFormat("FROM (");
            {
                String schema = Conversion.Schema();

                sb.Append("SELECT ");
                if (perVehicle)
                {
                    sb.Append("d.name AS VehicleHash, ");
                }
                else
                {
                    sb.Append("CASE ");
                    foreach (IGrouping<VehicleCategory, uint> categoryGroup in parameters.Vehicles.GroupBy(item => item.Value, item => item.Key))
                    {
                        sb.AppendFormat("WHEN d.name IN ({0}) THEN '{1}' ", String.Join(", ", categoryGroup), categoryGroup.Key.ToString());
                    }
                    sb.Append("ELSE 'Unknown' ");
                    sb.Append("END AS VehicleCategory, ");
                }
                sb.Append("SUM(d.dist) AS DistanceDriven, ");
                sb.Append("COUNT(d.SubmissionId) AS TimesDriven, ");
                sb.Append("COUNT(DISTINCT(d.Header_AccountId)) AS UniqueGamers ");
                sb.AppendFormat("FROM {0}.Telemetry_Gta5_VEHICLE_DIST_DRIVEN d ", schema);
                AddEmbeddedRestrictions(sb, parameters, "d");
                sb.AppendFormat("GROUP BY {0} ", (perVehicle ? "VehicleHash" : "VehicleCategory"));
            }
            sb.Append(") AS AllStats ");
            sb.AppendFormat("GROUP BY {0};", (perVehicle ? "VehicleHash" : "VehicleCategory"));
            return sb.ToString();
        }
        #endregion // Private Methods
    } // VehicleDistanceDrivenReport
}
