﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Linq;
using System.Text;
using RSG.ROS;
using RSG.Statistics.Common.Model.Telemetry;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.VerticaServices.DataAccess;
using RSG.Statistics.VerticaServices.Util;
using Vertica.Data.VerticaClient;

namespace RSG.Statistics.VerticaServices.Report.Telemetry.PlayerStats
{
    /// <summary>
    /// Report that provides wanted level statistics.
    /// </summary>
    [Export(typeof(IVerticaReport))]
    internal class WantedLevelReport : PlayerStatsReportBase<List<WantedLevelStat>, TelemetryParams>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public WantedLevelReport()
            : base(ReportNames.WantedLevelReport)
        {
        }
        #endregion // Constructor(s)

        #region VerticaReportBase Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        protected override List<WantedLevelStat> Generate(ISession session, TelemetryParams parameters)
        {
            List<WantedLevelStat> stats = new List<WantedLevelStat>();

            VerticaDataReader dr = session.ExecuteQuery(CreateSqlQueryString(parameters));
            while (dr.Read())
            {
                WantedLevelStat stat = new WantedLevelStat();
                stat.Level = dr.GetUInt32(0);
                stat.TimesAchieved = dr.GetUInt64(1);
                stat.UniqueGamers = dr.GetUInt32(2);
                stats.Add(stat);
            }
            return stats;
        }
        #endregion // IStatsReport Implementation

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private String CreateSqlQueryString(TelemetryParams parameters)
        {
            // We want to replicate a query along the following lines:
            //
            // SELECT
            //     wl.wantedlvl AS WantedLevel,
            //     COUNT(*) AS TimesAchieved,
            //     COUNT(DISTINCT(h.AccountId)) AS NumGamers
            // FROM dev_gta5_11_ps3.Telemetry_Gta5_WANTED_LEVEL wl
            // INNER JOIN dev_gta5_11_ps3.Telemetry_Gta5_header h ON h.SubmissionId=wl.SubmissionId
            // WHERE h.ver=5632 AND wl.wantedlvl != 0
            // GROUP BY wl.wantedlvl;

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("SELECT ");
            sb.AppendFormat("WantedLevel, ");
            sb.AppendFormat("SUM(TimesAchieved) AS TimesAchieved, ");
            sb.AppendFormat("SUM(NumGamers) AS NumGamers ");
            sb.AppendFormat("FROM (");
            {
                String schema = Conversion.Schema();
                sb.Append("SELECT ");
                sb.Append("wl.wantedlvl AS WantedLevel, ");
                sb.Append("COUNT(*) AS TimesAchieved, ");
                sb.Append("COUNT(DISTINCT(h.AccountId)) AS NumGamers ");
                sb.AppendFormat("FROM {0}.Telemetry_Gta5_WANTED_LEVEL wl ", schema);
                sb.AppendFormat("INNER JOIN {0}.Telemetry_Gta5_header h ON h.SubmissionId=wl.SubmissionId ", schema);
                AddRestrictions(sb, parameters, "wl", new String[] { "wl.wantedlvl != 0" });
                sb.AppendFormat("GROUP BY wl.wantedlvl ");
            }
            sb.Append(") AS AllStats ");
            sb.Append("GROUP BY WantedLevel;");
            return sb.ToString();
        }
        #endregion // Private Methods
    } // WantedLevelReport
}
