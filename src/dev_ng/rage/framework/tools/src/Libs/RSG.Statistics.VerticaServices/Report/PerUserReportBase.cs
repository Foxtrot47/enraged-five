﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.ROS;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;

namespace RSG.Statistics.VerticaServices.Report
{
    /// <summary>
    /// Base class for per user based reports.
    /// </summary>
    internal abstract class PerUserReportBase<TResult, TParams> : VerticaReportBase<TResult, TParams>
        where TResult : class
        where TParams : PerUserParams
    {
        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        protected PerUserReportBase(String name)
            : base(name)
        {
        }
        #endregion // Constructor(s)

        #region Protected Methods
        /// <summary>
        /// Adds the required restrictions to the string builder.
        /// </summary>
        protected void AddRestrictions(StringBuilder sb, TParams parameters, String tableAlias, ROSPlatform platform)
        {
            // Get the list of restrictions and add them to the query string if we got some.
            IEnumerable<String> restrictions = GetRestrictions(parameters, tableAlias, platform);
            if (restrictions.Any())
            {
                sb.Append(String.Format(" WHERE "));
                sb.Append(String.Join(" AND ", restrictions));
            }
        }

        /// <summary>
        /// Retrieves a list of restrictions that should be added to the sql query string.
        /// </summary>
        protected virtual IEnumerable<String> GetRestrictions(TParams parameters, String tableAlias, ROSPlatform platform)
        {
            yield return String.Format("{0}.PlatformId = {1}", tableAlias, (int)platform);

            // Did the user supply start/end dates?
            if (parameters.StartDate.HasValue && parameters.EndDate.HasValue)
            {
                yield return (tableAlias + ".DateTime BETWEEN '" + parameters.StartDate.Value.ToString("u") +
                    "' AND '" + parameters.EndDate.Value.ToString("u") + "'");
            }
            else if (parameters.StartDate.HasValue)
            {
                yield return (tableAlias + ".DateTime > '" + parameters.StartDate.Value.ToString("u") + "'");
            }
            else if (parameters.EndDate.HasValue)
            {
                yield return (tableAlias + ".DateTime < '" + parameters.EndDate.Value.ToString("u") + "'");
            }

            IEnumerable<String> gamerHandles = parameters.GamerHandlePlatformPairs.Where(item => item.Item2 == platform).Select(item => item.Item1);
            if (gamerHandles.Any())
            {
                yield return (String.Format("pa.UserName IN ('{0}')", String.Join("', '", gamerHandles)));
            }
        }
        #endregion // Protected Methods
    } // PerUserReportBase
}
