﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Linq;
using System.Text;
using RSG.ROS;
using RSG.Statistics.Common.Dto.ProfileStats;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.VerticaServices.Util;
using Vertica.Data.VerticaClient;
using RSG.Base.Security.Cryptography;
using RSG.Statistics.VerticaServices.DataAccess;

namespace RSG.Statistics.VerticaServices.Report.Profile
{
    /// <summary>
    /// 
    /// </summary>
    [Export(typeof(IVerticaReport))]
    internal class DividedDiffProfileStatReport : ProfileStatReportBase<DividedProfileStatParams>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public DividedDiffProfileStatReport()
            : base(ReportNames.DividedDiffProfileStatReport)
        {
        }
        #endregion // Constructor(s)

        #region VerticaReportBase Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        protected override List<ProfileStatDto> Generate(ISession session, DividedProfileStatParams parameters)
        {
            // Determine which sql query we should create based on the input parameters.
            String sql;
            if (parameters.BucketSize.HasValue)
            {
                sql = CreateBucketBasedQuery(parameters);
            }
            else
            {
                sql = CreateSoloQuery(parameters);
            }

            // Run the query against vertica.
            VerticaDataReader dr = session.ExecuteQuery(sql);

            // Convert the results to a nicer temporary format.
            List<ProfileStatDto> stats = new List<ProfileStatDto>();
            while (dr.Read())
            {
                ProfileStatDto stat = new ProfileStatDto(dr.GetDouble(0), (parameters.BucketSize.HasValue ? parameters.BucketSize.Value : 0.0));

                if (dr[1] != DBNull.Value)
                {
                    stat.Total = dr.GetDecimal(1).ToString();
                }
                if (dr[2] != DBNull.Value)
                {
                    stat.YValue = dr.GetUInt64(2).ToString();
                }

                stats.Add(stat);
            }

            return stats.OrderBy(item => item.BucketMin).ToList();
        }
        #endregion // IStatsReport Implementation

        #region Private Methods
        /// <summary>
        /// Creates the query to bucket the data into the requested bucket sizes.
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private String CreateBucketBasedQuery(DividedProfileStatParams parameters)
        {
            // We want to replicate a query along the following lines:
            // (SCARY!!!!)
            //
            // SELECT
            //     Bucket,
            //     SUM(Value) AS Total,
            //     COUNT(DISTINCT(PlayerAccountId)) AS Gamers
            // FROM
            // (
            //     SELECT
            //         IFNULL(NumeratorStats.Value, 0) / DenominatorStats.Value AS Value,
            //         DenominatorStats.GamerHandle AS GamerHandle,
            //         FLOOR((IFNULL(NumeratorStats.Value, 0) / DenominatorStats.Value) / 1.3888888888888888e-7) * 1.3888888888888888e-7 AS Bucket
            //     FROM
            //     (
            //         SELECT
            //             CurrentStats.SummedValues - CASE WHEN PreviousStats.SummedValues IS NULL OR PreviousStats.SummedValues > CurrentStats.SummedValues THEN 0 ELSE PreviousStats.SummedValues END AS Value,
            //             CurrentStats.GamerHandle AS GamerHandle,
            //             CurrentStats.Platform AS Platform
            //         FROM
            //         (
            //             SELECT
            //                 SUM(Value) AS SummedValues,
            //                 Platform,
            //                 GamerHandle
            //             FROM
            //             (
            //                 SELECT 'PS3' AS Platform, GamerHandle, LastUpdated,
            //                     ROW_NUMBER() OVER(PARTITION BY PlayerAccountId, StatId ORDER BY LastUpdated DESC) AS HistoryIndex,
            //                     CASE
            //                         WHEN IntegerValue IS NOT NULL THEN IntegerValue
            //                         WHEN FloatValue IS NOT NULL THEN FloatValue
            //                         ELSE 0.0
            //                     END AS Value
            //                 FROM dev_gta5_11_ps3.ProfileStats2
            //                 WHERE LastUpdated BETWEEN '2013-06-01' AND '2013-06-15' AND StatName IN ('SP0_DEATHS', 'SP1_DEATHS', 'SP2_DEATHS', 'MP0_DEATHS', 'MP1_DEATHS', 'MP2_DEATHS', 'MP3_DEATHS', 'MP4_DEATHS')
            //             ) AS AllStats
            //             WHERE HistoryIndex = 1
            //             GROUP BY Platform, PlayerAccountId
            //         ) AS CurrentStats
            //         LEFT JOIN
            //         (
            //             SELECT
            //                 SUM(Value) AS SummedValues,
            //                 Platform,
            //                 GamerHandle
            //             FROM
            //             (
            //                 SELECT 'PS3' AS Platform, GamerHandle, LastUpdated,
            //                     ROW_NUMBER() OVER(PARTITION BY PlayerAccountId, StatId ORDER BY LastUpdated DESC) AS HistoryIndex,
            //                     CASE
            //                         WHEN IntegerValue IS NOT NULL THEN IntegerValue
            //                         WHEN FloatValue IS NOT NULL THEN FloatValue
            //                         ELSE 0.0
            //                     END AS Value
            //                 FROM dev_gta5_11_ps3.ProfileStats2
            //                 WHERE LastUpdated < '2013-06-01' AND StatName IN ('SP0_DEATHS', 'SP1_DEATHS', 'SP2_DEATHS', 'MP0_DEATHS', 'MP1_DEATHS', 'MP2_DEATHS', 'MP3_DEATHS', 'MP4_DEATHS')
            //             ) AS AllStats
            //             WHERE HistoryIndex = 1
            //             GROUP BY Platform, PlayerAccountId
            //         ) AS PreviousStats
            //         ON CurrentStats.GamerHandle = PreviousStats.GamerHandle AND CurrentStats.Platform = PreviousStats.Platform
            //     ) AS NumeratorStats
            //     RIGHT JOIN
            //     (
            //         SELECT
            //             CurrentStats.SummedValues - CASE WHEN PreviousStats.SummedValues IS NULL OR PreviousStats.SummedValues > CurrentStats.SummedValues THEN 0 ELSE PreviousStats.SummedValues END AS Value,
            //             CurrentStats.GamerHandle AS GamerHandle,
            //             CurrentStats.Platform AS Platform
            //         FROM
            //         (
            //             SELECT
            //                 SUM(Value) AS SummedValues,
            //                 Platform,
            //                 GamerHandle
            //             FROM
            //             (
            //                 SELECT 'PS3' AS Platform, GamerHandle, LastUpdated,
            //                     ROW_NUMBER() OVER(PARTITION BY PlayerAccountId, StatId ORDER BY LastUpdated DESC) AS HistoryIndex,
            //                     CASE
            //                         WHEN IntegerValue IS NOT NULL THEN IntegerValue
            //                         WHEN FloatValue IS NOT NULL THEN FloatValue
            //                         ELSE 0.0
            //                     END AS Value
            //                 FROM dev_gta5_11_ps3.ProfileStats2
            //                 WHERE LastUpdated BETWEEN '2013-06-01' AND '2013-06-15' AND StatName IN ('SP0_TOTAL_PLAYING_TIME', 'SP1_TOTAL_PLAYING_TIME', 'SP2_TOTAL_PLAYING_TIME', 'MP0_TOTAL_PLAYING_TIME', 'MP1_TOTAL_PLAYING_TIME', 'MP2_TOTAL_PLAYING_TIME', 'MP3_TOTAL_PLAYING_TIME', 'MP4_TOTAL_PLAYING_TIME')
            //             ) AS AllStats
            //             WHERE HistoryIndex = 1
            //             GROUP BY Platform, PlayerAccountId
            //         ) AS CurrentStats
            //         LEFT JOIN
            //         (
            //             SELECT
            //                 SUM(Value) AS SummedValues,
            //                 Platform,
            //                 GamerHandle
            //             FROM
            //             (
            //                 SELECT 'PS3' AS Platform, GamerHandle, LastUpdated,
            //                     ROW_NUMBER() OVER(PARTITION BY PlayerAccountId, StatId ORDER BY LastUpdated DESC) AS HistoryIndex,
            //                     CASE
            //                         WHEN IntegerValue IS NOT NULL THEN IntegerValue
            //                         WHEN FloatValue IS NOT NULL THEN FloatValue
            //                         ELSE 0.0
            //                     END AS Value
            //                 FROM dev_gta5_11_ps3.ProfileStats2
            //                 WHERE LastUpdated < '2013-06-01' AND StatName IN ('SP0_TOTAL_PLAYING_TIME', 'SP1_TOTAL_PLAYING_TIME', 'SP2_TOTAL_PLAYING_TIME', 'MP0_TOTAL_PLAYING_TIME', 'MP1_TOTAL_PLAYING_TIME', 'MP2_TOTAL_PLAYING_TIME', 'MP3_TOTAL_PLAYING_TIME', 'MP4_TOTAL_PLAYING_TIME')
            //             ) AS AllStats
            //             WHERE HistoryIndex = 1
            //             GROUP BY Platform, PlayerAccountId
            //         ) AS PreviousStats
            //         ON CurrentStats.GamerHandle = PreviousStats.GamerHandle AND CurrentStats.Platform = PreviousStats.Platform
            //     ) AS DenominatorStats
            //     ON NumeratorStats.GamerHandle = DenominatorStats.GamerHandle AND NumeratorStats.Platform = DenominatorStats.Platform
            //     WHERE DenominatorStats.Value <> 0
            // ) AS PerGamerStats
            // GROUP BY Bucket;

            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT ");
            sb.Append("Bucket, ");
            sb.Append("SUM(Value) AS Total, ");
            sb.Append("COUNT(DISTINCT(PlayerAccountId)) AS Gamers ");
            sb.Append("FROM (");
            sb.Append("SELECT ");
            sb.Append("IFNULL(NumeratorStats.Value, 0) / DenominatorStats.Value AS Value, ");
            sb.Append("DenominatorStats.PlayerAccountId AS PlayerAccountId, ");
            sb.AppendFormat("FLOOR((IFNULL(NumeratorStats.Value, 0) / DenominatorStats.Value) / {0}) * {0} AS Bucket ", parameters.BucketSize);
            sb.Append("FROM (");
            sb.Append("SELECT ");
            sb.Append("CurrentStats.SummedValues - CASE WHEN PreviousStats.SummedValues IS NULL OR PreviousStats.SummedValues > CurrentStats.SummedValues THEN 0 ELSE PreviousStats.SummedValues END AS Value, ");
            sb.Append("CurrentStats.PlayerAccountId AS PlayerAccountId, ");
            sb.Append("CurrentStats.PlatformId AS PlatformId ");
            sb.Append("FROM (");
            sb.Append("SELECT ");
            sb.Append("SUM(Value) AS SummedValues, ");
            sb.Append("PlayerAccountId, ");
            sb.Append("PlatformId ");
            sb.Append("FROM (");
            {
                sb.Append("SELECT PlatformId, PlayerAccountId, ");
                sb.Append("ROW_NUMBER() OVER(PARTITION BY PlayerAccountId, StatId ORDER BY LastUpdated DESC) AS HistoryIndex, ");
                sb.Append("COALESCE(IntegerValue, FloatValue, 0) AS Value ");
                sb.AppendFormat("FROM {0}.ProfileStats2 p ", Conversion.Schema());
                AddCurrentRestrictions(sb, parameters, parameters.NumeratorStatNames.Select(item => item.Item1), "p");
            }
            sb.Append(") AS AllCurrentStats ");
            sb.Append("WHERE HistoryIndex = 1 ");
            sb.Append("GROUP BY PlatformId, PlayerAccountId ");
            sb.Append(") AS CurrentStats ");
            sb.Append("LEFT JOIN ( ");
            sb.Append("SELECT ");
            sb.Append("SUM(Value) AS SummedValues, ");
            sb.Append("PlayerAccountId, ");
            sb.Append("PlatformId ");
            sb.Append("FROM (");
            {
                sb.Append("SELECT PlatformId, PlayerAccountId, ");
                sb.Append("ROW_NUMBER() OVER(PARTITION BY PlayerAccountId, StatId ORDER BY LastUpdated DESC) AS HistoryIndex, ");
                sb.Append("COALESCE(IntegerValue, FloatValue, 0) AS Value ");
                sb.AppendFormat("FROM {0}.ProfileStats2 p ", Conversion.Schema());
                AddPreviousRestrictions(sb, parameters, parameters.NumeratorStatNames.Select(item => item.Item1), "p");
            }
            sb.Append(") AS AllPreviousStats ");
            sb.Append("WHERE HistoryIndex = 1 ");
            sb.Append("GROUP BY PlatformId, PlayerAccountId ");
            sb.Append(") AS PreviousStats ");
            sb.Append("ON CurrentStats.PlayerAccountId = PreviousStats.PlayerAccountId AND CurrentStats.PlatformId = PreviousStats.PlatformId ");
            sb.Append(") AS NumeratorStats ");
            sb.Append("RIGHT JOIN ( ");
            sb.Append("SELECT ");
            sb.Append("CurrentStats.SummedValues - CASE WHEN PreviousStats.SummedValues IS NULL OR PreviousStats.SummedValues > CurrentStats.SummedValues THEN 0 ELSE PreviousStats.SummedValues END AS Value, ");
            sb.Append("CurrentStats.PlayerAccountId AS PlayerAccountId, ");
            sb.Append("CurrentStats.PlatformId AS PlatformId ");
            sb.Append("FROM (");
            sb.Append("SELECT ");
            sb.Append("SUM(Value) AS SummedValues, ");
            sb.Append("PlayerAccountId, ");
            sb.Append("PlatformId ");
            sb.Append("FROM (");
            {
                sb.Append("SELECT PlatformId, PlayerAccountId, ");
                sb.Append("ROW_NUMBER() OVER(PARTITION BY PlayerAccountId, StatId ORDER BY LastUpdated DESC) AS HistoryIndex, ");
                sb.Append("COALESCE(IntegerValue, FloatValue, 0) AS Value ");
                sb.AppendFormat("FROM {0}.ProfileStats2 p ", Conversion.Schema());
                AddCurrentRestrictions(sb, parameters, parameters.DenominatorStatNames.Select(item => item.Item1), "p");
            }
            sb.Append(") AS AllCurrentStats ");
            sb.Append("WHERE HistoryIndex = 1 ");
            sb.Append("GROUP BY PlatformId, PlayerAccountId ");
            sb.Append(") AS CurrentStats ");
            sb.Append("LEFT JOIN ( ");
            sb.Append("SELECT ");
            sb.Append("SUM(Value) AS SummedValues, ");
            sb.Append("PlayerAccountId, ");
            sb.Append("PlatformId ");
            sb.Append("FROM (");
            {
                sb.Append("SELECT PlatformId, PlayerAccountId, ");
                sb.Append("ROW_NUMBER() OVER(PARTITION BY PlayerAccountId, StatId ORDER BY LastUpdated DESC) AS HistoryIndex, ");
                sb.Append("COALESCE(IntegerValue, FloatValue, 0) AS Value ");
                sb.AppendFormat("FROM {0}.ProfileStats2 p ", Conversion.Schema());
                AddPreviousRestrictions(sb, parameters, parameters.DenominatorStatNames.Select(item => item.Item1), "p");
            }
            sb.Append(") AS AllPreviousStats ");
            sb.Append("WHERE HistoryIndex = 1 ");
            sb.Append("GROUP BY PlatformId, PlayerAccountId ");
            sb.Append(") AS PreviousStats ");
            sb.Append("ON CurrentStats.PlayerAccountId = PreviousStats.PlayerAccountId AND CurrentStats.PlatformId = PreviousStats.PlatformId ");
            sb.Append(") AS DenominatorStats ");
            sb.Append("ON NumeratorStats.PlayerAccountId = DenominatorStats.PlayerAccountId AND NumeratorStats.PlatformId = DenominatorStats.PlatformId ");
            sb.Append("WHERE DenominatorStats.Value <> 0 ");
            sb.Append(") AS PerGamerStats ");
            sb.Append("GROUP BY Bucket;");
            return sb.ToString();
        }

        /// <summary>
        /// Creates the query to return the single value for the requested stats.
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private String CreateSoloQuery(DividedProfileStatParams parameters)
        {
            // We want to replicate a query along the following lines:
            //
            // SELECT
            //     0 AS Bucket,
            //     SUM(IFNULL(NumeratorStats.Value, 0) / DenominatorStats.Value) AS Total,
            //     COUNT(DISTINCT(DenominatorStats.GamerHandle)) AS GamerHandle
            // FROM
            // (
            //     SELECT
            //         CurrentStats.SummedValues - IFNULL(PreviousStats.SummedValues, 0) AS Value,
            //         CurrentStats.GamerHandle AS GamerHandle,
            //         CurrentStats.Platform AS Platform
            //     FROM
            //     (
            //         SELECT
            //             SUM(Value) AS SummedValues,
            //             Platform,
            //             GamerHandle
            //         FROM
            //         (
            //             SELECT 'PS3' AS Platform, GamerHandle, LastUpdated,
            //                 ROW_NUMBER() OVER(PARTITION BY PlayerAccountId, StatId ORDER BY LastUpdated DESC) AS HistoryIndex,
            //                 CASE
            //                     WHEN IntegerValue IS NOT NULL THEN IntegerValue
            //                     WHEN FloatValue IS NOT NULL THEN FloatValue
            //                     ELSE 0.0
            //                 END AS Value
            //             FROM dev_gta5_11_ps3.ProfileStats2
            //             WHERE LastUpdated BETWEEN '2013-06-01' AND '2013-06-15' AND StatName IN ('SP0_DEATHS', 'SP1_DEATHS', 'SP2_DEATHS', 'MP0_DEATHS', 'MP1_DEATHS', 'MP2_DEATHS', 'MP3_DEATHS', 'MP4_DEATHS')
            //         ) AS AllStats
            //         WHERE HistoryIndex = 1
            //         GROUP BY Platform, PlayerAccountId
            //     ) AS CurrentStats
            //     LEFT JOIN
            //     (
            //         SELECT
            //             SUM(Value) AS SummedValues,
            //             Platform,
            //             GamerHandle
            //         FROM
            //         (
            //             SELECT 'PS3' AS Platform, GamerHandle, LastUpdated,
            //                 ROW_NUMBER() OVER(PARTITION BY PlayerAccountId, StatId ORDER BY LastUpdated DESC) AS HistoryIndex,
            //                 CASE
            //                     WHEN IntegerValue IS NOT NULL THEN IntegerValue
            //                     WHEN FloatValue IS NOT NULL THEN FloatValue
            //                     ELSE 0.0
            //                 END AS Value
            //             FROM dev_gta5_11_ps3.ProfileStats2
            //             WHERE LastUpdated < '2013-06-01' AND StatName IN ('SP0_DEATHS', 'SP1_DEATHS', 'SP2_DEATHS', 'MP0_DEATHS', 'MP1_DEATHS', 'MP2_DEATHS', 'MP3_DEATHS', 'MP4_DEATHS')
            //         ) AS AllStats
            //         WHERE HistoryIndex = 1
            //         GROUP BY Platform, PlayerAccountId
            //     ) AS PreviousStats
            //     ON CurrentStats.GamerHandle = PreviousStats.GamerHandle AND CurrentStats.Platform = PreviousStats.Platform
            //     WHERE CurrentStats.SummedValues >= IFNULL(PreviousStats.SummedValues, 0)
            // ) AS NumeratorStats
            // RIGHT JOIN
            // (
            //     SELECT
            //         CurrentStats.SummedValues - IFNULL(PreviousStats.SummedValues, 0) AS Value,
            //         CurrentStats.GamerHandle AS GamerHandle,
            //         CurrentStats.Platform AS Platform
            //     FROM
            //     (
            //         SELECT
            //             SUM(Value) AS SummedValues,
            //             Platform,
            //             GamerHandle
            //         FROM
            //         (
            //             SELECT 'PS3' AS Platform, GamerHandle, LastUpdated,
            //                 ROW_NUMBER() OVER(PARTITION BY PlayerAccountId, StatId ORDER BY LastUpdated DESC) AS HistoryIndex,
            //                 CASE
            //                     WHEN IntegerValue IS NOT NULL THEN IntegerValue
            //                     WHEN FloatValue IS NOT NULL THEN FloatValue
            //                     ELSE 0.0
            //                 END AS Value
            //             FROM dev_gta5_11_ps3.ProfileStats2
            //             WHERE LastUpdated BETWEEN '2013-06-01' AND '2013-06-15' AND StatName IN ('SP0_TOTAL_PLAYING_TIME', 'SP1_TOTAL_PLAYING_TIME', 'SP2_TOTAL_PLAYING_TIME', 'MP0_TOTAL_PLAYING_TIME', 'MP1_TOTAL_PLAYING_TIME', 'MP2_TOTAL_PLAYING_TIME', 'MP3_TOTAL_PLAYING_TIME', 'MP4_TOTAL_PLAYING_TIME')
            //         ) AS AllStats
            //         WHERE HistoryIndex = 1
            //         GROUP BY Platform, PlayerAccountId
            //     ) AS CurrentStats
            //     LEFT JOIN
            //     (
            //         SELECT
            //             SUM(Value) AS SummedValues,
            //             Platform,
            //             GamerHandle
            //         FROM
            //         (
            //             SELECT 'PS3' AS Platform, GamerHandle, LastUpdated,
            //                 ROW_NUMBER() OVER(PARTITION BY PlayerAccountId, StatId ORDER BY LastUpdated DESC) AS HistoryIndex,
            //                 CASE
            //                     WHEN IntegerValue IS NOT NULL THEN IntegerValue
            //                     WHEN FloatValue IS NOT NULL THEN FloatValue
            //                     ELSE 0.0
            //                 END AS Value
            //             FROM dev_gta5_11_ps3.ProfileStats2
            //             WHERE LastUpdated < '2013-06-01' AND StatName IN ('SP0_TOTAL_PLAYING_TIME', 'SP1_TOTAL_PLAYING_TIME', 'SP2_TOTAL_PLAYING_TIME', 'MP0_TOTAL_PLAYING_TIME', 'MP1_TOTAL_PLAYING_TIME', 'MP2_TOTAL_PLAYING_TIME', 'MP3_TOTAL_PLAYING_TIME', 'MP4_TOTAL_PLAYING_TIME')
            //         ) AS AllStats
            //         WHERE HistoryIndex = 1
            //         GROUP BY Platform, PlayerAccountId
            //     ) AS PreviousStats
            //     ON CurrentStats.GamerHandle = PreviousStats.GamerHandle AND CurrentStats.Platform = PreviousStats.Platform
            //     WHERE CurrentStats.SummedValues >= IFNULL(PreviousStats.SummedValues, 0)
            // ) AS DenominatorStats
            // ON NumeratorStats.GamerHandle = DenominatorStats.GamerHandle AND NumeratorStats.Platform = DenominatorStats.Platform
            // WHERE DenominatorStats.Value <> 0;

            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT ");
            sb.Append("0 AS Bucket, ");
            sb.Append("SUM(IFNULL(NumeratorStats.Value, 0) / DenominatorStats.Value) AS Total, ");
            sb.Append("COUNT(DISTINCT(DenominatorStats.PlayerAccountId)) AS PlayerAccountId ");
            sb.Append("FROM (");
            sb.Append("SELECT ");
            sb.Append("CurrentStats.SummedValues - IFNULL(PreviousStats.SummedValues, 0) AS Value, ");
            sb.Append("CurrentStats.PlayerAccountId AS PlayerAccountId, ");
            sb.Append("CurrentStats.PlatformId AS PlatformId ");
            sb.Append("FROM (");
            sb.Append("SELECT ");
            sb.Append("SUM(Value) AS SummedValues, ");
            sb.Append("PlayerAccountId, ");
            sb.Append("PlatformId ");
            sb.Append("FROM (");
            {
                sb.Append("SELECT PlatformId, PlayerAccountId, ");
                sb.Append("ROW_NUMBER() OVER(PARTITION BY PlayerAccountId, StatId ORDER BY LastUpdated DESC) AS HistoryIndex, ");
                sb.Append("COALESCE(IntegerValue, FloatValue, 0) AS Value ");
                sb.AppendFormat("FROM {0}.ProfileStats2 p ", Conversion.Schema());
                AddCurrentRestrictions(sb, parameters, parameters.NumeratorStatNames.Select(item => item.Item1), "p");
            }
            sb.Append(") AS AllCurrentStats ");
            sb.Append("WHERE HistoryIndex = 1 ");
            sb.Append("GROUP BY PlatformId, PlayerAccountId ");
            sb.Append(") AS CurrentStats ");
            sb.Append("LEFT JOIN ( ");
            sb.Append("SELECT ");
            sb.Append("SUM(Value) AS SummedValues, ");
            sb.Append("PlayerAccountId, ");
            sb.Append("PlatformId ");
            sb.Append("FROM (");
            {
                sb.Append("SELECT PlatformId, PlayerAccountId, ");
                sb.Append("ROW_NUMBER() OVER(PARTITION BY PlayerAccountId, StatId ORDER BY LastUpdated DESC) AS HistoryIndex, ");
                sb.Append("COALESCE(IntegerValue, FloatValue, 0) AS Value ");
                sb.AppendFormat("FROM {0}.ProfileStats2 p ", Conversion.Schema());
                AddPreviousRestrictions(sb, parameters, parameters.NumeratorStatNames.Select(item => item.Item1), "p");
            }
            sb.Append(") AS AllPreviousStats ");
            sb.Append("WHERE HistoryIndex = 1 ");
            sb.Append("GROUP BY PlatformId, PlayerAccountId ");
            sb.Append(") AS PreviousStats ");
            sb.Append("ON CurrentStats.PlayerAccountId = PreviousStats.PlayerAccountId AND CurrentStats.PlatformId = PreviousStats.PlatformId ");
            sb.Append("WHERE CurrentStats.SummedValues >= IFNULL(PreviousStats.SummedValues, 0) AND ");
            sb.AppendFormat("CurrentStats.SummedValues - IFNULL(PreviousStats.SummedValues, 0) < {1} ", parameters.MaxNumeratorValue);
            sb.Append(") AS NumeratorStats ");
            sb.Append("RIGHT JOIN ( ");
            sb.Append("SELECT ");
            sb.Append("CurrentStats.SummedValues - IFNULL(PreviousStats.SummedValues, 0) AS Value, ");
            sb.Append("CurrentStats.PlayerAccountId AS PlayerAccountId, ");
            sb.Append("CurrentStats.PlatformId AS PlatformId ");
            sb.Append("FROM (");
            sb.Append("SELECT ");
            sb.Append("SUM(Value) AS SummedValues, ");
            sb.Append("PlayerAccountId, ");
            sb.Append("PlatformId ");
            sb.Append("FROM (");
            {
                sb.Append("SELECT PlatformId, PlayerAccountId, ");
                sb.Append("ROW_NUMBER() OVER(PARTITION BY PlayerAccountId, StatId ORDER BY LastUpdated DESC) AS HistoryIndex, ");
                sb.Append("COALESCE(IntegerValue, FloatValue, 0) AS Value ");
                sb.AppendFormat("FROM {0}.ProfileStats2 p ", Conversion.Schema());
                AddCurrentRestrictions(sb, parameters, parameters.DenominatorStatNames.Select(item => item.Item1), "p");
            }
            sb.Append(") AS AllCurrentStats ");
            sb.Append("WHERE HistoryIndex = 1 ");
            sb.Append("GROUP BY PlatformId, PlayerAccountId ");
            sb.Append(") AS CurrentStats ");
            sb.Append("LEFT JOIN ( ");
            sb.Append("SELECT ");
            sb.Append("SUM(Value) AS SummedValues, ");
            sb.Append("PlayerAccountId, ");
            sb.Append("PlatformId ");
            sb.Append("FROM (");
            {
                sb.Append("SELECT PlatformId, PlayerAccountId, ");
                sb.Append("ROW_NUMBER() OVER(PARTITION BY PlayerAccountId, StatId ORDER BY LastUpdated DESC) AS HistoryIndex, ");
                sb.Append("COALESCE(IntegerValue, FloatValue, 0) AS Value ");
                sb.AppendFormat("FROM {0}.ProfileStats2 p ", Conversion.Schema());
                AddPreviousRestrictions(sb, parameters, parameters.DenominatorStatNames.Select(item => item.Item1), "p");
            }
            sb.Append(") AS AllPreviousStats ");
            sb.Append("WHERE HistoryIndex = 1 ");
            sb.Append("GROUP BY PlatformId, PlayerAccountId ");
            sb.Append(") AS PreviousStats ");
            sb.Append("ON CurrentStats.PlayerAccountId = PreviousStats.PlayerAccountId AND CurrentStats.PlatformId = PreviousStats.PlatformId ");
            sb.Append("WHERE CurrentStats.SummedValues >= IFNULL(PreviousStats.SummedValues, 0) AND ");
            sb.AppendFormat("CurrentStats.SummedValues - IFNULL(PreviousStats.SummedValues, 0) < {1} ", parameters.MaxDenominatorValue);
            sb.Append(") AS DenominatorStats ");
            sb.Append("ON NumeratorStats.PlayerAccountId = DenominatorStats.PlayerAccountId AND NumeratorStats.PlatformId = DenominatorStats.PlatformId ");
            sb.Append("WHERE DenominatorStats.Value <> 0;");
            return sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        private void AddCurrentRestrictions(StringBuilder sb, DividedProfileStatParams parameters, IEnumerable<String> statNames, String tableAlias)
        {
            if (parameters.AgeMin != null || parameters.AgeMax != null || parameters.CountryCodes.Any() ||
                parameters.SocialClubUsernames.Any() || parameters.GamerHandlePlatformPairs.Any())
            {
                AddPlayerAccountJoin(sb, String.Format("{0}.PlatformId", tableAlias), String.Format("{0}.PlayerAccountId", tableAlias));

                if (parameters.AgeMin != null || parameters.AgeMax != null || parameters.CountryCodes.Any() ||
                    parameters.SocialClubUsernames.Any())
                {
                    AddRockstarAccountJoin(sb);
                }
            }

            IEnumerable<String> restrictions = GetCurrentRestrictions(parameters, statNames, tableAlias);

            // Check if there are any restrictions to add.
            if (restrictions.Any())
            {
                sb.Append(String.Format("WHERE "));
                sb.Append(String.Join(" AND ", restrictions));
            }
        }

        /// <summary>
        /// Retrieves a list of restrictions that should be added to the sql query string.
        /// </summary>
        protected virtual IEnumerable<String> GetCurrentRestrictions(DividedProfileStatParams parameters, IEnumerable<String> statNames, String tableAlias)
        {
            IEnumerable<int> hashesOfStatsNames = statNames.Select(item => (int)OneAtATime.ComputeHash(item));
            yield return String.Format("{0}.StatId IN ({1})", tableAlias, String.Join(", ", hashesOfStatsNames));

            if (parameters.Platforms.Any())
            {
                yield return (String.Format("{0}.PlatformId IN ({1})", tableAlias, String.Join(",", parameters.Platforms.Select(item => (int)item))));
            }

            // Did the user supply an end dates?
            if (parameters.EndDate.HasValue)
            {
                yield return (String.Format("{0}.LastUpdated BETWEEN '{1}' AND '{2}'",
                    tableAlias, parameters.StartDate.Value.ToString("u"), parameters.EndDate.Value.ToString("u")));
            }
            else
            {
                yield return (String.Format("{0}.LastUpdated >= '{1}'", tableAlias, parameters.StartDate.Value.ToString("u")));
            }

            if (parameters.CountryCodes.Any())
            {
                yield return (String.Format("ra.CountryCode IN ('{0}')", String.Join("', '", parameters.CountryCodes)));
            }

            if (parameters.SocialClubUsernames.Any())
            {
                yield return (String.Format("ra.Nickname IN ('{0}')", String.Join("', '", parameters.SocialClubUsernames)));
            }

            IEnumerable<String> gamerHandles = parameters.GamerHandlePlatformPairs.Select(item => item.Item1);
            if (gamerHandles.Any())
            {
                yield return (String.Format("pa.UserName IN ('{0}')", String.Join("', '", gamerHandles)));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void AddPreviousRestrictions(StringBuilder sb, DividedProfileStatParams parameters, IEnumerable<String> statNames, String tableAlias)
        {
            if (parameters.AgeMin != null || parameters.AgeMax != null || parameters.CountryCodes.Any() ||
                parameters.SocialClubUsernames.Any() || parameters.GamerHandlePlatformPairs.Any())
            {
                AddPlayerAccountJoin(sb, String.Format("{0}.PlatformId", tableAlias), String.Format("{0}.PlayerAccountId", tableAlias));

                if (parameters.AgeMin != null || parameters.AgeMax != null || parameters.CountryCodes.Any() ||
                    parameters.SocialClubUsernames.Any())
                {
                    AddRockstarAccountJoin(sb);
                }
            }

            IEnumerable<String> restrictions = GetPreviousRestrictions(parameters, statNames, tableAlias);

            // Check if there are any restrictions to add.
            if (restrictions.Any())
            {
                sb.Append(String.Format("WHERE "));
                sb.Append(String.Join(" AND ", restrictions));
            }
        }

        /// <summary>
        /// Retrieves a list of restrictions that should be added to the sql query string.
        /// </summary>
        protected virtual IEnumerable<String> GetPreviousRestrictions(DividedProfileStatParams parameters, IEnumerable<String> statNames, String tableAlias)
        {
            IEnumerable<int> hashesOfStatsNames = statNames.Select(item => (int)OneAtATime.ComputeHash(item));
            yield return String.Format("{0}.StatId IN ({1})", tableAlias, String.Join(", ", hashesOfStatsNames));
            yield return (String.Format("{0}.LastUpdated <= '{1}'", tableAlias, parameters.StartDate.Value.ToString("u")));

            if (parameters.Platforms.Any())
            {
                yield return (String.Format("{0}.PlatformId IN ({1})", tableAlias, String.Join(",", parameters.Platforms.Select(item => (int)item))));
            }

            if (parameters.CountryCodes.Any())
            {
                yield return (String.Format("ra.CountryCode IN ('{0}')", String.Join("', '", parameters.CountryCodes)));
            }

            if (parameters.SocialClubUsernames.Any())
            {
                yield return (String.Format("ra.Nickname IN ('{0}')", String.Join("', '", parameters.SocialClubUsernames)));
            }

            IEnumerable<String> gamerHandles = parameters.GamerHandlePlatformPairs.Select(item => item.Item1);
            if (gamerHandles.Any())
            {
                yield return (String.Format("pa.UserName IN ('{0}')", String.Join("', '", gamerHandles)));
            }
        }
        #endregion // Private Methods
    } // DividedDiffProfileStatReport
}
