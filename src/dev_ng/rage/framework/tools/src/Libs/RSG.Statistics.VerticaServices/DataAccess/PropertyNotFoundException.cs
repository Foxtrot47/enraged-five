﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.VerticaServices.DataAccess
{
    /// <summary>
    /// Exception that gets thrown when attempting to map data returned from executing a query to a particular type.
    /// </summary>
    internal class PropertyNotFoundException : Exception
    {
        private readonly Type c_targetType;
        private readonly string c_propertyName;

        /// <summary>
        /// Initializes a new instance of the <see cref="PropertyNotFoundException" /> class,
        /// used when a property get/set accessor is missing.
        /// </summary>
        /// <param name="targetType">The <see cref="System.Type" /> that is missing the property</param>
        /// <param name="propertyName">The name of the missing property</param>
        public PropertyNotFoundException(System.Type targetType, String propertyName)
            : base(String.Format("Could not find property '{0}' in class '{1}'", propertyName, targetType))
        {
            c_targetType = targetType;
            c_propertyName = propertyName;
        }

        public Type TargetType
        {
            get { return c_targetType; }
        }

        public string PropertyName
        {
            get { return c_propertyName; }
        }
    } // PropertyNotFoundException
}
