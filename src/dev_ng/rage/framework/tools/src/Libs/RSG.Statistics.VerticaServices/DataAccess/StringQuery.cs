﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.VerticaServices.DataAccess
{
    /// <summary>
    /// Basic string based query.
    /// </summary>
    internal class StringQuery : IQuery
    {
        #region Member Data
        /// <summary>
        /// Query to execute.
        /// </summary>
        private String m_query;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        internal StringQuery(String query)
        {
            m_query = query;
        }
        #endregion // Constructor(s)

        #region IQuery Implementation
        /// <summary>
        /// Retrieves a string that represents the query to execute against the database.
        /// </summary>
        public String GetQueryString()
        {
            return m_query;
        }
        #endregion // IQuery Implementation
    } // StringQuery
}
