﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.Text;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.Common.Vertica.ServiceContract;
using RSG.Statistics.VerticaServices.DataAccess;
using RSG.Statistics.VerticaServices.Report;
using Vertica.Data.VerticaClient;

namespace RSG.Statistics.VerticaServices.Service
{
    /// <summary>
    /// Vertica based report service.
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class ReportService : ServiceBase, IReportService
    {
        #region Properties
        /// <summary>
        /// Mapping of report identifiers to report instances.
        /// </summary>
        private IDictionary<String, IVerticaReport> ReportLookup { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="server"></param>
        public ReportService(IVerticaServer server)
            : base(server)
        {
            ReportLookup = new Dictionary<String, IVerticaReport>();

            //
            VerticaReportCollection.Initialise(server);
            foreach (IVerticaReport report in VerticaReportCollection.Instance.AllReports.OrderBy(item => item.Name))
            {
                Debug.Assert(!ReportLookup.ContainsKey(report.Name),
                    String.Format("Multiple reports with friendly name '{0}' exist.  This is not allowed.", report.Name));
                if (ReportLookup.ContainsKey(report.Name))
                {
                    throw new ArgumentException(String.Format("Multiple reports with friendly name '{0}' exist.  This is not allowed.", report.Name));
                }

                ReportLookup[report.Name] = report;
                Log.Message("Registered Report: {0}.", report.Name);
            }

        }
        #endregion // Constructor(s)

        #region IReportService Implementation
        /// <summary>
        /// Executes a particular report with the passed in parameters.
        /// </summary>
        public object RunReport(String reportIdentifier, object parameters)
        {
            EnsureReportIdentifierIsValid(reportIdentifier);

            IVerticaReport report = ReportLookup[reportIdentifier];
            return ExecuteCommand(session => RunReportCommand(session, report, parameters));
        }
        #endregion // IReportService Implementation

        #region Private Methods
        /// <summary>
        /// Throws an argument exception if the identifier doesn't correspond to a valid report name.
        /// </summary>
        private void EnsureReportIdentifierIsValid(String identifier)
        {
            Debug.Assert(ReportLookup.ContainsKey(identifier),
                String.Format("Report with identifier '{0}' doesn't exist.", identifier));
            if (!ReportLookup.ContainsKey(identifier))
            {
                throw new ArgumentException(String.Format("Report with identifier '{0}' doesn't exist.", identifier));
            }
        }

        /// <summary>
        /// Executes the report in question.
        /// </summary>
        /// <returns></returns>
        private object RunReportCommand(ISession session, IVerticaReport report, object parameters)
        {
            Log.Profile("Executing {0} report", report.Name);
            object result = report.Generate(new VerticaReportContext { Session = session, Parameters = parameters });
            Log.ProfileEnd();
            return result;
        }
        #endregion // Private Methods
    } // VerticaReportService
}
