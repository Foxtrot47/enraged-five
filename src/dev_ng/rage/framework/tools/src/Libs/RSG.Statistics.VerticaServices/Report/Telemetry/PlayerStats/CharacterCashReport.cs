﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Linq;
using System.Text;
using RSG.ROS;
using RSG.Statistics.Common.Model.Telemetry;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.VerticaServices.DataAccess;
using RSG.Statistics.VerticaServices.Util;
using Vertica.Data.VerticaClient;

namespace RSG.Statistics.VerticaServices.Report.Telemetry.PlayerStats
{
    /// <summary>
    /// Report that provides average cash values for the 3 characters at the end of all missions.
    /// </summary>
    [Export(typeof(IVerticaReport))]
    internal class CharacterCashReport : PlayerStatsReportBase<List<PerMissionCharacterCashStats>, TelemetryParams>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public CharacterCashReport()
            : base(ReportNames.CharacterCashReport)
        {
        }
        #endregion // Constructor(s)

        #region VerticaReportBase Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        protected override List<PerMissionCharacterCashStats> Generate(ISession session, TelemetryParams parameters)
        {
            return session.RetrieveAll<PerMissionCharacterCashStats>(new StringQuery(CreateSqlQueryString(parameters))).ToList();
        }
        #endregion // IStatsReport Implementation

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private String CreateSqlQueryString(TelemetryParams parameters)
        {
            // We want to replicate a query along the following lines:
            //
            // SELECT
            //     AVG(CashMichael) AS MichaelMean,
            //     AVG(CashFranklin) AS FranklinMean,
            //     AVG(CashTrevor) AS TrevorMean,
            //     AVG(MichaelMedian) AS MichaelMedian,
            //     AVG(FranklinMedian) AS FranklinMedian,
            //     AVG(TrevorMedian) AS TrevorMedian
            // FROM
            // (
            //     SELECT
            //         mo.*,
            //         MEDIAN(mo.CashMichael) OVER (PARTITION BY mo.name) AS MichaelMedian,
            //         MEDIAN(mo.CashFranklin) OVER (PARTITION BY mo.name) AS FranklinMedian,
            //         MEDIAN(mo.CashTrevor) OVER (PARTITION BY mo.name) AS TrevorMedian
            //     FROM prod_gta5_11_ps3.Telemetry_Gta5_MISSION_OVER mo
            //     WHERE mo.DateTime BETWEEN '2013-12-01 00:00:00' AND '2013-12-01 08:00:00' AND mo.name='ARM3'
            // ) AS AllStats;

            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT ");
            sb.Append("name AS MissionId, ");
            sb.Append("AVG(CashMichael) AS MichaelAvg, ");
            sb.Append("AVG(CashFranklin) AS FranklinAvg, ");
            sb.Append("AVG(CashTrevor) AS TrevorAvg, ");
            sb.Append("AVG(MichaelMedian) AS MichaelMedian, ");
            sb.Append("AVG(FranklinMedian) AS FranklinMedian, ");
            sb.Append("AVG(TrevorMedian) AS TrevorMedian ");
            sb.Append("FROM (");

            sb.Append("SELECT *, ");
            sb.Append("MEDIAN(CashMichael) OVER (PARTITION BY name) AS MichaelMedian, ");
            sb.Append("MEDIAN(CashFranklin) OVER (PARTITION BY name) AS FranklinMedian, ");
            sb.Append("MEDIAN(CashTrevor) OVER (PARTITION BY name) AS TrevorMedian ");

            sb.Append("FROM (");
            {
                String schema = Conversion.Schema();
                sb.Append("SELECT mo.name, mo.CashMichael, mo.CashFranklin, mo.CashTrevor ");
                sb.AppendFormat("FROM {0}.Telemetry_Gta5_MISSION_OVER mo ", schema);
                AddEmbeddedRestrictions(sb, parameters, "mo", new String[] { "mo.Replay = false"});
            }
            sb.Append(") AS AllStats");
            sb.Append(") AS MedianStats ");
            sb.Append("GROUP BY name;");
            return sb.ToString();
        }
        #endregion // Private Methods
    } // CharacterCashReport
}
