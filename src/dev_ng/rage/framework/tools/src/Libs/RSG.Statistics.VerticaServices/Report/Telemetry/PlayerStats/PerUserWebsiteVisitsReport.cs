﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Linq;
using System.Text;
using RSG.ROS;
using RSG.Statistics.Common.Model.Telemetry;
using RSG.Statistics.Common.Model.Telemetry.PerUser;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.VerticaServices.DataAccess;
using RSG.Statistics.VerticaServices.Util;
using Vertica.Data.VerticaClient;

namespace RSG.Statistics.VerticaServices.Report.Telemetry.PlayerStats
{
    /// <summary>
    /// Report that provides wanted website visit statistics.
    /// </summary>
    [Export(typeof(IVerticaReport))]
    internal class PerUserWebsiteVisitsReport : PerUserTelemetryReportBase<List<PerUserWebsiteVisitStats>, PerUserTelemetryParams>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public PerUserWebsiteVisitsReport()
            : base(ReportNames.PerUserWebsiteVisitsReport)
        {
        }
        #endregion // Constructor(s)

        #region VerticaReportBase Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        protected override List<PerUserWebsiteVisitStats> Generate(ISession session, PerUserTelemetryParams parameters)
        {
            List<PerUserWebsiteVisitStats> allStats = new List<PerUserWebsiteVisitStats>();

            foreach (ROSPlatform platform in parameters.GamerHandlePlatformPairs.Select(item => item.Item2).Distinct())
            {
                String dbQueryString = CreateSqlQueryString(platform, parameters);
                allStats.AddRange(RetrievePerUserData(session, dbQueryString, platform));
            }

            return allStats;
        }
        #endregion // IStatsReport Implementation

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private String CreateSqlQueryString(ROSPlatform platform, PerUserTelemetryParams parameters)
        {
            // We want to replicate a query along the following lines:
            //
            // SELECT
            //     pa.UserName AS GamerHandle,
            //     wv.id AS Website,
            //     COUNT(wv.SubmissionId) AS TimesVisited
            // FROM dev_gta5_11_ps3.Telemetry_Gta5_WEBSITE_VISITED wv
            // INNER JOIN dev_gta5_11_ps3.Telemetry_Gta5_header h on h.SubmissionId=wv.SubmissionId
            // INNER JOIN SocialClub_np_dev.PlayerAccount pa on pa.AccountId=h.AccountId
            // WHERE pa.UserName IN ('MrT_RSN')
            // GROUP BY pa.UserName, wv.id;
            String schema = Conversion.Schema();

            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT ");
            sb.Append("pa.UserName AS GamerHandle, ");
            sb.Append("wv.id AS Website, ");
            sb.Append("COUNT(wv.SubmissionId) AS TimesVisited ");
            sb.Append(String.Format("FROM {0}.Telemetry_Gta5_WEBSITE_VISITED wv ", schema));
            sb.Append(String.Format("INNER JOIN {0}.Telemetry_Gta5_header h on h.SubmissionId=wv.SubmissionId ", schema));
            AddPlayerAccountJoin(sb, "h.PlatformId", "h.AccountId");
            AddRestrictions(sb, parameters, "wv", platform);
            sb.Append("GROUP BY pa.UserName, wv.id;");
            return sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        private IList<PerUserWebsiteVisitStats> RetrievePerUserData(ISession session, String sql, ROSPlatform platform)
        {
            VerticaDataReader dr = session.ExecuteQuery(sql);

            // Convert the results to a nicer temporary format.
            IDictionary<String, PerUserWebsiteVisitStats> perUserStats = new Dictionary<String, PerUserWebsiteVisitStats>();

            while (dr.Read())
            {
                int idx = 0;
                String username = dr.GetString(idx++);

                if (!perUserStats.ContainsKey(username))
                {
                    perUserStats.Add(username, new PerUserWebsiteVisitStats(username, platform));
                }

                MediaStat stat = new MediaStat();
                stat.Hash = dr.GetUInt32(idx++);
                stat.Count = dr.GetUInt64(idx++);
                perUserStats[username].Data.Add(stat);
            }
            return perUserStats.Values.ToList();
        }
        #endregion // Private Methods
    } // PerUserWebsiteVisitsReport
}
