﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using RSG.Base.Logging;
using Vertica.Data.VerticaClient;

namespace RSG.Statistics.VerticaServices.DataAccess
{
    /// <summary>
    /// Vertica based db session.
    /// </summary>
    internal class VerticaSession : ISession
    {
        #region Member Data
        /// <summary>
        /// Reference to the vertica database connection.
        /// </summary>
        private VerticaConnection m_connection;

        /// <summary>
        /// Log object.
        /// </summary>
        private ILog m_log;

        /// <summary>
        /// How long commands have to run before they timeout and an exception is thrown by Vertica.
        /// </summary>
        private int m_commandTimeout;
        #endregion // Member Data

        #region Properties
        /// <summary>
        /// Accessor for the vertica connection object.
        /// </summary>
        [Obsolete("Only provided for legacy purposes.  Do not use as it will be removed!")]
        internal VerticaConnection Connection
        {
            get { return m_connection; }
        }
        #endregion // Properties

        #region Static Member Data
        /// <summary>
        /// Keep track of the property infos per type to avoid the reflection overhead for each call.
        /// </summary>
        private static IDictionary<Type, IDictionary<String, PropertyInfo>> s_propertyInfos =
            new Dictionary<Type, IDictionary<String, PropertyInfo>>();
        private static object s_lockObject = new object();
        #endregion // Static Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        internal VerticaSession(VerticaConnection conn, uint commandTimeout)
        {
            m_connection = conn;
            m_commandTimeout = (int)commandTimeout;
            m_log = LogFactory.CreateUniversalLog("VerticaSession");
        }
        #endregion // Constructor(s)

        #region IQuery Implementation
        /// <summary>
        /// Executes the query and returns all results returned by the query.
        /// </summary>
        public IEnumerable<T> RetrieveAll<T>(IQuery query) where T : new()
        {
            ulong resultCount = 0;

            m_log.Profile("RetrieveAll");
            m_log.Debug(query.GetQueryString());

            // Construct the query command object.
            VerticaCommand command = m_connection.CreateCommand();
            command.CommandText = query.GetQueryString();
            command.CommandTimeout = m_commandTimeout;

            // Execute the query
            VerticaDataReader dr = command.ExecuteReader(CommandBehavior.Default);

            // Get some information about the returned data.
            Type returnType = typeof(T);

            // Check whether the return type is a class or a value type.
            if (returnType.IsClass)
            {
                String[] columns = GetColumns(dr);
                IDictionary<String, PropertyInfo> propInfoLookup = GetPropertyInfoLookup(returnType);
                ValidateColumnsAgainstProperties(columns, propInfoLookup, returnType);

                while (dr.Read())
                {
                    T result = new T();

                    for (int idx = 0; idx < columns.Length; ++idx)
                    {
                        PropertyInfo propInfo = propInfoLookup[columns[idx]];
                        object value = GetValueOfType(dr, idx, propInfo.PropertyType);
                        propInfo.SetValue(result, value, null);
                    }

                    ++resultCount;
                    yield return result;
                }
            }
            else if (returnType.IsValueType)
            {
                while (dr.Read())
                {
                    ++resultCount;
                    yield return (T)GetValueOfType(dr, 0, returnType);
                }
            }
            else
            {
                throw new ArgumentException(String.Format("Unknown return type '{0}' encountered.", returnType));
            }

            m_log.Message("{0} results returned.", resultCount);
            m_log.ProfileEnd();
        }

        /// <summary>
        /// Executes the query and returns the first row in the result set returned by the query.
        /// All other rows are ignored.
        /// </summary>
        public T RetrieveSingleRow<T>(IQuery query) where T : new()
        {
            m_log.Profile("RetrieveSingleRow");
            m_log.Debug(query.GetQueryString());

            // Construct the query command object.
            VerticaCommand command = m_connection.CreateCommand();
            command.CommandText = query.GetQueryString();
            command.CommandTimeout = m_commandTimeout;

            // Execute the query
            VerticaDataReader dr = command.ExecuteReader(CommandBehavior.Default);

            // Get some information about the returned data.
            Type returnType = typeof(T);
            String[] columns = GetColumns(dr);
            IDictionary<String, PropertyInfo> propInfoLookup = GetPropertyInfoLookup(returnType);
            ValidateColumnsAgainstProperties(columns, propInfoLookup, returnType);

            T result = new T();
            if (dr.HasRows)
            {
                dr.Read();
                for (int idx = 0; idx < columns.Length; ++idx)
                {
                    PropertyInfo propInfo = propInfoLookup[columns[idx]];
                    object value = GetValueOfType(dr, idx, propInfo.PropertyType);
                    propInfo.SetValue(result, value, null);
                }
            }

            m_log.ProfileEnd();
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        public T RetrieveScalar<T>(IQuery query)
        {
            m_log.Profile("RetrieveScalar");
            m_log.Debug(query.GetQueryString());

            // Construct the query command object.
            VerticaCommand command = m_connection.CreateCommand();
            command.CommandText = query.GetQueryString();
            command.CommandTimeout = m_commandTimeout;
            object result = command.ExecuteScalar();

            m_log.ProfileEnd();
            return (T)Convert.ChangeType(result, typeof(T));
        }

        /// <summary>
        /// Executes a string query.
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [Obsolete("Here for legacy purposes.  Don't use it anymore as it will be removed.")]
        public VerticaDataReader ExecuteQuery(String query, CommandBehavior behaviour = CommandBehavior.Default)
        {
            m_log.Profile("ExecuteQuery");
            m_log.Debug(query);

            VerticaCommand command = m_connection.CreateCommand();
            command.CommandText = query;
            command.CommandTimeout = m_commandTimeout;
            VerticaDataReader dr = command.ExecuteReader(behaviour);

            m_log.ProfileEnd();
            return dr;
        }
        #endregion // IQuery Implementation

        #region Private Methods
        /// <summary>
        /// Get the names of the columns that were returned from the query.
        /// </summary>
        private String[] GetColumns(IDataReader dr)
        {
            String[] columns = new String[dr.FieldCount];
            for (int i = 0; i < dr.FieldCount; i++)
            {
                columns[i] = dr.GetName(i);
            }
            return columns;
        }

        /// <summary>
        /// Retrieves a list of all the properties for a particular type.
        /// </summary>
        private IDictionary<String, PropertyInfo> GetPropertyInfoLookup(Type type)
        {
            IDictionary<String, PropertyInfo> lookup;
            if (!s_propertyInfos.TryGetValue(type, out lookup))
            {
                lock (s_lockObject)
                {
                    if (!s_propertyInfos.TryGetValue(type, out lookup))
                    {
                        lookup = new Dictionary<String, PropertyInfo>();
                        foreach (PropertyInfo pInfo in type.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.FlattenHierarchy))
                        {
                            lookup.Add(pInfo.Name, pInfo);
                        }
                        s_propertyInfos[type] = lookup;
                    }
                }
            }
            return lookup;
        }

        /// <summary>
        /// Validates that a property exists for all the columns that are returned by the query.
        /// </summary>
        private void ValidateColumnsAgainstProperties(String[] columns, IDictionary<String, PropertyInfo> propInfoLookup, Type returnType)
        {
            for (int i = 0; i < columns.Length; i++)
            {
                // Check that a property exists for this column.
                PropertyInfo propInfo;
                if (!propInfoLookup.TryGetValue(columns[i], out propInfo))
                {
                    throw new PropertyNotFoundException(returnType, columns[i]);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private object GetValueOfType(VerticaDataReader dr, int idx, Type type)
        {
            // Check whether the value is null.  If so use the types default value.
            if (dr.IsDBNull(idx))
            {
                if (type == typeof(String))
                {
                    return null;
                }
                else
                {
                    return Activator.CreateInstance(type);
                }
            }

            // If this is a nullable type, get the parameter type instead.
            Type internalType = type;
            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                internalType = type.GetGenericArguments().First();
            }
            
            // Check what type we are after.
            if (internalType == typeof(Int32))
                return dr.GetInt32(idx);
            else if (internalType.IsEnum && dr.GetFieldType(idx) == typeof(String))
            {
                if (Enum.IsDefined(internalType, dr.GetString(idx)))
                {
                    return Enum.Parse(internalType, dr.GetString(idx));
                }
                else
                {
                    return Activator.CreateInstance(internalType);
                }
            }
            else if (internalType.IsEnum)
            {
                if (Enum.IsDefined(internalType, dr.GetInt32(idx)))
                {
                    return Enum.ToObject(internalType, dr.GetInt32(idx));
                }
                else
                {
                    return Activator.CreateInstance(internalType);
                }
            }
            else if (internalType == typeof(Int64))
                return dr.GetInt64(idx);
            else if (internalType == typeof(UInt32))
                return dr.GetUInt32(idx);
            else if (internalType == typeof(UInt64))
                return dr.GetUInt64(idx);
            else if (internalType == typeof(Decimal))
                return dr.GetDecimal(idx);
            else if (internalType == typeof(Single))
                return dr.GetFloat(idx);
            else if (internalType == typeof(Double))
                return dr.GetDouble(idx);
            else if (internalType == typeof(Boolean))
                return dr.GetBoolean(idx);
            else if (internalType == typeof(String))
                return dr.GetString(idx);
            else if (internalType == typeof(DateTime))
                return dr.GetDateTime(idx);
            else if (internalType == typeof(TimeSpan))
                return dr.GetInterval(idx);
            else
                throw new NotSupportedException(String.Format("Unsupported type '{0}' encountered.", type));
        }
        #endregion // Private Methods
    } // VerticaSession
}
