﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Linq;
using System.Text;
using RSG.ROS;
using RSG.Statistics.Common.Model.Telemetry;
using RSG.Statistics.Common.Model.Telemetry.PerUser;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.VerticaServices.DataAccess;
using RSG.Statistics.VerticaServices.Util;
using Vertica.Data.VerticaClient;

namespace RSG.Statistics.VerticaServices.Report.Telemetry.PlayerStats
{
    /// <summary>
    /// Report that provides wanted level statistics.
    /// </summary>
    [Export(typeof(IVerticaReport))]
    internal class PerUserWantedLevelsReport : PerUserTelemetryReportBase<List<PerUserWantedLevelStats>, PerUserTelemetryParams>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public PerUserWantedLevelsReport()
            : base(ReportNames.PerUserWantedLevelsReport)
        {
        }
        #endregion // Constructor(s)

        #region VerticaReportBase Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        protected override List<PerUserWantedLevelStats> Generate(ISession session, PerUserTelemetryParams parameters)
        {
            List<PerUserWantedLevelStats> allStats = new List<PerUserWantedLevelStats>();

            foreach (ROSPlatform platform in parameters.GamerHandlePlatformPairs.Select(item => item.Item2).Distinct())
            {
                String dbQueryString = CreateSqlQueryString(platform, parameters);
                allStats.AddRange(RetrievePerUserData(session, dbQueryString, platform));
            }

            return allStats;
        }
        #endregion // IStatsReport Implementation

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private String CreateSqlQueryString(ROSPlatform platform, PerUserTelemetryParams parameters)
        {
            // We want to replicate a query along the following lines:
            //
            // SELECT
            //     pa.UserName AS GamerHandle,
            //     wl.wantedlvl AS WantedLevel,
            //     COUNT(wl.SubmissionId) AS TimesAchieved
            // FROM dev_gta5_11_ps3.Telemetry_Gta5_WANTED_LEVEL wl
            // INNER JOIN dev_gta5_11_ps3.Telemetry_Gta5_header h on h.SubmissionId=wl.SubmissionId
            // INNER JOIN SocialClub_np_dev.PlayerAccount pa on pa.AccountId=h.AccountId
            // WHERE pa.UserName IN ('MrT_RSN')
            // GROUP BY pa.UserName, wl.wantedlvl;
            String schema = Conversion.Schema();

            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT ");
            sb.Append("pa.UserName AS GamerHandle, ");
            sb.Append("wl.wantedlvl AS WantedLevel, ");
            sb.Append("COUNT(wl.SubmissionId) AS TimesAchieved ");
            sb.Append(String.Format("FROM {0}.Telemetry_Gta5_WANTED_LEVEL wl ", schema));
            sb.Append(String.Format("INNER JOIN {0}.Telemetry_Gta5_header h on h.SubmissionId=wl.SubmissionId ", schema));
            AddPlayerAccountJoin(sb, "h.PlatformId", "h.AccountId");
            AddRestrictions(sb, parameters, "wl", platform);
            sb.Append("GROUP BY pa.UserName, wl.wantedlvl;");
            return sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        private IList<PerUserWantedLevelStats> RetrievePerUserData(ISession session, String sql, ROSPlatform platform)
        {
            VerticaDataReader dr = session.ExecuteQuery(sql);

            // Convert the results to a nicer temporary format.
            IDictionary<String, PerUserWantedLevelStats> perUserStats = new Dictionary<String, PerUserWantedLevelStats>();

            while (dr.Read())
            {
                int idx = 0;
                String username = dr.GetString(idx++);

                if (!perUserStats.ContainsKey(username))
                {
                    perUserStats.Add(username, new PerUserWantedLevelStats(username, platform));
                }

                WantedLevelStat stat = new WantedLevelStat();
                stat.Level = dr.GetUInt32(idx++);
                stat.TimesAchieved = dr.GetUInt32(idx++);
                perUserStats[username].Data.Add(stat);
            }
            return perUserStats.Values.ToList();
        }
        #endregion // Private Methods
    } // PerUserWantedLevelsReport
}
