﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Linq;
using System.Text;
using RSG.ROS;
using RSG.Statistics.Common.Model.Telemetry;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.VerticaServices.DataAccess;
using RSG.Statistics.VerticaServices.Util;
using Vertica.Data.VerticaClient;

namespace RSG.Statistics.VerticaServices.Report.Telemetry.PlayerStats
{
    /// <summary>
    /// Report that provides website visit statistics.
    /// </summary>
    [Export(typeof(IVerticaReport))]
    internal class SPCheaterReport : PlayerStatsReportBase<List<CheatStat>, TelemetryParams>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public SPCheaterReport()
            : base(ReportNames.SPCheaterReport)
        {
        }
        #endregion // Constructor(s)

        #region VerticaReportBase Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        protected override List<CheatStat> Generate(ISession session, TelemetryParams parameters)
        {
            List<CheatStat> stats = new List<CheatStat>();

            VerticaDataReader dr = session.ExecuteQuery(CreateSqlQueryString(parameters));
            while (dr.Read())
            {
                CheatStat stat = new CheatStat();
                stat.CheatName = dr.GetString(0);
                stat.TimesUsed = dr.GetUInt32(1);
                stat.UniqueGamers = dr.GetUInt32(2);
                stats.Add(stat);
            }
            return stats;
        }
        #endregion // IStatsReport Implementation

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private String CreateSqlQueryString(TelemetryParams parameters)
        {
            // We want to replicate a query along the following lines:
            //
            // SELECT
            //     c.name AS CheatName,
            //     COUNT(c.SubmissionId) AS TimesUsed,
            //     COUNT(DISTINCT(h.AccountId)) AS NumGamers
            // FROM dev_gta5_11_ps3.Telemetry_Gta5_SP_CHEAT c
            // INNER JOIN dev_gta5_11_ps3.Telemetry_Gta5_header h on h.SubmissionId=c.SubmissionId
            // WHERE h.ver=5632
            // GROUP BY c.name;

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("SELECT ");
            sb.AppendFormat("CheatName, ");
            sb.AppendFormat("SUM(TimesUsed) AS TimesUsed, ");
            sb.AppendFormat("SUM(NumGamers) AS NumGamers ");
            sb.AppendFormat("FROM (");
            {
                String schema = Conversion.Schema();
                sb.Append("SELECT ");
                sb.Append("c.name AS CheatName, ");
                sb.Append("COUNT(c.SubmissionId) AS TimesUsed, ");
                sb.Append("COUNT(DISTINCT(h.AccountId)) AS NumGamers ");
                sb.AppendFormat("FROM {0}.Telemetry_Gta5_SP_CHEAT c ", schema);
                sb.AppendFormat("INNER JOIN {0}.Telemetry_Gta5_header h ON h.SubmissionId=c.SubmissionId ", schema);

                AddRestrictions(sb, parameters, "c");
                sb.AppendFormat("GROUP BY c.name ");
            }
            sb.Append(") AS AllStats ");
            sb.Append("GROUP BY CheatName;");
            return sb.ToString();
        }
        #endregion // Private Methods
    } // SPCheatReport
}
