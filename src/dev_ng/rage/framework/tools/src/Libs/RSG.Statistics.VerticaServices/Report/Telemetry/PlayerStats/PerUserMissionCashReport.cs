﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Linq;
using System.Text;
using RSG.ROS;
using RSG.Statistics.Common.Model.Telemetry;
using RSG.Statistics.Common.Model.Telemetry.PerUser;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.VerticaServices.DataAccess;
using RSG.Statistics.VerticaServices.Util;
using Vertica.Data.VerticaClient;

namespace RSG.Statistics.VerticaServices.Report.Telemetry.PlayerStats
{
    /// <summary>
    /// Report that provides per mission cash statistics.
    /// </summary>
    [Export(typeof(IVerticaReport))]
    internal class PerUserMissionCashReport : PerUserTelemetryReportBase<List<PerUserMissionCashStats>, PerUserMissionCashParams>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public PerUserMissionCashReport()
            : base(ReportNames.PerUserMissionCashReport)
        {
        }
        #endregion // Constructor(s)

        #region VerticaReportBase Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        protected override List<PerUserMissionCashStats> Generate(ISession session, PerUserMissionCashParams parameters)
        {
            List<PerUserMissionCashStats> allStats = new List<PerUserMissionCashStats>();

            foreach (ROSPlatform platform in parameters.GamerHandlePlatformPairs.Select(item => item.Item2).Distinct())
            {
                String dbQueryString = CreateSqlQueryString(platform, parameters);
                allStats.AddRange(RetrievePerUserData(session, dbQueryString, platform));
            }

            return allStats;
        }
        #endregion // IStatsReport Implementation

        #region Overrides
        /// <summary>
        /// Overridden to add mission hash restrictions.
        /// </summary>
        protected override IEnumerable<String> GetRestrictions(PerUserMissionCashParams parameters, String tableAlias, ROSPlatform platform)
        {
            if (parameters.MissionNames.Any())
            {
                yield return (String.Format("{0}.name IN ('{1}')", tableAlias, String.Join("', '", parameters.MissionNames)));
            }

            foreach (String restriction in base.GetRestrictions(parameters, tableAlias, platform))
            {
                yield return restriction;
            }
        }
        #endregion // Overrides

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private String CreateSqlQueryString(ROSPlatform platform, PerUserMissionCashParams parameters)
        {
            // We want to replicate a query along the following lines:
            //
            // SELECT
            //     pa.UserName AS Username,
            //     mo.name AS MissionName,
            //     SUM(r.tp) / 1000 AS TimeSpentListening,
            //     COUNT(DISTINCT h.AccountId) AS NumGamers
            // FROM dev_gta5_11_ps3.Telemetry_Gta5_MISSION_OVER mo
            // INNER JOIN dev_gta5_11_ps3.Telemetry_Gta5_header h on h.SubmissionId=mo.SubmissionId
            // INNER JOIN SocialClub_np_dev.PlayerAccount pa on pa.AccountId=h.AccountId
            // WHERE pa.UserName IN ('MrT_RSN')
            // GROUP BY pa.UserName, mo.name;
            String schema = Conversion.Schema();

            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT ");
            sb.Append("pa.UserName AS Username, ");
            sb.Append("mo.name AS MissionName, ");
            sb.Append("MIN(mo.cash) AS Cash ");
            sb.Append(String.Format("FROM {0}.Telemetry_Gta5_MISSION_OVER mo ", schema));
            sb.Append(String.Format("INNER JOIN {0}.Telemetry_Gta5_header h on h.SubmissionId=mo.SubmissionId ", schema));
            AddPlayerAccountJoin(sb, "h.PlatformId", "h.AccountId");
            AddRestrictions(sb, parameters, "mo", platform);
            sb.Append("GROUP BY pa.UserName, mo.name;");
            return sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        private IList<PerUserMissionCashStats> RetrievePerUserData(ISession session, String sql, ROSPlatform platform)
        {
            VerticaDataReader dr = session.ExecuteQuery(sql);

            // Convert the results to a nicer temporary format.
            IDictionary<String, PerUserMissionCashStats> perUserStats = new Dictionary<String, PerUserMissionCashStats>();

            while (dr.Read())
            {
                int idx = 0;
                String username = dr.GetString(idx++);

                if (!perUserStats.ContainsKey(username))
                {
                    perUserStats.Add(username, new PerUserMissionCashStats(username, platform));
                }

                MissionCashStat stat = new MissionCashStat();
                stat.MissionName = dr.GetString(idx++);
                if (dr[idx] != DBNull.Value)
                {
                    stat.Cash = dr.GetUInt32(idx++);
                }
                perUserStats[username].Data.Add(stat);
            }
            return perUserStats.Values.ToList();
        }
        #endregion // Private Methods
    } // PerUserMissionCashReport
}
