﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Linq;
using System.Text;
using RSG.ROS;
using RSG.Statistics.Common.Dto.ProfileStats;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.VerticaServices.DataAccess;
using RSG.Statistics.VerticaServices.Util;
using Vertica.Data.VerticaClient;

namespace RSG.Statistics.VerticaServices.Report.Profile
{
    /// <summary>
    /// 
    /// </summary>
    [Export(typeof(IVerticaReport))]
    internal class DividedProfileStatReport : ProfileStatReportBase<DividedProfileStatParams>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public DividedProfileStatReport()
            : base(ReportNames.DividedProfileStatReport)
        {
        }
        #endregion // Constructor(s)

        #region VerticaReportBase Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        protected override List<ProfileStatDto> Generate(ISession session, DividedProfileStatParams parameters)
        {
            // Determine which sql query we should create based on the input parameters.
            String sql;
            if (parameters.BucketSize.HasValue)
            {
                sql = CreateBucketBasedQuery(parameters);
            }
            else
            {
                sql = CreateSoloQuery(parameters);
            }

            // Run the query against vertica.
            VerticaDataReader dr = session.ExecuteQuery(sql);

            // Convert the results to a nicer temporary format.
            List<ProfileStatDto> stats = new List<ProfileStatDto>();
            while (dr.Read())
            {
                ProfileStatDto stat = new ProfileStatDto(dr.GetDouble(0), (parameters.BucketSize.HasValue ? parameters.BucketSize.Value : 0.0));

                if (dr[1] != DBNull.Value)
                {
                    stat.Total = dr.GetUInt64(1).ToString();
                }
                if (dr[2] != DBNull.Value)
                {
                    stat.YValue = dr.GetUInt64(2).ToString();
                }

                stats.Add(stat);
            }

            // How many gamers contributed to these values?
            ulong gamerCount = (ulong)stats.Sum(item => Decimal.Parse(item.YValue));

            // Check whether we need to pad the data out due to default values.
            ulong totalGamerCount = GetGamerCount(session, parameters);
            if (gamerCount < totalGamerCount)
            {
                double defaultValue = 0.0;
                double numeratorDefaultValue = GetDefaultValueForStats(parameters.NumeratorStatNames);
                double denominatorDefaultValue = GetDefaultValueForStats(parameters.DenominatorStatNames);
                if (denominatorDefaultValue != 0.0)
                {
                    defaultValue = numeratorDefaultValue / denominatorDefaultValue;
                }

                // What is the default bucket to add these to?
                ProfileStatDto defaultBucket = stats.FirstOrDefault(item => item.BucketMin <= defaultValue && item.BucketMax >= defaultValue);
                if (defaultBucket == null)
                {
                    double start = (parameters.BucketSize.HasValue ? Math.Floor(defaultValue / parameters.BucketSize.Value) : 0);
                    defaultBucket = new ProfileStatDto(start, (parameters.BucketSize.HasValue ? parameters.BucketSize.Value : 0.0));
                    stats.Add(defaultBucket);
                }

                for (ulong i = gamerCount; i < totalGamerCount; ++i)
                {
                    defaultBucket.AddDoubleValue(defaultValue);
                }
            }

            return stats.OrderBy(item => item.BucketMin).ToList();
        }
        #endregion // IStatsReport Implementation

        #region Private Methods
        /// <summary>
        /// Creates the query to bucket the data into the requested bucket sizes.
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private String CreateBucketBasedQuery(DividedProfileStatParams parameters)
        {
            // We want to replicate a query along the following lines:
            // (SCARY!!!!)
            //
            // SELECT
            //     Bucket,
            //     SUM(Value) AS Total,
            //     COUNT(DISTINCT(PlayerAccountId)) AS Gamers
            // FROM
            // (
            //     SELECT
            //             NumeratorStats.SummedValues / DenominatorStats.SummedValues AS Value,
            //             NumeratorStats.GamerHandle AS GamerHandle,
            //             FLOOR((NumeratorStats.SummedValues / DenominatorStats.SummedValues) / 0.1) * 0.1) AS Bucket
            //     FROM
            //     (
            //             SELECT
            //                     SUM(Value) AS SummedValues,
            //                     GamerHandle,
            //                     Platform
            //             FROM
            //                     (
            //                             SELECT 'PS3' AS Platform, GamerHandle,
            //                                     ROW_NUMBER() OVER(PARTITION BY PlayerAccountId, StatId ORDER BY LastUpdated DESC) AS HistoryIndex,
            //                                     CASE
            //                                             WHEN IntegerValue IS NOT NULL THEN IntegerValue
            //                                             WHEN FloatValue IS NOT NULL THEN FloatValue
            //                                             ELSE 0.0
            //                                     END AS Value
            //                             FROM dev_gta5_11_ps3.ProfileStats2
            //                             WHERE LastUpdated BETWEEN '2013-06-01' AND '2013-06-15' AND StatName IN ('SP0_TOTAL_PLAYING_TIME', 'SP1_TOTAL_PLAYING_TIME', 'SP2_TOTAL_PLAYING_TIME')
            //                     UNION ALL 
            //                             SELECT 'Xbox360' AS Platform, GamerHandle,
            //                                     ROW_NUMBER() OVER(PARTITION BY PlayerAccountId, StatId ORDER BY LastUpdated DESC) AS HistoryIndex,
            //                                     CASE
            //                                             WHEN IntegerValue IS NOT NULL THEN IntegerValue
            //                                             WHEN FloatValue IS NOT NULL THEN FloatValue
            //                                             ELSE 0.0
            //                                     END AS Value
            //                             FROM dev_gta5_11_ps3.ProfileStats2
            //                             WHERE LastUpdated < '2013-06-01' AND StatName IN ('SP0_TOTAL_PLAYING_TIME', 'SP1_TOTAL_PLAYING_TIME', 'SP2_TOTAL_PLAYING_TIME')
            //                     ) AS AllStats
            //             WHERE HistoryIndex = 1
            //             GROUP BY Platform, PlayerAccountId
            //             ) AS NumeratorStats
            //             INNER JOIN
            //             (
            //             SELECT
            //                     SUM(Value) AS SummedValues,
            //                     GamerHandle,
            //                     Platform
            //             FROM
            //                     (
            //                             SELECT 'PS3' AS Platform, GamerHandle,
            //                                     ROW_NUMBER() OVER(PARTITION BY PlayerAccountId, StatId ORDER BY LastUpdated DESC) AS HistoryIndex,
            //                                     CASE
            //                                             WHEN IntegerValue IS NOT NULL THEN IntegerValue
            //                                             WHEN FloatValue IS NOT NULL THEN FloatValue
            //                                             ELSE 0.0
            //                                     END AS Value
            //                             FROM dev_gta5_11_ps3.ProfileStats2
            //                             WHERE LastUpdated BETWEEN '2013-06-01' AND '2013-06-15' AND StatName IN ('MP0_TOTAL_PLAYING_TIME', 'MP1_TOTAL_PLAYING_TIME', 'MP2_TOTAL_PLAYING_TIME', 'MP3_TOTAL_PLAYING_TIME', 'MP4_TOTAL_PLAYING_TIME')
            //                     UNION ALL 
            //                             SELECT 'Xbox360' AS Platform, GamerHandle,
            //                                     ROW_NUMBER() OVER(PARTITION BY PlayerAccountId, StatId ORDER BY LastUpdated DESC) AS HistoryIndex,
            //                                     CASE
            //                                             WHEN IntegerValue IS NOT NULL THEN IntegerValue
            //                                             WHEN FloatValue IS NOT NULL THEN FloatValue
            //                                             ELSE 0.0
            //                                     END AS Value
            //                             FROM dev_gta5_11_ps3.ProfileStats2
            //                             WHERE LastUpdated < '2013-06-01' AND StatName IN ('MP0_TOTAL_PLAYING_TIME', 'MP1_TOTAL_PLAYING_TIME', 'MP2_TOTAL_PLAYING_TIME', 'MP3_TOTAL_PLAYING_TIME', 'MP4_TOTAL_PLAYING_TIME')
            //                     ) AS AllStats
            //             WHERE HistoryIndex = 1
            //             GROUP BY Platform, PlayerAccountId
            //             ) AS DenominatorStats
            //             ON NumeratorStats.GamerHandle = DenominatorStats.GamerHandle AND NumeratorStats.Platform = DenominatorStats.Platform
            //     WHERE DenominatorStats.SummedValues <> 0
            // ) AS PerGamerStats
            // GROUP BY Bucket;

            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT ");
            sb.Append("Bucket, ");
            sb.Append("SUM(Value) AS Total, ");
            sb.Append("COUNT(DISTINCT(PlayerAccountId)) AS Gamers ");
            sb.Append("FROM (");
            sb.Append("SELECT ");
            sb.Append("PerGamerNumeratorStats.SummedValues / PerGamerDenominatorStats.SummedValues AS Value, ");
            sb.Append("PerGamerNumeratorStats.GamerHandle AS GamerHandle, ");
            sb.AppendFormat("FLOOR((PerGamerNumeratorStats.SummedValues / PerGamerDenominatorStats.SummedValues) / {0}) * {0} AS Bucket ", parameters.BucketSize);
            sb.Append("FROM (");
            sb.Append("SELECT ");
            sb.Append("SUM(Value) AS SummedValues, ");
            sb.Append("GamerHandle, ");
            sb.Append("PlatformId ");
            sb.Append("FROM (");
            {
                sb.Append("SELECT PlatformId, GamerHandle, ");
                sb.Append("ROW_NUMBER() OVER(PARTITION BY PlayerAccountId, StatId ORDER BY LastUpdated DESC) AS HistoryIndex, ");
                sb.Append("COALESCE(IntegerValue, FloatValue, 0) AS Value ");
                sb.AppendFormat("FROM {0}.ProfileStats2 p ", Conversion.Schema());
                AddRestrictions(sb, parameters, parameters.NumeratorStatNames.Select(item => item.Item1), "p");
            }
            sb.Append(") AS AllNumeratorStats ");
            sb.AppendFormat("WHERE HistoryIndex = 1 AND Value < {1} ", parameters.MaxNumeratorValue);
            sb.Append("GROUP BY PlatformId, PlayerAccountId ");
            sb.Append(") AS PerGamerNumeratorStats ");
            sb.Append("INNER JOIN ( ");
            sb.Append("SELECT ");
            sb.Append("SUM(Value) AS SummedValues, ");
            sb.Append("GamerHandle, ");
            sb.Append("PlatformId ");
            sb.Append("FROM (");
            {
                sb.Append("SELECT PlatformId, GamerHandle, ");
                sb.Append("ROW_NUMBER() OVER(PARTITION BY PlayerAccountId, StatId ORDER BY LastUpdated DESC) AS HistoryIndex, ");
                sb.Append("COALESCE(IntegerValue, FloatValue, 0) AS Value ");
                sb.AppendFormat("FROM {0}.ProfileStats2 p ", Conversion.Schema());
                AddRestrictions(sb, parameters, parameters.DenominatorStatNames.Select(item => item.Item1), "p");
            }
            sb.Append(") AS AllDenominatorStats ");
            sb.AppendFormat("WHERE HistoryIndex = 1 AND Value < {0} ", parameters.MaxDenominatorValue);
            sb.Append("GROUP BY PlatformId, PlayerAccountId ");
            sb.Append(") AS PerGamerDenominatorStats ");
            sb.Append("ON PerGamerNumeratorStats.GamerHandle = PerGamerDenominatorStats.GamerHandle AND PerGamerNumeratorStats.PlatformId = PerGamerDenominatorStats.PlatformId ");
            sb.Append("WHERE PerGamerDenominatorStats.SummedValues <> 0 ");
            sb.Append(") AS PerGamerStats ");
            sb.Append("GROUP BY Bucket;");
            return sb.ToString();
        }

        /// <summary>
        /// Creates the query to return the single value for the requested stats.
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private String CreateSoloQuery(DividedProfileStatParams parameters)
        {
            // We want to replicate a query along the following lines:
            //
            // SELECT
            //         0 AS Bucket,
            //         SUM(NumeratorStats.SummedValues / DenominatorStats.SummedValues) AS Total,
            //         COUNT(DISTINCT(NumeratorStats.GamerHandle)) AS GamerHandle
            // FROM
            // (
            //         SELECT
            //                 SUM(Value) AS SummedValues,
            //                 GamerHandle,
            //                 Platform
            //         FROM
            //                 (
            //                         SELECT 'PS3' AS Platform, GamerHandle,
            //                                 ROW_NUMBER() OVER(PARTITION BY PlayerAccountId, StatId ORDER BY LastUpdated DESC) AS HistoryIndex,
            //                                 CASE
            //                                         WHEN IntegerValue IS NOT NULL THEN IntegerValue
            //                                         WHEN FloatValue IS NOT NULL THEN FloatValue
            //                                         ELSE 0.0
            //                                 END AS Value
            //                         FROM dev_gta5_11_ps3.ProfileStats2
            //                         WHERE LastUpdated BETWEEN '2013-06-01' AND '2013-06-15' AND StatName IN ('SP0_TOTAL_PLAYING_TIME', 'SP1_TOTAL_PLAYING_TIME', 'SP2_TOTAL_PLAYING_TIME')
            //                 UNION ALL 
            //                         SELECT 'Xbox360' AS Platform, GamerHandle,
            //                                 ROW_NUMBER() OVER(PARTITION BY PlayerAccountId, StatId ORDER BY LastUpdated DESC) AS HistoryIndex,
            //                                 CASE
            //                                         WHEN IntegerValue IS NOT NULL THEN IntegerValue
            //                                         WHEN FloatValue IS NOT NULL THEN FloatValue
            //                                         ELSE 0.0
            //                                 END AS Value
            //                         FROM dev_gta5_11_ps3.ProfileStats2
            //                         WHERE LastUpdated < '2013-06-01' AND StatName IN ('SP0_TOTAL_PLAYING_TIME', 'SP1_TOTAL_PLAYING_TIME', 'SP2_TOTAL_PLAYING_TIME')
            //                 ) AS AllStats
            //         WHERE HistoryIndex = 1
            //         GROUP BY Platform, PlayerAccountId
            //         ) AS NumeratorStats
            //         INNER JOIN
            //         (
            //         SELECT
            //                 SUM(Value) AS SummedValues,
            //                 GamerHandle,
            //                 Platform
            //         FROM
            //                 (
            //                         SELECT 'PS3' AS Platform, GamerHandle,
            //                                 ROW_NUMBER() OVER(PARTITION BY PlayerAccountId, StatId ORDER BY LastUpdated DESC) AS HistoryIndex,
            //                                 CASE
            //                                         WHEN IntegerValue IS NOT NULL THEN IntegerValue
            //                                         WHEN FloatValue IS NOT NULL THEN FloatValue
            //                                         ELSE 0.0
            //                                 END AS Value
            //                         FROM dev_gta5_11_ps3.ProfileStats2
            //                         WHERE LastUpdated BETWEEN '2013-06-01' AND '2013-06-15' AND StatName IN ('MP0_TOTAL_PLAYING_TIME', 'MP1_TOTAL_PLAYING_TIME', 'MP2_TOTAL_PLAYING_TIME', 'MP3_TOTAL_PLAYING_TIME', 'MP4_TOTAL_PLAYING_TIME')
            //                 UNION ALL 
            //                         SELECT 'Xbox360' AS Platform, GamerHandle,
            //                                 ROW_NUMBER() OVER(PARTITION BY PlayerAccountId, StatId ORDER BY LastUpdated DESC) AS HistoryIndex,
            //                                 CASE
            //                                         WHEN IntegerValue IS NOT NULL THEN IntegerValue
            //                                         WHEN FloatValue IS NOT NULL THEN FloatValue
            //                                         ELSE 0.0
            //                                 END AS Value
            //                         FROM dev_gta5_11_ps3.ProfileStats2
            //                         WHERE LastUpdated < '2013-06-01' AND StatName IN ('MP0_TOTAL_PLAYING_TIME', 'MP1_TOTAL_PLAYING_TIME', 'MP2_TOTAL_PLAYING_TIME', 'MP3_TOTAL_PLAYING_TIME', 'MP4_TOTAL_PLAYING_TIME')
            //                 ) AS AllStats
            //         WHERE HistoryIndex = 1
            //         GROUP BY Platform, PlayerAccountId
            //         ) AS DenominatorStats
            //         ON NumeratorStats.GamerHandle = DenominatorStats.GamerHandle AND NumeratorStats.Platform = DenominatorStats.Platform
            // WHERE DenominatorStats.SummedValues <> 0;

            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT ");
            sb.Append("0 AS Bucket, ");
            sb.Append("SUM(NumeratorStats.SummedValues / DenominatorStats.SummedValues) AS Total, ");
            sb.Append("COUNT(DISTINCT(NumeratorStats.GamerHandle)) AS GamerHandle ");
            sb.Append("FROM (");
            sb.Append("SELECT ");
            sb.Append("SUM(Value) AS SummedValues, ");
            sb.Append("GamerHandle, ");
            sb.Append("PlatformId ");
            sb.Append("FROM (");
            {
                sb.Append("SELECT PlatformId, GamerHandle, ");
                sb.Append("ROW_NUMBER() OVER(PARTITION BY PlayerAccountId, StatId ORDER BY LastUpdated DESC) AS HistoryIndex, ");
                sb.Append("COALESCE(IntegerValue, FloatValue, 0) AS Value ");
                sb.AppendFormat("FROM {0}.ProfileStats2 p ", Conversion.Schema());
                AddRestrictions(sb, parameters, parameters.NumeratorStatNames.Select(item => item.Item1), "p");
            }
            sb.Append(") AS AllNumeratorStats ");
            sb.AppendFormat("WHERE HistoryIndex = 1 AND Value < {1} ", parameters.MaxNumeratorValue);
            sb.Append("GROUP BY PlatformId, PlayerAccountId ");
            sb.Append(") AS PerGamerNumeratorStats ");
            sb.Append("INNER JOIN ( ");
            sb.Append("SELECT ");
            sb.Append("SUM(Value) AS SummedValues, ");
            sb.Append("GamerHandle, ");
            sb.Append("PlatformId ");
            sb.Append("FROM (");
            {
                sb.Append("SELECT PlatformId, GamerHandle, ");
                sb.Append("ROW_NUMBER() OVER(PARTITION BY PlayerAccountId, StatId ORDER BY LastUpdated DESC) AS HistoryIndex, ");
                sb.Append("COALESCE(IntegerValue, FloatValue, 0) AS Value ");
                sb.AppendFormat("FROM {0}.ProfileStats2 p ", Conversion.Schema());
                AddRestrictions(sb, parameters, parameters.DenominatorStatNames.Select(item => item.Item1), "p");
            }
            sb.Append(") AS AllDenominatorStats ");
            sb.AppendFormat("WHERE HistoryIndex = 1 AND Value < {0} ", parameters.MaxDenominatorValue);
            sb.Append("GROUP BY PlatformId, PlayerAccountId ");
            sb.Append(") AS PerGamerDenominatorStats ");
            sb.Append("ON PerGamerNumeratorStats.GamerHandle = PerGamerDenominatorStats.GamerHandle AND PerGamerNumeratorStats.PlatformId = PerGamerDenominatorStats.PlatformId ");
            sb.Append("WHERE PerGamerDenominatorStats.SummedValues <> 0;");
            return sb.ToString();
        }
        #endregion // Private Methods
    } // DividedProfileStatReport
}
