﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Linq;
using System.Text;
using RSG.ROS;
using RSG.Statistics.Common.Model.Telemetry.PerUser;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.VerticaServices.DataAccess;
using RSG.Statistics.VerticaServices.Util;
using Vertica.Data.VerticaClient;

namespace RSG.Statistics.VerticaServices.Report.Telemetry.PlayerStats
{
    /// <summary>
    /// Report that returns the number of times users have changed items of clothing.
    /// </summary>
    [Export(typeof(IVerticaReport))]
    internal class PerUserClothChangesReport : PerUserTelemetryReportBase<List<PerUserClothesChangeStat>, PerUserTelemetryParams>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public PerUserClothChangesReport()
            : base(ReportNames.PerUserClothChangesReport)
        {
        }
        #endregion // Constructor(s)

        #region VerticaReportBase Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        protected override List<PerUserClothesChangeStat> Generate(ISession session, PerUserTelemetryParams parameters)
        {
            List<PerUserClothesChangeStat> allStats = new List<PerUserClothesChangeStat>();

            foreach (ROSPlatform platform in parameters.GamerHandlePlatformPairs.Select(item => item.Item2).Distinct())
            {
                String dbQueryString = CreateSqlQueryString(platform, parameters);
                allStats.AddRange(RetrievePerUserData(session, dbQueryString, platform));
            }

            return allStats;
        }
        #endregion // IStatsReport Implementation

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private String CreateSqlQueryString(ROSPlatform platform, PerUserTelemetryParams parameters)
        {
            // We want to replicate a query along the following lines:
            //
            // SELECT
            //     pa.UserName AS GamerHandle,
            //     COUNT(cc.SubmissionId) AS Changes
            // FROM dev_gta5_11_ps3.Telemetry_Gta5_CLOTHES_CHANGE cc
            // INNER JOIN dev_gta5_11_ps3.Telemetry_Gta5_header h on h.SubmissionId=cc.SubmissionId
            // INNER JOIN SocialClub_np_dev.PlayerAccount pa on pa.AccountId=h.AccountId
            // WHERE pa.UserName IN ('MrT_RSN')
            // GROUP BY pa.UserName;
            String schema = Conversion.Schema();

            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT ");
            sb.Append("pa.UserName AS GamerHandle, ");
            sb.Append("COUNT(cc.SubmissionId) AS TimesAchieved ");
            sb.Append(String.Format("FROM {0}.Telemetry_Gta5_CLOTHES_CHANGE cc ", schema));
            sb.Append(String.Format("INNER JOIN {0}.Telemetry_Gta5_header h on h.SubmissionId=cc.SubmissionId ", schema));
            AddPlayerAccountJoin(sb, "h.PlatformId", "h.AccountId");
            AddRestrictions(sb, parameters, "cc", platform);
            sb.Append("GROUP BY pa.UserName;");
            return sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        private IList<PerUserClothesChangeStat> RetrievePerUserData(ISession session, String sql, ROSPlatform platform)
        {
            VerticaDataReader dr = session.ExecuteQuery(sql);

            // Convert the results to a nicer temporary format.
            IDictionary<String, PerUserClothesChangeStat> perUserStats = new Dictionary<String, PerUserClothesChangeStat>();

            while (dr.Read())
            {
                int idx = 0;
                String username = dr.GetString(idx++);

                if (!perUserStats.ContainsKey(username))
                {
                    perUserStats.Add(username, new PerUserClothesChangeStat(username, platform));
                }

                perUserStats[username].Data = dr.GetUInt64(idx++);
            }
            return perUserStats.Values.ToList();
        }
        #endregion // Private Methods
    } // PerUserClothChangesReport
}
