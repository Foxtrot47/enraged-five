﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Text;
using RSG.Statistics.Common.Config;

namespace RSG.Statistics.VerticaServices.Service
{
    /// <summary>
    /// 
    /// </summary>
    public class VerticaInstanceProvider : IInstanceProvider, IContractBehavior
    {
        #region Member Data
        private readonly Type c_serviceType;
        private readonly IVerticaServer c_server;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public VerticaInstanceProvider(IVerticaServer server, Type serviceType)
        {
            Debug.Assert(server != null, "Server parameter is null.");
            if (server == null)
            {
                throw new ArgumentNullException("server");
            }

            c_server = server;
            c_serviceType = serviceType;
        }
        #endregion // Constructor(s)

        #region IInstanceProvider Implementation
        /// <summary>
        /// 
        /// </summary>
        public object GetInstance(InstanceContext instanceContext, Message message)
        {
            return GetInstance(instanceContext);
        }

        /// <summary>
        /// Creates the new service instance passing in the server config object.
        /// </summary>
        public object GetInstance(InstanceContext instanceContext)
        {
            return Activator.CreateInstance(c_serviceType, c_server);
        }

        /// <summary>
        /// 
        /// </summary>
        public void ReleaseInstance(InstanceContext instanceContext, object instance)
        {
        }
        #endregion // IInstanceProvider Implementation

        #region IContractBehavior Implementation
        /// <summary>
        /// 
        /// </summary>
        public void AddBindingParameters(ContractDescription contractDescription, ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public void ApplyClientBehavior(ContractDescription contractDescription, ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
        }

        /// <summary>
        /// Tells the dispatch runtime that this object is in charge of creating service instances.
        /// </summary>
        public void ApplyDispatchBehavior(ContractDescription contractDescription, ServiceEndpoint endpoint, DispatchRuntime dispatchRuntime)
        {
            dispatchRuntime.InstanceProvider = this;
        }

        /// <summary>
        /// 
        /// </summary>
        public void Validate(ContractDescription contractDescription, ServiceEndpoint endpoint)
        {
            if (!c_server.UsingIIS)
            {
                String modifiedAddress = endpoint.Address.Uri.ToString();
                if (endpoint.Binding is WebHttpBinding)
                {
                    modifiedAddress = modifiedAddress.Replace("8080", c_server.Port.ToString());
                }
                endpoint.Address = new EndpointAddress(modifiedAddress);
            }
        }
        #endregion // IContractBehavior Implementation
    } // VerticaInstanceProvider
}
