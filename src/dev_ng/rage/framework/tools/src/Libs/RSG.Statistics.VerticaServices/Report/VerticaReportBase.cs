﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using RSG.Model.Common.Report;
using RSG.ROS;
using RSG.Statistics.Common;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.VerticaServices.DataAccess;
using RSG.Statistics.VerticaServices.Util;
using Vertica.Data.VerticaClient;

namespace RSG.Statistics.VerticaServices.Report
{
    /// <summary>
    /// Base class for vertica reports.
    /// </summary>
    internal abstract class VerticaReportBase<TResult, TParam> : IVerticaReport
        where TParam : class
    {
        #region Properties
        /// <summary>
        /// Report name.
        /// </summary>
        public String Name { get; private set; }
        #endregion // Properties

        #region Events
        /// <summary>
        /// Event that can be fired to notify of report generation progress.
        /// Not currently in use anywhere.
        /// </summary>
        public event ReportProgressEventHandler ProgressChanged;
        #endregion // Events

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        protected VerticaReportBase(String name)
        {
            Name = name;
        }
        #endregion // Constructor(s)

        #region IReportDataProvider Implementation
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public object Generate(IReportContext context)
        {
            // Make sure the report context is of the right type.
            VerticaReportContext verticaContext = context as VerticaReportContext;
            Debug.Assert(verticaContext != null, "Context isn't a VerticaReportContext object.");
            if (verticaContext == null)
            {
                throw new ArgumentException("Context isn't a VerticaReportContext object.");
            }

            // Make sure the report parameters are of the right type.
            TParam typedParams = verticaContext.Parameters as TParam;
            Debug.Assert(typedParams != null, "Report parameters are of the wrong type.");
            if (typedParams == null)
            {
                throw new ArgumentException("Invalid parameter type supplied.");
            }

            return Generate(verticaContext.Session, typedParams);
        }
        #endregion // IReportDataProvider Implementation

        #region Abstract Mathods
        /// <summary>
        /// Typed version of the generate method.
        /// </summary>
        protected virtual TResult Generate(ISession session, TParam parameters)
        {
            return default(TResult);
        }
        #endregion // Abstract Methods

        #region Protected Methods
        /// <summary>
        /// Adds the distinct player account join.
        /// </summary>
        protected void AddPlayerAccountJoin(StringBuilder sb, String platformColumn, String playerAccountColumn)
        {
            // The join needs to take into account the most recent record via the LastUpdatedDate.
            //
            // INNER JOIN
            // (
            //     SELECT *, ROW_NUMBER() OVER(PARTITION BY AccountId ORDER BY LastUpdatedDate DESC) AS HistoryIndex
            //     FROM SocialClub.PlayerAccount
            // ) pa ON HistoryIndex=1 AND pa.AccountId=h.AccountId
            sb.AppendFormat("INNER JOIN ");
            sb.AppendFormat("( ");
            sb.AppendFormat("SELECT *, ROW_NUMBER() OVER(PARTITION BY AccountId, PlatformId ORDER BY ISNULL(LastUpdatedDate, CreatedDate) DESC) AS HistoryIndex ");
            sb.AppendFormat("FROM {0}.PlayerAccount ", Conversion.SocialClubSchema());
            sb.AppendFormat(") pa ON pa.HistoryIndex=1 AND pa.AccountId={0} AND pa.PlatformId={1} ", playerAccountColumn, platformColumn);
        }

        /// <summary>
        /// Adds the distinct rockstar account join.
        /// </summary>
        protected void AddRockstarAccountJoin(StringBuilder sb)
        {
            // The join needs to take the LastUpdatedDate into account.
            //
            // INNER JOIN
            // (
            //      SELECT *, ROW_NUMBER() OVER(PARTITION BY RockstarId ORDER BY LastUpdatedDate DESC) AS HistoryIndex
            //      FROM SocialClub.RockstarAccount
            // ) ra ON HistoryIndex=1 AND ra.RockstarId=pa.RockstarId
            sb.AppendFormat("INNER JOIN ");
            sb.AppendFormat("( ");
            sb.AppendFormat("SELECT *, ROW_NUMBER() OVER(PARTITION BY RockstarId ORDER BY ISNULL(LastUpdatedDate, CreatedDate) DESC) AS HistoryIndex ");
            sb.AppendFormat("FROM {0}.RockstarAccount ra ", Conversion.SocialClubSchema());
            sb.AppendFormat(") ra ON ra.HistoryIndex=1 AND ra.RockstarId=pa.RockstarId ");
        }

        /// <summary>
        /// Adds a latest UGC mission table join.
        /// </summary>
        protected void AddUGCMissionJoin(StringBuilder sb, String ugcIdColumn, String selectColumns = "*", String joinType = "INNER")
        {
            // The join needs to take into account the most recent record via the UpdatedDate.
            //
            // INNER JOIN
            // (
            //     SELECT *, ROW_NUMBER() OVER(PARTITION BY PublicContentId ORDER BY UpdatedDate DESC) AS HistoryIndex
            //     FROM ugc.gta5mission_metadata
            // ) ugcm ON HistoryIndex=1 AND ugcm.PublicContentId=mf.mid
            sb.AppendFormat("{0} JOIN ", joinType);
            sb.AppendFormat("( ");
            sb.AppendFormat("SELECT {0}, ", selectColumns);
            sb.AppendFormat("ROW_NUMBER() OVER(PARTITION BY PublicContentId ORDER BY ISNULL(UpdatedDate, CreatedDate) DESC) AS HistoryIndex ");
            sb.AppendFormat("FROM {0}.gta5mission_metadata ", Conversion.UGCSchema());
            sb.AppendFormat(") ugcm ON ugcm.HistoryIndex=1 AND ugcm.PublicContentId={0} ", ugcIdColumn);
        }

        /// <summary>
        /// Creates a query string for creating the datetime for a particular group mode.
        /// </summary>
        protected String CreateDateTimeGroupQuery(DateTimeGroupMode groupMode, DateTimeClampMode clampMode, String dateColumn)
        {
            if (groupMode == DateTimeGroupMode.FiveMinutely || groupMode == DateTimeGroupMode.QuarterHourly || groupMode == DateTimeGroupMode.HalfHourly)
            {
                uint minuteMultiplier = 30;
                if (groupMode == DateTimeGroupMode.FiveMinutely)
                {
                    minuteMultiplier = 5;
                }
                else if (groupMode == DateTimeGroupMode.QuarterHourly)
                {
                    minuteMultiplier = 15;
                }

                return String.Format("TIMESTAMPADD({0}, (CAST(FLOOR(DATEDIFF({0}, DATE(1), {1}) / {2}) AS INTEGER){3}) * {2}, DATE(1))",
                    Conversion.GetGroupModeFunction(groupMode), dateColumn, minuteMultiplier,
                    (clampMode == DateTimeClampMode.Floor ? "" : "+1"));
            }
            else
            {
                return String.Format("TIMESTAMPADD({0}, DATEDIFF({0}, DATE(1), {1}){2}, DATE(1))",
                    Conversion.GetGroupModeFunction(groupMode), dateColumn,
                    (clampMode == DateTimeClampMode.Floor ? "" : "+1"));
            }
        }
        #endregion // Protected Methods
    } // VerticaReportBase

    /// <summary>
    /// 
    /// </summary>
    public enum DateTimeClampMode
    {
        Floor,

        Ceiling
    } // ClampMode
}
