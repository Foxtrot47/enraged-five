﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common.Report;
using RSG.Statistics.VerticaServices.DataAccess;
using Vertica.Data.VerticaClient;

namespace RSG.Statistics.VerticaServices.Report
{
    /// <summary>
    /// 
    /// </summary>
    internal class VerticaReportContext : IReportContext
    {
        internal ISession Session { get; set; }
        internal object Parameters { get; set; }
    } // VerticaReportContext
}
