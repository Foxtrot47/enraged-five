﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Linq;
using System.Text;
using RSG.Base.Extensions;
using RSG.ROS;
using RSG.Statistics.Common.Model.Telemetry;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.VerticaServices.DataAccess;
using RSG.Statistics.VerticaServices.Util;
using Vertica.Data.VerticaClient;

namespace RSG.Statistics.VerticaServices.Report.Telemetry.PlayerStats
{
    /// <summary>
    /// Report that provides website visit statistics.
    /// </summary>
    [Export(typeof(IVerticaReport))]
    internal class CashPurchaseReport : PlayerStatsReportBase<CashPurchaseData, CashPurchaseParams>
    {
        #region Internal Classes
        /// <summary>
        /// 
        /// </summary>
        internal class AmountQuantifiedCashPurchaseStat : HistoricalCashPurchaseStat
        {
            public uint Amount { get; set; }
        }
        #endregion // Internal Classes

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public CashPurchaseReport()
            : base(ReportNames.CashPurchaseReport)
        {
        }
        #endregion // Constructor(s)
        
        #region VerticaReportBase Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        protected override CashPurchaseData Generate(ISession session, CashPurchaseParams parameters)
        {
            CashPurchaseData reportResults = new CashPurchaseData();

            reportResults.CashPackData.Add(GetStats(session, parameters, "$3", 100000, "A"));
            reportResults.CashPackData.Add(GetStats(session, parameters, "$5", 200000, "B"));
            reportResults.CashPackData.Add(GetStats(session, parameters, "$10", 500000, "C"));
            reportResults.CashPackData.Add(GetStats(session, parameters, "$20", 1250000, "D"));
            reportResults.CashPackData.Add(GetStats(session, parameters, "$50", 3500000, "E"));
            reportResults.CashPackData.Add(GetStats(session, parameters, "$100", 8000000, "F"));

            // Determine how many players actually bought cash in one form or another.
            reportResults.UniqueGamers = GetUniqueGamerCount(session, parameters);

            // Finally, determine what percentage of all purchases that the reported on data covers.
            reportResults.TotalPercentage = 1.0;

            return reportResults;
        }
        #endregion // VerticaReportBase Implementation

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cashPackName"></param>
        /// <param name="vcAmount"></param>
        /// <param name="lookupColumnName"></param>
        /// <returns></returns>
        private HistoricalCashPurchaseStats GetStats(ISession session, CashPurchaseParams parameters, String cashPackName, uint vcAmount, String lookupColumnName)
        {
            HistoricalCashPurchaseStats cashPackStat = new HistoricalCashPurchaseStats();
            cashPackStat.CashPackName = cashPackName;
            cashPackStat.InGameAmount = vcAmount;
            cashPackStat.Stats.AddRange(session.RetrieveAll<HistoricalCashPurchaseStat>(new StringQuery(CreatePurchaseQuery(parameters, lookupColumnName))));
            return cashPackStat;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="lookupColumnName"></param>
        /// <returns></returns>
        private String CreatePurchaseQuery(CashPurchaseParams parameters, String lookupColumnName)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT ");
            sb.Append("\"Start\", ");
            sb.Append("\"End\", ");
            sb.Append("1 AS StartInclusive, ");
            sb.Append("0 AS EndInclusive, ");
            sb.Append("SUM(TimesPurchased) AS TimesPurchased, ");
            sb.Append("SUM(UniqueGamers) AS UniqueGamers ");
            sb.Append("FROM (");
            {
                String schema = Conversion.Schema();
                sb.Append("SELECT  ");
                sb.AppendFormat("{0} AS \"Start\", ", CreateDateTimeGroupQuery(parameters.GroupMode, DateTimeClampMode.Floor, "DateTime"));
                sb.AppendFormat("{0} AS \"End\", ", CreateDateTimeGroupQuery(parameters.GroupMode, DateTimeClampMode.Ceiling, "DateTime"));
                sb.Append("COUNT(*) AS Transactions, ");
                sb.Append("COUNT(DISTINCT Header_AccountId) AS UniqueGamers, ");
                sb.AppendFormat("SUM(ISNULL({0},0)) as TimesPurchased ", lookupColumnName);
                sb.AppendFormat("FROM {0}.Telemetry_Gta5Online_PURCH p ", schema);
                sb.Append("LEFT JOIN analytics.VCConvertComplete ON p.amount = VCConvertComplete.VC AND p.usde = VCConvertComplete.usde ");
                AddEmbeddedRestrictions(sb, parameters, "p", new String[] { String.Format("{0} != 0", lookupColumnName) });
                sb.Append("GROUP BY \"Start\", \"End\" ");
            }
            sb.AppendFormat(") AS AllStats ");
            sb.Append("GROUP BY \"Start\", \"End\" ");
            return sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private long GetUniqueGamerCount(ISession session, CashPurchaseParams parameters)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT ");
            sb.Append("SUM(UniqueGamers) AS UniqueGamers ");
            sb.Append("FROM (");
            {
                String schema = Conversion.Schema();
                sb.Append("SELECT COUNT(DISTINCT p.Header_AccountId) AS UniqueGamers ");
                sb.AppendFormat("FROM {0}.Telemetry_Gta5Online_PURCH p ", schema);
                AddEmbeddedRestrictions(sb, parameters, "p");
            }
            sb.Append(") AS AllStats;");

            return session.RetrieveScalar<long>(new StringQuery(sb.ToString()));
        }
        #endregion // Private Methods
    } // CashPurchaseReport
}
