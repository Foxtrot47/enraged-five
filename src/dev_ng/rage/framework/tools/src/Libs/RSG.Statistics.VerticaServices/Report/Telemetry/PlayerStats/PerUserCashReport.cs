﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Linq;
using System.Text;
using RSG.Base.Extensions;
using RSG.Model.Common.Mission;
using RSG.ROS;
using RSG.Statistics.Common;
using RSG.Statistics.Common.Model.Telemetry;
using RSG.Statistics.Common.Model.Telemetry.PerUser;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.VerticaServices.DataAccess;
using RSG.Statistics.VerticaServices.Util;
using Vertica.Data.VerticaClient;

namespace RSG.Statistics.VerticaServices.Report.Telemetry.PlayerStats
{
    /// <summary>
    /// Report that provides per user cash/xp/mp match statistics.
    /// </summary>
    [Export(typeof(IVerticaReport))]
    internal class PerUserCashReport : VerticaReportBase<PerUserHourlyCashStat, MPUserHourlyParams>
    {
        #region Intermediate Result Classes
        /// <summary>
        /// Per hour match info.
        /// </summary>
        private class MatchStat
        {
            public uint Hour { get; set; }
            public uint Experience { get; set; }
            public int CashEarned { get; set; }
            public ulong MatchId { get; set; }
            public String UGCIdentifier { get; set; }
            public String MatchName { get; set; }
            public String MatchCreator { get; set; }
            public ROSPlatform? MatchCreatorPlatform { get; set; }
            public int? MatchType { get; set; }
            public DateTime Timestamp { get; set; }
            public JobResult Result { get; set; }
            public uint MaxSurvivalWave { get; set; }
            public uint Rank { get; set; }
            public uint Players { get; set; }
            public uint Duration { get; set; }
            public int BetWinnings { get; set; }
        } // MatchStat

        /// <summary>
        /// Per hour rank info.
        /// </summary>
        private class RankStat
        {
            public uint Hour { get; set; }
            public uint Rank { get; set; }
        } // RankStat

        /// <summary>
        /// Per match betting information.
        /// </summary>
        private class BetStat
        {
            public ulong MatchId { get; set; }
            public uint AmountBet { get; set; }
        } // BetStat

        /// <summary>
        /// Per match checkpoint information.
        /// </summary>
        private class CheckpointStat
        {
            public ulong MatchId { get; set; }
            public uint CheckpointCount { get; set; }
        } // CheckpointStat

        /// <summary>
        /// Per match checkpoint information.
        /// </summary>
        private class JobEarnStat
        {
            public ulong MatchId { get; set; }
            public uint Amount { get; set; }
        } // JobEarnStat
        #endregion // Intermediate Result Classes

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public PerUserCashReport()
            : base(ReportNames.PerUserMPHourlyCashReport)
        {
        }
        #endregion // Constructor(s)

        #region VerticaReportBase Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        protected override PerUserHourlyCashStat Generate(ISession session, MPUserHourlyParams parameters)
        {
            // Create a lookup for all the stats based on the hour of play.
            IDictionary<uint, HourlyCashStat> statLookup = new SortedDictionary<uint, HourlyCashStat>();

            // Get cash spent/earned info.
            foreach (HourlyCashStatDetail stat in GetCashEarnedStats(session, parameters))
            {
                // Check whether our lookup already contains a stat for this hour.
                uint hour = stat.PlayingTime / 3600000u;
                HourlyCashStat hourlyStat;
                if (!statLookup.TryGetValue(hour, out hourlyStat))
                {
                    hourlyStat = new HourlyCashStat(hour, 1);
                    statLookup[hour] = hourlyStat;
                }

                hourlyStat.AddCashEarnedDetail(stat);
            }

            foreach (HourlyCashStatDetail stat in GetCashSpentStats(session, parameters))
            {
                // Check whether our lookup already contains a stat for this hour.
                uint hour = stat.PlayingTime / 3600000u;
                HourlyCashStat hourlyStat;
                if (!statLookup.TryGetValue(hour, out hourlyStat))
                {
                    hourlyStat = new HourlyCashStat(hour, 1);
                    statLookup[hour] = hourlyStat;
                }

                hourlyStat.AddCashSpentDetail(stat);
            }

            // Get xp info.
            foreach (HourlyCashStatDetail stat in GetExperienceStats(session, parameters))
            {
                // Check whether our lookup already contains a stat for this hour.
                uint hour = stat.PlayingTime / 3600000u;
                HourlyCashStat hourlyStat;
                if (!statLookup.TryGetValue(hour, out hourlyStat))
                {
                    hourlyStat = new HourlyCashStat(hour, 1);
                    statLookup[hour] = hourlyStat;
                }

                hourlyStat.AddXPEarnedDetail(stat);
            }

            // Get rank info.
            foreach (RankStat stat in GetRankStats(session, parameters))
            {
                HourlyCashStat hourlyStat;
                if (!statLookup.TryGetValue(stat.Hour, out hourlyStat))
                {
                    hourlyStat = new HourlyCashStat(stat.Hour, 1);
                    statLookup[stat.Hour] = hourlyStat;
                }
                hourlyStat.Rank = stat.Rank;
            }

            // We may need to patch up the rank data slightly as we won't have ranked up every hour.
            float previousRank = 0;
            foreach (HourlyCashStat stat in statLookup.Values)
            {
                if (stat.Rank == 0.0)
                {
                    stat.Rank = previousRank;
                }
                previousRank = stat.Rank;
            }

            // Get give/received cash.
            foreach (HourlyCashStatDetail stat in GetGiveStats(session, parameters, "PURCH"))
            {
                // Check whether our lookup already contains a stat for this hour.
                uint hour = stat.PlayingTime / 3600000u;
                HourlyCashStat hourlyStat;
                if (!statLookup.TryGetValue(hour, out hourlyStat))
                {
                    hourlyStat = new HourlyCashStat(hour, 1);
                    statLookup[hour] = hourlyStat;
                }

                hourlyStat.AddCashEarnedDetail(stat);
            }

            // Get give/received cash.
            foreach (HourlyCashStatDetail stat in GetGiveStats(session, parameters, "GIVE"))
            {
                // Check whether our lookup already contains a stat for this hour.
                uint hour = stat.PlayingTime / 3600000u;
                HourlyCashStat hourlyStat;
                if (!statLookup.TryGetValue(hour, out hourlyStat))
                {
                    hourlyStat = new HourlyCashStat(hour, 1);
                    statLookup[hour] = hourlyStat;
                }

                hourlyStat.AddCashSpentDetail(stat);
            }

            foreach (HourlyCashStatDetail stat in GetGiveStats(session, parameters, "GIVE_JOB"))
            {
                // Check whether our lookup already contains a stat for this hour.
                uint hour = stat.PlayingTime / 3600000u;
                HourlyCashStat hourlyStat;
                if (!statLookup.TryGetValue(hour, out hourlyStat))
                {
                    hourlyStat = new HourlyCashStat(hour, 1);
                    statLookup[hour] = hourlyStat;
                }

                hourlyStat.AddCashSpentDetail(stat);
            }

            foreach (HourlyCashStatDetail stat in GetReceiveStats(session, parameters, "GIVE", "RECEIVE"))
            {
                // Check whether our lookup already contains a stat for this hour.
                uint hour = stat.PlayingTime / 3600000u;
                HourlyCashStat hourlyStat;
                if (!statLookup.TryGetValue(hour, out hourlyStat))
                {
                    hourlyStat = new HourlyCashStat(hour, 1);
                    statLookup[hour] = hourlyStat;
                }

                hourlyStat.AddCashEarnedDetail(stat);
            }

            foreach (HourlyCashStatDetail stat in GetReceiveStats(session, parameters, "GIVE_JOB", "RECEIVE_JOB"))
            {
                // Check whether our lookup already contains a stat for this hour.
                uint hour = stat.PlayingTime / 3600000u;
                HourlyCashStat hourlyStat;
                if (!statLookup.TryGetValue(hour, out hourlyStat))
                {
                    hourlyStat = new HourlyCashStat(hour, 1);
                    statLookup[hour] = hourlyStat;
                }

                hourlyStat.AddCashEarnedDetail(stat);
            }

            // Get matches performed.
            foreach (MatchStat stat in GetMatchStats(session, parameters))
            {
                // Check whether our lookup already contains a stat for this hour.
                HourlyCashStat hourlyStat;
                if (!statLookup.TryGetValue(stat.Hour, out hourlyStat))
                {
                    hourlyStat = new HourlyCashStat(stat.Hour, 1);
                    statLookup[stat.Hour] = hourlyStat;
                }

                // Add this to the list of matches.
                RSG.Statistics.Common.Model.Telemetry.MatchStat matchStatDto = new RSG.Statistics.Common.Model.Telemetry.MatchStat();
                matchStatDto.MatchId = stat.MatchId;
                matchStatDto.UGCIdentifier = stat.UGCIdentifier;
                matchStatDto.Name = stat.MatchName;
                matchStatDto.Creator = stat.MatchCreator;
                matchStatDto.CreatorPlatform = stat.MatchCreatorPlatform;
                matchStatDto.Type = (stat.MatchType != null ? stat.MatchType.ToString() : null);
                matchStatDto.Timestamp = stat.Timestamp;
                matchStatDto.XPEarned = stat.Experience;
                matchStatDto.CashEarned = stat.CashEarned;
                matchStatDto.Result = stat.Result;
                matchStatDto.MaxSurvivalWave = stat.MaxSurvivalWave;
                matchStatDto.Rank = stat.Rank;
                matchStatDto.Players = stat.Players;
				matchStatDto.Duration = stat.Duration;
                matchStatDto.BetWinnings = stat.BetWinnings;

                hourlyStat.AddMatchStat(matchStatDto);
            }

            // Create a match id => match stat lookup.
            IDictionary<ulong, RSG.Statistics.Common.Model.Telemetry.MatchStat> matchStatLookup =
                new Dictionary<ulong, RSG.Statistics.Common.Model.Telemetry.MatchStat>();
            foreach (RSG.Statistics.Common.Model.Telemetry.MatchStat matchStat in statLookup.Values.SelectMany(item => item.Matches))
            {
                matchStatLookup[matchStat.MatchId] = matchStat;
            }

            // Match up the amount bet to the match stats.
            foreach (BetStat betStat in session.RetrieveAll<BetStat>(new StringQuery(CreateBettingQuery(parameters))))
            {
                RSG.Statistics.Common.Model.Telemetry.MatchStat matchStat;
                if (matchStatLookup.TryGetValue(betStat.MatchId, out matchStat))
                {
                    matchStat.AmountBet = betStat.AmountBet;
                }
            }

            // Match up the checkpoint stats to the match stats.
            try
            {
                foreach (CheckpointStat checkpointStat in session.RetrieveAll<CheckpointStat>(new StringQuery(CreateCheckpointQuery(parameters))))
                {
                    RSG.Statistics.Common.Model.Telemetry.MatchStat matchStat;
                    if (matchStatLookup.TryGetValue(checkpointStat.MatchId, out matchStat))
                    {
                        matchStat.CheckpointCount = checkpointStat.CheckpointCount;
                    }
                }
            }
            catch (System.Exception ex)
            {
            	// Ok to ignore.  prod doesn't have the checkpoint table yet.
            }

            // Match up the job reward stats to the match stats.
            foreach (JobEarnStat earnStat in session.RetrieveAll<JobEarnStat>(new StringQuery(CreateJobEarnQuery(parameters))))
            {
                RSG.Statistics.Common.Model.Telemetry.MatchStat matchStat;
                if (matchStatLookup.TryGetValue(earnStat.MatchId, out matchStat))
                {
                    matchStat.JobRewardCash = earnStat.Amount;
                }
            }

            // Get hold ups performed.
            foreach (MatchStat stat in GetHoldUpStats(session, parameters))
            {
                // Check whether our lookup already contains a stat for this hour.
                HourlyCashStat hourlyStat;
                if (!statLookup.TryGetValue(stat.Hour, out hourlyStat))
                {
                    hourlyStat = new HourlyCashStat(stat.Hour, 1);
                    statLookup[stat.Hour] = hourlyStat;
                }

                // Add this to the list of matches.
                RSG.Statistics.Common.Model.Telemetry.MatchStat matchStatDto = new RSG.Statistics.Common.Model.Telemetry.MatchStat();
                matchStatDto.Name = stat.MatchName;
                matchStatDto.Timestamp = stat.Timestamp;
                matchStatDto.XPEarned = stat.Experience;
                matchStatDto.CashEarned = stat.CashEarned;

                hourlyStat.AddMatchStat(matchStatDto);
            }

            // Finally create the return object.
            PerUserHourlyCashStat result =
                new PerUserHourlyCashStat(parameters.AccountId, parameters.Platform, parameters.CharacterSlot, parameters.CharacterId);
            result.Data.AddRange(statLookup.Values);
            return result;
        }
        #endregion // IStatsReport Implementation

        #region Private Methods
        /// <summary>
        /// Retrieves the amount of cash earned for the specified set of users.
        /// </summary>
        private IEnumerable<HourlyCashStatDetail> GetCashEarnedStats(ISession session, MPUserHourlyParams parameters)
        {
            VerticaDataReader dr = session.ExecuteQuery(CreateEarnSqlQueryString(parameters));
            while (dr.Read())
            {
                HourlyCashStatDetail stat = new HourlyCashStatDetail();
                stat.PlayingTime = dr.GetUInt32(0);
                stat.Timestamp = dr.GetDateTime(1);
                stat.Action = dr.GetString(2);
                if (dr[3] != DBNull.Value)
                {
                    stat.Category = dr.GetString(3);
                }
                stat.Amount = dr.GetUInt32(4);
                yield return stat;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private String CreateEarnSqlQueryString(MPUserHourlyParams parameters)
        {
            // We want to replicate a query along the following lines:
            //
            // SELECT e.PlayingTime, e.DateTime, e.Action, e.Category, e.Amount
            // FROM dev_gta5_11_ps3.Telemetry_Gta5Online_EARN e
            // INNER JOIN dev_gta5_11_ps3.Telemetry_Gta5_header h on h.SubmissionId=e.SubmissionId
            // INNER JOIN SocialClub_np_dev.PlayerAccount pa on pa.AccountId=h.AccountId
            // WHERE pa.UserName IN ('MrT_RSN') AND h.MpCharacterSlot=0 AND h.MpCharacterId=1;
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT e.PlayingTime, e.DateTime, e.Action, e.Category, e.Amount ");
            sb.AppendFormat("FROM {0}.Telemetry_Gta5Online_EARN2 e ", Conversion.Schema());
            AddRestrictions(sb, parameters, "e");
            sb.Append("ORDER BY e.PlayingTime;");
            return sb.ToString();
        }

        /// <summary>
        /// Adds the required restrictions to the string builder.
        /// </summary>
        private void AddRestrictions(StringBuilder sb, MPUserHourlyParams parameters, String tableAlias, IEnumerable<String> additionalRestrictions = null)
        {
            // Check whether we need to join against the header table
            if (parameters.Builds.Any())
            {
                sb.AppendFormat("INNER JOIN {0}.Telemetry_Gta5_header h on h.SubmissionId=s.SubmissionId ",
                    Conversion.Schema());
            }

            // Get the list of restrictions and add them to the query string if we got some.
            IEnumerable<String> restrictions = GetRestrictions(parameters, tableAlias);
            if (additionalRestrictions != null)
            {
                restrictions = restrictions.Union(additionalRestrictions);
            }

            if (restrictions.Any())
            {
                sb.AppendFormat("WHERE {0} ", String.Join(" AND ", restrictions));
            }
        }

        /// <summary>
        /// Retrieves a list of restrictions that should be added to the sql query string.
        /// </summary>
        private IEnumerable<String> GetRestrictions(MPUserHourlyParams parameters, String tableAlias)
        {
            yield return (String.Format("{0}.PlatformId = '{1}'", tableAlias, (int)parameters.Platform));
            yield return (String.Format("{0}.Header_AccountId = '{1}'", tableAlias, parameters.AccountId));
            yield return (String.Format("{0}.Header_cid = {1}", tableAlias, parameters.CharacterSlot));
            yield return (String.Format("{0}.Header_mp = {1}", tableAlias, parameters.CharacterId));
            if (parameters.Builds.Any())
            {
                yield return String.Format("h.ver IN ({0})", String.Join(",", parameters.Builds));
            }
        }

        /// <summary>
        /// Retrieves the amount of cash spent for the specified set of users.
        /// </summary>
        private IEnumerable<HourlyCashStatDetail> GetCashSpentStats(ISession session, MPUserHourlyParams parameters)
        {
            VerticaDataReader dr = session.ExecuteQuery(CreateSpendSqlQueryString(parameters));
            while (dr.Read())
            {
                HourlyCashStatDetail stat = new HourlyCashStatDetail();
                stat.PlayingTime = dr.GetUInt32(0);
                stat.Timestamp = dr.GetDateTime(1);
                stat.Action = dr.GetString(2);
                if (dr[3] != DBNull.Value)
                {
                    stat.Category = dr.GetString(3);
                }
                stat.Amount = dr.GetUInt32(4);
                yield return stat;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private String CreateSpendSqlQueryString(MPUserHourlyParams parameters)
        {
            // We want to replicate a query along the following lines:
            //
            // SELECT s.PlayingTime, s.DateTime, s.Action, s.Category, s.Amount
            // FROM dev_gta5_11_ps3.Telemetry_Gta5Online_SPEND s
            // INNER JOIN dev_gta5_11_ps3.Telemetry_Gta5_header h on h.SubmissionId=s.SubmissionId
            // INNER JOIN SocialClub_np_dev.PlayerAccount pa on pa.AccountId=h.AccountId
            // WHERE pa.UserName IN ('MrT_RSN') AND h.MpCharacterSlot=0 AND h.MpCharacterId=1;
            String schema = Conversion.Schema();

            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT s.PlayingTime, s.DateTime, s.Action, s.Category, s.Amount ");
            sb.AppendFormat("FROM {0}.Telemetry_Gta5Online_SPEND2 s ", schema);
            AddRestrictions(sb, parameters, "s");
            sb.Append("ORDER BY s.PlayingTime;");
            return sb.ToString();
        }

        /// <summary>
        /// Retrieves rank information.
        /// </summary>
        private IEnumerable<RankStat> GetRankStats(ISession session, MPUserHourlyParams parameters)
        {
            VerticaDataReader dr = session.ExecuteQuery(CreateRankQuery(parameters));
            while (dr.Read())
            {
                RankStat stat = new RankStat();
                stat.Hour = dr.GetUInt32(0);
                stat.Rank = dr.GetUInt32(1);
                yield return stat;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private String CreateRankQuery(MPUserHourlyParams parameters)
        {
            // We want to replicate a query along the following lines:
            //
            // SELECT FLOOR(r.PlayingTime / 3600000) AS Hour, MAX(r.Rank) AS Rank
            // FROM dev_gta5_11_ps3.Telemetry_Gta5_RANK_UP r
            // INNER JOIN dev_gta5_11_ps3.Telemetry_Gta5_header h on h.SubmissionId=r.SubmissionId
            // INNER JOIN SocialClub_np_dev.PlayerAccount pa on r.AccountId=h.AccountId
            // WHERE pa.UserName IN ('MrT_RSN') AND h.MpCharacterSlot=0 AND h.MpCharacterId=1
            // GROUP BY Hour;
            String schema = Conversion.Schema();

            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT FLOOR(r.PlayingTime / 3600000) AS Hour, MAX(r.Rank) AS Rank ");
            sb.AppendFormat("FROM {0}.Telemetry_Gta5_RANK_UP r ", schema);
            AddRestrictions(sb, parameters, "r");
            sb.Append("GROUP BY Hour;");
            return sb.ToString();
        }

        /// <summary>
        /// Retrieves the amount of cash spent for the specified set of users.
        /// </summary>
        private IEnumerable<HourlyCashStatDetail> GetExperienceStats(ISession session, MPUserHourlyParams parameters)
        {
            VerticaDataReader dr = session.ExecuteQuery(CreateExperienceSqlQueryString(parameters));
            while (dr.Read())
            {
                HourlyCashStatDetail stat = new HourlyCashStatDetail();
                if (dr[0] != DBNull.Value)
                {
                    stat.PlayingTime = dr.GetUInt32(0);
                }
                stat.Timestamp = dr.GetDateTime(1);
                stat.Action = dr.GetString(2);
                stat.Category = dr.GetString(3);
                stat.Amount = dr.GetUInt32(4);
                yield return stat;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private String CreateExperienceSqlQueryString(MPUserHourlyParams parameters)
        {
            // We want to replicate a query along the following lines:
            //
            // SELECT xp.PlayingTime, xp.DateTime, xp.AwardType, xp.Category, xp.Amount
            // FROM dev_gta5_11_ps3.Telemetry_Gta5Online_AWARD_XP xp
            // INNER JOIN dev_gta5_11_ps3.Telemetry_Gta5_header h on h.SubmissionId=xp.SubmissionId
            // INNER JOIN SocialClub_np_dev.PlayerAccount pa on pa.AccountId=h.AccountId
            // WHERE pa.UserName IN ('MrT_RSN') AND h.MpCharacterSlot=0 AND h.MpCharacterId=1;
            String schema = Conversion.Schema();

            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT xp.PlayingTime, xp.DateTime, xp.AwardType, xp.Category, xp.Amount ");
            sb.AppendFormat("FROM {0}.Telemetry_Gta5Online_AWARD_XP xp ", schema);
            AddRestrictions(sb, parameters, "xp");
            sb.Append(";");
            return sb.ToString();
        }

        /// <summary>
        /// Retrieves the amount of cash earned for the specified set of users.
        /// </summary>
        private IEnumerable<HourlyCashStatDetail> GetGiveStats(ISession session, MPUserHourlyParams parameters, String table)
        {
            VerticaDataReader dr = session.ExecuteQuery(CreateGiveQuery(parameters, table));
            while (dr.Read())
            {
                HourlyCashStatDetail stat = new HourlyCashStatDetail();
                stat.PlayingTime = dr.GetUInt32(0);
                stat.Timestamp = dr.GetDateTime(1);
                stat.Action = table;
                stat.Amount = dr.GetUInt32(2);
                yield return stat;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private String CreateGiveQuery(MPUserHourlyParams parameters, String table)
        {
            // We want to replicate a query along the following lines:
            //
            // SELECT g.PlayingTime, g.DateTime, g.Amount AS Amount
            // FROM dev_gta5_11_ps3.Telemetry_Gta5Online_GIVE g
            // INNER JOIN dev_gta5_11_ps3.Telemetry_Gta5_header h on h.SubmissionId=g.SubmissionId
            // INNER JOIN SocialClub_np_dev.PlayerAccount pa on pa.AccountId=h.AccountId
            // WHERE pa.UserName IN ('MrT_RSN') AND h.MpCharacterSlot=0 AND h.MpCharacterId=1;
            String schema = Conversion.Schema();

            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT g.PlayingTime, g.DateTime, g.Amount ");
            sb.AppendFormat("FROM {0}.Telemetry_Gta5Online_{1} g ", schema, table);
            AddRestrictions(sb, parameters, "g");
            sb.Append("ORDER BY g.PlayingTime;");
            return sb.ToString();
        }

        /// <summary>
        /// Retrieves the amount of cash earned for the specified set of users.
        /// </summary>
        private IEnumerable<HourlyCashStatDetail> GetReceiveStats(ISession session, MPUserHourlyParams parameters, String table, String action)
        {
            VerticaDataReader dr = session.ExecuteQuery(CreateReceiveQuery(parameters, table));
            while (dr.Read())
            {
                HourlyCashStatDetail stat = new HourlyCashStatDetail();
                stat.PlayingTime = dr.GetUInt32(0);
                stat.Timestamp = dr.GetDateTime(1);
                stat.Action = action;
                stat.Amount = dr.GetUInt32(2);
                yield return stat;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private String CreateReceiveQuery(MPUserHourlyParams parameters, String table)
        {
            // We want to replicate a query along the following lines:
            //
            // SELECT g.PlayingTime, g.DateTime, g.Amount AS Amount
            // FROM dev_gta5_11_ps3.Telemetry_Gta5Online_GIVE g
            // INNER JOIN dev_gta5_11_ps3.Telemetry_Gta5_header h on h.SubmissionId=g.SubmissionId
            // INNER JOIN SocialClub_np_dev.PlayerAccount pa on pa.AccountId=h.AccountId
            // WHERE pa.UserName IN ('MrT_RSN') AND h.MpCharacterSlot=0 AND h.MpCharacterId=1;
            String schema = Conversion.Schema();

            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT g.PlayingTime, g.DateTime, g.Amount ");
            sb.AppendFormat("FROM {0}.Telemetry_Gta5Online_{1} g ", schema, table);
            AddCustomPlayerAccountJoin(sb, parameters.Platform);
            sb.AppendFormat("WHERE pa.AccountId={0} ", parameters.AccountId);
            sb.Append("ORDER BY g.PlayingTime;");
            return sb.ToString();
        }

        /// <summary>
        /// Adds the distinct player account join.
        /// </summary>
        protected void AddCustomPlayerAccountJoin(StringBuilder sb, ROSPlatform platform)
        {
            // The join needs to take into account the most recent record via the LastUpdatedDate.
            //
            // INNER JOIN
            // (
            //     SELECT *, ROW_NUMBER() OVER(PARTITION BY AccountId ORDER BY LastUpdatedDate DESC) AS HistoryIndex
            //     FROM SocialClub.PlayerAccount
            // ) pa ON HistoryIndex=1 AND pa.AccountId=h.AccountId
            sb.AppendFormat("INNER JOIN ");
            sb.AppendFormat("( ");
            sb.AppendFormat("SELECT *, ROW_NUMBER() OVER(PARTITION BY AccountId, PlatformId ORDER BY LastUpdatedDate DESC) AS HistoryIndex ");
            sb.AppendFormat("FROM {0}.PlayerAccount ", Conversion.SocialClubSchema());
            sb.AppendFormat("WHERE PlatformId={0} ", platform.PlatformToRosId());
            sb.AppendFormat(") pa ON pa.HistoryIndex=1 AND pa.UserId=g.ToUserId ");
        }

        /// <summary>
        /// Retrieves the amount of cash spent for the specified set of users.
        /// </summary>
        private IEnumerable<MatchStat> GetMatchStats(ISession session, MPUserHourlyParams parameters)
        {
            return session.RetrieveAll<MatchStat>(new StringQuery(CreateJobQuery(parameters)));
        }

        /// <summary>
        /// 
        /// </summary>
        private String CreateJobQuery(MPUserHourlyParams parameters)
        {
            // We want to replicate a query along the following lines:
            //
            // SELECT
            //     JobStats.GamerHandle AS GamerHandle,
            //     JobStats.Hour AS Hour,
            //     JobStats.Experience AS xp,
            //     JobStats.Cash AS Cash,
            //     JobStats.DateTime AS Timestamp,
            //     JobStats.Version AS Version,
            //     MatchStats.mt AS MatchType,
            //     MatchStats.mc AS MatchCreator,
            //     MatchStats.mid AS UGCId
            // FROM
            // (
            //     SELECT
            //         pa.UserName AS GamerHandle,
            //         FLOOR(pmb.playingTime / 3600000) AS Hour,
            //         pmb.xp AS Experience,
            //         pmb.cash AS Cash,
            //         h.mid
            //      FROM dev_gta5_11_ps3.Telemetry_Gta5_JOB pmb
            //      INNER JOIN dev_gta5_11_ps3.Telemetry_Gta5_header h on h.SubmissionId=pmb.SubmissionId
            //      INNER JOIN (SELECT pa.AccountId, pa.UserName FROM SocialClub_np_dev.PlayerAccount pa GROUP BY pa.AccountId, pa.UserName) pa on pa.AccountId=h.AccountId
            //      WHERE pa.UserName IN ('ColinH__RSN_56') AND h.ver=5488
            // ) AS JobStats
            // ORDER BY JobStats.DateTime;
            String schema = Conversion.Schema();

            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT ");
            sb.Append("JobStats.Hour AS Hour, ");
            sb.Append("JobStats.Experience AS Experience, ");
            sb.Append("JobStats.Cash AS CashEarned, ");
            sb.Append("JobStats.Timestamp AS Timestamp, ");
            sb.Append("JobStats.MatchType AS MatchType, ");
            sb.Append("JobStats.MatchCreator AS MatchCreator, ");
            sb.Append("JobStats.PublicContentId AS UGCIdentifier, ");
            sb.Append("ugcm.ContentName AS MatchName, ");
            sb.Append("ugcm.PlatformId AS MatchCreatorPlatform, ");
            sb.Append("JobStats.Result AS Result, ");
            sb.Append("JobStats.MaxSurvivalWave AS MaxSurvivalWave, ");
            sb.Append("JobStats.Rank AS Rank, ");
            sb.Append("PlayersPerMatch.ParticipantCount AS Players, ");
			sb.Append("JobStats.MatchDuration AS Duration, ");
            sb.Append("JobStats.MatchId AS MatchId, ");
            sb.Append("JobStats.BetWinnings AS BetWinnings ");
            sb.Append("FROM (");
            {
                sb.Append("SELECT ");
                sb.Append("job.DateTime AS Timestamp, ");
                sb.Append("FLOOR(job.PlayingTime / 3600000) AS Hour, ");
                sb.Append("job.XP AS Experience, ");
                sb.Append("IFNULL(job.CashEarned, 0) AS Cash, ");
                sb.Append("job.MatchType, ");
                sb.Append("job.MatchCreator, ");
                sb.Append("job.PublicContentId, ");
                sb.Append("IFNULL(job.Result, 6) AS Result, ");
                sb.Append("job.MaxSurvivalWave, ");
                sb.Append("job.Rank, ");
				sb.Append("IFNULL(job.MatchDuration, 0) AS MatchDuration, ");
                sb.Append("job.Header_mid AS MatchId, ");
                sb.Append("job.BetWinnings AS BetWinnings ");
                sb.AppendFormat("FROM {0}.Telemetry_Gta5_JOB job ", schema);

                IList<String> restrictions = new List<String>();
                restrictions.Add("job.PlayingTime IS NOT NULL");
                restrictions.Add("job.Header_mid != 0");
                if (parameters.Builds.Any())
                {
                    restrictions.Add(String.Format("h.ver IN ({0})", String.Join(",", parameters.Builds)));
                }
                AddRestrictions(sb, parameters, "job", restrictions);
            }
            sb.Append(") AS JobStats ");
            sb.Append("INNER JOIN ( ");
            {
                sb.Append("SELECT ");
                sb.Append("job.Header_mid AS MatchId, ");
                sb.Append("COUNT(DISTINCT job.Header_AccountId) AS ParticipantCount ");
                sb.AppendFormat("FROM {0}.Telemetry_Gta5_JOB job ", schema);

                IList<String> restrictions = new List<String>();
                restrictions.Add("job.Header_mid != 0");
                if (parameters.Builds.Any())
                {
                    sb.AppendFormat("INNER JOIN {0}.Telemetry_Gta5_header h on h.SubmissionId=job.SubmissionId ", schema);
                    restrictions.Add(String.Format("h.ver IN ({0})", String.Join(",", parameters.Builds)));
                }
                sb.AppendFormat("WHERE {0} ", String.Join(" AND ", restrictions));
                sb.Append("GROUP BY job.Header_mid ");
            }
            sb.Append(") AS PlayersPerMatch ON PlayersPerMatch.MatchId=JobStats.MatchId ");
            sb.Append("LEFT JOIN ( ");
            //AddUGCInfoQuery(sb);


            sb.Append("SELECT PublicContentId, ContentName, PlatformId, ");
            sb.Append("ROW_NUMBER() OVER(PARTITION BY PublicContentId ORDER BY UpdatedDate DESC) AS HistoryIndex ");
            sb.Append("FROM ugc.gta5mission_metadata ");
            sb.Append(") AS ugcm ON ugcm.HistoryIndex=1 AND ugcm.PublicContentId=JobStats.PublicContentId;");



            //sb.Append(") AS UGCInfo ON JobStats.PublicContentId=UGCInfo.UGCIdentifier; ");
            return sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        private void AddCustomMatchRestrictions(StringBuilder sb, MPUserHourlyParams parameters, String tableAlias)
        {
            // Get the list of restrictions and add them to the query string if we got some.
            IList<String> restrictions = GetRestrictions(parameters, tableAlias).ToList();
            restrictions.Add(tableAlias + ".playingTime IS NOT NULL");
            restrictions.Add(tableAlias + ".cash IS NOT NULL");
            restrictions.Add(tableAlias + ".Header_mid != 0");
            if (parameters.Builds.Any())
            {
                restrictions.Add(String.Format("h.ver IN ({0})", String.Join(",", parameters.Builds)));
            }

            sb.AppendFormat("WHERE {0} ", String.Join(" AND ", restrictions));
        }

        /// <summary>
        /// 
        /// </summary>
        private void AddUGCInfoQuery(StringBuilder sb)
        {
            sb.Append("SELECT ");
            sb.Append("AllUGC.PublicContentId AS UGCIdentifier, ");
            sb.Append("AllUGC.ContentName AS UGCName, ");
            sb.Append("AllPlayerAccounts.UserName AS CreatorName, ");
            sb.Append("AllUGC.PlatformId AS CreatorPlatform ");
            sb.Append("FROM ( ");
            sb.Append("SELECT *, ROW_NUMBER() OVER(PARTITION BY PublicContentId ORDER BY UpdatedDate DESC) AS HistoryIndex ");
            sb.Append("FROM ugc.gta5mission_metadata ");
            sb.Append(") AS AllUGC ");
            sb.Append("INNER JOIN (");
            sb.Append("SELECT *, ROW_NUMBER() OVER(PARTITION BY AccountId, PlatformId ORDER BY UpdatedDate DESC) AS HistoryIndex ");
            sb.Append("FROM SocialClub.PlayerAccount ");
            sb.Append(") AllPlayerAccounts ON AllUGC.HistoryIndex=1 AND AllPlayerAccounts.HistoryIndex=1 AND ");
            sb.Append("AllUGC.UserId=AllPlayerAccounts.UserId AND AllUGC.PlatformId=AllPlayerAccounts.PlatformId ");
        }

        /// <summary>
        /// 
        /// </summary>
        private String CreateBettingQuery(MPUserHourlyParams parameters)
        {
            // We want to create a query along the lines of the following:
            //
            // SELECT rb.Header_mid AS MatchId, SUM(rb.Amount) AS AmountBet
            // FROM dev_gta5_11_ps3.Telemetry_Gta5_ROS_BET rb
            // INNER JOIN
            // (
            //     SELECT *, ROW_NUMBER() OVER(PARTITION BY AccountId, PlatformId ORDER BY LastUpdatedDate DESC) AS HistoryIndex
            //     FROM SocialClub.PlayerAccount
            // ) pa ON pa.HistoryIndex=1 AND rc.Header_AccountId=pa.AccountId AND pa.PlatformId=2
            // WHERE pa.UserName IN ('ColinH__RSN_56') AND h.ver=5488
            // GROUP BY rb.Header_mid;
            String schema = Conversion.Schema();

            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT rb.Header_mid AS MatchId, SUM(rb.Amount) AS AmountBet ");
            sb.AppendFormat("FROM {0}.Telemetry_Gta5_ROS_BET rb ", schema);
            AddRestrictions(sb, parameters, "rb");
            sb.Append("GROUP BY rb.Header_mid;");

            return sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        private String CreateCheckpointQuery(MPUserHourlyParams parameters)
        {
            // We want to create a query along the lines of the following:
            //
            // SELECT rc.Header_mid AS MatchId, COUNT(*)
            // FROM dev_gta5_11_ps3.Telemetry_Gta5_RACE_CHECKPOINT rc
            // INNER JOIN dev_gta5_11_ps3.Telemetry_Gta5_JOB j ON j.Header_mid = rc.Header_mid AND j.Header_AccountId = rc.Header_AccountId
            // INNER JOIN
            // (
            //     SELECT *, ROW_NUMBER() OVER(PARTITION BY AccountId, PlatformId ORDER BY LastUpdatedDate DESC) AS HistoryIndex
            //     FROM SocialClub.PlayerAccount
            // ) pa ON pa.HistoryIndex=1 AND rc.Header_AccountId=pa.AccountId AND pa.PlatformId=2
            // WHERE j.MatchType IN (2, 8) AND pa.UserName IN ('ColinH__RSN_56') AND h.ver=5488
            // GROUP BY rc.Header_mid;
            String schema = Conversion.Schema();

            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT rc.Header_mid AS MatchId, COUNT(*) AS CheckpointCount ");
            sb.AppendFormat("FROM {0}.Telemetry_Gta5_RACE_CHECKPOINT rc ", schema);
            sb.AppendFormat("INNER JOIN {0}.Telemetry_Gta5_JOB j ON j.Header_mid = rc.Header_mid AND j.Header_AccountId = rc.Header_AccountId ", schema);
            AddRestrictions(sb, parameters, "rc", new String[] { "j.MatchType IN (2, 8)" });
            sb.Append("GROUP BY rc.Header_mid;");

            return sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        private String CreateJobEarnQuery(MPUserHourlyParams parameters)
        {
            // We want to create a query along the lines of the following:
            //
            // SELECT e.MatchId AS MatchId, e.Amount) AS Amount
            // FROM dev_gta5_11_ps3.Telemetry_Gta5Online_EARN e
            // INNER JOIN dev_gta5_11_ps3.Telemetry_Gta5_header h ON h.SubmissionId=e.SubmissionId
            // INNER JOIN
            // (
            //     SELECT *, ROW_NUMBER() OVER(PARTITION BY AccountId, PlatformId ORDER BY LastUpdatedDate DESC) AS HistoryIndex
            //     FROM SocialClub.PlayerAccount
            // ) pa ON pa.HistoryIndex=1 AND rc.Header_AccountId=pa.AccountId AND pa.PlatformId=2
            // WHERE j.MatchType IN (2, 8) AND pa.UserName IN ('ColinH__RSN_56') AND h.ver=5488;
            String schema = Conversion.Schema();

            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT e.Header_mid AS MatchId, e.Amount AS Amount ");
            sb.AppendFormat("FROM {0}.Telemetry_Gta5Online_EARN2 e ", schema);
            AddRestrictions(sb, parameters, "e", new String[] { "e.Action='JOB'", "e.Header_mid != 0" });
            sb.Append(";");

            return sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        private IEnumerable<MatchStat> GetHoldUpStats(ISession session, MPUserHourlyParams parameters)
        {
            VerticaDataReader dr = session.ExecuteQuery(CreateHoldUpQuery(parameters));
            while (dr.Read())
            {
                MatchStat stat = new MatchStat();
                stat.Hour = dr.GetUInt32(0) / 3600000;
                stat.Timestamp = dr.GetDateTime(1);
                stat.CashEarned = dr.GetInt32(2);
                stat.Experience = dr.GetUInt32(3);
                stat.MatchName = "HOLD UP";
                yield return stat;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private String CreateHoldUpQuery(MPUserHourlyParams parameters)
        {
            // We want to replicate a query along the following lines:
            //
            // SELECT
	        //     hu.PlayingTime, hu.DateTime, hu.ce - hu.cs AS CashEarned, hu.xp
            // FROM dev_gta5_11_ps3.Telemetry_Gta5_HOLD_UP hu
            // INNER JOIN dev_gta5_11_ps3.Telemetry_Gta5_header h on h.SubmissionId=hu.SubmissionId
            // INNER JOIN
            // (
            //     SELECT *, ROW_NUMBER() OVER(PARTITION BY AccountId, PlatformId ORDER BY LastUpdatedDate DESC) AS HistoryIndex
            //     FROM SocialClub.PlayerAccount
            // ) pa ON pa.HistoryIndex=1 AND h.AccountId=pa.AccountId AND pa.PlatformId=2
            // WHERE pa.UserName IN ('ColinH__RSN_56') AND h.ver=5728 AND hu.PlayingTime IS NOT NULL
            // ORDER BY hu.DateTime ASC;
            String schema = Conversion.Schema();

            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT hu.PlayingTime, hu.DateTime, hu.ce - hu.cs AS CashEarned, hu.xp ");
            sb.AppendFormat("FROM {0}.Telemetry_Gta5_HOLD_UP hu ", schema);
            AddRestrictions(sb, parameters, "hu", new String[] { "hu.PlayingTime IS NOT NULL" });
            sb.Append("ORDER BY hu.DateTime ASC;");
            return sb.ToString();
        }
        #endregion // Private Methods
    } // PerUserCashReport
}
