﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common.Config;

namespace RSG.Statistics.VerticaServices.TransManager
{
    /// <summary>
    /// Vertica based transaction manager factory.
    /// </summary>
    internal sealed class TransManagerFactory : ITransManagerFactory
    {
        #region Static Instance
        /// <summary>
        /// Transaction manager factory.
        /// </summary>
        internal static ITransManagerFactory Instance { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Private constructor as there should only ever be one instance of this class.
        /// </summary>
        private TransManagerFactory()
        {
        }

        /// <summary>
        /// Static constructor for creating the trans manager factory instance.
        /// </summary>
        static TransManagerFactory()
        {
            Instance = new TransManagerFactory();
        }
        #endregion // Constructor(s)

        #region ITransManagerFactory Implementation
        /// <summary>
        /// Simply creates a new transaction manager.
        /// </summary>
        /// <returns></returns>
        public ITransManager CreateManager(IVerticaServer server, bool readOnly)
        {
            return new TransManager(server);
        }
        #endregion // ITransManagerFactory Implementation
    } // TransManagerFactory
}
