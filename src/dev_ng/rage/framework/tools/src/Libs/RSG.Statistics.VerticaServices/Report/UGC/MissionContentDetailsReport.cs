﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Linq;
using System.Text;
using RSG.ROS;
using RSG.Statistics.Common;
using RSG.Statistics.Common.Model.UGC;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.VerticaServices.DataAccess;
using RSG.Statistics.VerticaServices.Util;
using Vertica.Data.VerticaClient;

namespace RSG.Statistics.VerticaServices.Report.UGC
{
    /// <summary>
    /// Report that provides detailed UGC mission information.
    /// </summary>
    [Export(typeof(IVerticaReport))]
    internal class MissionContentDetailsReport : VerticaReportBase<List<MissionDetails>, MissionContentParams>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public MissionContentDetailsReport()
            : base(ReportNames.MissionContentDetailsReport)
        {
        }
        #endregion // Constructor(s)

        #region VerticaReportBase Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        protected override List<MissionDetails> Generate(ISession session, MissionContentParams parameters)
        {
            List<MissionDetails> details = session.RetrieveAll<MissionDetails>(new StringQuery(CreateQuery(parameters))).ToList();

            // Patch up the radio station names before returning the data.
            MatchType[] radioTypes = new MatchType[]
                {
                    MatchType.Race,
                    MatchType.Deathmatch,
                    MatchType.Survival,
                    MatchType.BaseJump,
                    MatchType.Mission
                };

            foreach (MissionDetails detail in details.Where(item => radioTypes.Contains(item.Type)))
            {
                String radioName;
                if (detail.RadioStationHash == 0)
                {
                    radioName = "Not Set";
                }
                else if (!parameters.RadioStationLookup.TryGetValue(detail.RadioStationHash, out radioName))
                {
                    radioName = String.Format("Unknown Radio Station (Hash: {0})", detail.RadioStationHash);
                }
                detail.RadioStation = radioName;
            }
            return details;
        }
        #endregion // IStatsReport Implementation

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private String CreateQuery(MissionContentParams parameters)
        {
            // We want to replicate a query along the following lines:
            //
            // SELECT
            //     PublicContentId, Data_Gen_Type AS Type, Data_Gen_Subtype AS SubType, ContentName AS Name, Description, Data_Gen_Rank AS Rank,
            //     Data_Gen_MinPlayers AS MinPlayers, Data_Gen_MaxPlayers AS MaxPlayers, Data_Gen_MinTeams AS MinTeams, Data_Gen_MaxTeams AS MaxTeams,
            //     Data_Gen_CharacterContact AS ContactId, UserName AS CreatorName, AllMissions.PlatformId AS CreatorPlatform,
            //     CreatedDate AS CreationDate, PublishedDate, UpdatedDate, DeletedDate, CategoryId AS Category, Data_Rule_TimeOfDay AS Availability,
            //     IFNULL(Data_Gen_StartX, 0) AS StartX, IFNULL(Data_Gen_StartY, 0) AS StartY, IFNULL(Data_Gen_StartZ, 0) AS StartZ,
            //     IFNULL(Data_Gen_Xp, 0) AS XP, IFNULL(Data_Gen_Cash, 0) AS Cash, Data_Gen_Music AS RadioStationHash
            // FROM
            // (
            //     SELECT *, ROW_NUMBER() OVER(PARTITION BY RootContentId ORDER BY UpdatedDate DESC) AS HistoryIndex
            //     FROM ugc.gta5mission_metadata
            // ) AS AllMissions
            // INNER JOIN
            // (
            //     SELECT UserName, UserId, PlatformId, ROW_NUMBER() OVER(PARTITION BY AccountId, PlatformId ORDER BY ISNULL(LastUpdatedDate, CreatedDate) DESC) AS HistoryIndex
            //     FROM SocialClub.PlayerAccount
            // ) AllPlayerAccounts
            // ON AllMissions.HistoryIndex=1 AND AllPlayerAccounts.HistoryIndex=1 AND AllMissions.UserId=AllPlayerAccounts.UserId AND AllMissions.PlatformId=AllPlayerAccounts.PlatformId
            // WHERE CategoryId=1 AND DeletedDate IS NULL AND LatestVersionContentId = ContentId;

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("SELECT ");
            sb.AppendFormat("PublicContentId, Data_Gen_Type AS Type, Data_Gen_Subtype AS SubType, ContentName AS Name, Description, Data_Gen_Rank AS Rank, ");
            sb.AppendFormat("Data_Gen_MinPlayers AS MinPlayers, Data_Gen_MaxPlayers AS MaxPlayers, Data_Gen_MinTeams AS MinTeams, Data_Gen_MaxTeams AS MaxTeams, ");
            sb.AppendFormat("Data_Gen_CharacterContact AS ContactId, IFNULL(UserName, AllMissions.UserId) AS CreatorName, AllMissions.PlatformId AS CreatorPlatform, ");
            sb.AppendFormat("CreatedDate AS CreationDate, PublishedDate, UpdatedDate, DeletedDate, CategoryId AS Category, ");
            sb.AppendFormat("Data_Rule_TimeOfDay AS Availability, Data_Gen_LaunchTimes AS TimeOfDaySurrogate, ");
            sb.AppendFormat("IFNULL(Data_Gen_StartX, 0) AS StartX, IFNULL(Data_Gen_StartY, 0) AS StartY, IFNULL(Data_Gen_StartZ, 0) AS StartZ, ");
            sb.AppendFormat("IFNULL(Data_Gen_Xp, 0) AS XP, IFNULL(Data_Gen_Cash, 0) AS Cash, Data_Gen_Music AS RadioStationHash ");
            sb.AppendFormat("FROM (");
            sb.AppendFormat("SELECT *, ROW_NUMBER() OVER(PARTITION BY {0} ORDER BY UpdatedDate DESC) AS HistoryIndex ",
                (parameters.LatestVersionsOnly == true ? "RootContentId" : "ContentId"));
            sb.AppendFormat("FROM ugc.gta5mission_metadata ");
            sb.AppendFormat(") AS AllMissions ");
            sb.AppendFormat("LEFT JOIN (");
            sb.AppendFormat("SELECT UserName, UserId, PlatformId, ROW_NUMBER() OVER(PARTITION BY AccountId, PlatformId ORDER BY ISNULL(LastUpdatedDate, CreatedDate) DESC) AS HistoryIndex ");
            sb.AppendFormat("FROM SocialClub.PlayerAccount ");
            sb.AppendFormat(") AS AllPlayerAccounts ");
            sb.AppendFormat("ON AllMissions.HistoryIndex=1 AND AllPlayerAccounts.HistoryIndex=1 AND AllMissions.UserId=AllPlayerAccounts.UserId AND AllMissions.PlatformId=AllPlayerAccounts.PlatformId ");

            // Add the list of restrictions.
            IList<String> restrictions = GetRestrictions(parameters).ToList();
            restrictions.Add("AllMissions.Data_Gen_Type IS NOT NULL");
            restrictions.Add("AllMissions.HistoryIndex = 1");
            restrictions.Add("IFNULL(AllPlayerAccounts.HistoryIndex, 1) = 1");
            if (restrictions.Any())
            {
                sb.AppendFormat("WHERE {0} ", String.Join(" AND ", restrictions));
            }
            sb.AppendFormat(";");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the list of restrictions to apply to the query.
        /// </summary>
        private IEnumerable<String> GetRestrictions(MissionContentParams parameters)
        {
            if (parameters.MissionCategories.Any())
            {
                yield return String.Format("AllMissions.CategoryId IN ({0})", String.Join(", ", parameters.MissionCategories.Select(item => (int)item)));
            }
            if (parameters.MissionCreatedStart.HasValue && parameters.MissionCreatedEnd.HasValue)
            {
                yield return String.Format("AllMissions.CreatedDate BETWEEN '{0}' AND '{1}'",
                    parameters.MissionCreatedStart.Value.ToString("u"), parameters.MissionCreatedEnd.Value.ToString("u"));
            }
            else if (parameters.MissionCreatedStart.HasValue)
            {
                yield return String.Format("AllMissions.CreatedDate >= '{0}'", parameters.MissionCreatedStart.Value.ToString("u"));
            }
            else if (parameters.MissionCreatedEnd.HasValue)
            {
                yield return String.Format("AllMissions.CreatedDate <= '{0}'", parameters.MissionCreatedEnd.Value.ToString("u"));
            }
            if (parameters.MissionPublishedOnly.HasValue)
            {
                yield return String.Format("AllMissions.PublishedDate {0}", parameters.MissionPublishedOnly.Value ? "IS NOT NULL" : "IS NULL");
            }
            if (parameters.MissionCreatorUserIdPlatformPairs.Any())
            {
                yield return String.Format("AllMissions.UserId IN ('{0}')",
                    String.Join("', '", parameters.MissionCreatorUserIdPlatformPairs.Select(item => item.Item1)));
            }
            if (parameters.LatestVersionsOnly.HasValue)
            {
                yield return String.Format("AllMissions.LatestVersionContentId {0} ContentId", (parameters.LatestVersionsOnly.Value ? "=" : "!="));
            }
            if (parameters.IncludeDeleted.HasValue)
            {
                yield return String.Format("AllMissions.DeletedDate {0}", (parameters.IncludeDeleted.Value ? "IS NOT NULL" : "IS NULL"));
            }
        }
        #endregion // Private Methods
    } // MissionContentDetailsReport
}
