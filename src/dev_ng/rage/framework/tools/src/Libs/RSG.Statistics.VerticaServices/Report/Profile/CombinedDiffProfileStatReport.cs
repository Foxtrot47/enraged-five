﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Linq;
using System.Text;
using RSG.ROS;
using RSG.Statistics.Common.Dto.ProfileStats;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.VerticaServices.Util;
using Vertica.Data.VerticaClient;
using RSG.Base.Security.Cryptography;
using RSG.Statistics.VerticaServices.DataAccess;

namespace RSG.Statistics.VerticaServices.Report.Profile
{
    /// <summary>
    /// 
    /// </summary>
    [Export(typeof(IVerticaReport))]
    internal class CombinedDiffProfileStatReport : ProfileStatReportBase<CombinedProfileStatParams>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public CombinedDiffProfileStatReport()
            : base(ReportNames.CombinedDiffProfileStatReport)
        {
        }
        #endregion // Constructor(s)

        #region VerticaReportBase Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        protected override List<ProfileStatDto> Generate(ISession session, CombinedProfileStatParams parameters)
        {
            // Determine which sql query we should create based on the input parameters.
            String sql;
            if (parameters.BucketSize.HasValue)
            {
                sql = CreateBucketBasedQuery(parameters);
            }
            else
            {
                sql = CreateSoloQuery(parameters);
            }

            // Run the query against vertica.;
            VerticaDataReader dr = session.ExecuteQuery(sql);

            // Convert the results to a nicer temporary format.
            List<ProfileStatDto> stats = new List<ProfileStatDto>();
            while (dr.Read())
            {
                ProfileStatDto stat = new ProfileStatDto(dr.GetDouble(0), (parameters.BucketSize.HasValue ? parameters.BucketSize.Value : 0.0));

                if (dr[1] != DBNull.Value)
                {
                    stat.Total = dr.GetDecimal(1).ToString();
                }
                if (dr[2] != DBNull.Value)
                {
                    stat.YValue = dr.GetUInt64(2).ToString();
                }

                stats.Add(stat);
            }

            return stats.OrderBy(item => item.BucketMin).ToList();
        }
        #endregion // IStatsReport Implementation

        #region Private Methods
        /// <summary>
        /// Creates the query to bucket the data into the requested bucket sizes.
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private String CreateBucketBasedQuery(CombinedProfileStatParams parameters)
        {
            // We want to replicate a query along the following lines:
            //
            // SELECT
            //     Bucket,
            //     SUM(Value) AS Total,
            //     COUNT(DISTINCT PlayerAccountId) AS PlayerAccountId
            // FROM
            // (
            //     SELECT
            //         SUM(CASE
            //             WHEN CurrentIntegerValue IS NOT NULL AND CurrentIntegerValue >= PreviousIntegerValue THEN CurrentIntegerValue - PreviousIntegerValue
            //             WHEN CurrentIntegerValue IS NOT NULL THEN CurrentIntegerValue
            //             WHEN CurrentFloatValue >= PreviousFloatValue THEN CurrentFloatValue - PreviousFloatValue
            //             ELSE CurrentFloatValue
            //             END) AS Value,
            //         FLOOR(SUM(CASE
            //             WHEN CurrentIntegerValue IS NOT NULL AND CurrentIntegerValue >= PreviousIntegerValue THEN CurrentIntegerValue - PreviousIntegerValue
            //             WHEN CurrentIntegerValue IS NOT NULL THEN CurrentIntegerValue
            //             WHEN CurrentFloatValue >= PreviousFloatValue THEN CurrentFloatValue - PreviousFloatValue
            //             ELSE CurrentFloatValue
            //             END) / 3600000) * 3600000 AS Bucket,
            //         Platform,
            //         PlayerAccountId
            //     FROM
            //     (
            //         SELECT 'PS3' AS Platform, PlayerAccountId, LastUpdated,
            //             IntegerValue AS CurrentIntegerValue,
            //             LAG(IntegerValue, 1, 0) OVER(PARTITION BY PlayerAccountId, StatId ORDER BY LastUpdated) AS PreviousIntegerValue,
            //             FloatValue AS CurrentFloatValue,
            //             LAG(FloatValue, 1, 0) OVER(PARTITION BY PlayerAccountId, StatId ORDER BY LastUpdated) AS PreviousFloatValue
            //         FROM prod_gta5_11_xbox360.ProfileStats2
            //         WHERE LastUpdated < '2013-10-09 00:00:00Z' AND StatName IN ('MP0_TOTAL_PLAYING_TIME', 'MP1_TOTAL_PLAYING_TIME', 'MP2_TOTAL_PLAYING_TIME', 'MP3_TOTAL_PLAYING_TIME', 'MP4_TOTAL_PLAYING_TIME')
            //     ) AS AllStats
            //     WHERE LastUpdated >= '2013-10-01 00:00:00Z'
            //     GROUP BY Platform, PlayerAccountId
            // ) PerGamerStats
            // GROUP BY Bucket;

            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT ");
            sb.Append("Bucket, ");
            sb.Append("SUM(Value) AS Total, ");
            sb.Append("COUNT(DISTINCT PlayerAccountId) AS PlayerCount ");
            sb.Append("FROM (");
            {
                // Get the summed values on a per gamer basis.
                sb.Append("SELECT ");
                sb.Append("SUM(CurrentValue - PreviousValue) AS Value, ");
                sb.AppendFormat("FLOOR(SUM(CurrentValue - PreviousValue) / {0}) * {0} AS Bucket, ", parameters.BucketSize);
                sb.Append("PlayerAccountId, ");
                sb.Append("PlatformId ");
                sb.Append("FROM (");

                // Select the current and previous values for each player/stat combination.
                sb.Append("SELECT PlatformId, PlayerAccountId, LastUpdated, ");
                sb.Append("COALESCE(IntegerValue, FloatValue, 0) AS CurrentValue, ");
                sb.Append("COALESCE(LAG(IntegerValue, 1) OVER(PARTITION BY PlayerAccountId, StatId ORDER BY LastUpdated), ");
                sb.Append("LAG(FloatValue, 1) OVER(PARTITION BY PlayerAccountId, StatId ORDER BY LastUpdated), 0) AS PreviousValue ");
                sb.AppendFormat("FROM {0}.ProfileStats2 p ", Conversion.Schema());
                AddRestrictions(sb, parameters, "p");

                sb.Append(") AS AllStats ");
                sb.AppendFormat("WHERE LastUpdated >= '{0}'  AND CurrentValue >= PreviousValue AND CurrentValue - PreviousValue < {1} ",
                    parameters.StartDate.Value.ToString("u"), parameters.MaxValue);
                sb.Append("GROUP BY PlatformId, PlayerAccountId ");
            }
            sb.Append(") AS PerGamerStats ");
            sb.Append("GROUP BY Bucket;");
            return sb.ToString();
        }

        /// <summary>
        /// Creates the query to return the single value for the requested stats.
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private String CreateSoloQuery(CombinedProfileStatParams parameters)
        {
            // We want to replicate a query along the following lines:
            //
            // SELECT
            //     0 AS Bucket,
            //     SUM(SummedValues) AS Total,
            //     COUNT(DISTINCT PlayerAccountId) AS PlayerAccountId
            // FROM
            // (
	        //     SELECT
		    //         SUM(CASE
			//             WHEN CurrentIntegerValue IS NOT NULL AND CurrentIntegerValue >= PreviousIntegerValue THEN CurrentIntegerValue - PreviousIntegerValue
			//             WHEN CurrentIntegerValue IS NOT NULL THEN CurrentIntegerValue
			//             WHEN CurrentFloatValue >= PreviousFloatValue THEN CurrentFloatValue - PreviousFloatValue
			//             ELSE CurrentFloatValue
			//             END) AS SummedValues,
		    //         Platform,
		    //         PlayerAccountId
	        //     FROM
	        //     (
		    //         SELECT 'PS3' AS Platform, PlayerAccountId, LastUpdated,
			//             IntegerValue AS CurrentIntegerValue,
			//             LAG(IntegerValue, 1, 0) OVER(PARTITION BY PlayerAccountId, StatId ORDER BY LastUpdated) AS PreviousIntegerValue,
			//             FloatValue AS CurrentFloatValue,
			//             LAG(FloatValue, 1, 0) OVER(PARTITION BY PlayerAccountId, StatId ORDER BY LastUpdated) AS PreviousFloatValue
		    //         FROM prod_gta5_11_xbox360.ProfileStats2
		    //         WHERE LastUpdated < '2013-10-09 00:00:00Z' AND StatName IN ('MP0_TOTAL_PLAYING_TIME', 'MP1_TOTAL_PLAYING_TIME', 'MP2_TOTAL_PLAYING_TIME', 'MP3_TOTAL_PLAYING_TIME', 'MP4_TOTAL_PLAYING_TIME')
	        //     ) AS AllStats
	        //     WHERE LastUpdated >= '2013-10-01 00:00:00Z'
	        //     GROUP BY Platform, PlayerAccountId
            // ) SummedStats;

            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT ");
            sb.Append("0 AS Bucket, ");
            sb.Append("SUM(SummedValues) AS Total, ");
            sb.Append("COUNT(DISTINCT PlayerAccountId) AS PlayerCount ");
            sb.Append("FROM (");
            {
                // Get the summed values on a per gamer basis.
                sb.Append("SELECT ");
                sb.Append("SUM(CurrentValue - PreviousValue) AS SummedValues, ");
                sb.Append("PlayerAccountId, ");
                sb.Append("PlatformId ");
                sb.Append("FROM (");

                // Select the current and previous values for each player/stat combination.
                sb.Append("SELECT PlatformId, PlayerAccountId, LastUpdated, ");
                sb.Append("COALESCE(IntegerValue, FloatValue, 0) AS CurrentValue, ");
                sb.Append("COALESCE(LAG(IntegerValue, 1) OVER(PARTITION BY PlayerAccountId, StatId ORDER BY LastUpdated), ");
                sb.Append("LAG(FloatValue, 1) OVER(PARTITION BY PlayerAccountId, StatId ORDER BY LastUpdated), 0) AS PreviousValue ");
                sb.AppendFormat("FROM {0}.ProfileStats2 p ", Conversion.Schema());
                AddRestrictions(sb, parameters, "p");

                sb.Append(") AS AllStats ");
                sb.AppendFormat("WHERE LastUpdated >= '{0}'  AND CurrentValue >= PreviousValue AND CurrentValue - PreviousValue < {1} ",
                    parameters.StartDate.Value.ToString("u"), parameters.MaxValue);
                sb.Append("GROUP BY PlatformId, PlayerAccountId ");
            }
            sb.Append(") AS PerGamerStats;");
            return sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        private void AddRestrictions(StringBuilder sb, CombinedProfileStatParams parameters, String tableAlias)
        {
            if (parameters.AgeMin != null || parameters.AgeMax != null || parameters.CountryCodes.Any() ||
                parameters.SocialClubUsernames.Any() || parameters.GamerHandlePlatformPairs.Any())
            {
                AddPlayerAccountJoin(sb, String.Format("{0}.PlatformId", tableAlias), String.Format("{0}.PlayerAccountId", tableAlias));

                if (parameters.AgeMin != null || parameters.AgeMax != null || parameters.CountryCodes.Any() ||
                    parameters.SocialClubUsernames.Any())
                {
                    AddRockstarAccountJoin(sb);
                }
            }

            // Check if there are any restrictions to add.
            IEnumerable<String> restrictions = GetRestrictions(parameters, tableAlias);
            if (restrictions.Any())
            {
                sb.AppendFormat("WHERE {0} ", String.Join(" AND ", restrictions));
            }
        }

        /// <summary>
        /// Retrieves a list of restrictions that should be added to the sql query string.
        /// </summary>
        private IEnumerable<String> GetRestrictions(CombinedProfileStatParams parameters, String tableAlias)
        {
            IEnumerable<int> hashesOfStatsNames = parameters.StatNames.Select(item => (int)OneAtATime.ComputeHash(item.Item1));
            yield return String.Format("{0}.StatId IN ({1})", tableAlias, String.Join(", ", hashesOfStatsNames));
            yield return (String.Format("{0}.LastUpdated < '{1}'", tableAlias, parameters.EndDate.Value.ToString("u")));

            if (parameters.Platforms.Any())
            {
                yield return (String.Format("{0}.PlatformId IN ({1})", tableAlias, String.Join(",", parameters.Platforms.Select(item => (int)item))));
            }

            if (parameters.CountryCodes.Any())
            {
                yield return (String.Format("ra.CountryCode IN ('{0}')", String.Join("', '", parameters.CountryCodes)));
            }

            if (parameters.SocialClubUsernames.Any())
            {
                yield return (String.Format("ra.Nickname IN ('{0}')", String.Join("', '", parameters.SocialClubUsernames)));
            }

            IEnumerable<String> gamerHandles = parameters.GamerHandlePlatformPairs.Select(item => item.Item1);
            if (gamerHandles.Any())
            {
                yield return (String.Format("pa.UserName IN ('{0}')", String.Join("', '", gamerHandles)));
            }
        }
        #endregion // Private Methods
    } // CombinedDiffProfileStatReport
}
