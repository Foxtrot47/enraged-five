﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.VerticaServices.DataAccess
{
    /// <summary>
    /// Query interface to abstract the construction of an SQL query string.
    /// </summary>
    internal interface IQuery
    {
        /// <summary>
        /// Retrieves a string that represents the query to execute against the database.
        /// </summary>
        String GetQueryString();
    } // IQuery
}
