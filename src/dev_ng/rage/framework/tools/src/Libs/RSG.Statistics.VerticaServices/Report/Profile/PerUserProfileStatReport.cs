﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Linq;
using System.Text;
using RSG.Base.Extensions;
using RSG.ROS;
using RSG.Statistics.Common.Dto.ProfileStats;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using Vertica.Data.VerticaClient;
using RSG.Statistics.VerticaServices.Util;
using RSG.Base.Security.Cryptography;
using RSG.Statistics.VerticaServices.DataAccess;

namespace RSG.Statistics.VerticaServices.Report.Profile
{
    /// <summary>
    /// 
    /// </summary>
    [Export(typeof(IVerticaReport))]
    internal class PerUserProfileStatReport : VerticaReportBase<List<PerUserProfileStatValues>, PerUserProfileStatParams>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public PerUserProfileStatReport()
            : base(ReportNames.LatestProfileStatReport)
        {
        }
        #endregion // Constructor(s)

        #region Protected Classes
        /// <summary>
        /// 
        /// </summary>
        protected class PerGamerStatData
        {
            public String GamerHandle { get; set; }
            public String StatName { get; set; }
            public double Value { get; set; }
        } // PerGamerStatData
        #endregion // Protected Classes

        #region VerticaReportBase Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        protected override List<PerUserProfileStatValues> Generate(ISession session, PerUserProfileStatParams parameters)
        {
            List<PerUserProfileStatValues> results = CreateResultsMatrix(parameters);

            foreach (ROSPlatform platform in parameters.GamerHandlePlatformPairs.Select(item => item.Item2).Distinct())
            {
                RetrievePerUserData(session, platform, parameters, results);
            }

            return results;
        }
        #endregion // IStatsReport Implementation

        #region Private Methods
        /// <summary>
        /// Creates result objects for all combinations of gamer handle/stats.
        /// </summary>
        /// <param name="parameters"></param>
        private List<PerUserProfileStatValues> CreateResultsMatrix(PerUserProfileStatParams parameters)
        {
            List<PerUserProfileStatValues> results = new List<PerUserProfileStatValues>();

            foreach (Tuple<String, ROSPlatform> gamerTuple in parameters.GamerHandlePlatformPairs.OrderBy(item => item.Item1).ThenBy(item => item.Item2))
            {
                PerUserProfileStatValues result = new PerUserProfileStatValues(gamerTuple.Item1, gamerTuple.Item2);

                foreach (Tuple<String, String> statTuple in parameters.StatNames.OrderBy(item => item.Item1))
                {
                    result.Data.Add(new ProfileStatValue(statTuple.Item1, statTuple.Item2));
                }

                results.Add(result);
            }

            return results;
        }

        /// <summary>
        /// 
        /// </summary>
        private void RetrievePerUserData(ISession session, ROSPlatform platform, PerUserProfileStatParams parameters, List<PerUserProfileStatValues> results)
        {
            // Create a lookup for results in this platform.
            IDictionary<String, PerUserProfileStatValues> perUserStatsLookup =
                results.Where(item => item.Platform == platform)
                       .ToDictionary(item => item.GamerHandle, item => item);

            // Run the query.
            VerticaDataReader dr = session.ExecuteQuery(CreateSqlQueryString(platform, parameters));

            // Overwrite the values in the results for which we retrieved data.
            while (dr.Read())
            {
                String username = dr.GetString(0);
                String statName = dr.GetString(1);

                PerUserProfileStatValues perUserStat = perUserStatsLookup[username];
                ProfileStatValue statValue = perUserStat.Data.First(item => item.StatName == statName);

                if (dr[2] != DBNull.Value)
                {
                    statValue.Value = dr.GetDouble(2).ToString();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private String CreateSqlQueryString(ROSPlatform platform, PerUserProfileStatParams parameters)
        {
            String tableName = Conversion.Schema();

            // Creates a query that looks like this:
            //
            // SELECT UserName, StatName, SUM(COALESCE(IntegerValue, 0)) + SUM(COALESCE(FloatValue, 0)) AS Value
            // FROM (SELECT *,
            //     ROW_NUMBER() 
            //         OVER(PARTITION BY pa.UserName, StatId 
            //             ORDER BY LastUpdated DESC) AS HistoryIndex 
            //     FROM dev_gta5_11_ps3.ProfileStats2
            //     INNER JOIN SocialClub_np_dev.PlayerAccount pa on pa.UserName=p.GamerHandle
            //     WHERE LastUpdated <= '2012-12-15'
            //          AND pa.UserName IN ('Josh_BR')
            //          AND StatName IN ('SP0_TOTAL_PLAYING_TIME', 'SP1_TOTAL_PLAYING_TIME', 'SP2_TOTAL_PLAYING_TIME')
            //     ) AS AllStats
            // WHERE HistoryIndex = 1
            // GROUP BY GamerHandle, StatName;
            //
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT UserName, StatName, SUM(COALESCE(IntegerValue, 0)) + SUM(COALESCE(FloatValue, 0)) AS Value " +
                "FROM (SELECT p.*, pa.UserName, " +
                "    ROW_NUMBER() " +
                "        OVER(PARTITION BY pa.UserName, StatId " +
                "            ORDER BY LastUpdated DESC) AS HistoryIndex " +
                "    FROM ");
            sb.Append(String.Format("{0}.ProfileStats2 p ", tableName));

            if (parameters.GamerHandlePlatformPairs.Any())
            {
                AddPlayerAccountJoin(sb, "p.PlatformId", "p.PlayerAccountId");
            }

            IList<String> restrictions = new List<String>();
            restrictions.Add(String.Format("p.PlatformId = {0}", (int)platform));
            
            // Did the user supply start/end dates?
            if (parameters.EndDate.HasValue)
            {
                restrictions.Add("LastUpdated < '" + parameters.EndDate.Value.ToString("u") + "'");
            }

            IEnumerable<String> gamerHandles = parameters.GamerHandlePlatformPairs.Where(item => item.Item2 == platform).Select(item => item.Item1);
            if (gamerHandles.Any())
            {
                restrictions.Add(String.Format("pa.UserName IN ('{0}')", String.Join("', '", gamerHandles)));
            }

            if (parameters.StatNames.Any())
            {
                IEnumerable<int> hashesOfStatsNames = parameters.StatNames.Select(item => (int)OneAtATime.ComputeHash(item.Item1));
                restrictions.Add("StatId IN (" + String.Join(", ", hashesOfStatsNames) + ")");
            }

            // Check if there are any restrictions to add.
            if (restrictions.Any())
            {
                sb.Append(String.Format(" WHERE "));
                sb.Append(String.Join(" AND ", restrictions));
            }

            sb.Append(") AS AllStats " +
                "WHERE AllStats.HistoryIndex = 1 ");
            sb.Append("GROUP BY UserName, StatName;");

            return sb.ToString();
        }
        #endregion // Private Methods
    } // PerUserProfileStatReport
}
