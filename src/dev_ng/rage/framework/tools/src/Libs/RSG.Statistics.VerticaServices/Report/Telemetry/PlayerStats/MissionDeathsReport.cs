﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Linq;
using System.Text;
using RSG.ROS;
using RSG.Statistics.Common.Model.Telemetry;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.VerticaServices.DataAccess;
using RSG.Statistics.VerticaServices.Util;
using Vertica.Data.VerticaClient;

namespace RSG.Statistics.VerticaServices.Report.Telemetry.PlayerStats
{
    /// <summary>
    /// Report that provides death information on a per mission basis.
    /// </summary>
    [Export(typeof(IVerticaReport))]
    internal class MissionDeathsReport : PlayerStatsReportBase<List<PerMissionDeathStat>, TelemetryParams>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public MissionDeathsReport()
            : base(ReportNames.MissionDeathsReport)
        {
        }
        #endregion // Constructor(s)

        #region VerticaReportBase Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        protected override List<PerMissionDeathStat> Generate(ISession session, TelemetryParams parameters)
        {
            List<PerMissionDeathStat> stats = new List<PerMissionDeathStat>();

            VerticaDataReader dr = session.ExecuteQuery(CreateSqlQueryString(parameters));
            while (dr.Read())
            {
                PerMissionDeathStat stat = new PerMissionDeathStat();
                stat.MissionName = dr.GetString(0);
                stat.TotalDeaths = dr.GetUInt64(1);
                stat.NumberOfAttempts = dr.GetUInt64(2);
                stat.UniqueGamers = dr.GetUInt64(3);
                stats.Add(stat);
            }
            return stats;
        }
        #endregion // IStatsReport Implementation

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private String CreateSqlQueryString(TelemetryParams parameters)
        {
            // We want to replicate a query along the following lines:
            //
            // SELECT
	        //     MissionDetails.MissionName AS MissionName,
	        //     IFNULL(SUM(DeathCountPerMission.NumDeaths), 0) AS TotalDeaths,
	        //     COUNT(MissionDetails.mid) AS NumberOfAttempts,
            //     COUNT(DISTINCT(MissionDetails.AccountId)) AS UniqueGamers
            // FROM
            // (
	        //     SELECT
		    //         h.mid,
		    //         h.AccountId,
		    //         mo.name AS MissionName
	        //     FROM dev_gta5_11_ps3.Telemetry_Gta5_MISSION_OVER mo
	        //     INNER JOIN dev_gta5_11_ps3.Telemetry_Gta5_header h ON h.SubmissionId=mo.SubmissionId
	        //     WHERE h.ver=5632 AND h.mid != 0 AND h.mp=0
            // ) AS MissionDetails
            // LEFT JOIN
            // (
	        //     SELECT
		    //         d.Header_mid,
		    //         d.Header_AccountId,
		    //         COUNT(*) AS NumDeaths
	        //     FROM dev_gta5_11_ps3.Telemetry_Gta5_DEATH d
	        //     INNER JOIN dev_gta5_11_ps3.Telemetry_Gta5_header h ON h.SubmissionId=d.SubmissionId
	        //     WHERE h.ver=5632 AND h.mid != 0 AND h.mp=0
	        //     GROUP BY h.mid, h.AccountId
            // ) AS DeathCountPerMission
            // ON DeathCountPerMission.Header_mid = MissionDetails.mid AND DeathCountPerMission.Header_AccountId = MissionDetails.AccountId
            // GROUP BY MissionDetails.MissionName;
            
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("SELECT ");
            sb.AppendFormat("MissionName, ");
            sb.AppendFormat("SUM(TotalDeaths), ");
            sb.AppendFormat("SUM(NumberOfAttempts), ");
            sb.AppendFormat("SUM(UniqueGamers) ");
            sb.AppendFormat("FROM (");
            {
                String schema = Conversion.Schema();
                sb.Append("SELECT ");
                sb.Append("MissionDetails.MissionName AS MissionName, ");
                sb.Append("IFNULL(SUM(DeathCountPerMission.NumDeaths), 0) AS TotalDeaths, ");
                sb.Append("COUNT(MissionDetails.Header_mid) AS NumberOfAttempts, ");
                sb.Append("COUNT(DISTINCT(MissionDetails.Header_AccountId)) AS UniqueGamers ");
                sb.Append("FROM ( ");
                sb.Append("SELECT mo.Header_mid, mo.Header_AccountId, mo.name AS MissionName ");
                sb.AppendFormat("FROM {0}.Telemetry_Gta5_MISSION_OVER mo ", schema);
                AddEmbeddedRestrictions(sb, parameters, "mo", new String[] { "mo.Header_mid != 0" });
                sb.Append(") AS MissionDetails ");
                sb.Append("LEFT JOIN ( ");
                sb.Append("SELECT d.Header_mid, d.Header_AccountId, COUNT(*) AS NumDeaths ");
                sb.AppendFormat("FROM {0}.Telemetry_Gta5_DEATH d ", schema);
                AddEmbeddedRestrictions(sb, parameters, "d", new String[] { "d.Header_mid != 0" });
                sb.Append("GROUP BY d.Header_mid, d.Header_AccountId ");
                sb.Append(") AS DeathCountPerMission ");
                sb.Append("ON DeathCountPerMission.Header_mid = MissionDetails.Header_mid AND DeathCountPerMission.Header_AccountId = MissionDetails.Header_AccountId ");
                sb.Append("GROUP BY MissionDetails.MissionName ");
            }
            sb.Append(") AS AllStats ");
            sb.Append("GROUP BY MissionName;");
            return sb.ToString();
        }
        #endregion // Private Methods
    } // MissionDeathsReport
}
