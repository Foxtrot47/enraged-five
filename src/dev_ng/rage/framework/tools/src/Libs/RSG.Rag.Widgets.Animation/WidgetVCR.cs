﻿using RSG.Rag.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.Widgets.Animation
{
    /// <summary>
    /// 
    /// </summary>
    [WidgetGuidAttribute('a', 'v', 'c', 'r')]
    public class WidgetVCR : Widget
    {
        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        private VCRButtonStyle _buttonStyle;

        /// <summary>
        /// 
        /// </summary>
        private VCRButtons _enabledButtons;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public WidgetVCR(uint id, IWidgetResolver resolver, IPacketDispatcher dispatcher)
            : base(id, resolver, dispatcher)
        {
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public VCRButtonStyle ButtonStyle
        {
            get { return _buttonStyle; }
        }

        /// <summary>
        /// 
        /// </summary>
        public VCRButtons EnabledButtons
        {
            get { return _enabledButtons; }
            protected set { SetProperty(ref _enabledButtons, value); }
        }
        #endregion // Properties

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        public async Task GotoStartOrRewind()
        {
            RemotePacketBuilder builder = new RemotePacketBuilder((ushort)BankCommand.User0, this.Guid.ToInteger(), Id);
            await Dispatcher.SendPacketAsync(builder.ToRemotePacket());
        }

        /// <summary>
        /// 
        /// </summary>
        public async Task GotoPreviousOrStepBackward()
        {
            RemotePacketBuilder builder = new RemotePacketBuilder((ushort)BankCommand.User1, this.Guid.ToInteger(), Id);
            await Dispatcher.SendPacketAsync(builder.ToRemotePacket());
        }

        /// <summary>
        /// 
        /// </summary>
        public async Task PlayBackwards()
        {
            RemotePacketBuilder builder = new RemotePacketBuilder((ushort)BankCommand.User2, this.Guid.ToInteger(), Id);
            await Dispatcher.SendPacketAsync(builder.ToRemotePacket());
        }

        /// <summary>
        /// 
        /// </summary>
        public async Task Pause()
        {
            RemotePacketBuilder builder = new RemotePacketBuilder((ushort)BankCommand.User3, this.Guid.ToInteger(), Id);
            await Dispatcher.SendPacketAsync(builder.ToRemotePacket());
        }

        /// <summary>
        /// 
        /// </summary>
        public async Task PlayForwards()
        {
            RemotePacketBuilder builder = new RemotePacketBuilder((ushort)BankCommand.User4, this.Guid.ToInteger(), Id);
            await Dispatcher.SendPacketAsync(builder.ToRemotePacket());
        }

        /// <summary>
        /// 
        /// </summary>
        public async Task GotoNextOrStepForward()
        {
            RemotePacketBuilder builder = new RemotePacketBuilder((ushort)BankCommand.User5, this.Guid.ToInteger(), Id);
            await Dispatcher.SendPacketAsync(builder.ToRemotePacket());
        }

        /// <summary>
        /// 
        /// </summary>
        public async Task GotoEndOrFastForward()
        {
            RemotePacketBuilder builder = new RemotePacketBuilder((ushort)BankCommand.User6, this.Guid.ToInteger(), Id);
            await Dispatcher.SendPacketAsync(builder.ToRemotePacket());
        }
        #endregion // Controller Methods

        #region Message Processing
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void ProcessMessageCore(RemotePacketEventArgs e)
        {
            switch ((BankCommand)e.Packet.Command)
            {
                case BankCommand.Changed:
                    ProcessChangedMessage(e);
                    break;

                // These are all messages from RAG to the game.
                case BankCommand.User0:
                case BankCommand.User1:
                case BankCommand.User2:
                case BankCommand.User3:
                case BankCommand.User4:
                case BankCommand.User5:
                case BankCommand.User6:
                    e.Packet.ReadToEnd();
                    break;

                default:
                    base.ProcessMessageCore(e);
                    break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        public override bool ProcessCreateMessage(RemotePacketEventArgs e)
        {
            if (!base.ProcessCreateMessage(e))
            {
                return false;
            }

            _buttonStyle = (VCRButtonStyle)e.Packet.ReadUInt32();
            _enabledButtons = (VCRButtons)e.Packet.ReadUInt32();
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void ProcessChangedMessage(RemotePacketEventArgs e)
        {
            EnabledButtons = (VCRButtons)e.Packet.ReadUInt32();
        }
        #endregion // Message Processing
    } // WidgetVCR
}
