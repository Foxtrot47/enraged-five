﻿using RSG.Rag.ViewModel.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using RSG.Editor;

namespace RSG.Rag.Widgets.Animation
{
    /// <summary>
    /// 
    /// </summary>
    public class WidgetVCRViewModel : WidgetViewModel<WidgetVCR>
    {
        #region Fields
        /// <summary>
        /// 
        /// </summary>
        private IAsyncCommand _gotoStartCommand;

        /// <summary>
        /// 
        /// </summary>
        private IAsyncCommand _stepBackwardCommand;

        /// <summary>
        /// 
        /// </summary>
        private IAsyncCommand _playBackwardCommand;

        /// <summary>
        /// 
        /// </summary>
        private IAsyncCommand _pauseCommand;

        /// <summary>
        /// 
        /// </summary>
        private IAsyncCommand _playForwardCommand;

        /// <summary>
        /// 
        /// </summary>
        private IAsyncCommand _stepForwardCommand;

        /// <summary>
        /// 
        /// </summary>
        private IAsyncCommand _gotoEndCommand;
        #endregion // Fields

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="widget"></param>
        public WidgetVCRViewModel(WidgetVCR widget)
            : base(widget)
        {
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public bool ShowVCRButtons
        {
            get { return Widget.ButtonStyle == VCRButtonStyle.VCR; }
        }
        
        /// <summary>
        /// 
        /// </summary>
        public bool ShowAnimationButtons
        {
            get { return Widget.ButtonStyle == VCRButtonStyle.Animation; }
        }
        #endregion // Properties

        #region Commands
        /// <summary>
        /// 
        /// </summary>
        public IAsyncCommand GotoStartCommand
        {
            get
            {
                if (_gotoStartCommand == null)
                {
                    _gotoStartCommand = new RelayCommandAsync(param => GotoStart(param), param => CanGotoStart(param));
                }
                return _gotoStartCommand;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public IAsyncCommand StepBackwardCommand
        {
            get
            {
                if (_stepBackwardCommand == null)
                {
                    _stepBackwardCommand = new RelayCommandAsync(param => StepBackward(param), param => CanStepBackward(param));
                }
                return _stepBackwardCommand;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public IAsyncCommand PlayBackwardCommand
        {
            get
            {
                if (_playBackwardCommand == null)
                {
                    _playBackwardCommand = new RelayCommandAsync(param => PlayBackward(param), param => CanPlayBackward(param));
                }
                return _playBackwardCommand;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public IAsyncCommand PauseCommand
        {
            get
            {
                if (_pauseCommand == null)
                {
                    _pauseCommand = new RelayCommandAsync(param => Pause(param), param => CanPause(param));
                }
                return _pauseCommand;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public IAsyncCommand PlayForwardCommand
        {
            get
            {
                if (_playForwardCommand == null)
                {
                    _playForwardCommand = new RelayCommandAsync(param => PlayForward(param), param => CanPlayForward(param));
                }
                return _playForwardCommand;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public IAsyncCommand StepForwardCommand
        {
            get
            {
                if (_stepForwardCommand == null)
                {
                    _stepForwardCommand = new RelayCommandAsync(param => StepForward(param), param => CanStepForward(param));
                }
                return _stepForwardCommand;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public IAsyncCommand GotoEndCommand
        {
            get
            {
                if (_gotoEndCommand == null)
                {
                    _gotoEndCommand = new RelayCommandAsync(param => GotoEnd(param), param => CanGotoEnd(param));
                }
                return _gotoEndCommand;
            }
        }
        #endregion // Commands

        #region Command Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool CanGotoStart(Object parameter)
        {
            return !ReadOnly && Widget.EnabledButtons.HasFlag(VCRButtons.GoToStart);
        }

        /// <summary>
        /// Called to press the button.
        /// </summary>
        public async Task GotoStart(Object parameter)
        {
            await Widget.GotoStartOrRewind();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool CanStepBackward(Object parameter)
        {
            return !ReadOnly && Widget.EnabledButtons.HasFlag(VCRButtons.StepBackward);
        }

        /// <summary>
        /// Called to press the button.
        /// </summary>
        public async Task StepBackward(Object parameter)
        {
            await Widget.GotoPreviousOrStepBackward();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool CanPlayBackward(Object parameter)
        {
            return !ReadOnly && Widget.EnabledButtons.HasFlag(VCRButtons.PlayBackwards);
        }

        /// <summary>
        /// Called to press the button.
        /// </summary>
        public async Task PlayBackward(Object parameter)
        {
            await Widget.PlayBackwards();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool CanPause(Object parameter)
        {
            return !ReadOnly && Widget.EnabledButtons.HasFlag(VCRButtons.Pause);
        }

        /// <summary>
        /// Called to press the button.
        /// </summary>
        public async Task Pause(Object parameter)
        {
            await Widget.Pause();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool CanPlayForward(Object parameter)
        {
            return !ReadOnly && Widget.EnabledButtons.HasFlag(VCRButtons.PlayForwards);
        }

        /// <summary>
        /// Called to press the button.
        /// </summary>
        public async Task PlayForward(Object parameter)
        {
            await Widget.PlayForwards();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool CanStepForward(Object parameter)
        {
            return !ReadOnly && Widget.EnabledButtons.HasFlag(VCRButtons.StepForward);
        }

        /// <summary>
        /// Called to press the button.
        /// </summary>
        public async Task StepForward(Object parameter)
        {
            await Widget.GotoNextOrStepForward();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool CanGotoEnd(Object parameter)
        {
            return !ReadOnly && Widget.EnabledButtons.HasFlag(VCRButtons.GoToEnd);
        }

        /// <summary>
        /// Called to press the button.
        /// </summary>
        public async Task GotoEnd(Object parameter)
        {
            await Widget.GotoEndOrFastForward();
        }
        #endregion // Command Methods

        #region Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyName"></param>
        protected override void OnWidgetPropertyChanged(String propertyName)
        {
            if (propertyName == "EnabledButtons")
            {
                NotifyPropertyChanged("ShowVCRButtons", "ShowAnimationButtons");
            }
            else
            {
                base.OnWidgetPropertyChanged(propertyName);
            }
        }
        #endregion // Event Handlers
    } // WidgetVCRViewModel
}
