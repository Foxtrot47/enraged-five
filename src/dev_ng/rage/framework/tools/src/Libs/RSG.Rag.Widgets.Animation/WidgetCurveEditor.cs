﻿using RSG.Rag.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Editor.Controls.CurveEditor.Model;
using System.Collections.ObjectModel;
using System.Windows.Media;
using RSG.Base.Extensions;

namespace RSG.Rag.Widgets.Animation
{
    /// <summary>
    /// 
    /// </summary>
    [WidgetGuidAttribute('k', 'u', 'r', 'v')]
    public class WidgetCurveEditor : Widget
    {
        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        private float _minX;

        /// <summary>
        /// 
        /// </summary>
        private float _maxX;

        /// <summary>
        /// 
        /// </summary>
        private readonly ObservableCollection<Curve> _curves = new ObservableCollection<Curve>();

        /// <summary>
        /// Lookup that maps identifiers to curves.
        /// </summary>
        private readonly IDictionary<uint, Curve> _idToCurveLookup = new Dictionary<uint, Curve>();

        /// <summary>
        /// Lookup that maps curves to identifiers.
        /// </summary>
        private readonly IDictionary<Curve, uint> _curveToIdLookup = new Dictionary<Curve, uint>();

        /// <summary>
        /// Temporary buffer for reading chunked incoming point data.
        /// </summary>
        private Point[] _incomingPoints;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public WidgetCurveEditor(uint id, IWidgetResolver resolver, IPacketDispatcher dispatcher)
            : base(id, resolver, dispatcher)
        {
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public float MinX
        {
            get { return _minX; }
            set { SetProperty(ref _minX, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public float MaxX
        {
            get { return _maxX; }
            set { SetProperty(ref _maxX, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public IReadOnlyCollection<Curve> Curves
        {
            get { return _curves; }
        }
        #endregion // Properties

        #region Message Processing
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void ProcessMessageCore(RemotePacketEventArgs e)
        {
            switch ((BankCommand)e.Packet.Command)
            {
                case BankCommand.Changed:
                    ProcessChangedMessage(e);
                    break;

                case BankCommand.User0:
                    ProcessAddCurveMessage(e);
                    break;

                case BankCommand.User1:
                    ProcessRemoveCurveMessage(e);
                    break;

                case BankCommand.User2:
                    ProcessCurveKeyframeDataMessage(e);
                    break;

                case BankCommand.User3:
                    ProcessKeyframeChangedMessage(e);
                    break;

                case BankCommand.User4:
                    ProcessKeyframeAddedMessage(e);
                    break;

                case BankCommand.User5:
                    ProcessKeyframeDeletedMessage(e);
                    break;

                default:
                    base.ProcessMessageCore(e);
                    break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        public override bool ProcessCreateMessage(RemotePacketEventArgs e)
        {
            if (!base.ProcessCreateMessage(e))
            {
                return false;
            }

            _minX = e.Packet.ReadFloat();
            _maxX = e.Packet.ReadFloat();
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void ProcessChangedMessage(RemotePacketEventArgs e)
        {
            MinX = e.Packet.ReadFloat();
            MaxX = e.Packet.ReadFloat();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void ProcessAddCurveMessage(RemotePacketEventArgs e)
        {
            // Read the relevant data from the message.
            String name = e.Packet.ReadString();
            uint id = e.Packet.ReadUInt32();
            byte r = (byte)Math.Round(e.Packet.ReadFloat() * 255.0f, MidpointRounding.AwayFromZero);
            byte g = (byte)Math.Round(e.Packet.ReadFloat() * 255.0f, MidpointRounding.AwayFromZero);
            byte b = (byte)Math.Round(e.Packet.ReadFloat() * 255.0f, MidpointRounding.AwayFromZero);
            Color color = Color.FromRgb(r, g, b);

            if (_idToCurveLookup.ContainsKey(id))
            {
                e.AddErrorMessage("Curve with id {0} already exists.", id);
            }
            else
            {
                // Create and add the new curve.
                Curve curve = new Curve(name, color);
                _curves.Add(curve);

                // Add this combination to our lookups.
                _idToCurveLookup.Add(id, curve);
                _curveToIdLookup.Add(curve, id);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void ProcessRemoveCurveMessage(RemotePacketEventArgs e)
        {
            uint id = e.Packet.ReadUInt32();

            Curve curve;
            if (_idToCurveLookup.TryGetValue(id, out curve))
            {
                _curves.Remove(curve);
                _idToCurveLookup.Remove(id);
                _curveToIdLookup.Remove(curve);
            }
            else
            {
                e.AddErrorMessage("Curve with id {0} doesn't exist.", id);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void ProcessCurveKeyframeDataMessage(RemotePacketEventArgs e)
        {
            Curve curve = RetrieveCurveForMessage(e);
            if (curve != null)
            {
                uint totalKeyFrameCount = e.Packet.ReadUInt32();
                uint packetKeyFrameCount = e.Packet.ReadUInt32();
                uint offset = e.Packet.ReadUInt32();
                uint remainingPackets = e.Packet.ReadUInt32();

                if (offset == 0)
                {
                    _incomingPoints = new Point[totalKeyFrameCount];
                }

                for (int i = 0; i < packetKeyFrameCount; ++i)
                {
                    float key = e.Packet.ReadFloat();
                    float value = e.Packet.ReadFloat();

                    _incomingPoints[offset + i] =  new Point(curve, key, value);
                }

                if (remainingPackets == 0)
                {
                    curve.ResetPoints(_incomingPoints);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void ProcessKeyframeChangedMessage(RemotePacketEventArgs e)
        {
            Curve curve = RetrieveCurveForMessage(e);
            if (curve != null)
            {
                uint keyIndex = e.Packet.ReadUInt32();
                float key = e.Packet.ReadFloat();
                float value = e.Packet.ReadFloat();

                curve.Points[(int)keyIndex].PositionX = key;
                curve.Points[(int)keyIndex].PositionY = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void ProcessKeyframeAddedMessage(RemotePacketEventArgs e)
        {
            Curve curve = RetrieveCurveForMessage(e);
            if (curve != null)
            {
                uint keyIndex = e.Packet.ReadUInt32();
                float key = e.Packet.ReadFloat();
                float value = e.Packet.ReadFloat();

                Point point = new Point(curve, key, value);
                curve.InsertPoint((int)keyIndex, point);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void ProcessKeyframeDeletedMessage(RemotePacketEventArgs e)
        {
            Curve curve = RetrieveCurveForMessage(e);
            if (curve != null)
            {
                uint keyIndex = e.Packet.ReadUInt32();
                if (keyIndex > curve.Points.Count)
                {
                    e.AddErrorMessage("Trying to remove a keyframe (idx {0}) from curve '{1}' which doesn't exist.", keyIndex, curve.Name);
                }
                else
                {
                    curve.RemovePointAt((int)keyIndex);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        private Curve RetrieveCurveForMessage(RemotePacketEventArgs e)
        {
            uint id = e.Packet.ReadUInt32();

            Curve curve = null;
            if (!_idToCurveLookup.TryGetValue(id, out curve))
            {
                e.AddErrorMessage("Curve with id {0} doesn't exist.", id);
            }
            return curve;
        }
        #endregion // Message Processing
    } // WidgetCurve
}
