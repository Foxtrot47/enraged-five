﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.Widgets.Animation
{
    /// <summary>
    /// Keep these in sync with base\src\cranimation\crVCR.h
    /// </summary>
    [Flags]
    public enum VCRButtons
    {
        /// <summary>
        /// 
        /// </summary>
        GoToStart = 0x01,

        /// <summary>
        /// 
        /// </summary>
        StepBackward = 0x02,

        /// <summary>
        /// 
        /// </summary>
        PlayBackwards = 0x04,

        /// <summary>
        /// 
        /// </summary>
        Pause = 0x08,

        /// <summary>
        /// 
        /// </summary>
        PlayForwards = 0x10,

        /// <summary>
        /// 
        /// </summary>
        StepForward = 0x20,

        /// <summary>
        /// 
        /// </summary>
        GoToEnd = 0x40,

        /// <summary>
        /// 
        /// </summary>
        All = 0x7F,
    }
}
