﻿using RSG.Rag.ViewModel.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using RSG.Editor.Controls.CurveEditor.ViewModel;
using System.Collections.Specialized;
using System.Windows;
using System.Collections;
using RSG.Editor.Controls.CurveEditor.Model;
using BaseCols = RSG.Base.Collections;

namespace RSG.Rag.Widgets.Animation
{
    /// <summary>
    /// 
    /// </summary>
    public class WidgetCurveEditorViewModel : WidgetViewModel<WidgetCurveEditor>, IWeakEventListener
    {
        #region Fields
        /// <summary>
        /// 
        /// </summary>
        private readonly ObservableCollection<CurveViewModel> _curves = new ObservableCollection<CurveViewModel>();

        /// <summary>
        /// 
        /// </summary>
        private readonly BaseCols.Range<float> _horizontalLimits = new BaseCols.Range<float>(0.0f, 0.0f);
        #endregion // Fields

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="widget"></param>
        public WidgetCurveEditorViewModel(WidgetCurveEditor widget)
            : base(widget)
        {
            _horizontalLimits.Start = Widget.MinX;
            _horizontalLimits.End = Widget.MaxX;

            INotifyCollectionChanged notifyCollection = widget.Curves as INotifyCollectionChanged;
            if (notifyCollection != null)
            {
                CollectionChangedEventManager.AddListener(notifyCollection, this);
            }
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public BaseCols.Range<float> HorizontalLimits
        {
            get { return _horizontalLimits; }
        }

        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<CurveViewModel> Curves
        {
            get { return _curves; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Vector Scale
        {
            get { return _scale; }
            set
            {
                if (SetProperty(ref _scale, value))
                {
                    foreach (CurveViewModel vm in Curves)
                    {
                        vm.Scale = value;
                    }
                }
            }
        }
        private Vector _scale;

        /// <summary>
        /// 
        /// </summary>
        public System.Windows.Point Offset
        {
            get { return _offset; }
            set
            {
                if (SetProperty(ref _offset, value))
                {
                    foreach (CurveViewModel vm in Curves)
                    {
                        vm.Offset = value;
                    }
                }
            }
        }
        private System.Windows.Point _offset;
        #endregion // Properties

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyName"></param>
        protected override void OnWidgetPropertyChanged(string propertyName)
        {
            if (propertyName == "MinX" || propertyName == "MaxX")
            {
                _horizontalLimits.Start = Widget.MinX;
                _horizontalLimits.End = Widget.MaxX;
                OnWidgetPropertyChanged("HorizontalLimits");
            }

            base.OnWidgetPropertyChanged(propertyName);
        }
        #endregion // Overrides

        #region IWeakEventListener Implementation
        /// <summary>
        /// Receives events from the centralized event manager.
        /// </summary>
        /// <param name="managerType">
        /// The type of the System.Windows.WeakEventManager calling this method.
        /// </param>
        /// <param name="sender">
        /// Object that originated the event.
        /// </param>
        /// <param name="e">
        /// Event data.
        /// </param>
        /// <returns>
        /// True if the listener handled the event; otherwise, false.
        /// </returns>
        public bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
        {
            NotifyCollectionChangedEventArgs args = e as NotifyCollectionChangedEventArgs;
            if (args == null || managerType != typeof(CollectionChangedEventManager))
            {
                return false;
            }

            switch (args.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    {
                        for (int i = 0; i < args.NewItems.Count; i++)
                        {
                            Curve item = (Curve)args.NewItems[i];
                            this.InsertSourceItem(args.NewStartingIndex + i, item);
                        }
                    }

                    break;
                case NotifyCollectionChangedAction.Remove:
                    {
                        for (int i = 0; i < args.OldItems.Count; i++)
                        {
                            this.RemoveSourceItem(args.OldStartingIndex);
                        }
                    }

                    break;
                case NotifyCollectionChangedAction.Replace:
                    {
                        for (int i = 0; i < args.NewItems.Count; i++)
                        {
                            Curve item = (Curve)args.NewItems[i];
                            this.ReplaceSourceItem(args.OldStartingIndex + i, item);
                        }
                    }

                    break;
                case NotifyCollectionChangedAction.Move:
                    {
                        for (int i = 0; i < args.NewItems.Count; i++)
                        {
                            int oldIndex = args.OldStartingIndex + i;
                            int newIndex = args.NewStartingIndex + i;
                            this.MoveSourceItem(oldIndex, newIndex);
                        }
                    }

                    break;
                case NotifyCollectionChangedAction.Reset:
                    {
                        this.ResetSourceItems(Widget.Curves);
                    }

                    break;
            }

            return true;
        }

        /// <summary>
        /// Inserts the converted item from the specified source item into this collection at
        /// the specified index.
        /// </summary>
        /// <param name="index">
        /// The zero-based index where the new item should be inserted.
        /// </param>
        /// <param name="item">
        /// The item whose converted value will added to this collection.
        /// </param>
        protected void InsertSourceItem(int index, Curve item)
        {
            Application app = Application.Current;
            if (!app.Dispatcher.CheckAccess())
            {
                app.Dispatcher.Invoke(() => InsertSourceItem(index, item));
                return;
            }

            CurveViewModel vm = new CurveViewModel(item);
            vm.Scale = _scale;
            vm.Offset = _offset;
            _curves.Insert(index, vm);
        }

        /// <summary>
        /// Moves the item at the specified old index to the new index in the collection.
        /// </summary>
        /// <param name="oldIndex">
        /// The zero-based index specifying the location of the item to move.
        /// </param>
        /// <param name="newIndex">
        /// The zero-based index specifying the new location of the item.
        /// </param>
        protected virtual void MoveSourceItem(int oldIndex, int newIndex)
        {
            Application app = Application.Current;
            if (!app.Dispatcher.CheckAccess())
            {
                app.Dispatcher.Invoke(() => MoveSourceItem(oldIndex, newIndex));
                return;
            }

            _curves.Move(oldIndex, newIndex);
        }

        /// <summary>
        /// Removes the item at the specified zero-based index from this collection.
        /// </summary>
        /// <param name="index">
        /// The zero-based index of the item to remove.
        /// </param>
        protected virtual void RemoveSourceItem(int index)
        {
            Application app = Application.Current;
            if (!app.Dispatcher.CheckAccess())
            {
                app.Dispatcher.Invoke(() => RemoveSourceItem(index));
                return;
            }

            CurveViewModel vm = _curves[index];
            _curves.RemoveAt(index);
        }

        /// <summary>
        /// Replaces the item at the specified zero-based index with the specified item.
        /// </summary>
        /// <param name="index">
        /// The zero-based index of the item to replace.
        /// </param>
        /// <param name="item">
        /// The item that will replace the item at the specified index.
        /// </param>
        protected virtual void ReplaceSourceItem(int index, Curve item)
        {
            Application app = Application.Current;
            if (!app.Dispatcher.CheckAccess())
            {
                app.Dispatcher.Invoke(() => ReplaceSourceItem(index, item));
                return;
            }

            CurveViewModel newVm = new CurveViewModel(item);
            newVm.Scale = _scale;
            newVm.Offset = _offset;
            _curves[index] = newVm;
        }

        /// <summary>
        /// Resets the items in this collection to the items specified.
        /// </summary>
        /// <param name="newItems">
        /// The items that will replace the items currently in this collection.
        /// </param>
        protected virtual void ResetSourceItems(IEnumerable newItems)
        {
            Application app = Application.Current;
            if (!app.Dispatcher.CheckAccess())
            {
                app.Dispatcher.Invoke(() => ResetSourceItems(newItems));
                return;
            }

            _curves.Clear();
            if (newItems != null)
            {
                int num = 0;
                IEnumerator enumerator = newItems.GetEnumerator();
                try
                {
                    while (enumerator.MoveNext())
                    {
                        Curve item = (Curve)enumerator.Current;
                        InsertSourceItem(num, item);
                        num++;
                    }
                }
                finally
                {
                    IDisposable disposable = enumerator as IDisposable;
                    if (disposable != null)
                    {
                        disposable.Dispose();
                    }
                }
            }
        }
        #endregion // IWeakEventListener Implementation
    } // WidgetCurveViewModel
}
