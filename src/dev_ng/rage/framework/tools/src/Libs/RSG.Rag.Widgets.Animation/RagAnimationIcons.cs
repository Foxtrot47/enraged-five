﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace RSG.Rag.Widgets.Animation
{
    /// <summary>
    /// 
    /// </summary>
    public static class RagAnimationIcons
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="FastForward"/> property.
        /// </summary>
        private static BitmapSource _fastForward;

        /// <summary>
        /// The private field used for the <see cref="GotoEnd"/> property.
        /// </summary>
        private static BitmapSource _gotoEnd;

        /// <summary>
        /// The private field used for the <see cref="GotoStart"/> property.
        /// </summary>
        private static BitmapSource _gotoStart;

        /// <summary>
        /// The private field used for the <see cref="Pause"/> property.
        /// </summary>
        private static BitmapSource _pause;

        /// <summary>
        /// The private field used for the <see cref="PlayBackwards"/> property.
        /// </summary>
        private static BitmapSource _playBackwards;

        /// <summary>
        /// The private field used for the <see cref="PlayForwards"/> property.
        /// </summary>
        private static BitmapSource _playForward;

        /// <summary>
        /// The private field used for the <see cref="Rewind"/> property.
        /// </summary>
        private static BitmapSource _rewind;

        /// <summary>
        /// The private field used for the <see cref="StepBackwards"/> property.
        /// </summary>
        private static BitmapSource _stepBackwards;

        /// <summary>
        /// The private field used for the <see cref="StepForwards"/> property.
        /// </summary>
        private static BitmapSource _stepForwards;

        /// <summary>
        /// The private field used for the <see cref="Stop"/> property.
        /// </summary>
        private static BitmapSource _stop;

        /// <summary>
        /// A generic object that provides thread safety access to the image sources.
        /// </summary>
        private static object _syncRoot = new object();
        #endregion Fields

        #region Properties
        /// <summary>
        /// Gets the icon that shows a bank icon.
        /// </summary>
        public static BitmapSource FastForward
        {
            get
            {
                if (_fastForward == null)
                {
                    lock (_syncRoot)
                    {
                        if (_fastForward == null)
                        {
                            _fastForward = EnsureLoaded("fastForward16.png");
                        }
                    }
                }

                return _fastForward;
            }
        }

        /// <summary>
        /// Gets the icon that shows a group icon.
        /// </summary>
        public static BitmapSource GotoEnd
        {
            get
            {
                if (_gotoEnd == null)
                {
                    lock (_syncRoot)
                    {
                        if (_gotoEnd == null)
                        {
                            _gotoEnd = EnsureLoaded("gotoEnd16.png");
                        }
                    }
                }

                return _gotoEnd;
            }
        }

        /// <summary>
        /// Gets the icon that shows a bank icon.
        /// </summary>
        public static BitmapSource GotoStart
        {
            get
            {
                if (_gotoStart == null)
                {
                    lock (_syncRoot)
                    {
                        if (_gotoStart == null)
                        {
                            _gotoStart = EnsureLoaded("gotoStart16.png");
                        }
                    }
                }

                return _gotoStart;
            }
        }

        /// <summary>
        /// Gets the icon that shows a group icon.
        /// </summary>
        public static BitmapSource Pause
        {
            get
            {
                if (_pause == null)
                {
                    lock (_syncRoot)
                    {
                        if (_pause == null)
                        {
                            _pause = EnsureLoaded("pause16.png");
                        }
                    }
                }

                return _pause;
            }
        }

        /// <summary>
        /// Gets the icon that shows a group icon.
        /// </summary>
        public static BitmapSource PlayBackwards
        {
            get
            {
                if (_playBackwards == null)
                {
                    lock (_syncRoot)
                    {
                        if (_playBackwards == null)
                        {
                            _playBackwards = EnsureLoaded("playBackwards16.png");
                        }
                    }
                }

                return _playBackwards;
            }
        }

        /// <summary>
        /// Gets the icon that shows a group icon.
        /// </summary>
        public static BitmapSource PlayForwards
        {
            get
            {
                if (_playForward == null)
                {
                    lock (_syncRoot)
                    {
                        if (_playForward == null)
                        {
                            _playForward = EnsureLoaded("playForwards16.png");
                        }
                    }
                }

                return _playForward;
            }
        }

        /// <summary>
        /// Gets the icon that shows a group icon.
        /// </summary>
        public static BitmapSource Rewind
        {
            get
            {
                if (_rewind == null)
                {
                    lock (_syncRoot)
                    {
                        if (_rewind == null)
                        {
                            _rewind = EnsureLoaded("rewind16.png");
                        }
                    }
                }

                return _rewind;
            }
        }

        /// <summary>
        /// Gets the icon that shows a group icon.
        /// </summary>
        public static BitmapSource StepBackwards
        {
            get
            {
                if (_stepBackwards == null)
                {
                    lock (_syncRoot)
                    {
                        if (_stepBackwards == null)
                        {
                            _stepBackwards = EnsureLoaded("stepBackwards16.png");
                        }
                    }
                }

                return _stepBackwards;
            }
        }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Ensures that the specified image source is loaded with the specified resource name.
        /// </summary>
        /// <param name="resourceName">
        /// The name of the resource the loaded image source should set as its source.
        /// </param>
        private static BitmapSource EnsureLoaded(String resourceName)
        {
            String assemblyName = typeof(RagAnimationIcons).Assembly.GetName().Name;
            String bitmapPath = String.Format("pack://application:,,,/{0};component/Resources/{1}", assemblyName, resourceName);
            Uri uri = new Uri(bitmapPath, UriKind.RelativeOrAbsolute);

            BitmapSource source = new BitmapImage(uri);
            source.Freeze();
            return source;
        }
        #endregion //  Methods
    } // RagAnimationIcons
}
