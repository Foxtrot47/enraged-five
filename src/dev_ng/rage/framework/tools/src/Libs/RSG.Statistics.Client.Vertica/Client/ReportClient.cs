﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Logging;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Vertica.ServiceContract;

namespace RSG.Statistics.Client.Vertica.Client
{
    /// <summary>
    /// Client for consuming reports.
    /// </summary>
    public class ReportClient : ClientBaseEx<IReportService>, IReportService
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public ReportClient(IVerticaServer server)
            : base(server)
        {
        }
        #endregion // Constructor(s)

        #region IReportService Implementation
        /// <summary>
        /// Returns a list of all the available reports.
        /// </summary>
        /// <returns></returns>
        public object RunReport(String reportIdentifier, object parameters)
        {
            try
            {
                Log.Log__Profile("Executing {0} vertica report.", reportIdentifier);
                return ExecuteCommand(() => base.Channel.RunReport(reportIdentifier, parameters));
            }
            finally
            {
                Log.Log__ProfileEnd();
            }
        }
        #endregion // IReportService Implementation
    } // ReportClient
}
