﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.ROS;
using RSG.Statistics.Common;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Model.SocialClub;
using RSG.Statistics.Common.Model.VerticaData;
using RSG.Statistics.Common.Vertica.Dto;
using RSG.Statistics.Common.Vertica.ServiceContract;

namespace RSG.Statistics.Client.Vertica.Client
{
    /// <summary>
    /// Client for consuming build related data..
    /// </summary>
    public class DataClient : ClientBaseEx<IDataService>, IDataService
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public DataClient(IVerticaServer server)
            : base(server)
        {
        }
        #endregion // Constructor(s)

        #region Builds
        /// <summary>
        /// Returns a list of all the available reports.
        /// </summary>
        /// <returns></returns>
        public List<BuildFeatures> GetAllBuildFeatures()
        {
            return ExecuteCommand(() => base.Channel.GetAllBuildFeatures());
        }
        #endregion // Builds

        #region Clans
        /// <summary>
        /// Searches for a clan based on a partial name.
        /// </summary>
        public List<Clan> SearchForClan(String searchName)
        {
            return ExecuteCommand(() => base.Channel.SearchForClan(searchName));
        }

        /// <summary>
        /// Returns the list of gamers that are members of a particular clan.
        /// </summary>
        public List<Gamer> GetClanMembers(String id)
        {
            return ExecuteCommand(() => base.Channel.GetClanMembers(id));
        }
        #endregion // Clans

        #region Gamers
        /// <summary>
        /// Searches for a user with a particular name and optionally restricting the search to a particular platform.
        /// </summary>
        public List<Gamer> SearchForGamer(String searchName, IList<ROSPlatform> platforms)
        {
            return ExecuteCommand(() => base.Channel.SearchForGamer(searchName, platforms));
        }

        /// <summary>
        /// Checks that the list of gamers passed in exist in the Vertica DB.  
        /// </summary>
        /// <returns>List of valid gamers.</returns>
        public List<Gamer> ValidateGamerList(List<Gamer> gamers)
        {
            return ExecuteCommand(() => base.Channel.ValidateGamerList(gamers));
        }
        #endregion // Gamers

        #region Social Club
        /// <summary>
        /// Searches for a social club user with a particular name.
        /// </summary>
        public List<String> SearchForUser(String searchName)
        {
            return ExecuteCommand(() => base.Channel.SearchForUser(searchName));
        }

        /// <summary>
        /// Checks that the list of gamers passed in exist in the Vertica DB.  
        /// </summary>
        /// <returns>List of valid users.</returns>
        public List<String> ValidateUserList(List<String> users)
        {
            return ExecuteCommand(() => base.Channel.ValidateUserList(users));
        }

        /// <summary>
        /// Searches for a UGC mission with a particular name.
        /// </summary>
        public List<UGCMission> SearchForMission(String searchName, MissionSearchFilters filters)
        {
            return ExecuteCommand(() => base.Channel.SearchForMission(searchName, filters));
        }

        /// <summary>
        /// Retrieves the list of countries.
        /// </summary>
        public List<Country> GetCountries()
        {
            return ExecuteCommand(() => base.Channel.GetCountries());
        }
        #endregion // Social Club
    } // BuildClient
}
