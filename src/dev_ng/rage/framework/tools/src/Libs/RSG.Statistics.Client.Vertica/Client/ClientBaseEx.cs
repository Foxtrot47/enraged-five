﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using RSG.Base.Logging;
using System.Windows;
using RSG.Statistics.Common.Config;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;

namespace RSG.Statistics.Client.Vertica.Client
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class ClientBaseEx<TChannel> : ClientBase<TChannel> where TChannel : class
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="displayMessage"></param>
        public ClientBaseEx(IVerticaServer server)
            : base()
        {
            // Update the endpoint address based on the config
            string modifiedAddress = this.Endpoint.Address.Uri.ToString();
            modifiedAddress = modifiedAddress.Replace("127.0.0.1", server.Address);
            modifiedAddress = modifiedAddress.Replace("8080", server.Port.ToString());
            modifiedAddress = modifiedAddress.Replace("VerticaService", server.Service);

            // Set the modified address
            this.Endpoint.Address = new EndpointAddress(modifiedAddress);

            // Make sure that the data contract behaviour can cope with large numbers of items in the object graph.
            foreach (OperationDescription op in this.Endpoint.Contract.Operations)
            {
                var dataContractBehaviour = op.Behaviors.Find<DataContractSerializerOperationBehavior>();
                if (dataContractBehaviour != null)
                {
                    dataContractBehaviour.MaxItemsInObjectGraph = Int32.MaxValue;
                }
            }
        }
        #endregion // Constructor(s)

        #region Protected Methods
        /// <summary>
        /// Executes a command through a transaction manager
        /// </summary>
        /// <param name="command"></param>
        protected void ExecuteCommand(Action command)
        {
            try
            {
                command.Invoke();
            }
            catch (Exception e)
            {
                Log.Log__Exception(e, "Unhandled exception while communicating with the vertica services.");
                throw;
            }
        }

        /// <summary>
        /// Executes a command through a transaction manager
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="command"></param>
        /// <returns></returns>
        protected T ExecuteCommand<T>(Func<T> command)
        {
            try
            {
                return command.Invoke();
            }
            catch (Exception e)
            {
                Log.Log__Exception(e, "Unhandled exception while communicating with the vertica services.");
                throw;
            }
        }
        #endregion // Protected Methods
    } // ClientBaseEx<T>
}
