﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using RSG.Services.Persistence.Mappings;

namespace RSG.Services.Assets.Data.Mappings
{
    /// <summary>
    /// NHibernate mapping class for the
    /// <see cref="RSG.Services.Assets.Data.Entities.Object"/> class.
    /// </summary>
    public class ObjectMap : DomainEntityBaseMap<RSG.Services.Assets.Data.Entities.Object>
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="ObjectMap"/>.
        /// </summary>
        public ObjectMap()
        {
            Table("SceneXmlObject");

            Property(x => x.Guid);

            Property(x => x.Name);
            Property(x => x.Class);
            Property(x => x.SuperClass);
            Property(x => x.AttributeClass);
            Property(x => x.PolyCount);
            Property(x => x.ObjectTranslationX);
            Property(x => x.ObjectTranslationY);
            Property(x => x.ObjectTranslationZ);
            Property(x => x.ObjectRotationX);
            Property(x => x.ObjectRotationY);
            Property(x => x.ObjectRotationZ);
            Property(x => x.ObjectRotationW);
            Property(x => x.ObjectScaleX);
            Property(x => x.ObjectScaleY);
            Property(x => x.ObjectScaleZ);
            Property(x => x.NodeTranslationX);
            Property(x => x.NodeTranslationY);
            Property(x => x.NodeTranslationZ);
            Property(x => x.NodeRotationX);
            Property(x => x.NodeRotationY);
            Property(x => x.NodeRotationZ);
            Property(x => x.NodeRotationW);
            Property(x => x.NodeScaleX);
            Property(x => x.NodeScaleY);
            Property(x => x.NodeScaleZ);
            Property(x => x.LocalBoundingBoxMinX);
            Property(x => x.LocalBoundingBoxMinY);
            Property(x => x.LocalBoundingBoxMinZ);
            Property(x => x.LocalBoundingBoxMaxX);
            Property(x => x.LocalBoundingBoxMaxY);
            Property(x => x.LocalBoundingBoxMaxZ);
            Property(x => x.WorldBoundingBoxMinX);
            Property(x => x.WorldBoundingBoxMinY);
            Property(x => x.WorldBoundingBoxMinZ);
            Property(x => x.WorldBoundingBoxMaxX);
            Property(x => x.WorldBoundingBoxMaxY);
            Property(x => x.WorldBoundingBoxMaxZ);

            ManyToOne(x => x.ParentObject, m =>
            {
                m.Column("ParentObjectId");
                m.ForeignKey("FK_SceneXmlObject_ParentObjectId_SceneXmlObject_Id");
            });
            ManyToOne(x => x.ParentScene, m =>
            {
                m.Column("ParentSceneId");
                m.ForeignKey("FK_SceneXmlObject_ParentSceneId_SceneXmlScene_Id");
            });
            ManyToOne(x => x.Material, m =>
            {
                m.Column("MaterialId");
                m.ForeignKey("FK_SceneXmlObject_MaterialId_SceneXmlMaterial_Id");
            });
            ManyToOne(x => x.Scene, m =>
            {
                m.Column("SceneId");
                m.ForeignKey("FK_SceneXmlObject_SceneId_SceneXmlScene_Id");
            });

            Bag(x => x.Children, c =>
            {
                c.Key(k =>
                {
                    k.Column("ParentObjectId");
                    k.NotNullable(true);
                });
                c.Cascade(Cascade.All);
                c.Inverse(true);
            }, r => r.OneToMany());

            Bag(x => x.Attributes, c =>
            {
                c.Key(k =>
                {
                    k.Column("ObjectId");
                    k.NotNullable(true);
                });
                c.Cascade(Cascade.All);
                c.Inverse(true);
            }, r => r.OneToMany());
            
            Bag(x => x.Parameters, c =>
            {
                c.Key(k =>
                {
                    k.Column("ObjectId");
                    k.NotNullable(true);
                });
                c.Cascade(Cascade.All);
                c.Inverse(true);
            }, r => r.OneToMany());
        }
    }
}
