﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using RSG.Services.Assets.Data.Entities;
using RSG.Services.Persistence.Mappings;

namespace RSG.Services.Assets.Data.Mappings
{
    /// <summary>
    /// NHibernate mapping class for the <see cref="Parameter"/> class.
    /// </summary>
    public class ParameterMap : DomainEntityBaseMap<Parameter>
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="ParameterMap"/>.
        /// </summary>
        public ParameterMap()
        {
            Table("SceneXmlParameter");

            Property(x => x.Name);
            Property(x => x.StringValue);
            Property(x => x.IntValue);
            Property(x => x.FloatValue);
            Property(x => x.BoolValue);

            ManyToOne(x => x.Object, m =>
            {
                m.Column("ObjectId");
                m.ForeignKey("FK_SceneXmlParameter_ObjectId_SceneXmlObject_Id");
            });
        }
    }
}
