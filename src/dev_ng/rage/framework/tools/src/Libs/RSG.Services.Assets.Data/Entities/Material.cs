﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Services.Assets.Contracts.DataContracts;
using RSG.Services.Persistence.Data;

namespace RSG.Services.Assets.Data.Entities
{
    /// <summary>
    /// Domain entity class which contains information about a material
    /// in a SceneXml file.
    /// </summary>
    public class Material : DomainEntityBase
    {
        /// <summary>
        /// Unique guid for this material.
        /// </summary>
        public virtual Guid Guid { get; set; }

        /// <summary>
        /// Type of material this is.
        /// </summary>
        public virtual MaterialType Type { get; set; }

        /// <summary>
        /// Name of the shader preset that this material uses (if any).
        /// </summary>
        public virtual string Preset { get; set; }

        /// <summary>
        /// Reference to the parent file if it's a direct parent.
        /// </summary>
        public virtual Scene ParentScene { get; set; }

        /// <summary>
        /// Reference to the parent material (set if this material is part of a mult-material).
        /// </summary>
        public virtual Material ParentMaterial { get; set; }

        /// <summary>
        /// Index of this material.
        /// </summary>
        public virtual uint MaterialIndex { get; set; }

        /// <summary>
        /// Contains the sub-materials if this is a multi material.
        /// </summary>
        public virtual IList<Material> SubMaterials { get; set; }

        /// <summary>
        /// List of textures this material uses.  Note that this is only textures that are
        /// immediately used by this material.  Multi materials won't automatically have all
        /// sub-material textures populated.
        /// </summary>
        public virtual IList<Texture> Textures { get; set; }

        /// <summary>
        /// Reference to the scene that this material is a part of.
        /// </summary>
        public virtual Scene Scene { get; set; }
    }
}
