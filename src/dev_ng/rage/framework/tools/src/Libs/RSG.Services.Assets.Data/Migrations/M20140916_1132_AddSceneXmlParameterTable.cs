﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;
using RSG.Services.Persistence.Migrations;

namespace RSG.Services.Assets.Data.Migrations
{
    /// <summary>
    /// Migration for adding the new scene xml parameter table.
    /// </summary>
    [DatetimeMigration("tom.francis", 2014, 09, 16, 11, 32, "Adding new scene xml parameter table.")]
    public class M20140916_1132_AddSceneXmlParameterTable : Migration
    {
        /// <summary>
        /// Operations to perform when migrating up to this change.
        /// </summary>
        public override void Up()
        {
            Create.Table("SceneXmlParameter")
                .WithColumn("Id")
                    .AsInt64()
                    .NotNullable()
                    .PrimaryKey()
                    .Identity()
                .WithColumn("Name")
                    .AsString()
                    .Nullable()
                .WithColumn("StringValue")
                    .AsString()
                    .Nullable()
                .WithColumn("IntValue")
                    .AsInt32()
                    .Nullable()
                .WithColumn("FloatValue")
                    .AsFloat()
                    .Nullable()
                .WithColumn("BoolValue")
                    .AsBoolean()
                    .Nullable()
                .WithColumn("ObjectId")
                    .AsInt64()
                    .Nullable();

            Create.ForeignKey("FK_SceneXmlParameter_ObjectId_SceneXmlObject_Id")
                .FromTable("SceneXmlParameter")
                    .ForeignColumn("ObjectId")
                .ToTable("SceneXmlObject")
                    .PrimaryColumn("Id");
        }

        /// <summary>
        /// Operations to undo the changes this migration introduced.
        /// </summary>
        public override void Down()
        {
            Delete.ForeignKey("FK_SceneXmlParameter_ObjectId_SceneXmlObject_Id").OnTable("SceneXmlParameter");
            Delete.Table("SceneXmlParameter");
        }
    }
}
