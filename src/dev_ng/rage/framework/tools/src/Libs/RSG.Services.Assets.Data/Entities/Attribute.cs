﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Services.Persistence.Data;

namespace RSG.Services.Assets.Data.Entities
{
    /// <summary>
    /// Domain entity class which contains information about a single attribute
    /// for a scene xml object.
    /// </summary>
    public class Attribute : DomainEntityBase
    {
        /// <summary>
        /// Name of the attribute.
        /// </summary>
        public virtual String Name { get; set; }

        /// <summary>
        /// String attribute value.
        /// </summary>
        public virtual String StringValue { get; set; }

        /// <summary>
        /// Integer attribute value.
        /// </summary>
        public virtual int? IntValue { get; set; }

        /// <summary>
        /// Float attribute value.
        /// </summary>
        public virtual float? FloatValue { get; set; }

        /// <summary>
        /// Bool attribute value.
        /// </summary>
        public virtual bool? BoolValue { get; set; }

        /// <summary>
        /// Object that this attribute is for.
        /// </summary>
        public virtual Object Object { get; set; }
    }
}
