﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using RSG.Services.Assets.Data.Entities;
using RSG.Services.Persistence.Mappings;

namespace RSG.Services.Assets.Data.Mappings
{
    /// <summary>
    /// NHibernate mapping class for the <see cref="Material"/> class.
    /// </summary>
    public class MaterialMap : DomainEntityBaseMap<Material>
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="MaterialMap"/>.
        /// </summary>
        public MaterialMap()
        {
            Table("SceneXmlMaterial");

            Property(x => x.Guid);
            Property(x => x.Type);
            Property(x => x.Preset);
            Property(x => x.MaterialIndex);

            ManyToOne(x => x.ParentScene, m =>
            {
                m.Column("ParentSceneId");
                m.ForeignKey("FK_SceneXmlMaterial_ParentSceneId_SceneXmlScene_Id");
            });
            ManyToOne(x => x.ParentMaterial, m =>
            {
                m.Column("ParentMaterialId");
                m.ForeignKey("FK_SceneXmlMaterial_ParentMaterialId_SceneXmlMaterial_Id");
            });
            ManyToOne(x => x.Scene, m =>
            {
                m.Column("SceneId");
                m.ForeignKey("FK_SceneXmlMaterial_SceneId_SceneXmlScene_Id");
            });

            Bag(x => x.SubMaterials, c =>
            {
                c.Key(k =>
                {
                    k.Column("ParentMaterialId");
                    k.NotNullable(true);
                });
                c.Cascade(Cascade.All);
                c.Inverse(true);
            }, r => r.OneToMany());

            Bag(x => x.Textures, c =>
            {
                c.Key(k =>
                {
                    k.Column("MaterialId");
                    k.NotNullable(true);
                });
                c.Cascade(Cascade.All);
                c.Inverse(true);
            }, r => r.OneToMany());
        }
    }
}
