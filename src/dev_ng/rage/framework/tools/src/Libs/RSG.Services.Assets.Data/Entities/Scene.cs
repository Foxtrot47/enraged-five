﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Services.Persistence.Data;

namespace RSG.Services.Assets.Data.Entities
{
    /// <summary>
    /// Domain entity class which contains information about a single
    /// SceneXml file.
    /// </summary>
    public class Scene : DomainEntityBase, IHasCreatedInformation
    {
        /// <summary>
        /// Datetime as to when this entity was created.
        /// </summary>
        /// <remarks>
        /// This will automatically be set by the <see cref="AuditEventListener"/>
        /// that is setup to respond to PreInsert event.
        /// </remarks>
        public virtual DateTime CreatedOn { get; set; }

        /// <summary>
        /// Path to the file.
        /// </summary>
        public virtual String Filename { get; set; }

        /// <summary>
        /// Version of the SceneXml library that was used to save out the file.
        /// </summary>
        public virtual float Version { get; set; }

        /// <summary>
        /// Time the file was last exported.
        /// </summary>
        public virtual DateTime Timestamp { get; set; }

        /// <summary>
        /// Name of the user that last exported the file.
        /// </summary>
        public virtual String Username { get; set; }

        /// <summary>
        /// List of objects that this file has at the root level.
        /// </summary>
        public virtual IList<Object> Objects { get; set; }

        /// <summary>
        /// List of materials that this file has at the root level.
        /// </summary>
        public virtual IList<Material> Materials { get; set; }

        /// <summary>
        /// List of all objects this file contains. Note that this isn't only the root
        /// level objects.
        /// </summary>
        public virtual IList<Object> AllObjects { get; set; }

        /// <summary>
        /// List of materials this file contains.  Note that this isn't only the root
        /// level objects.
        /// </summary>
        public virtual IList<Material> AllMaterials { get; set; }
    }
}
