﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using RSG.Services.Assets.Data.Entities;
using RSG.Services.Persistence.Mappings;

namespace RSG.Services.Assets.Data.Mappings
{
    /// <summary>
    /// NHibernate mapping class for the <see cref="Scene"/> class.
    /// </summary>
    public class SceneMap : DomainEntityBaseMap<Scene>
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="SceneMap"/>.
        /// </summary>
        public SceneMap()
        {
            Table("SceneXmlScene");

            Property(x => x.CreatedOn, m => m.NotNullable(true));
            Property(x => x.Filename);
            Property(x => x.Timestamp);
            Property(x => x.Username);
            Property(x => x.Version);

            Bag(x => x.Objects, c =>
            {
                c.Key(k =>
                {
                    k.Column("ParentSceneId");
                    k.NotNullable(true);
                });
                c.Cascade(Cascade.All);
                c.Inverse(true);
            }, r => r.OneToMany());

            Bag(x => x.Materials, c =>
            {
                c.Key(k =>
                {
                    k.Column("ParentSceneId");
                    k.NotNullable(true);
                });
                c.Cascade(Cascade.All);
                c.Inverse(true);
            }, r => r.OneToMany());

            Bag(x => x.AllObjects, c =>
            {
                c.Key(k =>
                {
                    k.Column("SceneId");
                    k.NotNullable(true);
                });
                c.Cascade(Cascade.None);
                c.Inverse(true);
            }, r => r.OneToMany());

            Bag(x => x.AllMaterials, c =>
            {
                c.Key(k =>
                {
                    k.Column("SceneId");
                    k.NotNullable(true);
                });
                c.Cascade(Cascade.None);
                c.Inverse(true);
            }, r => r.OneToMany());
        }
    }
}
