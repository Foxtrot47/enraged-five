﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using RSG.Services.Assets.Data.Entities;
using RSG.Services.Persistence.Mappings;

namespace RSG.Services.Assets.Data.Mappings
{
    /// <summary>
    /// NHibernate mapping class for the <see cref="Texture"/> class.
    /// </summary>
    public class TextureMap : DomainEntityBaseMap<Texture>
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="TextureMap"/>.
        /// </summary>
        public TextureMap()
        {
            Table("SceneXmlTexture");

            Property(x => x.Filename);
            Property(x => x.AlphaFilename);
            Property(x => x.Type);

            ManyToOne(x => x.Material, m =>
            {
                m.Column("MaterialId");
                m.ForeignKey("FK_SceneXmlTexture_MaterialId_SceneXmlMaterial_Id");
            });
            ManyToOne(x => x.Scene, m =>
            {
                m.Column("SceneId");
                m.ForeignKey("FK_SceneXmlTexture_SceneId_SceneXmlScene_Id");
            });
        }
    }
}
