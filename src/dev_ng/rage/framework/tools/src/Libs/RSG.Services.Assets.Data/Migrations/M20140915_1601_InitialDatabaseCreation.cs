﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;
using RSG.Services.Persistence.Migrations;

namespace RSG.Services.Assets.Data.Migrations
{
    /// <summary>
    /// Migration for the initial creation of the database.
    /// </summary>
    [DatetimeMigration("michael.taschler", 2014, 09, 15, 16, 01, "Initial creation of the database")]
    public class M20140915_1601_InitialDatabaseCreation : Migration
    {
        /// <summary>
        /// Operations to perform when migrating up to this change.
        /// </summary>
        public override void Up()
        {
            Create.Table("SceneXmlScene")
                .WithColumn("Id")
                    .AsInt64()
                    .NotNullable()
                    .PrimaryKey()
                    .Identity()
                .WithColumn("CreatedOn")
                    .AsDateTime()
                    .NotNullable()
                .WithColumn("Filename")
                    .AsString()
                    .Nullable()
                .WithColumn("Version")
                    .AsFloat()
                    .Nullable()
                .WithColumn("Timestamp")
                    .AsDateTime()
                    .Nullable()
                .WithColumn("Username")
                    .AsString()
                    .Nullable();

            Create.Table("SceneXmlObject")
                .WithColumn("Id")
                    .AsInt64()
                    .NotNullable()
                    .PrimaryKey()
                    .Identity()
                .WithColumn("Guid")
                    .AsGuid()
                    .Nullable()
                .WithColumn("Name")
                    .AsString()
                    .Nullable()
                .WithColumn("Class")
                    .AsString()
                    .Nullable()
                .WithColumn("SuperClass")
                    .AsString()
                    .Nullable()
                .WithColumn("AttributeClass")
                    .AsString()
                    .Nullable()
                .WithColumn("PolyCount")
                    .AsUInt32()
                    .Nullable()
                .WithColumn("ParentObjectId")
                    .AsInt64()
                    .Nullable()
                .WithColumn("ParentSceneId")
                    .AsInt64()
                    .Nullable()
                .WithColumn("MaterialId")
                    .AsInt64()
                    .Nullable()
                .WithColumn("SceneId")
                    .AsInt64()
                    .Nullable()
                .WithColumn("ObjectTranslationX")
                    .AsFloat()
                    .Nullable()
                .WithColumn("ObjectTranslationY")
                    .AsFloat()
                    .Nullable()
                .WithColumn("ObjectTranslationZ")
                    .AsFloat()
                    .Nullable()
                .WithColumn("ObjectRotationX")
                    .AsFloat()
                    .Nullable()
                .WithColumn("ObjectRotationY")
                    .AsFloat()
                    .Nullable()
                .WithColumn("ObjectRotationZ")
                    .AsFloat()
                    .Nullable()
                .WithColumn("ObjectRotationW")
                    .AsFloat()
                    .Nullable()
                .WithColumn("ObjectScaleX")
                    .AsFloat()
                    .Nullable()
                .WithColumn("ObjectScaleY")
                    .AsFloat()
                    .Nullable()
                .WithColumn("ObjectScaleZ")
                    .AsFloat()
                    .Nullable()
                .WithColumn("NodeTranslationX")
                    .AsFloat()
                    .Nullable()
                .WithColumn("NodeTranslationY")
                    .AsFloat()
                    .Nullable()
                .WithColumn("NodeTranslationZ")
                    .AsFloat()
                    .Nullable()
                .WithColumn("NodeRotationX")
                    .AsFloat()
                    .Nullable()
                .WithColumn("NodeRotationY")
                    .AsFloat()
                    .Nullable()
                .WithColumn("NodeRotationZ")
                    .AsFloat()
                    .Nullable()
                .WithColumn("NodeRotationW")
                    .AsFloat()
                    .Nullable()
                .WithColumn("NodeScaleX")
                    .AsFloat()
                    .Nullable()
                .WithColumn("NodeScaleY")
                    .AsFloat()
                    .Nullable()
                .WithColumn("NodeScaleZ")
                    .AsFloat()
                    .Nullable()
                .WithColumn("LocalBoundingBoxMinX")
                    .AsFloat()
                    .Nullable()
                .WithColumn("LocalBoundingBoxMinY")
                    .AsFloat()
                    .Nullable()
                .WithColumn("LocalBoundingBoxMinZ")
                    .AsFloat()
                    .Nullable()
                .WithColumn("LocalBoundingBoxMaxX")
                    .AsFloat()
                    .Nullable()
                .WithColumn("LocalBoundingBoxMaxY")
                    .AsFloat()
                    .Nullable()
                .WithColumn("LocalBoundingBoxMaxZ")
                    .AsFloat()
                    .Nullable()
                .WithColumn("WorldBoundingBoxMinX")
                    .AsFloat()
                    .Nullable()
                .WithColumn("WorldBoundingBoxMinY")
                    .AsFloat()
                    .Nullable()
                .WithColumn("WorldBoundingBoxMinZ")
                    .AsFloat()
                    .Nullable()
                .WithColumn("WorldBoundingBoxMaxX")
                    .AsFloat()
                    .Nullable()
                .WithColumn("WorldBoundingBoxMaxY")
                    .AsFloat()
                    .Nullable()
                .WithColumn("WorldBoundingBoxMaxZ")
                    .AsFloat()
                    .Nullable();

            Create.Table("SceneXmlMaterial")
                .WithColumn("Id")
                    .AsInt64()
                    .NotNullable()
                    .PrimaryKey()
                    .Identity()
                .WithColumn("Guid")
                    .AsGuid()
                    .Nullable()
                .WithColumn("Type")
                    .AsInt32()
                    .Nullable()
                .WithColumn("Preset")
                    .AsString()
                    .Nullable()
                .WithColumn("MaterialIndex")
                    .AsUInt32()
                    .Nullable()
                .WithColumn("ParentSceneId")
                    .AsInt64()
                    .Nullable()
                .WithColumn("ParentMaterialId")
                    .AsInt64()
                    .Nullable()
                .WithColumn("SceneId")
                    .AsInt64()
                    .Nullable();

            Create.Table("SceneXmlTexture")
                .WithColumn("Id")
                    .AsInt64()
                    .NotNullable()
                    .PrimaryKey()
                    .Identity()
                .WithColumn("Filename")
                    .AsString()
                    .Nullable()
                .WithColumn("AlphaFilename")
                    .AsString()
                    .Nullable()
                .WithColumn("Type")
                    .AsInt32()
                    .Nullable()
                .WithColumn("MaterialId")
                    .AsInt64()
                    .Nullable()
                .WithColumn("SceneId")
                    .AsInt64()
                    .Nullable();

            Create.Table("SceneXmlAttribute")
                .WithColumn("Id")
                    .AsInt64()
                    .NotNullable()
                    .PrimaryKey()
                    .Identity()
                .WithColumn("Name")
                    .AsString()
                    .Nullable()
                .WithColumn("StringValue")
                    .AsString()
                    .Nullable()
                .WithColumn("IntValue")
                    .AsInt32()
                    .Nullable()
                .WithColumn("FloatValue")
                    .AsFloat()
                    .Nullable()
                .WithColumn("BoolValue")
                    .AsBoolean()
                    .Nullable()
                .WithColumn("ObjectId")
                    .AsInt64()
                    .Nullable();

            // Create foreign keys after we've added all the tables.

            // SceneXmlObject foreign keys.
            Create.ForeignKey("FK_SceneXmlObject_ParentObjectId_SceneXmlObject_Id")
                .FromTable("SceneXmlObject")
                    .ForeignColumn("ParentObjectId")
                .ToTable("SceneXmlObject")
                    .PrimaryColumn("Id");

            Create.ForeignKey("FK_SceneXmlObject_ParentSceneId_SceneXmlScene_Id")
                .FromTable("SceneXmlObject")
                    .ForeignColumn("ParentSceneId")
                .ToTable("SceneXmlScene")
                    .PrimaryColumn("Id");

            Create.ForeignKey("FK_SceneXmlObject_MaterialId_SceneXmlMaterial_Id")
                .FromTable("SceneXmlObject")
                    .ForeignColumn("MaterialId")
                .ToTable("SceneXmlMaterial")
                    .PrimaryColumn("Id");

            Create.ForeignKey("FK_SceneXmlObject_SceneId_SceneXmlScene_Id")
                .FromTable("SceneXmlObject")
                    .ForeignColumn("SceneId")
                .ToTable("SceneXmlScene")
                    .PrimaryColumn("Id");

            // SceneXmlMaterial foreign keys.
            Create.ForeignKey("FK_SceneXmlMaterial_ParentSceneId_SceneXmlScene_Id")
                .FromTable("SceneXmlMaterial")
                    .ForeignColumn("ParentSceneId")
                .ToTable("SceneXmlScene")
                    .PrimaryColumn("Id");

            Create.ForeignKey("FK_SceneXmlMaterial_ParentMaterialId_SceneXmlMaterial_Id")
                .FromTable("SceneXmlMaterial")
                    .ForeignColumn("ParentMaterialId")
                .ToTable("SceneXmlMaterial")
                    .PrimaryColumn("Id");

            Create.ForeignKey("FK_SceneXmlMaterial_SceneId_SceneXmlScene_Id")
                .FromTable("SceneXmlMaterial")
                    .ForeignColumn("SceneId")
                .ToTable("SceneXmlScene")
                    .PrimaryColumn("Id");

            // SceneXmlTexture foreign keys.
            Create.ForeignKey("FK_SceneXmlTexture_MaterialId_SceneXmlMaterial_Id")
                .FromTable("SceneXmlTexture")
                    .ForeignColumn("MaterialId")
                .ToTable("SceneXmlMaterial")
                    .PrimaryColumn("Id");

            Create.ForeignKey("FK_SceneXmlTexture_SceneId_SceneXmlScene_Id")
                .FromTable("SceneXmlTexture")
                    .ForeignColumn("SceneId")
                .ToTable("SceneXmlScene")
                    .PrimaryColumn("Id");

            // SceneXmlAttribute foreign keys
            Create.ForeignKey("FK_SceneXmlAttribute_ObjectId_SceneXmlObject_Id")
                .FromTable("SceneXmlAttribute")
                    .ForeignColumn("ObjectId")
                .ToTable("SceneXmlObject")
                    .PrimaryColumn("Id");
        }

        /// <summary>
        /// Operations to undo the changes this migration introduced.
        /// </summary>
        public override void Down()
        {
            Delete.ForeignKey("FK_SceneXmlAttribute_ObjectId_SceneXmlObject_Id").OnTable("SceneXmlAttribute");
            Delete.ForeignKey("FK_SceneXmlTexture_SceneId_SceneXmlScene_Id").OnTable("SceneXmlTexture");
            Delete.ForeignKey("FK_SceneXmlTexture_MaterialId_SceneXmlMaterial_Id").OnTable("SceneXmlTexture");
            Delete.ForeignKey("FK_SceneXmlMaterial_SceneId_SceneXmlScene_Id").OnTable("SceneXmlMaterial");
            Delete.ForeignKey("FK_SceneXmlMaterial_ParentMaterialId_SceneXmlMaterial_Id").OnTable("SceneXmlMaterial");
            Delete.ForeignKey("FK_SceneXmlMaterial_ParentSceneId_SceneXmlScene_Id").OnTable("SceneXmlMaterial");
            Delete.ForeignKey("FK_SceneXmlObject_SceneId_SceneXmlScene_Id").OnTable("SceneXmlObject");
            Delete.ForeignKey("FK_SceneXmlObject_MaterialId_SceneXmlMaterial_Id").OnTable("SceneXmlObject");
            Delete.ForeignKey("FK_SceneXmlObject_ParentSceneId_SceneXmlScene_Id").OnTable("SceneXmlObject");
            Delete.ForeignKey("FK_SceneXmlObject_ParentObjectId_SceneXmlObject_Id").OnTable("SceneXmlObject");

            Delete.Table("SceneXmlAttribute");
            Delete.Table("SceneXmlTexture");
            Delete.Table("SceneXmlMaterial");
            Delete.Table("SceneXmlObject");
            Delete.Table("SceneXmlScene");
        }
    }
}
