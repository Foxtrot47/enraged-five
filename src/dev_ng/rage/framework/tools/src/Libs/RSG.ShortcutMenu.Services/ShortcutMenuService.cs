﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using RSG.ShortcutMenu.Common;

namespace RSG.ShortcutMenu.Services
{
    /// <summary>
    /// 
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class ShortcutMenuService : IShortcutMenuService
    {
        #region Fields
        /// <summary>
        /// Reference to the object that can invoke shortcut item.
        /// </summary>
        private IShortcutItemInvoker _shortcutItemInvoker;
        #endregion // Fields

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="invoker"></param>
        public ShortcutMenuService(IShortcutItemInvoker invoker)
        {
            _shortcutItemInvoker = invoker;
        }
        #endregion // Constructor(s)

        #region IShortcutMenuService Implementation
        /// <summary>
        /// Invokes a particular shortcut item based on it's guid.
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public bool InvokeShortcutItem(Guid guid)
        {
            return _shortcutItemInvoker.InvokeShortcutItem(guid);
        }
        #endregion // IShortcutMenuService Implementation
    } // ShortcutMenuService
}
