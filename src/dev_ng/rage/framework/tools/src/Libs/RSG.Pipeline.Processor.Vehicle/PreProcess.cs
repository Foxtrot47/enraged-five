﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using SIO = System.IO;
using RSG.Base.Configuration;
using RSG.Base.Collections;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Attributes;
using RSG.Pipeline.Content;
using RSG.Pipeline.Processor;
using RSG.Pipeline.Services;
using RSG.Pipeline.Services.AssetPack;
using RSG.Pipeline.Services.Rage;
using RSG.Platform;
using XGE = RSG.Interop.Incredibuild.XGE;

namespace RSG.Pipeline.Processor.Vehicle
{

    /// <summary>
    /// Vehicle asset pre-processor.
    /// </summary>
    [Export(typeof(IProcessor))]
    [ProcessorFlags(ProcessorFlags.Compatibility)]
    class PreProcess :
        ProcessorBase,
        IProcessor
    {
        #region Constants
        /// <summary>
        /// Processor description string.
        /// </summary>
        private static readonly String DESCRIPTION = "Vehicle Asset Processor";

        /// <summary>
        /// Processor log context.
        /// </summary>
        private static readonly String LOG_CTX = "Vehicle";
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public PreProcess()
            : base(DESCRIPTION)
        {
        }
        #endregion // Constructor(s)

        #region IProcessor Interface Methods
        /// <summary>
        /// Prepare content; first pass of the on-disk content-tree.  Returns a
        /// set of IProcess objects after determining all required inputs, 
        /// outputs and whether the process' needs to change (this allows the 
        /// concept of 'preprocessors').
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="process">Process to prebuild.</param>
        /// <param name="processors">Processor collection to reference for new resultant proceses.</param>
        /// <param name="owner">Owning IContentTree.</param>
        /// <param name="syncDependencies">Dependencies to sync.</param>
        /// <param name="resultantProcesses">RawProcesses that will actually be built.</param>
        /// <returns>true iff successful; false otherwise</returns>
        /// The resultant processes are used by the pipeline engine after these
        /// calls.
        /// 
        public override bool Prebuild(IEngineParameters param,
            IProcess process, IProcessorCollection processors,
            IContentTree owner, out IEnumerable<IContentNode> syncDependencies,
            out IEnumerable<IProcess> resultantProcesses)
        {
            bool result = true;
            List<IProcess>  processes = new List<IProcess>();
            List<IContentNode> syncDepends = new List<IContentNode>();

            // Recursive prebuild, sync dependancies first
            if (process.State == ProcessState.Initialised)
            {
#warning DHM FIX ME: need to handle directory specifying additional property to describe its content for Rage dependencies.
                // Add vehicles texture metadata directory; this is maybe temporary until 
                // we have directories that can hint to the Rage processor about its contents.
                String textureMetadataTemplateDir = SIO.Path.Combine(param.Branch.Metadata, "textures", "templates", "vehicles");
                String textureMetadataTemplateMpDir = SIO.Path.Combine(param.Branch.Metadata, "textures", "templates", "vehicles_mp");
                String textureMetadataDir = SIO.Path.Combine(param.Branch.Metadata, "textures", "vehicles");
                String textureMetadataMpDir = SIO.Path.Combine(param.Branch.Metadata, "textures", "vehicles_mp");
                syncDepends.Add(owner.CreateDirectory(textureMetadataTemplateDir));
                syncDepends.Add(owner.CreateDirectory(textureMetadataTemplateMpDir));
                syncDepends.Add(owner.CreateDirectory(textureMetadataDir));
                syncDepends.Add(owner.CreateDirectory(textureMetadataMpDir));

                // First class dependencies
#warning LPXO: For some fucked up reason, the shared tetxures are exported and submitted to a seperate location.
                String vehTextureDir = param.Branch.Environment.Subst("$(assets)/vehicles/textures");
                String vehTextureAltDir = param.Branch.Environment.Subst("$(assets)/textures/vehicles");
                String vehTextureMetdataDir = param.Branch.Environment.Subst("$(assets)/metadata/textures/vehicles");
                IContentNode vehTextureContent = owner.CreateDirectory(vehTextureDir);
                IContentNode vehTextureAltContent = owner.CreateDirectory(vehTextureAltDir);
                IContentNode vehTextureMetaContent = owner.CreateDirectory(vehTextureMetdataDir);
                syncDepends.Add(vehTextureContent);
                syncDepends.Add(vehTextureAltContent);
                syncDepends.Add(vehTextureMetaContent);

                String vehAnimDir = param.Branch.Environment.Subst("$(assets)/vehicles/anims");
                IContentNode vehAnimDirContent = owner.CreateDirectory(vehAnimDir);
                syncDepends.Add(vehAnimDirContent);

                String vehLODHierachyDefs = param.Branch.Environment.Subst("$(assets)/vehicles/lods/lodhiearchydef.xml");
                IContentNode vehLODHierachyContent = owner.CreateFile(vehLODHierachyDefs);
                syncDepends.Add(vehLODHierachyContent);

                syncDependencies = syncDepends;
                resultantProcesses = processes;
                process.State = ProcessState.Prebuilding;
                return (result);
            }

            Content.Directory vehicleDirectory = (process.Inputs.First() as Content.Directory);
            String processedVehicleDirectory = RemapFilenameToProcessedCache(param.Branch, vehicleDirectory.AbsolutePath);

            // Split out inputs by their export format so they can be handled separately.
            IEnumerable<IContentNode> newVehicleInputs = vehicleDirectory.EvaluateInputs().Where(x => (x as File).AbsolutePath.EndsWith(".veh.zip"));
            var rejectList = vehicleDirectory.EvaluateInputs().Where(x => newVehicleInputs.Select(nv => (nv as File).Basename).Contains((x as File).Basename));
            IEnumerable<IContentNode> oldVehicleInputs = vehicleDirectory.EvaluateInputs().Except(rejectList);

            // single player Rage process 
            ProcessBuilder pbsp = new ProcessBuilder("RSG.Pipeline.Processor.Platform.RagePreprocessor", processors, owner);
            pbsp.Parameters.Add(Core.Constants.RageProcess_DelayedDirectoryEvaluationParameterName, true);
            pbsp.Outputs.Add(process.Outputs.First());

            // multi player Rage process 
            ProcessBuilder pbmp = new ProcessBuilder("RSG.Pipeline.Processor.Platform.RagePreprocessor", processors, owner);
            pbmp.Parameters.Add(Core.Constants.RageProcess_DelayedDirectoryEvaluationParameterName, true);
            IContentNode spOutput = process.Outputs.First();
            String mpOutputFile = SIO.Path.Combine(SIO.Path.GetDirectoryName((spOutput as File).AbsolutePath), SIO.Path.GetFileNameWithoutExtension((spOutput as File).AbsolutePath) + "_mp.rpf");
            IContentNode outputFileNode = owner.CreateFile(mpOutputFile);
            pbmp.Outputs.Add(outputFileNode);

            if (oldVehicleInputs.Any())
            {
                String cache = PreProcess.GetCacheDir(param);
                List<Pair<IContentNode, IContentNode>> singleplayerProcessedContent = new List<Pair<IContentNode, IContentNode>>();
                List<Pair<IContentNode, IContentNode>> multiplayerProcessedContent = new List<Pair<IContentNode, IContentNode>>();

                // Loop through all out inputs and create the necessary extraction processes.
                foreach (IContentNode dictionary in oldVehicleInputs)
                {
                    // Quick Export mode support for vehicles.
                    // DHM FIX ME: this could be improved by prebuilding parts so that the RPF would
                    // pick additional vehicles up from the cache if they existed.
                    if (param.Flags.HasFlag(EngineFlags.Selected) && vehicleDirectory.HasParameter(Constants.Convert_Filenames))
                    {
                        Content.File vehDictionary = (Content.File)dictionary;
                        List<String> convertFilenames = vehicleDirectory.GetParameter(Constants.Convert_Filenames, new List<String>());
                        if (!convertFilenames.Contains(vehDictionary.AbsolutePath))
                            continue; // Skip as its not an input file.
                    }

                    String dictionaryName = Filename.GetBasename(SIO.Path.GetFileName((dictionary as RSG.Pipeline.Content.File).AbsolutePath));
                    String rootDir = SIO.Path.Combine(cache, dictionaryName);
                    String processedDir = SIO.Path.Combine(processedVehicleDirectory, dictionaryName);
                    String extractionDirectory = SIO.Path.Combine(rootDir, "extraction");
                    String spExtractDir = SIO.Path.Combine(extractionDirectory, "sp");
                    String mpExtractDir = SIO.Path.Combine(extractionDirectory, "mp");

                    IContentNode extractionDirNode = owner.CreateDirectory(extractionDirectory, "*.*", true);
                    ProcessBuilder extraction = new ProcessBuilder("RSG.Pipeline.Processor.Common.AssetExtractionProcessor", processors, owner);
                    extraction.Inputs.Add((dictionary as RSG.Pipeline.Content.File));
                    extraction.Outputs.Add(extractionDirNode);

                    IContentNode singleplayerData = owner.CreateDirectory(spExtractDir, "*.*");
                    IContentNode multiplayerData = owner.CreateDirectory(mpExtractDir, "*.*");

                    extraction.Outputs.Add(singleplayerData);
                    extraction.Outputs.Add(multiplayerData);

                    processes.Add(extraction.ToProcess());

                    // Store up content lists of multiplayer/singleplayer content for generating our rage processors.
                    singleplayerProcessedContent.Add(new Pair<IContentNode, IContentNode>(singleplayerData, extractionDirNode));
                    multiplayerProcessedContent.Add(new Pair<IContentNode, IContentNode>(multiplayerData, extractionDirNode));
                }

                // Add single player content inputs
                foreach (Pair<IContentNode, IContentNode> contentPair in singleplayerProcessedContent)
                {
                    pbsp.Inputs.Add(contentPair.First);
                    pbsp.Inputs.Add(contentPair.Second);
                }

                // Add multi player content inputs
                foreach (Pair<IContentNode, IContentNode> contentPair in multiplayerProcessedContent)
                {
                    pbmp.Inputs.Add(contentPair.First);
                    pbmp.Inputs.Add(contentPair.Second);
                }
            }
            
            // New style .veh.zip assets
            if (newVehicleInputs.Any())
            {
                String cache = PreProcess.GetCacheDir(param);
                String lodDescFile =  SIO.Path.Combine(param.CoreBranch.Assets, "vehicles", "lods", "lodhiearchydef.xml");
                if (!SIO.File.Exists(lodDescFile))
                {
                    // DHM FIX ME: this is a sync dependency.  First time round its not there.
                    // The processor should set state as prebuilding until its there if we
                    // need to read the file here!
                    param.Log.ErrorCtx(LOG_CTX, "New style vehicle assets have been provided but no definition file exists: {0}", lodDescFile);
                    result = false;
                }
                IContentNode lodDescFileNode = owner.CreateFile(lodDescFile);
                IEnumerable<ILodDescription> lodDescs = LodDescription.LoadLodDescriptionsFromFile(lodDescFile, param.Branch.Project.Config, param.Log);
                Dictionary<Tuple<String, RSG.Platform.Platform, Mode>, List<IContentNode>> processedNodes = new Dictionary<Tuple<String, RSG.Platform.Platform, Mode>, List<IContentNode>>(); 
                
                // Loop through all our inputs creating the necessary processes.
                foreach (IContentNode dictionary in newVehicleInputs)
                {
                    // Quick Export mode support for vehicles.
                    // DHM FIX ME: this could be improved by prebuilding parts so that the RPF would
                    // pick additional vehicles up from the cache if they existed.
                    if (param.Flags.HasFlag(EngineFlags.Selected) && vehicleDirectory.HasParameter(Constants.Convert_Filenames))
                    {
                        Content.File vehDictionary = (Content.File)dictionary;
                        List<String> convertFilenames = vehicleDirectory.GetParameter(Constants.Convert_Filenames, new List<String>());
                        if (!convertFilenames.Contains(vehDictionary.AbsolutePath))
                            continue; // Skip as its not an input file.
                    }

                    bool hasAnims = false;
                    IEnumerable<String> zipFileList;// = new List<String>();
                    Zip.GetFileList((dictionary as RSG.Pipeline.Content.File).AbsolutePath, out zipFileList);
                    if (zipFileList.Where(f => SIO.Path.GetExtension(f).Replace(".","") == FileType.AnimFile.GetExportExtension()).Any())
                        hasAnims = true;
                    String dictionaryName = Filename.GetBasename(SIO.Path.GetFileName((dictionary as RSG.Pipeline.Content.File).AbsolutePath));
                    String rootDir = SIO.Path.Combine(cache, dictionaryName);
                    String processedDir = SIO.Path.Combine(processedVehicleDirectory, dictionaryName);
                    String extractionDirectory = SIO.Path.Combine(rootDir, "extraction_ng");
                    String fabricationDirectory = SIO.Path.Combine(rootDir, "fabrication");

                    IContentNode extractionDirNode = owner.CreateDirectory(extractionDirectory, "*.*", true);
                    ProcessBuilder extraction = new ProcessBuilder("RSG.Pipeline.Processor.Common.AssetExtractionProcessor", processors, owner);
                    extraction.Inputs.Add((dictionary as RSG.Pipeline.Content.File));
                    extraction.Outputs.Add(extractionDirNode);
                    
                    // Lod generator processor for fabricating our various independent fragment data.
                    IContentNode fabricationDirNode = owner.CreateDirectory(fabricationDirectory, "*.*", true);
                    ProcessBuilder lodGeneration = new ProcessBuilder("RSG.Pipeline.Processor.Vehicle.LodGenerator", processors, owner);
                    lodGeneration.AdditionalDependentContent.Add(lodDescFileNode);
                    lodGeneration.Inputs.Add(extractionDirNode);
                    lodGeneration.Outputs.Add(fabricationDirNode);

                    // Asset packer for packing up our fabricated vehicles
                    AssetPackScheduleBuilder assetPackScheduleBuilder = new AssetPackScheduleBuilder();
                    ProcessBuilder assetPacker = new ProcessBuilder("RSG.Pipeline.Processor.Common.AssetPackProcessor", processors, owner);
                    String assetPackSchedulePathname = System.IO.Path.Combine(rootDir, "asset_pack_schedule.xml");
                    
                    IContentNode assetPackScheduleNode = owner.CreateFile(assetPackSchedulePathname);
                    assetPacker.Inputs.Add(assetPackScheduleNode);

                    Dictionary<String, List<String>> textureCollections = new Dictionary<String, List<String>>();

                    foreach (ILodDescription lodDesc in lodDescs)
                    {
                        String outputDirDesc = lodDesc.GetDataLocation(fabricationDirectory);
                        String textureDir = SIO.Path.Combine(outputDirDesc, "textures");
                        String animDir = SIO.Path.Combine(outputDirDesc, "anim");
                        String animDirCompressed = SIO.Path.Combine(outputDirDesc, "anim", "compressed");
                        String animGroupingDir = SIO.Path.Combine(outputDirDesc, "anim", "group");
                        IContentNode outputDirDescNode = owner.CreateDirectory(outputDirDesc, "*.*", true);
                        IContentNode textureDirNode = owner.CreateDirectory(textureDir, "*.*", true);
                        
                        lodGeneration.Outputs.Add(outputDirDescNode);
                        lodGeneration.Outputs.Add(textureDirNode);

                        if (hasAnims)
                        {
                            IContentNode animDirNode = owner.CreateDirectory(animDir, "*.*", true);
                            lodGeneration.Outputs.Add(animDirNode);

                            // Compress the animations - group first into correct format
                            this.LoadParameters(param);

                            IContentNode groupDirNode = owner.CreateDirectory(animGroupingDir, "*.*");
                            ProcessBuilder group = new ProcessBuilder("RSG.Pipeline.Processor.Animation.Common.ClipDictionaryProcessor", processors, owner);
                            group.Inputs.Add(animDirNode);
                            group.Outputs.Add(groupDirNode);
                            group.Parameters.Add("Grouping", true);

                            object defaultSkel;
                            if (!this.Parameters.TryGetValue("DefaultSkel", out defaultSkel))
                            {
                                param.Log.ErrorCtx(LOG_CTX, "Missing Paramater 'DefaultSkel'");
                                syncDependencies = new List<IContentNode>();
                                resultantProcesses = processes;
                                return (false);
                            }

                            IContentNode animCompressionDirNode = owner.CreateDirectory(animDirCompressed, "*.*");
                            ProcessBuilder compression = new ProcessBuilder("RSG.Pipeline.Processor.Animation.Common.Compression", processors, owner);
                            compression.Inputs.Add(groupDirNode);
                            compression.AdditionalDependentContent.Add(owner.CreateFile((string)defaultSkel));
                            //compression.AdditionalDependentContent.Add(owner.CreateDirectory(compressionSettingsFolder, "*.*")); // Compression folder
                            compression.Outputs.Add(animCompressionDirNode);

                            processes.Add(group.ToProcess());
                            processes.Add(compression.ToProcess());
                        }

                        String processedDirDesc = lodDesc.GetShortDataLocation(processedDir);
                        String fragzip = String.Empty;
                        Tuple<String, RSG.Platform.Platform, Mode> key = new Tuple<String, RSG.Platform.Platform, Mode>(dictionaryName, PlatformUtils.RagebuilderPlatformToPlatform(lodDesc.Platform), lodDesc.ModeN);
                       
                        String texzip = SIO.Path.Combine(processedDirDesc, dictionaryName + ".itd.zip");
                        if (lodDesc.Split == Split.High)
                        {
                            fragzip = SIO.Path.Combine(processedDirDesc, dictionaryName + "_hi.ift.zip");
                            IContentNode fragNode = owner.CreateFile(fragzip);
                            List<IContentNode> files = new List<IContentNode>(new IContentNode[] { fragNode });
                            if (processedNodes.ContainsKey(key))
                                processedNodes[key].AddRange(files);
                            else
                                processedNodes.Add(key, files);

                            if (textureCollections.ContainsKey(texzip))
                                textureCollections[texzip].Add(textureDir);
                            else
                            {
                                textureCollections.Add(texzip, new List<String>() { textureDir });
                            }
                        }
                        else
                        {
                            fragzip = SIO.Path.Combine(processedDirDesc, dictionaryName + ".ift.zip");

                            // Only add our texture and anim dictionaries once for each split type.
                            
                            String animzip = SIO.Path.Combine(processedDirDesc, "va_" + dictionaryName + ".icd.zip");

                            IContentNode fragNode = owner.CreateFile(fragzip);
                            IContentNode texNode = owner.CreateFile(texzip);
                            
                            IContentNode processedDirNode = owner.CreateDirectory(processedDirDesc, "*.*");

                            AssetPackZipArchive animArchive = assetPackScheduleBuilder.AddZipArchive(animzip);

                            if (textureCollections.ContainsKey(texzip))
                                textureCollections[texzip].Add(textureDir);
                            else
                            {
                                textureCollections.Add(texzip, new List<String>() { textureDir });
                            }
                            animArchive.AddDirectory(animDirCompressed);
                            
                            List<IContentNode> files = new List<IContentNode>(){ fragNode, texNode };
                            if (hasAnims)
                            {
                                IContentNode animNode = owner.CreateFile(animzip);
                                files.Add(animNode);
                            }
                            if(processedNodes.ContainsKey(key))
                                processedNodes[key].AddRange(files);
                            else
                                processedNodes.Add(key, files);
                        }

                        

                        AssetPackZipArchive fragZipArchive = assetPackScheduleBuilder.AddZipArchive(fragzip);
                        fragZipArchive.AddDirectory(outputDirDesc);
                    }

                    // Handle the textures per mode
                    foreach (KeyValuePair<String, List<String>> kvp in textureCollections)
                    {
                        AssetPackZipArchive texZipArchive = assetPackScheduleBuilder.AddZipArchive(kvp.Key, DuplicateBehaviour.AcceptFirst);
                        foreach (String texDir in kvp.Value)
                        {
                            texZipArchive.AddDirectory(texDir);
                        }
                    }

                    assetPackScheduleBuilder.Save(assetPackSchedulePathname);

                    processes.Add(extraction.ToProcess());
                    processes.Add(lodGeneration.ToProcess());
                    processes.Add(assetPacker.ToProcess());
                }

                // Loop through all out inputs and add the processed dir nodes as inputs to our single and multiplayer rage processes
                foreach (IContentNode dictionary in newVehicleInputs)
                {
                    try
                    {
                        this.CreateRageProcesses(dictionary, processedNodes, Mode.Default, pbsp);
                        this.CreateRageProcesses(dictionary, processedNodes, Mode.Multiplayer, pbmp);
                    }
                    catch (Exception ex)
                    {
                        param.Log.ToolExceptionCtx(LOG_CTX, ex, "Exception creating the rage processes for {0}", dictionary.Name);
                    }
                }
            }

            // Add the single player process to our process list
            IProcess spResourcingProcess = pbsp.ToProcess();
            if (spResourcingProcess.Inputs.Count() == 0)
            {
                param.Log.ErrorCtx(LOG_CTX, "No inputs for rage process...");
                result = false;
            }
            processes.Add(spResourcingProcess);

            bool buildMPRPF = false;
            if (process.Parameters.ContainsKey(Constants.Vehicle_BuildMPRPF))
                buildMPRPF = (bool)process.Parameters[Constants.Vehicle_BuildMPRPF];                
            

            // If we're not in preview, add the multiplayer process too
            if (!param.Flags.HasFlag(EngineFlags.Preview) && buildMPRPF)
            {
                IProcess mpResourcingProcess = pbmp.ToProcess();
                if (mpResourcingProcess.Inputs.Count() == 0)
                {
                    param.Log.ErrorCtx(LOG_CTX, "No inputs for rage process...");
                    result = false;
                }
                processes.Add(mpResourcingProcess);
            }

            process.State = ProcessState.Discard;
            resultantProcesses = processes;
            syncDependencies = syncDepends;
            return(result);
        }

        /// <summary>
        /// Prepare a build for the processes; populating the XGE project with
        /// the tools, environments and tasks required.  Including setting up
        /// the task dependency chain.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="processes"></param>
        /// <param name="tools"></param>
        /// <param name="tasks"></param>
        /// <returns></returns>
        public override bool Prepare(IEngineParameters param,
            IEnumerable<IProcess> processes, out IEnumerable<XGE.ITool> tools,
            out IEnumerable<XGE.ITask> tasks)
        {
            // Assign method output.
            tools = new List<XGE.ITool>();
            tasks = new List<XGE.ITask>();

            return (true);
        }
        #endregion // IProcessor Interface Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dictionary"></param>
        /// <param name="processedNodes"></param>
        /// <param name="mode"></param>
        /// <param name="processBuilder"></param>
        private void CreateRageProcesses(IContentNode dictionary, Dictionary<Tuple<String, RSG.Platform.Platform, Mode>, List<IContentNode>> processedNodes, Mode mode, ProcessBuilder processBuilder)
        {
            String dictionaryName = Filename.GetBasename(SIO.Path.GetFileName((dictionary as RSG.Pipeline.Content.File).AbsolutePath));

            // Single player
            Dictionary<Tuple<String, RSG.Platform.Platform, Mode>, List<IContentNode>> spProcessedNodes = processedNodes.Where(pn => pn.Key.Item3 == mode && pn.Key.Item1 == dictionaryName).ToDictionary(item => item.Key, item => item.Value);
            foreach (KeyValuePair<Tuple<String, RSG.Platform.Platform, Mode>, List<IContentNode>> processedNode in spProcessedNodes)
            {
                Tuple<String, RSG.Platform.Platform, Mode> key = processedNode.Key;
                List<IContentNode> processedDirNodes = processedNode.Value;


                foreach (IContentNode node in processedDirNodes)
                {
                    if (key.Item2 != Platform.Platform.Independent)
                        node.Parameters.Add(Constants.Platform_Override, key.Item2);

                    processBuilder.Inputs.Add(node);
                }
            }
        }
        #endregion // Priate Methods

        #region Private Static Methods
        /// <summary>
        /// Convert a filename string to a target filename string; remapping it
        /// into the branch target's resource cache.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static String RemapFilenameToProcessedCache(IBranch branch, String filename)
        {
            String normalisedFilename = SIO.Path.GetFullPath(branch.Environment.Subst(filename));
            if (normalisedFilename.ToLower().StartsWith(branch.Export.ToLower()))
                normalisedFilename = normalisedFilename.ToLower().Replace(branch.Export.ToLower(), branch.Processed);
           
            return (normalisedFilename);
        }
        /// <summary>
        /// Return cache dir for the branch.  Could be a property if we kept the params as a member?
        /// </summary>
        /// <returns></returns>
        private static String GetCacheDir(IEngineParameters param)
        {
            return (SIO.Path.Combine(param.Branch.Project.Cache, "convert", param.Branch.Name, "vehicles"));
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Processor.Vehicle namespace
