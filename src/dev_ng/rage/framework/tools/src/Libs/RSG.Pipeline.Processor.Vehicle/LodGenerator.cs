﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;

using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Attributes;
using RSG.Pipeline.Content;
using RSG.Pipeline.Processor;
using RSG.Pipeline.Services;
using XGE = RSG.Interop.Incredibuild.XGE;

namespace RSG.Pipeline.Processor.Vehicle
{
    /// <summary>
    /// 
    /// </summary>
    [Export(typeof(IProcessor))]
    [ProcessorFlags(ProcessorFlags.Compatibility)]
    public class LodGenerator :
        ProcessorBase,
        IProcessor
    {
        #region Constants
        /// <summary>
        /// Processor description string.
        /// </summary>
        private static readonly String DESCRIPTION = "Vehicle Asset Processor";

        /// <summary>
        /// Processor log context.
        /// </summary>
        private static readonly String LOG_CTX = "Vehicle Lod Generator";
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public LodGenerator()
            : base(DESCRIPTION)
        {
        }
        #endregion // Constructor(s)

        #region IProcessor Interface Methods
        /// <summary>
        /// Prepare content; first pass of the on-disk content-tree.  Returns a
        /// set of IProcess objects after determining all required inputs, 
        /// outputs and whether the process' needs to change (this allows the 
        /// concept of 'preprocessors').
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="process">Process to prebuild.</param>
        /// <param name="processors">Processor collection to reference for new resultant proceses.</param>
        /// <param name="owner">Owning IContentTree.</param>
        /// <param name="syncDependencies">Dependencies to sync.</param>
        /// <param name="resultantProcesses">RawProcesses that will actually be built.</param>
        /// <returns>true iff successful; false otherwise</returns>
        /// The resultant processes are used by the pipeline engine after these
        /// calls.
        /// 
        public override bool Prebuild(IEngineParameters param,
            IProcess process, IProcessorCollection processors,
            IContentTree owner, out IEnumerable<IContentNode> syncDependencies,
            out IEnumerable<IProcess> resultantProcesses)
        {
            List<IProcess> processes = new List<IProcess>();

            process.State = ProcessState.Prebuilt;
            processes.Add(process);
            resultantProcesses = processes;
            syncDependencies = new List<IContentNode>();
            return (true);
        }

        /// <summary>
        /// Prepare a build for the processes; populating the XGE project with
        /// the tools, environments and tasks required.  Including setting up
        /// the task dependency chain.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="processes"></param>
        /// <param name="tools"></param>
        /// <param name="tasks"></param>
        /// <returns></returns>
        public override bool Prepare(IEngineParameters param,
            IEnumerable<IProcess> processes, out IEnumerable<XGE.ITool> tools,
            out IEnumerable<XGE.ITask> tasks)
        {
            bool result = true;
            this.LoadParameters(param);

            int taskCounter = 0;
            // Assign method output.
            List<XGE.ITool> prepTools = new List<XGE.ITool>();
            List<XGE.ITask> prepTasks = new List<XGE.ITask>();

            XGE.ITool tool = XGEFactory.GetProcessorTool(param.Log, param.Branch.Project.Config, XGEFactory.CommonToolType.LodGenerator, "LOD Generation");
            tool.Path = param.Branch.Environment.Subst(tool.Path);
            prepTools.Add(tool);

            foreach (IProcess process in processes)
            {
                try
                {
                    Directory inputDir = process.Inputs.First() as Directory;
                    Directory outpuDir = process.Outputs.First() as Directory;
                    File lodDescFile = process.AdditionalDependentContent.First() as File;

                    String taskName = String.Format("Fabricate LOD Hierarchies {0} [{1}]", taskCounter++, inputDir.AbsolutePath);
                    XGE.ITaskJob task = new XGE.TaskJob(taskName);
                    prepTasks.Add(task);

                    task.Caption = taskName;
                    task.Tool = tool;
                    task.Parameters = String.Format("--loddesc {0} --input {1} --output {2}",
                        lodDescFile.AbsolutePath, inputDir.AbsolutePath, outpuDir.AbsolutePath);
                    task.SourceFile = lodDescFile.AbsolutePath;
                    task.InputFiles.Add(inputDir.AbsolutePath);

                    // Define the XGE task for the IProcess.
                    process.Parameters.Add(Constants.ProcessXGE_TaskName, taskName.Replace(' ', '_'));
                    process.Parameters.Add(Constants.ProcessXGE_Task, task);
                     
                }
                catch (Exception ex)
                {
                    param.Log.ExceptionCtx(LOG_CTX, ex, "Error preparing LOD Fabrication processor.");
                    result = false;
                }
            }

            tools = prepTools;
            tasks = prepTasks;

            return (result);
        }
         #endregion // IProcessor Interface Methods
    }
}
