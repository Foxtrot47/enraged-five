﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Linq;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Pipeline.Content;
using RSG.Pipeline.Content.Algorithm;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Attributes;
using RSG.Pipeline.Services;
using RSG.Pipeline.Services.InstancePlacement;
using RSG.Pipeline.Services.Metadata;
using RSG.SourceControl.Perforce;
using SIO = System.IO;
using XGE = RSG.Interop.Incredibuild.XGE;


namespace RSG.Pipeline.Processor.Map.InstancePlacement
{
    /// <summary>
	/// Instance placement preprocessor; this processor determines all of the dependencies.
	/// </summary>
    [Export(typeof(IProcessor))]
    [ProcessorFlags(ProcessorFlags.Compatibility)]
	class PreProcess :
        ProcessorBase,
		IProcessor
    {
        #region Constants
        /// <summary>
        /// 
        /// </summary>
        private static readonly String DESCRIPTION = "Instance Placement Preprocessor";

        /// <summary>
        /// 
        /// </summary>
        private static readonly String LOG_CTX = "Instance Placement Processor";

        /// <summary>
        /// 
        /// </summary>
        private static readonly String INSTANCE_PLACEMENT_DIRECTORY = "instance_placement";

        /// <summary>
        /// 
        /// </summary>
        private static readonly List<String> ACCEPTED_IMAGE_FORMATS = new List<String>() {".bmp", ".tif", ".tga"};

        /// <summary>
        /// Path to our instance placement (metadata) XML filename.
        /// </summary>
        private static readonly String InstancePlacementXmlFilename = "$(assets)/metadata/terrain/InstancePlacement.xml";
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public PreProcess()
            : base(DESCRIPTION)
        {
        }
        #endregion // Constructor(s)

        #region IProcessor Interface Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <param name="process"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="syncDependencies"></param>
        /// <param name="resultantProcesses"></param>
        /// <returns></returns>
        public override bool Prebuild(IEngineParameters param,
            IProcess process, IProcessorCollection processors,
            IContentTree owner, out IEnumerable<IContentNode> syncDependencies,
            out IEnumerable<IProcess> resultantProcesses)
        {
            List<IProcess> processes = new List<IProcess>();
            List<IContentNode> syncDepends = new List<IContentNode>();

            bool result = true;

            this.LoadParameters(param);

            List<String> outputFiles = new List<String>();
            List<String> outputDirectories = new List<String>();
            HashSet<IContentNode> exportXMLDependencies = new HashSet<IContentNode>();
            List<IContentNode> metadataInputsToProcess = new List<IContentNode>();
            // Extreac and process inputs?

            ContentTreeHelper contentTreeHelper = new ContentTreeHelper(process.Owner);

            List<ProcessBuilder> instancePlacementProcessBuilders = null;
            List<ProcessBuilder> instancePlacementTextureProcessBuilders = null;
            ProcessBuilder instancePlacementBoundsManifestBuilder = null;
            
            List<MapMetadataSerialiserManifestDependency> additionalManifestDependencies = new List<MapMetadataSerialiserManifestDependency>();
            Dictionary<InstancePlacementType, Dictionary<String, List<IContentNode>>> textureInputs;
            String boundsManifestXML;

            // :(
            using (P4 p4 = param.Branch.Project.SCMConnect())
            {
                // Global textures need to be split into multiple parts and also use a bounds manifest.
                // Otherwise we just want to take the container specific textures and feed them into the Instance Placement Processor directly.
                if (process.Parameters.ContainsKey(Consts.InstancePlacement_GlobalTextureParameter) &&
                    (bool)process.Parameters[Consts.InstancePlacement_GlobalTextureParameter] == true)
                {
                    this.PrepareInstancePlacementBoundsManifest(param, process, processors, owner, contentTreeHelper, out boundsManifestXML, out instancePlacementBoundsManifestBuilder, p4);

                    this.PrepareInstancePlacementTextures(param, process, processors, owner, contentTreeHelper, boundsManifestXML, out instancePlacementTextureProcessBuilders, out textureInputs, p4);

                    processes.Add(instancePlacementBoundsManifestBuilder.ToProcess());

                    foreach (ProcessBuilder pb in instancePlacementTextureProcessBuilders)
                        processes.Add(pb.ToProcess());
                }
                else
                {
                    this.PrepareInstancePlacementTextures(param, process, processors, owner, contentTreeHelper, out textureInputs, p4);
                }


                List<IContentNode> metadataOutputsToProcess = new List<IContentNode>();
                if (this.CreateInstancePlacementProcessBuilder(param, process, processors,
                    owner, contentTreeHelper, metadataInputsToProcess, exportXMLDependencies, textureInputs,
                    out metadataOutputsToProcess, out instancePlacementProcessBuilders, p4))
                {
                    IProcessor instancePlacementProcessor = processors.FirstOrDefault(p => String.Equals("RSG.Pipeline.Processor.Map.InstancePlacement.InstancePlacement", p.Name));

                    List<IContentNode> outputs = new List<IContentNode>();
                    List<String> instancePlacementAdditions = new List<String>();

                    foreach (ProcessBuilder pb in instancePlacementProcessBuilders)
                    {
                        // Make sure we're only added manifest additions to instance placement processors.
                        if (!pb.ProcessorClassName.Equals(instancePlacementProcessor.Name))
                        {
                            processes.Add(pb.ToProcess());
                            continue;
                        }

                        String containerName = SIO.Path.GetFileNameWithoutExtension((pb.Inputs.FirstOrDefault() as Content.File).AbsolutePath);

                        String instancePlacementAdditionsPathname = SIO.Path.Combine(param.Branch.Project.Cache, param.Branch.Name, "manifest", process.Outputs.First().Name, String.Format("{0}_InstancePlacementAdditions.xml", containerName));

                        pb.Parameters.Add(Core.Constants.ManifestProcess_InstancePlacementProcessorAdditions, instancePlacementAdditionsPathname);
                        instancePlacementAdditions.Add(instancePlacementAdditionsPathname);

                        outputs.AddRange(pb.Outputs);
                        processes.Add(pb.ToProcess());
                    }

                    ProcessBuilder ragePb = new ProcessBuilder("RSG.Pipeline.Processor.Platform.Rage",
                        processors, owner);
                    ragePb.Inputs.AddRange(outputs);
                    ragePb.Outputs.Add(process.Outputs.First());

                    ragePb.Parameters.Add(Core.Constants.RageProcess_DelayedDirectoryEvaluationParameterName, true);
                    ragePb.Parameters.Add(Core.Constants.ManifestProcess_InstancePlacementProcessorAdditions, instancePlacementAdditions);

                    // Generate all Rage processes for all the processed content we have created
                    IProcess resourcingProcess = ragePb.ToProcess();
                    if (resourcingProcess.Inputs.Count() == 0)
                    {
                        param.Log.ErrorCtx(LOG_CTX, "No inputs for rage process...");
                        result = false;
                    }
                    processes.Add(resourcingProcess);
                }
            }

            process.State = ProcessState.Discard;
            resultantProcesses = processes;
            syncDependencies = syncDepends;

            return (result);
        }

        /// <summary>
        /// Prepare a build for the processes; populating the XGE project with
        /// the tools, environments and tasks required.  Including setting up
        /// the task dependency chain.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="processes"></param>
        /// <param name="tools"></param>
        /// <param name="tasks"></param>
        /// <returns></returns>
        public override bool Prepare(IEngineParameters param, IEnumerable<IProcess> processes,
            out IEnumerable<XGE.ITool> tools, out IEnumerable<XGE.ITask> tasks)
        {
            // Assign method output.
            tools = new List<XGE.ITool>();
            tasks = new List<XGE.ITask>();

            return (false);
        }
        #endregion // IProcessor Interface Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private bool PrepareInstancePlacement(IEngineParameters param,
            IProcess process, IProcessorCollection processors, IContentTree targetContentTree,
            ContentTreeHelper contentTreeHelper, HashSet<IContentNode> exportXMLDependencies,
            ProcessBuilder instanceMetadataProcessBuilder,
            List<MapMetadataSerialiserManifestDependency> additionalManifestDependencies,
            List<String> outputFiles, List<String> outputDirectories,
            ref Dictionary<InstancePlacementType, Dictionary<String, List<IContentNode>>> textureInputs,
            ref List<IContentNode> metadataInputsToProcess,
            ref List<ProcessBuilder> instancePlacementProcessBuilders)
        {
            bool result = true;
            bool intersectionEnabled = false;

            using (P4 p4 = param.Branch.Project.SCMConnect())
            {
                List<IContentNode> metadataOutputsToProcess = new List<IContentNode>();
                if (this.CreateInstancePlacementProcessBuilder(param, process, processors,
                    targetContentTree, contentTreeHelper, metadataInputsToProcess, exportXMLDependencies, textureInputs,
                    out metadataOutputsToProcess, out instancePlacementProcessBuilders, p4))
                {
#warning LPXO: May need to preload image files to get the correct manifest data.  At the moment I'm relying on the last InstancePlacementAdditions.xml generated by all processes to hold everything required.
                    //MapMetadataSerialiserManifestDependency manifestDependency = new MapMetadataSerialiserManifestDependency("X:/gta5/cache/instance_placement/dev_ng/ip_testbed_ip/instance_placement/ip_testbed_grass.imap", false);
                    //additionalManifestDependencies.Add(manifestDependency);
                }
            }
            return true;
        }

        /// <summary>
        /// Build up the texture processor requirements.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="process"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="contentTreeHelper"></param>
        /// <param name="boundsManifestFile"></param>
        /// <param name="instancePlacementTextureProcessBuilders"></param>
        /// <param name="textureInputs"></param>
        /// <param name="p4"></param>
        /// <returns></returns>
        private bool PrepareInstancePlacementTextures(IEngineParameters param, IProcess process, IProcessorCollection processors,
            IContentTree owner, ContentTreeHelper contentTreeHelper, string boundsManifestFile,
            out List<ProcessBuilder> instancePlacementTextureProcessBuilders,
            out Dictionary<InstancePlacementType, Dictionary<string, List<IContentNode>>> textureInputs,
            P4 p4)
        {
            List<object> enabledPlacementTypes = this.Parameters[Consts.InstancePlacement_EnabledPlacementTypes] as List<object>;
            List<InstancePlacementType> instancePlacementTypes = GetInstancePlacementTypeList(param, enabledPlacementTypes);

            textureInputs = new Dictionary<InstancePlacementType, Dictionary<String, List<IContentNode>>>();
            instancePlacementTextureProcessBuilders = new List<ProcessBuilder>();

            String textureMetadataPath = String.Empty;

            textureMetadataPath = param.Branch.Environment.Subst(InstancePlacementXmlFilename);

            TextureMetadata textureMetadata = TextureMetadata.Load(textureMetadataPath, param.Branch, param.Log);

            IProcessor instancePlacementTextureProcessor = processors.FirstOrDefault(p =>"RSG.Pipeline.Processor.Map.InstancePlacement.InstancePlacementTextureProcessor".Equals(p.Name));
            Debug.Assert(null != instancePlacementTextureProcessor, "Instance placement texture processor not defined.");

            bool hasGlobalDataTexture = false;
            bool hasGlobalTintTexture = false;

            String globalDataTexture = textureMetadata.GlobalDataTexture;
            String globalTintTexture = textureMetadata.GlobalTintTexture;

            // Handle the explicit data texture.
            if (!String.IsNullOrEmpty(globalDataTexture))
            {
                globalDataTexture = param.Branch.Environment.Subst(globalDataTexture);
                hasGlobalDataTexture = true;
            }

            // Handle the explicit tint texture.
            if (!String.IsNullOrEmpty(globalTintTexture))
            {
                hasGlobalTintTexture = true;
                globalTintTexture = param.Branch.Environment.Subst(globalTintTexture);
            }

            bool processedGlobalTextures = false;

            foreach (InstancePlacementType placementType in instancePlacementTypes)
            {
                List<IContentNode> inputTextureFileNodes = new List<IContentNode>();

                if (this.Parameters.ContainsKey(Consts.InstancePlacement_GlobalTexture))
                {
                    String globalTexturePathParam = (String)this.Parameters[Consts.InstancePlacement_GlobalTexture];
                    String globalTexturePath = param.Branch.Environment.Subst(globalTexturePathParam);
                    globalTexturePath = globalTexturePath.Replace("$(instance_category)", placementType.ToString());

                    String[] globalTextures = System.IO.Directory.GetFiles(SIO.Path.GetDirectoryName(globalTexturePath), SIO.Path.GetFileNameWithoutExtension(globalTexturePath) + "*", SIO.SearchOption.TopDirectoryOnly);
                    foreach (String globalTexture in globalTextures)
                    {
                        if (ACCEPTED_IMAGE_FORMATS.Contains(SIO.Path.GetExtension(globalTexture.ToLower())))
                        {
                            IContentNode textureInput = owner.CreateFile(globalTexture);
                            inputTextureFileNodes.Add(textureInput);
                        }
                    }
                }
                else
                {
                    param.Log.ErrorCtx(LOG_CTX, String.Format("No global texture specified."));
                    return false; //No global texture specified.
                }

                Dictionary<String, List<IContentNode>> textureEntries = new Dictionary<String, List<IContentNode>>();
                textureInputs.Add(placementType, textureEntries);

                foreach (IContentNode inputNode in process.Inputs)
                {
                    if (inputNode is Content.Directory)
                    {
                        // Collect a list of instance placement targets.
                        Content.Directory inputDirectoryNode = inputNode as Content.Directory;
                        List<IContentNode> content = new List<IContentNode>();

                        // If we built from a single .ipxml file then only build that one ipxml file.
                        if (param.Flags.HasFlag(EngineFlags.Selected) && inputNode.HasParameter(RSG.Pipeline.Core.Constants.Convert_Filenames))
                        {
                            IEnumerable<String> convertFilenames = inputNode.GetParameter(RSG.Pipeline.Core.Constants.Convert_Filenames, new List<String>());
                            foreach (String convertFilename in convertFilenames)
                            {
                                IContentNode convertNode = owner.CreateFile(convertFilename);
                                content.Add(convertNode);
                            }
                        }
                        else
                        {
                            content = inputDirectoryNode.EvaluateInputs().ToList();
                        }

                        String cacheDirectory = (String)this.Parameters[Consts.InstancePlacement_ProcessedMapCacheDirectory];

                        ProcessBuilder instancePlacementGlobalDataTextureProcessBuilder = null;
                        ProcessBuilder instancePlacementGlobalTintTextureProcessBuilder = null;

                        if (!processedGlobalTextures)
                        {
                            // Add our global data texture.
                            if (hasGlobalDataTexture)
                            {
                                instancePlacementGlobalDataTextureProcessBuilder = new ProcessBuilder(instancePlacementTextureProcessor, owner);
                                instancePlacementTextureProcessBuilders.Add(instancePlacementGlobalDataTextureProcessBuilder);

                                IContentNode globalDataTextureNode = owner.CreateFile(globalDataTexture);
                                if (!globalDataTextureNode.Parameters.ContainsKey(Consts.InstancePlacement_GlobalDataTexture))
                                    globalDataTextureNode.Parameters.Add(Consts.InstancePlacement_GlobalDataTexture, true);
                                instancePlacementGlobalDataTextureProcessBuilder.Inputs.Add(globalDataTextureNode);

                                if (!instancePlacementGlobalDataTextureProcessBuilder.Parameters.ContainsKey(Consts.InstancePlacement_ProcessedMapCacheDirectory))
                                    instancePlacementGlobalDataTextureProcessBuilder.Parameters.Add(Consts.InstancePlacement_ProcessedMapCacheDirectory, cacheDirectory);

                                instancePlacementGlobalDataTextureProcessBuilder.Inputs.Add(owner.CreateFile(boundsManifestFile));
                            }

                            // Add our global tint texture.
                            if (hasGlobalTintTexture)
                            {
                                instancePlacementGlobalTintTextureProcessBuilder = new ProcessBuilder(instancePlacementTextureProcessor, owner);
                                instancePlacementTextureProcessBuilders.Add(instancePlacementGlobalTintTextureProcessBuilder);

                                IContentNode globalTintTextureNode = owner.CreateFile(globalTintTexture);
                                if (!globalTintTextureNode.Parameters.ContainsKey(Consts.InstancePlacement_GlobalTintTexture))
                                    globalTintTextureNode.Parameters.Add(Consts.InstancePlacement_GlobalTintTexture, true);
                                instancePlacementGlobalTintTextureProcessBuilder.Inputs.Add(globalTintTextureNode);

                                if (!instancePlacementGlobalTintTextureProcessBuilder.Parameters.ContainsKey(Consts.InstancePlacement_ProcessedMapCacheDirectory))
                                    instancePlacementGlobalTintTextureProcessBuilder.Parameters.Add(Consts.InstancePlacement_ProcessedMapCacheDirectory, cacheDirectory);

                                instancePlacementGlobalTintTextureProcessBuilder.Inputs.Add(owner.CreateFile(boundsManifestFile));
                            }
                        }

                        // Region specific global textures to be generated.
                        foreach (File contentNode in content.OfType<File>())
                        {
                            String container = contentNode.Basename;
                            String outputCacheDirectory = GetSourceZipAssetCacheDir(param, contentNode);
                            String processedBounds = GenerateProcessedCollisionFilename(p4, param.Branch, contentTreeHelper, contentNode);

                            // Add a reference to this container.
                            if (!textureEntries.ContainsKey(container))
                                textureEntries.Add(container, new List<IContentNode>());

                            // We only create the global data texture once per type.
                            if (hasGlobalDataTexture && !processedGlobalTextures)
                            {
                                String globalDataTextureOutput = SIO.Path.Combine(outputCacheDirectory, SIO.Path.GetFileNameWithoutExtension(globalDataTexture) + ".bmp");
                                IContentNode globalDataTextureOutputNode = owner.CreateFile(globalDataTextureOutput);
                                globalDataTextureOutputNode.SetParameter(Consts.InstancePlacement_GlobalDataTexture, true);
                                globalDataTextureOutputNode.SetParameter(Consts.InstancePlacement_ProcessedCollision, processedBounds);
                                globalDataTextureOutputNode.SetParameter(Consts.InstancePlacement_PlacementType, InstancePlacementType.Unknown);
                                instancePlacementGlobalDataTextureProcessBuilder.Inputs.Add(owner.CreateFile(processedBounds));
                                instancePlacementGlobalDataTextureProcessBuilder.Outputs.Add(globalDataTextureOutputNode);
                                textureEntries[container].Add(globalDataTextureOutputNode);
                            }
                            // Next time around just add the data texture to our entries list, we already created a processor for it from the last type.
                            else if (hasGlobalDataTexture && processedGlobalTextures)
                            {
                                String globalDataTextureOutput = SIO.Path.Combine(outputCacheDirectory, SIO.Path.GetFileNameWithoutExtension(globalDataTexture) + ".bmp");
                                IContentNode globalDataTextureOutputNode = owner.CreateFile(globalDataTextureOutput);
                                textureEntries[container].Add(globalDataTextureOutputNode);
                            }

                            // We only create the global tint texture once per type.
                            if (hasGlobalTintTexture && !processedGlobalTextures)
                            {
                                String globalTintTextureOutput = SIO.Path.Combine(outputCacheDirectory, SIO.Path.GetFileNameWithoutExtension(globalTintTexture) + ".bmp");
                                IContentNode globalTintTextureOutputNode = owner.CreateFile(globalTintTextureOutput);
                                globalTintTextureOutputNode.SetParameter(Consts.InstancePlacement_GlobalTintTexture, true);
                                globalTintTextureOutputNode.SetParameter(Consts.InstancePlacement_ProcessedCollision, processedBounds);
                                globalTintTextureOutputNode.SetParameter(Consts.InstancePlacement_PlacementType, InstancePlacementType.Unknown);
                                instancePlacementGlobalTintTextureProcessBuilder.Inputs.Add(owner.CreateFile(processedBounds));
                                instancePlacementGlobalTintTextureProcessBuilder.Outputs.Add(globalTintTextureOutputNode);
                                textureEntries[container].Add(globalTintTextureOutputNode);
                            }
                            // Next time around just add the tint texture to our entries list, we already created a processor for it from the last type.
                            else if (hasGlobalTintTexture && processedGlobalTextures)
                            {
                                String globalTintTextureOutput = SIO.Path.Combine(outputCacheDirectory, SIO.Path.GetFileNameWithoutExtension(globalTintTexture) + ".bmp");
                                IContentNode globalTintTextureOutputNode = owner.CreateFile(globalTintTextureOutput);
                                textureEntries[container].Add(globalTintTextureOutputNode);
                            }
                        }

                        processedGlobalTextures = true;

                        foreach (IContentNode textureInput in inputTextureFileNodes)
                        {
                            ProcessBuilder instancePlacementTextureProcessBuilder = new ProcessBuilder(instancePlacementTextureProcessor, owner);
                            instancePlacementTextureProcessBuilders.Add(instancePlacementTextureProcessBuilder);

                            if (!instancePlacementTextureProcessBuilder.Parameters.ContainsKey(Consts.InstancePlacement_ProcessedMapCacheDirectory))
                                instancePlacementTextureProcessBuilder.Parameters.Add(Consts.InstancePlacement_ProcessedMapCacheDirectory, cacheDirectory);

                            // Add our input as the base texture.
                            instancePlacementTextureProcessBuilder.Inputs.Add(textureInput);
                            instancePlacementTextureProcessBuilder.Inputs.Add(owner.CreateFile(boundsManifestFile));

                            // Region specific textures to be generated.
                            foreach (File fileNode in content.OfType<File>())
                            {
                                // Add an input to our collision files. If any of them change, we'll need to rebuild the textures.
                                String processedBounds = GenerateProcessedCollisionFilename(p4, param.Branch, contentTreeHelper, fileNode);
                                // probably not needed anymore because we check that in the GenerateProcessedCollisionFilename() call
                                //if (!SIO.File.Exists(processedBounds))
                                //{

                                //    param.Log.ErrorCtx(LOG_CTX, String.Format("Processed bounds do not exist for {0}.  Aborting instance placement.", processedBounds));
                                //    return false;
                                //}
                                instancePlacementTextureProcessBuilder.Inputs.Add(owner.CreateFile(processedBounds));

                                String container = fileNode.Basename;
                                String outputCacheDirectory = GetSourceZipAssetCacheDir(param, fileNode);
                                String outputTexture = SIO.Path.Combine(outputCacheDirectory, (textureInput as Content.File).Basename + ".bmp");
                                IContentNode outputTextureNode = owner.CreateFile(outputTexture);

                                // Add a reference to this container.
                                if (!textureEntries.ContainsKey(container))
                                    textureEntries.Add(container, new List<IContentNode>());

                                // Keep track of which textures are assigned to what container as well as build up the process output.
                                textureEntries[container].Add(outputTextureNode);
                                outputTextureNode.SetParameter(Consts.InstancePlacement_ProcessedCollision, processedBounds);
                                outputTextureNode.SetParameter(Consts.InstancePlacement_PlacementType, placementType);
                                instancePlacementTextureProcessBuilder.Outputs.Add(outputTextureNode);

                                instancePlacementTextureProcessBuilder.AdditionalDependentContent.Add(owner.CreateFile(textureMetadataPath));
                            }
                        }
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <param name="process"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="contentTreeHelper"></param>
        /// <param name="textureInputs"></param>
        /// <param name="p4"></param>
        /// <returns></returns>
        private bool PrepareInstancePlacementTextures(IEngineParameters param, IProcess process, IProcessorCollection processors,
            IContentTree owner, ContentTreeHelper contentTreeHelper, out Dictionary<InstancePlacementType, Dictionary<string, List<IContentNode>>> textureInputs,
            P4 p4)
        {
            List<object> enabledPlacementTypes = this.GetParameter(Consts.InstancePlacement_EnabledPlacementTypes, new List<object>());
            List<InstancePlacementType> instancePlacementTypes = GetInstancePlacementTypeList(param, enabledPlacementTypes);

            textureInputs = new Dictionary<InstancePlacementType, Dictionary<String, List<IContentNode>>>();

            String textureMetadataPath = param.Branch.Environment.Subst(InstancePlacementXmlFilename);

            TextureMetadata textureMetadata = TextureMetadata.Load(textureMetadataPath, param.Branch, param.Log);

            bool globalTextures = GetParameter(Consts.InstancePlacement_GlobalTextureParameter, false);

            foreach (InstancePlacementType placementType in instancePlacementTypes)
            {
                List<IContentNode> inputTextureFileNodes = new List<IContentNode>();
                String regionTexturePath = "";

                if (param.Branch.Project.IsDLC && this.Parameters.ContainsKey(Consts.InstancePlacement_GlobalTexture_DLC))
                {
                    String regionTexturePathParam = (String)this.Parameters[Consts.InstancePlacement_GlobalTexture_DLC];
                    regionTexturePath = param.Branch.Environment.Subst(regionTexturePathParam);
                    regionTexturePath = regionTexturePath.Replace("$(instance_category)", placementType.ToString());
                }
                else
                {
                    if (this.Parameters.ContainsKey(Consts.InstancePlacement_GlobalTexture))
                    {
                        String regionTexturePathParam = (String)this.Parameters[Consts.InstancePlacement_GlobalTexture];
                        regionTexturePath = param.Branch.Environment.Subst(regionTexturePathParam);
                        regionTexturePath = regionTexturePath.Replace("$(instance_category)", placementType.ToString());
                    }
                    else
                    {
                        param.Log.ErrorCtx(LOG_CTX, String.Format("No instance placement texture_path specified in the content tree edge."));
                        return false; //No region texture path specified.
                    }
                }

                Dictionary<String, List<IContentNode>> textureEntries = new Dictionary<String, List<IContentNode>>();
                textureInputs.Add(placementType, textureEntries);

                foreach (Directory inputDirectoryNode in process.Inputs.OfType<Directory>())
                {
                    // Collect a list of instance placement targets.
                    List<IContentNode> content = inputDirectoryNode.EvaluateInputs().ToList();

                    // (not used)
                    String cacheDirectory = this.GetParameter(Consts.InstancePlacement_ProcessedMapCacheDirectory, "");

                    foreach (File inputContentNode in content.OfType<File>())
                    {
                        String container = inputContentNode.Basename;
                        String processedBounds = GenerateProcessedCollisionFilename(p4, param.Branch, contentTreeHelper, inputContentNode);
                        
                        // Cache the processed bounds filename, we use the tree to retrieve this later.
                        owner.CreateFile(processedBounds);

                        // Gather just the textures that are associated with this container.
                        String[] regionTextures = System.IO.Directory.GetFiles(regionTexturePath, container + "*", SIO.SearchOption.TopDirectoryOnly);

                        List<IContentNode> containerInputTextures = new List<IContentNode>();

                        foreach (String regionTexture in regionTextures)
                        {
                            if (ACCEPTED_IMAGE_FORMATS.Contains(SIO.Path.GetExtension(regionTexture.ToLower())))
                            {
                                IContentNode textureInput = owner.CreateFile(regionTexture);
                                containerInputTextures.Add(textureInput);
                            }
                        }

                        foreach (IContentNode outputTextureNode in containerInputTextures)
                        {
                            // Add a reference to this container.
                            if (!textureEntries.ContainsKey(container))
                                textureEntries.Add(container, new List<IContentNode>());

                            // Keep track of which textures are assigned to what container as well as build up the process output.
                            textureEntries[container].Add(outputTextureNode);
                            outputTextureNode.Parameters.Add(Consts.InstancePlacement_ProcessedCollision, processedBounds);
                            outputTextureNode.Parameters.Add(Consts.InstancePlacement_PlacementType, placementType);
                            outputTextureNode.Parameters.Add(Consts.InstancePlacement_GlobalTextureParameter, globalTextures);

                            inputTextureFileNodes.Add(outputTextureNode);
                        }
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Build up the bounds processor requirements.
        /// This is used to speed up the texture splitting so we only need to load the input collision once
        /// instead of once per texture process. Also adds better dependency checking so we don't need to keep
        /// processing bounds everytime just a single texture changes.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="process"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="contentTreeHelper"></param>
        /// <param name="boundsManifestXML"></param>
        /// <param name="instancePlacementBoundsManifestBuilder"></param>
        /// <param name="p4"></param>
        private void PrepareInstancePlacementBoundsManifest(IEngineParameters param, IProcess process, IProcessorCollection processors,
            IContentTree owner, ContentTreeHelper contentTreeHelper,
            out string boundsManifestXML,
            out ProcessBuilder instancePlacementBoundsManifestBuilder, P4 p4)
        {
            IProcessor instancePlacementBoundsManifestProcessor = processors.GetProcessor(
                "RSG.Pipeline.Processor.Map.InstancePlacement.InstancePlacementBoundsManifestProcessor");
            Debug.Assert(null != instancePlacementBoundsManifestProcessor, "Instance placement bounds manifest processor not defined.");

            instancePlacementBoundsManifestBuilder = new ProcessBuilder(instancePlacementBoundsManifestProcessor, owner);

            String textureMetadataPath = param.Branch.Environment.Subst(InstancePlacementXmlFilename);

            instancePlacementBoundsManifestBuilder.Inputs.Add(owner.CreateFile(textureMetadataPath));

            foreach (IContentNode inputNode in process.Inputs)
            {
                if (inputNode is Content.Directory)
                {
                    // Collect a list of instance placement targets.
                    Content.Directory inputDirectoryNode = inputNode as Content.Directory;
                    List<IContentNode> content = new List<IContentNode>();

                    // If we built from a single .ipxml file then only build that one ipxml file.
                    if (param.Flags.HasFlag(EngineFlags.Selected) && inputNode.HasParameter(RSG.Pipeline.Core.Constants.Convert_Filenames))
                    {
                        IEnumerable<String> convertFilenames = inputNode.GetParameter(RSG.Pipeline.Core.Constants.Convert_Filenames, new List<String>());
                        foreach (String convertFilename in convertFilenames)
                        {
                            IContentNode convertNode = owner.CreateFile(convertFilename);
                            content.Add(convertNode);
                        }
                    }
                    else
                    {
                        content = inputDirectoryNode.EvaluateInputs().ToList();
                    }

                    foreach (File contentNode in content.OfType<File>())
                    {
                        String processedBounds = GenerateProcessedCollisionFilename(p4, param.Branch, contentTreeHelper, contentNode);
                        instancePlacementBoundsManifestBuilder.Inputs.Add(owner.CreateFile(processedBounds));
                    }
                }
            }

            Content.Asset outputAsset = (Asset)process.Outputs.First();

            String processedDestination = GetProcessedZipAssetCacheDir(param, outputAsset);
            boundsManifestXML = SIO.Path.Combine(processedDestination, "instance_placement_bounds_manifest.xml");

            String cacheDirectory = (String)this.Parameters[Consts.InstancePlacement_ProcessedMapCacheDirectory];
            if (!instancePlacementBoundsManifestBuilder.Parameters.ContainsKey(Consts.InstancePlacement_ProcessedMapCacheDirectory))
                instancePlacementBoundsManifestBuilder.Parameters.Add(Consts.InstancePlacement_ProcessedMapCacheDirectory, cacheDirectory);

            instancePlacementBoundsManifestBuilder.Outputs.Add(owner.CreateFile(boundsManifestXML));
        }

        /// <summary>
        /// Takes the object list of placement types and casts them into a list of InstancePlacementType enums.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="enabledPlacementTypes"></param>
        /// <returns></returns>
        private List<InstancePlacementType> GetInstancePlacementTypeList(IEngineParameters param, List<object> enabledPlacementTypes)
        {
            List<InstancePlacementType> instancePlacementTypes = new List<InstancePlacementType>();

            foreach (object enabledPlacementTypeObj in enabledPlacementTypes)
            {
                String enabledPlacementType = (String)enabledPlacementTypeObj;
                InstancePlacementType placementType = InstancePlacementType.Unknown;

                if (!Enum.TryParse<InstancePlacementType>(enabledPlacementType, out placementType))
                {
                    param.Log.ErrorCtx(LOG_CTX, String.Format("Unable to resolve instance placement type {0}.", enabledPlacementType));
                    continue;
                }

                instancePlacementTypes.Add(placementType);
            }

            return instancePlacementTypes;
        }

        /// <summary>
        /// Create the instance placement process.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="process"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="metadataProcess"></param>
        /// <returns></returns>
        private bool CreateInstancePlacementProcessBuilder(IEngineParameters param,
            IProcess process, IProcessorCollection processors, IContentTree owner, ContentTreeHelper contentTreeHelper,
            List<IContentNode> metadataInputs, HashSet<IContentNode> exportXMLDependencies, Dictionary<InstancePlacementType, Dictionary<String, List<IContentNode>>> textureInputs,
            out List<IContentNode> metadataOutputs, out List<ProcessBuilder> instancePlacementProcessBuilders, P4 p4)
        {
            instancePlacementProcessBuilders = null;
            metadataOutputs = null;

            Content.Asset outputAsset = (Asset)process.Outputs.First();

            // DHM - why is this required?  The IProcess should capture anything you need here.
            IEnumerable<IProcess> processes = null;
            DependencyWalker.OutputsFrom(outputAsset, out processes);
            if (!processes.Any())
                return false; //No processes.

            IProcessor instancePlacementProcessor = processors.GetProcessor(
                "RSG.Pipeline.Processor.Map.InstancePlacement.InstancePlacement");
            Debug.Assert(null != instancePlacementProcessor, "Instance placement processor not defined.");

            String processedDestination = GetProcessedZipAssetCacheDir(param, outputAsset);
            String placementDirectory = System.IO.Path.Combine(processedDestination, INSTANCE_PLACEMENT_DIRECTORY);

            IContentNode instancePlacementDirNode = owner.CreateDirectory(placementDirectory);
            List<object> enabledPlacementTypes = this.Parameters[Consts.InstancePlacement_EnabledPlacementTypes] as List<object>;
            List<InstancePlacementType> instancePlacementTypes = GetInstancePlacementTypeList(param, enabledPlacementTypes);

            foreach (IContentNode inputNode in process.Inputs)
            {
                // Content we are going to build; independent of input content (directories or files).
                List<Content.File> content = new List<Content.File>();
                if (inputNode is Content.File)
                {
                    content.Add((Content.File)inputNode);
                }
                else
                {
                    // Collect a list of instance placement targets.
                    Content.Directory inputDirectoryNode = inputNode as Content.Directory;

                    // If we built from a single .ipxml file then only build that one ipxml file.
                    if (param.Flags.HasFlag(EngineFlags.Selected) && inputNode.HasParameter(RSG.Pipeline.Core.Constants.Convert_Filenames))
                    {
                        IEnumerable<String> convertFilenames = inputNode.GetParameter(RSG.Pipeline.Core.Constants.Convert_Filenames, new List<String>());
                        foreach (String convertFilename in convertFilenames)
                        {
                            Content.File convertNode = (Content.File)owner.CreateFile(convertFilename);
                            content.Add(convertNode);
                        }
                    }
                    else
                    {
                        content = inputDirectoryNode.EvaluateInputs().OfType<Content.File>().ToList();
                    }
                }

                foreach (Content.File contentNode in content)
                {
                    metadataOutputs = new List<IContentNode>();
                    List<IContentNode> inputMetadataNodes = new List<IContentNode>();
                    String sceneXML = String.Empty;

                    if (String.Equals(contentNode.Extension, ".ipxml"))
                    {
                        sceneXML = GetSceneXmlPathFromIpxml(p4, contentNode, contentTreeHelper, param);

                        String exclusionFile = SIO.Path.Combine(placementDirectory, String.Format("{0}.xml", contentNode.Name));
                        bool sceneXmlExists = SIO.File.Exists(sceneXML);
                        if (sceneXmlExists)
                        {
                            inputMetadataNodes.Add(owner.CreateFile(sceneXML));
                            metadataOutputs.Add(owner.CreateFile(exclusionFile));
                        }
                        else
                        {
                            param.Log.ErrorCtx(LOG_CTX, "Could not find scene XML file required for instance placement processing: {0}", sceneXML);
                            return false;
                        }
                    }
                    else
                    {
                        param.Log.WarningCtx(LOG_CTX, "Found unexpected file when walking IPXML files: {0}", contentNode.Name);
                        continue;
                    }

                    if (!inputMetadataNodes.Any())
                    {
                        //No transformation will take place.
                        metadataOutputs = metadataInputs;
                    }

                    List<IContentNode> textureEntries = new List<IContentNode>();
                    String container = contentNode.Basename;

                    foreach (InstancePlacementType placementType in instancePlacementTypes)
                    {
                        textureEntries.AddRange(textureInputs[placementType][container]);
                    }

                    if (textureEntries.Any())
                    {
                        ProcessBuilder instancePlacementProcessBuilder = new ProcessBuilder(instancePlacementProcessor, owner);
                        if (instancePlacementProcessBuilders == null)
                            instancePlacementProcessBuilders = new List<ProcessBuilder>();

                        instancePlacementProcessBuilders.Add(instancePlacementProcessBuilder);

                        if (inputMetadataNodes.Any())
                        {
                            instancePlacementProcessBuilder.Inputs.AddRange(inputMetadataNodes.ToList());
                            //Add the scene XML file excluded objects in it.
                            instancePlacementProcessBuilder.Outputs.AddRange(metadataOutputs.ToList());
                        }

                        instancePlacementProcessBuilder.Inputs.AddRange(textureEntries);
                        instancePlacementProcessBuilder.Outputs.Add(instancePlacementDirNode);

                        String cacheDirectory = (String)this.Parameters[Consts.InstancePlacement_ProcessedMapCacheDirectory];
                        if (!instancePlacementProcessBuilder.Parameters.ContainsKey(Consts.InstancePlacement_ProcessedMapCacheDirectory))
                            instancePlacementProcessBuilder.Parameters.Add(Consts.InstancePlacement_ProcessedMapCacheDirectory, cacheDirectory);

                        String sourceCacheDirectory = (String)this.Parameters[Consts.InstancePlacement_SourceMapCacheDirectory];
                        if (!instancePlacementProcessBuilder.Parameters.ContainsKey(Consts.InstancePlacement_SourceMapCacheDirectory))
                            instancePlacementProcessBuilder.Parameters.Add(Consts.InstancePlacement_SourceMapCacheDirectory, sourceCacheDirectory);

                        instancePlacementProcessBuilder.Parameters.Add(Consts.MetadataSerialiser_InputToProcessParameterPrefix, inputMetadataNodes);
                        instancePlacementProcessBuilder.Parameters.Add(Consts.InstancePlacement_EnabledPlacementTypes, this.Parameters[Consts.InstancePlacement_EnabledPlacementTypes]);

                        /*
                        // Add our overlapping containers so we can extract their collision.
                        if (checkContainerOverlaps)
                        {
                            instancePlacementProcessBuilder.Parameters.Add(Consts.InstancePlacement_OverlappingContainerDependencies, overlappedOutputPaths);
                        }
                        */

                        //Sets all SceneXML dependencies for this process.
                        instancePlacementProcessBuilder.Parameters.Add(Consts.InstancePlacement_AdditionalSceneXMLDependencies, exportXMLDependencies);
                    }
                }
            }            

            if (instancePlacementProcessBuilders != null)
                return true;

            return false;
        }
        
        /// <summary>
        /// Return processed map cache directory for an asset.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="asset"></param>
        /// <returns></returns>
        private String GetProcessedZipAssetCacheDir(IEngineParameters param, Content.File asset)
        {
            Debug.Assert(this.Parameters.ContainsKey(Consts.InstancePlacement_ProcessedMapCacheDirectory),
                "Cache directory not found in parameters!  Internal error.");
            if (!this.Parameters.ContainsKey(Consts.InstancePlacement_ProcessedMapCacheDirectory))
            {
                param.Log.ErrorCtx(LOG_CTX, "Cache directory not found in parameters!  Internal error.");
                throw (new NotSupportedException("Cache directory not found in parameters!  Internal error."));
            }

            String cacheDirectory = (String)this.Parameters[Consts.InstancePlacement_ProcessedMapCacheDirectory];
            String cacheRoot = String.Empty;
            if (param.Branch.Project.IsDLC)
                cacheRoot = SIO.Path.GetFullPath(param.Branch.Environment.Subst(cacheDirectory));
            else
                cacheRoot = SIO.Path.GetFullPath(param.CoreBranch.Environment.Subst(cacheDirectory));

            return (SIO.Path.Combine(cacheRoot, asset.Name));
        }

        /// <summary>
        /// Return source map cache directory for an asset.
        /// </summary>
        /// <param name="asset"></param>
        /// <returns></returns>
        private String GetSourceZipAssetCacheDir(IEngineParameters param, Content.File asset)
        {
            Debug.Assert(this.Parameters.ContainsKey(Consts.InstancePlacement_SourceMapCacheDirectory),
                "Cache directory not found in parameters!  Internal error.");
            if (!this.Parameters.ContainsKey(Consts.InstancePlacement_SourceMapCacheDirectory))
            {
                param.Log.ErrorCtx(LOG_CTX, "Cache directory not found in parameters!  Internal error.");
                throw (new NotSupportedException("Cache directory not found in parameters!  Internal error."));
            }

            String cacheDirectory = (String)this.Parameters[Consts.InstancePlacement_SourceMapCacheDirectory];
            String cacheRoot = String.Empty;
            if (asset.AbsolutePath.StartsWith(param.Branch.Export))
                cacheRoot = SIO.Path.GetFullPath(param.Branch.Environment.Subst(cacheDirectory));
            else
                cacheRoot = SIO.Path.GetFullPath(param.CoreBranch.Environment.Subst(cacheDirectory));

            return (SIO.Path.Combine(cacheRoot, asset.Name));
        }



        //private string 
        #endregion // Private Methods

        #region Private Static Helpers
        /// <summary>
        /// Gets the processed collision file for an IPxml
        /// Falls back to Core processed collision if we're in a DLC context and the associated collision cannot be found
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="branch"></param>
        /// <param name="cth"></param>
        /// <param name="inputContentNode"></param>
        /// <returns></returns>
        private static string GenerateProcessedCollisionFilename(P4 p4, IBranch branch, ContentTreeHelper cth, File inputContentNode)
        {
            // get a local (in DLC for DLC, core for core) sceneXml
            string localSceneXml = InferSceneXmlPath(inputContentNode);

            // get the IContentNode for that local sceneXml
            File sceneContentNode = cth.GetAllMapSceneXmlNodes()
                .OfType<File>()
                .FirstOrDefault(x => localSceneXml.Equals(x.AbsolutePath, StringComparison.OrdinalIgnoreCase));

            IContentNode exportZip = cth.GetExportZipNodeFromSceneXmlNode(sceneContentNode);

            File processedZip = cth.GetProcessedZipNodeFromExportZipNode(exportZip) as File;
            string processedPath = SIO.Path.GetDirectoryName(processedZip.AbsolutePath);
            string procFile = SIO.Path.GetFileNameWithoutExtension(processedZip.AbsolutePath);

            // string replace because the processed nodes in ContentTree do not currently point to _collision.zip files
            string collisionPath = SIO.Path.Combine(processedPath, procFile + "_collision.zip");

            if (branch.Project.IsDLC)
            {
                // check if we have a "DLC" collision file that exists
                if (SIO.File.Exists(collisionPath))
                    return collisionPath;

                // Check if we have a PERFORCE file existing
                // this will happen only once on the builder, because next time it'll hit a File.Exists() == true (hopefully)
                // sorry in advance RSGEDI IT
                FileState[] filestates = FileState.Create(p4, new[] { collisionPath + "#head" });
                FileState fs = filestates.FirstOrDefault();

                if (fs != null)
                {
                    if (fs.HeadAction != FileAction.Delete &&
                        fs.HeadAction != FileAction.MoveAndDelete &&
                        fs.HeadAction != FileAction.Purge &&
                        fs.HeadAction != FileAction.Unknown)
                    {
                        return collisionPath;
                    }
                }

                // otherwise, fallback to core's collision for that one.
                processedZip = cth.GetCoreBranchEquivalent(branch, processedZip) as File;
                processedPath = processedZip.AbsolutePath;
                procFile = SIO.Path.GetFileNameWithoutExtension(processedPath); // ex: name_01
                collisionPath = processedPath.Replace(procFile, procFile + "_collision", StringComparison.OrdinalIgnoreCase);
            }

            return collisionPath;
        }

        /// <summary>
        /// Gets the associated SceneXml from an IPxml file
        /// Falls back to Core SceneXml if we're in a DLC context, and the SceneXml cannot be found.
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="ipxmlFileNode"></param>
        /// <param name="contentTreeHelper"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        public static string GetSceneXmlPathFromIpxml(P4 p4, File ipxmlFileNode, ContentTreeHelper contentTreeHelper, IEngineParameters param)
        {
            string sceneXmlPath = InferSceneXmlPath(ipxmlFileNode);

            if (param.Branch.Project.IsDLC)
            {
                // if file does not exist locally in the DLC
                if (SIO.File.Exists(sceneXmlPath))
                    return sceneXmlPath;

                FileState[] filestates = FileState.Create(p4, new[] { sceneXmlPath + "#head" });
                FileState fs = filestates.FirstOrDefault();

                if (fs != null)
                {
                    if (fs.HeadAction != FileAction.Delete &&
                        fs.HeadAction != FileAction.MoveAndDelete &&
                        fs.HeadAction != FileAction.Purge &&
                        fs.HeadAction != FileAction.Unknown)
                    {
                        return sceneXmlPath;
                    }
                }

                // fall back to core
                ipxmlFileNode = contentTreeHelper.GetCoreBranchEquivalent(param.Branch, ipxmlFileNode) as File;
                // Generate our paired sceneXML path name and validate it's existence.
                return InferSceneXmlPath(ipxmlFileNode);
            }

            return sceneXmlPath;
        }

        /// <summary>
        /// Quick utility method to remap path from an ipxml file to a sceneXml just above
        /// </summary>
        /// <param name="contentNode"></param>
        /// <returns></returns>
        private static string InferSceneXmlPath(File contentNode)
        {
            string parentDir = SIO.Path.GetDirectoryName(SIO.Directory.GetParent(contentNode.AbsolutePath).FullName);
            string sceneXmlFilename = SIO.Path.ChangeExtension(SIO.Path.GetFileName(contentNode.AbsolutePath), "xml");
            return SIO.Path.Combine(parentDir, sceneXmlFilename);
        }
        #endregion
    }

} // RSG.Pipeline.Processor.Map.InstancePlacement namespace
