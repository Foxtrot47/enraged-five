﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Pipeline.Processor.Map.InstancePlacement
{
    /// <summary>
    /// Instance placement processor(s) constants.
    /// </summary>
    public class Consts
    {
        /// <summary>
        /// Parameter for specifying any additional manifest dependencies to the metadata serialiser.
        /// </summary>
        internal static readonly String InstancePlacement_AdditionalManifestDependenciesParameter =
            "additional_manifest_dependencies";

        /// <summary>
        /// List of all of the enabled instance placement types in the pipeline.
        /// </summary>
        internal static readonly String InstancePlacement_EnabledPlacementTypes =
            "Enabled Instance Placement Types";

        /// <summary>
        /// Specifies a global texture to use in the pipeline for placing instances in the core game.
        /// </summary>
        internal static readonly String InstancePlacement_GlobalTexture =
            "Global Instance Placement Texture";

        /// <summary>
        /// Specifies a global texture to use in the pipeline for placing instances in DLC.
        /// </summary>
        internal static readonly String InstancePlacement_GlobalTexture_DLC =
            "Global Instance Placement Texture DLC";

        /// <summary>
        /// Circumvents the content system and instead allows the pipeline to drop instances on
        /// all map_container nodes.
        /// </summary>
        internal static readonly String InstancePlacement_PlaceOnMapContainers =
            "Place Instances on All Map Containers";

        /// <summary>
        /// Type of object to drop..
        /// </summary>
        internal static readonly String InstancePlacement_PlacementType =
            "instance_placement_type";

        /// <summary>
        /// Type of object to drop..
        /// </summary>
        internal static readonly String InstancePlacement_InputTextures =
            "instance_placement_input_textures";

        /// <summary>
        /// Determines whether this process has a 3D Studio Max file that needs to be processed.
        /// </summary>
        internal static readonly String InstancePlacement_AdditionalSceneXMLDependencies =
            "additional_scene_xml_dependencies";

        /// <summary>
        /// Overlapping container nodes.
        /// </summary>
        internal static readonly String InstancePlacement_OverlappingContainerDependencies =
            "instance_placement_overlapping_containers";

        /// <summary>
        /// Option to enable the processing of instance placement.
        /// </summary>
        internal static readonly String InstancePlacement_MapInstancePlacementIntersectionEnabled =
            "Enable Instance Placement Intersection Checks";

        /// <summary>
        /// Cache directory for source map extraction.
        /// </summary>
        internal static readonly String InstancePlacement_SourceMapCacheDirectory =
            "Source Cache Directory";

        /// <summary>
        /// Cache directory for processed map construction.
        /// </summary>
        internal static readonly String InstancePlacement_ProcessedMapCacheDirectory =
            "Processed Cache Directory";

        /// <summary>
        /// Path to our global data texture to be passed to the tool.
        /// </summary>
        internal static readonly String InstancePlacement_GlobalDataTexture =
            "Global Data Texture";

        /// <summary>
        /// Path to our global data texture to be passed to the tool.
        /// </summary>
        internal static readonly String InstancePlacement_GlobalTintTexture =
            "Global Tint Texture";

        /// <summary>
        /// Processed collision name for a given output texture. Used to calculate texture boundries.
        /// </summary>
        internal static readonly String InstancePlacement_ProcessedCollision =
            "Processed Collision";

        /// <summary>
        /// Parameter for global textures.
        /// </summary>
        internal static readonly String InstancePlacement_GlobalTextureParameter =
            "global";

#warning LPXO: TEMPORARY TO GET THIS TO COMPILE
        /// <summary>
        /// Prefix for a parameter that specifies an input to be processed by the metadata serialiser
        /// </summary>
        internal static readonly String MetadataSerialiser_InputToProcessParameterPrefix =
            "input_to_process_";
    }
}
