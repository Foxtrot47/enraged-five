﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using SIO = System.IO;
using System.Linq;
using System.Text;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using XGE = RSG.Interop.Incredibuild.XGE;
using RSG.Pipeline.Content;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Attributes;
using RSG.Pipeline.Services;
using RSG.Pipeline.Services.Metadata;

namespace RSG.Pipeline.Processor.Map.InstancePlacement
{
    [Export(typeof(IProcessor))]
    [ProcessorFlags(ProcessorFlags.Compatibility)]
    public class InstancePlacementBoundsManifestProcessor :
        ProcessorBase,
        IProcessor
    {
        #region Constants
        /// <summary>
        /// Processor description.
        /// </summary>
        private static readonly String DESCRIPTION = "Instance Placement Bounds Manifest Processor";

        /// <summary>
        /// Processor log context.
        /// </summary>
        private static readonly String LOG_CTX = "Instance Placement Bounds Manifest Processor";

        /// <summary>
        /// Path to our instance placement (metadata) XML filename.
        /// </summary>
        private static readonly String InstancePlacementXmlFilename = "$(assets)/metadata/terrain/InstancePlacement.xml";
        #endregion

        #region Properties
        private String TextureMetadataPath;
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public InstancePlacementBoundsManifestProcessor()
            : base(DESCRIPTION)
        {

        }
        #endregion // Constructor(s)

        #region IProcessor Interface Methods
        /// <summary>
        /// Prebuild stage; contains simple validation of passed process.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="process"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="syncDependencies"></param>
        /// <param name="resultantProcesses"></param>
        /// <returns></returns>
        public override bool Prebuild(IEngineParameters param,
            IProcess process, IProcessorCollection processors,
            IContentTree owner, out IEnumerable<IContentNode> syncDependencies,
            out IEnumerable<IProcess> resultantProcesses)
        {
            // Process verification.
            Debug.Assert(process.Inputs.Count() > 0);
            Debug.Assert(process.Outputs.Count() > 0);

            bool result = true;
            process.State = ProcessState.Prebuilt;

            TextureMetadataPath = param.Branch.Environment.Subst(InstancePlacementXmlFilename);

            // Sync all the processed collision as well as the IP configuration file.
            syncDependencies = new List<IContentNode>();
            foreach (IContentNode inputNode in process.Inputs)
                (syncDependencies as List<IContentNode>).Add(inputNode);

            resultantProcesses = new List<IProcess>();
            (resultantProcesses as List<IProcess>).Add(process);

            return result;
        }

        /// <summary>
        /// Prepare a build for the processes; populating the XGE project with
        /// the tools, environments and tasks required.  Including setting up
        /// the task dependency chain.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processes"></param>
        /// <param name="tools"></param>
        /// <param name="tasks"></param>
        /// <returns></returns>
        public override bool Prepare(IEngineParameters param, IEnumerable<IProcess> processes,
            out IEnumerable<XGE.ITool> tools, out IEnumerable<XGE.ITask> tasks)
        {
            bool xgeAllowRemote = GetXGEAllowRemote(param, true);

            XGE.ITaskJobGroup taskGroup = new XGE.TaskGroup(DESCRIPTION);
            XGE.ITool tool = XGEFactory.GetMapProcessorTool(param.Log, param.Branch.Project.Config, XGEFactory.MapToolType.InstancePlacementProcessor, DESCRIPTION, xgeAllowRemote);

            ICollection<XGE.ITool> convertTools = new List<XGE.ITool>();
            convertTools.Add(tool);

            bool result = true;
            int index = 0;
            List<IProcess> processList = processes.ToList();
            ICollection<XGE.ITask> convertTasks = new List<XGE.ITask>();

            InstancePlacementSerialiserOptions options = new InstancePlacementSerialiserOptions();

            // One bounds manifest per region.
            foreach (IProcess process in processList)
            {
                String caption = String.Format("{0} {1}", DESCRIPTION, index);
                XGE.TaskJob xgeTask = new XGE.TaskJob(caption);

                process.Parameters.Add(Constants.ProcessXGE_Task, xgeTask);
                process.Parameters.Add(Constants.ProcessXGE_TaskName, caption);

                List<IContentNode> inputNodes = process.Inputs.Where(n => ((n is Content.File) && (n as Content.File).Extension == ".zip")).ToList();

                List<InstancePlacementSerialiserInput> collisionInputs = new List<InstancePlacementSerialiserInput>();

                foreach (IContentNode content in inputNodes)
                {
                    String collisionPath = (content as Content.File).AbsolutePath;
                    InstancePlacementSerialiserInput collisionInput = new InstancePlacementSerialiserInput("", collisionPath);
                    collisionInputs.Add(collisionInput);

                    xgeTask.InputFiles.Add(collisionPath);
                }

                List<InstancePlacementSerialiserOutput> outputs = new List<InstancePlacementSerialiserOutput>();
                String boundsManifestXML = (process.Outputs.First() as Content.File).AbsolutePath;
                InstancePlacementSerialiserOutput output = new InstancePlacementSerialiserOutput(SIO.Path.GetDirectoryName(boundsManifestXML),
                                                                                                boundsManifestXML, "", "", "");
                outputs.Add(output);
                xgeTask.OutputFiles.Add(boundsManifestXML);

#warning TALL: Do we want to create another configuration type just for the instance placement bounds manifest processor?

                // This is just extra fluff to adhere to the configuration file syntax.
                List<InstancePlacementIntersectionInput> intersectionInputs = new List<InstancePlacementIntersectionInput>();
                List<InstancePlacementSerialiserInputTexture> inputTextures = new List<InstancePlacementSerialiserInputTexture>();
                List<String> sceneXMLDependencies = new List<String>();
                InstancePlacementType type = InstancePlacementType.Unknown;

                InstancePlacementSerialiserTask instancePlacementManifestTask = new InstancePlacementSerialiserTask("BoundsManifest",
                    collisionInputs, intersectionInputs, inputTextures, outputs, sceneXMLDependencies, type);

                List<InstancePlacementSerialiserTask> tasksData = new List<InstancePlacementSerialiserTask>();
                tasksData.Add(instancePlacementManifestTask);

                InstancePlacementSerialiserConfig config = new InstancePlacementSerialiserConfig(options, tasksData);

                String cacheDirectory = (String)processes.First().Parameters[Consts.InstancePlacement_ProcessedMapCacheDirectory];
                cacheDirectory = param.Branch.Environment.Subst(cacheDirectory);
                String configDataFilename = String.Format("instance_placement_bounds_{0}.xml", index);
                String configDataPathname = SIO.Path.Combine(cacheDirectory, configDataFilename);
                configDataPathname = SIO.Path.GetFullPath(configDataPathname);
                config.Serialise(param.Log, configDataPathname);

                StringBuilder paramSb = new StringBuilder();
                if (param.Flags.HasFlag(EngineFlags.Rebuild))
                    paramSb.Append("-rebuild ");
                paramSb.AppendFormat("-boundsManifest -config \"{0}\" ", configDataPathname);

                index++;

                xgeTask.Parameters = paramSb.ToString();
                xgeTask.SourceFile = configDataPathname;
                xgeTask.InputFiles.Add(configDataPathname);
                xgeTask.InputFiles.Add(TextureMetadataPath);
                xgeTask.Caption = caption;
                xgeTask.Tool = tool;

                convertTasks.Add(xgeTask);
            }

            foreach (XGE.ITaskJob taskJob in convertTasks)
                taskGroup.Tasks.Add(taskJob);

            tools = convertTools;
            tasks = new List<XGE.ITask>(new XGE.ITask[] { taskGroup });

            return (result);
        }
        #endregion
    }
}
