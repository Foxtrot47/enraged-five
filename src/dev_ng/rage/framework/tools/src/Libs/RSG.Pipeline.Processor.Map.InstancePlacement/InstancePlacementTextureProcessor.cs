﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using SIO = System.IO;
using System.Linq;
using System.Text;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using XGE = RSG.Interop.Incredibuild.XGE;
using RSG.Pipeline.Content;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Attributes;
using RSG.Pipeline.Services;
using RSG.Pipeline.Services.Metadata;

namespace RSG.Pipeline.Processor.Map.InstancePlacement
{
    /// <summary>
    /// Instance placement texture processor; splits up the main textures into
    /// smaller region specific textures.
    /// </summary>
    [Export(typeof(IProcessor))]
    [ProcessorFlags(ProcessorFlags.Compatibility)]
    public class InstancePlacementTextureProcessor :
        ProcessorBase,
        IProcessor
    {
        #region Constants
        /// <summary>
        /// Processor description.
        /// </summary>
        private static readonly String DESCRIPTION = "Instance Placement Texture Processor";

        /// <summary>
        /// Processor log context.
        /// </summary>
        private static readonly String LOG_CTX = "Instance Placement Texture Processor";

        /// <summary>
        /// Path to our instance placement (metadata) XML filename.
        /// </summary>
        private static readonly String InstancePlacementXmlFilename = "$(assets)/metadata/terrain/InstancePlacement.xml";
        #endregion

        #region Properties
        private String TextureMetadataPath;
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public InstancePlacementTextureProcessor()
            : base(DESCRIPTION)
        {

        }
        #endregion // Constructor(s)

        #region IProcessor Interface Methods
        /// <summary>
        /// Prebuild stage; contains simple validation of passed process.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="process"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="syncDependencies"></param>
        /// <param name="resultantProcesses"></param>
        /// <returns></returns>
        public override bool Prebuild(IEngineParameters param,
            IProcess process, IProcessorCollection processors,
            IContentTree owner, out IEnumerable<IContentNode> syncDependencies,
            out IEnumerable<IProcess> resultantProcesses)
        {
            // Process verification.
            Debug.Assert(process.Inputs.Count() > 0);
            Debug.Assert(process.Outputs.Count() > 0);

            // Gather all the dependencies, this should just be processed collision zip files and an image file (can be any format: .tif, .bmp, etc)
            IEnumerable<IContentNode> inputDependencies = process.Inputs.Where(input => (input as Content.File).Extension != ".xml");

            TextureMetadataPath = param.Branch.Environment.Subst(InstancePlacementXmlFilename);

            bool result = true;
            process.State = ProcessState.Prebuilt;

            syncDependencies = new List<IContentNode>();
            (syncDependencies as List<IContentNode>).Add(owner.CreateFile(TextureMetadataPath));
            (syncDependencies as List<IContentNode>).AddRange(inputDependencies);

            resultantProcesses = new List<IProcess>();
            (resultantProcesses as List<IProcess>).Add(process);

            return result;
        }

        /// <summary>
        /// Prepare a build for the processes; populating the XGE project with
        /// the tools, environments and tasks required.  Including setting up
        /// the task dependency chain.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processes"></param>
        /// <param name="tools"></param>
        /// <param name="tasks"></param>
        /// <returns></returns>
        public override bool Prepare(IEngineParameters param, IEnumerable<IProcess> processes,
            out IEnumerable<XGE.ITool> tools, out IEnumerable<XGE.ITask> tasks)
        {
            // This is setup as local only because of an Incredibuild issue that hasn't been solved yet.
            // Will turn this back on when we have a proper fix from Xoreax.
            bool xgeAllowRemote = false; //GetXGEAllowRemote(param, true);

            XGE.ITaskJobGroup taskGroup = new XGE.TaskGroup(DESCRIPTION);
            XGE.ITool tool = XGEFactory.GetMapProcessorTool(param.Log, param.Branch.Project.Config, XGEFactory.MapToolType.InstancePlacementProcessor, DESCRIPTION, xgeAllowRemote);

            // Only want to run one instance per machine otherwise the memory usage will spike like crazy on a single machine.
            // In theory we only need as many machines as we have number of textures.
            tool.SingleInstancePerAgent = true;

            ICollection<XGE.ITool> convertTools = new List<XGE.ITool>();
            convertTools.Add(tool);

            bool result = true;
            int index = 0;
            List<IProcess> processList = processes.ToList();
            ICollection<XGE.ITask> convertTasks = new List<XGE.ITask>();

            ICollection<InstancePlacementTextureSerialiserTask> tasksData = new List<InstancePlacementTextureSerialiserTask>();

            // One texture process per machine.
            foreach (IProcess process in processList)
            {
                String caption = String.Format("{0} {1}", DESCRIPTION, index);
                XGE.TaskJob xgeTask = new XGE.TaskJob(caption);

                process.Parameters.Add(Constants.ProcessXGE_Task, xgeTask);
                process.Parameters.Add(Constants.ProcessXGE_TaskName, caption);

                String inputTexturePath = (process.Inputs.ElementAt(0) as Content.File).AbsolutePath;
                String boundsManifestXML = (process.Inputs.ElementAt(1) as Content.File).AbsolutePath;
                xgeTask.InputFiles.Add(boundsManifestXML);

                InstancePlacementSerialiserInputTexture inputTexture = new InstancePlacementSerialiserInputTexture(inputTexturePath);
                List<InstancePlacementTextureSerialiserOutput> outputs = new List<InstancePlacementTextureSerialiserOutput>();

                foreach (IContentNode outputTexture in process.Outputs)
                {
                    string processedCollision = "";
                    if (outputTexture.Parameters.ContainsKey(Consts.InstancePlacement_ProcessedCollision))
                        processedCollision = (String)outputTexture.Parameters[Consts.InstancePlacement_ProcessedCollision];

                    String outputTexturePath = (outputTexture as Content.File).AbsolutePath;

                    InstancePlacementTextureSerialiserOutput outputEntry = new InstancePlacementTextureSerialiserOutput(outputTexturePath, processedCollision);
                    outputs.Add(outputEntry);

                    xgeTask.OutputFiles.Add(outputTexturePath);
                }

                InstancePlacementTextureSerialiserTask task = new InstancePlacementTextureSerialiserTask(caption, boundsManifestXML, inputTexture, outputs);
                List<InstancePlacementTextureSerialiserTask> serialiserTasks = new List<InstancePlacementTextureSerialiserTask>();
                serialiserTasks.Add(task);

                InstancePlacementTextureSerialiserConfig configSerialiser = new InstancePlacementTextureSerialiserConfig(serialiserTasks);

                String cacheDirectory = (String)processes.First().Parameters[Consts.InstancePlacement_ProcessedMapCacheDirectory];
                cacheDirectory = param.Branch.Environment.Subst(cacheDirectory);
                String configDataFilename = String.Format("instance_placement_textures_{0}.xml", index);
                String configDataPathname = SIO.Path.Combine(cacheDirectory, configDataFilename);
                configDataPathname = SIO.Path.GetFullPath(configDataPathname);

                configSerialiser.Serialise(param.Log, configDataPathname);

                StringBuilder paramSb = new StringBuilder();
                if (param.Flags.HasFlag(EngineFlags.Rebuild))
                    paramSb.Append("-rebuild ");
                paramSb.AppendFormat("-splitTextures -config \"{0}\" ", configDataPathname);

                index++;

                xgeTask.Parameters = paramSb.ToString();
                xgeTask.SourceFile = configDataPathname;
                xgeTask.InputFiles.Add(configDataPathname);
                xgeTask.InputFiles.Add(inputTexturePath);
                xgeTask.InputFiles.Add(TextureMetadataPath);
                xgeTask.Caption = caption;
                xgeTask.Tool = tool;

                convertTasks.Add(xgeTask);
            }

            foreach (XGE.ITaskJob taskJob in convertTasks)
                taskGroup.Tasks.Add(taskJob);

            tools = convertTools;
            tasks = new List<XGE.ITask>(new XGE.ITask[] { taskGroup });

            return (result);
        }
        #endregion
    }
}
