﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Linq;
using System.Text;
using RSG.Pipeline.Content;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Attributes;
using RSG.Pipeline.Services;
using RSG.Pipeline.Services.Metadata;
using RSG.SceneXml.MapExport;
using SIO = System.IO;
using XGE = RSG.Interop.Incredibuild.XGE;

namespace RSG.Pipeline.Processor.Map.InstancePlacement
{
    /// <summary>
    /// Instance placement processor.
    /// </summary>
    [Export(typeof(IProcessor))]
    [ProcessorFlags(ProcessorFlags.Compatibility)]
    public class InstancePlacement :
        ProcessorBase
    {
        #region Constants
        /// <summary>
        /// Processor description.
        /// </summary>
        private static readonly String DESCRIPTION = "Instance Placement Processor";

        /// <summary>
        /// Processor log context.
        /// </summary>
        private static readonly String LOG_CTX = "Instance Placement Processor";

        /// <summary>
        /// Path to our instance placement (metadata) XML filename.
        /// </summary>
        private static readonly String InstancePlacementXmlFilename = "$(assets)/metadata/terrain/InstancePlacement.xml";

        /// <summary>
        /// List of accepted image file extensions.
        /// </summary>
        private static readonly List<String> ACCEPTED_IMAGE_FORMATS = new List<string>() {".bmp", ".tif", ".tga"};
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public InstancePlacement()
            : base(DESCRIPTION)
        {

        }
        #endregion // Constructor(s)

        #region IProcessor Interface Methods
        /// <summary>
        /// Prebuild stage; contains simple validation of passed process.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="process"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="syncDependencies"></param>
        /// <param name="resultantProcesses"></param>
        /// <returns></returns>
        public override bool Prebuild(IEngineParameters param,
            IProcess process, IProcessorCollection processors,
            IContentTree owner, out IEnumerable<IContentNode> syncDependencies,
            out IEnumerable<IProcess> resultantProcesses)
        {
            // Process verification.
            Debug.Assert(process.Inputs.Any());
            Debug.Assert(process.Outputs.Any());

            List<IContentNode> dependencies = new List<IContentNode>();
            ICollection<IProcess> processes = new List<IProcess>();
            this.LoadParameters(param);
            if (process.State == ProcessState.Initialised)
            {
                // OfType<File> allows to do a first filter on the process collection
                IEnumerable<File> inputNodes = process.Inputs.OfType<File>().Where(n => n.Extension == ".xml");
                foreach (File inputNode in inputNodes)
                {
                    string filename = SIO.Path.GetFileNameWithoutExtension(inputNode.AbsolutePath);
                    string collisionFilename = filename + "_collision.zip";

                    // the contentTree passed to Prebuild() contains only what's needed.
                    // We can retrieve the collision path as a node in it.
                    Asset processedBounds = owner.Nodes.OfType<Asset>().
                        FirstOrDefault(n => n.AbsolutePath.EndsWith(collisionFilename, StringComparison.OrdinalIgnoreCase));

                    if (processedBounds == null)
                    {
                        // log error processed bounds not found in the content tree
                        param.Log.Error("Processed bound not found in the content tree.");
                    }

                    dependencies.Add(processedBounds);
                    dependencies.Add(inputNode);
                }
                process.State = ProcessState.Prebuilding;
                resultantProcesses = processes;
                syncDependencies = dependencies;
                return true;
            }

            String textureMetadataPath = param.Branch.Environment.Subst(InstancePlacementXmlFilename);

            dependencies.Add(owner.CreateFile(textureMetadataPath));

            process.State = ProcessState.Prebuilt;
            processes.Add(process);
            resultantProcesses = processes;
            syncDependencies = dependencies;

            return true;
        }

        /// <summary>
        /// Prepare a build for the processes; populating the XGE project with
        /// the tools, environments and tasks required.  Including setting up
        /// the task dependency chain.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="processes"></param>
        /// <param name="tools"></param>
        /// <param name="tasks"></param>
        /// <returns></returns>
        public override bool Prepare(IEngineParameters param, IEnumerable<IProcess> processes,
            out IEnumerable<XGE.ITool> tools, out IEnumerable<XGE.ITask> tasks)
        {
            // This is setup as local only because of an Incredibuild issue that hasn't been solved yet.
            // Will turn this back on when we have a proper fix from Xoreax.
            bool xgeAllowRemote = false; //GetXGEAllowRemote(param, true);

            XGE.ITaskJobGroup taskGroup = new XGE.TaskGroup(DESCRIPTION);
            XGE.ITool tool = XGEFactory.GetMapProcessorTool(param.Log, param.Branch.Project.Config, XGEFactory.MapToolType.InstancePlacementProcessor, DESCRIPTION, xgeAllowRemote);
            ICollection<XGE.ITool> convertTools = new List<XGE.ITool>();
            convertTools.Add(tool);

            String textureMetadataPath = param.Branch.Environment.Subst(InstancePlacementXmlFilename);

            String instancePlacementOptionsPath = SIO.Path.Combine(param.Branch.Project.Config.ToolsConfig,
                "processors", "RSG.Pipeline.Processor.InstancePlacement.PreProcess.xml");

            bool result = true;

            InstancePlacementSerialiserOptions options = new InstancePlacementSerialiserOptions(this.Parameters);

            int index = 0;
            List<IProcess> processList = processes.ToList();
            const int maxProcessesPerTask = 8;
            int maxBuckets = (int)Math.Ceiling((double)processList.Count() / (double)maxProcessesPerTask);
            ICollection<XGE.ITask> convertTasks = new List<XGE.ITask>();

            for (int bucketIndex = 0; bucketIndex < maxBuckets; ++bucketIndex)
            {
                int startBucketIndex = bucketIndex * maxProcessesPerTask;
                List<IProcess> bucketProcesses = processList.GetRange(startBucketIndex, Math.Min(maxProcessesPerTask, processList.Count() - startBucketIndex));

                foreach (IProcess process in bucketProcesses)
                {
                    List<File> inputNodes = process.Inputs.OfType<File>().Where(n => n.Extension == ".xml").ToList();
                    List<File> inputTextureNodes = process.Inputs.OfType<File>().Where(n => ACCEPTED_IMAGE_FORMATS.Contains(n.Extension, StringComparer.OrdinalIgnoreCase)).ToList();

                    foreach (File inputNode in inputNodes)
                    {
                        //Create the configuration options file for this process.
                        ICollection<InstancePlacementSerialiserTask> tasksData = new List<InstancePlacementSerialiserTask>();

                        String caption = String.Format("{0} {1}", DESCRIPTION, index);
                        XGE.TaskJob xgeTask = new XGE.TaskJob(caption);

                        string filename = SIO.Path.GetFileNameWithoutExtension(inputNode.AbsolutePath);
                        string collisionFilename = filename + "_collision.zip";

                        // the contentTree passed to Prebuild() contains only what's needed.
                        // We can retrieve the collision path as a node in it.
                        Asset processedBoundsNode = process.Owner.Nodes.OfType<Asset>().
                            FirstOrDefault(n => n.AbsolutePath.EndsWith(collisionFilename, StringComparison.OrdinalIgnoreCase));

                        // These might not exist yet if they are being built in a full level conversion so let it pass.
                        if (!SIO.File.Exists(processedBoundsNode.AbsolutePath))
                            param.Log.MessageCtx(LOG_CTX, String.Format("Could not find processed bounds {0} for input node {1}. Might be getting built in another process.", processedBoundsNode, inputNode.AbsolutePath));

                        Content.File sceneXmlFile = null;
                        List<String> sceneXmlDependencies = null;
#warning LPXO:  Not sure what to make of this for now.  We should already have the inputs we need?
                        //IList<IContentNode> metadataInputs = (IList<IContentNode>)process.Parameters[Consts.MetadataSerialiser_InputToProcessParameterPrefix];
                        IList<IContentNode> metadataInputs = new List<IContentNode>();
                        metadataInputs.Add(inputNode);

                        if (metadataInputs.Any())
                        {
                            //Add the SceneXML from the Max file and all export zip dependencies
                            //to be used by the instance placement processor.
                            sceneXmlFile = process.Outputs.First() as Content.File;

                            sceneXmlDependencies = new List<String>();
                            ICollection<IContentNode> sceneXmlDependenciesCollection = (ICollection<IContentNode>)process.Parameters[Consts.InstancePlacement_AdditionalSceneXMLDependencies];
                            foreach (IContentNode sceneXmlDependency in sceneXmlDependenciesCollection)
                            {
                                Content.File sceneXmlDependencyFile = sceneXmlDependency as Content.File;
                                sceneXmlDependencies.Add(sceneXmlDependencyFile.AbsolutePath);
                            }
                        }

                        List<InstancePlacementSerialiserInput> inputs = new List<InstancePlacementSerialiserInput>();

                        Dictionary<InstancePlacementType, List<InstancePlacementSerialiserInputTexture>> placementTextures = new Dictionary<InstancePlacementType, List<InstancePlacementSerialiserInputTexture>>();

                        List<object> enabledPlacementTypes = process.Parameters[Consts.InstancePlacement_EnabledPlacementTypes] as List<object>;
                        List<InstancePlacementType> placementTypes = GetInstancePlacementTypeList(param, enabledPlacementTypes);

                        foreach (InstancePlacementType type in placementTypes)
                            placementTextures.Add(type, new List<InstancePlacementSerialiserInputTexture>());

                        //Find all texture inputs.
                        foreach (File inputTextureFileNode in inputTextureNodes)
                        {
                            InstancePlacementTextureType textureType = InstancePlacementTextureType.Default;
                            if (inputTextureFileNode.Parameters.ContainsKey(Consts.InstancePlacement_GlobalDataTexture))
                                textureType = InstancePlacementTextureType.Global_Data;
                            if (inputTextureFileNode.Parameters.ContainsKey(Consts.InstancePlacement_GlobalTintTexture))
                                textureType = InstancePlacementTextureType.Global_Tint;

                            InstancePlacementSerialiserInputTexture inputTexture = new InstancePlacementSerialiserInputTexture(inputTextureFileNode.AbsolutePath, textureType);

                            if (textureType == InstancePlacementTextureType.Global_Data || textureType == InstancePlacementTextureType.Global_Tint)
                            {
                                foreach (InstancePlacementType type in placementTypes)
                                    placementTextures[type].Add(inputTexture);
                            }
                            else
                            {
                                if (inputTextureFileNode.Parameters.ContainsKey(Consts.InstancePlacement_PlacementType))
                                {
                                    InstancePlacementType texturePlacementType = (InstancePlacementType)inputTextureFileNode.Parameters[Consts.InstancePlacement_PlacementType];
                                    placementTextures[texturePlacementType].Add(inputTexture);
                                }
                                else
                                {
                                    param.Log.Error("Could not find placement type for texture: {0}", inputTextureFileNode.AbsolutePath);
                                    continue;
                                }
                            }

                            xgeTask.InputFiles.Add(inputTextureFileNode.AbsolutePath);
                        }

                        //Acquire all SceneXML inputs.
                        foreach (IContentNode metadataInput in metadataInputs)
                        {
                            Content.File metadataFileInput = metadataInput as Content.File;
                            InstancePlacementSerialiserInput input = new InstancePlacementSerialiserInput(metadataFileInput.AbsolutePath, processedBoundsNode.AbsolutePath);
                            inputs.Add(input);

                            xgeTask.InputFiles.Add(metadataFileInput.AbsolutePath);
                        }

                        List<InstancePlacementIntersectionInput> intersectionInputs = new List<InstancePlacementIntersectionInput>();
#warning LPXO:  Support for overlapping containers currently disabled
                        /*
                        //Find all intersection inputs
                        if (process.Parameters.ContainsKey(Consts.InstancePlacement_OverlappingContainerDependencies))
                        {
                            IList<String> intersectionContainers = (IList<String>)process.Parameters[Consts.InstancePlacement_OverlappingContainerDependencies];
                            foreach (String intersectionInput in intersectionContainers)
                            {
                                String containerName = SIO.Path.GetFileName(intersectionInput);

                                IEnumerable<String> intersectionInputFiles = SIO.Directory.GetFiles(intersectionInput, "*");
                                string intersectionZipFile;

                                if (!Collision.GetUnprocessedCollisionPathname(intersectionInputFiles, out intersectionZipFile))
                                {
                                    // TODO - This should be an error because we need to make sure the collision exists for all overlapping containers.
                                    // RDR doesn't seem to have all sections with collision setup yet though.
                                    param.Log.WarningCtx(LOG_CTX, String.Format("Unable to determine collision input path for overlapped container {0}.", containerName));
                                    continue;
                                }

                                String collisionPath = intersectionZipFile;

                                InstancePlacementIntersectionInput input = new InstancePlacementIntersectionInput(containerName, collisionPath);
                                intersectionInputs.Add(input);
                            }
                        }
                        */
                        //Acquire all outputs.
                        Content.Directory outputDirectory = process.Outputs.ElementAt(process.Outputs.Count() - 1) as Content.Directory;
                        List<InstancePlacementSerialiserOutput> outputs = new List<InstancePlacementSerialiserOutput>();


                        String instancePlacementAdditionsPathname = (String)process.Parameters[Core.Constants.ManifestProcess_InstancePlacementProcessorAdditions];

                        InstancePlacementSerialiserOutput output = new InstancePlacementSerialiserOutput(outputDirectory.AbsolutePath, null, null, sceneXmlFile.AbsolutePath, instancePlacementAdditionsPathname);
                        outputs.Add(output);

                        foreach (InstancePlacementType type in placementTypes)
                        {
                            string name = MapAsset.AppendMapPrefixIfRequired(param.Branch.Project, inputNode.Name);

                            InstancePlacementSerialiserTask task = new InstancePlacementSerialiserTask(name, inputs, intersectionInputs, placementTextures[type], outputs,
                                sceneXmlDependencies, type);

                            tasksData.Add(task);
                        }

                        process.SetParameter(Constants.ProcessXGE_Task, xgeTask);
                        process.SetParameter(Constants.ProcessXGE_TaskName, caption);

                        InstancePlacementSerialiserConfig config = new InstancePlacementSerialiserConfig(options, tasksData);
                        String cacheDirectory = (String)processes.First().Parameters[Consts.InstancePlacement_ProcessedMapCacheDirectory];
                        cacheDirectory = param.Branch.Environment.Subst(cacheDirectory);
                        String configDataFilename = String.Format("instance_placement_{0}.xml", index);
                        String configDataPathname = SIO.Path.Combine(cacheDirectory, configDataFilename);
                        configDataPathname = SIO.Path.GetFullPath(configDataPathname);
                        config.Serialise(param.Log, configDataPathname);

                        StringBuilder paramSb = new StringBuilder();
                        if (param.Flags.HasFlag(EngineFlags.Rebuild))
                            paramSb.Append("-rebuild ");
                        paramSb.Append("-serialiseManifest ");
                        paramSb.AppendFormat("-config \"{0}\" ", configDataPathname);

                        if (param.Branch.Project.IsDLC)
                            paramSb.AppendFormat(" --branch {0} --dlc {1}", param.Branch.Name, param.Branch.Project.Name);
                        else
                            paramSb.AppendFormat(" --branch {0}",param.Branch.Name);
                        index++;

                        xgeTask.Parameters = paramSb.ToString();
                        xgeTask.SourceFile = configDataPathname;
                        xgeTask.InputFiles.Add(configDataPathname);
                        xgeTask.InputFiles.Add(textureMetadataPath);
                        xgeTask.InputFiles.Add(instancePlacementOptionsPath);
                        xgeTask.Caption = caption;
                        xgeTask.Tool = tool;

                        convertTasks.Add(xgeTask);
                    }
                }
            }

            foreach (XGE.ITaskJob taskJob in convertTasks)
                taskGroup.Tasks.Add(taskJob);

            tools = convertTools;
            tasks = new List<XGE.ITask>(new XGE.ITask[] {taskGroup});
            return (result);
        }
        #endregion // IProcessor Interface Methods

        #region Private Methods
#warning LPXO: Move to Pipeline.Services?
        /// <summary>
        /// Takes the object list of placement types and casts them into a list of InstancePlacementType enums.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="enabledPlacementTypes"></param>
        /// <returns></returns>
        private static List<InstancePlacementType> GetInstancePlacementTypeList(IEngineParameters param, List<object> enabledPlacementTypes)
        {
            List<InstancePlacementType> instancePlacementTypes = new List<InstancePlacementType>();

            foreach (object enabledPlacementTypeObj in enabledPlacementTypes)
            {
                String enabledPlacementType = (String)enabledPlacementTypeObj;
                InstancePlacementType placementType = InstancePlacementType.Unknown;

                if (!Enum.TryParse<InstancePlacementType>(enabledPlacementType, out placementType))
                {
                    param.Log.ErrorCtx(LOG_CTX, String.Format("Unable to resolve instance placement type {0}.", enabledPlacementType));
                    continue;
                }

                instancePlacementTypes.Add(placementType);
            }

            return instancePlacementTypes;
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Processor.Map namespace
