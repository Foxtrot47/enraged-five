﻿//---------------------------------------------------------------------------------------------
// <copyright file="PlatformPathConversionTests.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2012-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Tests
{
    using System;
    using System.IO;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using RSG.Base.Configuration;
    using RSG.Pipeline.Services.Platform;

    /// <summary>
    /// Unit tests for Platform Path conversions.
    /// </summary>
    [TestClass]
    public class PlatformPathConversionTests
    {
        [TestMethod]
        public void Test1()
        {
            IConfig config = ConfigFactory.CreateConfig();
            IBranch branch = config.Project.DefaultBranch;
            ITarget targetPS3 = branch.Targets[RSG.Platform.Platform.PS3];
            ITarget target360 = branch.Targets[RSG.Platform.Platform.Xbox360];

            String path1 = @"x:\gta5\assets\export\asset1.rpf";
            String path2 = @"x:\gta5\assets\export\asset1.idr.zip";
            String path3 = @"x:\gta5\assets\export\asset1.idr";
            String path4 = @"x:\gta5\assets\export\asset1.meta";
            String path5 = @"x:\gta5\assets\export\asset1.pso.meta";
            String build = branch.Environment.Subst(@"$(build)");

            // PS3 test.
            String path1ps3 = PlatformPathConversion.ConvertAndRemapFilenameToPlatform(targetPS3, path1);
            String path2ps3 = PlatformPathConversion.ConvertAndRemapFilenameToPlatform(targetPS3, path2);
            String path3ps3 = PlatformPathConversion.ConvertAndRemapFilenameToPlatform(targetPS3, path3);
            String path4ps3 = PlatformPathConversion.ConvertAndRemapFilenameToPlatform(targetPS3, path4);
            String path5ps3 = PlatformPathConversion.ConvertAndRemapFilenameToPlatform(targetPS3, path5);

            Assert.AreEqual(Path.Combine(build, "ps3", Path.ChangeExtension("asset1", "rpf")), path1ps3);
            Assert.AreEqual(Path.Combine(build, "ps3", Path.ChangeExtension("asset1", "cdr")), path2ps3);
            Assert.AreEqual(Path.Combine(build, "ps3", Path.ChangeExtension("asset1", "cdr")), path3ps3);
            Assert.AreEqual(Path.Combine(build, "ps3", Path.ChangeExtension("asset1", "cmt")), path4ps3);
            Assert.AreEqual(Path.Combine(build, "ps3", Path.ChangeExtension("asset1", "pso")), path5ps3);

            // Xenon test.
            String path1360 = PlatformPathConversion.ConvertAndRemapFilenameToPlatform(target360, path1);
            String path2360 = PlatformPathConversion.ConvertAndRemapFilenameToPlatform(target360, path2);
            String path3360 = PlatformPathConversion.ConvertAndRemapFilenameToPlatform(target360, path3);
            String path4360 = PlatformPathConversion.ConvertAndRemapFilenameToPlatform(target360, path4);
            String path5360 = PlatformPathConversion.ConvertAndRemapFilenameToPlatform(target360, path5);

            Assert.AreEqual(Path.Combine(build, "xbox360", Path.ChangeExtension("asset1", "rpf")), path1360);
            Assert.AreEqual(Path.Combine(build, "xbox360", Path.ChangeExtension("asset1", "xdr")), path2360);
            Assert.AreEqual(Path.Combine(build, "xbox360", Path.ChangeExtension("asset1", "xdr")), path3360);
            Assert.AreEqual(Path.Combine(build, "xbox360", Path.ChangeExtension("asset1", "xmt")), path4360);
            Assert.AreEqual(Path.Combine(build, "xbox360", Path.ChangeExtension("asset1", "pso")), path5360);
        }
    }

} // RSG.Pipeline.Tests namespace
