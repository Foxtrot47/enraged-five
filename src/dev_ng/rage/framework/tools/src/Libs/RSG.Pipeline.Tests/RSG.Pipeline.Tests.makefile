#
# RSG.Pipeline.Tests.makefile
#
 
Project RSG.Pipeline.Tests
 
FrameworkVersion 4.5
 
OutputPath $(toolsroot)\ironlib\lib\
 
Files {
	ContentTreeDynamic.cs
	ContentTreeLoadTest.cs
	PlatformPathConversionTests.cs
	ProcessResolver.cs
	Directory Properties {
			AssemblyInfo.cs
	}
}
 
ProjectReferences {
	..\..\..\..\..\base\tools\libs\RSG.Base\RSG.Base.csproj
	..\..\..\..\..\base\tools\libs\RSG.Base.Configuration\RSG.Base.Configuration.csproj
	..\..\..\..\..\base\tools\libs\RSG.GraphML\RSG.GraphML.csproj
	..\RSG.Pipeline.Content\RSG.Pipeline.Content.csproj
	..\RSG.Pipeline.Core\RSG.Pipeline.Core.csproj
	..\RSG.Pipeline.Engine\RSG.Pipeline.Engine.csproj
	..\RSG.Pipeline.Services\RSG.Pipeline.Services.csproj
}
References {
	Microsoft.VisualStudio.QualityTools.UnitTestFramework
	System
	System.ComponentModel.Composition
	System.Core
	System.Xml.Linq
	System.Data.DataSetExtensions
	Microsoft.CSharp
	System.Data
	System.Xml
}
