﻿//---------------------------------------------------------------------------------------------
// <copyright file="ContentTreeLoadTest.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2012-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Tests
{
    using System;
    using System.Diagnostics;
    using SIO = System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using RSG.Base.Configuration;
    using RSG.Base.Logging;
    using RSG.GraphML;
    using RSG.Pipeline.Core;
    using RSG.Pipeline.Content;

    /// <summary>
    /// 
    /// </summary>
    [TestClass]
    public class ContentTreeLoadTest
    {
        /// <summary>
        /// Unit test initialisation.
        /// </summary>
        [TestInitialize]
        public void Setup()
        {
            Debug.Listeners.Clear();
        }

        /// <summary>
        /// Attempts to load the content-tree just using the GraphML library.
        /// This shows compatibility with GraphML.
        /// </summary>
        [TestMethod]
        public void TestContentTreeLoadAsGraph()
        {
            IConfig config = ConfigFactory.CreateConfig();
            IBranch branch = config.Project.DefaultBranch;
            String filename = branch.Environment.Subst(branch.Content);
            Assert.IsTrue(SIO.File.Exists(filename));

            GraphCollection graphs = new GraphCollection(filename, branch);
            Assert.IsTrue(graphs.Count > 0);
            Assert.IsTrue(graphs.Attributes.Count > 0); // From XInclude'd data

            IContentTree tree = Factory.CreateTree(branch, graphs);
            Assert.IsTrue(tree.Processes.Any());
        }

        /// <summary>
        /// We have noticed that multiple threads in the automation monitor loading
        /// the content-tree causes assertions; maybe the xi:include reader isn't
        /// thread safe.  This tries to catch them.  See Bug #2025883.
        /// </summary>
        [TestMethod]
        public void TestContentTreeLoadInParallel()
        {
            IConfig config = ConfigFactory.CreateConfig();
            IBranch branch = config.Project.DefaultBranch;
            String filename = branch.Environment.Subst(branch.Content);
            Assert.IsTrue(SIO.File.Exists(filename));

            Parallel.For(0, 5, delegate(int i)
            {
                GraphCollection graphs = new GraphCollection(filename, branch);
                Assert.IsTrue(graphs.Count > 0);
                Assert.IsTrue(graphs.Attributes.Count > 0); // From XInclude'd data

                IContentTree tree = Factory.CreateTree(branch, graphs);
                Assert.IsTrue(tree.Processes.Any());
            });
        }
    }

} // RSG.Pipeline.Tests namespace
