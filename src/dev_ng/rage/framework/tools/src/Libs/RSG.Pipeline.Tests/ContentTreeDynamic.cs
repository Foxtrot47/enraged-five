﻿//---------------------------------------------------------------------------------------------
// <copyright file="ContentTreeDynamic.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2012-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Tests
{
    using System;
    using System.Collections.Generic;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System.Linq;
    using RSG.Base.Configuration;
    using RSG.Pipeline.Core;
    using RSG.Pipeline.Content;

    /// <summary>
    /// 
    /// </summary>
    [TestClass]
    public class ContentTreeDynamic
    {
        /// <summary>
        /// 
        /// </summary>
        [TestMethod]
        public void TestDynamicTree()
        {
            IConfig config = ConfigFactory.CreateConfig();
            IBranch branch = config.Project.DefaultBranch;
            IContentTree tree = Factory.CreateEmptyTree(branch);

            Assert.AreEqual(0, tree.Processes.Count());
        }
    }

} // RSG.Pipeline.Tests namespace
