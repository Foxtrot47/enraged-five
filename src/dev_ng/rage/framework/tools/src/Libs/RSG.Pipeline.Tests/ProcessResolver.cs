﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProcessResolver.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2012-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Tests
{
    using System;
    using System.Collections.Generic;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using RSG.Base.Configuration;
    using RSG.Base.Logging;
    using RSG.Base.Logging.Universal;
    using RSG.Pipeline.Content;
    using RSG.Pipeline.Core;
    using RSG.Pipeline.Core.Build;
    using RSG.Pipeline.Engine;

    /// <summary>
    /// Summary description for ProcessResolver
    /// </summary>
    [TestClass]
    public class ProcessResolver
    {
        private IConfig m_config;
        private IProject m_project;
        private IBranch m_branch;
        private IContentTree m_loadedTree;
        private IUniversalLog m_log;
        private IEngineParameters m_engineParameters;

        [TestInitialize]
        public void TestInitializer()
        {
            LogFactory.Initialize();
            m_log = LogFactory.CreateUniversalLog("test");
            m_config = ConfigFactory.CreateConfig();
            m_project = m_config.Project;
            m_branch = m_project.DefaultBranch;
            m_loadedTree = Factory.CreateTree(m_branch);
            m_engineParameters = new EngineParameters(m_branch, EngineFlags.Default, null, BuildType.All, m_log);
        }

        [TestMethod]
        public void ResolveRecursively()
        {
            String[] filenames = { @"X:\gta5\assets_ng\export\levels\gta5\_citye\downtown_01\dt1_01.zip" };
            IEnumerable<IProcess> processes = Services.ProcessResolver.ResolveProcessRecursive(m_engineParameters, m_loadedTree, filenames, true);

            foreach (IProcess p in processes)
            {
                foreach (IContentNode o in p.Outputs)
                    m_log.Message("Output: {0}.", ((IFilesystemNode)o).AbsolutePath);
            }
        }
    }

} // RSG.Pipeline.Tests namespace
