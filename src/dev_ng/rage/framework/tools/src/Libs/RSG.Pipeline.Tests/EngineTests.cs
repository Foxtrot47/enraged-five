﻿//---------------------------------------------------------------------------------------------
// <copyright file="EngineTests.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2012-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using RSG.Base.Configuration;
    using RSG.Base.Logging;
    using RSG.Pipeline.Core;
    using RSG.Pipeline.Core.Build;
    using RSG.Pipeline.Content;
    using RSG.Pipeline.Engine;
    using RSG.Pipeline.Services;
    using RSG.Platform;

    /// <summary>
    /// Engine class unit tests.
    /// </summary>
    [TestClass]
    public class EngineTests
    {
        #region Member Data
        /// <summary>
        /// Configuration data object for unit testing.
        /// </summary>
        private static IConfig Config { get; set; }

        /// <summary>
        /// Project branch.
        /// </summary>
        private static IBranch Branch { get; set; }

        /// <summary>
        /// Engine object for unit testing.
        /// </summary>
        private IEngine Engine { get; set; }
        #endregion // Member Data

        /// <summary>
        /// Class Test initialisation.
        /// </summary>
        [ClassInitialize]
        public static void Setup(TestContext context)
        {
            LogFactory.Initialize();
            LogFactory.CreateApplicationConsoleLogTarget();
            EngineTests.Config = ConfigFactory.CreateConfig();
            EngineTests.Branch = EngineTests.Config.Project.DefaultBranch;
        }

        /// <summary>
        /// Class Test shutdown.
        /// </summary>
        [ClassCleanup]
        public static void Shutdown()
        {
            LogFactory.ApplicationShutdown();
        }

        /// <summary>
        /// Engine unit test: basic asset conversion using RAGE processor.
        /// </summary>
        [TestMethod]
        public void TestEngine1()
        {
            CommandOptions opts = GetEmptyCommandOptions();
            this.Engine = new Engine(opts, opts.Config, opts.Project, opts.Branch, EngineFlags.Default);
            Assert.AreEqual(EngineFlags.Default, this.Engine.Flags);

            IEnumerable<IOutput> outputs;
            TimeSpan build_time;
            IInput input = new BuildInput(EngineTests.Branch);

            IContentTree tree = Factory.CreateEmptyTree(EngineTests.Branch);
            IContentNode pistolDrawable = tree.CreateFile(Path.Combine(EngineTests.Branch.Export, "models", "cdimages", "pistol_grip_mesh.idr.zip"));
            IContentNode pistolDrawable2 = tree.CreateFile(Path.Combine("$(target)", "models", "cdimages", "pistol_grip_mesh.cdr"));
            ProcessBuilder pistolPb = new ProcessBuilder("RSG.Pipeline.Processor.Platform.Rage", tree);
            pistolPb.Inputs.Add(pistolDrawable);
            pistolPb.Outputs.Add(pistolDrawable2);
            IProcess pistolProcess = pistolPb.ToProcess();

            IContentNode rifleDrawable = tree.CreateFile(Path.Combine(EngineTests.Branch.Export, "models", "cdimages", "rifle_grip_mesh.idr.zip"));
            IContentNode rifleDrawable2 = tree.CreateFile(Path.Combine("$(target)", "models", "cdimages", "rifle_grip_mesh.cdr"));
            ProcessBuilder riflePb = new ProcessBuilder("RSG.Pipeline.Processor.Platform.Rage", tree);
            riflePb.Inputs.Add(rifleDrawable);
            riflePb.Outputs.Add(rifleDrawable2);
            IProcess rifleProcess = riflePb.ToProcess();

            IContentNode txdTest = tree.CreateFile(Path.Combine(EngineTests.Branch.Export, "models", "cdimages", "w_am_laptop.itd.zip"));
            IContentNode txdTest2 = tree.CreateFile(Path.Combine("$(target)", "models", "cdimages", "w_am_laptop.ctd"));
            ProcessBuilder txdPb = new ProcessBuilder("RSG.Pipeline.Processor.Platform.Rage", tree);
            txdPb.Inputs.Add(txdTest);
            txdPb.Outputs.Add(txdTest2);
            IProcess txdProcess = txdPb.ToProcess();

            input.RawProcesses.Add(pistolProcess);
            input.RawProcesses.Add(rifleProcess);
            input.RawProcesses.Add(txdProcess);

            bool result = this.Engine.Build(ref input, out outputs, out build_time);

            Assert.IsTrue(result);
            Assert.IsTrue(build_time > TimeSpan.Zero);
        }

        /// <summary>
        /// Engine unit test: zip asset conversion using RAGE processor.
        /// </summary>
        [TestMethod]
        public void TestEngine2()
        {
            CommandOptions opts = GetEmptyCommandOptions();
            this.Engine = new Engine(opts, opts.Config, opts.Project, opts.Branch, EngineFlags.Default | EngineFlags.Rebuild);

            IEnumerable<IOutput> outputs;
            TimeSpan build_time;
            IInput input = new BuildInput(EngineTests.Branch);

            IContentTree tree = Factory.CreateEmptyTree(EngineTests.Branch);
            IContentNode inputZip1 = tree.CreateFile(Path.Combine(EngineTests.Branch.Export, "anim", "expressions.zip"));
            IContentNode outputRpf1 = tree.CreateFile(Path.Combine("$(target)", "anim", "expressions.rpf"));
            ProcessBuilder expressionPb = new ProcessBuilder("RSG.Pipeline.Processor.Platform.Rage", tree);
            expressionPb.Inputs.Add(inputZip1);
            expressionPb.Outputs.Add(outputRpf1);
            IProcess expressionProcess = expressionPb.ToProcess();

            IContentNode inputZip2 = tree.CreateFile(Path.Combine(EngineTests.Branch.Export, "data", "cdimages", "scaleform_generic.zip"));
            IContentNode outputRpf2 = tree.CreateFile(Path.Combine("$(target)", "data", "cdimages", "scaleform_generic.rpf"));
            ProcessBuilder sfPb = new ProcessBuilder("RSG.Pipeline.Processor.Platform.Rage", tree);
            sfPb.Inputs.Add(inputZip2);
            sfPb.Outputs.Add(outputRpf2);
            IProcess scaleformProcess = sfPb.ToProcess();

            bool result = this.Engine.Build(ref input, out outputs, out build_time);

            Assert.IsTrue(result);
            Assert.IsTrue(build_time > TimeSpan.Zero);
        }

        /// <summary>
        /// Engine unit test: directory to RPF conversion using RAGE processor.
        /// </summary>
        [TestMethod]
        public void TestEngine3()
        {
            CommandOptions opts = GetEmptyCommandOptions();
            this.Engine = new Engine(opts, opts.Config, opts.Project, opts.Branch, EngineFlags.Default);
            Assert.AreEqual(EngineFlags.Default, this.Engine.Flags);

            IEnumerable<IOutput> outputs;
            TimeSpan build_time;
            IInput input = new BuildInput(EngineTests.Branch);

            IContentTree tree = Factory.CreateEmptyTree(EngineTests.Branch);
            IContentNode inputData = tree.CreateDirectory(Path.Combine(EngineTests.Branch.Export, "levels", "gta5", "vehicles"));
            IContentNode outputRpf = tree.CreateFile(Path.Combine("$(target)", "levels", "gta5", "vehicles.rpf"));
            ProcessBuilder pb = new ProcessBuilder("RSG.Pipeline.Processor.Platform.Rage", tree);
            pb.Inputs.Add(inputData);
            pb.Outputs.Add(outputRpf);
            IProcess vehiclesProcess = pb.ToProcess();

            input.RawProcesses.Add(vehiclesProcess);

            bool result = this.Engine.Build(ref input, out outputs, out build_time);

            Assert.IsTrue(result);
            Assert.IsTrue(build_time > TimeSpan.Zero);
        }

        /// <summary>
        /// Engine unit test; multiple input assets to RPF.
        /// </summary>
        /// Ref: url:bugstar:570740.
        /// 
        [TestMethod]
        public void TestEngine4()
        {
            CommandOptions opts = GetEmptyCommandOptions();
            this.Engine = new Engine(opts, opts.Config, opts.Project, opts.Branch, EngineFlags.Default);
            Assert.AreEqual(EngineFlags.Default, this.Engine.Flags);

            IEnumerable<IOutput> outputs;
            TimeSpan build_time;
            IInput input = new BuildInput(EngineTests.Branch);

            IContentTree tree = Factory.CreateEmptyTree(EngineTests.Branch);
            IContentNode weapon1Drawable = tree.CreateFile(Path.Combine(EngineTests.Branch.Export, "models", "cdimages", "pistol_grip_mesh.idr.zip"));
            IContentNode weapon2Drawable = tree.CreateFile(Path.Combine(EngineTests.Branch.Export, "models", "cdimages", "rifle_grip_mesh.idr.zip"));
            IContentNode weaponRpf = tree.CreateFile(Path.Combine("$(target)", "models", "cdimages", "weapon_test.rpf"));
            ProcessBuilder pb = new ProcessBuilder("RSG.Pipeline.Processor.Platform.Rage", tree);
            pb.Inputs.Add(weapon1Drawable);
            pb.Inputs.Add(weapon2Drawable);
            pb.Outputs.Add(weaponRpf);
            IProcess weaponRpfProcess = pb.ToProcess();
            input.RawProcesses.Add(weaponRpfProcess);

            bool result = this.Engine.Build(ref input, out outputs, out build_time);

            Assert.IsTrue(result);
            Assert.IsTrue(build_time > TimeSpan.Zero);
        }

        /// <summary>
        /// Engine unit test; map for RPF sort testing with ITYP file.
        /// </summary>
        [TestMethod]
        public void TestEngine5()
        {
            CommandOptions opts = GetEmptyCommandOptions();
            this.Engine = new Engine(opts, opts.Config, opts.Project, opts.Branch, EngineFlags.Default | EngineFlags.Rebuild);
            Assert.IsTrue(this.Engine.Flags.HasFlag(EngineFlags.Rebuild));

            IEnumerable<IOutput> outputs;
            TimeSpan build_time;
            IInput input = new BuildInput(EngineTests.Branch);

            IContentTree tree = Factory.CreateEmptyTree(EngineTests.Branch);
            IContentNode map = tree.CreateFile(Path.Combine(EngineTests.Branch.Export, "levels", "gta5", "props", "vegetation", "v_bush.zip"));
            IContentNode rpf = tree.CreateFile(Path.Combine("$(target)", "levels", "gta5", "props", "vegetation", "v_bush.rpf"));
            ProcessBuilder pb = new ProcessBuilder("RSG.Pipeline.Processor.Platform.Rage", tree);
            pb.Inputs.Add(map);
            pb.Outputs.Add(rpf);
            IProcess mapProcess = pb.ToProcess();
            input.RawProcesses.Add(mapProcess);

            bool result = this.Engine.Build(ref input, out outputs, out build_time);

            Assert.IsTrue(result);
            Assert.IsTrue(build_time > TimeSpan.Zero);
        }

        /// <summary>
        /// "Rebuild" enabled unit tests.
        /// </summary>
        [TestMethod]
        public void TestEngine1_Rebuild()
        {
            CommandOptions opts = GetEmptyCommandOptions();
            this.Engine = new Engine(opts, opts.Config, opts.Project, opts.Branch, EngineFlags.Default | EngineFlags.Rebuild);
            Assert.IsTrue(this.Engine.Flags.HasFlag(EngineFlags.Rebuild));
        }

        /// <summary>
        /// "Preview" enabled unit tests.
        /// </summary>
        [TestMethod]
        public void TestEngine1_Preview()
        {
            CommandOptions opts = GetEmptyCommandOptions();
            this.Engine = new Engine(opts, opts.Config, opts.Project, opts.Branch, EngineFlags.Default | EngineFlags.Preview);
            Assert.IsTrue(this.Engine.Flags.HasFlag(EngineFlags.Preview));

            IEnumerable<IOutput> outputs;
            TimeSpan build_time;
            IInput input = new BuildInput(EngineTests.Branch);

            IContentTree tree = Factory.CreateEmptyTree(EngineTests.Branch);
            IContentNode map = tree.CreateFile(Path.Combine(EngineTests.Branch.Export, "levels", "gta5", "props", "vegetation", "v_bush.zip"));
            IContentNode rpf = tree.CreateFile(Path.Combine("$(target)", "levels", "gta5", "props", "vegetation", "v_bush.rpf"));
            ProcessBuilder pb = new ProcessBuilder("RSG.Pipeline.Processor.Platform.Rage", tree);
            pb.Inputs.Add(map);
            pb.Outputs.Add(rpf);
            IProcess mapProcess = pb.ToProcess();
            input.RawProcesses.Add(mapProcess);

            bool result = this.Engine.Build(ref input, out outputs, out build_time);

            Assert.IsTrue(result);
            Assert.IsTrue(build_time > TimeSpan.Zero);
        }

        /// <summary>
        /// Returns empty command options
        /// </summary>
        /// <returns></returns>
        private CommandOptions GetEmptyCommandOptions()
        {
            String[] args = System.Environment.GetCommandLineArgs();
            String[] realArgs = args.Skip(1).ToArray();
            CommandOptions opts = new CommandOptions(realArgs, new RSG.Base.OS.LongOption[] { });
            return opts;
        }
    }

} //  RSG.Pipeline.Tests namespace
