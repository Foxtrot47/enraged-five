﻿//---------------------------------------------------------------------------------------------
// <copyright file="DependencyTests.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2012-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Tests
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using RSG.Base.Configuration;
    using RSG.Base.Logging;
    using RSG.Pipeline.Core;
    using RSG.Pipeline.Content;
    using RSG.Pipeline.Content.Algorithm;
    using RSG.Pipeline.Engine;
    using RSG.Pipeline.Services;

    /// <summary>
    /// Content-Tree Dependency unit tests.
    /// </summary>
    [TestClass]
    public class DependencyTests
    {
        #region Member Data
        /// <summary>
        /// Configuration data object for unit testing.
        /// </summary>
        private IConfig Config { get; set; }

        /// <summary>
        /// Project branch.
        /// </summary>
        private IBranch Branch { get; set; }

        /// <summary>
        /// Console log.
        /// </summary>
        private ConsoleLog ConsoleLog { get; set; }

        /// <summary>
        /// Engine object for unit testing.
        /// </summary>
        private IEngine Engine { get; set; }
        #endregion // Member Data

        /// <summary>
        /// Test initialisation.
        /// </summary>
        [TestInitialize]
        public void TestStartup()
        {
            LogFactory.Initialize();
            LogFactory.CreateApplicationConsoleLogTarget();
            this.Config = ConfigFactory.CreateConfig();
            this.Branch = this.Config.Project.Branches["dev"];
        }

        /// <summary>
        /// Test shutdown.
        /// </summary>
        [TestCleanup]
        public void TestShutdown()
        {
            LogFactory.ApplicationShutdown();
        }

        /// <summary>
        /// Dependency test for single process content-tree.
        /// </summary>
        [TestMethod]
        public void TestEmpty()
        {
            CommandOptions opts = GetEmptyCommandOptions();
            this.Engine = new Engine(opts, opts.Config, opts.Project, opts.Branch, EngineFlags.None);
            Assert.AreEqual(EngineFlags.None, this.Engine.Flags);

            String exportPath = Path.Combine(this.Branch.Assets, "export");
            String resourcePath = this.Branch.Targets[RSG.Platform.Platform.PS3].ResourcePath;

            IContentTree tree = Factory.CreateEmptyTree(this.Branch);
            IContentNode pistolDrawable = tree.CreateFile(Path.Combine(exportPath, "models", "cdimages", "pistol_grip_mesh.idr.zip"));
            IContentNode pistolDrawable2 = tree.CreateFile(Path.Combine(resourcePath, "models", "cdimages", "pistol_grip_mesh.cdr"));
            ProcessBuilder pb = new ProcessBuilder("RSG.Pipeline.Processor.Platform.Rage", tree);
            pb.Inputs.Add(pistolDrawable);
            pb.Inputs.Add(pistolDrawable2);
            IProcess pistolProcess = pb.ToProcess();

            IEnumerable<IProcess> dependencies = pistolProcess.DependsOn();
            Assert.AreEqual(0, dependencies.Count());
        }

        /// <summary>
        /// Dependency tests for simple dependency involving two processes.
        /// </summary>
        [TestMethod]
        public void TestSimple()
        {
            CommandOptions opts = GetEmptyCommandOptions();
            this.Engine = new Engine(opts, opts.Config, opts.Project, opts.Branch, EngineFlags.None);
            Assert.AreEqual(EngineFlags.None, this.Engine.Flags);

            String exportPath = Path.Combine(this.Branch.Assets, "export");
            String resourcePath = this.Branch.Targets[RSG.Platform.Platform.PS3].ResourcePath;

            IContentTree tree = Factory.CreateEmptyTree(this.Branch);
            IContentNode pistolDrawable = tree.CreateFile(Path.Combine(exportPath, "models", "cdimages", "pistol_grip_mesh.idr.zip"));
            IContentNode pistolDrawable2 = tree.CreateFile(Path.Combine(resourcePath, "models", "cdimages", "pistol_grip_mesh.cdr"));
            ProcessBuilder pb = new ProcessBuilder("RSG.Pipeline.Processor.Platform.Rage", tree);
            pb.Inputs.Add(pistolDrawable);
            pb.Outputs.Add(pistolDrawable2);
            IProcess pistolProcess = pb.ToProcess();

            IContentNode pistolRpf = tree.CreateAsset(Path.Combine(resourcePath, "test.rpf"), RSG.Platform.Platform.PS3);
            ProcessBuilder pb2 = new ProcessBuilder("RSG.Pipeline.Processor.Platform.RpfCreateProcessor", tree);
            pb2.Inputs.Add(pistolDrawable2);
            IProcess rpfProcess = pb2.ToProcess();

            IEnumerable<IProcess> dependencies = pistolProcess.DependsOn();
            Assert.AreEqual(0, dependencies.Count());

            dependencies = pistolProcess.DependsOn(DependencyTrackMode.Immediate);
            Assert.AreEqual(0, dependencies.Count());

            dependencies = rpfProcess.DependsOn();
            Assert.AreEqual(1, dependencies.Count());
            IProcess dependentProcess = dependencies.ElementAt(0);
            Assert.AreSame(pistolProcess, dependentProcess);

            dependencies = rpfProcess.DependsOn(DependencyTrackMode.Immediate);
            Assert.AreEqual(1, dependencies.Count());
            dependentProcess = dependencies.ElementAt(0);
            Assert.AreSame(pistolProcess, dependentProcess);
        }

        /// <summary>
        /// Dependency testing for a 3 process chain; used to test tracking
        /// mode.
        /// </summary>
        [TestMethod]
        public void TestChain()
        {
            CommandOptions opts = GetEmptyCommandOptions();
            this.Engine = new Engine(opts, opts.Config, opts.Project, opts.Branch, EngineFlags.None);
            Assert.AreEqual(EngineFlags.None, this.Engine.Flags);

            String exportPath = Path.Combine(this.Branch.Assets, "export");
            String resourcePath = this.Branch.Targets[RSG.Platform.Platform.PS3].ResourcePath;

            IContentTree tree = Factory.CreateEmptyTree(this.Branch);
            IContentNode a = tree.CreateFile("a.txt");
            IContentNode b = tree.CreateFile("b.txt");
            IContentNode c = tree.CreateFile("c.txt");
            IContentNode d = tree.CreateFile("d.txt");

            ProcessBuilder pb1 = new ProcessBuilder("RSG.Pipeline.Processor.Common.Test.DummyProcessor", tree);
            pb1.Inputs.Add(a);
            pb1.Outputs.Add(b);
            IProcess p1 = pb1.ToProcess();

            ProcessBuilder pb2 = new ProcessBuilder("RSG.Pipeline.Processor.Common.Test.DummyProcessor", tree);
            pb2.Inputs.Add(b);
            pb2.Outputs.Add(c);
            IProcess p2 = pb2.ToProcess();

            ProcessBuilder pb3 = new ProcessBuilder("RSG.Pipeline.Processor.Common.Test.DummyProcessor", tree);
            pb2.Inputs.Add(c);
            pb2.Outputs.Add(d);
            IProcess p3 = pb3.ToProcess();

            IEnumerable<IProcess> dependencies = p1.DependsOn();
            Assert.AreEqual(0, dependencies.Count());
            dependencies = p1.DependsOn(DependencyTrackMode.Immediate);
            Assert.AreEqual(0, dependencies.Count());
            dependencies = p1.DependsOn(DependencyTrackMode.Recursive);
            Assert.AreEqual(0, dependencies.Count());

            dependencies = p2.DependsOn();
            Assert.AreEqual(1, dependencies.Count());
            dependencies = p2.DependsOn(DependencyTrackMode.Immediate);
            Assert.AreEqual(1, dependencies.Count());
            dependencies = p2.DependsOn(DependencyTrackMode.Recursive);
            Assert.AreEqual(1, dependencies.Count());

            dependencies = p3.DependsOn();
            Assert.AreEqual(1, dependencies.Count());
            dependencies = p3.DependsOn(DependencyTrackMode.Immediate);
            Assert.AreEqual(1, dependencies.Count());
            dependencies = p3.DependsOn(DependencyTrackMode.Recursive);
            Assert.AreEqual(2, dependencies.Count());

            // Test sorting the processes collection and the ProcessExecOrderComparer.
            List<IProcess> processes = new List<IProcess>(tree.Processes);
            RandomiseList(ref processes);

            processes.Sort(new ProcessExecOrderComparer());
            Assert.AreEqual(3, processes.Count);
            Assert.AreSame(p1, processes[0]);
            Assert.AreSame(p2, processes[1]);
            Assert.AreSame(p3, processes[2]);
        }

        /// <summary>
        /// Fisher–Yates shuffle.
        /// </summary>
        /// <param name="processes"></param>
        private void RandomiseList(ref List<IProcess> processes)
        {
            Random rng = new Random();   // i.e., java.util.Random.
            int n = processes.Count;        // The number of items left to shuffle (loop invariant).
            while (n > 1)
            {
                int k = rng.Next(n);  // 0 <= k < n.
                n--;                     // n is now the last pertinent index;
                IProcess temp = processes[n];     // swap array[n] with array[k] (does nothing if k == n).
                processes[n] = processes[k];
                processes[k] = temp;
            }
        }

        /// <summary>
        /// Returns empty command options
        /// </summary>
        /// <returns></returns>
        private CommandOptions GetEmptyCommandOptions()
        {
            String[] args = System.Environment.GetCommandLineArgs();
            String[] realArgs = args.Skip(1).ToArray();
            CommandOptions opts = new CommandOptions(realArgs, new RSG.Base.OS.LongOption[] { });
            return opts;
        }
    }

} // RSG.Pipeline.Tests namespace
