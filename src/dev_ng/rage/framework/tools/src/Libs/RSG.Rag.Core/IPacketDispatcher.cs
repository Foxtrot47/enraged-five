﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.Core
{
    /// <summary>
    /// 
    /// </summary>
    public interface IPacketDispatcher
    {
        /// <summary>
        /// Sends the specified packet.
        /// </summary>
        /// <param name="packet"></param>
        void SendPacket(RemotePacket packet);

        /// <summary>
        /// Sends the specified packet asynchronously.
        /// </summary>
        /// <param name="packet"></param>
        /// <returns></returns>
        Task SendPacketAsync(RemotePacket packet);
    } // IRemotePacketDispatcher
}
