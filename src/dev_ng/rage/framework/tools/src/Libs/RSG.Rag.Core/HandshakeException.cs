﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Rag.Core
{
    /// <summary>
    /// 
    /// </summary>
    public class HandshakeException : Exception
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="HandshakeException"/> class.
        /// </summary>
        public HandshakeException()
            : base()
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="HandshakeException"/> class with a specified error message.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public HandshakeException(String message)
            : base(message)
        {
        }
        #endregion // Constructor(s)
    } // HandshakeException
}
