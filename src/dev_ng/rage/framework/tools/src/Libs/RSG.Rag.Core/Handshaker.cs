﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using RSG.Base.Logging;

namespace RSG.Rag.Core
{
    /// <summary>
    /// Class that performs the initial handshake.
    /// </summary>
    public class Handshaker
    {
        #region Constants
        /// <summary>
        /// 
        /// </summary>
        private const float RAG_VERSION = 2.0f;
        #endregion // Constants

        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        private readonly INamedPipe _pipe;

        /// <summary>
        /// 
        /// </summary>
        private readonly int _basePort;

        /// <summary>
        /// 
        /// </summary>
        private readonly ILog _log;

        /// <summary>
        /// 
        /// </summary>
        private readonly HandshakeResult _result = new HandshakeResult();
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pipe"></param>
        /// <param name="basePort"></param>
        /// <param name="log"></param>
        public Handshaker(INamedPipe pipe, int basePort, ILog log)
        {
            _pipe = pipe;
            _basePort = basePort;
            _log = log;
        }
        #endregion // Constructor(s)

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public HandshakeResult PerformHandshake()
        {
            _log.Profile("Initial Handshake");
            bool handshakeComplete = false;
            while (!handshakeComplete)
            {
                // Wait a max of 4 seconds to retrieve each handshake packet.
                RemotePacket p = ReceivePacket(4000);
                if (p == null)
                {
                    throw new TimeoutException("Rag timed out waiting for the initial handshake packet.");
                }
                handshakeComplete = ProcessMessage(p);
            }
            _log.ProfileEnd();
            return _result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="timeout"></param>
        /// <returns></returns>
        private RemotePacket ReceivePacket(int timeout)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            do
            {
                if (_pipe.HasData)
                {
                    // Read in the header data.
                    byte[] headerData = new byte[RemotePacket.HEADER_SIZE];
                    _pipe.ReadData(headerData, RemotePacket.HEADER_SIZE, true);
                    RemotePacketHeader header = new RemotePacketHeader(headerData);

                    // Read in the storage data.
                    byte[] storage = new byte[RemotePacket.MAX_DATA_SIZE];
                    if (header.Length > 0)
                    {
                        _pipe.ReadData(storage, header.Length, true);
                    }

                    return new RemotePacket(header, storage);
                }
            } while (sw.ElapsedMilliseconds < timeout);

            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="packet"></param>
        /// <returns></returns>
        private bool ProcessMessage(RemotePacket packet)
        {
            bool handshakeComplete = false;

            switch ((HandshakePacketType)packet.Command)
            {
                case HandshakePacketType.InitialHandshake:
                    ProcessInitialHandshakePacket(packet);
                    break;

                case HandshakePacketType.NumApplications:
                    ProcessNumApplicationsPacket(packet);
                    break;

                case HandshakePacketType.ApplicationName:
                    ProcessApplicationNamePacket(packet);
                    break;

                case HandshakePacketType.ApplicationVisibleName:
                    ProcessApplicationVisibleNamePacket(packet);
                    break;

                case HandshakePacketType.ApplicationArgument:
                    ProcessApplicationArgumentPacket(packet);
                    break;

                case HandshakePacketType.BankPipeName:
                    ProcessBankPipeNamePacket(packet);
                    break;

                case HandshakePacketType.OutputPipeName:
                    ProcessOutputPipeNamePacket(packet);
                    break;

                case HandshakePacketType.EventPipeName:
                    ProcessEventPipeNamePacket(packet);
                    break;

                case HandshakePacketType.WindowHandle:
                    ProcessWindowHandlePacket(packet);
                    break;

                case HandshakePacketType.PlatformInfo:
                    ProcessPlatformInfoPacket(packet);
                    break;

                case HandshakePacketType.Ps3TargetAddress:
                    ProcessPS3TargetAddressPacket(packet);
                    break;

                case HandshakePacketType.BuildConfiguration:
                    ProcessBuildConfigurationPacket(packet);
                    break;

                case HandshakePacketType.HandshakeComplete:
                    handshakeComplete = true;
                    break;

                default:
                    throw new HandshakeException(String.Format("Unknown handshake packet '{0}' received.", packet.Command));
            }

            if (!packet.WasAllDataRead())
            {
                throw new HandshakeException(String.Format("Not all of the packet's contents were read."));
            }

            return handshakeComplete;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="packet"></param>
        private void ProcessInitialHandshakePacket(RemotePacket packet)
        {
            _log.Message("Received initial handshake packet.");
            float ragVersion = packet.ReadFloat();
            if (!packet.WasAllDataRead())
            {
                packet.ReadString();        // Application name, required for protocol but not used in this version of rag
            }
            
            _log.Message("RAG Version: {0}", ragVersion);

            if (ragVersion != RAG_VERSION)
            {
                String msg = String.Format("Rag and Rage bank code are out of sync.  Expected {0} but received {1} from Bank.\n\n" +
                    "Please update {2}.",
                    RAG_VERSION,
                    ragVersion,
                    (RAG_VERSION < ragVersion) ? "Rag" : "your game's bank code");
                throw new HandshakeException(msg);
            }

            // Send port/RAG version to the game.
            RemotePacketBuilder builder = new RemotePacketBuilder((ushort)HandshakePacketType.InitialHandshake, 0, 0);
            builder.Write(_basePort);
            builder.Write(RAG_VERSION);

            IPacketDispatcher dispatcher = new NamedPipePacketDispatcher(_pipe);
            dispatcher.SendPacket(builder.ToRemotePacket());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="packet"></param>
        private void ProcessNumApplicationsPacket(RemotePacket packet)
        {
            _log.Message("Received number of applications packet.");
            uint numApps = packet.ReadUInt32();
            _result.Applications = new ApplicationInfo[numApps];
            for (int i = 0; i < numApps; ++i)
            {
                _result.Applications[i] = new ApplicationInfo();
            }
            _result.MasterIndex = packet.ReadUInt32();

            Debug.Assert(_result.MasterIndex < numApps, "master index < numApps");
            if (_result.MasterIndex >= numApps)
            {
                throw new HandshakeException(String.Format("Master index is out of bounds.  Num Apps: {0}, Master Index: {1}", _result.MasterIndex, numApps));
            }
            _log.Message("NumApps: {0}", numApps);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="packet"></param>
        private void ProcessApplicationNamePacket(RemotePacket packet)
        {
            _log.Message("Received application name packet.");
            uint index = packet.ReadUInt32();
            String appName = packet.ReadString();
            _result.Applications[index].Name += appName;
            _log.Message("Application[{0}].Name: {1}", index, appName);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="packet"></param>
        private void ProcessApplicationVisibleNamePacket(RemotePacket packet)
        {
            _log.Message("Received visible application name packet.");
            uint index = packet.ReadUInt32();
            String visibleName = packet.ReadString();
            _result.Applications[index].VisibleName += visibleName;
            _log.Message("Application[{0}].VisibleName: {1}", index, visibleName);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="packet"></param>
        private void ProcessApplicationArgumentPacket(RemotePacket packet)
        {
            _log.Message("Received application arguments packet.");
            uint index = packet.ReadUInt32();
            String args = packet.ReadString();
            _result.Applications[index].Arguments += args;
            _log.Message("Application[{0}].Arguments: {1}", index, args);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="packet"></param>
        private void ProcessBankPipeNamePacket(RemotePacket packet)
        {
            _log.Message("Received bank pipe name packet.");
            uint index = packet.ReadUInt32();
            String name = packet.ReadString();
            _result.Applications[index].BankPipeName += name;
            _log.Message("Applications[{0}].BankPipeName: {1}", index, name);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="packet"></param>
        private void ProcessOutputPipeNamePacket(RemotePacket packet)
        {
            _log.Message("Received output pipe name packet.");
            uint index = packet.ReadUInt32();
            String name = packet.ReadString();
            _result.Applications[index].OutputPipeName += name;
            _log.Message("Applications[{0}].OutputPipeName: {1}", index, name);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="packet"></param>
        private void ProcessEventPipeNamePacket(RemotePacket packet)
        {
            _log.Message("Received events pipe name packet.");
            uint index = packet.ReadUInt32();
            String name = packet.ReadString();
            _result.Applications[index].EventPipeName += name;
            _log.Message("Applications[{0}].EventPipeName: {1}", index, name);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="packet"></param>
        private void ProcessWindowHandlePacket(RemotePacket packet)
        {
            _log.Message("Ignoring window handle packet.");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="packet"></param>
        private void ProcessPlatformInfoPacket(RemotePacket packet)
        {
            _log.Message("Received platform info packet.");
            uint index = packet.ReadUInt32();
            String platformString = packet.ReadString();
            _result.PlatformName = platformString;
            _log.Message("PlatformName: {0}", platformString);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="packet"></param>
        private void ProcessBuildConfigurationPacket(RemotePacket packet)
        {
            _log.Message("Received build config packet.");
            uint index = packet.ReadUInt32();
            String buildConfigString = packet.ReadString();
            _result.BuildConfigName = buildConfigString;
            _log.Message("BuildConfigName: {0}", buildConfigString);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="packet"></param>
        private void ProcessPS3TargetAddressPacket(RemotePacket packet)
        {
            _log.Message("Received ps3 target address packet.");
            uint index = packet.ReadUInt32();
            String ps3TargetAddr = packet.ReadString();
            _result.PS3TargetAddress = ps3TargetAddr;
            _log.Message("PS3TargetAddress: {0}", ps3TargetAddr);
        }
        #endregion // Methods
    } // Handshaker
}
