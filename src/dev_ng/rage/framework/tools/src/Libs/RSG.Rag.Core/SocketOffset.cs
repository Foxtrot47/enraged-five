﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.Core
{
    /// <summary>
    /// This list mirrors bkRemotePacket::EnumSocketOffset in rage/base/src/bank/packet.h
    /// Access additional offsets with User5+1 through User5+40.
    /// </summary>
    public enum SocketOffset
    {
        /// <summary>
        /// 
        /// </summary>
        Bank,

        /// <summary>
        /// 
        /// </summary>
        Output,

        /// <summary>
        /// 
        /// </summary>
        Events,

        /// <summary>
        /// 
        /// </summary>
        Profile,

        /// <summary>
        /// 
        /// </summary>
        User0,

        /// <summary>
        /// 
        /// </summary>
        User1,

        /// <summary>
        /// 
        /// </summary>
        User2,

        /// <summary>
        /// 
        /// </summary>
        User3,

        /// <summary>
        /// 
        /// </summary>
        User4,

        /// <summary>
        /// 
        /// </summary>
        User5
    } // SocketOffset
}
