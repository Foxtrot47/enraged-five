﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.Core
{
    /// <summary>
    /// Handshake packet command types.
    /// </summary>
    public enum HandshakePacketType
    {
        /// <summary>
        /// 
        /// </summary>
        InitialHandshake,

        /// <summary>
        /// 
        /// </summary>
        NumApplications,

        /// <summary>
        /// 
        /// </summary>
        ApplicationName,

        /// <summary>
        /// 
        /// </summary>
        ApplicationVisibleName,

        /// <summary>
        /// 
        /// </summary>
        ApplicationArgument,

        /// <summary>
        /// 
        /// </summary>
        BankPipeName,

        /// <summary>
        /// 
        /// </summary>
        OutputPipeName,

        /// <summary>
        /// 
        /// </summary>
        EventPipeName,

        /// <summary>
        /// 
        /// </summary>
        HandshakeComplete,

        /// <summary>
        /// 
        /// </summary>
        WindowHandle,

        /// <summary>
        /// 
        /// </summary>
        PlatformInfo,

        /// <summary>
        /// 
        /// </summary>
        Ps3TargetAddress,

        /// <summary>
        /// 
        /// </summary>
        BuildConfiguration
    } // HandshakePacketType
}
