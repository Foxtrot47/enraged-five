﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.Core
{
    /// <summary>
    /// 
    /// </summary>
    public class WidgetEventArgs : EventArgs
    {
        /// <summary>
        /// 
        /// </summary>
        private IWidget _widget;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="widget"></param>
        public WidgetEventArgs(IWidget widget)
            : base()
        {
            _widget = widget;
        }

        /// <summary>
        /// 
        /// </summary>
        public IWidget Widget
        {
            get { return _widget; }
        }
    } // WidgetEventArgs
}
