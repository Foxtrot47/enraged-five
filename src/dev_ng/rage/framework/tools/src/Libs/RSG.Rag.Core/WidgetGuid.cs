﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.Core
{
    /// <summary>
    /// 
    /// </summary>
    public class WidgetGuid : IEquatable<WidgetGuid>
    {
        #region Member Data
        /// <summary>
        /// Individual components of the guid.
        /// </summary>
        private char _a;
        private char _b;
        private char _c;
        private char _d;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="guid"></param>
        public WidgetGuid(int guid)
        {
            _a = Convert.ToChar(guid & 0xff);
            _b = Convert.ToChar((guid >> 8) & 0xff);
            _c = Convert.ToChar((guid >> 16) & 0xff);
            _d = Convert.ToChar((guid >> 24) & 0xff);
            //
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <param name="d"></param>
        public WidgetGuid(char a, char b, char c, char d)
        {
            _a = a;
            _b = b;
            _c = c;
            _d = d;
        }

        /// <summary>
        /// Explicit constructor.
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public static explicit operator WidgetGuid(int guid)
        {
            return new WidgetGuid(guid);
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int ToInteger()
        {
            return (int)(Convert.ToByte(_a) | (Convert.ToByte(_b) << 8) | (Convert.ToByte(_c) << 16) | (Convert.ToByte(_d) << 24));
        }
        #endregion // Public Methods

        #region Conversion Operator
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static explicit operator int(WidgetGuid guid)
        {
            return guid.ToInteger();
        }
        #endregion // Conversion Operator

        #region Object Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return String.Format("{0}{1}{2}{3}", _a, _b, _c, _d);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            WidgetGuid guid = obj as WidgetGuid;
            if (guid == null)
            {
                return false;
            }
            else
            {
                return Equals(guid);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return ToInteger();
        }
        #endregion // Object Overrides

        #region IEquatable<WidgetGuid> Implementation
        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>true if the current object is equal to the other parameter; otherwise, false.</returns>
        public bool Equals(WidgetGuid other)
        {
            if (other == null)
            {
                return false;
            }
            else
            {
                return (_a == other._a && _b == other._b && _c == other._c && _d == other._d);
            }
        }
        #endregion // IEquatable<WidgetGuid> Implementation
    } // WidgetGuid
}
