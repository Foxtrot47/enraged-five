﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.Core
{
    /// <summary>
    /// 
    /// </summary>
    public interface IWidgetResolver
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        IWidget GetWidgetById(uint widgetId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="widget"></param>
        void TrackWidget(IWidget widget);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="widget"></param>
        void WidgetDestroyed(IWidget widget);
    } // IWidgetResolver
}
