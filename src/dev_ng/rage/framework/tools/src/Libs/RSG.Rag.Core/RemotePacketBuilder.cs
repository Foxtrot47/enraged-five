﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.Core
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class RemotePacketBuilder
    {
        #region Member Data
        /// <summary>
        /// Header data.
        /// </summary>
        private RemotePacketHeader _header;

        /// <summary>
        /// Storage data.
        /// </summary>
        private byte[] _data;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public RemotePacketBuilder(ushort command, int guid, uint id)
        {
            _header = new RemotePacketHeader(command, guid, id);
            _data = new byte[RemotePacket.MAX_DATA_SIZE];
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public RemotePacket ToRemotePacket()
        {
            return new RemotePacket(_header, _data);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public void Write(byte value)
        {
            EnsureBufferHasSpaceFor(1);
            _data[_header.Length++] = value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public void Write(sbyte value)
        {
            Write((byte)value);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public void Write(bool value)
        {
            Write((byte)((value == true) ? 1 : 0));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public void Write(ushort value)
        {
            EnsureBufferHasSpaceFor(2);
            _data[_header.Length++] = (byte)(value);
            _data[_header.Length++] = (byte)(value >> 8);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public void Write(short value)
        {
            Write((ushort)value);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public void Write(uint value)
        {
            EnsureBufferHasSpaceFor(4);
            _data[_header.Length++] = (byte)(value);
            _data[_header.Length++] = (byte)(value >> 8);
            _data[_header.Length++] = (byte)(value >> 16);
            _data[_header.Length++] = (byte)(value >> 24);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public void Write(int value)
        {
            Write((uint)value);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public void Write(float value)
        {
            FloatToInt x;
            x.u = 0;
            x.f = value;
            Write(x.u);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public void Write(String value)
        {
            if (value == null)
            {
                EnsureBufferHasSpaceFor(1);
                _data[_header.Length++] = 0;
            }
            else
            {
                Debug.Assert(value.Length < 256, "String is longer than 255 characters.");
                EnsureBufferHasSpaceFor((uint)value.Length);

                _data[_header.Length++] = (byte)(value.Length + 1);
                for (int i = 0; i < value.Length; i++)
                {
                    _data[_header.Length++] = Convert.ToByte(value[i]);
                }
                _data[_header.Length++] = 0; // null terminator (is this needed?  we are including the string length...).
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public void WriteWidget(uint value)
        {
            WritePtr(value);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public void WritePtr(uint value)
        {
            Write((uint)value);
        }
        #endregion // Public Methods

        #region Private Methods

        /// <summary>
        /// Checks whether there is enough space in the buffer to accommodate the number of 
        /// bytes specified.
        /// </summary>
        /// <param name="bytes"></param>
        private void EnsureBufferHasSpaceFor(uint bytes)
        {
            Debug.Assert(_header.Length + bytes <= RemotePacket.MAX_DATA_SIZE,
                String.Format("Not enough space to accommodate {0} bytes of data. Currently using {1} of {2} bytes.",
                    bytes, _header.Length, RemotePacket.MAX_DATA_SIZE));
        }
        #endregion // Private Methods
    } // RemotePacketBuilder
}
