﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Rag.Core
{
    /// <summary>
    /// TODO: Clean up
    /// </summary>
    public class HandshakeResult
    {
        /// <summary>
        /// 
        /// </summary>
        public String PlatformName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public String PS3TargetAddress { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public String BuildConfigName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ApplicationInfo[] Applications { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ApplicationInfo MasterApplication
        {
            get { return Applications[MasterIndex]; }
        }


        /// <summary>
        /// 
        /// </summary>
        public uint MasterIndex { get; set; }
    }

    /// <summary>
    /// TODO: Clean up
    /// </summary>
    public class ApplicationInfo
    {

        /// <summary>
        /// 
        /// </summary>
        public ApplicationInfo()
        {
            Name = "";
            VisibleName = "";
            Arguments = "";
            BankPipeName = "";
            OutputPipeName = "";
            EventPipeName = "";
        }


        /// <summary>
        /// 
        /// </summary>
        public String Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public String VisibleName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public String Arguments { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public String BankPipeName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public String OutputPipeName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public String EventPipeName { get; set; }
    }
}
