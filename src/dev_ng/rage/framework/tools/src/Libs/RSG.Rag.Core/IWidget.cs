﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace RSG.Rag.Core
{
    /// <summary>
    /// 
    /// </summary>
    public interface IWidget : INotifyPropertyChanged
    {
        #region Properties
        /// <summary>
        /// Guid for this widget.
        /// </summary>
        WidgetGuid Guid { get; }

        /// <summary>
        /// Unique identifier for this widget as set by the game.
        /// </summary>
        uint Id { get; }

        /// <summary>
        /// Name of the widget.
        /// </summary>
        String Name { get; }

        /// <summary>
        /// Parent of this widget.
        /// </summary>
        IWidgetGroup Parent { get; }

        /// <summary>
        /// Full path to this widget.
        /// </summary>
        String Path { get; }

        /// <summary>
        /// Tooltip for this widget.
        /// </summary>
        String Tooltip { get; }

        /// <summary>
        /// Color to use for the widget's background.
        /// </summary>
        Color FillColor { get; }

        /// <summary>
        /// Flag indicating whether this widget is read-only.
        /// </summary>
        bool ReadOnly { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Processes the create message.
        /// </summary>
        /// <param name="e"></param>
        bool ProcessCreateMessage(RemotePacketEventArgs e);

        /// <summary>
        /// Processes an arbitrary message.
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        void ProcessMessage(RemotePacketEventArgs e);

        /// <summary>
        /// Called when the widget is being destroyed.
        /// </summary>
        void Destroy();
        #endregion // Methods
    } // IWidget
}
