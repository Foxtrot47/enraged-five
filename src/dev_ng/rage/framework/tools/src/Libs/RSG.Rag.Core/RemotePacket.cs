﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.Core
{
    /// <summary>
    /// 
    /// </summary>
    public class RemotePacket
    {
        #region Constants
        /// <summary>
        /// Max size of a packet.
        /// </summary>
        private const int PACKET_SIZE = 512;

        /// <summary>
        /// Size of the header.
        /// </summary>
        public const int HEADER_SIZE = 12;

        /// <summary>
        /// Max size of the data sent in the packet.
        /// </summary>
        public const int MAX_DATA_SIZE = PACKET_SIZE - HEADER_SIZE;
        #endregion // Constants

        #region Member Data
        /// <summary>
        /// Header data.
        /// </summary>
        private RemotePacketHeader _header;

        /// <summary>
        /// Storage data.
        /// </summary>
        private byte[] _data = new byte[MAX_DATA_SIZE];

        /// <summary>
        /// Offset that we're at while reading data out of the storage buffer.
        /// </summary>
        protected int _readOffset;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Creates a new remote packet from the specified buffer.
        /// </summary>
        /// <param name="buffer">Buffer which contains both the header and data.</param>
        /// <param name="offset">Offset at which to start reading from the buffer.</param>
        public RemotePacket(byte[] buffer, int offset)
        {
            int headerSize = Marshal.SizeOf(typeof(RemotePacketHeader));
            Debug.Assert(headerSize == HEADER_SIZE, "Serialised size differs from constant size.");
            _header = RawDeserialize<RemotePacketHeader>(buffer, offset);

            if (_header.Length > 0)
            {
                if (_header.Length > MAX_DATA_SIZE)
                {
                    throw new DataException("Header length is larger than the max data size.");
                }

                if (offset + headerSize + _header.Length > buffer.Length)
                {
                    String message = String.Format("Buffer not large enough to deserialise the data. Buffer size: {0}, Header size: {1}, Header length: {2}",
                        buffer.Length,
                        headerSize,
                        _header.Length);
                    Debug.Fail(message);
                    throw new IndexOutOfRangeException(message);
                }

                // Copy the data into our internal data buffer.
                Array.ConstrainedCopy(buffer, offset + headerSize, _data, 0, _header.Length);
            }
        }

        /// <summary>
        /// Creates a new remote packet with the specified header/data.
        /// </summary>
        public RemotePacket(RemotePacketHeader header, byte[] data)
        {
            Debug.Assert(data.Length == MAX_DATA_SIZE, "Invalid storage length.");
            _header = header;
            _data = data;
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public ushort Length
        {
            get { return _header.Length; }
        }

        /// <summary>
        /// 
        /// </summary>
        public ushort Command
        {
            get { return _header.Command; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int Guid
        {
            get { return _header.Guid; }
        }

        /// <summary>
        /// 
        /// </summary>
        public uint Id
        {
            get { return _header.Id; }
        }

        /// <summary>
        /// 
        /// </summary>
        public byte[] Data
        {
            get { return _data; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int ReadOffset
        {
            get { return _readOffset; }
        }
        #endregion // Properties

        #region Read Methods
        /// <summary>
        /// 
        /// </summary>
        public void ResetReadOffset()
        {
            _readOffset = 0;
        }

        /// <summary>
        /// 
        /// </summary>
        public bool WasAllDataRead()
        {
            return _readOffset == _header.Length;
        }

        /// <summary>
        /// 
        /// </summary>
        public void ReadToEnd()
        {
            _readOffset = _header.Length;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public byte ReadByte()
        {
            return _data[_readOffset++];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public sbyte ReadSByte()
        {
            return (sbyte)ReadByte();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool ReadBoolean()
        {
            return ReadByte() != 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ushort ReadUInt16()
        {
            ushort result = _data[_readOffset++];
            return (ushort)(result | (_data[_readOffset++] << 8));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public short ReadInt16()
        {
            return (short)ReadUInt16();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public uint ReadUInt32()
        {
            uint result = _data[_readOffset++];
            result |= (uint)(_data[_readOffset++] << 8);
            result |= (uint)(_data[_readOffset++] << 16);
            return result | (uint)(_data[_readOffset++] << 24);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int ReadInt32()
        {
            return (int)ReadUInt32();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public float ReadFloat()
        {
            FloatToInt x;
            x.f = 0.0f;
            x.u = ReadUInt32();
            return x.f;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public String ReadString()
        {
            if (_data[_readOffset] > 0)
            {
                int sl = ReadByte();
                char[] array = new char[sl - 1];
                for (int i = 0; i < sl - 1; i++)
                {
                    array[i] = Convert.ToChar(ReadByte());
                }
                ReadByte(); //read null character
                return new String(array);
            }
            else
            {
                ++_readOffset;
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public uint ReadWidget()
        {
            return ReadPtr();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public uint ReadPtr()
        {
            return (uint)ReadUInt32();
        }
        #endregion // Read Methods

        #region Serialisation
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public byte[] ToBuffer()
        {
            // Serialise the header data
            byte[] headerData = RawSerialize(_header);

            // Cache the lengths
            int headerLength = headerData.Length;
            int dataLength = _header.Length;

            // Create new buffer of the required size and copy the header and data into it
            byte[] buffer = new byte[RemotePacket.HEADER_SIZE + dataLength];

            try
            {
                System.Buffer.BlockCopy(headerData, 0, buffer, 0, RemotePacket.HEADER_SIZE);
                System.Buffer.BlockCopy(_data, 0, buffer, RemotePacket.HEADER_SIZE, dataLength);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return buffer;
        }

        /// <summary>
        /// Serialises an object to the byte array.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private byte[] RawSerialize<T>(T obj)
        {
            int rawsize = Marshal.SizeOf(obj);
            IntPtr buffer = Marshal.AllocHGlobal(rawsize);
            Marshal.StructureToPtr(obj, buffer, false);
            byte[] rawdata = new byte[rawsize];
            Marshal.Copy(buffer, rawdata, 0, rawsize);
            Marshal.FreeHGlobal(buffer);
            return rawdata;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        private T RawDeserialize<T>(byte[] data, int offset)
        {
            int size = Marshal.SizeOf(typeof(T));
            Debug.Assert(data.Length >= offset + size, "Not enough data in the buffer to parse object of type '{0}'.", typeof(T).Name);

            IntPtr buffer = Marshal.AllocHGlobal(size);
            Marshal.Copy(data, offset, buffer, size);

            T obj = (T)Marshal.PtrToStructure(buffer, typeof(T));
            Marshal.FreeHGlobal(buffer);
            return obj;
        }
        #endregion // Serialisation
    } // Remote Packet
}
