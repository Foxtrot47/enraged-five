﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;
using LitJson;
using RSG.Base.Logging;

namespace RSG.Rag.Core
{
    /// <summary>
    /// The purpose of this class is to manage a connection to the proxy. 
    /// Depending on the name you pass into the constructor, the proxy will 
    /// create an appropriate AppProxy. Currently only one is implemented:
    /// "Rag Proxy" will yield a RagProxy instance that you can connect Rag to.
    /// </summary>
    public class ProxyConnection
    {
        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        private ILog _log;

        /// <summary>
        /// 
        /// </summary>
        private int _localPort;

        /// <summary>
        /// 
        /// </summary>
        private int _remotePort;

        /// <summary>
        /// 
        /// </summary>
        private String _proxyName;

        /// <summary>
        /// 
        /// </summary>
        private TcpClient _client;

        /// <summary>
        /// 
        /// </summary>
        private int _proxyUID;

        /// <summary>
        /// Flag indicating whether we are connected to the proxy.
        /// </summary>
        private bool _proxyConnected;

        /// <summary>
        /// Flag indicating that the user initiated a disconnect (e.g. by shutting down the RAG window).
        /// </summary>
        private bool _userDisconnect;

        /// <summary>
        /// Thread used to check status of the connection to the RAG proxy.
        /// </summary>
        private Thread _checkProxyThread;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="proxyName"></param>
        /// <param name="log"></param>
        public ProxyConnection(String proxyName, ILog log)
        {
            _proxyName = proxyName;
            _log = log;
        }
        #endregion // Constructor(s)

        #region Events
        /// <summary>
        /// Event fired when the client gets disconnected from the proxy.
        /// </summary>
        public event EventHandler ProxyDisconnected;
        #endregion // Events

        #region Properties
        /// <summary>
        /// The local port used to communicate with the RAG proxy.
        /// </summary>
        public int LocalPort
        {
            get { return _localPort; }
        }

        /// <summary>
        /// The remote RAG proxy port that we are using for communication.
        /// TODO: RemotePort should be passed in via the constructor.
        /// </summary>
        public int RemotePort
        {
            get { return _remotePort; }
            set { _remotePort = value; }
        }

        /// <summary>
        /// The unique identifier of app connection listener we are connected to.
        /// </summary>
        public int ProxyUID
        {
            get { return _proxyUID; }
        }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Creates the connection to the proxy, reserving a local port until the rag pipe gets connected.
        /// </summary>
        /// <param name="basePort">The base port number to attempt to listen to data coming from the proxy.</param>
        /// <returns></returns>
        public ProxyLock ConnectToProxy(int basePort)
        {
            ProxyLock proxyLock = new ProxyLock(basePort);

            try
            {
                _localPort = proxyLock.Port;
                _client = CreateClientToProxy();
                NetworkStream stream = _client.GetStream();

                LitJson.JsonData jsonData = new LitJson.JsonData();
                jsonData["action"] = "connect";
                jsonData["name"] = _proxyName;
                jsonData["hostname"] = IPAddress.Loopback.ToString();
                jsonData["port"] = _localPort;
                string json = jsonData.ToJson();
                Byte[] data = Encoding.ASCII.GetBytes(json);

                _log.Message("Connected to proxy, sending JSON: {0}", json.Replace("{", "{{"));
                stream.Write(data, 0, data.Length);

                // Receive back ACK, including a UID
                data = new byte[128];
                int bytesRead = stream.Read(data, 0, data.Length);
                string jsonReceived = Encoding.ASCII.GetString(data, 0, bytesRead);
                LitJson.JsonData jsonDataReceived = LitJson.JsonMapper.ToObject(jsonReceived);

                string connectAck = (string)jsonDataReceived["connect"];
                _proxyUID = (int)jsonDataReceived["uid"];
                _log.Message("App connected to proxy: '{0}'[{1}] ", _proxyName, _proxyUID);

                _proxyConnected = true;
                _userDisconnect = false;

                _checkProxyThread = new Thread(VerifyProxyConnection);
                _checkProxyThread.Name = "ProxyConnection Check Thread";
                _checkProxyThread.IsBackground = true;
                _checkProxyThread.Start();
            }
            catch (Exception)
            {
                // Release the proxy lock and rethrow the exception.
                proxyLock.Release();
                throw;
            }

            return proxyLock;
        }

        /// <summary>
        /// Disconnects from the proxy.
        /// </summary>
        public void DisconnectFromProxy()
        {
            // Do a wait before the call to IsConnected because it might take a while
            // for the connection to die.
            Thread.Sleep(1000);
            if (!_proxyConnected)
            {
                _log.Message("ProxyConnection already disconnected.");
                return;
            }

            try
            {
                _userDisconnect = true;

                LitJson.JsonData jsonData = new LitJson.JsonData();
                jsonData["action"] = "disconnect";
                jsonData["name"] = _proxyName;
                jsonData["uid"] = _proxyUID;
                string json = jsonData.ToJson();
                Byte[] data = Encoding.ASCII.GetBytes(json);

                NetworkStream stream = _client.GetStream();
                stream.ReadTimeout = 5 * 1000;
                _log.Message(_proxyName + ": Sending disconnect");
                stream.Write(data, 0, data.Length);

                data = new byte[128];
                int bytesRead = stream.Read(data, 0, data.Length);
                string jsonReceived = Encoding.ASCII.GetString(data, 0, bytesRead);
                LitJson.JsonData jsonDataReceived = LitJson.JsonMapper.ToObject(jsonReceived);

                string disconnectAck = (string)jsonDataReceived["disconnect"];
                _log.Message(_proxyName + ": Received disconnect ACK: " + disconnectAck);

                stream.Close();
                _client.Close();
            }
            catch (SocketException ex)
            {
                _log.ToolException(ex, "ProxyConnection presumably already disconnected.");
            }
            catch (IOException ex)
            {
                _log.ToolException(ex, "ProxyConnection presumably already disconnected.");
            }
        }

        /// <summary>
        /// Thread method that checks the connection to the proxy, firing an event if the connection goes down.
        /// </summary>
        private void VerifyProxyConnection()
        {
            while (_proxyConnected && !_userDisconnect)
            {
                Thread.Sleep(500);
                UpdateConnectionStatus();
            }

            // Check whether we need to notify others about the disconnect.
            // We need to do this if it wasn't an user initiate disconnect so that the window can shut down.
            if (!_proxyConnected && ProxyDisconnected != null)
            {
                _log.Message("ProxyConnection lost connection to proxy.");
                _proxyConnected = false;
                ProxyDisconnected(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Updates the local flag containing the connection status.
        /// TODO: Could potential remove the member flag and return the result from this method instead.
        /// </summary>
        /// <returns></returns>
        private void UpdateConnectionStatus()
        {
            if (_proxyConnected)
            {
                try
                {
                    if (_client == null || !_client.Connected)
                    {
                        _proxyConnected = false;
                    }

                    if (_client.Client.Poll(1, SelectMode.SelectError))
                    {
                        _proxyConnected = false;
                    }

                    if (_client.Client.Poll(1, SelectMode.SelectRead))
                    {
                        /*
                         * From MSDN on Poll+SelectRead:
                         * true if Listen has been called and a connection is pending;
                         * -or-
                         * true if data is available for reading;
                         * -or-
                         * true if the connection has been closed, reset, or terminated; 
                        */
                        if (_client.Client.Available == 0)
                        {
                            _proxyConnected = false;
                        }
                    }
                }
                catch (System.Exception)
                {
                    _proxyConnected = false;
                }
            }
        }

        /// <summary>
        /// Creates a tcp client connected to the RAG Proxy's App Connection Listener proxy.
        /// </summary>
        /// <returns></returns>
        private TcpClient CreateClientToProxy()
        {
            int timeOutRetries = 10;
            int timeToWait = 3 * 1000;          // 3 seconds

            while (--timeOutRetries > 0)
            {
                try
                {
                    TcpClient client = new TcpClient(IPAddress.Loopback.ToString(), RemotePort);
                    client.NoDelay = true;
                    client.Client.ReceiveTimeout = 5 * 1000;
                    return client;
                }
                catch (SocketException ex)
                {
                    _log.ToolException(ex, "Could not connect to proxy.");
                }

                Thread.Sleep(timeToWait);
            }

            throw new TimeoutException("Timed out while waiting for proxy.");
        }
        #endregion // Methods
    } // ProxyConnection
}
