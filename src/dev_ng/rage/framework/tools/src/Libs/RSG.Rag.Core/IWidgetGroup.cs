﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.Core
{
    /// <summary>
    /// 
    /// </summary>
    public interface IWidgetGroup : IWidget
    {
        #region Properties
        /// <summary>
        /// Child widgets that this group contains.
        /// </summary>
        IReadOnlyCollection<IWidget> Children { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Adds a child to this widget.
        /// </summary>
        /// <param name="widget"></param>
        void AddChild(IWidget widget);

        /// <summary>
        /// Removes a child from this group.
        /// </summary>
        /// <param name="widget"></param>
        void RemoveChild(IWidget widget);

        /// <summary>
        /// Makes a note that something is actively making use of the widget.
        /// </summary>
        void AddOpenReference();

        /// <summary>
        /// Removes a reference from this widget.
        /// </summary>
        void RemoveOpenReference();
        #endregion // Methods
    } // IWidgetGroup
}
