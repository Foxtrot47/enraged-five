﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace RSG.Rag.Core
{
    /// <summary>
    /// Helper class for acquiring a free port number and locking it until it's needed.
    /// </summary>
    public class ProxyLock
    {
        #region Member Data
        /// <summary>
        /// Listener we managed to create from which we will "steal" the port.
        /// </summary>
        private TcpListener _listener;

        /// <summary>
        /// Static lock object for thread synchronisation.
        /// </summary>
        private static object _lockObj = new object();
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="basePort"></param>
        public ProxyLock(int basePort)
        {
            Monitor.Enter(_lockObj);
            try
            {
                _listener = AcquireFreePortNumber(basePort);
            }
            catch (Exception)
            {
                Monitor.Exit(_lockObj);
                throw;
            }
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// Port that the internal listener is listening on.
        /// </summary>
        public int Port
        {
            get { return (_listener != null ? ((IPEndPoint)_listener.LocalEndpoint).Port : -1); }
        }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Stops the listener and releases the monitor's lock.
        /// Call this method when you wish to reuse the listener's port.
        /// </summary>
        public void Release()
        {
            try
            {
                _listener.Stop();
            }
            finally
            {
                Monitor.Exit(_lockObj);
            }
        }

        /// <summary>
        /// Releases the listener from the proxy lock's control.
        /// </summary>
        /// <returns></returns>
        public TcpListener SurrenderListener()
        {
            Monitor.Exit(_lockObj);
            return _listener;
        }

        /// <summary>
        /// Attempts to create a listener at the first available port after basePort.
        /// </summary>
        /// <param name="basePort"></param>
        /// <returns></returns>
        private TcpListener AcquireFreePortNumber(int basePort)
        {
            int portNumber = basePort;
            while (portNumber < basePort + 1000)
            {
                // Erm...
                ++portNumber;

                try
                {
                    TcpListener listener = new TcpListener(IPAddress.Any, portNumber);
                    listener.Start();
                    return listener;
                }
                catch (Exception)
                {
                }

                Thread.Sleep(10);
            }

            throw new Exception("Could not acquire free port number.");
        }
        #endregion // Methods
    } // ProxyLock
}
