﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.Core
{
    /// <summary>
    /// Summary description for PipeID.
    /// </summary>
    public class PipeId
    {
        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        private String _name;

        /// <summary>
        /// 
        /// </summary>
        private IPAddress _address;

        /// <summary>
        /// 
        /// </summary>
        private int _port;

        /// <summary>
        /// 
        /// </summary>
        private TcpListener _tcpListener;
        #endregion // Member Data

        #region Properties
        /// <summary>
        /// Name of the pipe.
        /// </summary>
        public String Name
        {
            get { return _name; }
        }

        /// <summary>
        /// Address to connect to.
        /// </summary>
        public IPAddress Address
        {
            get { return _address; }
        }

        /// <summary>
        /// Port to connect on.
        /// </summary>
        public int Port
        {
            get { return _port; }
        }

        /// <summary>
        /// Optional listener that this pipe is already using.
        /// </summary>
        public TcpListener Listener
        {
            get { return _tcpListener; }
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="addr"></param>
        /// <param name="port"></param>
        /// <param name="tcpListener"></param>
        public PipeId(String name, IPAddress addr, int port, TcpListener tcpListener)
        {
            _name = name;
            _address = addr;
            _port = port;
            _tcpListener = tcpListener;
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// Adds a postfix to the pipes name.
        /// </summary>
        /// <param name="postfix"></param>
        public void AppendNamePostfix(String postfix)
        {
            if (_name == null)
            {
                _name = postfix;
            }
            else
            {
                _name += postfix;
            }
        }
        #endregion // Public Methods

        #region Object Overrides
        /// <summary>
        /// 
        /// </summary>
        public override String ToString()
        {
            if (_name != null)
            {
                return String.Format("{0}@{1}:{2}", _name, _address, _port);
            }
            else
            {
                return String.Format("{0}:{1}", _address, _port);
            }
        }
        #endregion // Object Overrides
    } // PipeId
}
