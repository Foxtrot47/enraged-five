﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LitJson;
using System.Net.Sockets;
using System.Threading;
using System.IO;
using RSG.Base.Logging;

namespace RSG.Rag.Core
{
    /// <summary>
    /// Utility for parsing JSON data from a network socket.
    /// TODO: Sort this out.
    /// </summary>
    public static class JsonNetUtil
    {
        /// <summary>
        /// Reads data from a network stream and attempts to convert it to JSON.
        /// </summary>
        public static JsonData GetJsonFromConnection(NetworkStream socket, int timeOutMilliseconds, ILog log)
        {
            // Make sure we can read from the network stream
            if (socket.CanRead)
            {
                String jsonStr = "";
                byte[] data = new byte[1024];

                while (timeOutMilliseconds > 0)
                {
                    try
                    {
                        // Make sure there is data available on the socket.
                        // Read blocks if no data is available which could lead to this method hanging.
                        if (socket.DataAvailable)
                        {
                            int bytesRead = socket.Read(data, 0, data.Length);
                            jsonStr += System.Text.Encoding.UTF8.GetString(data, 0, bytesRead);
                        }
                    }
                    catch (SocketException ex)
                    {
                        if (ex.SocketErrorCode == SocketError.WouldBlock ||
                            ex.SocketErrorCode == SocketError.IOPending ||
                            ex.SocketErrorCode == SocketError.NoBufferSpaceAvailable)
                        {
                            // socket buffer is probably empty, wait and try again
                            Thread.Sleep(30);
                        }
                    }
                    catch (IOException ex)
                    {
                        log.ToolException(ex, "");
                        break;
                    }

                    if (jsonStr == "")
                    {
                        Thread.Sleep(10);
                        timeOutMilliseconds -= 10; // not exactly exact but decent enough
                        continue;
                    }

                    // Try and convert the string to JSON.
                    LitJson.JsonData jsonData;
                    try
                    {
                        LitJson.JsonReader reader = new LitJson.JsonReader(jsonStr);
                        jsonData = LitJson.JsonMapper.ToObject(jsonStr);
                        log.Message("Received JSON: {0}", jsonStr.Replace("{", "{{"));
                    }
                    catch
                    {
                        // Fine to ignore exception since the whole JSON string may
                        // not have come through yet.
                        timeOutMilliseconds -= 10;      // Make sure the timeout is decreased as we may have been sent duff JSON
                        continue;
                    }

                    return jsonData;
                }
            }

            return null;
        }
    } // JsonNetUtil
}
