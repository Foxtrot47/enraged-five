﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Rag.Core
{
    /// <summary>
    /// 
    /// </summary>
    public enum BankCommand
    {
        /// <summary>
        /// 
        /// </summary>
        Create,

        /// <summary>
        /// 
        /// </summary>
        Destroy,

        /// <summary>
        /// 
        /// </summary>
        Changed,

        /// <summary>
        /// 
        /// </summary>
        FillColor,
        
        /// <summary>
        /// 
        /// </summary>
        ReadOnly,

        /// <summary>
        /// 
        /// </summary>
        User0,

        /// <summary>
        /// 
        /// </summary>
        User1,

        /// <summary>
        /// 
        /// </summary>
        User2,

        /// <summary>
        /// 
        /// </summary>
        User3,

        /// <summary>
        /// 
        /// </summary>
        User4,

        /// <summary>
        /// 
        /// </summary>
        User5,

        /// <summary>
        /// 
        /// </summary>
        User6,

        /// <summary>
        /// 
        /// </summary>
        User7,

        /// <summary>
        /// 
        /// </summary>
        User8,

        /// <summary>
        /// 
        /// </summary>
        User9,

        /// <summary>
        /// 
        /// </summary>
        User10,

        /// <summary>
        /// 
        /// </summary>
        User11,

        /// <summary>
        /// 
        /// </summary>
        User12,

        /// <summary>
        /// 
        /// </summary>
        User13,

        /// <summary>
        /// 
        /// </summary>
        User14,

        /// <summary>
        /// 
        /// </summary>
        User15,

        /// <summary>
        /// 
        /// </summary>
        User16,

        /// <summary>
        /// 
        /// </summary>
        User17,

        /// <summary>
        /// 
        /// </summary>
        User18,

        /// <summary>
        /// 
        /// </summary>
        User19
    } // BankCommand
}
