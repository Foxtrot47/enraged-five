﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.Core
{
    /// <summary>
    /// Event arguments for handling a remote packet.
    /// </summary>
    public class RemotePacketEventArgs : EventArgs
    {
        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        private RemotePacket _packet;
        
        /// <summary>
        /// 
        /// </summary>
        private IList<String> _errorMessages = new List<String>();
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="packet"></param>
        public RemotePacketEventArgs(RemotePacket packet)
        {
            _packet = packet;
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// Remote packet these event args relate to.
        /// </summary>
        public RemotePacket Packet
        {
            get { return _packet; }
        }

        /// <summary>
        /// Flag indicating whether the packet was successfully processed.
        /// </summary>
        public bool Handled
        {
            get { return !_errorMessages.Any(); }
        }

        /// <summary>
        /// 
        /// </summary>
        public IReadOnlyCollection<String> ErrorMessages
        {
            get { return new ReadOnlyCollection<String>(_errorMessages); }
        }
        #endregion // Properties

        #region Public Methods
        /// <summary>
        /// Adds an error message.
        /// </summary>
        /// <param name="error"></param>
        /// <param name="args"></param>
        public void AddErrorMessage(String error, params Object[] args)
        {
            _errorMessages.Add(String.Format(error, args));
        }
        #endregion // Public Methods
    } // RemotePacketEventArgs
}
