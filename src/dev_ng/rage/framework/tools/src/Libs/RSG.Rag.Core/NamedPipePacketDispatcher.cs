﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.Core
{
    /// <summary>
    /// Dispatches messages via a named pipe.
    /// </summary>
    public class NamedPipePacketDispatcher : IPacketDispatcher
    {
        #region Member Data
        /// <summary>
        /// Pipe to use to send data.
        /// </summary>
        private INamedPipe _pipe;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pipe"></param>
        public NamedPipePacketDispatcher(INamedPipe pipe)
        {
            _pipe = pipe;
        }
        #endregion // Constructor(s)

        #region IRemotePacketDispatcher Implementation
        /// <summary>
        /// Sends the specified packet.
        /// </summary>
        /// <param name="packet"></param>
        public void SendPacket(RemotePacket packet)
        {
            byte[] data = packet.ToBuffer();
            _pipe.WriteDataAsync(data, data.Length);
        }

        /// <summary>
        /// Sends the specified packet asynchronously.
        /// </summary>
        /// <param name="packet"></param>
        public async Task SendPacketAsync(RemotePacket packet)
        {
            byte[] data = packet.ToBuffer();
            await _pipe.WriteDataAsync(data, data.Length);
        }
        #endregion // IRemotePacketDispatcher Implementation
    } // NamedPipeDispatcher
}
