﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.Core
{
    /// <summary>
    /// Helper struct for byte casting a float to an int.
    /// </summary>
    [StructLayout(LayoutKind.Explicit)]
    public struct FloatToInt
    {
        /// <summary>
        /// Float value.
        /// </summary>
        [FieldOffset(0)]
        public float f;

        /// <summary>
        /// Unsigned integer value.
        /// </summary>
        [FieldOffset(0)]
        public uint u;

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="f"></param>
        public FloatToInt(float f)
        {
            this.u = 0;
            this.f = f;
        }
    } // FloatToInt
}
