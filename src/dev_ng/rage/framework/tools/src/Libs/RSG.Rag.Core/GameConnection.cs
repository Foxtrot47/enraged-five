﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using LitJson;
using RSG.Base.Logging;

namespace RSG.Rag.Core
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class GameConnection : IEquatable<GameConnection>
    {
        #region Constants
        /// <summary>
        /// Port that the proxy listens on for game connection information.
        /// </summary>
        private const int CONNECTION_SERVICE_PORT = 10000;
        #endregion // Constants

        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        private String _platform;

        /// <summary>
        /// 
        /// </summary>
        private String _ipAddress;

        /// <summary>
        /// 
        /// </summary>
        private int _port;

        /// <summary>
        /// 
        /// </summary>
        private int _consolePort;

        /// <summary>
        /// 
        /// </summary>
        private bool _default;
        #endregion // Member Data

        #region Static Members
        /// <summary>
        /// 
        /// </summary>
        [Obsolete("Don't make use of this.  It's nastey!")]
        public static GameConnection CurrentClientGameConnection = null;

        /// <summary>
        /// Private field for the <see cref="ConnectionServicePort"/> property.
        /// </summary>
        private static int _connectionServicePort = CONNECTION_SERVICE_PORT;
        #endregion // Static Members

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        private GameConnection(JsonData gameData)
        {
            _ipAddress = (String)gameData["gameip"];
            _platform = (String)gameData["gameplatform"];
            _port = (int)gameData["port"];
            _consolePort = (int)gameData["consoleport"];
            if (gameData.ContainsKey("default"))
            {
                _default = (bool)gameData["default"];
            }
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// Platform that the game is running on.
        /// TODO: Convert to use a RSG.Platform.Platform enum object.
        /// </summary>
        public String Platform
        {
            get { return _platform; }
        }

        /// <summary>
        /// IP of the console the game is running on.
        /// TODO: Convert to use the IPAddress class.
        /// </summary>
        public String IPAddress
        {
            get { return _ipAddress; }
        }

        /// <summary>
        /// Port the app connection listener is listening on.
        /// </summary>
        public int Port
        {
            get { return _port; }
        }

        /// <summary>
        /// Port the console proxy is listening on.
        /// </summary>
        public int ConsolePort
        {
            get { return _consolePort; }
        }

        /// <summary>
        ///  Flag indicating whether the connection is connected.
        /// </summary>
        public bool Connected
        {
            get { return _ipAddress != "<unconnected>"; }
        }

        /// <summary>
        /// Flag indicating whether this is the default connection.
        /// </summary>
        public bool Default
        {
            get { return _default; }
        }

        /// <summary>
        /// The port that we're listening on for game list requests.
        /// </summary>
        public static int ConnectionServicePort
        {
            get { return _connectionServicePort; }
            set { _connectionServicePort = value; }
        }
        #endregion // Properties

        #region Object Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(Object obj)
        {
            GameConnection gc = obj as GameConnection;
            if (gc != null)
            {
                return Equals(gc);
            }
            else
            {
                return false;
            }
        }
    
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return String.Format("{0} - {1}:{2}", Platform, IPAddress, ConsolePort);
        }
    
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return IPAddress.GetHashCode();
        }
        #endregion // Object Overrides
    
        #region IEquatable<GameConnection> Implementation
        /// <summary>
        /// Checks for equality of multiple game connections
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(GameConnection other)
        {
            return (this.Platform == other.Platform &&
                this.IPAddress == other.IPAddress &&
                //this.Port == other.Port &&
                this.ConsolePort == other.ConsolePort &&
                this.Connected == other.Connected);
        }
        #endregion // IEquatable<GameConnection> Implementation

        #region Static Methods
        /// <summary>
        /// Retrives the list of games that are currently connected to the proxy.
        /// </summary>
        /// <returns></returns>
        public static IList<GameConnection> AcquireGameList(ILog log)
        {
            IList<GameConnection> result = new List<GameConnection>();

            TcpClient client;
            try
            {
                client = new TcpClient(System.Net.IPAddress.Loopback.ToString(), ConnectionServicePort);
            }
            catch (SocketException ex)
            {
                log.ToolException(ex, "AcquireGameList: Could not connect to {0}.", ConnectionServicePort);
                return result;
            }

            String connectAction = "{\"action\":\"getgamelist\"}";
            byte[] data = Encoding.UTF8.GetBytes(connectAction);
            client.GetStream().Write(data, 0, data.Length);

            log.Message("AcquireGameList: Querying proxy for gamelist on port {0}", ConnectionServicePort);
            JsonData jsonData = JsonNetUtil.GetJsonFromConnection(client.GetStream(), 1000, log);
            if (jsonData == null)
            {
                log.Error("AcquireGameList: Failed to retrieve JSON data.");
                return new List<GameConnection>();
            }

            foreach (LitJson.JsonData gameData in jsonData["hubs"])
            {
                result.Add(new GameConnection(gameData));
            }

            return result;
        }
        #endregion // Static Methods
    } // Game Connection
}
