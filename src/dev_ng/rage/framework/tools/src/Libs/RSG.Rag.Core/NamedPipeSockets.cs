using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Security.Permissions;
using System.Text;
using System.Threading;
using System.Net.NetworkInformation;
using RSG.Base.Logging;
using System.Threading.Tasks;

namespace RSG.Rag.Core
{
    /// <summary>
    /// Summary description for NamedPipeSocket.
    /// </summary>
    public class NamedPipeSocket : INamedPipe
    {
        #region Constants
        /// <summary>
        /// 15 second timeout.
        /// </summary>
        private const int PIPE_TIMEOUT = 15 * 1000;
        #endregion // Constants

        #region Member Data
        /// <summary>
        /// Log object.
        /// </summary>
        private ILog _log;

        /// <summary>
        /// Name of the pipe.
        /// </summary>b
        private String _name;

        /// <summary>
        /// Flag that holds the result of the last connection test result.
        /// </summary>
        private bool _isConnected;

        /// <summary>
        /// Time that we last checked the connection state.
        /// </summary>
        private DateTime _lastConnectedTime;

        /// <summary>
        /// Underlying socket that is used for the connection.
        /// </summary>
        private Socket _socket;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Creates a named pipe socket that still needs to be connected.
        /// </summary>
        /// <param name="log"></param>
        public NamedPipeSocket(ILog log)
        {
            _log = log;
        }
        
        /// <summary>
        /// Creates an already connected named pipe socket.
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="log"></param>
        public NamedPipeSocket(Socket socket, ILog log)
            : this(log)
        {
            _socket = socket;
            _isConnected = _socket.Connected;
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// Name of the pipe.
        /// </summary>
        public String Name
        {
            get { return _name; }
        }

        /// <summary>
        /// Whether the pipe is connected or not.
        /// </summary>
        public bool Connected
        {
            get
            {
                RefreshConnectionStatus();
                return _isConnected;
            }
        }

        /// <summary>
        /// Sets whether the underlying socket should use the nagle algorithm.
        /// </summary>
        public bool NoDelay
        {
            get
            {
                return (_socket != null ? _socket.NoDelay : false);
            }
            set
            {
                if (_socket != null)
                {
                    _socket.NoDelay = value;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool HasData
        {
            get { return NumBytesAvailable != 0; }
        }

        /// <summary>
        /// Retrieves the number of bytes that are available for read from the underlying socket.
        /// </summary>
        public int NumBytesAvailable
        {
            get
            {
                if (_isConnected && _socket != null)
                {
                    return _socket.Available;
                }
                else
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public IPAddress LocalIP
        {
            get
            {
                if (_socket != null)
                {
                    return ((IPEndPoint)_socket.LocalEndPoint).Address;
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public IPAddress RemoteIP
        {
            get
            {
                if (_socket != null)
                {
                    return ((IPEndPoint)_socket.RemoteEndPoint).Address;
                }
                else
                {
                    return null;
                }
            }
        }
        #endregion // Properties

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected delegate Socket AcceptSocketDel();
        private ManualResetEvent _createResetEvent = new ManualResetEvent(false);
        private TcpListener _listener;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pipeId"></param>
        /// <param name="wait"></param>
        /// <returns></returns>
        public bool Create(PipeId pipeId, bool wait)
        {
            _log.Message("NamedPipeSocket: Creating pipe " + pipeId.ToString());
            _isConnected = false;
            _name = pipeId.ToString();

            // try to get a connection;
            if (_listener == null)
            {
                if (pipeId.Listener != null)
                {
                    _listener = pipeId.Listener;
                }
                else
                {
                    _listener = new TcpListener(pipeId.Address, pipeId.Port);
                    _listener.Start();
                }

                AcceptSocketDel del = new AcceptSocketDel(_listener.AcceptSocket);
                IAsyncResult result = del.BeginInvoke(null, null);

                // Either wait indefinitely or for a given timeout period to accept a socket
                // or until another thread asks to cancel.
                WaitHandle[] handles = new WaitHandle[] { _createResetEvent, result.AsyncWaitHandle };
                int handleIndex = 0;
                if (wait)
                {
                    handleIndex = WaitHandle.WaitAny(handles);
                }
                else
                {
                    handleIndex = WaitHandle.WaitAny(handles, PIPE_TIMEOUT);
                }

                if (handleIndex == WaitHandle.WaitTimeout)
                {
                    _log.Error("INamedPipe: Timed out while waiting for connection to port " + pipeId.Port.ToString());
                    return false;
                }
                else if (handleIndex == 0)
                {
                    _log.Warning("INamedPipe: Reset event received while waiting for connection to port " + pipeId.Port.ToString());
                    //m_CreateResetEvent.Reset();
                    return false;
                }

                _socket = del.EndInvoke(result);
                if (_socket != null)
                {
                    // Won't use this yet but I'll leave it in commented out for future use if necessary.
                    LingerOption lingerOption = new LingerOption(false, 0);
                    _socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Linger, lingerOption);

                    //m_Socket.SetSocketOption( SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true );
                    _listener.Server.Close();
                    _listener.Stop();
                    _isConnected = true;

                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        public void CancelCreate()
        {
            if (_listener == null)
            {
                return;
            }

            if (!_isConnected)
            {
                _log.Message("Named pipe '{0}' cancelling Create()");
                _createResetEvent.Set();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void Close()
        {
            _log.Message("NamedPipeSocket: Closing '{0}'", Name);
            _isConnected = false;

            if (_socket != null)
            {
                try
                {
                    if (_socket.Connected)
                    {
                        _socket.Shutdown(SocketShutdown.Both);
                    }
                }
                catch (System.Exception ex)
                {
                    _log.ToolException(ex, "Caught an exception while trying to shutdown socket.");
                }
                finally
                {
                    _socket.Close();
                }
            }

            if (_listener != null)
            {
                try
                {
                    if (_listener.Server.Connected)
                    {
                        _listener.Server.Shutdown(SocketShutdown.Both);
                    }
                }
                catch (System.Exception ex)
                {
                    _log.ToolException(ex, "Caught an exception while trying to shutdown socket.");
                }
                finally
                {
                    _listener.Server.Close();
                    _listener.Stop();
                    _listener = null;
                }
            }

            _log.Message("NamedPipeSocket: Closed '{0}'", Name);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="numBytesToRead"></param>
        /// <param name="exact"></param>
        /// <returns></returns>
        public int ReadData(byte[] buffer, int numBytesToRead, bool exact)
        {
            int numBytesRead = 0;
            try
            {
                lock (_socket)
                {
                    // If Receive returns 0 bytes it means that the remote end has closed the socket.
                    int thisRead;
                    do
                    {
                        thisRead = _socket.Receive(buffer, numBytesRead, numBytesToRead, SocketFlags.None);
                        numBytesRead += thisRead;
                        numBytesToRead -= thisRead;
                    } while (exact && numBytesToRead > 0 && thisRead > 0);
                }
            }
            catch (Exception)
            {
                //OnReadWriteFailure(this, e);
                _isConnected = false;
            }

            //OnDataReceived(buffer, numBytesRead);

            return numBytesRead;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="numBytesToWrite"></param>
        /// <returns></returns>
        public int WriteData(byte[] buffer, int numBytesToWrite)
        {
            try
            {
                lock (_socket)
                {
                    _socket.Send(buffer, numBytesToWrite, SocketFlags.None);
                }
            }
            catch (Exception)
            {
                //OnReadWriteFailure(this, e);
                _isConnected = false;
                return 0;
            }

            return numBytesToWrite;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="numBytesToWrite"></param>
        /// <returns></returns>
        public async Task<int> WriteDataAsync(byte[] buffer, int numBytesToWrite)
        {
            return await Task.Factory.StartNew(() => WriteData(buffer, numBytesToWrite));
        }
        #endregion // Public Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private void RefreshConnectionStatus()
        {
            // If we've already detected a disconnect don't bother checking again.
            if (!_isConnected)
            {
                return;
            }

            // Don't "spam" the connection unnecessarily: If it was connected one second ago, it
            // probably still is. May return true even though it's actually false, but 
            // I *think* that shouldn't be a big issue. 
            lock (_socket)
            {
                TimeSpan timeSinceLastConnectionCheck = DateTime.Now - _lastConnectedTime;
                if (timeSinceLastConnectionCheck > TimeSpan.FromSeconds(1))
                {
                    _lastConnectedTime = DateTime.Now;
                }
                else
                {
                    return;
                }
            }

            try
            {
                // checks to see if we're disconnected:
                if (_socket.Connected == false)
                {
                    _isConnected = false;
                    return;
                }

                // according to the net, this is the best way to check for a disconnect - we poll and then peek:
                else
                {
                    // Need to lock the socket, otherwise if there's data on the socket, Poll will return true. 
                    // If another thread then manages to read that data before the call to Receive here, then
                    // the socket will be empty, and it'll look as though it's been disconnected.
                    lock (_socket)
                    {
                        if (_socket.Poll(0, SelectMode.SelectRead))
                        {
                            byte[] dummy = new byte[1];
                            _socket.ReceiveTimeout = 5 * 1000; // will cause a SocketException if timed out

                            try
                            {
                                Int32 data = _socket.Receive(dummy, 1, SocketFlags.Peek);
                                if (data == 0)
                                {
                                    _isConnected = false;
                                    return;
                                }
                            }
                            catch (SocketException ex)
                            {
                                _log.ToolException(ex, "Named pipe '{0}' timed out during Socket.Receive test. Connected={1}", Name, _socket.Connected);
                            }
                        }
                    }
                }
            }
            catch (SocketException ex)
            {
                _log.ToolException(ex, "Named pipe '{0}' disconnected.", Name);
                _isConnected = false;
                return;
            }
        }
        #endregion // Private Methods
    } // NamedPipeSocket
}
