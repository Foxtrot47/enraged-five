﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.Core
{
    /// <summary>
    /// 
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, Inherited=false, AllowMultiple=false)]
    public class WidgetGuidAttribute : Attribute
    {
        #region Member Data
        /// <summary>
        /// The private field used for the <see cref="Guid"/> property.
        /// </summary>
        private WidgetGuid _guid;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <param name="d"></param>
        public WidgetGuidAttribute(char a, char b, char c, char d)
        {
            _guid = new WidgetGuid(a, b, c, d);
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// Gets the GUID of the target.
        /// </summary>
        public WidgetGuid Guid
        {
            get { return _guid; }
        }
        #endregion // Properties
    } // WidgetGuidAttribute
}
