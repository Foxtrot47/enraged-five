﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.Core
{
    /// <summary>
    /// Header that is sent for all remote packets.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct RemotePacketHeader
    {
        #region Member Data
        /// <summary>
        /// Length of the data (*not* including the twelve-byte header).
        /// </summary>
        public ushort Length;

        /// <summary>
        /// Command associated with this message.
        /// </summary>
        public ushort Command;

        /// <summary>
        /// Guid of the class that knows how to process the data sent in the packet.
        /// </summary>
        public int Guid;

        /// <summary>
        /// Id of the object that sent this message.
        /// </summary>
        public uint Id;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <param name="guid"></param>
        /// <param name="id"></param>
        internal RemotePacketHeader(ushort command, int guid, uint id)
        {
            Length = 0;
            Command = command;
            Guid = guid;
            Id = id;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="headerData"></param>
        internal RemotePacketHeader(byte[] headerData)
        {
            using (MemoryStream stream = new MemoryStream(headerData))
            {
                using (BinaryReader reader = new BinaryReader(stream))
                {
                    Length = reader.ReadUInt16();
                    Command = reader.ReadUInt16();
                    Guid = reader.ReadInt32();
                    //Id = reader.ReadUInt32();
                    Id = 0;
                }
            }
        }
        #endregion // Constructor(s)
    } // RemotePacketHeader
}
