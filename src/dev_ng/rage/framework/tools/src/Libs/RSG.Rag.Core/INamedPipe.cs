﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.Core
{
    /// <summary>
    /// 
    /// </summary>
    public interface INamedPipe
    {
        #region Properties
        /// <summary>
        /// Name of the pipe.
        /// </summary>
        String Name { get; }

        /// <summary>
        /// Whether the pipe is connected or not.
        /// </summary>
        bool Connected { get; }

        /// <summary>
        /// Sets whether the underlying socket should use the nagle algorithm.
        /// </summary>
        bool NoDelay { get; set; }

        /// <summary>
        /// Flag indicating that there is data available to read from the underlying socket.
        /// </summary>
        bool HasData { get; }

        /// <summary>
        /// Retrieves the number of bytes that are available for read from the underlying socket.
        /// </summary>
        int NumBytesAvailable { get; }

        /// <summary>
        /// 
        /// </summary>
        IPAddress LocalIP { get; }

        /// <summary>
        /// 
        /// </summary>
        IPAddress RemoteIP { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pipeId"></param>
        /// <param name="wait"></param>
        /// <returns></returns>
        bool Create(PipeId pipeId, bool wait);

        /// <summary>
        /// 
        /// </summary>
        void CancelCreate();
        
        /// <summary>
        /// 
        /// </summary>
        void Close();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="numBytesToRead"></param>
        /// <param name="exact"></param>
        /// <returns></returns>
        int ReadData(byte[] buffer, int numBytesToRead, bool exact);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="numBytesToWrite"></param>
        /// <returns></returns>
        int WriteData(byte[] buffer, int numBytesToWrite);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="numBytesToWrite"></param>
        /// <returns></returns>
        Task<int> WriteDataAsync(byte[] buffer, int numBytesToWrite);
        #endregion // Methods
    } // INamedPipe
}
