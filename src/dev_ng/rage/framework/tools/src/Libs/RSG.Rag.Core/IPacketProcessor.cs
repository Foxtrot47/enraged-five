﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.Core
{
    /// <summary>
    /// 
    /// </summary>
    public interface IPacketProcessor
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        bool Update();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        bool Process();
    } // IPacketProcessor
}
