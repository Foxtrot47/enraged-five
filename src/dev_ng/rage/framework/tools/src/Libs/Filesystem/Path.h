//
// filename:	Path.h
// description:	
//

#ifndef INC_PATH_H_
#define INC_PATH_H_

// --- Include Files ------------------------------------------------------------

// C++ Stdlib Header Files
#include <cstdarg>
#include <string>
#include <vector>

// --- Defines ------------------------------------------------------------------

namespace Filesystem
{

// --- Constants ----------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------

/**
 * @brief Filesystem path and filename utility functions.
 */
class cPath
{
public:
	/**
	 * @brief Combine multiple directory/file parts to form a single path.
	 */
	static void combine( std::string& result, ... );
	
	/**
	 * @brief Normalise a path.
	 */
	static void normalise( std::string& result, const std::string& path );

	/**
	 * @brief Normalise a path.
	 */
	static void normalise( std::string& path );

	/**
	 * @brief Split a path string into path components.
	 */
	static bool split( std::vector<std::string>& parts, const std::string& path );

	/**
	 * @brief Return directory.
	 */
	static void GetDirectory( std::string& directory, const std::string& path );

	/**
	 * @brief Return filename (without extension or directory) from path string.
	 */
	static void GetBasename( std::string& basename, const std::string& path );

private:

	/**
	 * @brief Return true iff character is valid directory separator.
	 */
	static bool IsDirectorySeparator( char c );

	cPath( );				//!< Hide default constructor
	cPath( cPath& path );	//!< Hide copy constructor
};

} // End of Filesystem namespace

#endif // !INC_PATH_H_
