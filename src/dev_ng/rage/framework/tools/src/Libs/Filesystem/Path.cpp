//
// filename:	RSNConfig.cpp
// description:	RSNConfig Class Implementation
//
// references:
//	http://www.codeproject.com/KB/files/SplitPath.aspx
//

// --- Include Files ------------------------------------------------------------

// Boost Header Files
#pragma warning( push )
#pragma warning( disable : 4512 )
#include "boost/algorithm/string/replace.hpp"
#include "boost/algorithm/string/case_conv.hpp"
#pragma warning( pop )

// C++ Stdlib Header Files
#include <cstdarg>
#include <cstdio>
#include <vector>
using namespace std;

// Local Header Files
#include "Path.h"

// --- Defines ------------------------------------------------------------------

#define MAX_PATH_LENGTH (512)

namespace Filesystem
{

// --- Constants ----------------------------------------------------------------

static const int NUM_DIRSEPS = 2;
static const char DIRECTORY_SEPARATORS[NUM_DIRSEPS] = { '\\', '/' }; 

// --- Structure/Class Definitions ----------------------------------------------

// --- Globals ------------------------------------------------------------------

// --- Static Globals -----------------------------------------------------------

// --- Static class members -----------------------------------------------------

// --- Code ---------------------------------------------------------------------

void 
cPath::combine( string& result, ... )
{
	va_list args;
	char sBuffer[MAX_PATH_LENGTH];

	va_start( args, result );
	vsprintf_s( sBuffer, MAX_PATH_LENGTH, "%s", args );
	va_end( args );

	result = string( sBuffer );
	normalise( result, result );
}

void 
cPath::normalise( string& result, const string& path )
{
	if ( 0 == path.size() )
		result = "";

	result = path;
	boost::algorithm::to_lower( result );
	boost::replace_all( result, "\\", "/" );
	boost::replace_all( result, "//", "/" );
}

void 
cPath::normalise( string& path )
{
	normalise( path, path );
}

bool 
cPath::split( vector<string>& parts, const string& path )
{
	string piece = "";

	for ( std::string::const_iterator it = path.begin();
		it != path.end();
		++it )
	{
		if ( IsDirectorySeparator( *it ) )
		{
			if ( piece.empty() )
				continue;
			parts.push_back( piece );
			piece = "";
		}
		else
		{
			piece += *it;
		}
	}

	// add last piece
	if ( piece.size() )
	{
		parts.push_back( piece );
	}
	return ( parts.size() > 0 );
}

void 
cPath::GetDirectory( std::string& directory, const std::string& path )
{
	size_t last = ( path.size() > 0 ) ? ( path.size() - 1 ) : string::npos;
	if ( last == string::npos )
	{
		directory = "";
		return;
	}
	// Find last separator
	size_t sep = string::npos;
	for ( string::const_reverse_iterator it = path.rbegin();
		it != path.rend();
		++it )
	{
		if ( IsDirectorySeparator( *it ) )
		{
			sep = *it;
			break;
		}
	}

	// If last separator is last character...
	if ( sep == last )
	{
		directory = path;
	}
	// Otherwise lets do some chopping...
	else if ( sep != string::npos )
	{
		directory = path.substr( 0, ( sep + 1 ) );
	}
	else
	{
		directory = "";
	}
}

void 
cPath::GetBasename( string& basename, const string& path )
{
	string dir;
	GetDirectory( dir, path );
	size_t pathsize = path.size();
	size_t dirsize = dir.size();

	if ( dirsize < pathsize )
	{
		basename = path.substr( dirsize, ( pathsize - dirsize ) );
	}
	else
	{
		basename = "";
	}
}

bool 
cPath::IsDirectorySeparator( char c )
{
	for ( int i = 0; i < NUM_DIRSEPS; ++i )
	{
		if ( DIRECTORY_SEPARATORS[i] == c )
			return ( true );
	}

	return ( false );
}

} // End of Filesystem namespace
