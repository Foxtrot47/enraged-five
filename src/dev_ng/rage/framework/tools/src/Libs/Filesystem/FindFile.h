//
// filename:	FindFile.h
// description:	
//

#ifndef INC_FINDFILE_H_
#define INC_FINDFILE_H_

// --- Include Files ------------------------------------------------------------

// STL Headers
#include <string>
#include <vector>

namespace Filesystem
{

// --- Defines ------------------------------------------------------------------

// --- Constants ----------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------

//!< 
typedef std::vector<std::string> tFindFileList;
//!< 
typedef tFindFileList::iterator tFindFileListIter;
//!< 
typedef tFindFileList::const_iterator tFindFileListConstIter;

// --- Globals ------------------------------------------------------------------

/**
 * @brief Find files in a filesystem path (optionally recursive) based on a file
 * filter.
 */
bool FindFiles( const std::string& path, const std::string& filter, bool recurse, tFindFileList& output );

} // End of Filesystem namespace

#endif // !INC_FINDFILE_H_
