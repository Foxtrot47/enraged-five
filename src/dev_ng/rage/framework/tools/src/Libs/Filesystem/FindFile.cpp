//
// filename:	FindFile.cpp
// description:	
//

// --- Include Files ------------------------------------------------------------

// Local Headers
#include "FindFile.h"

// Platform SDK Headers
#include <Windows.h>

using namespace std;

// --- Defines ------------------------------------------------------------------


namespace Filesystem
{

// --- Constants ----------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------

// --- Globals ------------------------------------------------------------------

// --- Static Globals -----------------------------------------------------------

// --- Static class members -----------------------------------------------------

// --- Code ---------------------------------------------------------------------

bool 
	FindFiles( const string& path, const string& filter, bool recurse, vector<string>& output )
{
	WIN32_FIND_DATA ffd;
	HANDLE hFind;

	// Find files first...
	string searchfile = path + "\\" + filter;
	hFind = FindFirstFile( searchfile.c_str(), &ffd );

	if ( INVALID_HANDLE_VALUE != hFind )
	{
		do 
		{
			if ( !( ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY ) )
			{
				output.push_back( ffd.cFileName );
			}
		} while( 0 != FindNextFile( hFind, &ffd ) );
		FindClose( hFind );
	}

	// Now recurse if requested...
	if ( recurse )
	{
		string searchdir = path + "\\*";
		hFind = FindFirstFile( searchdir.c_str(), &ffd );
		if ( INVALID_HANDLE_VALUE != hFind )
		{
			do 
			{
				if ( ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY )
				{
					// Recuse into this directory...
					FindFiles( path + "\\" + ffd.cFileName, filter, recurse, output );
				}
			} while( 0 != FindNextFile( hFind, &ffd ) );
		}
	}

	return ( output.size() > 0 );
}

} // End of Filesystem namespace
