﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using RSG.Services.Common.Clients;
using RSG.Base.Configuration.Services;
using RSG.Pipeline.Automation.Database.Common.ServiceContract;
using RSG.Pipeline.Automation.Database.Domain.Entities.Jobs;
using RSG.Pipeline.Automation.Database.Domain.Entities;
using RSG.Pipeline.Automation.Database.Domain.Entities.JobResults;
using RSG.Pipeline.Automation.Database.Domain.Entities.Stats;
using RSG.Pipeline.Automation.Database.Domain.Entities.Logs;

namespace RSG.Pipeline.Automation.Database.Consumers
{
    /// <summary>
    /// Consumer to be used by other applications to access the Database Servers end points.
    /// This will provide a nice interface and for those end points. It will also convert data between types 
    /// if required such as Guid to String.
    /// </summary>
    public class JobDatabaseConsumer :
        ConfigAwareClient<IJobDatabaseService>,
        IJobDatabaseService
    {
        #region Constructor(s)
		/// <summary>
        /// <summary>
        /// 
        /// </summary>
        public JobDatabaseConsumer(IServiceHostConfig config)
            : base(config)
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="binding"></param>
        /// <param name="remoteAddress"></param>    
        public JobDatabaseConsumer(System.ServiceModel.Channels.Binding binding, EndpointAddress remoteAddress) 
            : base(binding, remoteAddress) 
        {
        }
        #endregion // Constructor(s)

        /// <summary>
        /// Gets a list of all jobs
        /// </summary>
        /// <returns></returns>
        public IList<JobModel> GetAllJobs(String server = "", int limit = -1, int offset = -1)
        {
            try
            {
                return (base.Channel.GetAllJobs(server, limit, offset));
            }
            catch (System.Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Gets a list of all jobs with specified type
        /// </summary>
        /// <returns></returns>
        public IList<JobModel> GetAllJobsByType(string type, String server = "", int limit = -1, int offset = -1)
        {
            try
            {
                return (base.Channel.GetAllJobsByType(type, server, limit, offset));
            }
            catch (System.Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Gets a list of all jobs with specified type
        /// </summary>
        /// <returns></returns>
        public IList<JobModel> GetAllJobsByType(Type type, String server = "", int limit = -1, int offset = -1)
        {
            try
            {
                return (base.Channel.GetAllJobsByType(type.AssemblyQualifiedName, server, limit, offset));
            }
            catch (System.Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Gets a list of all jobs with specified processing host
        /// </summary>
        /// <returns></returns>
        public IList<JobModel> GetAllJobsForProcessingHost(string host, int limit = -1, int offset = -1)
        {
            try
            {
                return (base.Channel.GetAllJobsForProcessingHost(host, limit, offset));
            }
            catch (System.Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Gets a job by GUID
        /// </summary>
        /// <returns></returns>
        public JobModel GetJob(String id)
        {
            try
            {
                return (base.Channel.GetJob(id));
            }
            catch (System.Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Gets a job by GUID
        /// </summary>
        /// <returns></returns>
        public JobModel GetJob(Guid id)
        {
            try
            {
                return (base.Channel.GetJob(id.ToString()));
            }
            catch (System.Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Gets all the changes for a job via a string representation of the job id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IEnumerable<Change> GetAllChangesForJob(string id)
        {
            try
            {
                return (base.Channel.GetAllChangesForJob(id));
            }
            catch (System.Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Gets all the changes for a job via a Guid representation of the job id. 
        /// This will convert the Guid to a string and use the string version to 
        /// call the endpoint.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IEnumerable<Change> GetAllChangesForJob(Guid id)
        {
            try
            {
                return (GetAllChangesForJob(id.ToString()));
            }
            catch (System.Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Creates a single job
        /// </summary>
        /// <param name="job"></param>
        public bool CreateJob(JobModel job)
        {
            try
            {
                return base.Channel.CreateJob(job);
            }
            catch (System.Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Updates a single job
        /// </summary>
        /// <param name="job"></param>
        public bool UpdateJob(JobModel job)
        {
            try
            {
                return base.Channel.UpdateJob(job);
            }
            catch (System.Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Creates a single job
        /// </summary>
        /// <param name="job"></param>
        public bool CreateJobResult(JobResultModel jobResult)
        {
            try
            {
                return base.Channel.CreateJobResult(jobResult);
            }
            catch (System.Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Updates a single job
        /// </summary>
        /// <param name="job"></param>
        public bool UpdateJobResult(JobResultModel jobResult)
        {
            try
            {
                return base.Channel.UpdateJobResult(jobResult);
            }
            catch (System.Exception e)
            {
                throw e;
            }
        }

        public JobResultModel GetJobResult(Guid id)
        {
            try
            {
                return GetJobResult(id.ToString());
            }
            catch (System.Exception e)
            {
                throw e;
            }
        }

        public JobResultModel GetJobResult(String id)
        {
            try
            {
                return base.Channel.GetJobResult(id);
            }
            catch (System.Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="limit"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public IList<JobModel> GetAllJobsByDate(DateTime from, DateTime to, String server ="", int limit = -1, int offset = -1)
        {
            try
            {
                return GetAllJobsByDate(from.ToUniversalTime().ToString(), to.ToUniversalTime().ToString(), server, limit, offset);
            }
            catch (System.Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="limit"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public IList<JobModel> GetAllJobsByDate(String from, String to, String server = "", int limit = -1, int offset = -1)
        {
            try
            {
                return base.Channel.GetAllJobsByDate(from, to, server, limit, offset);
            }
            catch (System.Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="typeStr"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="limit"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public IList<JobModel> GetAllJobsByDateAndType(Type type, DateTime from, DateTime to, String server = "", int limit = -1, int offset = -1)
        {
            try
            {
                return GetAllJobsByDateAndType(type.AssemblyQualifiedName, from.ToUniversalTime().ToString(), to.ToUniversalTime().ToString(), server, limit, offset);
            }
            catch (System.Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="typeStr"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="limit"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public IList<JobModel> GetAllJobsByDateAndType(string typeStr, DateTime from, DateTime to, String server = "", int limit = -1, int offset = -1)
        {
            try
            {
                return GetAllJobsByDateAndType(typeStr, from.ToUniversalTime().ToString(), to.ToUniversalTime().ToString(), server, limit, offset);
            }
            catch (System.Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="typeStr"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="limit"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public IList<JobModel> GetAllJobsByDateAndType(string typeStr, String from, String to, String server = "", int limit = -1, int offset = -1)
        {
            try
            {
                return base.Channel.GetAllJobsByDateAndType(typeStr, from, to, server, limit, offset);
            }
            catch (System.Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="typeStr"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="limit"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ServerSummary GetJobStats(String server, Type type)
        {
            try
            {
                return GetJobStats(server, type.AssemblyQualifiedName);
            }
            catch (System.Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="typeStr"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="limit"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public ServerSummary GetJobStats(String server, string typeStr)
        {
            try
            {
                return base.Channel.GetJobStats(server, typeStr);
            }
            catch (System.Exception e)
            {
                throw e;
            }
        }


        /// <summary>
        /// Create log message entries in the database.
        /// </summary>
        /// <param name="session"></param>
        public bool CreateLogMessage(MessageModel logMessage)
        {
            try
            {
                return base.Channel.CreateLogMessage(logMessage);
            }
            catch (System.Exception e)
            {
                throw e;
            }
        }


        /// <summary>
        /// Create log message entries in the database.
        /// </summary>
        /// <param name="session"></param>
        public bool CreateLogMessageFromList(IEnumerable<MessageModel> logMessages)
        {
            try
            {
                return base.Channel.CreateLogMessageFromList(logMessages);
            }
            catch (System.Exception e)
            {
                throw e;
            }
        }

        public IEnumerable<MessageModel> GetErrorsForJob(Guid id)
        {
            try
            {
                return GetErrorsForJob(id.ToString());
            }
            catch (System.Exception e)
            {
                throw e;
            }
        }

        public IEnumerable<MessageModel> GetErrorsForJob(String id)
        {
            try
            {
                return base.Channel.GetErrorsForJob(id);
            }
            catch (System.Exception e)
            {
                throw e;
            }
        }

    }
}
