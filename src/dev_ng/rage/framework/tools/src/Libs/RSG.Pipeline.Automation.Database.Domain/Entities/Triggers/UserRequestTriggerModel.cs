﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Pipeline.Automation.Database.Domain.Entities.Triggers
{
    public class UserRequestTriggerModel : FilesTriggerModel
    {
        public virtual String Username { get; set; }
        public virtual String FormattedCommand { get; set; }
        public virtual String Description { get; set; }
    }
}
