﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Pipeline.Automation.Database.Domain.Entities.JobResults
{
    /// <summary>
    /// A POCO representation of the AssetBuilderJobResult. This is used to store
    /// data via NHibernate.
    /// 
    /// <see cref="RSG.Pipeline.Automation.Common.Jobs.AssetBuilderJobResult"/>
    /// </summary>
    public class AssetBuilderJobResultModel : JobResultModel
    {
        public virtual int SubmittedChangelistNumber { get; set; }
    }
}
