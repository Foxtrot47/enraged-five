﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Pipeline.Automation.Database.Domain.Entities.Triggers
{
    public class FilesTriggerModel : TriggerModel
    {
        public virtual String[] Files { get; set; }
    }
}
