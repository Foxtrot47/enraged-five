﻿using NHibernate.Cfg;
using NHibernate.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Pipeline.Automation.Database.Domain.Config
{
    /// <summary>
    /// Used to give us the option to add a prefix to all table names.
    /// </summary>
    [Serializable]
    public class RSGPrefixNamingStrategy : INamingStrategy
    {
		/// <summary>
		/// The singleton instance
		/// </summary>
        public static readonly INamingStrategy Instance = new RSGPrefixNamingStrategy();

        /// <summary>
        /// 
        /// </summary>
        public string Prefix { get; set; }

        private RSGPrefixNamingStrategy()
		{
		}

		#region INamingStrategy Members

		/// <summary>
		/// Return the unqualified class name
		/// </summary>
		/// <param name="className"></param>
		/// <returns></returns>
		public string ClassToTableName(string className)
		{
			return String.Format("{0}{1}", Prefix, StringHelper.Unqualify(className));
		}

		/// <summary>
		/// Return the unqualified property name
		/// </summary>
		/// <param name="propertyName"></param>
		/// <returns></returns>
		public string PropertyToColumnName(string propertyName)
		{
			return StringHelper.Unqualify(propertyName);
		}

		/// <summary>
		/// Return the argument
		/// </summary>
		/// <param name="tableName"></param>
		/// <returns></returns>
		public string TableName(string tableName)
		{
			return String.Format("{0}{1}", Prefix, tableName);
		}

		/// <summary>
		/// Return the argument
		/// </summary>
		/// <param name="columnName"></param>
		/// <returns></returns>
		public string ColumnName(string columnName)
		{
			return columnName;
		}

		/// <summary>
		/// Return the unqualified property name
		/// </summary>
		/// <param name="className"></param>
		/// <param name="propertyName"></param>
		/// <returns></returns>
		public string PropertyToTableName(string className, string propertyName)
		{
			return StringHelper.Unqualify(propertyName);
		}

		public string LogicalColumnName(string columnName, string propertyName)
		{
			return StringHelper.IsNotEmpty(columnName) ? columnName : StringHelper.Unqualify(propertyName);
		}

		#endregion
    }
}
