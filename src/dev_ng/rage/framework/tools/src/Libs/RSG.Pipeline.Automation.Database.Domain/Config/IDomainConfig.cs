﻿using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Pipeline.Automation.Database.Domain.Config
{
    /// <summary>
    /// 
    /// </summary>
    public interface IDomainConfig
    {

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        Configuration NHibernateConfig { get; }

        /// <summary>
        /// 
        /// </summary>
        SchemaExport SchemaExport { get; }

        /// <summary>
        /// 
        /// </summary>
        SchemaUpdate SchemaUpdate { get; }
        #endregion // Properties
    }
}
