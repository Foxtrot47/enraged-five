﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Pipeline.Automation.Database.Domain.Entities
{
    /// <summary>
    /// An object which represents and state change for a job. For example the job
    /// could go from Pending -> Assigned. This is means to be created once and 
    /// never edited.
    /// </summary>
    public class Change : IDBEntity
    {
        public Guid ID { get; set; }
        public DateTime ChangeTime { get; set; }
        public string Property { get; set; }
        public object PreviousValue { get; set; }
        public object NewValue { get; set; }
    }
}
