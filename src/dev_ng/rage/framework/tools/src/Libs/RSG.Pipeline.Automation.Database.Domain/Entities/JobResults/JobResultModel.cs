﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Pipeline.Automation.Database.Domain.Entities.JobResults
{
    /// <summary>
    /// A POCO representation of obResult. This is used to store
    /// data via NHibernate. This is used as a base for all job result
    /// types.
    /// 
    /// <see cref="RSG.Pipeline.Automation.Common.Jobs.AssetBuilderJobResult"/>
    /// </summary>
    [DataContract]
    [KnownType(typeof(AssetBuilderJobResultModel))]
    public class JobResultModel : IDBEntity
    {
        [DataMember]
        public virtual Guid JobId { get; set; }
        [DataMember]
        public virtual bool Succeeded { get; set; }

        public virtual IEnumerable<Change> Changes { get; set; }
    }
}
