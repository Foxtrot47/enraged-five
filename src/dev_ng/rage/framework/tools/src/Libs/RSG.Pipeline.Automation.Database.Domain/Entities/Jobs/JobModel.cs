﻿using RSG.Pipeline.Automation.Database.Domain.Entities.Triggers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Pipeline.Automation.Database.Domain.Entities.Jobs
{
    /// <summary>
    /// A POCO representation of JobModel. This is used to store
    /// data via NHibernate. This is used as the base class for all 
    /// job models used. This also needs to contain all the known 
    /// types so that WCF can serialise/deserialise the objects.
    /// 
    /// <see cref="RSG.Pipeline.Automation.Common.Jobs.JobModel"/>
    /// </summary>
    [KnownType(typeof(AssetBuilderJobModel))]
    [KnownType(typeof(CodeBuilderJobModel))]
    [KnownType(typeof(CommandRunnerJobModel))]
    [KnownType(typeof(ScriptBuilderJobModel))]
    public class JobModel : IDBEntity
    {
        public virtual Guid ID { get; set; }
        public virtual Guid ConsumedByJobID { get; set; }
        public virtual string State { get; set; }
        public virtual string Priority { get; set; }
        public virtual string Role { get; set; }
        public virtual TriggerModel Trigger { get; set; }
        public virtual DateTime? CreatedAt { get; set; }
        public virtual DateTime? ProcessedAt { get; set; }
        public virtual String ProcessingHost { get; set; }
        public virtual DateTime? CompletedAt { get; set; }
        public virtual TimeSpan ProcessingTime { get; set; }
        public virtual Object UserData { get; set; }
        public virtual IEnumerable<String> PrebuildCommands { get; set; }
        public virtual IEnumerable<String> PostbuildCommands { get; set; }

        public virtual String Server { get; set; }

        public virtual IEnumerable<Change> Changes { get; set; }
    }
}
