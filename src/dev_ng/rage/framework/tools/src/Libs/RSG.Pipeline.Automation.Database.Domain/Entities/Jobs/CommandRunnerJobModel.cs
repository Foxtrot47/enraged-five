﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Pipeline.Automation.Database.Domain.Entities.Jobs
{
    /// <summary>
    /// A POCO representation of CommandRunnerJob. This is used to store
    /// data via NHibernate.
    /// 
    /// <see cref="RSG.Pipeline.Automation.Common.Jobs.CommandRunnerJob"/>
    /// </summary>
    public class CommandRunnerJobModel : JobModel
    {
        public virtual IEnumerable<CommandRunnerJobInfoModel> Commands { get; set; }
        public virtual String CommandName { get; set; }
        public virtual bool RequiresPostJobNotification { get; set; }
        public virtual String BranchName { get; set; }
        public virtual String TargetDir { get; set; }
        public virtual String TargetRegex { get; set; }
        public virtual String PublishDir { get; set; }
    }
}
