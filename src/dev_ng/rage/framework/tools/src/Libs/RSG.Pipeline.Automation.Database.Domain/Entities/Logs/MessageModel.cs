﻿using RSG.Base.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.Automation.Database.Domain.Entities.Logs
{
    public class MessageModel : IDBEntity
    {
        public virtual Guid ID { get; set; }
        public virtual DateTime Timestamp { get; set; }
        public virtual LogLevel Level { get; set; }
        public virtual string Context { get; set; }
        public virtual string Message { get; set; }
        public virtual Guid JobId { get; set; }

        public MessageModel()
        {

        }

        public MessageModel(Guid jobId, DateTime timestamp, LogLevel level, string context, string message)
        {
            ID = Guid.NewGuid();
            Timestamp = timestamp;
            Level = level;
            Context = context;
            Message = message;
            JobId = jobId;
        }
    }
}
