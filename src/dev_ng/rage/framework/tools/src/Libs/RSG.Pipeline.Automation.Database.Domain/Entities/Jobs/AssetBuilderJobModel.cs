﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Pipeline.Automation.Database.Domain.Entities.Jobs
{
    /// <summary>
    /// A POCO representation of AssetBuilderJob. This is used to store
    /// data via NHibernate.
    /// 
    /// <see cref="RSG.Pipeline.Automation.Common.Jobs.AssetBuilderJob"/>
    /// </summary>
    public class AssetBuilderJobModel : JobModel
    {
        public virtual string EngineFlags { get; set; }
    }
}
