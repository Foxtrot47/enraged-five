﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Pipeline.Automation.Database.Domain.Entities
{
    /// <summary>
    /// Used to indicate that the object is able to be used through the
    /// repository system.
    /// </summary>
    public interface IDBEntity
    {
    }
}
