﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Pipeline.Automation.Database.Domain.Entities.Jobs
{
    /// <summary>
    /// A POCO representation of CommandRunnerJobInfo. This is used to store
    /// data via NHibernate.
    /// 
    /// <see cref="RSG.Pipeline.Automation.Common.Jobs.JobData.CommandRunnerJobInfo"/>
    /// </summary>
    public class CommandRunnerJobInfoModel
    {
        public virtual Guid ID { get; set; }
        public virtual String Command { get; set; }
        public virtual String Args { get; set; }
        public virtual String ErrorRegex { get; set; }
        public virtual String WarningRegex { get; set; }
    }
}
