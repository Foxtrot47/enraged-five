﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Pipeline.Automation.Database.Domain.Entities.Jobs
{
    /// <summary>
    /// A POCO representation of CodeBuilderJob. This is used to store
    /// data via NHibernate.
    /// 
    /// <see cref="RSG.Pipeline.Automation.Common.Jobs.CodeBuilderJob"/>
    /// </summary>
    public class CodeBuilderJobModel : JobModel
    {
        public virtual String SolutionFilename { get; set; }
        public virtual String TargetDir { get; set; }
        public virtual String PublishDir { get; set; }
        public virtual String BuildConfig { get; set; }
        public virtual String Platform { get; set; }
        public virtual String Tool { get; set; }
        public virtual bool Rebuild { get; set; }
        public virtual int SkipConsume { get; set; }
        public virtual bool RequiresPostJobNotification { get; set; }
    }
}
