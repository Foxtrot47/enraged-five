﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Pipeline.Automation.Database.Domain.Entities.Stats
{
    public class ServerSummary
    {
        public String Name { get; set; }
        public int TotalJobs { get; set; }
        public Dictionary<string, int> JobStateTotals { get; set; }

        public ServerSummary()
        {
            JobStateTotals = new Dictionary<string, int>();
        }
    }
}
