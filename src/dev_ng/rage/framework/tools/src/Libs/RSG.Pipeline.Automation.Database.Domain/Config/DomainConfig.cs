﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using System.Diagnostics;
using RSG.Base.Configuration.Services;
using RSG.Base.Logging.Universal;

namespace RSG.Pipeline.Automation.Database.Domain.Config
{
    /// <summary>
    /// 
    /// </summary>
    public class DomainConfig : IDomainConfig
    {
        #region Properties
        /// <summary>
        /// NHibernate configuration
        /// </summary>
        public Configuration NHibernateConfig { get; private set; }


        /// <summary>
        /// 
        /// </summary>
        public SchemaExport SchemaExport
        {
            get
            {
                if (m_schemaExportInstance == null)
                {
                    m_schemaExportInstance = new SchemaExport(NHibernateConfig);
                }

                return m_schemaExportInstance;
            }
        }
        private SchemaExport m_schemaExportInstance;

        /// <summary>
        /// 
        /// </summary>
        public SchemaUpdate SchemaUpdate
        {
            get
            {
                if (m_schemaUpdateInstance == null)
                {
                    m_schemaUpdateInstance = new SchemaUpdate(NHibernateConfig);
                }
                return m_schemaUpdateInstance;
            }
        }
        private SchemaUpdate m_schemaUpdateInstance;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public DomainConfig(IUniversalLog log, INHibernateConfig config)
        {
            CreateNHibernateConfig(log, config);
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private void CreateNHibernateConfig(IUniversalLog log, INHibernateConfig config)
        {
            // Construct the connection string
            string dbConnectionString =
                String.Format("Database={0};Data Source={1};User Id={2};Password={3};",
                              config.DatabaseName,
                              config.DatabaseLocation,
                              config.DatabaseUsername,
                              config.DatabasePassword);

            //dbConnectionString = String.Format(@"Server=.\SQLExpress;Database={1};Trusted_Connection=True;", server.DatabaseLocation, server.DatabaseName);
           log.Debug("DB Connection String: {0}", dbConnectionString);

            // Set up the config
            try
            {
                NHibernateConfig = new Configuration();
            }
            catch (System.Exception ex)
            {
                log.ToolException(ex, "Error while attempting to create the nhibernate config.");
            }

            NHibernateConfig.SetProperty("dialect", config.HibernateDialect);
            NHibernateConfig.SetProperty("connection.driver_class", config.DatabaseDriver);
            NHibernateConfig.SetProperty("connection.connection_string", dbConnectionString);
            NHibernateConfig.SetProperty("show_sql", config.DebugMode.ToString());
            NHibernateConfig.SetProperty("format_sql", config.DebugMode.ToString());
            NHibernateConfig.SetProperty("command_timeout", config.CommandTimeout.ToString());

            // Allows us to set a prefix on the database tables
            RSGPrefixNamingStrategy naming = RSGPrefixNamingStrategy.Instance as RSGPrefixNamingStrategy;
            naming.Prefix = config.DatabaseTablePrefix;
            log.Message(String.Format("Database Table Prefix is {0}", naming.Prefix));
            NHibernateConfig.SetNamingStrategy(RSGPrefixNamingStrategy.Instance);

            NHibernateConfig.AddAssembly("RSG.Pipeline.Automation.Database.Domain");

            SchemaUpdate.Execute(true, true);
        }
        #endregion // Private Methods
    } // NHibernateConfig
}
