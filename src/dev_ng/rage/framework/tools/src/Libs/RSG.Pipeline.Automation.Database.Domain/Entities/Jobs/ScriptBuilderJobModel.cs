﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Pipeline.Automation.Database.Domain.Entities.Jobs
{
    /// <summary>
    /// A POCO representation of ScriptBuilderJob. This is used to store
    /// data via NHibernate.
    /// 
    /// <see cref="RSG.Pipeline.Automation.Common.Jobs.ScriptBuilderJob"/>
    /// </summary>
    public class ScriptBuilderJobModel : JobModel
    {
        public virtual String ScriptProjectFilename { get; set; }
        public virtual String TargetDir { get; set; }
        public virtual String PublishDir { get; set; }
        public virtual String ZipFilePath { get; set; }
        public virtual String BuildConfig { get; set; }
        public virtual String Tool { get; set; }
        public virtual bool Rebuild { get; set; }
        public virtual int SkipConsume { get; set; }
        public virtual bool RequiresPostJobNotification { get; set; }
    }
}
