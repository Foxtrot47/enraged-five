﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Pipeline.Automation.Database.Domain.Entities.Triggers
{
    public class ChangelistTriggerModel : FilesTriggerModel
    {
        public virtual uint Changelist { get; set; }
        public virtual String Description { get; set; }
        public virtual String Username { get; set; }
        public virtual String Client { get; set; }
        public virtual IEnumerable<String> MonitoredPaths { get; set; }
    }
}
