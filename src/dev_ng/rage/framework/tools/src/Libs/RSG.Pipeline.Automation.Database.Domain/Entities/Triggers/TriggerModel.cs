﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Pipeline.Automation.Database.Domain.Entities.Triggers
{
    [KnownType(typeof(ChangelistTriggerModel))]
    [KnownType(typeof(FilesTriggerModel))]
    [KnownType(typeof(UserRequestTriggerModel))]
    public class TriggerModel : IDBEntity
    {
        public virtual Guid TriggerId { get; set; }
        public virtual DateTime TriggeredAt { get; set; }
    }
}
