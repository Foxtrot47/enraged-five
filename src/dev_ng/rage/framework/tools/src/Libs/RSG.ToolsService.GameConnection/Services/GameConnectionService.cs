﻿//---------------------------------------------------------------------------------------------
// <copyright file="GameConnectionService.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2015. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.ToolsService.GameConnection.Services
{
    using System.Collections.Generic;
    using System.ServiceModel;
    using RSG.Base.Logging.Universal;
    using RSG.Platform;
    using RSG.Rag.Clients;
    using RSG.Services.Configuration;
    using RSG.Services.Configuration.GameConnection;
    using RSG.Services.GameConnection.Contracts.DataContracts;
    using RSG.Services.GameConnection.Contracts.ServiceContracts;

    /// <summary>
    /// WCF service implementation of IGameConnectionService.
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single,
        ConcurrencyMode = ConcurrencyMode.Single)]
    internal class GameConnectionService : IGameConnectionService
    {
        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        IUniversalLog _log;

        /// <summary>
        /// Service config.
        /// </summary>
        GameConnectionConfig _config;
        
        /// <summary>
        /// Service server.
        /// </summary>
        IServer _server;
        #endregion  // Member Data

        #region Constructors
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="config"></param>
        /// <param name="server"></param>
        public GameConnectionService(IUniversalLog log, GameConnectionConfig config, 
            IServer server)
        {
            _log = log;
            _config = config;
            _server = server;
        }
        #endregion // Constructors

        #region IGameConnectionService Interface Methods
        /// <summary>
        /// Return information about all available game connections.
        /// </summary>
        /// <returns></returns>
        /// Note: this currently just passes straight-through to ask the RAG proxy
        /// the same information.  In future this information will be owned and
        /// maintained withing this service or process.
        public IEnumerable<IGameConnection> GetGameConnections()
        {
            List<GameConnection> gameConnections = new List<GameConnection>();
            IEnumerable<RSG.Rag.Contracts.Data.GameConnection> ragGameConnections = RagClientFactory.GetGameConnections();
            foreach (RSG.Rag.Contracts.Data.GameConnection ragGameConnection in ragGameConnections)
            {
                GameBuildType buildType = GameBuildType.Beta;
                System.Enum.TryParse(ragGameConnection.BuildConfig, out buildType);

                Platform platform = PlatformUtils.GamePlatformToPlatform(ragGameConnection.Platform);

                GameConnection connection = new GameConnection(ragGameConnection.GameIP,
                    platform, ragGameConnection.RagProxyIP, ragGameConnection.ExecutableName, 
                    buildType, ragGameConnection.ConnectedAt);
                gameConnections.Add(connection);
            }
            return (gameConnections);
        }
        #endregion // IGameConnectionService Interface Methods
    }

} // RSG.ToolsService.GameConnection.Services namespace
