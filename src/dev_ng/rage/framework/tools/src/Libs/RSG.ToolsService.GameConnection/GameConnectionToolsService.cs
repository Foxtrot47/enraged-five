﻿//---------------------------------------------------------------------------------------------
// <copyright file="GameConnectionService.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2015. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.ToolsService.GameConnection
{
    using System;
    using System.ComponentModel.Composition;
    using System.ServiceModel;
    using RSG.Base.Logging.Universal;
    using RSG.Services.Configuration;
    using RSG.Services.Configuration.GameConnection;
    using RSG.Services.Configuration.ServiceModel;
    using RSG.Services.GameConnection.Contracts.ServiceContracts;
    using RSG.ToolsService.Contracts;

    /// <summary>
    /// 
    /// </summary>
    [Export(typeof(IToolsService))]
    class GameConnectionToolsService : 
        ToolsServiceBase,
        IToolsService
    {
        #region Member Data
        /// <summary>
        /// Game connection service.
        /// </summary>
        private IGameConnectionService _gameConnectionService;

        /// <summary>
        /// WCF service host.
        /// </summary>
        private ServiceHost _host;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public GameConnectionToolsService()
        {
            this.ServiceName = Resources.StringTable.GameConnectionServiceName;
            this.Description = Resources.StringTable.GameConnectionServiceDescription;
        }
        #endregion // Constructor(s)

        #region ToolsServiceBase Method Implementation
        /// <summary>
        /// Game Connection Service: Startup handler.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="config"></param>
        protected override void OnStart(IUniversalLog log, RSG.Configuration.IConfig config)
        {
            log.DebugCtx(this.ServiceName, "GameConectionToolsService::OnStart()");

            // Create a connection to the asset services.
            GameConnectionConfig gameConnConfig = new GameConnectionConfig(config);
            IServer server = gameConnConfig.DefaultServer;

            _gameConnectionService = new Services.GameConnectionService(log, gameConnConfig,
                server);
            _host = new ConfigServiceHost(server, _gameConnectionService);

            // Start the service.
            try
            {
                _host.Open();
            }
            catch (Exception ex)
            {
                log.ToolException(ex, "Exception starting WCF GameConnectionService.");
                throw;
            }
        }

        /// <summary>
        /// Game Connection Service: Shutdown handler.
        /// </summary>
        /// <param name="log"></param>
        protected override void OnStop(IUniversalLog log)
        {
            log.DebugCtx(this.ServiceName, "GameConectionToolsService::OnStop()");
            if (null != _host)
            {
                _host.Close();
            }
        }
        #endregion // ToolsServiceBase Method Implementation
    }

} // RSG.ToolsService.GameConnection namespace
