﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using RSG.Metadata.Model;
using RSG.Metadata.Parser;
using RSG.Metadata.Data;
using RSG.Base.Configuration;
using RSG.Base.Logging.Universal;

namespace RSG.Export
{
    /// <summary>
    /// Class to load and analyse XML report created from rex
    /// </summary>
    public class RexReport
    {
        #region constants
        private static readonly string SECTION = "RexReportSection";
        private static readonly string SECTION_NAME = "name";
        private static readonly string SECTION_ENTRIES = "entries";
        private static readonly string ENTRY_OBJ_NAME = "objectName";
        private static readonly string ENTRY_MTL_NAME = "mtlName";
        private static readonly string ENTRY_SHADERINDEX = "shaderIndex";

        private static readonly string MTLMAP_NAME = "MaterialMap";

        #endregion // constants

        #region Properties
        public Dictionary<string, UInt32> MtlNameShaderMap
        {
            get;
            private set;
        }
        #endregion // Properties

        #region cstor/factory
        /// <summary>
        /// Explicit factory loading
        /// </summary>
        private RexReport()
        {
            MtlNameShaderMap = new Dictionary<string, UInt32>();
        }

        /// <summary>
        /// Factory method to load with XML parsing
        /// </summary>
        /// <param name="filepath"></param>
        /// <returns></returns>
        public static RexReport LoadXml(Stream xmlStream)
        {
            return null;
        }

        /// <summary>
        /// Factory method to load with XML parsing
        /// </summary>
        /// <param name="filepath"></param>
        /// <returns></returns>
        public static RexReport LoadXml(Uri filepath)
        {
            return null;
        }

        public static RexReport LoadMeta(Stream metaStream, IBranch branch, IUniversalLog log)
        {
            log.Profile("Loading animation dictionary modification metadata.");
            StructureDictionary sd = new StructureDictionary();
            //depot/gta5/assets_ng/metadata/definitions/rage/base/tools/dcc/libs/rexBase/rexReportFile.psc#1
            String rexReportPSC = Path.Combine(branch.Metadata, "definitions", "rage", "base", "tools", "dcc", "libs", "rexBase");
            sd.Load(branch, rexReportPSC);

            Structure s = sd["RexReportFile"];
            MetaFile mf = new MetaFile(s);
            mf.Deserialise(metaStream);
            log.ProfileEnd();

            RexReport report = new RexReport();

            ITunable sectionArray = RSG.Metadata.Util.Tunable.FindFirstStuctureNamed("sections", mf.Members);
            foreach (ITunable sectionTunable in (sectionArray as ArrayTunable).Items)
            {
                StringTunable sectionName = (sectionTunable as StructureTunable)[SECTION_NAME] as StringTunable;
                if (sectionName.Value == MTLMAP_NAME)
                {
                    ArrayTunable entries = (sectionTunable as StructureTunable)[SECTION_ENTRIES] as ArrayTunable;
                    foreach (ITunable entryTunable in entries.Items)
                    {
                        StructureTunable reportEntry = entryTunable as StructureTunable;
                        string mtlName = (reportEntry[ENTRY_MTL_NAME] as StringTunable).Value;
                        if(report.MtlNameShaderMap.ContainsKey(mtlName))
                        {
                            log.ErrorCtx((reportEntry[ENTRY_OBJ_NAME] as StringTunable).Value, "Duplicate material names found on object. Please name uniquely for UV animation export mapping.");
                            continue;
                        }
                        report.MtlNameShaderMap.Add(mtlName, (reportEntry[ENTRY_SHADERINDEX] as U32Tunable).Value);
                    }
                }
            }
            return report;
        }

        /// <summary>
        ///  factory method to use metadata parsing
        ///  TODO: implement metadata parsing
        /// </summary>
        /// <param name="filepath"></param>
        /// <param name="branch"></param>
        /// <param name="log"></param>
        /// <returns></returns>
        public static RexReport LoadMeta(Uri filepath, IBranch branch, IUniversalLog log)
        {
            FileStream fileStream = File.Open(filepath.ToString(), FileMode.Open);
            return LoadMeta(fileStream, branch, log);
        }
        #endregion

        #region controller methods
        #region public
        
        #endregion // public
        #region private
        
        #endregion

        #endregion
    }
}
