﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Pipeline.Core;
using RSG.Pipeline.Content;
using RSG.Pipeline.Processor;

namespace RSG.Pipeline.Processor.Animation.Common
{

    /// <summary>
    /// The IAnimation interface; not currently used but all animation processors conform
    /// </summary>
    public interface IAnimationProcessor : IProcessor
    {
        #region Methods

        #endregion
    }

} // RSG.Pipeline.Processor.Animation.Common namespace
