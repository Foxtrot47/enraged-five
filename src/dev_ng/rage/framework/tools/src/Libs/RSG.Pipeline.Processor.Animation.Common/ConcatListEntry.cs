﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using SIO = System.IO;

namespace RSG.Pipeline.Processor.Animation.Common
{
    public class ConcatListEntry
    {
        #region Fields
        /// <summary>
        /// Path to source anim/clip pairing
        /// </summary>
        private readonly String m_Filename;

        /// <summary>
        /// Path to source anim/clip pairing
        /// </summary>
        private readonly String m_OutputPath;

        /// <summary>
        /// Path to source anim/clip pairing
        /// </summary>
        private readonly String m_AudioName;

        /// <summary>
        /// Path to source anim/clip pairing
        /// </summary>
        private readonly String m_SectionMethod = "0";

        /// <summary>
        /// Path to source anim/clip pairing
        /// </summary>
        private readonly String m_SectionDuration = "0";

        /// <summary>
        /// Path to source anim/clip pairing
        /// </summary>
        private readonly List<string> m_SceneFolders = new List<string>();
        #endregion

        #region Properties
        /// <summary>
        /// Path to source anim/clip pairing
        /// </summary>
        public String Filename
        {
            get { return m_Filename; }
        }

        /// <summary>
        /// Path to source anim/clip pairing
        /// </summary>
        public String OutputPath
        {
            get { return m_OutputPath; }
        }

        /// <summary>
        /// Path to source anim/clip pairing
        /// </summary>
        public String AudioName
        {
            get { return m_AudioName; }
        }

        /// <summary>
        /// Path to source anim/clip pairing
        /// </summary>
        public String SectionMethod
        {
            get { return m_SectionMethod; }
        }

        /// <summary>
        /// Path to source anim/clip pairing
        /// </summary>
        public String SectionDuration
        {
            get { return m_SectionDuration; }
        }

        /// <summary>
        /// Path to source anim/clip pairing
        /// </summary>
        public List<String> SceneFolders
        {
            get { return m_SceneFolders; }
        }

        #endregion

        public ConcatListEntry(String file)
        {
            var xmlDoc = new XmlDocument();
            xmlDoc.Load(file);

            XmlNodeList partNodes = xmlDoc.SelectNodes("/RexRageCutscenePartFile/parts/Item/filename");

            for (int i = 0; i < partNodes.Count; ++i)
            {
                m_SceneFolders.Add(SIO.Path.Combine(SIO.Path.GetDirectoryName(partNodes[i].InnerText.Trim()),
                    SIO.Path.GetFileNameWithoutExtension(partNodes[i].InnerText.Trim())));
            }

            XmlNode pathNode = xmlDoc.SelectSingleNode("/RexRageCutscenePartFile/path");
            XmlNode audioNode = xmlDoc.SelectSingleNode("/RexRageCutscenePartFile/audio");
            XmlNode sectionMethodNode = xmlDoc.SelectSingleNode("/RexRageCutscenePartFile/sectionMethodIndex");
            XmlNode sectionDurationNode = xmlDoc.SelectSingleNode("/RexRageCutscenePartFile/sectionTimeDuration");

            if(pathNode != null)
                m_OutputPath = pathNode.InnerText.Trim();

            if(audioNode != null)
                m_AudioName = audioNode.InnerText.Trim();

            if (sectionDurationNode != null)
                m_SectionDuration = sectionDurationNode.Attributes["value"].InnerText.Trim();

            if(sectionMethodNode != null)
                m_SectionMethod = sectionMethodNode.Attributes["value"].InnerText.Trim();

            m_Filename = file;
        }
    }
}
