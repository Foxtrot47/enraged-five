#
# RSG.Pipeline.Processor.Animation.Common.makefile
# - Initally generated by x:/gta5/tools_ng/ironlib/util/projGen/generate_makefiles.rb
# - Edit as required.
#
 
Project RSG.Pipeline.Processor.Animation.Common
 
FrameworkVersion 4.0
 
OutputPath $(toolsroot)\ironlib\lib\Processors\
 
Files {
	BaseAnimationProcessor.cs
	ClipDictionaryProcessor.cs
	Compression.cs
	ConcatListEntry.cs
	ConcatListManager.cs
	Extract.cs
	IAnimationProcessor.cs
	Preparation.cs
	Directory Properties {
			AssemblyInfo.cs
	}
	MergeEntry.cs
}
 
ProjectReferences {
	..\..\..\..\..\..\..\..\..\3rdparty\dev\cli\DotNetZip\Zip Partial DLL\Zip Partial DLL.csproj NOCOPYLOCAL
	..\..\..\..\..\base\tools\libs\RSG.Base.Configuration\RSG.Base.Configuration.csproj NOCOPYLOCAL
	..\..\..\..\..\base\tools\libs\RSG.Base\RSG.Base.csproj NOCOPYLOCAL
	..\..\..\..\..\base\tools\libs\RSG.Metadata\RSG.Metadata.csproj NOCOPYLOCAL
	..\..\..\..\..\base\tools\ui\libs\RSG.Base.Editor\RSG.Base.Editor.csproj NOCOPYLOCAL
	..\..\..\..\..\base\tools\libs\RSG.Interop.Incredibuild\RSG.Interop.Incredibuild.csproj NOCOPYLOCAL
	..\RSG.Pipeline.Content\RSG.Pipeline.Content.csproj NOCOPYLOCAL
	..\RSG.Pipeline.Core\RSG.Pipeline.Core.csproj NOCOPYLOCAL
	..\RSG.Pipeline.Processor.Common\RSG.Pipeline.Processor.Common.csproj NOCOPYLOCAL
	..\RSG.Pipeline.Services\RSG.Pipeline.Services.csproj NOCOPYLOCAL
}
References {
	System
	System.ComponentModel.Composition
	System.Core
	System.Xml.Linq
	System.Data.DataSetExtensions
	Microsoft.CSharp
	System.Data
	System.Xml
}
