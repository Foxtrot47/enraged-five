﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using RSG.Base.Configuration;
using RSG.Base.Logging.Universal;
using SIO = System.IO;
using System.Xml;
using RSG.Pipeline.Core;

namespace RSG.Pipeline.Processor.Animation.Common
{
    /// <summary>
    /// Singleton class for handling the dictionary modification metadata
    /// </summary>
    public sealed class ConcatListManager
    {
        #region Constants
        /// <summary>
        /// The singleton instance.
        /// </summary>
        private static readonly ConcatListManager _Instance = new ConcatListManager();

        /// <summary>
        /// Processor log context.
        /// </summary>
        private static readonly String LOG_CTX = "Animation:DictionaryModificationManager";
        #endregion // Constants

        #region Fields
        private Dictionary<String, List<ConcatListEntry> > m_ConcatEntries;
        private Dictionary<String, List<MergeEntry>> m_MergeEntries;
        private List<String> m_ConcatNames;
        private List<String> m_MergeNames;
        private Dictionary<String, String> m_ConcatMissionOverloads;
        private Dictionary<String, String> m_ConcatRPFOverloads;
        private List<String> m_ExcludedNames;
        #endregion // Fields

        #region Properties
        /// <summary>
        /// The singleton instance accessor.
        /// </summary>
        public static ConcatListManager Instance
        {
            get { return _Instance; }
        }

        public Dictionary<String, List<ConcatListEntry> > ConcatEntries
        {
            get { return m_ConcatEntries; }
        }

        public List<String> ConcatNames
        {
            get { return m_ConcatNames; }
        }

        public Dictionary<String, List<MergeEntry>> MergeEntries
        {
            get { return m_MergeEntries; }
        }

        public List<String> MergeNames
        {
            get { return m_MergeNames; }
        }

        public Dictionary<String, String> ConcatMissionOverloads
        {
            get { return m_ConcatMissionOverloads; }
        }

        public Dictionary<String, String> ConcatRPFOverloads
        {
            get { return m_ConcatRPFOverloads; }
        }

        public List<String> ExcludedNames
        {
            get { return m_ExcludedNames; }
        }

        #endregion // Properties

        #region Public Methods
        /// <summary>
        /// Entry point for loading up the metadata and populating our data structures
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="dictionaryModFile"></param>
        /// <param name="log"></param>
        public bool InitWith(IEngineParameters param, String concatListDir, String mergeListDir, String missionOverloadFile, String exclusionListFile, IUniversalLog log)
        {
            Reset();

            try
            {
                if (Directory.Exists(concatListDir))
                {
                    string[] files = SIO.Directory.GetFiles(concatListDir, "*.concatlist");
                    foreach (string file in files)
                    {
                        ConcatListEntry c = new ConcatListEntry(file);
                        foreach (string folder in c.SceneFolders)
                        {
                            if (!m_ConcatEntries.ContainsKey(SIO.Path.GetFileName(SIO.Path.GetDirectoryName(folder)).ToLower()))
                            {
                                List<ConcatListEntry> lstConcats = new List<ConcatListEntry>();
                                lstConcats.Add(c);

                                m_ConcatEntries.Add(SIO.Path.GetFileName(SIO.Path.GetDirectoryName(folder)).ToLower(), lstConcats);
                            }
                            else
                            {
                                List<ConcatListEntry> lstConcats = m_ConcatEntries[SIO.Path.GetFileName(SIO.Path.GetDirectoryName(folder)).ToLower()];
                                if(!lstConcats.Contains(c))
                                    lstConcats.Add(c);
                            }
                        }
                        m_ConcatNames.Add(SIO.Path.GetFileName(c.OutputPath).ToLower());
                    }
                }
                else
                {
                    param.Log.Warning("Concat list directory '{0}' does not exist.", concatListDir);
                }
            }
            catch ( Exception ex )
            {
                param.Log.Warning("*EXCEPTION* : {0}", ex.Message);
            }

            try
            {
                if (Directory.Exists(mergeListDir))
                {
                    string[] files = SIO.Directory.GetFiles(mergeListDir, "*.mergelist");
                    foreach (string file in files)
                    {
                        MergeEntry c = new MergeEntry(file);
                        foreach (string folder in c.SceneFolders)
                        {
                            if (!m_MergeEntries.ContainsKey(SIO.Path.GetFileName(folder).ToLower()))
                            {
                                List<MergeEntry> lstConcats = new List<MergeEntry>();
                                lstConcats.Add(c);

                                m_MergeEntries.Add(SIO.Path.GetFileName(folder).ToLower(), lstConcats);
                            }
                            else
                            {
                                List<MergeEntry> lstConcats = m_MergeEntries[SIO.Path.GetFileName(folder).ToLower()];
                                if (!lstConcats.Contains(c))
                                    lstConcats.Add(c);
                            }
                        }
                        m_MergeNames.Add(SIO.Path.GetFileName(c.OutputPath).ToLower());
                    }
                }
                else
                {
                    param.Log.Warning("Merge list directory '{0}' does not exist.", mergeListDir);
                }
            }
            catch (Exception ex)
            {
                param.Log.Warning("*EXCEPTION* : {0}", ex.Message);
            }

            if (File.Exists(missionOverloadFile))
            {
                var xmlDoc = new XmlDocument();
                xmlDoc.Load(missionOverloadFile);

                XmlNodeList missionOverloads = xmlDoc.SelectNodes("/MissionOverloads/MissionOverload");

                foreach (XmlNode node in missionOverloads)
                {
                    m_ConcatMissionOverloads.Add(node.Attributes["name"].InnerText.ToLower(), node.Attributes["mission"].InnerText);
                }

                XmlNodeList rpfOverloads = xmlDoc.SelectNodes("/MissionOverloads/RPFOverload");

                foreach (XmlNode node in rpfOverloads)
                {
                    m_ConcatRPFOverloads.Add(node.Attributes["name"].InnerText.ToLower(), node.Attributes["mission"].InnerText);
                }
            }
            else
            {
                param.Log.Warning("File '{0}' does not exist", missionOverloadFile);
            }

            if (File.Exists(exclusionListFile))
            {
                string line = String.Empty;
                System.IO.StreamReader excludedFile = new System.IO.StreamReader(exclusionListFile);
                while ((line = excludedFile.ReadLine()) != null)
                {
                    if(line.Trim() != String.Empty)
                        m_ExcludedNames.Add(line.ToLower());
                }

                excludedFile.Close();
            }
            else
            {
                param.Log.Warning("File '{0}' does not exist", exclusionListFile);
            }

            return true;
        }
        #endregion // Public Methods

        #region Private Methods

        /// <summary>
        /// Reset all entries
        /// </summary>
        private void Reset()
        {
            m_ConcatEntries.Clear();
            m_ConcatNames.Clear();
            m_ConcatMissionOverloads.Clear();
            m_ConcatRPFOverloads.Clear();
            m_ExcludedNames.Clear();
            m_MergeEntries.Clear();
            m_MergeNames.Clear();

        }

        #endregion // Private Methods

        #region Constructor
        /// <summary>
        /// Private constructor
        /// </summary>
        private ConcatListManager()
        {
            m_ConcatEntries = new Dictionary<String, List<ConcatListEntry>>();
            m_ConcatNames = new List<String>();
            m_ConcatMissionOverloads = new Dictionary<String, String>();
            m_ConcatRPFOverloads = new Dictionary<String, String>();
            m_ExcludedNames = new List<String>();
            m_MergeEntries = new Dictionary<String, List<MergeEntry>>();
            m_MergeNames = new List<String>();
        }
        #endregion // Constructor
    }
}
