﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Attributes;
using RSG.Pipeline.Content;
using RSG.Pipeline.Processor;
using RSG.Pipeline.Services;
using XGE = RSG.Interop.Incredibuild.XGE;

namespace RSG.Pipeline.Processor.Animation.Common
{

    /// <summary>
    /// The Preperation processor creates a direcory of data that has been grouped according to clip data.  Subsequent
    /// processors need to process this data as groups
    /// </summary>
    [Export(typeof(IProcessor))]
    [ProcessorFlags(ProcessorFlags.Compatibility)]
    public class Preparation :
        BaseAnimationProcessor
    {
        #region Constants
        /// <summary>
        /// Processor description string.
        /// </summary>
        private static readonly String DESCRIPTION = "Animation Prepartation Processor";

        /// <summary>
        /// Processor log context.
        /// </summary>
        private static readonly String LOG_CTX = "Animation:Preparation";

        /// <summary>
        /// Inclusion file name.
        /// </summary>
        private static readonly String INCLUSION_FILE = "inclusions.modlist";

        /// <summary>
        ///Exclusion file name.
        /// </summary>
        private static readonly String EXCLUSION_FILE = "exclusions.modlist";

        #endregion // Constants

        #region Properties
        
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public Preparation()
            : base(DESCRIPTION)
        {
        }
        #endregion // Constructor(s)

        #region IProcessor Interface Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="process"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="resultantProcesses"></param>
        /// <returns></returns>
        public override bool Prebuild(IEngineParameters param,
            IProcess process, IProcessorCollection processors,
            IContentTree owner, out IEnumerable<IContentNode> syncDependencies,
            out IEnumerable<IProcess> resultantProcesses)
        {
            List<IProcess> processes = new List<IProcess>();

            process.State = ProcessState.Prebuilt;
            processes.Add(process);
            resultantProcesses = processes;
            syncDependencies = new List<IContentNode>();
            return (true);
        }
        
        /// <summary>
        /// Prepare a build for the processes; populating the XGE project with
        /// the tools, environments and tasks required.  Including setting up
        /// the task dependency chain.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="processes"></param>
        /// <param name="tools"></param>
        /// <param name="tasks"></param>
        /// <returns></returns>
        public override bool Prepare(IEngineParameters param, IEnumerable<IProcess> processes,
            out IEnumerable<XGE.ITool> tools, out IEnumerable<XGE.ITask> tasks)
        {
            List<XGE.ITool> prepTools = new List<XGE.ITool>();
            List<XGE.ITask> prepTasks = new List<XGE.ITask>();

            XGE.ITool tool = XGEFactory.GetAnimationProcessorTool(param.Log, param.Branch.Project.Config, XGEFactory.AnimationToolType.ClipGroup, "Animation Preparation");
            
            prepTools.Add(tool);
            foreach (IProcess process in processes)
            {
                XGE.ITask task = XGEUtil.GetGroupingTask(param.Log, param.Branch.Environment, process, tool);
                tool.Path = param.Branch.Environment.Subst(tool.Path);

                prepTasks.Add(task);
            }

            tools = prepTools;
            tasks = prepTasks;
            return (true);
        }

        /// <summary>
        /// Parse log information; processors are only passed log data for their
        /// respective IProcess output.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="taskLogData"></param>
        /// <returns>true for successful process; false for error/failure.</returns>
        public override bool ParseLog(IEngineParameters param, IEnumerable<String> taskLogData)
        {
            bool hasErrors = false;
            XGEUtil.ParsePreparationToolOutput(param.Log, taskLogData, LOG_CTX,
                out hasErrors);
            return (!hasErrors);
        }
        #endregion // IProcessor Interface Methods
    }

} // RSG.Pipeline.Processor.Animation.Cutscene namespace
