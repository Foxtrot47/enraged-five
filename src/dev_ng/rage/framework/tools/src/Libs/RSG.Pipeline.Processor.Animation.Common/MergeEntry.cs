﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using SIO = System.IO;

namespace RSG.Pipeline.Processor.Animation.Common
{
    public class MergeEntry
    {
#region Fields
        /// <summary>
        /// Path to source anim/clip pairing
        /// </summary>
        private readonly String m_Filename;

        /// <summary>
        /// Path to source anim/clip pairing
        /// </summary>
        private readonly String m_OutputPath;

        /// <summary>
        /// Path to source anim/clip pairing
        /// </summary>
        private readonly List<string> m_SceneFolders = new List<string>();
#endregion

#region Properties
        /// <summary>
        /// Path to source anim/clip pairing
        /// </summary>
        public String Filename
        {
            get { return m_Filename; }
        }

        /// <summary>
        /// Path to source anim/clip pairing
        /// </summary>
        public String OutputPath
        {
            get { return m_OutputPath; }
        }

        /// <summary>
        /// Path to source anim/clip pairing
        /// </summary>
        public List<String> SceneFolders
        {
            get { return m_SceneFolders; }
        }

#endregion
        public MergeEntry(String file)
        {
            var xmlDoc = new XmlDocument();
            xmlDoc.Load(file);

            XmlNodeList partNodes = xmlDoc.SelectNodes("/RexRageCutscenePartFile/parts/Item/filename");

            for (int i = 0; i < partNodes.Count; ++i)
            {
                m_SceneFolders.Add(partNodes[i].InnerText.Trim());
            }

            XmlNode pathNode = xmlDoc.SelectSingleNode("/RexRageCutscenePartFile/path");

            if (pathNode != null)
                m_OutputPath = pathNode.InnerText.Trim();

            m_Filename = file;
        }
    }
}
