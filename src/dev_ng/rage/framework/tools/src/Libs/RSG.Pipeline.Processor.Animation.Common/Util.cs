﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using RSG.Base.Logging.Universal;
using RSG.Base.Configuration;
using RSG.Pipeline.Core;
using XGE = RSG.Interop.Incredibuild.XGE;
using IB = RSG.Interop.Incredibuild;


namespace RSG.Pipeline.Processor.Animation.Common
{
    /// <summary>
    /// Bits and bobs required by animation processors, need to be moved.
    /// </summary>
    public static class XGEUtil
    {
        public enum ToolType
        {
            TYPE_CLIP_GROUP,
            TYPE_COALESCE,
            TYPE_COMPRESS,
        }

        #region Constants
        private static readonly String LOG_CTX = "Animation:Utility";
        
        private static readonly String TOOL_COALESCE = "$(toolsbin)/anim/clipcoalesce.exe";
        private static readonly String TOOL_RENAME = "$(toolsbin)/anim/cliprename.exe";
        private static readonly String TOOL_COMPRESS = "$(toolsbin)/anim/animcompress.exe";
        private static readonly String TOOL_COMBINE = "$(toolsbin)/anim/animcombine.exe";
        private static readonly String TOOL_CONCAT = "$(toolsbin)/anim/animconcat.exe";
        private static readonly String TOOL_EDIT = "$(toolsbin)/anim/animedit.exe";
        private static readonly String TOOL_RUBY = "$(toolsbin)/ruby/bin/ruby.exe";
        private static readonly String TOOL_IRON_RUBY = "$(toolsbin)/ironruby/bin/ir.exe";

        private static readonly String TOOL_CUTSCENE_LIGHTMERGE = "$(toolsbin)/anim/cutscene/cutflightmerge.exe";
        private static readonly String TOOL_CUTSCENE_ANIMCOMBINE = "$(toolsbin)/anim/cutscene/cutfanimcombine.exe";
        private static readonly String TOOL_CUTSCENE_ANIMSECTION = "$(toolsbin)/anim/cutscene/cutfanimsection.exe";
        private static readonly String TOOL_CUTSCENE_ANIMEDIT = "$(toolsbin)/cutscene/animedit.exe";
        private static readonly String TOOL_CUTSCENE_CLIPEDIT = "$(toolsbin)/cutscene/clipedit.exe";
        private static readonly String TOOL_CUTSCENE_SECTION = "$(toolsbin)/anim/cutscene/cutfsection.exe";
        private static readonly String TOOL_CUTSCENE_CONCAT = "$(toolsbin)/anim/cutscene/cutfconcat.exe";
        private static readonly String TOOL_CUTSCENE_CLIPCONCAT = "$(toolsbin)/anim/cutscene/cutfclipconcat.exe";

        private static readonly String PATH_SKELETON = "$(toolsconfig)/config/anim/skeletons";
        private static readonly String PATH_TEMPLATE = "$(toolsconfig)/config/anim/compression_templates";

        private static readonly String FILE_SKEL_DOF = "$(toolsroot)/etc/config/anim/dof_injection_skeletons.xml";

        private static readonly String CONTEXT_PREPARATION = @"Unpacking (?<src>[:\.\\/\w]*) to (?<dst>[:\.\\/\w]*)";
        private static readonly String CONTEXT_COALESCE = @"Coalescing (?<src>[:\.\\/\w]*) to (?<dst>[:\.\\/\w]*)";
        private static readonly String CONTEXT_COMPRESS = @"Compressing (?<src>[:\.\\/\w]*) to (?<dst>[:\.\\/\w]*)";
        private static readonly String CONTEXT_POSTPROCESS = @"Packing (?<src>[:\.\\/\w]*) to (?<dst>[:\.\\/\w]*)";

        private static readonly Regex REGEX_ERROR = new Regex("Error: (.*)",
            RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase);
        private static readonly Regex REGEX_WARN = new Regex("Warning: (.*)",
            RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase);
        #endregion // Constants

        #region Static Member Data
        /// <summary>
        /// int storing packet counts for conversion tasks.
        /// </summary>
        static int m_TaskCount = 0;


        #endregion // Static Member Data

        #region Static Controller Methods

        public static void ResetTaskCounter()
        {
            m_TaskCount = 0;
        }

        #region Grouping / Preparation
        /// <summary>
        /// Parse Preparation output errors, warnings and context into a log.
        /// </summary>
        /// <param name="log">Target log.</param>
        /// <param name="logfile">Preparation log filename.</param>
        public static void ParsePreparationToolOutput(IUniversalLog log,
            IEnumerable<String> logData, String logCtx, out bool hasErrors)
        {
            hasErrors = false;
            try
            {
                String contextSource = String.Empty;
                String contextDest = String.Empty;
                foreach (String line in logData)
                {
                    Regex regexContext = new Regex(CONTEXT_PREPARATION, RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase);
                    Match match = regexContext.Match(line);
                    if (match.Success)
                    {
                        contextSource = match.Groups["src"].Value;
                        contextDest = match.Groups["dst"].Value;
                        continue;
                    }
                    match = REGEX_ERROR.Match(line);
                    if (match.Success)
                    {
                        log.ErrorCtx(LOG_CTX, "Error preparing asset: {0} to {1}.",
                            contextSource, contextDest);
                        log.ErrorCtx(LOG_CTX, match.Value);
                        hasErrors = true;
                        continue;
                    }
                    match = REGEX_WARN.Match(line);
                    if (match.Success)
                    {
                        log.WarningCtx(LOG_CTX, "Warning preparing asset: {0} to {1}.",
                            contextSource, contextDest);
                        log.WarningCtx(LOG_CTX, match.Value);
                        hasErrors = true;
                        continue;
                    }
                }
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception parsing preparation output.");
            }
        }

        /// <summary>
        /// Create an XGE task object for a grouping conversion process.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="processes"></param>
        /// <param name="tool"></param>
        /// <returns></returns>
        public static XGE.ITask GetGroupingTask(IUniversalLog log, IEnvironment environment,
            IProcess process, XGE.ITool tool)
        {
            String taskName = String.Format("Clip Group Packet [{0}]",
                m_TaskCount++);
            String safeTaskName = taskName.Replace(' ', '_');

            XGE.ITaskJob task = new XGE.TaskJob(taskName);
            //task.StopOnErrors = true;
            try
            {
               
                Content.IFilesystemNode input = ((Content.IFilesystemNode)process.Inputs.ElementAt(0)); 
                Content.IFilesystemNode output = ((Content.IFilesystemNode)process.Outputs.First());
                String inputPathStr = input.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                String outputPathStr = output.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                String modDir = String.Empty;
                if ((process.AdditionalDependentContent).Count() > 0)
                {
                    Content.IFilesystemNode inputMods = ((Content.IFilesystemNode)process.AdditionalDependentContent.ElementAt(0));
                    modDir = inputMods.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                }

                String tool_rename = environment.Subst(TOOL_RENAME);

                tool_rename = Path.GetFullPath(tool_rename).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                // Define the XGE task for the IProcess.
                process.Parameters.Add(Constants.ProcessXGE_TaskName, safeTaskName);
                process.Parameters.Add(Constants.ProcessXGE_Task, task);

                task.Caption = taskName;
                if (modDir != String.Empty)
                {
                    task.Parameters = String.Format("-zip {0} -outdir {1} -moddir {2} -cliprename {3} -taskname {4}",
                        inputPathStr, outputPathStr, modDir, tool_rename, safeTaskName);
                }
                else
                {
                    task.Parameters = String.Format("-zip {0} -outdir {1} -cliprename {2} -taskname {3}",
                        inputPathStr, outputPathStr, tool_rename, safeTaskName);
                }
                task.InheritParams = true;
                task.Tool = tool;
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception creating XGE task: {0}.", taskName);
            }
            return (task);
        }
        #endregion // Grouping / Preparation

        #region Coalesce
        
        /// <summary>
        /// Parse Coalesce output errors, warnings and context into a log.
        /// </summary>
        /// <param name="log">Target log.</param>
        /// <param name="logfile">Coalesce log filename.</param>
        public static void ParseCoalesceToolOutput(IUniversalLog log, 
            IEnumerable<String> logData, String logCtx, out bool hasErrors)
        {
            hasErrors = false;
            try
            {
                String contextSource = String.Empty;
                String contextDest = String.Empty;
                foreach (String line in logData)
                {
                    Regex regexContext = new Regex(CONTEXT_COALESCE, RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase);
                    Match match = regexContext.Match(line);
                    if (match.Success)
                    {
                        contextSource = match.Groups["src"].Value;
                        contextDest = match.Groups["dst"].Value;
                        continue;
                    }
                    match = REGEX_ERROR.Match(line);
                    if (match.Success)
                    {
                        log.ErrorCtx(LOG_CTX, "Error coalescing assets: {0} to {1}.",
                            contextSource, contextDest);
                        log.ErrorCtx(LOG_CTX, match.Value);
                        hasErrors = true;
                        continue;
                    }
                    match = REGEX_WARN.Match(line);
                    if (match.Success)
                    {
                        log.WarningCtx(LOG_CTX, "Warning coalescing assets: {0} to {1}.",
                            contextSource, contextDest);
                        log.WarningCtx(LOG_CTX, match.Value);
                        continue;
                    }
                }
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception parsing coalesce output.");
            }
        }

        /// <summary>
        /// Create an XGE task object for a grouping conversion process.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="processes"></param>
        /// <param name="tool"></param>
        /// <returns></returns>
        public static XGE.ITask GetCoalesceTask(IUniversalLog log, IEnvironment environment,
            IProcess process, XGE.ITool tool)
        {
            String taskName = String.Format("Clip Coalesce Packet [{0}]",
                m_TaskCount++);
            String safeTaskName = taskName.Replace(' ', '_');

            XGE.ITaskJob task = new XGE.TaskJob(taskName);
            try
            {

                Content.IFilesystemNode input = ((Content.IFilesystemNode)process.Inputs.First());
                Content.IFilesystemNode output = ((Content.IFilesystemNode)process.Outputs.First());
                String inputPathStr = input.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                String outputPathStr = output.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                // Define the XGE task for the IProcess.
                process.Parameters.Add(Constants.ProcessXGE_TaskName, safeTaskName);
                process.Parameters.Add(Constants.ProcessXGE_Task, task);

                task.Caption = taskName;

                String tool_coalesce = environment.Subst(TOOL_COALESCE);
                String tool_edit = environment.Subst(TOOL_EDIT);
                String tool_combine = environment.Subst(TOOL_COMBINE);
                String tool_dofinjection = environment.Subst(FILE_SKEL_DOF);

                tool_coalesce = Path.GetFullPath(tool_coalesce).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                tool_edit = Path.GetFullPath(tool_edit).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                tool_combine = Path.GetFullPath(tool_combine).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                tool_dofinjection = Path.GetFullPath(tool_dofinjection).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                if (!Directory.Exists(outputPathStr))
                {
                    System.IO.Directory.CreateDirectory(outputPathStr);
                }

                
                task.Parameters = String.Format("-coalesce {0} -combine {1} -edit {2}  -inputdir {3} -outputdir {4} -framelimit {5} -dofinjection {6} -taskname {7}",
                    tool_coalesce, tool_combine, tool_edit, inputPathStr, outputPathStr, 200, tool_dofinjection, safeTaskName);
                
                task.InheritParams = false;
                task.Tool = tool;
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception creating XGE task: {0}.", taskName);
            }
            return (task);
        }
        #endregion // Coalesce

        #region Compression
        /// <summary>
        /// Parse Compression output errors, warnings and context into a log.
        /// </summary>
        /// <param name="log">Target log.</param>
        /// <param name="logData">Compression log filename.</param>
        /// <param name="logCtx"></param>
        /// <param name="hasErrors"></param>
        public static void ParseCompressionToolOutput(IUniversalLog log, 
            IEnumerable<String> logData, String logCtx, out bool hasErrors)
        {
            hasErrors = false;
            try
            {
                String contextSource = String.Empty;
                String contextDest = String.Empty;
                foreach (String line in logData)
                {
                    Regex regexContext = new Regex(CONTEXT_COMPRESS, RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase);
                    Match match = regexContext.Match(line);
                    if (match.Success)
                    {
                        contextSource = match.Groups["src"].Value;
                        contextDest = match.Groups["dst"].Value;
                        continue;
                    }
                    match = REGEX_ERROR.Match(line);
                    if (match.Success)
                    {
                        log.ErrorCtx(LOG_CTX, "Error compressing assets: {0} to {1}.",
                            contextSource, contextDest);
                        log.ErrorCtx(LOG_CTX, match.Value);
                        hasErrors = true;
                        continue;
                    }
                    match = REGEX_WARN.Match(line);
                    if (match.Success)
                    {
                        log.WarningCtx(LOG_CTX, "Warning compressing assets: {0} to {1}.",
                            contextSource, contextDest);
                        log.WarningCtx(LOG_CTX, match.Value);
                        continue;
                    }
                }
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception parsing compression output.");
            }
        }

        /// <summary>
        /// Create an XGE task object for a compression process.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="processes"></param>
        /// <param name="tool"></param>
        /// <returns></returns>
        public static XGE.ITask GetCompressionTask(IUniversalLog log, IEnvironment environment,
            IProcess process, XGE.ITool tool)
        {
            String taskName = String.Format("Clip Compression Packet [{0}]",
                m_TaskCount++);
            String safeTaskName = taskName.Replace(' ', '_');

            XGE.ITaskJob task = new XGE.TaskJob(taskName);
            try
            {

                Content.IFilesystemNode input = ((Content.IFilesystemNode)process.Inputs.First());
                Content.IFilesystemNode output = ((Content.IFilesystemNode)process.Outputs.First());
                String inputPathStr = input.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                String outputPathStr = output.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                // Define the XGE task for the IProcess.
                process.Parameters.Add(Constants.ProcessXGE_TaskName, safeTaskName);
                process.Parameters.Add(Constants.ProcessXGE_Task, task);

                task.Caption = taskName;

                //String script_compress = environment.Subst(SCRIPT_COMPRESS);
                String tool_compress = environment.Subst(TOOL_COMPRESS);
                String path_skeleton = environment.Subst(PATH_SKELETON);
                String path_template = environment.Subst(PATH_TEMPLATE);

                //script_compress = Path.GetFullPath(script_compress).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                tool_compress = Path.GetFullPath(tool_compress).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                path_skeleton = Path.GetFullPath(path_skeleton).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                path_template = Path.GetFullPath(path_template).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                String defaultSkeleton = String.Empty;
                if ((process.AdditionalDependentContent).Count() > 0)
                {
                    Content.IFilesystemNode inputMods = ((Content.IFilesystemNode)process.AdditionalDependentContent.ElementAt(0));
                    defaultSkeleton = inputMods.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                }

                //System.IO.Directory.CreateDirectory(outputPathStr);

                //task.SourceFile = script_compress;
                task.Parameters = String.Format("-compress {0} -templatepath {1} -skeletonpath {2} -inputdir {3} -outputdir {4} -defaultskeleton {5} -taskname {6}",
                    tool_compress, path_template, path_skeleton, inputPathStr, outputPathStr, defaultSkeleton, safeTaskName);
                task.InheritParams = false;
                task.Tool = tool;
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception creating XGE task: {0}.", taskName);
            }
            return (task);
        }
        #endregion // Compression

        #region Extract

        /// <summary>
        /// Parse Coalesce output errors, warnings and context into a log.
        /// </summary>
        /// <param name="log">Target log.</param>
        /// <param name="logfile">Coalesce log filename.</param>
        public static void ParseExtractToolOutput(IUniversalLog log,
            IEnumerable<String> logData, String logCtx, out bool hasErrors)
        {
            hasErrors = false;
            try
            {
                String contextSource = String.Empty;
                String contextDest = String.Empty;
                foreach (String line in logData)
                {
                    Regex regexContext = new Regex(CONTEXT_COALESCE, RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase);
                    Match match = regexContext.Match(line);
                    if (match.Success)
                    {
                        contextSource = match.Groups["src"].Value;
                        contextDest = match.Groups["dst"].Value;
                        continue;
                    }
                    match = REGEX_ERROR.Match(line);
                    if (match.Success)
                    {
                        log.ErrorCtx(LOG_CTX, "Error coalescing assets: {0} to {1}.",
                            contextSource, contextDest);
                        log.ErrorCtx(LOG_CTX, match.Value);
                        hasErrors = true;
                        continue;
                    }
                    match = REGEX_WARN.Match(line);
                    if (match.Success)
                    {
                        log.WarningCtx(LOG_CTX, "Warning coalescing assets: {0} to {1}.",
                            contextSource, contextDest);
                        log.WarningCtx(LOG_CTX, match.Value);
                        continue;
                    }
                }
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception parsing coalesce output.");
            }
        }

        /// <summary>
        /// Create an XGE task object for a grouping conversion process.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="processes"></param>
        /// <param name="tool"></param>
        /// <returns></returns>
        public static XGE.ITask GetExtractTask(IUniversalLog log, IEnvironment environment,
            IProcess process, XGE.ITool tool)
        {
            String taskName = String.Format("Clip Extract Packet [{0}]",
                m_TaskCount++);
            String safeTaskName = taskName.Replace(' ', '_');

            XGE.ITaskJob task = new XGE.TaskJob(taskName);
            try
            {

                Content.IFilesystemNode input = ((Content.IFilesystemNode)process.Inputs.First());
                Content.IFilesystemNode output = ((Content.IFilesystemNode)process.Outputs.First());
                String inputPathStr = input.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                String outputPathStr = output.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                // Define the XGE task for the IProcess.
                process.Parameters.Add(Constants.ProcessXGE_TaskName, safeTaskName);
                process.Parameters.Add(Constants.ProcessXGE_Task, task);

                task.Caption = taskName;

                if (!Directory.Exists(outputPathStr))
                {
                    System.IO.Directory.CreateDirectory(outputPathStr);
                }


                task.Parameters = String.Format("-inputfile {0} -outputdir {1} -taskname {2}",
                   inputPathStr, outputPathStr, safeTaskName);

                task.InheritParams = false;
                task.Tool = tool;
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception creating XGE task: {0}.", taskName);
            }
            return (task);
        }
        #endregion // Coalesce

        #region Post Process
        /// <summary>
        /// Parse Compression output errors, warnings and context into a log.
        /// </summary>
        /// <param name="log">Target log.</param>
        /// <param name="logfile">Compression log filename.</param>
        /// <param name="hasErrors"></param>
        public static void ParsePostProcessToolOutput(IUniversalLog log, 
            IEnumerable<String> logData, String logCtx, out bool hasErrors)
        {
            hasErrors = false;
            try
            {
                String contextSource = String.Empty;
                String contextDest = String.Empty;
                foreach (String line in logData)
                {
                    Regex regexContext = new Regex(CONTEXT_POSTPROCESS, RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase);
                    Match match = regexContext.Match(line);
                    if (match.Success)
                    {
                        contextSource = match.Groups["src"].Value;
                        contextDest = match.Groups["dst"].Value;
                        continue;
                    }
                    match = REGEX_ERROR.Match(line);
                    if (match.Success)
                    {
                        log.ErrorCtx(LOG_CTX, "Error packing assets: {0} to {1}.",
                            contextSource, contextDest);
                        log.ErrorCtx(LOG_CTX, match.Value);
                        hasErrors = true;
                        continue;
                    }
                    match = REGEX_WARN.Match(line);
                    if (match.Success)
                    {
                        log.WarningCtx(LOG_CTX, "Warning packing assets: {0} to {1}.",
                            contextSource, contextDest);
                        log.WarningCtx(LOG_CTX, match.Value);
                        continue;
                    }
                }
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception parsing postprocess output.");
            }
        }

        /// <summary>
        /// Create an XGE task object for the post process.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="processes"></param>
        /// <param name="tool"></param>
        /// <returns></returns>
        public static XGE.ITask GetPostProcessTask(IUniversalLog log, IEnvironment environment,
            IProcess process, XGE.ITool tool)
        {
            String taskName = String.Format("Post Process Packet [{0}]",
                m_TaskCount++);
            String safeTaskName = taskName.Replace(' ', '_');

            XGE.ITaskJob task = new XGE.TaskJob(taskName);
            try
            {

                Content.IFilesystemNode input = ((Content.IFilesystemNode)process.Inputs.First());
                Content.IFilesystemNode output = ((Content.IFilesystemNode)process.Outputs.First());
                String inputPathStr = input.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                String outputFile = output.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                process.Parameters.Add(Constants.ProcessXGE_TaskName, safeTaskName);
                process.Parameters.Add(Constants.ProcessXGE_Task, task);

                task.Caption = taskName;

                System.IO.Directory.CreateDirectory(Path.GetDirectoryName(outputFile));

                task.Parameters = String.Format("-inputdir {0} -outputfile {1} -taskname {2}",
                    inputPathStr, outputFile, safeTaskName);
                task.InheritParams = false;
                task.Tool = tool;
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception creating XGE task: {0}.", taskName);
            }
            return (task);
        }
        #endregion // Post Process

        #region Dictionary Build
        /// <summary>
        /// Parse Compression output errors, warnings and context into a log.
        /// </summary>
        /// <param name="log">Target log.</param>
        /// <param name="logfile">Compression log filename.</param>
        /// <param name="hasErrors"></param>
        public static void ParseDictionaryMetadataToolOutput(IUniversalLog log,
            IEnumerable<String> logData, String logCtx, out bool hasErrors)
        {
            hasErrors = false;
            try
            {
                String contextSource = String.Empty;
                String contextDest = String.Empty;
                foreach (String line in logData)
                {
                    Regex regexContext = new Regex(CONTEXT_POSTPROCESS, RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase);
                    Match match = regexContext.Match(line);
                    if (match.Success)
                    {
                        contextSource = match.Groups["src"].Value;
                        contextDest = match.Groups["dst"].Value;
                        continue;
                    }
                    match = REGEX_ERROR.Match(line);
                    if (match.Success)
                    {
                        log.ErrorCtx(LOG_CTX, "Error packing assets: {0} to {1}.",
                            contextSource, contextDest);
                        log.ErrorCtx(LOG_CTX, match.Value);
                        hasErrors = true;
                        continue;
                    }
                    match = REGEX_WARN.Match(line);
                    if (match.Success)
                    {
                        log.WarningCtx(LOG_CTX, "Warning packing assets: {0} to {1}.",
                            contextSource, contextDest);
                        log.WarningCtx(LOG_CTX, match.Value);
                        continue;
                    }
                }
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception parsing postprocess output.");
            }
        }

        /// <summary>
        /// Create an XGE task object for the post process.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="processes"></param>
        /// <param name="tool"></param>
        /// <returns></returns>
        public static XGE.ITask GetDictionaryMetadataTask(IUniversalLog log, IEnvironment environment,
            IProcess process, XGE.ITool tool)
        {
            String taskName = String.Format("Dictionary Build Packet [{0}]",
                m_TaskCount++);
            String safeTaskName = taskName.Replace(' ', '_');

            XGE.ITaskJob task = new XGE.TaskJob(taskName);
            try
            {
                Content.IFilesystemNode input = ((Content.IFilesystemNode)process.Inputs.First());
                Content.IFilesystemNode inputProcessed = ((Content.IFilesystemNode)process.Inputs.ToList()[1]);
                Content.IFilesystemNode output = ((Content.IFilesystemNode)process.Outputs.First());
                String inputPathStr = input.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                String inputProcessedPathStr = inputProcessed.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                String outputFile = output.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                process.Parameters.Add(Constants.ProcessXGE_TaskName, safeTaskName);
                process.Parameters.Add(Constants.ProcessXGE_Task, task);

                task.Caption = taskName;

                System.IO.Directory.CreateDirectory(Path.GetDirectoryName(outputFile));

                task.Parameters = String.Format("-sourcedir {0} -processeddir {1} -outputfile {2} -taskname {3}",
                    inputPathStr, inputProcessedPathStr, outputFile, safeTaskName);
                task.InheritParams = false;
                task.Tool = tool;
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception creating XGE task: {0}.", taskName);
            }
            return (task);
        }
        #endregion // Post Process

        #region Cutscene Light Merge

        /// <summary>
        /// Create an XGE task object for a compression process.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="processes"></param>
        /// <param name="tool"></param>
        /// <returns></returns>
        public static XGE.ITask GetCutsceneLightMergeTask(IUniversalLog log, IEnvironment environment,
            IProcess process, XGE.ITool tool)
        {
            String taskName = String.Format("Cutscene Light Merge Packet [{0}]",
                m_TaskCount++);
            String safeTaskName = taskName.Replace(' ', '_');

            XGE.ITaskJob task = new XGE.TaskJob(taskName);
            try
            {
                Content.IFilesystemNode input = ((Content.IFilesystemNode)process.Inputs.First());
                Content.IFilesystemNode output = ((Content.IFilesystemNode)process.Outputs.First());
                String inputPathStr = input.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                String outputPathStr = output.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                // Define the XGE task for the IProcess.
                process.Parameters.Add(Constants.ProcessXGE_TaskName, safeTaskName);
                process.Parameters.Add(Constants.ProcessXGE_Task, task);

                task.Caption = taskName;

                String tool_lightmerge = environment.Subst(TOOL_CUTSCENE_LIGHTMERGE);

                tool_lightmerge = Path.GetFullPath(tool_lightmerge).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                System.IO.Directory.CreateDirectory(outputPathStr);

                 task.Parameters = String.Format("-cutmerge {0} -inputfile {1} -outputfile {2} -lightfile {3} -taskname {4}",
                    tool_lightmerge, Path.Combine(inputPathStr, "data.cutxml"), Path.Combine(outputPathStr, "data_stream.cutxml" ), Path.Combine(inputPathStr, "data.lightxml"), safeTaskName);
                task.InheritParams = false;
                task.Tool = tool;
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception creating XGE task: {0}.", taskName);
            }
            return (task);
        }
        #endregion // Compression

        #region Cutscene Facial Merge

        /// <summary>
        /// Create an XGE task object for a compression process.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="processes"></param>
        /// <param name="tool"></param>
        /// <returns></returns>
        public static XGE.ITask GetCutsceneFacialMergeTask(IUniversalLog log, IEnvironment environment,
            IProcess process, XGE.ITool tool)
        {
            String taskName = String.Format("Cutscene Facial Merge Packet [{0}]",
                m_TaskCount++);
            String safeTaskName = taskName.Replace(' ', '_');

            XGE.ITaskJob task = new XGE.TaskJob(taskName);
            try
            {
                Content.IFilesystemNode input = ((Content.IFilesystemNode)process.Inputs.First());
                Content.IFilesystemNode output = ((Content.IFilesystemNode)process.Outputs.First());
                String inputPathStr = input.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                String outputPathStr = output.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                // Define the XGE task for the IProcess.
                process.Parameters.Add(Constants.ProcessXGE_TaskName, safeTaskName);
                process.Parameters.Add(Constants.ProcessXGE_Task, task);

                task.Caption = taskName;

                String tool_lightmerge = environment.Subst(TOOL_CUTSCENE_LIGHTMERGE);
                String tool_cutfanimcombine = environment.Subst(TOOL_CUTSCENE_ANIMCOMBINE);
                String tool_animcombine = environment.Subst(TOOL_COMBINE);
                String tool_animedit = environment.Subst(TOOL_CUTSCENE_ANIMEDIT);
                String tool_clipedit = environment.Subst(TOOL_CUTSCENE_CLIPEDIT);

                tool_lightmerge = Path.GetFullPath(tool_lightmerge).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                tool_cutfanimcombine = Path.GetFullPath(tool_cutfanimcombine).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                tool_animcombine = Path.GetFullPath(tool_animcombine).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                tool_animedit = Path.GetFullPath(tool_animedit).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                tool_clipedit = Path.GetFullPath(tool_clipedit).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                System.IO.Directory.CreateDirectory(outputPathStr);

                task.Parameters = String.Format("-inputdir {0} -outputdir {1} -cutfile {2} -cutfanimcombine {3} -animcombine {4} -animedit {5} -clipedit {6}",
                   inputPathStr, outputPathStr, Path.Combine(inputPathStr, "data_stream.cutxml"), tool_cutfanimcombine, tool_animcombine, tool_animedit, tool_clipedit );
                task.InheritParams = false;
                task.Tool = tool;
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception creating XGE task: {0}.", taskName);
            }
            return (task);
        }
        #endregion // Compression

        #region Cutscene Section

        /// <summary>
        /// Create an XGE task object for a compression process.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="processes"></param>
        /// <param name="tool"></param>
        /// <returns></returns>
        public static XGE.ITask GetCutsceneSectionTask(IUniversalLog log, IEnvironment environment,
            IProcess process, XGE.ITool tool)
        {
            String taskName = String.Format("Cutscene Section Packet [{0}]",
                m_TaskCount++);
            String safeTaskName = taskName.Replace(' ', '_');

            XGE.ITaskJob task = new XGE.TaskJob(taskName);
            try
            {
                Content.IFilesystemNode input = ((Content.IFilesystemNode)process.Inputs.First());
                Content.IFilesystemNode inputFace = ((Content.IFilesystemNode)process.Inputs.ToList()[1]);
                Content.IFilesystemNode inputLight = ((Content.IFilesystemNode)process.Inputs.ToList()[2]);
                Content.IFilesystemNode output = ((Content.IFilesystemNode)process.Outputs.First());
                String inputPathStr = input.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                String inputFacePathStr = inputFace.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                String inputLightPathStr = inputLight.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                String outputPathStr = output.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                // Define the XGE task for the IProcess.
                process.Parameters.Add(Constants.ProcessXGE_TaskName, safeTaskName);
                process.Parameters.Add(Constants.ProcessXGE_Task, task);

                task.Caption = taskName;

                String tool_cutfanimsection = environment.Subst(TOOL_CUTSCENE_ANIMSECTION);
                String tool_animedit = environment.Subst(TOOL_CUTSCENE_ANIMEDIT);
                String tool_clipedit = environment.Subst(TOOL_CUTSCENE_CLIPEDIT);

                tool_cutfanimsection = Path.GetFullPath(tool_cutfanimsection).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                tool_animedit = Path.GetFullPath(tool_animedit).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                tool_clipedit = Path.GetFullPath(tool_clipedit).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                System.IO.Directory.CreateDirectory(outputPathStr);

                task.Parameters = String.Format("-inputdir {0} -outputdir {1} -inputfacedir {2} -inputlightdir {3} -cutfanimsection {4} -animedit {5} -clipedit {6}",
                   inputPathStr, outputPathStr, inputFacePathStr, inputLightPathStr, tool_cutfanimsection, tool_animedit, tool_clipedit);
                task.InheritParams = false;
                task.Tool = tool;
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception creating XGE task: {0}.", taskName);
            }
            return (task);
        }
        #endregion // Compression

        #region Cutscene Finalise

        /// <summary>
        /// Create an XGE task object for a compression process.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="processes"></param>
        /// <param name="tool"></param>
        /// <returns></returns>
        public static XGE.ITask GetCutsceneFinaliseTask(IUniversalLog log, IEnvironment environment,
            IProcess process, XGE.ITool tool)
        {
            String taskName = String.Format("Cutscene Finalise Packet [{0}]",
                m_TaskCount++);
            String safeTaskName = taskName.Replace(' ', '_');

            XGE.ITaskJob task = new XGE.TaskJob(taskName);
            try
            {
                Content.IFilesystemNode input = ((Content.IFilesystemNode)process.Inputs.First());
                Content.IFilesystemNode inputSceneName = ((Content.IFilesystemNode)process.Inputs.ToList()[1]);
                Content.IFilesystemNode output = ((Content.IFilesystemNode)process.Outputs.First());
                String inputPathStr = input.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                String inputSceneNameStr = Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(inputSceneName.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar)));
                String outputPathStr = output.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                // Define the XGE task for the IProcess.
                process.Parameters.Add(Constants.ProcessXGE_TaskName, safeTaskName);
                process.Parameters.Add(Constants.ProcessXGE_Task, task);

                task.Caption = taskName;

                String tool_cutfsection = environment.Subst(TOOL_CUTSCENE_SECTION);

                tool_cutfsection = Path.GetFullPath(tool_cutfsection).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                System.IO.Directory.CreateDirectory(outputPathStr);

                task.Parameters = String.Format("-inputdir {0} -outputdir {1} -scenename {2} -cutfsection {3}",
                   inputPathStr, outputPathStr, inputSceneNameStr, tool_cutfsection);
                task.InheritParams = false;
                task.Tool = tool;
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception creating XGE task: {0}.", taskName);
            }
            return (task);
        }
        #endregion // Compression

        #region Cutscene Post Process

        /// <summary>
        /// Create an XGE task object for a compression process.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="processes"></param>
        /// <param name="tool"></param>
        /// <returns></returns>
        public static XGE.ITask GetCutscenePostProcessTask(IUniversalLog log, IEnvironment environment,
            IProcess process, XGE.ITool tool)
        {
            String taskName = String.Format("Cutscene Post Process Packet [{0}]",
                m_TaskCount++);
            String safeTaskName = taskName.Replace(' ', '_');

            XGE.ITaskJob task = new XGE.TaskJob(taskName);
            try
            {
                Content.IFilesystemNode input = ((Content.IFilesystemNode)process.Inputs.First());
                Content.IFilesystemNode inputSceneName = ((Content.IFilesystemNode)process.Inputs.ToList()[1]);
                Content.IFilesystemNode inputFinalise = ((Content.IFilesystemNode)process.Inputs.ToList()[2]);
                Content.IFilesystemNode output = ((Content.IFilesystemNode)process.Outputs.First());
                String inputPathStr = input.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                String inputSceneNameStr = Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(inputSceneName.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar)));
                String inputFinalisePathStr = inputFinalise.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                String outputPathStr = Path.GetDirectoryName(output.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar));

                // Define the XGE task for the IProcess.
                process.Parameters.Add(Constants.ProcessXGE_TaskName, safeTaskName);
                process.Parameters.Add(Constants.ProcessXGE_Task, task);

                task.Caption = taskName;

                System.IO.Directory.CreateDirectory(outputPathStr);

                task.Parameters = String.Format("-inputdir {0} -outputdir {1} -finalisedir {2} -scenename {3}",
                   inputPathStr, outputPathStr, inputFinalisePathStr, inputSceneNameStr);
                task.InheritParams = false;
                task.Tool = tool;
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception creating XGE task: {0}.", taskName);
            }
            return (task);
        }
        #endregion // Compression

        #region Cutscene Concatenation

        /// <summary>
        /// Create an XGE task object for a compression process.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="processes"></param>
        /// <param name="tool"></param>
        /// <returns></returns>
        public static XGE.ITask GetCutsceneConcatenationTask(IUniversalLog log, IEnvironment environment,
            IProcess process, XGE.ITool tool)
        {
            String taskName = String.Format("Cutscene Concatenation Packet [{0}]",
                m_TaskCount++);
            String safeTaskName = taskName.Replace(' ', '_');

            XGE.ITaskJob task = new XGE.TaskJob(taskName);
            try
            {
                String inputDirs = String.Empty;

                var enumerator = process.Inputs.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    Content.IFilesystemNode input = ((Content.IFilesystemNode)enumerator.Current);

                    inputDirs += input.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar) + ",";
                }

                //Content.IFilesystemNode input = ((Content.IFilesystemNode)process.Inputs.First());
                Content.IFilesystemNode output = ((Content.IFilesystemNode)process.Outputs.First());
                //String inputPathStr = input.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                String outputPathStr = output.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                // Define the XGE task for the IProcess.
                process.Parameters.Add(Constants.ProcessXGE_TaskName, safeTaskName);
                process.Parameters.Add(Constants.ProcessXGE_Task, task);

                task.Caption = taskName;

                String tool_animconcat = environment.Subst(TOOL_CONCAT);
                String tool_cutfclipconcat = environment.Subst(TOOL_CUTSCENE_CLIPCONCAT);
                String tool_cutfconcat = environment.Subst(TOOL_CUTSCENE_CONCAT);

                tool_animconcat = Path.GetFullPath(tool_animconcat).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                tool_cutfclipconcat = Path.GetFullPath(tool_cutfclipconcat).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                tool_cutfconcat = Path.GetFullPath(tool_cutfconcat).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                String sectionMethod = "0";
                String sectionDuration = "0";
                String audio = String.Empty;

                if (process.Parameters.ContainsKey(RSG.Pipeline.Core.Constants.Cutscene_Concat_Audio))
                {
                    audio = process.Parameters[RSG.Pipeline.Core.Constants.Cutscene_Concat_Audio].ToString();
                }

                if (process.Parameters.ContainsKey(RSG.Pipeline.Core.Constants.Cutscene_Concat_SectionMethod))
                {
                    sectionMethod = process.Parameters[RSG.Pipeline.Core.Constants.Cutscene_Concat_SectionMethod].ToString();
                }

                if (process.Parameters.ContainsKey(RSG.Pipeline.Core.Constants.Cutscene_Concat_SectionMethod))
                {
                    sectionMethod = process.Parameters[RSG.Pipeline.Core.Constants.Cutscene_Concat_SectionMethod].ToString();
                }

                if (process.Parameters.ContainsKey(RSG.Pipeline.Core.Constants.Cutscene_Concat_SectionDuration))
                {
                    sectionDuration = process.Parameters[RSG.Pipeline.Core.Constants.Cutscene_Concat_SectionDuration].ToString();
                }

                System.IO.Directory.CreateDirectory(outputPathStr);

                task.Parameters = String.Format("-inputdir {0} -outputdir {1} -audio {2} -sectionmethod {3} -sectionduration {4} -animconcat {5} -clipconcat {6} -cutconcat {7}",
                   inputDirs, outputPathStr, audio, sectionMethod, sectionDuration, tool_animconcat, tool_cutfclipconcat, tool_cutfconcat);
                task.InheritParams = false;
                task.Tool = tool;
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception creating XGE task: {0}.", taskName);
            }
            return (task);
        }
        #endregion // Compression

        #endregion // Static Controller Methods

        #region Private Static Methods
        /// <summary>
        /// Add the process input and output filenames to the XGE task.
        /// </summary>
        /// <param name="process"></param>
        /// <param name="task"></param>
        private static void AddProcessInputsAndOutputsToTask(IProcess process, ref XGE.ITaskJob task)
        {
            foreach (IContentNode node in process.Inputs)
            {
                if (!(node is Content.IFilesystemNode))
                    continue;
                Content.IFilesystemNode fsNode = (node as Content.IFilesystemNode);
                if(fsNode is RSG.Pipeline.Content.Directory)
                {
                    task.InputFiles.Add(Path.Combine(fsNode.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar), (fsNode as RSG.Pipeline.Content.Directory).Wildcard));
                }
                else
                {
                    task.InputFiles.Add(fsNode.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar));
                }
            }
            foreach (IContentNode node in process.Outputs)
            {
                if (!(node is Content.IFilesystemNode))
                    continue;
                Content.IFilesystemNode fsNode = (node as Content.IFilesystemNode);
                if (fsNode is RSG.Pipeline.Content.Directory)
                {
                    task.OutputFiles.Add(Path.Combine(fsNode.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar), (fsNode as RSG.Pipeline.Content.Directory).Wildcard));
                }
                else
                {
                    task.OutputFiles.Add(fsNode.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar));
                }
            }
        }
         #endregion // Private Static Methods
    }
}
