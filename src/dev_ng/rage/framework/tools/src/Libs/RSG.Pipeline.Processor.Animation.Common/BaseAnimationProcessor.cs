﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using IB = RSG.Interop.Incredibuild;
using XGE = RSG.Interop.Incredibuild.XGE;
using RSG.Pipeline.Core;
using Content = RSG.Pipeline.Content;
using RSG.Pipeline.Content;
using RSG.Pipeline.Processor;
using RSG.Pipeline.Services;

namespace RSG.Pipeline.Processor.Animation.Common
{

    /// <summary>
    /// Abstract base processor class for all animation processors.
    /// </summary>
    /// 
    public abstract class BaseAnimationProcessor :
        ProcessorBase,
        IProcessor
    {       
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public BaseAnimationProcessor(String description)
            : base(description)
        {
        }
        #endregion // Constructor(s)

        #region IProcessor Controller Methods
        /// <summary>
        /// Parse log information; processors are only passed log data for their
        /// respective IProcess output.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="taskLogData">Log string data.</param>
        /// <returns>true for successful process; false for error/failure.</returns>
        public override bool ParseLog(IEngineParameters param, 
            IEnumerable<String> taskLogData)
        {
            bool hasErrors = false;
            LogParsing.ParseRageToolOutput(param.Log, "BaseAnimationProcessor", 
                taskLogData, out hasErrors);
            return (hasErrors);
        }
        #endregion // IProcessor Controller Methods
    }

} // RSG.Pipeline.Processor.Platform namespace