﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace RSG.Rag.Contracts.Services
{
    /// <summary>
    /// Service contract for registering/receiving updates regarding the games that are
    /// connected to the proxy.
    /// </summary>
    [ServiceContract(
        SessionMode = SessionMode.Required,
        CallbackContract = typeof(IConnectionEvents))]
    public interface IConnectionSubscriptionService : ISubscriptionService
    {
    }
}
