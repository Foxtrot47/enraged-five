﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using RSG.Rag.Contracts.Data;

namespace RSG.Rag.Contracts.Services
{
    /// <summary>
    /// Service contract for the RAG console.
    /// </summary>
    [ServiceContract]
    public interface IConsoleService
    {
        /// <summary>
        /// Executes a console command.
        /// </summary>
        /// <param name="command">Command to execute.</param>
        /// <returns>Results of executing the command.</returns>
        [OperationContract]
        ConsoleCommandResponse ExecuteCommand(String command);
    }
}
