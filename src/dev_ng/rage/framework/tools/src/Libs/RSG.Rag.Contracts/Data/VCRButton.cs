﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Rag.Contracts.Data
{
    /// <summary>
    /// Enumberation of the VCR Buttons.
    /// </summary>
    public enum VCRButton
    {
        Pause,
        PlayForwards,
        PlayBackwards,
        StepForwards,
        StepBackwards,
        Rewind,
        FastForward,
    }
}
