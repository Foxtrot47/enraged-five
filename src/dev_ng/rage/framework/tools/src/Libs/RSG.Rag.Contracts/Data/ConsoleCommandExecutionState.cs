﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Rag.Contracts.Data
{
    /// <summary>
    /// Enumeration containing the possible results of running a command at
    /// the interactive console.
    /// </summary>
    public enum ConsoleCommandExecutionState
    {
        /// <summary>
        /// Command was successfully processed by Rag.
        /// </summary>
        ProcessedByRag,

        /// <summary>
        /// Command was successfully processed by the game.
        /// </summary>
        ProcessedByGame,

        /// <summary>
        /// Command was ignored due to it either being not a registered command
        /// or because it contained only whitespace.
        /// </summary>
        Ignored,

        /// <summary>
        /// An exception occurred while executing the command.
        /// </summary>
        Faulted
    }
}
