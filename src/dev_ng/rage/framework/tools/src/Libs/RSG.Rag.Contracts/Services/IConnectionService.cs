﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using RSG.Rag.Contracts.Data;

namespace RSG.Rag.Contracts.Services
{
    /// <summary>
    /// Service contract for retrieving connected game information.
    /// </summary>
    [ServiceContract]
    public interface IConnectionService
    {
        /// <summary>
        /// Retrieves the list of game connecitons the proxy currently has.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        IEnumerable<GameConnection> GetGameConnections();
    }
}
