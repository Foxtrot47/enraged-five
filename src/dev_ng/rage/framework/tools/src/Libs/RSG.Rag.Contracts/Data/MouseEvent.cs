// ---------------------------------------------------------------------------------------------
// <copyright file="MouseEvent.cs" company="Rockstar Games">
//      Copyright � Rockstar Games 2016. All rights reserved
// </copyright>
// ---------------------------------------------------------------------------------------------

namespace RSG.Rag.Contracts.Data
{
    /// <summary>
    /// Mouse events
    /// </summary>
    public enum MouseEvent
    {
        /// <summary>
        /// WM_MOUSEMOVE
        /// </summary>
        MouseMove = 0x0200,

        /// <summary>
        /// WM_LBUTTONDOWN
        /// </summary>
        LeftButtonDown = 0x0201,

        /// <summary>
        /// WM_LBUTTONUP
        /// </summary>
        LeftButtonUp = 0x0202,

        /// <summary>
        /// WM_RBUTTONDOWN
        /// </summary>
        RightButtonDown = 0x0204,

        /// <summary>
        /// WM_RBUTTONUP
        /// </summary>
        RightButtonUp = 0x0205,

        /// <summary>
        /// WM_MBUTTONDOWN
        /// </summary>
        MiddleButtonDown = 0x0207,

        /// <summary>
        /// WM_MBUTTONUP
        /// </summary>
        MiddleButtonUp = 0x0208,

        /// <summary>
        /// MK_LBUTTON
        /// </summary>
        LeftButton = 0x0001,

        /// <summary>
        /// MK_RBUTTON
        /// </summary>
        RightButton = 0x0002,

        /// <summary>
        /// MK_MBUTTON
        /// </summary>
        MiddleButton = 0x0010
    }
}