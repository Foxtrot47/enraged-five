﻿// ---------------------------------------------------------------------------------------------
// <copyright file="IIInputService.cs" company="Rockstar Games">
//      Copyright © Rockstar Games 2016. All rights reserved
// </copyright>
// ---------------------------------------------------------------------------------------------

namespace RSG.Rag.Contracts.Services
{
    using System.ServiceModel;
    using System.Windows.Input;
    using Data;

    /// <summary>
    /// Service to send input commands to the proxy.
    /// </summary>
    [ServiceContract]
    public interface IInputService
    {
        #region Methods
        /// <summary>
        /// Queues a keyboard event for the specified key.
        /// </summary>
        /// <param name="evt">Keyboard event</param>
        /// <param name="key">Key that needs to be handled</param>
        [OperationContract]
        void QueueKeyboardEvent(KeyboardEvent evt, Key key);

        /// <summary>
        /// Queues a mouse event.
        /// </summary>
        /// <param name="evt">Mouse event</param>
        /// <param name="x"> Xlocation of mouse</param>
        /// <param name="y">Y location of mouse</param>
        /// <param name="wheelDelta">Mouse wheel delta</param>
        /// <param name="width">Width ??</param>
        /// <param name="height">Height ??</param>
        [OperationContract]
        void QueueMouseEvent(MouseEvent evt, double x, double y, int wheelDelta, double width, double height);
        #endregion
    }
}