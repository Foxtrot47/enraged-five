﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using RSG.Rag.Contracts.Data;

namespace RSG.Rag.Contracts.Services
{
    /// <summary>
    /// Callback interface for the <see cref="IConnectionSubscriptionService"/> interface.
    /// </summary>
    public interface IConnectionEvents
    {
        /// <summary>
        /// Event that is fired when a new game connects with the proxy.
        /// </summary>
        /// <param name="connection"></param>
        [OperationContract(IsOneWay = true)]
        void OnGameConnected(GameConnection connection);

        /// <summary>
        /// Event that is fired when the rag proxy loses its connection to a game.
        /// </summary>
        /// <param name="connection"></param>
        [OperationContract(IsOneWay = true)]
        void OnGameDisconnected(GameConnection connection);
    }
}
