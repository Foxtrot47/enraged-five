﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Base.Logging;

namespace RSG.Rag.Contracts.Data
{
    /// <summary>
    /// Return data from running a console command against the <see cref="IConsoleService"/>.
    /// </summary>
    [DataContract]
    public class ConsoleCommandResponse
    {
        #region Constants
        /// <summary>
        /// Static response that can be used when a command is ignored.
        /// </summary>
        public static readonly ConsoleCommandResponse IgnoredResponse =
            new ConsoleCommandResponse(ConsoleCommandExecutionState.Ignored, String.Empty);
        #endregion

        #region Fields
        /// <summary>
        /// Private field for the <see cref="Result"/> property.
        /// </summary>
        private ConsoleCommandExecutionState _state;

        /// <summary>
        /// Private field for the <see cref="Response"/> property.
        /// </summary>
        private String _response;

        /// <summary>
        /// Private field for the <see cref="ExceptionDetails"/> property.
        /// </summary>
        private String _exceptionDetails;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ConsoleCommandData"/> class.
        /// </summary>
        public ConsoleCommandResponse()
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ConsoleCommandData"/> class using
        /// the provided command result and response.
        /// </summary>
        /// <param name="result"></param>
        /// <param name="response"></param>
        public ConsoleCommandResponse(ConsoleCommandExecutionState result, String response)
        {
            _state = result;
            _response = response;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ConsoleCommandData"/> class that
        /// is in the faulted state and which makes use of the provided exception.
        /// </summary>
        /// <param name="e"></param>
        public ConsoleCommandResponse(Exception e)
        {
            _state = ConsoleCommandExecutionState.Faulted;

            ExceptionFormatter formatter = new ExceptionFormatter(e);
            _exceptionDetails = String.Join("\n", formatter.Format());
        }
        #endregion

        #region Properties
        /// <summary>
        /// Value indicating whether the command was successfully executed.
        /// </summary>
        [DataMember]
        public ConsoleCommandExecutionState State
        {
            get { return _state; }
            set { _state = value; }
        }

        /// <summary>
        /// Response from running the command.
        /// </summary>
        [DataMember]
        public String Response
        {
            get { return _response; }
            set { _response = value; }
        }

        /// <summary>
        /// This is the string representation of the exception that was thrown if the
        /// <see cref="Steate"/> is set to <see cref="ConsoleCommandExecutionState.Faulted"/>.
        /// </summary>
        [DataMember]
        public String ExceptionDetails
        {
            get { return _exceptionDetails; }
            set { _exceptionDetails = value; }
        }
        #endregion
    }
}
