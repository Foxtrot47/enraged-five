﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace RSG.Rag.Contracts.Services
{
    /// <summary>
    /// Generic interface for subscription based services.
    /// You'll need to inherit from this interface to specify the callback interface WCF
    /// should use.
    /// </summary>
    [ServiceContract]
    public interface ISubscriptionService
    {
        /// <summary>
        /// Allows clients to subscribe to receive events.
        /// </summary>
        [OperationContract]
        void Subscribe();

        /// <summary>
        /// Allows clients to unsubscribe from any further events.
        /// </summary>
        [OperationContract]
        void Unsubscribe();
    }
}
