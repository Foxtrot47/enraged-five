﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.Collections;
using RSG.Base.Math;
using RSG.Rag.Contracts.Data;

namespace RSG.Rag.Contracts.Services
{
    /// <summary>
    /// Service contract for reading/writing widget values.
    /// </summary>
    [ServiceContract]
    public interface IWidgetService
    {
        #region General Operations
        /// <summary>
        /// Returns whether a particular widget exists.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        [OperationContract(Name = "WidgetExistsByPath")]
        bool WidgetExists(String widgetPath);

        /// <summary>
        /// Returns whether a particular widget exists.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        [OperationContract(Name = "WidgetExistsById")]
        bool WidgetExists(uint widgetId);

        /// <summary>
        /// Retrieves a particular widgets id.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        [OperationContract]
        uint GetWidgetId(String widgetPath);

        /// <summary>
        /// Retrieves a particular widgets path.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        [OperationContract]
        String GetWidgetPath(uint widgetId);

        /// <summary>
        /// Adds a reference to the specified bank/group widget indicating that it is
        /// actively in use and to ensure that we receive value updates for child widgets.
        /// </summary>
        /// <param name="bankPath"></param>
        /// <returns></returns>
        [OperationContract(Name = "AddBankReferenceByPath")]
        void AddBankReference(String bankPath);

        /// <summary>
        /// Adds a reference to the specified bank/group widget indicating that it is
        /// actively in use and to ensure that we receive value updates for child widgets.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        [OperationContract(Name = "AddBankReferenceById")]
        void AddBankReference(uint widgetId);

        /// <summary>
        /// Removes a reference to the specified bank/group widget.
        /// </summary>
        /// <param name="bankPath"></param>
        /// <returns></returns>
        [OperationContract(Name = "RemoveBankReferenceByPath")]
        void RemoveBankReference(String bankPath);

        /// <summary>
        /// Removes a reference to the specified bank/group widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        [OperationContract(Name = "RemoveBankReferenceById")]
        void RemoveBankReference(uint widgetId);

        /// <summary>
        /// A special command that will block until all currently pending bank packets sent
        /// from the game have been processed. This is extremely useful when many widgets
        /// are being changed that may be dependent on each other.
        /// </summary>
        /// <param name="timeout">
        /// Optional timeout (in ms) for how long we should wait before aborting the sync
        /// command.
        /// </param>
        [OperationContract]
        void SendSyncCommand(uint timeout = 0);

        /// <summary>
        /// Presses the specified widget as a button.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        [OperationContract(Name = "PressButtonByPath")]
        void PressButton(String widgetPath);

        /// <summary>
        /// Presses the specified widget as a button.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        [OperationContract(Name = "PressButtonById")]
        void PressButton(uint widgetId);

        /// <summary>
        /// Presses the specified widget as a button.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        [OperationContract(Name = "PressVCRButtonByPath")]
        void PressVCRButton(String widgetPath, VCRButton button);

        /// <summary>
        /// Presses the specified widget as a button.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        [OperationContract(Name = "PressVCRButtonById")]
        void PressVCRButton(uint widgetId, VCRButton button);
        #endregion

        #region Read Operations
        /// <summary>
        /// Reads a boolean value from the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        [OperationContract(Name = "ReadBoolWidgetByPath")]
        bool ReadBoolWidget(String widgetPath);

        /// <summary>
        /// Reads a boolean value from the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        [OperationContract(Name = "ReadBoolWidgetById")]
        bool ReadBoolWidget(uint widgetId);

        /// <summary>
        /// Reads a float value from the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        [OperationContract(Name = "ReadFloatWidgetByPath")]
        float ReadFloatWidget(String widgetPath);

        /// <summary>
        /// Reads a float value from the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        [OperationContract(Name = "ReadFloatWidgetById")]
        float ReadFloatWidget(uint widgetId);

        /// <summary>
        /// Retrieves a widgets floating point limits.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        [OperationContract(Name = "ReadFloatWidgetLimitsByPath")]
        Range<float> ReadFloatWidgetLimits(String widgetPath);

        /// <summary>
        /// Retrieves a widgets floating point limits.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        [OperationContract(Name = "ReadFloatWidgetLimitsById")]
        Range<float> ReadFloatWidgetLimits(uint widgetId);

        /// <summary>
        /// Reads an integer value from the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        [OperationContract(Name = "ReadIntWidgetByPath")]
        int ReadIntWidget(String widgetPath);

        /// <summary>
        /// Reads an integer value from the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        [OperationContract(Name = "ReadIntWidgetById")]
        int ReadIntWidget(uint widgetId);

        /// <summary>
        /// Reads a string value from the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        [OperationContract(Name = "ReadStringWidgetByPath")]
        String ReadStringWidget(String widgetPath);

        /// <summary>
        /// Reads a string value from the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        [OperationContract(Name = "ReadStringWidgetById")]
        String ReadStringWidget(uint widgetId);

        /// <summary>
        /// Reads a vector2 value from the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        [OperationContract(Name = "ReadVector2WidgetByPath")]
        Vector2f ReadVector2Widget(String widgetPath);

        /// <summary>
        /// Reads a vector2 value from the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        [OperationContract(Name = "ReadVector2WidgetById")]
        Vector2f ReadVector2Widget(uint widgetId);

        /// <summary>
        /// Reads a vector3 value from the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        [OperationContract(Name = "ReadVector3WidgetByPath")]
        Vector3f ReadVector3Widget(String widgetPath);

        /// <summary>
        /// Reads a vector3 value from the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        [OperationContract(Name = "ReadVector3WidgetById")]
        Vector3f ReadVector3Widget(uint widgetId);
        
        /// <summary>
        /// Reads a vector4 value from the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        [OperationContract(Name = "ReadVector4WidgetByPath")]
        Vector4f ReadVector4Widget(String widgetPath);

        /// <summary>
        /// Reads a vector4 value from the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        [OperationContract(Name = "ReadVector4WidgetById")]
        Vector4f ReadVector4Widget(uint widgetId);

        /// <summary>
        /// Reads a matrix33 value from the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        [OperationContract(Name = "ReadMatrix33WidgetByPath")]
        Matrix33f ReadMatrix33Widget(String widgetPath);

        /// <summary>
        /// Reads a matrix33 value from the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        [OperationContract(Name = "ReadMatrix33WidgetById")]
        Matrix33f ReadMatrix33Widget(uint widgetId);

        /// <summary>
        /// Reads a matrix34 value from the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        [OperationContract(Name = "ReadMatrix34WidgetByPath")]
        Matrix34f ReadMatrix34Widget(String widgetPath);

        /// <summary>
        /// Reads a matrix34 value from the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        [OperationContract(Name = "ReadMatrix34WidgetById")]
        Matrix34f ReadMatrix34Widget(uint widgetId);

        /// <summary>
        /// Reads a matrix44 value from the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        [OperationContract(Name = "ReadMatrix44WidgetByPath")]
        Matrix44f ReadMatrix44Widget(String widgetPath);

        /// <summary>
        /// Reads a matrix44 value from the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        [OperationContract(Name = "ReadMatrix44WidgetById")]
        Matrix44f ReadMatrix44Widget(uint widgetId);

        /// <summary>
        /// Reads a byte array value from the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        [OperationContract(Name = "ReadDataWidgetByPath")]
        byte[] ReadDataWidget(String widgetPath);

        /// <summary>
        /// Reads a byte array value from the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        [OperationContract(Name = "ReadDataWidgetById")]
        byte[] ReadDataWidget(uint widgetId);
        #endregion

        #region Write Operations
        /// <summary>
        /// Writes a boolean value to the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <param name="value"></param>
        [OperationContract(Name = "WriteBoolWidgetByPath")]
        void WriteBoolWidget(String widgetPath, bool value);

        /// <summary>
        /// Writes a boolean value to the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="value"></param>
        [OperationContract(Name = "WriteBoolWidgetById")]
        void WriteBoolWidget(uint widgetId, bool value);

        /// <summary>
        /// Writes a float value to the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        [OperationContract(Name = "WriteFloatWidgetByPath")]
        void WriteFloatWidget(String widgetPath, float value);

        /// <summary>
        /// Writes a float value to the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        [OperationContract(Name = "WriteFloatWidgetById")]
        void WriteFloatWidget(uint widgetId, float value);

        /// <summary>
        /// Writes an integer value to the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        [OperationContract(Name = "WriteIntWidgetByPath")]
        void WriteIntWidget(String widgetPath, int value);

        /// <summary>
        /// Writes an integer value to the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        [OperationContract(Name = "WriteIntWidgetById")]
        void WriteIntWidget(uint widgetId, int value);

        /// <summary>
        /// Writes a string value to the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        [OperationContract(Name = "WriteStringWidgetByPath")]
        void WriteStringWidget(String widgetPath, String value);

        /// <summary>
        /// Writes a string value to the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        [OperationContract(Name = "WriteStringWidgetById")]
        void WriteStringWidget(uint widgetId, String value);

        /// <summary>
        /// Writes a vector2 value to the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <param name="value"></param>
        [OperationContract(Name = "WriteVector2WidgetByPath")]
        void WriteVector2Widget(String widgetPath, Vector2f value);

        /// <summary>
        /// Writes a vector2 value to the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="value"></param>
        [OperationContract(Name = "WriteVector2WidgetById")]
        void WriteVector2Widget(uint widgetId, Vector2f value);

        /// <summary>
        /// Writes a vector3 value to the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <param name="value"></param>
        [OperationContract(Name = "WriteVector3WidgetByPath")]
        void WriteVector3Widget(String widgetPath, Vector3f value);

        /// <summary>
        /// Writes a vector3 value to the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="value"></param>
        [OperationContract(Name = "WriteVector3WidgetById")]
        void WriteVector3Widget(uint widgetId, Vector3f value);

        /// <summary>
        /// Writes a vector4 value to the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <param name="value"></param>
        [OperationContract(Name = "WriteVector4WidgetByPath")]
        void WriteVector4Widget(String widgetPath, Vector4f value);

        /// <summary>
        /// Writes a vector4 value to the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="value"></param>
        [OperationContract(Name = "WriteVector4WidgetById")]
        void WriteVector4Widget(uint widgetId, Vector4f value);

        /// <summary>
        /// Writes a matrix33 value to the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <param name="value"></param>
        [OperationContract(Name = "WriteMatrix33WidgetByPath")]
        void WriteMatrix33Widget(String widgetPath, Matrix33f value);

        /// <summary>
        /// Writes a matrix33 value to the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="value"></param>
        [OperationContract(Name = "WriteMatrix33WidgetById")]
        void WriteMatrix33Widget(uint widgetId, Matrix33f value);

        /// <summary>
        /// Writes a matrix34 value to the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <param name="value"></param>
        [OperationContract(Name = "WriteMatrix34WidgetByPath")]
        void WriteMatrix34Widget(String widgetPath, Matrix34f value);

        /// <summary>
        /// Writes a matrix34 value to the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="value"></param>
        [OperationContract(Name = "WriteMatrix34WidgetById")]
        void WriteMatrix34Widget(uint widgetId, Matrix34f value);

        /// <summary>
        /// Writes a matrix44 value to the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <param name="value"></param>
        [OperationContract(Name = "WriteMatrix44WidgetByPath")]
        void WriteMatrix44Widget(String widgetPath, Matrix44f value);

        /// <summary>
        /// Writes a matrix44 value to the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="value"></param>
        [OperationContract(Name = "WriteMatrix44WidgetById")]
        void WriteMatrix44Widget(uint widgetId, Matrix44f value);

        /// <summary>
        /// Writes a byte array to the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <param name="value"></param>
        [OperationContract(Name = "WriteDataWidgetByPath")]
        void WriteDataWidget(String widgetPath, byte[] value);

        /// <summary>
        /// Writes a byte array to the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="value"></param>
        [OperationContract(Name = "WriteDataWidgetById")]
        void WriteDataWidget(uint widgetId, byte[] value);
        #endregion
    }
}
