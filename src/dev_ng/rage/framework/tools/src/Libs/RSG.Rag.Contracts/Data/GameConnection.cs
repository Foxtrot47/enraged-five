﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.Contracts.Data
{
    /// <summary>
    /// Data pertaining to a single game connection.
    /// </summary>
    [DataContract]
    public class GameConnection : IEquatable<GameConnection>
    {
        #region Properties
        /// <summary>
        /// The game's IP.
        /// </summary>
        public IPAddress GameIP { get; set; }

        /// <summary>
        /// Surrogate property for serialising the game ip.
        /// </summary>
        [DataMember]
        private byte[] GameIPSurrogate
        {
            get { return GameIP.GetAddressBytes(); }
            set { GameIP = new IPAddress(value); }
        }

        /// <summary>
        /// The IP of the proxy this game connection is being hosted by.
        /// </summary>
        public IPAddress RagProxyIP { get; set; }

        /// <summary>
        /// Surrogate property for serialising the game ip.
        /// </summary>
        [DataMember]
        private byte[] RagProxyIPSurrogate
        {
            get { return RagProxyIP.GetAddressBytes(); }
            set { RagProxyIP = new IPAddress(value); }
        }

        /// <summary>
        /// The platform the game is running on.
        /// TODO: Convert this to a value from the RSG.Platform.Platform enum.
        /// </summary>
        [DataMember]
        public String Platform { get; set; }

        /// <summary>
        /// The build configuration that the game is running in.
        /// TODO: Convert this to an enum value.
        /// </summary>
        [DataMember]
        public String BuildConfig { get; set; }

        /// <summary>
        /// The port that the widget service is being hosted on.
        /// </summary>
        [DataMember]
        public int ServicesPort { get; set; }

        /// <summary>
        /// The name of the executable that is running.
        /// </summary>
        [DataMember]
        public String ExecutableName { get; set; }

        /// <summary>
        /// Whether this is the default connection to send requests to.
        /// TODO: This should be removed.
        /// </summary>
        [DataMember]
        public bool DefaultConnection { get; set; }

        /// <summary>
        /// Timestamp for when the game first connected to the proxy.
        /// </summary>
        [DataMember]
        public DateTime ConnectedAt { get; set; }
        #endregion

        #region IEquatable<GameConnection> Implementation
        /// <summary>
        /// Indicates whether this GameConnection is equal to another one.  Equality is
        /// determined by the game's connection time and IP.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(GameConnection other)
        {
            if (other == null)
            {
                return false;
            }

            return (this.ConnectedAt == other.ConnectedAt && this.GameIP == other.GameIP);
        }
        #endregion

        #region Object Overrides
        /// <summary>
        /// Indicates whether this GameConnection is equal to another object.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(Object obj)
        {
            GameConnection connection = obj as GameConnection;
            if (connection == null)
            {
                return false;
            }

            return Equals(connection);
        }

        /// <summary>
        /// Retrieves the hash code for this object.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return this.ConnectedAt.GetHashCode() ^ this.GameIP.GetHashCode();
        }
        #endregion

        #region Operators
        /// <summary>
        /// Indicates whether two GameConnection objects can be considered to be equal.
        /// </summary>
        /// <param name="person1"></param>
        /// <param name="person2"></param>
        /// <returns></returns>
        public static bool operator ==(GameConnection connection1, GameConnection connection2)
        {
            if ((object)connection1 == null || ((object)connection2) == null)
            {
                return Object.Equals(connection1, connection2);
            }

            return connection1.Equals(connection2);
        }

        /// <summary>
        /// Indicates whether two GameConnection objects aren't the same.
        /// </summary>
        /// <param name="person1"></param>
        /// <param name="person2"></param>
        /// <returns></returns>
        public static bool operator !=(GameConnection connection1, GameConnection connection2)
        {
            if (connection1 == null || connection2 == null)
            {
                return !Object.Equals(connection1, connection2);
            }

            return !connection1.Equals(connection2);
        }
        #endregion
    }
}
