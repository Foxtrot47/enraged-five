﻿//---------------------------------------------------------------------------------------------
// <copyright file="PreProcess.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2012-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Processor.Map2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.Composition;
    using System.Diagnostics;
    using SIO = System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml.Linq;
    using RSG.Base.Configuration;
    using RSG.Base.Extensions;
    using RSG.Base.Logging;
    using XGE = RSG.Interop.Incredibuild.XGE;
    using RSG.Pipeline.Core;
    using RSG.Pipeline.Content;
    using RSG.Pipeline.Content.Algorithm;
    using RSG.Pipeline.Services;
    using RSG.Pipeline.Services.AssetPack;
    using RSG.Pipeline.Services.AssetProcessor;
    using RSG.Pipeline.Services.Metadata;
    using RSG.Pipeline.Services.Map.AssetCombine;
    using SXML = RSG.SceneXml;
    using MEXAC = RSG.SceneXml.MapExport.AssetCombine;
    using RSG.Pipeline.Processor.Map2.Data;
    using RSG.Pipeline.Services.Platform;

    /// <summary>
    /// Map preprocessor (MKII); this processor determines all of the map dependencies
    /// and handles DLC-content.
    /// </summary>
    [Export(typeof(IProcessor))]
    class PreProcess :
        MapProcessorBase
    {
        #region Constants
        private static readonly String DESCRIPTION = "Map PreProcess";
        private static readonly String LOG_CTX = "Map PreProcess";
        #endregion // Constants
        
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public PreProcess()
            : base(DESCRIPTION)
        {
        }
        #endregion // Constructor(s)

        #region IProcessor Interface Methods
        /// <summary>
        /// Map PreProcess Prebuild stage.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processes"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="syncDependencies"></param>
        /// <param name="resultantProcesses"></param>
        /// <returns></returns>
        public override bool Prebuild(IEngineParameters param, IEnumerable<IProcess> processes, 
            IProcessorCollection processors, IContentTree owner, 
            out IDictionary<IProcess, IEnumerable<IContentNode>> syncDependencies, 
            out IEnumerable<IProcess> resultantProcesses)
        {
            using (ProfileContext ctx = new ProfileContext(param.Log, "Map PreProcessor Prebuild."))
            {
                return (PrebuildImpl(param, processes, processors, owner, out syncDependencies, out resultantProcesses));
            }
        }

        /// <summary>
        /// Prepare; this should never get called.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processes"></param>
        /// <param name="tools"></param>
        /// <param name="tasks"></param>
        /// <returns></returns>
        public override bool Prepare(IEngineParameters param, IEnumerable<IProcess> processes,
            out IEnumerable<XGE.ITool> tools, out IEnumerable<XGE.ITask> tasks)
        {
            // This should never get hit; the preprocess just creates other
            // processes.
            throw (new NotImplementedException());
        }
        #endregion // IProcessor Interface Methods

        #region Private Methods
        /// <summary>
        /// Validate all processes for map processing; pre-pass in Prebuild.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processes"></param>
        /// <returns></returns>
        private bool ValidateProcesses(IEngineParameters param, IEnumerable<IProcess> processes)
        {
            using (new ProfileContext(param.Log, "Map2::PreProcess::ValidateProcesses"))
            {
                bool result = true;
                foreach (IProcess process in processes)
                {
                    if (!process.Inputs.Any())
                    {
                        param.Log.ErrorCtx(LOG_CTX, "Map PreProcess requires one or more input nodes.");
                        result = false;
                    }

                    if (process.Outputs.Count() != 1)
                    {
                        param.Log.ErrorCtx(LOG_CTX, "Map PreProcess requires one output node.");
                        return false;
                    }

                    IContentNode output = process.Outputs.First();
                    if (!(output is Asset))
                    {
                        param.Log.ErrorCtx(LOG_CTX, "Map PreProcess output must be of type Asset.");
                        return false;
                    }

                    Asset outputAsset = (Asset)output;
                    if (outputAsset.AssetType != RSG.Platform.FileType.ZipArchive)
                    {
                        param.Log.ErrorCtx(LOG_CTX, "Map PreProcess output Asset must have an AssetType of ZipArchive.");
                        return false;
                    }

                    // Processor Parameter validation.
                    result &= ValidateHasParameter(param, this, Parameter.MapProcess_MapCacheDirectory);

                    // Process Parameter validation.
                    // None required yet.
                }

                return result;
            }
        }

        /// <summary>
        /// Map PreProcess Prebuild stage; actual implementation for surrounding
        /// profile call.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processes"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="syncDependencies"></param>
        /// <param name="resultantProcesses"></param>
        /// <returns></returns>
        /// For all of the input processes we:
        ///  1. Group them by combine district.
        ///  2. Extract
        ///  3. Create a Map Metadata Process
        ///  4. Create a Map Collision Process
        ///  5. Where required, create an Asset Pack process for merged content (e.g. dt1_01 => downtown)
        ///  6. An Asset Pack process for the processed content
        ///  7. A RAGE process to convert data.
        ///  
        private bool PrebuildImpl(IEngineParameters param, IEnumerable<IProcess> processes,
            IProcessorCollection processors, IContentTree owner,
            out IDictionary<IProcess, IEnumerable<IContentNode>> syncDependencies,
            out IEnumerable<IProcess> resultantProcesses)
        {
            // Set trivial parameters to allow for early return.
            bool result = true;
            resultantProcesses = new IProcess[0];
            syncDependencies = new Dictionary<IProcess, IEnumerable<IContentNode>>();

            // Validate processes.
            this.LoadParameters(param);
            if (!ValidateProcesses(param, processes))
                return (false);
            
            // 1. Group processes; analyse content-tree to determine what we are going to build.
            ContentTreeHelper treeHelper = new ContentTreeHelper(owner);
            String cacheDirectory = this.GetParameter(Parameter.MapProcess_MapCacheDirectory, String.Empty);
            IEnumerable<MapBuildData> mapsBuildData = new MapBuildData[0];
            using (new ProfileContext(param.Log, "Analysing Content-Tree and Preloading Scenes."))
                mapsBuildData = MapBuildData.Create(param, processors, processes, cacheDirectory);
            // Log some information regarding the groups (and others we have found).
            param.Log.MessageCtx(LOG_CTX, "Map PreProcess: {0} input processes.", processes.Count());
            param.Log.MessageCtx(LOG_CTX, "Map PreProcess: {0} map build groups found.", mapsBuildData.Count());

            ICollection<IProcess> outputProcesses = new List<IProcess>();
            IDictionary<IProcess, IEnumerable<IContentNode>> syncDeps =
                new Dictionary<IProcess, IEnumerable<IContentNode>>();
            
            // Loop through our process groups.
            foreach (MapBuildData mapBuildData in mapsBuildData)
            {
                // 2. Analyse and collect process inputs.
                // 3. Setup Asset Combine.
                // 4. Metadata 
                // 5. Collision
                IDictionary<IProcess, IEnumerable<IContentNode>> groupDependencies;
                IEnumerable<IProcess> groupProcesses;

                switch (mapBuildData.BuildType)
                {
                    case MapBuildType.EnvironmentMerge:
                        if (PrebuildImplProcess(param, mapBuildData, processors, owner,
                            out groupDependencies, out groupProcesses))
                        {
                            outputProcesses.AddRange(groupProcesses);
                            syncDeps.Merge(groupDependencies, false);
                        }
                        else
                        {
                            param.Log.ErrorCtx(LOG_CTX, "Failed to Prebuild Map process group.  See previous errors.");
                            result = false;
                        }
                        break;
                    case MapBuildType.EnvironmentNonMerge:
                    case MapBuildType.EnvironmentArchetypeMerge:
                    case MapBuildType.EnvironmentArchetypesOnly:
                        throw (new NotImplementedException());
                    default:
                        param.Log.ToolErrorCtx(LOG_CTX, "Failed to PreBuild Map process group.  Unrecognised MapBuildType: {0}.", 
                            mapBuildData.BuildType);
                        result = false;
                        break;
                }
            }

            // Final step; kill our preprocess.
            foreach (IProcess p in processes)
                p.State = ProcessState.Discard;

            syncDependencies = syncDeps;
            resultantProcesses = outputProcesses;
            return (result);
        }
        
        /// <summary>
        /// Map PreProcess Prebuild: group of map processes sharing a combine stage.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="mapBuildData"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="syncDependencies"></param>
        /// <param name="resultantProcesses"></param>
        /// <returns></returns>
        private bool PrebuildImplProcess(IEngineParameters param, MapBuildData mapBuildData,
            IProcessorCollection processors, IContentTree owner, 
            out IDictionary<IProcess, IEnumerable<IContentNode>> syncDependencies,
            out IEnumerable<IProcess> resultantProcesses)
        {
            using (new ProfileContext(param.Log, "Map2::PreProcess::PrebuildImplProcessGroup"))
            {
                bool result = true;
                ICollection<IProcess> outputProcesses = new List<IProcess>();
                ICollection<IContentNode> platformConversionInputs = new List<IContentNode>();
                IDictionary<IProcess, IEnumerable<IContentNode>> syncDeps = new Dictionary<IProcess, IEnumerable<IContentNode>>();
                ContentTreeHelper treeHelper = new ContentTreeHelper(owner);

                // During our initial analysis we know the exported data contained within
                // each export zip; here we simply setup the extraction processes.
                IEnumerable<ProcessBuilder> extractionProcesses;
                bool extractProcessResult = CreateExtractionProcess(param, mapBuildData, owner, processors,
                    out extractionProcesses);
                if (!extractProcessResult)
                {
                    param.Log.ToolErrorCtx(LOG_CTX, "Map PreBuild failed to extract map inputs.");
                    result = false;
                }

                // Analyse and process each map input SceneXml file (for Asset Combine).
                IEnumerable<ProcessBuilder> combineAssetCreateProcesses;
                bool assetCombineResult = AnalyseInputsForAssetCombine(param, mapBuildData, owner, 
                    processors, out combineAssetCreateProcesses);
                if (!assetCombineResult)
                {
                    param.Log.ToolErrorCtx(LOG_CTX, "Map PreBuild failed to generate Asset Combine data.");
                    result = false;
                }

                // Metadata; single process to handle Map Metadata.
                ProcessBuilder pbMetadataSerialiser;
                String metadataCacheDirectory = mapBuildData.MetadataCacheDirectory;
                String audioCollisionMapFilename = SIO.Path.Combine(param.CoreBranch.Assets,
                    "maps", "ModelAudioCollisionList.csv");
                String sceneOverridesDirectory = SIO.Path.Combine(param.CoreBranch.Assets, "maps", "scene_overrides");
                bool metadataProcessCreateResult = CreateMetadataSerialiserProcessBuilderForMergeSplit(param,
                    mapBuildData, processors, owner, treeHelper, audioCollisionMapFilename, sceneOverridesDirectory, 
                    syncDeps, platformConversionInputs, out pbMetadataSerialiser);
                if (!metadataProcessCreateResult)
                {
                    param.Log.ToolErrorCtx(LOG_CTX, "Map PreBuild failed to generate Metadata Serialiser ProcessBuilder.");
                    result = false;
                }

                // Collision; multiple processes one for each Map Container.
                IEnumerable<ProcessBuilder> pbCollisionProcesses;
                bool collisionProcessCreateResult = CreateCollisionProcessBuilders(param, mapBuildData,
                    processors, owner, platformConversionInputs, out pbCollisionProcesses);
                if (!collisionProcessCreateResult)
                {
                    param.Log.ToolErrorCtx(LOG_CTX, "Map PreBuild failed to generate Collision ProcessBuilders.");
                    result = false;
                }

                // Occlusion; multiple processes one for each Map Container.
                IEnumerable<ProcessBuilder> pbOcclusionProcesses;
                bool occlusionProcessCreateResult = CreateOcclusionProcessBuilders(param, mapBuildData,
                    processors, owner, platformConversionInputs, out pbOcclusionProcesses);
                if (!occlusionProcessCreateResult)
                {
                    param.Log.ErrorCtx(LOG_CTX, "Map PreBuild failed to generate Occlusion ProcessBuilders.");
                    result = false;
                }
                
                // Queue our process builder objects.
                outputProcesses.AddRange(extractionProcesses.Select(p => p.ToProcess()));
                outputProcesses.AddRange(combineAssetCreateProcesses.Select(p => p.ToProcess()));
                outputProcesses.Add(pbMetadataSerialiser.ToProcess());
                outputProcesses.AddRange(pbCollisionProcesses.Select(p => p.ToProcess()));

                // Create Platform Conversion Processes.
#if false
                List<ProcessBuilder> platformConversionProcesses = new List<ProcessBuilder>();
                if (!CreatePlatformConversionProcesses(param, mapBuildData, owner,
                    processors, platformConversionInputs, platformConversionProcesses))
                {
                    param.Log.ErrorCtx(LOG_CTX, "Failed to create platform conversion processes.");
                    result = false;
                }
                else
                {
                    outputProcesses.AddRange(platformConversionProcesses.Select(p => p.ToProcess()));
                }
#endif

                // Create our resultant RPFs.
                // DHM TODO

                // Create our resultant processed Zips (metadata and collision).
                // These are typically only built on the Asset Builders and used for reporting,
                // DLC and debugging purposes.  They are vital.
                List<ProcessBuilder> processedZipProcesses = new List<ProcessBuilder>();
                if (!CreateProcessedZipPackages(param, mapBuildData, owner, processors, 
                    pbMetadataSerialiser, pbCollisionProcesses, processedZipProcesses))
                {
                    param.Log.ToolErrorCtx(LOG_CTX, "Failed to create processed zip construction processes.");
                    result = false;
                }
                else
                {
                    outputProcesses.AddRange(processedZipProcesses.Select(p => p.ToProcess()));
                }

                // Assign outputs.
                resultantProcesses = outputProcesses;
                syncDependencies = syncDeps;
                return (result);
            }
        }

        /// <summary>
        /// Analyse all input zips and collect filenames.  Note: no zip 
        /// extraction as thats done with XGE in parallel later.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="mapBuildData"></param>
        /// <param name="owner"></param>
        /// <param name="processors"></param>
        /// <param name="pbExtractionProceses"></param>
        /// <returns></returns>
        private bool CreateExtractionProcess(IEngineParameters param, MapBuildData mapBuildData, 
            IContentTree owner, IProcessorCollection processors, 
            out IEnumerable<ProcessBuilder> pbExtractionProceses)
        {
            IProcessor extractionProcessor = processors.GetProcessor(
                "RSG.Pipeline.Processor.Common.AssetExtractionProcessor");

            bool result = true;
            IList<ProcessBuilder> pbZipExtractionProcesses = new List<ProcessBuilder>();

            // Loop through each of the raw MapContainerData objects setting up an
            // extraction process.
            foreach (MapContainerData mapContainerData in mapBuildData.MapsContainerData)
            {
                // Create extraction process.
                ProcessBuilder pbExtract = new ProcessBuilder(extractionProcessor, owner);
                pbExtract.Inputs.Add(owner.CreateFile(mapContainerData.ZipFilename));
                pbExtract.Outputs.Add(owner.CreateDirectory(mapContainerData.ExtractDirectory));
                pbExtract.Outputs.AddRange(mapContainerData.ExtractedFilenames.Select(f => owner.CreateFile(f)));
                pbZipExtractionProcesses.Add(pbExtract);
            }

            // Set output.
            pbExtractionProceses = pbZipExtractionProcesses;
            return (result);
        }

        /// <summary>
        /// Analyse all input SceneXml files.  Note: the Asset Combine exe
        /// is run but no zips are constructed as thats done in XGE in
        /// parallel later.
        /// </summary>
        /// <param name="param">Engine Parameters.</param>
        /// <param name="mapBuildData">Prebuild map data.</param>
        /// <param name="owner">Content-Tree owner.</param>
        /// <param name="processors">All pipeline processors.</param>
        /// <param name="pbZipCreates">Asset combined zip creation processes.</param>
        /// <returns></returns>
        private bool AnalyseInputsForAssetCombine(IEngineParameters param, MapBuildData mapBuildData,
            IContentTree owner, IProcessorCollection processors,
            out IEnumerable<ProcessBuilder> pbZipCreates)
        {
            using (new ProfileContext(param.Log, "Map2::PreProcess::AnalyseInputsForAssetCombine"))
            {
                IProcessor extractionProcessor = processors.GetProcessor(
                    "RSG.Pipeline.Processor.Common.AssetPackProcessor");

                ContentTreeHelper treeHelper = new ContentTreeHelper(mapBuildData.Owner);
                String assetCombineConfigFilename = SIO.Path.Combine(mapBuildData.MergeCombineCacheDirectory,
                    String.Format("asset_combine_{0}_config.meta", mapBuildData.MergeCombineDataOutput.Name));

                // Create directory.
                SIO.Directory.CreateDirectory(mapBuildData.MergeCombineCacheDirectory);

                // Start constructing the Asset Combine configuration data.
                List<AssetCombineConfigInput> assetCombineInputs = new List<AssetCombineConfigInput>();
                IEnumerable<Content.File> allInputSceneXmlFiles = mapBuildData.MapsContainerData.Select(m => 
                    (Content.File)owner.CreateFile(m.SceneXmlFilename));
                foreach (Content.File combineInput in allInputSceneXmlFiles)
                {
                    if (param.Branch.Project.IsDLC && SIO.File.Exists(combineInput.AbsolutePath))
                    {
                        // DLC.
                        String sceneType = treeHelper.GetSceneTypeForExportNode(combineInput);
                        String xmlFilename = combineInput.AbsolutePath;
                        String zipFilename = SIO.Path.ChangeExtension(xmlFilename, ".zip");
                        assetCombineInputs.Add(new AssetCombineConfigInput(xmlFilename, zipFilename, sceneType, true));
                    }

                    // Always add core for combine.
                    String coreSceneType = treeHelper.GetSceneTypeForExportNode(combineInput);
                    String coreXmlFilename = combineInput.AbsolutePath.Replace(param.Branch.Export, param.CoreBranch.Export, StringComparison.OrdinalIgnoreCase);
                    String coreZipFilename = SIO.Path.ChangeExtension(coreXmlFilename, ".zip");
                    assetCombineInputs.Add(new AssetCombineConfigInput(coreXmlFilename, coreZipFilename, coreSceneType, false));
                }

                AssetCombineConfigOutput assetCombineOutput = new AssetCombineConfigOutput(mapBuildData.MergeCombineCacheDirectory, mapBuildData.MergeCombineDocumentFilename);
                AssetCombineConfigTask task = new AssetCombineConfigTask(mapBuildData.MergeCombineDataOutput.Name, assetCombineInputs, assetCombineOutput);
                AssetCombineConfig assetCombineConfig = new AssetCombineConfig(new AssetCombineConfigTask[1] { task });
                assetCombineConfig.Save(assetCombineConfigFilename); // Saved for debugging only.
                                
                // Run the combine process algorithms inline; this saves us loading the SceneXml
                // data more than once in Prebuild (which is what happens with the algorithms being
                // out-of-process).
                using (new ProfileContext(param.Log, "Asset Combine Analysis: Running Process inline."))
                {
                    if (!AnalyseInputsForAssetCombineConfig(param, mapBuildData, task,
                        mapBuildData.MergeCombineDocumentFilename))
                    {
                        param.Log.Error("Asset Combine processing failed.  See previous errors.");
                        pbZipCreates = new ProcessBuilder[0];
                        return (false);
                    }
                }
            
                // Create merged Drawable and Texture Dictionary Zip Creation Processes.
                using (new ProfileContext(param.Log, "Asset Combine Process Construction."))
                {
                    ICollection<ProcessBuilder> assetCombineProcesses = new List<ProcessBuilder>();
                    IProcessor assetProcessor = processors.GetProcessor("RSG.Pipeline.Processor.Common.AssetProcessor");
                    AssetCombineDocument combineDocument = AssetCombineDocument.Load(mapBuildData.MergeCombineDocumentFilename);
                    foreach (DrawableDictionary drawableDictionary in combineDocument.Map.DrawableDictionaries)
                    {
                        if (!drawableDictionary.Inputs.Any())
                        {
                            param.Log.Error("Combined Drawable Dictionary '{0}' has no inputs.  Asset Combine problem?  Skipping.",
                                drawableDictionary.Name);
                            continue;
                        }

                        String drawableDictionaryFilename = SIO.Path.Combine(mapBuildData.MergeCombineCacheDirectory,
                            drawableDictionary.Name + ".idd.zip");

                        ProcessBuilder pbDrawableDictionary = new ProcessBuilder(assetProcessor, owner);
                        pbDrawableDictionary.Inputs.AddRange(drawableDictionary.Inputs.Select(i => 
                            owner.CreateFile(SIO.Path.Combine(i.CacheDirectory, i.Name + ".idr.zip"))));
                        pbDrawableDictionary.Outputs.Add(owner.CreateFile(drawableDictionaryFilename));
                        
                        IDictionary<Operation, IEnumerable<IContentNode>> ops = new Dictionary<Operation, IEnumerable<IContentNode>>();
                        ops.Add(AssetProcessorHelper.CreateOperationDrawableMerge());

                        pbDrawableDictionary.SetParameter(Constants.AssetProcessor_Operations, ops);
                        assetCombineProcesses.Add(pbDrawableDictionary);
                    }
                    foreach (TextureDictionary textureDictionary in combineDocument.Map.TextureDictionaries)
                    {
                        if (!textureDictionary.Inputs.Any())
                        {
                            param.Log.Error("Combined Texture Dictionary '{0}' has no inputs.  Asset Combine problem?  Skipping.",
                                textureDictionary.Name);
                            continue;
                        }

                        String textureDictionaryFilename = SIO.Path.Combine(mapBuildData.MergeCombineCacheDirectory,
                            textureDictionary.Name + ".itd.zip");

                        ProcessBuilder pbTextureDictionary = new ProcessBuilder(assetProcessor, owner);
                        pbTextureDictionary.Inputs.AddRange(textureDictionary.Inputs.Select(i => 
                            owner.CreateFile(SIO.Path.Combine(i.CacheDirectory, i.Name + ".itd.zip"))));
                        pbTextureDictionary.Outputs.Add(owner.CreateFile(textureDictionaryFilename));

                        IDictionary<Operation, IEnumerable<IContentNode>> ops = new Dictionary<Operation, IEnumerable<IContentNode>>();
                        ops.Add(AssetProcessorHelper.CreateOperationMergeFiles(pbTextureDictionary.Inputs));

                        pbTextureDictionary.SetParameter(Constants.AssetProcessor_Operations, ops);
                        assetCombineProcesses.Add(pbTextureDictionary);
                    }
                    pbZipCreates = assetCombineProcesses;
                }            

                return (true);
            }
        }

        /// <summary>
        /// Run the Asset Combine process algorithm (this replaces the previous 
        /// MapAssetCombine executable saving us loading the SceneXml data
        /// more than once in Prebuild).
        /// </summary>
        /// <param name="param"></param>
        /// <param name="mapBuildData"></param>
        /// <param name="combineTask"></param>
        /// <param name="outputFilename"></param>
        /// <returns></returns>
        private bool AnalyseInputsForAssetCombineConfig(IEngineParameters param, 
            MapBuildData mapBuildData, AssetCombineConfigTask combineTask, String outputFilename)
        {
            List<AssetCombineInput> processorInputs = new List<AssetCombineInput>();
            IEnumerable<AssetCombineConfigInput> dlcInputs = combineTask.Inputs.Where(i => i.IsDLC);
            IEnumerable<AssetCombineConfigInput> coreInputs = combineTask.Inputs.Where(i => !i.IsDLC);
            List<AssetCombineConfigInput> coreInputsToIgnore = new List<AssetCombineConfigInput>();

            // Loop through each DLC input first.
            foreach (AssetCombineConfigInput configInput in dlcInputs)
            {
                // Determine if we have an equivalent core input.
                AssetCombineConfigInput coreInput = coreInputs.FirstOrDefault(i => String.Equals(
                    SIO.Path.GetFileName(i.ZipPathname), 
                    SIO.Path.GetFileName(configInput.ZipPathname), StringComparison.OrdinalIgnoreCase));
                if (null != coreInput)
                {
                    // Load DLC Scene.
                    String dlcSceneFilename = configInput.SceneXmlPathname;
                    String coreSceneFilename = coreInput.SceneXmlPathname;
                    SXML.Scene dlcScene = LoadCachedScene(param, mapBuildData, dlcSceneFilename);
                    SXML.Scene coreScene = LoadCachedScene(param, mapBuildData, coreSceneFilename);

                    AssetCombineInput input = AssetCombineInput.Create(coreInput, coreScene, coreScene.SceneType, configInput, dlcScene, dlcScene.SceneType);
                    processorInputs.Add(input);
                    coreInputsToIgnore.Add(coreInput);
                }
                else
                {
                    String sceneXmlFilename = configInput.SceneXmlPathname;
                    SXML.Scene scene = LoadCachedScene(param, mapBuildData, sceneXmlFilename);
                    processorInputs.Add(AssetCombineInput.Create(configInput, scene));
                }
            }

            // Loop through each Core input first.
            foreach (AssetCombineConfigInput configInput in coreInputs.Except(coreInputsToIgnore))
            {
                String sceneXmlFilename = configInput.SceneXmlPathname;
                SXML.Scene scene = LoadCachedScene(param, mapBuildData, sceneXmlFilename);
                processorInputs.Add(AssetCombineInput.Create(configInput, scene));
            }

            // Run the collating algorithms.
            // DHM FIX ME: make the AssetCombineProcessor use the new method for creating the Asset Combine Document (AssetCombineDocumentBuilder)
            AssetCombineDocument document = AssetCombineProcessor.ProcessArea(param.Log, param.Branch, 
                processorInputs, combineTask.Output.CacheDirectory, combineTask.Name);
            SIO.Directory.CreateDirectory(SIO.Path.GetDirectoryName(outputFilename)); 
            document.Save(outputFilename);
            
            return (true);
        }

        /// <summary>
        /// Create Map Collision ProcessBuilder (if applicable).
        /// </summary>
        /// <param name="param"></param>
        /// <param name="mapBuildData"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="platformConversionInputs"></param>
        /// <param name="collisionProcessBuilders"></param>
        /// <returns></returns>
        private bool CreateCollisionProcessBuilders(IEngineParameters param,
            MapBuildData mapBuildData, IProcessorCollection processors, 
            IContentTree owner, ICollection<IContentNode> platformConversionInputs,
            out IEnumerable<ProcessBuilder> collisionProcessBuilders)
        {
            IList<ProcessBuilder> pbCollisionProcesses = new List<ProcessBuilder>();

            // For each merge process; we run a single process that brings in all
            // the collision for each container (separate _collision zip file).
            foreach (MapContainerMergeData mapMergeData in mapBuildData.MapsContainerMergeData)
            {
                ProcessBuilder pbColl = new ProcessBuilder("RSG.Pipeline.Processor.Map2.BoundsProcessor", owner);
                pbColl.SetParameter(Parameter.MapProcess_AssociatedMapBuildData, mapBuildData);
                pbColl.SetParameter(Parameter.MapProcess_AssociatedMapMergeContainerData, mapMergeData);

                IContentNode boundsCacheDirectoryNode = owner.CreateDirectory(mapMergeData.BoundsCacheDirectory);
                pbColl.Outputs.Add(boundsCacheDirectoryNode);
                platformConversionInputs.Add(boundsCacheDirectoryNode);

                foreach (MapContainerData mapContainerData in mapMergeData.MapContainers)
                {
                    IEnumerable<String> collisionFiles = GroupCollisionData(mapContainerData.ExtractedFilenames);
                    if (!collisionFiles.Any())
                        continue;
                    
                    pbColl.Inputs.AddRange(collisionFiles.Select(f => owner.CreateFile(f)));
                }
                
                if (pbColl.Inputs.Any())
                    pbCollisionProcesses.Add(pbColl);
            }

            collisionProcessBuilders = pbCollisionProcesses;
            return (true);
        }

        /// <summary>
        /// Create Map Occlusion ProcessBuilder (if applicable).
        /// </summary>
        /// <param name="param"></param>
        /// <param name="mapBuildData"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="platformConversionInputs"></param>
        /// <param name="occlusionProcessBuilders"></param>
        /// <returns></returns>
        private bool CreateOcclusionProcessBuilders(IEngineParameters param,
            MapBuildData mapBuildData, IProcessorCollection processors,
            IContentTree owner, ICollection<IContentNode> platformConversionInputs, 
            out IEnumerable<ProcessBuilder> occlusionProcessBuilders)
        {
            // DHM FIX ME
#if false
            IList<ProcessBuilder> pbOcclusionProcesses = new List<ProcessBuilder>();
            foreach (MapContainerMergeData mapData in mapBuildData.MapsContainerData)
            {
                IProcess process = mapData.PreProcess;
                IEnumerable<String> occlusionFiles = GroupOcclusionData(extractedFiles[process]);
                if (!occlusionFiles.Any())
                    continue;

                ProcessBuilder pbOccl = new ProcessBuilder("RSG.Pipeline.Processor.Map2.OcclusionProcessor",
                    processors, owner);
                pbOccl.Inputs.AddRange(occlusionFiles.Select(f => owner.CreateFile(f)));
                pbOccl.Outputs.Add(owner.CreateDirectory(mapData.OcclusionCacheDirectory));
                // We attach the associated MapContainerMergeData object for processing;
                // rather than a bazillion parameters.
                pbOccl.SetParameter(Parameter.MapProcess_AssociatedMapContainerData, mapData);
                pbOcclusionProcesses.Add(pbOccl);
            }

            occlusionProcessBuilders = pbOcclusionProcesses;
#endif
            occlusionProcessBuilders = new List<ProcessBuilder>();
            return (true);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processes"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="mapExportAdditionsPathname"></param>
        /// <param name="audioCollisionMapPathname"></param>
        /// <param name="sceneOverridesDirectory"></param>
        /// <param name="metadataPb"></param>
        /// <returns></returns>
        private bool CreateMetadataSerialiserProcessBuilder(IEngineParameters param,
            IEnumerable<IProcess> processes, IProcessorCollection processors,
            IContentTree owner, String mapExportAdditionsPathname, 
            String audioCollisionMapPathname, String sceneOverridesDirectory, 
            out ProcessBuilder metadataPb)
        {
            using (new ProfileContext(param.Log, "Map2::PreProcess::CreateMetadataSerialiserProcessBuilder"))
            {
                throw (new NotImplementedException());
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <param name="mapBuildData"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="treeHelper"></param>
        /// <param name="audioCollisionMapFilename"></param>
        /// <param name="sceneOverridesDirectory"></param>
        /// <param name="syncDependencies"></param>
        /// <param name="platformConversionInputs"></param>
        /// <param name="metadataPb"></param>
        /// <returns></returns>
        private bool CreateMetadataSerialiserProcessBuilderForMergeSplit(IEngineParameters param,
            MapBuildData mapBuildData, IProcessorCollection processors,
            IContentTree owner, ContentTreeHelper treeHelper, 
            String audioCollisionMapFilename, String sceneOverridesDirectory,
            IDictionary<IProcess, IEnumerable<IContentNode>> syncDependencies,
            ICollection<IContentNode> platformConversionInputs,
            out ProcessBuilder metadataPb)
        {
            using (new ProfileContext(param.Log, "Map2::PreProcess::CreateMetadataSerialiserProcessBuilderForMergeSplit"))
            {
                IEnumerable<IContentNode> allInputSceneXmlFiles = mapBuildData.MapsContainerData.Select(m => owner.CreateFile(m.SceneXmlFilename));
                Content.Asset outputAsset = (Content.Asset)owner.CreateFile(mapBuildData.ProcessedMetadataZipFilename);
                String metadataOutputDirectory = mapBuildData.MetadataCacheDirectory;
                IProcessor metadataProcessor = processors.GetProcessor("RSG.Pipeline.Processor.Map2.MetadataProcessor");
                Debug.Assert(null != metadataProcessor, "Map Metadata Serialiser processor not defined.");
                metadataPb = new ProcessBuilder(metadataProcessor, owner);
                metadataPb.Inputs.AddRange(allInputSceneXmlFiles);
                metadataPb.SetParameter(Parameter.MapProcess_AssociatedMapBuildData, mapBuildData);

                // DHM FIX ME: remove these parameters
                // OLD
                metadataPb.SetParameter(Parameter.MapProcess_MetadataAudioCollisionFilename,
                    audioCollisionMapFilename);
                metadataPb.SetParameter(Parameter.MapProcess_MetadataSceneOverridesDirectory,
                    sceneOverridesDirectory);
                // END OLD

                // If we are converting DLC then we add the parameter to mark up the Core Metadata Zip.
                if (param.Branch.Project.IsDLC)
                {
                    String metadataZipFilename = outputAsset.AbsolutePath;
                    String coreMetadataZipFilename = metadataZipFilename.Replace(param.Branch.Processed,
                        param.CoreBranch.Processed, StringComparison.OrdinalIgnoreCase);
                    metadataPb.SetParameter(Constants.ParamMap_CoreMetadataZip, coreMetadataZipFilename);
#if false
                    if (!syncDependencies.ContainsKey(mapBuildData.MetadataMergeProcess))
                        syncDependencies.Add(mapBuildData.MetadataMergeProcess, new List<IContentNode>());
                    syncDependencies[mapBuildData.MetadataMergeProcess].Add(owner.CreateFile(coreMetadataZipFilename));
#endif
                }

                // As runtime want IMAP merge to stay as is, but ITYP merge to be global,
                // we have fairly complex information to the metadata serialiser.
                // We need to go back to the associated zips to find the IMAP merge (based on common processed output) rules.
                IDictionary<IContentNode, List<IContentNode>> processedToXmlInputsMap = new Dictionary<IContentNode, List<IContentNode>>();
                foreach (IContentNode inputSceneXmlNode in allInputSceneXmlFiles)
                {
                    if (param.Branch.Project.IsDLC)
                    {
                        // For DLC we add the DLC SceneXml if it exists; otherwise fallback to core project.
                        IFilesystemNode inputSceneXmlFile = (IFilesystemNode)inputSceneXmlNode;
                        if (SIO.File.Exists(inputSceneXmlFile.AbsolutePath))
                            metadataPb.Inputs.Add(inputSceneXmlNode);
                        else
                        {
                            String coreSceneXmlFilename = inputSceneXmlFile.AbsolutePath.Replace(
                                param.Branch.Export, param.CoreBranch.Export, StringComparison.OrdinalIgnoreCase);
                            metadataPb.Inputs.Add(owner.CreateFile(coreSceneXmlFilename));
                        }
                    }
                    else
                    {
                        // Default case; just add the current SceneXml filename.
                        metadataPb.Inputs.Add(inputSceneXmlNode);
                    }
                }
                
                IContentNode audioCollisionMapFileNode = owner.CreateFile(audioCollisionMapFilename);
                IContentNode sceneOverridesDirectoryNode = owner.CreateDirectory(sceneOverridesDirectory);
                metadataPb.Inputs.Add(audioCollisionMapFileNode);
                metadataPb.Inputs.Add(sceneOverridesDirectoryNode);
                
                // Setup metadata serialiser output parameters
                IContentNode metadataOutputDirectoryNode = (Content.Directory)owner.CreateDirectory(metadataOutputDirectory);
                metadataPb.Outputs.Add(metadataOutputDirectoryNode);
                metadataPb.Outputs.Add(owner.CreateFile(mapBuildData.MetadataManifestAdditionsFilename));

                return (true);
            }    
        }

        /// <summary>
        /// Create our platform processes for all the passed in process builders output.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="mapBuildData"></param>
        /// <param name="inputs"></param>
        /// <param name="platformConversionProcesses"></param>
        /// <returns></returns>
        private bool CreatePlatformConversionProcesses(IEngineParameters param, MapBuildData mapBuildData,
            IContentTree owner, IProcessorCollection processors, IEnumerable<IContentNode> inputs, 
            ICollection<ProcessBuilder> platformConversionProcesses)
        {
            using (new ProfileContext(param.Log, "Map2::PreProcess::CreatePlatformConversionProcesses"))
            {
                foreach (IContentNode input in inputs)
                {
                    if (input is Directory)
                    {
                        IEnumerable<ProcessBuilder> pbConverts = PlatformProcessBuilder.CreateSimpleResourcesInCacheForDirectory(
                            param, processors, owner, (Directory)input);
                        platformConversionProcesses.AddRange(pbConverts);
                    }
                    else if (input is Asset)
                    {
                        IEnumerable<ProcessBuilder> pbConverts = PlatformProcessBuilder.CreateSimpleResourceInCache(
                            param, processors, owner, input);
                        platformConversionProcesses.AddRange(pbConverts);
                    }
                }
                return (true);
            }
        }

        /// <summary>
        /// Create our processed zip packages (if required for the current user).  This includes
        /// the Metadata Zip and Collision Zip.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="mapBuildData"></param>
        /// <param name="owner"></param>
        /// <param name="processors"></param>
        /// <param name="pbMetadataProcesses"></param>
        /// <param name="pbCollisionProcesses"></param>
        /// <param name="outputProcesses"></param>
        /// <returns></returns>
        private bool CreateProcessedZipPackages(IEngineParameters param, MapBuildData mapBuildData,
            IContentTree owner, IProcessorCollection processors, ProcessBuilder pbMetadataProcess, 
            IEnumerable<ProcessBuilder> pbCollisionProcesses, ICollection<ProcessBuilder> outputProcesses)
        {
            if (!IsCurrentUserToBuildProcessedZips(param))
                return (true);

            using (new ProfileContext(param.Log, "Map2::PreProcess::CreateProcessedZipPackages"))
            {
                bool result = true;
                if (!CreateProcessedMetadataZipProcessBuilder(param, mapBuildData, owner, processors,
                    pbMetadataProcess, outputProcesses))
                {
                    param.Log.Error("Failed to create Processed Metadata Zip process.");
                    result = false;
                }

                if (!CreateProcessedCollisionZipProcessBuilders(param, mapBuildData, owner, processors,
                    pbCollisionProcesses, outputProcesses))
                {
                    param.Log.Error("Failed to create Processed Collision Zip processes.");
                    result = false;
                }
                return (result);
            }
        }

        /// <summary>
        /// Create Processed Metadata Zip processes.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="mapBuildData"></param>
        /// <param name="owner"></param>
        /// <param name="processors"></param>
        /// <param name="pbMetadataProcesses"></param>
        /// <param name="pbMetadataZipBuilders"></param>
        /// <returns></returns>
        private bool CreateProcessedMetadataZipProcessBuilder(IEngineParameters param, MapBuildData mapBuildData,
            IContentTree owner, IProcessorCollection processors, ProcessBuilder pbMetadataProcess, 
            ICollection<ProcessBuilder> pbMetadataZipBuilders)
        {
            IProcessor processor = processors.GetProcessor("RSG.Pipeline.Processor.Common.AssetPackProcessor");
            bool result = true;
            switch (mapBuildData.BuildType)
            {
                case MapBuildType.EnvironmentArchetypeMerge:
                case MapBuildType.EnvironmentMerge:
                    {
                        // Build Asset Pack Config (DHM FIX ME: Asset Pack Prebuild should do this!!)
                        String basename = SIO.Path.GetFileNameWithoutExtension(mapBuildData.ProcessedMetadataZipFilename);
                        String configFilename = SIO.Path.Combine(mapBuildData.CacheDirectory, "asset_pack", basename + "_config.xml");
                        AssetPackScheduleBuilder scheduleBuilder = new AssetPackScheduleBuilder();
                        AssetPackZipArchive zipArchive = scheduleBuilder.AddZipArchive(mapBuildData.ProcessedMetadataZipFilename);
                        pbMetadataProcess.Outputs.OfType<Content.File>().ForEach(o => zipArchive.AddFile(o.AbsolutePath));
                        pbMetadataProcess.Outputs.OfType<Content.Directory>().ForEach(o => zipArchive.AddDirectory(o.AbsolutePath));
                        scheduleBuilder.Save(configFilename);

                        ProcessBuilder pbProcessedZip = new ProcessBuilder(processor, owner);
                        pbProcessedZip.Inputs.Add(owner.CreateFile(configFilename));
                        pbProcessedZip.Outputs.Add(owner.CreateFile(mapBuildData.ProcessedMetadataZipFilename));
                        pbMetadataZipBuilders.Add(pbProcessedZip);
                    }
                    break;
                case MapBuildType.EnvironmentArchetypesOnly:
                case MapBuildType.EnvironmentNonMerge:
                    throw (new NotImplementedException());
                default:
                    param.Log.ToolError("Unrecognised MapBuildType: '{0}'.  Internal error.", mapBuildData.BuildType);
                    result = false;
                    break;
            }

            return (result);
        }

        /// <summary>
        /// Create Processed Collision Zip processes.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="mapBuildData"></param>
        /// <param name="owner"></param>
        /// <param name="processors"></param>
        /// <param name="pbCollisionProcesses"></param>
        /// <param name="pbCollisionZipBuilders"></param>
        /// <returns></returns>
        private bool CreateProcessedCollisionZipProcessBuilders(IEngineParameters param, MapBuildData mapBuildData,
            IContentTree owner, IProcessorCollection processors, IEnumerable<ProcessBuilder> pbCollisionProcesses, 
            ICollection<ProcessBuilder> pbCollisionZipBuilders)
        {
            IProcessor processor = processors.GetProcessor("RSG.Pipeline.Processor.Common.AssetPackProcessor");
            foreach (ProcessBuilder process in pbCollisionProcesses)
            {
                MapContainerMergeData mergeData = (MapContainerMergeData)process.GetParameter(
                    Parameter.MapProcess_AssociatedMapMergeContainerData, null);

                // Build Asset Pack Config (DHM FIX ME: Asset Pack Prebuild should do this!!)
                String basename = SIO.Path.GetFileNameWithoutExtension(mergeData.ProcessedBoundsZipFilename);
                String configFilename = SIO.Path.Combine(mapBuildData.CacheDirectory, "asset_pack", basename + "_config.xml");
                AssetPackScheduleBuilder scheduleBuilder = new AssetPackScheduleBuilder();
                AssetPackZipArchive zipArchive = scheduleBuilder.AddZipArchive(mergeData.ProcessedBoundsZipFilename);
                process.Outputs.OfType<Content.File>().ForEach(o => zipArchive.AddFile(o.AbsolutePath));
                process.Outputs.OfType<Content.Directory>().ForEach(o => zipArchive.AddDirectory(o.AbsolutePath));
                scheduleBuilder.Save(configFilename);

                ProcessBuilder pbProcessedZip = new ProcessBuilder(processor, owner);
                pbProcessedZip.Inputs.Add(owner.CreateFile(configFilename));
                pbProcessedZip.Outputs.Add(owner.CreateFile(mergeData.ProcessedBoundsZipFilename));

                pbCollisionZipBuilders.Add(pbProcessedZip);
            }

            return (true);
        }

        /// <summary>
        /// Attempt to access Scene from cache; otherwise load it.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="mapBuildData"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
        private SXML.Scene LoadCachedScene(IEngineParameters param, MapBuildData mapBuildData, String filename)
        {
            MapContainerData map = mapBuildData.MapsContainerData.FirstOrDefault(m =>
                m.SceneXmlFilename.Equals(filename, StringComparison.OrdinalIgnoreCase));
            if (null != map)
                return (map.Scene);

            String assetBasename = SIO.Path.GetFileNameWithoutExtension(filename);
            param.Log.WarningCtx(assetBasename, "SceneXml file not cached; loading '{0}'.", filename);
            return (new SXML.Scene(SXML.SceneType.Unknown, filename, SXML.LoadOptions.Objects, true));
        }

        /// <summary>
        /// Determine whether the current user has to build processed zip data.
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        private bool IsCurrentUserToBuildProcessedZips(IEngineParameters param)
        {
            IConfig config = param.Branch.Project.Config;
            IUsertype builderClientUsertype = config.Usertypes.FirstOrDefault(
                usertype => "builder_client".Equals(usertype.Name, StringComparison.OrdinalIgnoreCase));
            IUsertype builderServerUsertype = config.Usertypes.FirstOrDefault(
                usertype => "builder_server".Equals(usertype.Name, StringComparison.OrdinalIgnoreCase));
            bool userRequiresProcessedZipBuilds = ((builderClientUsertype != null) && ((config.Usertype & builderClientUsertype.Flags) != 0)) ||
                                                  ((builderServerUsertype != null) && ((config.Usertype & builderServerUsertype.Flags) != 0));
            
            bool overrideRequiresProcessedZipBuilds = this.GetParameter(Parameter.MapProcess_ForceProcessedMapZipCreation, false);
            return (userRequiresProcessedZipBuilds || overrideRequiresProcessedZipBuilds);
        }

        /// <summary>
        /// Return files that are for collision processor.
        /// </summary>
        /// <param name="extractedFiles"></param>
        /// <returns></returns>
        private IEnumerable<String> GroupCollisionData(IEnumerable<String> extractedFiles)
        {
            RSG.Base.IO.Wildcard wcCollision = new RSG.Base.IO.Wildcard(Wildcards.CollisionWildcard);
            return (extractedFiles.Where(f => wcCollision.IsMatch(f)));
        }

        /// <summary>
        /// Return files that are for occlusion processor.
        /// </summary>
        /// <param name="extractedFiles"></param>
        /// <returns></returns>
        private IEnumerable<String> GroupOcclusionData(IEnumerable<String> extractedFiles)
        {
            RSG.Base.IO.Wildcard wcCollision = new RSG.Base.IO.Wildcard(Wildcards.OcclusionWildcard);
            return (extractedFiles.Where(f => wcCollision.IsMatch(f)));
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Processor.Map namespace
