﻿//---------------------------------------------------------------------------------------------
// <copyright file="CustomRBSManager.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Processor.Map2.AssetCombine
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// 
    /// </summary>
    /// DHM TODO: move into RSG.Pipeline.Services.Platform?
    class CustomRBSManager
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public CustomRBSManager()
        {
            dataInitialised_ = false;
            customRBSPathnameMap_ = new Dictionary<String, String>();
            embeddedTextureCustomRBSPathnameMap_ = new Dictionary<String, String>();
        }
        #endregion // Constructor(s)

        #region Internal Methods
        internal void CreateCustomRBSFiles(string cacheDirectory)
        {
            if(dataInitialised_)
                return;

            foreach (String supportedLODLevel in supportedLODLevels_)
            {
                String customRBSPathname = 
                    Path.Combine(cacheDirectory, dataSubDirectoryName_, supportedLODLevel, customRBSFilename_);
                CreateCustomRBSFile(supportedLODLevel, false, customRBSPathname);
                customRBSPathnameMap_.Add(supportedLODLevel, customRBSPathname);

                String embeddedTextureCustomRBSPathname = 
                    Path.Combine(cacheDirectory, dataSubDirectoryName_, supportedLODLevel, embeddedTextureDirectoryName_, customRBSFilename_);
                CreateCustomRBSFile(supportedLODLevel, true, embeddedTextureCustomRBSPathname);
                embeddedTextureCustomRBSPathnameMap_.Add(supportedLODLevel, embeddedTextureCustomRBSPathname);
            }

            customFinishPathname_ = Path.Combine(cacheDirectory, dataSubDirectoryName_, customFinishRBSFilename_);
            CreateCustomFinishRBSFile(false, customFinishPathname_);

            embeddedTextureCustomFinishPathname_ = Path.Combine(cacheDirectory, dataSubDirectoryName_, embeddedTextureDirectoryName_, customFinishRBSFilename_);
            CreateCustomFinishRBSFile(true, embeddedTextureCustomFinishPathname_);

            dataInitialised_ = true;
        }

        internal String GetCustomRBSPathname(String lodLevel, bool hasEmbeddedTextures)
        {
            Debug.Assert(dataInitialised_);

            if (hasEmbeddedTextures)
            {
                Debug.Assert(embeddedTextureCustomRBSPathnameMap_.ContainsKey(lodLevel));
                return embeddedTextureCustomRBSPathnameMap_[lodLevel];
            }
            else
            {
                Debug.Assert(customRBSPathnameMap_.ContainsKey(lodLevel));
                return customRBSPathnameMap_[lodLevel];
            }
        }

        internal String GetCustomFinishRBSPathname(bool hasEmbeddedTextures)
        {
            Debug.Assert(dataInitialised_);

            if (hasEmbeddedTextures)
            {
                return embeddedTextureCustomFinishPathname_;
            }
            else
            {
                return customFinishPathname_;
            }
        }
        #endregion Internal Methods

        #region Private Methods
        private void CreateCustomRBSFile(String lodLevel, bool hasEmbeddedTextures, String pathname)
        {
            String directoryName = Path.GetDirectoryName(pathname);
            if (!Directory.Exists(directoryName))
                Directory.CreateDirectory(directoryName);

            using (System.IO.StreamWriter streamWriter = System.IO.File.CreateText(pathname))
            {
                streamWriter.WriteLine("set_dwd_type( \"{0}\" )", lodLevel);
                if (hasEmbeddedTextures)
                    streamWriter.WriteLine("set_auto_texdict( \"true\" )");
            }
        }

        private void CreateCustomFinishRBSFile(bool hasEmbeddedTextures, String pathname)
        {
            String directoryName = Path.GetDirectoryName(pathname);
            if (!Directory.Exists(directoryName))
                Directory.CreateDirectory(directoryName);

            using (System.IO.StreamWriter streamWriter = System.IO.File.CreateText(pathname))
            {
                if (hasEmbeddedTextures)
                    streamWriter.WriteLine("set_auto_texdict( \"false\" )");
                streamWriter.WriteLine("set_dwd_type( \"HD\" )");
            }
        }
        #endregion Private Methods

        #region Private Data
        private readonly String[] supportedLODLevels_ = new String[] { "HD", "LOD", "SLOD1", "SLOD2", "SLOD3" };
        private readonly String dataSubDirectoryName_ = "custom_rbs";
        private readonly String embeddedTextureDirectoryName_ = "embedded";
        private readonly String customRBSFilename_ = "custom.rbs";
        private readonly String customFinishRBSFilename_ = "custom_finish.rbs";

        private bool dataInitialised_;
        private Dictionary<String, String> customRBSPathnameMap_;
        private Dictionary<String, String> embeddedTextureCustomRBSPathnameMap_;
        private String customFinishPathname_;
        private String embeddedTextureCustomFinishPathname_;
        #endregion Private Data
    }

} // RSG.Pipeline.Processor.Map2.AssetCombine namespace
