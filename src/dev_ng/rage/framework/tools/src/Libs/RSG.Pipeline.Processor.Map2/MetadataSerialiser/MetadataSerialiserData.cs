﻿//---------------------------------------------------------------------------------------------
// <copyright file="MetadataSerialiserData.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Processor.Map2.MetadataSerialiser
{
    using System;
    using System.Collections.Generic;
    using RSG.Pipeline.Core;

    /// <summary>
    /// 
    /// </summary>
    internal class MetadataSerialiserData
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public IContentNode Output { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<IContentNode> InputScenes { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<IContentNode> Dependencies { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public String AdditionsFilename { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="output"></param>
        /// <param name="inputs"></param>
        /// <param name="dependencies"></param>
        /// <param name="additionsFilename"></param>
        public MetadataSerialiserData(IContentNode output, IEnumerable<IContentNode> inputs, 
            IEnumerable<IContentNode> dependencies, String additionsFilename)
        {
            this.Output = output;
            this.InputScenes = inputs;
            this.Dependencies = dependencies;
            this.AdditionsFilename = additionsFilename;
        }
        #endregion // Constructor(s)
    }

} // RSG.Pipeline.Processor.Map2.MetadataSerialiser namespace
