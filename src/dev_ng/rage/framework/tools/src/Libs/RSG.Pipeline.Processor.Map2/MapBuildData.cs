﻿//---------------------------------------------------------------------------------------------
// <copyright file="MapBuildData.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Processor.Map2
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using SIO = System.IO;
    using System.Linq;
    using RSG.Base.Configuration;
    using RSG.Base.Extensions;
    using RSG.Base.Logging.Universal;
    using RSG.Pipeline.Core;
    using RSG.Pipeline.Content;
    using RSG.Pipeline.Content.Algorithm;
    using RSG.SceneXml;

    /// <summary>
    /// Class representing a single map asset build; all of the various types of "map"
    /// we support.
    /// </summary>
    /// Note: this abstract should allow us to change the map content-tree but keep
    /// the map PreProcess and pipeline the same.
    /// 
    internal class MapBuildData
    {
        #region Enumerations
        /// <summary>
        /// Map build type; what we're going to build, determine fully from content-tree.
        /// </summary>
        public enum MapBuildType
        {
            /// <summary>
            /// Regular environment merge; metadata and LOD siblings merged.
            /// </summary>
            EnvironmentMerge,

            /// <summary>
            /// Regular environment build; no special merging.
            /// </summary>
            EnvironmentNonMerge,

            /// <summary>
            /// Environment archetypes environment build; interior and props only.
            /// </summary>
            EnvironmentArchetypesOnly,

            /// <summary>
            /// Environment archetypes environment build; merge props/interiors.
            /// </summary>
            EnvironmentArchetypeMerge,
        }
        #endregion // Enumerations

        #region Properties
        /// <summary>
        /// Map build type; type of build this object represents.
        /// </summary>
        public MapBuildType BuildType
        {
            get;
            private set;
        }

        /// <summary>
        /// Map cache root directory.
        /// </summary>
        public String CacheDirectory
        {
            get;
            private set;
        }

        /// <summary>
        /// Map merge combine output (can be null depending on BuildType).
        /// </summary>
        public IContentNode MergeCombineDataOutput
        {
            get;
            private set;
        }

        /// <summary>
        /// Map merge combine cache directory.
        /// </summary>
        public String MergeCombineCacheDirectory
        {
            get;
            private set;
        }

        /// <summary>
        /// Map metadata output node.
        /// </summary>
        public IContentNode MetadataOutput
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public bool MetadataExportArchetypes
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public bool MetadataExportEntities
        {
            get;
            private set;
        }

        /// <summary>
        /// Map metadata cache directory.
        /// </summary>
        public String MetadataCacheDirectory
        {
            get;
            private set;
        }

        /// <summary>
        /// Map metadata process (can be null depending on BuildType).
        /// </summary>
        public IProcess MetadataMergeProcess
        {
            get;
            private set;
        }

        /// <summary>
        /// Regular Map preprocesses.
        /// </summary>
        public IProcess[] PreProcesses
        {
            get;
            private set;
        }

        /// <summary>
        /// SceneXml inputs.
        /// </summary>
        public Content.File[] SceneXmlInputs
        {
            get;
            private set;
        }

        /// <summary>
        /// Scene objects.
        /// </summary>
        public IDictionary<String, RSG.SceneXml.Scene> Scenes
        {
            get;
            private set;
        }

        /// <summary>
        /// SceneXml dependencies (e.g. referenced SceneXml files for props and interiors).
        /// </summary>
        public Content.File[] SceneXmlDependencies
        {
            get;
            private set;
        }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// Constructor; for Environment and Archetype Merge operations.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="treeHelper"></param>
        /// <param name="mergeCombine"></param>
        /// <param name="metadataOutput"></param>
        /// <param name="metadataMergeProcess"></param>
        /// <param name="preProcesses"></param>
        /// <param name="sceneXmlInputs"></param>
        /// <param name="sceneXmlDeps"></param>
        /// <param name="cacheDirectory"></param>
        private MapBuildData(IEngineParameters param, ContentTreeHelper treeHelper,
            IContentNode mergeCombine, IContentNode metadataOutput, 
            IProcess metadataMergeProcess, IEnumerable<IProcess> preProcesses,
            IEnumerable<Content.File> sceneXmlInputs, String cacheDirectory)
        {
            if (null == mergeCombine)
                throw (new ArgumentNullException("mergeCombine"));
            if (null == metadataOutput)
                throw (new ArgumentNullException("metadataOutput"));
            if (null == metadataMergeProcess)
                throw (new ArgumentNullException("metadataMergeProcess"));
            if (null == preProcesses)
                throw (new ArgumentNullException("preProcesses"));
            if (null == sceneXmlInputs)
                throw (new ArgumentNullException("sceneXmlInputs"));

            this.BuildType = MapBuildType.EnvironmentMerge;
            this.CacheDirectory = cacheDirectory;
            this.MergeCombineDataOutput = mergeCombine;
            this.MergeCombineCacheDirectory = SIO.Path.GetFullPath(
                param.Branch.Environment.Subst(SIO.Path.Combine(cacheDirectory, mergeCombine.Name)));
            this.MetadataOutput = metadataOutput;
            this.MetadataExportArchetypes = true;
            this.MetadataExportEntities = true;
            this.MetadataCacheDirectory = SIO.Path.GetFullPath(
                param.Branch.Environment.Subst(SIO.Path.Combine(cacheDirectory, metadataOutput.Name)));
            this.MetadataMergeProcess = metadataMergeProcess;
            this.PreProcesses = preProcesses.ToArray();
            this.SceneXmlInputs = sceneXmlInputs.ToArray();
            InitScenesAndDependencies(param, treeHelper);
        }

        /// <summary>
        /// Constructor; for non-merge operations.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="treeHelper"></param>
        /// <param name="buildType"></param>
        /// <param name="metadataOutput"></param>
        /// <param name="preProcesses"></param>
        /// <param name="sceneXmlInputs"></param>
        /// <param name="sceneXmlDeps"></param>
        /// <param name="cacheDirectory"></param>
        private MapBuildData(IEngineParameters param, ContentTreeHelper treeHelper,
            MapBuildType buildType, IContentNode metadataOutput,
            IEnumerable<IProcess> preProcesses, IEnumerable<Content.File> sceneXmlInputs,
            IEnumerable<Content.File> sceneXmlDeps, String cacheDirectory)
        {
            Debug.Assert(MapBuildType.EnvironmentNonMerge == buildType ||
                MapBuildType.EnvironmentArchetypesOnly == buildType);
            if (MapBuildType.EnvironmentNonMerge != buildType &&
                MapBuildType.EnvironmentArchetypesOnly != buildType)
                throw (new NotSupportedException());
            if (null == metadataOutput)
                throw (new ArgumentNullException("metadataOutput"));
            if (null == preProcesses)
                throw (new ArgumentNullException("preProcess"));
            if (null == sceneXmlInputs)
                throw (new ArgumentNullException("sceneXmlInputs"));
            if (null == sceneXmlDeps)
                throw (new ArgumentNullException("sceneXmlDeps"));

            this.BuildType = buildType;
            this.CacheDirectory = cacheDirectory;
            this.MergeCombineDataOutput = null;
            this.MergeCombineCacheDirectory = String.Empty;
            this.MetadataOutput = metadataOutput;
            this.MetadataCacheDirectory = SIO.Path.GetFullPath(
                param.Branch.Environment.Subst(SIO.Path.Combine(cacheDirectory, metadataOutput.Name)));
            this.MetadataMergeProcess = null;
            this.PreProcesses = preProcesses.ToArray();
            this.SceneXmlInputs = sceneXmlInputs.ToArray();
            this.SceneXmlDependencies = sceneXmlDeps.ToArray();
            InitScenesAndDependencies(param, treeHelper);
        }
        #endregion // Constructor(s)

        #region Static Controller Methods
        /// <summary>
        /// Factory method to create new MapBuildData objects.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processors"></param>
        /// <param name="processes"></param>
        /// <param name="cacheDirectoryRoot"></param>
        /// <returns></returns>
        public static IEnumerable<MapBuildData> Create(IEngineParameters param, IProcessorCollection processors, 
            IEnumerable<IProcess> processes, String cacheDirectoryRoot)
        {
            ISet<IProcess> preProcessesAnalysed = new HashSet<IProcess>();
            IList<MapBuildData> mapBuilds = new List<MapBuildData>();
            foreach (IProcess process in processes)
            {
                if (preProcessesAnalysed.Contains(process))
                    continue; // Skip it.

                // Create a new helper each time because owning tree may be different.
                ContentTreeHelper treeHelper = new ContentTreeHelper(process.Owner, processors);
                IContentNode combineOutput = treeHelper.GetCombineOutputForNodes(process.Inputs);
                // DHM TODO: the metadata merge content-tree markup isn't really required?
                IContentNode metadataOutput = treeHelper.GetMetadataMergeNodeFromSceneXmlNodes(process.Inputs);
                if (null != combineOutput && null != metadataOutput)
                {
                    // We have a combine output defined; so we add all processes that
                    // contribute to this.  This ensures from now on we're considering
                    // the entire map "district" (as defined by the merge process).
                    ISet<IContentNode> combineInputs = treeHelper.GetAllInputsForNode(combineOutput);
                    IEnumerable<IProcess> combineProcesses = combineInputs.SelectMany(i => 
                        process.Owner.FindProcessesWithInput(i)).Distinct();
                    IEnumerable<IProcess> mapPreProcesses = combineProcesses.Where(p => 
                        p.Processor.Name.Equals("RSG.Pipeline.Processor.Map2.PreProcess")).Distinct();

                    // Mark preprocesses as processed.
                    preProcessesAnalysed.AddRange(mapPreProcesses);

                    // Get SceneXml inputs.
                    IEnumerable<Content.File> xmlInputs = mapPreProcesses.SelectMany(p => p.Inputs.OfType<Content.File>().
                        Where(n => n.AbsolutePath.EndsWith(".xml"))).Distinct();

                    IEnumerable<IProcess> metadataProcesses = treeHelper.ContentTree.FindProcessesWithOutput(metadataOutput);
                    Debug.Assert(metadataProcesses.Count() == 1);

                    // Create preprocesssed object.
                    MapBuildData mapBuildData = new MapBuildData(param, treeHelper, combineOutput, metadataOutput, 
                        metadataProcesses.First(), mapPreProcesses, xmlInputs, cacheDirectoryRoot);
                    mapBuilds.Add(mapBuildData);
                }
                else
                {
                    // We do not have a combine output defined; this is a standalone "map" but
                    // may be merged with others.
                    // DHM TODO: need to analyse content-tree to determine what of the other
                    // types we are building (ensure detect the prop merge too).
                    throw (new NotImplementedException());
                }
            }

            return (mapBuilds);
        }
        #endregion // Static Controller Methods

        #region Private Methods
        /// <summary>
        /// Initialise Scene objects and determine dependencies.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="treeHelper"></param>
        private void InitScenesAndDependencies(IEngineParameters param, ContentTreeHelper treeHelper)
        {
            IContentNode[] source3dsmaxFileNodes = treeHelper.GetAllMapExportNodes();
            IList<Content.File> dependencies = new List<Content.File>();
            IDictionary<String, Scene> scenes = new Dictionary<String, Scene>();
            foreach (Content.File sceneXmlInput in this.SceneXmlInputs)
            {
                Scene scene = new Scene(treeHelper.ContentTree, treeHelper, sceneXmlInput.AbsolutePath, LoadOptions.Objects);
                scenes.Add(sceneXmlInput.AbsolutePath, scene);
                
                // DHM TODO : optimisation - just use raw XML data!!
                // Find external dependencies.
                foreach (TargetObjectDef obj in scene.Walk(Scene.WalkMode.BreadthFirst))
                {
                    if (!obj.IsRefObject())
                        continue; // Skip.

                    if (String.IsNullOrEmpty(obj.RefFile))
                    {
                        param.Log.WarningCtx(sceneXmlInput.Basename,
                            "External reference '{0}' does not contain filename.  Bug in the RsRef system.",
                            obj.Name);
                    }
                    else
                    {
                        // Find our reference max file in the content-tree.
                        String referenceFile = SIO.Path.GetFullPath(obj.RefFile);
                        dependencies.Add((Content.File)treeHelper.ContentTree.CreateFile(referenceFile));
                    }
                }
            }
            this.Scenes = scenes;
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Processor.Map2 namespace
