﻿//---------------------------------------------------------------------------------------------
// <copyright file="PreProcess.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2012-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Processor.Map2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.Composition;
    using System.Diagnostics;
    using SIO = System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml.Linq;
    using RSG.Base.Configuration;
    using RSG.Base.Extensions;
    using RSG.Base.Logging;
    using XGE = RSG.Interop.Incredibuild.XGE;
    using RSG.Pipeline.Core;
    using RSG.Pipeline.Content;
    using RSG.Pipeline.Processor.Map2.Data;
    using RSG.Pipeline.Services;
    using RSG.Pipeline.Services.BoundsProcessor;
    using RSG.SceneXml;

    /// <summary>
    /// Map Bounds processor (MKII); this processor handles the bounds data from the
    /// Map PreProcess (MKII).  We support the new MapBuildData object rather than
    /// passing a gazillion parameters around.
    /// </summary>
    /// We expect an IProcess with one or more input _collision.zip files and an output 
    /// directory.
    /// 
    [Export(typeof(IProcessor))]
    class BoundsProcessor :
        MapProcessorBase
    {
        #region Constants
        private static readonly String DESCRIPTION = "Map Bounds Processor";
        private static readonly String LOG_CTX = "Map Bounds Processor";
        #endregion // Constants
        
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public BoundsProcessor()
            : base(DESCRIPTION)
        {
        }
        #endregion // Constructor(s)

        #region IProcessor Interface Methods
        /// <summary>
        /// Map Bounds Processor Prebuild stage.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processes"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="syncDependencies"></param>
        /// <param name="resultantProcesses"></param>
        /// <returns></returns>
        public override bool Prebuild(IEngineParameters param, IEnumerable<IProcess> processes,
            IProcessorCollection processors, IContentTree owner,
            out IDictionary<IProcess, IEnumerable<IContentNode>> syncDependencies,
            out IEnumerable<IProcess> resultantProcesses)
        {
            using (ProfileContext ctx = new ProfileContext(param.Log, "Map Bounds Processor Prebuild."))
            {
                return (PrebuildImpl(param, processes, processors, owner, out syncDependencies, out resultantProcesses));
            }
        }

        /// <summary>
        /// Prepare; this turns the IProcess objects into XGE tools and tasks.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processes"></param>
        /// <param name="tools"></param>
        /// <param name="tasks"></param>
        /// <returns></returns>
        public override bool Prepare(IEngineParameters param, IEnumerable<IProcess> processes,
            out IEnumerable<XGE.ITool> tools, out IEnumerable<XGE.ITask> tasks)
        {
            bool allow_remote = GetXGEAllowRemote(param, true);
            XGE.ITool serialiserTool = XGEFactory.GetMapProcessorTool(param.Log,
                param.Branch.Project.Config, XGEFactory.MapToolType.BoundsProcessor,
                "Bounds Processor", allow_remote);
            XGE.ITaskJobGroup taskGroup = new XGE.TaskGroup("Bounds Processor");

            int taskCounter = 0;
            foreach (IProcess process in processes)
            {
                MapBuildData mapBuildData = (MapBuildData)process.GetParameter(Parameter.MapProcess_AssociatedMapBuildData, null);
                if (null == mapBuildData)
                {
                    param.Log.ErrorCtx(LOG_CTX, "Fatal error preparing bounds processor.  Missing Associated Map Build Data.");
                    continue; // Skip.
                }
                MapContainerMergeData mapMergeContainerData = (MapContainerMergeData)process.GetParameter(Parameter.MapProcess_AssociatedMapMergeContainerData, null);
                if (null == mapMergeContainerData)
                {
                    param.Log.ErrorCtx(LOG_CTX, "Fatal error preparing bounds processor.  Missing Associated Map Merge Container Data.");
                    continue; // Skip.
                }

                // Build the config for the bounds processor
                String configFilename = CreateBoundsProcessorConfig(process, mapBuildData, mapMergeContainerData);

                Content.Directory output = (Content.Directory)process.Outputs.First();
                String processedMapName = SIO.Directory.GetParent(output.AbsolutePath).Name;
                
                //string sceneName = Path.GetFileNameWithoutExtension(sourceFile);
                IEnumerable<String> inputPathnames = process.Inputs.OfType<Content.File>().Select(input => input.AbsolutePath);
                IEnumerable<String> inputDrawables = process.Inputs.OfType<Content.Asset>().Select(inputDrawable => inputDrawable.AbsolutePath);
                IEnumerable<String> outputDrawables = process.Outputs.OfType<Content.Asset>().Select(outputDrawable => outputDrawable.AbsolutePath);

                String taskName = String.Format("Bounds Processor {0} [{1}]", taskCounter++, processedMapName);
                XGE.ITaskJob task = new XGE.TaskJob(taskName);
                task.Tool = serialiserTool;
                task.Caption = taskName;
                task.InputFiles.AddRange(inputPathnames);
                task.InputFiles.AddRange(inputDrawables);
                task.OutputFiles.AddRange(outputDrawables);
                task.SourceFile = inputPathnames.First();

                if (param.Branch.Project.IsDLC)
                    task.Parameters = String.Format("--config {0} --branch {1} --dlc {2} --logdir {3}", configFilename, 
                        param.Branch.Name, param.Branch.Project.Name, LogFactory.ParentProcessLogDirectory);
                else
                    task.Parameters = String.Format("--config {0} --branch {1} --logdir {2}", configFilename, 
                        param.Branch.Name, LogFactory.ParentProcessLogDirectory);

                process.SetParameter(Constants.ProcessXGE_Task, task);
                process.SetParameter(Constants.ProcessXGE_TaskName, taskName.Replace(" ", "_"));
                taskGroup.Tasks.Add(task);
            }

            // Assign method output.
            tools = new List<XGE.ITool>(new XGE.ITool[] { serialiserTool });
            tasks = new List<XGE.ITask>(new XGE.ITask[] { taskGroup });

            return (true);
        }
        #endregion // IProcessor Interface Methods
   
        #region Private Methods
        /// <summary>
        /// Validate all processes for map processing; pre-pass in Prebuild.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processes"></param>
        /// <returns></returns>
        private bool ValidateProcesses(IEngineParameters param, IEnumerable<IProcess> processes)
        {
            using (new ProfileContext(param.Log, "Map2::BoundsProcessor::ValidateProcesses"))
            {
                bool result = true;
                foreach (IProcess process in processes)
                {
                    if (!process.Inputs.Any())
                    {
                        param.Log.ErrorCtx(LOG_CTX, "Map Bounds Processor requires one or more input nodes.");
                        result = false;
                    }

                    if (process.Outputs.Count() != 1)
                    {
                        param.Log.ErrorCtx(LOG_CTX, "Map Bounds Processor requires one output node.");
                        return false;
                    }

                    IEnumerable<IContentNode> inputs = process.Inputs.OfType<Content.Asset>().Where(a => 
                        a.AssetType == RSG.Platform.FileType.ZipArchive);
                    if (!inputs.Any())
                    {
                        param.Log.ErrorCtx(LOG_CTX, "Map Bounds Processor inputs must have an AssetType of ZipArchive.");
                        return false;
                    }
                    
                    IContentNode output = process.Outputs.First();
                    if (!(output is Directory))
                    {
                        param.Log.ErrorCtx(LOG_CTX, "Map Bounds Processor output must be of type Directory.");
                        return false;
                    }

                    // Processor Parameter validation.
                    // None required yet.

                    // Process Parameter validation.
                    result &= ValidateHasParameter(param, process, Parameter.MapProcess_AssociatedMapBuildData);
                    result &= ValidateHasParameter(param, process, Parameter.MapProcess_AssociatedMapMergeContainerData);
                }

                return result;
            }
        }

        /// <summary>
        /// Map PreProcess Prebuild stage; actual implementation for surrounding
        /// profile call.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processes"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="syncDependencies"></param>
        /// <param name="resultantProcesses"></param>
        /// <returns></returns>
        /// For all of the input processes we:
        ///  1. Group them by combine district.
        ///  2. Extract
        ///  3. Create a Map Metadata Process
        ///  4. Create a Map Collision Process
        ///  5. Where required, create an Asset Pack process for merged content (e.g. dt1_01 => downtown)
        ///  6. An Asset Pack process for the processed content
        ///  7. A RAGE process to convert data.
        ///  
        private bool PrebuildImpl(IEngineParameters param, IEnumerable<IProcess> processes,
            IProcessorCollection processors, IContentTree owner,
            out IDictionary<IProcess, IEnumerable<IContentNode>> syncDependencies,
            out IEnumerable<IProcess> resultantProcesses)
        {
            // Set trivial parameters to allow for early return.
            resultantProcesses = new IProcess[0];
            syncDependencies = new Dictionary<IProcess, IEnumerable<IContentNode>>();

            // Validate processes.
            this.LoadParameters(param);
            if (!ValidateProcesses(param, processes))
                return (false);

            List<IProcess> prebuiltProcesses = new List<IProcess>();
            foreach (IProcess process in processes)
            {
                process.State = ProcessState.Prebuilt;
                prebuiltProcesses.Add(process);
            }
            resultantProcesses = prebuiltProcesses;
            return (true);
        }

        /// <summary>
        /// Write the bounds processor configuration file to disk; return its absolute filename.
        /// </summary>
        /// <param name="process"></param>
        /// <param name="mapBuildData"></param>
        /// <param name="containerData"></param>
        /// <returns></returns>
        private String CreateBoundsProcessorConfig(IProcess process, MapBuildData mapBuildData, MapContainerMergeData containerData)
        {
            // DHM FIX ME: additional IMAPs - what are they used for?  (See old pipeline).
            // DHM FIX ME: handle drawables with packed bounds (use Asset Processor)


            Content.Directory outputDirectory = (Content.Directory)process.Outputs.First();           
            List<BoundsProcessorTask> tasks = new List<BoundsProcessorTask>();
            List<BoundsProcessorInput> inputs = new List<BoundsProcessorInput>();
            foreach (Content.File input in containerData.PreProcess.Inputs.OfType<Content.File>())
            {
                if (".xml" != input.Extension)
                    continue;

                String sceneType = "unknown";
                MapContainerData map = mapBuildData.MapsContainerData.FirstOrDefault(m =>
                    m.SceneXmlFilename.Equals(input.AbsolutePath, StringComparison.OrdinalIgnoreCase));
                if (null != map)
                    sceneType = map.SceneType;

                Content.IFilesystemNode collisionInput = (Content.IFilesystemNode)process.Inputs.First();
                inputs.Add(new BoundsProcessorInput(input.AbsolutePath, sceneType,
                    collisionInput.AbsolutePath, "", "", new String[0]));
            }

            BoundsProcessorOutput output = new BoundsProcessorOutput(containerData.MergeName, outputDirectory.AbsolutePath, 
                containerData.BoundsManifestAdditionsFilename);
            BoundsProcessorOptions defaultOptions = new BoundsProcessorOptions();
            BoundsProcessorTask task = new BoundsProcessorTask(inputs, output);
            tasks.Add(task);

            String configFilename = SIO.Path.Combine(containerData.CacheDirectory, "bounds_processor_config.xml");
            SIO.Directory.CreateDirectory(SIO.Path.GetDirectoryName(configFilename));
            BoundsProcessorConfig config = new BoundsProcessorConfig(defaultOptions, tasks);
            config.Save(configFilename);

            return (configFilename);
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Processor.Map2 namespace
