﻿//---------------------------------------------------------------------------------------------
// <copyright file="Constants.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Processor.Map2
{
    using System;
    using RSG.Pipeline.Core.Attributes;
    using RSG.Pipeline.Processor.Map2.Data;

    /// <summary>
    /// Map Pipeline Parameter Constants.
    /// </summary>
    /// Note: please carefully consider adding these.  We have very carefully abstracted out
    /// the map build data into a series of classes which is where most of this parametric
    /// data should go.
    /// 
    internal static class Parameter
    {
        /// <summary>
        /// Map cache directory for processed map construction.
        /// </summary>
        [ParameterType(typeof(String))]
        internal const String MapProcess_MapCacheDirectory =
            "Map Cache Directory";

        /// <summary>
        /// Process associated MapBuildData object; this replaces many of the old
        /// parameters we used to pass around as the MapBuildData object abstracts
        /// a lot of it.
        /// </summary>
        [ParameterType(typeof(MapBuildData))]
        internal const String MapProcess_AssociatedMapBuildData =
            "Associated Map Build Data";

        /// <summary>
        /// Process associated MapContainerMergeData object; this replaces many of the
        /// old parameters we used to pass around as the MapContainerMergeData object
        /// abstracts a lot of it.
        /// </summary>
        [ParameterType(typeof(MapContainerMergeData))]
        internal const String MapProcess_AssociatedMapMergeContainerData =
            "Associated Map Merge Container Data";

        /// <summary>
        /// Process associated MapContainerData object; this replaces many of the
        /// old parameters we used to pass around as the MapContainerMergeData object
        /// abstracts a lot of it.
        /// </summary>
        [ParameterType(typeof(MapContainerData))]
        internal const String MapProcess_AssociatedMapContainerData =
            "Associated Map Container Data";

        /// <summary>
        /// Map cache directory for processed map construction.
        /// </summary>
        [ParameterType(typeof(String))]
        internal const String MapProcess_ForceProcessedMapZipCreation =
            "Force Processed Map Zip Creation";

        // DHM BELOW THIS TO REMOVE
        
        /// <summary>
        /// Map Metadata; audio collision CSV data filename.
        /// </summary>
        [ParameterType(typeof(String))]
        internal const String MapProcess_MetadataAudioCollisionFilename =
            "Metadata Audio Collision Filename";

        /// <summary>
        /// Map Metadata; scene overrides directory.
        /// </summary>
        [ParameterType(typeof(String))]
        internal const String MapProcess_MetadataSceneOverridesDirectory =
            "Metadata Scene Overrides Directory";

        /// <summary>
        /// Semi-colon delimited string of which time cycle types to exclude from serialisation.
        /// </summary>
        [ParameterType(typeof(String))]
        internal const String MapProcess_MetadataNonSerialisedTimeCycleTypes =
            "Non Serialised TimeCycle Types";
    }

} // RSG.Pipeline.Processor.Map2 namespace
