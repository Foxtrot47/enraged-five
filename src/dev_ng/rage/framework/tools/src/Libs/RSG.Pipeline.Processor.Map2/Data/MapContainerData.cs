﻿//---------------------------------------------------------------------------------------------
// <copyright file="MapContainerData.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Processor.Map2.Data
{
    using System;
    using System.Collections.Generic;
    using SIO = System.IO;
    using System.Linq;
    using RSG.Base.Configuration;
    using RSG.Base.Extensions;
    using RSG.Pipeline.Core;
    using RSG.Pipeline.Services;
    using RSG.SceneXml;
    using RSG.Pipeline.Content;
    
    /// <summary>
    /// Class representing a single map container export; where its data lives on disk during
    /// a build and any related cache directories etc.
    /// </summary>
    public class MapContainerData
    {
        #region Properties
        /// <summary>
        /// Map container zip extraction directory.
        /// </summary>
        public String ExtractDirectory
        {
            get;
            private set;
        }

        /// <summary>
        /// Exported SceneXml filename.
        /// </summary>
        public String SceneXmlFilename
        {
            get;
            private set;
        }

        /// <summary>
        /// Exported zip filename.
        /// </summary>
        public String ZipFilename
        {
            get;
            private set;
        }

        /// <summary>
        /// Exported zip file content filenames (absolute paths).
        /// </summary>
        public String[] ExtractedFilenames
        {
            get;
            private set;
        }

        /// <summary>
        /// Map container Scene type (from content-tree).
        /// </summary>
        /// DHM TODO FIX ME: should be based on enum SceneType rather than string.
        /// 
        public String SceneType
        {
            get;
            private set;
        }

        /// <summary>
        /// Map container Scene object.
        /// </summary>
        public Scene Scene
        {
            get;
            private set;
        }

        /// <summary>
        /// Map container Scene dependencies.
        /// </summary>
        public Content.File[] SceneDependencies
        {
            get;
            private set;
        }

        /// <summary>
        /// Dynamic object names within this map container.
        /// </summary>
        public String[] DynamicObjectNames
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="exportProcess"></param>
        /// <param name="cacheDirectoryRoot"></param>
        public MapContainerData(IEngineParameters param, IProcess exportProcess,
            String cacheDirectoryRoot, ContentTreeHelper treeHelper)
        {
            IBranch branch = param.Branch;
            String outputBasename = exportProcess.Outputs.First().Name;

            this.ExtractDirectory = branch.Environment.Subst(SIO.Path.Combine(cacheDirectoryRoot, outputBasename));
            this.ExtractDirectory = SIO.Path.GetFullPath(this.ExtractDirectory);

            IEnumerable<Content.File> outputs = exportProcess.Outputs.OfType<Content.File>();
            this.SceneXmlFilename = outputs.First(o => o.Extension.Equals(".xml")).AbsolutePath;
            this.ZipFilename = outputs.First(o => o.Extension.Equals(".zip")).AbsolutePath;
            InitExtractFilenames(param);
            InitScene(param, treeHelper);
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// Initialise our list of filenames within the zip.
        /// </summary>
        private void InitExtractFilenames(IEngineParameters param)
        {
            IList<String> extractedFiles = new List<String>();
            String assetBasename = SIO.Path.GetFileNameWithoutExtension(this.ZipFilename);
            if (SIO.File.Exists(this.ZipFilename))
            {
                // Read map zip file.                    
                IEnumerable<String> files;
                bool readOk = Zip.GetFileList(this.ZipFilename, out files);
                if (readOk && files.Any())
                {
                    IEnumerable<String> outputFiles = files.Select(f => SIO.Path.Combine(this.ExtractDirectory, f));
                    extractedFiles.AddRange(outputFiles);
                }
                else if (!readOk)
                {
                    param.Log.ErrorCtx(assetBasename, "Reading '{0}' failed.  Map processing will likely fail.",
                        this.ZipFilename);
                }
            }
            else
            {
                param.Log.ErrorCtx(assetBasename, "Map export data file does not exist: '{0}'.", this.ZipFilename);
            }
            this.ExtractedFilenames = extractedFiles.ToArray();
        }

        /// <summary>
        /// Initialise our Scene and related data.
        /// </summary>
        /// <param name="param"></param>
        private void InitScene(IEngineParameters param, ContentTreeHelper treeHelper)
        {
            IList<String> dynamicObjects = new List<String>();
            IList<Content.File> dependencies = new List<Content.File>();
            IContentNode sceneNode = treeHelper.CreateFile(this.SceneXmlFilename);
            this.SceneType = treeHelper.GetSceneTypeForExportNode(sceneNode);
            this.Scene = new Scene(treeHelper.ContentTree, treeHelper, this.SceneXmlFilename, LoadOptions.Objects | LoadOptions.Materials);
            
            // DHM TODO : optimisation - use raw xml? Well try it.
            foreach (TargetObjectDef obj in this.Scene.Walk(Scene.WalkMode.BreadthFirst))
            {
                // Handle Externally Referenced Objects
                if (obj.IsRefObject())
                {
                    if (String.IsNullOrEmpty(obj.RefFile))
                    {
                        param.Log.WarningCtx(sceneNode.Name,
                            "External reference '{0}' does not contain filename.  Bug in the RsRef system.",
                            obj.Name);
                    }
                    else
                    {
                        // Find our reference max file in the content-tree.
                        String referenceFile = SIO.Path.GetFullPath(obj.RefFile);
                        dependencies.Add((Content.File)treeHelper.ContentTree.CreateFile(referenceFile));
                    }
                }
                // Handle Dynamic Objects (for Bounds Packing).
                else if (obj.IsObject() && !obj.IsFragment() && (obj.IsDynObject() || obj.IsPartOfAnEntitySet()))
                {
                    dynamicObjects.Add(obj.Name);
                }
            }
            this.SceneDependencies = dependencies.ToArray();
            this.DynamicObjectNames = dynamicObjects.ToArray();
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Processor.Map2.Data namespace
