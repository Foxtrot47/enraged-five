﻿//---------------------------------------------------------------------------------------------
// <copyright file="MapBuildData.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Processor.Map2.Data
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using SIO = System.IO;
    using System.Linq;
    using RSG.Base.Configuration;
    using RSG.Base.Extensions;
    using RSG.Base.Logging.Universal;
    using RSG.Pipeline.Core;
    using RSG.Pipeline.Content;
    using RSG.Pipeline.Content.Algorithm;
    using RSG.SceneXml;
    
    /// <summary>
    /// Class representing a single map asset build; all of the various types of "map"
    /// we support.
    /// </summary>
    /// <seealso cref="MapContainerData"/>
    /// <seealso cref="MapContainerMergeData"/>
    ///
    /// Note: this abstraction should allow us to change the map content-tree but keep
    /// the map PreProcess and pipeline the same.
    /// 
    /// Note: do not use IContentNode here.  They belong to a tree - store filenames
    /// and directories instead.  If unsure speak to me.
    /// 
    internal class MapBuildData
    {
        #region Properties
        /// <summary>
        /// Map build type; type of build this object represents.
        /// </summary>
        public MapBuildType BuildType
        {
            get;
            private set;
        }
        
        /// <summary>
        /// Map content-nodes owner.
        /// </summary>
        public IContentTree Owner
        {
            get;
            private set;
        }

        /// <summary>
        /// Map cache root directory.
        /// </summary>
        public String CacheDirectory
        {
            get;
            private set;
        }

        /// <summary>
        /// Map cache root directory (for core project).
        /// </summary>
        public String CoreCacheDirectory
        {
            get;
            private set;
        }

        /// <summary>
        /// Map merge combine output (can be null depending on BuildType).
        /// </summary>
        public IContentNode MergeCombineDataOutput
        {
            get;
            private set;
        }

        /// <summary>
        /// Map merge combine cache directory.
        /// </summary>
        public String MergeCombineCacheDirectory
        {
            get;
            private set;
        }

        /// <summary>
        /// Map merge combine Asset Combine output filename.
        /// </summary>
        public String MergeCombineDocumentFilename
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public String MetadataName
        {
            get;
            private set;
        }

        /// <summary>
        /// Map metadata output zip filename.
        /// </summary>
        public String ProcessedMetadataZipFilename
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public bool MetadataExportArchetypes
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public bool MetadataExportEntities
        {
            get;
            private set;
        }

        /// <summary>
        /// Map metadata cache directory.
        /// </summary>
        public String MetadataCacheDirectory
        {
            get;
            private set;
        }

        /// <summary>
        /// Map metadata manifest additions XML output filename.
        /// </summary>
        public String MetadataManifestAdditionsFilename
        {
            get;
            private set;
        }

        /// <summary>
        /// Map metadata process (can be null depending on BuildType).
        /// </summary>
        public IProcess MetadataMergeProcess
        {
            get;
            private set;
        }

        /// <summary>
        /// Regular map container data; including pre-processes and other per-container data.
        /// </summary>
        /// This gives access to the raw container data; excludes any merge data as that is
        /// only applicable for certain scenarios.
        /// 
        public MapContainerData[] MapsContainerData
        {
            get;
            private set;
        }

        /// <summary>
        /// Map merge container data; including pre-processes and other per-container data.
        /// </summary>
        /// This gives access to the merge container data; for raw container data see the 
        /// MapsContainerData array.
        /// 
        public MapContainerMergeData[] MapsContainerMergeData
        {
            get;
            private set;
        }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// Constructor; for Environment and Archetype Merge operations.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="treeHelper"></param>
        /// <param name="mergeCombine"></param>
        /// <param name="metadataOutput"></param>
        /// <param name="metadataMergeProcess"></param>
        /// <param name="preProcesses"></param>
        /// <param name="sceneXmlInputs"></param>
        /// <param name="sceneXmlDeps"></param>
        /// <param name="cacheDirectory"></param>
        private MapBuildData(IEngineParameters param, ContentTreeHelper treeHelper,
            IContentNode mergeCombine, IContentNode metadataOutput, 
            IProcess metadataMergeProcess, IEnumerable<IProcess> preProcesses,
            String cacheDirectory)
        {
            if (null == mergeCombine)
                throw (new ArgumentNullException("mergeCombine"));
            if (null == metadataOutput)
                throw (new ArgumentNullException("metadataOutput"));
            if (null == metadataMergeProcess)
                throw (new ArgumentNullException("metadataMergeProcess"));
            if (null == preProcesses)
                throw (new ArgumentNullException("preProcesses"));
            
            IBranch branch = param.Branch;
            this.BuildType = MapBuildType.EnvironmentMerge;
            this.Owner = metadataOutput.Owner;
            this.CacheDirectory = SIO.Path.GetFullPath(branch.Environment.Subst(cacheDirectory));
            this.CoreCacheDirectory = SIO.Path.GetFullPath(branch.Environment.Subst(cacheDirectory)); // DHM FIX ME
            this.MergeCombineDataOutput = mergeCombine;
            this.MergeCombineCacheDirectory = SIO.Path.GetFullPath(
                param.Branch.Environment.Subst(SIO.Path.Combine(cacheDirectory, mergeCombine.Name)));
            this.MergeCombineDocumentFilename = SIO.Path.GetFullPath(
                SIO.Path.Combine(this.MergeCombineCacheDirectory, String.Format("asset_combine_{0}.meta", this.MergeCombineDataOutput.Name)));
            this.MetadataName = metadataOutput.Name;
            this.ProcessedMetadataZipFilename = ((IFilesystemNode)metadataOutput).AbsolutePath;
            this.MetadataExportArchetypes = true;
            this.MetadataExportEntities = true;

            // Flo: Passed down with the correct value?
            String metadataCacheDirectoryRoot = SIO.Path.GetFullPath(param.Branch.Environment.Subst(SIO.Path.Combine(cacheDirectory, metadataOutput.Name)));
            this.MetadataCacheDirectory = SIO.Path.Combine(metadataCacheDirectoryRoot, "map_metadata");
            this.MetadataManifestAdditionsFilename = SIO.Path.Combine(metadataCacheDirectoryRoot, "MapExportAdditions.xml");
            this.MetadataMergeProcess = metadataMergeProcess;
            InitMapContainerData(param, preProcesses, cacheDirectory, treeHelper);
        }

        /// <summary>
        /// Constructor; for non-merge operations.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="treeHelper"></param>
        /// <param name="buildType"></param>
        /// <param name="metadataOutput"></param>
        /// <param name="preProcesses"></param>
        /// <param name="sceneXmlInputs"></param>
        /// <param name="sceneXmlDeps"></param>
        /// <param name="cacheDirectory"></param>
        private MapBuildData(IEngineParameters param, ContentTreeHelper treeHelper,
            MapBuildType buildType, IContentNode metadataOutput,
            IEnumerable<IProcess> preProcesses, String cacheDirectory)
        {
            Debug.Assert(MapBuildType.EnvironmentNonMerge == buildType ||
                MapBuildType.EnvironmentArchetypesOnly == buildType);
            if (MapBuildType.EnvironmentNonMerge != buildType &&
                MapBuildType.EnvironmentArchetypesOnly != buildType)
                throw (new NotSupportedException());
            if (null == metadataOutput)
                throw (new ArgumentNullException("metadataOutput"));
            if (null == preProcesses)
                throw (new ArgumentNullException("preProcess"));
            
            IBranch branch = param.Branch;
            this.BuildType = buildType;
            this.Owner = metadataOutput.Owner;
            this.CacheDirectory = SIO.Path.GetFullPath(branch.Environment.Subst(cacheDirectory)); ;
            this.MergeCombineDataOutput = null;
            this.MergeCombineCacheDirectory = String.Empty;
            this.MetadataCacheDirectory = SIO.Path.GetFullPath(
                param.Branch.Environment.Subst(SIO.Path.Combine(cacheDirectory, metadataOutput.Name)));
            this.MetadataManifestAdditionsFilename = SIO.Path.Combine(
                this.MetadataCacheDirectory, "MapExportAdditions.xml");
            this.MetadataMergeProcess = null;
            this.MetadataName = metadataOutput.Name;
            this.ProcessedMetadataZipFilename = ((IFilesystemNode)metadataOutput).AbsolutePath;
            InitMapContainerData(param, preProcesses, cacheDirectory, treeHelper);
        }
        #endregion // Constructor(s)

        #region Static Controller Methods
        /// <summary>
        /// Factory method to create new MapBuildData objects.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processors"></param>
        /// <param name="processes"></param>
        /// <param name="cacheDirectoryRoot"></param>
        /// <returns></returns>
        public static IEnumerable<MapBuildData> Create(IEngineParameters param, IProcessorCollection processors, 
            IEnumerable<IProcess> processes, String cacheDirectoryRoot)
        {
            ISet<IProcess> preProcessesAnalysed = new HashSet<IProcess>();
            IList<MapBuildData> mapBuilds = new List<MapBuildData>();
            foreach (IProcess process in processes)
            {
                if (preProcessesAnalysed.Contains(process))
                    continue; // Skip it.

                // Create a new helper each time because owning tree may be different.
                ContentTreeHelper treeHelper = new ContentTreeHelper(process.Owner);
                IContentNode combineOutput = treeHelper.GetCombineOutputForNodes(process.Inputs);
                // DHM TODO: the metadata merge content-tree markup isn't really required?
                IContentNode metadataOutput = treeHelper.GetMetadataMergeNodeFromSceneXmlNodes(process.Inputs);
                if (null != combineOutput && null != metadataOutput)
                {
                    // We have a combine output defined; so we add all processes that
                    // contribute to this.  This ensures from now on we're considering
                    // the entire map "district" (as defined by the merge process).
                    ISet<IContentNode> combineInputs = treeHelper.GetAllInputsForNode(combineOutput);
                    IEnumerable<IProcess> combineProcesses = combineInputs.SelectMany(i => 
                        process.Owner.FindProcessesWithInput(i)).Distinct();
                    IEnumerable<IProcess> mapPreProcesses = combineProcesses.Where(p => 
                        p.ProcessorClassName.Equals("RSG.Pipeline.Processor.Map2.PreProcess")).Distinct();

                    // Mark preprocesses as processed.
                    preProcessesAnalysed.AddRange(mapPreProcesses);
                    
                    IEnumerable<IProcess> metadataProcesses = treeHelper.ContentTree.FindProcessesWithOutput(metadataOutput);
                    Debug.Assert(metadataProcesses.Count() == 1);

                    // Create preprocesssed object.
                    MapBuildData mapBuildData = new MapBuildData(param, treeHelper, combineOutput, metadataOutput, 
                        metadataProcesses.First(), mapPreProcesses, cacheDirectoryRoot);
                    mapBuilds.Add(mapBuildData);
                }
                else
                {
                    // We do not have a combine output defined; this is a standalone "map" but
                    // may be merged with others.
                    // DHM TODO: need to analyse content-tree to determine what of the other
                    // types we are building (ensure detect the prop merge too).
                    throw (new NotImplementedException());
                }
            }

            return (mapBuilds);
        }
        #endregion // Static Controller Methods

        #region Private Methods
        /// <summary>
        /// Initialise MapContainerMergeData array.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="preProcesses"></param>
        /// <param name="cacheDirectoryRoot"></param>
        /// <param name="treeHelper"></param>
        private void InitMapContainerData(IEngineParameters param, IEnumerable<IProcess> preProcesses, 
            String cacheDirectoryRoot, ContentTreeHelper treeHelper)
        {
            List<MapContainerMergeData> containerData = new List<MapContainerMergeData>();
            foreach (IProcess preProcess in preProcesses)
            {
                MapContainerMergeData mapData = new MapContainerMergeData(param, preProcess, cacheDirectoryRoot, treeHelper);
                containerData.Add(mapData);            
            }
            this.MapsContainerMergeData = containerData.ToArray();
            this.MapsContainerData = containerData.SelectMany(c => c.MapContainers).ToArray();
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Processor.Map2.Data namespace
