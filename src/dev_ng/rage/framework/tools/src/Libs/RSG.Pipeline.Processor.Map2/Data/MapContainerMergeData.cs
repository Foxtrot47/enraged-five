﻿//---------------------------------------------------------------------------------------------
// <copyright file="MapContainerMergeData.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Processor.Map2.Data
{
    using System;
    using System.Collections.Generic;
    using SIO = System.IO;
    using System.Linq;
    using RSG.Base.Configuration;
    using RSG.Base.Extensions;
    using RSG.Pipeline.Core;
    using RSG.Pipeline.Content;
    using RSG.Pipeline.Content.Algorithm;

    /// <summary>
    /// Class representing a map container merge build of more than one map container.  
    /// Instances of these are grouped together in a MapBuildData object.  Constructed by 
    /// the PreProcess and utilised in the Bounds Processor and others that operate per-container.
    /// </summary>
    /// This represents a basic map merge; typically of a container and a _props container;
    /// i.e. a single Map2 PreProcess.
    /// 
    /// Note: this abstract should allow us to change the map content-tree but keep
    /// the map PreProcess and pipeline the same.
    /// 
    internal class MapContainerMergeData
    {
        #region Properties
        /// <summary>
        /// Merged Name.
        /// </summary>
        public String MergeName
        {
            get;
            private set;
        }

        /// <summary>
        /// Represented Map PreProcess.
        /// </summary>
        public IProcess PreProcess
        {
            get;
            private set;
        }

        /// <summary>
        /// Map PreProcess cache directory.
        /// </summary>
        public String CacheDirectory
        {
            get;
            private set;
        }

        /// <summary>
        /// Bounds Processor cache directory.
        /// </summary>
        public String BoundsCacheDirectory
        {
            get;
            private set;
        }

        /// <summary>
        /// Processed bounds zip filename.
        /// </summary>
        public String ProcessedBoundsZipFilename
        {
            get;
            private set;
        }

        /// <summary>
        /// Bounds Processor manifest additions XML output filename.
        /// </summary>
        public String BoundsManifestAdditionsFilename
        {
            get;
            private set;
        }

        /// <summary>
        /// Map container exports.
        /// </summary>
        public MapContainerData[] MapContainers
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="preProcess"></param>
        /// <param name="cacheDirectoryRoot"></param>
        /// <param name="treeHelper"></param>
        public MapContainerMergeData(IEngineParameters param, IProcess preProcess, 
            String cacheDirectoryRoot, ContentTreeHelper treeHelper)
        {
            if (null == preProcess)
                throw (new ArgumentNullException("preProcess"));
            if (String.IsNullOrEmpty(cacheDirectoryRoot))
                throw (new ArgumentException("cacheDirectoryRoot"));

            IBranch branch = param.Branch;
            String outputBasename = preProcess.Outputs.First().Name;
            this.MergeName = outputBasename;
            this.PreProcess = preProcess;

            // Group based on content-tree export processes.
            List<MapContainerData> mapContainerData = new List<MapContainerData>();
            ISet<IProcess> exportProcesses = new HashSet<IProcess>();
            foreach (IContentNode node in preProcess.Inputs)
                exportProcesses.AddRange(preProcess.Owner.FindProcessesWithOutput(node));
            
            // Each export-process becomes a MapContainerData object.
            foreach (IProcess exportProcess in exportProcesses)
            {
                MapContainerData mapContainer = new MapContainerData(param, exportProcess, cacheDirectoryRoot, treeHelper);
                mapContainerData.Add(mapContainer);
            }
            this.MapContainers = mapContainerData.ToArray();

            this.CacheDirectory = branch.Environment.Subst(SIO.Path.Combine(cacheDirectoryRoot, outputBasename));
            this.CacheDirectory = SIO.Path.GetFullPath(this.CacheDirectory);

            this.BoundsCacheDirectory = branch.Environment.Subst(SIO.Path.Combine(this.CacheDirectory, "processed_bounds"));
            this.BoundsCacheDirectory = SIO.Path.GetFullPath(this.BoundsCacheDirectory);

            this.BoundsManifestAdditionsFilename = branch.Environment.Subst(SIO.Path.Combine(cacheDirectoryRoot, "boundsAdditions.xml"));
            this.BoundsManifestAdditionsFilename = SIO.Path.GetFullPath(this.BoundsManifestAdditionsFilename);

            MapContainerData firstMapContainer = this.MapContainers.First();
            this.ProcessedBoundsZipFilename = SIO.Path.Combine(SIO.Path.GetDirectoryName(firstMapContainer.ZipFilename),
                String.Format("{0}_collision.zip", this.MergeName));
            this.ProcessedBoundsZipFilename = this.ProcessedBoundsZipFilename.Replace(param.Branch.Export, 
                param.Branch.Processed, StringComparison.OrdinalIgnoreCase);
        }
        #endregion // Constructor(s)
    }

} // RSG.Pipeline.Processor.Map2.Data namespace
