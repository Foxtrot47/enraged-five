#
# RSG.Pipeline.Processor.Map2.makefile
#
 
Project RSG.Pipeline.Processor.Map2
 
FrameworkVersion 4.0
 
OutputPath $(toolsroot)\ironlib\lib\Processors\
 
Files {
	Directory Properties {
		AssemblyInfo.cs
	}
	Directory AssetCombine {
		CustomRBSManager.cs
	}
	Directory Data {
		MapBuildData.cs
		MapBuildType.cs
		MapContainerData.cs
		MapContainerMergeData.cs
	}
	BoundsProcessor.cs
	MapProcessorBase.cs
	MetadataProcessor.cs
	OcclusionProcessor.cs
	Parameter.cs
	PreProcess.cs
	RSG.Pipeline.Processor.Map2.makefile
	Wildcards.cs
}
 
ProjectReferences {
	..\..\..\..\..\base\tools\libs\RSG.Base.Configuration\RSG.Base.Configuration.csproj NOCOPYLOCAL
	..\..\..\..\..\base\tools\libs\RSG.Base\RSG.Base.csproj NOCOPYLOCAL
	..\..\..\..\..\base\tools\libs\RSG.Interop.Incredibuild\RSG.Interop.Incredibuild.csproj NOCOPYLOCAL
	..\..\..\..\..\base\tools\libs\RSG.Platform\RSG.Platform.csproj NOCOPYLOCAL
	..\..\..\..\..\base\tools\ui\libs\RSG.Base.Editor\RSG.Base.Editor.csproj NOCOPYLOCAL
	..\RSG.Pipeline.Content\RSG.Pipeline.Content.csproj NOCOPYLOCAL
	..\RSG.Pipeline.Core\RSG.Pipeline.Core.csproj NOCOPYLOCAL
	..\RSG.Pipeline.Services\RSG.Pipeline.Services.csproj NOCOPYLOCAL
	..\RSG.Pipeline.Services\RSG.Pipeline.Services.Map.csproj NOCOPYLOCAL
}
References {
	System
	System.ComponentModel.Composition
	System.Core
	System.Xml.Linq
	System.Data.DataSetExtensions
	Microsoft.CSharp
	System.Data
	System.Xml
}
