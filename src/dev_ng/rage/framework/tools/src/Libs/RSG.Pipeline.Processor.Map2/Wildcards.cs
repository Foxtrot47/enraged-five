﻿//---------------------------------------------------------------------------------------------
// <copyright file="Constants.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Processor.Map2
{
    using System;

    /// <summary>
    /// Map Processor Wildcard string; for determining which pipeline steps to execute as many
    /// are optional.
    /// </summary>
    static class Wildcards
    {
        /// <summary>
        /// Occlusion data wildcard.
        /// </summary>
        internal const String OcclusionWildcard = "*.occl.zip";

        /// <summary>
        /// Collision data wildcard.
        /// </summary>
        internal const String CollisionWildcard = "*_collision.zip";
    }

} // RSG.Pipeline.Processor.Map2 namespace
