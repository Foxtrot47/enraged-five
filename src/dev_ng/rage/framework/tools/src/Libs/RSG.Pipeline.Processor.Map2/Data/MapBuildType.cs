﻿//---------------------------------------------------------------------------------------------
// <copyright file="MapBuildType.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Processor.Map2.Data
{

    /// <summary>
    /// Map build type; what we're going to build, determine fully from content-tree.
    /// </summary>
    public enum MapBuildType
    {
        /// <summary>
        /// Regular environment merge; metadata and LOD siblings merged.
        /// </summary>
        EnvironmentMerge,

        /// <summary>
        /// Regular environment build; no special merging.
        /// </summary>
        EnvironmentNonMerge,

        /// <summary>
        /// Environment archetypes environment build; interior and props only.
        /// </summary>
        EnvironmentArchetypesOnly,

        /// <summary>
        /// Environment archetypes environment build; merge props/interiors.
        /// </summary>
        EnvironmentArchetypeMerge,
    }

} // RSG.Pipeline.Processor.Map2.Data namespace
