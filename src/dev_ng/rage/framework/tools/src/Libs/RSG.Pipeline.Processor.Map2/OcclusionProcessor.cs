﻿//---------------------------------------------------------------------------------------------
// <copyright file="OcclusionProcessor.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Processor.Map2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.Composition;
    using System.Linq;
    using RSG.Base.Configuration;
    using RSG.Base.Extensions;
    using RSG.Base.Logging;
    using XGE = RSG.Interop.Incredibuild.XGE;
    using RSG.Pipeline.Content;
    using RSG.Pipeline.Core;

    /// <summary>
    /// Map occlusion processor.
    /// </summary>
    [Export(typeof(IProcessor))]
    class OcclusionProcessor :
        MapProcessorBase
    {
        #region Constants
        private static readonly String DESCRIPTION = "Map Occlusion Processor";
        private static readonly String LOG_CTX = "Map Occlusion";
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public OcclusionProcessor()
            : base(DESCRIPTION)
        {
        }
        #endregion // Constructor(s)

        #region IProcessor Interface Methods
        /// <summary>
        /// Map Occlusion Prebuild stage.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processes"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="syncDependencies"></param>
        /// <param name="resultantProcesses"></param>
        /// <returns></returns>
        public override bool Prebuild(IEngineParameters param, IEnumerable<IProcess> processes, 
            IProcessorCollection processors, IContentTree owner, 
            out IDictionary<IProcess, IEnumerable<IContentNode>> syncDependencies, 
            out IEnumerable<IProcess> resultantProcesses)
        {
            using (ProfileContext ctx = new ProfileContext(param.Log, "Map PreProcessor Prebuild."))
            {
                return (PrebuildImpl(param, processes, processors, owner, out syncDependencies, out resultantProcesses));
            }
        }

        /// <summary>
        /// Prepare; this should never get called.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processes"></param>
        /// <param name="tools"></param>
        /// <param name="tasks"></param>
        /// <returns></returns>
        public override bool Prepare(IEngineParameters param, IEnumerable<IProcess> processes,
            out IEnumerable<XGE.ITool> tools, out IEnumerable<XGE.ITask> tasks)
        {
            throw (new NotImplementedException());
        }
        #endregion // IProcessor Interface Methods
        
        #region Private Methods
        /// <summary>
        /// Validate all processes for map processing; pre-pass in Prebuild.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processes"></param>
        /// <returns></returns>
        private bool ValidateProcesses(IEngineParameters param, IEnumerable<IProcess> processes)
        {
            bool result = true;
            foreach (IProcess process in processes)
            {
                if (process.Inputs.Count() < 1)
                {
                    param.Log.ErrorCtx(LOG_CTX, "Map Occlusion requires one or more input nodes.");
                    result = false;
                }

                if (process.Outputs.Count() != 1)
                {
                    param.Log.ErrorCtx(LOG_CTX, "Map Occlusion requires one or more input nodes.");
                    return false;
                }

                IContentNode output = process.Outputs.First();
                if (!(output is Asset))
                {
                    param.Log.ErrorCtx(LOG_CTX, "Map Occlusion output must be of type Asset.");
                    return false;
                }

                Asset outputAsset = (Asset)output;
                if (outputAsset.AssetType != RSG.Platform.FileType.ZipArchive)
                {
                    param.Log.ErrorCtx(LOG_CTX, "Map Occlusion output Asset must have an AssetType of ZipArchive.");
                    return false;
                }

                // Processor Parameter validation.
                // None required yet.

                // Process Parameter validation.
                result &= ValidateHasParameter(param, process, Parameter.MapProcess_AssociatedMapContainerData);
            }

            return result;
        }

        /// <summary>
        /// Map Occlusion Prebuild stage; actual implementation for surrounding
        /// profile call.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processes"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="syncDependencies"></param>
        /// <param name="resultantProcesses"></param>
        /// <returns></returns>
        ///  
        private bool PrebuildImpl(IEngineParameters param, IEnumerable<IProcess> processes,
            IProcessorCollection processors, IContentTree owner,
            out IDictionary<IProcess, IEnumerable<IContentNode>> syncDependencies,
            out IEnumerable<IProcess> resultantProcesses)
        {
            // Set trivial parameters to allow for early return.
            resultantProcesses = new IProcess[0];
            syncDependencies = new Dictionary<IProcess, IEnumerable<IContentNode>>();

            // Validate processes.
            this.LoadParameters(param);
            if (!ValidateProcesses(param, processes))
                return (false);

            return (false);
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Processor.Map2 namespace
