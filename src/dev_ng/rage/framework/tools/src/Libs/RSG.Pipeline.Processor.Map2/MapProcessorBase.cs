﻿//---------------------------------------------------------------------------------------------
// <copyright file="MapProcessorBase.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Processor.Map2
{
    using System;
    using System.Collections.Generic;
    using SIO = System.IO;
    using System.Linq;
    using RSG.Base.Configuration;
    using RSG.Base.Extensions;
    using RSG.Base.Logging;
    using XGE = RSG.Interop.Incredibuild.XGE;
    using RSG.Pipeline.Core;
    using RSG.Pipeline.Services;

    /// <summary>
    /// Abstract map base processor class; defines the parameter dictionary.
    /// </summary>
    abstract class MapProcessorBase :
        ProcessorBase2
    {
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public MapProcessorBase(String description)
            : base(description)
        {
        }
        #endregion // Constructor(s)

        #region IProcessor Interface Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="process"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="syncDependencies"></param>
        /// <param name="resultantProcesses"></param>
        /// <returns></returns>
        public override bool Prebuild(IEngineParameters param,
            IProcess process, IProcessorCollection processors,
            IContentTree owner, out IEnumerable<IContentNode> syncDependencies,
            out IEnumerable<IProcess> resultantProcesses)
        {
            throw (new NotImplementedException());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processes"></param>
        /// <param name="tools"></param>
        /// <param name="tasks"></param>
        /// <returns></returns>
        public override bool Prepare(IEngineParameters param, IEnumerable<IProcess> processes,
            out IEnumerable<XGE.ITool> tools, out IEnumerable<XGE.ITask> tasks)
        {
            throw (new NotImplementedException());
        }
        #endregion // IProcessor Interface Methods

        #region ProcessorBase2 Method Implementation
        /// <summary>
        /// Return parameters XML filename.
        /// </summary>
        /// <returns></returns>
        /// This is abstract forcing Processor's to implement this.  The general goal
        /// is to reduce the number of parameter files so please implement this
        /// in base processors and share parameter files, using different ParameterDictionary's
        /// where required.
        /// 
        protected override String GetParametersFilename(IEngineParameters param)
        {
            return (SIO.Path.Combine(param.Branch.Project.Config.ToolsConfig,
                "processors", "RSG.Pipeline.Processor.Map2.meta"));
        }
        #endregion // ProcessorBase2 Methods Implementation
    }

} // RSG.Pipeline.Processor.Map2 namespace
