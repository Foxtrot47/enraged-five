﻿//---------------------------------------------------------------------------------------------
// <copyright file="MetadataProcessor.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Processor.Map2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.Composition;
    using System.Diagnostics;
    using SIO = System.IO;
    using System.Linq;
    using RSG.Base.Configuration;
    using RSG.Base.Extensions;
    using XGE = RSG.Interop.Incredibuild.XGE;
    using RSG.Pipeline.Content;
    using RSG.Pipeline.Core;
    using RSG.Pipeline.Services;
    using RSG.Pipeline.Services.Metadata;
    using RSG.Platform;
    using RSG.SceneXml.MapExport;
    using RSG.Pipeline.Processor.Map2.Data;

    /// <summary>
    /// Map Metadata Processor; invokes the Map Metadata Serialiser.
    /// </summary>
    /// This processor expects an IProcess with the following:
    ///   - Inputs
    ///         1) SceneXml files to process Archetypes/Entities for.
    ///         2) Scene Overrides Directory
    ///   - Outputs - 
    ///         1) Output directory.
    ///         2) Export Additions XML file.
    ///   - AdditionalDependentContent - additional SceneXml file dependencies.
    ///   - Parameters
    ///   
    [Export(typeof(IProcessor))]
    class MetadataProcessor :
        MapProcessorBase
    {
        #region Constants
        /// <summary>
        /// Processor description.
        /// </summary>
        private static readonly String DESCRIPTION = "Map Metadata Processor";

        /// <summary>
        /// Processor log context.
        /// </summary>
        private static readonly String LOG_CTX = "Map Metadata Processor";
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public MetadataProcessor()
            : base(DESCRIPTION)
        {
        }
        #endregion // Constructor(s)

        #region IProcessor Interface Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processes"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="syncDependencies"></param>
        /// <param name="resultantProcesses"></param>
        /// <returns></returns>
        public override bool Prebuild(IEngineParameters param, IEnumerable<IProcess> processes, 
            IProcessorCollection processors, IContentTree owner, 
            out IDictionary<IProcess, IEnumerable<IContentNode>> syncDependencies, 
            out IEnumerable<IProcess> resultantProcesses)
        {
            resultantProcesses = new IProcess[0];
            syncDependencies = new Dictionary<IProcess, IEnumerable<IContentNode>>();

            // Validate processes.
            this.LoadParameters(param);
            if (!ValidateProcesses(param, processes))
                return (false);

            // DHM FIX ME: add core metadata zip file if DLC.

            // Loop through all of our processes; marking prebuilt.
            ICollection<IProcess> outputProcesses = new List<IProcess>();
            foreach (IProcess process in processes)
            {
                process.State = ProcessState.Prebuilt;
                outputProcesses.Add(process);
            }

            return (true);
        }

        /// <summary>
        /// Prepare a build for the processes; populating the XGE project with
        /// the tools, environments and tasks required.  Including setting up
        /// the task dependency chain.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="processes"></param>
        /// <param name="tools"></param>
        /// <param name="tasks"></param>
        /// <returns></returns>
        public override bool Prepare(IEngineParameters param, IEnumerable<IProcess> processes,
            out IEnumerable<XGE.ITool> tools, out IEnumerable<XGE.ITask> tasks)
        {
            this.LoadParameters(param);
            bool allow_remote = GetXGEAllowRemote(param, false);
            
            bool result = true;
            XGE.ITool serialiserTool = XGEFactory.GetMapProcessorTool(param.Log,
                param.Branch.Project.Config, XGEFactory.MapToolType.MetadataSerialiser2,
                "Map Metadata Serialiser", allow_remote);
            XGE.ITaskJobGroup taskGroup = new XGE.TaskGroup("Map Metadata Serialiser");

            IEnumerable<XGE.ITaskJob> taskJobs;
            result &= CreateTaskJobsFromProcesses(param, processes, serialiserTool, out taskJobs);
            taskGroup.Tasks.AddRange(taskJobs);

            // Assign method output.
            tools = new List<XGE.ITool>(new XGE.ITool[] { serialiserTool });
            tasks = new List<XGE.ITask>(new XGE.ITask[] { taskGroup });

            return (result);
        }
        #endregion // IProcessor Interface Methods

        #region Private Methods
        /// <summary>
        /// Validate all processes for metadata serialisation; pre-pass in Prebuild.
        /// </summary>
        /// <param name="param">Engine parameters.</param>
        /// <param name="processes">Processes to validate.</param>
        /// <returns></returns>
        private bool ValidateProcesses(IEngineParameters param, IEnumerable<IProcess> processes)
        {
            // Loop through validating all process objects.
            bool result = true;
            foreach (IProcess process in processes)
            {
                if (!process.Inputs.Any())
                {
                    param.Log.ErrorCtx(LOG_CTX, "Map Metadata Process has no inputs.");
                    result = false;
                }
                if (!process.Outputs.Any())
                {
                    param.Log.ErrorCtx(LOG_CTX, "Map Metadata Process has no output.");
                    result = false;
                }
                if (process.Outputs.Count() != 2)
                {
                    param.Log.ErrorCtx(LOG_CTX, "Map Metadata Process expects only two outputs.");
                    result = false;
                }
                if (!(process.Outputs.FirstOrDefault() is Content.Directory))
                {
                    param.Log.ErrorCtx(LOG_CTX, "Map Metadata Process expects output directory.");
                    result = false;
                }

                // Processor Parameter validation.
                result &= ValidateHasParameter(param, this, Parameter.MapProcess_MetadataNonSerialisedTimeCycleTypes);

                // Process Parameter validation.
                result &= ValidateHasParameter(param, process, Parameter.MapProcess_AssociatedMapBuildData);
                result &= ValidateHasParameter(param, process, Parameter.MapProcess_MetadataAudioCollisionFilename);
                result &= ValidateHasParameter(param, process, Parameter.MapProcess_MetadataSceneOverridesDirectory);
            }

            return (true);
        }
        
        /// <summary>
        /// Given a collection of processes, create a collection of ITaskJob objects.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processes"></param>
        /// <param name="serialiserTool"></param>
        /// <param name="taskJobs"></param>
        /// <returns></returns>
        private bool CreateTaskJobsFromProcesses(IEngineParameters param, IEnumerable<IProcess> processes,
            XGE.ITool serialiserTool, out IEnumerable<XGE.ITaskJob> taskJobs)
        {
            List<XGE.ITaskJob> taskJobsList = new List<XGE.ITaskJob>();

            // Pass our IProcesses to construct metadata serialiser options about them.
            IList<Tuple<IProcess, MapMetadataSerialiserOptions>> serialiserProcesses =
                new List<Tuple<IProcess, MapMetadataSerialiserOptions>>();

            // Loop through our processes; creating options.
            bool result = true;
            foreach (IProcess process in processes)
            {   
                // DHM TODO: optimisation - lump all non-merge containers together.
                // See previous implementation.
                XGE.ITaskJob taskJob = null;
                if (CreateTaskJobForProcess(param, process, serialiserTool, out taskJob))
                {
                    taskJobsList.Add(taskJob);
                }
                else
                {
                    param.Log.Error("Failed to create Task Job.");
                    result = false;
                }
            }

            taskJobs = taskJobsList.ToArray();
            return (result && taskJobs.Any());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <param name="process"></param>
        /// <param name="serialiserTool"></param>
        /// <param name="taskJob"></param>
        /// <returns></returns>
        private bool CreateTaskJobForProcess(IEngineParameters param, IProcess process, XGE.ITool serialiserTool, 
            out XGE.ITaskJob taskJob)
        {
            bool result = true;
            MapBuildData mapBuildData = (MapBuildData)process.GetParameter(Parameter.MapProcess_AssociatedMapBuildData, null);
            IEnumerable<String> allInputSceneXmlFiles = mapBuildData.MapsContainerData.Select(m => m.SceneXmlFilename);
            String audioCollisionMapFilename = process.GetParameter(Parameter.MapProcess_MetadataAudioCollisionFilename, String.Empty);
            String sceneOverridesDirectory = process.GetParameter(Parameter.MapProcess_MetadataSceneOverridesDirectory, String.Empty);
            String nonSerialisedTimeCycleTypesText = this.GetParameter(Parameter.MapProcess_MetadataNonSerialisedTimeCycleTypes, String.Empty);

            // create the options object
            String[] NonSerialiserTimeCycleTypes = nonSerialisedTimeCycleTypesText.Split(';');
            MapMetadataSerialiserOptions options = new MapMetadataSerialiserOptions(
                audioCollisionMapFilename, sceneOverridesDirectory, NonSerialiserTimeCycleTypes);

            String xgeTaskName = mapBuildData.MetadataName;
            if (param.Branch.Project.IsDLC)
                xgeTaskName = MapAsset.AppendMapPrefixIfRequired(param.Branch.Project, xgeTaskName);

            XGE.ITaskJob task = new XGE.TaskJob(xgeTaskName);

            IEnumerable<String> forcedItypDependencies = process.GetParameter(Constants.ParamMap_ForceItypDependency, new String[0]);

            String taskName = String.Format("Map Metadata [{0}]", xgeTaskName);

            // Create task structure for our new metadata serialiser.
            String assetCombineFilename = mapBuildData.MergeCombineDocumentFilename;
            String mapExportAdditionsFilename = mapBuildData.MetadataManifestAdditionsFilename;

            List<MapMetadataSerialiserManifestDependency> additionalManifestDependencies = new List<MapMetadataSerialiserManifestDependency>();
            IEnumerable<MapMetadataSerialiserSceneXmlDependency> additionalSceneXmlDependencies =
                process.AdditionalDependentContent.OfType<IFilesystemNode>().Select(i => new MapMetadataSerialiserSceneXmlDependency(i.AbsolutePath));
            
            List<MapMetadataSerialiserOutput> outputData = new List<MapMetadataSerialiserOutput>();

            // Create our base platform which is for backwards compatibility with the current .ityp and imap files.
            MapMetadataSerialiserOutput baseEntry = new MapMetadataSerialiserOutput(
                    xgeTaskName, mapBuildData.MetadataCacheDirectory, mapBuildData.MetadataExportArchetypes, mapBuildData.MetadataExportEntities, forcedItypDependencies,
                    param.Flags.HasFlag(EngineFlags.Preview), "independent");
            outputData.Add(baseEntry);

            List<MapMetadataSerialiserCoreGameMetadataZipDependency> mapMetadataZips = new List<MapMetadataSerialiserCoreGameMetadataZipDependency>();
            String metadataZipFilename = process.GetParameter(Constants.ParamMap_CoreMetadataZip, String.Empty);
            if (!String.IsNullOrEmpty(metadataZipFilename))
            {
                mapMetadataZips.Add(new MapMetadataSerialiserCoreGameMetadataZipDependency(metadataZipFilename));
            }

            MapMetadataSerialiserTaskBase metadataTask = null;
            switch (mapBuildData.BuildType)
            {
                case MapBuildType.EnvironmentMerge:
                case MapBuildType.EnvironmentArchetypeMerge:
                    {
                        IList<MapMetadataSerialiserMergeInputGroup> inputGroups = new List<MapMetadataSerialiserMergeInputGroup>();
                        foreach (MapContainerMergeData mergeData in mapBuildData.MapsContainerMergeData)
                        {
                            MapMetadataSerialiserMergeInputGroup inputGroup = new MapMetadataSerialiserMergeInputGroup(mergeData.MergeName,
                                mergeData.MapContainers.Select(m => m.SceneXmlFilename));
                            inputGroups.Add(inputGroup);
                        }
                        // Create the merge task from all of our input groups.
                        metadataTask = new MapMetadataSerialiserMergeTask(xgeTaskName, inputGroups, additionalSceneXmlDependencies,
                            additionalManifestDependencies, mapMetadataZips, outputData,
                            assetCombineFilename, mapExportAdditionsFilename);
                    }
                    break;
                case MapBuildType.EnvironmentNonMerge:
                case MapBuildType.EnvironmentArchetypesOnly:
                    {
                        IList<MapMetadataSerialiserInput> inputs = new List<MapMetadataSerialiserInput>();
                        foreach (MapContainerData mapData in mapBuildData.MapsContainerData)
                        {
                            MapMetadataSerialiserInput input = new MapMetadataSerialiserInput(mapData.SceneXmlFilename);
                            inputs.Add(input);
                        }
                        // Create the serialiser task from our inputs.
                        metadataTask = new MapMetadataSerialiserTask(xgeTaskName, inputs, additionalSceneXmlDependencies,
                            additionalManifestDependencies, mapMetadataZips, outputData,
                            assetCombineFilename, mapExportAdditionsFilename);
                    }
                    break;
                default:
                    param.Log.ToolError("Unrecognised MapBuildType: '{0}'.  Internal error.", mapBuildData.BuildType);
                    result = false;
                    break;
            }

            // Set XGE parameters for dependency tracking.
            process.SetParameter(Constants.ProcessXGE_Task, task);
            process.SetParameter(Constants.ProcessXGE_TaskName, xgeTaskName.Replace(" ", "_"));

            String configDataFilename = SIO.Path.Combine(mapBuildData.CacheDirectory, String.Format("map_metadata_serialiser_{0}_config.xml", xgeTaskName));
            configDataFilename = SIO.Path.GetFullPath(param.Branch.Environment.Subst(configDataFilename));
            MapMetadataSerialiserConfig configData = new MapMetadataSerialiserConfig(
                options,
                new MapMetadataSerialiserTaskBase[] { metadataTask });
            result &= configData.Serialise(param.Log, configDataFilename);

            // Create XGE task.
            task.Tool = serialiserTool;
            task.Caption = taskName;
            task.InputFiles.Add(configDataFilename);
            if (param.Branch.Project.IsDLC)
                task.Parameters = String.Format("--config {0} --branch {1} --dlc {2}", configDataFilename, param.Branch.Name, param.Branch.Project.Name);
            else
                task.Parameters = String.Format("--config {0} --branch {1}", configDataFilename, param.Branch.Name);
            task.SourceFile = configDataFilename;

            taskJob = task;
            return (result);
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Processor.Map2 namespace
