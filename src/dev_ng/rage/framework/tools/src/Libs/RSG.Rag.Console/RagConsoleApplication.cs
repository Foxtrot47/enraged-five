﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.Math;
using RSG.Editor.Controls;
using RSG.Rag.Clients;
using RSG.Rag.Contracts.Data;
using RSG.Rag.Contracts.Services;
using RSG.Services.Common.Clients;

namespace RSG.Rag.Console
{
    /// <summary>
    /// 
    /// </summary>
    public class RagConsoleApplication : RsInteractiveConsoleApplication, IConnectionEvents
    {
        #region Program Entry Point
        /// <summary>
        /// Program entry point.
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        [STAThread]
        static int Main(string[] args)
        {
            RagConsoleApplication app = new RagConsoleApplication();
            return app.Run();
        }
        #endregion

        #region Fields
        /// <summary>
        /// Reference to the game connection we are currently issuing commands to.
        /// </summary>
        private GameConnection _connectedHub;

        /// <summary>
        /// Client for receiving connection events.
        /// </summary>
        private ConnectionSubscriptionClient _subscriptionClient;
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="RagConsoleApplication"/> class.
        /// </summary>
        public RagConsoleApplication()
            : base()
        {
            // Register the commands we wish to support.
            RegisterConsoleCommand("connections", GameConnectionsCommandHandler, "Lists the available game connections.");
            RegisterConsoleCommand("connect", ConnectCommandHandler, "<IP|DEFAULT> Connects to a particular game connection.");
            RegisterConsoleCommand("disconnect", DisconnectCommandHandler, "Disconnects from the connected game connection.");
            RegisterConsoleCommand("subscribe", SubscribeCommandHandler, "Subscribes for game connection events.");
            RegisterConsoleCommand("unsubscribe", UnsubscribeCommandHandler, "Unsubscribes from game connection events.");
            RegisterConsoleCommand("console", ConsoleCommandHandler, "<CONSOLECOMMAND> Issues a command against the console service.");
            RegisterConsoleCommand("exists", WidgetExistsCommandHandler, "<WIDGETPATH> Reads a particular widget's value.");

            RegisterConsoleCommand("test", TestCommandHandler, "Test of the moment.");
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets a semi-colon separate list of the application authors email addresses. These
        /// are the addresses which are used by the unhandled exception dialog to determine
        /// where the mail should be sent by default.
        /// e.g.
        /// *tools@rockstarnorth.com
        /// michael.taschler@rockstarnorth.com;dave.evans@rockstarnorth.com
        /// </summary>
        public override string AuthorEmailAddress
        {
            get { return "RDR3Tools@rockstarsandiego.com"; }
        }

        /// <summary>
        /// Text to display in the console's caret.
        /// </summary>
        protected override string CaretText
        {
            get
            {
                if (_connectedHub != null)
                {
                    return String.Format("{0}@{1}", Environment.UserName, _connectedHub.GameIP);
                }
                else
                {
                    return Environment.UserName;
                }
            }
        }
        #endregion

        #region Console Command Handlers
        /// <summary>
        /// Test command handler for the test flavour of the day.
        /// </summary>
        /// <param name="command"></param>
        /// <param name="arguments"></param>
        private void TestCommandHandler(String command, String[] arguments)
        {
            WidgetClient client = null;
            try
            {
                client = RagClientFactory.CreateWidgetClient(_connectedHub);
                //client.WriteVector3Widget(arguments[0], new Vector3f(0.0f, 1.0f, 2.0f));
                client.WriteBoolWidget("Cut Scene Debug/Current Scene/Skeleton overrides/Activate Skeleton Override", true);
            }
            catch (Exception ex)
            {
                Log.ToolExceptionCtx(CommandLogCtx, ex, "Error connecting to shortcut menu service.");
            }
            finally
            {
                client.CloseOrAbort();
            }
        }

        /// <summary>
        /// Lists the available game connections.
        /// </summary>
        /// <param name="command"></param>
        /// <param name="arguments"></param>
        private void GameConnectionsCommandHandler(String command, String[] arguments)
        {
            try
            {
                IEnumerable<GameConnection> connections = RagClientFactory.GetGameConnections();

                String format = "{0,-15} {1,-12} {2,-5}";
                String[] headers = new String[] { "IP", "Platform", "Default" };
                String header = String.Format(format, headers);

                PrettyPrintTable(
                    header,
                    connections,
                    new Action<GameConnection>(connection =>
                    {
                        object[] fields = { connection.GameIP, connection.Platform, connection.DefaultConnection };
                        System.Console.WriteLine(String.Format(format, fields));
                    }));
            }
            catch (Exception ex)
            {
                Log.ToolExceptionCtx(CommandLogCtx, ex, "Error connecting to shortcut menu service.");
            }
        }

        /// <summary>
        /// Connects to a particular game connection (based on IP).
        /// </summary>
        /// <param name="command"></param>
        /// <param name="arguments"></param>
        private void ConnectCommandHandler(String command, String[] arguments)
        {
            if (1 != arguments.Length)
            {
                Log.ErrorCtx(
                    CommandLogCtx,
                    "Invalid number of arguments ({0}); expecting 1: <IP|DEFAULT>.",
                    arguments.Length);
                HelpCommandHandler(command, arguments);
                return;
            }

            try
            {
                IEnumerable<GameConnection> connections = RagClientFactory.GetGameConnections();

                if (connections.Any())
                {
                    if (String.Equals(arguments[0], "DEFAULT", StringComparison.InvariantCultureIgnoreCase))
                    {
                        _connectedHub = connections.First(item => item.DefaultConnection);
                    }
                    else
                    {
                        IPAddress ip;
                        if (IPAddress.TryParse(arguments[0], out ip))
                        {
                            _connectedHub = connections.FirstOrDefault(item => item.GameIP.Equals(ip));
                            if (_connectedHub == null)
                            {
                                Log.ToolErrorCtx(CommandLogCtx, "Connection with ip '{0}' doesn't exist.  Verify it is correct.", arguments[0]);
                            }
                        }
                        else
                        {
                            Log.ToolErrorCtx(CommandLogCtx, "Unparseable IP specified.");
                        }
                    }
                }
                else
                {
                    Log.ToolErrorCtx(CommandLogCtx, "No games connected to RAG.");
                }
            }
            catch (Exception ex)
            {
                Log.ToolExceptionCtx(CommandLogCtx, ex, "Error connecting to shortcut menu service.");
            }
        }

        /// <summary>
        /// Disconnects from the connected game.
        /// </summary>
        /// <param name="command"></param>
        /// <param name="arguments"></param>
        private void DisconnectCommandHandler(String command, String[] arguments)
        {
            _connectedHub = null;
        }

        /// <summary>
        /// Subscribes to receive game connection/disconnection events.
        /// </summary>
        /// <param name="command"></param>
        /// <param name="arguments"></param>
        private void SubscribeCommandHandler(String command, String[] arguments)
        {
            if (0 != arguments.Length)
            {
                Log.ErrorCtx(
                    CommandLogCtx,
                    "Invalid number of arguments ({0}); expecting 0.",
                    arguments.Length);
                HelpCommandHandler(command, arguments);
                return;
            }

            _subscriptionClient = RagClientFactory.CreateConnectionSubscriptionClient(this);
            _subscriptionClient.Subscribe();
        }

        /// <summary>
        /// Unsubscribes from game connection/disconnection events.
        /// </summary>
        /// <param name="command"></param>
        /// <param name="arguments"></param>
        private void UnsubscribeCommandHandler(String command, String[] arguments)
        {
            if (0 != arguments.Length)
            {
                Log.ErrorCtx(
                    CommandLogCtx,
                    "Invalid number of arguments ({0}); expecting 0.",
                    arguments.Length);
                HelpCommandHandler(command, arguments);
                return;
            }

            _subscriptionClient.Unsubscribe();
            _subscriptionClient.Close();
            _subscriptionClient = null;
        }

        /// <summary>
        /// Issues a command against the console service.
        /// </summary>
        /// <param name="command"></param>
        /// <param name="arguments"></param>
        private void ConsoleCommandHandler(String command, String[] arguments)
        {
            if (1 != arguments.Length)
            {
                Log.ErrorCtx(
                    CommandLogCtx,
                    "Invalid number of arguments ({0}); expecting 1: <CONSOLECOMMAND>.",
                    arguments.Length);
                HelpCommandHandler(command, arguments);
                return;
            }

            if (_connectedHub == null)
            {
                Log.ErrorCtx(
                    CommandLogCtx,
                    "Not connected to a hub.  Issue a connect command first.");
                return;
            }

            ConsoleClient client = null;
            try
            {
                client = RagClientFactory.CreateConsoleClient(_connectedHub);
                ConsoleCommandResponse response = client.ExecuteCommand(arguments[0]);

                if (response.State == ConsoleCommandExecutionState.Ignored)
                {
                    Log.ErrorCtx(CommandLogCtx, "Command was ignored.");
                }
                else if (response.State == ConsoleCommandExecutionState.Faulted)
                {
                    Log.ErrorCtx(CommandLogCtx, "Command faulted.");
                    Log.ErrorCtx(CommandLogCtx, response.ExceptionDetails);
                }
                else
                {
                    Log.MessageCtx(CommandLogCtx, response.Response);
                }
            }
            catch (Exception ex)
            {
                Log.ToolExceptionCtx(CommandLogCtx, ex, "Error connecting to shortcut menu service.");
            }
            finally
            {
                client.CloseOrAbort();
            }
        }

        /// <summary>
        /// Contacts the widget service to determine whether a widget exists.
        /// </summary>
        /// <param name="command"></param>
        /// <param name="arguments"></param>
        private void WidgetExistsCommandHandler(String command, String[] arguments)
        {
            if (1 != arguments.Length)
            {
                Log.ErrorCtx(
                    CommandLogCtx,
                    "Invalid number of arguments ({0}); expecting 1: <WIDGETPATH>.",
                    arguments.Length);
                HelpCommandHandler(command, arguments);
                return;
            }

            if (_connectedHub == null)
            {
                Log.ErrorCtx(
                    CommandLogCtx,
                    "Not connected to a hub.  Issue a connect command first.");
                return;
            }

            WidgetClient client = null;
            try
            {
                client = RagClientFactory.CreateWidgetClient(_connectedHub);
                Log.MessageCtx(CommandLogCtx, "{0}", client.WidgetExists(arguments[0]));
            }
            catch (Exception ex)
            {
                Log.ToolExceptionCtx(CommandLogCtx, ex, "Error connecting to shortcut menu service.");
            }
            finally
            {
                client.CloseOrAbort();
            }
        }
        #endregion

        #region Event Handlers
        /// <summary>
        /// Event that is fired when a new game connects with the proxy.
        /// </summary>
        /// <param name="connection"></param>
        public void OnGameConnected(GameConnection connection)
        {
            Log.Message("New game connection detected: {0}@{1}", connection.Platform, connection.GameIP);
        }

        /// <summary>
        /// Event that is fired when the rag proxy loses its connection to a game.
        /// </summary>
        /// <param name="connection"></param>
        public void OnGameDisconnected(GameConnection connection)
        {
            Log.Message("Lost game connection: {0}@{1}", connection.Platform, connection.GameIP);
            if (connection == _connectedHub)
            {
                _connectedHub = null;
            }
        }
        #endregion
    }
}
