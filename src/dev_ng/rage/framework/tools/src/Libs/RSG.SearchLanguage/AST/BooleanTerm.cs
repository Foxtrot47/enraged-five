﻿//---------------------------------------------------------------------------------------------
// <copyright file="BooleanExpression.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.SearchLanguage.AST
{
    using System;
    using RSG.Base.Language;

    /// <summary>
    /// 
    /// </summary>
    public class BooleanTerm : AstNodeBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public bool Invert { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public ObjectProperty ObjectProperty { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public OperatorType Operation { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public ILiteralValue Literal { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="position"></param>
        /// <param name="invert"></param>
        /// <param name="identifier"></param>
        /// <param name="op"></param>
        /// <param name="literal"></param>
        public BooleanTerm(IPosition position, bool invert, ObjectProperty objectProperty,
            OperatorType op, ILiteralValue literal)
            : base(position)
        {
            this.Invert = invert;
            this.ObjectProperty = objectProperty;
            this.Operation = op;
            this.Literal = literal;
        }
        #endregion // Constructor(s)
    }

} // RSG.Search.AST namespace
