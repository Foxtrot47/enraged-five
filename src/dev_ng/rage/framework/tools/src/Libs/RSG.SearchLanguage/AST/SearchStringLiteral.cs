﻿//---------------------------------------------------------------------------------------------
// <copyright file="SearchStringLiteral.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.SearchLanguage.AST
{
    using System;
    using System.Diagnostics;
    using RSG.Base.Language;

    /// <summary>
    /// Search query representing a simple text string search.
    /// </summary>
    public class SearchStringLiteral : SearchBase
    {
        #region Properties
        /// <summary>
        /// String literal expression for search.
        /// </summary>
        /// <see cref="StringLiteralExpression"/>
        /// <see cref="LiteralValue"/>
        public IAstNode Expression
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor; specifying the literal text to search for.
        /// </summary>
        /// <param name="position"></param>
        /// <param name="expr"></param>
        public SearchStringLiteral(IPosition position, IAstNode expr)
            : base(position)
        {
            Debug.Assert(expr is LiteralValue<String> || expr is StringLiteralExpression,
                "Unexpected expression type for SearchStringLiteral.");
            
            this.Expression = expr;
        }
        #endregion // Constructor(s)
    }

} // RSG.Search.AST namespace
