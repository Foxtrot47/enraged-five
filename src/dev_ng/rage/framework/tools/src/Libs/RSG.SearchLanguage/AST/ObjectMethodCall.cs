﻿//---------------------------------------------------------------------------------------------
// <copyright file="ObjectMethodCall.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.SearchLanguage.AST
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using RSG.Base.Language;

    /// <summary>
    /// 
    /// </summary>
    public class ObjectMethodCall : AstNodeBase
    {
        #region Properties
        /// <summary>
        /// Object symbol.
        /// </summary>
        public SearchSymbol Object
        {
            get;
            private set;
        }

        /// <summary>
        /// Method symbol.
        /// </summary>
        public SearchSymbol Method
        {
            get;
            private set;
        }

        /// <summary>
        /// Method call parameters.
        /// </summary>
        public IEnumerable<ILiteralValue> Parameters
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="position"></param>
        /// <param name="obj"></param>
        public ObjectMethodCall(IPosition position, SearchSymbol obj,
            SearchSymbol method)
            : base(position)
        {
            Debug.Assert(obj.Type.Equals(SearchSymbolType.Object));
            Debug.Assert(method.Type.Equals(SearchSymbolType.MethodCall));

            this.Object = obj;
            this.Method = method;
            this.Parameters = new ILiteralValue[0];
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="position"></param>
        /// <param name="obj"></param>
        /// <param name="method"></param>
        /// <param name="parameters"></param>
        public ObjectMethodCall(IPosition position, SearchSymbol obj,
            SearchSymbol method, IEnumerable<ILiteralValue> parameters)
            : base(position)
        {
            Debug.Assert(obj.Type.Equals(SearchSymbolType.Object));
            Debug.Assert(method.Type.Equals(SearchSymbolType.MethodCall));

            this.Object = obj;
            this.Method = method;
            this.Parameters = parameters;
        }
        #endregion // Constructor(s)

        #region Object Overridden Methods
        /// <summary>
        /// Return String representation.
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return (String.Format("{0}.{1}({2})", this.Object.Name, this.Method.Name,
                String.Join(".", this.Parameters)));
        }
        #endregion // Object Overridden Methods
    }

} // RSG.SearchLanguage.AST namespace
