﻿//---------------------------------------------------------------------------------------------
// <copyright file="SearchSymbol.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.SearchLanguage
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using RSG.Base.Language;

    /// <summary>
    /// 
    /// </summary>
    public class SearchSymbol : ISymbol<SearchSymbolType>
    {
        #region Properties
        /// <summary>
        /// Symbol name.
        /// </summary>
        public String Name
        {
            get;
            private set;
        }

        /// <summary>
        /// Symbol type.
        /// </summary>
        public SearchSymbolType Type
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="type"></param>
        public SearchSymbol(String name, SearchSymbolType type)
        {
            this.Name = name;
            this.Type = type;
        }
        #endregion // Constructor(s)
    }

} // RSG.SearchLanguage namespace
