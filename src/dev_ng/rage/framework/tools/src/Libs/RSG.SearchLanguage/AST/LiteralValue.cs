﻿//---------------------------------------------------------------------------------------------
// <copyright file="LiteralValue.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.SearchLanguage.AST
{
    using RSG.Base.Language;
    
    /// <summary>
    /// AST node representing a literal value.
    /// </summary>
    public class LiteralValue<T> : 
        AstNodeBase,
        ILiteralValue
    {
        #region Properties
        /// <summary>
        /// Literal value.
        /// </summary>
        public T Value
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="position"></param>
        /// <param name="value"></param>
        public LiteralValue(IPosition position, T value)
            : base(position)
        {
            this.Value = value;
        }
        #endregion // Constructor(s)
    }

} // RSG.SearchLanguage.AST namespace
