﻿//---------------------------------------------------------------------------------------------
// <copyright file="Identifier.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.SearchLanguage.AST
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using RSG.Base.Language;

    /// <summary>
    /// Identifier AST node.
    /// </summary>
    public class Identifier : AstNodeBase
    {
        #region Properties
        /// <summary>
        /// Identifier symbol.
        /// </summary>
        public SearchSymbol Symbol
        {
            get;
            private set;
        }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="position"></param>
        /// <param name="name"></param>
        public Identifier(IPosition position, SearchSymbol symbol)
            : base(position)
        {
            this.Symbol = symbol;
        }
        #endregion // Constructor(s)
    }

} // RSG.Search.AST
