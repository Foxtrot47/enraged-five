grammar SearchLanguage;

/*
 * Parser Rules
 */
@header {
	using System;
	using RSG.Base.Language;
	using RSG.Interop.Antlr;
	using RSG.SearchLanguage.AST;
}
@parser::members {
	// Symbol table for object identifiers.
	SearchSymbolTable SymbolTable = new SearchSymbolTable();
}

// Top-level Query and Grammar Entry-Point
query returns [SearchBase q]
	: string_literal_expression 
		{ 
			IPosition pos = new AntlrPosition($string_literal_expression.qs.Position);
			$q = new SearchStringLiteral(pos, $string_literal_expression.qs);
		}
	| find_query 
		{ 
			$q = $find_query.qf; 
		}
	;

// String literal search; includes expression syntax for OR/AND and Google-like default AND.
string_literal_expression returns [IAstNode qs]
	: s=StringLiteral 
		{ 
			IPosition pos = new AntlrPosition($s);
			String literalText = $s.text.TrimStart('"').TrimEnd('"');
			$qs = new LiteralValue<string>(pos, literalText); 
		}
	| s1=StringLiteral s2=StringLiteral
		{
			// Google-like 'AND' syntax.
			IPosition literal1Pos = new AntlrPosition($s1);
			IPosition literal2Pos = new AntlrPosition($s2);
			String literal1Text = $s1.text.TrimStart('"').TrimEnd('"');
			String literal2Text = $s2.text.TrimStart('"').TrimEnd('"');
			LiteralValue<String> l1 = new LiteralValue<String>(literal1Pos, literal1Text);
			LiteralValue<String> l2 = new LiteralValue<String>(literal2Pos, literal2Text);

			$qs = new StringLiteralExpression(literal1Pos, l1, LogicalOperatorType.And, l2);
		}
	| s=StringLiteral logical_operator e=string_literal_expression
		{
			IPosition literalPos = new AntlrPosition($s);
			String literalText = $s.text.TrimStart('"').TrimEnd('"');
			LiteralValue<String> l = new LiteralValue<String>(literalPos, literalText);

			$qs = new StringLiteralExpression(literalPos, l, $logical_operator.o, $e.qs);
		}
	;

// Complex SQL-like FIND queries with WHERE conditions.
find_query returns [SearchBase qf]
	: FIND id=object_identifier FROM ss=search_spaces 
		{ 
			IPosition pos = new AntlrPosition($FIND);
			Identifier i = $id.i;
			IdentifierList idl = $ss.ss;
			$qf = new SearchAll(pos, i, idl); 
		}
	| FIND id=object_identifier FROM ss=search_spaces WHERE search_condition 
		{ 
			IPosition pos = new AntlrPosition($FIND);
			Identifier i = $id.i;
			IdentifierList idl = $ss.ss;
			$qf = new SearchQuery(pos, i, idl, $search_condition.expr); 
		}
	;

// Object identifier; can be used in WHERE condition.
object_identifier returns [Identifier i]
	: IDENTIFIER 
		{ 
			IPosition pos = new AntlrPosition($IDENTIFIER);
			SearchSymbol sym = new SearchSymbol($IDENTIFIER.text, SearchSymbolType.Object);
			$i = new Identifier(pos, sym); 
		}
	;

// Search spaces (list of identifiers).
search_spaces returns [IdentifierList ss]
	: IDENTIFIER
		{
			IPosition pos = new AntlrPosition($IDENTIFIER);
			SearchSymbol sym = new SearchSymbol($IDENTIFIER.text, SearchSymbolType.SearchSpace);
			Identifier ident = new Identifier(pos, sym); 
			$ss = new IdentifierList(pos, ident);
		}
	| IDENTIFIER COMMA list=search_spaces
		{
			IPosition pos = new AntlrPosition($IDENTIFIER);
			SearchSymbol sym = new SearchSymbol($IDENTIFIER.text, SearchSymbolType.SearchSpace);
			Identifier ident = new Identifier(pos, sym);
			List<Identifier> identifiers = new List<Identifier>();
			identifiers.Add(ident);
			identifiers.AddRange($list.ss); 
			$ss = new IdentifierList(pos, identifiers);
		}
	;
	
search_condition returns [IAstNode expr]
	: boolean_value_expression
		{
			$expr = $boolean_value_expression.expr;
		}
	;

boolean_value_expression returns [IAstNode expr]
	: LPAREN boolean_term RPAREN
		{ 
			$expr = $boolean_term.term;
		}
	| boolean_term
		{ 
			$expr = $boolean_term.term;
		}
	| boolean_term OR be=boolean_value_expression
		{
			IPosition pos = new AntlrPosition($boolean_term.term.Position);
			IAstNode rhs = $be.expr;
			$expr = new BooleanCompositeExpression(pos, $boolean_term.term, LogicalOperatorType.Or, rhs);
		}
	| boolean_term AND be=boolean_value_expression
		{
			IPosition pos = new AntlrPosition($boolean_term.term.Position);
			IAstNode rhs = $be.expr;
			$expr = new BooleanCompositeExpression(pos, $boolean_term.term, LogicalOperatorType.And, rhs);
		}
	;

boolean_term returns [BooleanTerm term]
	: (NOT*) object_property operator literal_value
		{
			IPosition pos = new AntlrPosition($object_property.prop.Position);
			bool invert = (null != $NOT);
			$term = new BooleanTerm(pos, invert, $object_property.prop, $operator.o, $literal_value.l);
		}
	| (NOT*) object_property
		{
			IPosition pos = new AntlrPosition($object_property.prop.Position);
			bool invert = (null != $NOT);
			// Rather than have to deal with inverted logic in applications; we simply 
			// set the correct literal here to compare.
			if (invert)
				$term = new BooleanTerm(pos, false, $object_property.prop, OperatorType.Equals, new LiteralValue<bool>(pos, false));
			else
				$term = new BooleanTerm(pos, false, $object_property.prop, OperatorType.Equals, new LiteralValue<bool>(pos, true));
		}
	;

object_property returns [ObjectProperty prop]
	: IDENTIFIER PERIOD property
		{
			IPosition pos = new AntlrPosition($IDENTIFIER);
			SearchSymbol sym = new SearchSymbol($IDENTIFIER.text, SearchSymbolType.Object);
			$prop = new ObjectProperty(pos, sym, $property.sym);
		}
	;

// Object Property Accessor
property returns [SearchSymbol sym]
	: IDENTIFIER
		{
			IPosition pos = new AntlrPosition($IDENTIFIER);
			$sym = new SearchSymbol($IDENTIFIER.text, SearchSymbolType.Property);
		}
	| IDENTIFIER PERIOD trail=property
		{
			IPosition pos = new AntlrPosition($IDENTIFIER);
			String propertyName = String.Format("{0}.{1}", $IDENTIFIER.text, $trail.sym.Name);
			$sym = new SearchSymbol(propertyName, SearchSymbolType.Property);
		}
	;
	
operator returns [OperatorType o]
	: EQUALITY { $o = OperatorType.Equals; }
	| GT { $o = OperatorType.GreaterThan; }
	| GTEQ { $o = OperatorType.GreaterThanOrEqual; }
	| LT { $o = OperatorType.LessThan; }
	| LTEQ { $o = OperatorType.LessThanOrEqual; }
	;

// Logical operators; for combining String literals.
logical_operator returns [LogicalOperatorType o]
	: OR { $o = LogicalOperatorType.Or; }
	| AND { $o = LogicalOperatorType.And; }
	;

// Literal values
literal_value returns [ILiteralValue l]
	: literal=IntegerLiteral
		{
			IPosition pos = new AntlrPosition($literal);
			int value = default(int);
			if (int.TryParse($literal.text, out value))
				$l = new LiteralValue<int>(pos, value);
			else
			{
				this.NotifyErrorListeners($literal, "Integer literal not recognised.", null);
				$l = new LiteralValue<int>(pos, 0);
			}
		}
	| literal=FloatLiteral
		{
			IPosition pos = new AntlrPosition($literal);
			float value = default(float);
			if (float.TryParse($literal.text, out value))
				$l = new LiteralValue<float>(pos, value);
			else
			{
				this.NotifyErrorListeners($literal, "Float literal not recognised.", null);
				$l = new LiteralValue<float>(pos, 0);
			}
		}
	| literal=StringLiteral
		{
			IPosition pos = new AntlrPosition($literal);
			$l = new LiteralValue<String>(pos, $literal.text);
		}
	| literal=TrueValue
		{
			IPosition pos = new AntlrPosition($literal);
			$l = new LiteralValue<bool>(pos, true);
		}
	| literal=FalseValue
		{
			IPosition pos = new AntlrPosition($literal);
			$l = new LiteralValue<bool>(pos, false);
		}
	;

/*
 * Lexer Rules
 */

FIND       : 'FIND';
FROM       : 'FROM';
WHERE      : 'WHERE';
AND        : 'AND';
OR         : 'OR';
NOT		   : 'NOT';
EQUALITY   : '==';
LT         : '<';
LTEQ       : '<=';
GT         : '>';
GTEQ       : '>=';
LPAREN     : '(';
RPARAN     : ')';
COMMA      : ',';
PERIOD     : '.';
IDENTIFIER : IDENTIFIER_NONDIGIT ( IDENTIFIER_NONDIGIT | DIGIT )*;
IDENTIFIER_NONDIGIT : [A-Za-z];
DIGIT : [0-9];

IntegerLiteral : DIGIT+;
FloatLiteral : DIGIT+ '.' DIGIT+;
TrueValue : 'TRUE';
FalseValue : 'FALSE';

StringLiteral
	: '"' StringCharacters? '"'
	;
fragment
StringCharacters
	:   StringCharacter+
	;
fragment
StringCharacter
	:   ~["\\];
