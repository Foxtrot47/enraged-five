﻿//---------------------------------------------------------------------------------------------
// <copyright file="SearchParserResult.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.SearchLanguage
{
    using System.Collections.Generic;
    using RSG.Base.Language;
    using RSG.SearchLanguage.AST;

    /// <summary>
    /// 
    /// </summary>
    public class SearchParserResult
    {
        #region Properties
        /// <summary>
        /// AST for the search query.
        /// </summary>
        public SearchBase Query
        {
            get;
            private set;
        }

        /// <summary>
        /// Available syntax errors.
        /// </summary>
        public IEnumerable<ISyntaxError> SyntaxErrors
        {
            get;
            private set;
        }
        #endregion // Properties


        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="syntaxErrors"></param>
        public SearchParserResult(SearchBase query, IEnumerable<ISyntaxError> syntaxErrors)
        {
            this.Query = query;
            this.SyntaxErrors = new List<ISyntaxError>(syntaxErrors);
        }
        #endregion // Constructor(s)
    }

} // RSG.SearchLanguage namespace
