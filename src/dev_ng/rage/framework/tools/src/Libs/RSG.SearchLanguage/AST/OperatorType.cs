﻿//---------------------------------------------------------------------------------------------
// <copyright file="OperatorType.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.SearchLanguage.AST
{

    /// <summary>
    /// 
    /// </summary>
    public enum OperatorType
    {
        Equals,
        GreaterThan,
        GreaterThanOrEqual,
        LessThan,
        LessThanOrEqual,
    }

} // RSG.SearchLanguage.AST namespace
