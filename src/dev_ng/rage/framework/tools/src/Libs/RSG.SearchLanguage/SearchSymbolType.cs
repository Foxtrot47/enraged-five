﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.SearchLanguage
{

    /// <summary>
    /// 
    /// </summary>
    public enum SearchSymbolType
    {
        Object,
        Property,
        MethodCall,
        SearchSpace,
    }

} // RSG.SearchLanguage namespace
