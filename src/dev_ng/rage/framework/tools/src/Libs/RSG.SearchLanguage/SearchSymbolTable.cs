﻿//---------------------------------------------------------------------------------------------
// <copyright file="SearchSymbolTable.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.SearchLanguage
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using RSG.Base.Language;

    /// <summary>
    /// 
    /// </summary>
    public class SearchSymbolTable : ISymbolTable<SearchSymbol>
    {
        #region Member Data
        /// <summary>
        /// Symbol dictionary.
        /// </summary>
        private IDictionary<SearchSymbol, Object> _symbols;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public SearchSymbolTable()
        {
            this._symbols = new Dictionary<SearchSymbol, Object>();
        }
        #endregion // Constructor(s)

        #region ISymbolTable Interface Methods
        /// <summary>
        /// 
        /// <summary>
        /// Add a new symbol.
        /// </summary>
        /// <param name="sym"></param>
        /// <param name="value"></param>
        public void Add(SearchSymbol sym, Object value)
        {
            if (this._symbols.ContainsKey(sym))
                throw (new ArgumentException(String.Format("Symbol {0} already defined.", sym.Name)));

            this._symbols.Add(sym, value);
        }

        /// <summary>
        /// Remove a previously defined symbol.
        /// </summary>
        /// <param name="sym"></param>
        /// <see cref="Add"/>
        /// <see cref="Lookup"/>
        /// <see cref="IsDefined"/>
        public void Remove(SearchSymbol sym)
        {
            if (!this._symbols.ContainsKey(sym))
                throw (new ArgumentException(String.Format("Cannot remove.  Symbol {0} not defined.", sym.Name)));

            this._symbols.Remove(sym);
        }

        /// <summary>
        /// Lookup the value of a symbol.
        /// </summary>
        /// <param name="sym"></param>
        /// <returns></returns>
        /// <see cref="IsDefined" />
        public Object Lookup(SearchSymbol sym)
        {
            if (!this._symbols.ContainsKey(sym))
                throw (new ArgumentException(String.Format("Cannot lookup.  Symbol {0} not defined.", sym.Name)));

            return (this._symbols[sym]);
        }

        /// <summary>
        /// Determine whether a symbol is defined.
        /// </summary>
        /// <param name="sym"></param>
        /// <returns></returns>
        public bool IsDefined(SearchSymbol sym)
        {
            return (this._symbols.ContainsKey(sym));
        }
        #endregion // ISymbolTable Interface Methods
    }

} // RSG.SearchLanguage namespace
