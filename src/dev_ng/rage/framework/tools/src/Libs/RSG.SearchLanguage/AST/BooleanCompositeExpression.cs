﻿//---------------------------------------------------------------------------------------------
// <copyright file="BooleanCompositeExpression.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.SearchLanguage.AST
{
    using System;
    using System.Diagnostics;
    using RSG.Base.Language;

    /// <summary>
    /// Boolean Composite Expression; Boolean Term on the left-hand side and
    /// another BooleanTerm or BooleanCompositeExpression on the right.
    /// </summary>
    public class BooleanCompositeExpression : AstNodeBase
    {
        #region Properties
        /// <summary>
        /// Left-hand BooleanTerm.
        /// </summary>
        public BooleanTerm Term
        {
            get;
            private set;
        }

        /// <summary>
        /// Logical operator.
        /// </summary>
        public LogicalOperatorType Operator
        {
            get;
            private set;
        }

        /// <summary>
        /// Right-hand side expression.
        /// </summary>
        public IAstNode Expression
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="position"></param>
        /// <param name="term"></param>
        /// <param name="op"></param>
        /// <param name="rhs"></param>
        public BooleanCompositeExpression(IPosition position, BooleanTerm term, 
            LogicalOperatorType op, IAstNode rhs)
            : base(position)
        {
            Debug.Assert(rhs is BooleanCompositeExpression || rhs is BooleanTerm);
            if (!(rhs is BooleanCompositeExpression || rhs is BooleanTerm))
                throw (new NotSupportedException("Right-hand side must be BooleanCompositeExpression or BooleanTerm."));

            this.Term = term;
            this.Operator = op;
            this.Expression = rhs;
        }
        #endregion // Constructor(s)
    }

} // RSG.SearchLanguage.AST namespace
