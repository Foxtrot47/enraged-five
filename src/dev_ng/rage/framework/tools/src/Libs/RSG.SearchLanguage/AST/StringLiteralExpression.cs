﻿//---------------------------------------------------------------------------------------------
// <copyright file="StringLiteralExpression.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.SearchLanguage.AST
{
    using System;
    using RSG.Base.Language;

    /// <summary>
    /// String literal expression; logical operator for multiple String literals.
    /// </summary>
    /// E.g.
    ///     "carbine" AND "gun"
    ///     "prop" OR "barrel"
    public class StringLiteralExpression : AstNodeBase
    {
        #region Properties
        /// <summary>
        /// Expression argument A (lhs).
        /// </summary>
        public LiteralValue<String> Literal
        { 
            get; 
            private set; 
        }

        /// <summary>
        /// Expression operator.
        /// </summary>
        public LogicalOperatorType Operator
        {
            get;
            private set;
        }

        /// <summary>
        /// Expression argument B (rhs).
        /// </summary>
        public IAstNode Expression
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="position"></param>
        /// <param name="a"></param>
        /// <param name="op"></param>
        /// <param name="b"></param>
        public StringLiteralExpression(IPosition position, LiteralValue<String> a, 
            LogicalOperatorType op, IAstNode b)
            : base(position)
        {
            this.Literal = a;
            this.Operator = op;
            this.Expression = b;
        }
        #endregion // Constructor(s)
    }

} // RSG.SearchLanguage.AST namespace
