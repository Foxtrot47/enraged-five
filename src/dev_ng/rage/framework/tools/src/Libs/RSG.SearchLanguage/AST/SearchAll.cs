﻿//---------------------------------------------------------------------------------------------
// <copyright file="SearchQuery.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.SearchLanguage.AST
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using RSG.Base.Language;

    /// <summary>
    /// 
    /// </summary>
    public class SearchAll : SearchBase
    {
        #region Properties
        /// <summary>
        /// Object identifier.
        /// </summary>
        public Identifier ObjectIdentifier
        {
            get;
            private set;
        }

        /// <summary>
        /// Search spaces.
        /// </summary>
        public IdentifierList SearchSpaces
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="position"></param>
        /// <param name="identifier"></param>
        /// <param name="spaces"></param>
        public SearchAll(IPosition position, Identifier identifier, IdentifierList spaces)
            : base(position)
        {
            this.ObjectIdentifier = identifier;
            this.SearchSpaces = spaces;
        }
        #endregion // Constructor(s)
    }

} // RSG.SearchLanguage.AST namespace
