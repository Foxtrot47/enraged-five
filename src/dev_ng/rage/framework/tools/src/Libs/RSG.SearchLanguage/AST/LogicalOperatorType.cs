﻿//---------------------------------------------------------------------------------------------
// <copyright file="StringOperatorType.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.SearchLanguage.AST
{

    /// <summary>
    /// String literal operator types.
    /// </summary>
    public enum LogicalOperatorType
    {
        Or,
        And
    }

} // RSG.SearchLanguage.AST namespace
