﻿//---------------------------------------------------------------------------------------------
// <copyright file="LiteralList.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.SearchLanguage.AST
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using RSG.Base.Language;

    /// <summary>
    /// 
    /// </summary>
    public class LiteralList :
        AstNodeBase,
        IEnumerable<ILiteralValue>
    {
        #region Member Data
        /// <summary>
        /// Identifier data.
        /// </summary>
        private List<ILiteralValue> _literals;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor; single literal.
        /// </summary>
        /// <param name="position"></param>
        /// <param name="literal"></param>
        public LiteralList(IPosition position, ILiteralValue literal)
            : base(position)
        {
            this._literals = new List<ILiteralValue>();
            this._literals.Add(literal);
        }

        /// <summary>
        /// Constructor; multiple literals.
        /// </summary>
        /// <param name="position"></param>
        /// <param name="identifiers"></param>
        public LiteralList(IPosition position, IEnumerable<ILiteralValue> literals)
            : base(position)
        {
            this._literals = new List<ILiteralValue>(literals);
        }
        #endregion // Constructor(s)

        #region IEnumerable<ILiteralValue> Interface Methods
        public IEnumerator<ILiteralValue> GetEnumerator()
        {
            return (this._literals.GetEnumerator());
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return (this.GetEnumerator());
        }
        #endregion // IEnumerable<IAstNode> Interface Methods
    }

} // RSG.SearchLanguage.AST namespace
