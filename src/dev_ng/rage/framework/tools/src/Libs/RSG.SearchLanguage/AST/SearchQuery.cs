﻿//---------------------------------------------------------------------------------------------
// <copyright file="SearchQuery.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.SearchLanguage.AST
{
    using System;
    using RSG.Base.Language;

    /// <summary>
    /// Search query.
    /// </summary>
    public class SearchQuery : SearchAll
    {
        #region Properties
        /// <summary>
        /// Search where expression.
        /// </summary>
        public IAstNode WhereExpression
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="position"></param>
        /// <param name="identifier"></param>
        /// <param name="spaces"></param>
        /// <param name="where"></param>
        public SearchQuery(IPosition position, Identifier identifier, IdentifierList spaces,
            IAstNode where)
            : base(position, identifier, spaces)
        {
            if (null == where)
                throw (new ArgumentNullException("where"));

            this.WhereExpression = where;
        }
        #endregion // Constructor(s)
    }

} // RSG.Search.AST namespace
