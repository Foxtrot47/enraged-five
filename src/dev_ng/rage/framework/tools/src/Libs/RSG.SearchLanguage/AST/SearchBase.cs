﻿//---------------------------------------------------------------------------------------------
// <copyright file="SearchBase.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.SearchLanguage.AST
{
    using RSG.Base.Language;

    /// <summary>
    /// Top-level Search Query AST abstract base node; parsing will return a concrete
    /// <see cref="SearchStringLiteral"/> (literal string) or <see cref="SearchQuery"/>.
    /// </summary>
    public abstract class SearchBase : AstNodeBase
    {
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="position"></param>
        public SearchBase(IPosition position)
            : base(position)
        {
        }
        #endregion // Constructor(s)
    }

} // RSG.Search.AST namespace
