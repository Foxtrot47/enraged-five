﻿//---------------------------------------------------------------------------------------------
// <copyright file="SearchLanguage.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.SearchLanguage
{
    using System;
    using System.Diagnostics;
    using Antlr4.Runtime;
    using Antlr4.Runtime.Tree;
    using RSG.Base.Language;
    using RSG.Base.Logging.Universal;
    using RSG.Interop.Antlr;
    using RSG.SearchLanguage.Antlr;

    /// <summary>
    /// Search Language Parser; allows user-code to be independent of the parser generator
    /// implementation and only deal with IAstNode interpretation.
    /// </summary>
    public class SearchLanguage
    {
        #region Controller Methods
        /// <summary>
        /// Parse a String (no error handler).
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public SearchParserResult Parse(String expression)
        {
            AntlrInputStream input = new AntlrInputStream(expression);
            return (Parse(input, null));
        }

        /// <summary>
        /// Parse a String with errors being output to log.
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="log"></param>
        /// <returns></returns>
        public SearchParserResult Parse(String expression, IUniversalLog log)
        {
            LogErrorHandler handler = new LogErrorHandler(log);
            return (Parse(expression, handler));
        }

        /// <summary>
        /// Parse a String using a custom error handler.
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="handler"></param>
        /// <returns></returns>
        public SearchParserResult Parse(String expression, IErrorHandler handler)
        {
            AntlrInputStream input = new AntlrInputStream(expression);
            return (Parse(input, handler));
        }

        /// <summary>
        /// Parse a Stream.
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public SearchParserResult Parse(System.IO.Stream stream)
        {
            AntlrInputStream input = new AntlrInputStream(stream);
            return (Parse(input, null));
        }

        /// <summary>
        /// Parse a stream; logging results to specified log.
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="log"></param>
        /// <returns></returns>
        public SearchParserResult Parse(System.IO.Stream stream, IUniversalLog log)
        {
            LogErrorHandler handler = new LogErrorHandler(log);
            AntlrInputStream input = new AntlrInputStream(stream);
            return (Parse(input, null));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="handler"></param>
        /// <returns></returns>
        public SearchParserResult Parse(System.IO.Stream stream, IErrorHandler handler)
        {
            AntlrInputStream input = new AntlrInputStream(stream);
            return (Parse(input, handler));
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Parse an AntlrInputStream; returning top-level AST node.
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="handler"></param>
        /// <returns></returns>
        private SearchParserResult Parse(AntlrInputStream stream, IErrorHandler handler)
        {
            SearchLanguageLexer lexer = new SearchLanguageLexer(stream);
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            SearchLanguageParser parser = new SearchLanguageParser(tokens);
            parser.BuildParseTree = false;
            
            if (null != handler)
            {
                // Remove default ANTLR handlers.
                parser.RemoveErrorListeners();
                
                AntlrErrorHandlerWrapper handlerWrapper = new AntlrErrorHandlerWrapper(handler);
                parser.AddErrorListener(handlerWrapper);
            }

            // Add a storage error handler so we can push all errors into our
            // resultant object.
            StoreErrorHandler storeErrorHandler = new StoreErrorHandler();
            parser.AddErrorListener(new AntlrErrorHandlerWrapper(storeErrorHandler));

            // Parse and construct our resultant object.
            AST.SearchBase ast = parser.query().q;
            SearchParserResult result = new SearchParserResult(ast, storeErrorHandler.SyntaxErrors);
                        
            return (result);
        }
        #endregion // Private Methods
    }

} // RSG.SearchLanguage
