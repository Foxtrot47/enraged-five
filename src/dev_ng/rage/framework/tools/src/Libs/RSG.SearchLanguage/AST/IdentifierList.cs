﻿//---------------------------------------------------------------------------------------------
// <copyright file="IdentifierList.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.SearchLanguage.AST
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using RSG.Base.Language;

    /// <summary>
    /// 
    /// </summary>
    public class IdentifierList : 
        AstNodeBase,
        IEnumerable<Identifier>
    {
        #region Member Data
        /// <summary>
        /// Identifier data.
        /// </summary>
        private List<Identifier> _identifiers;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor; single identifier.
        /// </summary>
        /// <param name="position"></param>
        /// <param name="identifier"></param>
        public IdentifierList(IPosition position, Identifier identifier)
            : base(position)
        {
            this._identifiers = new List<Identifier>();
            this._identifiers.Add(identifier);
        }

        /// <summary>
        /// Constructor; multiple identifiers.
        /// </summary>
        /// <param name="position"></param>
        /// <param name="identifiers"></param>
        public IdentifierList(IPosition position, IEnumerable<Identifier> identifiers)
            : base(position)
        {
            this._identifiers = new List<Identifier>(identifiers);
        }
        #endregion // Constructor(s)

        #region IEnumerable<Identifier> Interface Methods
        public IEnumerator<Identifier> GetEnumerator()
        {
            return (this._identifiers.GetEnumerator());
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return (this.GetEnumerator());
        }
        #endregion // IEnumerable<IAstNode> Interface Methods
    }

} // RSG.SearchLanguage.AST namespace
