﻿//---------------------------------------------------------------------------------------------
// <copyright file="AstNodeBase.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.SearchLanguage.AST
{
    using RSG.Base.Language;

    /// <summary>
    /// 
    /// </summary>
    public abstract class AstNodeBase : IAstNode
    {
        #region Properties
        /// <summary>
        /// String positional information for this AST node (and children).
        /// </summary>
        public IPosition Position { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="position"></param>
        public AstNodeBase(IPosition position)
        {
            this.Position = position;
        }
        #endregion // Constructor(s)
    }

} // RSG.Search.AST namespace
