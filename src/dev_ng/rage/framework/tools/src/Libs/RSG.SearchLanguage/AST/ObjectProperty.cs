﻿//---------------------------------------------------------------------------------------------
// <copyright file="ObjectProperty.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.SearchLanguage.AST
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using RSG.Base.Language;
    
    /// <summary>
    /// 
    /// </summary>
    public class ObjectProperty : AstNodeBase
    {
        #region Properties
        /// <summary>
        /// Object symbol.
        /// </summary>
        public SearchSymbol Object
        {
            get;
            private set;
        }

        /// <summary>
        /// Property symbols.
        /// </summary>
        public SearchSymbol Property
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="position"></param>
        /// <param name="obj"></param>
        /// <param name="property"></param>
        public ObjectProperty(IPosition position, SearchSymbol obj, 
            SearchSymbol property)
            : base(position)
        {
            if (null == property)
                throw (new ArgumentNullException("property"));

            Debug.Assert(obj.Type.Equals(SearchSymbolType.Object));
            this.Object = obj;
            this.Property = property;
        }
        #endregion // Constructor(s)

        #region Object Overridden Methods
        /// <summary>
        /// Return String representation.
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return (String.Format("{0}.{1}", this.Object.Name, this.Property));
        }
        #endregion // Object Overridden Methods
    }

} // RSG.SearchLanguage.AST namespace
