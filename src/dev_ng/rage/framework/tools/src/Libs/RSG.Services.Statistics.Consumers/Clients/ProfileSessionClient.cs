﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProfileSessionClient.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using RSG.Services.Configuration;
using RSG.Services.Configuration.ServiceModel;
using RSG.Services.Statistics.Contracts.DataContracts;
using RSG.Services.Statistics.Contracts.ServiceContracts;

namespace RSG.Services.Statistics.Consumers.Clients
{
    /// <summary>
    /// WCF client for communicating with a service that implements the
    /// <see cref="IProfileSessionService"/> service contract.
    /// </summary>
    internal class ProfileSessionClient : ConfigClient<IProfileSessionService>, IProfileSessionService
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ApplicationSessionClient"/>
        /// using the specified server configuration object and uri scheme.
        /// </summary>
        internal ProfileSessionClient(IServer server, string uriScheme)
            : base(server, uriScheme)
        {
        }
        #endregion

        #region IProfileSessionService Methods
        /// <summary>
        /// Tracks a new profile session.
        /// </summary>
        /// <param name="profileSessionDto"></param>
        public void AddSession(ProfileSession profileSessionDto)
        {
            base.Channel.AddSession(profileSessionDto);
        }
        #endregion
    }
}
