﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProfileSessionContext.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using RSG.Base.Logging;
using RSG.Services.Configuration;
using RSG.Services.Configuration.ServiceModel;
using RSG.Services.Statistics.Consumers.Clients;
using RSG.Services.Statistics.Contracts.DataContracts;

namespace RSG.Services.Statistics.Consumers
{
    /// <summary>
    /// Used for keeping track of a single profile session.  Automatically attaches a
    /// custom log target to the specified log to track any profile start/end samples
    /// which are uploaded to the statistics server when the object is disposed.
    /// </summary>
    public class ProfileSessionContext<T> : IDisposable where T : ProfileSession, new()
    {
        #region Fields
        /// <summary>
        /// Flag indicating whether this object has already been disposed of.
        /// </summary>
        private bool _disposed;

        /// <summary>
        /// Reference to the log object that we are gathering profile samples from.
        /// </summary>
        private readonly ILog _log;

        /// <summary>
        /// The dto object containing all the profile information that will be sent to
        /// the stats server.
        /// </summary>
        private readonly T _profileSession;

        /// <summary>
        /// The log target for capturing the profile messages.
        /// </summary>
        private readonly ProfileSessionTarget _profileSessionTarget;

        /// <summary>
        /// The statistics server configuration object that contains information about
        /// the server to send the profile session information to.
        /// </summary>
        private readonly IServer _statisticsServer;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ProfileSessionContext"/> class
        /// using the specified log and statistics server configuration object.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="statisticsServer"></param>
        /// <param name="name"></param>
        /// <param name="applicationSessionGuid"></param>
        /// <param name="parentProfileSessionGuid"></param>
        public ProfileSessionContext(ILog log, IServer statisticsServer, string name, Guid applicationSessionGuid, Guid? parentProfileSessionGuid = null)
        {
            _log = log;
            _statisticsServer = statisticsServer;
            _profileSession = new T();
            _profileSession.ApplicationSessionGuid = applicationSessionGuid;
            _profileSession.Name = name;
            _profileSession.ParentSessionGuid = parentProfileSessionGuid;
            _profileSession.Samples = new List<ProfileSample>();
            _profileSession.SessionGuid = Guid.NewGuid();
            _profileSession.StartTime = DateTime.UtcNow;

            _profileSessionTarget = new ProfileSessionTarget(_profileSession);
            _profileSessionTarget.Connect(_log);
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets a value indicating whether this object has already been disposed.
        /// </summary>
        protected bool Disposed
        {
            get { return _disposed; }
        }

        /// <summary>
        /// Gets a reference to the underlying profile session dto objects.
        /// </summary>
        protected T ProfileSession
        {
            get { return _profileSession; }
        }
        #endregion

        #region IDisposable Methods
        /// <summary>
        /// Disposes of this profile session context.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }

        /// <summary>
        /// Closes the profile session and attempts to send it to the statistics server. 
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }

            if (disposing)
            {
                _profileSessionTarget.Disconnect(_log);
                _profileSessionTarget.Shutdown();
                _profileSession.EndTime = DateTime.UtcNow;

                ProfileSessionClient client = null;
                try
                {
                    client = new ProfileSessionClient(_statisticsServer, Uri.UriSchemeNetTcp);
                    client.AddSession(_profileSession);
                }
                catch (Exception ex)
                {
                    _log.ToolException(ex, "Unable to send profile session to the statistics server.");
                }
                finally
                {
                    if (client != null)
                    {
                        client.CloseOrAbort();
                    }
                }
            }

            _disposed = true;
        }
        #endregion
    }
}
