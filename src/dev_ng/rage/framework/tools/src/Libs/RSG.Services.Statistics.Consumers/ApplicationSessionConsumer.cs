﻿//---------------------------------------------------------------------------------------------
// <copyright file="ApplicationSessionConsumer.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.SystemSpecs;
using RSG.Configuration;
using RSG.Services.Configuration;
using RSG.Services.Configuration.ServiceModel;
using RSG.Services.Statistics.Consumers.Clients;
using RSG.Services.Statistics.Contracts.DataContracts;

namespace RSG.Services.Statistics.Consumers
{
    /// <summary>
    /// Consumer for creating/tracking application sessions.
    /// </summary>
    public class ApplicationSessionConsumer
    {
        #region Fields
        /// <summary>
        /// Server configuration object that contains the details of the server we should
        /// connect to.
        /// </summary>
        private readonly IServer _server;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ApplicationSessionConsumer"/>
        /// class using the specified server configuration object.
        /// </summary>
        /// <param name="server"></param>
        public ApplicationSessionConsumer(IServer server)
        {
            _server = server;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Tracks a new application session.
        /// </summary>
        /// <param name="sessionGuid"></param>
        /// <param name="parentSessionGuid"></param>
        /// <param name="applicationName"></param>
        /// <param name="version"></param>
        /// <param name="launchLocation"></param>
        /// <param name="timestamp"></param>
        /// <param name="project"></param>
        public void TrackApplicationSession(
            Guid sessionGuid,
            Guid? parentSessionGuid,
            string applicationName,
            string version,
            string launchLocation,
            DateTime timestamp,
            IProject project)
        {
            ApplicationSession session = new ApplicationSession();
            session.SessionGuid = sessionGuid;
            session.ParentSessionGuid = parentSessionGuid;
            session.OpenTimestamp = timestamp.ToUniversalTime();

            Application application = new Application();
            application.ApplicationName = applicationName;
            application.Version = version;
            application.Project = project.Name;
            application.ToolsRoot = project.Config.ToolsRootDirectory;
            application.LaunchLocation = launchLocation;

            User user = new User();
            user.UserName = Environment.UserName;
            user.Studio = project.Config.CurrentStudio.Name;

            Machine machine = new Machine();
            machine.MachineName = Environment.MachineName;
            machine.ProcessorName = SystemInfo.Processor.Name;
            machine.LogicalProcessorCount = SystemInfo.Processor.NumberOfLogicalProcessors;
            machine.CoreCount = SystemInfo.Processor.NumberOfCores;
            machine.MemoryCapacity = SystemInfo.Memory.Capacity;
            machine.GraphicsCardName = SystemInfo.GraphicsCard.Name;

            session.Application = application;
            session.User = user;
            session.Machine = machine;

            ApplicationSessionClient client = null;
            try
            {
                client = new ApplicationSessionClient(_server, Uri.UriSchemeNetTcp);
                client.AddSession(session);
            }
            finally
            {
                if (client != null)
                {
                    client.CloseOrAbort();
                }
            }
        }
        
        /// <summary>
        /// Tracks an application exit.
        /// </summary>
        /// <param name="sessionGuid"></param>
        /// <param name="exitCode"></param>
        /// <param name="timestamp"></param>
        public void TrackApplicationExit(
            Guid sessionGuid,
            int exitCode,
            DateTime timestamp)
        {
            ApplicationSessionExit exitInfo = new ApplicationSessionExit();
            exitInfo.CloseTimestamp = DateTime.UtcNow;
            exitInfo.ExitCode = exitCode;
            exitInfo.SessionGuid = sessionGuid;

            ApplicationSessionClient client = null;
            try
            {
                client = new ApplicationSessionClient(_server, Uri.UriSchemeNetTcp);
                client.SetSessionExitInfo(exitInfo);
            }
            finally
            {
                if (client != null)
                {
                    client.CloseOrAbort();
                }
            }
        }
        #endregion
    }
}
