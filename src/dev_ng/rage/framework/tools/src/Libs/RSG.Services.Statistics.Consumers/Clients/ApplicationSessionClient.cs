﻿//---------------------------------------------------------------------------------------------
// <copyright file="ApplicationSessionClient.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using RSG.Services.Configuration;
using RSG.Services.Configuration.ServiceModel;
using RSG.Services.Statistics.Contracts.DataContracts;
using RSG.Services.Statistics.Contracts.ServiceContracts;

namespace RSG.Services.Statistics.Consumers.Clients
{
    /// <summary>
    /// WCF client for communicating with a service that implements the
    /// <see cref="IApplicationSessionService"/> service contract.
    /// </summary>
    internal class ApplicationSessionClient : ConfigClient<IApplicationSessionService>, IApplicationSessionService
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ApplicationSessionClient"/>
        /// using the specified server configuration object and uri scheme.
        /// </summary>
        internal ApplicationSessionClient(IServer server, string uriScheme)
            : base(server, uriScheme)
        {
        }
        #endregion

        #region IApplicationSessionService Methods
        /// <summary>
        /// Tracks a new application session.
        /// </summary>
        /// <param name="applicationSessionDto"></param>
        public void AddSession(ApplicationSession applicationSessionDto)
        {
            base.Channel.AddSession(applicationSessionDto);
        }

        /// <summary>
        /// Updates an application session with exit information.
        /// </summary>
        /// <param name="applicationSessionExitDto"></param>
        public void SetSessionExitInfo(ApplicationSessionExit applicationSessionExitDto)
        {
            base.Channel.SetSessionExitInfo(applicationSessionExitDto);
        }
        #endregion
    }
}
