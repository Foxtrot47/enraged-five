﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProfileSessionTarget.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System.Collections.Generic;
using System.Linq;
using RSG.Base.Logging;
using RSG.Services.Statistics.Contracts.DataContracts;

namespace RSG.Services.Statistics.Consumers
{
    /// <summary>
    /// 
    /// </summary>
    internal class ProfileSessionTarget : ThreadedLogTargetBase
    {
        #region Fields
        /// <summary>
        /// The stack of currently active profile samples.
        /// </summary>
        private readonly Stack<ProfileSample> _profileSampleStack;

        /// <summary>
        /// Reference to the profile session that the profile samples will be added to.
        /// </summary>
        private readonly ProfileSession _profileSession;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ProfileSessionTarget"/> class.
        /// </summary>
        internal ProfileSessionTarget(ProfileSession profileSession)
        {
            _profileSampleStack = new Stack<ProfileSample>();
            _profileSession = profileSession;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Abstract method that handles logging (called within thread).
        /// </summary>
        /// <param name="args"></param>
        protected override void LogMessage(LogMessageEventArgs args)
        {
            // We're only interested in profile messages.
            if (args.Level == LogLevel.Profile)
            {
                ProfileSample sample = new ProfileSample();
                sample.ChildSamples = new List<ProfileSample>();
                sample.Metric = args.Message;
                sample.StartTime = args.Timestamp.ToUniversalTime();

                if (_profileSampleStack.Any())
                {
                    ProfileSample currentTop = _profileSampleStack.Peek();
                    currentTop.ChildSamples.Add(sample);
                }

                _profileSampleStack.Push(sample);
            }
            else if (args.Level == LogLevel.ProfileEnd)
            {
                ProfileSample sample = _profileSampleStack.Pop();
                sample.EndTime = args.Timestamp.ToUniversalTime();

                if (!_profileSampleStack.Any())
                {
                    _profileSession.Samples.Add(sample);
                }
            }
            
        }

        /// <summary>
        /// 
        /// </summary>
        public override void Flush()
        {
            // Nothing to do.
        }
        #endregion
    }
}
