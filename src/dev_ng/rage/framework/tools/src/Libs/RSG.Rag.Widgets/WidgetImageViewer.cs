﻿using RSG.Rag.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    [WidgetGuidAttribute('i', 'm', 'g', 'v')]
    public class WidgetImageViewer : Widget
    {
        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        private String _filename;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public WidgetImageViewer(uint id, IWidgetResolver resolver, IPacketDispatcher dispatcher)
            : base(id, resolver, dispatcher)
        {
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public String Filename
        {
            get { return _filename; }
            set
            {
                if (SetProperty(ref _filename, value) && !ProcessingMessage)
                {
                    SendChangedMessage();
                }
            }
        }
        #endregion // Properties

        #region Message Processing
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void ProcessMessageCore(RemotePacketEventArgs e)
        {
            switch ((BankCommand)e.Packet.Command)
            {
                case BankCommand.Changed:
                    ProcessChangedMessage(e);
                    break;

                default:
                    base.ProcessMessageCore(e);
                    break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void ProcessChangedMessage(RemotePacketEventArgs e)
        {
            string newFilename = e.Packet.ReadString();
            bool filenameChanged = newFilename != Filename;
            Filename = newFilename;

            // Force through a property changed event if the filename didn't change
            // so that the image in the UI gets updated.
            if (!filenameChanged)
            {
                NotifyPropertyChanged("Filename");
            }
        }
        #endregion // Message Processing

        #region Message Dispatching
        /// <summary>
        /// 
        /// </summary>
        private void SendChangedMessage()
        {
            RemotePacketBuilder builder = new RemotePacketBuilder((ushort)BankCommand.Changed, this.Guid.ToInteger(), Id);
            builder.Write(_filename);
            Dispatcher.SendPacketAsync(builder.ToRemotePacket());
        }
        #endregion // Message Dispatching
    } // WidgetImageViewer
}
