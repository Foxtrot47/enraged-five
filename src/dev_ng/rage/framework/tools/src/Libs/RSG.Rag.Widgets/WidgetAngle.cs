﻿using RSG.Base;
using RSG.Rag.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    //[WidgetGuidAttribute('a', 'n', 'g', 'l')]
    public class WidgetAngle : Widget
    {
        #region Enums
        /// <summary>
        /// 
        /// </summary>
        public enum AngleType
        {
            /// <summary>
            /// 
            /// </summary>
            Degree,

            /// <summary>
            /// 
            /// </summary>
            Fraction,
            
            /// <summary>
            /// 
            /// </summary>
            Radian,

            /// <summary>
            /// 
            /// </summary>
            Vector2
        }
        #endregion // Enums

        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        private AngleType _angleType;

        /// <summary>
        /// 
        /// </summary>
        private float[] _value;

        /// <summary>
        /// 
        /// </summary>
        private float[] _originalValue;

        /// <summary>
        /// 
        /// </summary>
        private float _minValue;

        /// <summary>
        /// 
        /// </summary>
        private float _maxValue;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public WidgetAngle(uint id, IWidgetResolver resolver, IPacketDispatcher dispatcher)
            : base(id, resolver, dispatcher)
        {
        }
        #endregion // Constructor(s)
        
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public AngleType Type
        {
            get { return _angleType; }
        }

        /// <summary>
        /// 
        /// </summary>
        public float[] Value
        {
            get { return _value; }
            set
            {
                if (SetProperty(ref _value, value, StructuralEqualityComparer<float[]>.Default) && !ProcessingMessage)
                {
                    SendChangedMessage();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public float[] OriginalValue
        {
            get { return _originalValue; }
        }

        /// <summary>
        /// 
        /// </summary>
        public float MinValue
        {
            get { return _minValue; }
        }

        /// <summary>
        /// 
        /// </summary>
        public float MaxValue
        {
            get { return _maxValue; }
        }
        #endregion // Properties

        #region Message Processing
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void ProcessMessageCore(RemotePacketEventArgs e)
        {
            switch ((BankCommand)e.Packet.Command)
            {
                case BankCommand.Changed:
                    ProcessChangedMessage(e);
                    break;

                default:
                    base.ProcessMessageCore(e);
                    break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        public override bool ProcessCreateMessage(RemotePacketEventArgs e)
        {
            if (!base.ProcessCreateMessage(e))
            {
                return false;
            }

            _angleType = (AngleType)e.Packet.ReadInt32();
            _minValue = e.Packet.ReadFloat();
            _maxValue = e.Packet.ReadFloat();
            _value = new float[2];
            _value[0] = e.Packet.ReadFloat();
            if (_angleType == AngleType.Vector2)
            {
                _value[1] = e.Packet.ReadFloat();
            }
            _originalValue = (float[])_value.Clone();
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void ProcessChangedMessage(RemotePacketEventArgs e)
        {
            float[] newValue = new float[2];
            newValue[0] = e.Packet.ReadFloat();
            if (_angleType == AngleType.Vector2)
            {
                newValue[1] = e.Packet.ReadFloat();
            }
            Value = newValue;
        }
        #endregion // Message Processing

        #region Message Dispatching
        /// <summary>
        /// 
        /// </summary>
        private void SendChangedMessage()
        {
            RemotePacketBuilder builder = new RemotePacketBuilder((ushort)BankCommand.Changed, this.Guid.ToInteger(), Id);
            builder.Write(_value[0]);
            if (_angleType == AngleType.Vector2)
            {
                builder.Write(_value[1]);
            }
            Dispatcher.SendPacket(builder.ToRemotePacket());
        }
        #endregion // Message Dispatching
    } // WidgetAngle
}
