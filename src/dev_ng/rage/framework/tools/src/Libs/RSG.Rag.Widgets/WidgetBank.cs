﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Rag.Core;

namespace RSG.Rag.Widgets
{
    /// <summary>
    /// This class represents a Bank, which is a top-level collection of Widgets.
    /// </summary>
    [WidgetGuidAttribute('b', 'a', 'n', 'k')]
    public class WidgetBank : WidgetGroupBase
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public WidgetBank(uint id, IWidgetResolver resolver, IPacketDispatcher dispatcher)
            : base(id, resolver, dispatcher)
        {
        }
        #endregion // Constructor(s)
    } // WidgetBank
}
