﻿using RSG.Rag.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Base;
using System.Diagnostics;

namespace RSG.Rag.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    //[WidgetGuidAttribute('d', 'a', 't', 'a')]
    public class WidgetData : Widget
    {
        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        private byte[] _data;

        /// <summary>
        /// 
        /// </summary>
        private ushort _maxBufferSize;

        /// <summary>
        /// Temporary buffer used for receiving the chunked widget data.
        /// </summary>
        private byte[] _changeBuffer;

        /// <summary>
        /// 
        /// </summary>
        private bool _showDataView;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public WidgetData(uint id, IWidgetResolver resolver, IPacketDispatcher dispatcher)
            : base(id, resolver, dispatcher)
        {
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public byte[] Data
        {
            get { return _data; }
            set
            {
                if (SetProperty(ref _data, value, StructuralEqualityComparer<byte[]>.Default) && !ProcessingMessage)
                {
                    SendChangedMessage();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public ushort MaxBufferSize
        {
            get { return _maxBufferSize; }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool ShowDataView
        {
            get { return _showDataView; }
        }
        #endregion // Properties

        #region Message Processing
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void ProcessMessageCore(RemotePacketEventArgs e)
        {
            switch ((BankCommand)e.Packet.Command)
            {
                case BankCommand.Changed:
                    ProcessChangedMessage(e);
                    break;

                default:
                    base.ProcessMessageCore(e);
                    break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        public override bool ProcessCreateMessage(RemotePacketEventArgs e)
        {
            if (!base.ProcessCreateMessage(e))
            {
                return false;
            }

            _maxBufferSize = e.Packet.ReadUInt16();
            _showDataView = e.Packet.ReadBoolean();
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void ProcessChangedMessage(RemotePacketEventArgs e)
        {
            ushort length = e.Packet.ReadUInt16();
            ushort count = e.Packet.ReadUInt16();
            ushort offset = e.Packet.ReadUInt16();
            ushort remainingPackets = e.Packet.ReadUInt16();

            if (length == 0)
            {
                Debug.Assert(remainingPackets == 0, "0 length data sent when there are remaining packets to be sent.");
                Data = null;
            }
            else if (_changeBuffer == null)
            {
                _changeBuffer = new byte[length];
            }

            if (_changeBuffer != null)
            {
                Debug.Assert(offset + count <= length, "Too much data sent.");
                Debug.Assert(length == _changeBuffer.Length, "Data length changed between update packets.");

                for (int i = offset; i < offset + count; ++i)
                {
                    _changeBuffer[i] = e.Packet.ReadByte();
                }

                // Only set the new data once everything has been received.
                if (remainingPackets == 0)
                {
                    Data = _changeBuffer;
                }
            }
        }
        #endregion // Message Processing

        #region Message Dispatching
        /// <summary>
        /// 
        /// </summary>
        private void SendChangedMessage()
        {
            if (Data == null)
            {
                RemotePacketBuilder builder = new RemotePacketBuilder((ushort)BankCommand.Changed, this.Guid.ToInteger(), Id);
                builder.Write((ushort)0);
                builder.Write((ushort)0);
                builder.Write((ushort)0);
                builder.Write((ushort)0);
                Dispatcher.SendPacketAsync(builder.ToRemotePacket());
            }
            else
            {
                // 8 = total length, packet length, offset, remaining packets
                const short payloadSize = RemotePacket.MAX_DATA_SIZE - 8;
                ushort offset = 0;

                // Send the data across in "payload" sized chunks.
                int packetsRemaining = (int)Math.Ceiling((decimal)Data.Length / payloadSize) - 1;
                while (packetsRemaining >= 0)
                {
                    ushort packetDataLength = (ushort)(Data.Length - offset > payloadSize ? payloadSize : Data.Length - offset);

                    RemotePacketBuilder builder = new RemotePacketBuilder((ushort)BankCommand.Changed, this.Guid.ToInteger(), Id);
                    builder.Write((ushort)Data.Length);
                    builder.Write(packetDataLength);
                    builder.Write(offset);
                    builder.Write((ushort)packetsRemaining);

                    for (int i = 0; i < packetDataLength; ++i)
                    {
                        builder.Write(Data[offset + i]);
                    }

                    Dispatcher.SendPacketAsync(builder.ToRemotePacket());

                    offset += packetDataLength;
                    --packetsRemaining;
                }
            }
        }
        #endregion // Message Dispatching
    } // WidgetData
}
