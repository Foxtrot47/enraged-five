﻿using RSG.Rag.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    [WidgetGuidAttribute('c', 'o', 'u', '3')]
    public class WidgetComboUInt32 : WidgetCombo<UInt32>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public WidgetComboUInt32(uint id, IWidgetResolver resolver, IPacketDispatcher dispatcher)
            : base(id, resolver, dispatcher)
        {
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="packet"></param>
        /// <returns></returns>
        protected override UInt32 ReadValue(RemotePacket packet)
        {
            return packet.ReadUInt32();
        }

        /// <summary>
        /// Method for writing a value to the remote packet builder.
        /// </summary>
        /// <param name="builder">Reference to the remote packet builder.</param>
        /// <param name="value">Value to write.</param>
        protected override void WriteValue(RemotePacketBuilder builder, UInt32 value)
        {
            builder.Write(value);
        }
        #endregion // Overrides
    } // WidgetComboUInt32
}
