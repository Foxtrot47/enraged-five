﻿using RSG.Rag.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.Widgets
{
    /// <summary>
    /// Widget class that acts as a sink for incoming messages for widgets that
    /// RAG doesn't know about.
    /// </summary>
    [WidgetGuidAttribute('u', 'k', 'w', 'n')]
    public class WidgetUnknown : Widget
    {
        #region Member Data
        /// <summary>
        /// The widget guid that this unknown is being a surrogate for.
        /// </summary>
        private WidgetGuid _underlyingGuid;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public WidgetUnknown(uint id, IWidgetResolver resolver, IPacketDispatcher dispatcher, WidgetGuid underlyingGuid)
            : base(id, resolver, dispatcher)
        {
            _underlyingGuid = underlyingGuid;
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// Guid for this widget.
        /// </summary>
        public override WidgetGuid Guid
        {
            get { return _underlyingGuid; }
        }
        #endregion // Properties

        #region Message Processing
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void ProcessMessageCore(RemotePacketEventArgs e)
        {
            // Ignore all incoming messages (we don't know how to deal with them).
            e.Packet.ReadToEnd();
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        public override bool ProcessCreateMessage(RemotePacketEventArgs e)
        {
            if (!base.ProcessCreateMessage(e))
            {
                return false;
            }

            e.Packet.ReadToEnd();
            return true;
        }
        #endregion // Message Processing
    } // WidgetUnknown
}
