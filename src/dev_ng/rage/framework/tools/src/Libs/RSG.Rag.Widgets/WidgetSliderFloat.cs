﻿using RSG.Rag.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    [WidgetGuidAttribute('s', 'l', 'f', 'l')]
    public class WidgetSliderFloat : WidgetSlider<float>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public WidgetSliderFloat(uint id, IWidgetResolver resolver, IPacketDispatcher dispatcher)
            : base(id, resolver, dispatcher)
        {
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="packet"></param>
        /// <returns></returns>
        protected override float ReadValue(RemotePacket packet)
        {
            return packet.ReadFloat();
        }

        /// <summary>
        /// Method for writing a value to the remote packet builder.
        /// </summary>
        /// <param name="builder">Reference to the remote packet builder.</param>
        /// <param name="value">Value to write.</param>
        protected override void WriteValue(RemotePacketBuilder builder, float value)
        {
            builder.Write(value);
        }
        #endregion // Overrides
    } // WidgetSliderFloat
}
