﻿using RSG.Rag.Core;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    [WidgetGuidAttribute('l', 'i', 's', 't')]
    public class WidgetList : Widget
    {
        #region Enums
        /// <summary>
        /// 
        /// </summary>
        public enum ColumnType
        {
            /// <summary>
            /// 
            /// </summary>
            String,

            /// <summary>
            /// 
            /// </summary>
            Integer,

            /// <summary>
            /// 
            /// </summary>
            Float
        }
        #endregion // Enums

        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        private readonly IList<String> _columnHeaders = new List<String>();

        /// <summary>
        /// 
        /// </summary>
        private readonly IReadOnlyList<String> _readOnlyColumnHeaders;

        /// <summary>
        /// 
        /// </summary>
        private readonly IList<ColumnType> _columnTypes = new List<ColumnType>();

        /// <summary>
        /// 
        /// </summary>
        private readonly IReadOnlyList<ColumnType> _readOnlyColumnTypes;

        /// <summary>
        /// 
        /// </summary>
        private readonly IDictionary<int, IList<object>> _values = new Dictionary<int, IList<object>>();

        /// <summary>
        /// 
        /// </summary>
        private readonly IReadOnlyDictionary<int, IList<object>> _readOnlyValues;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public WidgetList(uint id, IWidgetResolver resolver, IPacketDispatcher dispatcher)
            : base(id, resolver, dispatcher)
        {
            _readOnlyColumnHeaders = new ReadOnlyCollection<String>(_columnHeaders);
            _readOnlyColumnTypes = new ReadOnlyCollection<ColumnType>(_columnTypes);
            _readOnlyValues = new ReadOnlyDictionary<int, IList<object>>(_values);
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public IReadOnlyList<String> ColumnHeaders
        {
            get { return _readOnlyColumnHeaders; }
        }

        /// <summary>
        /// 
        /// </summary>
        public IReadOnlyList<ColumnType> ColumnTypes
        {
            get { return _readOnlyColumnTypes; }
        }

        /// <summary>
        /// 
        /// </summary>
        public IReadOnlyDictionary<int, IList<object>> Values
        {
            get { return _readOnlyValues; }
        }
        #endregion // Properties

        #region Message Handling
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        public override void ProcessMessageCore(RemotePacketEventArgs e)
        {
            switch ((BankCommand)e.Packet.Command)
            {
                case BankCommand.User0:
                    ProcessAddColumnMessage(e);
                    break;

                case BankCommand.User1:
                    ProcessSetValueMessage(e);
                    break;

                case BankCommand.User2:
                    ProcessRemoveItemMessage(e);
                    break;

                // Messages from the UI to the game.
                case BankCommand.User3:
                case BankCommand.User4:
                case BankCommand.User5:
                    break;

                default:
                    base.ProcessMessageCore(e);
                    break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void ProcessAddColumnMessage(RemotePacketEventArgs e)
        {
            int column = e.Packet.ReadInt32();
            String heading = e.Packet.ReadString();
            ColumnType type = (ColumnType)e.Packet.ReadUInt32();

            if (column >= _columnHeaders.Count)
            {
                for (int c = _columnHeaders.Count; c <= column; ++c)
                {

                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void ProcessSetValueMessage(RemotePacketEventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void ProcessRemoveItemMessage(RemotePacketEventArgs e)
        {
        }
        #endregion // Message Handling
    } // WidgetList
}
