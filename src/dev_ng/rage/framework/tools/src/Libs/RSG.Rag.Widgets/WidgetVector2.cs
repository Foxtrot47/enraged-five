﻿using RSG.Rag.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    [WidgetGuidAttribute('v', 'e', 'c', '2')]
    public class WidgetVector2 : WidgetVector
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public WidgetVector2(uint id, IWidgetResolver resolver, IPacketDispatcher dispatcher)
            : base(id, resolver, dispatcher)
        {
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public override int NumComponents
        {
            get { return 2; }
        }
        #endregion // Properties
    } // WidgetVector2
}
