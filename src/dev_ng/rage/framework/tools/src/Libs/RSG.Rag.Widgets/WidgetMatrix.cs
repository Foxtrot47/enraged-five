﻿using RSG.Base;
using RSG.Rag.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class WidgetMatrix : Widget
    {
        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        private float[][] _value;

        /// <summary>
        /// 
        /// </summary>
        private float[][] _originalValue;

        /// <summary>
        /// 
        /// </summary>
        private float _minValue;

        /// <summary>
        /// 
        /// </summary>
        private float _maxValue;

        /// <summary>
        /// 
        /// </summary>
        private float _step;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        protected WidgetMatrix(uint id, IWidgetResolver resolver, IPacketDispatcher dispatcher)
            : base(id, resolver, dispatcher)
        {
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// Number of rows this matrix contains.
        /// </summary>
        public abstract int NumRows { get; }

        /// <summary>
        /// Number of columns this matrix contains.
        /// </summary>
        public abstract int NumColumns { get; }

        /// <summary>
        /// 
        /// </summary>
        public float[][] Value
        {
            get { return _value; }
            set
            {
                if (SetProperty(ref _value, value, StructuralEqualityComparer<float[][]>.Default) && !ProcessingMessage)
                {
                    SendChangedMessage();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public float[][] OriginalValue
        {
            get { return _originalValue; }
        }

        /// <summary>
        /// 
        /// </summary>
        public float MinValue
        {
            get { return _minValue; }
        }

        /// <summary>
        /// 
        /// </summary>
        public float MaxValue
        {
            get { return _maxValue; }
        }

        /// <summary>
        /// 
        /// </summary>
        public float Step
        {
            get { return _step; }
        }
        #endregion // Properties

        #region Message Processing
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void ProcessMessageCore(RemotePacketEventArgs e)
        {
            switch ((BankCommand)e.Packet.Command)
            {
                case BankCommand.Changed:
                    ProcessChangedMessage(e);
                    break;

                // These are all messages from RAG to the game.
                case BankCommand.User0:
                case BankCommand.User1:
                case BankCommand.User2:
                case BankCommand.User3:
                case BankCommand.User4:
                case BankCommand.User5:
                case BankCommand.User6:
                case BankCommand.User7:
                case BankCommand.User8:
                case BankCommand.User9:
                case BankCommand.User10:
                case BankCommand.User11:
                    e.Packet.ReadToEnd();
                    break;

                default:
                    base.ProcessMessageCore(e);
                    break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        public override bool ProcessCreateMessage(RemotePacketEventArgs e)
        {
            if (!base.ProcessCreateMessage(e))
            {
                return false;
            }

            _value = new float[NumRows][];
            for (int r = 0; r < NumRows; ++r)
            {
                _value[r] = new float[NumColumns];

                for (int c = 0; c < NumColumns; ++c)
                {
                    _value[r][c] = e.Packet.ReadFloat();
                }
            }
            _originalValue = _value;
            _minValue = e.Packet.ReadFloat();
            _maxValue = e.Packet.ReadFloat();
            _step = e.Packet.ReadFloat();
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void ProcessChangedMessage(RemotePacketEventArgs e)
        {
            float[][] newValue  = new float[NumRows][];
            for (int r = 0; r < NumRows; ++r)
            {
                _value[r] = new float[NumColumns];

                for (int c = 0; c < NumColumns; ++c)
                {
                    _value[r][c] = e.Packet.ReadFloat();
                }
            }
            Value = newValue;
        }
        #endregion // Message Processing

        #region Message Dispatching
        /// <summary>
        /// 
        /// </summary>
        private void SendChangedMessage()
        {
            RemotePacketBuilder builder = new RemotePacketBuilder((ushort)BankCommand.Changed, this.Guid.ToInteger(), Id);
            for (int r = 0; r < NumRows; ++r)
            {
                for (int c = 0; c < NumColumns; ++c)
                {
                    builder.Write(_value[r][c]);
                }
            }
            Dispatcher.SendPacket(builder.ToRemotePacket());
        }
        #endregion // Message Dispatching
    } // WidgetMatrix
}
