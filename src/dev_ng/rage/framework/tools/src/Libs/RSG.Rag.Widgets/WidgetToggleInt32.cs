﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Rag.Core;

namespace RSG.Rag.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    [WidgetGuidAttribute('t', 'g', 's', '3')]
    public class WidgetToggleInt32 : WidgetToggle<Int32>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public WidgetToggleInt32(uint id, IWidgetResolver resolver, IPacketDispatcher dispatcher)
            : base(id, resolver, dispatcher)
        {
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="packet"></param>
        /// <returns></returns>
        protected override Int32 ReadMask(RemotePacket packet)
        {
            return packet.ReadInt32();
        }
        #endregion // Overrides
    } // WidgetToggleInt32
}
