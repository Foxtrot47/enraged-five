﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using RSG.Rag.Core;

namespace RSG.Rag.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    [WidgetGuidAttribute('v', 'c', 'l', 'r')]
    public class WidgetColor : Widget
    {
        #region Enums
        /// <summary>
        /// 
        /// </summary>
        public enum ColorType
        {
            /// <summary>
            /// 
            /// </summary>
            Vector3,

            /// <summary>
            /// 
            /// </summary>
            Vector4,

            /// <summary>
            /// 
            /// </summary>
            Color32
        }
        #endregion

        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        private ColorType _type;

        /// <summary>
        /// 
        /// </summary>
        private float _red;

        /// <summary>
        /// 
        /// </summary>
        private float _green;

        /// <summary>
        /// 
        /// </summary>
        private float _blue;

        /// <summary>
        /// 
        /// </summary>
        private float _alpha;

        /// <summary>
        /// 
        /// </summary>
        private float _defaultRed;

        /// <summary>
        /// 
        /// </summary>
        private float _defaultGreen;

        /// <summary>
        /// 
        /// </summary>
        private float _defaultBlue;

        /// <summary>
        /// 
        /// </summary>
        private float _defaultAlpha;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public WidgetColor(uint id, IWidgetResolver resolver, IPacketDispatcher dispatcher)
            : base(id, resolver, dispatcher)
        {
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public ColorType Type
        {
            get { return _type; }
        }

        /// <summary>
        /// 
        /// </summary>
        public float Red
        {
            get { return _red; }
            set
            {
                if (SetProperty(ref _red, value) && !ProcessingMessage)
                {
                    SendChangedMessage();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public float Green
        {
            get { return _green; }
            set
            {
                if (SetProperty(ref _green, value) && !ProcessingMessage)
                {
                    SendChangedMessage();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public float Blue
        {
            get { return _blue; }
            set
            {
                if (SetProperty(ref _blue, value) && !ProcessingMessage)
                {
                    SendChangedMessage();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public float Alpha
        {
            get { return _alpha; }
            set
            {
                if (SetProperty(ref _alpha, value) && !ProcessingMessage)
                {
                    SendChangedMessage();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public float DefaultRed
        {
            get { return _defaultRed; }
        }

        /// <summary>
        /// 
        /// </summary>
        public float DefaultGreen
        {
            get { return _defaultGreen; }
        }

        /// <summary>
        /// 
        /// </summary>
        public float DefaultBlue
        {
            get { return _defaultBlue; }
        }

        /// <summary>
        /// 
        /// </summary>
        public float DefaultAlpha
        {
            get { return _defaultAlpha; }
        }
        #endregion // Properties

        #region Message Processing
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void ProcessMessageCore(RemotePacketEventArgs e)
        {
            switch ((BankCommand)e.Packet.Command)
            {
                case BankCommand.Changed:
                    ProcessChangedMessage(e);
                    break;

                default:
                    base.ProcessMessageCore(e);
                    break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        public override bool ProcessCreateMessage(RemotePacketEventArgs e)
        {
            if (!base.ProcessCreateMessage(e))
            {
                return false;
            }

            _type = (ColorType)e.Packet.ReadUInt32();
            _red = e.Packet.ReadFloat();
            _green = e.Packet.ReadFloat();
            _blue = e.Packet.ReadFloat();
            _alpha = e.Packet.ReadFloat();
            _defaultRed = _red;
            _defaultGreen = _green;
            _defaultBlue = _blue;
            _defaultAlpha = _alpha;
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void ProcessChangedMessage(RemotePacketEventArgs e)
        {
            Red = e.Packet.ReadFloat();
            Green = e.Packet.ReadFloat();
            Blue = e.Packet.ReadFloat();
            Alpha = e.Packet.ReadFloat();
        }
        #endregion // Message Processing

        #region Message Dispatching
        /// <summary>
        /// 
        /// </summary>
        private void SendChangedMessage()
        {
            RemotePacketBuilder builder = new RemotePacketBuilder((ushort)BankCommand.Changed, this.Guid.ToInteger(), Id);
            builder.Write(Red);
            builder.Write(Green);
            builder.Write(Blue);
            builder.Write(Alpha);
            Dispatcher.SendPacket(builder.ToRemotePacket());
        }
        #endregion // Message Dispatching
    } // WidgetColor
}
