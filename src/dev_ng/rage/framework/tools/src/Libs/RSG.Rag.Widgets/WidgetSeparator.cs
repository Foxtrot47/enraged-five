﻿using RSG.Rag.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    [WidgetGuidAttribute('s', 'e', 'p', 'r')]
    public class WidgetSeparator : Widget
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public WidgetSeparator(uint id, IWidgetResolver resolver, IPacketDispatcher dispatcher)
            : base(id, resolver, dispatcher)
        {
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public override String Name
        {
            get { return "<<Separator>>"; }
        }
        #endregion // Properties
    } // Widget Separator
}
