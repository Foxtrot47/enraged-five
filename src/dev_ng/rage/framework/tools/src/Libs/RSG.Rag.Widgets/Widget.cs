﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using RSG.Base.Attributes;
using RSG.Rag.Core;
using RSG.Editor;


namespace RSG.Rag.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class Widget : NotifyPropertyChangedBase, IWidget
    {
        #region Constants
        /// <summary>
        /// Invalid widget id.
        /// </summary>
        public const uint INVALID_ID = 0;
        #endregion // Constatns

        #region Member
        /// <summary>
        /// 
        /// </summary>
        private readonly uint _id;

        /// <summary>
        /// 
        /// </summary>
        private String _name;

        /// <summary>
        /// 
        /// </summary>
        private String _tooltip;

        /// <summary>
        /// 
        /// </summary>
        private Color _fillColor;

        /// <summary>
        /// 
        /// </summary>
        private bool _readOnly;

        /// <summary>
        /// 
        /// </summary>
        private IWidgetGroup _parent;

        /// <summary>
        /// Flag indicating whether the widget is currently processing a remote message.
        /// </summary>
        private bool _processingMessage;

        /// <summary>
        /// 
        /// </summary>
        private readonly IWidgetResolver _widgetResolver;

        /// <summary>
        /// 
        /// </summary>
        private readonly IPacketDispatcher _dispatcher;
        #endregion // Member Data

        #region Static Data
        /// <summary>
        /// WidgetGuid lookup.
        /// </summary>
        private static IDictionary<Type, WidgetGuid> _widgetGuidLookup = new Dictionary<Type, WidgetGuid>();
        #endregion // Static Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        protected Widget(uint id, IWidgetResolver resolver, IPacketDispatcher dispatcher)
        {
            // Make sure the guid for this widget type is set up.
            Type widgetType = GetType();
            if (!_widgetGuidLookup.ContainsKey(widgetType))
            {
                WidgetGuidAttribute att = (WidgetGuidAttribute)widgetType.GetCustomAttributes(typeof(WidgetGuidAttribute), true).FirstOrDefault();
                Debug.Assert(att != null, "Widget is missing WidgetGuidAttribute.");
                if (att == null)
                {
                    throw new ArgumentNullException("WidgetGuidAttribute", "All classes derived from Widget must have a WidgetGuidAttribute defined.");
                }
                _widgetGuidLookup.Add(widgetType, att.Guid);
            }

            // Make sure the id isn't the invalid one.
            Debug.Assert(id != INVALID_ID, "Creating a widget with the invalid id.");
            _id = id;
            _widgetResolver = resolver;
            _dispatcher = dispatcher;
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// Guid for this widget.
        /// </summary>
        public virtual WidgetGuid Guid
        {
            get { return _widgetGuidLookup[GetType()]; }
        }

        /// <summary>
        /// Unique identifier for this widget as set by the game.
        /// </summary>
        public uint Id
        {
            get { return _id; }
        }

        /// <summary>
        /// Name of the widget.
        /// </summary>
        public virtual String Name
        {
            get { return _name; }
        }

        /// <summary>
        /// Parent of this widget.
        /// </summary>
        public IWidgetGroup Parent
        {
            get { return _parent; }
        }

        /// <summary>
        /// Full path to this widget.
        /// </summary>
        public String Path
        {
            get { return (_parent != null ? String.Format("{0}\\{1}", _parent.Path, Name) : Name); }
        }

        /// <summary>
        /// Tooltip for this widget.
        /// </summary>
        public String Tooltip
        {
            get { return _tooltip; }
            private set { SetProperty(ref _tooltip, value); }
        }

        /// <summary>
        /// Color to use for the widget's background.
        /// </summary>
        public Color FillColor
        {
            get { return _fillColor; }
            private set { SetProperty(ref _fillColor, value); }
        }

        /// <summary>
        /// Flag indicating whether this widget is read-only.
        /// </summary>
        public bool ReadOnly
        {
            get { return _readOnly; }
            private set { SetProperty(ref _readOnly, value); }
        }

        /// <summary>
        /// Flag indicating whether we are currently processing an remote message.
        /// </summary>
        protected bool ProcessingMessage
        {
            get { return _processingMessage; }
        }

        /// <summary>
        /// Reference to the object responsible for converting widget id's into widgets.
        /// </summary>
        protected IWidgetResolver WidgetResolver
        {
            get { return _widgetResolver; }
        }

        /// <summary>
        /// Reference to the object that is responsible for dispatching messages.
        /// </summary>
        protected IPacketDispatcher Dispatcher
        {
            get { return _dispatcher; }
        }
        #endregion // Properties

        #region Message Processing
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        public virtual bool ProcessCreateMessage(RemotePacketEventArgs e)
        {
            uint parentId = e.Packet.ReadWidget();

            IWidget parent = WidgetResolver.GetWidgetById(parentId);
            if (parent == null)
            {
                e.AddErrorMessage("Unable to determine parent.");
                return false;
            }
            else if (!(parent is IWidgetGroup))
            {
                e.AddErrorMessage("Parent is not a group widget.");
                return false;
            }
            else
            {
                _parent = (IWidgetGroup)parent;
                _name = e.Packet.ReadString();
                _tooltip = e.Packet.ReadString();
                _fillColor = RSG.Rag.Widgets.Widget.DeserializeColor(e.Packet.ReadString());
                _readOnly = e.Packet.ReadBoolean();
                return true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        public void ProcessMessage(RemotePacketEventArgs e)
        {
            _processingMessage = true;
            ProcessMessageCore(e);
            _processingMessage = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected virtual void ProcessMessageCore(RemotePacketEventArgs e)
        {
            switch ((BankCommand)e.Packet.Command)
            {
                case BankCommand.FillColor:
                    ProcessFillColorMessage(e);
                    break;

                case BankCommand.ReadOnly:
                    ProcessReadOnlyMessage(e);
                    break;

                default:
                    e.AddErrorMessage("Unsupported ...");
                    break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual void Destroy()
        {
            // Inform the widget resolver that this widget is being destroyed.
            WidgetResolver.WidgetDestroyed(this);

            // Remove the widget from the parent if it has one.
            if (_parent != null)
            {
                _parent.RemoveChild(this);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void ProcessFillColorMessage(RemotePacketEventArgs e)
        {
            FillColor = Widget.DeserializeColor(e.Packet.ReadString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void ProcessReadOnlyMessage(RemotePacketEventArgs e)
        {
            ReadOnly = e.Packet.ReadBoolean();
        }
        #endregion // Message Processing

        #region Static Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        public static String SerializeColor(Color color)
        {
            return string.Format("{0}:{1}:{2}:{3}:{4}", ColorFormat.ARGBColor, color.A, color.R, color.G, color.B);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        public static Color DeserializeColor(String color)
        {
            if (String.IsNullOrEmpty(color))
            {
                return Colors.White;
            }

            byte a, r, g, b;

            string[] pieces = color.Split(new char[] { ':' });
            if (pieces.Length > 0)
            {
                ColorFormat colorType = (ColorFormat)Enum.Parse(typeof(ColorFormat), pieces[0], true);

                switch (colorType)
                {
                    case ColorFormat.NamedColor:
                        if (pieces.Length >= 2)
                        {
                            return (Color)ColorConverter.ConvertFromString(pieces[1]);
                        }
                        break;

                    case ColorFormat.ARGBColor:
                        if (pieces.Length >= 5)
                        {
                            a = byte.Parse(pieces[1]);
                            r = byte.Parse(pieces[2]);
                            g = byte.Parse(pieces[3]);
                            b = byte.Parse(pieces[4]);
                            return Color.FromArgb(a, r, g, b);
                        }
                        break;
                }
            }

            return Colors.White;
        }
        #endregion // Static Methods
    } // Widget
}
