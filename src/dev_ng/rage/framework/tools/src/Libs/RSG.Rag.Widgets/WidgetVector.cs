﻿using RSG.Base;
using RSG.Rag.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class WidgetVector : Widget
    {
        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        private float[] _value;

        /// <summary>
        /// 
        /// </summary>
        private float[] _originalValue;

        /// <summary>
        /// 
        /// </summary>
        private float _minimum;

        /// <summary>
        /// 
        /// </summary>
        private float _maximum;

        /// <summary>
        /// 
        /// </summary>
        private float _step;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        protected WidgetVector(uint id, IWidgetResolver resolver, IPacketDispatcher dispatcher)
            : base(id, resolver, dispatcher)
        {
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// Number of components this vector widget has.
        /// </summary>
        public abstract int NumComponents { get; }

        /// <summary>
        /// 
        /// </summary>
        public float[] Value
        {
            get { return _value; }
            set
            {
                if (SetProperty(ref _value, value, StructuralEqualityComparer<float[]>.Default) && !ProcessingMessage)
                {
                    SendChangedMessage();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public float[] OriginalValue
        {
            get { return _originalValue; }
        }

        /// <summary>
        /// 
        /// </summary>
        public float Minimum
        {
            get { return _minimum; }
        }

        /// <summary>
        /// 
        /// </summary>
        public float Maximum
        {
            get { return _maximum; }
        }

        /// <summary>
        /// 
        /// </summary>
        public float Step
        {
            get { return _step; }
        }
        #endregion // Properties

        #region Message Processing
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void ProcessMessageCore(RemotePacketEventArgs e)
        {
            switch ((BankCommand)e.Packet.Command)
            {
                case BankCommand.Changed:
                    ProcessChangedMessage(e);
                    break;

                // These are all messages from RAG to the game.
                case BankCommand.User0:
                case BankCommand.User1:
                case BankCommand.User2:
                case BankCommand.User3:
                case BankCommand.User4:
                    e.Packet.ReadToEnd();
                    break;

                default:
                    base.ProcessMessageCore(e);
                    break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        public override bool ProcessCreateMessage(RemotePacketEventArgs e)
        {
            if (!base.ProcessCreateMessage(e))
            {
                return false;
            }

            _value = new float[NumComponents];
            for (int i = 0; i < NumComponents; ++i)
            {
                _value[i] = e.Packet.ReadFloat();
            }
            _originalValue = (float[])_value.Clone();

            _minimum = e.Packet.ReadFloat();
            _maximum = e.Packet.ReadFloat();
            _step = e.Packet.ReadFloat();

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void ProcessChangedMessage(RemotePacketEventArgs e)
        {
            float[] newValue = new float[NumComponents];
            for (int i = 0; i < NumComponents; ++i)
            {
                newValue[i] = e.Packet.ReadFloat();
            }
            Value = newValue;
        }
        #endregion // Message Processing

        #region Message Dispatching
        /// <summary>
        /// 
        /// </summary>
        private void SendChangedMessage()
        {
            RemotePacketBuilder builder = new RemotePacketBuilder((ushort)BankCommand.Changed, this.Guid.ToInteger(), Id);
            for (int i = 0; i < NumComponents; ++i)
            {
                builder.Write(_value[i]);
            }
            Dispatcher.SendPacket(builder.ToRemotePacket());
        }
        #endregion // Message Dispatching
    } // WidgetVector
}
