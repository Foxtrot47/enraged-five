﻿using RSG.Rag.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    [WidgetGuidAttribute('s', 'l', 's', '8')]
    public class WidgetSliderInt8 : WidgetSlider<SByte>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public WidgetSliderInt8(uint id, IWidgetResolver resolver, IPacketDispatcher dispatcher)
            : base(id, resolver, dispatcher)
        {
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="packet"></param>
        /// <returns></returns>
        protected override SByte ReadValue(RemotePacket packet)
        {
            return packet.ReadSByte();
        }

        /// <summary>
        /// Method for writing a value to the remote packet builder.
        /// </summary>
        /// <param name="builder">Reference to the remote packet builder.</param>
        /// <param name="value">Value to write.</param>
        protected override void WriteValue(RemotePacketBuilder builder, SByte value)
        {
            builder.Write(value);
        }
        #endregion // Overrides
    } // WidgetSliderInt8
}
