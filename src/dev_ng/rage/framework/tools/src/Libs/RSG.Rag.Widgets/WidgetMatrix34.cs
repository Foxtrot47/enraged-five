﻿using RSG.Rag.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    //[WidgetGuidAttribute('m', 't', 'x', '2')]
    public class WidgetMatrix34 : WidgetMatrix
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public WidgetMatrix34(uint id, IWidgetResolver resolver, IPacketDispatcher dispatcher)
            : base(id, resolver, dispatcher)
        {
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public override int NumRows
        {
            get { return 4; }
        }

        /// <summary>
        /// 
        /// </summary>
        public override int NumColumns
        {
            get { return 3; }
        }
        #endregion // Properties
    } // WidgetMatrix34
}
