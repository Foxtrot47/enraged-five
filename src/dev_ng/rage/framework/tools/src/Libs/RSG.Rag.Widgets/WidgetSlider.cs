﻿using RSG.Rag.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.Widgets
{
    /// <summary>
    /// Base class for slider based widgets.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class WidgetSlider<T> : Widget
    {
        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        private T _value;

        /// <summary>
        /// 
        /// </summary>
        private T _originalValue;

        /// <summary>
        /// 
        /// </summary>
        private T _minValue;

        /// <summary>
        /// 
        /// </summary>
        private T _maxValue;

        /// <summary>
        /// 
        /// </summary>
        private T _step;

        /// <summary>
        /// 
        /// </summary>
        private bool _exponential;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        protected WidgetSlider(uint id, IWidgetResolver resolver, IPacketDispatcher dispatcher)
            : base(id, resolver, dispatcher)
        {
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public T Value
        {
            get { return _value; }
            set
            {
                if (SetProperty(ref _value, value) && !ProcessingMessage)
                {
                    SendChangedMessage();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public T OriginalValue
        {
            get { return _originalValue; }
        }

        /// <summary>
        /// 
        /// </summary>
        public T MinValue
        {
            get { return _minValue; }
            protected set { _minValue = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public T MaxValue
        {
            get { return _maxValue; }
            protected set { _maxValue = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public T Step
        {
            get { return _step; }
            protected set { _step = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool Exponential
        {
            get { return _exponential; }
        }
        #endregion // Properties

        #region Message Processing
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void ProcessMessageCore(RemotePacketEventArgs e)
        {
            switch ((BankCommand)e.Packet.Command)
            {
                case BankCommand.Changed:
                    ProcessChangedMessage(e);
                    break;

                case BankCommand.User0:
                    ProcessLimitsMessage(e);
                    break;

                case BankCommand.User1:
                    ProcessStepMessage(e);
                    break;

                default:
                    base.ProcessMessageCore(e);
                    break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        public override bool ProcessCreateMessage(RemotePacketEventArgs e)
        {
            if (!base.ProcessCreateMessage(e))
            {
                return false;
            }

            _value = ReadValue(e.Packet);
            _originalValue = _value;
            _minValue = ReadValue(e.Packet);
            _maxValue = ReadValue(e.Packet);
            _step = ReadValue(e.Packet);
            _exponential = e.Packet.ReadBoolean();
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void ProcessChangedMessage(RemotePacketEventArgs e)
        {
            Value = ReadValue(e.Packet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void ProcessLimitsMessage(RemotePacketEventArgs e)
        {
            MinValue = ReadValue(e.Packet);
            MaxValue = ReadValue(e.Packet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void ProcessStepMessage(RemotePacketEventArgs e)
        {
            Step = ReadValue(e.Packet);
        }

        /// <summary>
        /// Abstract method for reading the correct type of value from the packet.
        /// </summary>
        /// <param name="packet"></param>
        /// <returns></returns>
        protected abstract T ReadValue(RemotePacket packet);
        #endregion // Message

        #region Message Dispatching
        /// <summary>
        /// 
        /// </summary>
        private void SendChangedMessage()
        {
            RemotePacketBuilder builder = new RemotePacketBuilder((ushort)BankCommand.Changed, this.Guid.ToInteger(), Id);
            WriteValue(builder, _value);
            Dispatcher.SendPacketAsync(builder.ToRemotePacket());
        }

        /// <summary>
        /// Method for writing a value to the remote packet builder.
        /// </summary>
        /// <param name="builder">Reference to the remote packet builder.</param>
        /// <param name="value">Value to write.</param>
        protected abstract void WriteValue(RemotePacketBuilder builder, T value);
        #endregion // Message Dispatching
    } // WidgetSliderBase<T>
}
