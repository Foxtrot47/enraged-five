﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Rag.Core;

namespace RSG.Rag.Widgets
{
    /// <summary>
    /// Base class for groups of widgets.
    /// </summary>
    public abstract class WidgetGroupBase : Widget, IWidgetGroup
    {
        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        private readonly ICollection<IWidget> _children;

        /// <summary>
        /// 
        /// </summary>
        private readonly IReadOnlyCollection<IWidget> _readOnlyChildren;

        /// <summary>
        /// How many things are actively showing/using widgets within this group.
        /// </summary>
        private int _openCount = 0;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        protected WidgetGroupBase(uint id, IWidgetResolver resolver, IPacketDispatcher dispatcher)
            : base(id, resolver, dispatcher)
        {
            _children = new ObservableCollection<IWidget>();
            _readOnlyChildren = new ReadOnlyObservableCollection<IWidget>((ObservableCollection<IWidget>)_children);
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// Child widgets that this group contains.
        /// </summary>
        public IReadOnlyCollection<IWidget> Children
        {
            get { return _readOnlyChildren; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int OpenCount
        {
            get { return _openCount; }
            protected set
            {
                if (SetProperty(ref _openCount, value) && !ProcessingMessage)
                {
                    SendChangedMessage();
                }
            }
        }
        #endregion // Properties

        #region IWidgetGroup Implementation
        /// <summary>
        /// Adds a child to this widget.
        /// </summary>
        /// <param name="widget"></param>
        public virtual void AddChild(IWidget widget)
        {
            _children.Add(widget);
        }

        /// <summary>
        /// Removes a child from this group.
        /// </summary>
        /// <param name="widget"></param>
        public virtual void RemoveChild(IWidget widget)
        {
            _children.Remove(widget);
        }

        /// <summary>
        /// Adds a reference to the fact that this group is in active use.
        /// </summary>
        public virtual void AddOpenReference()
        {
            OpenCount++;

            if (Parent != null)
            {
                Parent.AddOpenReference();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual void RemoveOpenReference()
        {
            OpenCount--;

            if (Parent != null)
            {
                Parent.RemoveOpenReference();
            }
        }
        #endregion // IWidgetGroup Implementation

        #region Message Processing
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void ProcessMessageCore(RemotePacketEventArgs e)
        {
            switch ((BankCommand)e.Packet.Command)
            {
                case BankCommand.Changed:
                    OpenCount = (int)e.Packet.ReadInt16();
                    break;

                case BankCommand.User0:
                    // Syncing of open flags.  Is this needed???
                    OpenCount = e.Packet.ReadInt32();
                    break;

                default:
                    base.ProcessMessageCore(e);
                    break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        public override bool ProcessCreateMessage(RemotePacketEventArgs e)
        {
            if (!base.ProcessCreateMessage(e))
            {
                return false;
            }

            _openCount = e.Packet.ReadInt32();
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        public override void Destroy()
        {
            // Destroy all the children before destroying ourself.
            foreach (IWidget child in Children.ToArray())
            {
                child.Destroy();
            }

            base.Destroy();
        }
        #endregion // Message Processing

        #region Message Dispatching
        /// <summary>
        /// 
        /// </summary>
        private void SendChangedMessage()
        {
            RemotePacketBuilder builder = new RemotePacketBuilder((ushort)BankCommand.Changed, this.Guid.ToInteger(), Id);
            builder.Write((short)_openCount);
            Dispatcher.SendPacketAsync(builder.ToRemotePacket());

            builder = new RemotePacketBuilder((ushort)BankCommand.User0, this.Guid.ToInteger(), Id);
            builder.Write(_openCount);
            Dispatcher.SendPacketAsync(builder.ToRemotePacket());
        }
        #endregion // Message Dispatching
    } // WidgetGroupBase
}
