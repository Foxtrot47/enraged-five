﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Rag.Core;

namespace RSG.Rag.Widgets
{
    /// <summary>
    /// Base class for toggle related widgets.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class WidgetToggle<T> : Widget
    {
        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        private bool _toggled;

        /// <summary>
        /// 
        /// </summary>
        private bool _originalValue;

        /// <summary>
        /// 
        /// </summary>
        private T _mask;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        protected WidgetToggle(uint id, IWidgetResolver resolver, IPacketDispatcher dispatcher)
            : base(id, resolver, dispatcher)
        {
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public bool Toggled
        {
            get { return _toggled; }
            set
            {
                if (SetProperty(ref _toggled, value) && !ProcessingMessage)
                {
                    SendChangedMessage();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool OriginalValue
        {
            get { return _originalValue; }
        }
        #endregion // Properties

        #region Message Processing
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void ProcessMessageCore(RemotePacketEventArgs e)
        {
            switch ((BankCommand)e.Packet.Command)
            {
                case BankCommand.Changed:
                    ProcessChangedMessage(e);
                    break;

                default:
                    base.ProcessMessageCore(e);
                    break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        public override bool ProcessCreateMessage(RemotePacketEventArgs e)
        {
            if (!base.ProcessCreateMessage(e))
            {
                return false;
            }

            _toggled = e.Packet.ReadBoolean();
            _originalValue = _toggled;
            _mask = ReadMask(e.Packet);
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void ProcessChangedMessage(RemotePacketEventArgs e)
        {
            Toggled = e.Packet.ReadBoolean();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="packet"></param>
        /// <returns></returns>
        protected abstract T ReadMask(RemotePacket packet);
        #endregion // Message Processing

        #region Message Dispatching
        /// <summary>
        /// 
        /// </summary>
        private void SendChangedMessage()
        {
            RemotePacketBuilder builder = new RemotePacketBuilder((ushort)BankCommand.Changed, this.Guid.ToInteger(), Id);
            builder.Write(Toggled);

            Dispatcher.SendPacketAsync(builder.ToRemotePacket());
        }
        #endregion // Message Dispatching
    } // WidgetToggle
}
