﻿using RSG.Rag.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    [WidgetGuidAttribute('v', 'e', 'c', '3')]
    public class WidgetVector3 : WidgetVector
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public WidgetVector3(uint id, IWidgetResolver resolver, IPacketDispatcher dispatcher)
            : base(id, resolver, dispatcher)
        {
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public override int NumComponents
        {
            get { return 3; }
        }
        #endregion // Properties
    } // WidgetVector3
}
