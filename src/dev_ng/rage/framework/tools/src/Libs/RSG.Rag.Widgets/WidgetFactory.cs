﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.Logging;
using RSG.Rag.Core;

namespace RSG.Rag.Widgets
{
    /// <summary>
    /// Used for creating widgets based on their widget guid.
    /// This class self registers widget types by reflecting over all loaded
    /// assemblies that have a name starting with RSG.Rag.Widgets looking for
    /// types that implement the IWidget interface and have a WidgetGuid attribute.
    /// </summary>
    public class WidgetFactory
    {
        #region Constants
        /// <summary>
        /// Log context.
        /// </summary>
        private const String WIDGET_FACTORY_CTX = "Widget Factory";
        #endregion // Constants

        #region Member Data
        /// <summary>
        /// Log object.
        /// </summary>
        private readonly ILog _log;

        /// <summary>
        /// Registered widget types that we know can be handled.
        /// </summary>
        private readonly IDictionary<WidgetGuid, Type> _widgetTypes = new Dictionary<WidgetGuid, Type>();
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public WidgetFactory(ILog log)
        {
            _log = log;

            _log.ProfileCtx(WIDGET_FACTORY_CTX, "Discovering widgets.");

            // Iterate over the loaded assemblies 
            Assembly[] loadedAssemblies = AppDomain.CurrentDomain.GetAssemblies();
            foreach (Assembly assembly in loadedAssemblies)
            {
                // Is this an assembly we're interested in.
                if (assembly.GetName().Name.StartsWith("RSG.Rag.Widgets", StringComparison.OrdinalIgnoreCase))
                {
                    RegisterWidgetTypesInAssembly(assembly);
                }
            }

            _log.ProfileEnd();
        }
        #endregion // Constructor(s)

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of a widget based on it's guid.
        /// </summary>
        /// <param name="guid">Widget guid for the type of widget we wish to create.</param>
        /// <param name="widgetId">Unique id for the widget.</param>
        /// <param name="resolver">Widget resolver.</param>
        /// <param name="dispatcher">Packet dispatcher.</param>
        /// <returns></returns>
        public IWidget CreateWidget(WidgetGuid guid, uint widgetId, IWidgetResolver resolver, IPacketDispatcher dispatcher)
        {
            Type widgetType;
            if (!_widgetTypes.TryGetValue(guid, out widgetType))
            {
                //_log.ErrorCtx(WIDGET_FACTORY_CTX, "'{0}' is not a registered widget guid.", guid);
                return new WidgetUnknown(widgetId, resolver, dispatcher, guid);
            }

            return (IWidget)Activator.CreateInstance(widgetType, widgetId, resolver, dispatcher);
        }

        /// <summary>
        /// Returns whether the passed in guid is known by the factory.
        /// </summary>
        /// <param name="guid"></param>
        /// <returns>True if the particular guid is registered, otherwise false.</returns>
        public bool IsRegistered(WidgetGuid guid)
        {
            return _widgetTypes.ContainsKey(guid);
        }
        #endregion // Factory Methods

        #region Registration Methods
        /// <summary>
        /// Used to register all widget types that are present in a particular assembly.
        /// </summary>
        /// <param name="assembly">Assembly to reflect over.</param>
        private void RegisterWidgetTypesInAssembly(Assembly assembly)
        {
            Type iWidgetType = typeof(IWidget);

            // Iterate over all types in the assembly that are classes and not abstract.
            foreach (Type type in assembly.GetTypes().Where(type => type.IsClass && !type.IsAbstract))
            {
                if (iWidgetType.IsAssignableFrom(type))
                {
                    WidgetGuidAttribute attribute = 
                        type.GetCustomAttributes(typeof(WidgetGuidAttribute), false).FirstOrDefault() as WidgetGuidAttribute;
                    if (attribute != null)
                    {
                        _widgetTypes.Add(attribute.Guid, type);
                        _log.MessageCtx(WIDGET_FACTORY_CTX, "Registered the '{0}' widget type.", attribute.Guid);
                    }
                }
            }
        }
        #endregion // Registration Methods
    } // WidgetFactory
}
