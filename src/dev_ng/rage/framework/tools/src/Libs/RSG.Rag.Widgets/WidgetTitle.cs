﻿using RSG.Rag.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    [WidgetGuidAttribute('t', 'i', 't', 'l')]
    public class WidgetTitle : Widget
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public WidgetTitle(uint id, IWidgetResolver resolver, IPacketDispatcher dispatcher)
            : base(id, resolver, dispatcher)
        {
        }
        #endregion // Constructor(s)
    } // WidgetTitle
}
