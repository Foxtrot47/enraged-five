﻿using RSG.Rag.Core;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Base;

namespace RSG.Rag.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    public class ItemChangedEventArgs : EventArgs
    {
        private readonly int _index;
        private readonly String _value;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <param name="value"></param>
        public ItemChangedEventArgs(int index, String value)
        {
            _index = index;
            _value = value;
        }

        /// <summary>
        /// 
        /// </summary>
        public int Index
        {
            get { return _index; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String Value
        {
            get { return _value; }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void ItemChangedEventHandler(object sender, ItemChangedEventArgs e);

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class WidgetCombo<T> : Widget
    {
        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        private T _selectedItem;

        /// <summary>
        /// 
        /// </summary>
        private T _defaultItem;

        /// <summary>
        /// 
        /// </summary>
        private int _offset;

        /// <summary>
        /// 
        /// </summary>
        private String[] _items;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public WidgetCombo(uint id, IWidgetResolver resolver, IPacketDispatcher dispatcher)
            : base(id, resolver, dispatcher)
        {
        }
        #endregion // Constructor(s)

        #region Events
        /// <summary>
        /// 
        /// </summary>
        public event ItemChangedEventHandler ItemChanged;
        #endregion // Events

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public T SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                if (SetProperty(ref _selectedItem, value) && !ProcessingMessage)
                {
                    SendChangedMessage();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public T DefaultItem
        {
            get { return _defaultItem; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int Offset
        {
            get { return _offset; }
            protected set { SetProperty(ref _offset, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public String[] Items
        {
            get { return _items; }
            protected set { SetProperty(ref _items, value, StructuralEqualityComparer<String[]>.Default); }
        }
        #endregion // Properties

        #region Message Processing
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void ProcessMessageCore(RemotePacketEventArgs e)
        {
            switch ((BankCommand)e.Packet.Command)
            {
                case BankCommand.Changed:
                    ProcessChangedMessage(e);
                    break;

                case BankCommand.User0:
                    ProcessSetItemValueMessage(e);
                    break;

                case BankCommand.User1:
                    ProcessUpdateAllMessage(e);
                    break;

                default:
                    base.ProcessMessageCore(e);
                    break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        public override bool ProcessCreateMessage(RemotePacketEventArgs e)
        {
            if (!base.ProcessCreateMessage(e))
            {
                return false;
            }

            _selectedItem = ReadValue(e.Packet);
            _defaultItem = _selectedItem;

            int count = e.Packet.ReadInt32();
            _items = new String[count];
            for (int i = 0; i < count; ++i)
            {
                _items[i] = "<<no name>>";
            }

            _offset = e.Packet.ReadInt32();

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void ProcessChangedMessage(RemotePacketEventArgs e)
        {
            SelectedItem = ReadValue(e.Packet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void ProcessSetItemValueMessage(RemotePacketEventArgs e)
        {
            int index = e.Packet.ReadInt32();
            String value = e.Packet.ReadString();

            Debug.Assert(index >=0 && index < _items.Length, "Trying to set combo item string which is outside the bounds of the number of items.");
            if (index >= 0 && index < Items.Length)
            {
                _items[index] = value;

                if (ItemChanged != null)
                {
                    ItemChanged(this, new ItemChangedEventArgs(index, value));
                }
            }
            else
            {
                e.AddErrorMessage("Trying to set combo item string which is outside the bounds of the number of items.  Index: {0}, Count: {1}", index, _items.Length);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void ProcessUpdateAllMessage(RemotePacketEventArgs e)
        {
            // These need to be rethought...
            String newTitle = e.Packet.ReadString();
            String newTooltip = e.Packet.ReadString();
            SelectedItem = ReadValue(e.Packet);
            Items = new String[e.Packet.ReadInt32()];
            Offset = e.Packet.ReadInt32();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="packet"></param>
        /// <returns></returns>
        protected abstract T ReadValue(RemotePacket packet);
        #endregion // Message Processing

        #region Message Dispatching
        /// <summary>
        /// 
        /// </summary>
        private void SendChangedMessage()
        {
            RemotePacketBuilder builder = new RemotePacketBuilder((ushort)BankCommand.Changed, this.Guid.ToInteger(), Id);
            WriteValue(builder, _selectedItem);
            Dispatcher.SendPacketAsync(builder.ToRemotePacket());
        }

        /// <summary>
        /// Method for writing a value to the remote packet builder.
        /// </summary>
        /// <param name="builder">Reference to the remote packet builder.</param>
        /// <param name="value">Value to write.</param>
        protected abstract void WriteValue(RemotePacketBuilder builder, T value);
        #endregion // Message Dispatching
    } // WidgetCombo<T>
}
