﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Rag.Core;

namespace RSG.Rag.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    [WidgetGuidAttribute('b', 'm', 'g', 'r')]
    public class BankManager : WidgetGroupBase
    {
        #region Member Data
        /// <summary>
        /// Flag determining whether we are part of the proxy or not.
        /// </summary>
        private bool _inProxy;

        /// <summary>
        /// 
        /// </summary>
        private String _timingInformation;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public BankManager(uint id, IWidgetResolver resolver, IPacketDispatcher dispatcher)
            : base(id, resolver, dispatcher)
        {
            _inProxy = false;
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public override string Name
        {
            get { return "<<Bank Manager>>"; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String TimingInformation
        {
            get { return _timingInformation; }
            protected set
            {
                SetProperty(ref _timingInformation, value);
            }
        }
        #endregion // Properties

        #region Child Management
        /// <summary>
        /// Adds a child to this widget.
        /// </summary>
        /// <param name="widget"></param>
        public override void AddChild(IWidget widget)
        {
            // Make sure we are only adding bank widgets.
            Debug.Assert(widget is WidgetBank,
                String.Format("Unable to add '{0}' as a child of the bank manager as it can only have bank widgets as children.", widget.GetType().Name));
            if (!(widget is WidgetBank))
            {
                throw new ArgumentException(String.Format("Unable to add '{0}' as a child of the bank manager as it can only have bank widgets as children.", widget.GetType().Name));
            }
            base.AddChild(widget);
        }

        /// <summary>
        /// Removes a child from this group.
        /// </summary>
        /// <param name="widget"></param>
        public override void RemoveChild(IWidget widget)
        {
            // Make sure we are only attempting to remove bank widgets.
            Debug.Assert(widget is WidgetBank,
                String.Format("Unable to remove '{0}' as a child of the bank manager as it can only have bank widgets as children.", widget.GetType().Name));
            if (!(widget is WidgetBank))
            {
                throw new ArgumentException(String.Format("Unable to add '{0}' as a child of the bank manager as it can only have bank widgets as children.", widget.GetType().Name));
            }
            base.RemoveChild(widget);
        }

        /// <summary>
        /// Adds a reference to the fact that this group is in active use.
        /// </summary>
        public override void AddOpenReference()
        {
            // Do nothing
        }

        /// <summary>
        /// 
        /// </summary>
        public override void RemoveOpenReference()
        {
            // Do nothing
        }
        #endregion // Child Management

        #region Message Processing Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        public override bool ProcessCreateMessage(RemotePacketEventArgs e)
        {
            // Bank manager creation packets don't contain any additional data.
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void ProcessMessageCore(RemotePacketEventArgs e)
        {
            switch ((BankCommand)e.Packet.Command)
            {
                // Open file dialog support #1:
                case BankCommand.User0:
                    ProcessOpenFileMessage(e);
                    break;

                // Signal that the client has finished his initialization phase:
                case BankCommand.User2:
                    ProcessInitCompleteMessage(e);
                    break;

                // Time information:
                case BankCommand.User5:
                    ProcessTimingInformationMessage(e);
                    break;

                // Ping response:
                case BankCommand.User6:
                    ProcessPingResponseMessage(e);
                    break;

                // Clipboard copy:
                case BankCommand.User7:
                    ProcessClipboardCopyMessage(e);
                    break;

                // Clipboard clear:
                case BankCommand.User8:
                    ProcessClipboardClearMessage(e);
                    break;

                // Create output window:
                case BankCommand.User14:
                    ProcessCreateOutputWindowMessage(e);
                    break;

                // Sub channel information:
                case BankCommand.User15:
                    ProcessSubChannelInformationMessage(e);
                    break;

                default:
                    base.ProcessMessageCore(e);
                    break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void ProcessOpenFileMessage(RemotePacketEventArgs e)
        {
            // Proxy only message.
            if (!_inProxy)
            {
                e.Packet.ReadToEnd();
                return;
            }

            // TODO
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void ProcessInitCompleteMessage(RemotePacketEventArgs e)
        {
            // TODO
            OpenCount = e.Packet.ReadInt32();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void ProcessTimingInformationMessage(RemotePacketEventArgs e)
        {
            TimingInformation = e.Packet.ReadString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void ProcessPingResponseMessage(RemotePacketEventArgs e)
        {
            // TODO
            e.Packet.ReadUInt32();
            e.Packet.ReadBoolean();
            //uint data = e.Packet.ReadUInt32();
            //bool delayed = e.Packet.ReadBoolean();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void ProcessClipboardCopyMessage(RemotePacketEventArgs e)
        {
            // Proxy only message.
            if (!_inProxy)
            {
                e.Packet.ReadToEnd();
                return;
            }

            // TODO
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void ProcessClipboardClearMessage(RemotePacketEventArgs e)
        {
            // Proxy only message.
            if (!_inProxy)
            {
                e.Packet.ReadToEnd();
                return;
            }

            // TODO
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void ProcessCreateOutputWindowMessage(RemotePacketEventArgs e)
        {
            // TODO
            e.Packet.ReadString();
            e.Packet.ReadString();
            //string channelName = e.Packet.ReadString();
            //string color = e.Packet.ReadString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void ProcessSubChannelInformationMessage(RemotePacketEventArgs e)
        {
            // TODO
            e.Packet.ReadString();
            e.Packet.ReadString();
            //string channelName = e.Packet.ReadString();
            //string subChannelName = e.Packet.ReadString();
        }
        #endregion // Message Processing Methods
    } // BankManager
}
