﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Rag.Core;

namespace RSG.Rag.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    [WidgetGuidAttribute('t', 'g', 'f', 'b')]
    public class WidgetToggleFixedBitSet : WidgetToggle<UInt32>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public WidgetToggleFixedBitSet(uint id, IWidgetResolver resolver, IPacketDispatcher dispatcher)
            : base(id, resolver, dispatcher)
        {
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="packet"></param>
        /// <returns></returns>
        protected override UInt32 ReadMask(RemotePacket packet)
        {
            return packet.ReadUInt32();
        }
        #endregion // Overrides
    } // WidgetToggleFixedBitSet
}
