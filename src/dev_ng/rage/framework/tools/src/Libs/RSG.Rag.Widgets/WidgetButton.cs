﻿using RSG.Rag.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace RSG.Rag.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    [WidgetGuidAttribute('b', 'u', 't', 'n')]
    public class WidgetButton : Widget
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public WidgetButton(uint id, IWidgetResolver resolver, IPacketDispatcher dispatcher)
            : base(id, resolver, dispatcher)
        {
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// Called to press the button.
        /// </summary>
        public async Task Activate()
        {
            RemotePacketBuilder builder = new RemotePacketBuilder((ushort)BankCommand.Changed, this.Guid.ToInteger(), Id);
            await Dispatcher.SendPacketAsync(builder.ToRemotePacket());
        }
        #endregion // Public Methods

        #region Message Processing
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void ProcessMessageCore(RemotePacketEventArgs e)
        {
            switch ((BankCommand)e.Packet.Command)
            {
                // Message sent from RAG to the game.
                case BankCommand.Changed:
                    break;

                default:
                    base.ProcessMessageCore(e);
                    break;
            }
        }
        #endregion // Message Processing
    } // WidgetButton
}
