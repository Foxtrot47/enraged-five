﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Rag.Core;

namespace RSG.Rag.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    [WidgetGuidAttribute('t', 'e', 'x', 't')]
    public class WidgetText : Widget
    {
        #region Enums
        /// <summary>
        /// 
        /// </summary>
        public enum TextDataType
        {
            /// <summary>
            /// 
            /// </summary>
            Bool,

            /// <summary>
            /// 
            /// </summary>
            Float,

            /// <summary>
            /// 
            /// </summary>
            Integer,

            /// <summary>
            /// 
            /// </summary>
            String
        }
        #endregion // Enums

        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        private TextDataType _type;

        /// <summary>
        /// 
        /// </summary>
        private String _text;

        /// <summary>
        /// 
        /// </summary>
        private String _originalText;

        /// <summary>
        /// 
        /// </summary>
        private bool _originalStringSet;

        /// <summary>
        /// 
        /// </summary>
        private String _partialText;

        /// <summary>
        /// 
        /// </summary>
        private uint _maxStringLength;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public WidgetText(uint id, IWidgetResolver resolver, IPacketDispatcher dispatcher)
            : base(id, resolver, dispatcher)
        {
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public TextDataType Type
        {
            get { return _type; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String Text
        {
            get { return _text; }
            set
            {
                if (SetProperty(ref _text, value) && !ProcessingMessage)
                {
                    SendChangedMessage();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public String OriginalText
        {
            get { return _originalText; }
        }

        /// <summary>
        /// 
        /// </summary>
        public uint MaxLength
        {
            get { return _maxStringLength; }
        }
        #endregion // Properties

        #region Message Processing
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void ProcessMessageCore(RemotePacketEventArgs e)
        {
            switch ((BankCommand)e.Packet.Command)
            {
                case BankCommand.Changed:
                    ProcessChangedMessage(e);
                    break;

                default:
                    base.ProcessMessageCore(e);
                    break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        public override bool ProcessCreateMessage(RemotePacketEventArgs e)
        {
            if (!base.ProcessCreateMessage(e))
            {
                return false;
            }

            // ICK!!!!
            _type = (TextDataType)e.Packet.ReadInt32();
            if (_type == TextDataType.String)
            {
                _maxStringLength = e.Packet.ReadUInt32();
            }
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void ProcessChangedMessage(RemotePacketEventArgs e)
        {
            ushort count = e.Packet.ReadUInt16();
            ushort offset = e.Packet.ReadUInt16();
            short packetsRemaining = e.Packet.ReadInt16();

            if (offset == 0)
            {
                _partialText = "";
            }

            switch (_type)
            {
                case TextDataType.Bool:
                    _partialText = e.Packet.ReadBoolean().ToString();
                    break;

                case TextDataType.Float:
                    _partialText = e.Packet.ReadFloat().ToString();
                    break;

                case TextDataType.Integer:
                    _partialText = e.Packet.ReadInt32().ToString();
                    break;

                case TextDataType.String:
                    byte[] currentTextBytes = new byte[count];
                    for (int i = 0; i < count; ++i)
                    {
                        currentTextBytes[i] = e.Packet.ReadByte();
                    }
                    // Take only the non \0 values.
                    _partialText += Encoding.Default.GetString(currentTextBytes.TakeWhile(item => item != 0).ToArray());
                    break;
            }

            // Check if there are any packets remaining to read.
            if (packetsRemaining == 0)
            {
                Text = _partialText;

                if (!_originalStringSet)
                {
                    _originalText = _partialText;
                    _originalStringSet = true;
                }
            }
        }
        #endregion // Message Processing

        #region Message Dispatching
        /// <summary>
        /// 
        /// </summary>
        private void SendChangedMessage()
        {
            if (_type != TextDataType.String)
            {
                RemotePacketBuilder builder = new RemotePacketBuilder((ushort)BankCommand.Changed, this.Guid.ToInteger(), Id);
                builder.Write((ushort)0);
                builder.Write((ushort)0);
                builder.Write((short)0);

                switch (_type)
                {
                    case TextDataType.Bool:
                        builder.Write(Boolean.Parse(Text));
                        break;

                    case TextDataType.Float:
                        builder.Write(Single.Parse(Text));
                        break;

                    case TextDataType.Integer:
                        builder.Write(Int32.Parse(Text));
                        break;
                }

                Dispatcher.SendPacketAsync(builder.ToRemotePacket());
            }
            else
            {
                // 8 = count, offset, remaining packets
                const short payloadSize = RemotePacket.MAX_DATA_SIZE - 6;
                ushort offset = 0;

                // Send the data across in "payload" sized chunks.
                short packetsRemaining = (short)Math.Ceiling((decimal)_maxStringLength / payloadSize);

                // Convert the text to a byte array.
                byte[] stringBytes = new byte[_maxStringLength];
                Encoding.Default.GetBytes(Text, 0, Text.Length, stringBytes, 0);

                while (packetsRemaining > 0)
                {
                    ushort packetDataLength = (ushort)(_maxStringLength - offset > payloadSize ? payloadSize : (short)_maxStringLength - offset);

                    RemotePacketBuilder builder = new RemotePacketBuilder((ushort)BankCommand.Changed, this.Guid.ToInteger(), Id);
                    builder.Write(packetDataLength);
                    builder.Write(offset);
                    builder.Write((short)(packetsRemaining - 1));

                    for (int i = 0; i < packetDataLength; ++i)
                    {
                        builder.Write(stringBytes[offset + i]);
                    }

                    Dispatcher.SendPacketAsync(builder.ToRemotePacket());

                    offset += packetDataLength;
                    --packetsRemaining;
                }
            }
        }
        #endregion // Message Dispatching
    } // WidgetText
}
