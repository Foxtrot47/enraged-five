﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Rag.Core;

namespace RSG.Rag.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    [WidgetGuidAttribute('t', 'g', 'b', 'o')]
    public class WidgetToggleBool : WidgetToggle<bool>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public WidgetToggleBool(uint id, IWidgetResolver resolver, IPacketDispatcher dispatcher)
            : base(id, resolver, dispatcher)
        {
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="packet"></param>
        /// <returns></returns>
        protected override bool ReadMask(RemotePacket packet)
        {
            return packet.ReadBoolean();
        }
        #endregion // Overrides
    } // WidgetToggleBool
}
