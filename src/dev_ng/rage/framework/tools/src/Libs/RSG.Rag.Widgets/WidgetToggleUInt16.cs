﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Rag.Core;

namespace RSG.Rag.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    [WidgetGuidAttribute('t', 'g', 'u', '1')]
    public class WidgetToggleUInt16 : WidgetToggle<UInt16>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public WidgetToggleUInt16(uint id, IWidgetResolver resolver, IPacketDispatcher dispatcher)
            : base(id, resolver, dispatcher)
        {
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="packet"></param>
        /// <returns></returns>
        protected override UInt16 ReadMask(RemotePacket packet)
        {
            return packet.ReadUInt16();
        }
        #endregion // Overrides
    } // WidgetToggleUInt16
}
