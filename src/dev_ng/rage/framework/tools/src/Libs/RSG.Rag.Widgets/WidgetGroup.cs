﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Rag.Core;

namespace RSG.Rag.Widgets
{
    /// <summary>
    /// Group widget.
    /// </summary>
    [WidgetGuidAttribute('g', 'r', 'u', 'p')]
    public class WidgetGroup : WidgetGroupBase
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public WidgetGroup(uint id, IWidgetResolver resolver, IPacketDispatcher dispatcher)
            : base(id, resolver, dispatcher)
        {
        }
        #endregion // Constructor(s)
    } // WidgetGroup
}
