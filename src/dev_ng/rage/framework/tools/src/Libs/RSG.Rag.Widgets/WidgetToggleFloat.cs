﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Rag.Core;

namespace RSG.Rag.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    [WidgetGuidAttribute('t', 'g', 'f', 'l')]
    public class WidgetToggleFloat : WidgetToggle<float>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public WidgetToggleFloat(uint id, IWidgetResolver resolver, IPacketDispatcher dispatcher)
            : base(id, resolver, dispatcher)
        {
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="packet"></param>
        /// <returns></returns>
        protected override float ReadMask(RemotePacket packet)
        {
            return packet.ReadFloat();
        }
        #endregion // Overrides
    } // WidgetToggleFloat
}
