﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.ShortcutMenu.Common
{
    /// <summary>
    /// Interface for objects that can invoke shortcut items.
    /// </summary>
    public interface IShortcutItemInvoker
    {
        /// <summary>
        /// Invokes the particular shortcut item based on it's guid.
        /// </summary>
        /// <param name="guid"></param>
        /// <returns>Whether an item with the specified guid exists.</returns>
        bool InvokeShortcutItem(Guid guid);
    } // IShortcutItemInvoker
}
