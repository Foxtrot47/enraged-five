﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace RSG.ShortcutMenu.Common
{
    /// <summary>
    /// 
    /// </summary>
    [ServiceContract]
    public interface IShortcutMenuService
    {
        /// <summary>
        /// Invokes a particular shortcut item based on it's guid.
        /// </summary>
        /// <param name="guid"></param>
        /// <returns>Whether an item with the specified guid exists.</returns>
        [OperationContract]
        bool InvokeShortcutItem(Guid guid);
    }
}
