﻿using System;
using System.Collections.Generic;
using RSG.Pipeline.Core;

namespace RSG.Pipeline.Engine
{

    /// <summary>
    /// Engine delegates.
    /// </summary>
    public class EngineDelegate : IEngineDelegate
    {
        #region Properties
        /// <summary>
        /// User-data object for PreBuild callback.
        /// </summary>
        public Object PreBuildUserData { get; set; }

        /// <summary>
        /// User-data object for Build callback.
        /// </summary>
        public Object PostBuildUserData { get; set; }

        /// <summary>
        /// Content collection callback for prebuild.  This is invoked AFTER prebuild has run.
        /// </summary>
        public ContentCollectionDelegate PreBuildCallback { get; set; }

        /// <summary>
        /// Content collection callback for postbuild.  This is invoked AFTER build has run.
        /// </summary>
        public ContentCollectionDelegate PostBuildCallback { get; set; }
        #endregion // Properties
    }

} // RSG.Pipeline.Engine namespace
