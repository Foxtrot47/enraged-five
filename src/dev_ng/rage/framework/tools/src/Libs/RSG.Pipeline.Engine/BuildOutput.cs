﻿using System;
using System.Collections.Generic;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Build;

namespace RSG.Pipeline.Engine
{

    /// <summary>
    /// Representation of an Engine's output; groups any errors and warnings
    /// generated whilst the asset is created.
    /// </summary>
    /// Note: this is deliberately internal.  Its internal to keep the ability
    /// to replace the Engine completely.
    /// 
    internal class BuildOutput : IOutput
    {
        #region Properties
        /// <summary>
        /// Time taken for this build to complete.
        /// </summary>
        public TimeSpan BuildTime
        {
            get;
            protected set;
        }

        /// <summary>
        /// Enumerable of IContentNode objects describing what has been built.
        /// </summary>
        public IEnumerable<IContentNode> Items
        {
            get;
            protected set;
        }

        /// <summary>
        /// Errors generated whilst the output was created.
        /// </summary>
        public IEnumerable<String> Errors
        {
            get;
            private set;
        }

        /// <summary>
        /// Warnings generated whilst the output was created.
        /// </summary>
        public IEnumerable<String> Warnings
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="buildTime"></param>
        /// <param name="outputs"></param>
        public BuildOutput(TimeSpan buildTime, IEnumerable<IContentNode> outputs)
            : this(buildTime, outputs, null, null)
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="buildTime"></param>
        /// <param name="outputs"></param>
        /// <param name="errors"></param>
        /// <param name="warnings"></param>
        public BuildOutput(TimeSpan buildTime, IEnumerable<IContentNode> outputs,
            IEnumerable<String> errors, IEnumerable<String> warnings)
        {
            if (null == outputs)
                throw (new ArgumentNullException("outputs"));

            this.BuildTime = buildTime;
            this.Items = new List<IContentNode>(outputs);

            if (null == errors)
                this.Errors = new String[0];
            else
                this.Errors = new List<String>(errors);

            if (null == warnings)
                this.Warnings = new String[0];
            else
                this.Warnings = new List<String>(warnings);
        }
        #endregion // Constructor(s)
    }

} // RSG.Pipeline.Engine namespace
