﻿//---------------------------------------------------------------------------------------------
// <copyright file="Parameter.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2012-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Engine
{
    using System;
    using RSG.Pipeline.Core;
    using RSG.Pipeline.Core.Attributes;

    /// <summary>
    /// Engine Parameter Constants.
    /// </summary>
    internal class Parameter
    {
        /// <summary>
        /// Enables the Perforce sync progress bar user interface.
        /// </summary>
        [ParameterScope(ParameterScope.Engine)]
        [ParameterType(typeof(bool))]
        public const String EnableSyncProgressBar = "Enable Sync Progress Bar";

        /// <summary>
        /// Determines when a progress bar is shown in the user interface, based on a number of files.
        /// </summary>
        [ParameterScope(ParameterScope.Engine)]
        [ParameterType(typeof(int))]
        public const String SyncProgressThreshold = "Sync Progress Threshold";

        /// <summary>
        /// Determines how precise the progress bar is, by splitting up this number of files into groups to sync each.
        /// </summary>
        [ParameterScope(ParameterScope.Engine)]
        [ParameterType(typeof(int))]
        public const String SyncProgressFileGroupSize = "Sync Progress File Group Size";
    }

} // RSG.Pipeline.Engine namespace
