﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using RSG.Pipeline.Core;
using RSG.Pipeline.Content;

namespace RSG.Pipeline.Engine.Caching
{

    /// <summary>
    /// Content object cache manager class.
    /// </summary>
    /// This class is responsible for caching the IProcess 'Build' status and
    /// IContentNode modified times.
    /// 
    /// Additional content object properties may be added over time.
    /// 
    /// This replaces the previous Engine.BuildTable.
    /// 
    /// DHM TODO: add dynamic Directory content EvaluateInputs caching.
    /// 
    internal sealed class ContentObjectCacheManager
    {
        #region Properties
        /// <summary>
        /// Process build table entry count.
        /// </summary>
        public int ProcessBuildEntryCount
        {
            get { return (m_ProcessBuildTable.Count); }
        }

        /// <summary>
        /// Content-node modified time entry count.
        /// </summary>
        public int NodeModifiedTimeEntryCount
        {
            get { return (m_NodeModifiedCacheTable.Count); }
        }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Engine process build table (cleared each Build; private state tracking).
        /// </summary>
        private IDictionary<IProcess, bool> m_ProcessBuildTable;

        /// <summary>
        /// Engine content-node modified times cache (cleared each time disk is written,
        /// at end of Prebuild(s) (for syncs) and Build).
        /// </summary>
        /// This is an optimisation to prevent disk thrashing.
        /// 
        private IDictionary<IContentNode, DateTime> m_NodeModifiedCacheTable;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ContentObjectCacheManager()
        {
            this.m_ProcessBuildTable = new Dictionary<IProcess, bool>();
            this.m_NodeModifiedCacheTable = new Dictionary<IContentNode, DateTime>();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Update process build entry table.
        /// </summary>
        /// <param name="process"></param>
        /// <param name="build"></param>
        public void ModifyProcessBuildEntry(IProcess process, bool build)
        {
            this.m_ProcessBuildTable[process] = build;
        }

        /// <summary>
        /// Read process build entry table.
        /// </summary>
        /// <param name="process"></param>
        /// <returns></returns>
        public bool ReadProcessBuildEntry(IProcess process)
        {
            Debug.Assert(this.HasProcessBuildEntry(process),
                "No process entry found!  Internal error.");
            if (!this.HasProcessBuildEntry(process))
                return (false);

            return (this.m_ProcessBuildTable[process]);
        }

        /// <summary>
        /// Determine whether process build table has an entry for the process.
        /// </summary>
        /// <param name="process"></param>
        /// <returns></returns>
        public bool HasProcessBuildEntry(IProcess process)
        {
            return (this.m_ProcessBuildTable.ContainsKey(process));
        }

        /// <summary>
        /// Update content-nodes modified time entry table (re-evaluates modified times).
        /// </summary>
        /// <param name="node"></param>
        /// <param name="force"></param>
        public void CacheNodeModifiedTimeEntry(IEnumerable<IContentNode> nodes, bool force)
        {
            IEnumerable<IFilesystemNode> filesystemNodes = nodes.OfType<IFilesystemNode>();
            foreach (IFilesystemNode fsNode in filesystemNodes)
            {
                if (!force && this.m_NodeModifiedCacheTable.ContainsKey(fsNode))
                    continue;
                this.m_NodeModifiedCacheTable[fsNode] = fsNode.GetModifiedTimestamp();
            }
        }

        /// <summary>
        /// Update content-node modified time entry table (re-evaluates modified time).
        /// </summary>
        /// <param name="node"></param>
        public void CacheNodeModifiedTimeEntry(IContentNode node, bool force)
        {
            if (!(node is IFilesystemNode))
                return;
            if (!force && this.m_NodeModifiedCacheTable.ContainsKey(node))
                return;

            this.m_NodeModifiedCacheTable[node] = (node as IFilesystemNode).GetModifiedTimestamp();
        }

        /// <summary>
        /// Read content-node modified time entry table.
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public DateTime ReadNodeModifiedTimeEntry(IContentNode node)
        {
            Debug.Assert(this.HasNodeModifiedTimeEntry(node),
                "No content-node modified time found!  Internal error.");
            if (!this.HasNodeModifiedTimeEntry(node))
                return (DateTime.MinValue);

            return (this.m_NodeModifiedCacheTable[node]);
        }

        /// <summary>
        /// Determine whether content-node modified time table has an entry for the
        /// node.
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public bool HasNodeModifiedTimeEntry(IContentNode node)
        {
            return (this.m_NodeModifiedCacheTable.ContainsKey(node)); 
        }

        /// <summary>
        /// Clear all content object cache tables.
        /// </summary>
        public void Clear()
        {
            this.m_ProcessBuildTable.Clear();
            this.m_NodeModifiedCacheTable.Clear();
        }

        /// <summary>
        /// Clear content-node modified time table.
        /// </summary>
        public void ClearNodeModifiedTimeEntries()
        {
            this.m_NodeModifiedCacheTable.Clear();
        }
        #endregion // Controller Methods
    }

} // RSG.Pipeline.Engine.Caching namespace
