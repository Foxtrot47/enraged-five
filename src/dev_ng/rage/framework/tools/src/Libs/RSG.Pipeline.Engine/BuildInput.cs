﻿using System;
using System.Collections.Generic;
using RSG.Base.Configuration;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Build;
using RSG.Pipeline.Content;

namespace RSG.Pipeline.Engine
{

    /// <summary>
    /// Engine build unit.
    /// </summary>
    public class BuildInput : IInput
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public IBranch Branch
        {
            get;
            private set;
        }

        /// <summary>
        /// Build type to be done.
        /// </summary>
        public BuildType Types
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public IContentTree RawTree
        {
            get;
            private set;
        }

        /// <summary>
        /// Processes to be prebuilt (raw content-tree).
        /// </summary>
        public ICollection<IProcess> RawProcesses
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public IContentTree PrebuiltTree
        {
            get;
            private set;
        }

        /// <summary>
        /// Processes to be built (preprocessed in engine).
        /// </summary>
        public ICollection<IProcess> PrebuiltProcesses
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="branch"></param>
        public BuildInput(IBranch branch)
            : this(branch, BuildType.All, new List<IProcess>())
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="type"></param>
        public BuildInput(IBranch branch, BuildType type)
            : this(branch, type, new List<IProcess>())
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="type"></param>
        /// <param name="items"></param>
        public BuildInput(IBranch branch, BuildType type, IEnumerable<IProcess> items)
        {
            this.Branch = branch;
            this.Types = type;
            this.RawProcesses = new List<IProcess>(items);
            this.PrebuiltTree = Factory.CreateEmptyTree(this.Branch);
            this.PrebuiltProcesses = new List<IProcess>();
        }
        #endregion // Constructor(s)
    }

} // RSG.Pipeline.Engine namespace
