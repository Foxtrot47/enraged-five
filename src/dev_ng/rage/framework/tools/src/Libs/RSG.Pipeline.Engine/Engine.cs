﻿//---------------------------------------------------------------------------------------------
// <copyright file="Engine.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2012-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Engine
{
	using System;
	using System.Collections.Generic;
	using System.Diagnostics;
	using System.Linq;
	using System.Reflection;
	using System.Text.RegularExpressions;
	using System.Threading;
	using System.Threading.Tasks;
	using P4API;
	using RSG.Base.Configuration;
	using RSG.Base.Extensions;
	using RSG.Base.Logging;
	using RSG.Base.Logging.Universal;
	using RSG.Base.Tasks;
	using RSG.Base.Xml;
	using RSG.Pipeline.Content;
	using RSG.Pipeline.Content.Algorithm;
	using RSG.Pipeline.Core;
	using RSG.Pipeline.Core.Build;
	using RSG.Pipeline.Engine.Caching;
	using RSG.Pipeline.Services;
	using RSG.Pipeline.Services.Platform;
	using RSG.SourceControl.Perforce;
	using SIO = System.IO;
	using XGE = RSG.Interop.Incredibuild.XGE;

	/*! \mainpage
	 *
	 * \section intro_sec Purpose
	 *
	 * The RSG.Pipeline.Engine assembly provides the content engine that is the
	 * entry point for the asset pipeline.  The engine drives the asset pipeline
	 * processes; transforming the inputs to outputs and handling the 
	 * dependencies between processes.
	 * 
	 * The main Engine class is intended to be able to coexist with other 
	 * engines as required (e.g. for a significant optimisation or for developing
	 * one in parallel).
	 * 
	 * \section unit_test Unit Tests
	 * 
	 * There are a few very simple unit tests available in this assembly.
	 */

	/// <summary>
	/// The processor engine; controlling the individual processors passing
	/// through content as defined in the content-tree.
	/// </summary>
	public sealed class Engine : IEngine
	{
		#region Constants
		/// <summary>
		/// Log context.
		/// </summary>
		private static readonly String LOG_CTX = "Engine";

		/// <summary>
		/// Mutex name for locking engine controller operations.
		/// </summary>
		private static readonly String MUTEX_NAME = "Global\\RSG_AP3_ENGINE";

		/// <summary>
		/// Mutex timeout (in milliseconds).
		/// </summary>
		private static int MUTEX_TIMEOUT = 1500;

		/// <summary>
		/// XGE task name output logging tag.
		/// </summary>
		private static readonly String TASK_NAME_CONTEXT = @"TASK: (?<taskname>[A-Z_0-9\[\]\(\)]*)";
		private static readonly Regex REGEX_TASK_NAME_CONTEXT = new Regex(TASK_NAME_CONTEXT,
			RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase);

		/// <summary>
		/// Maximum number of 
		/// </summary>
		private static readonly int PREBUILD_MAX_ITERATIONS = 100;
		#endregion // Constants

		#region Properties
		/// <summary>
		/// Command options service.
		/// </summary>
		public CommandOptions Options
		{
			get;
			private set;
		}

		/// <summary>
		/// Pipeline configuration object.
		/// </summary>
		public IConfig Config
		{
			get;
			private set;
		}

		/// <summary>
		/// Project branch object.
		/// </summary>
		public IBranch Branch
		{
			get;
			private set;
		}

		/// <summary>
		/// Current engine flags of operation.
		/// </summary>
		public EngineFlags Flags
		{
			get;
			private set;
		}

		/// <summary>
		/// Platforms to covert (defaults to installer-defined platforms).
		/// </summary>
		public IEnumerable<RSG.Platform.Platform> Platforms
		{
			get;
			private set;
		}

		/// <summary>
		/// Available processors.
		/// </summary>
		public IProcessorCollection Processors
		{
			get;
			private set;
		}

		/// <summary>
		/// Engine content-object cache manager.
		/// </summary>
		private ContentObjectCacheManager ContentObjectCacheManager
		{
			get;
			set;
		}

		/// <summary>
		/// Stopwatch used to track internal modified time caching (ContentObjectCacheManager).
		/// </summary>
		private Stopwatch m_swContentModifiedTimeAccess;

		/// <summary>
		/// Callback invoked during prebuild on processor output
		/// </summary>
		private IEngineDelegate EngineDelegate
		{
			get;
			set;
		}
		#endregion // Properties

		#region Member Data
		/// <summary>
		/// Parameter dictionary.
		/// </summary>
		private ParameterDictionary _parameters;

		/// <summary>
		/// Perforce Sync Progress member data.
		/// </summary>
		private bool _enableSyncProgress = false;
		private int _syncProgressThreshold = 100;
		private int _syncProgressFileGroupSize = 100;
		#endregion // Properties

		#region Constructor(s)
		/// <summary>
		/// Constructor; default flags and processor collection.
		/// </summary>
		/// <param name="options"></param>
#warning DHM FIX ME: Options?  And config?  And project?  And branch?  Really?
		public Engine(CommandOptions options, IConfig config, IProject project, IBranch branch)
			: this(options, config, project, branch, EngineFlags.Default, null, null, null)
		{
		}

		/// <summary>
		/// Constructor; user-specified flags and default processor collection.
		/// </summary>
		/// <param name="options"></param>
		/// <param name="flags"></param>
#warning DHM FIX ME: Options?  And config?  And project?  And branch?  Really?
		public Engine(CommandOptions options, IConfig config, IProject project, IBranch branch, EngineFlags flags)
			: this(options, config, project, branch, flags, null, null, null)
		{
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="options">Pipeline option service.</param>
		/// <param name="config"></param>
		/// <param name="project"></param>
		/// <param name="branch"></param>
		/// <param name="flags">Engine flags.</param>
		/// <param name="processorPredecate"></param>
		/// <param name="engineDelegate"></param>
		/// <param name="platforms"></param>
#warning DHM FIX ME: Options?  And config?  And project?  And branch?  Really?
		public Engine(CommandOptions options, IConfig config, IProject project, IBranch branch, EngineFlags flags, 
			Func<IProcessor, bool> processorPredecate, IEngineDelegate engineDelegate,
			IEnumerable<RSG.Platform.Platform> platforms)
		{
			IUniversalLog log = LogFactory.ApplicationLog;
			log.Message("Initialising Pipeline Engine for {0}, branch: {1}, flags: {2}.",
				project.FriendlyName, branch.Name, flags);

			this.Options = options;
			this.Branch = branch;
			this.Config = config;
			this.Flags = flags;
			this.ContentObjectCacheManager = new ContentObjectCacheManager();
			this.m_swContentModifiedTimeAccess = new Stopwatch();
			this.EngineDelegate = engineDelegate;

			// Set platforms override.
			if (null == platforms)
				this.Platforms = PlatformProcessBuilder.GetInstallerEnabledPlatforms(branch);
			else
				this.Platforms = platforms;

			if (null == processorPredecate)
				this.Processors = new ProcessorCollection(this.Config, processor => true);
			else
				this.Processors = new ProcessorCollection(this.Config, processorPredecate);

			if (!this.Flags.HasFlag(EngineFlags.Lock))
				log.WarningCtx(LOG_CTX, "Engine created without built-in locking.  This is unusual and not recommended.");
			
			LoadParameters(log, this.Branch);
		}
		#endregion // Constructor(s)

		#region Controller Methods
		/// <summary>
		/// Prebuild content; prebuild the passed in input filenames invoking
		/// the Processor's prebuild as required.  This interface allows external
		/// code to utilise the IProcessor dependency content-tree transforms.
		/// </summary>
		/// <param name="filenames"></param>
		/// <param name="outputs"></param>
		/// <param name="prebuild_time"></param>
		/// <returns></returns>
		public bool Prebuild(IEnumerable<String> filenames, out IEnumerable<IOutput> outputs,
			out TimeSpan prebuild_time)
		{
			IUniversalLog buildLog = LogFactory.CreateUniversalLog(LOG_CTX);
			IEngineParameters param = new EngineParameters(this.Branch, this.Flags,
				this.EngineDelegate, BuildType.All, buildLog, this.Platforms);
			bool result = Prebuild(param, filenames, out outputs, out prebuild_time);

			if (buildLog.HasErrors)
				result = false;
			LogFactory.CloseUniversalLog(buildLog);

			return (result);
		}

		/// <summary>
		/// Prebuild content; prebuild the passed in input filenames invoking
		/// the Processor's prebuild as required.  This interface allows external
		/// code to utilise the IProcessor dependency content-tree transforms.        
		/// </summary>
		/// <param name="param"></param>
		/// <param name="filenames"></param>
		/// <param name="outputs"></param>
		/// <param name="prebuild_time"></param>
		/// <returns></returns>
		public bool Prebuild(IEngineParameters param, IEnumerable<String> filenames, out IEnumerable<IOutput> outputs,
			out TimeSpan prebuild_time)
		{
			if (this.Flags.HasFlag(EngineFlags.Lock))
			{
				using (Mutex m = new Mutex(true, MUTEX_NAME))
				{
					WaitForMutex(param.Log, m);
					return (PrebuildImpl(param, filenames, out outputs, out prebuild_time));
				}
			}
			else
			{
				return (PrebuildImpl(param, filenames, out outputs, out prebuild_time));
			}
		}

		/// <summary>
		/// Prebuild content; prebuild the passed in input invoking the 
		/// Processor's prebuild as required.  This interface allows external
		/// code to utilise the IProcessor dependency content-tree transforms.
		/// </summary>
		/// <param name="input"></param>
		/// <param name="outputs"></param>
		/// <param name="prebuild_time"></param>
		/// <returns></returns>
		public bool Prebuild(ref IInput input, out IEnumerable<IOutput> outputs,
			out TimeSpan prebuild_time)
		{
			IUniversalLog buildLog = LogFactory.CreateUniversalLog(LOG_CTX);
			IEngineParameters param = new EngineParameters(this.Branch, this.Flags,
				this.EngineDelegate, BuildType.All, buildLog, this.Platforms);

			bool result;
			if (this.Flags.HasFlag(EngineFlags.Lock))
			{
				using (Mutex m = new Mutex(true, MUTEX_NAME))
				{
					WaitForMutex(param.Log, m);
					result = PrebuildImpl(param, ref input, out outputs, out prebuild_time);
				}
			}
			else
			{
				result = PrebuildImpl(param, ref input, out outputs, out prebuild_time);
			}

			if (buildLog.HasErrors)
				result = false;
			LogFactory.CloseUniversalLog(buildLog);
			return (result);
		}

		/// <summary>
		/// Build content; build the passed in input filenames invoking the
		/// Processors as required.
		/// </summary>
		/// <param name="filenames"></param>
		/// <param name="outputs"></param>
		/// <param name="build_time"></param>
		/// <returns></returns>
		public bool Build(IEnumerable<String> filenames, out IEnumerable<IOutput> outputs,
			out TimeSpan build_time)
		{
			return (this.Build(filenames, BuildType.All, out outputs, out build_time));
		}

		/// <summary>
		/// Build content; build the passed in input filenames invoking the
		/// Processors as required.
		/// </summary>
		/// <param name="filenames"></param>
		/// <param name="types"></param>
		/// <param name="outputs"></param>
		/// <param name="build_time"></param>
		/// <returns></returns>
		public bool Build(IEnumerable<String> filenames, BuildType types,
			out IEnumerable<IOutput> outputs, out TimeSpan build_time)
		{
			IUniversalLog buildLog = LogFactory.CreateUniversalLog(LOG_CTX);
			IEngineParameters param = new EngineParameters(this.Branch, this.Flags, 
				this.EngineDelegate, types, buildLog, this.Platforms);

			this.ContentObjectCacheManager.Clear();
			bool result;
			IEnumerable<String> normalisedFilenames = filenames.Select(f => f.ToLower());
			if (this.Flags.HasFlag(EngineFlags.Lock))
			{
				using (Mutex m = new Mutex(true, MUTEX_NAME))
				{
					WaitForMutex(buildLog, m);
					result = BuildImpl(param, normalisedFilenames, types, out outputs, out build_time);
				}
			}
			else
			{
				result = BuildImpl(param, normalisedFilenames, types, out outputs, out build_time);
			}

			if (buildLog.HasErrors)
				result = false;
			LogFactory.CloseUniversalLog(buildLog);
			return (result);
		}

		/// <summary>
		/// Build content; build the passed input content dat, invoking the
		/// processors as required.  This is the only IEngine interface method
		/// and handles all prebuild stages of the processors.
		/// </summary>
		/// <param name="input"></param>
		/// <param name="outputs"></param>
		/// <param name="build_time">build time.</param>
		/// <returns>true if no errors; false otherwise</returns>
		/// 
		public bool Build(ref IInput input, out IEnumerable<IOutput> outputs, out TimeSpan build_time)
		{
			IUniversalLog buildLog = LogFactory.CreateUniversalLog(LOG_CTX);
			IEngineParameters param = new EngineParameters(this.Branch, this.Flags,
				this.EngineDelegate, input.Types, buildLog, this.Platforms);

			this.ContentObjectCacheManager.Clear();
			bool result;
			if (this.Flags.HasFlag(EngineFlags.Lock))
			{
				using (Mutex m = new Mutex(true, MUTEX_NAME))
				{
					WaitForMutex(buildLog, m);
					result = BuildImpl(param, ref input, out outputs, out build_time);
				}
			}
			else
			{
				result = BuildImpl(param, ref input, out outputs, out build_time);
			}

			if (buildLog.HasErrors)
				result = false;
			LogFactory.CloseUniversalLog(buildLog);
			return (result);
		}

		/// <summary>
		/// Clean content; run through the input content data cleaning all 
		/// output files as required.
		/// </summary>
		/// <param name="input"></param>
		/// <param name="clean_time"></param>
		/// <returns></returns>
		public bool Clean(IInput input, out TimeSpan clean_time)
		{
			IUniversalLog cleanLog = LogFactory.CreateUniversalLog(LOG_CTX);
			bool result;
			if (this.Flags.HasFlag(EngineFlags.Lock))
			{
				using (Mutex m = new Mutex(true, MUTEX_NAME))
				{
					WaitForMutex(cleanLog, m);
					result = CleanImpl(cleanLog, input, out clean_time);
				}
			}
			else
			{
				result = CleanImpl(cleanLog, input, out clean_time);
			}

			if (cleanLog.HasErrors)
				result = false;
			LogFactory.CloseUniversalLog(cleanLog);
			return (result);
		}
		#endregion // Controller Methods

		#region Private Methods
		/// <summary>
		/// Block until engine lock can be acquired.
		/// </summary>
		/// <param name="log"></param>
		/// <param name="m"></param>
		private void WaitForMutex(IUniversalLog log, Mutex m)
		{
			try
			{
				// Wait until its safe to enter.
				while (!m.WaitOne(TimeSpan.FromMilliseconds(MUTEX_TIMEOUT)))
				{
					log.Message("Waiting for Engine lock.  Do you have another build running?");
				}
			}
			catch (AbandonedMutexException)
			{
				// Other process died.
			}

			log.Message("Acquired Engine lock.");
		}
		
		/// <summary>
		/// Prebuild content; implementation method with filename lookup for 
		/// being wrapped by optional locking mechanism.
		/// </summary>
		/// <param name="param"></param>
		/// <param name="filenames"></param>
		/// <param name="outputs"></param>
		/// <param name="prebuild_time"></param>
		/// <returns></returns>
		private bool PrebuildImpl(IEngineParameters param, IEnumerable<String> filenames, 
			out IEnumerable<IOutput> outputs, out TimeSpan prebuild_time)
		{
			IInput input = new BuildInput(this.Branch, BuildType.All);
			IContentTree loadedTree = null;
			try
			{
				if (this.Flags.HasFlag(EngineFlags.LoadContent))
					loadedTree = Factory.CreateTree(this.Branch);
				else
					loadedTree = Factory.CreateEmptyTree(this.Branch);
			}
			catch (ContentException)
			{
				param.Log.ErrorCtx(LOG_CTX, "Content-Tree load failed.  See log for details.");
				outputs = new List<IOutput>();
				prebuild_time = TimeSpan.Zero;
				return (false);
			}

			IEnumerable<IProcess> resolvedProcesses = ProcessResolver.Resolve(
				param, loadedTree, filenames);
			if (!resolvedProcesses.Any())
			{
				param.Log.ErrorCtx(LOG_CTX, "No processes resolved for input file list.  Aborting.");
				outputs = new List<IOutput>();
				prebuild_time = new TimeSpan();
				return (false);
			}

			input.RawProcesses.AddRange(resolvedProcesses);
			return (this.PrebuildImpl(param, ref input, out outputs, out prebuild_time));
		}

		/// <summary>
		/// Prebuild content; implementation method for being wrapped by optional
		/// locking mechanism.
		/// </summary>
		/// <param name="param"></param>
		/// <param name="input"></param>
		/// <param name="outputs"></param>
		/// <param name="prebuild_time"></param>
		/// <returns></returns>
		private bool PrebuildImpl(IEngineParameters param, ref IInput input, 
			out IEnumerable<IOutput> outputs, out TimeSpan prebuild_time)
		{
			bool result = true;
			Stopwatch sw = new Stopwatch();
			outputs = new List<IOutput>();

			// Warn the user if no targets are enabled; only a warning because
			// we may want to do this to process data but not resource it.
			bool enabledTarget = (PlatformProcessBuilder.GetEnabledTargets(param).Any());
			if (!enabledTarget)
				param.Log.WarningCtx(LOG_CTX, "No target platforms enabled in tools configuration.");

			sw.Start();
			result &= PrebuildImpl(param, ref input, out outputs);
			sw.Stop();
			prebuild_time = sw.Elapsed;

			return (result);
		}

		/// <summary>
		/// Prepare content; pass of the content and invoke processors prebuild.
		/// Returns a further list of content that needs to be synced, built or 
		/// read from a cache.
		/// </summary>
		/// <param name="param"></param>
		/// <param name="input"></param>
		/// <returns></returns>
		/// Note: this handles the case where a prebuilt process is passed back 
		/// in the resultant processes enumerable.
		/// 
		private bool PrebuildImpl(IEngineParameters param, ref IInput build, out IEnumerable<IOutput> outputs)
		{
			Stopwatch sw = Stopwatch.StartNew();
			outputs = new List<IOutput>();
#warning DHM FIX ME: caching mechanism of the prepared content-tree.
			IContentTree tree = build.PrebuiltTree;
			bool result = true;

			using (ProfileContext ctx = new ProfileContext(param.Log, LOG_CTX, "Prebuilding processes."))
			{
				int prebuildIteration = 0;
				ISet<IProcess> processes = new HashSet<IProcess>(build.RawProcesses);
				IList<IProcess> processesToPrebuild = processes.Where(p =>
					ProcessState.Initialised == p.State).ToList();
				while (processesToPrebuild.Count > 0)
				{
					// We now optimise the invocation of IProcessor.Prebuild to
					// attempt to prebuild all applicable processes for a given
					// processor.  This should allow some processors to optimise
					// their prebuild methods (e.g. maps for lod dependencies).
					IDictionary<IProcess, IEnumerable<IContentNode>> dependenciesToSync =
						new Dictionary<IProcess, IEnumerable<IContentNode>>();

					//*********************************************************************************
					// DHM TODO HACK: fix properly such that we group IProcess' together
					// by IProcessor but also taking into account dependencies between
					// processes.
					// This ensures that the Character OutfitPreProcess Prebuild gets
					// executed first.  We will change this to ensure that this happens
					// for any IProcess that has dependencies later on and may actually
					// fix quite a few issues people have been having creating processors.
					IEnumerable<IProcess> characterOutfitPreProcesses = processesToPrebuild.Where(p =>
						p.ProcessorClassName.Equals("RSG.Pipeline.Processor.Character.OutfitPreProcess")).ToList();
					if (characterOutfitPreProcesses.Any())
					{
						// Prebuild first.
						IDictionary<IProcess, IEnumerable<IContentNode>> syncDependencies;
						IEnumerable<IProcess> resultantProcesses;

						IProcessor processor = this.Processors.GetProcessor(characterOutfitPreProcesses.First().ProcessorClassName);
						result &= processor.Prebuild(param, characterOutfitPreProcesses, this.Processors,
							tree, out syncDependencies, out resultantProcesses);
						if (syncDependencies.Any())
							dependenciesToSync.AddRange(syncDependencies);

						// Verify all resultant processes.
						bool resultantProcessesOk = VerifyPrebuildResultantProcesses(param, processor, 
							characterOutfitPreProcesses, resultantProcesses);

						result &= resultantProcessesOk;
						if (!resultantProcessesOk)
							return (result);

						// Add our initial prebuilt processes to our collection.
						processes.AddRange(resultantProcesses);

						if (!result)
						{
							param.Log.Error("Errors during HACK Character Outfit PreProcess.");
							return (false);
						}

						// Remove Character OutfitPreProcess processes.
						foreach (IProcess p in characterOutfitPreProcesses)
							processesToPrebuild.Remove(p);
					}
					// DHM END TODO HACK
					//*********************************************************************************

					// Construct the per-processor dictionary.
					foreach (IProcessor processor in this.Processors)
					{
						// DHM TODO HACK: see above - skip our OutfitPreProcess.
						if (processor.Name.Equals("RSG.Pipeline.Processor.Character.OutfitPreProcess"))
							continue;
						// DHM END TODO HACK
						IEnumerable<IProcess> procProcesses = processesToPrebuild.Where(p => 
							p.ProcessorClassName.Equals(processor.Name)).ToList();
						if (!procProcesses.Any())
							continue; // Skip for processors with no processes.

						IDictionary<IProcess, IEnumerable<IContentNode>> syncDependencies;
						IEnumerable<IProcess> resultantProcesses;

						result &= processor.Prebuild(param, procProcesses, this.Processors,
							tree, out syncDependencies, out resultantProcesses);
						if (syncDependencies.Any())
							dependenciesToSync.AddRange(syncDependencies);

						// Verify all resultant processes.
						bool resultantProcessesOk = VerifyPrebuildResultantProcesses(param, processor, procProcesses, resultantProcesses);

						result &= resultantProcessesOk;
						if (!resultantProcessesOk)
							return (result);

						// Add our initial prebuilt processes to our collection.
						processes.AddRange(resultantProcesses);

						if (!result)
						{
							param.Log.Error("Errors during initial engine prebuild from processor: {0}.",
								processor.Name);
							return (false);
						}
					}
					// Sync dependencies for this complete pass at processes.
					if ((dependenciesToSync.Count > 0) && (param.Flags.HasFlag(EngineFlags.SyncDependencies)))
						SyncDependencies(param, dependenciesToSync);
					else if (dependenciesToSync.Count > 0)
						param.Log.WarningCtx(LOG_CTX, "Perforce dependency sync disabled; {0} dependencies will not be synced.",
							dependenciesToSync.Count);

					processesToPrebuild = processes.Where(p =>
						(ProcessState.Initialised == p.State) ||
						(ProcessState.Prebuilding == p.State)).ToList();
					param.Log.Message("Engine Prebuild iteration {0}: {1} processes left.",
						prebuildIteration++, processesToPrebuild.Count);

					// Infinite recursion detection; for misbehaving processors that
					// don't maintain IProcess state correctly.  If we hit the
					// maximum number of prebuild iterations then we abort.
					if (prebuildIteration > PREBUILD_MAX_ITERATIONS)
					{
						param.Log.Error("Engine Prebuild maximum iterations exceeded ({0}).  Internal error; misbehaving processors.  Aborting.",
							PREBUILD_MAX_ITERATIONS);
						return (false);
					}
				}

				IEnumerable<IProcess> prebuiltProcesses = processes.Where(p => ProcessState.Prebuilt == p.State);
				build.PrebuiltProcesses.AddRange(prebuiltProcesses);
			
				// Set our outputs.
				sw.Stop();
				List<IOutput> outs = new List<IOutput>();
				foreach (IProcess p in prebuiltProcesses)
				{
					IEnumerable<IContentNode> outnodes = p.Outputs;
					if (outnodes.Any())
					{
						IOutput output = new BuildOutput(sw.Elapsed, outnodes);
						outs.Add(output);
					}
				}
				outputs = outs;
			 
				param.Log.Message("Prebuild complete: {0} processes to build.", prebuiltProcesses.Count());
			}
			
#if false // DEBUG
			// Determine whether any processes share outputs.  This is not supported
			// because of the up-front dependency determination.
			using (ProfileContext ctx = new ProfileContext(param.Log, "Checking for duplicate process outputs: {0} processes.",
				prebuiltProcesses.Count()))
			{
				foreach (IProcess process in prebuiltProcesses)
				{
					foreach (IProcess p in prebuiltProcesses)
					{
						if (process == p)
							continue;

						IEnumerable<IContentNode> sharedOutputs = process.Outputs.Union(p.Outputs);
						if (sharedOutputs.Any())
						{
							param.Log.ToolErrorCtx(LOG_CTX, "Internal Processor Error; two or more processes share outputs:");
							sharedOutputs.OfType<IFilesystemNode>().ForEach(o => param.Log.ToolErrorCtx(LOG_CTX, "\t{0}", o.AbsolutePath));
							result = false;
							break;
						}
					}
				}
			}
#endif // DEBUG

			return (result);
		}

		/// <summary>
		/// Build content; implementation function for being wrapped by optional
		/// locking mechanism.
		/// </summary>
		/// <param name="param"></param>
		/// <param name="filenames"></param>
		/// <param name="types"></param>
		/// <param name="outputs"></param>
		/// <param name="build_time"></param>
		/// <returns></returns>
		private bool BuildImpl(IEngineParameters param, IEnumerable<String> filenames, 
			BuildType types, out IEnumerable<IOutput> outputs, out TimeSpan build_time)
		{
			IInput input = new BuildInput(this.Branch, types);
			IContentTree loadedTree = null;
			try
			{
				if (this.Flags.HasFlag(EngineFlags.LoadContent))
					loadedTree = Factory.CreateTree(this.Branch);
				else
					loadedTree = Factory.CreateEmptyTree(this.Branch);
			}
			catch (ContentException)
			{
				param.Log.ErrorCtx(LOG_CTX, "Content-Tree load failed.  See log for details.");
				outputs = new List<IOutput>();
				build_time = TimeSpan.Zero;
				return (false);
			}

			IEnumerable<IProcess> resolvedProcesses = ProcessResolver.ResolveProcessRecursive(
				param, loadedTree, filenames);
			if (!resolvedProcesses.Any())
			{
				param.Log.ErrorCtx(LOG_CTX, "No processes resolved for input file list.  Aborting.");
				outputs = new List<IOutput>();
				build_time = new TimeSpan();
				return (false);
			}

			input.RawProcesses.AddRange(resolvedProcesses);
			bool result = this.Build(ref input, out outputs, out build_time);

			return (result);
		}

		/// <summary>
		/// Build content; implementation function for being wrapped by optional
		/// locking mechanism.
		/// </summary>
		/// <param name="param"></param>
		/// <param name="input"></param>
		/// <param name="outputs"></param>
		/// <param name="build_time"></param>
		/// <returns></returns>
		private bool BuildImpl(IEngineParameters param, ref IInput input, 
			out IEnumerable<IOutput> outputs, out TimeSpan build_time)
		{
			bool result = true;
			Stopwatch sw = new Stopwatch();

			// Warn the user if no targets are enabled; only a warning because
			// we may want to do this to process data but not resource it.
			bool enabledTarget = (PlatformProcessBuilder.GetEnabledTargets(param).Any());
			if (!enabledTarget)
				param.Log.WarningCtx(LOG_CTX, "No target platforms enabled in tools configuration.");

			sw.Start();
			result &= PrebuildImpl(param, ref input, out outputs);

			// Populate output and invoke prebuild callback if one is set 
			ICollection<IContentNode> collectedOutputs = new List<IContentNode>();
			foreach (IProcess process in input.PrebuiltProcesses)
			{
				collectedOutputs.AddRange(process.Outputs);
			}
			
			build_time = sw.Elapsed;
			// Flo: PrebuildImpl does a new List<IOutput>. So it *is* a List<T>.
			((List<IOutput>)outputs).Add(new BuildOutput(build_time, collectedOutputs));
			if (param.EngineDelegate != null && param.EngineDelegate.PreBuildCallback != null)
			{
				if (!param.EngineDelegate.PreBuildCallback(this.Branch, collectedOutputs, param.EngineDelegate.PreBuildUserData))
				{
					param.Log.ErrorCtx(LOG_CTX, "Prebuild callback failed.");
				}
			}

			if (this.Flags.HasFlag(EngineFlags.Rebuild))
				result &= Clean(param, input.PrebuiltProcesses);

			IEnumerable<IProcess> failedProcesses;
			result &= Build(param, input, out outputs, out failedProcesses);
#warning DHM FIX ME: not sure whether to clean outputs for failed processes before or after this callback!
#if false // CLEAN currently disabled.  Want to migrate to per-file error clean.
			if (this.Flags.HasFlag(EngineFlags.CleanErroredOutputs) &&
				failedProcesses.Any())
			{
				Engine.Log.MessageCtx(LOG_CTX, "Cleaning outputs for {0} failed processes.",
					failedProcesses.Count());
				result &= Clean(param, failedProcesses);
			}          
#endif

#warning LPXO FIX ME: Look into operating on outputs from the processes that have been built.
			if (param.EngineDelegate != null && param.EngineDelegate.PostBuildCallback != null)
			{
				if (!param.EngineDelegate.PostBuildCallback(this.Branch, collectedOutputs, param.EngineDelegate.PostBuildUserData))
				{
					param.Log.ErrorCtx(LOG_CTX, "Build callback failed.");
				}
			}

			// Verify loaded assemblies are unqiue; this is done at the end of
			// the build to ensure all referenced assemblies are loaded.
			result &= VerifyLoadedAssemblies(param);

			sw.Stop();

			build_time = sw.Elapsed;

			param.Log.MessageCtx(LOG_CTX, "Modified time cache size: {0}", 
				this.ContentObjectCacheManager.NodeModifiedTimeEntryCount);
			param.Log.MessageCtx(LOG_CTX, "Modified time stopwatch: {0} ms", 
				this.m_swContentModifiedTimeAccess.ElapsedMilliseconds);

			return (result);
		}

		/// <summary>
		/// Clean content; implementation function for being wrapped by optional
		/// locking mechanism.
		/// </summary>
		/// <param name="log"></param>
		/// <param name="input"></param>
		/// <param name="clean_time"></param>
		/// <returns></returns>
		private bool CleanImpl(IUniversalLog log, IInput input, out TimeSpan clean_time)
		{
			bool result = true;
			Stopwatch sw = new Stopwatch();

			IEngineParameters param = new EngineParameters(this.Branch, this.Flags, this.EngineDelegate,
				input.Types, log, this.Platforms);
			this.Clean(param, input.PrebuiltProcesses);
			sw.Stop();

			clean_time = sw.Elapsed;
			return (result);
		}

		/// <summary>
		/// Build content.
		/// </summary>
		/// <param name="param"></param>
		/// <param name="input"></param>
		/// <param name="outputs"></param>
		/// <returns></returns>
		private bool Build(IEngineParameters param, IInput input, 
			out IEnumerable<IOutput> outputs, out IEnumerable<IProcess> failedProcesses)
		{
#warning DHM FIX ME: we don't currently support non-XGE usage (support local and SN-DBS in future)
			bool result = true;
			bool useXGE = this.Flags.HasFlag(EngineFlags.XGE);

			if (!useXGE)
			{
				param.Log.WarningCtx(LOG_CTX, "User has disabled XGE in Tools Installer; this configuration is not recommended.");
			}
			else
			{
				param.Log.MessageCtx(LOG_CTX, "Xoreax Incredibuild Version: {0}.",
					RSG.Interop.Incredibuild.Incredibuild.Version);
				param.Log.MessageCtx(LOG_CTX, "Xoreax Incredibuild Agent enabled: {0}.",
					RSG.Interop.Incredibuild.Incredibuild.IsAgentEnabled);
				param.Log.MessageCtx(LOG_CTX, "Xoreax Incredibuild Agent standalone: {0}.",
					RSG.Interop.Incredibuild.Incredibuild.IsAgentStandalone);
				param.Log.MessageCtx(LOG_CTX, "Xoreax Incredibuild Agent force CPU count: {0}.",
					RSG.Interop.Incredibuild.Incredibuild.AgentForceCPUCount);
				param.Log.MessageCtx(LOG_CTX, "Available CPUs: {0} physical, {1} cores, {2} logical.",
					Machine.PhysicalProcessorCount, Machine.CoreCount,
					Machine.LogicalProcessorCount);

				// If we're meant to be using XGE and the Agent isn't running: error.
				if (RSG.Interop.Incredibuild.Incredibuild.IsAgentServiceRunning())
					param.Log.MessageCtx(LOG_CTX, "Xoreax Incredibuild Agent Service is running.");
				else
				{
					param.Log.ErrorCtx(LOG_CTX, "Xoreax Incredibuild Agent Service is stopped.  Start service and try again.");
					result = false;
				}
			}

			if (useXGE)
			{
				result &= BuildXGE(param, input, out outputs, out failedProcesses);
			}
			else
			{
				result &= BuildLocal(param, input, out outputs, out failedProcesses);
			}

			return (result);
		}

		/// <summary>
		/// Local build path; when XGE is disabled or not installed.  This can
		/// be particularly slow.
		/// </summary>
		/// <param name="param"></param>
		/// <param name="input"></param>
		/// <param name="outputs"></param>
		/// <param name="failedProcesses"></param>
		/// <returns></returns>
		private bool BuildLocal(IEngineParameters param, IInput input, 
			out IEnumerable<IOutput> outputs, out IEnumerable<IProcess> failedProcesses)
		{
			outputs = new List<IOutput>();
			failedProcesses = new List<IProcess>();

			param.Log.ErrorCtx(LOG_CTX, "Build local not currently supported.  Install Xoreax Incredibuild.");
			return (false);
#if false
			bool result = true;

			// Cache our processes recursive dependencies; used in build requirement
			// determination and XGE hack.
			IDictionary<IProcess, IEnumerable<IProcess>> recursiveDependencies =
				new Dictionary<IProcess, IEnumerable<IProcess>>();
			Engine.Log.ProfileCtx(LOG_CTX, "Calculating recursive process dependencies.");
			CalculateProcessDependencies(input.PrebuiltProcesses, recursiveDependencies);
			Engine.Log.ProfileEnd();

			// Sort the prebuilt processes based on dependencies order.
			List<IProcess> sortedProcesses = new List<IProcess>(input.PrebuiltProcesses);
			sortedProcesses.Sort(new Content.Algorithm.ProcessExecOrderComparer(recursiveDependencies));

			// Loop through all of our prebuilt processes; executing the 
			// processors build.
			ICollection<IOutput> outs = new List<IOutput>();
			ICollection<IProcess> erroredProcesses = new List<IProcess>();
			foreach (IProcess process in sortedProcesses)
			{
				if (!BuildRequired(process, recursiveDependencies))
				{
					foreach (IContentNode node in process.Outputs)
					{
						if (!(node is IFilesystemNode))
							continue;
						IFilesystemNode fsNode = (node as IFilesystemNode);
						Engine.Log.Message("Output '{0}' up-to-date.", fsNode.AbsolutePath);
					}
					continue;
				}

				// Clean processes to be built if we have the corresponding flag.
				if (param.Flags.HasFlag(EngineFlags.CleanForBuild)) 
					Clean(param, new IProcess[] { process });

				Engine.Log.Message("Building '{0}'.", String.Join(", ",
					process.Outputs.OfType<IFilesystemNode>().Select(o => o.AbsolutePath)));
				result &= false; // process.Processor.Build(param, process);

				if (!result)
				{
					erroredProcesses.Add(process);
					Engine.Log.ErrorCtx(LOG_CTX, "Errors during engine build from processor: {0}.",
						process.Processor.Name);
				}
			}
			outputs = outs;
			failedProcesses = erroredProcesses;

			return (result);
#endif
		}

		/// <summary>
		/// Xoreax XGE build path; when XGE is enabled and installed.
		/// </summary>
		/// <param name="param"></param>
		/// <param name="input"></param>
		/// <param name="outputs"></param>
		/// <param name="failedProcesses"></param>
		/// <returns></returns>
		private bool BuildXGE(IEngineParameters param, IInput input, 
			out IEnumerable<IOutput> outputs, out IEnumerable<IProcess> failedProcesses)
		{
			bool result = true;

			// Determine whether XGE is already running a build; and ask the user
			// what to do as we can stop other builds.
			if (XGE.XGE.IsBuildRunning() && !this.Options.NoPopups)
			{
				param.Log.MessageCtx(LOG_CTX, "XGE build already running; prompting for force-stop.");
				System.Windows.MessageBoxResult userResult =
					MessageBox.Show("XGE build is already running, maybe from a previous asset conversion.\n\nWould you like to stop this build to continue?  Otherwise this conversion will wait.",
						System.Windows.MessageBoxButton.YesNo,
						System.Windows.MessageBoxImage.Question,
						System.Windows.MessageBoxResult.No,
						(uint)20);
				if (System.Windows.MessageBoxResult.Yes == userResult)
				{
					param.Log.MessageCtx(LOG_CTX, "User requested to stop XGE build.");
					XGE.XGE.Stop();
				}
			}

			// Wait until XGE build has finished.
			while (XGE.XGE.IsBuildRunning())
			{
				param.Log.Message("Waiting for XGE build to complete.  Use Build Monitor 'File' -> 'Stop Build' to force stopping previous build.");
				System.Threading.Thread.Sleep(MUTEX_TIMEOUT);
			}

			// Clean out XGE temporary directory from our last run.
			String xgeTempDirectory = XGEFactory.GetTempDirectory(param.Branch);
			if (param.Flags.HasFlag(EngineFlags.Lock) && 
				SIO.Directory.Exists(xgeTempDirectory))
			{
				try
				{
					param.Log.MessageCtx(LOG_CTX, "Cleaning out XGE temporary directory: {0}.",
						xgeTempDirectory);
					SIO.Directory.Delete(xgeTempDirectory, true);
				}
				catch (SIO.IOException ex)
				{
					param.Log.ToolExceptionCtx(LOG_CTX, ex, "Error deleting XGE temporary directory.");
				}
			}

			// Cache our processes recursive dependencies; used in build requirement
			// determination and XGE hack.
			IDictionary<IProcess, IEnumerable<IProcess>> recursiveDependencies =
				new Dictionary<IProcess, IEnumerable<IProcess>>();

			param.Log.ProfileCtx(LOG_CTX, "Calculating recursive process dependencies.");
			CalculateProcessDependencies(input.PrebuiltProcesses, recursiveDependencies);
			param.Log.ProfileEnd();

			param.Log.ProfileCtx(LOG_CTX, "Determining process build list.");
			List<IProcess> prebuiltProcesses = new List<IProcess>();
			prebuiltProcesses.AddRange(input.PrebuiltProcesses.Where(p => BuildRequired(param, p, recursiveDependencies)));

			// Collate any given processors processes together; we only
			// include IProcess' when a build is required.
			IDictionary<IProcessor, ICollection<IProcess>> processes =
				new Dictionary<IProcessor, ICollection<IProcess>>();
			foreach (IProcessor processor in this.Processors)
			{
				List<IProcess> procs = new List<IProcess>();
				String processorClassName = processor.Name;
				procs.AddRange(prebuiltProcesses.Where(p => p.ProcessorClassName.Equals(processorClassName)));

				if (procs.Count > 0)
					processes.Add(processor, procs);
			}
			param.Log.ProfileEnd();

			// Clean processes to be built if we have the corresponding flag.
			param.Log.ProfileCtx(LOG_CTX, "Cleaning process output.");
			IEnumerable<IProcess> processesToBuild = processes.SelectMany(kvp => kvp.Value);
			if (param.Flags.HasFlag(EngineFlags.CleanForBuild))
				Clean(param, processesToBuild);
			param.Log.ProfileEnd();

			// Loop through all of our prebuilt processes; ensuring that
			// all processors used implement IProcessorXGE and invoking
			// the Prepare method.
			List<XGE.ITool> tools = new List<XGE.ITool>();
			List<XGE.ITask> tasks = new List<XGE.ITask>();
			ICollection<IOutput> outs = new List<IOutput>();
			ICollection<IProcess> erroredProcesses = new List<IProcess>();
			foreach (KeyValuePair<IProcessor, ICollection<IProcess>> kvp in processes)
			{
				IEnumerable<XGE.ITool> processorTools;
				IEnumerable<XGE.ITask> processorTasks;
				IProcessor processor = (kvp.Key as IProcessor);

				result &= processor.Prepare(param, kvp.Value,
					out processorTools, out processorTasks);
				// Verify we have IProcess task names for dependency tracking.
				// This catches badly behaved IProcessorXGE's.
				foreach (IProcess process in kvp.Value)
				{
					Debug.Assert(process.Parameters.ContainsKey(Constants.ProcessXGE_TaskName),
						"Process does not contain XGE Task name parameter.",
						"Process for Processor: {0} does not contain XGE Task name parameter: {1}.",
						process.ProcessorClassName, Constants.ProcessXGE_TaskName);
					if (!process.Parameters.ContainsKey(Constants.ProcessXGE_TaskName))
						param.Log.WarningCtx(LOG_CTX, "Process for Processor: {0} does not contain XGE Task name parameter: {1}.",
							process.ProcessorClassName, Constants.ProcessXGE_TaskName);
				}

				if (result)
				{
					// DLC change in map PreProcess seemed to require this; but
					// its safer anyway.  Although we need a better way to make
					// them unique.
					IEnumerable<String> existingToolNames = tools.Select(t => t.Name);
					tools.AddRange(processorTools.Where(t => !existingToolNames.Contains(t.Name)));
					tasks.AddRange(processorTasks);
				}
				else
				{
					param.Log.ErrorCtx(LOG_CTX, "Errors during XGE prepare from processor: {0}.",
						processor.Name);
				}
			}

			// Early exit if we're not going to build anything.
			if (!processesToBuild.Any())
			{
				param.Log.MessageCtx(LOG_CTX, " ");
				param.Log.MessageCtx(LOG_CTX, "*** Nothing to build. ***");
				param.Log.MessageCtx(LOG_CTX, " ");
				outputs = new List<IOutput>();
				failedProcesses = new List<IProcess>();
				return (true);
			}
			param.Log.MessageCtx(LOG_CTX, "Building {0} processes.", processesToBuild.Count());
			
			// Setup our task dependencies; ready for serialisation to
			// the XGE setup XML.
			param.Log.Profile("XGE dependency injection.");
			SetupXGETaskDependencies(param, processesToBuild, tasks, recursiveDependencies);
			param.Log.ProfileEnd();

			String title = String.Format("{0} [{1}]: Asset Build Pipeline (Started: {2})",
				this.Branch.Project.FriendlyName, this.Branch.Name, DateTime.Now);
			String logFile = System.IO.Path.Combine(XGEFactory.GetTempDirectory(this.Branch), "convert.log");
			XGE.Environment environment = new XGE.Environment("Default", tools);
			XGE.IProject project = new XGE.Project(this.Branch.Project.Name);
			project.DefaultEnvironment = environment;
			project.Tasks.AddRange(tasks);

			// XGE Startup Notification.
			foreach (IProcessor processor in this.Processors)
			{
				processor.Started(param);
			}
			// Invoke XGE.
			result &= XGEFactory.Start(this.Branch, project, title, logFile,
				this.Flags.HasFlag(EngineFlags.Rebuild));

			// Parse XGE log.
			ParseXGELog(param, prebuiltProcesses, tasks, logFile, 
				ref erroredProcesses);

			// XGE Finish Notification.
			foreach (IProcessor processor in this.Processors)
			{
				processor.Finished(param);
			}

			// Output statistics based on engine flags.
			if (param.Flags.HasFlag(EngineFlags.StatisticsFileSizes))
			{
				using (new ProfileContext(param.Log, LOG_CTX, "Pipeline Statistics Calculations"))
				{
					IList<IContentNode> inputs = processesToBuild.SelectMany(p => p.Inputs).Distinct(i => i.ID).ToList();
					CalculateAndPrintTotalFilesize(param, inputs, "Total Input Data Size: ");
					IList<IContentNode> additionalInputs = processesToBuild.SelectMany(p => p.AdditionalDependentContent).Distinct(i => i.ID).ToList();
					CalculateAndPrintTotalFilesize(param, additionalInputs, "Total Additional Dependent Input Data Size: ");
					IList<IContentNode> o = processesToBuild.SelectMany(p => p.Outputs).Distinct(i => i.ID).ToList();
					CalculateAndPrintTotalFilesize(param, o, "Total Output Data Size: ");
				}
			}

			outputs = outs;
			failedProcesses = erroredProcesses;
			
			// When utilising XGE there is network latency and we have noticed 
			// that occasionally subsequent incremental builds are building more
			// than required because the output timestamps aren't correct.  This
			// happens with chained tasks in XGE, where locally executed ones
			// don't flush mtime before dependent tasks.  This bit of code
			// fixes up those timestamps; see GTA5 Bug #715057.
			if (this.Flags.HasFlag(EngineFlags.CleanForBuild) &&
				this.Flags.HasFlag(EngineFlags.XGE))
			{
				TouchDependentOutputsForXGE(param, prebuiltProcesses, recursiveDependencies);
			}

			return (result);
		}

		/// <summary>
		/// Clean content.
		/// </summary>
		/// <param name="processes"></param>
		/// <returns></returns>
		private bool Clean(IEngineParameters param, IEnumerable<IProcess> processes)
		{
			bool result = true;

			param.Log.MessageCtx(LOG_CTX, "Cleaning process output.");
			foreach (IProcess process in processes)
			{
				IProcessor processor = this.Processors.GetProcessor(process.ProcessorClassName);
				if (null == processor)
				{
					param.Log.ErrorCtx(LOG_CTX, "Failed to get IProcessor '{0}'.", process.ProcessorClassName);
					continue;
				}

				processor.Clean(param, process);
			}

			return (result);
		}

		/// <summary>
		/// Determine whether or not an IProcess needs to be executed based on
		/// inputs and outputs or its rebuild state.
		/// </summary>
		/// <param name="param"></param>
		/// <param name="process"></param>
		/// <param name="recursiveDependencies">Cached recursive dependencies.</param>
		/// <returns></returns>
		/// The conditions for a process to be built are:
		///   1. Any output doesn't exist.
		///   2. Any input is more recent than any output.
		///   3. Any dependent process will be built (updating inputs).
		/// 
		private bool BuildRequired(IEngineParameters param, IProcess process, 
			IDictionary<IProcess, IEnumerable<IProcess>> recursiveDependencies)
		{
			// Check rebuild flags; these force processes to always be built.
			if (process.Rebuild == RebuildType.Yes || this.Flags.HasFlag(EngineFlags.Rebuild))
				return (true);

#warning LPXO: FIXME: Real fix here is to rmeove discarded prcoess from prebuilt process list in the Prebuild. I.e. Discarded processes should never make it this far.
			if (process.State == ProcessState.Discard)
				return (false);

			// Use cached result if its available (previous calls to this may
			// have determined other process build requirements).
			if (this.ContentObjectCacheManager.HasProcessBuildEntry(process))
				return (this.ContentObjectCacheManager.ReadProcessBuildEntry(process));
			this.ContentObjectCacheManager.ModifyProcessBuildEntry(process, false);

			// 1. Any output doesn't exist.
			IEnumerable<bool> outputFilesExists = process.Outputs.OfType<IFilesystemNode>().Select(f => f.Exists());
			IEnumerable<bool> outputDirectoriesEmpty = process.Outputs.OfType<Directory>().Select(d => !d.EvaluateInputs().Any());
			if (outputFilesExists.Any(v => false == v) || outputDirectoriesEmpty.Any(v => true == v))
			{
				// For verbose executions we output the output files that don't 
				// currently exist.
				if (param.Flags.HasFlag(EngineFlags.Verbose))
				{
					List<String> outputDontExistFiles = process.Outputs.OfType<IFilesystemNode>().Where(f => !f.Exists()).Select(f => f.AbsolutePath).ToList();
					if (outputDontExistFiles.Any())
					{
						param.Log.DebugCtx(LOG_CTX, "{0} process outputs don't exist:", outputDontExistFiles.Count);
						foreach (String outputFile in outputDontExistFiles)
						{
							param.Log.DebugCtx(LOG_CTX, "\tFile: {0}.", outputFile);
						}
					}

					List<String> outputEmptyDirectories = process.Outputs.OfType<Directory>().Where(d => !d.EvaluateInputs().Any()).Select(d => d.AbsolutePath).ToList();
					if (outputEmptyDirectories.Any())
					{
						param.Log.DebugCtx(LOG_CTX, "{0} process outputs are empty directores:", outputEmptyDirectories.Count);
						foreach (String outputDir in outputEmptyDirectories)
						{
							param.Log.DebugCtx(LOG_CTX, "\tDirectory: {0}.", outputDir);
						}
					}
				}
				this.ContentObjectCacheManager.ModifyProcessBuildEntry(process, true);
				return (true); // Optimisation; early out.
			}

			// 2. Any input or additional-dependent-content is more recent than any output.
			List<IContentNode> updatedInputs = new List<IContentNode>();
			IEnumerable<IFilesystemNode> outputFiles = process.Outputs.OfType<IFilesystemNode>();

			foreach (IFilesystemNode output in outputFiles)
			{
				this.m_swContentModifiedTimeAccess.Start();
#if MODIFIED_TIME_CACHE_DISABLED
				IEnumerable<IFilesystemNode> moreRecentInputs =
					process.Inputs.OfType<IFilesystemNode>().Where(f => f.GetModifiedTimestamp().IsLaterThan(output.GetModifiedTimestamp()));
				updatedInputs.AddRange(moreRecentInputs);
#else
				// Cache our inputs and output modified times.
				this.ContentObjectCacheManager.CacheNodeModifiedTimeEntry(output, false);
				this.ContentObjectCacheManager.CacheNodeModifiedTimeEntry(process.Inputs, false);
				this.ContentObjectCacheManager.CacheNodeModifiedTimeEntry(process.AdditionalDependentContent, false);
				DateTime outputModTime = this.ContentObjectCacheManager.ReadNodeModifiedTimeEntry(output);

				IEnumerable<IFilesystemNode> moreRecentInputs =
					process.Inputs.OfType<IFilesystemNode>().Where(f => 
						this.ContentObjectCacheManager.ReadNodeModifiedTimeEntry(f).IsLaterThan(outputModTime));
				updatedInputs.AddRange(moreRecentInputs);
				IEnumerable<IFilesystemNode> moreRecentAdditionalDependentContent =
					process.AdditionalDependentContent.OfType<IFilesystemNode>().Where(f =>
						this.ContentObjectCacheManager.ReadNodeModifiedTimeEntry(f).IsLaterThan(outputModTime));
				updatedInputs.AddRange(moreRecentAdditionalDependentContent);
#endif // MODIFIED_TIME_CACHE_DISABLED
				this.m_swContentModifiedTimeAccess.Stop();

				// For verbose executions we output the inputs that are more recent
				// than the outputs.
				if (param.Flags.HasFlag(EngineFlags.Verbose) && updatedInputs.Count > 0)
				{
					param.Log.DebugCtx(LOG_CTX, "{0} process inputs are more recent:",
						moreRecentInputs.Count());
					foreach (IFilesystemNode inputFile in updatedInputs)
						param.Log.DebugCtx(LOG_CTX, "\tFile: {0}.", inputFile.AbsolutePath);
				}
			}
			if (updatedInputs.Count > 0)
			{
				this.ContentObjectCacheManager.ModifyProcessBuildEntry(process, true);
				return (true); // Optimisation; early out.
			}
			
			// 3. Any dependent process will be built (updating inputs).
			// Here we use the build table; this is the cached result of whether
			// dependent process are to be built.
			IEnumerable<IProcess> dependents = null;
			if (recursiveDependencies.ContainsKey(process))
				dependents = recursiveDependencies[process];
			else
			{
				param.Log.MessageCtx(LOG_CTX, "Process has no dependency information; precalculated dependencies incomplete.  Dependencies will be calculated.");
				dependents = process.DependsOn();
			}
			foreach (IProcess p in dependents)
			{
				if (!this.ContentObjectCacheManager.HasProcessBuildEntry(p))
				{
					bool buildRequired = BuildRequired(param, p, recursiveDependencies);
					this.ContentObjectCacheManager.ModifyProcessBuildEntry(p, buildRequired);
				}

				// If a dependent process is being built; then we are too.
				if (this.ContentObjectCacheManager.ReadProcessBuildEntry(p))
				{
					this.ContentObjectCacheManager.ModifyProcessBuildEntry(process, true);
					break;
				}
			}

			bool processBuildRequired = this.ContentObjectCacheManager.ReadProcessBuildEntry(process);
			return (processBuildRequired);
		}

		/// <summary>
		/// Calculate dependencies for processes list.
		/// </summary>
		/// <param name="processes"></param>
		/// <param name="immediate"></param>
		private void CalculateProcessDependencies(
			IEnumerable<IProcess> processes,
			IDictionary<IProcess, IEnumerable<IProcess>> immediate)
		{
			if (null == immediate)
				throw new ArgumentNullException("immediate");

            IDictionary<IContentNode, ICollection<IProcess>> outputProcessMap = this.GetContentNodeProcessMap(processes);

			Parallel.ForEach(processes, process =>
			{
				IEnumerable<IProcess> dependencies = process.DependsOn(outputProcessMap, DependencyTrackMode.Immediate);
				lock (immediate)
				{
					immediate.Add(process, dependencies);
				}
			});
		}

        /// <summary>
        /// Acquires a list of content node-process list pairs to more quickly evaluate dependencies.
        /// This is used to optimised the searching of dependencies from one node on other processes.
        /// </summary>
        /// <param name="processes">
        /// Input processes.
        /// </param>
        /// <returns>
        /// Dependency process dictionary.
        /// </returns>
        private IDictionary<IContentNode, ICollection<IProcess>> GetContentNodeProcessMap(IEnumerable<IProcess> processes)
        {
            IDictionary<IContentNode, ICollection<IProcess>> outputProcessMap = new Dictionary<IContentNode, ICollection<IProcess>>();

            // Do not add any discarded or built processes to the list of dependencies.
            // These will not factor into the build iteration.
            IEnumerable<IProcess> ownerFilteredProcesses = processes.Where(process => process.State != ProcessState.Discard);

            foreach (IProcess ownerProcess in ownerFilteredProcesses)
            {
                foreach (IContentNode outputNode in ownerProcess.Outputs)
                {
                    if (!outputProcessMap.ContainsKey(outputNode))
                    {
                        outputProcessMap.Add(outputNode, new List<IProcess>());
                    }

                    outputProcessMap[outputNode].Add(ownerProcess);
                }
            }

            return outputProcessMap;
        }

		private bool SyncDependencies(IEngineParameters param,
			IDictionary<IProcess, IEnumerable<IContentNode>> syncDependencies)
		{
			param.Log.ProfileCtx(LOG_CTX, "Syncing dependencies for {0} processes.",
				syncDependencies.Count().ToString());
			P4 p4 = param.Branch.Project.SCMConnect();

			if (p4 == null)
			{
				// TODO Flo: whenever SCMConnect() is droppped (it's marked as Obsolete), make sure this is still relevant, and update accordingly
				param.Log.WarningCtx(LOG_CTX, "Version control disabled in Tools Installer. See https://devstar.rockstargames.com/wiki/index.php/Asset_Pipeline_Common_Errors#Version_control_disabled_in_Tools_Installer for more information.\nThis can lead to errors later on.");
				return true;
			}

			bool result = SyncDependencies(param, p4, syncDependencies);
			p4.Disconnect();

			return result;
		}

		/// <summary>
		/// Sync dependencies for the process list; ensuring that we handle
		/// process dependencies and sync in reverse order.
		/// </summary>
		/// <param name="param">Engine parameters for current build.</param>
		/// <param name="p4">the perforce connection used to perform syncing.</param>
		/// <param name="syncDependencies">Process and dependencies to sync.</param>
		private bool SyncDependencies(IEngineParameters param, P4 p4,
			IDictionary<IProcess, IEnumerable<IContentNode>> syncDependencies)
		{
			bool result = true;

			// Calculate recursive dependencies; we need this information
			// to control how we sync dependencies to ensure their modified
			// times do not affect their build required state.  We sync
			// dependencies in reverse order; grouping by processor.
			try
			{
				IEnumerable<IProcess> processes = syncDependencies.Keys;
				IDictionary<IProcess, IEnumerable<IProcess>> recursiveDependencies =
					new Dictionary<IProcess, IEnumerable<IProcess>>();
				param.Log.ProfileCtx(LOG_CTX, "Calculating temporary recursive process dependencies.");
				CalculateProcessDependencies(processes, recursiveDependencies);
				param.Log.ProfileEnd();

#warning DHM FIX ME: ** sort dependencies to sync by process dependencies before syncing **

				IEnumerable<IContentNode> allFileDependencies = 
					syncDependencies.SelectMany( kvp => kvp.Value ).Distinct();
				IEnumerable<String> fileDependencies = 
					allFileDependencies.OfType<File>().Select(node => node.AbsolutePath);
				IEnumerable<String> dirDependencies = 
					allFileDependencies.OfType<Directory>().Select(node => SIO.Path.Combine(node.AbsolutePath, "..."));

				// This uses Perforce where (FileMapping) to determine the
				// Perforce server location of the files; we should use depot
				// paths as they will sync even if the user doesn't have
				// those directories locally (local path wouldn't sync!).
				FileMapping[] fileMappings = new FileMapping[] { };
				if (fileDependencies.Any())
				{
					fileMappings = FileMapping.Create(p4, fileDependencies.ToArray());
				}
				IEnumerable<FileMapping> missingFiles = fileMappings.Where(fm => !fm.Mapped);
				if (missingFiles.Any())
				{
					param.Log.ErrorCtx(LOG_CTX, "There are build dependency files that are not mapped into your Perforce workspace:");
					missingFiles.ForEachWithIndex((fm, index) => param.Log.ErrorCtx(LOG_CTX, "\t{0}: {1}", index, fm.DepotFilename));
					result = false;
				}
				fileMappings = fileMappings.Where(fm => fm.Mapped).ToArray();

				FileMapping[] dirMappings = new FileMapping[] { };
				if (dirDependencies.Any())
					dirMappings = FileMapping.Create(p4, dirDependencies.ToArray());
				IEnumerable<FileMapping> missingDirs = dirMappings.Where(fm => !fm.Mapped);
				if (missingDirs.Any())
				{
					param.Log.ErrorCtx(LOG_CTX, "There are build dependency directories that are not mapped into your Perforce workspace:");
					missingDirs.ForEachWithIndex((fm, index) => param.Log.ErrorCtx(LOG_CTX, "\t{0}: {1}", index.ToString(), fm.DepotFilename));
					result = false;
				}
				dirMappings = dirMappings.Where(fm => fm.Mapped).ToArray();

				// Determine what needs syncing and perform the syncs.
				result &= TrySyncPerforceFiles(param, p4, fileMappings.Select(
					fm => fm.DepotFilename));
				result &= TrySyncPerforceFiles(param, p4, dirMappings.Select(
					fm => fm.DepotFilename));
			}
			catch (P4API.Exceptions.PerforceInitializationError ex)
			{
				param.Log.ToolExceptionCtx(LOG_CTX, ex, "Perforce connection error: {0} as {1}, client: {2}, current directory: {3}",
					p4.Port, p4.User, p4.Client, SIO.Directory.GetCurrentDirectory());
				result = false;
			}
			catch (Exception ex)
			{
				param.Log.ToolExceptionCtx(LOG_CTX, ex, "Exception syncing dependencies.");
				result = false;
			}
			finally
			{
				param.Log.ProfileEnd();
			}

			// Clear our cache of modified times; in case Perforce has touched an entry.
			this.ContentObjectCacheManager.ClearNodeModifiedTimeEntries();
			return (result);
		}


		/// <summary>
		/// Syncs a larger group of files in Perforce, splitting them up into arbitrary groups to track progress.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="progress"></param>
		/// <param name="p4"></param>
		/// <param name="depotFilenames"></param>
		/// <param name="recordSets"></param>
		private void SyncPerforceFiles(ITaskContext context, IProgress<TaskProgress> progress, P4 p4, IEnumerable<String> depotFilenames, out List<P4RecordSet> recordSets)
		{
			double currentProgress = 0.0;
			
			IEnumerable<IEnumerable<String>> groups = depotFilenames.Select( (filename, i) => new { Value = filename, Index = i })
							.GroupBy(item => item.Index / _syncProgressFileGroupSize, item => item.Value);

			P4RecordSet changesRecord = p4.Run("changes", false, "-m", "1", "-s", "submitted");
			int changelist = Int32.Parse(changesRecord.Records[0]["change"]);
			double increment = 1.0 / (double)groups.Count();
			List<P4RecordSet> records = new List<P4RecordSet>();
			groups.ForEachWithIndex((files, index) => 
				{
					IEnumerable<String> syncFiles = files.Select(file => (file += "@" + changelist.ToString()) );
					P4RecordSet recordSet = p4.Run("sync", false, syncFiles.ToArray());
					currentProgress += increment;
					progress.Report(new TaskProgress(currentProgress, String.Format("Syncing {0} files...", depotFilenames.Count())));
					records.Add(recordSet);
				});

			recordSets = records;
			progress.Report(new TaskProgress(1.0, "Syncing completed."));
			context.ReturnValue = true;
		}

		/// <summary>
		/// Load parameters data from file.
		/// </summary>
		/// <param name="log"></param>
		/// <param name="branch"></param>
		/// <returns></returns>
		private void LoadParameters(IUniversalLog log, IBranch branch)
		{
			String filename = (SIO.Path.Combine(branch.Project.Config.ToolsConfig,
				"processors", "RSG.Pipeline.Engine.meta"));
			if (!SIO.File.Exists(filename))
			{
				this._parameters = new ParameterDictionary();
				return;
			}
			
			log.MessageCtx(filename, "Loading parCodeGen parameters from '{0}'.", filename);
			try
			{
				ParameterDictionary dictionary = ParameterDictionary.Load(filename);

				if (null == dictionary)
					this._parameters = new ParameterDictionary();
				else
					this._parameters = dictionary;

				// Initial our parameter member data.
				_enableSyncProgress = _parameters.GetParameter(Parameter.EnableSyncProgressBar, false);
				_syncProgressThreshold = _parameters.GetParameter(Parameter.SyncProgressThreshold, 100);
				_syncProgressFileGroupSize = _parameters.GetParameter(Parameter.SyncProgressFileGroupSize, 100);
			}
			catch (NotSupportedException)
			{
				log.ErrorCtx(filename, "Parameter load failed.  Invalid file: '{0}'.", filename);
			}
		}

		/// <summary>
		/// Sync a set of files from Perforce to #head revision; the list
		/// is filtered for only those files requiring syncing.
		/// </summary>
		/// <param name="param"></param>
		/// <param name="p4"></param>
		/// <param name="depotFilenames"></param>
		/// <returns></returns>
		private bool TrySyncPerforceFiles(IEngineParameters param, P4 p4, 
			IEnumerable<String> depotFilenames)
		{
			if (!depotFilenames.Any())
				return (true);

			bool result = true;
			try
			{
				// "preview" sync-method which is much quicker; especially for
				// remote studios.
				List<String> syncArgs = new List<String>();
				syncArgs.Add("-n"); // preview sync
				syncArgs.AddRange(depotFilenames);
				P4RecordSet previewResult = p4.Run("sync", false, syncArgs.ToArray());
				List<String> oldDepotFilenames = previewResult.Records.
					Where(record => record.Fields.ContainsKey("depotFile")).
					Select(record => record.Fields["depotFile"]).
					ToList();

				int oldDepotFilenamesCount = oldDepotFilenames.Count;
				param.Log.MessageCtx(LOG_CTX, "Syncing {0} out-of-date dependencies from Perforce.", oldDepotFilenamesCount);
				if (oldDepotFilenamesCount > 0)
				{
					param.Log.MessageCtx(LOG_CTX, "Syncing {0}, ...",
						String.Join(", ", oldDepotFilenames.Take(Math.Min(oldDepotFilenamesCount, 5))));

					List<String> errors = new List<String>();
					P4RecordSet fileSyncRecords = null;
					
					if (!Options.NoPopups && _enableSyncProgress && oldDepotFilenamesCount > _syncProgressThreshold)
					{
						List<P4RecordSet> recordSets = new List<P4RecordSet>();
						ProgressBox.Show("Syncing...", new Action<ITaskContext, IProgress<TaskProgress>>
						(
							(context, progress) => SyncPerforceFiles(context, progress, p4, oldDepotFilenames, out recordSets)
						));
						
						recordSets.ForEach(recordSet => errors.AddRange(recordSet.Errors));
					}
					else
					{
						fileSyncRecords = p4.Run("sync", false, oldDepotFilenames.ToArray());
						errors.AddRange(fileSyncRecords.Errors);
					}

					// Prompt the user to allow them to force-sync on errors.
					if (errors.Any() && !this.Options.NoPopups)
					{
						param.Log.MessageCtx(LOG_CTX, "Prompting for optional force-sync of {0} files.",
							errors.Count());
						System.Windows.MessageBoxResult userResult =
							MessageBox.Show("Perforce errors syncing {0} files.  Would you like to force-sync?\n\n{1}, ...",
								System.Windows.MessageBoxButton.YesNo,
								System.Windows.MessageBoxImage.Question,
								System.Windows.MessageBoxResult.No,
								(uint)20,
								errors.Count(),
								String.Join("\n", errors.Take(Math.Min(errors.Count(), 5))));
						if (System.Windows.MessageBoxResult.Yes == userResult)
						{
							ICollection<String> args = new List<String>();
							args.Add("-f");
							args.AddRange(oldDepotFilenames);
							fileSyncRecords = p4.Run("sync", false, args.ToArray());
						}

						// We log errors; either from the initial sync or the optional
						// user-specified force-sync.
						if (fileSyncRecords.Errors.Any())
						{
							param.Log.ErrorCtx(LOG_CTX, "{0} errors syncing dependencies from Perforce:",
								fileSyncRecords.Errors.Count());
							fileSyncRecords.Errors.ForEachWithIndex((s, i) =>
								param.Log.ErrorCtx(LOG_CTX, "\t{0}: {1}", i + 1, s));
							result = false;
						}
					}
				}
				previewResult = p4.Run("sync", false, syncArgs.ToArray());
				oldDepotFilenames = previewResult.Records.
					Where(record => record.Fields.ContainsKey("depotFile")).
					Select(record => record.Fields["depotFile"]).
					ToList();
				if (oldDepotFilenames.Any())
					param.Log.WarningCtx(LOG_CTX, "{0} files still out-of-date!",
						oldDepotFilenames.Count);
			}
			catch (P4API.Exceptions.P4APIExceptions ex)
			{
				param.Log.ToolExceptionCtx(LOG_CTX, ex, "Perforce exception");
				result = false;
			}
			catch (Exception ex)
			{
				param.Log.ToolExceptionCtx(LOG_CTX, ex, "Unhandled exception");
				result = false;
			}
			return (result);
		}

		/// <summary>
		/// Setup the XGE task process dependencies to ensure XGE invokes them
		/// in the correct order.
		/// </summary>
		/// <param name="param"></param>
		/// <param name="processes"></param>
		/// <param name="tasks"></param>
		/// <param name="immediateDependencies">Precalculated process immediate dependencies</param>
		private void SetupXGETaskDependencies(IEngineParameters param, IEnumerable<IProcess> processes,
			IEnumerable<XGE.ITask> tasks, IDictionary<IProcess, IEnumerable<IProcess>> immediateDependencies)
		{
			foreach (IProcess process in processes.Where(d => d.State != ProcessState.Discard))
			{
				if (!process.Parameters.ContainsKey(Constants.ProcessXGE_Task))
				{
					param.Log.Warning("Process for Processor: {0} does not contain XGE Task parameter: {1}.",
						process.ProcessorClassName, Constants.ProcessXGE_Task);
					continue;
				}
				XGE.ITaskJob task = (process.Parameters[Constants.ProcessXGE_Task] as XGE.ITaskJob);
				if (null != task)
				{
					Debug.Assert(immediateDependencies.ContainsKey(process),
						"Process has no dependency information; precalculated dependencies incomplete.");
					IEnumerable<IProcess> dependencies = null;
					if (immediateDependencies.ContainsKey(process))
						dependencies = immediateDependencies[process];
					else
					{
						param.Log.MessageCtx(LOG_CTX, "Process has no dependency information; precalculated dependencies incomplete.  Dependencies will be calculated.");
						dependencies = process.DependsOn(DependencyTrackMode.Immediate);
					}

					foreach (IProcess dep in dependencies.Where(d => d.State != ProcessState.Discard))
					{
						if (!BuildRequired(param, dep, immediateDependencies))
							continue; // Skip dependency injection as task not being built.

						if (!dep.Parameters.ContainsKey(Constants.ProcessXGE_Task))
						{
							param.Log.Warning("Process for Processor: {0} does not contain XGE Task parameter: {1}.",
								dep.ProcessorClassName, Constants.ProcessXGE_Task);
							continue;
						}
						XGE.ITaskJob depTask = (dep.Parameters[Constants.ProcessXGE_Task] as XGE.ITaskJob);
						task.Dependencies.Add(depTask);
					}
				}
				else
				{
					param.Log.Warning("Process for Processor: {0} contains an invalid XGE Task parameter: {1}.",
						process.ProcessorClassName, Constants.ProcessXGE_Task);
				}
			}
		}

		/// <summary>
		/// Look through process outputs and their dependencies ensuring the
		/// timestamps are correct.
		/// </summary>
		/// <param name="param"></param>
		/// <param name="processes"></param>
		/// <param name="recursiveDependencies"></param>
		/// See GTA5 Bug #715057.
		/// 
		/// The basic algorithm goes something like this:
		///   1. Sort processes based on sequential build order; ensures we set
		///      modified timestamps correctly.
		///   2. Set each process' output modified times based on the most recent
		///      input.
		/// 
		private void TouchDependentOutputsForXGE(IEngineParameters param, IEnumerable<IProcess> processes,
			IDictionary<IProcess, IEnumerable<IProcess>> recursiveDependencies)
		{
			Debug.Assert(this.Flags.HasFlag(EngineFlags.XGE),
				"This should only be invoked for XGE builds.");
			Debug.Assert(this.Flags.HasFlag(EngineFlags.CleanForBuild),
				"This should only be invoked for CleanForBuild builds.");

			param.Log.ProfileCtx(LOG_CTX, "Touching process outputs for XGE consistency.");
			List<IProcess> sortedProcesses = new List<IProcess>(processes);
			param.Log.ProfileCtx(LOG_CTX, "Sorting processes.");
			sortedProcesses.Sort(new Content.Algorithm.ProcessExecOrderComparer(
				recursiveDependencies));
			param.Log.ProfileEnd();
			foreach (IProcess process in sortedProcesses)
			{
				// Get the most recently modified input modified time.
				DateTime mri = DateTime.MinValue;
				foreach (IContentNode input in process.Inputs)
				{
					if (!(input is IFilesystemNode))
						continue; // Skip non-filesystem nodes.
					IFilesystemNode fsNode = (IFilesystemNode)input;
					DateTime inputModTime = fsNode.GetModifiedTimestamp();
					if (inputModTime.IsLaterThan(mri))
						mri = inputModTime;
				}

				// For any outputs that are earlier than or equal to this 
				// modified time then fix them!
				DateTime mriSet = mri + TimeSpan.FromMilliseconds(1);
				foreach (IContentNode output in process.Outputs)
				{
					if (!(output is IFilesystemNode))
						continue; // Skip non-filesystem nodes.
					IFilesystemNode fsNode = (IFilesystemNode)output;
					DateTime outputModTime = fsNode.GetModifiedTimestamp();
					if (outputModTime.IsEarlierThan(mri) || outputModTime.Equals(mri))
						fsNode.SetModifiedTimestamp(mriSet);
				}
			}
			param.Log.ProfileEnd();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="param"></param>
		/// <param name="nodes"></param>
		/// <param name="message"></param>
		private void CalculateAndPrintTotalFilesize(IEngineParameters param, IList<IContentNode> nodes, String message)
		{
			long totalSizeInBytes = 0;
			foreach (IContentNode node in nodes)
			{
				if (!(node is IFilesystemNode))
					continue;
				totalSizeInBytes += ((IFilesystemNode)node).GetSize();
			}
			RSG.Base.FileSize fs = new RSG.Base.FileSize(totalSizeInBytes);
			param.Log.MessageCtx(LOG_CTX, "{0} {1} [{2}]", message, fs.ToString(), nodes.Count);
		}

		/// <summary>
		/// Parse the XGE log; invoking IProcessorXGE.ParseLog method to each
		/// processor as required.
		/// </summary>
		/// <param name="param"></param>
		/// <param name="processes"></param>
		/// <param name="tasks"></param>
		/// <param name="logfile"></param>
		/// <param name="erroredProcesses"></param>
		/// This method tries to match up the correct XGE task output to the
		/// individual IProcessor and IProcess' responsible.  This ensures that
		/// processors aren't trying to parse output that is not-associated.
		/// 
		/// The issue is that the log is separated using '|' character and 
		/// a CommandLine tag.
		/// 
		private void ParseXGELog(IEngineParameters param,
			IEnumerable<IProcess> processes, IEnumerable<XGE.ITask> tasks, 
			String logfile, ref ICollection<IProcess> erroredProcesses)
		{
			param.Log.ProfileCtx(LOG_CTX, "Parsing: {0}.", logfile);
			try
			{
				if (SIO.File.Exists(logfile))
				{
					//Cache a dictionary of task name/process list pairs for faster look-up when parsing
					//the XGE log.
					Dictionary<String, List<IProcess>> taskProcesses = new Dictionary<String, List<IProcess>>();
					foreach (IProcess process in processes)
					{
						String taskname = process.Parameters[Constants.ProcessXGE_TaskName] as String;
						if ( taskProcesses.ContainsKey(taskname) == false)
							taskProcesses.Add(taskname, new List<IProcess>());

						taskProcesses[taskname].Add(process);
					}

					int bufferSize = 1024 * 1024;
					List<String> strBuffer = new List<String>();
					using (SIO.FileStream fs = SIO.File.Open(logfile, SIO.FileMode.Open, SIO.FileAccess.Read, SIO.FileShare.Read))
					using (SIO.BufferedStream bs = new SIO.BufferedStream(fs, bufferSize))
					using (SIO.StreamReader sr = new SIO.StreamReader(bs))
					{
						String taskname = String.Empty;
						IProcessor processor = null;
						IProcess process = null;
						String line = String.Empty;
						while (null != (line = sr.ReadLine()))
						{
							String lowerLine = line.ToLower();
							if (lowerLine.StartsWith("task:"))
							{
								Match match = REGEX_TASK_NAME_CONTEXT.Match(line);
								if (match.Success)
								{
									taskname = match.Groups["taskname"].Value;
									if (taskProcesses.ContainsKey(taskname))
										process = taskProcesses[taskname][0];

									if (null != process)
										processor = this.Processors.GetProcessor(process.ProcessorClassName);
									continue;
								}
							}
							else if (lowerLine.StartsWith("| "))
							{
								// Parse.
								if ((null != processor) && (strBuffer.Count > 0))
								{
									param.Log.MessageCtx(LOG_CTX, "Parsing XGE task output: {0}", taskname);
									if (!processor.ParseLog(param, strBuffer))
									{
										if (null != process)
											erroredProcesses.Add(process);
									}
								}

								// Reset.
								strBuffer.Clear();
								taskname = String.Empty;
								processor = null;
								continue;
							}

							if (lowerLine.StartsWith("error executing"))
							{
								param.Log.ErrorCtx(LOG_CTX, line);
							}

							// If we currently have context then push the string
							// onto our buffer list.
							if (null != processor)
								strBuffer.Add(line);
						}

						// Ensure we parse the last task's data if we have context.
						if ((null != processor) && (strBuffer.Count > 0))
						{
							param.Log.MessageCtx(LOG_CTX, "Parsing XGE task output: {0}", taskname);
							processor.ParseLog(param, strBuffer);
						}
					}
				}
				else
				{
					param.Log.WarningCtx(LOG_CTX, "XGE log: {0} does not exist.", logfile);
				}
			}
			catch (Exception ex)
			{
				param.Log.ToolExceptionCtx(LOG_CTX, ex, "Exception parsing XGE log: {0}.", logfile);
			}
			finally
			{
				param.Log.ProfileEnd();
			}
		}

		/// <summary>
		/// Validate that our loaded assemblies are unique; with MEF we've had
		/// some processor assemblies being loaded from two locations with one
		/// of them out of date and causing runtime issues.
		/// </summary>
		/// <param name="param"></param>
		/// <returns></returns>
		/// This has lead to duplicate assemblies and asset-build issues using 
		/// old code.
		/// 
		/// DHM TODO: not sure this is adequate as I can't replicate the issues with duplicate assemblies loaded.
		/// 
		private bool VerifyLoadedAssemblies(IEngineParameters param)
		{
			bool result = true;
			IDictionary<String, Assembly> assemblyDict = new Dictionary<String, Assembly>();
			Assembly[] residentAssemblies = AppDomain.CurrentDomain.GetAssemblies();
			foreach (Assembly assembly in residentAssemblies)
			{
				String assemblyName = assembly.GetName().ToString();
				if (assemblyDict.ContainsKey(assemblyName))
				{
					param.Log.ErrorCtx(LOG_CTX, "Duplicate assembly loaded: {0}, from:", assemblyName);
					param.Log.ErrorCtx(LOG_CTX, "\t{0}", assemblyDict[assemblyName].Location);
					param.Log.ErrorCtx(LOG_CTX, "\t{0}", assembly.Location);
					result = false;
				}
			}
			return (result);
		}

		/// <summary>
		/// Verify processes returned by processor's Prebuild.
		/// </summary>
		/// <param name="param">Engine parameters.</param>
		/// <param name="processor">Processor used for prebuild.</param>
		/// <param name="prebuildProcesses"></param>
		/// <param name="resultantProcesses">Resultant processes from prebuild.</param>
		/// <returns>true iff successful; false otherwise.</returns>
		/// 
		private bool VerifyPrebuildResultantProcesses(IEngineParameters param, 
			IProcessor processor, IEnumerable<IProcess> prebuildProcesses, 
			IEnumerable<IProcess> resultantProcesses)
		{
			bool result = true;

			// Verify input process state changed or isn't in resultant list.
			foreach (IProcess prebuildProcess in prebuildProcesses)
			{
				Debug.Assert(!((ProcessState.Initialised == prebuildProcess.State) && (resultantProcesses.Contains(prebuildProcess))),
					String.Format("Processor {0} has input process and output process that did not change state.",
					prebuildProcess.ProcessorClassName));
				if ((ProcessState.Initialised == prebuildProcess.State) && (resultantProcesses.Contains(prebuildProcess)))
				{
					param.Log.ErrorCtx(LOG_CTX, "Processor {0} has input process and output process that did not change state.",
						prebuildProcess.ProcessorClassName);
					result = false;
				}
			}

			// Generate a message if no processes were produced by prebuild.
			// Note: this can be acceptable behaviour.
			if (!resultantProcesses.Any())
			{
				param.Log.MessageCtx(LOG_CTX, "No processes produced by prebuild for processor: '{0}'.",
					processor.Name);
			}

			// Only do our process without input check if we aren't building a process that allows empty input files.
			IEnumerable<IProcess> processesWithForceCreateEmpty = resultantProcesses.Where(
			   p => p.Parameters.ContainsKey(Constants.Force_Create_If_Empty));

			if (!processesWithForceCreateEmpty.Any())
			{
				// Verify processes produced have at least inputs defined.
				IEnumerable<IProcess> processesWithoutInputs = resultantProcesses.Where(p => !p.Inputs.Any());
				if (processesWithoutInputs.Any())
				{
					param.Log.ErrorCtx(LOG_CTX, "Processes produced by prebuild for processor: '{0}' have no inputs.",
						processor.Name);
					result = false;
				}
			}

			return (result);
		}
		#endregion // Private Methods
	}

} // RSG.Pipeline.Engine namespace
