﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using RSG.Base.Configuration;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Build;
using RSG.Pipeline.Services.Platform;

namespace RSG.Pipeline.Engine
{
    /// <summary>
    /// Concrete engine parameters class; provides engine parameters to 
    /// IProcessor's.
    /// </summary>
    public class EngineParameters : IEngineParameters
    {
        #region Constants
        /// <summary>
        /// Log context.
        /// </summary>
        private static readonly String LOG_CTX = "Engine";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Project branch object.
        /// </summary>
        public IBranch Branch
        {
            get;
            private set;
        }

        /// <summary>
        /// Core project's branch object.
        /// </summary>
        public IBranch CoreBranch 
        { 
            get;
            private set;
        }

        /// <summary>
        /// Platforms to use (defaults to installer enabled targets).
        /// </summary>
        public IEnumerable<RSG.Platform.Platform> Platforms
        { 
            get;
            private set;
        }

        /// <summary>
        /// Current engine operation flags.
        /// </summary>
        public EngineFlags Flags
        {
            get;
            private set;
        }

        /// <summary>
        /// Additional build type for current build.
        /// </summary>
        public BuildType Types
        {
            get;
            private set;
        }

        /// <summary>
        /// Engine log; for processor logging.
        /// </summary>
        /// Having a central log allows the engine to detect any errors in the
        /// entire process and report accordingly.
        /// 
        /// Processors should use the context where applicable.
        /// 
        public IUniversalLog Log
        {
            get;
            private set;
        }

        /// <summary>
        /// Wrapper object for all engine delegates
        /// </summary>
        public IEngineDelegate EngineDelegate
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="flags"></param>
        /// <param name="engineDelegate"></param>
        /// <param name="types"></param>
        /// <param name="log"></param>
        public EngineParameters(IBranch branch, EngineFlags flags, 
            IEngineDelegate engineDelegate, BuildType types, IUniversalLog log)
        {
            // Default platforms are those setup by the user-options in the installer.
            IEnumerable<RSG.Platform.Platform> platforms = PlatformProcessBuilder.GetInstallerEnabledPlatforms(branch);
            Initialise(branch, flags, engineDelegate, types, log, platforms);
        }

        /// <summary>
        /// Constructor; overridding installer enabled platforms.
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="flags"></param>
        /// <param name="engineDelegate"></param>
        /// <param name="types"></param>
        /// <param name="log"></param>
        /// <param name="platforms"></param>
        /// 
        public EngineParameters(IBranch branch, EngineFlags flags,
            IEngineDelegate engineDelegate, BuildType types, IUniversalLog log, 
            IEnumerable<RSG.Platform.Platform> platforms)
            : this(branch, flags, engineDelegate, types, log)
        {
            Initialise(branch, flags, engineDelegate, types, log, platforms);            
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// Initialise private data.
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="flags"></param>
        /// <param name="engineDelegate"></param>
        /// <param name="types"></param>
        /// <param name="log"></param>
        /// <param name="platforms"></param>
        private void Initialise(IBranch branch, EngineFlags flags,
            IEngineDelegate engineDelegate, BuildType types, IUniversalLog log,
            IEnumerable<RSG.Platform.Platform> platforms)
        {
            this.Branch = branch;
            this.Flags = flags;
            this.EngineDelegate = engineDelegate;
            this.Types = types;
            this.Log = log;

            IConfig config = branch.Project.Config;
            if (config.CoreProject.Branches.ContainsKey(branch.Name))
            {
                this.CoreBranch = config.CoreProject.Branches[branch.Name];
            }
            else
            {
                log.WarningCtx(LOG_CTX, "Core project has no equivalent DLC branch: {0}.  Setting default.",
                    branch.Name);
                this.CoreBranch = config.CoreProject.DefaultBranch;
            }
            
            // Verify that all platforms are available in the branch.
            foreach (RSG.Platform.Platform platform in platforms)
            {
                ITarget target = branch.Targets.Values.Where(t => t.Platform.Equals(platform)).
                    FirstOrDefault();
                if (null == target)
                {
                    log.Error("Invalid platform '{0}' specified for branch '{1}'.  No target exists.",
                        platform, branch.Name);
                }
            }
            this.Platforms = platforms;
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Engine namespace
