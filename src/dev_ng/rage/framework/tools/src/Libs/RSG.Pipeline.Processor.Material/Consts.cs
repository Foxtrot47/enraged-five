﻿using System;

namespace RSG.Pipeline.Processor.Material
{

    /// <summary>
    /// Material processor(s) constants.
    /// </summary>
    public class Consts
    {
        /// <summary>
        /// List of dependencies.
        /// </summary>
        internal static readonly String MaterialPreset_TemplateDirectory =
            "Template Directory";
        internal static readonly String MaterialPreset_TextureTemplateType =
            "Texture Template Type";
    }

} // RSG.Pipeline.Processor.Map namespace

