﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using RSG.Model.MaterialPresets;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Attributes;
using RSG.Pipeline.Services;
using RSG.Pipeline.Services.Platform.Texture;
using SIO = System.IO;
using XGE = RSG.Interop.Incredibuild.XGE;

namespace RSG.Pipeline.Processor.Material
{
    /// <summary>
    /// 
    /// </summary>
    [Export(typeof(IProcessor))]
    [ProcessorFlags(ProcessorFlags.Compatibility)]
    class MaterialPresetProcessor :
        ProcessorBase,
        IProcessor
    {
        #region Constants
        /// <summary>
        /// Processor description string.
        /// </summary>
        private static readonly String DESCRIPTION = "Material Preset Processor";

        /// <summary>
        /// Processor log context.
        /// </summary>
        private static readonly String LOG_CTX = "Material Presets";

        /// <summary>
        /// Shader variable output format.
        /// </summary>
        private static readonly String PARAM_OUTPUT_FORMAT = "Output Format";

        /// <summary>
        /// Default texture export output format.
        /// </summary>
        private static readonly String DEFAULT_OUTPUT_FORMAT = ".sva";

        /// <summary>
        /// Split characters for source texture filenames.
        /// </summary>
        private static readonly Char[] SOURCE_SPLIT_CHARS = new Char[] { '+', '>' };
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public MaterialPresetProcessor()
            : base(DESCRIPTION)
        {
        }
        #endregion // Constructor(s)

        #region IProcessor Interface Methods
        /// <summary>
        /// Prebuild stage; passing back enumerable of dependencies for the
        /// specific input data.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="process"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="resultantProcesses"></param>
        /// <returns></returns>
        public override bool Prebuild(IEngineParameters param,
            IProcess process, IProcessorCollection processors,
            IContentTree owner, out IEnumerable<IContentNode> syncDependencies,
            out IEnumerable<IProcess> resultantProcesses)
        {
            ICollection<IProcess> processes = new List<IProcess>();
            ICollection<IContentNode> syncDeps = new List<IContentNode>();
            this.LoadParameters(param);

            if (_parameters.ContainsKey(Consts.MaterialPreset_TemplateDirectory))
            {
                String templateDirectory = _parameters[Consts.MaterialPreset_TemplateDirectory] as String;
                templateDirectory = param.Branch.Environment.Subst(templateDirectory);
                syncDeps.Add(owner.CreateDirectory(templateDirectory));
            }

            /* Disabled for now, this will generate .tcs files from a material preset template.
             * Not sure if we want to be doing it here but it's a little cleaner than generating an entirely new tool to do the work.
             * 
            // TODO: This'll need to be passed in from the processor so we know which templates we are using.
            String textureTemplateType = "maps";
            if (m_Parameters.ContainsKey(Consts.MaterialPreset_TextureTemplateType))
            {
                textureTemplateType = m_Parameters[Consts.MaterialPreset_TextureTemplateType] as String;
            }

            ProcessTextureTemplates(param, process.Inputs.First() as Content.Directory, textureTemplateType);
            */
 
            process.State = ProcessState.Prebuilt;
            processes.Add(process);

            resultantProcesses = processes;
            syncDependencies = syncDeps;
            return (true);
        }

        public bool ProcessTextureTemplates(IEngineParameters param, Content.Directory assetDir, String textureTemplateType)
        {
            String drawableDirectory = assetDir.AbsolutePath;
            List<string> materialPresetFiles = new List<string>();

            // Add both .mpo and .mpi files for legacy reasons.
            materialPresetFiles.AddRange(System.IO.Directory.GetFiles(drawableDirectory, "*" + MaterialPreset.MaterialObjectExtension));
            materialPresetFiles.AddRange(System.IO.Directory.GetFiles(drawableDirectory, "*" + MaterialPreset.MaterialTemplateExportExtension));

            foreach (string presetFile in materialPresetFiles)
            {
                MaterialPreset preset = null;
                MaterialPreset.LoadPreset(param.Branch, param.Log, presetFile, out preset, param.Branch.Project.Config);
                preset.ExpandPreset();

                IDictionary<string, string> textureMaps = new Dictionary<string, string>();
                preset.GetTextureMapEntries(ref textureMaps);

                foreach (KeyValuePair<string, string> kvp in textureMaps)
                {
                    string textureFilename = kvp.Key;
                    string textureTemplate = kvp.Value;
                    string textureFile = SIO.Path.GetFileNameWithoutExtension(textureFilename);

                    string parentTemplate = "${RS_ASSETS}/metadata/textures/templates/" + textureTemplateType +"/" + textureTemplate;
                    string specificationFilename = SIO.Path.Combine(param.Branch.Metadata, "textures", textureTemplateType, textureFile + ".tcs");
                    string resourceTexturePath = SIO.Path.Combine(param.Branch.Assets, textureTemplateType, "textures", textureFile + ".tif");

                    List<string> sourceTextures = new List<string>();
                    sourceTextures.Add(textureFilename);

                    if (!SIO.File.Exists(specificationFilename))
                        SpecificationFactory.Create(specificationFilename, parentTemplate, sourceTextures, resourceTexturePath, false);
                }
            }

            return true;
        }

        /// <summary>
        /// Prepare a build for the processes; populating the XGE project with
        /// the tools, environments and tasks required.  Including setting up
        /// the task dependency chain.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="processes"></param>
        /// <param name="tools"></param>
        /// <param name="tasks"></param>
        /// <returns></returns>
        public override bool Prepare(IEngineParameters param, IEnumerable<IProcess> processes,
            out IEnumerable<XGE.ITool> tools, out IEnumerable<XGE.ITask> tasks)
        {
            bool result = true;
            XGE.ITool tool = XGEFactory.GetMaterialTool(param.Log, param.Branch.Project.Config, DESCRIPTION, XGEFactory.MaterialToolType.MaterialPresetConverter, false);
            ICollection<XGE.ITool> convertTools = new List<XGE.ITool>();
            convertTools.Add(tool);

            XGE.ITaskJobGroup taskGroup = new XGE.TaskGroup(DESCRIPTION);

            int exportJobCounter = 0;
            ICollection<XGE.ITask> convertTasks = new List<XGE.ITask>();
            foreach (IProcess process in processes)
            {
                IEnumerable<RSG.Pipeline.Content.Directory> inputs = process.Inputs.OfType<RSG.Pipeline.Content.Directory>();
                String inputDirectory = inputDirectory = inputs.Select(f => f.AbsolutePath).First();
                String outputZip = ((RSG.Pipeline.Content.File)process.Outputs.First()).AbsolutePath;
                String caption = String.Format("{0} {1}", DESCRIPTION, exportJobCounter++);
                StringBuilder paramSb = new StringBuilder();
                if (process.Rebuild == RebuildType.Yes || param.Flags.HasFlag(EngineFlags.Rebuild))
                    paramSb.Append("-force ");
                paramSb.AppendFormat("-input \"{0}\" ", inputDirectory);
                paramSb.AppendFormat("-output \"{0}\" ", outputZip);

                XGE.TaskJob task = new XGE.TaskJob(caption);
                task.Parameters = paramSb.ToString();
                task.SourceFile = inputDirectory;
                task.OutputFiles.Add(outputZip);
                task.Caption = caption;
                task.Tool = tool;

                convertTasks.Add(task);

                process.Parameters.Add(Constants.ProcessXGE_Task, task);
                process.Parameters.Add(Constants.ProcessXGE_TaskName, caption);
            }

            foreach (XGE.ITaskJob taskJob in convertTasks)
                taskGroup.Tasks.Add(taskJob);

            // Assign method output.
            tools = convertTools;
            tasks = new List<XGE.ITask>(new XGE.ITask[] { taskGroup });
            return (result);
        }
        #endregion // IProcessor Interface Methods
    }

} // RSG.Pipeline.Processor.Texture namespace
