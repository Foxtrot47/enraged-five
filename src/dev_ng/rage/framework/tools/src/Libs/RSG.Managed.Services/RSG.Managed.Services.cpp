#include "RSG.Managed.Services.h"

using namespace System;
using namespace System::Collections::Generic;
using namespace System::Linq;
using namespace System::Runtime::InteropServices;
using namespace System::Diagnostics;

// Internal RemoteClientWrapper implementation

void DebugWrite(String^ widget, String^ value)
{
	Debug::WriteLine(String::Format("DEBUG: '{0}' - {1}", widget, value));
}

void DebugWrite(String^ value)
{
	Debug::WriteLine(String::Format("DEBUG: '{0}'", value));
}

// Create a game connection and assign a proxy address. This will currently just return the first game connection if any.
bool RSGManagedServices::RemoteClientWrapper::AssignProxyAddress(const char* proxyAddress)
{
	try
	{	
		IEnumerable<RSG::Rag::Contracts::Data::GameConnection^>^ gameConnections = RSG::Rag::Clients::RagClientFactory::GetGameConnections(gcnew System::String(proxyAddress));

		if(Enumerable::Any(gameConnections))
		{
			_gameConnection = Enumerable::First(gameConnections);

			_dummy = gcnew RSGManagedServices::DummySubscription(_gameConnection);
			_subscriptionClient = RSG::Rag::Clients::RagClientFactory::CreateConnectionSubscriptionClient(_dummy);
			_subscriptionClient->Subscribe();

			return true;
		}
	}
	catch(Exception^ e)
	{
		DebugWrite(e->Message);
		return false;
	}

	return false;
}

bool RSGManagedServices::RemoteClientWrapper::IsConnected()
{
	if(_dummy != nullptr)
	{
		return _dummy->GetGameStatus();
	}

	return false;
}

void RSGManagedServices::RemoteClientWrapper::Disconnect()
{
	_gameConnection = nullptr;
	_dummy = nullptr;
}

// Create a WidgetClient, these instances are temp which get created for every widget command run.
RSG::Rag::Clients::WidgetClient^ RSGManagedServices::RemoteClientWrapper::CreateWidgetClient()
{
	if(_gameConnection != nullptr)
	{
		RSG::Rag::Clients::WidgetClient^ widget = RSG::Rag::Clients::RagClientFactory::CreateWidgetClient(_gameConnection);
		return widget;
	}
	
	return nullptr;
}

RSG::Rag::Clients::ConsoleClient^ RSGManagedServices::RemoteClientWrapper::CreateConsoleClient()
{
	if(_gameConnection != nullptr)
	{
		RSG::Rag::Clients::ConsoleClient^ console = RSG::Rag::Clients::RagClientFactory::CreateConsoleClient(_gameConnection);
		return console;
	}

	return nullptr;
}

const char* RSGManagedServices::RemoteClientWrapper::GetPlatform()
{
	if(_gameConnection != nullptr)
	{
		return (const char*)Marshal::StringToHGlobalAnsi(_gameConnection->Platform).ToPointer();
	}

	return NULL;
}

bool RSGManagedServices::RemoteClientWrapper::WidgetExists(const char* widgetPath)
{
	RSG::Rag::Clients::WidgetClient^ c = CreateWidgetClient();
	if(c != nullptr)
	{
		try
		{
			return c->WidgetExists(gcnew System::String(widgetPath));
		}
		catch(Exception^ e)
		{
			DebugWrite(gcnew System::String(widgetPath), e->Message);
		}
	}

	return false;
}

bool RSGManagedServices::RemoteClientWrapper::WidgetExists(unsigned int widgetId)
{
	RSG::Rag::Clients::WidgetClient^ c = CreateWidgetClient();
	if(c != nullptr)
	{
		try
		{
			return c->WidgetExists(widgetId);
		}
		catch(Exception^ e)
		{
			DebugWrite(widgetId.ToString(), e->Message);
		}
	}

	return false;
}

unsigned int RSGManagedServices::RemoteClientWrapper::GetWidgetId(const char* widgetPath)
{
	RSG::Rag::Clients::WidgetClient^ c = CreateWidgetClient();
	if(c != nullptr)
	{
		try
		{
			return c->GetWidgetId(gcnew System::String(widgetPath));
		}
		catch(Exception^ e)
		{
			DebugWrite(gcnew System::String(widgetPath), e->Message);
		}
	}

	return -1;
}

void RSGManagedServices::RemoteClientWrapper::WriteBoolWidget(const char* widgetPath, bool value)
{
	RSG::Rag::Clients::WidgetClient^ c = CreateWidgetClient();
	if(c != nullptr)
	{
		try
		{
			c->WriteBoolWidget(gcnew System::String(widgetPath), value);
		}
		catch(Exception^ e)
		{
			DebugWrite(gcnew System::String(widgetPath), e->Message);
		}
	}
}

void RSGManagedServices::RemoteClientWrapper::WriteBoolWidget(unsigned int widgetId, bool value)
{
	RSG::Rag::Clients::WidgetClient^ c = CreateWidgetClient();
	if(c != nullptr)
	{
		try
		{
			c->WriteBoolWidget(widgetId, value);
		}
		catch(Exception^ e)
		{
			DebugWrite(widgetId.ToString(), e->Message);
		}
	}
}

void RSGManagedServices::RemoteClientWrapper::WriteFloatWidget(const char* widgetPath, float value)
{
	RSG::Rag::Clients::WidgetClient^ c = CreateWidgetClient();
	if(c != nullptr)
	{
		try
		{
			c->WriteFloatWidget(gcnew System::String(widgetPath), value);
		}
		catch(Exception^ e)
		{
			DebugWrite(gcnew System::String(widgetPath), e->Message);
		}
	}
}

void RSGManagedServices::RemoteClientWrapper::WriteFloatWidget(unsigned int widgetId, float value)
{
	RSG::Rag::Clients::WidgetClient^ c = CreateWidgetClient();
	if(c != nullptr)
	{
		try
		{
			c->WriteFloatWidget(widgetId, value);
		}
		catch(Exception^ e)
		{
			DebugWrite(widgetId.ToString(), e->Message);
		}
	}
}

void RSGManagedServices::RemoteClientWrapper::WriteIntWidget(const char* widgetPath, int value)
{
	RSG::Rag::Clients::WidgetClient^ c = CreateWidgetClient();
	if(c != nullptr)
	{
		try
		{
			c->WriteIntWidget(gcnew System::String(widgetPath), value);
		}
		catch(Exception^ e)
		{
			DebugWrite(gcnew System::String(widgetPath), e->Message);
		}
	}
}

void RSGManagedServices::RemoteClientWrapper::WriteIntWidget(unsigned int widgetId, int value)
{
	RSG::Rag::Clients::WidgetClient^ c = CreateWidgetClient();
	if(c != nullptr)
	{
		try
		{
			c->WriteIntWidget(widgetId, value);
		}
		catch(Exception^ e)
		{
			DebugWrite(widgetId.ToString(), e->Message);
		}
	}
}

void RSGManagedServices::RemoteClientWrapper::WriteStringWidget(const char* widgetPath, const char* value)
{
	RSG::Rag::Clients::WidgetClient^ c = CreateWidgetClient();
	if(c != nullptr)
	{
		try
		{
			c->WriteStringWidget(gcnew System::String(widgetPath), gcnew System::String(value));
		}
		catch(Exception^ e)
		{
			DebugWrite(gcnew System::String(widgetPath), e->Message);
		}
	}
}

void RSGManagedServices::RemoteClientWrapper::WriteStringWidget(unsigned int widgetId, const char* value)
{
	RSG::Rag::Clients::WidgetClient^ c = CreateWidgetClient();
	if(c != nullptr)
	{
		try
		{
			c->WriteStringWidget(widgetId, gcnew System::String(value));
		}
		catch(Exception^ e)
		{
			DebugWrite(widgetId.ToString(), e->Message);
		}
	}
}

void RSGManagedServices::RemoteClientWrapper::WriteVector2Widget(const char* widgetPath, Vector2 value)
{
	RSG::Rag::Clients::WidgetClient^ c = CreateWidgetClient();
	if(c != nullptr)
	{
		try
		{
			RSG::Base::Math::Vector2f^ vec = gcnew RSG::Base::Math::Vector2f(value._x, value._y);
			c->WriteVector2Widget(gcnew System::String(widgetPath), vec);
		}
		catch(Exception^ e)
		{
			DebugWrite(gcnew System::String(widgetPath), e->Message);
		}
	}
}

void RSGManagedServices::RemoteClientWrapper::WriteVector2Widget(unsigned int widgetId, Vector2 value)
{
	RSG::Rag::Clients::WidgetClient^ c = CreateWidgetClient();
	if(c != nullptr)
	{
		try
		{
			RSG::Base::Math::Vector2f^ vec = gcnew RSG::Base::Math::Vector2f(value._x, value._y);
			c->WriteVector2Widget(widgetId, vec);
		}
		catch(Exception^ e)
		{
			DebugWrite(widgetId.ToString(), e->Message);
		}
	}
}

void RSGManagedServices::RemoteClientWrapper::WriteVector3Widget(const char* widgetPath, Vector3 value)
{
	RSG::Rag::Clients::WidgetClient^ c = CreateWidgetClient();
	if(c != nullptr)
	{
		try
		{
			RSG::Base::Math::Vector3f^ vec = gcnew RSG::Base::Math::Vector3f(value._x, value._y, value._z);
			c->WriteVector3Widget(gcnew System::String(widgetPath), vec);
		}
		catch(Exception^ e)
		{
			DebugWrite(gcnew System::String(widgetPath), e->Message);
		}
	}
}

void RSGManagedServices::RemoteClientWrapper::WriteVector3Widget(unsigned int widgetId, Vector3 value)
{
	RSG::Rag::Clients::WidgetClient^ c = CreateWidgetClient();
	if(c != nullptr)
	{
		try
		{
			RSG::Base::Math::Vector3f^ vec = gcnew RSG::Base::Math::Vector3f(value._x, value._y, value._z);
			c->WriteVector3Widget(widgetId, vec);
		}
		catch(Exception^ e)
		{
			DebugWrite(widgetId.ToString(), e->Message);
		}
	}
}

void RSGManagedServices::RemoteClientWrapper::WriteVector4Widget(const char* widgetPath, Vector4 value)
{
	RSG::Rag::Clients::WidgetClient^ c = CreateWidgetClient();
	if(c != nullptr)
	{
		try
		{
			RSG::Base::Math::Vector4f^ vec = gcnew RSG::Base::Math::Vector4f(value._x, value._y, value._z, value._w);
			c->WriteVector4Widget(gcnew System::String(widgetPath), vec);
		}
		catch(Exception^ e)
		{
			DebugWrite(gcnew System::String(widgetPath), e->Message);
		}
	}
}

void RSGManagedServices::RemoteClientWrapper::WriteVector4Widget(unsigned int widgetId, Vector4 value)
{
	RSG::Rag::Clients::WidgetClient^ c = CreateWidgetClient();
	if(c != nullptr)
	{
		try
		{
			RSG::Base::Math::Vector4f^ vec = gcnew RSG::Base::Math::Vector4f(value._x, value._y, value._z, value._w);
			c->WriteVector4Widget(widgetId, vec);
		}
		catch(Exception^ e)
		{
			DebugWrite(widgetId.ToString(), e->Message);
		}
	}
}

void RSGManagedServices::RemoteClientWrapper::WriteDataWidget(const char* widgetPath, void* value, int length)
{
	RSG::Rag::Clients::WidgetClient^ c = CreateWidgetClient();
	if(c != nullptr)
	{
		try
		{
#ifdef _WIN64 
			System::IntPtr dataptr((__int64)value);
#else
			System::IntPtr dataptr((int)value);
#endif
			array<Byte>^ byteArray = gcnew array<Byte>(length);
			Marshal::Copy(dataptr, byteArray, 0, length);

			c->WriteDataWidget(gcnew System::String(widgetPath), byteArray);
		}
		catch(Exception^ e)
		{
			DebugWrite(gcnew System::String(widgetPath), e->Message);
		}
	}
}

void RSGManagedServices::RemoteClientWrapper::WriteDataWidget(unsigned int widgetId, void* value, int length)
{
	RSG::Rag::Clients::WidgetClient^ c = CreateWidgetClient();
	if(c != nullptr)
	{
		try
		{
			System::IntPtr dataptr((int)value);
			array<Byte>^ byteArray = gcnew array<Byte>(length);
			Marshal::Copy(dataptr, byteArray, 0, length);

			c->WriteDataWidget(widgetId, byteArray);
		}
		catch(Exception^ e)
		{
			DebugWrite(widgetId.ToString(), e->Message);
		}
	}
}

bool RSGManagedServices::RemoteClientWrapper::ReadBoolWidget(const char* widgetPath)
{
	RSG::Rag::Clients::WidgetClient^ c = CreateWidgetClient();
	if(c != nullptr)
	{
		try
		{
			return c->ReadBoolWidget(gcnew System::String(widgetPath));
		}
		catch(Exception^ e)
		{
			DebugWrite(gcnew System::String(widgetPath), e->Message);
		}
	}

	return false;
}

bool RSGManagedServices::RemoteClientWrapper::ReadBoolWidget(unsigned int widgetId)
{
	RSG::Rag::Clients::WidgetClient^ c = CreateWidgetClient();
	if(c != nullptr)
	{
		try
		{
			return c->ReadBoolWidget(widgetId);
		}
		catch(Exception^ e)
		{
			DebugWrite(widgetId.ToString(), e->Message);
		}
	}

	return false;
}

float RSGManagedServices::RemoteClientWrapper::ReadFloatWidget(const char* widgetPath)
{
	RSG::Rag::Clients::WidgetClient^ c = CreateWidgetClient();
	if(c != nullptr)
	{
		try
		{
			return c->ReadFloatWidget(gcnew System::String(widgetPath));
		}
		catch(Exception^ e)
		{
			DebugWrite(gcnew System::String(widgetPath), e->Message);
		}
	}

	return -1;
}

float RSGManagedServices::RemoteClientWrapper::ReadFloatWidget(unsigned int widgetId)
{
	RSG::Rag::Clients::WidgetClient^ c = CreateWidgetClient();
	if(c != nullptr)
	{
		try
		{
			return c->ReadFloatWidget(widgetId);
		}
		catch(Exception^ e)
		{
			DebugWrite(widgetId.ToString(), e->Message);
		}
	}

	return -1;
}

int RSGManagedServices::RemoteClientWrapper::ReadIntWidget(const char* widgetPath)
{
	RSG::Rag::Clients::WidgetClient^ c = CreateWidgetClient();
	if(c != nullptr)
	{
		try
		{
			return c->ReadIntWidget(gcnew System::String(widgetPath));
		}
		catch(Exception^ e)
		{
			DebugWrite(gcnew System::String(widgetPath), e->Message);
		}
	}

	return -1;
}

int RSGManagedServices::RemoteClientWrapper::ReadIntWidget(unsigned int widgetId)
{
	RSG::Rag::Clients::WidgetClient^ c = CreateWidgetClient();
	if(c != nullptr)
	{
		try
		{
			return c->ReadIntWidget(widgetId);
		}
		catch(Exception^ e)
		{
			DebugWrite(widgetId.ToString(), e->Message);
		}
	}

	return -1;
}

const char* RSGManagedServices::RemoteClientWrapper::ReadStringWidget(const char* widgetPath)
{
	RSG::Rag::Clients::WidgetClient^ c = CreateWidgetClient();
	if(c != nullptr)
	{
		try
		{
			return (const char*)Marshal::StringToHGlobalAnsi(c->ReadStringWidget(gcnew System::String(widgetPath))).ToPointer();
		}
		catch(Exception^ e)
		{
			DebugWrite(gcnew System::String(widgetPath), e->Message);
		}
	}

	return NULL;
}

const char* RSGManagedServices::RemoteClientWrapper::ReadStringWidget(unsigned int widgetId)
{
	RSG::Rag::Clients::WidgetClient^ c = CreateWidgetClient();
	if(c != nullptr)
	{
		try
		{
			return (const char*)Marshal::StringToHGlobalAnsi(c->ReadStringWidget(widgetId)).ToPointer();
		}
		catch(Exception^ e)
		{
			DebugWrite(widgetId.ToString(), e->Message);
		}
	}

	return NULL;
}

Vector2 RSGManagedServices::RemoteClientWrapper::ReadVector2Widget(const char* widgetPath)
{
	RSG::Rag::Clients::WidgetClient^ c = CreateWidgetClient();
	if(c != nullptr)
	{
		RSG::Base::Math::Vector2f^ vec = c->ReadVector2Widget(gcnew System::String(widgetPath));
		return Vector2(vec->X, vec->Y);
	}
}

Vector2 RSGManagedServices::RemoteClientWrapper::ReadVector2Widget(unsigned int widgetId)
{
	RSG::Rag::Clients::WidgetClient^ c = CreateWidgetClient();
	if(c != nullptr)
	{
		RSG::Base::Math::Vector2f^ vec = c->ReadVector2Widget(widgetId);
		return Vector2(vec->X, vec->Y);
	}
}

Vector3 RSGManagedServices::RemoteClientWrapper::ReadVector3Widget(const char* widgetPath)
{
	RSG::Rag::Clients::WidgetClient^ c = CreateWidgetClient();
	if(c != nullptr)
	{
		RSG::Base::Math::Vector3f^ vec = c->ReadVector3Widget(gcnew System::String(widgetPath));
		return Vector3(vec->X, vec->Y, vec->Z);
	}
}

Vector3 RSGManagedServices::RemoteClientWrapper::ReadVector3Widget(unsigned int widgetId)
{
	RSG::Rag::Clients::WidgetClient^ c = CreateWidgetClient();
	if(c != nullptr)
	{
		RSG::Base::Math::Vector3f^ vec = c->ReadVector3Widget(widgetId);
		return Vector3(vec->X, vec->Y, vec->Z);
	}
}

Vector4 RSGManagedServices::RemoteClientWrapper::ReadVector4Widget(const char* widgetPath)
{
	RSG::Rag::Clients::WidgetClient^ c = CreateWidgetClient();
	if(c != nullptr)
	{
		RSG::Base::Math::Vector4f^ vec = c->ReadVector4Widget(gcnew System::String(widgetPath));
		return Vector4(vec->X, vec->Y, vec->Z, vec->W);
	}
}

Vector4 RSGManagedServices::RemoteClientWrapper::ReadVector4Widget(unsigned int widgetId)
{
	RSG::Rag::Clients::WidgetClient^ c = CreateWidgetClient();
	if(c != nullptr)
	{
		RSG::Base::Math::Vector4f^ vec = c->ReadVector4Widget(widgetId);
		return Vector4(vec->X, vec->Y, vec->Z, vec->W);
	}
}

const char* RSGManagedServices::RemoteClientWrapper::ReadDataWidget(const char* widgetPath)
{
	RSG::Rag::Clients::WidgetClient^ c = CreateWidgetClient();
	if(c != nullptr)
	{
		try
		{
			array<Byte>^ byteArray = c->ReadDataWidget(gcnew System::String(widgetPath));
			String^ string = BitConverter::ToString(byteArray);
			return (const char*)Marshal::StringToHGlobalAnsi(string).ToPointer();
		}
		catch(Exception^ e)
		{
			DebugWrite(gcnew System::String(widgetPath), e->Message);
		}
	}

	return NULL;
}

const char* RSGManagedServices::RemoteClientWrapper::ReadDataWidget(unsigned int widgetId)
{
	RSG::Rag::Clients::WidgetClient^ c = CreateWidgetClient();
	if(c != nullptr)
	{
		try
		{
			array<Byte>^ byteArray = c->ReadDataWidget(widgetId);
			String^ string = byteArray->ToString(); //System::Text::Encoding::GetString(byteArray);
			return (const char*)Marshal::StringToHGlobalAnsi(string).ToPointer();
		}
		catch(Exception^ e)
		{
			DebugWrite(widgetId.ToString(), e->Message);
		}
	}

	return NULL;
}

//RSG::Base::Math::Vector2f^ RSGManagedServices::WidgetClient::ReadVector2Widget(unsigned int widgetId)
//{
//	return widgeyClient->ReadVector2Widget(widgetId);
//}
//
//RSG::Base::Math::Vector3f^ RSGManagedServices::WidgetClient::ReadVector3Widget(const char* widgetPath)
//{
//	return widgeyClient->ReadVector3Widget(gcnew System::String(widgetPath));
//}
//
//RSG::Base::Math::Vector3f^ RSGManagedServices::WidgetClient::ReadVector3Widget(unsigned int widgetId)
//{
//	return widgeyClient->ReadVector3Widget(widgetId);
//}
//
//RSG::Base::Math::Vector4f^ RSGManagedServices::WidgetClient::ReadVector4Widget(const char* widgetPath)
//{
//	return widgeyClient->ReadVector4Widget(gcnew System::String(widgetPath));
//}
//
//RSG::Base::Math::Vector4f^ RSGManagedServices::WidgetClient::ReadVector4Widget(unsigned int widgetId)
//{
//	return widgeyClient->ReadVector4Widget(widgetId);
//}


void RSGManagedServices::RemoteClientWrapper::SendSyncCommand(unsigned int timeout)
{
	RSG::Rag::Clients::WidgetClient^ c = CreateWidgetClient();
	if(c != nullptr)
	{
		try
		{
			c->SendSyncCommand(timeout);
		}
		catch(Exception^ e)
		{
			DebugWrite(e->Message);
		}
	}
}

void RSGManagedServices::RemoteClientWrapper::PressButton(const char* widgetPath)
{
	RSG::Rag::Clients::WidgetClient^ c = CreateWidgetClient();
	if(c != nullptr)
	{
		try
		{
			c->PressButton(gcnew System::String(widgetPath));
		}
		catch(Exception^ e)
		{
			DebugWrite(gcnew System::String(widgetPath), e->Message);
		}
	}
}

void RSGManagedServices::RemoteClientWrapper::PressButton(unsigned int widgetId)
{
	RSG::Rag::Clients::WidgetClient^ c = CreateWidgetClient();
	if(c != nullptr)
	{
		try
		{
			c->PressButton(widgetId);
		}
		catch(Exception^ e)
		{
			DebugWrite(widgetId.ToString(), e->Message);
		}
	}
}

void RSGManagedServices::RemoteClientWrapper::PressVCRButton(const char* widgetPath, VCRButton vcrButton)
{
	RSG::Rag::Clients::WidgetClient^ c = CreateWidgetClient();
	if(c != nullptr)
	{
		try
		{
			c->PressVCRButton(gcnew System::String(widgetPath), (RSG::Rag::Contracts::Data::VCRButton)vcrButton);
		}
		catch(Exception^ e)
		{
			DebugWrite(gcnew System::String(widgetPath), e->Message);
		}
	}
}

void RSGManagedServices::RemoteClientWrapper::PressVCRButton(unsigned int widgetId, VCRButton vcrButton)
{
	RSG::Rag::Clients::WidgetClient^ c = CreateWidgetClient();
	if(c != nullptr)
	{
		try
		{
			c->PressVCRButton(widgetId, (RSG::Rag::Contracts::Data::VCRButton)vcrButton);
		}
		catch(Exception^ e)
		{
			DebugWrite(widgetId.ToString(), e->Message);
		}
	}
}

const char* RSGManagedServices::RemoteClientWrapper::SendCommand(const char* command)
{
	RSG::Rag::Clients::ConsoleClient^ c = CreateConsoleClient();
	if(c != nullptr)
	{
		try
		{
			RSG::Rag::Contracts::Data::ConsoleCommandResponse^ response = c->ExecuteCommand(gcnew String(command));
			if(response->State != RSG::Rag::Contracts::Data::ConsoleCommandExecutionState::Ignored && response->State != RSG::Rag::Contracts::Data::ConsoleCommandExecutionState::Faulted)
				return (const char*)Marshal::StringToHGlobalAnsi(response->Response).ToPointer();
			else
			{
				DebugWrite(response->ExceptionDetails);
			}
		}
		catch(Exception^ e)
		{
			DebugWrite(e->Message);
		}
	}

	return NULL;
}

void RSGManagedServices::RemoteClientWrapper::AddBankReference(const char* widgetPath)
{
	RSG::Rag::Clients::WidgetClient^ c = CreateWidgetClient();
	if(c != nullptr)
	{
		try
		{
			c->AddBankReference(gcnew System::String(widgetPath));
		}
		catch(Exception^ e)
		{
			DebugWrite(gcnew System::String(widgetPath), e->Message);
		}
	}
}

void RSGManagedServices::RemoteClientWrapper::AddBankReference(unsigned int widgetId)
{
	RSG::Rag::Clients::WidgetClient^ c = CreateWidgetClient();
	if(c != nullptr)
	{
		try
		{
			c->AddBankReference(widgetId);
		}
		catch(Exception^ e)
		{
			DebugWrite(widgetId.ToString(), e->Message);
		}
	}
}

void RSGManagedServices::RemoteClientWrapper::RemoveBankReference(const char* widgetPath)
{
	RSG::Rag::Clients::WidgetClient^ c = CreateWidgetClient();
	if(c != nullptr)
	{
		try
		{
			c->RemoveBankReference(gcnew System::String(widgetPath));
		}
		catch(Exception^ e)
		{
			DebugWrite(gcnew System::String(widgetPath), e->Message);
		}
	}
}

void RSGManagedServices::RemoteClientWrapper::RemoveBankReference(unsigned int widgetId)
{
	RSG::Rag::Clients::WidgetClient^ c = CreateWidgetClient();
	if(c != nullptr)
	{
		try
		{
			c->RemoveBankReference(widgetId);
		}
		catch(Exception^ e)
		{
			DebugWrite(widgetId.ToString(), e->Message);
		}
	}
}

//void RSGManagedServices::WidgetClient::WriteMatrix33Widget(const char* widgetPath, RSG::Base::Math::Matrix33f^ value)
//{
//	widgeyClient->WriteMatrix33Widget(gcnew System::String(widgetPath), value);
//}
//
//void RSGManagedServices::WidgetClient::WriteMatrix33Widget(unsigned int widgetId, RSG::Base::Math::Matrix33f^ value)
//{
//	widgeyClient->WriteMatrix33Widget(widgetId, value);
//}
//
//void RSGManagedServices::WidgetClient::WriteMatrix34Widget(const char* widgetPath, RSG::Base::Math::Matrix34f^ value)
//{
//	widgeyClient->WriteMatrix34Widget(gcnew System::String(widgetPath), value);
//}
//
//void RSGManagedServices::WidgetClient::WriteMatrix34Widget(unsigned int widgetId, RSG::Base::Math::Matrix34f^ value)
//{
//	widgeyClient->WriteMatrix34Widget(widgetId, value);
//}
//
//void RSGManagedServices::WidgetClient::WriteMatrix44Widget(const char* widgetPath, RSG::Base::Math::Matrix44f^ value)
//{
//	widgeyClient->WriteMatrix44Widget(gcnew System::String(widgetPath), value);
//}
//
//void RSGManagedServices::WidgetClient::WriteMatrix44Widget(unsigned int widgetId, RSG::Base::Math::Matrix44f^ value)
//{
//	widgeyClient->WriteMatrix44Widget(widgetId, value);
//}






//
//RSG::Base::Math::Matrix33f^ RSGManagedServices::WidgetClient::ReadMatrix33Widget(const char* widgetPath)
//{
//	return widgeyClient->ReadMatrix33Widget(gcnew System::String(widgetPath));
//}
//
//RSG::Base::Math::Matrix33f^ RSGManagedServices::WidgetClient::ReadMatrix33Widget(unsigned int widgetId)
//{
//	return widgeyClient->ReadMatrix33Widget(widgetId);
//}
//
//RSG::Base::Math::Matrix34f^ RSGManagedServices::WidgetClient::ReadMatrix34Widget(const char* widgetPath)
//{
//	return widgeyClient->ReadMatrix34Widget(gcnew System::String(widgetPath));
//}
//
//RSG::Base::Math::Matrix34f^ RSGManagedServices::WidgetClient::ReadMatrix34Widget(unsigned int widgetId)
//{
//	return widgeyClient->ReadMatrix34Widget(widgetId);
//}
//
//RSG::Base::Math::Matrix44f^ RSGManagedServices::WidgetClient::ReadMatrix44Widget(const char* widgetPath)
//{
//	return widgeyClient->ReadMatrix44Widget(gcnew System::String(widgetPath));
//}
//
//RSG::Base::Math::Matrix44f^ RSGManagedServices::WidgetClient::ReadMatrix44Widget(unsigned int widgetId)
//{
//	return widgeyClient->ReadMatrix44Widget(widgetId);
//}

RSGManagedServices::DummySubscription::DummySubscription(RSG::Rag::Contracts::Data::GameConnection^ connection)
{
	_gameConnection = connection;
	_gameStatus = true;
}

void RSGManagedServices::DummySubscription::OnGameConnected(RSG::Rag::Contracts::Data::GameConnection^ connection){}

void RSGManagedServices::DummySubscription::OnGameDisconnected(RSG::Rag::Contracts::Data::GameConnection^ connection)
{
	if(_gameConnection->Equals(connection))
	{
		_gameStatus = false;
		_gameConnection = nullptr;
	}
}

bool RSGManagedServices::DummySubscription::GetGameStatus()
{
	return _gameStatus;
}


