// Custom types for math functions, we don't want to use any RageCore functionality with rage math types.

class Vector2
{
public:
	Vector2(float x, float y){ _x = x; _y = y;};
	float _x;
	float _y;
};

class Vector3
{
public:
	Vector3(float x, float y, float z){ _x = x; _y = y; _z = z;};
	float _x;
	float _y;
	float _z;
};

class Vector4
{
public:
	Vector4(float x, float y, float z, float w){ _x = x; _y = y; _z = z; _w = w;};
	float _x;
	float _y;
	float _z;
	float _w;
};

enum VCRButton
{
	Pause,
	PlayForwards,
	PlayBackwards,
	StepForwards,
	StepBackwards,
	Rewind,
	FastForward,
};

