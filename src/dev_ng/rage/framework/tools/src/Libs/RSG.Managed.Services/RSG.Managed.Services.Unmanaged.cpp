#include "RSG.Managed.Services.h"

#pragma warning( disable : 4190 ) // We can ignore this warning about UDT (user-defined type) because we are using this solely in c++ - http://msdn.microsoft.com/en-us/library/1e02627y.aspx

using namespace System::Reflection;
using namespace System::IO;
using namespace System::Diagnostics;

static bool GlobalAssemblyResolver = false;

// By default a CLR dll will look for its dependencies in the executing processes path (.exe) and not its own path. 
// Resolve the dependent dll files to the CLR dll's directory path.
static Assembly^ AssemblyResolveEventHandler( Object^ sender, ResolveEventArgs^ args )
{
	String^ assemblyName = args->Name;

	// Strip irrelevant information, such as assembly, version etc.
	// Example: "Acme.Foobar, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null"
	if( assemblyName->Contains(",") ) 
	{
		assemblyName = assemblyName->Substring(0, assemblyName->IndexOf(","));
	}

	Assembly^ thisAssembly = Assembly::GetExecutingAssembly();
	String^ thisPath = thisAssembly->Location;
	String^ directory = Path::GetDirectoryName(thisPath);
	String^ pathToManagedAssembly = Path::Combine(directory, assemblyName + ".dll");

	Debug::WriteLine(pathToManagedAssembly);

	Assembly^ newAssembly = Assembly::LoadFile(pathToManagedAssembly);
	return newAssembly;
}

static void ResolveAssemblies()
{
	if(!GlobalAssemblyResolver)
	{
		AppDomain^ currentDomain = AppDomain::CurrentDomain;
		currentDomain->AssemblyResolve += gcnew ResolveEventHandler( AssemblyResolveEventHandler );
		GlobalAssemblyResolver = true;

		Debug::WriteLine("Resolve Assembly Handler");
	}
}

#ifdef __cplusplus
extern "C"
{
#endif

	__declspec(dllexport) bool Connect(const char* proxyAddress)
	{
		ResolveAssemblies();
		
		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		return w->AssignProxyAddress(proxyAddress);
	}

	__declspec(dllexport) void Disconnect()
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		return w->Disconnect();
	}

	__declspec(dllexport) bool IsConnected()
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		return w->IsConnected();
	}

	__declspec(dllexport) const char* GetPlatform()
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		return w->GetPlatform();
	}

	__declspec(dllexport) bool WidgetExistsByPath(const char* widgetPath)
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		return w->WidgetExists(widgetPath);
	}

	__declspec(dllexport) bool WidgetExistsById(unsigned int widgetId)
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		return w->WidgetExists(widgetId);
	}

	__declspec(dllexport) unsigned int GetWidgetId(const char* widgetPath)
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		return w->GetWidgetId(widgetPath);
	}

	__declspec(dllexport) void WriteBoolWidgetByPath(const char* widgetPath, bool value)
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		w->WriteBoolWidget(widgetPath, value);
	}

	__declspec(dllexport) void WriteBoolWidgetById(unsigned int widgetId, bool value)
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		w->WriteBoolWidget(widgetId, value);
	}

	__declspec(dllexport) void WriteFloatWidgetByPath(const char* widgetPath, float value)
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		w->WriteFloatWidget(widgetPath, value);
	}

	__declspec(dllexport) void WriteFloatWidgetById(unsigned int widgetId, float value)
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		w->WriteFloatWidget(widgetId, value);
	}

	__declspec(dllexport) void WriteIntWidgetByPath(const char* widgetPath, int value)
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		w->WriteIntWidget(widgetPath, value);
	}

	__declspec(dllexport) void WriteIntWidgetById(unsigned int widgetId, int value)
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		w->WriteIntWidget(widgetId, value);
	}

	__declspec(dllexport) void WriteStringWidgetByPath(const char* widgetPath, const char* value)
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		w->WriteStringWidget(widgetPath, value);
	}

	__declspec(dllexport) void WriteStringWidgetById(unsigned int widgetId, const char* value)
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		w->WriteStringWidget(widgetId, value);
	}

	__declspec(dllexport) void WriteVector2WidgetByPath(const char* widgetPath, Vector2 value)
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		w->WriteVector2Widget(widgetPath, value);
	}

	__declspec(dllexport) void WriteVector2WidgetById(unsigned int widgetId, Vector2 value)
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		w->WriteVector2Widget(widgetId, value);
	}

	__declspec(dllexport) void WriteVector3WidgetByPath(const char* widgetPath, Vector3 value)
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		w->WriteVector3Widget(widgetPath, value);
	}

	__declspec(dllexport) void WriteVector3WidgetById(unsigned int widgetId, Vector3 value)
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		w->WriteVector3Widget(widgetId, value);
	}

	__declspec(dllexport) void WriteVector4WidgetByPath(const char* widgetPath, Vector4 value)
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		w->WriteVector4Widget(widgetPath, value);
	}

	__declspec(dllexport) void WriteVector4WidgetById(unsigned int widgetId, Vector4 value)
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		w->WriteVector4Widget(widgetId, value);
	}

	__declspec(dllexport) void WriteDataWidgetByPath(const char* widgetPath, void* value, int length)
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		w->WriteDataWidget(widgetPath, value, length);
	}

	__declspec(dllexport) void WriteDataWidgetById(unsigned int widgetId, void* value, int length)
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		w->WriteDataWidget(widgetId, value, length);
	}

	__declspec(dllexport) bool ReadBoolWidgetByPath(const char* widgetPath)
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		return w->ReadBoolWidget(widgetPath);
	}

	__declspec(dllexport) bool ReadBoolWidgetById(unsigned int widgetId)
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		return w->ReadBoolWidget(widgetId);
	}

	__declspec(dllexport) float ReadFloatWidgetByPath(const char* widgetPath)
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		return w->ReadFloatWidget(widgetPath);
	}

	__declspec(dllexport) float ReadFloatWidgetById(unsigned int widgetId)
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		return w->ReadFloatWidget(widgetId);
	}

	__declspec(dllexport) int ReadIntWidgetByPath(const char* widgetPath)
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		return w->ReadIntWidget(widgetPath);
	}

	__declspec(dllexport) int ReadIntWidgetById(unsigned int widgetId)
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		return w->ReadIntWidget(widgetId);
	}

	__declspec(dllexport) const char* ReadStringWidgetByPath(const char* widgetPath)
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		return w->ReadStringWidget(widgetPath);
	}

	__declspec(dllexport) const char* ReadStringWidgetById(unsigned int widgetId)
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		return w->ReadStringWidget(widgetId);
	}

	__declspec(dllexport) Vector2 ReadVector2WidgetByPath(const char* widgetPath)
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		return w->ReadVector2Widget(widgetPath);
	}

	__declspec(dllexport) Vector2 ReadVector2WidgetById(unsigned int widgetId)
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		return w->ReadVector2Widget(widgetId);
	}

	__declspec(dllexport) Vector3 ReadVector3WidgetByPath(const char* widgetPath)
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		return w->ReadVector3Widget(widgetPath);
	}

	__declspec(dllexport) Vector3 ReadVector3WidgetById(unsigned int widgetId)
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		return w->ReadVector3Widget(widgetId);
	}

	__declspec(dllexport) Vector4 ReadVector4WidgetByPath(const char* widgetPath)
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		return w->ReadVector4Widget(widgetPath);
	}

	__declspec(dllexport) Vector4 ReadVector4WidgetById(unsigned int widgetId)
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		return w->ReadVector4Widget(widgetId);
	}

	__declspec(dllexport) const char* ReadDataWidgetByPath(const char* widgetPath)
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		return w->ReadDataWidget(widgetPath);
	}

	__declspec(dllexport) const char* ReadDataWidgetById(unsigned int widgetId)
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		return w->ReadDataWidget(widgetId);
	}

	__declspec(dllexport) void SendSyncCommand(/*unsigned int timeout = 0*/)
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		w->SendSyncCommand(0);
	}

	__declspec(dllexport) const char* SendCommand(const char* command)
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		return w->SendCommand(command);
	}

	__declspec(dllexport) void PressButtonByPath(const char* widgetPath)
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		w->PressButton(widgetPath);
	}

	__declspec(dllexport) void PressButtonById(unsigned int widgetId)
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		w->PressButton(widgetId);
	}

	__declspec(dllexport) void PressVCRButtonByPath(const char* widgetPath, VCRButton vcrButton)
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		w->PressVCRButton(widgetPath, vcrButton);
	}

	__declspec(dllexport) void PressVCRButtonById(unsigned int widgetId, VCRButton vcrButton)
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		w->PressVCRButton(widgetId, vcrButton);
	}

	__declspec(dllexport) void AddBankReferenceByPath(const char* widgetPath)
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		w->AddBankReference(widgetPath);
	}

	__declspec(dllexport) void AddBankReferenceById(unsigned int widgetId)
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		w->AddBankReference(widgetId);
	}

	__declspec(dllexport) void RemoveBankReferenceByPath(const char* widgetPath)
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		w->RemoveBankReference(widgetPath);
	}

	__declspec(dllexport) void RemoveBankReferenceById(unsigned int widgetId)
	{
		ResolveAssemblies();

		RSGManagedServices::RemoteClientWrapper^ w = RSGManagedServices::RemoteClientWrapper::GetInstance();
		w->RemoveBankReference(widgetId);
	}

#ifdef __cplusplus
}
#endif
