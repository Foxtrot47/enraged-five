// RSG.Managed.Services.h

#pragma once

#include <msclr\auto_gcroot.h>

#include "CustomTypes.h"
using namespace System;


namespace RSGManagedServices {

	ref class DummySubscription : public RSG::Rag::Contracts::Services::IConnectionEvents
	{ 
	public:
		DummySubscription(RSG::Rag::Contracts::Data::GameConnection^ connection);
		 virtual void OnGameConnected(RSG::Rag::Contracts::Data::GameConnection^ connection);
		 virtual void OnGameDisconnected(RSG::Rag::Contracts::Data::GameConnection^ connection);
		 bool GetGameStatus();
	private:
		RSG::Rag::Contracts::Data::GameConnection^ _gameConnection;
		bool _gameStatus;
	};

	// Widget Client Wrapper for communication with the RSG.Rag.Clients widget client. Singleton instance so we can store a game connection.
	public ref class RemoteClientWrapper
	{
	private:
		static RSG::Rag::Contracts::Data::GameConnection^ _gameConnection;
		static RSG::Rag::Clients::ConnectionSubscriptionClient^ _subscriptionClient;
		static DummySubscription^ _dummy;
		static RemoteClientWrapper^ _instance;
	
		RemoteClientWrapper()
		{
			_gameConnection = nullptr;
		}
	public:
		static RemoteClientWrapper^ GetInstance() {return _instance;};

		bool AssignProxyAddress(const char* proxyAddress);
		RSG::Rag::Clients::WidgetClient^ CreateWidgetClient();
		RSG::Rag::Clients::ConsoleClient^ CreateConsoleClient();

		bool IsConnected();
		void Disconnect();

		const char* GetPlatform();

		bool WidgetExists(const char* widgetPath);
		bool WidgetExists(unsigned int widgetId);
		unsigned int GetWidgetId(const char* widgetPath);

		void WriteBoolWidget(const char* widgetPath, bool value);
		void WriteBoolWidget(unsigned int widgetId, bool value);
		void WriteFloatWidget(const char* widgetPath, float value);
		void WriteFloatWidget(unsigned int widgetId, float value);
		void WriteIntWidget(const char* widgetPath, int value);
		void WriteIntWidget(unsigned int widgetId, int value);
		void WriteStringWidget(const char* widgetPath, const char*  value);
		void WriteStringWidget(unsigned int widgetId, const char*  value);

		void WriteDataWidget(const char* widgetPath, void* value, int length);
		void WriteDataWidget(unsigned int widgetId, void* value, int length);

		bool ReadBoolWidget(const char* widgetPath);
		bool ReadBoolWidget(unsigned int widgetId);
		float ReadFloatWidget(const char* widgetPath);
		float ReadFloatWidget(unsigned int widgetId);
		int ReadIntWidget(const char* widgetPath);
		int ReadIntWidget(unsigned int widgetId);
		const char* ReadStringWidget(const char* widgetPath);
		const char* ReadStringWidget(unsigned int widgetId);

		Vector2 ReadVector2Widget(const char* widgetPath);
		Vector2 ReadVector2Widget(unsigned int widgetId);
		Vector3 ReadVector3Widget(const char* widgetPath);
		Vector3 ReadVector3Widget(unsigned int widgetId);
		Vector4 ReadVector4Widget(const char* widgetPath);
		Vector4 ReadVector4Widget(unsigned int widgetId);

		const char* ReadDataWidget(const char* widgetPath);
		const char* ReadDataWidget(unsigned int widgetId);

		void WriteVector2Widget(const char* widgetPath, Vector2 value);
		void WriteVector2Widget(unsigned int widgetId, Vector2 value);
		void WriteVector3Widget(const char* widgetPath, Vector3 value);
		void WriteVector3Widget(unsigned int widgetId, Vector3 value);
		void WriteVector4Widget(const char* widgetPath, Vector4 value);
		void WriteVector4Widget(unsigned int widgetId, Vector4 value);
		
		//void WriteMatrix33Widget(const char* widgetPath, RSG::Base::Math::Matrix33f^ value);
		//void WriteMatrix33Widget(unsigned int widgetId, RSG::Base::Math::Matrix33f^ value);
		//void WriteMatrix34Widget(const char* widgetPath, RSG::Base::Math::Matrix34f^ value);
		//void WriteMatrix34Widget(unsigned int widgetId, RSG::Base::Math::Matrix34f^ value);
		//void WriteMatrix44Widget(const char* widgetPath, RSG::Base::Math::Matrix44f^ value);
		//void WriteMatrix44Widget(unsigned int widgetId, RSG::Base::Math::Matrix44f^ value);
			
		//RSG::Base::Math::Matrix33f^ ReadMatrix33Widget(const char* widgetPath);
		//RSG::Base::Math::Matrix33f^ ReadMatrix33Widget(unsigned int widgetId);
		//RSG::Base::Math::Matrix34f^ ReadMatrix34Widget(const char* widgetPath);
		//RSG::Base::Math::Matrix34f^ ReadMatrix34Widget(unsigned int widgetId);
		//RSG::Base::Math::Matrix44f^ ReadMatrix44Widget(const char* widgetPath);
		//RSG::Base::Math::Matrix44f^ ReadMatrix44Widget(unsigned int widgetId);

		void SendSyncCommand(unsigned int timeout);

		void PressButton(const char* widgetPath);
		void PressButton(unsigned int widgetId);

		void PressVCRButton(const char* widgetPath, VCRButton vcrButton);
		void PressVCRButton(unsigned int widgetId, VCRButton vcrButton);

		const char* SendCommand(const char* command);

		void AddBankReference(const char* widgetPath);
		void AddBankReference(unsigned int widgetId);
		void RemoveBankReference(const char* widgetPath);
		void RemoveBankReference(unsigned int widgetId);
	};
}
