#ifdef __cplusplus
extern "C"
{
#endif

#pragma warning( disable : 4190 ) // We can ignore this warning about UDT (user-defined type) because we are using this solely in c++ - http://msdn.microsoft.com/en-us/library/1e02627y.aspx

namespace Remote
{
	#include "CustomTypes.h"

	__declspec(dllexport) bool Connect(const char* proxyAddress);
	__declspec(dllexport) void Disconnect();
	__declspec(dllexport) bool IsConnected();
	__declspec(dllexport) bool WidgetExistsByPath(const char* widgetPath);
	__declspec(dllexport) bool WidgetExistsById(unsigned int widgetId);
	__declspec(dllexport) unsigned int GetWidgetId(const char* widgetPath);

	__declspec(dllexport) void WriteBoolWidgetByPath(const char* widgetPath, bool value);
	__declspec(dllexport) void WriteFloatWidgetByPath(const char* widgetPath, float value);
	__declspec(dllexport) void WriteIntWidgetByPath(const char* widgetPath, int value);
	__declspec(dllexport) void WriteStringWidgetByPath(const char* widgetPath, const char* value);
	__declspec(dllexport) void WriteBoolWidgetById(unsigned int widgetId, bool value);
	__declspec(dllexport) void WriteFloatWidgetById(unsigned int widgetId, float value);
	__declspec(dllexport) void WriteIntWidgetById(unsigned int widgetId, int value);
	__declspec(dllexport) void WriteStringWidgetById(unsigned int widgetId, const char* value);

	__declspec(dllexport) void WriteVector2WidgetByPath(const char* widgetPath, Vector2 value);
	__declspec(dllexport) void WriteVector2WidgetById(unsigned int widgetId, Vector2 value);
	__declspec(dllexport) void WriteVector3WidgetByPath(const char* widgetPath, Vector3 value);
	__declspec(dllexport) void WriteVector3WidgetById(unsigned int widgetId, Vector3 value);
	__declspec(dllexport) void WriteVector4WidgetByPath(const char* widgetPath, Vector4 value);
	__declspec(dllexport) void WriteVector4WidgetById(unsigned int widgetId, Vector4 value);

	__declspec(dllexport) void WriteDataWidgetByPath(const char* widgetPath, void* value, int length);
	__declspec(dllexport) void WriteDataWidgetById(unsigned int widgetId, void* value, int length);

	__declspec(dllexport) bool ReadBoolWidgetByPath(const char* widgetPath);
	__declspec(dllexport) float ReadFloatWidgetByPath(const char* widgetPath);
	__declspec(dllexport) int ReadIntWidgetByPath(const char* widgetPath);
	__declspec(dllexport) const char* ReadStringWidgetByPath(const char* widgetPath);
	__declspec(dllexport) bool ReadBoolWidgetById(unsigned int widgetId);
	__declspec(dllexport) float ReadFloatWidgetById(unsigned int widgetId);
	__declspec(dllexport) int ReadIntWidgetById(unsigned int widgetId);
	__declspec(dllexport) const char* ReadStringWidgetById(unsigned int widgetId);

	__declspec(dllexport) const char* ReadDataWidgetByPath(const char* widgetPath);
	__declspec(dllexport) const char* ReadDataWidgetById(unsigned int widgetId);

	__declspec(dllexport) Vector2 ReadVector2WidgetByPath(const char* widgetPath);
	__declspec(dllexport) Vector2 ReadVector2WidgetById(unsigned int widgetId);
	__declspec(dllexport) Vector3 ReadVector3WidgetByPath(const char* widgetPath);
	__declspec(dllexport) Vector3 ReadVector3WidgetById(unsigned int widgetId);
	__declspec(dllexport) Vector4 ReadVector4WidgetByPath(const char* widgetPath);
	__declspec(dllexport) Vector4 ReadVector4WidgetById(unsigned int widgetId);

	__declspec(dllexport) void SendSyncCommand(/*unsigned int timeout = 0*/);
	__declspec(dllexport) const char* SendCommand(const char* command);

	__declspec(dllexport) const char* GetPlatform();

	__declspec(dllexport) void PressButtonByPath(const char* widgetPath);
	__declspec(dllexport) void PressButtonById(unsigned int widgetId);

	__declspec(dllexport) void PressVCRButtonByPath(const char* widgetPath, VCRButton vcrButton);
	__declspec(dllexport) void PressVCRButtonById(unsigned int widgetId, VCRButton vcrButton);

	__declspec(dllexport) void AddBankReferenceByPath(const char* widgetPath);
	__declspec(dllexport) void AddBankReferenceById(unsigned int widgetId);

	__declspec(dllexport) void RemoveBankReferenceByPath(const char* widgetPath);
	__declspec(dllexport) void RemoveBankReferenceById(unsigned int widgetId);
}

#ifdef __cplusplus
}
#endif
