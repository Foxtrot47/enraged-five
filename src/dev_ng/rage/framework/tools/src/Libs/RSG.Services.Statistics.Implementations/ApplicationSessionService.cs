﻿//---------------------------------------------------------------------------------------------
// <copyright file="ApplicationSessionService.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using NHibernate;
using RSG.Base.Logging;
using RSG.Services.Configuration;
using RSG.Services.Persistence;
using RSG.Services.Statistics.Contracts.ServiceContracts;
using Db = RSG.Services.Statistics.Data.Entities;
using Dto = RSG.Services.Statistics.Contracts.DataContracts;

namespace RSG.Services.Statistics.Implementations
{
    /// <summary>
    /// Service implementation for the <see cref="IApplicationSessionService"/> service
    /// contract.
    /// </summary>
    public class ApplicationSessionService : TransactionalService, IApplicationSessionService
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ApplicationSessionService"/>
        /// class.
        /// </summary>
        public ApplicationSessionService(IServer server)
            : base(server)
        {
        }
        #endregion

        #region IApplicationSessionService Methods
        /// <summary>
        /// Tracks a new application session.
        /// </summary>
        /// <param name="applicationSessionDto"></param>
        public void AddSession(Dto.ApplicationSession applicationSessionDto)
        {
            // Validate the supplied data.
            if (applicationSessionDto.OpenTimestamp.Kind != DateTimeKind.Utc)
            {
                throw new ArgumentException("OpenTimestamp must be in Utc.");
            }

            ExecuteAsTransaction(session => AddSessionAction(session, applicationSessionDto));
        }

        /// <summary>
        /// Updates an application session with exit information.
        /// </summary>
        /// <param name="applicationSessionExitDto"></param>
        public void SetSessionExitInfo(Dto.ApplicationSessionExit applicationSessionExitDto)
        {
            // Validate the supplied data.
            if (applicationSessionExitDto.CloseTimestamp.Kind != DateTimeKind.Utc)
            {
                throw new ArgumentException("CloseTimestamp must be in Utc.");
            }

            ExecuteAsTransaction(session => SetSessionExitInfoAction(session, applicationSessionExitDto));
        }
        #endregion

        #region Transaction Methods
        /// <summary>
        /// Executes the NHibernate logic to add a new application session object that
        /// will be wrapped up inside of a transaction.
        /// </summary>
        /// <param name="session"></param>
        /// <param name="appSession"></param>
        private void AddSessionAction(ISession session, Dto.ApplicationSession applicationSessionDto)
        {
            Db.ApplicationSession applicationSession = CreateApplicationSessionForDto(session, applicationSessionDto);
            session.Save(applicationSession);
        }

        /// <summary>
        /// Retrieves an application database object for the specified dto object.  A new
        /// object gets added to the database if it doesn't yet exist.
        /// </summary>
        /// <param name="session"></param>
        /// <param name="applicationDto"></param>
        /// <returns></returns>
        private Db.Application CreateApplicationForDto(ISession session, Dto.Application applicationDto)
        {
            Db.Application application =
                session.QueryOver<Db.Application>()
                    .Where(a => a.ApplicationName == applicationDto.ApplicationName)
                    .And(a => a.Version == applicationDto.Version)
                    .And(a => a.Project == applicationDto.Project)
                    .And(a => a.ToolsRoot == applicationDto.ToolsRoot)
                    .And(a => a.LaunchLocation == applicationDto.LaunchLocation)
                    .SingleOrDefault();

            if (application == null)
            {
                application = new Db.Application();
                application.ApplicationName = applicationDto.ApplicationName;
                application.Version = applicationDto.Version;
                application.Project = applicationDto.Project;
                application.ToolsRoot = applicationDto.ToolsRoot;
                application.LaunchLocation = applicationDto.LaunchLocation;
                session.Save(application);
            }

            return application;
        }

        /// <summary>
        /// Converts an application session dto object into a database object.
        /// </summary>
        /// <param name="session"></param>
        /// <param name="sessionDto"></param>
        /// <returns></returns>
        private Db.ApplicationSession CreateApplicationSessionForDto(ISession session, Dto.ApplicationSession sessionDto)
        {
            Db.ApplicationSession applicationSession = new Db.ApplicationSession();
            applicationSession.SessionGuid = sessionDto.SessionGuid;
            applicationSession.OpenTimestamp = sessionDto.OpenTimestamp;

            // Check whether there is a parent session guid.
            if (sessionDto.ParentSessionGuid.HasValue)
            {
                Db.ApplicationSession parentApplicationSession =
                    session.QueryOver<Db.ApplicationSession>()
                        .Where(a => a.SessionGuid == sessionDto.ParentSessionGuid.Value)
                        .SingleOrDefault();
                if (parentApplicationSession == null)
                {
                    Log.Error("Unable to find parent session row with guid '{0}'", sessionDto.ParentSessionGuid.Value);
                }
                applicationSession.ParentSession = parentApplicationSession;
            }

            applicationSession.Application = CreateApplicationForDto(session, sessionDto.Application);
            applicationSession.User = CreateUserForDto(session, sessionDto.User);
            applicationSession.Machine = CreateMachineForDto(session, sessionDto.Machine);

            return applicationSession;
        }

        /// <summary>
        /// Retrieves a machine database object for the specified dto object.  A new
        /// object gets added to the database if it doesn't yet exist.
        /// </summary>
        /// <param name="session"></param>
        /// <param name="machineDto"></param>
        /// <returns></returns>
        private Db.Machine CreateMachineForDto(ISession session, Dto.Machine machineDto)
        {
            Db.Machine machine =
                session.QueryOver<Db.Machine>()
                    .Where(m => m.MachineName == machineDto.MachineName)
                    .And(m => m.ProcessorName == machineDto.ProcessorName)
                    .And(m => m.LogicalProcessorCount == machineDto.LogicalProcessorCount)
                    .And(m => m.CoreCount == machineDto.CoreCount)
                    .And(m => m.MemoryCapacity == machineDto.MemoryCapacity)
                    .And(m => m.GraphicsCardName == machineDto.GraphicsCardName)
                    .SingleOrDefault();

            if (machine == null)
            {
                machine = new Db.Machine();
                machine.MachineName = machineDto.MachineName;
                machine.ProcessorName = machineDto.ProcessorName;
                machine.LogicalProcessorCount = machineDto.LogicalProcessorCount;
                machine.CoreCount = machineDto.CoreCount;
                machine.MemoryCapacity = machineDto.MemoryCapacity;
                machine.GraphicsCardName = machineDto.GraphicsCardName;
                session.Save(machine);
            }

            return machine;
        }

        /// <summary>
        /// Retrieves an user database object for the specified dto object.  A new
        /// object gets added to the database if it doesn't yet exist.
        /// </summary>
        /// <param name="session"></param>
        /// <param name="userDto"></param>
        /// <returns></returns>
        private Db.User CreateUserForDto(ISession session, Dto.User userDto)
        {
            Db.User user =
                session.QueryOver<Db.User>()
                    .Where(u => u.UserName == userDto.UserName)
                    .And(u => u.Studio == userDto.Studio)
                    .SingleOrDefault();

            if (user == null)
            {
                user = new Db.User();
                user.UserName = userDto.UserName;
                user.Studio = userDto.Studio;
                session.Save(user);
            }

            return user;
        }

        /// <summary>
        /// Retrieves an application session database object for the specified guid.
        /// </summary>
        /// <param name="session"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException">
        /// Thrown if the application session guid doesn't exist in the database.
        /// </exception>
        private Db.ApplicationSession GetApplicationSessionByGuid(ISession session, Guid guid)
        {
            Db.ApplicationSession applicationSession =
                session.QueryOver<Db.ApplicationSession>()
                    .Where(a => a.SessionGuid == guid)
                    .SingleOrDefault();
            if (applicationSession == null)
            {
                ArgumentException exception = new ArgumentException("Application session doesn't exist in database.");
                exception.Data.Add("SessionGuid", guid);
                throw exception;
            }
            return applicationSession;
        }

        /// <summary>
        /// Executes the NHibernate logic to update an application session object with
        /// it's exit information.
        /// </summary>
        /// <param name="session"></param>
        /// <param name="exitInfoDto"></param>
        private void SetSessionExitInfoAction(ISession session, Dto.ApplicationSessionExit exitInfoDto)
        {
            Db.ApplicationSession applicationSession = GetApplicationSessionByGuid(session, exitInfoDto.SessionGuid);
            applicationSession.CloseTimestamp = exitInfoDto.CloseTimestamp;
            applicationSession.ExitCode = exitInfoDto.ExitCode;
            session.Save(applicationSession);
        }
        #endregion
    }
}
