﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProfileSessionService.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using NHibernate;
using RSG.Services.Configuration;
using RSG.Services.Persistence;
using RSG.Services.Statistics.Contracts.ServiceContracts;
using Db = RSG.Services.Statistics.Data.Entities;
using Dto = RSG.Services.Statistics.Contracts.DataContracts;

namespace RSG.Services.Statistics.Implementations
{
    /// <summary>
    /// Service implementation for the <see cref="IProfileSessionService"/> service
    /// contract.
    /// </summary>
    public class ProfileSessionService : TransactionalService, IProfileSessionService
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ProfileSessionService"/>
        /// class.
        /// </summary>
        public ProfileSessionService(IServer server)
            : base(server)
        {
        }
        #endregion

        #region IProfileSessionService Methods
        /// <summary>
        /// Tracks a new profile session.
        /// </summary>
        /// <param name="profileSessionDto"></param>
        public void AddSession(Dto.ProfileSession profileSessionDto)
        {
            // Validate the supplied data.
            if (profileSessionDto.StartTime.Kind != DateTimeKind.Utc)
            {
                throw new ArgumentException("StartTime must be in Utc.");
            }
            if (profileSessionDto.EndTime.Kind != DateTimeKind.Utc)
            {
                throw new ArgumentException("EndTime must be in Utc.");
            }

            ExecuteAsTransaction(session => AddSessionAction(session, profileSessionDto));
        }
        #endregion

        #region Transaction Methods
        /// <summary>
        /// Executes the NHibernate logic to add a new profile session object that
        /// will be wrapped up inside of a transaction.
        /// </summary>
        /// <param name="session"></param>
        /// <param name="appSession"></param>
        private void AddSessionAction(ISession session, Dto.ProfileSession profileSessionDto)
        {
            Db.ProfileSession profileSession = CreateProfileSessionForDto(session, profileSessionDto);
            session.Save(profileSession);
        }
        
        /// <summary>
        /// Converts a profile sample dto object into a database object.
        /// </summary>
        /// <param name="sampleDto"></param>
        /// <param name="session"></param>
        /// <param name="profileSession"></param>
        /// <param name="parentSample"></param>
        /// <returns></returns>
        private Db.ProfileSample CreateProfileSampleForDto(
            Dto.ProfileSample sampleDto,
            Db.ProfileSession profileSession,
            Db.ProfileSample parentSample)
        {
            // Validate the supplied data.
            if (sampleDto.StartTime.Kind != DateTimeKind.Utc)
            {
                throw new ArgumentException("StartTime must be in Utc.");
            }
            if (sampleDto.EndTime.Kind != DateTimeKind.Utc)
            {
                throw new ArgumentException("EndTime must be in Utc.");
            }

            Db.ProfileSample sample = new Db.ProfileSample();
            sample.ChildSamples = new List<Db.ProfileSample>();
            sample.EndTime = sampleDto.EndTime;
            sample.Metric = sampleDto.Metric;
            sample.ParentSample = parentSample;
            sample.ProfileSession = profileSession;
            sample.StartTime = sampleDto.StartTime;

            foreach (Dto.ProfileSample childSampleDto in sampleDto.ChildSamples)
            {
                Db.ProfileSample childSample = CreateProfileSampleForDto(childSampleDto, profileSession, sample);
                sample.ChildSamples.Add(childSample);
            }

            return sample;
        }

        /// <summary>
        /// Converts a profile session dto object into a database object.
        /// </summary>
        /// <param name="sessionDto"></param>
        /// <param name="session"></param>
        /// <returns></returns>
        private Db.ProfileSession CreateProfileSessionForDto(ISession session, Dto.ProfileSession sessionDto)
        {
            Db.ProfileSession profileSession;
            if (sessionDto is Dto.ExportProfileSession)
            {
                Db.ExportProfileSession exportProfileSession = new Db.ExportProfileSession();
                SetExportProfileSessionData(session, exportProfileSession, sessionDto as Dto.ExportProfileSession);
                profileSession = exportProfileSession;
            }
            else
            {
                profileSession = new Db.ProfileSession();
            }
            profileSession.ApplicationSession = GetApplicationSessionByGuid(session, sessionDto.ApplicationSessionGuid);
            profileSession.EndTime = sessionDto.EndTime;
            profileSession.Name = sessionDto.Name;
            profileSession.ParentSessionGuid = sessionDto.ParentSessionGuid;
            profileSession.SessionGuid = sessionDto.SessionGuid;
            profileSession.StartTime = sessionDto.StartTime;
            profileSession.TopLevelSamples = new List<Db.ProfileSample>();

            foreach (Dto.ProfileSample sampleDto in sessionDto.Samples)
            {
                Db.ProfileSample sample = CreateProfileSampleForDto(sampleDto, profileSession, null);
                profileSession.TopLevelSamples.Add(sample);
            }

            return profileSession;
        }

        /// <summary>
        /// Retrieves an application session database object for the specified guid.
        /// </summary>
        /// <param name="session"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException">
        /// Thrown if the application session guid doesn't exist in the database.
        /// </exception>
        private Db.ApplicationSession GetApplicationSessionByGuid(ISession session, Guid guid)
        {
            Db.ApplicationSession applicationSession =
                session.QueryOver<Db.ApplicationSession>()
                    .Where(a => a.SessionGuid == guid)
                    .SingleOrDefault();
            if (applicationSession == null)
            {
                ArgumentException exception = new ArgumentException("Application session doesn't exist in database.");
                exception.Data.Add("SessionGuid", guid);
                throw exception;
            }
            return applicationSession;
        }

        /// <summary>
        /// Retrieves an export file database object for the specified filepath.
        /// </summary>
        /// <param name="session"></param>
        /// <param name="filepath"></param>
        /// <returns></returns>
        private Db.ExportFile GetExportFileByPath(ISession session, string filepath)
        {
            Db.ExportFile exportFile =
                session.QueryOver<Db.ExportFile>()
                    .Where(e => e.Filepath == filepath)
                    .SingleOrDefault();
            if (exportFile == null)
            {
                exportFile = new Db.ExportFile();
                exportFile.Filepath = filepath;
                session.Save(exportFile);
            }
            return exportFile;
        }

        /// <summary>
        /// Sets any export profile session specific data for the database object.
        /// </summary>
        /// <param name="session"></param>
        /// <param name="profileSession"></param>
        /// <param name="sessionDto"></param>
        private void SetExportProfileSessionData(ISession session, Db.ExportProfileSession profileSession, Dto.ExportProfileSession sessionDto)
        {
            profileSession.ExportedFiles = new List<Db.ExportFile>();
            profileSession.IncredibuildEnabled = sessionDto.IncredibuildEnabled;
            profileSession.IncredibuildStandalone = sessionDto.IncredibuildStandalone;
            profileSession.IncredibuildForcedCPUCount = sessionDto.IncredibuildForcedCPUCount;
            profileSession.Platforms = sessionDto.Platforms.ToList();
            profileSession.PrivateBytesEnd = sessionDto.PrivateBytesEnd;
            profileSession.PrivateBytesStart = sessionDto.PrivateBytesStart;
            profileSession.Success = sessionDto.Success;
            profileSession.ToolsVersion = sessionDto.ToolsVersion;

            foreach (string filepath in sessionDto.ExportedFiles)
            {
                string fullPath = Path.GetFullPath(filepath).ToLower();
                Db.ExportFile exportFile = GetExportFileByPath(session, fullPath);
                profileSession.ExportedFiles.Add(exportFile);
            }
        }
        #endregion
    }
}
