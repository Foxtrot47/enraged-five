﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows.Media.Imaging;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using AutoMapper;
using RSG.Base.ConfigParser;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Base.Math;
using RSG.Model.Animation;
using RSG.Model.Asset.Factories;
using RSG.Model.Asset.Level;
using RSG.Model.Asset.Platform;
using RSG.Model.Asset.RadioStation;
using RSG.Model.Character;
using RSG.Model.Common;
using RSG.Model.Common.Animation;
using RSG.Model.Common.Map;
using RSG.Model.Common.Mission;
using RSG.Model.Common.RadioStation;
using RSG.Model.Map3;
using RSG.Model.Vehicle;
using RSG.Model.Weapon;
using RSG.Platform;
using RSG.Statistics.Client.ServiceClients;
using RSG.Statistics.Common;
using RSG.Statistics.Common.Dto.Enums;
using RSG.Statistics.Common.Dto.GameAssets;
using RSG.Statistics.Common.Dto.GameAssets.Bundles;
using RSG.Statistics.Common.Dto.GameAssetStats;
using RSG.Statistics.Common.Dto.GameAssetStats.Bundles;
using RSG.Statistics.Common.Model.Telemetry;
using RSG.Base.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Collections;
using RSG.Statistics.Common.Model.GameAssets;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Build;
using RSG.Pipeline.Services;
using RSG.Statistics.Common.Config;
using RSG.Base.Configuration.Bugstar;
using RSG.Model.Asset.GameText;
using RSG.Model.Common.GameText;
using RSG.Model.Asset.Util;
using RSG.Interop.Bugstar;
using RSG.Interop.Bugstar.Organisation;
using RSG.Interop.Bugstar.Game;

namespace RSG.Statistics.Client.Common
{
    /// <summary>
    /// This class can be used for syncing various game asset stats between
    /// scenexml/bugstar and the statistics server.
    /// </summary>
    public class AssetSyncService
    {
        #region Constants
        /// <summary>
        /// Path to the network options xml file relative to the build directory
        /// </summary>
        private const string c_networkOptionsRelativePath = @"$(common)\data\networkoptions.xml";

        /// <summary>
        /// Path to the levels xml file relative to the build directory
        /// </summary>
        private const string c_levelsRelativePath = @"$(common)\data\levels.xml";

        /// <summary>
        /// Path to the debug locations for stats capture zone names.
        /// </summary>
        private const String c_debugLocationListPath = @"$(common)\data\debugLocationList.xml";

        /// <summary>
        /// Path the game_strings xml file relative to the etc directory.
        /// </summary>
        private const string c_gameStringsRelativePath = @"$(toolsconfig)\config\statistics\game_strings.xml";

        /// <summary>
        /// Files which will contain the hashes for shop items.
        /// </summary>
        private const String c_shopsFile = @"$(text)\American\americanShops.txt";
        private const String c_vehiclesFile = @"$(text)\American\americanVehicleMods.txt";
        private const String c_weaponsFile = @"$(text)\American\americanWeapons.txt";
        private const String c_menuFile = @"$(text)\American\americanMenu.txt";
        #endregion // Constants

        #region Member Data
        /// <summary>
        /// Configuration object.
        /// </summary>
        private IBranch Branch { get; set; }

        /// <summary>
        /// Bugstar configuration object.
        /// </summary>
        private IBugstarConfig _bugstarConfig;

        /// <summary>
        /// Read-only bugstar connection.
        /// </summary>
        private BugstarConnection _bugstarConnection;

        /// <summary>
        /// Private log object.
        /// </summary>
        protected ILog Log { get; private set; }

        /// <summary>
        /// Content tree.
        /// </summary>
        private IContentTree ContentTree
        {
            get
            {
                if (m_contentTree == null)
                {
                    m_contentTree = RSG.Pipeline.Content.Factory.CreateTree(Branch);
                }
                return m_contentTree;
            }
        }
        private IContentTree m_contentTree;

        /// <summary>
        /// Statistics server to communicate with.
        /// </summary>
        protected IStatsServer m_server;
        #endregion

        #region Asset Collections
        /// <summary>
        /// Clip Dictionary collection 
        /// </summary>
        private IClipDictionaryCollection ClipDictionaries
        {
            get
            {
                if (m_clipDictionaries == null)
                {
                    AnimationFactory animationFactory = new AnimationFactory(Branch, ContentTree);
                    m_clipDictionaries = animationFactory.CreateClipDictionaryCollection(DataSource.SceneXml);
                }
                return m_clipDictionaries;
            }
        }
        private IClipDictionaryCollection m_clipDictionaries;

        /// <summary>
        /// Collection of all the levels (from the config).
        /// </summary>
        private ILevelCollection Levels
        {
            get
            {
                if (m_levels == null)
                {
                    ILevelFactory levelFactory = new LevelFactory(Branch.Project.Config, _bugstarConfig);
                    m_levels = levelFactory.CreateCollection(DataSource.SceneXml);
                }
                return m_levels;
            }
        }
        private ILevelCollection m_levels;

        /// <summary>
        /// Collection of all the characters (from export data).
        /// </summary>
        private ICharacterCollection Characters
        {
            get
            {
                if (m_characters == null)
                {
                    m_characters = CharacterFactory.CreateCollection(DataSource.SceneXml, Branch);
                }
                return m_characters;
            }
        }
        private ICharacterCollection m_characters;

        /// <summary>
        /// Collection of all the cutscenes (from export data).
        /// </summary>
        private ICutsceneCollection Cutscenes
        {
            get
            {
                if (m_cutscenes == null)
                {
                    IAnimationFactory animationFactory = new AnimationFactory(Branch, ContentTree);
                    m_cutscenes = animationFactory.CreateCutsceneCollection(DataSource.SceneXml);
                }
                return m_cutscenes;
            }
        }
        private ICutsceneCollection m_cutscenes;

        /// <summary>
        /// Collection of all the vehicles (from export data).
        /// </summary>
        private IVehicleCollection Vehicles
        {
            get
            {
                if (m_vehicles == null)
                {
                    m_vehicles = VehicleFactory.CreateCollection(DataSource.SceneXml, Branch);
                }
                return m_vehicles;
            }
        }
        private IVehicleCollection m_vehicles;

        /// <summary>
        /// Collection of all the weapons (from export data).
        /// </summary>
        private IWeaponCollection Weapons
        {
            get
            {
                if (m_weapons == null)
                {
                    m_weapons = WeaponFactory.CreateCollection(DataSource.SceneXml, Branch);
                }
                return m_weapons;
            }
        }
        private IWeaponCollection m_weapons;

        /// <summary>
        /// Collection of all the radio stations (from export data).
        /// </summary>
        private IRadioStationCollection RadioStations
        {
            get
            {
                if (m_radioStations == null)
                {
                    RadioStationFactory radioStationFactory = new RadioStationFactory(Branch.Project.Config);
                    m_radioStations = radioStationFactory.CreateCollection(DataSource.SceneXml);
                }
                return m_radioStations;
            }
        }
        private IRadioStationCollection m_radioStations;

        /// <summary>
        /// Mapping of levels to map hierarchis
        /// </summary>
        private IDictionary<ILevel, IMapHierarchy> MapHierarchies
        {
            get
            {
                if (m_mapHierarchies == null)
                {
                    IMapFactory mapFactory = new MapFactory(Branch.Project.Config, _bugstarConfig, _bugstarConnection);

                    m_mapHierarchies = new Dictionary<ILevel, IMapHierarchy>();
                    foreach (ILevel level in Levels)
                    {
                        m_mapHierarchies[level] = mapFactory.CreateHierarchy(DataSource.SceneXml, level);
                    }
                }
                return m_mapHierarchies;
            }
        }
        private IDictionary<ILevel, IMapHierarchy> m_mapHierarchies;

        /// <summary>
        /// Collection of all the weapons (from export data).
        /// </summary>
        private IMissionCollection Missions
        {
            get
            {
                if (m_missions == null)
                {
                    Project project = Project.GetProjectById(_bugstarConnection, _bugstarConfig.ProjectId);

                    MissionFactory missionFactory = new MissionFactory();
                    m_missions = missionFactory.CreateCollection(true, project);
                }
                return m_missions;
            }
        }
        private IMissionCollection m_missions;
        #endregion // Asset Collections

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public AssetSyncService(IBranch branch, IStatsServer server)
        {
            Branch = branch;
            m_server = server;
            Log = LogFactory.CreateUniversalLog("Asset Sync Service");

            _bugstarConfig = ConfigFactory.CreateBugstarConfig(branch.Project);
            _bugstarConnection = new BugstarConnection(_bugstarConfig.RESTService, _bugstarConfig.AttachmentService);
            _bugstarConnection.Login(_bugstarConfig.ReadOnlyUsername, _bugstarConfig.ReadOnlyPassword, "");
        }

        /// <summary>
        /// Static constructor which is in charge of setting up the required automapper mappings.
        /// </summary>
        static AssetSyncService()
        {
            // Levels
            Mapper.CreateMap<ILevel, LevelDto>()
                .ForMember(dto => dto.Identifier, opt => opt.MapFrom(src => src.Hash.ToString()));

            Mapper.CreateMap<ILevel, LevelStatBundleDto>()
                .ForMember(dto => dto.LowerLeft, opt => opt.MapFrom(src => src.ImageBounds.IsEmpty ? null : src.ImageBounds.Min))
                .ForMember(dto => dto.UpperRight, opt => opt.MapFrom(src => src.ImageBounds.IsEmpty ? null : src.ImageBounds.Max));

            // Archetypes
            Mapper.CreateMap<IArchetype, ArchetypeDto>()
                .Include<IDrawableArchetype, DrawableArchetypeDto>()
                .Include<IFragmentArchetype, FragmentArchetypeDto>()
                .Include<IInteriorArchetype, InteriorArchetypeDto>()
                .Include<IStatedAnimArchetype, StatedAnimArchetypeDto>()
                .ForMember(dto => dto.Identifier, opt => opt.MapFrom(src => src.Hash.ToString()));

            Mapper.CreateMap<IDrawableArchetype, DrawableArchetypeDto>()
                .ForMember(dto => dto.Identifier, opt => opt.MapFrom(src => src.Hash.ToString()));
            Mapper.CreateMap<IFragmentArchetype, FragmentArchetypeDto>()
                .ForMember(dto => dto.Identifier, opt => opt.MapFrom(src => src.Hash.ToString()));
            Mapper.CreateMap<IInteriorArchetype, InteriorArchetypeDto>()
                .ForMember(dto => dto.Identifier, opt => opt.MapFrom(src => src.Hash.ToString()));
            Mapper.CreateMap<IStatedAnimArchetype, StatedAnimArchetypeDto>()
                .ForMember(dto => dto.Identifier, opt => opt.MapFrom(src => src.Hash.ToString()));

            Mapper.CreateMap<IDrawableArchetype, DrawableArchetypeStatBundle>()
                .ForMember(dto => dto.ArchetypeIdentifier, opt => opt.MapFrom(src => src.Hash.ToString()))
                .ForMember(dto => dto.Shaders, opt => opt.Ignore())
                .ForMember(dto => dto.Textures, opt => opt.Ignore())
                .ForMember(dto => dto.ShaderStats, opt => opt.MapFrom(src => Mapper.Map<IEnumerable<IShader>, List<ShaderStatBundleDto>>(src.Shaders)));
            Mapper.CreateMap<IFragmentArchetype, FragmentArchetypeStatBundle>()
                .ForMember(dto => dto.ArchetypeIdentifier, opt => opt.MapFrom(src => src.Hash.ToString()))
                .ForMember(dto => dto.Shaders, opt => opt.Ignore())
                .ForMember(dto => dto.Textures, opt => opt.Ignore())
                .ForMember(dto => dto.ShaderStats, opt => opt.MapFrom(src => Mapper.Map<IEnumerable<IShader>, List<ShaderStatBundleDto>>(src.Shaders)));
            Mapper.CreateMap<IInteriorArchetype, InteriorArchetypeStatBundle>()
                .ForMember(dto => dto.ArchetypeIdentifier, opt => opt.MapFrom(src => src.Hash.ToString()))
                .ForMember(dto => dto.Shaders, opt => opt.Ignore())
                .ForMember(dto => dto.Textures, opt => opt.Ignore())
                .ForMember(dto => dto.ShaderStats, opt => opt.MapFrom(src => Mapper.Map<IEnumerable<IShader>, List<ShaderStatBundleDto>>(src.Shaders)));
            Mapper.CreateMap<IStatedAnimArchetype, StatedAnimArchetypeStatBundle>()
                .ForMember(dto => dto.ArchetypeIdentifier, opt => opt.MapFrom(src => src.Hash.ToString()))
                .ForMember(dto => dto.ComponentArchetypeIdentifiers, opt => opt.MapFrom(src => src.ComponentArchetypes.Select(item => item.Hash.ToString())))
                .ForMember(dto => dto.Shaders, opt => opt.Ignore())
                .ForMember(dto => dto.Textures, opt => opt.Ignore())
                .ForMember(dto => dto.ShaderStats, opt => opt.MapFrom(src => Mapper.Map<IEnumerable<IShader>, List<ShaderStatBundleDto>>(src.Shaders)));

            Mapper.CreateMap<KeyValuePair<string, uint>, TxdExportStatDto>()
                .ForMember(dto => dto.Name, opt => opt.MapFrom(src => src.Key))
                .ForMember(dto => dto.Size, opt => opt.MapFrom(src => src.Value));

            Mapper.CreateMap<KeyValuePair<MapDrawableLODLevel, IMapDrawableLOD>, DrawableLodStatDto>()
                .ForMember(dto => dto.Name, opt => opt.MapFrom(src => src.Value.Name))
                .ForMember(dto => dto.PolygonCount, opt => opt.MapFrom(src => src.Value.PolygonCount))
                .ForMember(dto => dto.LodDistance, opt => opt.MapFrom(src => src.Value.LodDistance))
                .ForMember(dto => dto.LodLevel, opt => opt.MapFrom(src => src.Key));

            Mapper.CreateMap<RSG.Model.Common.Map.MapDrawableLODLevel, MapDrawableLodLevelDto>()
                .ForMember(dto => dto.Value, opt => opt.MapFrom(src => src.ToString()));

            Mapper.CreateMap<IRoom, RoomStatBundle>()
                .ForMember(dto => dto.InteriorIdentifier, opt => opt.MapFrom(src => src.ParentArchetype.Hash.ToString()))
                .ForMember(dto => dto.RoomIdentifier, opt => opt.MapFrom(src => src.Hash.ToString()))
                .ForMember(dto => dto.Shaders, opt => opt.Ignore())
                .ForMember(dto => dto.Textures, opt => opt.Ignore())
                .ForMember(dto => dto.ShaderStats, opt => opt.MapFrom(src => Mapper.Map<IEnumerable<IShader>, List<ShaderStatBundleDto>>(src.Shaders)));
            Mapper.CreateMap<IPortal, OldPortalStatDto>()
                .ForMember(dto => dto.InteriorIdentifier, opt => opt.MapFrom(src => src.ParentArchetype.Hash.ToString()))
                .ForMember(dto => dto.RoomAIdentifier, opt => opt.MapFrom(src => (src.RoomA != null ? src.RoomA.Hash.ToString() : null)))
                .ForMember(dto => dto.RoomBIdentifier, opt => opt.MapFrom(src => (src.RoomB != null ? src.RoomB.Hash.ToString() : null)));

            // Characters
            Mapper.CreateMap<ICharacter, CharacterDto>()
                .ForMember(dto => dto.Identifier, opt => opt.MapFrom(src => src.Hash.ToString()));

            Mapper.CreateMap<ICharacter, CharacterStatBundleDto>()
                .ForMember(dto => dto.Shaders, opt => opt.Ignore())
                .ForMember(dto => dto.Textures, opt => opt.Ignore())
                .ForMember(dto => dto.ShaderStats, opt => opt.MapFrom(src => Mapper.Map<IEnumerable<IShader>, List<ShaderStatBundleDto>>(src.Shaders)));

            Mapper.CreateMap<ICharacterPlatformStat, CharacterPlatformStatBundleDto>();

            // Cutscenes
            Mapper.CreateMap<ICutscene, CutsceneDto>()
                .ForMember(dto => dto.Identifier, opt => opt.MapFrom(src => src.Hash.ToString()));

            // Entities
            Mapper.CreateMap<IEntity, EntityDto>()
                .ForMember(dto => dto.Identifier, opt => opt.MapFrom(src => src.Hash.ToString()));

            Mapper.CreateMap<IEntity, EntityStatBundle>()
                .ForMember(dto => dto.EntityIdentifier, opt => opt.MapFrom(src => src.Hash.ToString()))
                .ForMember(dto => dto.ArchetypeIdentifier, opt => opt.MapFrom(src => (src.ReferencedArchetype != null ? src.ReferencedArchetype.Hash.ToString() : null)))
                .ForMember(dto => dto.ArchetypeType, opt => opt.MapFrom(src => (src.ReferencedArchetype != null ? src.ReferencedArchetype.GetType().Name : null)))
                .ForMember(dto => dto.ParentIdentifier, opt => opt.MapFrom(src => src.Parent.Hash.ToString()))
                .ForMember(dto => dto.ParentType, opt => opt.MapFrom(src => src.Parent.GetType().Name))
                .ForMember(dto => dto.LodParentEntityIdentifier, opt => opt.MapFrom(src => (src.LodParent != null ? src.LodParent.Hash.ToString() : null)))
                .ForMember(dto => dto.LodDistanceOverride, opt => opt.MapFrom(src => (src.LodDistanceOverride == Single.MinValue ? null : src.LodDistanceOverride)));

            Mapper.CreateMap<RSG.Model.Common.Map.LodLevel, LodLevelDto>()
                .ForMember(dto => dto.Value, opt => opt.MapFrom(src => src.ToString()));

            // Rooms
            Mapper.CreateMap<IRoom, RoomDto>()
                .ForMember(dto => dto.Identifier, opt => opt.MapFrom(src => src.Hash.ToString()));

            // Spawn Points
            Mapper.CreateMap<ISpawnPoint, SpawnPointStatDto>();

            Mapper.CreateMap<RSG.Model.Common.Map.SpawnPointAvailableModes, SpawnPointAvailableModeDto>()
                .ForMember(dto => dto.Value, opt => opt.MapFrom(src => src.ToString()));

            // Vehicles
            Mapper.CreateMap<IVehicle, VehicleDto>()
                .ForMember(dto => dto.Identifier, opt => opt.MapFrom(src => src.Hash.ToString()))
                .ForMember(dto => dto.Category, opt => opt.MapFrom(src => src.Category.ToString()));

            Mapper.CreateMap<IVehicle, VehicleStatBundleDto>()
                .ForMember(dto => dto.Shaders, opt => opt.Ignore())
                .ForMember(dto => dto.Textures, opt => opt.Ignore())
                .ForMember(dto => dto.ShaderStats, opt => opt.MapFrom(src => Mapper.Map<IEnumerable<IShader>, List<ShaderStatBundleDto>>(src.Shaders)));

            Mapper.CreateMap<IVehiclePlatformStat, VehiclePlatformStatBundleDto>();

            // Weapons
            Mapper.CreateMap<IWeapon, WeaponDto>()
                .ForMember(dto => dto.Identifier, opt => opt.MapFrom(src => src.Hash.ToString()));

            Mapper.CreateMap<IWeapon, WeaponStatBundleDto>()
                .ForMember(dto => dto.Shaders, opt => opt.Ignore())
                .ForMember(dto => dto.Textures, opt => opt.Ignore())
                .ForMember(dto => dto.ShaderStats, opt => opt.MapFrom(src => Mapper.Map<IEnumerable<IShader>, List<ShaderStatBundleDto>>(src.Shaders)));

            Mapper.CreateMap<IWeaponPlatformStat, WeaponPlatformStatBundleDto>();

            // Map Areas
            Mapper.CreateMap<IMapArea, MapAreaDto>()
                .ForMember(dto => dto.Identifier, opt => opt.MapFrom(src => src.Hash.ToString()));

            // Map Sections
            Mapper.CreateMap<IMapSection, MapSectionDto>()
                .ForMember(dto => dto.Identifier, opt => opt.MapFrom(src => src.Hash.ToString()));

            // Map nodes to 
            Mapper.CreateMap<IMapNode, BasicMapNodeStatBaseDto>()
                .Include<IMapArea, BasicMapAreaStatDto>()
                .Include<IMapSection, BasicMapSectionStatDto>()
                .ForMember(dto => dto.Identifier, opt => opt.MapFrom(src => src.Hash));

            Mapper.CreateMap<IMapArea, BasicMapAreaStatDto>()
                .ForMember(dto => dto.Identifier, opt => opt.MapFrom(src => src.Hash))
                .ForMember(dto => dto.ChildNodes, opt => opt.Ignore());
            Mapper.CreateMap<IMapArea, MapAreaStatBundleDto>();

            Mapper.CreateMap<IMapSection, BasicMapSectionStatDto>()
                .ForMember(dto => dto.Identifier, opt => opt.MapFrom(src => src.Hash))
                .ForMember(dto => dto.VectorMapPoints, opt => opt.MapFrom(src => src.VectorMapPoints));
            Mapper.CreateMap<IMapSection, MapSectionStatBundleDto>()
                .ForMember(dto => dto.Attributes, opt => opt.MapFrom(src => src.ContainerAttributes));
            Mapper.CreateMap<KeyValuePair<CollisionType, uint>, CollisionPolyStatDto>()
                .ForMember(dto => dto.CollisionFlags, opt => opt.MapFrom(src => (uint)src.Key))
                .ForMember(dto => dto.PolygonCount, opt => opt.MapFrom(src => src.Value));
            Mapper.CreateMap<KeyValuePair<RSG.Platform.Platform, IMapSectionPlatformStat>, MapSectionPlatformStatDto>()
                .ForMember(dto => dto.Platform, opt => opt.MapFrom(src => src.Key))
                .ForMember(dto => dto.FileTypeSizes, opt => opt.MapFrom(src => src.Value.FileTypeSizes));
            Mapper.CreateMap<KeyValuePair<FileType, IMemoryStat>, MapSectionMemoryStatDto>()
                .ForMember(dto => dto.FileType, opt => opt.MapFrom(src => src.Key))
                .ForMember(dto => dto.MemorySat, opt => opt.MapFrom(src => src.Value));

            Mapper.CreateMap<StatKey, MapSectionAggregateStatDto>();
            Mapper.CreateMap<IMapSectionStatistic, MapSectionAggregateStatDto>();
            Mapper.CreateMap<KeyValuePair<StatKey, IMapSectionStatistic>, MapSectionAggregateStatDto>()
                .ConstructUsing(item =>
                {
                    MapSectionAggregateStatDto aggregate = Mapper.Map<StatKey, MapSectionAggregateStatDto>(item.Key);
                    Mapper.Map<IMapSectionStatistic, MapSectionAggregateStatDto>(item.Value, aggregate);
                    return aggregate;
                });
            Mapper.CreateMap<IMemoryStat, MemoryStatDto>();

            Mapper.CreateMap<IMapSectionPlatformStat, MapSectionPlatformStatDto>();
            Mapper.CreateMap<KeyValuePair<string, object>, MapSectionAttributeStatDto>()
                .ForMember(dto => dto.Name, opt => opt.MapFrom(src => src.Key))
                .ForMember(dto => dto.Type, opt => opt.MapFrom(src => src.Value.GetType().Name))
                .ForMember(dto => dto.Value, opt => opt.MapFrom(src => src.Value.ToString()));
            Mapper.CreateMap<ICarGen, CarGenStatDto>();

            Mapper.CreateMap<RSG.Platform.FileType, FileTypeDto>()
                .ForMember(dto => dto.Value, opt => opt.MapFrom(src => src.ToString()));

            // Common Game Asset Stats
            Mapper.CreateMap<ITexture, TextureBundleDto>();
            Mapper.CreateMap<IShader, ShaderBundleDto>();

            Mapper.CreateMap<IShader, ShaderStatBundleDto>()
                .ForMember(dto => dto.Textures, opt => opt.Ignore())
                .ForMember(dto => dto.Shader, opt => opt.MapFrom(src => Mapper.Map<IShader, ShaderBundleDto>(src)))
                .ForMember(dto => dto.TextureStats, opt => opt.MapFrom(src => Mapper.Map<IEnumerable<ITexture>, List<TextureStatBundleDto>>(src.Textures)));

            Mapper.CreateMap<ITexture, TextureStatBundleDto>()
                .ForMember(dto => dto.Texture, opt => opt.MapFrom(src => Mapper.Map<ITexture, TextureBundleDto>(src)));

            Mapper.CreateMap<RSG.Platform.Platform, PlatformDto>()
                .ForMember(dto => dto.Value, opt => opt.MapFrom(src => src.ToString()));

            Mapper.CreateMap<GameType, GameTypeDto>()
                .ForMember(dto => dto.Value, opt => opt.MapFrom(src => src.ToString()));
        }
        #endregion // Constructor(s)

        #region Asset Syncing
        /// <summary>
        /// Syncs all archetypes.
        /// </summary>
        /// <param name="build"></param>
        public void SyncArchetypes()
        {
            Log.Profile("Syncing Archetypes");

            try
            {
                using (ArchetypeClient client = new ArchetypeClient(m_server))
                {
                    foreach (IMapHierarchy mapHierarchy in MapHierarchies.Values.Where(item => item.OwningLevel.Name != "None"))
                    {
                        Log.Message("Processing level {0}", mapHierarchy.OwningLevel.Name);
                        PreloadPropAndInteriorArchetypes(mapHierarchy);

                        foreach (IMapSection section in mapHierarchy.AllSections)
                        {
                            Log.Debug("Updating archetypes for section {0}.", section.Name);
                            section.RequestAllStatistics(false);

                            IEnumerable<IDrawableArchetype> drawables = section.Archetypes.OfType<IDrawableArchetype>();
                            if (drawables.Any())
                            {
                                ArchetypeDtos dtos = new ArchetypeDtos();
                                // Neither of the following map correctly...stupid automapper...need to do this manually instead.
                                //dtos.Items = Mapper.Map<IEnumerable<IDrawableArchetype>, List<ArchetypeDto>>(drawables);
                                //dtos.Items = Mapper.Map<IEnumerable<IArchetype>, List<ArchetypeDto>>(drawables);
                                foreach (IDrawableArchetype archetype in drawables)
                                {
                                    dtos.Items.Add(Mapper.Map<IDrawableArchetype, DrawableArchetypeDto>(archetype));
                                }
                                client.PutDrawables(dtos);
                            }

                            IEnumerable<IFragmentArchetype> fragments = section.Archetypes.OfType<IFragmentArchetype>();
                            if (fragments.Any())
                            {
                                ArchetypeDtos dtos = new ArchetypeDtos();
                                foreach (IFragmentArchetype archetype in fragments)
                                {
                                    dtos.Items.Add(Mapper.Map<IFragmentArchetype, FragmentArchetypeDto>(archetype));
                                }
                                client.PutFragments(dtos);
                            }

                            IEnumerable<IInteriorArchetype> interiors = section.Archetypes.OfType<IInteriorArchetype>();
                            if (interiors.Any())
                            {
                                ArchetypeDtos dtos = new ArchetypeDtos();
                                foreach (IInteriorArchetype archetype in interiors)
                                {
                                    dtos.Items.Add(Mapper.Map<IInteriorArchetype, InteriorArchetypeDto>(archetype));
                                }
                                client.PutInteriors(dtos);
                            }

                            IEnumerable<IStatedAnimArchetype> statedAnims = section.Archetypes.OfType<IStatedAnimArchetype>();
                            if (statedAnims.Any())
                            {
                                ArchetypeDtos dtos = new ArchetypeDtos();
                                foreach (IStatedAnimArchetype archetype in statedAnims)
                                {
                                    dtos.Items.Add(Mapper.Map<IStatedAnimArchetype, StatedAnimArchetypeDto>(archetype));
                                }
                                client.PutStatedAnims(dtos);
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                Log.ToolException(ex, "Unhandled exception while syncing archetypes.");
            }

            Log.ProfileEnd();
        }

        /// <summary>
        /// Syncs all strings related to capture data.  Strings are retrieved from the
        /// </summary>
        public void SyncCaptureRelatedStrings()
        {
            Log.Profile("Syncing Capture Related Strings");

            string filepath = Branch.Environment.Subst(c_gameStringsRelativePath);
            Debug.Assert(File.Exists(filepath), String.Format("The capture related string file located at '{0}' doesn't exist.  Have you forgotten to sync it?", filepath));

            // Load up the file
            XDocument doc = XDocument.Load(filepath);

            try
            {
                using (CaptureClient client = new CaptureClient(m_server))
                {
                    // Sync the zones.
                    Log.Message("Syncing Capture Zones");

                    // The majority of the zones come from the debug location list file.
                    string zonesFilepath = Branch.Environment.Subst(c_debugLocationListPath);
                    if (File.Exists(zonesFilepath))
                    {
                        XDocument zonesDoc = XDocument.Load(zonesFilepath);

                        foreach (XElement elem in zonesDoc.XPathSelectElements("//Item/name"))
                        {
                            client.CreateZone(elem.Value);
                        }
                    }

                    // Add any additional zones that we are aware of from the config file.
                    foreach (XElement elem in doc.XPathSelectElements("//zones/zone"))
                    {
                        client.CreateZone(elem.Value);
                    }

                    // Create a zone for each of the cutscenes.
                    foreach (ICutscene cutscene in Cutscenes)
                    {
                        client.CreateZone(String.Format("zone_{0}", cutscene.Name));
                    }

                    // Cpu sets
                    Log.Message("Syncing Capture Cpu Sets");
                    foreach (XElement elem in doc.XPathSelectElements("//cpu_sets/cpu_set"))
                    {
                        client.CreateCpuSet(elem.Value);
                    }

                    // Cpu metrics
                    Log.Message("Syncing Capture Cpu Metrics");
                    foreach (XElement elem in doc.XPathSelectElements("//cpu_metrics/cpu_metric"))
                    {
                        client.CreateCpuMetric(elem.Value);
                    }

                    // Thread names
                    Log.Message("Syncing Capture Thread Names");
                    foreach (XElement elem in doc.XPathSelectElements("//thread_names/thread_name"))
                    {
                        client.CreateThreadName(elem.Value);
                    }

                    // Memory buckets
                    Log.Message("Syncing Capture Memory Buckets");
                    foreach (XElement elem in doc.XPathSelectElements("//memory_buckets/memory_bucket"))
                    {
                        client.CreateMemoryBucket(elem.Value);
                    }

                    // Streaming memory modules
                    Log.Message("Syncing Capture Streaming Memory Modules");
                    foreach (XElement elem in doc.XPathSelectElements("//streaming_memory_modules/streaming_memory_module"))
                    {
                        client.CreateStreamingMemoryModule(elem.Value);
                    }

                    // Streaming memory categories
                    Log.Message("Syncing Capture Streaming Memory Categories");
                    foreach (XElement elem in doc.XPathSelectElements("//streaming_memory_categories/streaming_memory_category"))
                    {
                        client.CreateStreamingMemoryCategory(elem.Value);
                    }
                }
            }
            catch (System.Exception ex)
            {
                Log.ToolException(ex, "Unhandled exception while syncing capture names.");
            }

            Log.ProfileEnd();
        }

        /// <summary>
        /// Loads the list of cash packs from file and sends them across to the server.
        /// </summary>
        public void SyncCashPacks()
        {
            const String c_cashPacksFile = @"$(toolsconfig)\config\statistics\cash_packs.xml";
            String filename = Branch.Environment.Subst(c_cashPacksFile);

            Log.Profile("Syncing Cash Packs");

            try
            {
                IList<CashPack> cashPacks = LoadCashPacksFromFile(filename);
                using (GameAssetClient client = new GameAssetClient(m_server))
                {
                    client.UpdateCashPacks(cashPacks);
                }
            }
            catch (System.Exception ex)
            {
                Log.ToolException(ex, "Unhandled exception while syncing cash packs.");
            }

            Log.ProfileEnd();
        }

        /// <summary>
        /// Syncs all the characters from the exported scene xml.
        /// </summary>
        public void SyncCharacters()
        {
            Log.Profile("Syncing Characters");

            ICharacterCollection collection = CharacterFactory.CreateCollection(DataSource.SceneXml, Branch);
            SyncCharacters(collection);

            foreach (IProject dlcProject in Branch.Project.Config.DLCProjects.Values)
            {
                IBranch dlcBranch = dlcProject.DefaultBranch;
                foreach (IBranch branch in dlcProject.Branches.Values)
                {
                    if (ContentTree.Branch.Name == branch.Name)
                    {
                        dlcBranch = branch;
                    }
                }

                // Create a content tree for each DLC
                collection = CharacterFactory.CreateCollection(DataSource.SceneXml, dlcProject.DefaultBranch);
                SyncCharacters(collection);
            }

            Log.ProfileEnd();
        }

        /// <summary>
        /// Helper for syncing characters from a particular collection
        /// </summary>
        private void SyncCharacters(ICharacterCollection collection)
        {
            try
            {
                using (CharacterClient client = new CharacterClient(m_server))
                {
                    foreach (ICharacter character in collection.AllCharacters)
                    {
                        Log.Debug("Updating {0}", character.Name);

                        // Update the character object
                        CharacterDto dto = Mapper.Map<ICharacter, CharacterDto>(character);
                        client.PutAsset(dto.Identifier, dto);
                    }
                }
            }
            catch (System.Exception ex)
            {
                Log.ToolException(ex, "Unhandled exception while syncing vehicles.");
            }
        }

        /// <summary>
        /// Syncs all the animations from the exported data
        /// </summary>
        public void SyncClipDictionaries()
        {
            Log.Profile("Syncing Clip Dictionaries");

            try
            {
                using (GameAssetClient client = new GameAssetClient(m_server))
                {
                    client.UpdateClipDictionaries(ClipDictionaries);
                }
            }
            catch (System.Exception ex)
            {
                Log.ToolException(ex, "Unhandled exception while syncing clip dictionaries.");
            }

            Log.ProfileEnd();
        }

        /// <summary>
        /// Syncs all the characters from the exported data
        /// </summary>
        public void SyncCutscenes()
        {
            Log.Profile("Syncing Cutscenes");

            try
            {
                foreach (ICutscene cutscene in Cutscenes)
                {
                    cutscene.LoadStats(new StreamableStat[] { StreamableCutsceneStat.Duration });
                }

                using (GameAssetClient client = new GameAssetClient(m_server))
                {
                    client.UpdateCutscenes(Cutscenes);
                }
            }
            catch (System.Exception ex)
            {
                Log.ToolException(ex, "Unhandled exception while syncing cutscenes.");
            }

            Log.ProfileEnd();
        }

        /// <summary>
        /// Generate the list of draw lists manually.  The draw list names are based off of those in:
        /// //depot/gta5/src/dev/game/renderer/DrawLists/drawListMgr.cpp
        /// </summary>
        public void SyncDrawLists()
        {
            const String c_drawListsFile = @"$(toolsconfig)\config\statistics\drawlists.xml";
            String filename = Branch.Environment.Subst(c_drawListsFile);

            Log.Profile("Syncing Drawlists");

            try
            {
                IDictionary<uint, String> drawLists = LoadAssetsFromFile(filename, "/drawlists/drawlists", "@name", "@name");
                using (GameAssetClient client = new GameAssetClient(m_server))
                {
                    client.UpdateDrawLists(drawLists.Select(item => new GameAsset(item.Value)).ToList());
                }
            }
            catch (System.Exception ex)
            {
                Log.ToolException(ex, "Unhandled exception while syncing drawlists.");
            }

            Log.ProfileEnd();
        }
        
        /// <summary>
        /// Syncs all entities
        /// </summary>
        public void SyncEntities()
        {
            Log.Profile("Syncing Entities");
            
            try
            {
                using (EntityClient client = new EntityClient(m_server))
                {
                    foreach (IMapHierarchy mapHierarchy in MapHierarchies.Values.Where(item => item.OwningLevel.Name != "None"))
                    {
                        Log.Message("Processing level {0}", mapHierarchy.OwningLevel.Name);
                        PreloadPropAndInteriorArchetypes(mapHierarchy);

                        foreach (IMapSection section in mapHierarchy.AllSections)
                        {
                            Log.Debug("Updating entities for section {0}.", section.Name);
                            section.RequestAllStatistics(false);

                            EntityDtos dtos = new EntityDtos();

                            if (section.ChildEntities.Any())
                            {
                                dtos.Items.AddRange(Mapper.Map<IEnumerable<IEntity>, IEnumerable<EntityDto>>(section.ChildEntities));
                            }

                            IEnumerable<IInteriorArchetype> interiors = section.Archetypes.OfType<InteriorArchetype>();
                            if (interiors.Any())
                            {
                                foreach (IInteriorArchetype interior in interiors)
                                {
                                    if (interior.ChildEntities.Any())
                                    {
                                        dtos.Items.AddRange(Mapper.Map<IEnumerable<IEntity>, IEnumerable<EntityDto>>(interior.ChildEntities));
                                    }

                                    foreach (IRoom room in interior.Rooms.Where(item => item.ChildEntities.Any()))
                                    {
                                        dtos.Items.AddRange(Mapper.Map<IEnumerable<IEntity>, IEnumerable<EntityDto>>(room.ChildEntities));
                                    }
                                }
                            }

                            if (dtos.Items.Any())
                            {
                                client.PutAssets(dtos);
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                Log.ToolException(ex, "Unhandled exception while syncing entities.");
            }

            Log.ProfileEnd();
        }

        /// <summary>
        /// Syncs all the levels from the content tree.
        /// </summary>
        public void SyncLevels()
        {
            Log.Profile("Syncing Levels");

            // Construct the full path to the file
            string filepath = Branch.Environment.Subst(c_levelsRelativePath);

            if (File.Exists(filepath))
            {
                XDocument doc = XDocument.Load(filepath);

                try
                {
                    using (LevelClient client = new LevelClient(m_server))
                    {
                        foreach (ILevel level in Levels.Where(item => item.Name != "None"))
                        {
                            Log.Debug("Updating {0}", level.Name);

                            LevelDto dto = Mapper.Map<ILevel, LevelDto>(level);

                            // Determine the level's index
                            // Level indices from the game start at 1...retarded I know...
                            uint idx = 1;
                            foreach (XElement elem in doc.XPathSelectElements("/levels/level"))
                            {
                                XAttribute att = elem.Attribute("friendlyName");
                                if (att != null && att.Value == level.Name)
                                {
                                    dto.LevelIdx = idx;
                                    break;
                                }
                                ++idx;
                            }

                            client.PutAsset(dto.Identifier, dto);
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    Log.ToolException(ex, "Unhandled exception while syncing levels.");
                }
            }

            Log.ProfileEnd();
        }

        /// <summary>
        /// Syncs the map area master list.
        /// </summary>
        public void SyncMapAreas()
        {
            Log.Profile("Syncing Map Areas");

            foreach (IMapHierarchy mapHierarchy in MapHierarchies.Values)
            {
                Log.Message("Processing level {0}", mapHierarchy.OwningLevel.Name);

                try
                {
                    using (MapAreaClient client = new MapAreaClient(m_server))
                    {
                        foreach (IMapArea area in mapHierarchy.AllAreas)
                        {
                            Log.Debug("Updating area {0}", area.Name);

                            MapAreaDto dto = Mapper.Map<IMapArea, MapAreaDto>(area);
                            client.PutAsset(dto.Identifier, dto);
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    Log.ToolException(ex, "Unhandled exception while syncing map areas.");
                }
            }

            Log.ProfileEnd();
        }

        /// <summary>
        /// Syncs the map area/section master list.
        /// </summary>
        public void SyncMapSections()
        {
            Log.Profile("Syncing Map Sections");

            foreach (IMapHierarchy mapHierarchy in MapHierarchies.Values)
            {
                Log.Message("Processing level {0}", mapHierarchy.OwningLevel.Name);

                try
                {
                    using (MapSectionClient client = new MapSectionClient(m_server))
                    {
                        foreach (IMapSection section in mapHierarchy.AllSections)
                        {
                            Log.Debug("Updating section {0}", section.Name);

                            MapSectionDto dto = Mapper.Map<IMapSection, MapSectionDto>(section);
                            client.PutAsset(dto.Identifier, dto);
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    Log.ToolException(ex, "Unhandled exception while syncing map sections.");
                }
            }

            Log.ProfileEnd();
        }

        /// <summary>
        /// Syncs all memory store and memory pool name/hashes
        /// </summary>
        public void SyncMemoryNames()
        {
            Log.Profile("Syncing MemoryNames");

            string fileNameMemoryStores = Path.Combine(Branch.Assets, "reports", "telemetry", "memory_stores.csv");
            string fileNameMemoryPools = Path.Combine(Branch.Assets, "reports", "telemetry", "memory_pools.csv");

            Dictionary<uint, string> memoryStores = new Dictionary<uint, string>();
            Dictionary<uint, string> memoryPools = new Dictionary<uint, string>();

            UpdateMemoryHash(fileNameMemoryStores, memoryStores);
            UpdateMemoryHash(fileNameMemoryPools, memoryPools);

            // These shouldn't be changing, so hard code them for now.
            String[] memoryHeapNames =
                new String[]
                    {
                        "v_game",
                        "v_frag",
                        "v_resource",
                        "v_streaming",
                        "v_resource_slush",
                        "v_smallocator",
                        "p_game",
                        "p_Resource",
                        "p_streaming",
                        "p_resource_slush",
                        "audVehicleAudioEntity",
                        "CObject",
                        "CTask",
                        "CTaskInfo",
                        "CVehicle",
                        "perm arch",
                        "streamed arch"
                    };

            IDictionary<uint, String> memoryHeaps = new Dictionary<uint, String>();
            foreach (String name in memoryHeapNames)
            {
                uint hash = OneAtATime.ComputeHash(name);
                memoryHeaps[hash] = name;
            }

            try
            {
                using (GameAssetClient client = new GameAssetClient(m_server))
                {
                    client.UpdateMemoryPools(memoryPools);
                    client.UpdateMemoryStores(memoryStores);
                    client.UpdateMemoryHeaps(memoryHeaps);
                }
            }
            catch (System.Exception ex)
            {
                Log.ToolException(ex, "Unhandled exception while syncing memory names.");
            }

            Log.ProfileEnd();
        }

        /// <summary>
        /// populate the dictionary of hash / name pairs from a CSV file
        /// </summary>
        /// <param name="filename">the CSV file</param>
        /// <param name="memoryHash">the dictionary to populate</param>
        private void UpdateMemoryHash(string filename, Dictionary<uint, string> memoryHash)
        {
            if (!File.Exists(filename))
            {
                Log.Error("{0} does not exist, memory name table in DB could be stale, telemtry might be lost.", filename);
                return;
            }

            try
            {
                string[] lines = File.ReadAllLines(filename);
                foreach (string line in lines.Skip(1))
                {
                    string[] vals = line.Split(',');
                    if (vals.Count() >= 2)
                    {
                        string name = vals[0].Trim();
                        string hash = vals[1].Trim();
                        uint hashValueDecimal;

                        if (uint.TryParse(hash, out hashValueDecimal) ||
                            (hash.StartsWith("0x") &&
                                uint.TryParse(hash.Substring(2), NumberStyles.AllowHexSpecifier | NumberStyles.HexNumber, CultureInfo.InvariantCulture, out hashValueDecimal))
                            )
                        {
                            Log.Debug("Updating Name Hash table {0} : {1}", name, hashValueDecimal.ToString());
                            memoryHash[hashValueDecimal] = name;

                            // Issue a warning if the hashing isn't what we expect
                            uint expectedHash = RSG.ManagedRage.StringHashUtil.atStringHash(name, 0);
                            if (expectedHash != hashValueDecimal)
                            {
                                Log.Warning("Hashing algorithm is not what is expected (file={0}) (atStringHash={1})", hashValueDecimal, expectedHash);
                            }
                        }
                        else
                        {
                            Log.Error("{0} contains unrecognisable hash {1}", filename, hash);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error during memory hash file handling", ex);
            }
        }

        /// <summary>
        /// Syncs list of mini-gmae variants from a local config file.
        /// </summary>
        public void SyncMiniGameVariants()
        {
            const String c_tvShowsFile = @"$(toolsconfig)\config\statistics\minigames.xml";
            String filename = Branch.Environment.Subst(c_tvShowsFile);

            Log.Profile("Syncing Mini-Game Variants");

            try
            {
                IList<MiniGameVariant> miniGames = LoadMiniGameVariantsFromFile(filename);
                using (GameAssetClient client = new GameAssetClient(m_server))
                {
                    client.UpdateMiniGameVariants(miniGames);
                }
            }
            catch (System.Exception ex)
            {
                Log.ToolException(ex, "Unhandled exception while syncing mini-game variants.");
            }

            Log.ProfileEnd();
        }

        /// <summary>
        /// Syncs all the missions from bugstar.
        /// </summary>
        public void SyncMissions()
        {
            Log.Profile("Syncing Missions");

            try
            {
                using (GameAssetClient client = new GameAssetClient(m_server))
                {
                    client.UpdateMissions(Missions);
                }
            }
            catch (System.Exception ex)
            {
                Log.ToolException(ex, "Unhandled exception while syncing missions.");
            }

            Log.ProfileEnd();
        }

        /// <summary>
        /// Sync the list of movies from an xml file.
        /// </summary>
        public void SyncMovies()
        {
            const String c_moviesFile = @"$(toolsconfig)\config\statistics\movies.xml";
            String filename = Branch.Environment.Subst(c_moviesFile);

            Log.Profile("Syncing movies");

            try
            {
                IList<FriendlyGameAsset> movies = LoadAssetsWithFriendlyNameFromFile(filename, "/movies/movie", "@asset_name", "@friendly_name");
                using (GameAssetClient client = new GameAssetClient(m_server))
                {
                    client.UpdateMovies(movies);
                }
            }
            catch (System.Exception ex)
            {
                Log.ToolException(ex, "Unhandled exception while syncing movies.");
            }

            Log.ProfileEnd();
        }

        /// <summary>
        /// Sync the list of properties from the appropriate gametext file.
        /// </summary>
        public void SyncProperties()
        {
            const String c_propertiesFile = @"$(text)\American\americanNetFreemode.txt";
            const String c_propertyRegexString = @"^\[MP_PROP_\d+\]$";

            Log.Profile("Syncing Properties");

            String filename = Branch.Environment.Subst(c_propertiesFile);
            IDictionary<uint, String> propertyLookup = new Dictionary<uint, String>();

            try
            {
                if (File.Exists(filename))
                {
                    using (StreamReader reader = new StreamReader(filename))
                    {
                        uint itemHash = 0;

                        String line;
                        while ((line = reader.ReadLine()) != null)
                        {
                            if (Regex.IsMatch(line, c_propertyRegexString))
                            {
                                // Trim off the square brackets.
                                String locName = line.Trim().Trim(new char[] { '[', ']' });
                                itemHash = OneAtATime.ComputeHash(locName);
                            }
                            else if (itemHash != 0)
                            {
                                propertyLookup[itemHash] = line;
                                itemHash = 0;
                            }
                        }
                    }
                }
                else
                {
                    Debug.Assert(false, String.Format("{0} item hash file doesn't exist.", filename));
                    Log.Error("{0} item hash file doesn't exist.", filename);
                }
            }
            catch (System.Exception ex)
            {
                Log.ToolException(ex, "Error parsing the {0} file to extract the item hashes.", filename);
            }

            try
            {
                using (GameAssetClient client = new GameAssetClient(m_server))
                {
                    client.UpdateProperties(propertyLookup);
                }
            }
            catch (System.Exception ex)
            {
                Log.ToolException(ex, "Unhandled exception while syncing memory names.");
            }

            Log.ProfileEnd();
        }

        /// <summary>
        /// Generate the list of radio stations manually as these are very infrequently updated.  Based off of the names found in:
        /// //depot/gta5/audio/dev/assets/Objects/Core/Audio/GameObjects/Radio/Stations.xml
        /// </summary>
        public void SyncRadioStations()
        {
            Log.Profile("Syncing Radio Stations");

            try
            {
                // Create the list of radio stations.
                IList<FriendlyGameAsset> radioStations = new List<FriendlyGameAsset>();
                foreach (IRadioStation radioStation in RadioStations)
                {
                    radioStations.Add(new FriendlyGameAsset(radioStation.Name, radioStation.FriendlyName));
                }

                // Update the radio stations on the server.
                using (GameAssetClient client = new GameAssetClient(m_server))
                {
                    client.UpdateRadioStations(radioStations);
                }
            }
            catch (System.Exception ex)
            {
                Log.ToolException(ex, "Unhandled exception while syncing radio stations.");
            }

            Log.ProfileEnd();
        }

        /// <summary>
        /// Syncs the master list of ranks from an XML file.
        /// </summary>
        public void SyncRanks()
        {
            Log.Profile("Syncing Ranks");

            const String c_unlocksFile = @"$(toolsconfig)\config\statistics\ranks.xml";
            String filename = Branch.Environment.Subst(c_unlocksFile);
            Debug.Assert(File.Exists(filename), "File doesn't exist");

            if (File.Exists(filename))
            {
                IList<Rank> ranks = new List<Rank>();

                XDocument doc = XDocument.Load(filename);
                foreach (XElement elem in doc.XPathSelectElements("/ranks/rank"))
                {
                    XAttribute levelAtt = elem.Attribute("level");
                    XAttribute xpAtt = elem.Attribute("xp");
                    XAttribute nameAtt = elem.Attribute("name");
                    Debug.Assert(levelAtt != null && xpAtt != null, "Invalid rank encountered in the unlocks.xml file.");

                    if (levelAtt != null && xpAtt != null)
                    {
                        Rank rank = new Rank
                        {
                            Level = UInt32.Parse(levelAtt.Value),
                            Experience = UInt32.Parse(xpAtt.Value),
                            Name = (nameAtt != null ? nameAtt.Value : null)
                        };
                        ranks.Add(rank);
                    }
                }

                try
                {
                    using (GameAssetClient client = new GameAssetClient(m_server))
                    {
                        client.UpdateRanks(ranks);
                    }
                }
                catch (System.Exception ex)
                {
                    Log.ToolException(ex, "Unhandled exception while syncing unlocks.");
                }
            }

            Log.ProfileEnd();
        }

        /// <summary>
        /// Syncs all rooms.
        /// </summary>
        public void SyncRooms()
        {
            Log.Profile("Syncing Rooms");

            try
            {
                using (RoomClient client = new RoomClient(m_server))
                {
                    foreach (IMapHierarchy mapHierarchy in MapHierarchies.Values.Where(item => item.OwningLevel.Name != "None"))
                    {
                        Log.Message("Processing level {0}", mapHierarchy.OwningLevel.Name);
                        PreloadPropAndInteriorArchetypes(mapHierarchy);

                        foreach (IMapSection section in mapHierarchy.AllSections)
                        {
                            Log.Debug("Updating rooms for section {0}.", section.Name);
                            section.RequestAllStatistics(false);

                            foreach (IInteriorArchetype archetype in section.Archetypes.OfType<IInteriorArchetype>())
                            {
                                foreach (IRoom room in archetype.Rooms)
                                {
                                    RoomDto dto = Mapper.Map<IRoom, RoomDto>(room);
                                    client.PutAsset(room.Hash.ToString(), dto);
                                }
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                Log.ToolException(ex, "Unhandled exception while syncing rooms.");
            }

            Log.ProfileEnd();
        }

        /// <summary>
        /// Synchronises the names of shops.
        /// </summary>
        public void SyncShops()
        {
            Log.Profile("Syncing Shops");

            const String c_shopFile = @"$(toolsconfig)\config\statistics\shops.xml";
            String filename = Branch.Environment.Subst(c_shopFile);
            Debug.Assert(File.Exists(filename), "File doesn't exist");

            if (File.Exists(filename))
            {
                IList<Shop> shops = new List<Shop>();
                
                XDocument doc = XDocument.Load(filename);
                foreach (XElement elem in doc.XPathSelectElements("/shops/shop"))
                {
                    XAttribute nameAtt = elem.Attribute("name");
                    XAttribute typeAtt = elem.Attribute("type");
                    XAttribute friendlyNameAtt = elem.Attribute("friendly_name");
                    Debug.Assert(nameAtt != null && typeAtt != null && friendlyNameAtt != null, "Invalid shop name encountered in the shops.xml file.");

                    if (nameAtt != null && typeAtt != null && friendlyNameAtt != null)
                    {
                        Shop shop = new Shop(nameAtt.Value, friendlyNameAtt.Value, (ShopType)Enum.Parse(typeof(ShopType), typeAtt.Value));
                        shops.Add(shop);
                    }
                }

                try
                {
                    using (GameAssetClient client = new GameAssetClient(m_server))
                    {
                        client.UpdateShops(shops);
                    }
                }
                catch (System.Exception ex)
                {
                    Log.ToolException(ex, "Unhandled exception while syncing shops.");
                }
            }

            Log.ProfileEnd();
        }

        /// <summary>
        /// Syncs the shop items.
        /// </summary>
        public void SyncShopItems()
        {
            Log.Profile("Syncing Shop Items");

            using (GameAssetClient client = new GameAssetClient(m_server))
            {
                Log.Debug("Syncing main shop items");
                IList<ShopItem> shopItems = GetMainShopItems(Branch);
                if (shopItems.Any())
                {
                    client.UpdateShopItems(shopItems);
                }

                foreach (IProject dlcProject in Branch.Project.Config.DLCProjects.Values)
                {
                    Log.Debug("Syncing shop items for DLC: {0}", dlcProject.Name);
                    shopItems = GetDLCShopItems(dlcProject.DefaultBranch);
                    client.UpdateShopItems(shopItems);
                }
            }

            Log.ProfileEnd();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branch"></param>
        /// <returns></returns>
        private IList<ShopItem> GetMainShopItems(IBranch branch)
        {
            IList<ShopItem> shopItems = new List<ShopItem>();
            shopItems.Add(new ShopItem("soda", "Soda", null, null));        // Special case for soda bought from the Sprunk Machines.

            // Get the list of items that have category/sub-category information.
            IDictionary<String, Tuple<String, String>> categoryInfo = GetShopItemsCategoryInformation();

            // Create the list of files to parse for shop item information.
            IList<String> filesToParse = new List<String>();
            filesToParse.Add(branch.Environment.Subst(c_shopsFile));
            filesToParse.Add(branch.Environment.Subst(c_vehiclesFile));
            filesToParse.Add(branch.Environment.Subst(c_weaponsFile));
            filesToParse.Add(branch.Environment.Subst(c_menuFile));

            // Parse the files getting the items we are interested in.
            foreach (String filename in filesToParse)
            {
                IDictionary<String, String> itemLookup = new Dictionary<String, String>();

                try
                {
                    Debug.Assert(File.Exists(filename), "File doesn't exist");
                    if (File.Exists(filename))
                    {
                        using (StreamReader reader = new StreamReader(filename))
                        {
                            String label = null;

                            String line;
                            while ((line = reader.ReadLine()) != null)
                            {
                                if (line.StartsWith("["))
                                {
                                    // Trim off the square brackets and take the bit before the colon.
                                    String locName = line.Trim().Trim(new char[] { '[', ']' });
                                    if (locName.Contains(":"))
                                    {
                                        locName = locName.Split(new char[] { ':' })[0];
                                    }
                                    label = locName;
                                }
                                else if (label != null)
                                {
                                    if (line.Length < 255)
                                    {
                                        itemLookup[label] = line;
                                    }
                                    label = null;
                                }
                            }
                        }
                    }
                    else
                    {
                        Log.Error("{0} item hash file doesn't exist.", filename);
                    }
                }
                catch (System.Exception ex)
                {
                    Log.ToolException(ex, "Error parsing the {0} file to extract the item hashes.", filename);
                }

                // Add the informations to the list of shop items.
                foreach (KeyValuePair<String, String> pair in itemLookup)
                {
                    String categoryName = null;
                    String subCategoryName = null;
                    if (categoryInfo.ContainsKey(pair.Key))
                    {
                        categoryName = categoryInfo[pair.Key].Item1;
                        subCategoryName = categoryInfo[pair.Key].Item2;
                    }
                    shopItems.Add(new ShopItem(pair.Key, pair.Value, categoryName, subCategoryName));
                }
            }

            return shopItems;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branch"></param>
        /// <returns></returns>
        private IList<ShopItem> GetDLCShopItems(IBranch branch)
        {
            IList<ShopItem> shopItems = new List<ShopItem>();

            GameTextCollection gameText = new GameTextCollection(branch);
            gameText.LoadLanguage(Language.American);

            //"TATTOO_SHOP_DLC_FILE"
            foreach (String tattooFile in DLCContentUtil.GetFilenames(branch, "TATTOO_SHOP_DLC_FILE"))
            {
                shopItems.AddRange(GetShopItemsFromDLCFile(tattooFile, gameText));
            }

            //"VEHICLE_SHOP_DLC_FILE"
            foreach (String vehicleFile in DLCContentUtil.GetFilenames(branch, "VEHICLE_SHOP_DLC_FILE"))
            {
                shopItems.AddRange(GetShopItemsFromDLCFile(vehicleFile, gameText));
            }

            //"SHOP_PED_APPAREL_META_FILE"
            foreach (String pedFile in DLCContentUtil.GetFilenames(branch, "SHOP_PED_APPAREL_META_FILE"))
            {
                shopItems.AddRange(GetShopItemsFromDLCFile(pedFile, gameText));
            }

            //"WEAPON_SHOP_INFO_METADATA_FILE"
            foreach (String weaponFile in DLCContentUtil.GetFilenames(branch, "WEAPON_SHOP_INFO_METADATA_FILE"))
            {
                shopItems.AddRange(GetShopItemsFromDLCFile(weaponFile, gameText));
            }

            return shopItems;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        private IEnumerable<ShopItem> GetShopItemsFromDLCFile(String filepath, GameTextCollection gameText)
        {
            if (File.Exists(filepath))
            {
                XDocument doc = XDocument.Load(filepath);
                foreach (XElement elem in doc.XPathSelectElements("//Item/textLabel"))
                {
                    String label = elem.Value;
                    String text;
                    if (gameText.TryGetText(label, Language.American, RSG.Platform.Platform.PS3, out text))
                    {
                        yield return new ShopItem(label, text, null, null);
                    }
                }
            }
        }

        /// <summary>
        /// Returns a tuple of item name, category name, sub category name.
        /// </summary>
        private IDictionary<String, Tuple<String, String>> GetShopItemsCategoryInformation()
        {
            const String c_shopItemsFile = @"$(toolsconfig)\config\statistics\shopitems.xml";
            String filename = Branch.Environment.Subst(c_shopItemsFile);
            Debug.Assert(File.Exists(filename), "File doesn't exist");

            IDictionary<String, Tuple<String, String>> items = new Dictionary<String, Tuple<String, String>>();
            if (File.Exists(filename))
            {
                XDocument doc = XDocument.Load(filename);
                foreach (XElement categoryElem in doc.XPathSelectElements("/shopitems/category"))
                {
                    XAttribute categoryNameAtt = categoryElem.Attribute("name");
                    if (categoryNameAtt != null)
                    {
                        foreach (XElement subCategoryElem in categoryElem.Elements("subcategory"))
                        {
                            XAttribute subCategoryNameAtt = subCategoryElem.Attribute("name");
                            if (subCategoryNameAtt != null)
                            {
                                foreach (XElement itemElem in subCategoryElem.Elements("item"))
                                {
                                    XAttribute itemNameAtt = itemElem.Attribute("name");
                                    if (itemNameAtt != null)
                                    {
                                        items[itemNameAtt.Value] = Tuple.Create(categoryNameAtt.Value, subCategoryNameAtt.Value);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return items;
        }

        /// <summary>
        /// Sync the list of TV shows from an xml file.
        /// </summary>
        public void SyncTVShows()
        {
            const String c_tvShowsFile = @"$(toolsconfig)\config\statistics\tv_shows.xml";
            String filename = Branch.Environment.Subst(c_tvShowsFile);

            Log.Profile("Syncing TV Show");

            try
            {
                IList<FriendlyGameAsset> tvShows = LoadAssetsWithFriendlyNameFromFile(filename, "/tv_shows/tv_show", "@asset_name", "@friendly_name");
                using (GameAssetClient client = new GameAssetClient(m_server))
                {
                    client.UpdateTVShows(tvShows);
                }
            }
            catch (System.Exception ex)
            {
                Log.ToolException(ex, "Unhandled exception while syncing tv shows.");
            }

            Log.ProfileEnd();
        }

        /// <summary>
        /// Synchronises the master list of unlocks.
        /// </summary>
        public void SyncUnlocks()
        {
            Log.Profile("Syncing Unlocks");

            const String c_unlocksFile = @"$(toolsconfig)\config\statistics\unlocks.xml";
            String filename = Branch.Environment.Subst(c_unlocksFile);
            Debug.Assert(File.Exists(filename), "File doesn't exist");

            if (File.Exists(filename))
            {
                IList<Unlock> unlocks = new List<Unlock>();

                XDocument doc = XDocument.Load(filename);
                foreach (XElement elem in doc.XPathSelectElements("/unlocks/unlock"))
                {
                    XAttribute nameAtt = elem.Attribute("name");
                    XAttribute rankAtt = elem.Attribute("rank");
                    XAttribute typeAtt = elem.Attribute("type");
                    Debug.Assert(nameAtt != null && rankAtt != null && typeAtt != null, "Invalid unlock encountered in the unlocks.xml file.");

                    if (nameAtt != null && rankAtt != null && typeAtt != null)
                    {
                        Unlock unlock = new Unlock
                        {
                            Name = nameAtt.Value,
                            Rank = UInt32.Parse(rankAtt.Value),
                            UnlockType = (UnlockType)Enum.Parse(typeof(UnlockType), typeAtt.Value)
                        };
                        unlocks.Add(unlock);
                    }
                }

                try
                {
                    using (GameAssetClient client = new GameAssetClient(m_server))
                    {
                        client.UpdateUnlocks(unlocks);
                    }
                }
                catch (System.Exception ex)
                {
                    Log.ToolException(ex, "Unhandled exception while syncing unlocks.");
                }
            }

            Log.ProfileEnd();
        }

        /// <summary>
        /// Syncs all the vehicles from the exported scene xml.
        /// </summary>
        public void SyncVehicles()
        {
            Log.Profile("Syncing Vehicles");

            IVehicleCollection collection = VehicleFactory.CreateCollection(DataSource.SceneXml, Branch);
            SyncVehicles(collection);

            foreach (IProject dlcProject in Branch.Project.Config.DLCProjects.Values)
            {
                collection = VehicleFactory.CreateCollection(DataSource.SceneXml, dlcProject.DefaultBranch);
                SyncVehicles(collection);
            }

            Log.ProfileEnd();
        }

        /// <summary>
        /// Helper for syncing vehicles from a particular collection
        /// </summary>
        private void SyncVehicles(IVehicleCollection collection)
        {
            try
            {
                using (VehicleClient client = new VehicleClient(m_server))
                {
                    foreach (IVehicle vehicle in collection)
                    {
                        Log.Debug("Updating {0}", vehicle.Name);
                        vehicle.LoadAllStats();

                        VehicleDto dto = new VehicleDto();
                        dto.Name = vehicle.Name;
                        dto.Identifier = vehicle.Hash.ToString();
                        dto.GameName = vehicle.Name;
                        dto.FriendlyName = vehicle.FriendlyName;
                        dto.Category = vehicle.Category.ToString();

                        client.PutAsset(dto.Identifier, dto);
                    }
                }
            }
            catch (System.Exception ex)
            {
                Log.ToolException(ex, "Unhandled exception while syncing vehicles.");
            }
        }

        /// <summary>
        /// Syncs all the weapons from the exported scene xml.
        /// </summary>
        public void SyncWeapons()
        {
            Log.Profile("Syncing Weapons");

            IWeaponCollection collection = WeaponFactory.CreateCollection(DataSource.SceneXml, Branch);
            SyncWeapons(collection);

            foreach (IProject dlcProject in Branch.Project.Config.DLCProjects.Values)
            {
                collection = WeaponFactory.CreateCollection(DataSource.SceneXml, dlcProject.DefaultBranch);
                SyncWeapons(collection);
            }

            Log.ProfileEnd();
        }

        /// <summary>
        /// Helper for syncing weapons from a particular collection
        /// </summary>
        private void SyncWeapons(IWeaponCollection collection)
        {
            try
            {
                using (WeaponClient client = new WeaponClient(m_server))
                {
                    foreach (IWeapon weapon in collection)
                    {
                        Log.Debug("Updating {0}", weapon.Name);

                        WeaponDto dto = new WeaponDto();
                        dto.Name = weapon.Name;
                        dto.Identifier = weapon.Hash.ToString();
                        dto.FriendlyName = weapon.FriendlyName;
                        dto.ModelName = weapon.ModelName;
                        dto.StatName = weapon.StatName;
                        dto.Category = weapon.Category.ToString();

                        client.PutAsset(dto.Identifier, dto);
                    }
                }
            }
            catch (System.Exception ex)
            {
                Log.ToolException(ex, "Unhandled exception while syncing vehicles.");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void SyncWebSites()
        {
            const String c_websitesFile = @"$(toolsconfig)\config\statistics\websites.xml";
            String filename = Branch.Environment.Subst(c_websitesFile);

            Log.Profile("Syncing Websites");

            try
            {
                IDictionary<uint, String> websites = LoadAssetsFromFile(filename, "/websites/website", "@name", "@name");
                using (GameAssetClient client = new GameAssetClient(m_server))
                {
                    client.UpdateWebsites(websites);
                }
            }
            catch (System.Exception ex)
            {
                Log.ToolException(ex, "Unhandled exception while syncing memory names.");
            }

            Log.ProfileEnd();
        }

        /// <summary>
        /// Sync the list of experience categories from an xml file.
        /// </summary>
        public void SyncXPCategories()
        {
            Log.Profile("Syncing XP categories");

            const String c_xpFile = @"$(toolsconfig)\config\statistics\xp.xml";
            String filename = Branch.Environment.Subst(c_xpFile);

            try
            {
                IList<FriendlyGameAsset> categories = LoadAssetsWithFriendlyNameFromFile(filename, "/xp/categories/category", "@name", "@friendly_name");
                using (GameAssetClient client = new GameAssetClient(m_server))
                {
                    client.UpdateXPCategories(categories);
                }
            }
            catch (System.Exception ex)
            {
                Log.ToolException(ex, "Unhandled exception while syncing experience categories.");
            }

            Log.ProfileEnd();
        }

        /// <summary>
        /// Sync the list of experience types from an xml file.
        /// </summary>
        public void SyncXPTypes()
        {
            Log.Profile("Syncing XP types");

            const String c_xpFile = @"$(toolsconfig)\config\statistics\xp.xml";
            String filename = Branch.Environment.Subst(c_xpFile);

            try
            {
                IList<FriendlyGameAsset> types = LoadAssetsWithFriendlyNameFromFile(filename, "/xp/types/type", "@name", "@friendly_name");
                using (GameAssetClient client = new GameAssetClient(m_server))
                {
                    client.UpdateXPTypes(types);
                }
            }
            catch (System.Exception ex)
            {
                Log.ToolException(ex, "Unhandled exception while syncing experience types.");
            }

            Log.ProfileEnd();
        }
        #endregion // Asset Syncing

        #region Asset Stat Syncing
        /// <summary>
        /// Syncs all the level statistics.
        /// </summary>
        public void SyncLevelStats(BuildDto build)
        {
            Log.Profile("Syncing Level Stats for Build '{0}'", build.Identifier);
            
            try
            {
                using (BuildClient client = new BuildClient(m_server))
                {
                    foreach (ILevel level in Levels.Where(item => item.Name != "None"))
                    {
                        Log.Debug("Updating {0}", level.Name);

                        // Ensure the level is fully loaded
                        level.LoadAllStats();

                        // Create the character stat bundle from the model object
                        LevelStatBundleDto bundle = Mapper.Map<ILevel, LevelStatBundleDto>(level);

                        // Handle the background image/bugstar id manually
                        if (level.Image != null)
                        {
                            using (MemoryStream stream = new MemoryStream())
                            {
                                JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                                encoder.QualityLevel = 100;
                                encoder.Frames.Add(BitmapFrame.Create(level.Image));
                                encoder.Save(stream);
                                stream.Position = 0;

                                using (BinaryReader br = new BinaryReader(stream))
                                {
                                    bundle.ImageData = br.ReadBytes((Int32)stream.Length);
                                }
                            }
                        }

                        // Getting the attachment id for the background image is a bit more involved
                        if (_bugstarConfig.Levels.Any(item => String.Compare(item.Key, level.Name, true) == 0))
                        {
                            uint levelId = _bugstarConfig.Levels.FirstOrDefault(item => String.Compare(item.Key, level.Name, true) == 0).Value;

                            try
                            {
                                Project project = Project.GetProjectById(_bugstarConnection, _bugstarConfig.ProjectId);
                                if (project != null)
                                {
                                    Map bugstarMap = Map.GetMapById(project, levelId);
                                    if (bugstarMap != null)
                                    {
                                        bundle.ImageBugstarId = bugstarMap.ImageAttachmentId;
                                    }
                                }
                            }
                            catch (System.Exception ex)
                            {
                                Log.ToolException(ex, "Unhandled exception during Bugstar connection.");
                            }
                        }

                        // Finally pass the bundle object to the server via the proxy to get created
                        client.CreateLevelStat(build.Identifier, level.Hash.ToString(), bundle);
                    }
                }
            }
            catch (System.Exception ex)
            {
                Log.ToolException(ex, "Unhandled exception while syncing builds.");
            }
            
            Log.ProfileEnd();
        }

        /// <summary>
        /// 
        /// </summary>
        public void SyncClipDictionaryStats(BuildDto build)
        {
            Log.Profile("Syncing Clip Dictionary Stats for Build '{0}'", build.Identifier);

            try
            {
                using (BuildClient client = new BuildClient(m_server))
                {
                    client.CreateClipDictionaryStats(build.Identifier, ClipDictionaries);
                }
            }
            catch (System.Exception ex)
            {
                Log.ToolException(ex, "Unhandled exception while syncing clip dictionaries.");
            }

            Log.ProfileEnd();
        }

        /// <summary>
        /// Syncs per build cutscene stats.
        /// </summary>
        /// <param name="build"></param>
        public void SyncCutsceneStats(BuildDto build)
        {
            Log.Profile("Syncing Cutscenes Stats for Build '{0}'", build.Identifier);

            try
            {
                foreach (ICutscene cutscene in Cutscenes)
                {
                    cutscene.LoadAllStats();
                }

                using (BuildClient client = new BuildClient(m_server))
                {
                    client.CreateCutsceneStats(build.Identifier, Cutscenes);
                }
            }
            catch (System.Exception ex)
            {
                Log.ToolException(ex, "Unhandled exception while syncing cutscene stats.");
            }

            Log.ProfileEnd();
        }

        /// <summary>
        /// Syncs all the character statistics based on scene xml/rpf data.
        /// </summary>
        /// <param name="build"></param>
        public void SyncCharacterStats(BuildDto build)
        {
            Log.Profile("Syncing Character Stats for Build '{0}'", build.Identifier);
            
            try
            {
                using (BuildClient client = new BuildClient(m_server))
                {
                    foreach (ICharacter character in Characters.AllCharacters)
                    {
                        Log.Debug("Updating {0}", character.Name);

                        // Ensure the character is fully loaded
                        character.LoadAllStats();

                        // Create the character stat bundle from the model object
                        CharacterStatBundleDto bundle = Mapper.Map<ICharacter, CharacterStatBundleDto>(character);

                        // Need to handle the platform stats manually
                        foreach (RSG.Platform.Platform platform in Enum.GetValues(typeof(RSG.Platform.Platform)))
                        {
                            if (character.PlatformStats.ContainsKey(platform))
                            {
                                ICharacterPlatformStat platformStat = character.PlatformStats[platform];

                                CharacterPlatformStatBundleDto platformStatBundle = Mapper.Map<ICharacterPlatformStat, CharacterPlatformStatBundleDto>(platformStat);
                                if (bundle.CharacterPlatformStats == null)
                                {
                                    bundle.CharacterPlatformStats = new List<CharacterPlatformStatBundleDto>();
                                }
                                bundle.CharacterPlatformStats.Add(platformStatBundle);
                            }
                        }

                        // Finally pass the bundle object to the server via the proxy to get created
                        client.CreateCharacterStat(build.Identifier, character.Hash.ToString(), bundle);
                    }
                }
            }
            catch (System.Exception ex)
            {
                Log.ToolException(ex, "Unhandled exception while syncing character stats.");
            }

            Log.ProfileEnd();
        }

        /// <summary>
        /// Syncs all the vehicle statistics based on scene xml/rpf data.
        /// </summary>
        /// <param name="build"></param>
        public void SyncVehicleStats(BuildDto build)
        {
            Log.Profile("Syncing Vehicle Stats for Build '{0}'", build.Identifier);
            
            try
            {
                using (BuildClient client = new BuildClient(m_server))
                {
                    foreach (IVehicle vehicle in Vehicles)
                    {
                        Log.Debug("Updating {0}", vehicle.Name);

                        // Ensure the weapon is fully loaded
                        vehicle.LoadAllStats();

                        // Create the vehicle stat bundle from the model object
                        VehicleStatBundleDto bundle = Mapper.Map<IVehicle, VehicleStatBundleDto>(vehicle);

                        // Need to handle the platform stats manually
                        foreach (RSG.Platform.Platform platform in Enum.GetValues(typeof(RSG.Platform.Platform)))
                        {
                            if (vehicle.PlatformStats.ContainsKey(platform))
                            {
                                IVehiclePlatformStat platformStat = vehicle.PlatformStats[platform];
                                VehiclePlatformStatBundleDto platformStatBundle = Mapper.Map<IVehiclePlatformStat, VehiclePlatformStatBundleDto>(platformStat);
                                if (bundle.VehiclePlatformStats == null)
                                {
                                    bundle.VehiclePlatformStats = new List<VehiclePlatformStatBundleDto>();
                                }
                                bundle.VehiclePlatformStats.Add(platformStatBundle);
                            }
                        }

                        // Finally pass the bundle object to the server via the proxy to get created
                        client.CreateVehicleStat(build.Identifier, vehicle.Hash.ToString(), bundle);
                    }
                }
            }
            catch (System.Exception ex)
            {
                Log.ToolException(ex, "Unhandled exception while syncing vehicle stats.");
            }

            Log.ProfileEnd();
        }

        /// <summary>
        /// Syncs all the weapon statistics based on scene xml/rpf data.
        /// </summary>
        /// <param name="build"></param>
        public void SyncWeaponStats(BuildDto build)
        {
            Log.Profile("Syncing Weapon Stats for Build '{0}'", build.Identifier);
            
            try
            {
                using (BuildClient client = new BuildClient(m_server))
                {
                    foreach (IWeapon weapon in Weapons)
                    {
                        Log.Debug("Updating {0}", weapon.Name);

                        // Ensure the weapon is fully loaded
                        weapon.LoadAllStats();

                        // Create the weapon stat bundle from the model object
                        WeaponStatBundleDto bundle = Mapper.Map<IWeapon, WeaponStatBundleDto>(weapon);

                        // Need to handle the platform stats manually
                        foreach (RSG.Platform.Platform platform in Enum.GetValues(typeof(RSG.Platform.Platform)))
                        {
                            if (weapon.PlatformStats.ContainsKey(platform))
                            {
                                IWeaponPlatformStat platformStat = weapon.PlatformStats[platform];
                                WeaponPlatformStatBundleDto platformStatBundle = Mapper.Map<IWeaponPlatformStat, WeaponPlatformStatBundleDto>(platformStat);
                                if (bundle.WeaponPlatformStats == null)
                                {
                                    bundle.WeaponPlatformStats = new List<WeaponPlatformStatBundleDto>();
                                }
                                bundle.WeaponPlatformStats.Add(platformStatBundle);
                            }
                        }

                        // Finally pass the bundle object to the server via the proxy to get created
                        client.CreateWeaponStat(build.Identifier, weapon.Hash.ToString(), bundle);
                    }
                }
            }
            catch (System.Exception ex)
            {
                Log.ToolException(ex, "Unhandled exception while syncing weapon stats.");
            }

            Log.ProfileEnd();
        }

        /// <summary>
        /// Creates the map hierarchy dto to send to the db recursively and sends it in one go.
        /// </summary>
        public void SyncMapHierarchy(BuildDto build)
        {
            Log.Profile("Syncing Map Hierarchy for Build '{0}'", build.Identifier);
            
            try
            {
                using (BuildClient client = new BuildClient(m_server))
                {
                    foreach (IMapHierarchy mapHierarchy in MapHierarchies.Values.Where(item => item.OwningLevel.Name != "None"))
                    {
                        Log.Debug("Updating hierarchy for {0}", mapHierarchy.OwningLevel.Name);

                        MapHierarchyBundleDto dto = new MapHierarchyBundleDto();
                        dto.ChildNodes = new List<BasicMapNodeStatBaseDto>();

                        foreach (IMapNode childNode in mapHierarchy.ChildNodes)
                        {
                            BasicMapNodeStatBaseDto childDto = Mapper.Map<IMapNode, BasicMapNodeStatBaseDto>(childNode);
                            dto.ChildNodes.Add(childDto);

                            if (childNode is IMapArea)
                            {
                                // Recurse onto the map areas children
                                AddMapAreaChildren((IMapArea)childNode, (BasicMapAreaStatDto)childDto);
                            }
                        }

                        // Finally pass the bundle object to the server via the proxy to get created
                        client.CreateMapHierarchy(build.Identifier, mapHierarchy.OwningLevel.Hash.ToString(), dto);
                    }
                }
            }
            catch (System.Exception ex)
            {
                Log.ToolException(ex, "Unhandled exception while syncing map hierarchy.");
            }

            Log.ProfileEnd();
        }

        /// <summary>
        /// Called after syncing the map hierarchy, this method patches up any remaining map
        /// area data that wasn't sent as part of the hierarchy.
        /// </summary>
        /// <param name="build"></param>
        public void UpdateMapAreaStats(BuildDto build)
        {
            Log.Profile("Updating Map Area Stats");
            
            try
            {
                using (BuildClient client = new BuildClient(m_server))
                {
                    foreach (IMapHierarchy mapHierarchy in MapHierarchies.Values.Where(item => item.OwningLevel.Name != "None"))
                    {
                        Log.Message("Processing level {0}", mapHierarchy.OwningLevel.Name);
                        PreloadPropAndInteriorArchetypes(mapHierarchy);

                        foreach (IMapArea area in mapHierarchy.AllAreas)
                        {
                            Log.Debug("Updating area {0}", area.Name);

                            MapAreaStatBundleDto dto = Mapper.Map<IMapArea, MapAreaStatBundleDto>(area);
                            client.UpdateMapAreaStat(build.Identifier, mapHierarchy.OwningLevel.Hash.ToString(), area.Hash.ToString(), dto);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                Log.ToolException(ex, "Unhandled exception while syncing map area stats.");
            }

            Log.ProfileEnd();

        }

        /// <summary>
        /// Called after syncing the map hierarchy, this method patches up any remaining map
        /// section data that wasn't sent as part of the hierarchy.
        /// </summary>
        /// <param name="build"></param>
        public void UpdateMapSectionStats(BuildDto build)
        {
            Log.Profile("Updating Map Section Stats");
            
            try
            {
                using (BuildClient client = new BuildClient(m_server))
                {
                    foreach (IMapHierarchy mapHierarchy in MapHierarchies.Values.Where(item => item.OwningLevel.Name != "None"))
                    {
                        Log.Message("Processing level {0}", mapHierarchy.OwningLevel.Name);
                        PreloadPropAndInteriorArchetypes(mapHierarchy);

                        foreach (IMapSection section in mapHierarchy.AllSections)
                        {
                            Log.Debug("Updating section {0}", section.Name);
                            if (section.PropGroup != null)
                            {
                                section.PropGroup.RequestAllStatistics(false);
                            }
                            section.RequestAllStatistics(false);

                            MapSectionStatBundleDto dto = Mapper.Map<IMapSection, MapSectionStatBundleDto>(section);
                            client.UpdateMapSectionStat(build.Identifier, mapHierarchy.OwningLevel.Hash.ToString(), section.Hash.ToString(), dto);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                Log.ToolException(ex, "Unhandled exception while syncing map secion stats.");
            }

            Log.ProfileEnd();
        }

        /// <summary>
        /// Syncs archetype stats for a particular build.
        /// </summary>
        /// <param name="build"></param>
        public void SyncArchetypeStats(BuildDto build)
        {
            Log.Profile("Syncing Archetypes Stats for Build '{0}'", build.Identifier);
            
            try
            {
                using (BuildClient client = new BuildClient(m_server))
                {
                    foreach (IMapHierarchy mapHierarchy in MapHierarchies.Values.Where(item => item.OwningLevel.Name != "None"))
                    {
                        Log.Message("Processing level {0}", mapHierarchy.OwningLevel.Name);
                        PreloadPropAndInteriorArchetypes(mapHierarchy);

                        foreach (IMapSection section in mapHierarchy.AllSections)
                        {
                            Log.Debug("Syncing archetype stats for section {0}.", section.Name);
                            if (section.PropGroup != null)
                            {
                                section.PropGroup.RequestAllStatistics(false);
                            }
                            section.RequestAllStatistics(false);

                            ArchetypeStatBundles bundles = new ArchetypeStatBundles();

                            foreach (IDrawableArchetype archetype in section.Archetypes.OfType<IDrawableArchetype>())
                            {
                                bundles.Items.Add(Mapper.Map<IDrawableArchetype, DrawableArchetypeStatBundle>(archetype));
                            }

                            foreach (IFragmentArchetype archetype in section.Archetypes.OfType<IFragmentArchetype>())
                            {
                                bundles.Items.Add(Mapper.Map<IFragmentArchetype, FragmentArchetypeStatBundle>(archetype));
                            }
                    
                            foreach (IInteriorArchetype archetype in section.Archetypes.OfType<IInteriorArchetype>())
                            {
                                bundles.Items.Add(Mapper.Map<IInteriorArchetype, InteriorArchetypeStatBundle>(archetype));
                            }
                    
                            foreach (IStatedAnimArchetype archetype in section.Archetypes.OfType<IStatedAnimArchetype>())
                            {
                                bundles.Items.Add(Mapper.Map<IStatedAnimArchetype, StatedAnimArchetypeStatBundle>(archetype));
                            }

                            // Send all the data to the server.
                            if (bundles.Items.Any())
                            {
                                client.CreateArchetypeStats(build.Identifier, mapHierarchy.OwningLevel.Hash.ToString(), section.Hash.ToString(), bundles);
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                Log.ToolException(ex, "Unhandled exception while syncing archetype stats.");
            }

            Log.ProfileEnd();
        }

        /// <summary>
        /// Syncs entity stats for a particular build.
        /// </summary>
        /// <param name="build"></param>
        public void SyncEntityStats(BuildDto build)
        {
            Log.Profile("Syncing Entity Stats for Build '{0}'", build.Identifier);
            
            try
            {
                using (BuildClient client = new BuildClient(m_server))
                {
                    foreach (IMapHierarchy mapHierarchy in MapHierarchies.Values.Where(item => item.OwningLevel.Name != "None"))
                    {
                        Log.Message("Processing level {0}", mapHierarchy.OwningLevel.Name);
                        PreloadPropAndInteriorArchetypes(mapHierarchy);

                        foreach (IMapSection section in mapHierarchy.AllSections)
                        {
                            Log.Debug("Syncing entity stats for section {0}.", section.Name);
                            section.RequestAllStatistics(false);

                            EntityStatBundles bundles = new EntityStatBundles();

                            // Section entities
                            foreach (IEntity entity in section.ChildEntities)
                            {
                                bundles.Items.Add(Mapper.Map<IEntity, EntityStatBundle>(entity));
                            }

                            foreach (IInteriorArchetype interior in section.Archetypes.OfType<IInteriorArchetype>())
                            {
                                // Interior entities
                                foreach (IEntity entity in interior.ChildEntities)
                                {
                                    bundles.Items.Add(Mapper.Map<IEntity, EntityStatBundle>(entity));
                                }

                                // Room entities
                                foreach (IRoom room in interior.Rooms)
                                {
                                    foreach (IEntity entity in room.ChildEntities)
                                    {
                                        bundles.Items.Add(Mapper.Map<IEntity, EntityStatBundle>(entity));
                                    }
                                }
                            }

                            // Send all the data to the server.
                            if (bundles.Items.Any())
                            {
                                client.CreateEntityStats(build.Identifier, mapHierarchy.OwningLevel.Hash.ToString(), section.Hash.ToString(), bundles);
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                Log.ToolException(ex, "Unhandled exception while syncing entity stats.");
            }

            Log.ProfileEnd();
        }

        /// <summary>
        /// Called to sync all the car gens.
        /// </summary>
        /// <param name="build"></param>
        public void SyncCarGens(BuildDto build)
        {
            Log.Profile("Syncing Car Gen Stats");
            
            try
            {
                using (BuildClient client = new BuildClient(m_server))
                {
                    foreach (IMapHierarchy mapHierarchy in MapHierarchies.Values.Where(item => item.OwningLevel.Name != "None"))
                    {
                        Log.Message("Processing level {0}", mapHierarchy.OwningLevel.Name);
                        PreloadPropAndInteriorArchetypes(mapHierarchy);

                        foreach (IMapSection section in mapHierarchy.AllSections)
                        {
                            Log.Debug("Syncing car gen stats for section {0}", section.Name);
                            section.RequestAllStatistics(false);

                            CarGenStatDtos dtos = new CarGenStatDtos();

                            foreach (ICarGen carGen in section.CarGens)
                            {
                                dtos.Items.Add(Mapper.Map<ICarGen, CarGenStatDto>(carGen));
                            }

                            if (dtos.Items.Any())
                            {
                                client.CreateCarGenStats(build.Identifier, mapHierarchy.OwningLevel.Hash.ToString(), section.Hash.ToString(), dtos);
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                Log.ToolException(ex, "Unhandled exception while syncing car gens.");
            }

            Log.ProfileEnd();
        }

        /// <summary>
        /// Called to sync all the spawn points.
        /// </summary>
        /// <param name="build"></param>
        public void SyncSpawnPoints(BuildDto build)
        {
            Log.Profile("Syncing Spawn Point Stats");
            SpawnPointStatDtos dtos = new SpawnPointStatDtos();

            // Sync level spawn points
            try
            {
                using (BuildClient client = new BuildClient(m_server))
                {
                    foreach (ILevel level in Levels.Where(item => item.Name != "None"))
                    {
                        Log.Message("Processing level {0}", level.Name);
                        level.LoadAllStats();
                
                        // Handle the level spawn points first.
                        IList<SpawnPointStatDto> allLevelDtos = new List<SpawnPointStatDto>();

                        foreach (ISpawnPoint spawnPoint in level.SpawnPoints)
                        {
                            SpawnPointStatDto dto = Mapper.Map<ISpawnPoint, SpawnPointStatDto>(spawnPoint);
                            dto.LevelIdentifier = level.Hash.ToString();
                            allLevelDtos.Add(dto);
                        }

                        if (allLevelDtos.Any())
                        {
                            // Send 1000 at a time
                            const int c_batchSize = 1000;

                            for (int i = 0; i < allLevelDtos.Count; i += c_batchSize)
                            {
                                dtos.Items = allLevelDtos.Skip(i).Take(c_batchSize).ToList();
                                client.CreateSpawnPointStats(build.Identifier, level.Hash.ToString(), dtos);
                            }
                        }
                    }

                    // Handle the archetype spawn points next
                    foreach (IMapHierarchy hierarchy in MapHierarchies.Values.Where(item => item.OwningLevel.Name != "None"))
                    {
                        PreloadPropAndInteriorArchetypes(hierarchy);

                        foreach (IMapSection section in hierarchy.AllSections)
                        {
                            Log.Debug("Syncing spawn point stats for section {0}", section.Name);
                            section.RequestAllStatistics(false);

                            foreach (ISimpleMapArchetype archetype in section.Archetypes.OfType<ISimpleMapArchetype>())
                            {
                                dtos.Items.Clear();

                                foreach (ISpawnPoint spawnPoint in archetype.SpawnPoints)
                                {
                                    SpawnPointStatDto dto = Mapper.Map<ISpawnPoint, SpawnPointStatDto>(spawnPoint);
                                    dto.ArchetypeIdentifier = archetype.Hash.ToString();
                                    //HACK...automapper giving me problems again :[
                                    if (spawnPoint.AvailabilityMode == null)
                                    {
                                        dto.AvailabilityMode = null;
                                    }
                                    dtos.Items.Add(dto);
                                }

                                if (dtos.Items.Any())
                                {
                                    client.CreateSpawnPointStats(build.Identifier, hierarchy.OwningLevel.Hash.ToString(), dtos);
                                }
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                Log.ToolException(ex, "Unhandled exception while syncing spawn points.");
            }

            Log.ProfileEnd();
        }
        #endregion // Asset Stat Syncing

        #region Private Methods
        /// <summary>
        /// Loads a file extracting hash/name pairs.
        /// </summary>
        /// <param name="filename">File to load up.</param>
        /// <param name="itemXPath">XPath to the items that contain the names.</param>
        private IDictionary<uint, String> LoadAssetsFromFile(String filename, String itemXPath, String hashXPath, String nameXPath)
        {
            IDictionary<uint, String> results = new Dictionary<uint, String>();
            Debug.Assert(File.Exists(filename), "File doesn't exist");

            if (File.Exists(filename))
            {
                XDocument doc = XDocument.Load(filename);

                foreach (XElement elem in doc.XPathSelectElements(itemXPath))
                {
                    XObject hashObj = (elem.XPathEvaluate(hashXPath) as IEnumerable).Cast<XObject>().FirstOrDefault();
                    XObject nameObj = (elem.XPathEvaluate(nameXPath) as IEnumerable).Cast<XObject>().FirstOrDefault();

                    if (hashObj != null)
                    {
                        uint hash = OneAtATime.ComputeHash(GetValueFromXObject(hashObj));
                        String name = (nameObj == null ? GetValueFromXObject(hashObj) : GetValueFromXObject(nameObj));
                        results[hash] = name;
                    }
                }
            }

            return results;
        }

        /// <summary>
        /// 
        /// </summary>
        private IList<FriendlyGameAsset> LoadAssetsWithFriendlyNameFromFile(String filename, String itemXPath, String assetNameXPath, String fiendlyNameXPath)
        {
            IList<FriendlyGameAsset> results = new List<FriendlyGameAsset>();
            Debug.Assert(File.Exists(filename), "File doesn't exist");

            if (File.Exists(filename))
            {
                XDocument doc = XDocument.Load(filename);

                foreach (XElement elem in doc.XPathSelectElements(itemXPath))
                {
                    XObject assetNameObj = (elem.XPathEvaluate(assetNameXPath) as IEnumerable).Cast<XObject>().FirstOrDefault();
                    XObject friendlyNameObj = (elem.XPathEvaluate(fiendlyNameXPath) as IEnumerable).Cast<XObject>().FirstOrDefault();

                    if (assetNameObj != null && friendlyNameObj != null)
                    {
                        String assetName = GetValueFromXObject(assetNameObj);
                        String friendlyName = GetValueFromXObject(friendlyNameObj);

                        results.Add(new FriendlyGameAsset(assetName, friendlyName));
                    }
                }
            }

            return results;
        }

        /// <summary>
        /// Helper method to get the value of either an attribute or an element.
        /// </summary>
        private String GetValueFromXObject(XObject obj)
        {
            if (obj is XElement)
            {
                return (obj as XElement).Value;
            }
            else if (obj is XAttribute)
            {
                return (obj as XAttribute).Value;
            }
            else
            {
                Debug.Fail("Unsupported XObject type encountered.");
                throw new ArgumentException("Unsupported XObject type encountered.");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private IList<MiniGameVariant> LoadMiniGameVariantsFromFile(String filename)
        {
            IList<MiniGameVariant> results = new List<MiniGameVariant>();
            Debug.Assert(File.Exists(filename), "File doesn't exist");

            if (File.Exists(filename))
            {
                XDocument doc = XDocument.Load(filename);

                foreach (XElement elem in doc.XPathSelectElements("/minigames/minigame"))
                {
                    XAttribute typeAtt = elem.Attribute("type");
                    XAttribute assetNameAtt = elem.Attribute("name");
                    XAttribute friendlyNameAtt = elem.Attribute("friendly_name");
                    XAttribute xAtt = elem.Attribute("x");
                    XAttribute yAtt = elem.Attribute("y");
                    XAttribute zAtt = elem.Attribute("z");

                    if (typeAtt != null && assetNameAtt != null && friendlyNameAtt != null && xAtt != null && yAtt != null && zAtt != null)
                    {
                        results.Add(new MiniGameVariant
                            {
                                GameName = assetNameAtt.Value,
                                FriendlyName = friendlyNameAtt.Value,
                                MatchType = (MatchType)Enum.Parse(typeof(MatchType), typeAtt.Value),
                                Location = new Vector3f(Single.Parse(xAtt.Value), Single.Parse(yAtt.Value), Single.Parse(zAtt.Value))
                            });
                    }
                }
            }
            else
            {
                Log.Warning("{0} file doesn't exist.", filename);
            }

            return results;
        }

        /// <summary>
        /// 
        /// </summary>
        private IList<CashPack> LoadCashPacksFromFile(String filename)
        {
            IList<CashPack> results = new List<CashPack>();
            Debug.Assert(File.Exists(filename), "File doesn't exist");

            if (File.Exists(filename))
            {
                XDocument doc = XDocument.Load(filename);

                foreach (XElement elem in doc.XPathSelectElements("/cash_packs/cash_pack"))
                {
                    XAttribute nameAtt = elem.Attribute("name");
                    XAttribute valueAtt = elem.Attribute("value");

                    if (nameAtt != null && valueAtt != null)
                    {
                        results.Add(new CashPack
                        {
                            GameName = nameAtt.Value,
                            InGameAmount = UInt32.Parse(valueAtt.Value)
                        });
                    }
                }
            }
            else
            {
                Log.Warning("{0} file doesn't exist.", filename);
            }

            return results;
        }

        /// <summary>
        /// Recursive helper function for adding map area children to a map area stat dto.
        /// </summary>
        /// <param name="mapArea"></param>
        /// <param name="dto"></param>
        private void AddMapAreaChildren(IMapArea mapArea, BasicMapAreaStatDto dto)
        {
            dto.ChildNodes = new List<BasicMapNodeStatBaseDto>();

            foreach (IMapNode childNode in mapArea.ChildNodes)
            {
                BasicMapNodeStatBaseDto childDto = Mapper.Map<IMapNode, BasicMapNodeStatBaseDto>(childNode);
                dto.ChildNodes.Add(childDto);

                if (childNode is IMapArea)
                {
                    AddMapAreaChildren((IMapArea)childNode, (BasicMapAreaStatDto)childDto);
                }
            }
        }

        /// <summary>
        /// Initialises a map hierarchy by loading all props and interiors so that archetype references can
        /// be patched up when loading map section data.
        /// </summary>
        /// <param name="level"></param>
        private void PreloadPropAndInteriorArchetypes(IMapHierarchy hierarchy)
        {
            IEnumerable<StreamableStat> statsToLoad = new StreamableStat[]
                { StreamableMapSectionStat.ExportEntities, StreamableMapSectionStat.ExportArchetypes, StreamableMapSectionStat.Archetypes };

            // Load the archetypes for all the prop/interior sections (do this in two steps)
            foreach (IMapSection propSection in hierarchy.AllSections.Where(item => item.Type == RSG.Model.Common.Map.SectionType.Prop))
            {
                propSection.RequestStatistics(statsToLoad, false);
            }

            foreach (IMapSection interiorSection in hierarchy.AllSections.Where(item => item.Type == RSG.Model.Common.Map.SectionType.Interior))
            {
                interiorSection.RequestStatistics(statsToLoad, false);
            }
        }
        #endregion // Private Methods

        #region Static Method for Vector Map Point Serialisation
        /// <summary>
        /// 
        /// </summary>
        /// <param name="vectorMapPoints"></param>
        /// <returns></returns>
        public static byte[] SerialiseSectionVectorMap(IList<Vector2f> vectorMapPoints)
        {
            using (Stream stream = new MemoryStream())
            {
                XmlWriter writer = XmlWriter.Create(stream);
                writer.WriteStartElement("vectormap");

                if (vectorMapPoints != null)
                {
                    foreach (Vector2f point in vectorMapPoints)
                    {
                        writer.WriteStartElement("point");
                        writer.WriteAttributeString("x", point.X.ToString());
                        writer.WriteAttributeString("y", point.Y.ToString());
                        writer.WriteEndElement();
                    }
                }

                writer.WriteEndElement();
                writer.Close();

                stream.Position = 0;

                using (BinaryReader br = new BinaryReader(stream))
                {
                    return br.ReadBytes((Int32)stream.Length);
                }
            }
        }
        #endregion

        #region IDisposable Methods
        /// <summary>
        /// Dispose of the config game view to release managed resources.
        /// </summary>
        public void Dispose()
        {
            Log.Debug("Disposing of asset sync service.");
        }
        #endregion // IDisposable Methods
    } // AssetSyncService
}
