﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using LitJson;
using RSG.Base.Configuration;
using RSG.Base.Configuration.ROS;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Platform;
using RSG.ROS;
using RSG.ROS.Accounts;
using RSG.Statistics.Client.Common.Telemetry;
using RSG.Statistics.Client.Common.Telemetry.Converter;
using RSG.Statistics.Client.ServiceClients;
using RSG.Statistics.Common;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Dto.GameAssets;
using RSG.Statistics.Common.Dto.Telemetry;
using RSG.Statistics.Common.Dto.Telemetry.Metadata;
using RSG.Statistics.Common.Dto.Telemetry.Raw;
using RSG.Statistics.Common.Model.Organisation;
using RSG.Statistics.Common.Model.SocialClub;

namespace RSG.Statistics.Client.Common
{
    /// <summary>
    /// Use this class for syncing telemetry data between ROS web services
    /// and the statistics server.
    /// </summary>
    public class TelemetrySyncService : GameDataSyncServiceBase
    {
        #region Member Data
        /// <summary>
        /// Default converter to use when doing json -> dto conversions.
        /// </summary>
        private IConverter m_defaultConverter;

        /// <summary>
        /// Custom converters for telemetry packets that need additional processing.
        /// </summary>
        private IDictionary<TelemetryMetric, IConverter> m_customConverters;

        /// <summary>
        /// Lookup containing level idx => level id's
        /// </summary>
        private IDictionary<uint, long> m_levelIdLookup;

        /// <summary>
        /// Lookup containing level identifier (hash) => level id's
        /// </summary>
        private IDictionary<uint, long> m_levelIdentifierLookup;

        /// <summary>
        /// Lookup containing build identifiers => build id's
        /// </summary>
        private IDictionary<string, long> m_buildIdLookup;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="outputDirectory">Location the parsed/failed telemetry files should be saved to.</param>
        public TelemetrySyncService(IServer server)
            : base(server)
        {
            SubmissionToDtoConverter subConverter = new SubmissionToDtoConverter();
            m_defaultConverter = new DefaultConverter(subConverter);

            m_customConverters = new Dictionary<TelemetryMetric, IConverter>();

            MatchConverter matchConverter = new MatchConverter(subConverter);
            m_customConverters.Add(TelemetryMetric.SessionMatchStarted, matchConverter);
            m_customConverters.Add(TelemetryMetric.SessionMatchEnded, matchConverter);

            m_customConverters.Add(TelemetryMetric.Shopping, new ShoppingConverter(subConverter));

            m_levelIdLookup = new Dictionary<uint, long>();
            m_levelIdentifierLookup = new Dictionary<uint, long>();
            m_buildIdLookup = new Dictionary<string, long>();
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// Syncs all telemetry data for a particular date range.
        /// </summary>
        public void SyncRequiredTelemetry(ROSPlatform? singlePlatform, uint maxHours)
        {
            AdminAuthService authService = new AdminAuthService(m_rosConfig);
            TelemetryService teleService = new TelemetryService(m_rosConfig);

            IList<ROSPlatform> platforms = new List<ROSPlatform>();
            if (singlePlatform != null)
            {
                platforms.Add(singlePlatform.Value);
            }
            else
            {
                platforms.AddRange(m_rosConfig.Platforms.Select(item => (ROSPlatform)Enum.Parse(typeof(ROSPlatform), item, true)));
            }

            // Get telemetry data for the required platforms.
            foreach (ROSPlatform platform in platforms)
            {
                // Determine the start/end dates
                DateTime startDate = DateTime.Now.AddHours(-maxHours);
                DateTime endDate = DateTime.Now;

                using (RawTelemetryClient client = new RawTelemetryClient(m_server))
                {
                    DateTime? maxDate = client.GetMaxSubmissionDate(platform.ToString());

                    if (maxDate != null)
                    {
                        startDate = maxDate.Value;
                        endDate = startDate.AddHours(maxHours);
                        if (endDate > DateTime.Now)
                        {
                            endDate = DateTime.Now;
                        }
                    }
                }

                m_log.Message("Retrieving {0} telemetry data from the ROS web services between {1} and {2}.", platform, startDate, endDate);

                // Get the authentication ticket for this platform
                AuthTicket ticket = CreateAuthenticationTicket(authService, platform);
                if (ticket == null || !ticket.IsValid)
                {
                    m_log.Error("Failed to get valid ROS authentication ticket.");
                    continue;
                }
                m_log.Debug("Ticket: {0}", ticket.EncryptedAuth);

                // Next download the telemetry stats for the previous day
                using (Stream stream = GetTelemetryData(teleService, ticket, startDate, endDate))
                {
                    if (stream != null)
                    {
                        // Save the file to the incoming directory.
                        String filename = String.Format("{0:d4}{1:d2}{2:d2}T{3:d2}{4:d2}{5:d2}Z-{6:d4}{7:d2}{8:d2}T{9:d2}{10:d2}{11:d2}Z_{12}.json",
                            startDate.Year, startDate.Month, startDate.Day, startDate.Hour, startDate.Minute, startDate.Second,
                            endDate.Year, endDate.Month, endDate.Day, endDate.Hour, endDate.Minute, endDate.Second,
                            platform.ToString().ToLower());
                        String filepath = null;
                        SaveTelemetryStream(stream, filepath);

                        // Reset the stream position and start the processing
                        stream.Position = 0;
                        ProcessTelemetryStream(stream, platform, filepath, ticket, startDate, endDate);
                        UpdateSocialClubCountries(ticket);
                        UpdateSocialClubGamerData(ticket);
                    }
                    else
                    {
                        m_log.Error("No telemetry data received.");
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="platform"></param>
        public void SyncTelemetryForDateRange(DateTime startDate, DateTime endDate, ROSPlatform? singlePlatform)
        {
            IList<ROSPlatform> platforms = new List<ROSPlatform>();
            if (singlePlatform != null)
            {
                platforms.Add(singlePlatform.Value);
            }
            else
            {
                platforms.AddRange(m_rosConfig.Platforms.Select(item => (ROSPlatform)Enum.Parse(typeof(ROSPlatform), item, true)));
            }

            AdminAuthService authService = new AdminAuthService(m_rosConfig);
            TelemetryService teleService = new TelemetryService(m_rosConfig);

            // Get telemetry data for the required platforms.
            foreach (ROSPlatform platform in platforms)
            {
                m_log.Message("Retrieving {0} telemetry data from the ROS web services between {1} and {2}.", platform, startDate, endDate);

                // Get the authentication ticket for this platform
                AuthTicket ticket = CreateAuthenticationTicket(authService, platform);
                if (ticket == null || !ticket.IsValid)
                {
                    m_log.Error("Failed to get valid ROS authentication ticket.");
                    continue;
                }
                m_log.Debug("Ticket: {0}", ticket.EncryptedAuth);

                // Next download the telemetry stats for the previous day
                using (Stream stream = GetTelemetryData(teleService, ticket, startDate, endDate))
                {
                    if (stream != null)
                    {
                        // Save the file to the incoming directory.
                        String filename = String.Format("{0:d4}{1:d2}{2:d2}T{3:d2}{4:d2}{5:d2}Z-{6:d4}{7:d2}{8:d2}T{9:d2}{10:d2}{11:d2}Z_{12}.json",
                            startDate.Year, startDate.Month, startDate.Day, startDate.Hour, startDate.Minute,
                            endDate.Year, endDate.Month, endDate.Day, endDate.Hour, endDate.Minute,
                            platform.ToString().ToLower());
                        String filepath = null;
                        SaveTelemetryStream(stream, filepath);

                        // Reset the stream positon and start the processing
                        stream.Position = 0;
                        ProcessTelemetryStream(stream, platform, filepath, ticket, startDate, endDate);
                        UpdateSocialClubCountries(ticket);
                        UpdateSocialClubGamerData(ticket);
                    }
                    else
                    {
                        m_log.Error("No telemetry data received.");
                    }
                }
            }
        }

        /// <summary>
        /// Processes a particular files telemetry data
        /// </summary>
        /// <param name="dataStream"></param>
        public void ProcessTelemetryFile(String filepath, ROSPlatform platform)
        {
            m_log.Message("Processing telemetry file located at: {0}.", filepath);

            if (File.Exists(filepath))
            {
                // Get the authentication ticket for this platform
                AdminAuthService authService = new AdminAuthService(m_rosConfig);
                AuthTicket ticket = CreateAuthenticationTicket(authService, platform);
                if (ticket == null || !ticket.IsValid)
                {
                    m_log.Error("Failed to get valid ROS authentication ticket.");
                    return;
                }
                m_log.Debug("Ticket: {0}", ticket.EncryptedAuth);

                using (FileStream stream = File.Open(filepath, FileMode.Open))
                {
                    // Copy the file over to the incoming directory.
                    String incomingFilepath = null;
                    SaveTelemetryStream(stream, incomingFilepath);

                    stream.Position = 0;
                    ProcessTelemetryStream(stream, platform, incomingFilepath, ticket);
                    UpdateSocialClubCountries(ticket);
                    UpdateSocialClubGamerData(ticket);
                }
            }
            else
            {
                m_log.Error("File doesn't exist!");
            }
        }
        #endregion // Public Methods

        #region Private Methods
        /// <summary>
        /// Queries the ROS telemetry web service to get the requested telemetry data
        /// </summary>
        /// <param name="teleService"></param>
        /// <param name="ticket"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        private Stream GetTelemetryData(TelemetryService teleService, AuthTicket ticket, DateTime startDate, DateTime endDate)
        {
            m_log.Message("Requesting telemetry data between {0} and {1}.", startDate, endDate);
            Stream dataStream = null;

            // Request the data from ROS
            using (Stream webStream = teleService.AdminQueryByDate(ticket, startDate, endDate))
            {
                if (webStream != null)
                {
                    dataStream = new MemoryStream();
                    webStream.CopyTo(dataStream);
                    dataStream.Flush();
                }
            }

            return dataStream;
        }

        /// <summary>
        /// Processes a stream of telemetry data saving it to the database
        /// </summary>
        /// <param name="data"></param>
        private void ProcessTelemetryStream(Stream stream, ROSPlatform rosPlatform, String incomingFilepath, AuthTicket rosTicket, DateTime? startDate = null, DateTime? endDate = null)
        {
            long processedToIdx = -1;
            TelemetryStatFileDto fileDto = null;

            // Convert the ros platform to a normal platform.
            // TODO: This is a bit iffy (it won't work for windows for instance, it will do for now).
            RSG.Platform.Platform platform = ConvertROSPlatformToGamePlatform(rosPlatform);

            // Get the stat file dto we will be dealing with.
            using (RawTelemetryClient client = new RawTelemetryClient(m_server))
            {
                // Check whether we are processing a new file based off of the start/end dates.
                if (startDate != null && endDate != null)
                {
                    fileDto = client.GetStatFile(startDate.Value, endDate.Value, platform.ToString());
                }

                if (fileDto == null)
                {
                    // Doesn't exist in the db yet, create a new telemetry stat file entry for this stream
                    fileDto = new TelemetryStatFileDto();
                    fileDto.Start = startDate;
                    fileDto.End = endDate;
                    fileDto.Platform = platform;
                    fileDto = client.CreateStatFile(fileDto);
                }
                else
                {
                    // It already existed, so check where we processed up until.
                    processedToIdx = client.GetMaxPacketIdx(fileDto.Id.ToString());
                }
            }

            bool success = true;
            uint packetIdx = 0;
            TelemetrySyncContext syncContext = new TelemetrySyncContext();
            try
            {
                // Note that we can't read in the whole stream as a json object due to memory constraints.
                // Instead we are relying on the assumption that at the root level of the data stream the
                // data is stored in a Json array with each array item on its own line along with the opening
                // and closing square brackets on their own lines as well.  This then lets us treat each
                // array item as a json object.
                stream.Position = 0;
                StreamReader streamReader = new StreamReader(stream);
                
                // Do a quick pass to determine the number of packets
                m_log.Profile("Entry counting");
                uint entries = 0;
                string line = null;
                while ((line = streamReader.ReadLine()) != null)
                {
                    if (!line.StartsWith("[") && !line.StartsWith("]"))
                    {
                        ++entries;
                    }
                }
                m_log.ProfileEnd();
                m_log.Message("{0} stat packets to process.", entries);

                // Reset the position to the beginning and start processing for real
                streamReader.BaseStream.Position = 0;

                m_log.Profile("Entry processing");
                while ((line = streamReader.ReadLine()) != null)
                {
                    // Ignore the json array tokens
                    if (!line.StartsWith("[") && !line.StartsWith("]"))
                    {
                        // Skip over packets we've already processed.
                        if (processedToIdx >= packetIdx)
                        {
                            packetIdx++;
                            continue;
                        }

                        // Trim off any commas at the beginning of the line
                        line = line.TrimStart(new char[] { ',' });

                        // Process each stat packet individually
                        ProcessStatPacket(line, packetIdx++, (long)fileDto.Id, rosPlatform, syncContext, rosTicket);

                        // Output some progress information.
                        if (packetIdx % 1000 == 0)
                        {
                            m_log.Message("Processed {0} of {1} packets.", packetIdx, entries);
                        }
                        else if (packetIdx % 100 == 0)
                        {
                            m_log.Debug("Processed {0} of {1} packets.", packetIdx, entries);
                        }
                    }
                }
                m_log.ProfileEnd();
            }
            catch (System.Exception ex)
            {
                m_log.Warning("Last packet to be processed was number {0}.", packetIdx);
                m_log.Exception(ex, "Unhandled exception occurred while processing the telemetry data stream.");
                success = false;
            }

            // Move the incoming telemetry file to the appropriate location.
            String filepath = null;
            if (File.Exists(filepath))
            {
                File.Delete(filepath);
            }
            File.Move(incomingFilepath, filepath);

            // Update the telemetry stat file entry with some stats
            fileDto.FilePath = filepath;
            if (fileDto.PacketCount == null)
            {
                fileDto.PacketCount = 0;
            }
            fileDto.PacketCount += syncContext.PacketsProcessed;
            if (fileDto.SubmissionCount == null)
            {
                fileDto.SubmissionCount = 0;
            }
            fileDto.SubmissionCount += syncContext.SubmissionsProcessed;

            using (RawTelemetryClient client = new RawTelemetryClient(m_server))
            {
                client.UpdateStatFile(fileDto.Id.ToString(), fileDto);
            }

            // Output some information to the log about the file we just processed
            m_log.Message("Processing complete.");
            m_log.Message("Packets:");
            m_log.Message("- Total:     {0}", syncContext.PacketsProcessed + syncContext.PacketsIgnored + syncContext.PacketsFailed);
            m_log.Message("- Processed: {0}", syncContext.PacketsProcessed);
            m_log.Message("- Ignored:   {0}", syncContext.PacketsIgnored);
            m_log.Message("- Failed:    {0}", syncContext.PacketsFailed);
            m_log.Message("Submissions:");
            m_log.Message("- Total:     {0}", syncContext.SubmissionsProcessed + syncContext.SubmissionsIgnored);
            m_log.Message("- Processed: {0}", syncContext.SubmissionsProcessed);
            m_log.Message("- Ignored:   {0}", syncContext.SubmissionsIgnored);
            m_log.Message("Metrics:");
            m_log.Message("- Known:   {0}", String.Join(", ", syncContext.KnownMetrics.OrderBy(item => item)));
            m_log.Message("- Unknown: {0}", String.Join(", ", syncContext.UnknownMetrics.OrderBy(item => item)));
        }

        /// <summary>
        /// Processes an individual stat packet
        /// </summary>
        /// <param name="packetData"></param>
        /// <param name="idx"></param>
        /// <param name="telemetryFileId"></param>
        /// <param name="platform"></param>
        private void ProcessStatPacket(string line, uint packetIdx, long telemetryFileId, ROSPlatform platform, TelemetrySyncContext syncContext, AuthTicket rosTicket)
        {
            try
            {
                // Convert the string to a json object
                JsonData packetData = JsonMapper.ToObject(line);

                // First submission contains game details for either singleplayer or multiplayer.
                // Only process all the submissions if there are other sub packets as well.
                if (packetData.ContainsKey("sub") && packetData["sub"].IsArray && packetData["sub"].Count > 1)
                {
                    SubmissionHeader header = ProcessGameInfoHeader(packetData["sub"][0]);

                    // Processing the packet if we managed to parse the header and the build version isn't 0
                    if (header != null && !(header.BuildMajorVersion == 0 && header.BuildMinorVersion == 0))
                    {
                        TelemetryStatPacketDto packetDto = new TelemetryStatPacketDto();
                        packetDto.PacketIdx = packetIdx;
                        packetDto.Date = header.SubmissionTime;
                        packetDto.GamerId = GetGamerId((string)packetData["gh"], platform);
                        packetDto.BuildId = GetBuildId(header.BuildMajorVersion, header.BuildMinorVersion);
                        packetDto.Platform = ConvertROSPlatformToGamePlatform(platform);
                        packetDto.LevelId = GetLevelId(header.LevelId);
                        packetDto.AttemptIdentifier = header.AttemptIdentifier;
                        packetDto.MultiplayerSessionId = header.MultiplayerSessionId;

                        // Process the individual submissions
                        packetDto.TelemetryStats = new List<TelemetryStatDto>();

                        for (int subIdx = 1; subIdx < packetData["sub"].Count; ++subIdx)
                        {
                            try
                            {
                                // Is the metric one that we know about?
                                string metricName = (string)packetData["sub"][subIdx]["m"];
                                TelemetryMetric metric;

                                if (TelemetryMetricUtils.TryParseFromRuntimeString(metricName, out metric))
                                {
                                    // Check which converter we should be using.
                                    IConverter converter = m_defaultConverter;
                                    if (m_customConverters.ContainsKey(metric))
                                    {
                                        converter = m_customConverters[metric];
                                    }

                                    // Convert the submission to a dto object and then add it to our list
                                    TelemetryStatDto dto = converter.ConvertJsonToDto(packetData["sub"][subIdx], metric, header.BaseTime, rosTicket);

                                    if (dto != null)
                                    {
                                        packetDto.TelemetryStats.Add(dto);
                                    }

                                    syncContext.KnownMetrics.Add(metric);
                                    syncContext.SubmissionsProcessed++;
                                }
                                else
                                {
                                    syncContext.UnknownMetrics.Add(metricName);
                                    syncContext.SubmissionsIgnored++;
                                }
                            }
                            catch (System.Exception ex)
                            {
                                m_log.Exception(ex, "Failed to process submission idx '{0}' in packet idx '{1}'", subIdx, packetIdx);
                                syncContext.SubmissionsIgnored++;
                            }
                        }

                        // Finally send the whole packet to the server (if it contains some valid telemetry stats))
                        if (packetDto.TelemetryStats.Count > 0)
                        {
                            using (RawTelemetryClient client = new RawTelemetryClient(m_server))
                            {
                                client.CreateStatPacket(telemetryFileId.ToString(), packetDto);
                            }
                            syncContext.PacketsProcessed++;
                        }
                        else
                        {
                            syncContext.PacketsIgnored++;
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                syncContext.PacketsFailed++;
                m_log.Error("Failed to process packet idx '{0}' due to a {1} exception:\n{2}.", packetIdx, ex.GetType().Name, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="headerData"></param>
        private SubmissionHeader ProcessGameInfoHeader(JsonData headerData)
        {
            SubmissionHeader header = null;

            if (headerData.IsObject && headerData.ContainsKey("game") && headerData["game"].IsObject)
            {
                header = new SubmissionHeader(headerData);
            }

            return header;
        }

        /// <summary>
        /// Saves the telemetry stream out to a file based on whether the file was successfully processed or not
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="filepath"></param>
        private void SaveTelemetryStream(Stream stream, string filepath)
        {
            stream.Position = 0;

            // Create the file path
            if (!Directory.Exists(Path.GetDirectoryName(filepath)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(filepath));
            }

            // Delete the file if it exists
            if (File.Exists(filepath))
            {
                File.Delete(filepath);
            }

            // Save out the file.
            using (FileStream fs = File.Create(filepath))
            {
                stream.CopyTo(fs);
                fs.Flush();
            }

            m_log.Message("Telemetry file saved to: {0}", filepath);
        }

        #region Cache Lookups
        /// <summary>
        /// Retrieves a build's id based on the major/minor versions.
        /// Note that if the build doesn't exist, it creates a new one.
        /// </summary>
        /// <param name="major"></param>
        /// <param name="minor"></param>
        /// <returns></returns>
        private long GetBuildId(uint major, uint minor)
        {
            string buildIdentifier = (minor == 0 ? major.ToString() : String.Format("{0}.{1}", major, minor));

            // Check whether the cache has been populated yet
            if (m_buildIdLookup.Count == 0)
            {
                using (BuildClient client = new BuildClient(m_server))
                {
                    foreach (BuildDto build in client.GetAll().Items)
                    {
                        if (build.Identifier != null)
                        {
                            m_buildIdLookup[build.Identifier] = (long)build.Id;
                        }
                    }
                }
            }

            long buildId = 0;

            // Does the cache contain this build?
            if (m_buildIdLookup.ContainsKey(buildIdentifier))
            {
                buildId = m_buildIdLookup[buildIdentifier];
            }
            else
            {
                using (BuildClient client = new BuildClient(m_server))
                {
                    // If not, create a new build and add it to the cache
                    BuildDto build = new BuildDto();
                    build.Identifier = buildIdentifier;
                    build.GameVersion = (major << 4) | minor;
                    build = client.PutAsset(buildIdentifier, build);
                    m_buildIdLookup[buildIdentifier] = (long)build.Id;
                    buildId = (long)build.Id;
                }
            }

            return buildId;
        }

        /// <summary>
        /// Currently supports both the old index and the new level identifier.
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        private long GetLevelId(uint levelIdx)
        {
            // Check whether the cache has been populated yet
            if (m_levelIdLookup.Count == 0)
            {
                using (LevelClient client = new LevelClient(m_server))
                {
                    foreach (LevelDto level in client.GetAll().Items)
                    {
                        if (level.LevelIdx != null)
                        {
                            m_levelIdLookup[(uint)level.LevelIdx] = (long)level.Id;
                        }
                        if (level.Identifier != null)
                        {
                            m_levelIdentifierLookup[UInt32.Parse(level.Identifier)] = (long)level.Id;
                        }
                    }
                }
            }

            // Look up the index in the cache
            if (m_levelIdentifierLookup.ContainsKey(levelIdx))
            {
                return m_levelIdentifierLookup[levelIdx];
            }
            else if (m_levelIdLookup.ContainsKey(levelIdx))
            {
                return m_levelIdLookup[levelIdx];
            }
            else
            {
                throw new ArgumentException("Level idx was not found in the database.");
            }
        }
        #endregion // Cache Lookups
        #endregion // Private Methods
    } // TelemetrySyncService
}
