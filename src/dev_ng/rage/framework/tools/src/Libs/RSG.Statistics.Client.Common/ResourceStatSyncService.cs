﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Client.ServiceClients;
using RSG.Statistics.Common;
using RSG.Model.Statistics.Captures;
using System.IO;
using System.Xml;
using RSG.Base.Logging;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Model.Statistics.Platform;
using RSG.Statistics.Common.Config;

namespace RSG.Statistics.Client.Common
{
    /// <summary>
    /// Service to populate database with stats from resourcing ( typically on Assetbuilder )
    /// </summary>
    public class ResourceStatSyncService
    {
        #region Public Methods
        /// <summary>
        /// Send the stats to the database that are found using a particular wildcard 
        /// </summary>
        /// <param name="wildcard">the filesystem wildcard you wish to upload</param>
        public void SyncStatsWildcard(String wildcard, IStatsServer server)
        {
            Log.Log__Message("Populating resourcing stats from wildcard {0}", wildcard);
            Log.Log__Profile("ResourceStatSyncService::SyncStatsWildcard");
            List<ResourceStat> resStats = ResourceStatFactory.CreateResourceStats(wildcard);

            // Send the resource stat objects to the server 
            using (ResourceStatClient client = new ResourceStatClient(server))
            {
                client.CreateResourceStats(resStats);
            }

            Log.Log__ProfileEnd();
        }
        #endregion // Public Methods
    } // ResourceStatSyncService
} // namespace RSG.Statistics.Client.Common
