﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Client.ServiceClients;
using RSG.Statistics.Common;
using RSG.Model.Statistics.Captures;
using System.IO;
using System.Xml;
using RSG.Base.Logging;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Statistics.Common.Config;

namespace RSG.Statistics.Client.Common
{
    /// <summary>
    /// 
    /// </summary>
    public class CaptureStatSyncService
    {
        #region Member Data
        /// <summary>
        /// Statistics server to communicate with.
        /// </summary>
        protected IStatsServer m_server;
        #endregion

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="server"></param>
        public CaptureStatSyncService(IStatsServer server)
        {
            m_server = server;
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// Send the stats associated with a particular stat file to the database for a particular changelist.
        /// </summary>
        /// <param name="statFile"></param>
        /// <param name="changelist"></param>
        /// <param name="level"></param>
        /// <param name="buildConfig"></param>
        public void SyncChangelistStats(String statFile, uint changelist, String level, BuildConfig buildConfig)
        {
            Log.Log__Message("Populating changelist capture stats for cl {0} from file {1}", changelist, statFile);

            TestSession testSession = LoadTestSession(statFile);

            if (testSession != null)
            {
                testSession.LevelName = level;
                testSession.BuildConfig = buildConfig.ToString();

                // Send the stats to the server.
                using (CaptureClient client = new CaptureClient(m_server))
                {
                    client.CreateChangelistStats(changelist.ToString(), testSession);
                }
            }
            else
            {
                Log.Log__Error("Population Failed as a valid test session object was not created.");
            }
        }

        /// <summary>
        /// Sends the stats associated with a particular stat file to the database, using the max changelist number that was
        /// extracted from the modifications file.
        /// </summary>
        /// <param name="statFile"></param>
        /// <param name="modsFile"></param>
        /// <param name="level"></param>
        /// <param name="buildConfig"></param>
        public void SyncChangelistStats(String statFile, String modsFile, String level, BuildConfig buildConfig)
        {
            uint maxChangelist = GetMaxChangelistNumber(modsFile);

            // Check that we received a valid changelist number.
            if (maxChangelist != default(uint))
            {
                SyncChangelistStats(statFile, maxChangelist, level, buildConfig);
            }
        }

        /// <summary>
        /// SyncPerfStats 
        /// </summary>
        /// <param name="wildcard">wildcard</param>
        /// <param name="level"></param>
        /// <param name="buildConfig"></param>
        public void SyncPerfStats(string wildcard, String level, BuildConfig buildConfig)
        {
            // The file will be a wildcard.
            string searchDir = System.IO.Path.GetDirectoryName(wildcard);
            string searchPattern = System.IO.Path.GetFileName(wildcard);

            // 1. Discover all these file names. 
            string[] allPerfFiles = Directory.GetFiles(searchDir, searchPattern, SearchOption.AllDirectories);

            // 2. For each determine the build number and add to a list of highest version sessions if required.
            float highestBuildVersion = float.MinValue;
            List<TestSession> testSessions = new List<TestSession>();
            foreach (string perfFilename in allPerfFiles.Where(n => File.Exists(n)))
            {
                TestSession testSession = LoadTestSession(perfFilename);          
                float buildVersion = float.Parse(testSession.Build);
                Log.Log__Message("{0} {1}", perfFilename, buildVersion);

                if (buildVersion > highestBuildVersion)
                {
                    testSessions.Clear();
                    highestBuildVersion = buildVersion;
                }

                if (buildVersion == highestBuildVersion)
                {
                    testSessions.Add(testSession);
                }
            }

            Log.Log__Message("Highest Build Version is {0}", highestBuildVersion);

            // 3. Aggregate test sessions & populate server.           
            TestSession testSessionsAggrgegated = new TestSession(testSessions);

            if (testSessionsAggrgegated != null)
            {
                testSessionsAggrgegated.LevelName = level;
                testSessionsAggrgegated.BuildConfig = buildConfig.ToString();

                // populate stats on the server.
                using (CaptureClient client = new CaptureClient(m_server))
                {
                    client.CreatePerfStats(testSessionsAggrgegated);
                }
            }
            else
            {
                Log.Log__Error("Population Failed as a valid test session object was not created.");
            }
        }
        #endregion // Public Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="statFile"></param>
        private TestSession LoadTestSession(String statFile)
        {
            TestSession session = null;

            // If the file exists, load it up and create a test session from it.
            if (File.Exists(statFile))
            {
                try
                {
                    DateTime timestamp = File.GetLastWriteTime(statFile);

                    using (FileStream stream = File.OpenRead(statFile))
                    {
                        session = new TestSession(stream, timestamp);
                    }
                }
                catch (Exception ex)
                {
                    Log.Log__Exception(ex, "Unexpected exception while loading up test session from stats.xml file.");
                }
            }

            return session;
        }

        /// <summary>
        /// Returns the maximum changelist number that was found in a particular modifications xml file.
        /// </summary>
        /// <param name="filepath"></param>
        /// <returns></returns>
        private uint GetMaxChangelistNumber(String filepath)
        {
            IList<ModificationInfo> modifications = new List<ModificationInfo>();

            if (File.Exists(filepath))
            {
                XDocument doc = XDocument.Load(filepath);

                foreach (XElement elem in doc.XPathSelectElements("//ArrayOfModification/Modification"))
                {
                    ModificationInfo modification = new ModificationInfo(elem);

                    // Ignore changes made by builder north and duplicate entries
                    if (modification.Username != "buildernorth" && !modifications.Contains(modification))
                    {
                        modifications.Add(modification);
                    }
                }
            }

            return modifications.Max(item => item.Changelist);
        }
        #endregion // Private Methods
    } // CaptureStatSyncService
}
