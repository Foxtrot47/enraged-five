﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Services.Assets.Contracts.ServiceContracts;
using RSG.Services.Configuration;
using RSG.Services.Configuration.ServiceModel;
using Dto = RSG.Services.Assets.Contracts.DataContracts;

namespace RSG.Services.Assets.Consumers
{
    /// <summary>
    /// WCF client for communicating with a service that implements the
    /// <see cref="IAssetPopulationService"/> service contract.
    /// </summary>
    public class AssetPopulationClient : ConfigClient<IAssetPopulationService>, IAssetPopulationService
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="AssetPopulationClient"/>
        /// using the specified server configuration object and uri scheme.
        /// </summary>
        public AssetPopulationClient(IServer server, string uriScheme)
            : base(server, uriScheme)
        {
        }
        #endregion

        #region IAssetPopulationService Methods
        /// <summary>
        /// Populates the database with a particular scene xml file.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="scene"></param>
        public void PopulateSceneXmlFile(string filename, Dto.Scene scene)
        {
            base.Channel.PopulateSceneXmlFile(filename, scene);
        }

        /// <summary>
        /// Deletes all data relating to a particular scene xml file.
        /// </summary>
        /// <param name="filename"></param>
        public void DeleteSceneXmlFile(string filename)
        {
            base.Channel.DeleteSceneXmlFile(filename);
        }
        #endregion
    }
}
