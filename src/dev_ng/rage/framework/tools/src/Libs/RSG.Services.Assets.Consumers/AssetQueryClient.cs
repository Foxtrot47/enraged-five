﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Services.Assets.Consumers.AsyncContracts;
using RSG.Services.Configuration;
using RSG.Services.Configuration.ServiceModel;
using Dto = RSG.Services.Assets.Contracts.DataContracts;

namespace RSG.Services.Assets.Consumers
{
    /// <summary>
    /// WCF client for communicating with a service that implements the
    /// <see cref="IAssetQueryService"/> service contract.
    /// </summary>
    public class AssetQueryClient : ConfigClient<IAsyncAssetQueryService>, IAsyncAssetQueryService
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="AssetQueryClient"/>
        /// using the specified server configuration object and uri scheme.
        /// </summary>
        public AssetQueryClient(IServer server, string uriScheme)
            : base(server, uriScheme)
        {
        }
        #endregion

        #region IAsyncAssetQueryService Methods
        /// <summary>
        /// Retrieves texture usage information for the specified textures.
        /// </summary>
        /// <param name="textures">List of textures to get usage information for.</param>
        /// <returns></returns>
        public IList<Dto.TextureUsage> GetTextureUsage(IEnumerable<string> textures)
        {
            return base.Channel.GetTextureUsage(textures);
        }

        /// <summary>
        /// Retrieves texture usage information for the specified textures.
        /// </summary>
        /// <param name="textures">List of textures to get usage information for.</param>
        /// <returns></returns>
        public async Task<IList<Dto.TextureUsage>> GetTextureUsageAsync(IEnumerable<string> textures)
        {
            return await base.Channel.GetTextureUsageAsync(textures);
        }

        /// <summary>
        /// Retrieves texture/alpha texture pairs for the passed in texture list.
        /// </summary>
        /// <param name="textures">List of textures to retrieve the pairs for.</param>
        /// <returns></returns>
        public IList<Dto.TexturePair> GetTexturePairs(IEnumerable<string> textures)
        {
            return base.Channel.GetTexturePairs(textures);
        }

        /// <summary>
        /// Retrieves texture/alpha texture pairs for the passed in texture list.
        /// </summary>
        /// <param name="textures">List of textures to retrieve the pairs for.</param>
        /// <returns></returns>
        public async Task<IList<Dto.TexturePair>> GetTexturePairsAsync(IEnumerable<string> textures)
        {
            return await base.Channel.GetTexturePairsAsync(textures);
        }
        #endregion
    }
}
