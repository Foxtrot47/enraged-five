﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using RSG.Services.Assets.Contracts.ServiceContracts;
using Dto = RSG.Services.Assets.Contracts.DataContracts;

namespace RSG.Services.Assets.Consumers.AsyncContracts
{
    /// <summary>
    /// Extends the <see cref="IAssetQueryService"/> interface with async versions of all
    /// the operation contracts.
    /// </summary>
    public interface IAsyncAssetQueryService : IAssetQueryService
    {
        /// <summary>
        /// Retrieves texture usage information for the specified textures.
        /// </summary>
        /// <param name="textures">List of textures to get usage information for.</param>
        /// <returns></returns>
        [OperationContract(
            Action = "http://tempuri.org/IAssetQueryService/GetTextureUsage",
            ReplyAction = "http://tempuri.org/IAssetQueryService/GetTextureUsageResponse")]
        Task<IList<Dto.TextureUsage>> GetTextureUsageAsync(IEnumerable<string> textures);

        /// <summary>
        /// Retrieves texture/alpha texture pairs for the passed in texture list.
        /// </summary>
        /// <param name="textures">List of textures to retrieve the pairs for.</param>
        /// <returns></returns>
        [OperationContract(
            Action = "http://tempuri.org/IAssetQueryService/GetTexturePairs",
            ReplyAction = "http://tempuri.org/IAssetQueryService/GetTexturePairsResponse")]
        Task<IList<Dto.TexturePair>> GetTexturePairsAsync(IEnumerable<string> textures);
    }
}
