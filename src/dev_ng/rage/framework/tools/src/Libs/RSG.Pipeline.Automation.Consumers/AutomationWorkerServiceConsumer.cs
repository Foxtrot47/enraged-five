﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Client;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Services.Messages;

namespace RSG.Pipeline.Automation.Consumers
{

    /// <summary>
    /// Enumeration representing worker consumer states.
    /// </summary>
    public enum WorkerConsumerState
    {
        Responsive,
        Unresponsive
    }

    /// <summary>
    /// AutomationWorkerService consumer class.
    /// </summary>
    public class AutomationWorkerServiceConsumer
    {
        #region Constants
        /// <summary>
        /// Log context.
        /// </summary>
        private static readonly String LOG_CTX = "Automation Worker Service Consumer";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Service connection string (e.g. "tcp.net://ediw-dmuir3:7001/automation_worker.svc")
        /// </summary>
        public Uri ServiceConnection
        {
            get { return _unresolvedUri; }
        }

        /// <summary>
        /// Job unique identifier (generated automatically).
        /// </summary>
        public Guid ID
        {
            get;
            private set;
        }

        /// <summary>
        /// Job capabilities.
        /// </summary>
        public Capability Capability
        {
            get;
            private set;
        }

        /// <summary>
        /// Assigned job Guid.
        /// </summary>
        public Guid AssignedJob
        {
            get;
            private set;
        }

        /// <summary>
        /// Log.
        /// </summary>
        private IUniversalLog Log
        {
            get;
            set;
        }

        /// <summary>
        /// State of worker consumer.  
        /// </summary>
        public WorkerConsumerState State
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Uri where the host name hasn't been resolved to an IP address.
        /// </summary>
        private readonly Uri _unresolvedUri;

        /// <summary>
        /// Uri where the host name has been resolved to an IP address.
        /// </summary>
        private readonly Uri _resolvedUri;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="ip"></param>
        /// <param name="capability"></param>
        /// <param name="cb"></param>
        public AutomationWorkerServiceConsumer(String service, CapabilityType role)
        {
            this._unresolvedUri = new Uri(service);
            this._resolvedUri = ResolveUri(this._unresolvedUri);

            this.ID = Guid.NewGuid();
            this.Capability = new Capability(role);
            LogFactory.Initialize();
            this.Log = LogFactory.CreateUniversalLog(LOG_CTX);
            this.State = WorkerConsumerState.Responsive;
        }
        #endregion // Constructor(s)

        // Controller methods here wrap the IAutomationWorkerService methods; 
        // hiding the details of the WCF service.  This allows our WCF service 
        // channels to be short-lived.
        #region Controller Methods
        /// <summary>
        /// Return current worker state.
        /// </summary>
        /// <returns></returns>
        public WorkerState GetWorkerState()
        {
            this.Log.DebugCtx(LOG_CTX, "AutomationWorkerServiceConsumer::GetState()");
            try
            {
                using (Clients.AutomationWorkerServiceClient client = this.Connect())
                {
                    try
                    {
                        WorkerState state = client.GetState();
                        return (state);
                    }
                    catch (Exception ex)
                    {
                        this.Log.ToolException(ex, "Error reading worker state.");
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.ToolException(ex, "Error disposing Automation Worker Service worker.");
            }
            this.State = WorkerConsumerState.Unresponsive;
            return (WorkerState.Error);
        }

        /// <summary>
        /// Send a job to be processed by the worker.
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        public bool Process(IJob job)
        {
            this.Log.DebugCtx(LOG_CTX, "AutomationWorkerServiceConsumer::Process({0})", job.ID);
            try
            {
                using (Clients.AutomationWorkerServiceClient client = this.Connect())
                {
                    try
                    {
                        bool result = client.Process(job);
                        if (result)
                        {
                            this.AssignedJob = job.ID;
                            job.ProcessedAt = DateTime.UtcNow;
                            job.State = JobState.Assigned;
                        }
                        else
                        {
                            this.AssignedJob = Guid.Empty;
                            job.State = JobState.Pending;
                        }
                        return (result);
                    }
                    catch (Exception ex)
                    {
                        this.Log.ToolException(ex, "Error processing job.");
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.ToolException(ex, "Error disposing Automation Worker Service worker.");
            }
            return (false);
        }

        /// <summary>
        /// Abort the job.
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        public bool Abort(IJob job)
        {
            this.Log.DebugCtx(LOG_CTX, "AutomationWorkerServiceConsumer::Abort({0})", job.ID);
            try
            {
                using (Clients.AutomationWorkerServiceClient client = this.Connect())
                {
                    try
                    {
                        bool result = client.Abort(job);
                        return (result);
                    }
                    catch (Exception ex)
                    {
                        this.Log.ToolException(ex, "Error aborting job.");
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.ToolException(ex, "Error disposing Automation Worker Service worker.");
            }
            return (false);
        }

        /// <summary>
        /// Worker job completed callback; invoked by the automation service
        /// to clear the assigned job property.
        /// </summary>
        /// <param name="job"></param>
        public void JobCompleted(IJob job)
        {
            this.Log.DebugCtx(LOG_CTX, "AutomationWorkerConsumer::JobCompleted()");

            // The automation service will handle any job properties.
            this.AssignedJob = Guid.Empty;
        }

        /// <summary>
        /// 
        /// </summary>
        public void Shutdown(ShutdownType shutdownType)
        {
            this.Log.DebugCtx(LOG_CTX, "AutomationWorkerAdminConsumer::Shutdown()");
            try
            {
                using (Clients.AutomationWorkerAdminServiceClient client = this.ConnectAdmin())
                {
                    try
                    {
                        client.Shutdown(shutdownType);
                    }
                    catch (Exception ex)
                    {
                        this.Log.ToolException(ex, "Error during shutdown request.");
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.ToolException(ex, "Error disposing Automation Worker Service worker.");
            }
        }

        /// <summary>
        /// Purge a client's cache
        /// </summary>
        public void FlagClearCache()
        {
            this.Log.DebugCtx(LOG_CTX, "AutomationWorkerServiceConsumer::FlagClearCache()");
            try
            {
                using (Clients.AutomationWorkerAdminServiceClient client = this.ConnectAdmin())
                {
                    try
                    {
                        client.FlagClearCache();
                    }
                    catch (Exception ex)
                    {
                        this.Log.ToolException(ex, "Error clearing cache.");
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.ToolException(ex, "Error disposing Automation Worker Service worker.");
            }
        }


        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Resolves the host of a uri and converts it to an IP address via a DNS lookup.
        /// </summary>
        /// <param name="uri"></param>
        /// <returns></returns>
        private Uri ResolveUri(Uri uri)
        {
            IPAddress[] addresslist = Dns.GetHostAddresses(uri.DnsSafeHost);
            if (addresslist.Length < 1)
            {
                throw new ArgumentException("Unable to resolve the service connection's IP.");
            }

            UriBuilder builder = new UriBuilder(uri);
            builder.Host = addresslist[0].ToString();
            return builder.Uri;
        }

        /// <summary>
        /// Create net.tcp connection to our service; return WCF worker.
        /// </summary>
        /// <returns></returns>
        private Clients.AutomationWorkerServiceClient Connect()
        {
            // Connect to automation service.
            this.Log.Message("Connecting: {0}.", this._unresolvedUri);
            EndpointAddress address = new EndpointAddress(this._resolvedUri);
            NetTcpBinding binding = new NetTcpBinding();
            binding.ReceiveTimeout = new TimeSpan(0, 30, 0);
            binding.SendTimeout = new TimeSpan(0, 30, 0);
            binding.MaxReceivedMessageSize = 100 * 1024 * 1024;
            binding.MaxBufferPoolSize = 100 * 1024 * 1024;
            binding.ReaderQuotas.MaxStringContentLength = 100 * 1024 * 1024;
            binding.ReliableSession.Enabled = false;
            binding.ReliableSession.InactivityTimeout = TimeSpan.MaxValue;

            Clients.AutomationWorkerServiceClient client = null;
            try
            {
                client = new Clients.AutomationWorkerServiceClient(binding, address);
                client.Open();
                return (client);
            }
            catch (Exception ex)
            {
                this.Log.ToolException(ex, "Error connecting to Automation Worker service.");
                return (null);
            }
        }

        /// <summary>
        /// Create net.tcp connection to our service; return WCF worker.
        /// </summary>
        /// <returns></returns>
        private Clients.AutomationWorkerAdminServiceClient ConnectAdmin()
        {
            // Connect to automation service.
            this.Log.Message("Connecting: {0}.", this._unresolvedUri);
            EndpointAddress address = new EndpointAddress(this._resolvedUri);
            NetTcpBinding binding = new NetTcpBinding();
            binding.ReceiveTimeout = new TimeSpan(0, 30, 0);
            binding.SendTimeout = new TimeSpan(0, 30, 0);
            binding.MaxReceivedMessageSize = 100 * 1024 * 1024;
            binding.MaxBufferPoolSize = 100 * 1024 * 1024;
            binding.ReaderQuotas.MaxStringContentLength = 100 * 1024 * 1024;
            binding.ReliableSession.Enabled = false;
            binding.ReliableSession.InactivityTimeout = TimeSpan.MaxValue;

            Clients.AutomationWorkerAdminServiceClient client = null;
            try
            {
                client = new Clients.AutomationWorkerAdminServiceClient(binding, address);
                client.Open();
                return (client);
            }
            catch (Exception ex)
            {
                this.Log.ToolException(ex, "Error connecting to Automation Worker Admin Service.");
                return (null);
            }
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Automation.Consumers namespace
