﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Services;

namespace RSG.Pipeline.Automation.Consumers.Clients
{

    /// <summary>
    /// Automation Service WCF client (consumers should be used externally).
    /// </summary>
    internal sealed class AutomationServiceClient : 
        ClientBase<IAutomationService>, 
        IAutomationService
    {
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="binding"></param>
        /// <param name="remoteAddress"></param>    
        public AutomationServiceClient(System.ServiceModel.Channels.Binding binding, EndpointAddress remoteAddress) 
            : base(binding, remoteAddress) 
        {
        }
        #endregion // Constructor(s)

        #region IAutomationService Interface Methods
        /// <summary>
        /// Register worker to the automation service; providing information 
        /// about what the worker's capabilities are.
        /// </summary>
        /// <param name="workerConnection"></param>
        /// <param name="capability"></param>
        public Guid RegisterWorker(String workerConnection, Capability capability)
        {
            return (base.Channel.RegisterWorker(workerConnection, capability));
        }

        /// <summary>
        /// Unregister worker from automation service; worker is being stopped,
        /// killed or shutdown.
        /// </summary>
        /// this.m_Workers.Add(worker);<param name="id">
        /// </param>
        public void UnregisterWorker(Guid id)
        {
            base.Channel.UnregisterWorker(id);
        }

        /// <summary>
        /// Queries to see if a worker with a given ID is registered
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool IsWorkerRegistered(Guid id)
        {
            return (base.Channel.IsWorkerRegistered(id));
        }

        /// <summary>
        /// Externally enqueue a job onto a task queue; based on the job role
        /// and priority.  This should be used for user-invoked rebuilds.
        /// </summary>
        /// <param name="job"></param>
        /// <returns>true iff successfully enqueued; false otherwise.</returns>
        public bool EnqueueJob(IJob job)
        {
            return (base.Channel.EnqueueJob(job));
        }

        /// <summary>
        /// Worker notification to automation service that the job was completed.
        /// </summary>
        /// <param name="job"></param>
        public void JobCompleted(IJob job, IEnumerable<IJobResult> jobResults)
        {
            base.Channel.JobCompleted(job, jobResults);
        }
        #endregion // IAutomationService Interface Methods
    }

} // RSG.Pipeline.Automation.Consumers.Clients
