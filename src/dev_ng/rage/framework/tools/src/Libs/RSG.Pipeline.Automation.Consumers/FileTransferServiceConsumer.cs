using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.ServiceModel;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Services.Messages;
using RSG.Pipeline.Automation.Common.Tasks;

namespace RSG.Pipeline.Automation.Consumers
{

    /// <summary>
    /// File Transfer Service Consumer; wrapper around the WCF client to ensure
    /// it is short-lived.
    /// </summary>
    public class FileTransferServiceConsumer
    {
        #region Constants
        /// <summary>
        /// Log context.
        /// </summary>
        private static readonly String LOG_CTX = "File Transfer Service Consumer";
        #endregion // Constants

        #region Member Data
        /// <summary>
        /// Configuration data.
        /// </summary>
        private readonly IConfig Config;

        /// <summary>
        /// Universal log.
        /// </summary>
        private readonly IUniversalLog Log;

        /// <summary>
        /// Uri where the host name hasn't been resolved to an IP address.
        /// </summary>
        private readonly Uri _unresolvedUri;

        /// <summary>
        /// Uri where the host name has been resolved to an IP address.
        /// </summary>
        private readonly Uri _resolvedUri;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="config"></param>
        /// <param name="configService"></param>
        public FileTransferServiceConsumer(IConfig config, Uri serviceConnection)
        {
            this._unresolvedUri = serviceConnection;
            this._resolvedUri = ResolveUri(serviceConnection);

            this.Config = config;
            LogFactory.Initialize();
            this.Log = LogFactory.CreateUniversalLog(LOG_CTX);
        }
        #endregion // Constructor(s)

        // Controller methods here wrap the IFileTransferService methods; 
        // hiding the details of the WCF services.  This allows our WCF service 
        // channels to be short-lived.
        #region Controller Methods
        /// <summary>
        /// Upload files for a job; used from the submission user's machine
        /// to upload local DCC source files to the ServerHost.
        /// </summary>
        /// <param name="job"></param>
        /// <param name="localFilename"></param>
        /// <param name="remoteFilename"></param>
        /// <returns></returns>
        public bool UploadFile(IJob job, String localFilename, String remoteFilename = "")
        {
            if (String.Empty.Equals(remoteFilename))
                remoteFilename = Path.GetFileName(localFilename);

            Debug.Assert(!Path.IsPathRooted(remoteFilename),
                "'remoteFilename' must be relative to automation Job directory.");

            if (Path.IsPathRooted(remoteFilename))
            {
                this.Log.ErrorCtx(LOG_CTX, "'remoteFilename' for File Transfer Consumer UploadFile is rooted ({0}).  Transfer aborted.",
                    remoteFilename);
                return (false);
            }

            this.Log.DebugCtx(LOG_CTX, "FileTransferServiceConsumer::UploadFile({0}, {1})",
                job.ID, localFilename);
            bool result = true;
            try
            {
                if (!File.Exists(localFilename))
                {
                    this.Log.ErrorCtx(LOG_CTX, "File {0} does not exist to upload.",
                        localFilename);
                    result = false;
                }
                else
                {
                    using (Clients.FileTransferServiceClient client = this.Connect())
                    {
                        try
                        {
                            FileInfo fi = new FileInfo(localFilename);
                            
                            using (FileStream fs = new FileStream(localFilename,
                                FileMode.Open, FileAccess.Read))
                            {
                                Byte[] hash = RSG.Base.IO.FileMD5.ComputeMD5(fs);
                                this.Log.MessageCtx(LOG_CTX, "Uploading {0} to {1}.  MD5: {2}.",
                                    localFilename, remoteFilename, RSG.Base.IO.FileMD5.Format(hash));

                                RemoteFile rmf = new RemoteFile()
                                {
                                    Filename = remoteFilename,
                                    Length = fi.Length,
                                    Hash = hash,
                                    Job = job.ID,
                                    Stream = fs
                                };
                                TransferResponse tr = client.UploadFile(rmf);
                                result = tr.Successful;
                            }
                        }
                        catch (Exception ex)
                        {
                            this.Log.ToolException(ex, "Error uploading file: {0}.",
                                localFilename);
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.ToolException(ex, "Error disposing File Transfer Service client.");
                result = false;
            }
            return (result);
        }
        
        /// <summary>
        /// Download a file from a job; used for downloading the export and
        /// build data to the user's machine.
        /// </summary>
        /// <param name="job">Job ID associated with file download.</param>
        /// <param name="remoteFilename">Filename to download.</param>
        /// <param name="localFilename"></param>
        /// <param name="md5"></param>
        /// <returns></returns>
        public bool DownloadFile(IJob job, String remoteFilename, String localFilename, out Byte[] md5)
        {
            Debug.Assert(!Path.IsPathRooted(remoteFilename), 
                "'remoteFilename' must be relative to automation Job directory.");
            Debug.Assert(!Path.IsPathRooted(localFilename), 
                "'localFilename' must be relative to automation Job directory.");
            if (Path.IsPathRooted(remoteFilename))
            {
                this.Log.ErrorCtx(LOG_CTX, "'remoteFilename' for File Transfer Consumer DownloadFile is rooted ({0}).  Transfer aborted.",
                    remoteFilename);
                md5 = RSG.Base.IO.FileMD5.Empty;
                return (false);
            }
            if (Path.IsPathRooted(localFilename))
            {
                this.Log.ErrorCtx(LOG_CTX, "'localFilename' for File Transfer Consumer DownloadFile is rooted ({0}).  Transfer aborted.",
                    localFilename);
                md5 = RSG.Base.IO.FileMD5.Empty;
                return (false);
            }

            md5 = RSG.Base.IO.FileMD5.Empty;
            this.Log.MessageCtx(LOG_CTX, "FileTransferServiceConsumer::DownloadFile({0}, {1})",
                job.ID, remoteFilename);
            bool result = true;
            try
            {
                using (Clients.FileTransferServiceClient client = this.Connect())
                {
                    String jobDownloadDirectory = this.GetClientFileDirectory(job);
                    String destinationFilename = Path.Combine(jobDownloadDirectory, localFilename);

                    try
                    {
                        int bufferSize = 65535;
                        Byte[] buffer = new Byte[bufferSize];

                        RemoteFileRequest rmfreq = new RemoteFileRequest()
                        {
                            Filename = remoteFilename,
                            Job = job.ID
                        };
                        RemoteFile rmf = client.DownloadFile(rmfreq);

                        if (!Directory.Exists(jobDownloadDirectory))
                            Directory.CreateDirectory(jobDownloadDirectory);
                        
                        using (FileStream fs = new FileStream(destinationFilename,
                            FileMode.Create, FileAccess.Write, FileShare.None))
                        {
                            int bytesRead = rmf.Stream.Read(buffer, 0, buffer.Length);
                            while (bytesRead > 0)
                            {
                                fs.Write(buffer, 0, bytesRead);

                                bytesRead = rmf.Stream.Read(buffer, 0, buffer.Length);
                            }
                        }

                        // Verify file size and MD5 hash.
                        FileInfo fi = new FileInfo(destinationFilename);
                        Debug.Assert(fi.Length == rmf.Length, 
                            "Downloaded file remote and local sizes do not match.");
                        if (fi.Length != rmf.Length)
                        {
                            this.Log.ErrorCtx(LOG_CTX, "Downloaded file remote and local sizes to not match.  File will be deleted.");
                            fi.Delete();
                            result = false;
                        }
                        else
                        {
                            // Verify MD5 hash; by recalculating it and comparing it to the
                            // RemoteFile request one.
                            md5 = RSG.Base.IO.FileMD5.ComputeMD5(destinationFilename);
                            if (!RSG.Base.IO.FileMD5.Equal(rmf.Hash, md5))
                            {
                                Log.ErrorCtx(LOG_CTX, "MD5 hash comparison failure: server: {0}, local: {1}.  File will be deleted.",
                                    RSG.Base.IO.FileMD5.Format(rmf.Hash), RSG.Base.IO.FileMD5.Format(md5));
                                fi.Delete();
                                result = false;
                            }
                            else
                            {
                                this.Log.MessageCtx(LOG_CTX, "MD5 hash match: {0}.",
                                    RSG.Base.IO.FileMD5.Format(md5));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        this.Log.ToolException(ex, "Error downloading file: {0} to {1}.",
                            remoteFilename, destinationFilename);
                        result = false;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.ToolException(ex, "Error disposing File Transfer Service client.");
            }
            return (result);
        }

        /// <summary>
        /// Read information about the available files.
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        public IEnumerable<String> FileAvailability(IJob job)
        {
            this.Log.DebugCtx(LOG_CTX, "FileTransferServiceConsumer::FileAvailbility({0})",
                job.ID);
            try
            {
                using (Clients.FileTransferServiceClient client = this.Connect())
                {
                    try
                    {
                        RemoteFileAvailabilityRequest rmf = new RemoteFileAvailabilityRequest(job.ID);
                        RemoteFileAvailability rmfresult = client.FileAvailability(rmf);
                        return (rmfresult.Filenames);
                    }
                    catch (Exception ex)
                    {
                        this.Log.ToolException(ex, "Error reading file availability.");
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.ToolException(ex, "Error disposing File Transfer Service client.");
            }
            return (new String[] { });
        }

        /// <summary>
        /// Read information about the available files.
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        public IEnumerable<RemoteFileInfo> FileAvailability2(IJob job)
        {
            this.Log.DebugCtx(LOG_CTX, "FileTransferServiceConsumer::FileAvailbility2({0})",
                job.ID);
            try
            {
                using (Clients.FileTransferServiceClient client = this.Connect())
                {
                    try
                    {
                        RemoteFileAvailabilityRequest rmf = new RemoteFileAvailabilityRequest(job.ID);
                        RemoteFileAvailability2 rmfresult = client.FileAvailability2(rmf);
                        return (rmfresult.Files);
                    }
                    catch (Exception ex)
                    {
                        this.Log.ToolException(ex, "Error reading file availability.");
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.ToolException(ex, "Error disposing File Transfer Service client.");
            }
            return (new RemoteFileInfo[] { });
        }

        /// <summary>
        /// Return directory used for files transferred to client.
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        public String GetClientFileDirectory(IJob job)
        {
            this.Log.DebugCtx(LOG_CTX, "FileTransferServiceConsumer::GetClientFileDirectory({0})",
                job.ID);
            try
            {
                using (Clients.FileTransferServiceClient client = this.Connect())
                {
                    try
                    {
                        String directory = client.GetClientFileDirectory(job);
                        return (directory);
                    }
                    catch (Exception ex)
                    {
                        this.Log.ToolException(ex, "Error reading file availability.");
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.ToolException(ex, "Error disposing File Transfer Service client.");
            }
            return (null);
        }

        /// <summary>
        /// Return the name of the service's host
        /// </summary>
        /// <returns>Name</returns>
        public String GetClientHostName()
        {
            return this._unresolvedUri.Host;
        }

        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Resolves the host of a uri and converts it to an IP address via a DNS lookup.
        /// </summary>
        /// <param name="uri"></param>
        /// <returns></returns>
        private Uri ResolveUri(Uri uri)
        {
            IPAddress[] addresslist = Dns.GetHostAddresses(uri.DnsSafeHost);
            if (addresslist.Length < 1)
            {
                throw new ArgumentException("Unable to resolve the service connection's IP.");
            }

            UriBuilder builder = new UriBuilder(uri);
            builder.Host = addresslist[0].ToString();
            return builder.Uri;
        }

        /// <summary>
        /// Create tcp.net connection to our File Transfer service; 
        /// return WCF client.
        /// </summary>
        /// <returns></returns>
        private Clients.FileTransferServiceClient Connect()
        {
            // Connect to automation service.
            this.Log.MessageCtx(LOG_CTX, "Connecting: {0}.", this._unresolvedUri);
            EndpointAddress address = new EndpointAddress(this._resolvedUri);
            NetTcpBinding binding = new NetTcpBinding();
            binding.ReceiveTimeout = new TimeSpan(0, 30, 0);
            binding.SendTimeout = new TimeSpan(0, 30, 0);
            binding.OpenTimeout = new TimeSpan(0, 0, 30);
            binding.CloseTimeout = new TimeSpan(0, 0, 30);
            binding.MaxReceivedMessageSize = Int32.MaxValue; // 2 * 1024 * 1024 * 1024;
            binding.ReaderQuotas.MaxStringContentLength = Int32.MaxValue;
            binding.TransferMode = TransferMode.Streamed;

            Clients.FileTransferServiceClient client = null;
            try
            {
                client = new Clients.FileTransferServiceClient(binding, address);
                client.Open();
                return (client);
            }
            catch (Exception ex)
            {
                this.Log.ToolException(ex, "Error connecting to File Transfer Service.");
                return (null);
            }
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Automation.Consumers namespace
