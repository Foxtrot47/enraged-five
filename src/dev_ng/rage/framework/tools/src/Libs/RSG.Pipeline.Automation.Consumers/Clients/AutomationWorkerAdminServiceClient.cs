﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using RSG.Pipeline.Automation.Common.Services;
using RSG.Pipeline.Automation.Common.Services.Messages;

namespace RSG.Pipeline.Automation.Consumers.Clients
{

    /// <summary>
    /// Automation Worker Admin Service WCF client (consumers should be used externally).
    /// </summary>
    internal sealed class AutomationWorkerAdminServiceClient :
        ClientBase<IAutomationWorkerAdminService>,
        IAutomationWorkerAdminService
    {
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="binding"></param>
        /// <param name="remoteAddress"></param>    
        public AutomationWorkerAdminServiceClient(System.ServiceModel.Channels.Binding binding, EndpointAddress remoteAddress) 
            : base(binding, remoteAddress) 
        {
        }
        #endregion // Constructor(s)

        #region IAutomationWorkerAdminService Interface Methods
        /// <summary>
        /// 
        /// </summary>
        public void Shutdown(ShutdownType shutdownType)
        {
            base.Channel.Shutdown(shutdownType);
        }

        /// <summary>
        /// 
        /// </summary>
        public void FlagClearCache()
        {
            base.Channel.FlagClearCache();
        }
        #endregion // IAutomationWorkerAdminService Interface Methods
    }

} // RSG.Pipeline.Automation.Consumers.Clients namespace
