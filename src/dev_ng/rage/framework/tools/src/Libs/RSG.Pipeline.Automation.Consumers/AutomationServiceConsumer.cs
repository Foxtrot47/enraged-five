﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Threading;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Tasks;

namespace RSG.Pipeline.Automation.Consumers
{

    /// <summary>
    /// AutomationService consumer class.
    /// </summary>
    public class AutomationServiceConsumer
    {
        #region Constants
        /// <summary>
        /// Log context.
        /// </summary>
        private static readonly String LOG_CTX = "Automation Service Consumer";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Service connection string (e.g. "tcp.net://ediw-dmuir3:7000/automation.svc")
        /// </summary>
        public Uri ServiceConnection
        {
            get { return _unresolvedUri; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Guid ID
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Log.
        /// </summary>
        private IUniversalLog Log;

        /// <summary>
        /// Uri where the host name hasn't been resolved to an IP address.
        /// </summary>
        private readonly Uri _unresolvedUri;

        /// <summary>
        /// Uri where the host name has been resolved to an IP address.
        /// </summary>
        private readonly Uri _resolvedUri;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="uri"></param>
        public AutomationServiceConsumer(Uri serviceConnection)
        {
#warning DHM FIX ME: service string validation?
#if false
            Debug.Assert(service.StartsWith("net.tcp://"),
                "Only support net.tcp protocol for automation service.");
            if (!service.StartsWith("net.tcp://"))
                throw new NotSupportedException("Only support net.tcp protocol for automation service.");
            
#endif
            this._unresolvedUri = serviceConnection;
            this._resolvedUri = ResolveUri(serviceConnection);
            LogFactory.Initialize();
            this.Log = LogFactory.CreateUniversalLog(LOG_CTX);
        }
        #endregion // Constructor(s)

        // Controller methods here wrap the IAutomationService and 
        // IAutomationAdminService methods; hiding the details of the WCF 
        // services.  This allows our WCF service channels to be short-lived.
        #region Controller Methods
        /// <summary>
        /// Register worker to the automation service; providing information 
        /// about what the worker's capabilities are.
        /// </summary>
        /// <param name="capability"></param>
        /// <returns></returns>
        public Guid RegisterWorker(Capability capability)
        {
            this.Log.DebugCtx(LOG_CTX, "AutomationServiceConsumer::RegisterWorker({0})",
                capability.ToString());
            try
            {
                using (Clients.AutomationServiceClient client =
                    this.ConnectAutomationService())
                {
                    if (client != null)
                    {
                        try
                        {
                            String service = String.Format("net.tcp://{0}:7001/automation_worker.svc",
                                Environment.MachineName);
                            this.ID = client.RegisterWorker(service, capability);
                            this.Log.MessageCtx(LOG_CTX,
                                "Registered worker - service = '{0}' - capability.Type = '{1}' - ID = '{2}'",
                                service, capability.Type.ToString(), this.ID.ToString());
                            return (this.ID);
                        }
                        catch (Exception ex)
                        {
                            this.Log.ToolException(ex, "Error registering worker.");
                        }
                        finally
                        {
                            client.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.ToolException(ex, "Error disposing Automation Service client.");
            }
            return (Guid.Empty);
        }

        /// <summary>
        /// Unregister worker from automation service; worker is being stopped,
        /// killed or shutdown.
        /// </summary>
        public void UnregisterWorker()
        {
            UnregisterWorker(this.ID);
        }

        /// <summary>
        /// Unregister worker from automation service; worker is being stopped,
        /// killed or shutdown.
        /// </summary>
        public void UnregisterWorker(Guid workerID)
        {
            this.Log.DebugCtx(LOG_CTX, "AutomationServiceConsumer::UnregisterWorker({0})", workerID.ToString());
            try
            {
                using (Clients.AutomationServiceClient client =
                    this.ConnectAutomationService())
                {
                    try
                    {
                        client.UnregisterWorker(workerID);
                        this.Log.MessageCtx(LOG_CTX, "Unregistered worker - ID = '{0}'", workerID.ToString());
                    }
                    catch (Exception ex)
                    {
                        this.Log.ToolException(ex, "Error unregistering worker.");
                    }
                    finally
                    {
                        client.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.ToolException(ex, "Error disposing Automation Service client.");
            }
        }

        /// <summary>
        /// Queries to see if a worker with a given ID is registered
        /// </summary>
        public bool IsWorkerRegistered()
        {
            return IsWorkerRegistered(this.ID);
        }

        /// <summary>
        /// Queries to see if a worker with a given ID is registered
        /// </summary>
        public bool IsWorkerRegistered(Guid workerID)
        {
            this.Log.DebugCtx(LOG_CTX, "AutomationServiceConsumer::IsWorkerRegistered({0})", 
                workerID.ToString());
            try
            {
                using (Clients.AutomationServiceClient client =
                    this.ConnectAutomationService())
                {
                    try
                    {
                        return client.IsWorkerRegistered(workerID);
                    }
                    catch (Exception ex)
                    {
                        this.Log.ToolException(ex, "Error checking worker registration.");
                    }
                    finally
                    {
                        client.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.ToolException(ex, "Error disposing Automation Service client.");
            }

            return false;
        }

        /// <summary>
        /// Externally enqueue a job onto a task queue; based on the job role
        /// and priority.  This should be used for user-invoked rebuilds.
        /// </summary>
        /// <param name="job"></param>
        /// <returns>true iff successfully enqueued; false otherwise.</returns>
        public bool EnqueueJob(IJob job)
        {
            this.Log.DebugCtx(LOG_CTX, "AutomationServiceConsumer::EnqueueJob({0})",
                job.ID);
            try
            {
                using (Clients.AutomationServiceClient client =
                    this.ConnectAutomationService())
                {
                    try
                    {
                        bool result = client.EnqueueJob(job);
                        return (result);
                    }
                    catch (Exception ex)
                    {
                        this.Log.ToolException(ex, "Error enqueuing job.");
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.ToolException(ex, "Error disposing Automation Service consumer.");
            }
            return (false);
        }

        /// <summary>
        /// Worker notification to automation service that the job was completed.
        /// </summary>
        /// <param name="job"></param>
        public void JobCompleted(IJob job, IEnumerable<IJobResult> jobResults)
        {
            this.Log.DebugCtx(LOG_CTX, "AutomationServiceConsumer::JobCompleted({0})",
                job.ID);
            try
            {
                using (Clients.AutomationServiceClient client =
                    this.ConnectAutomationService())
                {
                    try
                    {
                        this.Log.MessageCtx(LOG_CTX, "Starting {0} job complete process", job.ID.ToString());
                        client.JobCompleted(job, jobResults.ToArray());
                        this.Log.MessageCtx(LOG_CTX, "{0} job complete processed", job.ID.ToString());
                    }
                    catch (Exception ex)
                    {
                        this.Log.ToolException(ex, "Error marking job complete.");
                    }
                    finally
                    {
                        client.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.ToolException(ex, "Error disposing Automation Service client.");
            }
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Resolves the host of a uri and converts it to an IP address via a DNS lookup.
        /// </summary>
        /// <param name="uri"></param>
        /// <returns></returns>
        private Uri ResolveUri(Uri uri)
        {
            IPAddress[] addresslist = Dns.GetHostAddresses(uri.DnsSafeHost);
            if (addresslist.Length < 1)
            {
                throw new ArgumentException("Unable to resolve the service connection's IP.");
            }

            UriBuilder builder = new UriBuilder(uri);
            builder.Host = addresslist[0].ToString();
            return builder.Uri;
        }

        /// <summary>
        /// Create tcp.net connection to our Automation Service; return WCF client.
        /// </summary>
        /// <returns></returns>
        private Clients.AutomationServiceClient ConnectAutomationService()
        {
            // Connect to automation service.
            this.Log.MessageCtx(LOG_CTX, "Connecting: {0}.", this._unresolvedUri);
            EndpointAddress address = new EndpointAddress(this._resolvedUri);
            NetTcpBinding binding = new NetTcpBinding();
            binding.ReceiveTimeout = new TimeSpan(0, 30, 0);
            binding.SendTimeout = new TimeSpan(0, 30, 0);
            binding.OpenTimeout = new TimeSpan(0, 0, 30);
            binding.CloseTimeout = new TimeSpan(0, 0, 30);
            binding.MaxReceivedMessageSize = 100 * 1024 * 1024;
            binding.MaxBufferPoolSize = 100 * 1024 * 1024;
            binding.ReaderQuotas.MaxStringContentLength = 100 * 1024 * 1024;
            binding.ReliableSession.Enabled = false;
            binding.ReliableSession.InactivityTimeout = TimeSpan.MaxValue;

            Clients.AutomationServiceClient client = null;
            try
            {
                int triesRemaining = 100;

                while (triesRemaining > 0)
                {
                    try
                    {
                        client = new Clients.AutomationServiceClient(binding, address);
                        client.Open();

                        if (client != null)
                            break;
                    }
                    catch (Exception ex)
                    {
                        triesRemaining--;

                        if (triesRemaining == 0)
                            throw ex;

                        int sleepSeconds = 10;
                        this.Log.Warning("Opening Automation Service failed - retrying in {0}s ({1} retries remaining)", sleepSeconds, triesRemaining);
                        Thread.Sleep(1000 * sleepSeconds);
                    }
                }
                return (client);
            }
            catch (Exception ex)
            {
                this.Log.ToolException(ex, "Error connecting to Automation Service.");
                return (null);
            }
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Automation.Consumers namespace
