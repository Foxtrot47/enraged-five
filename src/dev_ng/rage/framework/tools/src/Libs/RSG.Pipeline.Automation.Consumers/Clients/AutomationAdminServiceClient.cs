﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Xml;
using RSG.Pipeline.Automation.Common.Services;
using RSG.Pipeline.Automation.Common.Services.Messages;
using RSG.Pipeline.Automation.Common.Tasks;
using RSG.Pipeline.Automation.Common.Client;
using RSG.Pipeline.Automation.Common;

namespace RSG.Pipeline.Automation.Consumers.Clients
{

    /// <summary>
    /// Automation Admin Service WCF client (consumers should be used externally).
    /// </summary>
    internal sealed class AutomationAdminServiceClient
        : ClientBase<IAutomationAdminService>,
        IAutomationAdminService
    {
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="binding"></param>
        /// <param name="remoteAddress"></param>    
        public AutomationAdminServiceClient(System.ServiceModel.Channels.Binding binding, EndpointAddress remoteAddress) 
            : base(binding, remoteAddress) 
        {
        }
        #endregion // Constructor(s)

        #region IAutomationAdminService Interface Methods
        /// <summary>
        /// Return status objects for tasks that are part of this automation.
        /// </summary>
        /// <returns></returns>
        public TaskStatus[] Monitor()
        {
            return (base.Channel.Monitor());
        }

        /// <summary>
        /// Return status objects for tasks that are part of this automation (filtered).
        /// service.
        /// </summary>
        /// <param name="role">Role filter</param>
        /// <param name="start">Start index</param>
        /// <param name="count">Count</param>
        /// <returns></returns>
        /// <seealso cref="MonitorFilteredCount"/>
        public TaskStatus[] MonitorFiltered(CapabilityType role, int start, int count)
        {
            return (base.Channel.MonitorFiltered(role, start, count));
        }

        /// <summary>
        /// Return job count for a particular task (used for paginating MonitorFiltered).
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public int MonitorFilteredCount(CapabilityType role)
        {
            return (base.Channel.MonitorFilteredCount(role));
        }

        /// <summary>
        /// Return status objects for all the clients that are part of this
        /// automation service.
        /// </summary>
        /// <returns></returns>
        public WorkerStatus[] Clients()
        {
            return (base.Channel.Clients());
        }

        /// <summary>
        /// Request to skip a job.
        /// </summary>
        /// <param name="changelist"></param>
        /// <returns></returns>
        public JobResponse SkipJob(Guid id)
        {
            return (base.Channel.SkipJob(id));
        }

        /// <summary>
        /// Request to prioritise a job.
        /// </summary>
        /// <param name="changelist"></param>
        /// <returns></returns>
        public JobResponse PrioritiseJob(Guid id)
        {
            return (base.Channel.PrioritiseJob(id));
        }

        /// <summary>
        /// Request to reprocess a job.
        /// </summary>
        /// <param name="changelist"></param>
        /// <returns></returns>
        public JobResponse ReprocessJob(Guid id)
        {
            return (base.Channel.ReprocessJob(id));
        }

        /// <summary>
        /// Return dictionary of int => String for a particular enum type.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        /// This service is required because the Microsoft JSON serialiser 
        /// doesn't serialise the String representation; which we typically
        /// want for friendly names etc on our pretty webpages.
        public IDictionary<int, String> EnumValues(String type)
        {
            return (base.Channel.EnumValues(type));
        }

        /// <summary>
        /// Return total message count on coordinator log.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public Int32 CoordinatorLogMessageCount()
        {
            return (base.Channel.CoordinatorLogMessageCount());
        }

        /// <summary>
        /// Return messages starting at startindx through to messageCount from coordinator log.
        /// </summary>
        /// <returns></returns>
        public XmlDocument CoordinatorLog(Int32 startIdx = 0, Int32 messageCount = 0)
        {
            return (base.Channel.CoordinatorLog(startIdx, messageCount));
        }

        /// <summary>
        /// Request for clear cache on all workers.
        /// </summary>
        public void ClearAllWorkersCache()
        {
            base.Channel.ClearAllWorkersCache();
        }

        /// <summary>
        /// Request for shutdown.
        /// </summary>
        public void Shutdown(ShutdownType shutdownType, String reason)
        {
            base.Channel.Shutdown(shutdownType, reason);
        }

        /// <summary>
        /// Set Task Parameters
        /// </summary>
        public void SetTaskParam(String taskName, String paramName, String paramValue)
        {
            base.Channel.SetTaskParam(taskName, paramName, paramValue);
        }

        /// <summary>
        /// Start task
        /// </summary>
        public void StartTask(String taskName)
        {
            base.Channel.StartTask(taskName);
        }

        /// <summary>
        /// Stop task
        /// </summary>
        public void StopTask(String taskName)
        {
            base.Channel.StopTask(taskName);
        }

        /// <summary>
        /// Get Task Parameters
        /// </summary>
        public IDictionary<String, Object> GetTaskParams(String taskName)
        {
            return (base.Channel.GetTaskParams(taskName));
        }

        /// <summary>
        /// Set the client update tick rate
        /// </summary>
        /// <param name="taskMs"></param>
        public void SetClientUpdateTick(Int32 tickMs)
        {
            base.Channel.SetClientUpdateTick(tickMs);
        }

        /// <summary>
        /// Set the job's dispatch host
        /// </summary>
        /// <param name="jobId"></param>
        /// <param name="host"></param>
        public void SetJobDispatch(Guid jobId, String host)
        {
            base.Channel.SetJobDispatch(jobId, host);
        }
        #endregion // IAutomationAdminService Interface Methods
    }

} // RSG.Pipeline.Automation.Consumers.Clients namespace
