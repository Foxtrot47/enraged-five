﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Client;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Tasks;
using RSG.Pipeline.Automation.Common.Services.Messages;

namespace RSG.Pipeline.Automation.Consumers
{

    /// <summary>
    /// AutomationAdminService consumer class.
    /// </summary>
    public class AutomationAdminConsumer
    {
        #region Constants
        /// <summary>
        /// Log context.
        /// </summary>
        private static readonly String LOG_CTX = "Automation Admin Service Consumer";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Service connection string (e.g. "net.tcp://ediw-dmuir3:7000/automation.svc")
        /// </summary>
        public Uri ServiceConnection
        {
            get { return _unresolvedUri; }
        }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Log.
        /// </summary>
        private readonly IUniversalLog Log;

        /// <summary>
        /// Uri where the host name hasn't been resolved to an IP address.
        /// </summary>
        private readonly Uri _unresolvedUri;

        /// <summary>
        /// Uri where the host name has been resolved to an IP address.
        /// </summary>
        private readonly Uri _resolvedUri;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="uri"></param>
        public AutomationAdminConsumer(Uri serviceConnection)
        {
            this._unresolvedUri = serviceConnection;
            this._resolvedUri = ResolveUri(serviceConnection);
            LogFactory.Initialize();
            this.Log = LogFactory.CreateUniversalLog(LOG_CTX);
        }
        #endregion // Constructor(s)

        // Controller methods here wrap the IAutomationService and 
        // IAutomationAdminService methods; hiding the details of the WCF 
        // services.  This allows our WCF service channels to be short-lived.
        #region Controller Methods
        /// <summary>
        /// Return status objects for all tasks that are part of this automation
        /// service.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TaskStatus> Monitor()
        {
            try
            {
                using (Clients.AutomationAdminServiceClient client =
                    this.ConnectAutomationAdminService())
                {
                    try
                    {
                        TaskStatus[] monitor = client.Monitor();
                        return (monitor);
                    }
                    catch (Exception ex)
                    {
                        this.Log.ToolException(ex, "Error fetching Automation Service task status.");
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.ToolException(ex, "Error disposing Automation Admin Service consumer.");
            }
            return (new TaskStatus[] { });
        }

        /// <summary>
        /// Return status objects for filtered tasks that are part of this automation
        /// </summary>
        /// <param name="role">Filter of role across tasks</param>
        /// <param name="startJob">Starting job index to begin returning jobs from ( pagination )</param>
        /// <param name="countJob">Max count of jobs to return in the task status</param>
        /// <returns></returns>
        public IEnumerable<TaskStatus> MonitorFiltered(CapabilityType role, int startJob, int countJob)
        {
            try
            {
                using (Clients.AutomationAdminServiceClient client =
                    this.ConnectAutomationAdminService())
                {
                    try
                    {
                        TaskStatus[] monitor = client.MonitorFiltered(role, startJob, countJob);
                        return (monitor);
                    }
                    catch (Exception ex)
                    {
                        this.Log.ToolException(ex, "Error fetching Automation Service task status.");
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.ToolException(ex, "Error disposing Automation Admin Service consumer.");
            }
            return (new TaskStatus[] { });
        }

        /// <summary>
        /// Return status objects for all clients connected to the automation
        /// service.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<WorkerStatus> Clients()
        {
            try
            {
                using (Clients.AutomationAdminServiceClient client =
                    this.ConnectAutomationAdminService())
                {
                    try
                    {
                        WorkerStatus[] clients = client.Clients();
                        return (clients);
                    }
                    catch (Exception ex)
                    {
                        this.Log.ToolException(ex, "Error fetching Automation Service task status.");
                    }
                }
            }
            catch (EndpointNotFoundException)
            {
                this.Log.ToolError("Could not connect to the end point.");
            }
            catch (Exception ex)
            {
                this.Log.ToolException(ex, "Error disposing Automation Admin Service consumer.");
            }
            return (new WorkerStatus[] { });
        }

        /// <summary>
        /// Trigger a shutdown of hosts and/or clients. 
        /// </summary>
        /// <param name="shutdownType">the type of shutdown</param>
        public void Shutdown(ShutdownType shutdownType, String reason)
        {
            try
            {
                using (Clients.AutomationAdminServiceClient client =
                    this.ConnectAutomationAdminService())
                {
                    try
                    {
                        client.Shutdown(shutdownType, reason);
                    }
                    catch (EndpointNotFoundException)
                    {
                        this.Log.ToolError("Could not connect to the end point.");
                    }
                    catch (Exception ex)
                    {
                        this.Log.ToolException(ex, "Error shutting down Automation Service");
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.ToolException(ex, "Error disposing Automation Admin Service consumer.");
            }
        }

        /// <summary>
        /// Clear Cache : Run on the server, goes through all workers and flags them to clear their cache.
        /// </summary>
        public void ClearAllWorkersCache()
        {
            try
            {
                using (Clients.AutomationAdminServiceClient client =
                    this.ConnectAutomationAdminService())
                {
                    try
                    {
                        client.ClearAllWorkersCache();
                    }
                    catch (EndpointNotFoundException)
                    {
                        this.Log.ToolError("Could not connect to the end point.");
                    }
                    catch (Exception ex)
                    {
                        this.Log.ToolException(ex, "Error shutting down Automation Service");
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.ToolException(ex, "Error disposing Automation Admin Service consumer.");
            }
        }

        /// <summary>
        /// Start a task
        /// </summary>
        /// <param name="taskName"></param>
        public void StartTask(String taskName)
        {
            try
            {
                using (Clients.AutomationAdminServiceClient client =
                    this.ConnectAutomationAdminService())
                {
                    try
                    {
                        client.StartTask(taskName);
                    }
                    catch (EndpointNotFoundException)
                    {
                        this.Log.ToolError("Could not connect to the end point.");
                    }
                    catch (Exception ex)
                    {
                        this.Log.ToolException(ex, "Error starting task {0}", taskName);
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.ToolException(ex, "Error disposing Automation Admin Service consumer.");
            }
        }

        /// <summary>
        /// Stop a task
        /// </summary>
        /// <param name="taskName"></param>
        public void StopTask(String taskName)
        {
            try
            {
                using (Clients.AutomationAdminServiceClient client =
                    this.ConnectAutomationAdminService())
                {
                    try
                    {
                        client.StopTask(taskName);
                    }
                    catch (EndpointNotFoundException)
                    {
                        this.Log.ToolError("Could not connect to the end point.");
                    }
                    catch (Exception ex)
                    {
                        this.Log.ToolException(ex, "Error stopping task {0}", taskName);
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.ToolException(ex, "Error disposing Automation Admin Service consumer.");
            }
        }


        /// <summary>
        /// Sets a parameter of a task.
        /// </summary>
        /// <param name="taskName"></param>
        /// <param name="paramName"></param>
        /// <param name="paramValue"></param>
        /// <returns></returns>
        public void SetTaskParam(String taskName, String paramName, String paramValue)
        {
            try
            {
                using (Clients.AutomationAdminServiceClient client =
                    this.ConnectAutomationAdminService())
                {
                    try
                    {
                        client.SetTaskParam(taskName, paramName, paramValue);
                    }
                    catch (EndpointNotFoundException)
                    {
                        this.Log.ToolError("Could not connect to the end point.");
                    }
                    catch (Exception ex)
                    {
                        this.Log.ToolException(ex, "Error setting task param {0} {1} {2}", taskName, paramName, paramValue);
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.ToolException(ex, "Error disposing Automation Admin Service consumer.");
            }
        }

        /// <summary>
        /// Gets parameters of a task.
        /// </summary>
        /// <param name="taskName"></param>
        /// <returns></returns>
        public IDictionary<String, Object> GetTaskParams(String taskName)
        {
            try
            {
                using (Clients.AutomationAdminServiceClient client =
                    this.ConnectAutomationAdminService())
                {
                    try
                    {
                        IDictionary<String, Object>  dict = client.GetTaskParams(taskName);
                        return dict;
                    }
                    catch (EndpointNotFoundException)
                    {
                        this.Log.ToolError("Could not connect to the end point.");
                    }
                    catch (Exception ex)
                    {
                        this.Log.ToolException(ex, "Error getting task params for task {0}", taskName);
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.ToolException(ex, "Error disposing Automation Admin Service consumer.");
            }

            return null;
        }

        /// <summary>
        /// Gets parameters of a task.
        /// </summary>
        /// <param name="taskName"></param>
        /// <returns></returns>
        public void SetClientUpdateTick(Int32 timeMs)
        {
            try
            {
                using (Clients.AutomationAdminServiceClient client =
                    this.ConnectAutomationAdminService())
                {
                    try
                    {
                        client.SetClientUpdateTick(timeMs);
                    }
                    catch (Exception ex)
                    {
                        this.Log.ToolException(ex, "Error setting client tick rate", timeMs);
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.ToolException(ex, "Error disposing Automation Admin Service consumer.");
            }
        }

        /// <summary>
        /// Set the dispatch of the job guid to a particular host.
        /// </summary>
        /// <param name="jobId"></param>
        /// <param name="hosts"></param>
        public void SetJobDispatch(Guid jobId, String host)
        {
            try
            {
                using (Clients.AutomationAdminServiceClient client =
                    this.ConnectAutomationAdminService())
                {
                    try
                    {
                        client.SetJobDispatch(jobId, host);
                    }
                    catch (Exception ex)
                    {
                        this.Log.ToolException(ex, "Error setting jobs dispatch");
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.ToolException(ex, "Error disposing Automation Admin Service consumer.");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="jobId"></param>
        /// <returns></returns>
        public JobResponse SkipJob(Guid jobId)
        {
            try
            {
                using (Clients.AutomationAdminServiceClient client =
                    this.ConnectAutomationAdminService())
                {
                    try
                    {
                        return client.SkipJob(jobId);
                    }
                    catch (Exception ex)
                    {
                        this.Log.ToolException(ex, "Error skipping job");
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.ToolException(ex, "Error disposing Automation Admin Service consumer.");
            }

            return new JobResponse(jobId, JobOp.Skip, false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="jobId"></param>
        /// <returns></returns>
        public JobResponse ReprocessJob(Guid jobId)
        {
            try
            {
                using (Clients.AutomationAdminServiceClient client =
                    this.ConnectAutomationAdminService())
                {
                    try
                    {
                        return client.ReprocessJob(jobId);
                    }
                    catch (Exception ex)
                    {
                        this.Log.ToolException(ex, "Error reprocessing job");
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.ToolException(ex, "Error disposing Automation Admin Service consumer.");
            }

            return new JobResponse(jobId, JobOp.Reprocess, false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="jobId"></param>
        /// <returns></returns>
        public JobResponse PrioritiseJob(Guid jobId)
        {
            try
            {
                using (Clients.AutomationAdminServiceClient client =
                    this.ConnectAutomationAdminService())
                {
                    try
                    {
                        return client.PrioritiseJob(jobId);
                    }
                    catch (Exception ex)
                    {
                        this.Log.ToolException(ex, "Error prioritising job");
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.ToolException(ex, "Error disposing Automation Admin Service consumer.");
            }

            return new JobResponse(jobId, JobOp.Prioritise, false);
        }

        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Resolves the host of a uri and converts it to an IP address via a DNS lookup.
        /// </summary>
        /// <param name="uri"></param>
        /// <returns></returns>
        private Uri ResolveUri(Uri uri)
        {
            IPAddress[] addresslist = Dns.GetHostAddresses(uri.DnsSafeHost);
            if (addresslist.Length < 1)
            {
                throw new ArgumentException("Unable to resolve the service connection's IP.");
            }

            UriBuilder builder = new UriBuilder(uri);
            builder.Host = addresslist[0].ToString();
            return builder.Uri;
        }

        /// <summary>
        /// Create tcp.net connection to our Automation Service; return WCF client.
        /// </summary>
        /// <returns></returns>
        private Clients.AutomationAdminServiceClient ConnectAutomationAdminService()
        {
            // Connect to automation service.
            this.Log.MessageCtx(LOG_CTX, "Connecting: {0}.", this._unresolvedUri);
            EndpointAddress address = new EndpointAddress(this._resolvedUri);
            NetTcpBinding binding = new NetTcpBinding();
            binding.ReceiveTimeout = new TimeSpan(0, 30, 0);
            binding.SendTimeout = new TimeSpan(0, 30, 0);
            binding.OpenTimeout = new TimeSpan(0, 0, 30);
            binding.CloseTimeout = new TimeSpan(0, 0, 30);
            binding.MaxReceivedMessageSize = 100 * 1024 * 1024;
            binding.MaxBufferPoolSize = 100 * 1024 * 1024;
            binding.ReaderQuotas.MaxStringContentLength = 100 * 1024 * 1024;
            binding.ReliableSession.Enabled = false;
            binding.ReliableSession.InactivityTimeout = TimeSpan.MaxValue;

            Clients.AutomationAdminServiceClient client = null;
            try
            {
                client = new Clients.AutomationAdminServiceClient(binding, address);
                client.Open();
                return (client);
            }
            catch (EndpointNotFoundException)
            {
                this.Log.ToolError("Could not connect to the end point.");
                return (null);
            }
            catch (Exception ex)
            {
                this.Log.ToolException(ex, "Error connecting to Automation Worker service.");
                return (null);
            }
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Automation.Consumers namespace
