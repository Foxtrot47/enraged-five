﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using RSG.Pipeline.Automation.Common.Client;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Services;
using RSG.Pipeline.Automation.Common.Services.Messages;

namespace RSG.Pipeline.Automation.Consumers.Clients
{

    /// <summary>
    /// File Transfer Service WCF client (consumers should be used externally).
    /// </summary>
    internal sealed class FileTransferServiceClient :
        ClientBase<IFileTransferService>,
        IFileTransferService
    {
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="binding"></param>
        /// <param name="remoteAddress"></param>    
        public FileTransferServiceClient(System.ServiceModel.Channels.Binding binding, EndpointAddress remoteAddress) 
            : base(binding, remoteAddress) 
        {
        }
        #endregion // Constructor(s)

        #region IFileTransferService Interface Methods
        /// <summary>
        /// Upload a file.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public TransferResponse UploadFile(RemoteFile request)
        {
            return (base.Channel.UploadFile(request));
        }

        /// <summary>
        /// Download a file.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public RemoteFile DownloadFile(RemoteFileRequest request)
        {
            return (base.Channel.DownloadFile(request));
        }

        /// <summary>
        /// Read information about the available files.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Obsolete("Use FileAvailability2 - more file information is provided in an extensible way.")]
        public RemoteFileAvailability FileAvailability(RemoteFileAvailabilityRequest request)
        {
            return (base.Channel.FileAvailability(request));
        }

        /// <summary>
        /// Read information about the availaility of files.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public RemoteFileAvailability2 FileAvailability2(RemoteFileAvailabilityRequest request)
        {
            return (base.Channel.FileAvailability2(request));
        }

        /// <summary>
        /// Return directory used for files transferred to client.
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        public String GetClientFileDirectory(IJob job)
        {
            return (base.Channel.GetClientFileDirectory(job));
        }
        #endregion // IFileTransferService Interface Methods
    }

} // RSG.Pipeline.Automation.Consumers.Clients namespace
