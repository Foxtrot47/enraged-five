﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Client;

namespace RSG.Pipeline.Automation.Consumers
{

    /// <summary>
    /// AutomationWorkerAdminService consumer class.
    /// </summary>
    public class AutomationWorkerAdminServiceConsumer
    {
        #region Constants
        /// <summary>
        /// Log context.
        /// </summary>
        private static readonly String LOG_CTX = "Automation Worker Admin Service Consumer";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Service connection string (e.g. "tcp.net://ediw-dmuir3:7001/automation_worker.svc/admin")
        /// </summary>
        public Uri ServiceConnection
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Log.
        /// </summary>
        private IUniversalLog Log;
        #endregion // Member Data
        
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="uri"></param>
        public AutomationWorkerAdminServiceConsumer(Uri serviceConnection)
        {
            this.ServiceConnection = serviceConnection;
            this.Log = LogFactory.CreateUniversalLog(LOG_CTX);
        }
        #endregion // Constructor(s)
        
        // Controller methods here wrap the IAutomationWorkerAdminService and 
        // IAutomationWorkerAdminService methods; hiding the details of the WCF 
        // services.  This allows our WCF service channels to be short-lived.
        #region Controller Methods
        #endregion // Controller Methods

        #region Private Methods
        #endregion // Private Methods
    }

} // RSG.Pipeline.Automation.Consumers namespace
