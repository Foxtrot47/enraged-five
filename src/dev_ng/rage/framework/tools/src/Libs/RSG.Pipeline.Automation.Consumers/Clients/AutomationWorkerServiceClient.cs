﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using RSG.Pipeline.Automation.Common.Client;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Services;

namespace RSG.Pipeline.Automation.Consumers.Clients
{

    /// <summary>
    /// Automation Worker Service WCF client (consumers should be used externally).
    /// </summary>
    internal sealed class AutomationWorkerServiceClient :
        ClientBase<IAutomationWorkerService>,
        IAutomationWorkerService
    {
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="binding"></param>
        /// <param name="remoteAddress"></param>    
        public AutomationWorkerServiceClient(System.ServiceModel.Channels.Binding binding, EndpointAddress remoteAddress) 
            : base(binding, remoteAddress) 
        {
        }
        #endregion // Constructor(s)

        #region IAutomationWorkerService Interface Methods
        /// <summary>
        /// Request current client state.
        /// </summary>
        /// <returns></returns>
        public WorkerState GetState()
        {
            return (base.Channel.GetState());
        }

        /// <summary>
        /// Request to process a particular job.
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        public bool Process(IJob job)
        {
            return (base.Channel.Process(job));
        }

        /// <summary>
        /// Request to abort particular job.
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        public bool Abort(IJob job)
        {
            return (base.Channel.Abort(job));
        }
        #endregion // IAutomationWorkerService Interface Methods
    }

} // RSG.Pipeline.Automation.Consumers.Clients namespace
