﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Services.Assets.Contracts.DataContracts
{
    /// <summary>
    /// Returns a single texture/scene usage pair.
    /// </summary>
    [DataContract]
    public class TextureUsage
    {
        /// <summary>
        /// Name of the texture.
        /// </summary>
        [DataMember]
        public string TextureName { get; set; }

        /// <summary>
        /// Name of the scene the texture is used in.
        /// </summary>
        [DataMember]
        public string SceneName { get; set; }
    }
}
