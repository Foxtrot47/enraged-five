﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Dto = RSG.Services.Assets.Contracts.DataContracts;

namespace RSG.Services.Assets.Contracts.ServiceContracts
{
    /// <summary>
    /// Service contract for manipulating scene xml data.
    /// </summary>
    [ServiceContract]
    public interface IAssetPopulationService
    {
        /// <summary>
        /// Populates the database with a particular scene xml file.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="scene"></param>
        [OperationContract]
        void PopulateSceneXmlFile(string filename, Dto.Scene scene);

        /// <summary>
        /// Deletes all data relating to a particular scene xml file.
        /// </summary>
        /// <param name="filename"></param>
        [OperationContract]
        void DeleteSceneXmlFile(string filename);
    }
}
