﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Services.Assets.Contracts.DataContracts
{
    /// <summary>
    /// Data transfer object for transferring scene xml object attribute data between server and client.
    /// </summary>
    [DataContract]
    public class Attribute
    {
        /// <summary>
        /// Attribute name.
        /// </summary>
        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// String attribute value.
        /// </summary>
        [DataMember]
        public string StringValue { get; set; }

        /// <summary>
        /// Integer attribute value.
        /// </summary>
        [DataMember]
        public int? IntValue { get; set; }

        /// <summary>
        /// Float attribute value.
        /// </summary>
        [DataMember]
        public float? FloatValue { get; set; }

        /// <summary>
        /// Bool attribute value.
        /// </summary>
        [DataMember]
        public bool? BoolValue { get; set; }
    }
}
