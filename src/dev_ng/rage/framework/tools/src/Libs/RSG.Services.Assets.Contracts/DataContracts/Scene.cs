﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Services.Assets.Contracts.DataContracts
{
    /// <summary>
    /// Data transfer object for transferring scene xml scene data between server and client.
    /// </summary>
    [DataContract]
    public class Scene
    {
        /// <summary>
        /// Path to the file for this scene.
        /// </summary>
        [DataMember]
        public string Filename { get; set; }

        /// <summary>
        /// Version of the scene xml library used to save this scene.
        /// </summary>
        [DataMember]
        public float Version { get; set; }

        /// <summary>
        /// Time the file was last exported.
        /// </summary>
        [DataMember]
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// Name of the user that last exported the file.
        /// </summary>
        [DataMember]
        public string Username { get; set; }

        /// <summary>
        /// Root level objects in this scene.
        /// </summary>
        [DataMember]
        public IList<Object> Objects { get; set; }

        /// <summary>
        /// Root level materials in this scene.
        /// </summary>
        [DataMember]
        public IList<Material> Materials { get; set; }
    }
}
