﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Services.Assets.Contracts.DataContracts
{
    /// <summary>
    /// Data transfer object for transferring scene xml material data between server and client.
    /// </summary>
    [DataContract]
    public class Material
    {
        /// <summary>
        /// Material's unique guid.
        /// </summary>
        [DataMember]
        public Guid Guid { get; set; }

        /// <summary>
        /// Type of material this is.
        /// </summary>
        [DataMember]
        public MaterialType Type { get; set; }

        /// <summary>
        /// Name of the shader preset that this material uses (if any).
        /// </summary>
        [DataMember]
        public string Preset { get; set; }

        /// <summary>
        /// List of sub-materials that make up this material.
        /// </summary>
        [DataMember]
        public IList<Material> SubMaterials { get; set; }

        /// <summary>
        /// List of textures this material uses.
        /// </summary>
        [DataMember]
        public IList<Texture> Textures { get; set; }
    }
}
