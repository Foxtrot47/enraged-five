﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Services.Assets.Contracts.DataContracts
{
    /// <summary>
    /// Texture Types (DiffuseMap, BumpMap, etc).
    /// </summary>
    /// <remarks>
    /// This should be kept in sync with the RSG.SceneXml.Material.TextureTypes enumeration.
    /// Note that these values are used in a database so don't remove any values or add
    /// any new values in the middle of the enumeration.
    /// </remarks>
    public enum TextureType
    {
        Unknown,
        DiffuseMap,
        BumpMap,
        SpecularMap,
        GlossMap,
        SpecularLevelMap,
        AmbientMap,
        SelfIlluminationMap,
        OpacityMap,
        FilterColourMap,
        ReflectionMap,
        RefractionMap,
        DisplacementMap,
        DiffuseAlpha,
        BumpAlpha,
        SpecularAlpha
    }
}
