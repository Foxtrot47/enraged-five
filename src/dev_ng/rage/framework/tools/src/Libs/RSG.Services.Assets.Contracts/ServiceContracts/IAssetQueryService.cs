﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Dto = RSG.Services.Assets.Contracts.DataContracts;

namespace RSG.Services.Assets.Contracts.ServiceContracts
{
    /// <summary>
    /// Service contract for querying asset data.
    /// </summary>
    [ServiceContract]
    public interface IAssetQueryService
    {
        /// <summary>
        /// Retrieves texture usage information for the specified textures.
        /// </summary>
        /// <param name="textures">List of textures to get usage information for.</param>
        /// <returns></returns>
        [OperationContract]
        IList<Dto.TextureUsage> GetTextureUsage(IEnumerable<string> textures);

        /// <summary>
        /// Retrieves texture/alpha texture pairs for the passed in texture list.
        /// </summary>
        /// <param name="textures">List of textures to retrieve the pairs for.</param>
        /// <returns></returns>
        [OperationContract]
        IList<Dto.TexturePair> GetTexturePairs(IEnumerable<string> textures);
    }
}
