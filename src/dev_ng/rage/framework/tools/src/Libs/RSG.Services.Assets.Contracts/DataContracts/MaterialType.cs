﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Services.Assets.Contracts.DataContracts
{
    /// <summary>
    /// Material types (Multi, Standard, Rage).
    /// </summary>
    public enum MaterialType
    {
        Unknown,
        MultiMaterial,
        Standard,
        Rage
    }
}
