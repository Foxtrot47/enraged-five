﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using RSG.Statistics.Common.ServiceContract;
using SceneXmlDto = RSG.Statistics.Common.Dto.SceneXml;

namespace RSG.Statistics.Client2
{
    /// <summary>
    /// Service contract for querying scene xml data.
    /// </summary>
    [ServiceContract]
    public interface ISceneXmlQueryClient : ISceneXmlQueryService
    {
        /// <summary>
        ///  Retrieves the list of scene xml files that are using the specified texture.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        [OperationContract(
            Action = "http://tempuri.org/ISceneXmlQueryService/GetSceneXmlFilesUsingTexture",
            ReplyAction = "http://tempuri.org/ISceneXmlQueryService/GetSceneXmlFilesUsingTextureResponse")]
        Task<IList<String>> GetSceneXmlFilesUsingTextureAsync(String filename);

        /// <summary>
        /// Retrieves texture usage information for the specified textures.
        /// </summary>
        /// <param name="textures">List of textures to get usage information for.</param>
        /// <returns></returns>
        [OperationContract(
            Action = "http://tempuri.org/ISceneXmlQueryService/GetTextureUsage",
            ReplyAction = "http://tempuri.org/ISceneXmlQueryService/GetTextureUsageResponse")]
        Task<IList<SceneXmlDto.TextureUsage>> GetTextureUsageAsync(IEnumerable<String> textures);

        /// <summary>
        /// Retrieves a list of materials that make use of the specified texture.
        /// </summary>
        /// <param name="filename">The texture to search for.</param>
        /// <returns></returns>
        [OperationContract(
            Action = "http://tempuri.org/ISceneXmlQueryService/GetMaterialsThatUseTexture",
            ReplyAction = "http://tempuri.org/ISceneXmlQueryService/GetMaterialsThatUseTextureResponse")]
        Task<IList<SceneXmlDto.Material>> GetMaterialsThatUseTextureAsync(String filename);
        /// <summary>
        /// Retrieves texture/alpha texture pairs for the passed in texture list.
        /// </summary>
        /// <param name="textures">List of textures to retrieve the pairs for.</param>
        /// <returns></returns>
        [OperationContract(
            Action = "http://tempuri.org/ISceneXmlQueryService/GetTexturePairs",
            ReplyAction = "http://tempuri.org/ISceneXmlQueryService/GetTexturePairsResponse")]
        Task<IList<SceneXmlDto.TexturePair>> GetTexturePairsAsync(IEnumerable<String> textures);
    } // ISceneXmlQueryClient
}
