﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.Configuration.Services;
using RSG.Services.Common.Clients;
using RSG.Statistics.Common.ServiceContract;
using SceneXmlDto = RSG.Statistics.Common.Dto.SceneXml;

namespace RSG.Statistics.Client2
{
    /// <summary>
    /// 
    /// </summary>
    public class SceneXmlQueryClient : ConfigAwareClient<ISceneXmlQueryClient>, ISceneXmlQueryClient
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public SceneXmlQueryClient(IServiceHostConfig config)
            : base(config)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="binding"></param>
        /// <param name="remoteAddress"></param>
        public SceneXmlQueryClient(Binding binding, EndpointAddress remoteAddress)
            : base(binding, remoteAddress)
        {
        }
        #endregion // Constructor(s)

        #region ISceneXmlQueryService Implementation
        /// <summary>
        /// Retrieves the list of scene xml files that are using the specified texture.
        /// </summary>
        /// <param name="filename">The texture to search for.</param>
        /// <returns></returns>
        public IList<String> GetSceneXmlFilesUsingTexture(String filename)
        {
            return base.Channel.GetSceneXmlFilesUsingTexture(filename);
        }

        /// <summary>
        /// Retrieves the list of scene xml files that are using the specified texture.
        /// </summary>
        /// <param name="filename">The texture to search for.</param>
        /// <returns></returns>
        public async Task<IList<String>> GetSceneXmlFilesUsingTextureAsync(String filename)
        {
            return await base.Channel.GetSceneXmlFilesUsingTextureAsync(filename);
        }

        /// <summary>
        /// Retrieves texture usage information for the specified textures.
        /// </summary>
        /// <param name="textures">List of textures to get usage information for.</param>
        /// <returns></returns>
        public IList<SceneXmlDto.TextureUsage> GetTextureUsage(IEnumerable<String> textures)
        {
            return base.Channel.GetTextureUsage(textures);
        }

        /// <summary>
        /// Retrieves texture usage information for the specified textures.
        /// </summary>
        /// <param name="textures">List of textures to get usage information for.</param>
        /// <returns></returns>
        public async Task<IList<SceneXmlDto.TextureUsage>> GetTextureUsageAsync(IEnumerable<String> textures)
        {
            return await base.Channel.GetTextureUsageAsync(textures);
        }

        /// <summary>
        /// Retrieves a list of materials that make use of the specified texture.
        /// </summary>
        /// <param name="filename">The texture to search for.</param>
        /// <returns></returns>
        public IList<SceneXmlDto.Material> GetMaterialsThatUseTexture(String filename)
        {
            return base.Channel.GetMaterialsThatUseTexture(filename);
        }

        /// <summary>
        /// Retrieves a list of materials that make use of the specified texture.
        /// </summary>
        /// <param name="filename">The texture to search for.</param>
        /// <returns></returns>
        public async Task<IList<SceneXmlDto.Material>> GetMaterialsThatUseTextureAsync(String filename)
        {
            return await base.Channel.GetMaterialsThatUseTextureAsync(filename);
        }

        /// <summary>
        /// Retrieves texture/alpha texture pairs for the passed in texture list.
        /// </summary>
        /// <param name="textures">List of textures to retrieve the pairs for.</param>
        /// <returns></returns>
        public IList<SceneXmlDto.TexturePair> GetTexturePairs(IEnumerable<String> textures)
        {
            return base.Channel.GetTexturePairs(textures);
        }

        /// <summary>
        /// Retrieves texture/alpha texture pairs for the passed in texture list.
        /// </summary>
        /// <param name="textures">List of textures to retrieve the pairs for.</param>
        /// <returns></returns>
        public async Task<IList<SceneXmlDto.TexturePair>> GetTexturePairsAsync(IEnumerable<String> textures)
        {
            return await base.Channel.GetTexturePairsAsync(textures);
        }
        #endregion // ISceneXmlQueryService Implementation
    } // SceneXmlQueryClient
}
