﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using RSG.Statistics.Domain.Entities;
using RSG.Statistics.Server.Repository;
using RSG.Statistics.Server.Extensions;
using NHibernate.Criterion;
using RSG.Statistics.Common.Report;
using RSG.Statistics.Domain.Entities.ResourceStats;
using RSG.Statistics.Common.Model.ResourceStats;
using RSG.Platform;

using ResourceStatsDomain = RSG.Statistics.Domain.Entities.ResourceStats;
using ModelPlatform = RSG.Model.Statistics.Platform;
using RSG.Base.Logging;
using System.Text;
using RSG.Model.Statistics.Platform;
using NHibernate;
using NHibernate.Transform;
using RSG.Statistics.Common.Config;

namespace RSG.Statistics.Server.Report.ResourceStats
{
    /// <summary>
    /// Report that provides resource stat bucket 'fragmentation' information.
    /// </summary>
    [Export(typeof(IStatsReport))]
    internal class ResourceStatReport : StatsReportBase<ResourceStatReportResult>
    {
        #region Constants
        /// <summary>
        /// Name of the report.
        /// </summary>
        private const String c_name = "ResourceStatReport";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Platform to restrict the results to.
        /// </summary>
        [ReportParameter(Required = true)]
        public String PlatformName { get; set; }

        /// <summary>
        /// Convenience property that transforms the platform names to platform enum values.
        /// </summary>
        protected RSG.Platform.Platform Platform
        {
            get
            {
                return (RSG.Platform.Platform)Enum.Parse(typeof(RSG.Platform.Platform), PlatformName, true);
            }
        }

        /// <summary>
        /// Optional start date.
        /// </summary>
        [ReportParameter]
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Optional end date.
        /// </summary>
        [ReportParameter]
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Optional ResourceNames.
        /// </summary>
        [ReportParameter]
        public List<String> ResourceNames { get; set; }

        /// <summary>
        /// Optional Filetypes.
        /// </summary>
        [ReportParameter]
        public List<String> FileTypeNames { get; set; }

        /// <summary>
        /// Convenience property that transforms the platform names to platform enum values.
        /// </summary>
        public IList<RSG.Platform.FileType> FileTypes
        {
            get
            {
                IList<RSG.Platform.FileType> fileTypes = new List<RSG.Platform.FileType>();
                foreach (String fileTypeName in FileTypeNames)
                {
                    RSG.Platform.FileType fileType;
                    if (Enum.TryParse(fileTypeName, out fileType))
                    {
                        fileTypes.Add(fileType);
                    }
                }
                return fileTypes;
            }
        }

        /// <summary>
        /// Optional storage of detail in the report.
        /// </summary>
        [ReportParameter]
        public bool ReportDetail { get; set; }
        #endregion // Properties

        #region Private Class
        /// <summary>
        /// Temp class required so that we can group virtual/physical results together.
        /// </summary>
        private class IntermediateResult
        {
            public long Id { get; set; }
            public String ResourceName { get; set; }
            public String SourceFilename { get; set; }
            public String DestinationFilename { get; set; }
            public RSG.Platform.FileType FileType { get; set; }
            public uint Used { get; set; }
            public uint Capacity { get; set; }
            public ResourceBucketType BucketType { get; set; }
            public uint BucketId { get; set; }
        } // IntermediateResult
        #endregion // Private Class

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public ResourceStatReport()
            : base(c_name)
        {
        }
        #endregion // Constructor(s)

        #region IStatsReport Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        /// <returns></returns>
        protected override ResourceStatReportResult Generate(IRepositoryLocator locator, ILog log, IStatsServer server)
        {
            IEnumRepository<RSG.Platform.Platform> platformRepo =
                    (IEnumRepository<RSG.Platform.Platform>)locator.GetRepository<EnumReference<RSG.Platform.Platform>>();
            EnumReference<RSG.Platform.Platform> platform = platformRepo.GetByValue(Platform);

            // Construct the sql query.
            StringBuilder sb = new StringBuilder();
            sb.Append(
                @"SELECT
                    s.Id AS Id,
                    rn.Name AS ResourceName,
                    s.SourceFilename AS SourceFilename,
                    s.DestinationFilename AS DestinationFilename,
                    ft.Value AS FileType,
                    b.Size AS Used,
                    b.Capacity AS Capacity,
                    b.BucketId AS BucketId,
                    bt.Value AS BucketType
                FROM ResourceStat s
                INNER JOIN
                (
	                SELECT MAX(Id) AS MaxId
	                FROM ResourceStat s
	                WHERE s.Timestamp BETWEEN :start AND :end AND s.PlatformId=:platformId ");
            if (ResourceNames.Any())
            {
                sb.Append("AND s.ResourceNameId IN (:resourceNameIds) ");
            }
            if (FileTypes.Any())
            {
                sb.Append("AND s.FileTypeId IN (:fileTypeIds) ");
            }
            sb.Append(@"GROUP BY s.ResourceNameId, s.FileTypeId
                ) MaxIdTable ON MaxIdTable.MaxId = s.Id ");
            sb.Append(
                @"LEFT JOIN ResourceBucket b ON b.ResourceStatId=s.Id
                INNER JOIN ResourceName rn ON s.ResourceNameId=rn.Id
                INNER JOIN FileType ft ON s.FileTypeId=ft.Id
                INNER JOIN ResourceBucketType bt ON b.BucketTypeId=bt.Id;");

            ISQLQuery sqlQuery = locator.CreateUnmanagedSQLQuery(sb.ToString());
            sqlQuery.SetParameter("start", StartDate);
            sqlQuery.SetParameter("end", EndDate);
            sqlQuery.SetParameter("platformId", platform.Id);
            if (ResourceNames.Any())
            {
                IGameAssetRepository<ResourceName> resouceNameRepo = (IGameAssetRepository<ResourceName>)locator.GetRepository<ResourceName>();
                List<ResourceName> resourceNames = ResourceNames.Select(item => resouceNameRepo.GetByName(item)).ToList();
                sqlQuery.SetParameterList("resourceNameIds", resourceNames.Select(item => item.Id).ToList());
            }
            if (FileTypes.Any())
            {
                IEnumRepository<RSG.Platform.FileType> fileTypeRepo =
                    (IEnumRepository<RSG.Platform.FileType>)locator.GetRepository<EnumReference<RSG.Platform.FileType>>();
                List<EnumReference<RSG.Platform.FileType>> fileTypes = FileTypes.Select(item => fileTypeRepo.GetByValue(item)).ToList();
                sqlQuery.SetParameterList("fileTypeIds", fileTypes.Select(item => item.Id).ToList());
            }
            sqlQuery.SetResultTransformer(Transformers.AliasToBean(typeof(IntermediateResult)));
            IList<IntermediateResult> interResults = sqlQuery.List<IntermediateResult>();

            // Results that grouped virtual/physical values.
            IDictionary<Tuple<String, FileType>, ModelPlatform.ResourceStat> results =
                new Dictionary<Tuple<String, FileType>, ModelPlatform.ResourceStat>();

            foreach (IntermediateResult interResult in interResults)
            {
                Tuple<String, FileType> key = new Tuple<String, FileType>(interResult.ResourceName, interResult.FileType);

                if (!results.ContainsKey(key))
                {
                    results[key] = new ModelPlatform.ResourceStat(interResult.ResourceName, 
                        interResult.SourceFilename, interResult.DestinationFilename,
                        interResult.FileType, Platform);
                }

                results[key].Buckets[interResult.BucketType].Add(
                    new ModelPlatform.ResourceBucket(interResult.BucketType, interResult.BucketId, interResult.Used, interResult.Capacity));
            }

            return new ResourceStatReportResult(results.Values, ReportDetail);
        }
        #endregion // IStatsReport region
    } // ResourceStatReport
}
