﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Criterion;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Statistics.Client.Vertica.Client;
using RSG.Statistics.Common;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Model.Telemetry;
using RSG.Statistics.Common.Report;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Dto.ReportResults;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Server.Repository;

namespace RSG.Statistics.Server.Report.Telemetry
{
    /// <summary>
    /// Report that provides radio station listening statistics.
    /// </summary>
    [Export(typeof(IStatsReport))]
    internal class VerticaShopPurchasesReport : VerticaTelemetryReportBase<List<ShopStoreStat>>
    {
        #region Constants
        /// <summary>
        /// Name of the report.
        /// </summary>
        private const String c_name = "VerticaShopPurchases";

        /// <summary>
        /// Sub category name for the vehicle paint colour items.
        /// </summary>
        private const String c_paintColourSubCategory = "Paint";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Flag indicating that we should group the data by type as opposed to shop name.
        /// </summary>
        [ReportParameter(Required = true)]
        public bool GroupByType { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public VerticaShopPurchasesReport()
            : base(c_name)
        {
        }
        #endregion // Constructor(s)

        #region IReportDataProvider Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        /// <returns></returns>
        protected override List<ShopStoreStat> Generate(IRepositoryLocator locator, ILog log, IStatsServer server)
        {
            // Create two lookups for the data.
            IGameAssetRepository<Shop> shopRepo = (IGameAssetRepository<Shop>)locator.GetRepository<Shop>();
            IDictionary<uint, Shop> shopLookup = shopRepo.CreateAssetLookup();

            // Convert the parameters to something vertica can work with.
            ShopPurchaseParams queryParams = new ShopPurchaseParams();
            queryParams.PaintColourHashes = GetPaintColourHashes(locator);
            queryParams.PaintColourLabels = GetPaintColourLabels(locator);
            queryParams.Groupings = GetDataGroupings(shopLookup.Values);
            SetDefaultParams(locator, queryParams);

            // Contact vertica for the report data.
            List<ShopStoreStat> shopStats;
            using (ReportClient client = new ReportClient(server.VerticaServer))
            {
                shopStats = (List<ShopStoreStat>)client.RunReport(ReportNames.ShopPurchasesReport, queryParams);
            }

            // Create a set of lookups that 
            if (shopStats != null && shopStats.Any())
            {
                IGameAssetRepository<ShopItem> shopItemRepo = (IGameAssetRepository<ShopItem>)locator.GetRepository<ShopItem>();
                IDictionary<uint, ShopItem> shopItemLookup = shopItemRepo.CreateAssetLookup();

                // Process the results that we received from vertica resolving hashes to strings and bucketing them based on the store name.
                foreach (ShopStoreStat storeStat in shopStats)
                {
                    // Resolve the shop name information
                    Shop shop;
                    if (!shopLookup.TryGetValue(storeStat.ShopHash, out shop))
                    {
                        log.Warning("Unknown shop name hash encountered in data returned from Vertica. Hash: {0}", storeStat.ShopHash);
                    }
                    else
                    {
                        storeStat.ShopName = shop.FriendlyName;
                        storeStat.ShopType = shop.ShopType.Value.Value;
                    }

                    IList<ShopItemStat> toRemove = new List<ShopItemStat>();
                    foreach (ShopItemStat itemStat in storeStat.Items)
                    {
                        ShopItem shopItem;
                        if (!shopItemLookup.TryGetValue(itemStat.ItemHash, out shopItem))
                        {
                            log.Warning("Unknown shop item hash encountered in data returned from Vertica. Hash: {0}", itemStat.ItemHash);
                            toRemove.Add(itemStat);
                        }
                        else
                        {
                            String friendlyName = shopItem.FriendlyName;
                            if (shopItem.FriendlyNameOverride != null)
                            {
                                friendlyName = shopItem.FriendlyNameOverride;
                            }

                            if (shopItem.SubCategory != null && shopItem.SubCategory != c_paintColourSubCategory)
                            {
                                itemStat.ItemName = String.Format("{0} ({1})", friendlyName, shopItem.SubCategory);
                            }
                            else if (shopItem.SubCategory == c_paintColourSubCategory && itemStat.ColorId != null)
                            {
                                itemStat.ItemName = String.Format("{0} ({1})", friendlyName, GetColorNameFromId((int)itemStat.ColorId.Value));
                            }
                            else if (shopItem.FriendlyName != null)
                            {
                                itemStat.ItemName = friendlyName;
                            }
                            else
                            {
                                itemStat.ItemName = shopItem.Name;
                            }
                        }
                    }

                    storeStat.Items.RemoveRange(toRemove);
                    storeStat.Items.Sort();
                }
            }

            return shopStats;
        }
        #endregion // IReportDataProvider Implementation

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private IList<uint> GetPaintColourHashes(IRepositoryLocator locator)
        {
            ICriteria criteria =
                locator.CreateCriteria<ShopItem>()
                    .SetProjection(Projections.ProjectionList()
                        .Add(Projections.Property("Identifier")))
                    .Add(Restrictions.Eq("SubCategory", c_paintColourSubCategory));

            IList<uint> hashes = new List<uint>();
            foreach (String hashString in criteria.List<String>())
            {
                uint hash;
                if (UInt32.TryParse(hashString, out hash))
                {
                    hashes.Add(hash);
                }
            }
            return hashes;
        }

        /// <summary>
        /// 
        /// </summary>
        private IList<String> GetPaintColourLabels(IRepositoryLocator locator)
        {
            ICriteria criteria =
                locator.CreateCriteria<ShopItem>()
                    .SetProjection(Projections.ProjectionList()
                        .Add(Projections.Property("Name")))
                    .Add(Restrictions.Eq("SubCategory", c_paintColourSubCategory));
            return criteria.List<String>();
        }

        /// <summary>
        /// 
        /// </summary>
        private IList<Tuple<ShopType, IList<uint>>> GetDataGroupings(IEnumerable<Shop> shops)
        {
            IList<Tuple<ShopType, IList<uint>>> groupings = new List<Tuple<ShopType, IList<uint>>>();
            if (GroupByType)
            {
                foreach (IGrouping<ShopType, Shop> group in shops.GroupBy(item => item.ShopType.Value.Value))
                {
                    IList<uint> grouping = new List<uint>();
                    foreach (String identifier in group.Select(item => item.Identifier))
                    {
                        uint hash;
                        if (UInt32.TryParse(identifier, out hash))
                        {
                            grouping.Add(hash);
                        }
                    }
                    if (group.Any())
                    {
                        groupings.Add(Tuple.Create(group.Key, grouping));
                    }
                }
            }
            else
            {
                foreach (IGrouping<String, Shop> group in shops.GroupBy(item => item.FriendlyName))
                {
                    IList<uint> grouping = new List<uint>();
                    foreach (String identifier in group.Select(item => item.Identifier))
                    {
                        uint hash;
                        if (UInt32.TryParse(identifier, out hash))
                        {
                            grouping.Add(hash);
                        }
                    }
                    if (group.Any())
                    {
                        groupings.Add(Tuple.Create(group.First().ShopType.Value.Value, grouping));
                    }
                }
            }
            return groupings;
        }

        /// <summary>
        /// 
        /// </summary>
        private String GetColorNameFromId(int id)
        {
            switch (id)
            {
                case -1:
                    return "Crew";
                case 0:
                    return "Metallic";
                case 1:
                    return "Classic";
                case 2:
                    return "Pearlescent";
                case 3:
                    return "Matte";
                case 4:
                    return "Metal";
                case 5:
                    return "Chrome";
                default:
                    return null;
            }
        }
        #endregion // Private Methods
    } // VerticaShopPurchasesReport
}
