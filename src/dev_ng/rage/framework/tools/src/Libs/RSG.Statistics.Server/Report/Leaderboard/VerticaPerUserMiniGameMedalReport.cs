﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using RSG.Base.Logging;
using RSG.ROS;
using RSG.Statistics.Client.Vertica.Client;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Model.Leaderboard;
using RSG.Statistics.Common.Report;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.Domain.Entities.Playthrough;
using RSG.Statistics.Server.Repository;

namespace RSG.Statistics.Server.Report.Leaderboard
{
    /// <summary>
    /// 
    /// </summary>
    [Export(typeof(IStatsReport))]
    internal class VerticaPerUserMiniGameMedalReport : VerticaPerUserReportBase<List<PerUserMiniGameMedalStats>>
    {
        #region Constants
        /// <summary>
        /// Name of the report.
        /// </summary>
        private const String c_name = "VerticaPerUserMiniGameMedals";
        #endregion // Constants
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public VerticaPerUserMiniGameMedalReport()
            : base(c_name)
        {
        }
        #endregion // Constructor(s)

        #region IStatsReport Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        protected override List<PerUserMiniGameMedalStats> Generate(IRepositoryLocator locator, ILog log, IStatsServer server)
        {
            // Convert the parameters to something vertica can work with.
            PerUserLeaderboardParams queryParams = new PerUserLeaderboardParams();
            SetDefaultParams(locator, queryParams);

            // Slight hack for playthrough users.
            // Check if any of the requested gamer handles are playthrough users and look up the leaderboard version that we should be querying for them.
            IList<PlaythroughUser> playthroughUsers =
                locator.CreateCriteria<PlaythroughUser>()
                    .Add(Restrictions.IsNotNull("GamerHandle"))
                    .Add(Restrictions.IsNotNull("LeaderboardVersion"))
                    .List<PlaythroughUser>();
            IDictionary<String, uint?> playthroughUserVersions = playthroughUsers.ToDictionary(item => item.GamerHandle, item => item.LeaderboardVersion);

            queryParams.GamerHandleLeaderboardVersions = new Dictionary<Tuple<String, ROSPlatform>, uint>();
            foreach (Tuple<String, ROSPlatform> tuple in queryParams.GamerHandlePlatformPairs)
            {
                uint? version;
                if (playthroughUserVersions.TryGetValue(tuple.Item1, out version))
                {
                    if (version != null)
                    {
                        queryParams.GamerHandleLeaderboardVersions[tuple] = (uint)version;
                    }
                }
            }

            // Get the results from the vertica service.
            using (ReportClient client = new ReportClient(server.VerticaServer))
            {
                return (List<PerUserMiniGameMedalStats>)client.RunReport(ReportNames.PerUserMiniGameMedalsReport, queryParams);
            }
        }
        #endregion // IStatsReport Implementation
    } // VerticaPerUserMiniGameMedalReport
}
