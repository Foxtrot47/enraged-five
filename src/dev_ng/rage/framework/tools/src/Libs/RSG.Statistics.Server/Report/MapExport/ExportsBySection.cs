﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using RSG.Base.Logging;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Report;
using RSG.Statistics.Server.Repository;
using ExpoModel = RSG.Statistics.Common.Model.MapExport;

namespace RSG.Statistics.Server.Report.MapExport
{
    /// <summary>
    /// Report that provides information about the exports grouped by section.
    /// </summary>
    [Export(typeof(IStatsReport))]
    internal class ExportsBySection : MapExportReportBase<List<ExpoModel.SectionSummary>>
    {
        #region Constants
        /// <summary>
        /// Name of the report.
        /// </summary>
        private const String c_name = "MapExportsBySection";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Start date.
        /// </summary>
        [ReportParameter(Required = true)]
        public DateTime StartDate { get; set; }

        /// <summary>
        /// End date.
        /// </summary>
        [ReportParameter(Required = true)]
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Username to view information for.
        /// </summary>
        [ReportParameter]
        public String Username { get; set; }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public ExportsBySection()
            : base(c_name)
        {
        }
        #endregion // Constructor(s)

        #region IReportDataProvider Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        /// <returns></returns>
        protected override List<ExpoModel.SectionSummary> Generate(IRepositoryLocator locator, ILog log, IStatsServer server)
        {
            return GetSectionSummaryInfo(locator, StartDate, EndDate, Username).ToList();
        }
        #endregion // IReportDataProvider Implementation
    } // ExportsBySection
}
