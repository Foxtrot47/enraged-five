﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using RSG.Statistics.Domain.Entities;
using System.ComponentModel.Composition;
using NHibernate.Criterion;
using System.Diagnostics;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Domain.Entities.GameAssetStats.Animation;
using RSG.Statistics.Domain.Entities.ResourceStats;

namespace RSG.Statistics.Server.Repository
{
    /// <summary>
    /// NHibernate repository locator
    /// </summary>
    public class RepositoryLocator : IRepositoryLocator
    {
        #region Properties
        /// <summary>
        /// NHibernate session.
        /// NOTE: This shouldn't be publicly exposed!
        /// </summary>
        public ISession Session
        {
            get
            {
                return m_session;
            }
        }
        private readonly ISession m_session;

        /// <summary>
        /// Lookup for repositories based on a db object type.
        /// </summary>
        private IDictionary<Type, object> RepositoryMap { get; set; }

        /// <summary>
        /// Map of db object type to repository type.
        /// </summary>
        private static IDictionary<Type, Type> RepositoryTypeMap { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public RepositoryLocator(ISession session)
        {
            m_session = session;
            RepositoryMap = new Dictionary<Type, object>();
        }

        /// <summary>
        /// 
        /// </summary>
        static RepositoryLocator()
        {
            RepositoryTypeMap = new Dictionary<Type, Type>();

            // Game Assets
            RepositoryTypeMap[typeof(Animation)] = typeof(GameAssetRepository<Animation>);
            RepositoryTypeMap[typeof(CashPack)] = typeof(GameAssetRepository<CashPack>);
            RepositoryTypeMap[typeof(Clip)] = typeof(GameAssetRepository<Clip>);
            RepositoryTypeMap[typeof(ClipDictionary)] = typeof(GameAssetRepository<ClipDictionary>);
            RepositoryTypeMap[typeof(Cutscene)] = typeof(GameAssetRepository<Cutscene>);
            RepositoryTypeMap[typeof(DrawList)] = typeof(GameAssetRepository<DrawList>);
            RepositoryTypeMap[typeof(MemoryHeap)] = typeof(GameAssetRepository<MemoryHeap>);
            RepositoryTypeMap[typeof(MemoryPool)] = typeof(GameAssetRepository<MemoryPool>);
            RepositoryTypeMap[typeof(MemoryStore)] = typeof(GameAssetRepository<MemoryStore>);
            RepositoryTypeMap[typeof(MiniGameVariant)] = typeof(GameAssetRepository<MiniGameVariant>);
            RepositoryTypeMap[typeof(Mission)] = typeof(MissionRepository);
            RepositoryTypeMap[typeof(Movie)] = typeof(GameAssetRepository<Movie>);
            RepositoryTypeMap[typeof(RSG.Statistics.Domain.Entities.GameAssets.Property)] = typeof(GameAssetRepository<RSG.Statistics.Domain.Entities.GameAssets.Property>);
            RepositoryTypeMap[typeof(RadioStation)] = typeof(GameAssetRepository<RadioStation>);
            RepositoryTypeMap[typeof(ResourceName)] = typeof(GameAssetRepository<ResourceName>);
            RepositoryTypeMap[typeof(Shop)] = typeof(GameAssetRepository<Shop>);
            RepositoryTypeMap[typeof(ShopItem)] = typeof(GameAssetRepository<ShopItem>);
            RepositoryTypeMap[typeof(TVShow)] = typeof(GameAssetRepository<TVShow>);
            RepositoryTypeMap[typeof(WebSite)] = typeof(GameAssetRepository<WebSite>);
            RepositoryTypeMap[typeof(XPCategory)] = typeof(GameAssetRepository<XPCategory>);
            RepositoryTypeMap[typeof(XPType)] = typeof(GameAssetRepository<XPType>);

            // Game Asset Stats
            RepositoryTypeMap[typeof(ClipDictionaryStat)] = typeof(ClipDictionaryStatRepository);
            RepositoryTypeMap[typeof(CutsceneStat)] = typeof(CutsceneStatRepository);

            // Enums
            // If you're adding a custom repository here you'll want to add the corresponding population call to the ServerBootStrapper.
            RepositoryTypeMap[typeof(EnumReference<RSG.Platform.Platform>)] = typeof(EnumRepository<RSG.Platform.Platform>);
            RepositoryTypeMap[typeof(EnumReference<RSG.Platform.FileType>)] = typeof(EnumRepository<RSG.Platform.FileType>);
            RepositoryTypeMap[typeof(EnumReference<RSG.Statistics.Common.GameType>)] = typeof(EnumRepository<RSG.Statistics.Common.GameType>);
            RepositoryTypeMap[typeof(EnumReference<RSG.Statistics.Common.BuildConfig>)] = typeof(EnumRepository<RSG.Statistics.Common.BuildConfig>);
            RepositoryTypeMap[typeof(EnumReference<RSG.Statistics.Common.MatchType>)] = typeof(EnumRepository<RSG.Statistics.Common.MatchType>);
            RepositoryTypeMap[typeof(EnumReference<RSG.Statistics.Common.SectionExportSubTask>)] = typeof(EnumRepository<RSG.Statistics.Common.SectionExportSubTask>);
            RepositoryTypeMap[typeof(EnumReference<RSG.Statistics.Common.ShopType>)] = typeof(EnumRepository<RSG.Statistics.Common.ShopType>);
            RepositoryTypeMap[typeof(EnumReference<RSG.Statistics.Common.UnlockType>)] = typeof(EnumRepository<RSG.Statistics.Common.UnlockType>);
            RepositoryTypeMap[typeof(EnumReference<RSG.Model.Common.Animation.ClipDictionaryCategory>)] = typeof(EnumRepository<RSG.Model.Common.Animation.ClipDictionaryCategory>);
            RepositoryTypeMap[typeof(EnumReference<RSG.Model.Common.Map.LodLevel>)] = typeof(EnumRepository<RSG.Model.Common.Map.LodLevel>);
            RepositoryTypeMap[typeof(EnumReference<RSG.Model.Common.Map.MapDrawableLODLevel>)] = typeof(EnumRepository<RSG.Model.Common.Map.MapDrawableLODLevel>);
            RepositoryTypeMap[typeof(EnumReference<RSG.Model.Common.Map.SpawnPointAvailableModes>)] = typeof(EnumRepository<RSG.Model.Common.Map.SpawnPointAvailableModes>);
            RepositoryTypeMap[typeof(EnumReference<RSG.Model.Common.Map.CollisionPrimitiveType>)] = typeof(EnumRepository<RSG.Model.Common.Map.CollisionPrimitiveType>);
            RepositoryTypeMap[typeof(EnumReference<RSG.Model.Common.Map.StatGroup>)] = typeof(EnumRepository<RSG.Model.Common.Map.StatGroup>);
            RepositoryTypeMap[typeof(EnumReference<RSG.Model.Common.Map.StatLodLevel>)] = typeof(EnumRepository<RSG.Model.Common.Map.StatLodLevel>);
            RepositoryTypeMap[typeof(EnumReference<RSG.Model.Common.Map.StatEntityType>)] = typeof(EnumRepository<RSG.Model.Common.Map.StatEntityType>);
            RepositoryTypeMap[typeof(EnumReference<RSG.Model.Common.Mission.MissionAttemptResult>)] = typeof(EnumRepository<RSG.Model.Common.Mission.MissionAttemptResult>);
            RepositoryTypeMap[typeof(EnumReference<RSG.Model.Common.Mission.MissionCategory>)] = typeof(EnumRepository<RSG.Model.Common.Mission.MissionCategory>);
            RepositoryTypeMap[typeof(EnumReference<RSG.Model.Statistics.Platform.ResourceBucketType>)] = typeof(EnumRepository<RSG.Model.Statistics.Platform.ResourceBucketType>);
            RepositoryTypeMap[typeof(EnumReference<RSG.ROS.ROSPlatform>)] = typeof(EnumRepository<RSG.ROS.ROSPlatform>);
        }
        #endregion // Constructor(s)

        #region Protected Methods
        /// <summary>
        /// 
        /// </summary>
        protected IRepository<T> CreateRepository<T>() where T : class, IDomainEntity
        {
            // Check whether we need to create a specific repository for this type.
            if (RepositoryTypeMap.ContainsKey(typeof(T)))
            {
                Type repoType = RepositoryTypeMap[typeof(T)];
                Debug.Assert(repoType.GetInterfaces().Contains(typeof(IRepository<T>)), "Non-repository type added for db object type.");
                return (IRepository<T>)Activator.CreateInstance(repoType, Session);
            }

            return new Repository<T>(Session);
        }
        #endregion // Protected Methods

        #region IRepositoryLocator Members
        /// <summary>
        /// Saves the given entity in the appropriate repository
        /// </summary>
        public T Save<T>(T instance) where T : class, IDomainEntity
        {
            return GetRepository<T>().Save(instance);
        }

        /// <summary>
        /// Updates the given entity in the appropriate repository
        /// </summary>
        public void Update<T>(T instance) where T : class, IDomainEntity
        {
            GetRepository<T>().Update(instance);
        }

        /// <summary>
        /// Saves or updates the given entity in the appropriate repository
        /// </summary>
        public void SaveOrUpdate<T>(T instance) where T : class, IDomainEntity
        {
            GetRepository<T>().SaveOrUpdate(instance);
        }

        /// <summary>
        /// Deletes the given entity from the appropriate repository
        /// </summary>
        public void Delete<T>(T instance) where T : class, IDomainEntity
        {
            GetRepository<T>().Delete(instance);
        }

        /// <summary>
        /// Retrieves the entity with the given id from the appropriate repository
        /// </summary>
        public T GetById<T>(long id) where T : class, IDomainEntity
        {
            return GetRepository<T>().GetById(id);
        }

        /// <summary>
        /// Returns the first entity to match the given criteria
        /// </summary>
        public T FindOne<T>(DetachedCriteria criteria) where T : class, IDomainEntity
        {
            return GetRepository<T>().FindOne(criteria);
        }

        /// <summary>
        /// Returns the first entity to match the given criteria
        /// </summary>
        public T FindFirst<T>(DetachedCriteria criteria) where T : class, IDomainEntity
        {
            return GetRepository<T>().FindFirst(criteria);
        }

        /// <summary>
        /// Returns the first entity to match the given criteria, ordered by the given order
        /// </summary>
        public T FindFirst<T>(DetachedCriteria criteria, Order order) where T : class, IDomainEntity
        {
            return GetRepository<T>().FindFirst(criteria, order);
        }

        /// <summary>
        /// Returns all entities from the appropriate repository
        /// </summary>
        public IQueryable<T> FindAll<T>() where T : class, IDomainEntity
        {
            return GetRepository<T>().FindAll();
        }

        /// <summary>
        /// Creates a criteria for the appropriate type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public ICriteria CreateCriteria<T>(String alias = null) where T : class, IDomainEntity
        {
            return GetRepository<T>().CreateCriteria(alias);
        }

        /// <summary>
        /// Returns a repository for the specific type.
        /// </summary>
        public IRepository<T> GetRepository<T>() where T : class, IDomainEntity
        {
            // Try and find the repository in our map
            Type type = typeof(T);
            if (RepositoryMap.Keys.Contains(type))
            {
                return RepositoryMap[type] as IRepository<T>;
            }

            IRepository<T> repository = CreateRepository<T>();
            RepositoryMap.Add(type, repository);
            return repository;
        }

        /// <summary>
        /// Returns each entity that matches the given criteria
        /// </summary>
        public IList<T> FindAll<T>(DetachedCriteria criteria) where T : class, IDomainEntity
        {
            return GetRepository<T>().FindAll<T>(criteria);
        }

        /// <summary>
        /// 
        /// </summary>
        public IList<TResult> FindAll<TEntity, TResult>(DetachedCriteria criteria) where TEntity : class, IDomainEntity
        {
            return GetRepository<TEntity>().FindAll<TResult>(criteria);
        }

        /// <summary>
        /// Returns each entity that matches the given criteria, and orders the results
        /// according to the given Orders
        /// </summary>
        public IList<T> FindAll<T>(DetachedCriteria criteria, params Order[] orders) where T : class, IDomainEntity
        {
            return GetRepository<T>().FindAll<T>(criteria, orders);
        }

        /// <summary>
        /// Returns each entity that matches the given criteria, and orders the results
        /// according to the given Orders
        /// </summary>
        public IList<TResult> FindAll<TEntity, TResult>(DetachedCriteria criteria, params Order[] orders) where TEntity : class, IDomainEntity
        {
            return GetRepository<TEntity>().FindAll<TResult>(criteria, orders);
        }

        /// <summary>
        /// Returns each entity that matches the given criteria, with support for paging,
        /// and orders the results according to the given Orders
        /// </summary>
        public IList<T> FindAll<T>(DetachedCriteria criteria, int firstResult, int numberOfResults, params Order[] orders) where T : class, IDomainEntity
        {
            return GetRepository<T>().FindAll<T>(criteria, firstResult, numberOfResults, orders);
        }

        /// <summary>
        /// Returns each entity that matches the given criteria, with support for paging,
        /// and orders the results according to the given Orders
        /// </summary>
        public IList<TResult> FindAll<TEntity, TResult>(DetachedCriteria criteria, int firstResult, int numberOfResults, params Order[] orders) where TEntity : class, IDomainEntity
        {
            return GetRepository<TEntity>().FindAll<TResult>(criteria, firstResult, numberOfResults, orders);
        }

        /// <summary>
        /// Returns the total number of entities that match the given criteria
        /// </summary>
        public long Count<T>(DetachedCriteria criteria) where T : class, IDomainEntity
        {
            return GetRepository<T>().Count(criteria);
        }
        #endregion // IRepositoryLocator Members

        #region Unmanaged Queries
        /// <summary>
        /// Creates an unmanaged query
        /// </summary>
        public IQuery CreateUnmanagedHQLQuery(string sql)
        {
            return Session.CreateQuery(sql);
        }

        /// <summary>
        /// Creates an unmanaged query
        /// </summary>
        public ISQLQuery CreateUnmanagedSQLQuery(string sql)
        {
            return Session.CreateSQLQuery(sql);
        }
        #endregion // Unmanaged Queries
    } // RepositoryLocator
}
