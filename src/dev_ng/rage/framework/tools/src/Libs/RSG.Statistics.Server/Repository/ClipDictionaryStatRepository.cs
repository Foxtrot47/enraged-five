﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Domain.Entities.GameAssetStats.Animation;

namespace RSG.Statistics.Server.Repository
{
    /// <summary>
    /// 
    /// </summary>
    internal class ClipDictionaryStatRepository : GameAssetStatRepository<ClipDictionaryStat, ClipDictionary>, IClipDictionaryStatRepository
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public ClipDictionaryStatRepository(ISession session)
            : base(session)
        {
        }
        #endregion // Constructor(s)

        #region IClipDicionaryStatRepository Methods
        /// <summary>
        /// Returns all stats associated with the specified build.
        /// </summary>
        public override IEnumerable<ClipDictionaryStat> GetStatsForBuild(Build build)
        {
            return GetStatsForBuild(build, true, true);
        }

        /// <summary>
        /// Returns all clip dictionary stats associated with the specified build optionally retrieving clips and anim data.
        /// </summary>
        public IEnumerable<ClipDictionaryStat> GetStatsForBuild(Build build, bool retrieveClips, bool retrieveAnims)
        {
            if (retrieveAnims)
            {
                ICriteria animCriteria =
                        Session.CreateCriteria<AnimationStat>()
                            .CreateAlias("Asset", "a")
                            .Add(Restrictions.Eq("Build", build));
                animCriteria.Future<AnimationStat>();
            }

            if (retrieveClips)
            {
                ICriteria clipCriteria =
                    Session.CreateCriteria<ClipStat>()
                        .CreateAlias("Asset", "a")
                        .Add(Restrictions.Eq("Build", build));
                if (retrieveAnims)
                {
                    clipCriteria.SetFetchMode("AnimationStats", FetchMode.Join)
                                .SetResultTransformer(Transformers.DistinctRootEntity);
                }
                clipCriteria.Future<ClipStat>();
            }

            ICriteria clipDictCriteria =
                Session.CreateCriteria<ClipDictionaryStat>()
                    .CreateAlias("Asset", "a")
                    .SetFetchMode("Category", FetchMode.Join)
                    .Add(Restrictions.Eq("Build", build));
            if (retrieveClips)
            {
                clipDictCriteria.SetFetchMode("ClipStats", FetchMode.Join)
                                .SetResultTransformer(Transformers.DistinctRootEntity);
            }
            return clipDictCriteria.Future<ClipDictionaryStat>();
        }
        #endregion // IClipDictionaryStatRepository Methods
    } // ClipDictionaryStatRepository
}
