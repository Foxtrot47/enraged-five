﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using RSG.Model.Common.Animation;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Domain.Entities.GameAssetStats.Animation;
using AnimModel = RSG.Model.Animation;

namespace RSG.Statistics.Server.Repository
{
    /// <summary>
    /// 
    /// </summary>
    internal class CutsceneStatRepository : GameAssetStatRepository<CutsceneStat, Cutscene>, ICutsceneStatRepository
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public CutsceneStatRepository(ISession session)
            : base(session)
        {
        }
        #endregion // Constructor(s)

        #region ICutsceneStatRepository Methods
        /// <summary>
        /// Returns all stats associated with the specified build.
        /// </summary>
        public override IEnumerable<CutsceneStat> GetStatsForBuild(Build build)
        {
            // Get the parts information.
            ICriteria partsCriteria =
                    Session.CreateCriteria<CutscenePartStat>()
                        .CreateAlias("CutsceneStat", "cs")
                        .Add(Restrictions.Eq("cs.Build", build));
            partsCriteria.Future<CutscenePartStat>();

            // Get the platform stat information.
            ICriteria platformStatsCriteria =
                    Session.CreateCriteria<CutscenePlatformSectionStat>()
                        .CreateAlias("AssetStat", "as")
                        .Add(Restrictions.Eq("as.Build", build));
            platformStatsCriteria.Future<CutscenePlatformSectionStat>();

            // Get the cutscene information.
            ICriteria cutsceneCriteria =
                Session.CreateCriteria<CutsceneStat>()
                    .CreateAlias("Asset", "a")
                    .SetFetchMode("PartStats", FetchMode.Join)
                    .SetFetchMode("PlatformSectionStats", FetchMode.Join)
                    .Add(Restrictions.Eq("Build", build))
                    .SetResultTransformer(Transformers.DistinctRootEntity);
            return cutsceneCriteria.Future<CutsceneStat>();
        }

        /// <summary>
        /// Creates a cutscene collection for the specified build.
        /// </summary>
        /// <param name="build"></param>
        /// <returns></returns>
        public ICutsceneCollection CreateCollectionForBuild(Build build)
        {
            ICutsceneCollection collection = new AnimModel.DbCutsceneCollection(build.Identifier);

            foreach (CutsceneStat stat in GetStatsForBuild(build))
            {
                // Generate the parts list.
                IList<ICutscenePart> parts = new List<ICutscenePart>();
                foreach (CutscenePartStat partStat in stat.PartStats)
                {
                    parts.Add(new AnimModel.CutscenePart(partStat.Name, partStat.CutFile, partStat.Duration));
                }

                // Generate the platform stats.
                IDictionary<RSG.Platform.Platform, ICutscenePlatformStat> platformStats = new Dictionary<RSG.Platform.Platform, ICutscenePlatformStat>();
                foreach (CutscenePlatformSectionStat sectionStat in stat.PlatformSectionStats)
                {
                    RSG.Platform.Platform platform = sectionStat.Platform.Value.Value;
                    ICutscenePlatformStat platformStat;

                    if (!platformStats.TryGetValue(platform, out platformStat))
                    {
                        platformStat = new AnimModel.CutscenePlatformStat(sectionStat.DataPath);
                        platformStats[platform] = platformStat;
                    }

                    platformStat.Sections.Add(new AnimModel.CutscenePlatformSectionStat(sectionStat.Index, sectionStat.PhysicalSize, sectionStat.VirtualSize));
                }

                String missionId = (stat.Asset.Mission != null ? stat.Asset.Mission.MissionId : null);
                AnimModel.DbCutscene model = new AnimModel.DbCutscene(stat.Asset.Name, stat.Asset.FriendlyName, missionId,
                    build.Identifier, stat.ExportZipFilepath, stat.IsConcat, stat.HasBranch, parts, platformStats);
                collection.Add(model);
            }

            return collection;
        }
        #endregion // ICutsceneStatRepository Methods
    } // CutsceneStatRepository
}
