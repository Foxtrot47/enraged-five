﻿using System.Net;
using System.ServiceModel;
using System.ServiceModel.Web;
using AutoMapper;
using RSG.Base.Configuration.Services;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Dto.GameAssets;
using RSG.Statistics.Common.ServiceContract;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Server.Repository;

namespace RSG.Statistics.Server.Services
{
    /// <summary>
    /// 
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class MissionService : GameAssetServiceBase<Mission, MissionDto, MissionDtos>, IMissionService
    {
        #region Constructor(s)
        /// <summary>
        /// Constructor taking a server configuration object.
        /// </summary>
        /// <param name="server"></param>
        public MissionService(IServiceHostConfig config)
            : base(config)
        {
        }
        #endregion // Constructor(s)

        #region GameAssetServiceBase Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        protected override MissionDto PutAssetCommand(IRepositoryLocator locator, string identifier, MissionDto dto)
        {
            if (identifier != dto.Identifier)
            {
                throw new WebFaultException<string>("Mission identifier specified in the Uri doesn't match the dto.", HttpStatusCode.BadRequest);
            }

            // Get the asset from the database
            Mission asset = GetAssetByIdentifier<Mission>(locator, identifier, false);

            // If it doesn't exist, create a new one
            if (asset == null)
            {
                asset = new Mission();
                asset.Id = 0;
            }

            // Need to update the game type manually (which is why we need to override in the first place).
            asset.GameType = GetEnumReferenceByValue(locator, dto.GameType.Value);

            // Update the asset, save it to the db and then return a dto version of it
            asset.Update(dto);

            locator.SaveOrUpdate(asset);
            return Mapper.Map<Mission, MissionDto>(asset);
        }

        #endregion // GameAssetServiceBase Overrides
    } // MissionService
}
