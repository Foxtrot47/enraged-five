﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using NHibernate.Transform;
using RSG.Base.Logging;
using RSG.Platform;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Model.GameAssets;
using RSG.Statistics.Common.Report;
using RSG.Statistics.Domain.Entities;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Domain.Entities.GameAssetStats;
using RSG.Statistics.Server.Extensions;
using RSG.Statistics.Server.Repository;
using AssModel = RSG.Statistics.Common.Model.GameAssets;

namespace RSG.Statistics.Server.Report.GameAssets
{
    /// <summary>
    /// Report that provides historical data for map section asset stats.
    /// </summary>
    [Export(typeof(IStatsReport))]
    internal class HistoricalMapSectionAssetBreakdown : StatsReportBase<List<AssModel.HistoricalSectionAssetStat>>
    {
        #region Constants
        /// <summary>
        /// Name of the report.
        /// </summary>
        private const String c_name = "GameAssetsHistoricalMapSectionAssetBreakdown";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Name of the platform we wish to see the data for.
        /// </summary>
        [ReportParameter(Required = true)]
        public String PlatformName { get; set; }

        /// <summary>
        /// Convenience property that transforms the platform names to platform enum values.
        /// </summary>
        public RSG.Platform.Platform Platform
        {
            get
            {
                RSG.Platform.Platform platform;
                if (Enum.TryParse(PlatformName, out platform))
                {
                    return platform;
                }
                return RSG.Platform.Platform.Independent;
            }
        }

        /// <summary>
        /// Name of the level we wish to see the data for.
        /// </summary>
        [ReportParameter(Required = true)]
        public String LevelIdentifier { get; set; }

        /// <summary>
        /// Optional file types to restrict the results to.
        /// </summary>
        [ReportParameter]
        public List<String> FileTypeNames { get; set; }

        /// <summary>
        /// Convenience property that transforms the platform names to platform enum values.
        /// </summary>
        public IList<FileType> FileTypes
        {
            get
            {
                IList<FileType> fileTypes = new List<FileType>();
                foreach (String fileTypeName in FileTypeNames)
                {
                    FileType fileType;
                    if (Enum.TryParse(fileTypeName, out fileType))
                    {
                        fileTypes.Add(fileType);
                    }
                }
                return fileTypes;
            }
        }
        #endregion // Properties

        #region Private Classes
        /// <summary>
        /// Temp class for nhibernate query results.
        /// </summary>
        private class HistoricalResult
        {
            public String MapSectionName { get; set; }
            public String BuildIdentifier { get; set; }
            public long PhysicalSize { get; set; }
            public long VirtualSize { get; set; }
        }
        #endregion // Private Classes

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public HistoricalMapSectionAssetBreakdown()
            : base(c_name)
        {
        }
        #endregion // Constructor(s)

        #region IReportDataProvider Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        protected override List<AssModel.HistoricalSectionAssetStat> Generate(IRepositoryLocator locator, ILog log, IStatsServer server)
        {
            // Convert the filters to a db object.
            Level level = locator.GetAssetByIdentifier<Level>(LevelIdentifier);
            EnumReference<RSG.Platform.Platform> platform = locator.GetEnumReferenceByValue(Platform);

            // Create the query.
            DetachedCriteria criteria =
                DetachedCriteria.For<MapSectionPlatformStat>()
                    .CreateAlias("MapSectionStat", "mss")
                    .CreateAlias("mss.MapSection", "ms")
                    .CreateAlias("mss.Build", "b")
                    .SetProjection(Projections.ProjectionList()
                        .Add(Projections.Property("ms.Name"), "MapSectionName")
                        .Add(Projections.Property("b.Identifier"), "BuildIdentifier")
                        .Add(Projections.Sum("PhysicalSize"), "PhysicalSize")
                        .Add(Projections.Sum("VirtualSize"), "VirtualSize")
                        .Add(Projections.GroupProperty("mss.MapSection"))
                        .Add(Projections.GroupProperty("mss.Build"))
                        )
                    .Add(Restrictions.Eq("Platform", platform))
                    .Add(Restrictions.Eq("mss.Level", level))
                    .SetResultTransformer(Transformers.AliasToBean(typeof(HistoricalResult)));

            if (FileTypes.Any())
            {
                IList<EnumReference<FileType>> fileTypes = new List<EnumReference<FileType>>();
                foreach (FileType fileType in FileTypes)
                {
                    fileTypes.Add(locator.GetEnumReferenceByValue(fileType));
                }
                criteria.Add(Restrictions.In("FileType", fileTypes.ToArray()));
            }

            // Execute the query.
            IList<HistoricalResult> results =
                locator.FindAll<MapSectionPlatformStat, HistoricalResult>(criteria);

            // Convert the results.
            List<HistoricalSectionAssetStat> historicalStats = new List<HistoricalSectionAssetStat>();

            foreach (IGrouping<String, HistoricalResult> sectionGroup in results.GroupBy(item => item.MapSectionName))
            {
                HistoricalSectionAssetStat stat = new HistoricalSectionAssetStat(sectionGroup.Key);
                historicalStats.Add(stat);

                foreach (HistoricalResult result in sectionGroup)
                {
                    stat.PerBuildStats.Add(result.BuildIdentifier, new AssetStat(result.PhysicalSize, result.VirtualSize));
                }
            }

            return historicalStats;
        }
        #endregion // IReportDataProvider Implementation
    } // HistoricalMapSectionAssetBreakdown
}
