﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using AutoMapper;
using NHibernate.Criterion;
using RSG.Model.Common;
using CommonAnim = RSG.Model.Common.Animation;
using RSG.Statistics.Common;
using RSG.Statistics.Common.ServiceContract;
using RSG.Statistics.Domain.Entities;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Server.Extensions;
using RSG.Statistics.Server.Repository;
using AssetDto = RSG.Statistics.Common.Model.GameAssets;
using RSG.Statistics.Common.Config;
using RSG.Model.Common.Weapon;
using RSG.Base.Configuration.Services;

namespace RSG.Statistics.Server.Services
{
    /// <summary>
    /// 
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class GameAssetService : ServiceBase, IGameAssetService
    {
        #region Constructor(s)
        /// <summary>
        /// Constructor taking a server configuration object.
        /// </summary>
        /// <param name="server"></param>
        public GameAssetService(IServiceHostConfig config)
            : base(config)
        {
        }
        #endregion // Constructor(s)
        
        #region Animations
        /// <summary>
        /// Updates or creates a list of ClipDictionaries ( animations ).
        /// </summary>
        public void UpdateClipDictionaries(RSG.Model.Common.Animation.IClipDictionaryCollection clipDictionaries)
        {
            // Loop over all the dictionaries adding them and their clips to the db.
            foreach (RSG.Model.Common.Animation.IClipDictionary clipDictionary in clipDictionaries.ClipDictionaries)
            {
                ExecuteCommand(locator => UpdateClipDictionariesCommand(locator, clipDictionary));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void UpdateClipDictionariesCommand(IRepositoryLocator locator, RSG.Model.Common.Animation.IClipDictionary clipDictionarySubmission)
        {
            // Get the repo's we need for this operation.
            IGameAssetRepository<ClipDictionary> clipDictRepo = (IGameAssetRepository<ClipDictionary>)locator.GetRepository<ClipDictionary>();
            IGameAssetRepository<Clip> clipRepo = (IGameAssetRepository<Clip>)locator.GetRepository<Clip>();
            IGameAssetRepository<Animation> animRepo = (IGameAssetRepository<Animation>)locator.GetRepository<Animation>();
            IEnumRepository<CommonAnim.ClipDictionaryCategory> clipDictCategoryRepo =
                (IEnumRepository<CommonAnim.ClipDictionaryCategory>)locator.GetRepository<EnumReference<CommonAnim.ClipDictionaryCategory>>();

            ClipDictionary clipDict = clipDictRepo.GetOrCreateByName(clipDictionarySubmission.Name);
            clipDict.Category = clipDictCategoryRepo.GetByValue(clipDictionarySubmission.Category);
            locator.Save(clipDict);

            // Now iterate through the model updating the database with other DB table objects
            foreach (RSG.Model.Common.Animation.IClip clipSubmission in clipDictionarySubmission.Clips)
            {
                clipRepo.GetOrCreateByName(clipSubmission.Name);

                // Now iterate over all the anims as well.
                foreach (RSG.Model.Common.Animation.IAnimation animSubmission in clipSubmission.Animations)
                {
                    animRepo.GetOrCreateByName(animSubmission.Name);
                }
            }
        }

        #endregion Animations

        #region Cash Packs
        /// <summary>
        /// Updates the list of cash-packs that are currently active in game.
        /// </summary>
        public void UpdateCashPacks(IList<AssetDto.CashPack> cashPacks)
        {
            ExecuteCommand(locator => UpdateCashPacksCommand(locator, cashPacks));
        }

        /// <summary>
        /// As called by UpdateUnlocks.
        /// </summary>
        private void UpdateCashPacksCommand(IRepositoryLocator locator, IList<AssetDto.CashPack> cashPacks)
        {
            IGameAssetRepository<CashPack> cashPackRepo = (IGameAssetRepository<CashPack>)locator.GetRepository<CashPack>();

            IList<CashPack> activeCashPacks = new List<CashPack>();
            foreach (AssetDto.CashPack cashPackSub in cashPacks)
            {
                CashPack cashPack = cashPackRepo.GetByHash(cashPackSub.Hash);
                if (cashPack == null)
                {
                    cashPack = new CashPack();
                    cashPack.Identifier = cashPackSub.Hash.ToString();
                    cashPack.Name = cashPackSub.GameName;
                }

                // Update the assets details and save it to the db.
                cashPack.InGameAmount = cashPackSub.InGameAmount;
                cashPack.Active = true;

                activeCashPacks.Add(cashPack);
                cashPackRepo.SaveOrUpdate(cashPack);
            }

            IDictionary<uint, CashPack> cashPackLookup = cashPackRepo.CreateAssetLookup();
            foreach (CashPack cashPack in cashPackLookup.Values.Except(activeCashPacks))
            {
                cashPack.Active = false;
                cashPackRepo.Save(cashPack);
            }
        }
        #endregion // Cash Packs

        #region Cutscenes
        /// <summary>
        /// Updates or creates a list of cutscenes.
        /// </summary>
        public void UpdateCutscenes(CommonAnim.ICutsceneCollection cutscenes)
        {
            ExecuteCommand(locator => UpdateCutscenesCommand(locator, cutscenes));
        }
        
        /// <summary>
        /// As called by UpdateUnlocks.
        /// </summary>
        private void UpdateCutscenesCommand(IRepositoryLocator locator, CommonAnim.ICutsceneCollection cutsceneSubs)
        {
            IGameAssetRepository<Cutscene> cutsceneRepo = (IGameAssetRepository<Cutscene>)locator.GetRepository<Cutscene>();
            IMissionRepository missionRepo = (IMissionRepository)locator.GetRepository<Mission>();

            foreach (CommonAnim.ICutscene cutsceneSub in cutsceneSubs)
            {
                Cutscene cutscene = cutsceneRepo.GetByHash(cutsceneSub.Hash);
                if (cutscene == null)
                {
                    cutscene = new Cutscene();
                    cutscene.Identifier = cutsceneSub.Hash.ToString();
                }

                // Update the shop's details and save it to the db.
                cutscene.Name = cutsceneSub.Name;
                cutscene.Duration = cutsceneSub.TotalDuration;
                cutscene.FriendlyName = cutsceneSub.FriendlyName;
                cutscene.Mission = missionRepo.GetByMissionId(cutsceneSub.MissionId);
                cutscene.HasBranch = cutsceneSub.HasBranch;
                cutsceneRepo.SaveOrUpdate(cutscene);
            }
        }
        #endregion // Cutscenes

        #region Draw Lists
        /// <summary>
        /// Updates the list of draw lists.
        /// </summary>
        public void UpdateDrawLists(IList<AssetDto.GameAsset> drawLists)
        {
            ExecuteCommand(locator => UpdateGameAssetsCommand<DrawList>(locator, drawLists));
        }

        /// <summary>
        /// Returns a list of all draw lists.
        /// </summary>
        /// <returns></returns>
        public IList<AssetDto.GameAsset> GetDrawLists()
        {
            return ExecuteReadOnlyCommand(locator => GetGameAssetsCommand<DrawList>(locator));
        }
        #endregion // Draw Lists

        #region Memory Pools, Stores & Heaps
        /// <summary>
        /// UpdateMemoryPools
        /// - given a hash dictionary of hash/name kev val pairs, update or add to the database.
        /// </summary>
        /// <param name="memoryPoolsDictionary"></param>
        public void UpdateMemoryPools(IDictionary<uint, String> memoryPoolsDictionary)
        {
            ExecuteCommand(locator => UpdateGameAssetsCommand<MemoryPool>(locator, memoryPoolsDictionary));
        }

        /// <summary>
        /// UpdateMemoryStores
        /// - given a hash dictionary of hash/name kev val pairs, update or add to the database.
        /// </summary>
        /// <param name="memoryStoresDictionary"></param>
        public void UpdateMemoryStores(IDictionary<uint, String> memoryStoresDictionary)
        {
            ExecuteCommand(locator => UpdateGameAssetsCommand<MemoryStore>(locator, memoryStoresDictionary));
        }

        /// <summary>
        /// UpdateMemoryHeaps
        /// - given a hash dictionary of hash/name kev val pairs, update or add to the database.
        /// </summary>
        /// <param name="memoryStoresDictionary"></param>
        public void UpdateMemoryHeaps(IDictionary<uint, String> memoryHeapsDictionary)
        {
            ExecuteCommand(locator => UpdateGameAssetsCommand<MemoryHeap>(locator, memoryHeapsDictionary));
        }
        #endregion // Memory Heaps

        #region Mini-Game Variants
        /// <summary>
        /// Updates the list of mini-game variants.
        /// </summary>
        public void UpdateMiniGameVariants(IList<AssetDto.MiniGameVariant> miniGames)
        {
            ExecuteCommand(locator => UpdateMiniGameVariantsCommand(locator, miniGames));
        }
        
        /// <summary>
        /// As called by UpdateUnlocks.
        /// </summary>
        private void UpdateMiniGameVariantsCommand(IRepositoryLocator locator, IList<AssetDto.MiniGameVariant> miniGameSubs)
        {
            IGameAssetRepository<MiniGameVariant> miniGameRepo = (IGameAssetRepository<MiniGameVariant>)locator.GetRepository<MiniGameVariant>();
            IEnumRepository<MatchType> matchTypeRepo = (IEnumRepository<MatchType>)locator.GetRepository<EnumReference<MatchType>>();

            foreach (AssetDto.MiniGameVariant assetSub in miniGameSubs)
            {
                MiniGameVariant asset = miniGameRepo.GetByHash(assetSub.Hash);
                if (asset == null)
                {
                    asset = new MiniGameVariant();
                    asset.Identifier = assetSub.Hash.ToString();
                    asset.Name = assetSub.GameName;
                }

                // Update the assets details and save it to the db.
                asset.FriendlyName = assetSub.FriendlyName;
                asset.MatchType = matchTypeRepo.GetByValue(assetSub.MatchType);
                asset.X = assetSub.Location.X;
                asset.Y = assetSub.Location.Y;
                asset.Z = assetSub.Location.Z;

                miniGameRepo.SaveOrUpdate(asset);
            }
        }
        #endregion // Mini-Game Variants

        #region Missions
        /// <summary>
        /// Returns a list of all missions.
        /// </summary>
        /// <returns></returns>
        public RSG.Model.Common.Mission.IMissionCollection GetMissions()
        {
            return ExecuteReadOnlyCommand(locator => GetMissionsCommand(locator));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <returns></returns>
        private RSG.Model.Common.Mission.IMissionCollection GetMissionsCommand(IRepositoryLocator locator)
        {
            DetachedCriteria missionCriteria = DetachedCriteria.For<Mission>();
            IList<Mission> dbMissions = locator.FindAll<Mission>(missionCriteria);

            IDictionary<long, RSG.Model.Common.Mission.Mission> missionLookup = new Dictionary<long, RSG.Model.Common.Mission.Mission>();
            RSG.Model.Common.Mission.MissionCollection missionCollection = new RSG.Model.Common.Mission.MissionCollection();

            foreach (Mission dbMission in dbMissions)
            {
                RSG.Model.Common.Mission.Mission modelMission = new RSG.Model.Common.Mission.Mission(dbMission.Name);
                modelMission.Description = dbMission.Description;
                modelMission.MissionId = dbMission.MissionId;
                modelMission.ScriptName = dbMission.ScriptName;
                modelMission.ProjectedAttempts = dbMission.ProjectedAttempts;
                modelMission.ProjectedAttemptsMin = dbMission.ProjectedAttemptsMin;
                modelMission.ProjectedAttemptsMax = dbMission.ProjectedAttemptsMax;
                modelMission.Singleplayer = (dbMission.GameType.Value == GameType.Singleplayer);
                modelMission.Active = dbMission.Active;
                modelMission.InBugstar = dbMission.InBugstar;
                modelMission.Category = (RSG.Model.Common.Mission.MissionCategory)dbMission.Category.Value;

                missionCollection.Add(modelMission);
                missionLookup.Add(dbMission.Id, modelMission);
            }

            // Associate the checkpoints with the missions.
            DetachedCriteria checkpointCriteria = DetachedCriteria.For<MissionCheckpoint>().Add(Expression.IsNotNull("Index"));
            IList<MissionCheckpoint> dbCheckpoints = locator.FindAll<MissionCheckpoint>(checkpointCriteria, Order.Asc("Index"));

            foreach (IGrouping<long, MissionCheckpoint> groupedDbCheckpoints in dbCheckpoints.GroupBy(item => item.Mission.Id))
            {
                if (missionLookup.ContainsKey(groupedDbCheckpoints.Key))
                {
                    RSG.Model.Common.Mission.Mission modelMission = missionLookup[groupedDbCheckpoints.Key];
                    modelMission.Checkpoints = new List<Model.Common.Mission.IMissionCheckpoint>();

                    foreach (MissionCheckpoint dbCheckpoint in groupedDbCheckpoints)
                    {
                        RSG.Model.Common.Mission.MissionCheckpoint modelCheckpoint =
                            new RSG.Model.Common.Mission.MissionCheckpoint(dbCheckpoint.Name);
                        modelCheckpoint.ProjectedAttempts = dbCheckpoint.ProjectedAttempts;
                        modelCheckpoint.ProjectedAttemptsMin = dbCheckpoint.ProjectedAttemptsMin;
                        modelCheckpoint.ProjectedAttemptsMax = dbCheckpoint.ProjectedAttemptsMax;
                        modelCheckpoint.InBugstar = dbCheckpoint.InBugstar;
                        modelCheckpoint.Index = dbCheckpoint.Index;
                        modelMission.Checkpoints.Add(modelCheckpoint);
                    }
                }
            }

            return missionCollection;
        }

        /// <summary>
        /// Updates or creates a list of missions.
        /// </summary>
        public void UpdateMissions(RSG.Model.Common.Mission.IMissionCollection missions)
        {
            ExecuteCommand(locator => UpdateMissionsCommand(locator, missions));
        }

        /// <summary>
        /// 
        /// </summary>
        private void UpdateMissionsCommand(IRepositoryLocator locator, RSG.Model.Common.Mission.IMissionCollection missions)
        {
            IList<Mission> updatedMissions = new List<Mission>();

            // Add/update the missions.
            foreach (RSG.Model.Common.Mission.Mission modelMission in missions)
            {
                // Attempt to retrieve an existing mission from the db.
                DetachedCriteria criteria =
                    DetachedCriteria.For<Mission>().Add(Expression.Eq("MissionId", modelMission.MissionId));
                Mission dbMission = locator.FindFirst<Mission>(criteria);
                if (dbMission == null)
                {
                    dbMission = new Mission();
                }

                // Update the db's version of the mission.
                Mapper.Map(modelMission, dbMission);
                if (dbMission.Identifier == "0")
                {
                    continue;
                }

                dbMission.GameType = GetEnumReferenceByValue(locator, (modelMission.Singleplayer ? GameType.Singleplayer : GameType.Multiplayer));
                if (dbMission.Category == null)
                {
                    dbMission.Category = GetEnumReferenceByValue(locator, modelMission.Category);
                }

                // Sync up the checkpoints.
                IList<MissionCheckpoint> updatedCheckpoints = new List<MissionCheckpoint>();

                foreach (RSG.Model.Common.Mission.IMissionCheckpoint modelCheckpoint in modelMission.Checkpoints)
                {
                    // Does the mission have this checkpoint?
                    MissionCheckpoint dbCheckpoint = dbMission.Checkpoints.FirstOrDefault(item => item.Index == modelCheckpoint.Index);
                    if (dbCheckpoint == null)
                    {
                        dbCheckpoint = new MissionCheckpoint();
                        dbMission.Checkpoints.Add(dbCheckpoint);
                    }

                    Mapper.Map(modelCheckpoint, dbCheckpoint);
                    dbCheckpoint.Mission = dbMission;
                    dbCheckpoint.InBugstar = true;

                    updatedCheckpoints.Add(dbCheckpoint);
                }

                // Null out the index of checkpoints that are no longer valid.
                foreach (MissionCheckpoint dbCheckpoint in dbMission.Checkpoints.Except(updatedCheckpoints))
                {
                    dbCheckpoint.InBugstar = false;
                }

                // Saving the mission will ensure all the checkpoints are updated as well.
                locator.SaveOrUpdate(dbMission);

                updatedMissions.Add(dbMission);
            }

            // Null out the index of checkpoints that are no longer valid.
            IList<Mission> allMissions = locator.CreateCriteria<Mission>().List<Mission>();

            foreach (Mission dbMission in allMissions.Except(updatedMissions))
            {
                dbMission.InBugstar = false;
                locator.SaveOrUpdate(dbMission);
            }
        }

        /// <summary>
        /// Deletes a single mission.
        /// </summary>
        public void DeleteMission(String identifier)
        {
            ExecuteCommand(locator => DeleteMissionCommand(locator, identifier));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="identifier"></param>
        private void DeleteMissionCommand(IRepositoryLocator locator, String identifier)
        {
            throw new NotImplementedException();
        }
        #endregion // Missions

        #region Movies
        /// <summary>
        /// Updates the list of movies.
        /// </summary>
        public void UpdateMovies(IList<AssetDto.FriendlyGameAsset> movies)
        {
            ExecuteCommand(locator => UpdateGameAssetsCommand<Movie>(locator, movies));
        }
        #endregion // Movies

        #region Properties
        /// <summary>
        /// Updates the list of properties.
        /// </summary>
        public void UpdateProperties(IDictionary<uint, String> properties)
        {
            ExecuteCommand(locator => UpdateGameAssetsCommand<RSG.Statistics.Domain.Entities.GameAssets.Property>(locator, properties));
        }
        #endregion // Properties

        #region Ranks
        /// <summary>
        /// Updates the list of ranks.
        /// </summary>
        public void UpdateRanks(IList<AssetDto.Rank> ranks)
        {
            ExecuteCommand(locator => UpdateRanksCommand(locator, ranks));
        }

        /// <summary>
        /// As called by UpdateUnlocks.
        /// </summary>
        private void UpdateRanksCommand(IRepositoryLocator locator, IList<AssetDto.Rank> rankSubs)
        {
            if (!rankSubs.Any())
            {
                throw new ArgumentException("No ranks submitted!  This would clear out the list of ranks so aborting this submission.");
            }

            // Clear out ranks that aren't present in the submitted list of ranks.
            IDictionary<uint, AssetDto.Rank> rankSubLookup = rankSubs.ToDictionary(item => item.Level);
            IList<Rank> existingRanks = locator.CreateCriteria<Rank>().List<Rank>();
            IDictionary<uint, Rank> existingRankLookup = new Dictionary<uint, Rank>();

            foreach (Rank rank in existingRanks)
            {
                // If the submissions doen't contain an rank with this level, then delete it from the db.
                if (!rankSubLookup.ContainsKey(rank.Level))
                {
                    locator.Delete(rank);
                }
                else
                {
                    // Otherwise keep track of it for when we process the submissions.
                    existingRankLookup[rank.Level] = rank;
                }
            }

            // Create the new list of unlocks.
            foreach (AssetDto.Rank rankSub in rankSubs)
            {
                Rank rank;
                if (!existingRankLookup.TryGetValue(rankSub.Level, out rank))
                {
                    rank = new Rank();
                    rank.Level = rankSub.Level;
                }

                rank.Experience = rankSub.Experience;
                rank.Name = rankSub.Name;
                locator.SaveOrUpdate(rank);
            }
        }
        #endregion // Ranks

        #region Radio Stations
        /// <summary>
        /// Updates the list of TV shows.
        /// </summary>
        public void UpdateRadioStations(IList<AssetDto.FriendlyGameAsset> radioStations)
        {
            ExecuteCommand(locator => UpdateGameAssetsCommand<RadioStation>(locator, radioStations));
        }
        #endregion // Radio Stations

        #region Shopping
        /// <summary>
        /// Updates all shop items.
        /// </summary>
        public void UpdateShops(IList<AssetDto.Shop> shopNames)
        {
            ExecuteCommand(locator => UpdateShopsCommand(locator, shopNames));
        }

        /// <summary>
        /// 
        /// </summary>
        private void UpdateShopsCommand(IRepositoryLocator locator, IList<AssetDto.Shop> shops)
        {
            IGameAssetRepository<Shop> gameAssetRepo = (IGameAssetRepository<Shop>)locator.GetRepository<Shop>();
            IEnumRepository<ShopType> unlockTypeRepo = (IEnumRepository<ShopType>)locator.GetRepository<EnumReference<ShopType>>();

            foreach (AssetDto.Shop shop in shops)
            {
                Shop asset = gameAssetRepo.GetByHash(shop.Hash);
                if (asset == null)
                {
                    asset = new Shop();
                    asset.Identifier = shop.Hash.ToString();
                }

                // Update the shop's details and save it to the db.
                asset.Name = shop.GameName;
                asset.FriendlyName = shop.FriendlyName;
                asset.ShopType = unlockTypeRepo.GetByValue(shop.ShopType);
                gameAssetRepo.SaveOrUpdate(asset);
            }
        }

        /// <summary>
        /// Updates or creates a dictionary of shop item hashes to shop item names.
        /// </summary>
        public void UpdateShopItems(IList<AssetDto.ShopItem> shopItems)
        {
            ExecuteCommand(locator => UpdateShopItemsCommand(locator, shopItems));
        }

        /// <summary>
        /// 
        /// </summary>
        private void UpdateShopItemsCommand(IRepositoryLocator locator, IList<AssetDto.ShopItem> shopItems)
        {
            IGameAssetRepository<ShopItem> gameAssetRepo = (IGameAssetRepository<ShopItem>)locator.GetRepository<ShopItem>();

            foreach (AssetDto.ShopItem shopItem in shopItems)
            {
                ShopItem asset = gameAssetRepo.GetByHash(shopItem.Hash);
                if (asset == null)
                {
                    asset = new ShopItem();
                    asset.Identifier = shopItem.Hash.ToString();
                }

                // Update the shop's details and save it to the db.
                asset.Name = shopItem.GameName;
                asset.FriendlyName = shopItem.FriendlyName;
                asset.Category = shopItem.Category;
                asset.SubCategory = shopItem.SubCategory;
                gameAssetRepo.SaveOrUpdate(asset);
            }
        }

        /// <summary>
        /// Returns a list of all shop items.
        /// </summary>
        /// <returns></returns>
        public IList<AssetDto.ShopItem> GetShopItems()
        {
            return ExecuteReadOnlyCommand(locator => GetShopItemsCommand(locator));
        }

        /// <summary>
        /// 
        /// </summary>
        private IList<AssetDto.ShopItem> GetShopItemsCommand(IRepositoryLocator locator)
        {
            IList<AssetDto.ShopItem> shopItems = new List<AssetDto.ShopItem>();

            IGameAssetRepository<ShopItem> gameAssetRepo = (IGameAssetRepository<ShopItem>)locator.GetRepository<ShopItem>();
            foreach (ShopItem item in gameAssetRepo.FindAll())
            {
                shopItems.Add(new AssetDto.ShopItem(item.Name, item.FriendlyName, item.Category, item.SubCategory));
            }

            return shopItems;
        }
        #endregion // Shopping

        #region TV Shows
        /// <summary>
        /// Updates the list of TV shows.
        /// </summary>
        public void UpdateTVShows(IList<AssetDto.FriendlyGameAsset> tvShows)
        {
            ExecuteCommand(locator => UpdateGameAssetsCommand<TVShow>(locator, tvShows));
        }
        #endregion // TV Shows

        #region Unlocks
        /// <summary>
        /// Updates the list of unlocks.
        /// </summary>
        public void UpdateUnlocks(IList<AssetDto.Unlock> unlocks)
        {
            ExecuteCommand(locator => UpdateUnlocksCommand(locator, unlocks));
        }

        /// <summary>
        /// As called by UpdateUnlocks.
        /// </summary>
        private void UpdateUnlocksCommand(IRepositoryLocator locator, IList<AssetDto.Unlock> unlockSubs)
        {
            if (!unlockSubs.Any())
            {
                throw new ArgumentException("No unlocks submitted!  This would clear out the list of unlocks so aborting this submission.");
            }

            // Clear out unlocks that aren't present in the submitted list of unlocks.
            IDictionary<Tuple<String, UnlockType>, AssetDto.Unlock> unlockSubLookup =
                new Dictionary<Tuple<String, UnlockType>, AssetDto.Unlock>();
            foreach (AssetDto.Unlock unlockSub in unlockSubs)
            {
                Tuple<String, UnlockType> key = Tuple.Create(unlockSub.Name, unlockSub.UnlockType);
                if (!unlockSubLookup.ContainsKey(key))
                {
                    unlockSubLookup[key] = unlockSub;
                }
            }

            IList<Unlock> existingUnlocks = locator.CreateCriteria<Unlock>().List<Unlock>();
            IDictionary<Tuple<String, UnlockType>, Unlock> existingUnlockLookup = new Dictionary<Tuple<String, UnlockType>, Unlock>();

            foreach (Unlock unlock in existingUnlocks)
            {
                // If the submissions doen't contain an unlock with this name, then delete it from the db.
                Tuple<String, UnlockType> key = Tuple.Create(unlock.Name, unlock.UnlockType.Value.Value);

                if (!unlockSubLookup.ContainsKey(key))
                {
                    locator.Delete(unlock);
                }
                else
                {
                    // Otherwise keep track of it for when we process the submissions.
                    existingUnlockLookup[key] = unlock;
                }
            }

            // Create the new list of unlocks.
            IEnumRepository<UnlockType> unlockTypeRepo = (IEnumRepository<UnlockType>)locator.GetRepository<EnumReference<UnlockType>>();

            foreach (AssetDto.Unlock unlockSub in unlockSubs)
            {
                Unlock unlock;
                if (!existingUnlockLookup.TryGetValue(Tuple.Create(unlockSub.Name, unlockSub.UnlockType), out unlock))
                {
                    unlock = new Unlock();
                    unlock.Name = unlockSub.Name;
                }

                unlock.Rank = unlockSub.Rank;
                unlock.UnlockType = unlockTypeRepo.GetByValue(unlockSub.UnlockType);
                locator.SaveOrUpdate(unlock);
            }
        }
        #endregion // Unlocks

        #region Vehicles
        /// <summary>
        /// Returns a list of all vehicles.
        /// </summary>
        public List<AssetDto.Vehicle> GetVehicles()
        {
            return ExecuteReadOnlyCommand(locator => GetVehiclesCommand(locator));
        }

        /// <summary>
        /// 
        /// </summary>
        private List<AssetDto.Vehicle> GetVehiclesCommand(IRepositoryLocator locator)
        {
            List<AssetDto.Vehicle> vehicleDtos = new List<AssetDto.Vehicle>();

            DetachedCriteria criteria = DetachedCriteria.For<Vehicle>();
            foreach (Vehicle vehicle in locator.FindAll<Vehicle>(criteria))
            {
                AssetDto.Vehicle dto = new AssetDto.Vehicle();
                dto.Identifier = vehicle.Identifier;
                dto.RuntimeName = vehicle.GameName;
                dto.FriendlyName = vehicle.FriendlyName;
                dto.ModelName = vehicle.Name;

                VehicleCategory category;
                if (Enum.TryParse(vehicle.Category, out category))
                {
                    dto.Category = category;
                }
                else
                {
                    dto.Category = VehicleCategory.Unknown;
                }

                vehicleDtos.Add(dto);
            }
            return vehicleDtos;
        }
        #endregion // Vehicles

        #region Weapons
        /// <summary>
        /// Returns a list of all weapons.
        /// </summary>
        public List<AssetDto.Weapon> GetWeapons()
        {
            return ExecuteReadOnlyCommand(locator => GetWeaponsCommand(locator));
        }

        /// <summary>
        /// 
        /// </summary>
        private List<AssetDto.Weapon> GetWeaponsCommand(IRepositoryLocator locator)
        {
            List<AssetDto.Weapon> weaponDtos = new List<AssetDto.Weapon>();

            DetachedCriteria criteria = DetachedCriteria.For<Weapon>();
            foreach (Weapon weapon in locator.FindAll<Weapon>(criteria))
            {
                AssetDto.Weapon dto = new AssetDto.Weapon();
                dto.Identifier = weapon.Identifier;
                dto.RuntimeName = weapon.Name;
                dto.FriendlyName = weapon.FriendlyName;
                dto.ProfileStatName = weapon.StatName;
                dto.ModelName = weapon.ModelName;

                WeaponCategory category;
                if (Enum.TryParse(weapon.Category, out category))
                {
                    dto.Category = category;
                }
                else
                {
                    dto.Category = WeaponCategory.Other;
                }

                weaponDtos.Add(dto);
            }
            return weaponDtos;
        }
        #endregion // Weapons

        #region Websites
        /// <summary>
        /// Updates the list of websites.
        /// </summary>
        public void UpdateWebsites(IDictionary<uint, String> websites)
        {
            ExecuteCommand(locator => UpdateGameAssetsCommand<WebSite>(locator, websites));
        }
        #endregion // Websites

        #region XP
        /// <summary>
        /// Updates the list of XP categories.
        /// </summary>
        public void UpdateXPCategories(IList<AssetDto.FriendlyGameAsset> categories)
        {
            ExecuteCommand(locator => UpdateGameAssetsCommand<XPCategory>(locator, categories));
        }

        /// <summary>
        /// Updates the list of XP types.
        /// </summary>
        public void UpdateXPTypes(IList<AssetDto.FriendlyGameAsset> types)
        {
            ExecuteCommand(locator => UpdateGameAssetsCommand<XPType>(locator, types));
        }
        #endregion // XP

        #region Private Methods
        /// <summary>
        /// Generic method for updating game assets that are simply hashes to names.
        /// </summary>
        private void UpdateGameAssetsCommand<T>(IRepositoryLocator locator, IDictionary<uint, String> assets) where T : GameAssetBase, new()
        {
            IGameAssetRepository<T> gameAssetRepo = (IGameAssetRepository<T>)locator.GetRepository<T>();

            foreach (KeyValuePair<uint, String> pair in assets)
            {
                T asset = gameAssetRepo.GetByHash(pair.Key);
                if (asset == null)
                {
                    asset = new T();
                    asset.Identifier = pair.Key.ToString();
                }

                // Update the shop's details and save it to the db.
                asset.Name = pair.Value;
                gameAssetRepo.SaveOrUpdate(asset);
            }
        }

        /// <summary>
        /// Simply converts the data to a dictionary before calling the dictionary version of this method.
        /// </summary>
        private void UpdateGameAssetsCommand<T>(IRepositoryLocator locator, IList<AssetDto.GameAsset> assets) where T : GameAssetBase, new()
        {
            UpdateGameAssetsCommand<T>(locator, assets.ToDictionary(item => item.Hash, item => item.GameName));
        }

        /// <summary>
        /// Generic method for updating game assets that are simply hashes to names.
        /// </summary>
        private void UpdateGameAssetsCommand<T>(IRepositoryLocator locator, IList<AssetDto.FriendlyGameAsset> assetSubs) where T : FriendlyGameAssetBase, new()
        {
            IGameAssetRepository<T> gameAssetRepo = (IGameAssetRepository<T>)locator.GetRepository<T>();

            foreach (AssetDto.FriendlyGameAsset assetSub in assetSubs)
            {
                T asset = gameAssetRepo.GetByHash(assetSub.Hash);
                if (asset == null)
                {
                    asset = new T();
                    asset.Identifier = assetSub.Hash.ToString();
                    asset.Name = assetSub.GameName;
                }

                // Update the assets details and save it to the db.
                asset.FriendlyName = assetSub.FriendlyName;
                gameAssetRepo.SaveOrUpdate(asset);
            }
        }

        /// <summary>
        /// Generic method for getting a mapping of hashes to strings for game assets.
        /// </summary>
        private IList<AssetDto.GameAsset> GetGameAssetsCommand<T>(IRepositoryLocator locator) where T : GameAssetBase
        {
            IGameAssetRepository<T> gameAssetRepo = (IGameAssetRepository<T>)locator.GetRepository<T>();
            IList<AssetDto.GameAsset> assets = new List<AssetDto.GameAsset>();
            foreach (GameAssetBase asset in gameAssetRepo.FindAll())
            {
                assets.Add(new AssetDto.GameAsset(asset.Name));
            }
            return assets;
        }
        #endregion // Private Methods
    } // GameAssetService

    /// <summary>
    /// Automapper profile.
    /// </summary>
    public class GameAssetServiceMappingProfile : Profile
    {
        /// <summary>
        /// 
        /// </summary>
        public override string ProfileName
        {
            get { return "DtoToDomainEntityMappings"; }
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void Configure()
        {
            // Missions
            Mapper.CreateMap<Mission, RSG.Model.Common.Mission.Mission>()
                .ForMember(model => model.Hash, opt => opt.MapFrom(domain => domain.Identifier))
                .ForMember(model => model.Checkpoints, opt => opt.Ignore());
            Mapper.CreateMap<RSG.Model.Common.Mission.IMission, Mission>()
                .ForMember(domain => domain.Identifier,
                           opt => opt.MapFrom(model => RSG.ManagedRage.StringHashUtil.atStringHash(model.MissionId, 0).ToString()))
                .ForMember(domain => domain.AltIdentifier,
                           opt => opt.MapFrom(model => RSG.ManagedRage.StringHashUtil.atStringHash(Path.GetFileNameWithoutExtension(model.ScriptName), 0).ToString()))
                .ForMember(domain => domain.Checkpoints, opt => opt.Ignore())
                .ForMember(domain => domain.Category, opt => opt.Ignore());

            // Checkpoints
            Mapper.CreateMap<RSG.Model.Common.Mission.IMissionCheckpoint, MissionCheckpoint>()
                .ForMember(domain => domain.Identifier, opt => opt.MapFrom(model => model.Hash.ToString()));
            Mapper.CreateMap<MissionCheckpoint, RSG.Model.Common.Mission.MissionCheckpoint>()
                .ForMember(domain => domain.Hash, opt => opt.MapFrom(model => model.Identifier));
        }
    } // GameAssetServiceMappingProfile
}
