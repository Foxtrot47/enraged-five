﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Domain.Config;
using RSG.Statistics.Server.TransManager;

namespace RSG.Statistics.Server.Context
{
    /// <summary>
    /// Interface for getting at objects shared on the global level
    /// </summary>
    public interface IGlobalContext
    {
        /// <summary>
        /// Database config object.
        /// </summary>
        IDomainConfig DomainConfig { get; }

        /// <summary>
        /// Transaction manager factory.
        /// </summary>
        ITransManagerFactory TransManagerFactory { get; }
    } // IGlobalContext
}
