﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using NHibernate.Spatial.Mapping;
using System.Diagnostics;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Diagnostics;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.Reflection;
using RSG.Statistics.Server.Context;
using RSG.Statistics.Domain.Config;
using RSG.Statistics.Server.Extensions;
using RSG.Statistics.Server.TransManager;
using RSG.Statistics.Server.Repository;
using RSG.Statistics.Domain.Entities;
using NHibernate.Criterion;
using RSG.Statistics.Common;
using RSG.Base.Logging;
using RSG.Statistics.Common.Config;
using RSG.Base.Configuration.Services;

namespace RSG.Statistics.Server
{
    /// <summary>
    /// Server Bootstrapper
    /// </summary>
    public static class ServerBootStrapper
    {
        #region Properties
        /// <summary>
        /// Flag indicating that the server bootstrapper has already run.
        /// </summary>
        private static bool _initialised;

        /// <summary>
        /// Log method for the server boot strapper to use.
        /// </summary>
        private static ILog _log;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Static constructor.
        /// </summary>
        static ServerBootStrapper()
        {
            _log = LogFactory.CreateUniversalLog("Server boot strapper");
        }
        #endregion // Constructor(s)

        #region Static Methods
        /// <summary>
        /// 
        /// </summary>
        public static void Initialise(INHibernateConfig config, bool updateSchema = false)
        {
            if (!_initialised)
            {
                GlobalContext.Initialise(config);
                IDomainConfig domainConfig = GlobalContext.Instance.DomainConfig;
                AutoMapperConfiguration.Install();

                if (updateSchema)
                {
                    string connString = domainConfig.NHibernateConfig.GetProperty("connection.connection_string");
                    _log.Profile("Updating database schema.");
                    domainConfig.SchemaUpdate.Execute(SchemaUpdateScriptAction, true);
                    _log.ProfileEnd();
                    _log.Message("Database schema successfully updated.");
                }

                UpdateEnums();
                _initialised = true;
            }
        }
        #endregion // Static Methods

        #region Private Methods
        /// <summary>
        /// Callback for logging messages generated during the schema update.
        /// </summary>
        /// <param name="line"></param>
        private static void SchemaUpdateScriptAction(string line)
        {
            _log.Message(line);
        }

        /// <summary>
        /// 
        /// </summary>
        private static void UpdateEnums()
        {
            _log.Message("Updating Database Enums");
            _log.Profile("Updating Database Enums");
            using (ITransManager manager = GlobalContext.Instance.TransManagerFactory.CreateManager(false))
            {
                manager.ExecuteCommand(locator => UpdateEnumsCommand(locator));
            }
            _log.ProfileEnd();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        private static void UpdateEnumsCommand(IRepositoryLocator locator)
        {
            // If you're adding an enum type here, you'll want to add a custom repository type to
            // the RepositoryLocator's static constructor.
            UpdateEnum<RSG.Platform.Platform>(locator);
            UpdateEnum<RSG.Platform.FileType>(locator);
            UpdateEnum<GameType>(locator);
            UpdateEnum<BuildConfig>(locator);
            UpdateEnum<MatchType>(locator);
            UpdateEnum<SectionExportSubTask>(locator);
            UpdateEnum<ShopType>(locator);
            UpdateEnum<UnlockType>(locator);
            UpdateEnum<RSG.Model.Common.Animation.ClipDictionaryCategory>(locator);
            UpdateEnum<RSG.Model.Common.Map.LodLevel>(locator);
            UpdateEnum<RSG.Model.Common.Map.MapDrawableLODLevel>(locator);
            UpdateEnum<RSG.Model.Common.Map.SpawnPointAvailableModes>(locator);
            UpdateEnum<RSG.Model.Common.Map.CollisionPrimitiveType>(locator);
            UpdateEnum<RSG.Model.Common.Map.StatGroup>(locator);
            UpdateEnum<RSG.Model.Common.Map.StatLodLevel>(locator);
            UpdateEnum<RSG.Model.Common.Map.StatEntityType>(locator);
            UpdateEnum<RSG.Model.Common.Mission.MissionAttemptResult>(locator);
            UpdateEnum<RSG.Model.Common.Mission.MissionCategory>(locator);
            UpdateEnum<RSG.Model.Statistics.Platform.ResourceBucketType>(locator);
            UpdateEnum<RSG.ROS.ROSPlatform>(locator);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        private static void UpdateEnum<TEnum>(IRepositoryLocator locator) where TEnum : struct
        {
            _log.Debug("Updating {0}", typeof(TEnum).Name);

            // This removes any values associated with enums values that were removed.
            foreach (EnumReference<TEnum> valueRef in locator.FindAll<EnumReference<TEnum>>())
            {
                locator.SaveOrUpdate(valueRef);
            }

            // Next make sure that all the code enums have an entry in the db.
            foreach (TEnum value in Enum.GetValues(typeof(TEnum)))
            {
                EnumReference<TEnum> valueRef = locator.GetEnumReferenceByValue<TEnum>(value, false);
                if (valueRef == null)
                {
                    valueRef = new EnumReference<TEnum>(value);
                    locator.Save(valueRef);
                }
            }
        }
        #endregion // Private Methods
    } // BootStrapper
}
