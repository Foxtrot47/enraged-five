﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using RSG.Base.Logging;
using RSG.Statistics.Client.Vertica.Client;
using RSG.Statistics.Common;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Model.Telemetry;
using RSG.Statistics.Common.Report;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.Server.Repository;

namespace RSG.Statistics.Server.Report.Telemetry.Online
{
    /// <summary>
    /// Report that provides the number of concurrent online users for a given time period/grouping.
    /// </summary>
    [Export(typeof(IStatsReport))]
    internal class VerticaConcurrentUsersReport : VerticaTelemetryReportBase<HistoricalConcurrentUserStats>
    {
        #region Constants
        /// <summary>
        /// Name of the report.
        /// </summary>
        private const String c_name = "VerticaConcurrentUsers";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// How the data is grouped.
        /// </summary>
        [ReportParameter(Required = true)]
        public String Grouping { get; set; }

        /// <summary>
        /// Convenience method for converting the grouping mode to an enum value.
        /// </summary>
        public DateTimeGroupMode GroupMode
        {
            get
            {
                DateTimeGroupMode mode;
                if (!Enum.TryParse(Grouping, true, out mode))
                {
                    mode = DateTimeGroupMode.Daily;
                }
                return mode;
            }
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public VerticaConcurrentUsersReport()
            : base(c_name)
        {
        }
        #endregion // Constructor(s)

        #region StatsReportBase Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        /// <returns></returns>
        protected override HistoricalConcurrentUserStats Generate(IRepositoryLocator locator, ILog log, IStatsServer server)
        {
            // Convert the parameters to something vertica can work with.
            HistoricalTelemetryParams queryParams = new HistoricalTelemetryParams();
            queryParams.GroupMode = this.GroupMode;
            SetDefaultParams(locator, queryParams);

            // Contact vertica for the report data.
            using (ReportClient client = new ReportClient(server.VerticaServer))
            {
                return (HistoricalConcurrentUserStats)client.RunReport(ReportNames.ConcurrentUsersReport, queryParams);
            }
        }
        #endregion // StatsReportBase Implementation
    } // VerticaConcurrentUsersReport
}
