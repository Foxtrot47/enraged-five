﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using RSG.Base.Logging;
using RSG.Statistics.Client.Vertica.Client;
using RSG.Statistics.Common;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Model.Telemetry;
using RSG.Statistics.Common.Report;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Server.Repository;

namespace RSG.Statistics.Server.Report.Telemetry
{
    /// <summary>
    /// Report that provides tv show viewer statistics.
    /// </summary>
    [Export(typeof(IStatsReport))]
    internal class VerticaKillDeathReport : VerticaTelemetryReportBase<List<KillDeathStat>>
    {
        #region Constants
        /// <summary>
        /// Name of the report.
        /// </summary>
        private const String c_name = "VerticaKillDeath";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Mode that the report is running in (Kills or Deaths).
        /// </summary>
        [ReportParameter(Required = true)]
        public bool KillMode { get; set; }

        /// <summary>
        /// Optional match type filters.
        /// </summary>
        [ReportParameter]
        public List<String> MatchTypeNames { get; set; }

        /// <summary>
        /// Convenience property that transforms the platform names to platform enum values.
        /// </summary>
        public IList<MatchType> MatchTypes
        {
            get
            {
                IList<MatchType> matchTypeNames = new List<MatchType>();
                foreach (String matchTypeName in MatchTypeNames)
                {
                    MatchType matchType;
                    if (Enum.TryParse(matchTypeName, out matchType))
                    {
                        matchTypeNames.Add(matchType);
                    }
                }
                return matchTypeNames;
            }
        }

        /// <summary>
        /// Flag to indicate that we only want to show freeroam related deaths/kills.
        /// </summary>
        [ReportParameter]
        public bool FreeroamOnly { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public VerticaKillDeathReport()
            : base(c_name)
        {
        }
        #endregion // Constructor(s)

        #region IReportDataProvider Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        /// <returns></returns>
        protected override List<KillDeathStat> Generate(IRepositoryLocator locator, ILog log, IStatsServer server)
        {
            // Convert the parameters to something vertica can work with.
            KillDeathParams queryParams = new KillDeathParams();
            queryParams.KillsOrDeaths = (KillMode ? KillDeathMode.Kills : KillDeathMode.Deaths);
            queryParams.MatchTypeRestrictions = MatchTypes;
            queryParams.FreeroamOnly = FreeroamOnly;
            SetDefaultParams(locator, queryParams);

            // Contact vertica for the report data.
            List<KillDeathStat> results;
            using (ReportClient client = new ReportClient(server.VerticaServer))
            {
                results = (List<KillDeathStat>)client.RunReport(ReportNames.KillDeathReport, queryParams);
            }

            // Convert the tv show names and remove movie results.
            IList<Weapon> allWeapons = locator.CreateCriteria<Weapon>().List<Weapon>();
            IDictionary<uint, Weapon> weaponLookup =
                allWeapons.ToDictionary(item => UInt32.Parse(item.Identifier));

            foreach (KillDeathStat result in results)
            {
                Weapon weapon;
                if (weaponLookup.TryGetValue(result.WeaponHash, out weapon))
                {
                    result.WeaponName = weapon.Name;
                    result.WeaponFriendlyName = weapon.FriendlyName;
                }
                else
                {
                    log.Warning("Unknown weapon hash encountered in data returned from Vertica. Hash: {0}", result.WeaponHash);
                }
            }
            return results;
        }
        #endregion // IReportDataProvider Implementation
    } // VerticaKillDeathReport
}
