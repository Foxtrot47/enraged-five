﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using RSG.Base.Attributes;
using RSG.Base.Extensions;
using RSG.Model.Common;
using RSG.Model.Common.Animation;
using RSG.Model.Common.Mission;
using RSG.Model.Common.Weapon;
using RSG.Model.Statistics.Platform;
using RSG.Platform;
using RSG.ROS;
using RSG.Statistics.Common;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Dto.Enums;
using RSG.Statistics.Common.ServiceContract;
using System.ComponentModel;
using RSG.Base.Configuration.Services;

namespace RSG.Statistics.Server.Services
{
    /// <summary>
    /// 
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class EnumService : ServiceBase, IEnumService
    {
        #region Constructor(s)
        /// <summary>
        /// Constructor taking a server configuration object.
        /// </summary>
        /// <param name="server"></param>
        public EnumService(IServiceHostConfig config)
            : base(config)
        {
        }
        #endregion // Constructor(s)
        
        #region IEnumService Implementation
        /// <summary>
        /// Returns a list of all build configs.
        /// </summary>
        public IList<EnumDto<BuildConfig>> GetBuildConfigs()
        {
            return GetAllEnums<BuildConfig>();
        }

        /// <summary>
        /// Returns a list of all clip dictionary categories.
        /// </summary>
        public IList<EnumDto<ClipDictionaryCategory>> GetClipDictionaryCategories()
        {
            return GetAllEnums<ClipDictionaryCategory>();
        }

        /// <summary>
        /// Returns a list of all date time group modes.
        /// </summary>
        public IList<EnumDto<DateTimeGroupMode>> GetDateTimeGroupModes()
        {
            return GetAllEnums<DateTimeGroupMode>();
        }

        /// <summary>
        /// Returns a list of all deathmatch sub types.
        /// </summary>
        public IList<EnumDto<DeathmatchSubType>> GetDeathmatchSubTypes()
        {
            return GetAllEnums<DeathmatchSubType>();
        }

        /// <summary>
        /// Returns a list of all file types.
        /// </summary>
        public IList<EnumDto<FileType>> GetFileTypes()
        {
            return GetAllEnums<FileType>();
        }

        /// <summary>
        /// Returns a list of all freemode mission categories.
        /// </summary>
        public IList<EnumDto<FreemodeMissionCategory>> GetFreemodeMissionCategories()
        {
            return GetAllEnums<FreemodeMissionCategory>();
        }

        /// <summary>
        /// Returns a list of all game types.
        /// </summary>
        public IList<EnumDto<GameType>> GetGameTypes()
        {
            return GetAllEnums<GameType>();
        }

        /// <summary>
        /// Returns a list of all job results.
        /// </summary>
        public IList<EnumDto<JobResult>> GetJobResults()
        {
            return GetAllEnums<JobResult>();
        }

        /// <summary>
        /// Returns a list of all match types.
        /// </summary>
        public IList<EnumDto<MatchType>> GetMatchTypes()
        {
            return GetAllEnums<MatchType>();
        }

        /// <summary>
        /// Returns a list of all mission attempt results.
        /// </summary>
        public IList<EnumDto<MissionAttemptResult>> GetMissionAttemptResults()
        {
            return GetAllEnums<MissionAttemptResult>();
        }

        /// <summary>
        /// Returns a list of all mission categories.
        /// </summary>
        public IList<EnumDto<MissionCategory>> GetMissionCategories()
        {
            return GetAllEnums<MissionCategory>();
        }

        /// <summary>
        /// Returns a list of all mission sub types.
        /// </summary>
        public IList<EnumDto<MissionSubType>> GetMissionSubTypes()
        {
            return GetAllEnums<MissionSubType>();
        }

        /// <summary>
        /// Returns a list of all platforms.
        /// </summary>
        public IList<EnumDto<RSG.Platform.Platform>> GetPlatforms()
        {
            return GetAllEnums<RSG.Platform.Platform>();
        }

        /// <summary>
        /// Returns a list of all race sub types.
        /// </summary>
        public IList<EnumDto<RaceSubType>> GetRaceSubTypes()
        {
            return GetAllEnums<RaceSubType>();
        }

        /// <summary>
        /// Returns a list of all resource bucket types.
        /// </summary>
        public IList<EnumDto<ResourceBucketType>> GetResourceBucketTypes()
        {
            return GetAllEnums<ResourceBucketType>();
        }       

        /// <summary>
        /// Returns a list of all ROS platforms.
        /// </summary>
        public IList<EnumDto<ROSPlatform>> GetROSPlatforms()
        {
            return GetAllEnums<ROSPlatform>();
        }

        /// <summary>
        /// Returns a list of all shop types.
        /// </summary>
        public IList<EnumDto<ShopType>> GetShopTypes()
        {
            return GetAllEnums<ShopType>();
        }

        /// <summary>
        /// Returns a list of all spend categories.
        /// </summary>
        public IList<EnumDto<SpendCategory>> GetSpendCategories()
        {
            return GetAllEnums<SpendCategory>();
        }

        /// <summary>
        /// Returns a list of all times of the day.
        /// </summary>
        public IList<EnumDto<TimeOfDay>> GetTimeOfDays()
        {
            return GetAllEnums<TimeOfDay>();
        }

        /// <summary>
        /// Returns a list of all vehicle categories.
        /// </summary>
        public IList<EnumDto<VehicleCategory>> GetVehicleCategories()
        {
            return GetAllEnums<VehicleCategory>();
        }

        /// <summary>
        /// Returns a list of all weapon categories.
        /// </summary>
        public IList<EnumDto<WeaponCategory>> GetWeaponCategories()
        {
            return GetAllEnums<WeaponCategory>();
        }

        /// <summary>
        /// Returns a list of all weather types.
        /// </summary>
        public IList<EnumDto<WeatherType>> GetWeatherTypes()
        {
            return GetAllEnums<WeatherType>();
        }
        #endregion // IEnumService Implementation

        #region Private Methods
        /// <summary>
        /// Returns a list of dtos for a particular enum type.
        /// </summary>
        private IList<EnumDto<TEnum>> GetAllEnums<TEnum>()
            where TEnum : struct
        {
            IList<EnumDto<TEnum>> dtos = new List<EnumDto<TEnum>>();

            foreach (TEnum val in Enum.GetValues(typeof(TEnum)))
            {
                Type type = val.GetType();
                FieldInfo fi = type.GetField(val.ToString());

                // Check whether the enum has a browsable attribute.
                BrowsableAttribute[] browsableAtts = fi.GetCustomAttributes(typeof(BrowsableAttribute), false) as BrowsableAttribute[];
                if (!browsableAtts.Any() || browsableAtts.First().Browsable)
                {
                    EnumDto<TEnum> dto = new EnumDto<TEnum>();
                    dto.Value = val;
                    dto.Name = val.ToString();

                    // Annoyingly we can't use the enum extension method because we're the generic enum parameter can't be restricted to enums :/.
                    FriendlyNameAttribute[] nameAtts = fi.GetCustomAttributes(typeof(FriendlyNameAttribute), false) as FriendlyNameAttribute[];
                    dto.FriendlyName = (nameAtts.Length > 0 ? nameAtts[0].FriendlyName : null);

                    dtos.Add(dto);
                }
            }

            return dtos;
        }
        #endregion // Private Methods
    } // EnumService
}
