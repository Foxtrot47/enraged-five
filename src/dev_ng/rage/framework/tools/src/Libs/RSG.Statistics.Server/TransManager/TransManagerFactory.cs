﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using System.Configuration;
using System.Diagnostics;
using System.ComponentModel.Composition;
using RSG.Statistics.Domain.Config;
using RSG.Statistics.Common.Config;

namespace RSG.Statistics.Server.TransManager
{
    /// <summary>
    /// NHibernate based transaction manager factory
    /// </summary>
    public class TransManagerFactory : ITransManagerFactory
    {
        #region Fields
        /// <summary>
        /// Factory for creating NHibernate sessions.
        /// </summary>
        private ISessionFactory _sessionFactory;

        /// <summary>
        /// Flag indicating whether we should be running in read-only mode.
        /// </summary>
        private bool _readOnly;
        #endregion // Fields

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public TransManagerFactory(IDomainConfig config, bool readOnlyMode)
        {
            _sessionFactory = config.NHibernateConfig.BuildSessionFactory();
            _readOnly = readOnlyMode;
        }
        #endregion // Constructor(s)

        #region ITransFactory Implementation
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ITransManager CreateManager(bool readOnly)
        {
            ISession session = _sessionFactory.OpenSession();
            session.DefaultReadOnly = _readOnly || readOnly;
            return new TransManager(session);
        }
        #endregion // ITransFactory Implementation
    } // TransManagerFactory
}
