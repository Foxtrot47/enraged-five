﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Linq;
using System.Text;
using RSG.Base.Logging;
using RSG.Statistics.Client.Vertica.Client;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Dto.ProfileStats;
using RSG.Statistics.Common.Report;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.Server.Repository;

namespace RSG.Statistics.Server.Report.ProfileStats
{
    /// <summary>
    /// Report based off of the division of two seperate sets of profile stats.
    /// </summary>
    [Export(typeof(IStatsReport))]
    internal class VerticaDividedDiffReport : VerticaProfileStatReportBase<List<ProfileStatDto>>
    {
        #region Constants
        /// <summary>
        /// Name of the report.
        /// </summary>
        private const String c_name = "VerticaDividedDiffProfileStats";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Optional end date.
        /// </summary>
        [ReportParameter(Required = true)]
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Names of the profile stats that will be combined to generate the numerator.
        /// </summary>
        [ReportParameter(Required = true)]
        public List<String> NumeratorStatNames { get; set; }

        /// <summary>
        /// Names of the profile stats that will be combined to generate the denominator.
        /// </summary>
        [ReportParameter(Required = true)]
        public List<String> DenominatorStatNames { get; set; }

        /// <summary>
        /// Maximum difference between two consecutively submitted values (allows us to filter out absurdly large changes).
        /// </summary>
        [ReportParameter]
        public float? MaxNumeratorDifference { get; set; }

        /// <summary>
        /// Maximum difference between two consecutively submitted values (allows us to filter out absurdly large changes).
        /// </summary>
        [ReportParameter]
        public float? MaxDenominatorDifference { get; set; }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public VerticaDividedDiffReport()
            : base(c_name)
        {
        }
        #endregion // Constructor(s)

        #region IReportDataProvider Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        /// <returns></returns>
        protected override List<ProfileStatDto> Generate(IRepositoryLocator locator, ILog log, IStatsServer server)
        {
            Debug.Assert(NumeratorStatNames.Any(), "No numerator stat names provided.");
            Debug.Assert(DenominatorStatNames.Any(), "No denominator stat names provided.");

            // Convert the parameters to something vertica can work with.
            DividedProfileStatParams queryParams = new DividedProfileStatParams();
            queryParams.StartDate = this.StartDate;
            queryParams.NumeratorStatNames = GetDefaultStatValues(locator, this.NumeratorStatNames);
            queryParams.DenominatorStatNames = GetDefaultStatValues(locator, this.DenominatorStatNames);
            queryParams.MaxNumeratorValue = this.MaxNumeratorDifference ?? 1000000;
            queryParams.MaxDenominatorValue = this.MaxDenominatorDifference ?? 1000000;
            SetDefaultParams(locator, queryParams);

            // Contact vertica for the report data.
            using (ReportClient client = new ReportClient(server.VerticaServer))
            {
                return (List<ProfileStatDto>)client.RunReport(ReportNames.DividedDiffProfileStatReport, queryParams);
            }
        }
        #endregion // IReportDataProvider Implementation
    } // VerticaDividedDiffReport
}
