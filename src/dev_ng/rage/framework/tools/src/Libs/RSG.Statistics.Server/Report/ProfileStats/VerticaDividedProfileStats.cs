﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Linq;
using System.Text;
using RSG.Base.Logging;
using RSG.ROS;
using RSG.Statistics.Client.Vertica.Client;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Dto.ProfileStats;
using RSG.Statistics.Common.Report;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.Server.Repository;

namespace RSG.Statistics.Server.Report.ProfileStats
{
    /// <summary>
    /// Report based off of the division of two seperate sets of profile stats.
    /// </summary>
    [Export(typeof(IStatsReport))]
    internal class VerticaDividedProfileStats : VerticaProfileStatReportBase<List<ProfileStatDto>>
    {
        #region Constants
        /// <summary>
        /// Name of the report.
        /// </summary>
        private const String c_name = "VerticaDividedProfileStats";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Names of the profile stats that will be combined to generate the numerator.
        /// </summary>
        [ReportParameter(Required = true)]
        public List<String> NumeratorStatNames { get; set; }

        /// <summary>
        /// Names of the profile stats that will be combined to generate the denominator.
        /// </summary>
        [ReportParameter(Required = true)]
        public List<String> DenominatorStatNames { get; set; }

        /// <summary>
        /// Maximum numerator value (allows us to filter out absurdly large values).
        /// </summary>
        [ReportParameter]
        public float? MaxNumeratorValue { get; set; }

        /// <summary>
        /// Maximum denominator value (allows us to filter out absurdly large values).
        /// </summary>
        [ReportParameter]
        public float? MaxDenominatorValue { get; set; }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public VerticaDividedProfileStats()
            : base(c_name)
        {
        }
        #endregion // Constructor(s)

        #region IReportDataProvider Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        /// <returns></returns>
        protected override List<ProfileStatDto> Generate(IRepositoryLocator locator, ILog log, IStatsServer server)
        {
            Debug.Assert(NumeratorStatNames.Any(), "No numerator stat names provided.");
            Debug.Assert(DenominatorStatNames.Any(), "No denominator stat names provided.");

            // Convert the parameters to something vertica can work with.
            DividedProfileStatParams queryParams = new DividedProfileStatParams();
            queryParams.NumeratorStatNames = GetDefaultStatValues(locator, this.NumeratorStatNames);
            queryParams.DenominatorStatNames = GetDefaultStatValues(locator, this.DenominatorStatNames);
            queryParams.MaxNumeratorValue = MaxNumeratorValue ?? 1000000;
            queryParams.MaxDenominatorValue = MaxDenominatorValue ?? 1000000;
            SetDefaultParams(locator, queryParams);

            // Contact vertica for the report data.
            using (ReportClient client = new ReportClient(server.VerticaServer))
            {
                return (List<ProfileStatDto>)client.RunReport(ReportNames.DividedProfileStatReport, queryParams);
            }
        }
        #endregion // IReportDataProvider Implementation
    } // VerticaDividedProfileStats
}
