﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Criterion;
using RSG.Base.Logging;
using RSG.ROS;
using RSG.Statistics.Client.Vertica.Client;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Dto.ProfileStats;
using RSG.Statistics.Common.Report;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.Domain.Entities.Profile;
using RSG.Statistics.Server.Repository;

namespace RSG.Statistics.Server.Report.ProfileStats
{
    /// <summary>
    /// Report based off of a combination of profile stats.
    /// </summary>
    [Export(typeof(IStatsReport))]
    internal class VerticaPerUserProfileStatReport : VerticaPerUserReportBase<List<PerUserProfileStatValues>>
    {
        #region Constants
        /// <summary>
        /// Name of the report.
        /// </summary>
        private const String c_name = "VerticaPerUserProfileStats";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Names of the stats we wish to see latest data for.
        /// </summary>
        [ReportParameter(Required = true)]
        public List<String> StatNames { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public VerticaPerUserProfileStatReport()
            : base(c_name)
        {
        }
        #endregion // Constructor(s)

        #region IReportDataProvider Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        /// <returns></returns>
        protected override List<PerUserProfileStatValues> Generate(IRepositoryLocator locator, ILog log, IStatsServer server)
        {
            Debug.Assert(StatNames.Any(), "No stat names provided.");

            // Convert the parameters to something vertica can work with.
            PerUserProfileStatParams queryParams = new PerUserProfileStatParams();
            queryParams.StatNames = GetDefaultStatValues(locator, StatNames);
            SetDefaultParams(locator, queryParams);
            
            // Contact vertica for the report data.
            using (ReportClient client = new ReportClient(server.VerticaServer))
            {
                return (List<PerUserProfileStatValues>)client.RunReport(ReportNames.LatestProfileStatReport, queryParams);
            }
        }
        #endregion // IReportDataProvider Implementation

        #region Private Methods
        /// <summary>
        /// Retrieves list of default values associated with the passed in stat names.
        /// </summary>
        /// <returns></returns>
        protected IList<Tuple<String, String>> GetDefaultStatValues(IRepositoryLocator locator, IList<String> statNames)
        {
            ICriteria criteria =
                locator.CreateCriteria<ProfileStatDefinition>()
                    .Add(Restrictions.In("Name", statNames.ToArray()));
            IList<ProfileStatDefinition> defs = criteria.List<ProfileStatDefinition>();

            return defs.Select(item => Tuple.Create(item.Name, item.DefaultValue)).ToList();
        }
        #endregion // Private Methods
    } // VerticaPerUserProfileStatReport
}
