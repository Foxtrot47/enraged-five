﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using RSG.Base.Logging;
using RSG.Base.Security.Cryptography;
using RSG.Statistics.Client.Vertica.Client;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Model.Telemetry;
using RSG.Statistics.Common.Model.Telemetry.PerUser;
using RSG.Statistics.Common.Report;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Server.Repository;

namespace RSG.Statistics.Server.Report.Telemetry.PerUser
{
    /// <summary>
    /// 
    /// </summary>
    [Export(typeof(IStatsReport))]
    internal class VerticaPerUserMissionCashReport : VerticaPerUserTelemetryReportBase<List<PerUserMissionCashStats>>
    {
        #region Constants
        /// <summary>
        /// Name of the report.
        /// </summary>
        private const String c_name = "VerticaPerUserMissionCash";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Missions that we are interested in seeing this information for..
        /// </summary>
        [ReportParameter(Required = true)]
        public List<String> MissionIdentifiers { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public VerticaPerUserMissionCashReport()
            : base(c_name)
        {
        }
        #endregion // Constructor(s)

        #region IStatsReport Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        protected override List<PerUserMissionCashStats> Generate(IRepositoryLocator locator, ILog log, IStatsServer server)
        {
            // Convert the parameters to something vertica can work with.
            PerUserMissionCashParams queryParams = new PerUserMissionCashParams();
            queryParams.MissionNames = MissionIdentifiers.Distinct().ToList();
            SetDefaultParams(locator, queryParams);

            // Get the results from the vertica service.
            List<PerUserMissionCashStats> results;

            using (ReportClient client = new ReportClient(server.VerticaServer))
            {
                results = (List<PerUserMissionCashStats>)client.RunReport(ReportNames.PerUserMissionCashReport, queryParams);
            }

            // Convert the oddjob name hashes to friendly strings.
            IMissionRepository missionRepo = (IMissionRepository)locator.GetRepository<Mission>();
            IDictionary<uint, Mission> missionLookup = missionRepo.CreateAssetLookup();
            IDictionary<uint, Mission> altMissionLookup = missionRepo.CreateAlternativeAssetLookup();

            foreach (PerUserMissionCashStats result in results)
            {
                foreach (MissionCashStat stat in result.Data)
                {
                    Mission mission;
                    if (missionLookup.TryGetValue(OneAtATime.ComputeHash(stat.MissionName), out mission))
                    {
                        stat.MissionName = mission.Name;
                        stat.MissionId = mission.MissionId;
                    }
                    else if (altMissionLookup.TryGetValue(OneAtATime.ComputeHash(stat.MissionName), out mission))
                    {
                        stat.MissionName = mission.Name;
                        stat.MissionId = mission.MissionId;
                    }
                    else
                    {
                        log.Warning("Unknown mission name encountered in data returned from Vertica. Name: {0}", stat.MissionName);
                    }
                }
            }
            return results;
        }
        #endregion // IStatsReport Implementation
    } // VerticaPerUserMissionCashReport
}
