﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Server.Repository;

namespace RSG.Statistics.Server.Report.Telemetry.PerUser
{
    /// <summary>
    /// 
    /// </summary>
    internal abstract class VerticaPerUserTelemetryReportBase<TResult> : VerticaPerUserReportBase<TResult> where TResult : class
    {
        #region Properties
        /// <summary>
        /// Optional build to restrict the results to.
        /// </summary>
        [ReportParameter]
        public List<String> BuildIdentifiers { get; set; }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public VerticaPerUserTelemetryReportBase(String name)
            : base(name)
        {
        }
        #endregion // Constructor(s)

        #region Protected Methods
        /// <summary>
        /// Sets parameters common to all telemetry based reports.
        /// </summary>
        protected void SetDefaultParams(IRepositoryLocator locator, PerUserTelemetryParams parameters)
        {
            base.SetDefaultParams(locator, parameters);
            parameters.Builds = this.BuildIdentifiers.Select(item => ConvertBuildIdentifierToVersion(item)).ToList();
        }
        #endregion // Protected Methods
    } // VerticaPerUserTelemetryReportBase
}
