﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using RSG.Base.Logging;
using RSG.Model.Common.Animation;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Report;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Domain.Entities.GameAssetStats.Animation;
using RSG.Statistics.Server.Extensions;
using RSG.Statistics.Server.Repository;

namespace RSG.Statistics.Server.Report.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    [Export(typeof(IStatsReport))]
    internal class PerBuildCutsceneReport : PerBuildReportBase<ICutsceneCollection>
    {
        #region Constants
        /// <summary>
        /// Name of the report.
        /// </summary>
        private const String c_name = "PerBuildCutscenes";
        #endregion // Constants
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public PerBuildCutsceneReport()
            : base(c_name)
        {
        }
        #endregion // Constructor(s)

        #region IStatsReport Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        protected override ICutsceneCollection Generate(IRepositoryLocator locator, ILog log, IStatsServer server)
        {
            ICutsceneStatRepository cutsceneStatRepo = (ICutsceneStatRepository)locator.GetRepository<CutsceneStat>();
            Build build = locator.GetAssetByIdentifier<Build>(BuildIdentifier);
            return cutsceneStatRepo.CreateCollectionForBuild(build);
        }
        #endregion // IStatsReport Implementation
    } // PerBuildCutsceneReport
}
