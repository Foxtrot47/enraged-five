﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using RSG.Base.Logging;
using RSG.ROS;
using RSG.Statistics.Client.Vertica.Client;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Model.Telemetry;
using RSG.Statistics.Common.Report;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Server.Repository;

namespace RSG.Statistics.Server.Report.Telemetry.Media
{
    /// <summary>
    /// Report that provides radio station listening statistics.
    /// </summary>
    [Export(typeof(IStatsReport))]
    internal class VerticaRadioStationListenerReport : VerticaTelemetryReportBase<List<MediaStat>>
    {
        #region Constants
        /// <summary>
        /// Name of the report.
        /// </summary>
        private const String c_name = "VerticaRadioStationListeners";
        #endregion // Constants
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public VerticaRadioStationListenerReport()
            : base(c_name)
        {
        }
        #endregion // Constructor(s)

        #region IReportDataProvider Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        /// <returns></returns>
        protected override List<MediaStat> Generate(IRepositoryLocator locator, ILog log, IStatsServer server)
        {
            // Convert the parameters to something vertica can work with.
            TelemetryParams queryParams = new TelemetryParams();
            SetDefaultParams(locator, queryParams);

            // Contact vertica for the report data.
            List<MediaStat> results;
            using (ReportClient client = new ReportClient(server.VerticaServer))
            {
                results = (List<MediaStat>)client.RunReport(ReportNames.RadioStationListenerReport, queryParams);
            }

            // Patch up the radio station names.
            if (results != null)
            {
                IGameAssetRepository<RadioStation> radioStationRepo = (IGameAssetRepository<RadioStation>)locator.GetRepository<RadioStation>();
                IDictionary<uint, RadioStation> radioStationLookup = radioStationRepo.CreateAssetLookup();

                foreach (MediaStat result in results)
                {
                    RadioStation radioStation;
                    if (radioStationLookup.TryGetValue(result.Hash, out radioStation))
                    {
                        result.Name = radioStation.Name;
                        result.FriendlyName = radioStation.FriendlyName;
                    }
                    else
                    {
                        log.Warning("Unknown radio station hash encountered in data returned from Vertica. Hash: {0}", result.Hash);
                    }
                }
            }

            return results;
        }
        #endregion // IReportDataProvider Implementation
    } // VerticaRadioStationListenerReport
}
