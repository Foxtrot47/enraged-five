﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using NHibernate;
using NHibernate.Criterion;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.ServiceContract;
using SceneXmlDto = RSG.Statistics.Common.Dto.SceneXml;
using SceneXmlDomain = RSG.Statistics.Domain.Entities.SceneXml;
using RSG.Statistics.Server.Repository;
using RSG.Base.Configuration.Services;
using NHibernate.SqlCommand;
using NHibernate.Transform;

namespace RSG.Statistics.Server.Services
{
    /// <summary>
    /// Service used to manipulate and query scene xml data.
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class SceneXmlService : ServiceBase, ISceneXmlManipulationService, ISceneXmlQueryService
    {
        #region Constructor(s)
        /// <summary>
        /// Constructor taking a server configuration object.
        /// </summary>
        /// <param name="server"></param>
        public SceneXmlService(IServiceHostConfig config)
            : base(config)
        {
        }
        #endregion // Constructor(s)

        #region ISceneXmlManipulationService Implementation
        /// <summary>
        /// Populates the database with a particular scene xml file.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="sceneDto"></param>
        public void PopulateSceneXmlFile(String filename, SceneXmlDto.Scene sceneDto)
        {
            ExecuteCommand(locator => PopulateSceneXmlFileCommand(locator, System.IO.Path.GetFullPath(filename).ToLower(), sceneDto));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="filename"></param>
        /// <param name="sceneDto"></param>
        private void PopulateSceneXmlFileCommand(IRepositoryLocator locator, String filename, SceneXmlDto.Scene sceneDto)
        {
            // Delete the existing scenexml data if it already exists for this file.
            DeleteSceneXmlFileCommand(locator, filename);

            // Create a new file entry for this file.
            SceneXmlDomain.Scene scene = new SceneXmlDomain.Scene();
            scene.Filename = filename;
            scene.Version = sceneDto.Version;
            scene.Timestamp = sceneDto.Timestamp;
            scene.Username = sceneDto.Username;

            // Create the materials that this file contains.
            IDictionary<Guid, SceneXmlDomain.Material> materialLookup = new Dictionary<Guid, SceneXmlDomain.Material>();
            uint index = 0;

            scene.Materials = new List<SceneXmlDomain.Material>();
            foreach (SceneXmlDto.Material materialDto in sceneDto.Materials)
            {
                SceneXmlDomain.Material material = CreateMaterialForDto(locator, materialDto, scene, null, materialLookup, index++);
                material.ParentScene = scene;
                scene.Materials.Add(material);
            }

            // Create the objects that this file contains.
            scene.Objects = new List<SceneXmlDomain.Object>();
            foreach (SceneXmlDto.Object objectDto in sceneDto.Objects)
            {
                SceneXmlDomain.Object obj = CreateObjectForDto(locator, objectDto, scene, null, materialLookup);
                obj.ParentScene = scene;
                scene.Objects.Add(obj);
            }

            locator.Save(scene);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="matDef"></param>
        /// <returns></returns>
        private SceneXmlDomain.Material CreateMaterialForDto(IRepositoryLocator locator, SceneXmlDto.Material materialDto, SceneXmlDomain.Scene scene, SceneXmlDomain.Material parent, IDictionary<Guid, SceneXmlDomain.Material> materialLookup, uint index)
        {
            SceneXmlDomain.Material material = new SceneXmlDomain.Material();
            material.Guid = materialDto.Guid;
            material.Type = materialDto.Type;
            material.Preset = materialDto.Preset;
            material.Scene = scene;
            material.ParentMaterial = parent;
            material.MaterialIndex = index;

            // Add any sub-materials this material contains.
            if (materialDto.SubMaterials.Any())
            {
                material.SubMaterials = new List<SceneXmlDomain.Material>();
                uint subIndex = 0;

                foreach (SceneXmlDto.Material subMaterialDto in materialDto.SubMaterials)
                {
                    SceneXmlDomain.Material subMaterial = CreateMaterialForDto(locator, subMaterialDto, scene, material, materialLookup, subIndex++);
                    material.SubMaterials.Add(subMaterial);
                }
            }

            // Add any textures this material makes use of.
            if (materialDto.Textures.Any())
            {
                material.Textures = new List<SceneXmlDomain.Texture>();

                foreach (SceneXmlDto.Texture textureDto in materialDto.Textures)
                {
                    SceneXmlDomain.Texture texture = CreateTextureForDto(locator, textureDto, scene, material);
                    material.Textures.Add(texture);
                }
            }

            // Keep track of this material for when we parse the objects.
            materialLookup.Add(material.Guid, material);

            return material;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="textureDef"></param>
        /// <returns></returns>
        private SceneXmlDomain.Texture CreateTextureForDto(IRepositoryLocator locator, SceneXmlDto.Texture textureDto, SceneXmlDomain.Scene scene, SceneXmlDomain.Material material)
        {
            SceneXmlDomain.Texture texture = new SceneXmlDomain.Texture();
            if (!String.IsNullOrEmpty(textureDto.Filename))
            {
                texture.Filename = System.IO.Path.GetFullPath(textureDto.Filename).ToLower();
            }
            else
            {
                texture.Filename = "";
            }
            if (!String.IsNullOrEmpty(textureDto.AlphaFilename))
            {
                texture.AlphaFilename = System.IO.Path.GetFullPath(textureDto.AlphaFilename).ToLower();
            }
            else
            {
                texture.AlphaFilename = "";
            }
            texture.Type = textureDto.Type;
            texture.Scene = scene;
            texture.Material = material;
            return texture;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="objectDef"></param>
        /// <param name="file"></param>
        /// <param name="parent"></param>
        /// <returns></returns>
        private SceneXmlDomain.Object CreateObjectForDto(IRepositoryLocator locator, SceneXmlDto.Object objectDto, SceneXmlDomain.Scene scene, SceneXmlDomain.Object parent, IDictionary<Guid, SceneXmlDomain.Material> materialLookup)
        {
            SceneXmlDomain.Object obj = new SceneXmlDomain.Object();
            obj.Guid = objectDto.Guid;
            obj.Name = objectDto.Name;
            obj.Class = objectDto.Class;
            obj.SuperClass = objectDto.SuperClass;
            obj.AttributeClass = objectDto.AttributeClass;
            obj.PolyCount = objectDto.PolyCount;
            obj.ParentObject = parent;
            obj.Scene = scene;

            obj.ObjectTranslationX = objectDto.ObjectTranslation.X;
            obj.ObjectTranslationY = objectDto.ObjectTranslation.Y;
            obj.ObjectTranslationZ = objectDto.ObjectTranslation.Z;

            obj.ObjectRotationX = objectDto.ObjectRotation.X;
            obj.ObjectRotationY = objectDto.ObjectRotation.Y;
            obj.ObjectRotationZ = objectDto.ObjectRotation.Z;
            obj.ObjectRotationW = objectDto.ObjectRotation.W;

            obj.ObjectScaleX = objectDto.ObjectScale.X;
            obj.ObjectScaleY = objectDto.ObjectScale.Y;
            obj.ObjectScaleZ = objectDto.ObjectScale.Z;

            obj.NodeTranslationX = objectDto.NodeTranslation.X;
            obj.NodeTranslationY = objectDto.NodeTranslation.Y;
            obj.NodeTranslationZ = objectDto.NodeTranslation.Z;

            obj.NodeRotationX = objectDto.NodeRotation.X;
            obj.NodeRotationY = objectDto.NodeRotation.Y;
            obj.NodeRotationZ = objectDto.NodeRotation.Z;
            obj.NodeRotationW = objectDto.NodeRotation.W;

            obj.NodeScaleX = objectDto.NodeScale.X;
            obj.NodeScaleY = objectDto.NodeScale.Y;
            obj.NodeScaleZ = objectDto.NodeScale.Z;

            obj.LocalBoundingBoxMinX = objectDto.LocalBoundingBox.Min.X;
            obj.LocalBoundingBoxMinY = objectDto.LocalBoundingBox.Min.Y;
            obj.LocalBoundingBoxMinZ = objectDto.LocalBoundingBox.Min.Z;
            obj.LocalBoundingBoxMaxX = objectDto.LocalBoundingBox.Max.X;
            obj.LocalBoundingBoxMaxY = objectDto.LocalBoundingBox.Max.Y;
            obj.LocalBoundingBoxMaxZ = objectDto.LocalBoundingBox.Max.Z;

            obj.WorldBoundingBoxMinX = objectDto.WorldBoundingBox.Min.X;
            obj.WorldBoundingBoxMinY = objectDto.WorldBoundingBox.Min.Y;
            obj.WorldBoundingBoxMinZ = objectDto.WorldBoundingBox.Min.Z;
            obj.WorldBoundingBoxMaxX = objectDto.WorldBoundingBox.Max.X;
            obj.WorldBoundingBoxMaxY = objectDto.WorldBoundingBox.Max.Y;
            obj.WorldBoundingBoxMaxZ = objectDto.WorldBoundingBox.Max.Z;

            // Set the material if the object has one.
            SceneXmlDomain.Material material;
            if (objectDto.MaterialGuid != Guid.Empty)
            {
                if (materialLookup.TryGetValue(objectDto.MaterialGuid, out material))
                {
                    obj.Material = material;
                }
                else
                {
                    Log.Warning("Unable to resolve material guid '{0}'.", objectDto.MaterialGuid);
                }
            }

            if (objectDto.Attributes.Any())
            {
                obj.Attributes = new List<SceneXmlDomain.Attribute>();

                foreach (SceneXmlDto.Attribute attributeDto in objectDto.Attributes)
                {
                    SceneXmlDomain.Attribute attribute = CreateAttributeForDto(locator, attributeDto, obj);
                    obj.Attributes.Add(attribute);
                }
            }

            if (objectDto.Parameters != null && objectDto.Parameters.Any())
            {
                obj.Parameters = new List<SceneXmlDomain.Parameter>();

                foreach (SceneXmlDto.Parameter parameterDto in objectDto.Parameters)
                {
                    SceneXmlDomain.Parameter parameter = CreateParameterForDto(locator, parameterDto, obj);
                    obj.Parameters.Add(parameter);
                }
            }
            
            // Recurse over any child objects.
            if (objectDto.Children.Any())
            {
                obj.Children = new List<SceneXmlDomain.Object>();

                foreach (SceneXmlDto.Object childObjectDto in objectDto.Children)
                {
                    SceneXmlDomain.Object childObj = CreateObjectForDto(locator, childObjectDto, scene, obj, materialLookup);
                    obj.Children.Add(childObj);
                }
            }

            return obj;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="attributeDto"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        private SceneXmlDomain.Attribute CreateAttributeForDto(IRepositoryLocator locator, SceneXmlDto.Attribute attributeDto, SceneXmlDomain.Object obj)
        {
            SceneXmlDomain.Attribute attribute = new SceneXmlDomain.Attribute();
            attribute.Name = attributeDto.Name;
            attribute.Object = obj;
            attribute.StringValue = attributeDto.StringValue;
            attribute.IntValue = attributeDto.IntValue;
            attribute.FloatValue = attributeDto.FloatValue;
            attribute.BoolValue = attributeDto.BoolValue;
            return attribute;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="parameterDto"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        private SceneXmlDomain.Parameter CreateParameterForDto(IRepositoryLocator locator, SceneXmlDto.Parameter parameterDto, SceneXmlDomain.Object obj)
        {
            SceneXmlDomain.Parameter parameter = new SceneXmlDomain.Parameter();
            parameter.Name = parameterDto.Name;
            parameter.Object = obj;
            parameter.StringValue = parameterDto.StringValue;
            parameter.IntValue = parameterDto.IntValue;
            parameter.FloatValue = parameterDto.FloatValue;
            parameter.BoolValue = parameterDto.BoolValue;
            return parameter;
        }

        /// <summary>
        /// Deletes all data relating to a particular scene xml file.
        /// </summary>
        /// <param name="filename"></param>
        public void DeleteSceneXmlFile(String filename)
        {
            ExecuteCommand(locator => DeleteSceneXmlFileCommand(locator, filename));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="filename"></param>
        private void DeleteSceneXmlFileCommand(IRepositoryLocator locator, String filename)
        {
            IRepository<SceneXmlDomain.Scene> sceneRepository = locator.GetRepository<SceneXmlDomain.Scene>();

            ICriteria criteria = sceneRepository.CreateCriteria().Add(Restrictions.Eq("Filename", filename));
            SceneXmlDomain.Scene scene = criteria.UniqueResult<SceneXmlDomain.Scene>();

            if (scene != null)
            {
                sceneRepository.Delete(scene);
            }
        }
        #endregion // ISceneXmlManipulationService Implementation

        #region ISceneXmlQueryService Implementation
        /// <summary>
        /// Retrieves the list of scene xml files that are using the specified texture.
        /// </summary>
        /// <param name="filename">The texture to search for.</param>
        public IList<String> GetSceneXmlFilesUsingTexture(String filename)
        {
            return ExecuteReadOnlyCommand(locator => GetSceneXmlFilesUsingTextureCommand(locator, System.IO.Path.GetFullPath(filename).ToLower()));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        private IList<String> GetSceneXmlFilesUsingTextureCommand(IRepositoryLocator locator, String filename)
        {
            // Construct the criteria for retrieving the results.
            ICriteria criteria =
                locator.CreateCriteria<SceneXmlDomain.Texture>()
                    .CreateAlias("Scene", "scene")
                    .SetProjection(Projections.Distinct(Projections.Property("scene.Filename")))
                    .Add(Restrictions.Eq("Filename", filename))
                    .AddOrder(Order.Asc("scene.Filename"));

            // Execute the query and return the results.
            return criteria.List<String>();
        }

        /// <summary>
        /// Retrieves texture usage information for the specified textures.
        /// </summary>
        /// <param name="textures">List of textures to get usage information for.</param>
        /// <returns></returns>
        public IList<SceneXmlDto.TextureUsage> GetTextureUsage(IEnumerable<String> textures)
        {
            return ExecuteReadOnlyCommand(locator => GetTextureUsageCommand(locator, textures.Select(item => System.IO.Path.GetFullPath(item).ToLower())));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        private IList<SceneXmlDto.TextureUsage> GetTextureUsageCommand(IRepositoryLocator locator, IEnumerable<string> textures)
        {
            // Construct the criteria for retrieving the results.
            ICriteria criteria =
                locator.CreateCriteria<SceneXmlDomain.Texture>()
                    .CreateAlias("Scene", "scene")
                    .SetProjection(Projections.ProjectionList()
                        .Add(Projections.Distinct(Projections.Property("scene.Filename")), "SceneName")
                        .Add(Projections.Property("Filename"), "TextureName"))
                    .Add(Restrictions.In("Filename", textures.ToArray()))
                    .SetResultTransformer(Transformers.AliasToBean(typeof(SceneXmlDto.TextureUsage)));

            // Execute the query and return the results.
            return criteria.List<SceneXmlDto.TextureUsage>();
        }

        /// <summary>
        /// Retrieves a list of materials that make use of the specified texture.
        /// </summary>
        /// <param name="filename">The texture to search for.</param>
        /// <returns></returns>
        public IList<SceneXmlDto.Material> GetMaterialsThatUseTexture(String filename)
        {
            return ExecuteReadOnlyCommand(locator => GetMaterialsThatUseTextureCommand(locator, System.IO.Path.GetFullPath(filename).ToLower()));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        private IList<SceneXmlDto.Material> GetMaterialsThatUseTextureCommand(IRepositoryLocator locator, String filename)
        {
            DetachedCriteria existsCriteria =
                DetachedCriteria.For<SceneXmlDomain.Texture>("t2")
                    .SetProjection(Projections.Property("t2.Material"))
                    .Add(Restrictions.EqProperty("t2.Material", "t.Material"))
                    .Add(Restrictions.Eq("t2.Filename", filename));

            ICriteria textureCriteria =
                locator.CreateCriteria<SceneXmlDomain.Texture>("t")
                    .CreateAlias("Material", "m")
                    .SetFetchMode("Material", FetchMode.Join)
                    .Add(Subqueries.Exists(existsCriteria));

            IList<SceneXmlDomain.Texture> textures = textureCriteria.List<SceneXmlDomain.Texture>();

            IDictionary<SceneXmlDomain.Material, SceneXmlDto.Material> materialDtoLookup =
                new Dictionary<SceneXmlDomain.Material, SceneXmlDto.Material>();
            foreach (SceneXmlDomain.Texture texture in textures.OrderBy(item => item.Id))
            {
                SceneXmlDomain.Material material = texture.Material;
                SceneXmlDto.Material materialDto;

                if (!materialDtoLookup.TryGetValue(material, out materialDto))
                {
                    materialDto = CreateDtoForMaterial(material);
                    materialDtoLookup[material] = materialDto;
                }

                materialDto.Textures.Add(CreateDtoForTexture(texture));
            }

            return materialDtoLookup.Values.ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="material"></param>
        /// <param name="includeTextures"></param>
        /// <returns></returns>
        private SceneXmlDto.Material CreateDtoForMaterial(SceneXmlDomain.Material material, bool includeTextures = false)
        {
            SceneXmlDto.Material dto = new SceneXmlDto.Material();
            dto.Guid = material.Guid;
            dto.Type = material.Type;
            dto.Preset = material.Preset;
            dto.Textures = new List<SceneXmlDto.Texture>();

            if (includeTextures)
            {
                // Add any textures this material makes use of.
                foreach (SceneXmlDomain.Texture texture in material.Textures)
                {
                    dto.Textures.Add(CreateDtoForTexture(texture));
                }
            }

            return dto;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="texture"></param>
        /// <returns></returns>
        private SceneXmlDto.Texture CreateDtoForTexture(SceneXmlDomain.Texture texture)
        {
            SceneXmlDto.Texture dto = new SceneXmlDto.Texture();
            dto.Filename = texture.Filename;
            dto.AlphaFilename = texture.AlphaFilename;
            dto.Type = texture.Type;
            return dto;
        }

        /// <summary>
        /// Retrieves texture/alpha texture pairs for the passed in texture list.
        /// </summary>
        /// <param name="textures">List of textures to retrieve the pairs for.</param>
        /// <returns></returns>
        public IList<SceneXmlDto.TexturePair> GetTexturePairs(IEnumerable<string> textures)
        {
            return ExecuteReadOnlyCommand(locator => GetTexturePairsCommand(locator, textures.Select(item => System.IO.Path.GetFullPath(item).ToLower())));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        private IList<SceneXmlDto.TexturePair> GetTexturePairsCommand(IRepositoryLocator locator, IEnumerable<string> textures)
        {
            // Construct the criteria for retrieving the results.
            ICriteria criteria =
                locator.CreateCriteria<SceneXmlDomain.Texture>()
                    .SetProjection(Projections.ProjectionList()
                        .Add(Projections.Property("Filename"), "TexturePath")
                        .Add(Projections.Property("AlphaFilename"), "AlphaTexturePath")
                        .Add(Projections.GroupProperty("Filename"))
                        .Add(Projections.GroupProperty("AlphaFilename")))
                    .Add(Restrictions.Or(
                        Restrictions.In("Filename", textures.ToArray()),
                        Restrictions.In("AlphaFilename", textures.ToArray())))
                    .SetResultTransformer(Transformers.AliasToBean(typeof(SceneXmlDto.TexturePair)));

            // Execute the query and return the results.
            return criteria.List<SceneXmlDto.TexturePair>();
        }
        #endregion // ISceneXmlQueryService Implementation
    } // SceneXmlService
}
