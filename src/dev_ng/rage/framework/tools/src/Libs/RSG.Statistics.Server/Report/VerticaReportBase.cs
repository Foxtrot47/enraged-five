﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Criterion;
using RSG.Base.Configuration;
using RSG.Base.Configuration.ROS;
using RSG.Base.Logging;
using RSG.ROS;
using RSG.ROS.UGC;
using RSG.Statistics.Domain.Entities;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Domain.Entities.SocialClub;
using RSG.Statistics.Server.Repository;

namespace RSG.Statistics.Server.Report
{
    /// <summary>
    /// Base class for reports that interact with Vertica.
    /// </summary>
    internal abstract class VerticaReportBase<TResult> : StatsReportBase<TResult>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public VerticaReportBase(String name)
            : base(name)
        {
        }
        #endregion // Constructor(s)

        #region Protected Methods
        /// <summary>
        /// Converts a build identifier to a version as is stored in the Vertica database.
        /// </summary>
        protected uint ConvertBuildIdentifierToVersion(String buildIdentifier)
        {
            String[] split = buildIdentifier.Split(new char[] { '.' });

            uint major = 0;
            uint minor = 0;

            if (split.Length > 0)
            {
                major = UInt32.Parse(split[0]);
            }
            if (split.Length > 1)
            {
                minor = UInt32.Parse(split[1]);
            }
            return (major << 4) | minor;
        }

        /// <summary>
        /// Converts a platform to a ROS platform.
        /// </summary>
        protected ROSPlatform ConvertPlatformToROSPlatform(RSG.Platform.Platform platform)
        {
            switch (platform)
            {
                case RSG.Platform.Platform.PS3:
                    return ROSPlatform.PS3;
                case RSG.Platform.Platform.Xbox360:
                    return ROSPlatform.Xbox360;
                case RSG.Platform.Platform.Win32:
                case RSG.Platform.Platform.Win64:
                    return ROSPlatform.Win;
                case RSG.Platform.Platform.XboxOne:
                    return ROSPlatform.XboxOne;
                case RSG.Platform.Platform.PS4:
                    return ROSPlatform.PS4;
                default:
                    Debug.Assert(false, "Unsupported platform encountered.");
                    throw new ArgumentException(String.Format("Unsupported platform '{0}' encountered.", platform));
            }
        }

        /// <summary>
        /// Attempts to convert a platform to a ROS platform, returning whether it was successful or not.
        /// </summary>
        protected bool TryConvertPlatformToROSPlatform(RSG.Platform.Platform platform, out ROSPlatform rosPlatform)
        {
            try
            {
                rosPlatform = ConvertPlatformToROSPlatform(platform);
                return true;
            }
            catch (System.Exception ex)
            {
                rosPlatform = ROSPlatform.PS3;
                return false;
            }
        }
        #endregion // Protected Methods
    } // VerticaReportBase
}
