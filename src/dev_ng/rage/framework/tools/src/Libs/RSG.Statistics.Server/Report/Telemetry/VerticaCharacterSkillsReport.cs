﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using RSG.Base.Logging;
using RSG.Base.Security.Cryptography;
using RSG.Statistics.Client.Vertica.Client;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Model.Telemetry;
using RSG.Statistics.Common.Report;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Server.Repository;

namespace RSG.Statistics.Server.Report.Telemetry
{
    /// <summary>
    /// Report that provides average readings
    /// </summary>
    [Export(typeof(IStatsReport))]
    internal class VerticaCharacterSkillsReport : VerticaTelemetryReportBase<List<PerMissionCharacterSkillStats>>
    {
        #region Constants
        /// <summary>
        /// Name of the report.
        /// </summary>
        private const String c_name = "VerticaCharacterSkills";
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public VerticaCharacterSkillsReport()
            : base(c_name)
        {
        }
        #endregion // Constructor(s)

        #region IReportDataProvider Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        /// <returns></returns>
        protected override List<PerMissionCharacterSkillStats> Generate(IRepositoryLocator locator, ILog log, IStatsServer server)
        {
            // Convert the parameters to something vertica can work with.
            TelemetryParams queryParams = new TelemetryParams();
            SetDefaultParams(locator, queryParams);

            // Contact vertica for the report data.
            List<PerMissionCharacterSkillStats> results;
            using (ReportClient client = new ReportClient(server.VerticaServer))
            {
                results = (List<PerMissionCharacterSkillStats>)client.RunReport(ReportNames.CharacterSkillsReport, queryParams);
            }

            // Patch up the mission names.
            if (results != null)
            {
                IMissionRepository missionRepo = (IMissionRepository)locator.GetRepository<Mission>();
                IDictionary<uint, Mission> missionLookup = missionRepo.CreateAssetLookup();
                IDictionary<uint, Mission> altMissionLookup = missionRepo.CreateAlternativeAssetLookup();

                foreach (PerMissionCharacterSkillStats result in results)
                {
                    Mission mission;
                    if (missionLookup.TryGetValue(OneAtATime.ComputeHash(result.MissionName), out mission))
                    {
                        result.MissionName = mission.Name;
                        result.MissionId = mission.MissionId;
                    }
                    else if (altMissionLookup.TryGetValue(OneAtATime.ComputeHash(result.MissionName), out mission))
                    {
                        result.MissionName = mission.Name;
                        result.MissionId = mission.MissionId;
                    }
                    else
                    {
                        log.Warning("Unknown mission name encountered in data returned from Vertica. Name: {0}", result.MissionName);
                    }
                }
            }

            return results;
        }
        #endregion // IReportDataProvider Implementation
    } // VerticaCharacterSkillsReport
}
