﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using RSG.Base.Configuration.Services;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Dto.ProfileStats;
using RSG.Statistics.Common.ServiceContract;
using RSG.Statistics.Domain.Entities.Profile;
using RSG.Statistics.Server.Repository;

namespace RSG.Statistics.Server.Services
{
    /// <summary>
    /// Profile stat service.
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class ProfileStatService : ServiceBase, IProfileStatService
    {
        #region Constructor(s)
        /// <summary>
        /// Constructor taking a server configuration object.
        /// </summary>
        /// <param name="server"></param>
        public ProfileStatService(IServiceHostConfig config)
            : base(config)
        {
        }
        #endregion // Constructor(s)
        
        #region IProfileStatService Implementation
        /// <summary>
        /// Updates the profile stat definitions that the server is aware of.
        /// </summary>
        /// <returns></returns>
        public void UpdateDefinitions(IList<ProfileStatDefinitionDto> definitions)
        {
            ExecuteCommand(locator => UpdateDefinitionsCommand(locator, definitions));
        }

        /// <summary>
        /// As called by UpdateDefinitions().
        /// </summary>
        private void UpdateDefinitionsCommand(IRepositoryLocator locator, IList<ProfileStatDefinitionDto> definitionSubs)
        {
            // Create a lookup of all profile stat definitions in the db based off of their ROS ids.
            ICriteria criteria = locator.CreateCriteria<ProfileStatDefinition>();
            IList<ProfileStatDefinition> allDefinitions = criteria.List<ProfileStatDefinition>();
            IDictionary<int, ProfileStatDefinition> definitionLookup = allDefinitions.ToDictionary(item => item.RosId);

            // Keep track of the definitions that were submitted (for updating the Active flags).
            ISet<ProfileStatDefinition> activeDefinitions = new HashSet<ProfileStatDefinition>();

            foreach (ProfileStatDefinitionDto definitionSub in definitionSubs)
            {
                ProfileStatDefinition definition;
                if (!definitionLookup.TryGetValue(definitionSub.RosId, out definition))
                {
                    definition = new ProfileStatDefinition();
                }

                // Update the definition's stats.
                definition.RosId = definitionSub.RosId;
                definition.Name = definitionSub.Name;
                definition.Type = definitionSub.Type;
                definition.DefaultValue = definitionSub.DefaultValue;
                definition.Comment = definitionSub.Comment;
                definition.Active = true;
                locator.Save(definition);

                activeDefinitions.Add(definition);
            }

            // Flag all non-active definitions as being so.
            foreach (ProfileStatDefinition definition in allDefinitions.Except(activeDefinitions))
            {
                definition.Active = false;
                locator.Save(definition);
            }
        }


        /// <summary>
        /// Retrieves the list of active definitions.
        /// </summary>
        public IList<ProfileStatDefinitionDto> GetActiveDefinitions()
        {
            return ExecuteCommand(locator => GetActiveDefinitionsCommand(locator));
        }

        /// <summary>
        /// As called by GetActiveDefinitions().
        /// </summary>
        private IList<ProfileStatDefinitionDto> GetActiveDefinitionsCommand(IRepositoryLocator locator)
        {
            ICriteria criteria =
                locator.CreateCriteria<ProfileStatDefinition>()
                    .Add(Restrictions.Eq("Active", true))
                    .SetProjection(Projections.ProjectionList()
                        .Add(Projections.Property("RosId"), "RosId")
                        .Add(Projections.Property("Name"), "Name")
                        .Add(Projections.Property("Type"), "Type")
                        .Add(Projections.Property("DefaultValue"), "DefaultValue")
                        .Add(Projections.Property("Comment"), "Comment"))
                    .SetResultTransformer(Transformers.AliasToBean(typeof(ProfileStatDefinitionDto)));
            return criteria.List<ProfileStatDefinitionDto>();
        }
        #endregion // IProfileStatService Implementation
    } // ProfileStatService
}
