﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using RSG.Base.Logging;
using RSG.Statistics.Client.Vertica.Client;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Model.Telemetry;
using RSG.Statistics.Common.Report;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Server.Repository;

namespace RSG.Statistics.Server.Report.Telemetry.Media
{
    /// <summary>
    /// Report that provides website visit statistics.
    /// </summary>
    [Export(typeof(IStatsReport))]
    internal class VerticaWebsiteVisitsReport : VerticaTelemetryReportBase<List<MediaStat>>
    {
        #region Constants
        /// <summary>
        /// Name of the report.
        /// </summary>
        private const String c_name = "VerticaWebsiteVisits";
        #endregion // Constants
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public VerticaWebsiteVisitsReport()
            : base(c_name)
        {
        }
        #endregion // Constructor(s)

        #region IReportDataProvider Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        /// <returns></returns>
        protected override List<MediaStat> Generate(IRepositoryLocator locator, ILog log, IStatsServer server)
        {
            // Convert the parameters to something vertica can work with.
            TelemetryParams queryParams = new TelemetryParams();
            SetDefaultParams(locator, queryParams);

            // Contact vertica for the report data.
            List<MediaStat> results;
            using (ReportClient client = new ReportClient(server.VerticaServer))
            {
                results = (List<MediaStat>)client.RunReport(ReportNames.WebsiteVisitsReport, queryParams);
            }

            // Convert the website name hashes to friendly strings.
            IGameAssetRepository<WebSite> webSiteRepo = (IGameAssetRepository<WebSite>)locator.GetRepository<WebSite>();
            IDictionary<uint, String> webSiteNameLookup = webSiteRepo.CreateAssetNameLookup();

            foreach (MediaStat result in results)
            {
                String name;
                if (webSiteNameLookup.TryGetValue(result.Hash, out name))
                {
                    result.Name = name;
                }
                else
                {
                    log.Warning("Unknown website hash encountered in data returned from Vertica. Hash: {0}", result.Hash);
                }
            }
            return results;
        }
        #endregion // IReportDataProvider Implementation
    } // VerticaWebsiteVisitsReport
}
