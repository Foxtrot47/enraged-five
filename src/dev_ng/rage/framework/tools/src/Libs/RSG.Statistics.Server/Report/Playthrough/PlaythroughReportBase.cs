﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using NHibernate.SqlCommand;
using RSG.Platform;
using RSG.Statistics.Domain.Entities;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Domain.Entities.Playthrough;
using RSG.Statistics.Server.Extensions;
using RSG.Statistics.Server.Repository;
using PlayModel = RSG.Statistics.Common.Model.Playthrough;

namespace RSG.Statistics.Server.Report.Playthrough
{
    /// <summary>
    /// Base class for playthrough based summary reports.
    /// </summary>
    internal abstract class PlaythroughReportBase<TResult> : StatsReportBase<TResult> where TResult : class
    {
        #region Properties
        /// <summary>
        /// Optional build to restrict the results to.
        /// </summary>
        [ReportParameter]
        public List<String> BuildIdentifiers { get; set; }

        /// <summary>
        /// Optional platform to restrict the results to.
        /// </summary>
        [ReportParameter]
        public List<String> PlatformIdentifiers { get; set; }

        /// <summary>
        /// Optional start date.
        /// </summary>
        [ReportParameter]
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Optional end date.
        /// </summary>
        [ReportParameter]
        public DateTime? EndDate { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public PlaythroughReportBase(String name)
            : base(name)
        {
        }
        #endregion // Constructor(s)

        #region Protected Methods
        /// <summary>
        /// Gets a list of generic playthrough comments.
        /// </summary>
        protected List<PlayModel.PlaythroughComment> GetGenericComments(IRepositoryLocator locator)
        {
            DetachedCriteria criteria =
                DetachedCriteria.For<PlaythroughComment>()
                    .CreateAlias("PlaythroughSession", "ps")
                    .CreateAlias("ps.User", "u")
                    .CreateAlias("Mission", "m", JoinType.LeftOuterJoin)
                    .CreateAlias("Vehicle", "v", JoinType.LeftOuterJoin)
                    .CreateAlias("Weapon", "w", JoinType.LeftOuterJoin);
            AddRestrictions(criteria, locator, "ps");

            List<PlayModel.PlaythroughComment> allComments = new List<PlayModel.PlaythroughComment>();
            foreach (PlaythroughComment dbComment in locator.FindAll<PlaythroughComment>(criteria))
            {
                // Convert the db comment to one we can return.
                PlayModel.PlaythroughComment comment;

                if (dbComment.Mission != null)
                {
                    comment = new PlayModel.PlaythroughMissionComment();
                    ((PlayModel.PlaythroughMissionComment)comment).MissionName = dbComment.Mission.Name;
                }
                else if (dbComment.Vehicle != null)
                {
                    comment = new PlayModel.PlaythroughVehicleComment();
                    ((PlayModel.PlaythroughVehicleComment)comment).VehicleFriendlyName = dbComment.Vehicle.FriendlyName;
                    ((PlayModel.PlaythroughVehicleComment)comment).VehicleGameName = dbComment.Vehicle.GameName;
                }
                else if (dbComment.Weapon != null)
                {
                    comment = new PlayModel.PlaythroughWeaponComment();
                    ((PlayModel.PlaythroughWeaponComment)comment).WeaponName = dbComment.Weapon.FriendlyName;
                }
                else
                {
                    comment = new PlayModel.PlaythroughComment();
                }

                // Set the common properties.
                comment.ParentId = dbComment.PlaythroughSession.Id;
                comment.Comment = dbComment.Comment;
                comment.Username = dbComment.PlaythroughSession.User.Name;
                comment.Timestamp = dbComment.CreatedOn;
                allComments.Add(comment);
            }

            return allComments;
        }

        /// <summary>
        /// Adds the common restrictions to a criteria.
        /// </summary>
        protected virtual void AddRestrictions(DetachedCriteria criteria, IRepositoryLocator locator, String sessionAlias = "")
        {
            String alias = sessionAlias;
            if (sessionAlias != "")
            {
                alias = sessionAlias + ".";
            }

            if (BuildIdentifiers.Any())
            {
                List<Build> builds = BuildIdentifiers.Select(item => locator.GetAssetByIdentifier<Build>(item)).ToList();
                criteria.Add(Expression.In(alias + "Build", builds));
            }
            if (PlatformIdentifiers.Any())
            {
                List<EnumReference<RSG.Platform.Platform>> platforms =
                    PlatformIdentifiers.Select(item => locator.GetEnumReferenceByValue(PlatformUtils.PlatformFromString(item))).ToList();
                criteria.Add(Expression.In(alias + "Platform", platforms));
            }
            if (StartDate != null && EndDate != null)
            {
                criteria.Add(Expression.Between(alias + "Start", StartDate, EndDate));
            }
            else if (StartDate != null)
            {
                criteria.Add(Expression.Ge(alias + "Start", StartDate));
            }
            else if (EndDate != null)
            {
                criteria.Add(Expression.Le(alias + "Start", EndDate));
            }
        }
        #endregion // Protected Methods
    } // PlaythroughReportBase
}
