﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using RSG.Base.Logging;
using RSG.Statistics.Client.Vertica.Client;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Model.Telemetry;
using RSG.Statistics.Common.Report;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Server.Repository;

namespace RSG.Statistics.Server.Report.Telemetry.Media
{
    /// <summary>
    /// Report that provides movie viewer statistics.
    /// </summary>
    [Export(typeof(IStatsReport))]
    internal class VerticaMovieReport : VerticaTelemetryReportBase<List<MediaStat>>
    {
        #region Constants
        /// <summary>
        /// Name of the report.
        /// </summary>
        private const String c_name = "VerticaMovieViewers";
        #endregion // Constants
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public VerticaMovieReport()
            : base(c_name)
        {
        }
        #endregion // Constructor(s)

        #region IReportDataProvider Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        /// <returns></returns>
        protected override List<MediaStat> Generate(IRepositoryLocator locator, ILog log, IStatsServer server)
        {
            // Convert the parameters to something vertica can work with.
            TelemetryParams queryParams = new TelemetryParams();
            SetDefaultParams(locator, queryParams);

            // Contact vertica for the report data.
            List<MediaStat> results;
            using (ReportClient client = new ReportClient(server.VerticaServer))
            {
                results = (List<MediaStat>)client.RunReport(ReportNames.VideoViewerReport, queryParams);
            }

            // Convert the movie names and remove tv show results.
            IGameAssetRepository<TVShow> tvShowRepo = (IGameAssetRepository<TVShow>)locator.GetRepository<TVShow>();
            IGameAssetRepository<Movie> movieRepo = (IGameAssetRepository<Movie>)locator.GetRepository<Movie>();
            IDictionary<uint, TVShow> tvShowLookup = tvShowRepo.CreateAssetLookup();
            IDictionary<uint, Movie> movieLookup = movieRepo.CreateAssetLookup();

            IList<MediaStat> toRemove = new List<MediaStat>();
            foreach (MediaStat result in results)
            {
                Movie movie;
                if (movieLookup.TryGetValue(result.Hash, out movie))
                {
                    result.Name = movie.Name;
                    result.FriendlyName = movie.FriendlyName;
                }
                else if (tvShowLookup.ContainsKey(result.Hash))
                {
                    toRemove.Add(result);
                }
                else
                {
                    log.Warning("Unknown tv show hash encountered in data returned from Vertica. Hash: {0}", result.Hash);
                }
            }

            // Remove the stats that are for tv shows (keep any unknown hashes).
            log.Message("{0} of {1} videos were movies.", results.Count - toRemove.Count, results.Count);
            return results.Except(toRemove).ToList();
        }
        #endregion // IReportDataProvider Implementation
    } // VerticaMovieReport
}
