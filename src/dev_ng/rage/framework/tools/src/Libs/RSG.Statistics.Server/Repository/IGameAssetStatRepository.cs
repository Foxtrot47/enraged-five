﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Domain.Entities.GameAssetStats;

namespace RSG.Statistics.Server.Repository
{
    /// <summary>
    /// Repository interface for game asset stats.
    /// </summary>
    /// <typeparam name="TStat">Stat type (e.g. ClipStat)</typeparam>
    /// <typeparam name="TAsset">Asset type (e.g. Clip)</typeparam>
    internal interface IGameAssetStatRepository<TStat, TAsset> : IRepository<TStat>
        where TStat : class, IGameAssetStat<TAsset>
        where TAsset : class, IGameAsset
    {
        #region Methods
        /// <summary>
        /// Returns all stats associated with the specified build.
        /// </summary>
        IEnumerable<TStat> GetStatsForBuild(Build build);
        #endregion // Methods
    } // IGameAssetStatRepository<TStat, TAsset>
}
