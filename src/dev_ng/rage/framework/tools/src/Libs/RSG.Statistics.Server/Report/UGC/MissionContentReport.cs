﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using RSG.Base.Logging;
using RSG.Base.Security.Cryptography;
using RSG.Statistics.Client.Vertica.Client;
using RSG.Statistics.Common;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Model.UGC;
using RSG.Statistics.Common.Report;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Server.Repository;

namespace RSG.Statistics.Server.Report.UGC
{
    /// <summary>
    /// 
    /// </summary>
    [Export(typeof(IStatsReport))]
    internal class MissionContentReport : MissionContentReportBase<MissionContentSummary>
    {
        #region Constants
        /// <summary>
        /// Name of the report.
        /// </summary>
        private const String c_name = "MissionContent";
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public MissionContentReport()
            : base(c_name)
        {
        }
        #endregion // Constructor(s)

        #region StatsReportBase Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        /// <returns></returns>
        protected override MissionContentSummary Generate(IRepositoryLocator locator, ILog log, IStatsServer server)
        {
            IGameAssetRepository<RadioStation> radioStationRepo = (IGameAssetRepository<RadioStation>)locator.GetRepository<RadioStation>();
            IDictionary<uint, RadioStation> allRadioStations = radioStationRepo.CreateAssetLookup();

            // Convert the parameters to something vertica can work with.
            MissionContentParams parameters = new MissionContentParams();
            parameters.RadioStationLookup = allRadioStations.ToDictionary(item => (int)item.Key, item => item.Value.FriendlyName);
            parameters.RadioStationLookup[(int)OneAtATime.ComputeHash("OFF")] = "Off";
            parameters.RadioStationLookup[-1] = "Random";
            SetDefaultParams(locator, parameters);

            // Contact vertica for the report data.
            using (ReportClient client = new ReportClient(server.VerticaServer))
            {
                return (MissionContentSummary)client.RunReport(ReportNames.MissionContentSummaryReport, parameters);
            }
        }
        #endregion // StatsReportBase Implementation
    } // MissionContentReport
}
