﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using RSG.Statistics.Common.Dto.Reports;
using RSG.Statistics.Server.Report;

namespace RSG.Statistics.Server.Extensions
{
    /// <summary>
    /// Server version of a report parameter.  
    /// </summary>
    public class ServerReportParameterInfo
    {
        #region Static Member Data
        /// <summary>
        /// 
        /// </summary>
        private readonly static IDictionary<Type, String> s_typeToStringMap;
        #endregion // Static Member Data

        #region Properties
        /// <summary>
        /// Name of the report parameter.
        /// </summary>
        public String Name { get; private set; }

        /// <summary>
        /// Type of parameter this is.
        /// </summary>
        public Type Type { get; private set; }

        /// <summary>
        /// Description for the parameter.
        /// </summary>
        public String Description { get; private set; }

        /// <summary>
        /// Whether the user must provide the parameter.
        /// </summary>
        public bool Required { get; private set; }

        /// <summary>
        /// Property info for the property this parameter relates to.
        /// </summary>
        public PropertyInfo PropertyInfo { get; private set; }

        /// <summary>
        /// Method to use for validating this parameter.
        /// </summary>
        private MethodInfo ValidationMethod { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public ServerReportParameterInfo(PropertyInfo pInfo)
        {
            ReportParameterAttribute[] attributes =
                pInfo.GetCustomAttributes(typeof(ReportParameterAttribute), false) as ReportParameterAttribute[];

            if (attributes.Any())
            {
                DescriptionAttribute[] descAttributes =
                    pInfo.GetCustomAttributes(typeof(DescriptionAttribute), false) as DescriptionAttribute[];
                String description = null;
                if (descAttributes.Any())
                {
                    description = descAttributes.First().Description;
                }

                Name = pInfo.Name;
                Type = pInfo.PropertyType;
                Description = description;
                Required = attributes[0].Required;
                PropertyInfo = pInfo;

                // Does this parameter have any special validation conditions?
                if (attributes[0].ValidationMethodName != null && attributes[0].ValidationDeclaringType != null)
                {
                    Type type = attributes[0].ValidationDeclaringType;
                    ValidationMethod = type.GetMethod(attributes[0].ValidationMethodName, BindingFlags.Public | BindingFlags.Static);
                }
            }
            else
            {
                Debug.Assert(false, "Property doesn't have a report parameter attribute associated with it.");
                throw new ArgumentException("Property doesn't have a report parameter attribute associated with it.");
            }
        }
        
        /// <summary>
        /// Static constructor.
        /// </summary>
        static ServerReportParameterInfo()
        {
            // Set up the type to string map.
            s_typeToStringMap = new Dictionary<Type, String>();
            s_typeToStringMap.Add(typeof(bool), "bool");
            s_typeToStringMap.Add(typeof(bool?), "bool");
            s_typeToStringMap.Add(typeof(int), "int");
            s_typeToStringMap.Add(typeof(int?), "int");
            s_typeToStringMap.Add(typeof(uint), "uint");
            s_typeToStringMap.Add(typeof(uint?), "uint");
            s_typeToStringMap.Add(typeof(long), "long");
            s_typeToStringMap.Add(typeof(long?), "long");
            s_typeToStringMap.Add(typeof(ulong), "ulong");
            s_typeToStringMap.Add(typeof(ulong?), "ulong");
            s_typeToStringMap.Add(typeof(float), "float");
            s_typeToStringMap.Add(typeof(float?), "float");
            s_typeToStringMap.Add(typeof(double), "double");
            s_typeToStringMap.Add(typeof(double?), "double");
            s_typeToStringMap.Add(typeof(string), "string");
            s_typeToStringMap.Add(typeof(DateTime), "datetime");
            s_typeToStringMap.Add(typeof(DateTime?), "datetime");
            s_typeToStringMap.Add(typeof(Tuple<String,String>), "tuple<string,string>");
            s_typeToStringMap.Add(typeof(List<Tuple<String, String>>), "list<tuple<string,string>>");
            s_typeToStringMap.Add(typeof(List<String>), "list<string>");
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ReportParameter CreateReportParameter()
        {
            return new ReportParameter(Name, ConvertTypeToString(Type), Description, Required);
        }

        /// <summary>
        /// Converts a string representation of a value to an actual value, validating it along the way.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public object ConvertValue(String value)
        {
            switch (ConvertTypeToString(Type))
            {
                case "bool":
                    return (value != null ? Boolean.Parse(value) : new Nullable<Boolean>());

                case "int":
                    return (value != null ? Int32.Parse(value) : new Nullable<Int32>());

                case "uint":
                    return (value != null ? UInt32.Parse(value) : new Nullable<UInt32>());

                case "long":
                    return (value != null ? Int64.Parse(value) : new Nullable<Int64>());

                case "ulong":
                    return (value != null ? UInt64.Parse(value) : new Nullable<UInt64>());

                case "float":
                    return (value != null ? Single.Parse(value) : new Nullable<Single>());

                case "double":
                    return (value != null ? Double.Parse(value) : new Nullable<Double>());

                case "string":
                    return value;

                case "tuple<string,string>":
                    return ConvertStringToTuple(value);

                case "list<tuple<string,string>>":
                    return (value != null ? value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(item => ConvertStringToTuple(item)).ToList() : new List<Tuple<String, String>>());;

                case "list<string>":
                    return (value != null ? value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList() : new List<String>());

                case "datetime":
                    return (value != null ? DateTime.Parse(value, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal) : new Nullable<DateTime>());

                default:
                    throw new ArgumentException();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public object ValidateValue(object value)
        {
            if (ValidationMethod != null)
            {
                value = ValidationMethod.Invoke(null, new object[] { value });
            }
            return value;
        }
        #endregion // Public Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private String ConvertTypeToString(Type type)
        {
            if (s_typeToStringMap.ContainsKey(type))
            {
                return s_typeToStringMap[type];
            }
            else
            {
                Debug.Assert(false, String.Format("Unknown type supplied: {0}.  Make sure it exists in the s_typeToStringMap.", type.FullName));
                throw new ArgumentException(String.Format("Unknown type supplied: {0}", type.FullName));
            }
        }

        /// <summary>
        /// Converts a string representation of tuple into an actual tuple.
        /// </summary>
        private Tuple<String, String> ConvertStringToTuple(String value)
        {
            Tuple<String, String> tuple = null;
            Regex regex = new Regex(@"^<(?'item1'.*)\|(?'item2'.*)>$");
            Match match = regex.Match(value);
            if (match.Success)
            {
                Group item1Group = match.Groups["item1"];
                Group item2Group = match.Groups["item2"];
                if (item1Group.Success && item2Group.Success)
                {
                    tuple = new Tuple<String, String>(item1Group.Value, item2Group.Value);
                }
            }
            return tuple;
        }
        #endregion // Private Methods
    } // ServerReportParameterInfo
}
