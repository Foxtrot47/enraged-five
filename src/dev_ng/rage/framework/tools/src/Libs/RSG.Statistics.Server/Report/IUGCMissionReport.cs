﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common;

namespace RSG.Statistics.Server.Report
{
    /// <summary>
    /// Interface for reports that require UGC mission parameters as inputs.
    /// </summary>
    internal interface IUGCMissionReport
    {
        /// <summary>
        /// List of mission category names that we wish to restrict the results to.
        /// </summary>
        List<String> MissionCategoryNames { get; set; }

        /// <summary>
        /// Convenience property that transforms the mission category names to mission category enum values.
        /// </summary>
        IList<FreemodeMissionCategory> MissionCategories { get; }

        /// <summary>
        /// Start date for when the match was created.
        /// </summary>
        DateTime? MissionCreatedStart { get; set; }

        /// <summary>
        /// End date for when the match was created.
        /// </summary>
        DateTime? MissionCreatedEnd { get; set; }

        /// <summary>
        /// Names of the gamer handles of mission creators we wish to see the data for.
        /// </summary>
        List<Tuple<String, String>> MissionCreatorUserIdPlatformPairs { get; set; }

        /// <summary>
        /// Optional flag to restrict the results to only published or non-published missions.
        /// </summary>
        bool? MissionPublishedOnly { get; set; }
    } // IUGCMissionReport
}
