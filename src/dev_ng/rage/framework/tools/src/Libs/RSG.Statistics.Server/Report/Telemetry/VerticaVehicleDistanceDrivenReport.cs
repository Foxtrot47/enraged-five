﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using RSG.Base.Logging;
using RSG.Model.Common;
using RSG.Statistics.Client.Vertica.Client;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Dto.GameAssetStats;
using RSG.Statistics.Common.Report;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Server.Repository;

namespace RSG.Statistics.Server.Report.Telemetry
{
    /// <summary>
    /// Report that provides radio station listening statistics.
    /// </summary>
    [Export(typeof(IStatsReport))]
    internal class VerticaVehicleDistanceDrivenReport : VerticaTelemetryReportBase<List<VehicleCategoryUsageStat>>
    {
        #region Constants
        /// <summary>
        /// Name of the report.
        /// </summary>
        private const String c_name = "VerticaVehicleDistanceDriven";
        #endregion // Constants
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public VerticaVehicleDistanceDrivenReport()
            : base(c_name)
        {
        }
        #endregion // Constructor(s)

        #region IReportDataProvider Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        /// <returns></returns>
        protected override List<VehicleCategoryUsageStat> Generate(IRepositoryLocator locator, ILog log, IStatsServer server)
        {
            // Get the list of vehicles.
            IList<Vehicle> allVehicles = locator.CreateCriteria<Vehicle>().List<Vehicle>();

            // Convert the parameters to something vertica can work with.
            VehicleTelemetryParams queryParams = new VehicleTelemetryParams();
            queryParams.Vehicles = allVehicles.ToDictionary(item => UInt32.Parse(item.Identifier),
                item => (VehicleCategory)Enum.Parse(typeof(VehicleCategory), item.Category));
            SetDefaultParams(locator, queryParams);

            // Contact vertica for the report data.
            List<VehicleCategoryUsageStat> results;
            using (ReportClient client = new ReportClient(server.VerticaServer))
            {
                results = (List<VehicleCategoryUsageStat>)client.RunReport(ReportNames.VehicleDistanceDrivenReport, queryParams);
            }

            // Patch up the vehicle names.
            if (results != null)
            {
                // Create a dictionary of vehicle hashes to vehicles.
                IDictionary<uint, Vehicle> vehicleLookup =
                    allVehicles.ToDictionary(item => UInt32.Parse(item.Identifier));

                foreach (VehicleUsageStat result in results.SelectMany(item => item.PerVehicleStats))
                {
                    Vehicle vehicle;
                    if (vehicleLookup.TryGetValue(result.Hash, out vehicle))
                    {
                        result.Name = vehicle.Name;
                        result.FriendlyName = vehicle.FriendlyName;
                    }
                    else
                    {
                        log.Warning("Unknown vehicle hash encountered in data returned from Vertica. Hash: {0}", result.Hash);
                    }
                }
            }

            return results;
        }
        #endregion // IReportDataProvider Implementation
    } // VerticaVehicleDistanceDrivenReport
}
