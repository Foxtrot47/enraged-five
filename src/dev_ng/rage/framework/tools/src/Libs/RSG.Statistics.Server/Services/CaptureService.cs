﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common.ServiceContract;
using System.ServiceModel;
using RSG.Statistics.Common.Dto;
using RSG.Statistics.Server.Repository;
using NHibernate.Criterion;
using RSG.Statistics.Domain.Entities.Captures;
using AutoMapper;
using RSG.Statistics.Domain.Entities;
using RSG.Model.Statistics.Captures;
using RSG.Platform;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Common;
using RSG.Base.Logging;
using RSG.Base.Extensions;
using RSG.Model.Statistics.Captures.Historical;
using System.ServiceModel.Web;
using System.Net;
using NHibernate;
using NHibernate.Transform;
using RSG.Statistics.Common.Config;
using RSG.Base.Configuration.Services;

namespace RSG.Statistics.Server.Services
{
    /// <summary>
    /// 
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class CaptureService : ServiceBase, ICaptureService
    {
        #region Constructor(s)
        /// <summary>
        /// Constructor taking a server configuration object.
        /// </summary>
        /// <param name="server"></param>
        public CaptureService(IServiceHostConfig config)
            : base(config)
        {
        }
        #endregion // Constructor(s)
    
        #region Data Submission
        #region Name Hashes
        /// <summary>
        /// Creates a new zone with the specified name.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public NameHashDto CreateZone(string zoneName)
        {
            return ExecuteCommand(locator => CreateNameHashCommand<CaptureZone>(locator, zoneName));
        }

        /// <summary>
        /// Creates a new cpu set with the specified name.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public NameHashDto CreateCpuSet(string setName)
        {
            return ExecuteCommand(locator => CreateNameHashCommand<CaptureCpuSet>(locator, setName));
        }

        /// <summary>
        /// Creates a new cpu metric with the specified name.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public NameHashDto CreateCpuMetric(string metricName)
        {
            return ExecuteCommand(locator => CreateNameHashCommand<CaptureCpuMetric>(locator, metricName));
        }

        /// <summary>
        /// Creates a new thread with the specified name.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public NameHashDto CreateThreadName(string name)
        {
            return ExecuteCommand(locator => CreateNameHashCommand<CaptureThreadName>(locator, name));
        }

        /// <summary>
        /// Creates a new memory bucket with the specified name.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public NameHashDto CreateMemoryBucket(string bucketName)
        {
            return ExecuteCommand(locator => CreateNameHashCommand<CaptureMemoryBucketName>(locator, bucketName));
        }

        /// <summary>
        /// Creates a new streaming memory module with the specified name.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public NameHashDto CreateStreamingMemoryModule(string moduleName)
        {
            return ExecuteCommand(locator => CreateNameHashCommand<CaptureStreamingMemoryModuleName>(locator, moduleName));
        }

        /// <summary>
        /// Creates a new streaming memory category with the specified name.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public NameHashDto CreateStreamingMemoryCategory(string categoryName)
        {
            return ExecuteCommand(locator => CreateNameHashCommand<CaptureStreamingMemoryCategoryName>(locator, categoryName));
        }

        /// <summary>
        /// Helper function for creating a name hash object of a specific type
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="hash"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        private NameHashDto CreateNameHashCommand<T>(IRepositoryLocator locator, string name) where T : NameHashBase, new()
        {
            T nameHash = GetOrCreateNameHash<T>(locator, name);
            return Mapper.Map<T, NameHashDto>(nameHash);
        }
        #endregion // Name Hashes

        #region Stats
        /// <summary>
        /// Creates a new set of stats associated with a particular changelist.
        /// </summary>
        public void CreateChangelistStats(String changelistNumber, TestSession session)
        {
            ExecuteCommand(locator => CreateChangelistStatsCommand(locator, UInt32.Parse(changelistNumber), session));
        }

        /// <summary>
        /// 
        /// </summary>
        private void CreateChangelistStatsCommand(IRepositoryLocator locator, uint changelistNumber, TestSession session)
        {
            // Convert the various identifiers into their corresponding DB objects (in the case of builds, create one if it doesn't yet exist).
            Build build = GetAssetByIdentifier<Build>(locator, session.Build, false);
            if (build == null)
            {
                build = new Build();
                build.Identifier = session.Build;
                locator.Save(build);
            }

            EnumReference<RSG.Platform.Platform> platform = GetEnumReferenceByValue<RSG.Platform.Platform>(locator, PlatformUtils.PlatformFromString(session.Platform));
            Level level = GetAssetByIdentifier<Level>(locator, RSG.ManagedRage.StringHashUtil.atStringHash(session.LevelName, 0).ToString());
            EnumReference<BuildConfig> buildConfig = GetEnumReferenceByValue<BuildConfig>(locator, BuildConfigUtils.ParseFromRuntimeString(session.BuildConfig));
            
            // Does a capture session already exist for this changelist? If so, delete it and overwrite it's data. (We shouldn't really hit this case tbh.)
            CaptureSession captureSession = GetExistingCaptureSession(locator, build, platform, level, buildConfig, changelistNumber);
            if (captureSession != null)
            {
                locator.Delete(captureSession);
            }

            // Create a new session and create all the capture results.
            captureSession = new CaptureSession();
            captureSession.Build = build;
            captureSession.Platform = platform;
            captureSession.Level = level;
            captureSession.BuildConfig = buildConfig;
            captureSession.Timestamp = session.Timestamp;
            captureSession.ChangelistNumber = changelistNumber;

            captureSession.CpuResults = new List<CaptureCpuResult>();
            captureSession.DrawListResults = new List<CaptureDrawListResult>();
            captureSession.FpsResults = new List<CaptureFpsResult>();
            captureSession.FrametimeResults = new List<CaptureFrametimeResult>();
            captureSession.GpuResults = new List<CaptureGpuResult>();
            captureSession.MemoryResults = new List<CaptureMemoryResult>();
            captureSession.PedAndVehicleResults = new List<CapturePedAndVehicleResult>();
            captureSession.ScriptMemoryResults = new List<CaptureScriptMemoryResult>();
            captureSession.StreamingMemoryResults = new List<CaptureStreamingMemoryResult>();
            captureSession.ThreadResults = new List<CaptureThreadResult>();

            // Process each test result
            foreach (TestResults results in session.TestResults)
            {
                // Get the zone that this set of results is for.
                CaptureZone captureZone = GetOrCreateNameHash<CaptureZone>(locator, results.Name);

                captureSession.CpuResults.AddRange(ProcessCpuResults(locator, captureSession, captureZone, results.CpuResults));
                captureSession.DrawListResults.AddRange(ProcessDrawListResults(locator, captureSession, captureZone, results.DrawListResults));
                captureSession.FpsResults.Add(ProcessFpsResult(locator, captureSession, captureZone, results.FpsResult));
                captureSession.FrametimeResults.Add(ProcessFrametimeResult(locator, captureSession, captureZone, results.FrametimeResult));
                captureSession.GpuResults.AddRange(ProcessGpuResults(locator, captureSession, captureZone, results.GpuResults));
                captureSession.MemoryResults.AddRange(ProcessMemoryResults(locator, captureSession, captureZone, results.MemoryResults));
                captureSession.PedAndVehicleResults.Add(ProcessPedAndVehicleResults(locator, captureSession, captureZone, results.PedAndVehicleResult));
                captureSession.ScriptMemoryResults.Add(ProcessScriptMemoryResult(locator, captureSession, captureZone, results.ScriptMemResult));
                captureSession.StreamingMemoryResults.AddRange(ProcessStreamingMemoryResults(locator, captureSession, captureZone, results.StreamingMemoryResults));
                captureSession.ThreadResults.AddRange(ProcessThreadResults(locator, captureSession, captureZone, results.ThreadResults));
            }

            // Saving the session while save all the individual capture results as well.
            locator.Save(captureSession);
        }


        /// <summary>
        /// Creates a new set of stats for perf stats.
        /// </summary>
        public void CreatePerfStats(TestSession session)
        {
            ExecuteCommand(locator => CreatePerfStatsCommand(locator, session));
        }

        /// <summary>
        /// 
        /// </summary>
        private void CreatePerfStatsCommand(IRepositoryLocator locator, TestSession session)
        {
            // Convert the various identifiers into their corresponding DB objects (in the case of builds, create one if it doesn't yet exist).
            Build build = GetAssetByIdentifier<Build>(locator, session.Build, false);
            if (build == null)
            {
                build = new Build();
                build.Identifier = session.Build;
                locator.Save(build);
            }

            EnumReference<RSG.Platform.Platform> platform = GetEnumReferenceByValue<RSG.Platform.Platform>(locator, PlatformUtils.PlatformFromString(session.Platform));
            Level level = GetAssetByIdentifier<Level>(locator, RSG.ManagedRage.StringHashUtil.atStringHash(session.LevelName, 0).ToString());
            EnumReference<BuildConfig> buildConfig = GetEnumReferenceByValue<BuildConfig>(locator, BuildConfigUtils.ParseFromRuntimeString(session.BuildConfig));

            // Does a capture session already exist for this build? If so, delete it and overwrite it's data. (We shouldn't really hit this case tbh.)
            CaptureSession captureSession = GetExistingPerfStatsCaptureSession(locator, build, platform, level, buildConfig );
            if (captureSession != null)
            {
                locator.Delete(captureSession);
            }

            // Create a new session and create all the capture results.
            captureSession = new CaptureSession();
            captureSession.PerfStats = true;
            captureSession.Build = build;
            captureSession.Platform = platform;
            captureSession.Level = level;
            captureSession.BuildConfig = buildConfig;
            captureSession.Timestamp = session.Timestamp;

            captureSession.CpuResults = new List<CaptureCpuResult>();
            captureSession.DrawListResults = new List<CaptureDrawListResult>();
            captureSession.FpsResults = new List<CaptureFpsResult>();
            captureSession.FrametimeResults = new List<CaptureFrametimeResult>();
            captureSession.GpuResults = new List<CaptureGpuResult>();
            captureSession.MemoryResults = new List<CaptureMemoryResult>();
            captureSession.PedAndVehicleResults = new List<CapturePedAndVehicleResult>();
            captureSession.ScriptMemoryResults = new List<CaptureScriptMemoryResult>();
            captureSession.StreamingMemoryResults = new List<CaptureStreamingMemoryResult>();
            captureSession.ThreadResults = new List<CaptureThreadResult>();

            // Process each test result
            foreach (TestResults results in session.TestResults)
            {
                // Get the zone that this set of results is for.
                CaptureZone captureZone = GetOrCreateNameHash<CaptureZone>(locator, results.Name);

                captureSession.CpuResults.AddRange(ProcessCpuResults(locator, captureSession, captureZone, results.CpuResults));
                captureSession.DrawListResults.AddRange(ProcessDrawListResults(locator, captureSession, captureZone, results.DrawListResults));
                captureSession.FpsResults.Add(ProcessFpsResult(locator, captureSession, captureZone, results.FpsResult));
                captureSession.FrametimeResults.Add(ProcessFrametimeResult(locator, captureSession, captureZone, results.FrametimeResult));
                captureSession.GpuResults.AddRange(ProcessGpuResults(locator, captureSession, captureZone, results.GpuResults));
                captureSession.MemoryResults.AddRange(ProcessMemoryResults(locator, captureSession, captureZone, results.MemoryResults));
                captureSession.PedAndVehicleResults.Add(ProcessPedAndVehicleResults(locator, captureSession, captureZone, results.PedAndVehicleResult));
                captureSession.ScriptMemoryResults.Add(ProcessScriptMemoryResult(locator, captureSession, captureZone, results.ScriptMemResult));
                captureSession.StreamingMemoryResults.AddRange(ProcessStreamingMemoryResults(locator, captureSession, captureZone, results.StreamingMemoryResults));
                captureSession.ThreadResults.AddRange(ProcessThreadResults(locator, captureSession, captureZone, results.ThreadResults));
            }

            // Saving the session while save all the individual capture results as well.
            locator.Save(captureSession);
        }

        #endregion // Stats
        #endregion // Data Submission

        #region Data Retrieval

        #region Availiable builds and changelists
        /// <summary>
        /// Retrieve collection of changelists in the capturesession table.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<uint> RetrieveChangelists()
        {
            return ExecuteCommand(locator => RetrieveChangelistsCommand(locator));
        }

        private IEnumerable<uint> RetrieveChangelistsCommand(IRepositoryLocator locator)
        {
            DetachedCriteria criteria = DetachedCriteria.For<CaptureSession>()
                                                        .Add(Expression.IsNotNull("ChangelistNumber"))
                                                        .SetProjection(Projections.ProjectionList()
                                                            .Add(Projections.Distinct(Projections.Property("ChangelistNumber"))))
                                                        .AddOrder(Order.Desc("ChangelistNumber"));

            IEnumerable<uint> changelists = locator.FindAll<CaptureSession, uint>(criteria);

            return changelists;
        }


        /// <summary>
        /// Retrieve collection of buildids in the capturesession table.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> RetrievePerfstatBuilds()
        {
            return ExecuteCommand(locator => RetrievePerfstatBuildsCommand(locator));
        }

        private IEnumerable<string> RetrievePerfstatBuildsCommand(IRepositoryLocator locator)
        {
            DetachedCriteria criteria = DetachedCriteria.For<CaptureSession>()
                                                        .Add(Expression.IsNotNull("Build"))
                                                        .CreateAlias("Build", "b")                     // Not just an alias, joins a table.                                                      
                                                        .SetProjection(Projections.ProjectionList()
                                                            .Add(Projections.Distinct(Projections.Property("b.Identifier"))))
                                                        .Add(Expression.Eq("PerfStats", true))
                                                        .AddOrder(Order.Desc("b.Identifier"));

            IEnumerable<string> builds = locator.FindAll<CaptureSession, string>(criteria);

            return builds;
        }

        #endregion // Data Retrieval 

        #region Per Changelist Stats
        /// <summary>
        /// Retrieves the changelist number of the latest set of changelist capture stat.
        /// </summary>
        public uint RetrieveLatestChangelistNumber()
        {
            return ExecuteCommand(locator => RetrieveLatestChangelistNumberCommand(locator));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <returns></returns>
        private uint RetrieveLatestChangelistNumberCommand(IRepositoryLocator locator)
        {
            DetachedCriteria criteria = DetachedCriteria.For<CaptureSession>()
                                                        .Add(Expression.IsNotNull("ChangelistNumber"));
            CaptureSession session = locator.FindFirst<CaptureSession>(criteria, Order.Desc("ChangelistNumber"));
            return (session != null ? session.ChangelistNumber.Value : 0);
        }

        /// <summary>
        /// Retrieves the zones that were a part of a particular per changelist capture.
        /// </summary>
        /// <param name="changelistNumber"></param>
        public NameHashDtos RetrieveChangelistZones(String changelistNumber)
        {
            return ExecuteCommand(locator => RetrieveChangelistZonesCommand(locator, UInt32.Parse(changelistNumber)));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="changelistNumber"></param>
        /// <returns></returns>
        private NameHashDtos RetrieveChangelistZonesCommand(IRepositoryLocator locator, uint changelist)
        {
            CaptureSession session = GetCaptureSessionByChangelist(locator, changelist);

            // Retrieve the list of zones using implicit polymorphism.
            DetachedCriteria criteria = DetachedCriteria.For<CaptureResultBase>()
                                                        .Add(Expression.Eq("Session", session))
                                                        .SetProjection(Projections.ProjectionList()
                                                            .Add(Projections.GroupProperty("Zone")));
            IList<CaptureZone> captureZones = locator.FindAll<CaptureSession, CaptureZone>(criteria);

            NameHashDtos dtos = new NameHashDtos();
            foreach (CaptureZone zone in captureZones)
            {
                // The group by zone only works on the individual tables, so we might end up with dupes, which is what we filter out here.
                NameHashDto dto = new NameHashDto(zone.Name);
                if (!dtos.Items.Contains(dto))
                {
                    dtos.Items.Add(dto);
                }
            }

            return dtos;
        }

        /// <summary>
        /// Retrieves all historical data associated with a particular zone.
        /// </summary>
        public HistoricalZoneResults RetrieveHistoricalResultsForZone(String changelistNumber, String zoneIdentifier, uint maxResults)
        {
            return ExecuteCommand(locator => RetrieveHistoricalResultsForZoneCommand(locator, UInt32.Parse(changelistNumber), UInt32.Parse(zoneIdentifier), maxResults));
        }

        /// <summary>
        /// 
        /// </summary>
        private HistoricalZoneResults RetrieveHistoricalResultsForZoneCommand(IRepositoryLocator locator, uint changelist, uint zoneHash, uint maxResults)
        {
            CaptureZone zone = GetNameHashByHash<CaptureZone>(locator, zoneHash);

            // Get all sessions that have a changelist number than the one supplied.
            DetachedCriteria criteria = DetachedCriteria.For<CaptureSession>()
                                                        .Add(Expression.Le("ChangelistNumber", changelist))
                                                        .Add(Expression.IsNotNull("ChangelistNumber"));
            IList<CaptureSession> sessions = locator.FindAll<CaptureSession>(criteria, 0, (int)maxResults, Order.Desc("ChangelistNumber"));

            // Gather the results for the requested zone
            HistoricalZoneResults zoneResults = new HistoricalZoneResults(zone.Name);

            foreach (CaptureSession session in sessions)
            {
                foreach (CaptureFpsResult fpsResult in session.FpsResults.Where(item => item.Zone == zone))
                {
                    zoneResults.FpsResults.AddResult((uint)session.ChangelistNumber, fpsResult.Average, fpsResult.StandardDeviation);
                }

                foreach (CaptureFrametimeResult ftResult in session.FrametimeResults.Where(item => item.Zone == zone))
                {
                    zoneResults.FrametimeResults.AddResult((uint)session.ChangelistNumber, ftResult.Average, ftResult.StandardDeviation);
                }

                foreach (CaptureCpuResult cpuResult in session.CpuResults.Where(item => item.Zone == zone))
                {
                    zoneResults.CpuResults.AddResult((uint)session.ChangelistNumber, ConvertToCpuResult(cpuResult));
                }

                foreach (CaptureGpuResult gpuResult in session.GpuResults.Where(item => item.Zone == zone))
                {
                    zoneResults.GpuResults.AddResult((uint)session.ChangelistNumber, ConvertToGpuResult(gpuResult));
                }

                foreach (CaptureThreadResult threadResult in session.ThreadResults.Where(item => item.Zone == zone))
                {
                    zoneResults.ThreadResults.AddResult((uint)session.ChangelistNumber, ConvertToThreadResult(threadResult));
                }
            }

            return zoneResults;
        }
        #endregion // Per Changelist Stats

        

        #region Perfstat Stats
        /// <summary>
        /// Retrieves the zones that were a part of a particular build perfstat capture.
        /// </summary>
        /// <param name="buildNumber"></param>
        public NameHashDtos RetrievePerfStatZones(String buildNumber)
        {
            return ExecuteCommand(locator => RetrievePerfStatBuildZonesCommand(locator, buildNumber));
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="buildNumber"></param>
        /// <returns></returns>
        private NameHashDtos RetrievePerfStatBuildZonesCommand(IRepositoryLocator locator, string buildNumber)
        {
            CaptureSession session = GetPerfStatCaptureSessionByBuild(locator, buildNumber);

            // Retrieve the list of zones using implicit polymorphism.
            DetachedCriteria criteria = DetachedCriteria.For<CaptureResultBase>()
                                                        .Add(Expression.Eq("Session", session))
                                                        .SetProjection(Projections.ProjectionList()
                                                            .Add(Projections.GroupProperty("Zone")));
            IList<CaptureZone> captureZones = locator.FindAll<CaptureSession, CaptureZone>(criteria);

            NameHashDtos dtos = new NameHashDtos();
            foreach (CaptureZone zone in captureZones)
            {
                // The group by zone only works on the individual tables, so we might end up with dupes, which is what we filter out here.
                NameHashDto dto = new NameHashDto(zone.Name);
                if (!dtos.Items.Contains(dto))
                {
                    dtos.Items.Add(dto);
                }
            }

            return dtos;
        }

        /// <summary>
        /// Retrieves all historical data associated with a particular zone.
        /// </summary>
        public HistoricalZoneResults RetrievePerfStatHistoricalResultsForZone(String buildNumber, String zoneIdentifier, uint maxResults)
        {
            return ExecuteCommand(locator => RetrievePerfStatHistoricalResultsForZoneCommand(locator, buildNumber, UInt32.Parse(zoneIdentifier), maxResults));
        }

        /// <summary>
        /// 
        /// </summary>
        private HistoricalZoneResults RetrievePerfStatHistoricalResultsForZoneCommand(IRepositoryLocator locator, string buildNumber, uint zoneHash, uint maxResults)
        {
            CaptureZone zone = GetNameHashByHash<CaptureZone>(locator, zoneHash);

            Build build = GetAssetByIdentifier<Build>(locator, buildNumber);

            // Get all sessions that have a changelist number than the one supplied.
            DetachedCriteria criteria = DetachedCriteria.For<CaptureSession>()
                                                        .CreateAlias("Build","b")
                                                        .Add(Expression.Le("b.Identifier", buildNumber))
                                                        .Add(Expression.Eq("PerfStats", true))
                                                        .AddOrder(Order.Desc("b.Identifier"));
            IList<CaptureSession> sessions = locator.FindAll<CaptureSession>(criteria, 0, (int)maxResults);

            // Gather the results for the requested zone
            HistoricalZoneResults zoneResults = new HistoricalZoneResults(zone.Name);

            foreach (CaptureSession session in sessions)
            {
                foreach (CaptureFpsResult fpsResult in session.FpsResults.Where(item => item.Zone == zone))
                {
                    zoneResults.FpsResults.AddResult(session.Build.Identifier, fpsResult.Average, fpsResult.StandardDeviation);
                }

                foreach (CaptureFrametimeResult ftResult in session.FrametimeResults.Where(item => item.Zone == zone))
                {
                    zoneResults.FrametimeResults.AddResult((uint)session.ChangelistNumber, ftResult.Average, ftResult.StandardDeviation);
                }

                foreach (CaptureCpuResult cpuResult in session.CpuResults.Where(item => item.Zone == zone))
                {
                    zoneResults.CpuResults.AddResult(session.Build.Identifier, ConvertToCpuResult(cpuResult));
                }

                foreach (CaptureGpuResult gpuResult in session.GpuResults.Where(item => item.Zone == zone))
                {
                    zoneResults.GpuResults.AddResult(session.Build.Identifier, ConvertToGpuResult(gpuResult));
                }

                foreach (CaptureThreadResult threadResult in session.ThreadResults.Where(item => item.Zone == zone))
                {
                    zoneResults.ThreadResults.AddResult(session.Build.Identifier, ConvertToThreadResult(threadResult));
                }
            }

            return zoneResults;
        }
        #endregion // Perfstat Stats

        #region Automated Tests Stats
        /// <summary>
        /// Retrieve the name of the metrics availableto show for a particular automated test.
        /// </summary>
        public List<String> RetrieveAutomatedTestPerformanceMetrics(String automatedTestNumber, String buildIdentifier, String platformIdentifier, uint levelIdentifier)
        {
            return ExecuteReadOnlyCommand(locator => RetrieveAutomatedTestPerformanceMetricsCommand(locator, UInt32.Parse(automatedTestNumber), buildIdentifier, PlatformUtils.PlatformFromString(platformIdentifier), levelIdentifier));
        }

        /// <summary>
        /// 
        /// </summary>
        private List<String> RetrieveAutomatedTestPerformanceMetricsCommand(IRepositoryLocator locator, uint automatedTestNumber, String buildIdentifier, RSG.Platform.Platform platformVal, uint levelIdentifier)
        {
            Build build = GetAssetByIdentifier<Build>(locator, buildIdentifier);
            Level level = GetAssetByIdentifier<Level>(locator, levelIdentifier.ToString());
            EnumReference<RSG.Platform.Platform> platform = GetEnumReferenceByValue(locator, platformVal);

            // This info might be captured by more than one machine which would lead to multiple capture sesssion
            // being created.
            IList<CaptureSession> sessions = GetExistingCaptureSessions(locator, build, platform, level, automatedTestNumber);

            List<String> metrics = new List<String>();

            // Check whether any sessions corresponding to the input parameters exist.
            if (sessions.Any())
            {
                // Do we have any fps results?
                DetachedCriteria criteria =
                    DetachedCriteria.For<CaptureFpsResult>()
                        .Add(Expression.In("Session", sessions.ToArray()));
                CaptureFpsResult fpsResults = locator.FindFirst<CaptureFpsResult>(criteria);
                if (fpsResults != null)
                {
                    metrics.Add("Fps");
                }

                // Do we have any frametime results?
                criteria =
                    DetachedCriteria.For<CaptureFrametimeResult>()
                        .Add(Expression.In("Session", sessions.ToArray()));
                CaptureFrametimeResult ftResults = locator.FindFirst<CaptureFrametimeResult>(criteria);
                if (fpsResults != null)
                {
                    metrics.Add("Frametime");
                }

                criteria =
                    DetachedCriteria.For<CaptureDrawListResult>()
                        .Add(Expression.In("Session", sessions.ToArray()));
                CaptureDrawListResult drawListResults = locator.FindFirst<CaptureDrawListResult>(criteria);
                if (fpsResults != null)
                {
                    metrics.Add("Draw List");
                }

                criteria =
                    DetachedCriteria.For<CaptureThreadResult>()
                        .CreateAlias("Name", "ctn")
                        .SetProjection(Projections.ProjectionList().Add(Projections.Distinct(Projections.Property("ctn.Name"))))
                        .Add(Expression.In("Session", sessions.ToArray()));
                IList<String> threadNames = locator.FindAll<CaptureThreadResult, String>(criteria);

                metrics.AddRange(threadNames);
            }

            return metrics;
        }

        /// <summary>
        /// Retrieves summary information for all automated capture stats associated with a particular build/level/platform.
        /// </summary>
        public TestSessionSummary RetreieveAutomatedTestPerformanceSummary(String automatedTestNumber, String buildIdentifier, String platformIdentifier, uint levelIdentifier)
        {
            return ExecuteReadOnlyCommand(locator => RetreieveAutomatedTestPerformanceSummaryCommand(locator, UInt32.Parse(automatedTestNumber), buildIdentifier, PlatformUtils.PlatformFromString(platformIdentifier), levelIdentifier));
        }

        /// <summary>
        /// 
        /// </summary>
        private TestSessionSummary RetreieveAutomatedTestPerformanceSummaryCommand(IRepositoryLocator locator, uint automatedTestNumber, String buildIdentifier, RSG.Platform.Platform platformVal, uint levelIdentifier)
        {
            Build build = GetAssetByIdentifier<Build>(locator, buildIdentifier);
            Level level = GetAssetByIdentifier<Level>(locator, levelIdentifier.ToString());
            EnumReference<RSG.Platform.Platform> platform = GetEnumReferenceByValue(locator, platformVal);
            
            // This info might be captured by more than one machine which would lead to multiple capture sesssion
            // being created.
            IList<CaptureSession> sessions = GetExistingCaptureSessions(locator, build, platform, level, automatedTestNumber);

            TestSessionSummary summary = new TestSessionSummary();

            // Check whether any sessions corresponding to the input parameters exist.
            if (sessions.Any())
            {
                DetachedCriteria criteria =
                    DetachedCriteria.For<CaptureFpsResult>()
                        .SetProjection(Projections.ProjectionList()
                            .Add(Projections.Min("Minimum"), "MinMin")
                            .Add(Projections.Avg("Minimum"), "AverageMin")
                            .Add(Projections.Max("Minimum"), "MaxMin")
                            .Add(Projections.Min("Maximum"), "MinMax")
                            .Add(Projections.Avg("Maximum"), "AverageMax")
                            .Add(Projections.Max("Maximum"), "MaxMax")
                            .Add(Projections.Min("Average"), "MinAverage")
                            .Add(Projections.Avg("Average"), "AverageAverage")
                            .Add(Projections.Max("Average"), "MaxAverage")
                        )
                        .Add(Expression.In("Session", sessions.ToArray()))
                        .SetFirstResult(0)
                        .SetMaxResults(1)
                        .SetResultTransformer(Transformers.AliasToBean(typeof(SummaryInfo)));
                summary.FpsInfo = locator.FindAll<CaptureFpsResult, SummaryInfo>(criteria).FirstOrDefault();

                criteria =
                    DetachedCriteria.For<CaptureThreadResult>()
                        .CreateAlias("Name", "tn")
                        .SetProjection(Projections.ProjectionList()
                            .Add(Projections.Property("tn.Name"), "Name")
                            .Add(Projections.Min("Minimum"), "MinMin")
                            .Add(Projections.Avg("Minimum"), "AverageMin")
                            .Add(Projections.Max("Minimum"), "MaxMin")
                            .Add(Projections.Min("Maximum"), "MinMax")
                            .Add(Projections.Avg("Maximum"), "AverageMax")
                            .Add(Projections.Max("Maximum"), "MaxMax")
                            .Add(Projections.Min("Average"), "MinAverage")
                            .Add(Projections.Avg("Average"), "AverageAverage")
                            .Add(Projections.Max("Average"), "MaxAverage")
                            .Add(Projections.GroupProperty("Name"))
                        )
                        .Add(Expression.In("Session", sessions.ToArray()))
                        .SetResultTransformer(Transformers.AliasToBean(typeof(ThreadSummaryInfo)));
                summary.ThreadInfo.AddRange(locator.FindAll<CaptureThreadResult, ThreadSummaryInfo>(criteria));

                criteria =
                    DetachedCriteria.For<CaptureCpuResult>()
                        .CreateAlias("Set", "s")
                        .CreateAlias("Metric", "m")
                        .SetProjection(Projections.ProjectionList()
                            .Add(Projections.Property("s.Name"), "Set")
                            .Add(Projections.Property("m.Name"), "Name")
                            .Add(Projections.Min("Minimum"), "MinMin")
                            .Add(Projections.Avg("Minimum"), "AverageMin")
                            .Add(Projections.Max("Minimum"), "MaxMin")
                            .Add(Projections.Min("Maximum"), "MinMax")
                            .Add(Projections.Avg("Maximum"), "AverageMax")
                            .Add(Projections.Max("Maximum"), "MaxMax")
                            .Add(Projections.Min("Average"), "MinAverage")
                            .Add(Projections.Avg("Average"), "AverageAverage")
                            .Add(Projections.Max("Average"), "MaxAverage")
                            .Add(Projections.GroupProperty("Set"))
                            .Add(Projections.GroupProperty("Metric"))
                        )
                        .Add(Expression.In("Session", sessions.ToArray()))
                        .SetResultTransformer(Transformers.AliasToBean(typeof(CpuSummaryInfo)));
                summary.CpuInfo.AddRange(locator.FindAll<CaptureThreadResult, CpuSummaryInfo>(criteria));

                criteria =
                    DetachedCriteria.For<CaptureDrawListResult>()
                        .CreateAlias("DrawList", "dl")
                        .SetProjection(Projections.ProjectionList()
                            .Add(Projections.Property("dl.Name"), "Name")
                            .Add(Projections.Min("Minimum"), "MinMin")
                            .Add(Projections.Avg("Minimum"), "AverageMin")
                            .Add(Projections.Max("Minimum"), "MaxMin")
                            .Add(Projections.Min("Maximum"), "MinMax")
                            .Add(Projections.Avg("Maximum"), "AverageMax")
                            .Add(Projections.Max("Maximum"), "MaxMax")
                            .Add(Projections.Min("Average"), "MinAverage")
                            .Add(Projections.Avg("Average"), "AverageAverage")
                            .Add(Projections.Max("Average"), "MaxAverage")
                            .Add(Projections.GroupProperty("DrawList"))
                        )
                        .Add(Expression.In("Session", sessions.ToArray()))
                        .SetResultTransformer(Transformers.AliasToBean(typeof(DrawListSummaryInfo)));
                summary.DrawListInfo.AddRange(locator.FindAll<CaptureDrawListResult, DrawListSummaryInfo>(criteria));
            }

            return summary;
        }
        #endregion // Automated Tests Stats
        #endregion // Data Retrieval

        #region Private Methods
        /// <summary>
        /// Helper function for retrieving an existing capture session.
        /// </summary>
        private CaptureSession GetExistingCaptureSession(IRepositoryLocator locator, Build build, EnumReference<RSG.Platform.Platform> platform, Level level, EnumReference<BuildConfig> buildConfig, uint changelistNumber)
        {
            DetachedCriteria criteria = DetachedCriteria.For<CaptureSession>()
                                                        .Add(Expression.Eq("Build", build))
                                                        .Add(Expression.Eq("Platform", platform))
                                                        .Add(Expression.Eq("Level", level))
                                                        .Add(Expression.Eq("BuildConfig", buildConfig))
                                                        .Add(Expression.Eq("ChangelistNumber", changelistNumber));
            return locator.FindFirst<CaptureSession>(criteria);
        }

        /// <summary>
        /// Helper function for retrieving existing capture sessions.
        /// </summary>
        private IList<CaptureSession> GetExistingCaptureSessions(IRepositoryLocator locator, Build build, EnumReference<RSG.Platform.Platform> platform, Level level, uint automatedTestNumber)
        {
            DetachedCriteria criteria = DetachedCriteria.For<CaptureSession>()
                                                        .Add(Expression.Eq("Build", build))
                                                        .Add(Expression.Eq("Platform", platform))
                                                        .Add(Expression.Eq("Level", level))
                                                        .Add(Expression.Eq("AutomatedTestNumber", automatedTestNumber));
            return locator.FindAll<CaptureSession>(criteria);
        }

        /// <summary>
        /// Helper function for retrieving an existing capture session (of the perf stats variety).
        /// </summary>
        private CaptureSession GetExistingPerfStatsCaptureSession(IRepositoryLocator locator, Build build, EnumReference<RSG.Platform.Platform> platform, Level level, EnumReference<BuildConfig> buildConfig)
        {
            DetachedCriteria criteria = DetachedCriteria.For<CaptureSession>()
                                                        .Add(Expression.Eq("Build", build))
                                                        .Add(Expression.Eq("Platform", platform))
                                                        .Add(Expression.Eq("Level", level))
                                                        .Add(Expression.Eq("BuildConfig", buildConfig))
                                                        .Add(Expression.Eq("PerfStats", true));
            return locator.FindFirst<CaptureSession>(criteria);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="locator"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        private T GetOrCreateNameHash<T>(IRepositoryLocator locator, String name) where T : NameHashBase, new()
        {
            // Get an existing draw list from the database and if it doesn't exist, create a new one.
            DetachedCriteria query = DetachedCriteria.For<T>()
                                                     .Add(Expression.Eq("Name", name));
            T nameHash = locator.FindFirst<T>(query);

            if (nameHash == null)
            {
                nameHash = new T();
                nameHash.Name = name;
                nameHash.Hash = RSG.ManagedRage.StringHashUtil.atStringHash(name, 0);
                locator.Save(nameHash);
            }

            return nameHash;
        }

        /// <summary>
        /// Retrieves a name hash value based on it's hash
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="locator"></param>
        /// <param name="hash"></param>
        /// <returns></returns>
        private T GetNameHashByHash<T>(IRepositoryLocator locator, uint hash) where T : NameHashBase
        {
            DetachedCriteria query = DetachedCriteria.For<T>()
                                                     .Add(Expression.Eq("Hash", hash));
            T nameHash = locator.FindFirst<T>(query);
            if (nameHash == null)
            {
                throw new WebFaultException<String>(String.Format("Name hash of type '{0}' with hash '{1}' doesn't exist in the database.", typeof(T).Name, hash), HttpStatusCode.BadRequest);
            }
            return nameHash;
        }

        /// <summary>
        /// Helper function that converts cpu results to database objects.
        /// </summary>
        private IEnumerable<CaptureCpuResult> ProcessCpuResults(IRepositoryLocator locator, CaptureSession session, CaptureZone zone, ICollection<CpuResult> results)
        {
            foreach (CpuResult result in results)
            {
                CaptureCpuResult captureResult = new CaptureCpuResult(session, zone);
                captureResult.Set = GetOrCreateNameHash<CaptureCpuSet>(locator, result.Set);
                captureResult.Metric = GetOrCreateNameHash<CaptureCpuMetric>(locator, result.Name);
                captureResult.Minimum = result.Min;
                captureResult.Maximum = result.Max;
                captureResult.Average = result.Average;
                captureResult.StandardDeviation = result.StandardDeviation;

                yield return captureResult;
            }
        }

        /// <summary>
        /// Helper function that converts draw list results to database objects.
        /// </summary>
        private IEnumerable<CaptureDrawListResult> ProcessDrawListResults(IRepositoryLocator locator, CaptureSession session, CaptureZone zone, ICollection<DrawListResult> results)
        {
            foreach (DrawListResult result in results)
            {
                CaptureDrawListResult captureResult = new CaptureDrawListResult(session, zone);
                captureResult.DrawList = GetDrawListByName(locator, result.Name);
                captureResult.Minimum = result.Min;
                captureResult.Maximum = result.Max;
                captureResult.Average = result.Average;
                captureResult.StandardDeviation = result.StandardDeviation;

                yield return captureResult;
            }
        }

        /// <summary>
        /// Gets a drawlist by its name.
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="hash"></param>
        /// <returns></returns>
        private DrawList GetDrawListByName(IRepositoryLocator locator, String name)
        {
            // Get the vehicle in question
            DetachedCriteria query = DetachedCriteria.For<DrawList>()
                                                     .Add(Expression.Eq("Name", name));
            return locator.FindFirst<DrawList>(query);
        }

        /// <summary>
        /// Helper function that converts a fps result to it's database version.
        /// </summary>
        private CaptureFpsResult ProcessFpsResult(IRepositoryLocator locator, CaptureSession session, CaptureZone zone, FpsResult result)
        {
            CaptureFpsResult captureResult = new CaptureFpsResult(session, zone);
            captureResult.Minimum = result.Min;
            captureResult.Maximum = result.Max;
            captureResult.Average = result.Average;
            captureResult.StandardDeviation = result.StandardDeviation;
            return captureResult;
        }

        /// <summary>
        /// Helper function that converts a frametime result to it's database version.
        /// </summary>
        private CaptureFrametimeResult ProcessFrametimeResult(IRepositoryLocator locator, CaptureSession session, CaptureZone zone, FrametimeResult result)
        {
            CaptureFrametimeResult captureResult = new CaptureFrametimeResult(session, zone);
            captureResult.Minimum = result.Min;
            captureResult.Maximum = result.Max;
            captureResult.Average = result.Average;
            captureResult.StandardDeviation = result.StandardDeviation;
            return captureResult;
        }

        /// <summary>
        /// Helper function that converts gpu results to database objects.
        /// </summary>
        private IEnumerable<CaptureGpuResult> ProcessGpuResults(IRepositoryLocator locator, CaptureSession session, CaptureZone zone, ICollection<GpuResult> results)
        {
            foreach (GpuResult result in results)
            {
                CaptureGpuResult captureResult = new CaptureGpuResult(session, zone);
                captureResult.DrawList = GetDrawListByName(locator, result.Name);
                captureResult.Time = result.Time;

                yield return captureResult;
            }
        }

        /// <summary>
        /// Helper function that converts memory results to database objects.
        /// </summary>
        private IEnumerable<CaptureMemoryResult> ProcessMemoryResults(IRepositoryLocator locator, CaptureSession session, CaptureZone zone, ICollection<MemoryResult> results)
        {
            foreach (MemoryResult result in results)
            {
                CaptureMemoryResult captureResult = new CaptureMemoryResult(session, zone);
                captureResult.BucketName = GetOrCreateNameHash<CaptureMemoryBucketName>(locator, result.Name);
                captureResult.MinimumGamePhysical = result.GamePhysical.Min;
                captureResult.MaximumGamePhysical = result.GamePhysical.Max;
                captureResult.AverageGamePhysical = result.GamePhysical.Average;
                captureResult.GamePhysicalStandardDeviation = result.GamePhysical.StandardDeviation;

                captureResult.MinimumGameVirtual = result.GameVirtual.Min;
                captureResult.MaximumGameVirtual = result.GameVirtual.Max;
                captureResult.AverageGameVirtual = result.GameVirtual.Average;
                captureResult.GameVirtualStandardDeviation = result.GameVirtual.StandardDeviation;

                captureResult.MinimumResourcePhysical = result.ResourcePhysical.Min;
                captureResult.MaximumResourcePhysical = result.ResourcePhysical.Max;
                captureResult.AverageResourcePhysical = result.ResourcePhysical.Average;
                captureResult.ResourcePhysicalStandardDeviation = result.ResourcePhysical.StandardDeviation;

                captureResult.MinimumResourceVirtual = result.ResourceVirtual.Min;
                captureResult.MaximumResourceVirtual = result.ResourceVirtual.Max;
                captureResult.AverageResourceVirtual = result.ResourceVirtual.Average;
                captureResult.ResourceVirtualStandardDeviation = result.ResourceVirtual.StandardDeviation;

                yield return captureResult;
            }
        }

        /// <summary>
        /// Helper function that converts a ped and vehicle result to a database object.
        /// </summary>
        private CapturePedAndVehicleResult ProcessPedAndVehicleResults(IRepositoryLocator locator, CaptureSession session, CaptureZone zone, PedAndVehicleResult result)
        {
            CapturePedAndVehicleResult captureResult = new CapturePedAndVehicleResult(session, zone);
            if (result.NumPeds != null)
            {
                captureResult.MinNumPeds = result.NumPeds.Min;
                captureResult.MaxNumPeds = result.NumPeds.Max;
                captureResult.AvgNumPeds = result.NumPeds.Average;
            }

            if (result.NumActivePeds != null)
            {
                captureResult.MinActivePeds = result.NumActivePeds.Min;
                captureResult.MaxActivePeds = result.NumActivePeds.Max;
                captureResult.AvgActivePeds = result.NumActivePeds.Average;
            }

            if (result.NumInactivePeds != null)
            {
                captureResult.MinInactivePeds = result.NumInactivePeds.Min;
                captureResult.MaxInactivePeds = result.NumInactivePeds.Max;
                captureResult.AvgInactivePeds = result.NumInactivePeds.Average;
            }

            if (result.NumEventScanHiPeds != null)
            {
                captureResult.MinEventScanHiPeds = result.NumEventScanHiPeds.Min;
                captureResult.MaxEventScanHiPeds = result.NumEventScanHiPeds.Max;
                captureResult.AvgEventScanHiPeds = result.NumEventScanHiPeds.Average;
            }

            if (result.NumEventScanLoPeds != null)
            {
                captureResult.MinEventScanLoPeds = result.NumEventScanLoPeds.Min;
                captureResult.MaxEventScanLoPeds = result.NumEventScanLoPeds.Max;
                captureResult.AvgEventScanLoPeds = result.NumEventScanLoPeds.Average;
            }

            if (result.NumMotionTaskHiPeds != null)
            {
                captureResult.MinMotionTaskHiPeds = result.NumMotionTaskHiPeds.Min;
                captureResult.MaxMotionTaskHiPeds = result.NumMotionTaskHiPeds.Max;
                captureResult.AvgMotionTaskHiPeds = result.NumMotionTaskHiPeds.Average;
            }

            if (result.NumMotionTaskLoPeds != null)
            {
                captureResult.MinMotionTaskLoPeds = result.NumMotionTaskLoPeds.Min;
                captureResult.MaxMotionTaskLoPeds = result.NumMotionTaskLoPeds.Max;
                captureResult.AvgMotionTaskLoPeds = result.NumMotionTaskLoPeds.Average;
            }

            if (result.NumPhysicsHiPeds != null)
            {
                captureResult.MinPhysicsHiPeds = result.NumPhysicsHiPeds.Min;
                captureResult.MaxPhysicsHiPeds = result.NumPhysicsHiPeds.Max;
                captureResult.AvgPhysicsHiPeds = result.NumPhysicsHiPeds.Average;
            }

            if (result.NumPhysicsLoPeds != null)
            {
                captureResult.MinPhysicsLoPeds = result.NumPhysicsLoPeds.Min;
                captureResult.MaxPhysicsLoPeds = result.NumPhysicsLoPeds.Max;
                captureResult.AvgPhysicsLoPeds = result.NumPhysicsLoPeds.Average;
            }

            if (result.NumEntityScanHiPeds != null)
            {
                captureResult.MinEntityScanHiPeds = result.NumEntityScanHiPeds.Min;
                captureResult.MaxEntityScanHiPeds = result.NumEntityScanHiPeds.Max;
                captureResult.AvgEntityScanHiPeds = result.NumEntityScanHiPeds.Average;
            }

            if (result.NumEntityScanLoPeds != null)
            {
                captureResult.MinEntityScanLoPeds = result.NumEntityScanLoPeds.Min;
                captureResult.MaxEntityScanLoPeds = result.NumEntityScanLoPeds.Max;
                captureResult.AvgEntityScanLoPeds = result.NumEntityScanLoPeds.Average;
            }

            if (result.NumVehicles != null)
            {
                captureResult.MinNumVehicles = result.NumVehicles.Min;
                captureResult.MaxNumVehicles = result.NumVehicles.Max;
                captureResult.AvgNumVehicles = result.NumVehicles.Average;
            }

            if (result.NumActiveVehicles != null)
            {
                captureResult.MinActiveVehicles = result.NumActiveVehicles.Min;
                captureResult.MaxActiveVehicles = result.NumActiveVehicles.Max;
                captureResult.AvgActiveVehicles = result.NumActiveVehicles.Average;
            }

            if (result.NumInactiveVehicles != null)
            {
                captureResult.MinInactiveVehicles = result.NumInactiveVehicles.Min;
                captureResult.MaxInactiveVehicles = result.NumInactiveVehicles.Max;
                captureResult.AvgInactiveVehicles = result.NumInactiveVehicles.Average;
            }

            if (result.NumRealVehicles != null)
            {
                captureResult.MinRealVehicles = result.NumRealVehicles.Min;
                captureResult.MaxRealVehicles = result.NumRealVehicles.Max;
                captureResult.AvgRealVehicles = result.NumRealVehicles.Average;
            }

            if (result.NumDummyVehicles != null)
            {
                captureResult.MinDummyVehicles = result.NumDummyVehicles.Min;
                captureResult.MaxDummyVehicles = result.NumDummyVehicles.Max;
                captureResult.AvgDummyVehicles = result.NumDummyVehicles.Average;
            }

            if (result.NumSuperDummyVehicles != null)
            {
                captureResult.MinSuperDummyVehicles = result.NumSuperDummyVehicles.Min;
                captureResult.MaxSuperDummyVehicles = result.NumSuperDummyVehicles.Max;
                captureResult.AvgSuperDummyVehicles = result.NumSuperDummyVehicles.Average;
            }

            if (result.NumNetworkDummyVehicles != null)
            {
                captureResult.MinNetworkDummyVehicles = result.NumNetworkDummyVehicles.Min;
                captureResult.MaxNetworkDummyVehicles = result.NumNetworkDummyVehicles.Max;
                captureResult.AvgNetworkDummyVehicles = result.NumNetworkDummyVehicles.Average;
            }
            
            return captureResult;
        }

        /// <summary>
        /// Helper function that converts a script memory result to a database object.
        /// </summary>
        private CaptureScriptMemoryResult ProcessScriptMemoryResult(IRepositoryLocator locator, CaptureSession session, CaptureZone zone, ScriptMemResult result)
        {
            CaptureScriptMemoryResult captureResult = new CaptureScriptMemoryResult(session, zone);
            captureResult.MinimumPhysical = result.Physical.Min;
            captureResult.MaximumPhysical = result.Physical.Max;
            captureResult.AveragePhysical = result.Physical.Average;
            captureResult.PhysicalStandardDeviation = result.Physical.StandardDeviation;
            captureResult.MinimumVirtual = result.Virtual.Min;
            captureResult.MaximumVirtual = result.Virtual.Max;
            captureResult.AverageVirtual = result.Virtual.Average;
            captureResult.VirtualStandardDeviation = result.Virtual.StandardDeviation;
            return captureResult;
        }

        /// <summary>
        /// Helper function that converts streaming memory results to database objects.
        /// </summary>
        private IEnumerable<CaptureStreamingMemoryResult> ProcessStreamingMemoryResults(IRepositoryLocator locator, CaptureSession session, CaptureZone zone, ICollection<StreamingMemoryResult> results)
        {
            foreach (StreamingMemoryResult result in results)
            {
                CaptureStreamingMemoryModuleName module = GetOrCreateNameHash<CaptureStreamingMemoryModuleName>(locator, result.ModuleName);

                foreach (KeyValuePair<String, StreamingMemoryStat> pair in result.Categories)
                {
                    CaptureStreamingMemoryResult captureResult = new CaptureStreamingMemoryResult(session, zone);
                    captureResult.Module = module;
                    captureResult.Category = GetOrCreateNameHash<CaptureStreamingMemoryCategoryName>(locator, pair.Key);
                    captureResult.PhysicalMemory = pair.Value.Physical;
                    captureResult.VirtualMemory = pair.Value.Virtual;

                    yield return captureResult;
                }
            }
        }

        /// <summary>
        /// Helper function that converts thread results to database objects.
        /// </summary>
        private IEnumerable<CaptureThreadResult> ProcessThreadResults(IRepositoryLocator locator, CaptureSession session, CaptureZone zone, ICollection<ThreadResult> results)
        {
            foreach (ThreadResult result in results)
            {
                CaptureThreadResult captureResult = new CaptureThreadResult(session, zone);
                captureResult.Name = GetOrCreateNameHash<CaptureThreadName>(locator, result.Name);
                captureResult.Minimum = result.Min;
                captureResult.Maximum = result.Max;
                captureResult.Average = result.Average;
                captureResult.StandardDeviation = result.StandardDeviation;

                yield return captureResult;
            }
        }

        /// <summary>
        /// Retrieves a capture session by a changelist number.
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="changelistNumber"></param>
        /// <returns></returns>
        private CaptureSession GetCaptureSessionByChangelist(IRepositoryLocator locator, uint changelistNumber)
        {
            DetachedCriteria criteria = DetachedCriteria.For<CaptureSession>()
                                                        .Add(Expression.Eq("ChangelistNumber", changelistNumber));
            CaptureSession session = locator.FindOne<CaptureSession>(criteria);
            if (session == null)
            {
                throw new WebFaultException<String>(String.Format("Capture session with changelist number {0} doesn't exist.", changelistNumber), HttpStatusCode.BadRequest);
            }
            return session;
        }

        /// <summary>
        /// Retrieves a perfstats capture session by a build number.
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="buildNumber"></param>
        /// <returns></returns>
        private CaptureSession GetPerfStatCaptureSessionByBuild(IRepositoryLocator locator, string buildNumber)
        {
            DetachedCriteria criteria = DetachedCriteria.For<CaptureSession>()
                                                        .CreateAlias("Build", "b")
                                                        .Add(Expression.Eq("b.Identifier", buildNumber))
                                                        .Add(Expression.Eq("PerfStats", true));
            CaptureSession session = locator.FindOne<CaptureSession>(criteria);
            if (session == null)
            {
                throw new WebFaultException<String>(String.Format("Capture session with build number {0} doesn't exist.", buildNumber), HttpStatusCode.BadRequest);
            }
            return session;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        private FpsResult ConvertToFpsResult(CaptureFpsResult dbResult)
        {
            FpsResult modelResult = new FpsResult();
            modelResult.Min = dbResult.Minimum;
            modelResult.Max = dbResult.Maximum;
            modelResult.Average = dbResult.Average;
            modelResult.StandardDeviation = dbResult.StandardDeviation;
            return modelResult;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        private FrametimeResult ConvertToFrametimeResult(CaptureFrametimeResult dbResult)
        {
            FrametimeResult modelResult = new FrametimeResult();
            modelResult.Min = dbResult.Minimum;
            modelResult.Max = dbResult.Maximum;
            modelResult.Average = dbResult.Average;
            modelResult.StandardDeviation = dbResult.StandardDeviation;
            return modelResult;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbResult"></param>
        /// <returns></returns>
        public DrawListResult ConvertToDrawListResult(CaptureDrawListResult dbResult)
        {
            DrawListResult modelResult = new DrawListResult();
            modelResult.Name = dbResult.DrawList.Name;
            modelResult.Min = dbResult.Minimum;
            modelResult.Max = dbResult.Maximum;
            modelResult.Average = dbResult.Average;
            modelResult.StandardDeviation = dbResult.StandardDeviation;
            return modelResult;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbResult"></param>
        /// <returns></returns>
        private CpuResult ConvertToCpuResult(CaptureCpuResult dbResult)
        {
            CpuResult modelResult = new CpuResult();
            modelResult.Name = dbResult.Metric.Name;
            modelResult.Set = dbResult.Set.Name;
            modelResult.Min = dbResult.Minimum;
            modelResult.Max = dbResult.Maximum;
            modelResult.Average = dbResult.Average;
            modelResult.StandardDeviation = dbResult.StandardDeviation;
            return modelResult;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbResult"></param>
        /// <returns></returns>
        private GpuResult ConvertToGpuResult(CaptureGpuResult dbResult)
        {
            GpuResult modelResult = new GpuResult();
            modelResult.Name = dbResult.DrawList.Name;
            modelResult.Time = dbResult.Time;
            return modelResult;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbResult"></param>
        /// <returns></returns>
        private ThreadResult ConvertToThreadResult(CaptureThreadResult dbResult)
        {
            ThreadResult modelResult = new ThreadResult();
            modelResult.Name = dbResult.Name.Name;
            modelResult.Min = dbResult.Minimum;
            modelResult.Max = dbResult.Maximum;
            modelResult.Average = dbResult.Average;
            modelResult.StandardDeviation = dbResult.StandardDeviation;
            return modelResult;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbResult"></param>
        /// <returns></returns>
        private MemoryResult ConvertToMemoryResult(CaptureMemoryResult dbResult)
        {
            MemoryResult modelResult = new MemoryResult();
            modelResult.Name = dbResult.BucketName.Name;
            modelResult.GamePhysical = new MemoryStat(dbResult.MinimumGamePhysical, dbResult.MaximumGamePhysical, dbResult.AverageGamePhysical, dbResult.GamePhysicalStandardDeviation);
            modelResult.GameVirtual = new MemoryStat(dbResult.MinimumGameVirtual, dbResult.MaximumGameVirtual, dbResult.AverageGameVirtual, dbResult.GameVirtualStandardDeviation);
            modelResult.ResourcePhysical = new MemoryStat(dbResult.MinimumResourcePhysical, dbResult.MaximumResourcePhysical, dbResult.AverageResourcePhysical, dbResult.ResourcePhysicalStandardDeviation);
            modelResult.ResourceVirtual = new MemoryStat(dbResult.MinimumResourceVirtual, dbResult.MaximumResourceVirtual, dbResult.AverageResourceVirtual, dbResult.ResourceVirtualStandardDeviation);
            return modelResult;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbResult"></param>
        /// <returns></returns>
        private StreamingMemoryResult ConvertToStreamingMemoryResult(String moduleName, IEnumerable<CaptureStreamingMemoryResult> dbResults)
        {
            StreamingMemoryResult modelResult = new StreamingMemoryResult();
            modelResult.ModuleName = moduleName;

            foreach (CaptureStreamingMemoryResult dbResult in dbResults)
            {
                StreamingMemoryStat stat = new StreamingMemoryStat(dbResult.Category.Name, dbResult.VirtualMemory, dbResult.PhysicalMemory);
                if (!modelResult.Categories.ContainsKey(dbResult.Category.Name))
                {
                    modelResult.Categories.Add(dbResult.Category.Name, stat);
                }
            }

            return modelResult;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbResult"></param>
        /// <returns></returns>
        private ScriptMemResult ConvertToScriptMemoryResult(CaptureScriptMemoryResult dbResult)
        {
            ScriptMemResult modelResult = new ScriptMemResult();
            modelResult.Physical = new MemoryStat(dbResult.MinimumPhysical, dbResult.MaximumPhysical, dbResult.AveragePhysical, dbResult.PhysicalStandardDeviation);
            modelResult.Virtual = new MemoryStat(dbResult.MinimumVirtual, dbResult.MaximumVirtual, dbResult.AverageVirtual, dbResult.VirtualStandardDeviation);
            return modelResult;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbResult"></param>
        /// <returns></returns>
        private PedAndVehicleResult ConvertToPedAndVehicleResult(CapturePedAndVehicleResult dbResult)
        {
            PedAndVehicleResult modelResult = new PedAndVehicleResult();
            modelResult.NumPeds = new PedAndVehicleStat(dbResult.MinNumPeds, dbResult.MaxNumPeds, dbResult.AvgNumPeds);
            modelResult.NumActivePeds = new PedAndVehicleStat(dbResult.MinActivePeds, dbResult.MaxActivePeds, dbResult.AvgActivePeds);
            modelResult.NumInactivePeds = new PedAndVehicleStat(dbResult.MinInactivePeds, dbResult.MaxInactivePeds, dbResult.AvgInactivePeds);
            modelResult.NumEventScanHiPeds = new PedAndVehicleStat(dbResult.MinEventScanHiPeds, dbResult.MaxEventScanHiPeds, dbResult.AvgEventScanHiPeds);
            modelResult.NumEventScanLoPeds = new PedAndVehicleStat(dbResult.MinEventScanLoPeds, dbResult.MaxEventScanLoPeds, dbResult.AvgEventScanLoPeds);
            modelResult.NumMotionTaskHiPeds = new PedAndVehicleStat(dbResult.MinMotionTaskHiPeds, dbResult.MaxMotionTaskHiPeds, dbResult.AvgMotionTaskHiPeds);
            modelResult.NumMotionTaskLoPeds = new PedAndVehicleStat(dbResult.MinMotionTaskLoPeds, dbResult.MaxMotionTaskLoPeds, dbResult.AvgMotionTaskLoPeds);
            modelResult.NumPhysicsHiPeds = new PedAndVehicleStat(dbResult.MinPhysicsHiPeds, dbResult.MaxPhysicsHiPeds, dbResult.AvgPhysicsHiPeds);
            modelResult.NumPhysicsLoPeds = new PedAndVehicleStat(dbResult.MinPhysicsLoPeds, dbResult.MaxPhysicsLoPeds, dbResult.AvgPhysicsLoPeds);
            modelResult.NumEntityScanHiPeds = new PedAndVehicleStat(dbResult.MinEntityScanHiPeds, dbResult.MaxEntityScanHiPeds, dbResult.AvgEntityScanHiPeds);
            modelResult.NumEntityScanLoPeds = new PedAndVehicleStat(dbResult.MinEntityScanLoPeds, dbResult.MaxEntityScanLoPeds, dbResult.AvgEntityScanLoPeds);
            modelResult.NumVehicles = new PedAndVehicleStat(dbResult.MinNumVehicles, dbResult.MaxNumVehicles, dbResult.AvgNumVehicles);
            modelResult.NumActiveVehicles = new PedAndVehicleStat(dbResult.MinActiveVehicles, dbResult.MaxActiveVehicles, dbResult.AvgActiveVehicles);
            modelResult.NumInactiveVehicles = new PedAndVehicleStat(dbResult.MinInactiveVehicles, dbResult.MaxInactiveVehicles, dbResult.AvgInactiveVehicles);
            modelResult.NumRealVehicles = new PedAndVehicleStat(dbResult.MinRealVehicles, dbResult.MaxRealVehicles, dbResult.AvgRealVehicles);
            modelResult.NumDummyVehicles = new PedAndVehicleStat(dbResult.MinDummyVehicles, dbResult.MaxDummyVehicles, dbResult.AvgDummyVehicles);
            modelResult.NumSuperDummyVehicles = new PedAndVehicleStat(dbResult.MinSuperDummyVehicles, dbResult.MaxSuperDummyVehicles, dbResult.AvgSuperDummyVehicles);
            modelResult.NumNetworkDummyVehicles = new PedAndVehicleStat(dbResult.MinNetworkDummyVehicles, dbResult.MaxNetworkDummyVehicles, dbResult.AvgNetworkDummyVehicles);
            return modelResult;
        }
        #endregion // Private Methods
    } // CaptureService
}
