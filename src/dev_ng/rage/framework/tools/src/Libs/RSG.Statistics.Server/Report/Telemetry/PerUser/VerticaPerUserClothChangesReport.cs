﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using RSG.Base.Logging;
using RSG.Statistics.Client.Vertica.Client;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Model.Telemetry.PerUser;
using RSG.Statistics.Common.Report;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.Server.Repository;

namespace RSG.Statistics.Server.Report.Telemetry.PerUser
{
    /// <summary>
    /// Report that returns the number of times users have changed items of clothing.
    /// </summary>
    [Export(typeof(IStatsReport))]
    internal class VerticaPerUserClothChangesReport : VerticaPerUserTelemetryReportBase<List<PerUserClothesChangeStat>>
    {
        #region Constants
        /// <summary>
        /// Name of the report.
        /// </summary>
        private const String c_name = "VerticaPerUserClothChanges";
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public VerticaPerUserClothChangesReport()
            : base(c_name)
        {
        }
        #endregion // Constructor(s)

        #region IStatsReport Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        protected override List<PerUserClothesChangeStat> Generate(IRepositoryLocator locator, ILog log, IStatsServer server)
        {
            // Convert the parameters to something vertica can work with.
            PerUserTelemetryParams queryParams = new PerUserTelemetryParams();
            SetDefaultParams(locator, queryParams);

            // Get the results from the vertica service.
            using (ReportClient client = new ReportClient(server.VerticaServer))
            {
                return (List<PerUserClothesChangeStat>)client.RunReport(ReportNames.PerUserClothChangesReport, queryParams);
            }
        }
        #endregion // IStatsReport Implementation
    } // VerticaPerUserClothChangesReport
}
