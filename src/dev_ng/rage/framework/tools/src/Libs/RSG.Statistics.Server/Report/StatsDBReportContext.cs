﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Logging;
using RSG.Model.Common.Report;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Report;
using RSG.Statistics.Server.Repository;

namespace RSG.Statistics.Server.Report
{
    /// <summary>
    /// Report context for stat server based reports.
    /// </summary>
    internal class StatsDBReportContext : IReportContext
    {
        /// <summary>
        /// Repository locator.
        /// </summary>
        public IRepositoryLocator Locator { get; set; }

        /// <summary>
        /// Log object.
        /// </summary>
        public ILog Log { get; set; }

        /// <summary>
        /// Stats server configuration object.
        /// </summary>
        public IStatsServer Server { get; set; }
    } // StatsDBReportContext
}
