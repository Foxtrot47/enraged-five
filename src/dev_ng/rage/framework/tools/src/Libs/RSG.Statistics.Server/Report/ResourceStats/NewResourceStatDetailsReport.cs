﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using RSG.Base.Logging;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Report;
using RSG.Statistics.Domain.Entities.ResourceStats;
using RSG.Statistics.Server.Repository;
using PlatformModel = RSG.Model.Statistics.Platform;

namespace RSG.Statistics.Server.Report.ResourceStats
{
    /// <summary>
    /// Report that provides detailed resource stat bucket 'fragmentation' information.
    /// </summary>
    [Export(typeof(IStatsReport))]
    internal class NewResourceStatDetailsReport : StatsReportBase<List<PlatformModel.ResourceBucket>>
    {
        #region Constants
        /// <summary>
        /// Name of the report.
        /// </summary>
        private const String c_name = "NewResourceStatDetails";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Id of the resource stat we wish to retrieve details for.
        /// </summary>
        [ReportParameter(Required = true)]
        public long ResourceStatId { get; set; }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public NewResourceStatDetailsReport()
            : base(c_name)
        {
        }
        #endregion // Constructor(s)

        #region IStatsReport Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        /// <returns></returns>
        protected override List<PlatformModel.ResourceBucket> Generate(IRepositoryLocator locator, ILog log, IStatsServer server)
        {
            // Get the resource stat db object.
            ResourceStat resouceStat = locator.GetById<ResourceStat>(ResourceStatId);
            if (resouceStat == null)
            {
                throw new ArgumentException("The requested resource stat id doesn't exist in the database.");
            }

            // Get the buckets associated with that resource stat.
            ICriteria bucketsCriteria =
                locator.CreateCriteria<ResourceBucket>()
                    .CreateAlias("BucketType", "bt")
                    .SetProjection(Projections.ProjectionList()
                        .Add(Projections.Property("Size"), "Size")
                        .Add(Projections.Property("Capacity"), "Capacity")
                        .Add(Projections.Property("BucketId"), "ID")
                        .Add(Projections.Property("bt.Value"), "BucketType"))
                    .Add(Restrictions.Eq("ResourceStat", resouceStat))
                    .SetResultTransformer(Transformers.AliasToBean(typeof(PlatformModel.ResourceBucket)));
            return bucketsCriteria.List<PlatformModel.ResourceBucket>().ToList();
        }
        #endregion // IStatsReport region
    } // NewResourceStatDetailsReport
}
