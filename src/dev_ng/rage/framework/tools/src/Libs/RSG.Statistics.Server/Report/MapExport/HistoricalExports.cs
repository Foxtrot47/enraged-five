﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Dialect.Function;
using NHibernate.Transform;
using NHibernate.Type;
using RSG.Base.Logging;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Report;
using RSG.Statistics.Domain.Entities.ExportStats;
using RSG.Statistics.Server.Repository;
using ExpoModel = RSG.Statistics.Common.Model.MapExport;

namespace RSG.Statistics.Server.Report.MapExport
{
    /// <summary>
    /// Report that provides historical export summary information.
    /// </summary>
    [Export(typeof(IStatsReport))]
    internal class HistoricalExports : StatsReportBase<ExpoModel.HistoricalStats>
    {
        #region Constants
        /// <summary>
        /// Name of the report.
        /// </summary>
        private const String c_name = "MapExportsHistorical";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// The start date.
        /// </summary>
        [ReportParameter(Required = true)]
        public DateTime StartDate { get; set; }
        
        /// <summary>
        /// The end date.
        /// </summary>
        [ReportParameter(Required = true)]
        public DateTime EndDate { get; set; }

        /// <summary>
        /// How the data is grouped.  Valid values are DAY, WEEK, MONTH, YEAR.
        /// </summary>
        [ReportParameter(Required = true)]
        public String Grouping { get; set; }
        #endregion // Properties

        #region Private Classes
        /// <summary>
        /// Temp class for nhibernate query results.
        /// </summary>
        private class ExportResult
        {
            public int Year { get; set; }
            public int GroupVal { get; set; }
            public long TotalCount { get; set; }
            public long SuccessfulCount { get; set; }
            public double AvgTime { get; set; }
            public ulong MaxTime { get; set; }
        }

        /// <summary>
        /// Temp class for nhibernate query results.
        /// </summary>
        private class SectionExportResult
        {
            public String SectionName { get; set; }
            public int Year { get; set; }
            public int GroupVal { get; set; }
            public long TotalCount { get; set; }
            public double SuccessfulCount { get; set; }
            public double AvgTotalTime { get; set; }
            public double MaxTotalTime { get; set; }
            public double AvgBuildTime { get; set; }
            public double MaxBuildTime { get; set; }
        }
        #endregion // Private Classes

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public HistoricalExports()
            : base(c_name)
        {
        }
        #endregion // Constructor(s)

        #region IReportDataProvider Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        /// <returns></returns>
        protected override ExpoModel.HistoricalStats Generate(IRepositoryLocator locator, ILog log, IStatsServer server)
        {
            ExpoModel.HistoricalStats stats = new ExpoModel.HistoricalStats();
            stats.GeneralStats = GatherGeneralStats(locator);
            stats.SectionStats = GatherPerSectionStats(locator);
            return stats;
        }
        #endregion // IReportDataProvider Implementation

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <returns></returns>
        private Dictionary<DateTime, ExpoModel.HistoricalExportStat> GatherGeneralStats(IRepositoryLocator locator)
        {
            // Projection for  calculating time.
            IProjection timeProj =
                Projections.SqlFunction("TIMESTAMPDIFF", NHibernateUtil.Int32, Projections.Property("Start"), Projections.Property("End"));

            // Annoying way of getting the grouping stuff to work with an SQL function.
            String yearGroupSql = "YEAR(Start)";
            String groupSql = GetGroupSql();
            
            // Construct the criteria.
            DetachedCriteria exportCriteria =
                DetachedCriteria.For<MapExportStat>()
                    .SetProjection(Projections.ProjectionList()
                        .Add(Projections.SqlGroupProjection(yearGroupSql + " AS Year", yearGroupSql, new String[] { "Year" }, new IType[] { NHibernateUtil.Int32 }))
                        .Add(Projections.SqlGroupProjection(groupSql + " AS GroupVal", groupSql, new String[] { "GroupVal" }, new IType[] { NHibernateUtil.Int32 }))
                        .Add(Projections.Count("Id"), "TotalCount")
                        .Add(Projections.Sum(Projections.Cast(NHibernateUtil.Int32, Projections.Property("Success"))), "SuccessfulCount")
                        .Add(Projections.Avg(timeProj), "AvgTime")
                        .Add(Projections.Max(timeProj), "MaxTime")
                    )
                    .Add(Restrictions.Between("Start", StartDate, EndDate))
                    .SetResultTransformer(Transformers.AliasToBean(typeof(ExportResult)));
            IList<ExportResult> exportResults =
                locator.FindAll<MapExportStat, ExportResult>(exportCriteria);

            // Convert the results.
            Dictionary<DateTime, ExpoModel.HistoricalExportStat> groupedResults =
                new Dictionary<DateTime, ExpoModel.HistoricalExportStat>();

            foreach (ExportResult exportResult in exportResults)
            {
                DateTime date = ConvertGroupValueToDateTime(exportResult.Year, exportResult.GroupVal);

                ExpoModel.HistoricalExportStat expoStats = new ExpoModel.HistoricalExportStat();
                expoStats.TotalCount = (uint)exportResult.TotalCount;
                expoStats.SuccessfulCount = (uint)exportResult.SuccessfulCount;
                expoStats.AvgTotalTime = exportResult.AvgTime;
                expoStats.MaxTotalTime = exportResult.MaxTime;

                groupedResults[date] = expoStats;
            }

            // Get the section export results.
            DetachedCriteria sectionCriteria =
                DetachedCriteria.For<MapExportSectionExportStat>()
                    .SetProjection(Projections.ProjectionList()
                        .Add(Projections.SqlGroupProjection(yearGroupSql + " AS Year", yearGroupSql, new String[] { "Year" }, new IType[] { NHibernateUtil.Int32 }))
                        .Add(Projections.SqlGroupProjection(groupSql + " AS GroupVal", groupSql, new String[] { "GroupVal" }, new IType[] { NHibernateUtil.Int32 }))
                        .Add(Projections.Avg(timeProj), "AvgTime")
                        .Add(Projections.Max(timeProj), "MaxTime")
                    )
                    .Add(Restrictions.Between("Start", StartDate, EndDate))
                    .SetResultTransformer(Transformers.AliasToBean(typeof(ExportResult)));
            IList<ExportResult> sectionResults =
                locator.FindAll<MapExportSectionExportStat, ExportResult>(sectionCriteria);

            foreach (ExportResult sectionResult in sectionResults)
            {
                DateTime date = ConvertGroupValueToDateTime(sectionResult.Year, sectionResult.GroupVal);

                if (groupedResults.ContainsKey(date))
                {
                    groupedResults[date].AvgSectionExportTime = sectionResult.AvgTime;
                    groupedResults[date].MaxSectionExportTime = sectionResult.MaxTime;
                }
            }

            // Finally get the build results.
            DetachedCriteria buildCriteria =
                DetachedCriteria.For<MapExportImageBuildStat>()
                    .SetProjection(Projections.ProjectionList()
                        .Add(Projections.SqlGroupProjection(yearGroupSql + " AS Year", yearGroupSql, new String[] { "Year" }, new IType[] { NHibernateUtil.Int32 }))
                        .Add(Projections.SqlGroupProjection(groupSql + " AS GroupVal", groupSql, new String[] { "GroupVal" }, new IType[] { NHibernateUtil.Int32 }))
                        .Add(Projections.Avg(timeProj), "AvgTime")
                        .Add(Projections.Max(timeProj), "MaxTime")
                    )
                    .Add(Restrictions.Between("Start", StartDate, EndDate))
                    .SetResultTransformer(Transformers.AliasToBean(typeof(ExportResult)));
            IList<ExportResult> buildResults =
                locator.FindAll<MapExportImageBuildStat, ExportResult>(buildCriteria);

            foreach (ExportResult buildResult in buildResults)
            {
                DateTime date = ConvertGroupValueToDateTime(buildResult.Year, buildResult.GroupVal);

                if (groupedResults.ContainsKey(date))
                {
                    groupedResults[date].AvgBuildTime = buildResult.AvgTime;
                    groupedResults[date].MaxBuildTime = buildResult.MaxTime;
                }
            }

            return groupedResults;
        }

        /// <summary>
        /// Sql to use for returning the sql method to use for grouping the results.
        /// </summary>
        /// <returns></returns>
        private String GetGroupSql()
        {
            if (String.Compare(Grouping, "DAY", true) == 0)
            {
                return "DAY(Start)";
            }
            else if (String.Compare(Grouping, "WEEK", true) == 0)
            {
                return "WEEK(Start)";
            }
            else if (String.Compare(Grouping, "MONTH", true) == 0)
            {
                return "MONTH(Start)";
            }
            else
            {
                // Fallback (won't really be used).
                return "YEAR(Start)";
            }
        }

        /// <summary>
        /// Converts a grouped value to a datetime.
        /// </summary>
        private DateTime ConvertGroupValueToDateTime(int year, int groupVal)
        {
            DateTime date = new DateTime(year, 1, 1);

            if (String.Compare(Grouping, "DAY", true) == 0)
            {
                date = date.AddDays(groupVal - 1);
            }
            else if (String.Compare(Grouping, "WEEK", true) == 0)
            {
                date = date.AddDays((groupVal * 7) - 1);
            }
            else if (String.Compare(Grouping, "MONTH", true) == 0)
            {
                date = date.AddMonths(groupVal - 1);
            }

            return date;
        }

        /// <summary>
        /// Gathers export stats on a per section basis.
        /// </summary>
        private Dictionary<String, Dictionary<DateTime, ExpoModel.HistoricalExportStat>> GatherPerSectionStats(IRepositoryLocator locator)
        {
            // Construct some of the sql query components.
            String selectSql = "SELECT ";
            String groupSql = "";
            if (String.Compare(Grouping, "DAY", true) == 0)
            {
                selectSql += "YEAR(mes.Start) AS Year, DAY(mes.Start) AS GroupVal, ";
                groupSql = "YEAR(mes.Start), DAY(mes.Start)";
            }
            else if (String.Compare(Grouping, "WEEK", true) == 0)
            {
                selectSql += "YEAR(mes.Start) AS Year, WEEK(mes.Start) AS GroupVal, ";
                groupSql = "YEAR(mes.Start), WEEK(mes.Start)";
            }
            else if (String.Compare(Grouping, "MONTH", true) == 0)
            {
                selectSql += "YEAR(mes.Start) AS Year, MONTH(mes.Start) AS GroupVal, ";
                groupSql = "YEAR(mes.Start), MONTH(mes.Start)";
            }
            else
            {
                selectSql += "YEAR(mes.Start) AS Year, ";
                groupSql = "YEAR(mes.Start)";
            }

            // Construct the sql query.
            String sql = selectSql +
                @"  ms.Name AS SectionName,
	                COUNT(mes.Id) AS TotalCount,
	                SUM(mes.Success) AS SuccessfulCount,
	                AVG(TIMESTAMPDIFF(SECOND, mes.Start, mes.End) / CountTable.Count) AS AvgTotalTime,
	                MAX(TIMESTAMPDIFF(SECOND, mes.Start, mes.End) / CountTable.Count) AS MaxTotalTime,
	                AVG(TIMESTAMPDIFF(SECOND, mebs.Start, mebs.End) / CountTable.Count) AS AvgBuildTime,
	                MAX(TIMESTAMPDIFF(SECOND, mebs.Start, mebs.End) / CountTable.Count) AS MaxBuildTime
                FROM MapExportStat mes
                INNER JOIN MapExportStat_MapSection mm ON mm.MapExportStatId=mes.Id
                INNER JOIN MapSection ms ON ms.Id=mm.MapSectionId
                INNER JOIN
                (
	                SELECT mes.Id, COUNT(mm.MapExportStatId) AS Count
	                FROM MapExportStat mes
	                INNER JOIN MapExportStat_MapSection mm ON mm.MapExportStatId=mes.Id
	                GROUP BY mes.Id
                ) AS CountTable ON CountTable.Id=mes.Id
                LEFT JOIN MapExportImageBuildStat mebs on mebs.MapExportStatId=mes.Id
                WHERE mes.Start BETWEEN :start AND :end
                GROUP BY SectionName, " + groupSql;
            ISQLQuery sqlQuery = locator.CreateUnmanagedSQLQuery(sql);
            sqlQuery.SetParameter("start", StartDate);
            sqlQuery.SetParameter("end", EndDate);
            sqlQuery.SetResultTransformer(Transformers.AliasToBean(typeof(SectionExportResult)));
            IList<SectionExportResult> exportResults = sqlQuery.List<SectionExportResult>();

            Dictionary<String, Dictionary<DateTime, ExpoModel.HistoricalExportStat>>
                groupedResults = new Dictionary<String, Dictionary<DateTime, ExpoModel.HistoricalExportStat>>();

            foreach (IGrouping<String, SectionExportResult> sectionGroup in exportResults.GroupBy(item => item.SectionName))
            {
                if (!groupedResults.ContainsKey(sectionGroup.Key))
                {
                    groupedResults[sectionGroup.Key] = new Dictionary<DateTime, ExpoModel.HistoricalExportStat>();
                }

                foreach (SectionExportResult exportResult in sectionGroup)
                {
                    DateTime date = ConvertGroupValueToDateTime(exportResult.Year, exportResult.GroupVal);

                    ExpoModel.HistoricalExportStat expoStats = new ExpoModel.HistoricalExportStat();
                    expoStats.TotalCount = (uint)exportResult.TotalCount;
                    expoStats.SuccessfulCount = (uint)exportResult.SuccessfulCount;
                    expoStats.AvgTotalTime = exportResult.AvgTotalTime;
                    expoStats.MaxTotalTime = (ulong)exportResult.MaxTotalTime;
                    expoStats.AvgBuildTime = exportResult.AvgBuildTime;
                    expoStats.MaxBuildTime = (ulong)exportResult.MaxBuildTime;

                    groupedResults[sectionGroup.Key][date] = expoStats;
                }
            }

            // Get the section export times next.
            String sectionSql = selectSql +
                @"  ms.Name AS SectionName,
	                AVG(TIMESTAMPDIFF(SECOND, meses.Start, meses.End)) AS AvgTotalTime,
	                MAX(TIMESTAMPDIFF(SECOND, meses.Start, meses.End)) AS MaxTotalTime
                FROM MapExportSectionExportStat meses
                INNER JOIN MapExportStat mes on mes.Id=meses.MapExportStatId
                INNER JOIN MapSection ms ON ms.Id=meses.MapSectionId
                WHERE mes.Start BETWEEN :start AND :end
                GROUP BY SectionName, " + groupSql;
            ISQLQuery sectionQuery = locator.CreateUnmanagedSQLQuery(sectionSql);
            sectionQuery.SetParameter("start", StartDate);
            sectionQuery.SetParameter("end", EndDate);
            sectionQuery.SetResultTransformer(Transformers.AliasToBean(typeof(SectionExportResult)));
            IList<SectionExportResult> sectionResults = sectionQuery.List<SectionExportResult>();

            foreach (SectionExportResult sectionResult in sectionResults)
            {
                String sectionName = sectionResult.SectionName;
                DateTime date = ConvertGroupValueToDateTime(sectionResult.Year, sectionResult.GroupVal);

                if (groupedResults.ContainsKey(sectionName) && groupedResults[sectionName].ContainsKey(date))
                {
                    groupedResults[sectionName][date].AvgSectionExportTime = sectionResult.AvgTotalTime;
                    groupedResults[sectionName][date].MaxSectionExportTime = (ulong)sectionResult.MaxTotalTime;
                }
            }

            return groupedResults;
        }
        #endregion // Private Methods
    } // HistoricalExports
}
