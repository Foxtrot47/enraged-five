﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using NHibernate.Criterion;
using NHibernate.Transform;
using CommonModel = RSG.Statistics.Common.Model.SocialClub;
using RSG.Statistics.Common.ServiceContract;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Domain.Entities.SocialClub;
using RSG.Statistics.Server.Repository;
using AutoMapper;
using NHibernate.SqlCommand;
using System.Diagnostics;
using RSG.Base.Extensions;
using RSG.Statistics.Common.Config;
using RSG.Base.Configuration.Services;

namespace RSG.Statistics.Server.Services
{
    /// <summary>
    /// Serverside service implementation of the social club services interface.
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class SocialClubService : ServiceBase, ISocialClubService
    {
        #region Constructor(s)
        /// <summary>
        /// Constructor taking a server configuration object.
        /// </summary>
        /// <param name="server"></param>
        public SocialClubService(IServiceHostConfig config)
            : base(config)
        {
        }
        #endregion // Constructor(s)
        
        #region Countries
        /// <summary>
        /// Retrieves the list of countries.
        /// </summary>
        /// <returns></returns>
        public List<CommonModel.Country> GetCountries()
        {
            return ExecuteReadOnlyCommand(locator => GetCountriesCommand(locator));
        }

        /// <summary>
        /// Action for retrieving the country list.
        /// </summary>
        private List<CommonModel.Country> GetCountriesCommand(IRepositoryLocator locator)
        {
            // Create and run the query.
            DetachedCriteria countriesCriteria =
                DetachedCriteria.For<SocialClubCountry>()
                                .SetProjection(Projections.ProjectionList()
                                    .Add(Projections.Property("Name"), "Name")
                                    .Add(Projections.Property("CountryCode"), "Code"))
                                .AddOrder(Order.Asc("Name"))
                                .SetResultTransformer(Transformers.AliasToBean(typeof(CommonModel.Country)));
            IList<CommonModel.Country> countries = locator.FindAll<SocialClubCountry, CommonModel.Country>(countriesCriteria);
            return countries.ToList();
        }

        /// <summary>
        /// Creates a new country.
        /// </summary>
        /// <returns></returns>
        public CommonModel.Country CreateCountry(CommonModel.Country country)
        {
            return ExecuteCommand(locator => CreateCountryCommand(locator, country));
        }

        /// <summary>
        /// Action for creating a new country.
        /// </summary>
        private CommonModel.Country CreateCountryCommand(IRepositoryLocator locator, CommonModel.Country modelCountry)
        {
            // Create the database version of the country.
            SocialClubCountry dbCountry = new SocialClubCountry();
            dbCountry.CountryCode = modelCountry.Code;
            dbCountry.Name = modelCountry.Name;
            locator.Save(dbCountry);

            // Set the return data.
            modelCountry.Id = dbCountry.Id;
            return modelCountry;
        }
        #endregion // Countries
    } // SocialClubService
}
