﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common.ServiceContract;
using RSG.Statistics.Domain.Entities;
using RSG.Statistics.Common.Dto;
using RSG.Statistics.Server.Repository;
using AutoMapper;
using RSG.Statistics.Common.Dto.GameAssets;
using System.ServiceModel.Web;
using System.Net;
using NHibernate.Criterion;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Common.Config;
using RSG.Base.Configuration.Services;

namespace RSG.Statistics.Server.Services
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <typeparam name="TDto"></typeparam>
    /// <typeparam name="TDtos"></typeparam>
    public abstract class GameAssetServiceBase<TEntity, TDto, TDtos> : ServiceBase, IGameAssetServiceBase<TDto, TDtos>
        where TEntity : GameAssetBase<TDto>, new()
        where TDto : GameAssetDtoBase
        where TDtos : class, IDtos<TDto>, new()
    {
        #region Constructor(s)
        /// <summary>
        /// Constructor taking a server configuration object.
        /// </summary>
        /// <param name="server"></param>
        public GameAssetServiceBase(IServiceHostConfig config)
            : base(config)
        {
        }
        #endregion // Constructor(s)
    
        #region IGameAssetServiceBase Implementation
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public TDtos GetAll()
        {
            return ExecuteCommand(locator => GetAllCommand(locator));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public TDto GetByIdentifier(string identifier)
        {
            return ExecuteCommand(locator => GetByIdentifierCommand(locator, identifier));
        }

        /// <summary>
        /// Updates or creates a list of game assets.
        /// </summary>
        /// <param name="assetId"></param>
        /// <param name="asset"></param>
        /// <returns></returns>
        public void PutAssets(TDtos dtos)
        {
            ExecuteCommand(locator => PutAssetsCommand(locator, dtos));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="identifier"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        public TDto PutAsset(string identifier, TDto dto)
        {
            return ExecuteCommand(locator => PutAssetCommand(locator, identifier, dto));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="identifier"></param>
        public void DeleteAsset(string identifier)
        {
            ExecuteCommand(locator => DeleteAssetCommand(locator, identifier));
        }
        #endregion // IVehicleService Implementation

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <returns></returns>
        protected virtual TDtos GetAllCommand(IRepositoryLocator locator)
        {
            DetachedCriteria query = DetachedCriteria.For<TEntity>();

            TDtos result = new TDtos();
            result.Items = Mapper.Map<IList<TEntity>, List<TDto>>(locator.FindAll<TEntity>(query));
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        private TDto GetByIdentifierCommand(IRepositoryLocator locator, string identifier)
        {
            // Get the asset in question
            DetachedCriteria query = DetachedCriteria.For<TEntity>()
                                                     .Add(Expression.Eq("Identifier", identifier));
            TEntity entity = locator.FindFirst<TEntity>(query);
            if (entity == null)
            {
                throw new WebFaultException<string>("Asset identifier doesn't exist in the database.", HttpStatusCode.BadRequest);
            }

            return Mapper.Map<TEntity, TDto>(entity);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        private void PutAssetsCommand(IRepositoryLocator locator, TDtos dtos)
        {
            foreach (TDto dto in dtos.Items)
            {
                PutAssetCommand(locator, dto.Identifier, dto);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        protected virtual TDto PutAssetCommand(IRepositoryLocator locator, string identifier, TDto dto)
        {
            if (identifier != dto.Identifier)
            {
                throw new WebFaultException<string>("Asset identifier specified in the Uri doesn't match the dto.", HttpStatusCode.BadRequest);
            }
            
            // Get the asset from the database
            TEntity asset = GetAssetByIdentifier<TEntity>(locator, identifier, false);

            // If it doesn't exist, create a new one
            if (asset == null)
            {
                asset = new TEntity();
                asset.Id = 0;
            }

            // Update the asset, save it to the db and then return a dto version of it
            asset.Update(dto);
            locator.SaveOrUpdate(asset);
            return Mapper.Map<TEntity, TDto>(asset);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="identifier"></param>
        private void DeleteAssetCommand(IRepositoryLocator locator, string identifier)
        {
            TEntity asset = GetAssetByIdentifier<TEntity>(locator, identifier);
            locator.Delete(asset);
        }
        #endregion // IGameAssetServiceBase Methods
    } // GameAssetServiceBase<TEntity, TDto>
}
