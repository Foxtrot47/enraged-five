﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.ROS;
using PlayModel = RSG.Statistics.Common.Model.Playthrough;
using RSG.Statistics.Common.Report;
using RSG.Statistics.Server.Extensions;
using RSG.Statistics.Server.Repository;
using RSG.Statistics.Domain.Entities.Playthrough;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Common.Config;

namespace RSG.Statistics.Server.Report.Playthrough
{
    /// <summary>
    /// Report that provides mission attempt summary information.
    /// </summary>
    [Export(typeof(IStatsReport))]
    internal class PerUserMissionRatingsReport : PerBuildReportBase<List<PlayModel.PerUserMissionRatingStats>>
    {
        #region Constants
        /// <summary>
        /// Name of the report.
        /// </summary>
        private const String c_name = "PlaythroughMissionRatings";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Optional user to restrict the results to.
        /// </summary>
        [ReportParameter]
        public List<String> Users { get; set; }

        /// <summary>
        /// Flag indicating whether we wish to exclude users instead of include them.
        /// </summary>
        [ReportParameter]
        public bool ExcludeUsers { get; set; }
        #endregion // Properties

        #region Private Classes
        /// <summary>
        /// 
        /// </summary>
        private class IntermediateRatingResult
        {
            public String UserName { get; set; }
            public String MissionName { get; set; }
            public String MissionId { get; set; }
            public double Rating { get; set; }
        } // IntermediateRatingResult
        #endregion // Private Classes

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public PerUserMissionRatingsReport()
            : base(c_name)
        {
        }
        #endregion // Constructor(s)

        #region IStatsReport Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        /// <returns></returns>
        protected override List<PlayModel.PerUserMissionRatingStats> Generate(IRepositoryLocator locator, ILog log, IStatsServer server)
        {
            // Construct the criteria for retrieving the results.
            ICriteria criteria =
                locator.CreateCriteria<PlaythroughMissionAttempt>()
                    .CreateAlias("PlaythroughSession", "ps")
                    .CreateAlias("ps.User", "u")
                    .CreateAlias("Mission", "m")
                    .SetProjection(Projections.ProjectionList()
                        .Add(Projections.Property("u.Name"), "UserName")
                        .Add(Projections.Property("m.Name"), "MissionName")
                        .Add(Projections.Property("m.MissionId"), "MissionId")
                        .Add(Projections.Avg("Rating"), "Rating")
                        .Add(Projections.GroupProperty("m.Name"))
                        .Add(Projections.GroupProperty("ps.User")))
                    .Add(Restrictions.IsNotNull("Rating"))
                    .AddOrder(Order.Asc("MissionName"))
                    .SetResultTransformer(Transformers.AliasToBean(typeof(IntermediateRatingResult)));
            AddRestrictions(criteria, locator);

            // Execute the query and group the results.
            IList<IntermediateRatingResult> missionRatings = criteria.List<IntermediateRatingResult>();
            List<PlayModel.PerUserMissionRatingStats> results = new List<PlayModel.PerUserMissionRatingStats>();

            foreach (IGrouping<String, IntermediateRatingResult> userGroup in missionRatings.GroupBy(item => item.UserName))
            {
                PlayModel.PerUserMissionRatingStats userResults = new PlayModel.PerUserMissionRatingStats(userGroup.Key, ROSPlatform.PS3);
                foreach (IntermediateRatingResult rating in userGroup)
                {
                    userResults.Data.Add(new PlayModel.MissionRatingStat
                        {
                            MissionName = rating.MissionName,
                            MissionId = rating.MissionId,
                            Rating = (float)rating.Rating
                        });
                }
                results.Add(userResults);
            }

            return results;
        }
        #endregion // IStatsReport Implementation

        #region Private Methods
        /// <summary>
        /// Helper method for adding the query restrictions.
        /// </summary>
        private void AddRestrictions(ICriteria criteria, IRepositoryLocator locator)
        {
            if (!String.IsNullOrEmpty(BuildIdentifier))
            {
                Build build = locator.GetAssetByIdentifier<Build>(BuildIdentifier);
                criteria.Add(Expression.Eq("ps.Build", build));
            }

            if (Users.Any())
            {
                List<PlaythroughUser> users = new List<PlaythroughUser>();
                foreach (String userName in Users)
                {
                    DetachedCriteria userCriteria = DetachedCriteria.For<PlaythroughUser>().Add(Expression.Eq("Name", userName));
                    PlaythroughUser user = locator.FindFirst<PlaythroughUser>(userCriteria);
                    if (user == null)
                    {
                        throw new ArgumentException("Playthrough user doesn't exist in the database.");
                    }
                    users.Add(user);
                }

                if (ExcludeUsers)
                {
                    criteria.Add(Restrictions.Not(Restrictions.In("ps.User", users)));
                }
                else
                {
                    criteria.Add(Restrictions.In("ps.User", users));
                }
            }
        }
        #endregion // Private Methods
    } // PerUserMissionRatingsReport
}
