﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Server.TransManager;
using RSG.Statistics.Domain.Config;
using RSG.Statistics.Common.Config;
using System.Diagnostics;
using RSG.Base.Configuration.Services;

namespace RSG.Statistics.Server.Context
{
    /// <summary>
    /// Global context that is shared by the whole server.
    /// </summary>
    public class GlobalContext : IGlobalContext
    {
        #region Fields
        /// <summary>
        /// Singleton instance.
        /// </summary>
        private static IGlobalContext _instance;

        /// <summary>
        /// NHibernate configuration object.
        /// </summary>
        private static INHibernateConfig _nhibernateConfig;

        /// <summary>
        /// Private field for the <see cref="DomainConfig"/> property.
        /// </summary>
        private readonly IDomainConfig _domainConfig;

        /// <summary>
        /// Private field for the <see cref="TransManagerFactory"/> property.
        /// </summary>
        private readonly ITransManagerFactory _transManagerFactory;
        #endregion // Fields

        #region Properties
        /// <summary>
        /// Singleton instance of the global context.
        /// </summary>
        public static IGlobalContext Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new GlobalContext();
                }
                return _instance;
            }
        }

        /// <summary>
        /// Domain config object.
        /// </summary>
        public IDomainConfig DomainConfig
        {
            get { return _domainConfig; }
        }

        /// <summary>
        /// Transaction manager factory.
        /// </summary>
        public ITransManagerFactory TransManagerFactory
        {
            get { return _transManagerFactory; }
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="config"></param>
        private GlobalContext()
        {
            Debug.Assert(_nhibernateConfig != null, "Trying to create a GlobalContext object when the class hasn't yet been initialised with the nhibernate config object.");
            if (_nhibernateConfig == null)
            {
                throw new ArgumentNullException("Trying to create a GlobalContext object when the class hasn't yet been initialised with the nhibernate config object.");
            }
            _domainConfig = new DomainConfig(_nhibernateConfig);
            _transManagerFactory = new TransManagerFactory(_domainConfig, _nhibernateConfig.ReadOnly);
        }
        #endregion // Constructor(s)

        #region Static Methods
        /// <summary>
        /// 
        /// </summary>
        public static void Initialise(INHibernateConfig config)
        {
            _nhibernateConfig = config;
        }
        #endregion // Static Methods
    } // GlobalContext
}
