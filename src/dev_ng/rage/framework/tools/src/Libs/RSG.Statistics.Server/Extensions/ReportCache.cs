﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace RSG.Statistics.Server.Extensions
{
    /// <summary>
    /// Stores cached results from running various reports.
    /// </summary>
    public class ReportCache
    {
        #region Member Data
        /// <summary>
        /// Base directory where all the caches live.
        /// </summary>
        private String BaseDirectory { get; set; }

        /// <summary>
        /// Mapping of report identifiers to binary file caches.
        /// </summary>
        private Dictionary<String, BinaryFileCache<String>> ReportCaches { get; set; }
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="baseDirectory"></param>
        public ReportCache(String baseDirectory)
        {
            BaseDirectory = baseDirectory;
            ReportCaches = new Dictionary<string, BinaryFileCache<String>>();

            if (!Directory.Exists(baseDirectory))
            {
                Directory.CreateDirectory(baseDirectory);
            }
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="report"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public bool ContainsCachedResults(String report, String key)
        {
            if (!ReportCaches.ContainsKey(report))
            {
                ReportCaches[report] = new BinaryFileCache<String>(Path.Combine(BaseDirectory, report));
            }

            return ReportCaches[report].ContainsKey(key);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="report"></param>
        /// <param name="parameters"></param>
        public void AddCachedResults(String report, String key, object results)
        {
            if (!ReportCaches.ContainsKey(report))
            {
                ReportCaches[report] = new BinaryFileCache<String>(Path.Combine(BaseDirectory, report));
            }

            ReportCaches[report].Add(key, results);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="report"></param>
        /// <param name="parameters"></param>
        public void RemoveCachedResults(String report, String key)
        {
            if (!ReportCaches.ContainsKey(report))
            {
                ReportCaches[report] = new BinaryFileCache<String>(Path.Combine(BaseDirectory, report));
            }

            ReportCaches[report].Remove(key);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="report"></param>
        /// <param name="key"></param>
        public object GetCachedResults(String report, String key)
        {
            if (!ReportCaches.ContainsKey(report))
            {
                ReportCaches[report] = new BinaryFileCache<String>(Path.Combine(BaseDirectory, report));
            }

            object result;
            ReportCaches[report].TryGetValue(key, out result);
            return result;
        }

        /// <summary>
        /// Clears out all the cached reports.
        /// </summary>
        public void ClearCache()
        {
            foreach (String directory in Directory.EnumerateDirectories(BaseDirectory))
            {
                Directory.Delete(directory, true);
            }
        }

        /// <summary>
        /// Clears out the cache associated with a particular report.
        /// </summary>
        /// <param name="identifier"></param>
        public void ClearReportCache(String identifier)
        {
            if (ReportCaches.ContainsKey(identifier))
            {
                ReportCaches[identifier].Clear();
            }
        }

        /// <summary>
        /// Creates a hash based on the supplied parameters.
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public String CreateHashKey(IDictionary<ServerReportParameterInfo, object> parameters)
        {
            using (MD5 md5Hash = MD5.Create())
            {
                IList<String> stringValues = new List<String>();
                foreach (KeyValuePair<ServerReportParameterInfo, object> pair in parameters.OrderBy(item => item.Key.Name))
                {
                    if (pair.Key.Type.IsGenericType && pair.Key.Type.GetGenericTypeDefinition() == typeof(List<>))
                    {
                        IList list = (IList)pair.Value;
                        List<String> stringList = new List<String>();
                        foreach (object value in list)
                        {
                            stringList.Add(value.ToString());
                        }
                        stringList.Sort();

                        stringValues.Add(String.Format("{0}:{1}", pair.Key.Name, String.Join(",", stringList)));
                    }
                    else
                    {
                        stringValues.Add(String.Format("{0}:{1}", pair.Key.Name, pair.Value));
                    }
                }
                return GetMd5Hash(md5Hash, String.Join(";", stringValues));
            }
        }
        #endregion // Public Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="md5Hash"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        private String GetMd5Hash(MD5 md5Hash, string input)
        {
            // Convert the input string to a byte array and compute the hash. 
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes 
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data  
            // and format each one as a hexadecimal string. 
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string. 
            return sBuilder.ToString();
        }
        #endregion // Private Methods
    } // ReportCache
}
