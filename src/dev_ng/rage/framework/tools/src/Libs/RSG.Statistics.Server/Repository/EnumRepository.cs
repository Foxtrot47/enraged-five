﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Criterion;
using RSG.Statistics.Domain.Entities;

namespace RSG.Statistics.Server.Repository
{
    /// <summary>
    /// Repository for enums that are stored in the db.
    /// </summary>
    /// <typeparam name="TEnum"></typeparam>
    internal class EnumRepository<TEnum> : Repository<EnumReference<TEnum>>, IEnumRepository<TEnum>
        where TEnum : struct
    {
        #region Static Members
        /// <summary>
        /// Enums don't change while the application is running, so this is an optimisation for storing
        /// enum values -> db id's for quicker lookup.
        /// </summary>
        public static IDictionary<object, long> s_enumLookup = new Dictionary<object, long>();
        #endregion // Static Members
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public EnumRepository(ISession session)
            : base(session)
        {
        }
        #endregion // Constructor(s)

        #region Methods
        /// <summary>
        /// Returns a enum ref object from the db for the requested value.
        /// </summary>
        public EnumReference<TEnum> GetByValue(TEnum value)
        {
            // Check whether the enum already exists in the local lookup.
            if (!s_enumLookup.ContainsKey(value))
            {
                ICriteria criteria =
                    CreateCriteria()
                        .Add(Restrictions.Eq("Name", value.ToString()))
                        .SetFirstResult(0)
                        .SetMaxResults(1);
                EnumReference<TEnum> asset = criteria.List<EnumReference<TEnum>>().FirstOrDefault();

                if (asset != null)
                {
                    s_enumLookup[value] = asset.Id;
                }
                return asset;
            }
            else
            {
                return GetById(s_enumLookup[value]);
            }
        }
        #endregion // Methods
    }
}
