﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Domain.Entities.GameAssets;

namespace RSG.Statistics.Server.Repository
{
    /// <summary>
    /// Repository interface for game assets.
    /// </summary>
    /// <typeparam name="TAsset"></typeparam>
    internal interface IGameAssetRepository<TAsset> : IRepository<TAsset> where TAsset : class, IGameAsset
    {
        #region Methods
        /// <summary>
        /// Retrieves a game asset by it's hash.
        /// </summary>
        TAsset GetByHash(uint hash);

        /// <summary>
        /// Retrieves a game asset by the string version of it's identifier.
        /// NOTE: This should eventually be made obsolete in favor of the former method.  Still need to 
        /// make the appropriate modifications to the game asset class though.
        /// </summary>
        TAsset GetByIdentifier(String identifier);

        /// <summary>
        /// Retrieves a game asset by it's name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        TAsset GetByName(String name);

        /// <summary>
        /// Attempts to retrieve a game asset by it's name and creating a new one if it doesn't exist.
        /// </summary>
        TAsset GetOrCreateByName(String name);

        /// <summary>
        /// Creates a hash to asset lookup for all assets.
        /// NOTE: That this will only work with assets that have hashes stored in their identifiers.
        /// </summary>
        /// <returns></returns>
        IDictionary<uint, TAsset> CreateAssetLookup();

        /// <summary>
        /// Creates a hash to name lookup for all assets.
        /// NOTE: That this will only work with assets that have hashes stored in their identifiers.
        /// </summary>
        /// <returns></returns>
        IDictionary<uint, String> CreateAssetNameLookup();
        #endregion // Methods
    } // IGameAssetRepository<TAsset>
}
