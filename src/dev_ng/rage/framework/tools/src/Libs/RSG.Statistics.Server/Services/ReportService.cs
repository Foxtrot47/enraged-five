﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.Text;
using RSG.Base.Logging;
using RSG.Statistics.Common.Config;
using TeleDto = RSG.Statistics.Common.Dto.Reports;
using RSG.Statistics.Common.ServiceContract;
using RSG.Statistics.Server.Report;
using RSG.Statistics.Server.Repository;
using System.ComponentModel;
using RSG.Statistics.Common.Async;
using RSG.Statistics.Server.Extensions;
using RSG.Statistics.Common.Report;
using NHibernate.Criterion;
using NHibernate.Transform;
using RSG.Statistics.Domain.Entities.Reports;
using RSG.Base.Security.Cryptography;
using RSG.Base.Configuration.Services;

namespace RSG.Statistics.Server.Services
{
    /// <summary>
    /// Class for caching information about a particular stylesheet.
    /// </summary>
    internal class StylesheetInfo
    {
        public String Identifier { get; set; }
        public String Filepath { get; set; }

        internal StylesheetInfo(String identifier, String filepath)
        {
            Identifier = identifier;
            Filepath = filepath;
        }
    } // StylesheetInfo

    /// <summary>
    /// Report service.
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class ReportService : ServiceBase, IReportService
    {
        #region Member Data
        /// <summary>
        /// Collection of report data providers.
        /// </summary>
        private StatsReportCollection DataProviderCollection { get; set; }

        /// <summary>
        /// Mapping of report identifiers to class types.
        /// </summary>
        private IDictionary<String, IStatsReport> ReportLookup { get; set; }

        /// <summary>
        /// Mapping of reports to the parameters that they contain.
        /// </summary>
        private IDictionary<String, IList<ServerReportParameterInfo>> ReportParameters { get; set; }

        /// <summary>
        /// Mapping of report identifiers to lists of stylesheet names.
        /// </summary>
        private IDictionary<String, List<StylesheetInfo>> ReportStylesheets { get; set; }

        /// <summary>
        /// File based cache for reports.
        /// </summary>
        private ReportCache Cache { get; set; }

        /// <summary>
        /// Directory where the report stylesheets live in.
        /// </summary>
        private String ReportStylesheetBaseDir { get; set; }
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ReportService(IServiceHostConfig config)
            : base(config)
        {
            ReportLookup = new Dictionary<String, IStatsReport>();
            ReportParameters = new Dictionary<String, IList<ServerReportParameterInfo>>();
            ReportStylesheets = new Dictionary<String, List<StylesheetInfo>>();

            //
            StatsReportCollection.Initialise(config as IStatsServer);
            foreach (IStatsReport report in StatsReportCollection.Instance.AllReports.OrderBy(item => item.Name))
            {
                Debug.Assert(!ReportLookup.ContainsKey(report.Name), String.Format("Multiple reports with friendly name '{0}' exist.  This is not allowed.", report.Name));
                if (ReportLookup.ContainsKey(report.Name))
                {
                    throw new ArgumentException(String.Format("Multiple reports with friendly name '{0}' exist.  This is not allowed.", report.Name));
                }

                ReportLookup[report.Name] = report;
                Log.Message("Registered Report: {0}.", report.Name);
            }

            // Create the stats config for determining where the report cache and stylesheet directory are located.
            Cache = new ReportCache((config as IStatsServer).ReportCacheDir);
            ReportStylesheetBaseDir = (config as IStatsServer).Config.StylesheetsDirectory;
        }
        #endregion // Constructor(s)

        #region IReportService Implementation
        /// <summary>
        /// Returns a list of all the available reports.
        /// </summary>
        /// <returns></returns>
        public List<String> GetAvailableReportList()
        {
            return ReportLookup.Keys.ToList();
        }

        /// <summary>
        /// Returns the list of parameters required for a particular report.
        /// </summary>
        /// <returns></returns>
        public List<TeleDto.ReportParameter> GetReportParameters(String identifier)
        {
            EnsureReportIdentifierIsValid(identifier);
            CacheReportParameters(identifier);

            return ReportParameters[identifier].Select(item => item.CreateReportParameter()).ToList();
        }

        /// <summary>
        /// Executes a particular report with the passed in parameters.
        /// </summary>
        /// <returns></returns>
        public object RunReport(String identifier, bool forceRegen, List<TeleDto.ReportParameter> parameters)
        {
            EnsureReportIdentifierIsValid(identifier);
            CacheReportParameters(identifier);

            // Create an instance of the requested report and execute it with the requested parameters.
            IStatsReport report = ReportLookup[identifier];
            return ExecuteCommand(locator => RunReportCommand(locator, report, parameters, forceRegen));
        }

        /// <summary>
        /// Executes a particular report with the passed in parameters in a background thread.
        /// </summary>
        /// <returns></returns>
        public IAsyncCommandResult RunReportAsync(String identifier, bool forceRegen, List<TeleDto.ReportParameter> parameters)
        {
            EnsureReportIdentifierIsValid(identifier);
            CacheReportParameters(identifier);

            IStatsReport report = ReportLookup[identifier];

            // If we're not forcing a regen, check to see whether the cache already contains results for the requested parameters.
            if (!forceRegen)
            {
                // Convert the supplied parameters to ones we can deal with.
                IDictionary<ServerReportParameterInfo, object> paramValues = ConvertSuppliedParameters(report, parameters);
                String hash = Cache.CreateHashKey(paramValues);
                object results = Cache.GetCachedResults(report.Name, hash);

                if (results != null)
                {
                    AsyncCommandResult asyncResults = new AsyncCommandResult(Guid.NewGuid());
                    asyncResults.Message = null;
                    asyncResults.Progress = 1.0;
                    asyncResults.Result = results;
                    return asyncResults;
                }
            }

            // If we get this far it means that we've never encountered the input parameters or a force regen is occurring.
            return ExecuteAsyncCommand((locator, results) => RunReportAsyncCommand(locator, results, report, parameters, forceRegen));
        }

        /// <summary>
        /// Returns the list of stylesheets that are available for the particular report.
        /// </summary>
        /// <returns></returns>
        public List<String> GetReportStylesheetNames(String identifier)
        {
            EnsureReportIdentifierIsValid(identifier);
            CacheStylesheetNamesForReport(identifier);
            return ReportStylesheets[identifier].Select(item => item.Identifier).ToList();
        }

        /// <summary>
        /// Returns a specific stylesheet for the requested report.
        /// </summary>
        /// <returns></returns>
        public Stream GetReportStylesheet(String identifier, String stylesheet)
        {
            EnsureReportIdentifierIsValid(identifier);
            CacheStylesheetNamesForReport(identifier);

            // Make sure that the requested stylesheet exists.
            StylesheetInfo info = ReportStylesheets[identifier].FirstOrDefault(item => item.Identifier == stylesheet);

            Debug.Assert(info != null,String.Format("Report '{0}' doesn't have a stylesheet with name '{1}'.", identifier, stylesheet));
            if (info == null)
            {
                throw new ArgumentException(String.Format("Report '{0}' doesn't have a stylesheet with name '{1}'.", identifier, stylesheet));
            }

            // Make sure the file exists (it should really).
            if (File.Exists(info.Filepath))
            {
                using (FileStream fStream = File.OpenRead(info.Filepath))
                {
                    MemoryStream stream = new MemoryStream();
                    fStream.CopyTo(stream);
                    stream.Position = 0;
                    return stream;
                }
            }
            else
            {
                Debug.Assert(false, String.Format("Stylesheet at location '{0}' has been deleted since the server started.", info.Filepath));
                return null;
            }
        }

        /// <summary>
        /// Returns a list of all presets that the database contains.
        /// </summary>
        /// <returns></returns>
        public List<TeleDto.ReportPreset> GetPresets()
        {
            return ExecuteReadOnlyCommand(locator => GetPresetsCommand(locator));
        }

        /// <summary>
        /// Creates a new report preset.
        /// </summary>
        public TeleDto.ReportPreset CreatePreset(TeleDto.ReportPreset preset)
        {
            return ExecuteCommand(locator => CreatePresetCommand(locator, preset));
        }

        /// <summary>
        /// Deletes an existing report preset.
        /// </summary>
        /// <param name="id"></param>
        public void DeletePreset(String id)
        {
            ExecuteCommand(locator => DeletePresetCommand(locator, Int64.Parse(id)));
        }

        /// <summary>
        /// Computes the uint32 hash for the provided input strings.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public IDictionary<String, uint> ComputeHashes(IList<String> inputs)
        {
            IDictionary<String, uint> outputs = new Dictionary<String, uint>(inputs.Count);
            foreach (String input in inputs)
            {
                outputs[input] = OneAtATime.ComputeHash(input);
            }
            return outputs;
        }
        #endregion // IReportService Implementation

        #region IQueryAsyncService Implementation
        /// <summary>
        /// Queries the current status of a task that is running asynchronously.
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public IAsyncCommandResult Query(string guid)
        {
            return QueryAsyncOperation(Guid.Parse(guid));
        }

        #endregion // IQueryAsyncService Implementation

        #region Private Methods
        /// <summary>
        /// Throws an argument exception if the identifier doesn't correspond to a valid report name.
        /// </summary>
        private void EnsureReportIdentifierIsValid(String identifier)
        {
            Debug.Assert(ReportLookup.ContainsKey(identifier), String.Format("Report with identifier '{0}' doesn't exist.", identifier));
            if (!ReportLookup.ContainsKey(identifier))
            {
                throw new ArgumentException(String.Format("Report with identifier '{0}' doesn't exist.", identifier));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="identifier"></param>
        private void CacheReportParameters(String identifier)
        {
            // Have we already requested the report parameters for this particular report?
            if (!ReportParameters.ContainsKey(identifier))
            {
                List<ServerReportParameterInfo> parameters = new List<ServerReportParameterInfo>();
                Type reportType = ReportLookup[identifier].GetType();

                foreach (PropertyInfo pInfo in reportType.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.FlattenHierarchy))
                {
                    object[] attributes = pInfo.GetCustomAttributes(typeof(ReportParameterAttribute), false);
                    if (attributes.Any())
                    {
                        parameters.Add(new ServerReportParameterInfo(pInfo));
                    }
                }

                ReportParameters[identifier] = parameters;
            }
        }

        /// <summary>
        /// Populate the stylesheet cache with all the available stylesheets for a particular report (if it hasn't been done yet).
        /// </summary>
        private void CacheStylesheetNamesForReport(String identifier)
        {
            // Have we already enumerated the list of available stylesheets for this report?
            if (!ReportStylesheets.ContainsKey(identifier))
            {
                List<StylesheetInfo> stylesheets = new List<StylesheetInfo>();

                String stylesheetDirectory = Path.Combine(ReportStylesheetBaseDir, identifier);
                if (Directory.Exists(stylesheetDirectory))
                {
                    foreach (String filepath in Directory.EnumerateFiles(stylesheetDirectory))
                    {
                        stylesheets.Add(new StylesheetInfo(Path.GetFileNameWithoutExtension(filepath), filepath));
                    }
                }

                ReportStylesheets[identifier] = stylesheets;
            }
        }

        /// <summary>
        /// Executes the report in question.
        /// </summary>
        /// <returns></returns>
        private object RunReportCommand(IRepositoryLocator locator, IStatsReport report, IList<TeleDto.ReportParameter> suppliedParameters, bool forceRegen)
        {
            return RunReportCommon(locator, report, suppliedParameters, forceRegen);
        }

        /// <summary>
        /// Executes a particular report with the passed in parameters.
        /// </summary>
        /// <returns></returns>
        private object RunReportAsyncCommand(IRepositoryLocator locator, IAsyncCommandResult results, IStatsReport report, IList<TeleDto.ReportParameter> suppliedParameters, bool forceRegen)
        {
            // Hook up the report's progress notification.
            report.ProgressChanged += (s, e) => { results.Progress = e.Progress; results.Message = e.Message; };
            return RunReportCommon(locator, report, suppliedParameters, forceRegen);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="report"></param>
        /// <param name="suppliedParameters"></param>
        /// <returns></returns>
        private object RunReportCommon(IRepositoryLocator locator, IStatsReport report, IList<TeleDto.ReportParameter> suppliedParameters, bool forceRegen)
        {
            try
            {
                Log.Profile("Executing {0} report", report.Name);

                // Convert the supplied parameters to ones we can deal with.
                IDictionary<ServerReportParameterInfo, object> paramValues = ConvertSuppliedParameters(report, suppliedParameters);

                // Check the cache whether the report with the supplied parameters already exists.
                String hash = Cache.CreateHashKey(paramValues);
                object results = null;

                if (!forceRegen && Cache.ContainsCachedResults(report.Name, hash))
                {
                    Log.Message("Returning cached data.");
                    results = Cache.GetCachedResults(report.Name, hash);
                }

                if (results == null)
                {
                    // Report wasn't in the cache, run the report and cache the results.
                    SetReportParameters(ref report, paramValues);
                    results = report.Generate(new StatsDBReportContext { Locator = locator, Log = this.Log, Server = this.Config as IStatsServer });

                    // Add the results to the cache.
                    if (forceRegen)
                    {
                        Cache.RemoveCachedResults(report.Name, hash);
                    }
                    Cache.AddCachedResults(report.Name, hash, results);
                }

                return results;
            }
            finally
            {
                Log.ProfileEnd();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="report"></param>
        /// <param name="suppliedParameters"></param>
        /// <returns></returns>
        private IDictionary<ServerReportParameterInfo, object> ConvertSuppliedParameters(IStatsReport report, IList<TeleDto.ReportParameter> suppliedParameters)
        {
            IDictionary<ServerReportParameterInfo, object> convertedParameters = new Dictionary<ServerReportParameterInfo, object>();

            // Make sure the user supplied the right number of parameters.
            IList<ServerReportParameterInfo> requiredParameters = ReportParameters[report.Name];
            Debug.Assert(suppliedParameters.Count == requiredParameters.Count, "Not enough report parameters supplied.");
            if (suppliedParameters.Count != requiredParameters.Count)
            {
                throw new ArgumentException("Not enough report parameters supplied.");
            }

            // Go over all the parameters that are required.
            foreach (ServerReportParameterInfo requiredParameter in requiredParameters)
            {
                // Get the supplied version of the parameter.
                TeleDto.ReportParameter suppliedParameter = suppliedParameters.FirstOrDefault(item => item.Name == requiredParameter.Name);
                if (suppliedParameter == null)
                {
                    throw new ArgumentException(String.Format("The '{0}' parameter wasn't present in the POSTed data.", requiredParameter.Name));
                }

                // Make sure the user provided a value if the parameter is a required one.
                if (requiredParameter.Required && suppliedParameter.Value == null)
                {
                    throw new ArgumentNullException(String.Format("No value was supplied for the required '{0}' parameter.", requiredParameter.Name));
                }

                // Retrieve the value, validate it's contents and add it to the return results.
                object value = requiredParameter.ConvertValue(suppliedParameter.Value);
                value = requiredParameter.ValidateValue(value);
                convertedParameters[requiredParameter] = value;
            }

            return convertedParameters;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="report"></param>
        /// <param name="parameters"></param>
        private void SetReportParameters(ref IStatsReport report, IDictionary<ServerReportParameterInfo, object> parameters)
        {
            foreach (KeyValuePair<ServerReportParameterInfo, object> parameter in parameters)
            {
                parameter.Key.PropertyInfo.SetValue(report, parameter.Value, null);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        private List<TeleDto.ReportPreset> GetPresetsCommand(IRepositoryLocator locator)
        {
            // Two queries
            // 1) Get basic information about all the presets.
            // 2) Get parameters for all presets.
            DetachedCriteria presetCriteria =
                DetachedCriteria.For<ReportPreset>()
                                .SetProjection(Projections.ProjectionList()
                                    .Add(Projections.Property("Id"), "Id")
                                    .Add(Projections.Property("Name"), "Name"))
                                .SetResultTransformer(Transformers.AliasToBean(typeof(TeleDto.ReportPreset)));
            IList<TeleDto.ReportPreset> presets = locator.FindAll<ReportPreset, TeleDto.ReportPreset>(presetCriteria);

            DetachedCriteria parameterCriteria =
                DetachedCriteria.For<ReportPresetParameter>()
                                .SetProjection(Projections.ProjectionList()
                                    .Add(Projections.Property("Preset.Id"), "PresetId")
                                    .Add(Projections.Property("Key"), "Key")
                                    .Add(Projections.Property("Value"), "Value"))
                                .SetResultTransformer(Transformers.AliasToBean(typeof(TeleDto.ReportPresetParameter)));
            IList<TeleDto.ReportPresetParameter> parameters = locator.FindAll<ReportPresetParameter, TeleDto.ReportPresetParameter>(parameterCriteria);

            // Now we have the data, tie the parameters to the presets.
            IDictionary<long, TeleDto.ReportPreset> presetLookup = presets.ToDictionary(item => item.Id);

            foreach (TeleDto.ReportPresetParameter parameter in parameters)
            {
                if (presetLookup.ContainsKey(parameter.PresetId))
                {
                    presetLookup[parameter.PresetId].Parameters.Add(parameter);
                }
            }

            return presets.ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private TeleDto.ReportPreset CreatePresetCommand(IRepositoryLocator locator, TeleDto.ReportPreset presetDto)
        {
            // Map the dto to a domain object.
            ReportPreset preset = new ReportPreset();
            preset.Name = presetDto.Name;
            preset.Parameters = new List<ReportPresetParameter>();

            foreach (TeleDto.ReportPresetParameter parameterDto in presetDto.Parameters)
            {
                ReportPresetParameter parameter = new ReportPresetParameter();
                parameter.Preset = preset;
                parameter.Key = parameterDto.Key;
                parameter.Value = parameterDto.Value;
                preset.Parameters.Add(parameter);
            }

            locator.Save(preset);
            
            // Set the id and return the dto.
            presetDto.Id = preset.Id;
            return presetDto;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="id"></param>
        private void DeletePresetCommand(IRepositoryLocator locator, long id)
        {
            ReportPreset preset = locator.GetById<ReportPreset>(id);
            locator.Delete(preset);
        }
        #endregion // Private Methods
    } // ReportService
}
