﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Model.Common.Report;
using RSG.ROS;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Report;
using RSG.Statistics.Domain.Entities.Vertica;
using RSG.Statistics.Server.Repository;

namespace RSG.Statistics.Server.Report
{
    /// <summary>
    /// Base class for report data providers.
    /// </summary>
    internal abstract class StatsReportBase<TResult> : IStatsReport
    {
        #region Properties
        /// <summary>
        /// Report name.
        /// </summary>
        public String Name { get; private set; }
        #endregion // Properties

        #region Events
        /// <summary>
        /// Event that can be fired to notify of report generation progress.
        /// </summary>
        public event ReportProgressEventHandler ProgressChanged;
        #endregion // Events

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="name"></param>
        protected StatsReportBase(String name)
        {
            Name = name;
        }
        #endregion // Constructor(s)

        #region IReportDataProvider Implementation
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public object Generate(IReportContext context)
        {
            // Make sure the report context is of the right type.
            StatsDBReportContext statsContext = context as StatsDBReportContext;
            Debug.Assert(statsContext != null, "Context isn't a StatsDBReportContext object.");
            if (statsContext == null)
            {
                throw new ArgumentException("Context isn't a StatsDBReportContext object.");
            }

            // Return the results of running the report.
            return Generate(statsContext.Locator, statsContext.Log, statsContext.Server);
        }
        #endregion // IReportDataProvider Implementation

        #region Abstract Mathods
        /// <summary>
        /// Typed version of the generate method.
        /// </summary>
        protected abstract TResult Generate(IRepositoryLocator locator, ILog log, IStatsServer server);
        #endregion // Abstract Methods

        #region Protected Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="progress"></param>
        /// <param name="message"></param>
        protected void NotifyProgress(float progress, String message)
        {
            if (ProgressChanged != null)
            {
                ProgressChanged(this, new ReportProgressEventArgs(progress, message));
            }
        }

        /// <summary>
        /// Converts the string representation of ROSPlatforms in the list of gamer platform tuples.
        /// </summary>
        protected IEnumerable<Tuple<String, ROSPlatform>> ConvertGamerPlatformTuples(List<Tuple<String, String>> gamerPairs)
        {
            foreach (Tuple<String, String> tuple in gamerPairs)
            {
                ROSPlatform platform;
                if (Enum.TryParse(tuple.Item2, out platform))
                {
                    yield return new Tuple<String, ROSPlatform>(tuple.Item1, platform);
                }
            }
        }

        /// <summary>
        /// Intermediate class for the results of the below criteria.
        /// NOTE: We can't use Tuple directly as there is no default constructor for it.
        /// </summary>
        private class GamerTuple
        {
            public String GamerHandle { get; set; }
            public ROSPlatform Platform { get; set; }
        }

        /// <summary>
        /// Converts a list of gamer groups in a list of gamer handle/platform tuples.
        /// </summary>
        protected IEnumerable<Tuple<String, ROSPlatform>> ConvertGamerGroups(IRepositoryLocator locator, List<String> gamerGroups)
        {
            ICriteria criteria =
                locator.CreateCriteria<VerticaGamerGroup>()
                    .SetFetchMode("Gamers", FetchMode.Join)
                    .CreateAlias("Gamers", "g")
                    .CreateAlias("g.Platform", "p")
                    .SetProjection(Projections.ProjectionList()
                        .Add(Projections.Property("g.Name"), "GamerHandle")
                        .Add(Projections.Property("p.Value"), "Platform"))
                    .Add(Restrictions.In("Name", gamerGroups))
                    .SetResultTransformer(Transformers.AliasToBean(typeof(GamerTuple)));


            foreach (GamerTuple tuple in criteria.List<GamerTuple>())
            {
                yield return new Tuple<String, ROSPlatform>(tuple.GamerHandle, tuple.Platform);
            }
        }

        /// <summary>
        /// Converts a list of social club user groups into a list of social club user names.
        /// </summary>
        protected IList<String> ConvertUserGroups(IRepositoryLocator locator, List<String> userGroups)
        {
            ICriteria criteria =
                locator.CreateCriteria<VerticaUserGroup>()
                    .SetFetchMode("Users", FetchMode.Join)
                    .CreateAlias("Users", "u")
                    .SetProjection(Projections.ProjectionList()
                        .Add(Projections.Property("u.Name")))
                    .Add(Restrictions.In("Name", userGroups));
            return criteria.List<String>();
        }
        #endregion // Protected Methods
    } // ReportDataProviderBase
}
