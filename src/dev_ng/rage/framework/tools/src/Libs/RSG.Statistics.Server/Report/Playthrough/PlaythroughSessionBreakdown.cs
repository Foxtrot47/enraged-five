﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using RSG.Statistics.Domain.Entities;
using RSG.Statistics.Domain.Entities.Playthrough;
using RSG.Statistics.Server.Repository;
using RSG.Statistics.Server.Extensions;
using NHibernate.Transform;
using RSG.Platform;
using RSG.Statistics.Common.Report;
using RSG.Base.Logging;
using RSG.Statistics.Domain.Entities.GameAssets;
using PlayModel = RSG.Statistics.Common.Model.Playthrough;
using RSG.Statistics.Common.Config;

namespace RSG.Statistics.Server.Report.Playthrough
{
    /// <summary>
    /// Report that provides summary information about the playthrough mission attempts in the db.
    /// </summary>
    [Export(typeof(IStatsReport))]
    internal class PlaythroughSessionBreakdown : PlaythroughReportBase<List<PlayModel.UserSessionBreakdown>>
    {
        #region Constants
        /// <summary>
        /// Name of the report.
        /// </summary>
        private const String c_name = "PlaythroughBreakdown";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Required user to restrict the results to.
        /// </summary>
        [ReportParameter(Required = true)]
        public String User { get; set; }
        #endregion // Properties

        #region Private Classes
        /// <summary>
        /// 
        /// </summary>
        private class MissionCheckpointInfo
        {
            public long MissionId { get; set; }
            public uint CheckpointCount { get; set; }
        } // MissionCheckpointInfo

        private class ResultInfo
        {
            public long SessionId { get; set; }
            public long MissionId { get; set; }
            public DateTime Start { get; set; }
            public RSG.Model.Common.Mission.MissionAttemptResult Result { get; set; }
        } // ResultInfo
        #endregion // Private Classes

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public PlaythroughSessionBreakdown()
            : base(c_name)
        {
        }
        #endregion // Constructor(s)

        #region IStatsReport Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        /// <returns></returns>
        protected override List<PlayModel.UserSessionBreakdown> Generate(IRepositoryLocator locator, ILog log, IStatsServer server)
        {
            // Retrieve the user we wish to view the mission attempt summary information for.
            DetachedCriteria userCriteria = DetachedCriteria.For<PlaythroughUser>().Add(Expression.Eq("Name", User));
            PlaythroughUser user = locator.FindFirst<PlaythroughUser>(userCriteria);
            if (user == null)
            {
                throw new ArgumentException("Playthrough user doesn't exist in the database.");
            }

            // Create the criteria to get the session information.
            DetachedCriteria sessionCriteria =
                DetachedCriteria.For<PlaythroughSession>()
                    .CreateAlias("Build", "b")
                    .CreateAlias("Platform", "p")
                    .SetProjection(Projections.ProjectionList()
                        .Add(Projections.Property("Id"), "Id")
                        .Add(Projections.Property("FriendlyName"), "FriendlyName")
                        .Add(Projections.Property("Start"), "Start")
                        .Add(Projections.Property("TimeSpentPlaying"), "TimeSpentPlaying")
                        .Add(Projections.Property("b.Identifier"), "BuildIdentifier")
                        .Add(Projections.Property("p.Value"), "PlatformHACK"))
                    .Add(Expression.Eq("User", user))
                    .SetResultTransformer(Transformers.AliasToBean(typeof(PlayModel.UserSessionBreakdown)));
            AddRestrictions(sessionCriteria, locator);
            List<PlayModel.UserSessionBreakdown> breakdowns =
                locator.FindAll<PlaythroughSession, PlayModel.UserSessionBreakdown>(sessionCriteria).ToList();

            // Get the mission attempt information next.
            DetachedCriteria missionAttemptCriteria =
                DetachedCriteria.For<PlaythroughMissionAttempt>()
                    .CreateAlias("PlaythroughSession", "ps")
                    .CreateAlias("Mission", "m")
                    .SetProjection(Projections.ProjectionList()
                        .Add(Projections.Property("ps.Id"), "SessionId")
                        .Add(Projections.Property("m.Id"), "MissionId")
                        .Add(Projections.Property("m.Name"), "MissionName")
                        .Add(Projections.Sum("TimeToComplete"), "TimeToComplete")
                        .Add(Projections.GroupProperty("Mission"))
                        .Add(Projections.GroupProperty("PlaythroughSession")))
                    .Add(Expression.Eq("ps.User", user))
                    .AddOrder(Order.Asc("Start"))
                    .SetResultTransformer(Transformers.AliasToBean(typeof(PlayModel.PlaythroughMissionAttempt)));
            AddRestrictions(missionAttemptCriteria, locator, "ps");
            IList<PlayModel.PlaythroughMissionAttempt> missionAttempts =
                locator.FindAll<PlaythroughMissionAttempt, PlayModel.PlaythroughMissionAttempt>(missionAttemptCriteria);

            // Hook up the mission attempts with the sessions.
            IDictionary<long, PlayModel.UserSessionBreakdown> sessionLookup = breakdowns.ToDictionary(item => item.Id);
            foreach (PlayModel.PlaythroughMissionAttempt missionAttempt in missionAttempts)
            {
                if (sessionLookup.ContainsKey(missionAttempt.SessionId))
                {
                    sessionLookup[missionAttempt.SessionId].MissionsAttempted.Add(missionAttempt);
                }
            }
            
            // Get the mission checkpoint information
            DetachedCriteria checkpointCriteria =
                DetachedCriteria.For<MissionCheckpoint>()
                    .CreateAlias("Mission", "m")
                    .SetProjection(Projections.ProjectionList()
                        .Add(Projections.Property("m.Id"), "MissionId")
                        .Add(Projections.Count("Id"), "CheckpointCount")
                        .Add(Projections.GroupProperty("m.Id")))
                    .Add(Restrictions.IsNotNull("Index"))
                    .SetResultTransformer(Transformers.AliasToBean(typeof(MissionCheckpointInfo)));
            IList<MissionCheckpointInfo> checkpointInfo =
                locator.FindAll<MissionCheckpoint, MissionCheckpointInfo>(checkpointCriteria);

            // Create a lookup and set the information in the return objects
            IDictionary<long, uint> missionCheckpointLookup =
                checkpointInfo.ToDictionary(item => item.MissionId, item => item.CheckpointCount);
            foreach (PlayModel.PlaythroughMissionAttempt missionAttempt in missionAttempts)
            {
                if (missionCheckpointLookup.ContainsKey(missionAttempt.MissionId))
                {
                    missionAttempt.CheckpointCount = missionCheckpointLookup[missionAttempt.MissionId];
                }
            }

            // Get the checkpoint attempt information next.
            DetachedCriteria checkpointAttemptCriteria =
                DetachedCriteria.For<PlaythroughCheckpointAttempt>()
                    .CreateAlias("PlaythroughMissionAttempt", "ma")
                    .CreateAlias("ma.PlaythroughSession", "ps")
                    .CreateAlias("Checkpoint", "c")
                    .CreateAlias("c.Mission", "m")
                    .SetProjection(Projections.ProjectionList()
                        .Add(Projections.Property("ps.Id"), "SessionId")
                        .Add(Projections.Property("m.Id"), "MissionId")
                        .Add(Projections.Property("c.Name"), "CheckpointName")
                        .Add(Projections.Property("c.Index"), "CheckpointIndex")
                        .Add(Projections.Property("Start"), "Start")
                        .Add(Projections.Property("End"), "End")
                        .Add(Projections.Property("TimeToComplete"), "TimeToComplete")
                        .Add(Projections.Property("Comment"), "Comment"))
                    .Add(Expression.Eq("ps.User", user))
                    .AddOrder(Order.Asc("Start"))
                    .SetResultTransformer(Transformers.AliasToBean(typeof(PlayModel.PlaythroughCheckpointAttempt)));
            AddRestrictions(checkpointAttemptCriteria, locator, "ps");
            IList<PlayModel.PlaythroughCheckpointAttempt> checkpointAttempts =
                locator.FindAll<PlaythroughCheckpointAttempt, PlayModel.PlaythroughCheckpointAttempt>(checkpointAttemptCriteria);

            // Hook up checkpoint attempts with mission attempts
            IDictionary<KeyValuePair<long, long>, PlayModel.PlaythroughMissionAttempt> missionAttemptLookup =
                missionAttempts.ToDictionary(item => new KeyValuePair<long, long>(item.SessionId, item.MissionId));
            foreach (PlayModel.PlaythroughCheckpointAttempt checkpointAttempt in checkpointAttempts)
            {
                KeyValuePair<long, long> key = new KeyValuePair<long, long>(checkpointAttempt.SessionId, checkpointAttempt.MissionId);
                if (missionAttemptLookup.ContainsKey(key))
                {
                    missionAttemptLookup[key].CheckpointAttempts.Add(checkpointAttempt);
                }
            }

            // Associate comments with the checkpoints.
            DetachedCriteria missionResultCriteria =
                DetachedCriteria.For<PlaythroughCheckpointAttempt>()
                    .CreateAlias("PlaythroughMissionAttempt", "ma")
                    .CreateAlias("ma.PlaythroughSession", "ps")
                    .CreateAlias("ma.Result", "r")
                    .CreateAlias("ma.Mission", "m")
                    .SetProjection(Projections.ProjectionList()
                        .Add(Projections.Property("ps.Id"), "SessionId")
                        .Add(Projections.Property("m.Id"), "MissionId")
                        .Add(Projections.Property("Start"), "Start")
                        .Add(Projections.Property("r.Value"), "Result"))
                    .Add(Expression.Eq("ps.User", user))
                    .AddOrder(Order.Asc("Start"))
                    .SetResultTransformer(Transformers.AliasToBean(typeof(ResultInfo)));
            AddRestrictions(checkpointAttemptCriteria, locator, "ps");
            IList<ResultInfo> missionResultInfos =
                locator.FindAll<PlaythroughCheckpointAttempt, ResultInfo>(missionResultCriteria);

            // Tie the results to the mission information.
            IEnumerable<ResultInfo> orderedCommentInfos =
                missionResultInfos.OrderBy(item => item.SessionId)
                    .ThenBy(item => item.MissionId)
                    .ThenByDescending(item => item.Start);
            long currentSessionId = 0;
            long currentMissionId = 0;

            foreach (ResultInfo info in orderedCommentInfos)
            {
                if (currentSessionId != info.SessionId || currentMissionId != info.MissionId)
                {
                    KeyValuePair<long, long> key = new KeyValuePair<long, long>(info.SessionId, info.MissionId);
                    if (missionAttemptLookup.ContainsKey(key))
                    {
                        missionAttemptLookup[key].Result = info.Result;
                    }

                    currentSessionId = info.SessionId;
                    currentMissionId = info.MissionId;
                }
            }

            // Get the generic playthrough comments and connect them to the session breakdowns.
            foreach (PlayModel.PlaythroughComment comment in GetGenericComments(locator))
            {
                if (sessionLookup.ContainsKey(comment.ParentId))
                {
                    sessionLookup[comment.ParentId].Comments.Add(comment);
                }
            }
            
            return breakdowns;
        }
        #endregion // IStatsReport Implementation
    } // PlaythroughSummary
}
