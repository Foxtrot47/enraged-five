﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Server.TransManager
{
    /// <summary>
    /// 
    /// </summary>
    public interface ITransManagerFactory
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        ITransManager CreateManager(bool readOnly);
    } // ITransManagerFactory
}
