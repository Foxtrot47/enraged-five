﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.SqlCommand;
using NHibernate.Transform;
using RSG.Statistics.Domain.Entities.ExportStats;
using RSG.Statistics.Server.Repository;
using ExpoModel = RSG.Statistics.Common.Model.MapExport;

namespace RSG.Statistics.Server.Report.MapExport
{
    /// <summary>
    /// Base class for map export reports.
    /// </summary>
    internal abstract class MapExportReportBase<TResult> : StatsReportBase<TResult> where TResult : class
    {
        #region Private Classes
        /// <summary>
        /// Temp class for nhibernate query results.
        /// </summary>
        private class ExportSectionResult
        {
            public long Id { get; set; }
            public long ExportId { get; set; }
            public String SectionName { get; set; }
            public ulong SectionExportTime { get; set; }
            public bool Success { get; set; }
            public DateTime Start { get; set; }
            public DateTime? End { get; set; }
        }

        /// <summary>
        /// Temp class for nhibernate query results.
        /// </summary>
        private class ExportPlatformResult
        {
            public long ExportId { get; set; }
            public RSG.Platform.Platform? Platform { get; set; }
        }
        #endregion // Private Classes

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public MapExportReportBase(String name)
            : base(name)
        {
        }
        #endregion // Constructor(s)

        #region Protected Methods
        /// <summary>
        /// 
        /// </summary>
        protected IList<ExpoModel.ExportDetails> GetExportDetails(IRepositoryLocator locator, DateTime startDate, DateTime endDate, String username = null)
        {
            // Create the criteria for the basic export information.
            IProjection exportTimeProj =
                Projections.SqlFunction("TIMESTAMPDIFF", NHibernateUtil.Int32, Projections.Property("Start"), Projections.Property("End"));

            DetachedCriteria exportsCriteria =
                DetachedCriteria.For<MapExportStat>()
                                .SetProjection(Projections.ProjectionList()
                                    .Add(Projections.Property("Id"), "Id")
                                    .Add(Projections.Property("Start"), "Start")
                                    .Add(Projections.Property("End"), "End")
                                    .Add(Projections.Alias(exportTimeProj, "TotalTime"))
                                    .Add(Projections.Property("Username"), "Username")
                                    .Add(Projections.Property("MachineName"), "MachineName")
                                    .Add(Projections.Property("ExportType"), "ExportType")
                                    .Add(Projections.Property("ToolsVersion"), "ToolsVersion")
                                    .Add(Projections.Property("XGEEnabled"), "XGEEnabled")
                                    .Add(Projections.Property("XGEStandalone"), "XGEStandalone")
                                    .Add(Projections.Property("XGEForceCPUCount"), "XGEForceCPUCount")
                                    .Add(Projections.Property("Success"), "Success")
                                    .Add(Projections.Property("ExportNumber"), "ExportNumber")
                                    .Add(Projections.Property("ProcessorName"), "ProcessorName")
                                    .Add(Projections.Property("ProcessorCores"), "ProcessorCores")
                                    .Add(Projections.Property("InstalledMemory"), "InstalledMemory")
                                    .Add(Projections.Property("PrivateBytesStart"), "PrivateBytesStart")
                                    .Add(Projections.Property("PrivateBytesEnd"), "PrivateBytesEnd")
                                )
                                .Add(Restrictions.Between("Start", startDate, endDate))
                                .SetResultTransformer(Transformers.AliasToBean(typeof(ExpoModel.ExportDetails)));
            if (username != null)
            {
                exportsCriteria.Add(Restrictions.Eq("Username", username));
            }
            IList<ExpoModel.ExportDetails> exportDetails =
                locator.FindAll<MapExportStat, ExpoModel.ExportDetails>(exportsCriteria);

            IDictionary<long, ExpoModel.ExportDetails> exportLookup = exportDetails.ToDictionary(item => item.Id);

            // Get the sections next.
            DetachedCriteria sectionCriteria =
                DetachedCriteria.For<MapExportStat>("mes")
                                .CreateCriteria("MapSections", "ms")
                                .SetProjection(Projections.ProjectionList()
                                    .Add(Projections.Property("mes.Id"), "ExportId")
                                    .Add(Projections.Property("ms.Name"), "SectionName"))
                                .Add(Restrictions.Between("mes.Start", startDate, endDate))
                                .SetResultTransformer(Transformers.AliasToBean(typeof(ExportSectionResult)));
            if (username != null)
            {
                exportsCriteria.Add(Restrictions.Eq("mes.Username", username));
            }
            IList<ExportSectionResult> sectionInfos =
                locator.FindAll<MapExportStat, ExportSectionResult>(sectionCriteria);

            foreach (ExportSectionResult sectionInfo in sectionInfos)
            {
                if (exportLookup.ContainsKey(sectionInfo.ExportId))
                {
                    exportLookup[sectionInfo.ExportId].SectionsExported.Add(sectionInfo.SectionName);
                }
            }

            // Get the platforms.
            DetachedCriteria platfomCriteria =
                DetachedCriteria.For<MapExportStat>("mes")
                                .CreateCriteria("Platforms", "p")
                                .SetProjection(Projections.ProjectionList()
                                    .Add(Projections.Property("mes.Id"), "ExportId")
                                    .Add(Projections.Property("p.Value"), "Platform"))
                                .Add(Restrictions.Between("mes.Start", startDate, endDate))
                                .SetResultTransformer(Transformers.AliasToBean(typeof(ExportPlatformResult)));
            if (username != null)
            {
                exportsCriteria.Add(Restrictions.Eq("mes.Username", username));
            }
            IList<ExportPlatformResult> platformInfos =
                locator.FindAll<MapExportStat, ExportPlatformResult>(platfomCriteria);

            foreach (ExportPlatformResult platformInfo in platformInfos)
            {
                if (platformInfo.Platform != null && exportLookup.ContainsKey(platformInfo.ExportId))
                {
                    exportLookup[platformInfo.ExportId].PlatformsEnabled.Add(platformInfo.Platform.Value);
                }
            }

            // Map check info.
            DetachedCriteria mapCheckCriteria =
                DetachedCriteria.For<MapExportMapCheckStat>()
                                .CreateAlias("MapExportStat", "mes")
                                .SetProjection(Projections.ProjectionList()
                                    .Add(Projections.Property("mes.Id"), "ParentId")
                                    .Add(Projections.Constant("Map Check"), "Name")
                                    .Add(Projections.Alias(exportTimeProj, "Time"))
                                    .Add(Projections.Property("Start"), "Start")
                                    .Add(Projections.Property("End"), "End")
                                    .Add(Projections.Property("Success"), "Success"))
                                .Add(Restrictions.Between("mes.Start", startDate, endDate))
                                .SetResultTransformer(Transformers.AliasToBean(typeof(ExpoModel.ExportDetailsSubStep)));
            if (username != null)
            {
                exportsCriteria.Add(Restrictions.Eq("mes.Username", username));
            }
            IList<ExpoModel.ExportDetailsSubStep> mapCheckDetails =
                locator.FindAll<MapExportSectionExportStat, ExpoModel.ExportDetailsSubStep>(mapCheckCriteria);

            foreach (ExpoModel.ExportDetailsSubStep mapCheckDetail in mapCheckDetails)
            {
                if (exportLookup.ContainsKey(mapCheckDetail.ParentId))
                {
                    exportLookup[mapCheckDetail.ParentId].SubSteps.Add(mapCheckDetail);
                }
            }

            // Section export information.
            DetachedCriteria sectionExportCriteria =
                DetachedCriteria.For<MapExportSectionExportStat>()
                                .CreateAlias("MapExportStat", "mes")
                                .CreateAlias("MapSection", "ms", JoinType.LeftOuterJoin)
                                .SetProjection(Projections.ProjectionList()
                                    .Add(Projections.Property("Id"), "Id")
                                    .Add(Projections.Property("mes.Id"), "ExportId")
                                    .Add(Projections.Property("ms.Name"), "SectionName")
                                    .Add(Projections.Alias(exportTimeProj, "SectionExportTime"))
                                    .Add(Projections.Property("Start"), "Start")
                                    .Add(Projections.Property("End"), "End")
                                    .Add(Projections.Property("Success"), "Success"))
                                .Add(Restrictions.Between("mes.Start", startDate, endDate))
                                .SetResultTransformer(Transformers.AliasToBean(typeof(ExportSectionResult)));
            if (username != null)
            {
                exportsCriteria.Add(Restrictions.Eq("mes.Username", username));
            }
            IList<ExportSectionResult> sectionExportDetails =
                locator.FindAll<MapExportSectionExportStat, ExportSectionResult>(sectionExportCriteria);
            IDictionary<long, ExpoModel.ExportDetailsSubStep> sectionExportLookup =
                new Dictionary<long, ExpoModel.ExportDetailsSubStep>();

            foreach (ExportSectionResult sectionExportDetail in sectionExportDetails)
            {
                if (exportLookup.ContainsKey(sectionExportDetail.ExportId))
                {
                    ExpoModel.ExportDetailsSubStep subStep = new ExpoModel.ExportDetailsSubStep();
                    subStep.Id = sectionExportDetail.Id;
                    subStep.Name = String.Format("{0} section export", (sectionExportDetail.SectionName == null ? "Unknown" : sectionExportDetail.SectionName));
                    subStep.Time = sectionExportDetail.SectionExportTime;
                    subStep.Start = sectionExportDetail.Start;
                    subStep.End = sectionExportDetail.End;
                    subStep.Success = sectionExportDetail.Success;

                    exportLookup[sectionExportDetail.ExportId].SubSteps.Add(subStep);
                    sectionExportLookup[sectionExportDetail.Id] = subStep;
                }
            }

            // Section export breakdown.
            DetachedCriteria sectionSubCriteria =
                DetachedCriteria.For<MapExportSectionExportSubStat>()
                                .CreateAlias("SectionExportStat", "meses")
                                .CreateAlias("meses.MapExportStat", "mes")
                                .CreateAlias("ExportSubTask", "est")
                                .SetProjection(Projections.ProjectionList()
                                    .Add(Projections.Property("meses.Id"), "ParentId")
                                    .Add(Projections.Property("est.Name"), "Name")
                                    .Add(Projections.Alias(exportTimeProj, "Time"))
                                    .Add(Projections.Property("Start"), "Start")
                                    .Add(Projections.Property("End"), "End")
                                    .Add(Projections.Property("Success"), "Success"))
                                .Add(Restrictions.Between("mes.Start", startDate, endDate))
                                .SetResultTransformer(Transformers.AliasToBean(typeof(ExpoModel.ExportDetailsSubStep)));
            if (username != null)
            {
                exportsCriteria.Add(Restrictions.Eq("mes.Username", username));
            }
            IList<ExpoModel.ExportDetailsSubStep> sectionSubDetails =
                locator.FindAll<MapExportSectionExportStat, ExpoModel.ExportDetailsSubStep>(sectionSubCriteria);

            foreach (ExpoModel.ExportDetailsSubStep sectionSubDetail in sectionSubDetails)
            {
                if (sectionExportLookup.ContainsKey(sectionSubDetail.ParentId))
                {
                    sectionExportLookup[sectionSubDetail.ParentId].SubSteps.Add(sectionSubDetail);
                }
            }

            // Image build info.
            DetachedCriteria buildCriteria =
                DetachedCriteria.For<MapExportImageBuildStat>()
                                .CreateAlias("MapExportStat", "mes")
                                .SetProjection(Projections.ProjectionList()
                                    .Add(Projections.Property("mes.Id"), "ParentId")
                                    .Add(Projections.Constant("Image Build"), "Name")
                                    .Add(Projections.Alias(exportTimeProj, "Time"))
                                    .Add(Projections.Property("Start"), "Start")
                                    .Add(Projections.Property("End"), "End")
                                    .Add(Projections.Property("Success"), "Success"))
                                .Add(Restrictions.Between("mes.Start", startDate, endDate))
                                .SetResultTransformer(Transformers.AliasToBean(typeof(ExpoModel.ExportDetailsSubStep)));
            if (username != null)
            {
                exportsCriteria.Add(Restrictions.Eq("mes.Username", username));
            }
            IList<ExpoModel.ExportDetailsSubStep> buildDetails =
                locator.FindAll<MapExportSectionExportStat, ExpoModel.ExportDetailsSubStep>(buildCriteria);

            foreach (ExpoModel.ExportDetailsSubStep buildDetail in buildDetails)
            {
                if (exportLookup.ContainsKey(buildDetail.ParentId))
                {
                    exportLookup[buildDetail.ParentId].SubSteps.Add(buildDetail);
                }
            }

            return exportDetails;
        }

        /// <summary>
        /// 
        /// </summary>
        protected IList<ExpoModel.SectionSummary> GetSectionSummaryInfo(IRepositoryLocator locator, DateTime startDate, DateTime endDate, String username = null)
        {
            // Construct the sql query.
            String sql =
                @"SELECT
                    ms.Name AS SectionName,
	                COUNT(mes.Id) AS TotalCount,
	                SUM(mes.Success) AS SuccessCount,
	                AVG(TIMESTAMPDIFF(SECOND, mes.Start, mes.End) / CountTable.Count) AS AvgTotalTime,
	                MAX(TIMESTAMPDIFF(SECOND, mes.Start, mes.End) / CountTable.Count) AS MaxTotalTime,
	                AVG(TIMESTAMPDIFF(SECOND, mebs.Start, mebs.End) / CountTable.Count) AS AvgBuildTime,
	                MAX(TIMESTAMPDIFF(SECOND, mebs.Start, mebs.End) / CountTable.Count) AS MaxBuildTime,
                    COUNT(mebs.Id) AS BuildCount,
                    SUM(mebs.Success) AS BuildSuccessCount
                FROM MapExportStat mes
                INNER JOIN MapExportStat_MapSection mm ON mm.MapExportStatId=mes.Id
                INNER JOIN MapSection ms ON ms.Id=mm.MapSectionId
                INNER JOIN
                (
	                SELECT mes.Id, COUNT(mm.MapExportStatId) AS Count
	                FROM MapExportStat mes
	                INNER JOIN MapExportStat_MapSection mm ON mm.MapExportStatId=mes.Id
	                GROUP BY mes.Id
                ) AS CountTable ON CountTable.Id=mes.Id
                LEFT JOIN MapExportImageBuildStat mebs on mebs.MapExportStatId=mes.Id
                WHERE mes.Start BETWEEN :start AND :end ";
            if (username != null)
            {
                sql += "AND mes.Username=:username ";
            }
            sql += "GROUP BY SectionName";
            ISQLQuery sqlQuery = locator.CreateUnmanagedSQLQuery(sql);
            sqlQuery.SetParameter("start", startDate);
            sqlQuery.SetParameter("end", endDate);
            if (username != null)
            {
                sqlQuery.SetParameter("username", username);
            }
            sqlQuery.SetResultTransformer(Transformers.AliasToBean(typeof(ExpoModel.SectionSummary)));
            IList<ExpoModel.SectionSummary> exportResults = sqlQuery.List<ExpoModel.SectionSummary>();

            Dictionary<String, ExpoModel.SectionSummary> sectionLookup = exportResults.ToDictionary(item => item.SectionName);

            // Get the section export times next.
            String sectionSql =
                @"SELECT
                    ms.Name AS SectionName,
	                AVG(TIMESTAMPDIFF(SECOND, meses.Start, meses.End)) AS AvgSectionExportTime,
	                MAX(TIMESTAMPDIFF(SECOND, meses.Start, meses.End)) AS MaxSectionExportTime,
                    COUNT(meses.Id) AS SectionExportCount,
                    SUM(meses.Success) AS SectionExportSuccessCount
                FROM MapExportSectionExportStat meses
                INNER JOIN MapExportStat mes on mes.Id=meses.MapExportStatId
                INNER JOIN MapSection ms ON ms.Id=meses.MapSectionId
                WHERE mes.Start BETWEEN :start AND :end ";
            if (username != null)
            {
                sectionSql += "AND mes.Username=:username ";
            }
            sectionSql += "GROUP BY SectionName";
            ISQLQuery sectionQuery = locator.CreateUnmanagedSQLQuery(sectionSql);
            sectionQuery.SetParameter("start", startDate);
            sectionQuery.SetParameter("end", endDate);
            if (username != null)
            {
                sqlQuery.SetParameter("username", username);
            }
            sectionQuery.SetResultTransformer(Transformers.AliasToBean(typeof(ExpoModel.SectionSummary)));
            IList<ExpoModel.SectionSummary> sectionResults = sectionQuery.List<ExpoModel.SectionSummary>();

            foreach (ExpoModel.SectionSummary sectionResult in sectionResults)
            {
                String sectionName = sectionResult.SectionName;

                if (sectionName != null && sectionLookup.ContainsKey(sectionName))
                {
                    sectionLookup[sectionName].AvgSectionExportTime = sectionResult.AvgSectionExportTime;
                    sectionLookup[sectionName].MaxSectionExportTime = sectionResult.MaxSectionExportTime;
                    sectionLookup[sectionName].SectionExportCount = sectionResult.SectionExportCount;
                    sectionLookup[sectionName].SectionExportSuccessCount = sectionResult.SectionExportSuccessCount;
                }
            }

            return exportResults;
        }
        #endregion // Protected Methods
    } // MapExportReportBase
}
