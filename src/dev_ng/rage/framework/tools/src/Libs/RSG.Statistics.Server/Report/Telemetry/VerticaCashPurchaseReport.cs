﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.ROS;
using RSG.Statistics.Client.Vertica.Client;
using RSG.Statistics.Common;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Model.Telemetry;
using RSG.Statistics.Common.Report;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Server.Repository;

namespace RSG.Statistics.Server.Report.Telemetry
{
    /// <summary>
    /// Report that provides in-game cash purchase statistics.
    /// </summary>
    [Export(typeof(IStatsReport))]
    internal class VerticaCashPurchaseReport : VerticaTelemetryReportBase<CashPurchaseData>
    {
        #region Constants
        /// <summary>
        /// Name of the report.
        /// </summary>
        private const String c_name = "VerticaCashPurchases";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// How the data is grouped.
        /// </summary>
        [ReportParameter(Required = true)]
        public String Grouping { get; set; }

        /// <summary>
        /// Convenience method for converting the grouping mode to an enum value.
        /// </summary>
        public DateTimeGroupMode GroupMode
        {
            get
            {
                DateTimeGroupMode mode;
                if (!Enum.TryParse(Grouping, true, out mode))
                {
                    mode = DateTimeGroupMode.Daily;
                }
                return mode;
            }
        }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public VerticaCashPurchaseReport()
            : base(c_name)
        {
        }
        #endregion // Constructor(s)

        #region IReportDataProvider Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        /// <returns></returns>
        protected override CashPurchaseData Generate(IRepositoryLocator locator, ILog log, IStatsServer server)
        {
            IList<CashPack> activeCashPacks = locator.CreateCriteria<CashPack>().Add(Restrictions.Eq("Active", true)).List<CashPack>();

            // Convert the parameters to something vertica can work with.
            CashPurchaseParams parameters = new CashPurchaseParams();
            SetDefaultParams(locator, parameters);
            parameters.GroupMode = GroupMode;
            parameters.ValidAmounts = activeCashPacks.Select(item => item.InGameAmount).ToList();

            // Contact vertica for the report data.
            CashPurchaseData data = null;
            using (ReportClient client = new ReportClient(server.VerticaServer))
            {
                data = (CashPurchaseData)client.RunReport(ReportNames.CashPurchaseReport, parameters);
            }

            return data;
        }
        #endregion // IReportDataProvider Implementation
    } // VerticaCashPurchaseReport
}
