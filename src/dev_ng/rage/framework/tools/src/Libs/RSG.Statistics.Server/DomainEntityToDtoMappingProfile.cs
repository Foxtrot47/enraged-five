﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using RSG.Base.Math;
using RSG.Statistics.Common.Dto;
using RSG.Statistics.Common.Dto.Enums;
using RSG.Statistics.Common.Dto.ExportStats;
using RSG.Statistics.Common.Dto.GameAssets;
using RSG.Statistics.Common.Dto.GameAssets.Bundles;
using RSG.Statistics.Common.Dto.GameAssetStats;
using RSG.Statistics.Common.Dto.GameAssetStats.Bundles;
using RSG.Statistics.Domain.Entities;
using RSG.Statistics.Domain.Entities.ExportStats;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Domain.Entities.GameAssetStats;

namespace RSG.Statistics.Server
{
    /// <summary>
    /// 
    /// </summary>
    public class DomainEntityToDtoMappingProfile : Profile
    {
        /// <summary>
        /// 
        /// </summary>
        public override string ProfileName
        {
            get { return "DomainEntityToDtoMappings"; }
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void Configure()
        {
            // Misc
            Mapper.CreateMap<Build, BuildDto>();
            Mapper.CreateMap<Mission, MissionDto>();

            Mapper.CreateMap<NameHashBase, NameHashDto>();

            Mapper.CreateMap<Shader, ShaderDto>();
            Mapper.CreateMap<Texture, TextureDto>();

            // Levels
            Mapper.CreateMap<Level, LevelDto>();

            Mapper.CreateMap<LevelStat, LevelStatBundleDto>()
                .ForMember(l => l.ImageBugstarId, opt => opt.MapFrom(src => src.BackgroundImage.BugstarId))
                .ForMember(l => l.ImageData, opt => opt.MapFrom(src => src.BackgroundImage.ImageData));

            // Archetypes
            Mapper.CreateMap<DrawableArchetype, DrawableArchetypeDto>();
            Mapper.CreateMap<FragmentArchetype, FragmentArchetypeDto>();
            Mapper.CreateMap<InteriorArchetype, InteriorArchetypeDto>();
            Mapper.CreateMap<StatedAnimArchetype, StatedAnimArchetypeDto>();

            Mapper.CreateMap<CarGenStat, CarGenStatDto>()
                .ForMember(dto => dto.Position, opt => opt.Ignore());

            Mapper.CreateMap<SpawnPointStat, SpawnPointStatDto>()
                .ForMember(dto => dto.Position, opt => opt.Ignore())
                .ForMember(dto => dto.AvailabilityMode, opt => opt.Ignore());

            Mapper.CreateMap<DrawableLodStat, DrawableLodStatDto>()
                .ForMember(dto => dto.LodLevel, opt => opt.Ignore());

            Mapper.CreateMap<MapSectionAttributeStat, MapSectionAttributeStatDto>();

            // Characters
            Mapper.CreateMap<Character, CharacterDto>();

            Mapper.CreateMap<CharacterStat, CharacterStatBundleDto>()
                .ForMember(c => c.ShaderStats,
                           opt => opt.MapFrom(src => Mapper.Map<IList<ShaderStat>, List<ShaderStatBundleDto>>(src.ShaderStats)))
                .ForMember(c => c.CharacterPlatformStats,
                           opt => opt.MapFrom(src => Mapper.Map<IList<CharacterPlatformStat>, List<CharacterPlatformStatBundleDto>>(src.CharacterPlatformStats)));
            
            Mapper.CreateMap<CharacterPlatformStat, CharacterPlatformStatBundleDto>()
                .ForMember(c => c.Platform,
                           opt => opt.MapFrom(src => src.Platform.Value));
            
            // Cutscenes
            Mapper.CreateMap<Cutscene, CutsceneDto>();

            // Entities
            Mapper.CreateMap<Entity, EntityDto>();

            // Map Areas
            Mapper.CreateMap<MapArea, MapAreaDto>();
            
            // Map Hierarchy Stats
            Mapper.CreateMap<MapSectionStat, BasicMapSectionStatDto>()
                .ForMember(dto => dto.Name, opt => opt.MapFrom(src => src.MapSection.Name))
                .ForMember(dto => dto.IdentifierString, opt => opt.MapFrom(src => src.MapSection.Identifier))
                .ForMember(dto => dto.Type, opt => opt.MapFrom(src => src.MapSection.Type))
                .ForMember(dto => dto.VectorMapPoints, opt => opt.Ignore());

            Mapper.CreateMap<MapSectionAggregateStat, MapSectionAggregateStatDto>()
                .ForMember(dto => dto.Group, opt => opt.Ignore())
                .ForMember(dto => dto.LodLevel, opt => opt.Ignore())
                .ForMember(dto => dto.EntityType, opt => opt.Ignore())
                .ForMember(dto => dto.CollisionTypePolygonCounts, opt => opt.Ignore());

            // Map Sections
            Mapper.CreateMap<MapSection, MapSectionDto>();

            // Map Section Export Stats
            Mapper.CreateMap<MapExportStat, MapExportStatDto>();
            Mapper.CreateMap<MapExportMapCheckStat, MapCheckStatDto>();
            Mapper.CreateMap<MapExportSectionExportStat, SectionExportStatDto>()
                .ForMember(dto => dto.MapSectionIdentifier, opt => opt.MapFrom(src => (src.MapSection == null ? null : src.MapSection.Identifier)));
            Mapper.CreateMap<MapExportImageBuildStat, ImageBuildStatDto>()
                .ForMember(dto => dto.MapSectionIdentifiers, opt => opt.MapFrom(src => src.MapSections.Select(item => item.Identifier).ToList()));
            Mapper.CreateMap<MapExportSectionExportSubStat, SectionExportSubStatDto>();

            // Vehicles
            Mapper.CreateMap<Vehicle, VehicleDto>();

            Mapper.CreateMap<VehicleStat, VehicleStatBundleDto>()
                .ForMember(v => v.ShaderStats,
                           opt => opt.MapFrom(src => Mapper.Map<IList<ShaderStat>, List<ShaderStatBundleDto>>(src.ShaderStats)))
                .ForMember(v => v.VehiclePlatformStats,
                           opt => opt.MapFrom(src => Mapper.Map<IList<VehiclePlatformStat>, List<VehiclePlatformStatBundleDto>>(src.VehiclePlatformStats)));
            
            Mapper.CreateMap<VehiclePlatformStat, VehiclePlatformStatBundleDto>()
                .ForMember(v => v.Platform,
                           opt => opt.MapFrom(src => src.Platform.Value));
            
            // Rooms
            Mapper.CreateMap<Room, RoomDto>();

            // Weapons
            Mapper.CreateMap<Weapon, WeaponDto>();

            Mapper.CreateMap<WeaponStat, WeaponStatBundleDto>()
                .ForMember(v => v.ShaderStats,
                           opt => opt.MapFrom(src => Mapper.Map<IList<ShaderStat>, List<ShaderStatBundleDto>>(src.ShaderStats)))
                .ForMember(v => v.WeaponPlatformStats,
                           opt => opt.MapFrom(src => Mapper.Map<IList<WeaponPlatformStat>, List<WeaponPlatformStatBundleDto>>(src.WeaponPlatformStats)));

            Mapper.CreateMap<WeaponPlatformStat, WeaponPlatformStatBundleDto>()
                .ForMember(w => w.Platform,
                           opt => opt.MapFrom(src => src.Platform.Value));

            // Common Game Asset Stats
            Mapper.CreateMap<Shader, ShaderBundleDto>()
                .ForMember(s => s.Hash,
                           opt => opt.MapFrom(src => Int64.Parse(src.Identifier)));

            Mapper.CreateMap<ShaderStat, ShaderStatBundleDto>()
                .ForMember(s => s.TextureStats,
                           opt => opt.MapFrom(src => Mapper.Map<IList<TextureStat>, List<TextureStatBundleDto>>(src.TextureStats)));

            Mapper.CreateMap<Texture, TextureBundleDto>()
                .ForMember(s => s.Hash,
                           opt => opt.MapFrom(src => Int64.Parse(src.Identifier)));

            Mapper.CreateMap<TextureStat, TextureStatBundleDto>();

            // Radio Stations
            Mapper.CreateMap<RadioStation, RadioStationDto>();


            // Spatial geometry types
            Mapper.CreateMap<GeoAPI.Geometries.IPoint, Vector2f>();
            Mapper.CreateMap<GeoAPI.Geometries.IPoint, Vector3f>();

            // Enums
            Mapper.CreateMap<EnumReference<RSG.Platform.Platform>, PlatformDto>();
            Mapper.CreateMap<EnumReference<RSG.Platform.Platform>, RSG.Platform.Platform>()
                .ConstructUsing(source => (source.Value == null ? RSG.Platform.Platform.Independent : source.Value.Value));

            Mapper.CreateMap<RSG.Platform.Platform, PlatformDto>()
                .ForMember(dto => dto.Value,
                           opt => opt.MapFrom(src => src));

            Mapper.CreateMap<EnumReference<RSG.Statistics.Common.GameType>, GameTypeDto>();
            Mapper.CreateMap<EnumReference<RSG.Statistics.Common.BuildConfig>, BuildConfigDto>();
            Mapper.CreateMap<EnumReference<RSG.Statistics.Common.SectionExportSubTask>, SectionExportSubTaskDto>();
        }
    } // DomainEntityToDtoMappingProfile
}
