﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using NHibernate.SqlCommand;
using NHibernate.Transform;
using RSG.Base.Logging;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Report;
using RSG.Statistics.Domain.Entities.ExportStats;
using RSG.Statistics.Server.Repository;
using ExpoModel = RSG.Statistics.Common.Model.MapExport;

namespace RSG.Statistics.Server.Report.MapExport
{
    /// <summary>
    /// Report that provides information about how "old" the various sections are in terms of when they were last exported.
    /// </summary>
    [Export(typeof(IStatsReport))]
    internal class SectionAge : MapExportReportBase<List<ExpoModel.SectionAge>>
    {
        #region Constants
        /// <summary>
        /// Name of the report.
        /// </summary>
        private const String c_name = "MapExportsSectionAge";
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public SectionAge()
            : base(c_name)
        {
        }
        #endregion // Constructor(s)

        #region IReportDataProvider Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        /// <returns></returns>
        protected override List<ExpoModel.SectionAge> Generate(IRepositoryLocator locator, ILog log, IStatsServer server)
        {
            DetachedCriteria criteria =
                DetachedCriteria.For<MapExportStat>("mes")
                                .CreateCriteria("MapSections", "ms")
                                .SetProjection(Projections.ProjectionList()
                                    .Add(Projections.Property("ms.Name"), "SectionName")
                                    .Add(Projections.Max("Start"), "MostRecentExport")
                                    .Add(Projections.GroupProperty("ms.Name")))
                                .Add(Restrictions.Eq("mes.Success", true))
                                .SetResultTransformer(Transformers.AliasToBean(typeof(ExpoModel.SectionAge)));

            return locator.FindAll<MapExportStat, ExpoModel.SectionAge>(criteria).ToList();
        }
        #endregion // IReportDataProvider Implementation
    } // SectionAge
}
