﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using RSG.Base.Logging;
using RSG.Statistics.Client.Vertica.Client;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Model.Telemetry;
using RSG.Statistics.Common.Report;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Server.Repository;

namespace RSG.Statistics.Server.Report.Telemetry.Media
{
    /// <summary>
    /// Report that provides tv show viewer statistics.
    /// </summary>
    [Export(typeof(IStatsReport))]
    internal class VerticaTVShowReport : VerticaTelemetryReportBase<List<MediaStat>>
    {
        #region Constants
        /// <summary>
        /// Name of the report.
        /// </summary>
        private const String c_name = "VerticaTVShowViewers";
        #endregion // Constants
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public VerticaTVShowReport()
            : base(c_name)
        {
        }
        #endregion // Constructor(s)

        #region IReportDataProvider Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        /// <returns></returns>
        protected override List<MediaStat> Generate(IRepositoryLocator locator, ILog log, IStatsServer server)
        {
            // Convert the parameters to something vertica can work with.
            TelemetryParams queryParams = new TelemetryParams();
            SetDefaultParams(locator, queryParams);

            // Contact vertica for the report data.
            List<MediaStat> results;
            using (ReportClient client = new ReportClient(server.VerticaServer))
            {
                results = (List<MediaStat>)client.RunReport(ReportNames.VideoViewerReport, queryParams);
            }

            // Convert the tv show names and remove movie results.
            IGameAssetRepository<TVShow> tvShowRepo = (IGameAssetRepository<TVShow>)locator.GetRepository<TVShow>();
            IGameAssetRepository<Movie> movieRepo = (IGameAssetRepository<Movie>)locator.GetRepository<Movie>();
            IDictionary<uint, TVShow> tvShowLookup = tvShowRepo.CreateAssetLookup();
            IDictionary<uint, Movie> movieLookup = movieRepo.CreateAssetLookup();

            IList<MediaStat> toRemove = new List<MediaStat>();
            foreach (MediaStat result in results)
            {
                TVShow tvShow;
                if (tvShowLookup.TryGetValue(result.Hash, out tvShow))
                {
                    result.Name = tvShow.Name;
                    result.FriendlyName = tvShow.FriendlyName;
                }
                else if (movieLookup.ContainsKey(result.Hash))
                {
                    toRemove.Add(result);
                }
                else
                {
                    log.Warning("Unknown tv show hash encountered in data returned from Vertica. Hash: {0}", result.Hash);
                }
            }

            // Remove the stats that are for movies (keep any unknown hashes).
            log.Message("{0} of {1} videos were tv-shows.", results.Count - toRemove.Count, results.Count);
            return results.Except(toRemove).ToList();
        }
        #endregion // IReportDataProvider Implementation
    } // VerticaTVShowReport
}
