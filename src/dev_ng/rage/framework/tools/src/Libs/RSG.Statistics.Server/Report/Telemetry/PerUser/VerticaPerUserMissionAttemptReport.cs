﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using RSG.Base.Logging;
using RSG.Base.Security.Cryptography;
using RSG.Statistics.Client.Vertica.Client;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Model.Telemetry;
using RSG.Statistics.Common.Model.Telemetry.PerUser;
using RSG.Statistics.Common.Report;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Server.Repository;

namespace RSG.Statistics.Server.Report.Telemetry.PerUser
{
    /// <summary>
    /// Returns per user mission attempt stats.
    /// </summary>
    [Export(typeof(IStatsReport))]
    internal class VerticaPerUserMissionAttemptReport : VerticaPerUserTelemetryReportBase<List<PerUserMissionAttemptStats>>
    {
        #region Constants
        /// <summary>
        /// Name of the report.
        /// </summary>
        private const String c_name = "VerticaPerUserMissionAttempts";
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public VerticaPerUserMissionAttemptReport()
            : base(c_name)
        {
        }
        #endregion // Constructor(s)

        #region IStatsReport Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        protected override List<PerUserMissionAttemptStats> Generate(IRepositoryLocator locator, ILog log, IStatsServer server)
        {
            // Convert the parameters to something vertica can work with.
            PerUserTelemetryParams queryParams = new PerUserTelemetryParams();
            SetDefaultParams(locator, queryParams);

            // Get the results from the vertica service.
            List<PerUserMissionAttemptStats> results;

            using (ReportClient client = new ReportClient(server.VerticaServer))
            {
                results = (List<PerUserMissionAttemptStats>)client.RunReport(ReportNames.PerUserMissionAttemptReport, queryParams);
            }

            // Convert the mission names to friendly strings.
            IMissionRepository missionRepo = (IMissionRepository)locator.GetRepository<Mission>();
            IDictionary<uint, Mission> missionLookup = missionRepo.CreateAssetLookup();
            IDictionary<uint, Mission> altMissionLookup = missionRepo.CreateAlternativeAssetLookup();

            foreach (PerUserMissionAttemptStats result in results)
            {
                foreach (MissionAttemptStat stat in result.Data)
                {
                    Mission mission;
                    if (missionLookup.TryGetValue(OneAtATime.ComputeHash(stat.MissionName), out mission))
                    {
                        stat.MissionName = mission.Name;
                        stat.MissionId = mission.MissionId;
                    }
                    else if (altMissionLookup.TryGetValue(OneAtATime.ComputeHash(stat.MissionName), out mission))
                    {
                        stat.MissionName = mission.Name;
                        stat.MissionId = mission.MissionId;
                    }
                    else
                    {
                        log.Warning("Unknown mission name encountered in data returned from Vertica. Name: {0}", stat.MissionName);
                    }
                }
            }
            return results;
        }
        #endregion // IStatsReport Implementation
    } // VerticaPerUserMissionAttemptReport
}
