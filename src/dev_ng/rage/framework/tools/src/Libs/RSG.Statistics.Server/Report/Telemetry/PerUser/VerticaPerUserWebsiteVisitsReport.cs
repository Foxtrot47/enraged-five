﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using RSG.Base.Logging;
using RSG.Statistics.Client.Vertica.Client;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Model.Telemetry;
using RSG.Statistics.Common.Model.Telemetry.PerUser;
using RSG.Statistics.Common.Report;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Server.Repository;

namespace RSG.Statistics.Server.Report.Telemetry.PerUser
{
    /// <summary>
    /// Report that returns the number of times a particular set of users visited a particular website.
    /// </summary>
    [Export(typeof(IStatsReport))]
    internal class VerticaPerUserWebsiteVisitsReport : VerticaPerUserTelemetryReportBase<List<PerUserWebsiteVisitStats>>
    {
        #region Constants
        /// <summary>
        /// Name of the report.
        /// </summary>
        private const String c_name = "VerticaPerUserWebsiteVisits";
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public VerticaPerUserWebsiteVisitsReport()
            : base(c_name)
        {
        }
        #endregion // Constructor(s)

        #region IStatsReport Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        protected override List<PerUserWebsiteVisitStats> Generate(IRepositoryLocator locator, ILog log, IStatsServer server)
        {
            // Convert the parameters to something vertica can work with.
            PerUserTelemetryParams queryParams = new PerUserTelemetryParams();
            SetDefaultParams(locator, queryParams);

            // Get the results from the vertica service.
            List<PerUserWebsiteVisitStats> results;

            using (ReportClient client = new ReportClient(server.VerticaServer))
            {
                results = (List<PerUserWebsiteVisitStats>)client.RunReport(ReportNames.PerUserWebsiteVisitsReport, queryParams);
            }

            // Convert the oddjob name hashes to friendly strings.
            IGameAssetRepository<WebSite> webSiteRepo = (IGameAssetRepository<WebSite>)locator.GetRepository<WebSite>();
            IDictionary<uint, String> webSiteNameLookup = webSiteRepo.CreateAssetNameLookup();

            foreach (PerUserWebsiteVisitStats result in results)
            {
                foreach (MediaStat stat in result.Data)
                {
                    String name;
                    if (webSiteNameLookup.TryGetValue(stat.Hash, out name))
                    {
                        stat.Name = name;
                    }
                    else
                    {
                        log.Warning("Unknown website hash encountered in data returned from Vertica. Hash: {0}", stat.Hash);
                    }
                }
            }
            return results;
        }
        #endregion // IStatsReport Implementation
    } // VerticaPerUserWebsiteVisitsReport
}
