﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using RSG.Base.Logging;
using RSG.Statistics.Client.Vertica.Client;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Model.Telemetry;
using RSG.Statistics.Common.Report;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.Server.Repository;

namespace RSG.Statistics.Server.Report.Telemetry
{
    /// <summary>
    /// Report that provides singleplayer cheat statistics.
    /// </summary>
    [Export(typeof(IStatsReport))]
    internal class VerticaSPCheaterReport : VerticaTelemetryReportBase<List<CheatStat>>
    {
        #region Constants
        /// <summary>
        /// Name of the report.
        /// </summary>
        private const String c_name = "VerticaSPCheaters";
        #endregion // Constants
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public VerticaSPCheaterReport()
            : base(c_name)
        {
        }
        #endregion // Constructor(s)

        #region IReportDataProvider Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        /// <returns></returns>
        protected override List<CheatStat> Generate(IRepositoryLocator locator, ILog log, IStatsServer server)
        {
            // Convert the parameters to something vertica can work with.
            TelemetryParams queryParams = new TelemetryParams();
            SetDefaultParams(locator, queryParams);

            // Contact vertica for the report data.
            using (ReportClient client = new ReportClient(server.VerticaServer))
            {
                return (List<CheatStat>)client.RunReport(ReportNames.SPCheaterReport, queryParams);
            }
        }
        #endregion // IReportDataProvider Implementation
    } // VerticaSPCheatReport
}
