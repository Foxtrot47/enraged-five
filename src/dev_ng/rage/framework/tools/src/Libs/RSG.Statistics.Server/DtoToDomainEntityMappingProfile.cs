﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using RSG.Statistics.Common.Dto.GameAssets;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Common.Dto.GameAssets.Bundles;
using RSG.Statistics.Domain.Entities.GameAssetStats;
using RSG.Base.Math;
using RSG.Statistics.Common.Dto.ProfileStats;
using RSG.Statistics.Common.Dto.GameAssetStats;
using RSG.Statistics.Common.Dto.ExportStats;
using RSG.Statistics.Domain.Entities.ExportStats;
using RSG.Statistics.Domain.Entities.Captures;
using RSG.Statistics.Common.Dto.GameAssetStats.Bundles;
using RSG.Statistics.Domain.Entities;
using RSG.Statistics.Common;
using GisSharpBlog.NetTopologySuite.Geometries;

namespace RSG.Statistics.Server
{
    /// <summary>
    /// 
    /// </summary>
    public class DtoToDomainEntityMappingProfile : Profile
    {
        /// <summary>
        /// 
        /// </summary>
        public override string ProfileName
        {
            get { return "DtoToDomainEntityMappings"; }
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void Configure()
        {
            // Build config mapping 
            Mapper.CreateMap<RSG.Statistics.Common.BuildConfig, EnumReference<BuildConfig>>();

            // for Location mapping 
            Mapper.CreateMap<Vector3f, Point>()
                  .ForMember(domain => domain.X, opt => opt.MapFrom(dto => dto.X))
                  .ForMember(domain => domain.Y, opt => opt.MapFrom(dto => dto.Y))
                  .ForMember(domain => domain.Z, opt => opt.MapFrom(dto => dto.Z));

            // Level Stats
            Mapper.CreateMap<LevelStatBundleDto, LevelStat>()
                .ForMember(domain => domain.LowerLeft, opt => opt.Ignore())
                .ForMember(domain => domain.UpperRight, opt => opt.Ignore());

            // Archetype Stats
            Mapper.CreateMap<ArchetypeStatBundle, ArchetypeStat>()
                .Include<DrawableArchetypeStatBundle, DrawableArchetypeStat>()
                .Include<FragmentArchetypeStatBundle, FragmentArchetypeStat>()
                .Include<InteriorArchetypeStatBundle, InteriorArchetypeStat>()
                .Include<StatedAnimArchetypeStatBundle, StatedAnimArchetypeStat>()
                .ForMember(domain => domain.BoundingBoxMin, opt => opt.Ignore())
                .ForMember(domain => domain.BoundingBoxMax, opt => opt.Ignore())
                .ForMember(domain => domain.ShaderStats, opt => opt.Ignore())
                .ForMember(domain => domain.TxdExportSizes, opt => opt.Ignore())
                .ForMember(domain => domain.CollisionTypePolygonCounts, opt => opt.Ignore());

            Mapper.CreateMap<DrawableArchetypeStatBundle, DrawableArchetypeStat>()
                .ForMember(domain => domain.BoundingBoxMin, opt => opt.Ignore())
                .ForMember(domain => domain.BoundingBoxMax, opt => opt.Ignore())
                .ForMember(domain => domain.ShaderStats, opt => opt.Ignore())
                .ForMember(domain => domain.TxdExportSizes, opt => opt.Ignore())
                .ForMember(domain => domain.CollisionTypePolygonCounts, opt => opt.Ignore())
                .ForMember(domain => domain.DrawableLods, opt => opt.Ignore());
            Mapper.CreateMap<FragmentArchetypeStatBundle, FragmentArchetypeStat>()
                .ForMember(domain => domain.BoundingBoxMin, opt => opt.Ignore())
                .ForMember(domain => domain.BoundingBoxMax, opt => opt.Ignore())
                .ForMember(domain => domain.ShaderStats, opt => opt.Ignore())
                .ForMember(domain => domain.TxdExportSizes, opt => opt.Ignore())
                .ForMember(domain => domain.CollisionTypePolygonCounts, opt => opt.Ignore())
                .ForMember(domain => domain.DrawableLods, opt => opt.Ignore());
            Mapper.CreateMap<InteriorArchetypeStatBundle, InteriorArchetypeStat>()
                .ForMember(domain => domain.BoundingBoxMin, opt => opt.Ignore())
                .ForMember(domain => domain.BoundingBoxMax, opt => opt.Ignore())
                .ForMember(domain => domain.ShaderStats, opt => opt.Ignore())
                .ForMember(domain => domain.TxdExportSizes, opt => opt.Ignore())
                .ForMember(domain => domain.CollisionTypePolygonCounts, opt => opt.Ignore());
            Mapper.CreateMap<StatedAnimArchetypeStatBundle, StatedAnimArchetypeStat>()
                .ForMember(domain => domain.BoundingBoxMin, opt => opt.Ignore())
                .ForMember(domain => domain.BoundingBoxMax, opt => opt.Ignore())
                .ForMember(domain => domain.ShaderStats, opt => opt.Ignore())
                .ForMember(domain => domain.TxdExportSizes, opt => opt.Ignore())
                .ForMember(domain => domain.CollisionTypePolygonCounts, opt => opt.Ignore());

            Mapper.CreateMap<RoomStatBundle, RoomStat>()
                .ForMember(domain => domain.BoundingBoxMin, opt => opt.Ignore())
                .ForMember(domain => domain.BoundingBoxMax, opt => opt.Ignore())
                .ForMember(domain => domain.ShaderStats, opt => opt.Ignore())
                .ForMember(domain => domain.TxdExportSizes, opt => opt.Ignore())
                .ForMember(domain => domain.CollisionTypePolygonCounts, opt => opt.Ignore());

            Mapper.CreateMap<DrawableLodStatDto, DrawableLodStat>()
                .ForMember(domain => domain.LodLevel, opt => opt.Ignore());

            // Character Stats
            Mapper.CreateMap<CharacterStatBundleDto, CharacterStat>()
                .ForMember(domain => domain.CharacterPlatformStats, opt => opt.Ignore())
                .ForMember(domain => domain.ShaderStats, opt => opt.Ignore());

            Mapper.CreateMap<CharacterPlatformStatBundleDto, CharacterPlatformStat>()
                .ForMember(domain => domain.Platform, opt => opt.Ignore());

            // Entity Stats
            Mapper.CreateMap<EntityStatBundle, EntityStat>()
                .ForMember(domain => domain.BoundingBoxMin, opt => opt.Ignore())
                .ForMember(domain => domain.BoundingBoxMax, opt => opt.Ignore())
                .ForMember(domain => domain.LodLevel, opt => opt.Ignore());

            // Map Hierarchy Stat Nodes
            Mapper.CreateMap<BasicMapNodeStatBaseDto, MapNodeStatBase>()
                .Include<BasicMapSectionStatDto, MapSectionStat>()
                .Include<BasicMapAreaStatDto, MapAreaStat>();

            Mapper.CreateMap<BasicMapSectionStatDto, MapSectionStat>()
                .ForMember(domain => domain.VectorMapPoints, opt => opt.Ignore());
            Mapper.CreateMap<BasicMapAreaStatDto, MapAreaStat>();
            Mapper.CreateMap<CarGenStatDto, CarGenStat>()
                .ForMember(domain => domain.Position, opt => opt.Ignore());
            Mapper.CreateMap<SpawnPointStatDto, SpawnPointStat>()
                .ForMember(domain => domain.AvailabilityMode, opt => opt.Ignore())
                .ForMember(domain => domain.Position, opt => opt.Ignore());

            Mapper.CreateMap<MapSectionAggregateStatDto, MapSectionAggregateStat>()
                .ForMember(domain => domain.Group, opt => opt.Ignore())
                .ForMember(domain => domain.LodLevel, opt => opt.Ignore())
                .ForMember(domain => domain.EntityType, opt => opt.Ignore())
                .ForMember(domain => domain.CollisionTypePolygonCounts, opt => opt.Ignore());

            // Map Section Export Stats
            Mapper.CreateMap<MapExportStatDto, MapExportStat>();
            Mapper.CreateMap<MapCheckStatDto, MapExportMapCheckStat>();
            Mapper.CreateMap<SectionExportStatDto, MapExportSectionExportStat>();
            Mapper.CreateMap<ImageBuildStatDto, MapExportImageBuildStat>();
            Mapper.CreateMap<SectionExportSubStatDto, MapExportSectionExportSubStat>()
                .ForMember(domain => domain.ExportSubTask, opt => opt.Ignore());

            // Vehicle Stats
            Mapper.CreateMap<VehicleStatBundleDto, VehicleStat>()
                .ForMember(domain => domain.VehiclePlatformStats, opt => opt.Ignore())
                .ForMember(domain => domain.ShaderStats, opt => opt.Ignore());

            Mapper.CreateMap<VehiclePlatformStatBundleDto, VehiclePlatformStat>()
                .ForMember(domain => domain.Platform, opt => opt.Ignore());

            // Weapon Stats
            Mapper.CreateMap<WeaponStatBundleDto, WeaponStat>()
                .ForMember(domain => domain.WeaponPlatformStats, opt => opt.Ignore())
                .ForMember(domain => domain.ShaderStats, opt => opt.Ignore());

            Mapper.CreateMap<WeaponPlatformStatBundleDto, WeaponPlatformStat>()
                .ForMember(domain => domain.Platform, opt => opt.Ignore());

            // Common Game Asset Stats
            Mapper.CreateMap<TextureBundleDto, Texture>()
                .ForMember(domain => domain.Identifier, opt => opt.MapFrom(src => src.Hash.ToString()));

            Mapper.CreateMap<ShaderBundleDto, Shader>()
                .ForMember(domain => domain.Identifier, opt => opt.MapFrom(src => src.Hash.ToString()));
        }
    } // DtoToDomainEntityMappingProfile
}
