﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using RSG.Statistics.Server.Repository;
using RSG.Statistics.Common.Dto;
using RSG.Base.Logging;
using System.ServiceModel.Web;
using System.Net;
using RSG.Statistics.Common.Async;
using RSG.Base.Tasks;

namespace RSG.Statistics.Server.TransManager
{
    /// <summary>
    /// NHibernate based transaction manager
    /// </summary>
    public class TransManager : ITransManager
    {
        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        private readonly ISession Session;
        #endregion // Member Data

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        protected bool IsInTranx { get; set; }

        /// <summary>
        /// 
        /// </summary>
        protected IRepositoryLocator Locator { get; set; }

        /// <summary>
        /// 
        /// </summary>
        protected ITask AsyncTask { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IAsyncCommandResult AsyncTaskResults { get; protected set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="session"></param>
        public TransManager(ISession session)
            : base()
        {
            Session = session;
            Locator = new RepositoryLocator(Session);
        }
        #endregion // Constructor(s)

        #region ITransManager Implementation
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="command"></param>
        /// <returns></returns>
        public void ExecuteCommand(Action<Repository.IRepositoryLocator> command)
        {
            try
            {
                BeginTransaction();
                command.Invoke(Locator);
                CommitTransaction();
            }
            catch (WebFaultException<string> e)
            {
                Log.Log__Error("Caught web fault exception with status code: {0}", e.StatusCode);
                Rollback();
                throw;
            }
            catch (Exception e)
            {
                Log.Log__Exception(e, "Unexpected exception occurred while executing a command.");
                Rollback();
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="command"></param>
        /// <returns></returns>
        public T ExecuteCommand<T>(Func<Repository.IRepositoryLocator, T> command)
        {
            try
            {
                BeginTransaction();
                T result = command.Invoke(Locator);
                CommitTransaction();
                return result;
            }
            catch (WebFaultException<string> e)
            {
                Log.Log__Error("Caught web fault exception with status code: {0}", e.StatusCode);
                Rollback();
                throw;
            }
            catch (Exception e)
            {
                Log.Log__Exception(e, "Unexpected exception occurred while executing a command.");
                Rollback();
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="command"></param>
        /// <returns></returns>
        public IAsyncCommandResult ExecuteAsyncCommand<T>(Func<Repository.IRepositoryLocator, IAsyncCommandResult, T> command)
        {
            // Create the structure that will keep track of the task's progress.
            AsyncTaskResults = new AsyncCommandResult(Guid.NewGuid());

            // Create the task that will run
            AsyncTask = new ActionTask("", 
                (ctx, progress) =>
                {
                    try
                    {
                        BeginTransaction();
                        T result = command.Invoke(Locator, AsyncTaskResults);
                        CommitTransaction();

                        // Set the result after the db transaction has been committed.
                        AsyncTaskResults.Result = result;
                    }
                    catch (WebFaultException<string> e)
                    {
                        Log.Log__Error("Caught web fault exception with status code: {0}", e.StatusCode);
                        Rollback();
                        AsyncTaskResults.Exception = e.ToString();
                    }
                    catch (Exception e)
                    {
                        Log.Log__Exception(e, "Unexpected exception occurred while executing a command.");
                        Rollback();
                        AsyncTaskResults.Exception = e.ToString();
                    }
                    finally
                    {
                        AsyncTaskResults.Message = null;
                        AsyncTaskResults.Progress = 1.0;
                    }
                });
            
            // Finally execute the task asynchronously
            AsyncTask.ExecuteAsync(new TaskContext());
            return AsyncTaskResults;
        }
        #endregion // ITransManager Implementation

        #region Private Methods
        /// <summary>
        /// Starts a new transaction.
        /// </summary>
        private void BeginTransaction()
        {
            IsInTranx = true;
            if (!Session.Transaction.IsActive)
            {
                Session.BeginTransaction();
            }
        }

        /// <summary>
        /// Commits any database changes that were made since the transaction was started.
        /// </summary>
        private void CommitTransaction()
        {
            IsInTranx = false;
            if (Session.Transaction.IsActive)
            {
                Session.Transaction.Commit();
            }
        }

        /// <summary>
        /// Rolls back any changes that have been made to the database since the transaction was started.
        /// </summary>
        private void Rollback()
        {
            IsInTranx = false;
            if (Session.Transaction.IsActive)
            {
                Session.Transaction.Rollback();
            }
        }
        #endregion // Private Methods

        #region IDisposable Implementation
        /// <summary>
        /// 
        /// </summary>
        protected bool IsDisposed
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="disposing"></param>
        protected void Dispose(bool disposing)
        {
            if (!disposing)
            {
                return;
            }

            // free managed resources
            if (!IsDisposed && IsInTranx)
            {
                Rollback();
            }
            Close();
            Locator = null;
            IsDisposed = true;
        }

        /// <summary>
        /// 
        /// </summary>
        protected void Close()
        {
            if (Session != null && Session.IsOpen)
            {
                Session.Close();
            }
        }
        #endregion // IDisposable Implementation
    } // TransManager
}
