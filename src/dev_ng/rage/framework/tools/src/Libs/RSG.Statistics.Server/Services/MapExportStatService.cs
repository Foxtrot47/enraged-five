﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Web;
using AutoMapper;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using RSG.Base.Configuration.Services;
using RSG.Base.Extensions;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Dto.ExportStats;
using RSG.Statistics.Common.ServiceContract;
using RSG.Statistics.Domain.Entities.ExportStats;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Server.Extensions;
using RSG.Statistics.Server.Repository;

namespace RSG.Statistics.Server.Services
{
    /// <summary>
    /// 
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class MapExportStatService : ServiceBase, IMapExportStatService
    {
        #region Constructor(s)
        /// <summary>
        /// Constructor taking a server configuration object.
        /// </summary>
        /// <param name="server"></param>
        public MapExportStatService(IServiceHostConfig config)
            : base(config)
        {
        }
        #endregion // Constructor(s)
        
        #region Private Classes
        /// <summary>
        /// Helper class that contains information for a single row for the users summary csv file.
        /// </summary>
        private class ComparativeUserSectionStat
        {
            public string Username { get; set; }
            public string SectionName { get; set; }
            public DateTime ExportDate { get; set; }
            public long? ExportTime { get; set; }
            public long? BuildTime { get; set; }
            public long? TotalTime { get; set; }
        } // ComparativeUserSectionStat

        /// <summary>
        /// Helper class for gathering the weekly export stats.
        /// </summary>
        private class WeeklySectionStat
        {
            public String SectionName { get; set; }
            public decimal AverageExportTime { get; set; }
            public DateTime StartOfWeek { get; set; }
        }
        #endregion // Private Classes

        #region IMapExportStatService Implementation
        /// <summary>
        /// Creates a new map network export stat from the specified database object.
        /// </summary>
        /// <param name="dto">
        /// The database object that should be used to 
        /// </param>
        public void CreateMapNetworkExportStat(MapNetworkExportStatDto dto)
        {
            ExecuteCommand(locator => CreateMapNetworkExportStatCommand(locator, dto));
        }

        /// <summary>
        /// Creates a new map export stat.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public MapExportStatDto CreateMapExportStat(MapExportStatDto dto)
        {
            return ExecuteCommand(locator => CreateMapExportStatCommand(locator, dto));
        }

        /// <summary>
        /// Creates a new map export stat.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public MapExportStatDto UpdateMapExportStat(string exportStatId, MapExportStatDto dto)
        {
            return ExecuteCommand(locator => UpdateMapExportStatCommand(locator, Int64.Parse(exportStatId), dto));
        }

        /// <summary>
        /// Creates a new map check stat.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public MapCheckStatDto CreateMapCheckStat(String exportStatId, MapCheckStatDto dto)
        {
            return ExecuteCommand(locator => CreateMapCheckStatCommand(locator, Int64.Parse(exportStatId), dto));
        }

        /// <summary>
        /// Creates a new map export stat.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public MapCheckStatDto UpdateMapCheckStat(String exportStatId, String mapCheckStatId, MapCheckStatDto dto)
        {
            return ExecuteCommand(locator => UpdateMapCheckStatCommand(locator, Int64.Parse(exportStatId), Int64.Parse(mapCheckStatId), dto));
        }

        /// <summary>
        /// Creates a new map check stat.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public SectionExportStatDto CreateSectionExportStat(String exportStatId, SectionExportStatDto dto)
        {
            return ExecuteCommand(locator => CreateSectionExportStatCommand(locator, Int64.Parse(exportStatId), dto));
        }

        /// <summary>
        /// Creates a new section export stat.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public SectionExportStatDto UpdateSectionExportStat(String exportStatId, String sectionExportStatId, SectionExportStatDto dto)
        {
            return ExecuteCommand(locator => UpdateSectionExportStatCommand(locator, Int64.Parse(exportStatId), Int64.Parse(sectionExportStatId), dto));
        }

        /// <summary>
        /// Creates a new section export sub stat.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public SectionExportSubStatDto CreateSectionExportSubStat(String exportStatId, String sectionExportStatId, SectionExportSubStatDto dto)
        {
            return ExecuteCommand(locator => CreateSectionExportSubStatCommand(locator, Int64.Parse(exportStatId), Int64.Parse(sectionExportStatId), dto));
        }

        /// <summary>
        /// Creates a new section export stat.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public SectionExportSubStatDto UpdateSectionExportSubStat(String exportStatId, String sectionExportStatId, String subStatId, SectionExportSubStatDto dto)
        {
            return ExecuteCommand(locator => UpdateSectionExportSubStatCommand(locator, Int64.Parse(exportStatId), Int64.Parse(sectionExportStatId), Int64.Parse(subStatId), dto));
        }

        /// <summary>
        /// Creates a new image build stat.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public ImageBuildStatDto CreateImageBuildStat(String exportStatId, ImageBuildStatDto dto)
        {
            return ExecuteCommand(locator => CreateImageBuildStatCommand(locator, Int64.Parse(exportStatId), dto));
        }

        /// <summary>
        /// Updates an image build stat.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public ImageBuildStatDto UpdateImageBuildStat(String exportStatId, String imageBuildStatId, ImageBuildStatDto dto)
        {
            return ExecuteCommand(locator => UpdateImageBuildStatCommand(locator, Int64.Parse(exportStatId), Int64.Parse(imageBuildStatId), dto));
        }

        /// <summary>
        /// Retrieves a list of users that have ever exported data.
        /// </summary>
        /// <returns></returns>
        public ExportUserDtos GetAllUsers()
        {
            return ExecuteCommand(locator => GetAllUsersCommand(locator));
        }

        /// <summary>
        /// Scrapes the database generating a csv containing comparative export times for all sections/users.
        /// </summary>
        /// <returns></returns>
        public ComparativeExportStatDtos GetComparitiveExportStats()
        {
            return ExecuteCommand(locator => GetComparitiveExportStatsCommand(locator));
        }

        /// <summary>
        /// Scrapes the database generating a csv containing comparative weekly export times for all sections.
        /// </summary>
        /// <returns></returns>
        public WeeklyExportStatDtos GetWeeklyExportStats()
        {
            return ExecuteCommand(locator => GetWeeklyExportStatsCommand(locator));
        }
        #endregion // IMapExportStatService Implementation

        #region Private Methods
        #region Data Submission
        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator">
        /// 
        /// </param>
        /// <param name="dto">
        /// 
        /// </param>
        private void CreateMapNetworkExportStatCommand(IRepositoryLocator locator, MapNetworkExportStatDto dto)
        {
            MapNetworkExportStat exportStat = new MapNetworkExportStat();
            exportStat.JobId = dto.JobId;
            exportStat.Username = dto.Username;
            exportStat.ExportType = dto.ExportType;
            exportStat.MachineName = dto.MachineName;
            exportStat.ExportType = dto.ExportType;
            exportStat.JobCreationNumber = dto.JobCreationNumber;

            foreach (String sectionName in dto.SectionNames)
            {
                DetachedCriteria sectionCriteria =
                    DetachedCriteria.For<MapSection>()
                                    .Add(Restrictions.Eq("Name", sectionName));
                MapSection section = locator.FindFirst<MapSection>(sectionCriteria);
                if (section != null)
                {
                    exportStat.MapSections.Add(section);
                }
            }

            locator.Save(exportStat);
        }

        /// <summary>
        /// 
        /// </summary>
        private MapExportStatDto CreateMapExportStatCommand(IRepositoryLocator locator, MapExportStatDto dto)
        {
            // Convert the dto to a stat.
            MapExportStat exportStat = new MapExportStat();
            exportStat.Start = dto.Start;
            exportStat.Username = dto.Username;
            exportStat.MachineName = dto.MachineName;
            exportStat.ExportType = dto.ExportType;
            exportStat.ToolsVersion = dto.ToolsVersion;
            exportStat.XGEEnabled = dto.XGEEnabled;
            exportStat.XGEStandalone = dto.XGEStandalone;
            exportStat.XGEForceCPUCount = dto.XGEForceCPUCount;
            exportStat.ExportNumber = dto.ExportNumber;
            exportStat.ProcessorName = dto.ProcessorName;
            exportStat.ProcessorCores = dto.ProcessorCores;
            exportStat.InstalledMemory = dto.InstalledMemory;
            exportStat.PrivateBytesStart = (ulong)dto.PrivateBytesStart;

            foreach (RSG.Platform.Platform platform in dto.Platforms)
            {
                exportStat.Platforms.Add(locator.GetEnumReferenceByValue(platform));
            }

            foreach (String sectionName in dto.SectionNames)
            {
                DetachedCriteria sectionCriteria =
                    DetachedCriteria.For<MapSection>()
                                    .Add(Restrictions.Eq("Name", sectionName));
                MapSection section = locator.FindFirst<MapSection>(sectionCriteria);
                if (section != null)
                {
                    exportStat.MapSections.Add(section);
                }
            }

            // Save the stat.
            locator.Save(exportStat);
            
            // Set the dto's id and return it.
            dto.Id = exportStat.Id;
            return dto;
        }

        /// <summary>
        /// 
        /// </summary>
        private MapExportStatDto UpdateMapExportStatCommand(IRepositoryLocator locator, long exportStatId, MapExportStatDto dto)
        {
            // Verify the input parameters
            if (dto.Id != exportStatId)
            {
                throw new ArgumentException("Map export stat id specified in the Uri doesn't match the dto.");
            }

            MapExportStat exportStat = locator.GetById<MapExportStat>(exportStatId);
            if (exportStat == null)
            {
                throw new ArgumentException("Map export stat id specified doesn't exist in the database.");
            }

            // Update the required information and then update the stat.
            exportStat.End = dto.End;
            exportStat.PrivateBytesEnd = (ulong)dto.PrivateBytesEnd;
            exportStat.Success = dto.Success;
            locator.Update(exportStat);
            return dto;
        }

        /// <summary>
        /// 
        /// </summary>
        private MapCheckStatDto CreateMapCheckStatCommand(IRepositoryLocator locator, long exportStatId, MapCheckStatDto dto)
        {
            // Verify the input parameters
            MapExportStat exportStat = locator.GetById<MapExportStat>(exportStatId);
            if (exportStat == null)
            {
                throw new WebFaultException<string>("Map export stat id specified doesn't exist in the database.", HttpStatusCode.BadRequest);
            }

            // Create the new entry
            MapExportMapCheckStat mapCheckStat = Mapper.Map<MapCheckStatDto, MapExportMapCheckStat>(dto);
            mapCheckStat.MapExportStat = exportStat;
            locator.Save(mapCheckStat);

            // Set the dto's id and return it.
            dto.Id = mapCheckStat.Id;
            return dto;
        }

        /// <summary>
        /// 
        /// </summary>
        private MapCheckStatDto UpdateMapCheckStatCommand(IRepositoryLocator locator, long exportStatId, long mapCheckStatId, MapCheckStatDto dto)
        {
            // Verify the input parameters
            MapExportStat exportStat = locator.GetById<MapExportStat>(exportStatId);
            if (exportStat == null)
            {
                throw new WebFaultException<string>("Map export stat id specified doesn't exist in the database.", HttpStatusCode.BadRequest);
            }

            if (dto.Id != mapCheckStatId)
            {
                throw new WebFaultException<string>("Map check stat id specified in the Uri doesn't match the dto.", HttpStatusCode.BadRequest);
            }

            MapExportMapCheckStat mapCheckStat = locator.GetById<MapExportMapCheckStat>(mapCheckStatId);
            if (mapCheckStat == null)
            {
                throw new WebFaultException<string>("Map check stat id specified doesn't exist in the database.", HttpStatusCode.BadRequest);
            }

            // Update the params
            mapCheckStat.MapExportStat = exportStat;
            mapCheckStat.Start = dto.Start;
            mapCheckStat.End = dto.End;
            mapCheckStat.Success = dto.Success;
            locator.Update(mapCheckStat);
            return dto;
        }

        /// <summary>
        /// 
        /// </summary>
        private SectionExportStatDto CreateSectionExportStatCommand(IRepositoryLocator locator, long exportStatId, SectionExportStatDto dto)
        {
            // Verify the input parameters
            MapExportStat exportStat = locator.GetById<MapExportStat>(exportStatId);
            if (exportStat == null)
            {
                throw new WebFaultException<string>("Map export stat id specified doesn't exist in the database.", HttpStatusCode.BadRequest);
            }

            // Allow the section to be null if the identifier isn't found
            MapSection section = GetAssetByIdentifier<MapSection>(locator, dto.MapSectionIdentifier, false);

            // Create the new entry
            MapExportSectionExportStat sectionExportStat = Mapper.Map<SectionExportStatDto, MapExportSectionExportStat>(dto);
            sectionExportStat.MapExportStat = exportStat;
            sectionExportStat.MapSection = section;

            if (section != null && !exportStat.MapSections.Contains(section))
            {
                exportStat.MapSections.Add(section);
                locator.Save(exportStat);
            }

            locator.Save(sectionExportStat);

            // Set the id and return the dto.
            dto.Id = sectionExportStat.Id;
            return dto;
        }

        /// <summary>
        /// 
        /// </summary>
        private SectionExportStatDto UpdateSectionExportStatCommand(IRepositoryLocator locator, long exportStatId, long sectionExportStatId, SectionExportStatDto dto)
        {
            // Verify the input parameters
            MapExportStat exportStat = locator.GetById<MapExportStat>(exportStatId);
            if (exportStat == null)
            {
                throw new WebFaultException<string>("Map export stat id specified doesn't exist in the database.", HttpStatusCode.BadRequest);
            }

            if (dto.Id != sectionExportStatId)
            {
                throw new WebFaultException<string>("Section export stat id specified in the Uri doesn't match the dto.", HttpStatusCode.BadRequest);
            }

            MapExportSectionExportStat sectionExportStat = locator.GetById<MapExportSectionExportStat>(sectionExportStatId);
            if (sectionExportStat == null)
            {
                throw new WebFaultException<string>("Section export stat id specified doesn't exist in the database.", HttpStatusCode.BadRequest);
            }

            // Allow the section to be null if the identifier isn't found
            MapSection section = GetAssetByIdentifier<MapSection>(locator, dto.MapSectionIdentifier, false);

            // Update the params
            sectionExportStat.MapExportStat = exportStat;
            sectionExportStat.MapSection = section;
            sectionExportStat.Start = dto.Start;
            sectionExportStat.End = dto.End;
            sectionExportStat.Success = dto.Success;
            locator.Update(sectionExportStat);
            return dto;
        }

        /// <summary>
        /// 
        /// </summary>
        private SectionExportSubStatDto CreateSectionExportSubStatCommand(IRepositoryLocator locator, long exportStatId, long sectionExportStatId, SectionExportSubStatDto dto)
        {
            // Verify the input parameters
            MapExportStat exportStat = locator.GetById<MapExportStat>(exportStatId);
            if (exportStat == null)
            {
                throw new WebFaultException<string>("Map export stat id specified doesn't exist in the database.", HttpStatusCode.BadRequest);
            }

            MapExportSectionExportStat sectionExportStat = locator.GetById<MapExportSectionExportStat>(sectionExportStatId);
            if (sectionExportStat == null)
            {
                throw new WebFaultException<string>("Section export stat id specified doesn't exist in the database.", HttpStatusCode.BadRequest);
            }

            // Create the new entry
            MapExportSectionExportSubStat subStat = Mapper.Map<SectionExportSubStatDto, MapExportSectionExportSubStat>(dto);
            subStat.SectionExportStat = sectionExportStat;
            subStat.ExportSubTask = GetEnumReferenceByValue(locator, dto.ExportSubTask.Value);
            locator.Save(subStat);

            // Set the id and return.
            dto.Id = subStat.Id;
            return dto;
        }

        /// <summary>
        /// 
        /// </summary>
        private SectionExportSubStatDto UpdateSectionExportSubStatCommand(IRepositoryLocator locator, long exportStatId, long sectionExportStatId, long subStatId, SectionExportSubStatDto dto)
        {
            // Verify the input parameters
            MapExportStat exportStat = locator.GetById<MapExportStat>(exportStatId);
            if (exportStat == null)
            {
                throw new WebFaultException<string>("Map export stat id specified doesn't exist in the database.", HttpStatusCode.BadRequest);
            }

            MapExportSectionExportStat sectionExportStat = locator.GetById<MapExportSectionExportStat>(sectionExportStatId);
            if (sectionExportStat == null)
            {
                throw new WebFaultException<string>("Section export stat id specified doesn't exist in the database.", HttpStatusCode.BadRequest);
            }

            if (dto.Id != subStatId)
            {
                throw new WebFaultException<string>("Section export sub stat id specified in the Uri doesn't match the dto.", HttpStatusCode.BadRequest);
            }

            MapExportSectionExportSubStat subStat = locator.GetById<MapExportSectionExportSubStat>(subStatId);
            if (subStat == null)
            {
                throw new WebFaultException<string>("Section export sub stat id specified doesn't exist in the database.", HttpStatusCode.BadRequest);
            }

            // Update the params
            subStat.SectionExportStat = sectionExportStat;
            subStat.ExportSubTask = GetEnumReferenceByValue(locator, dto.ExportSubTask.Value);
            subStat.Start = dto.Start;
            subStat.End = dto.End;
            subStat.Success = dto.Success;
            locator.Update(subStat);
            return dto;
        }

        /// <summary>
        /// 
        /// </summary>
        private ImageBuildStatDto CreateImageBuildStatCommand(IRepositoryLocator locator, long exportStatId, ImageBuildStatDto dto)
        {
            // Verify the input parameters
            MapExportStat exportStat = locator.GetById<MapExportStat>(exportStatId);
            if (exportStat == null)
            {
                throw new WebFaultException<string>("Map export stat id specified doesn't exist in the database.", HttpStatusCode.BadRequest);
            }

            // Retrieve the map sections this build stat is for
            ICollection<MapSection> sections = new List<MapSection>();
            foreach (string sectionIdentifier in dto.MapSectionIdentifiers)
            {
                // Ignore sections that can't be found
                MapSection section = GetAssetByIdentifier<MapSection>(locator, sectionIdentifier, false);
                if (section != null)
                {
                    sections.Add(section);
                }
            }
            
            // Create the new entry
            MapExportImageBuildStat imageBuildStat = Mapper.Map<ImageBuildStatDto, MapExportImageBuildStat>(dto);
            imageBuildStat.MapExportStat = exportStat;
            imageBuildStat.MapSections.AddRange(sections);
            locator.Save(imageBuildStat);

            // Set the id and return.
            dto.Id = imageBuildStat.Id;
            return dto;
        }

        /// <summary>
        /// 
        /// </summary>
        private ImageBuildStatDto UpdateImageBuildStatCommand(IRepositoryLocator locator, long exportStatId, long imageBuildStatId, ImageBuildStatDto dto)
        {
            // Verify the input parameters
            MapExportStat exportStat = locator.GetById<MapExportStat>(exportStatId);
            if (exportStat == null)
            {
                throw new WebFaultException<string>("Map export stat id specified doesn't exist in the database.", HttpStatusCode.BadRequest);
            }

            if (dto.Id != imageBuildStatId)
            {
                throw new WebFaultException<string>("Image build stat id specified in the Uri doesn't match the dto.", HttpStatusCode.BadRequest);
            }

            MapExportImageBuildStat imageBuildStat = locator.GetById<MapExportImageBuildStat>(imageBuildStatId);
            if (imageBuildStat == null)
            {
                throw new WebFaultException<string>("Image build stat id specified doesn't exist in the database.", HttpStatusCode.BadRequest);
            }

            // Retrieve the map sections this build stat is for
            ICollection<MapSection> sections = new List<MapSection>();
            foreach (string sectionIdentifier in dto.MapSectionIdentifiers)
            {
                // Ignore sections that can't be found
                MapSection section = GetAssetByIdentifier<MapSection>(locator, sectionIdentifier, false);
                if (section != null)
                {
                    sections.Add(section);
                }
            }

            // Check if the map sections have changed (they shouldn't have really :P)
            IList<MapSection> sectionsToAdd = sections.Except(imageBuildStat.MapSections).ToList();
            IList<MapSection> sectionsToRemove = sections.Except(imageBuildStat.MapSections).ToList();
            imageBuildStat.MapSections.RemoveRange(sectionsToRemove);
            imageBuildStat.MapSections.AddRange(sectionsToAdd);

            // Update the params
            imageBuildStat.MapExportStat = exportStat;
            imageBuildStat.Start = dto.Start;
            imageBuildStat.End = dto.End;
            imageBuildStat.Success = dto.Success;
            locator.Update(imageBuildStat);
            return dto;
        }
        #endregion // Data Submission

        #region Data Retrieval
        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <returns></returns>
        private ExportUserDtos GetAllUsersCommand(IRepositoryLocator locator)
        {
            // Select only the username and group by it.
            DetachedCriteria query = DetachedCriteria.For<MapExportStat>()
                                                     .Add(Expression.IsNotNull("End"))
                                                     .SetProjection(Projections.ProjectionList()
                                                        .Add(Projections.Property("Username"), "Username")
                                                        .Add(Projections.GroupProperty("Username")))
                                                     .SetResultTransformer(Transformers.AliasToBean(typeof(MapExportStat)));
            IEnumerable<MapExportStat> exportStats = locator.FindAll<MapExportStat>(query, Order.Asc("Username"));

            ExportUserDtos dtos = new ExportUserDtos();
            dtos.Items.AddRange(exportStats.Select(item => item.Username));
            return dtos;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <returns></returns>
        private ComparativeExportStatDtos GetComparitiveExportStatsCommand(IRepositoryLocator locator)
        {
            string sql =
                @"SELECT * FROM
                (
		                SELECT e.Username, e.`Start` AS ExportDate, s.`Name` AS SectionName,
				                TIMESTAMPDIFF(SECOND, se.`Start`, se.`End`) AS ExportTime,
				                TIMESTAMPDIFF(SECOND, b.`Start`, b.`End`) AS BuildTime,
				                TIMESTAMPDIFF(SECOND, e.`Start`, e.`End`) AS TotalTime
		                FROM MapExportStat e
		                INNER JOIN MapExportSectionExportStat se ON e.Id=se.MapExportStatId
		                INNER JOIN MapExportImageBuildstat b ON e.Id=b.MapExportStatId
		                INNER JOIN MapSection as s ON se.MapSectionId=s.Id
		                WHERE e.Success=true AND e.ExportType='Everything'
		                ORDER BY e.Username, e.`Start` DESC
                ) AS temp
                GROUP BY Username, SectionName";

            // Create and execute the query
            ISQLQuery rawSqlQuery = locator.CreateUnmanagedSQLQuery(sql);
            rawSqlQuery.SetResultTransformer(Transformers.AliasToBean(typeof(ComparativeUserSectionStat)));

            // Retrieve the results
            IList<ComparativeUserSectionStat> stats = rawSqlQuery.List<ComparativeUserSectionStat>();

            // Convert the results to dtos
            ComparativeExportStatDtos dtos = new ComparativeExportStatDtos();

            foreach (ComparativeUserSectionStat stat in stats)
            {
                ComparativeExportStatDto dto = new ComparativeExportStatDto
                    {
                        Username = stat.Username,
                        SectionName = stat.SectionName,
                        ExportTime = (stat.ExportTime.HasValue ? new TimeSpan(0, 0, (int)stat.ExportTime.Value) : (TimeSpan?)null),
                        BuildTime = (stat.BuildTime.HasValue ? new TimeSpan(0, 0, (int)stat.BuildTime.Value) : (TimeSpan?)null),
                        TotalTime = (stat.TotalTime.HasValue ? new TimeSpan(0, 0, (int)stat.TotalTime.Value) : (TimeSpan?)null),
                        ExportDate = stat.ExportDate
                    };
                dtos.Items.Add(dto);
            }

            return dtos;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <returns></returns>
        private WeeklyExportStatDtos GetWeeklyExportStatsCommand(IRepositoryLocator locator)
        {
            // The following SQL assumes that weeks start on a Monday.
            string sql =
                @"SELECT
                    sect.Name AS SectionName,
                    AVG(TIMESTAMPDIFF(SECOND, e.`Start`, e.`End`)) AS AverageExportTime,
                    DATE(DATE_SUB(e.`Start`, INTERVAL WEEKDAY(e.`Start`) DAY)) AS StartOfWeek
                FROM mapexportstat e
                INNER JOIN mapexportsectionexportstat s on e.Id=s.MapExportStatId
                INNER JOIN mapsection sect on s.MapSectionId = sect.Id
                WHERE e.`End` IS NOT NULL
                GROUP BY WEEK(e.`Start`, 1), s.MapSectionId
                ORDER BY StartOfWeek DESC, SectionName ASC";

            // Create and execute the query
            ISQLQuery rawSqlQuery = locator.CreateUnmanagedSQLQuery(sql);
            rawSqlQuery.SetResultTransformer(Transformers.AliasToBean(typeof(WeeklySectionStat)));

            // Retrieve the results
            IList<WeeklySectionStat> stats = rawSqlQuery.List<WeeklySectionStat>();

            // Convert the results to dtos
            WeeklyExportStatDtos dtos = new WeeklyExportStatDtos();
            IDictionary<DateTime, WeeklyExportStatDto> weekToDto = new Dictionary<DateTime, WeeklyExportStatDto>();

            foreach (WeeklySectionStat stat in stats)
            {
                if (!weekToDto.ContainsKey(stat.StartOfWeek))
                {
                    WeeklyExportStatDto newDto = new WeeklyExportStatDto(stat.StartOfWeek);
                    dtos.Items.Add(newDto);
                    weekToDto[stat.StartOfWeek] = newDto;
                }

                WeeklyExportStatDto weeklyDto = weekToDto[stat.StartOfWeek];
                weeklyDto.SectionStats.Add(new WeeklySectionExportStatDto(stat.SectionName, (double)stat.AverageExportTime));
            }

            return dtos;
        }
        #endregion // Data Retrieval
        #endregion // Private Methods
    } // MapExportStatService
}
