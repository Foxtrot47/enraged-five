﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using NHibernate.Criterion;
using RSG.Base.Logging;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Dto.ExportStats;
using RSG.Statistics.Common.Dto.Reports;
using RSG.Statistics.Common.Report;
using RSG.Statistics.Domain.Entities.ExportStats;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Server.Repository;

namespace RSG.Statistics.Server.Report.Test
{
    /// <summary>
    /// 
    /// </summary>
    [Export(typeof(IStatsReport))]
    internal class MapNetworkExportReport : StatsReportBase<List<MapNetworkExportStatDto>>
    {
        #region Constants
        /// <summary>
        /// Name of the report.
        /// </summary>
        private const String c_name = "MapNetworkExportReport";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Gets or sets the minimum creation time the returned database objects should have.
        /// </summary>
        [ReportParameter]
        public DateTime Start{ get; set; }

        /// <summary>
        /// Gets or sets the maximum creation time the returned database objects should have.
        /// </summary>
        [ReportParameter]
        public DateTime End{ get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="MapNetworkExportReport"/> class.
        /// </summary>
        public MapNetworkExportReport()
            : base(c_name)
        {
        }
        #endregion // Constructor(s)

        #region IStatsReport Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        /// <returns></returns>
        protected override List<MapNetworkExportStatDto> Generate(IRepositoryLocator locator, ILog log, IStatsServer server)
        {
            DetachedCriteria query = DetachedCriteria.For<MapNetworkExportStat>()
                                                     .Add(Expression.Between("CreatedOn", this.Start, this.End));
            IList<MapNetworkExportStat> exportStats = locator.FindAll<MapNetworkExportStat>(query);
            List<MapNetworkExportStatDto> dtos = new List<MapNetworkExportStatDto>(exportStats.Count);
            foreach (MapNetworkExportStat exportStat in exportStats)
            {
                MapNetworkExportStatDto dto = new MapNetworkExportStatDto();
                dto.CreatedOn = exportStat.CreatedOn;
                dto.ExportType = exportStat.ExportType;
                dto.Id = exportStat.Id;
                dto.JobCreationNumber = exportStat.JobCreationNumber;
                dto.JobId = exportStat.JobId;
                dto.MachineName = exportStat.MachineName;
                dto.Username = exportStat.Username;
                dto.SectionNames = new List<string>(exportStat.MapSections.Count);
                foreach (MapSection section in exportStat.MapSections)
                {
                    dto.SectionNames.Add(section.Name);
                }

                dtos.Add(dto);
            }

            return dtos;
        }
        #endregion // IStatsReport Implementation
    } // CutsceneSummaryReport
}
