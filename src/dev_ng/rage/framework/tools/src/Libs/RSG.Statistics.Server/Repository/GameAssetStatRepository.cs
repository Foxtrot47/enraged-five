﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Criterion;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Domain.Entities.GameAssetStats;

namespace RSG.Statistics.Server.Repository
{
    /// <summary>
    /// Game asset stat repository.
    /// </summary>
    /// <typeparam name="TStat"></typeparam>
    /// <typeparam name="TAsset"></typeparam>
    internal class GameAssetStatRepository<TStat, TAsset> : Repository<TStat>, IGameAssetStatRepository<TStat, TAsset>
        where TStat : class, IGameAssetStat<TAsset>
        where TAsset : class, IGameAsset
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public GameAssetStatRepository(ISession session)
            : base(session)
        {
        }
        #endregion // Constructor(s)

        #region IGameAssetStatRepository<TStat, TAsset> Methods
        /// <summary>
        /// Returns all stats associated with the specified build.
        /// </summary>
        public virtual IEnumerable<TStat> GetStatsForBuild(Build build)
        {
            ICriteria criteria = Session.CreateCriteria<TStat>().Add(Restrictions.Eq("Build", build));
            return criteria.List<TStat>();
        }
        #endregion // IGameAssetStatRepository<TStat, TAsset> Methods
    } // GameAssetStatRepository<TStat, TAsset>
}
