﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using RSG.Statistics.Common.Dto.GameAssets;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Common.Dto.GameAssets.Bundles;
using RSG.Statistics.Common.Dto;
using RSG.Statistics.Domain.Entities;
using RSG.Base.Extensions;

namespace RSG.Statistics.Server
{
    /// <summary>
    /// 
    /// </summary>
    public static class AutoMapperConfiguration
    {
        /// <summary>
        /// Sets up the automatic domain entity -> dto object mappings
        /// </summary>
        public static void Install()
        {
            Mapper.Initialize(x =>
            {
                GetProfiles().ForEach(type => x.AddProfile((Profile)Activator.CreateInstance(type)));
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private static IEnumerable<Type> GetProfiles()
        {
            // Iterate over all types in the assembly looking for ones that implement the automapper's Profile class.
            foreach (Type type in typeof(AutoMapperConfiguration).Assembly.GetTypes())
            {
                if (!type.IsAbstract && typeof(Profile).IsAssignableFrom(type))
                {
                    yield return type;
                }
            }
        }
    } // AutoMapperConfiguration
}
