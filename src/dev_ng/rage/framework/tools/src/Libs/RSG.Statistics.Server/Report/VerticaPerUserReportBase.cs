﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using RSG.Base.Extensions;
using RSG.ROS;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Domain.Entities.Vertica;
using RSG.Statistics.Server.Report.Util;
using RSG.Statistics.Server.Repository;

namespace RSG.Statistics.Server.Report
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TResult"></typeparam>
    internal abstract class VerticaPerUserReportBase<TResult> : VerticaReportBase<TResult> where TResult : class
    {
        #region Properties
        /// <summary>
        /// Names of the gamer handles we wish to see the data for.
        /// </summary>
        [ReportParameter]
        public List<Tuple<String, String>> GamerHandlePlatformPairs { get; set; }

        /// <summary>
        /// List of gamer handle groups for which we wish to see data.
        /// </summary>
        [ReportParameter]
        public List<String> GamerGroups { get; set; }

        /// <summary>
        /// Optional start date.
        /// </summary>
        [ReportParameter]
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Optional end date.
        /// </summary>
        [ReportParameter("ValidateEndDate", typeof(Validation))]
        public DateTime? EndDate { get; set; }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public VerticaPerUserReportBase(String name)
            : base(name)
        {
        }
        #endregion // Constructor(s)

        #region Protected Methods
        /// <summary>
        /// Sets parameters common to all telemetry based reports.
        /// </summary>
        protected void SetDefaultParams(IRepositoryLocator locator, PerUserParams parameters)
        {
            if (!GamerHandlePlatformPairs.Any() && !GamerGroups.Any())
            {
                throw new ArgumentException("At least one of GamerHandlePlatformPairs and GamerGroups must be provided.");
            }

            ISet<Tuple<String, ROSPlatform>> gamerTuples = new HashSet<Tuple<String, ROSPlatform>>();
            if (GamerHandlePlatformPairs.Any())
            {
                gamerTuples.AddRange(ConvertGamerPlatformTuples(GamerHandlePlatformPairs));
            }
            if (GamerGroups.Any())
            {
                gamerTuples.AddRange(ConvertGamerGroups(locator, GamerGroups));
            }

            parameters.GamerHandlePlatformPairs = gamerTuples.ToList();
            parameters.StartDate = this.StartDate;
            parameters.EndDate = this.EndDate;
        }
        #endregion // Protected Methods
    } // VerticaPerUserReportBase
}
