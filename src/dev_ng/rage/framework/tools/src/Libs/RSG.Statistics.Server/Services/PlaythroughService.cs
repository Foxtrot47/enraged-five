﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using NHibernate.Criterion;
using NHibernate.SqlCommand;
using NHibernate.Transform;
using RSG.Base.Configuration.Services;
using RSG.Statistics.Common;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.ServiceContract;
using RSG.Statistics.Domain.Entities;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Domain.Entities.Playthrough;
using RSG.Statistics.Server.Extensions;
using RSG.Statistics.Server.Repository;
using DomainAssets = RSG.Statistics.Domain.Entities.GameAssets;
using PlayModel = RSG.Statistics.Common.Model.Playthrough;

namespace RSG.Statistics.Server.Services
{
    /// <summary>
    /// 
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class PlaythroughService : ServiceBase, IPlaythroughService
    {
        #region Constructor(s)
        /// <summary>
        /// Constructor taking a server configuration object.
        /// </summary>
        /// <param name="server"></param>
        public PlaythroughService(IServiceHostConfig config)
            : base(config)
        {
        }
        #endregion // Constructor(s)
        
        #region Data Retrieval
        /// <summary>
        /// Retrieves list of all users that have performed a playthrough session.
        /// </summary>
        /// <returns></returns>
        public List<PlayModel.PlaythroughUser> GetUsers()
        {
            return ExecuteReadOnlyCommand(locator => GetUsersCommand(locator));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <returns></returns>
        private List<PlayModel.PlaythroughUser> GetUsersCommand(IRepositoryLocator locator)
        {
            // Retrieve all playthrough users.
            DetachedCriteria criteria = DetachedCriteria.For<PlaythroughUser>("ps")
                .CreateAlias("Platform", "p", JoinType.LeftOuterJoin)
                .SetProjection(Projections.ProjectionList()
                    .Add(Projections.Property("Name"), "Username")
                    .Add(Projections.Property("GamerHandle"), "GamerHandle")
                    .Add(Projections.Property("p.Value"), "Platform"))
                .SetResultTransformer(Transformers.AliasToBean(typeof(PlayModel.PlaythroughUser)));
            IList<PlayModel.PlaythroughUser> users = locator.FindAll<PlaythroughUser, PlayModel.PlaythroughUser>(criteria);
            return users.ToList();
        }

        /// <summary>
        /// Retrieves list of all builds that have been used for playthroughs.
        /// </summary>
        /// <returns></returns>
        public List<String> GetBuilds()
        {
            return ExecuteReadOnlyCommand(locator => GetBuildsCommand(locator));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <returns></returns>
        private List<String> GetBuildsCommand(IRepositoryLocator locator)
        {
            // Retrieve all playthrough sessions.
            DetachedCriteria criteria = DetachedCriteria.For<PlaythroughSession>("ps")
                .CreateAlias("ps.Build", "b", NHibernate.SqlCommand.JoinType.InnerJoin)
                .SetProjection(Projections.Distinct(Projections.ProjectionList().Add(Projections.Property("b.Identifier"))));
            IList<String> builds = locator.FindAll<PlaythroughSession, String>(criteria);

            return builds.ToList();
        }

        /// <summary>
        /// Retrieves list of all platforms that playthroughs have occurred on.
        /// </summary>
        /// <returns></returns>
        public List<RSG.Platform.Platform> GetPlatforms()
        {
            return ExecuteReadOnlyCommand(locator => GetPlatformsCommand(locator));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <returns></returns>
        private List<RSG.Platform.Platform> GetPlatformsCommand(IRepositoryLocator locator)
        {
            // Retrieve all playthrough sessions.
            DetachedCriteria criteria = DetachedCriteria.For<PlaythroughSession>()
                .CreateAlias("Platform", "p", NHibernate.SqlCommand.JoinType.InnerJoin)
                .SetProjection(Projections.Distinct(Projections.ProjectionList().Add(Projections.Property("p.Value"))))
                .Add(Expression.IsNotNull("p.Value"));
            IList<RSG.Platform.Platform> platforms = locator.FindAll<PlaythroughSession, RSG.Platform.Platform>(criteria);

            return platforms.ToList();
        }

        /// <summary>
        /// Returns the list of playthrough session the database contains.
        /// </summary>
        /// <returns></returns>
        public List<PlayModel.SessionSubmission> GetSessions()
        {
            return ExecuteReadOnlyCommand(locator => GetSessionsCommand(locator));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <returns></returns>
        private List<PlayModel.SessionSubmission> GetSessionsCommand(IRepositoryLocator locator)
        {
            DetachedCriteria criteria = DetachedCriteria.For<PlaythroughSession>();
            IList<PlaythroughSession> allSessions = locator.FindAll<PlaythroughSession>(criteria);

            List<PlayModel.SessionSubmission> sessionDtos = new List<PlayModel.SessionSubmission>();
            foreach (PlaythroughSession session in allSessions)
            {
                PlayModel.SessionSubmission sessionDto = new PlayModel.SessionSubmission();
                sessionDto.Id = session.Id;
                sessionDto.BuildIdentifier = session.Build.Identifier;
                sessionDto.Platform = session.Platform.Value.Value;
                sessionDto.UserName = session.User.Name;
                sessionDto.FriendlyName = session.FriendlyName;
                sessionDto.Start = session.Start;
                sessionDto.TimeSpentPlaying = session.TimeSpentPlaying;
                sessionDtos.Add(sessionDto);
            }

            return sessionDtos;
        }

        /// <summary>
        /// Returns the list of mission attempts associated with a particular session.
        /// </summary>
        /// <returns></returns>
        public List<PlayModel.MissionAttemptSubmission> GetMissionAttempts(String sessionId)
        {
            return ExecuteReadOnlyCommand(locator => GetMissionAttemptsCommand(locator, Int64.Parse(sessionId)));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <returns></returns>
        private List<PlayModel.MissionAttemptSubmission> GetMissionAttemptsCommand(IRepositoryLocator locator, long sessionId)
        {
            // Make sure the playthrough session exists.
            PlaythroughSession dbSession = locator.GetById<PlaythroughSession>(sessionId);
            if (dbSession == null)
            {
                throw new ArgumentException(String.Format(
                    "Session with id {0} doesn't exist in the database.", sessionId));
            }

            // Convert the db objects into dtos.
            List<PlayModel.MissionAttemptSubmission> attemptDtos = new List<PlayModel.MissionAttemptSubmission>();
            foreach (PlaythroughMissionAttempt missionAttempt in dbSession.MissionAttempts)
            {
                PlayModel.MissionAttemptSubmission attemptDto = new PlayModel.MissionAttemptSubmission();
                attemptDto.Id = missionAttempt.Id;
                attemptDto.UniqueIdentifier = missionAttempt.UniqueIdentifier;
                attemptDto.MissionIdentifier = missionAttempt.Mission.Name;
                attemptDto.Result = missionAttempt.Result.Value.Value;
                attemptDto.UserComment = missionAttempt.Comment;
                attemptDto.Start = missionAttempt.Start;
                attemptDto.End = missionAttempt.End;
                attemptDto.TimeToComplete = missionAttempt.TimeToComplete;
                attemptDto.Ignore = missionAttempt.Ignore;
                attemptDtos.Add(attemptDto);
            }

            return attemptDtos;
        }

        /// <summary>
        /// Returns the list of checkpoint attempts associated with a particular mission attempt.
        /// </summary>
        /// <returns></returns>
        public List<PlayModel.CheckpointAttemptSubmission> GetCheckpointAttempts(String sessionId, String missionAttemptId)
        {
            return ExecuteReadOnlyCommand(locator => GetCheckpointAttemptsCommand(locator, Int64.Parse(sessionId), Int64.Parse(missionAttemptId)));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <returns></returns>
        private List<PlayModel.CheckpointAttemptSubmission> GetCheckpointAttemptsCommand(IRepositoryLocator locator, long sessionId, long missionAttemptId)
        {
            // Make sure the playthrough session exists.
            PlaythroughSession dbSession = locator.GetById<PlaythroughSession>(sessionId);
            if (dbSession == null)
            {
                throw new ArgumentException(String.Format(
                    "Session with id {0} doesn't exist in the database.", sessionId));
            }

            // Make sure the mission attempt exists.
            PlaythroughMissionAttempt attemptStat = locator.GetById<PlaythroughMissionAttempt>(missionAttemptId);
            if (attemptStat == null)
            {
                throw new ArgumentException(String.Format(
                    "Mission attempt with id {0} doesn't exist in the database.", missionAttemptId));
            }

            // Convert the db objects into dtos.
            List<PlayModel.CheckpointAttemptSubmission> attemptDtos = new List<PlayModel.CheckpointAttemptSubmission>();
            foreach (PlaythroughCheckpointAttempt checkpointAttempt in attemptStat.CheckpointAttempts)
            {
                PlayModel.CheckpointAttemptSubmission attemptDto = new PlayModel.CheckpointAttemptSubmission();
                attemptDto.Id = checkpointAttempt.Id;
                attemptDto.PreviousCheckpointIdx = (checkpointAttempt.Checkpoint.Index.HasValue ? checkpointAttempt.Checkpoint.Index.Value : 0);
                attemptDto.NextCheckpointIdx = attemptDto.PreviousCheckpointIdx + 1;
                attemptDto.Start = checkpointAttempt.Start;
                attemptDto.End = checkpointAttempt.End;
                attemptDto.CheckpointName = checkpointAttempt.Checkpoint.Name;
                attemptDto.TimeToComplete = checkpointAttempt.TimeToComplete;
                attemptDto.UserComment = checkpointAttempt.Comment;
                attemptDto.Ignore = checkpointAttempt.Ignore;
                attemptDtos.Add(attemptDto);
            }

            return attemptDtos;
        }
        #endregion // Data Retrieval

        #region Data Submission
        /// <summary>
        /// Creates or updates a playthrough session.
        /// </summary>
        public PlayModel.SessionSubmission CreateSession(PlayModel.SessionSubmission session)
        {
            return ExecuteCommand(locator => CreateSessionCommand(locator, session));
        }

        /// <summary>
        /// 
        /// </summary>
        private PlayModel.SessionSubmission CreateSessionCommand(IRepositoryLocator locator, PlayModel.SessionSubmission modelSession)
        {
            // Create and populate the session information.
            PlaythroughSession dbSession = new PlaythroughSession();
            dbSession.Build = locator.GetAssetByIdentifier<Build>(modelSession.BuildIdentifier, false);
            dbSession.Platform = locator.GetEnumReferenceByValue(modelSession.Platform);
            dbSession.User = GetOrCreateUser(locator, modelSession.UserName);
            dbSession.FriendlyName = modelSession.FriendlyName;
            dbSession.Start = modelSession.Start;
            dbSession.TimeSpentPlaying = modelSession.TimeSpentPlaying;

            // Check whether the requested build exists.  If not add it to the db.
            if (dbSession.Build == null)
            {
                Build build = new Build();
                build.Identifier = modelSession.BuildIdentifier;
                locator.Save(build);

                dbSession.Build = build;
            }

            locator.Save(dbSession);

            // Convert the db session back to a model session.
            modelSession.Id = dbSession.Id;
            return modelSession;
        }

        /// <summary>
        /// Creates or updates a playthrough session.
        /// </summary>
        public void UpdateSession(String id, PlayModel.SessionSubmission session)
        {
            ExecuteCommand(locator => UpdateSessionCommand(locator, Int64.Parse(id), session));
        }

        /// <summary>
        /// 
        /// </summary>
        private void UpdateSessionCommand(IRepositoryLocator locator, long sessionId, PlayModel.SessionSubmission modelSession)
        {
            // Make sure the ids match.
            if (sessionId != modelSession.Id)
            {
                throw new ArgumentException(String.Format(
                    "Session id used in url doesn't match the session id in the posted object.  {0} vs {1}.",
                    sessionId, modelSession.Id));
            }

            // Make sure the playthrough session exists.
            PlaythroughSession dbSession = locator.GetById<PlaythroughSession>(sessionId);
            if (dbSession == null)
            {
                throw new ArgumentException(String.Format(
                    "Session with id {0} doesn't exist in the database.", sessionId));
            }

            dbSession.TimeSpentPlaying += modelSession.TimeSpentPlaying;
            locator.Update(dbSession);
        }

        /// <summary>
        /// Add mission attempt information to a playthrough session.
        /// </summary>
        public PlayModel.MissionAttemptSubmission AddMissionAttempt(String sessionId, PlayModel.MissionAttemptSubmission attempt)
        {
            return ExecuteCommand(locator => AddMissionAttemptCommand(locator, Int64.Parse(sessionId), attempt));
        }

        /// <summary>
        /// 
        /// </summary>
        private PlayModel.MissionAttemptSubmission AddMissionAttemptCommand(IRepositoryLocator locator, long sessionId, PlayModel.MissionAttemptSubmission modelAttempt)
        {
            // Make sure the playthrough session exists.
            PlaythroughSession dbSession = locator.GetById<PlaythroughSession>(sessionId);
            if (dbSession == null)
            {
                throw new ArgumentException(String.Format(
                    "Session with id {0} doesn't exist in the database.", sessionId));
            }

            PlaythroughMissionAttempt attemptStat = new PlaythroughMissionAttempt();
            attemptStat.UniqueIdentifier = modelAttempt.UniqueIdentifier;
            attemptStat.Start = modelAttempt.Start;
            attemptStat.End = modelAttempt.End;
            attemptStat.TimeToComplete = modelAttempt.TimeToComplete;
            attemptStat.PlaythroughSession = dbSession;
            attemptStat.Mission = GetOrCreateMission(locator, modelAttempt.MissionIdentifier);
            attemptStat.Result = locator.GetEnumReferenceByValue(RSG.Model.Common.Mission.MissionAttemptResult.Unknown);
            attemptStat.Ignore = false;
            attemptStat.Rating = modelAttempt.Rating;
            locator.Save(attemptStat);
            
            modelAttempt.Id = attemptStat.Id;
            return modelAttempt;
        }

        /// <summary>
        /// Update a particular mission attempt's information.
        /// </summary>
        /// <returns></returns>
        public void UpdateMissionAttempt(String sessionId, String attemptId, PlayModel.MissionAttemptSubmission attemptSub)
        {
            ExecuteCommand(locator => UpdateMissionAttemptCommand(locator, Int64.Parse(sessionId), Int64.Parse(attemptId), attemptSub));
        }

        /// <summary>
        /// 
        /// </summary>
        private void UpdateMissionAttemptCommand(IRepositoryLocator locator, long sessionId, long attemptId, PlayModel.MissionAttemptSubmission attemptSub)
        {
            // Make sure the ids match.
            if (attemptId != attemptSub.Id)
            {
                throw new ArgumentException(String.Format(
                    "Mission attempt id used in url doesn't match the mission attempt id in the posted object.  {0} vs {1}.",
                    attemptId, attemptSub.Id));
            }

            // Make sure the playthrough session exists.
            PlaythroughSession dbSession = locator.GetById<PlaythroughSession>(sessionId);
            if (dbSession == null)
            {
                throw new ArgumentException(String.Format(
                    "Session with id {0} doesn't exist in the database.", sessionId));
            }

            // Make sure the mission attempt exists.
            PlaythroughMissionAttempt attemptStat = locator.GetById<PlaythroughMissionAttempt>(attemptId);
            if (attemptStat == null)
            {
                throw new ArgumentException(String.Format(
                    "Mission attempt with id {0} doesn't exist in the database.", attemptId));
            }

            attemptStat.End = attemptSub.End;
            attemptStat.TimeToComplete = attemptSub.TimeToComplete;
            attemptStat.Result = locator.GetEnumReferenceByValue(attemptSub.Result);
            attemptStat.Ignore = attemptSub.Ignore;
            attemptStat.Rating = attemptSub.Rating;

            PlaythroughCheckpointAttempt lastCheckpointAttempt = attemptStat.CheckpointAttempts.OrderBy(item => item.Start).LastOrDefault();
            if (lastCheckpointAttempt != null)
            {
                lastCheckpointAttempt.Comment = attemptSub.UserComment;
            }
            else
            {
                attemptStat.Comment = attemptSub.UserComment;
            }

            locator.Save(attemptStat);
        }

        /// <summary>
        /// Add mission checkpoint attempt information to a playthrough session/mission attempt.
        /// </summary>
        public PlayModel.CheckpointAttemptSubmission AddCheckpointAttempt(String sessionId, String attemptId, PlayModel.CheckpointAttemptSubmission checkpointSub)
        {
            return ExecuteCommand(locator => AddCheckpointAttemptCommand(locator, Int64.Parse(sessionId), Int64.Parse(attemptId), checkpointSub));
        }

        /// <summary>
        /// 
        /// </summary>
        private PlayModel.CheckpointAttemptSubmission AddCheckpointAttemptCommand(IRepositoryLocator locator, long sessionId, long attemptId, PlayModel.CheckpointAttemptSubmission checkpointSub)
        {
            // Make sure the playthrough session exists.
            PlaythroughSession dbSession = locator.GetById<PlaythroughSession>(sessionId);
            if (dbSession == null)
            {
                throw new ArgumentException(String.Format(
                    "Session with id {0} doesn't exist in the database.", sessionId));
            }

            // Make sure the mission attempt exists.
            PlaythroughMissionAttempt attemptStat = locator.GetById<PlaythroughMissionAttempt>(attemptId);
            if (attemptStat == null)
            {
                throw new ArgumentException(String.Format(
                    "Mission attempt with id {0} doesn't exist in the database.", attemptId));
            }
            
            // Create a new checkpoint attempt
            PlaythroughCheckpointAttempt checkpointAttemptStat = new PlaythroughCheckpointAttempt();
            checkpointAttemptStat.PlaythroughMissionAttempt = attemptStat;
            checkpointAttemptStat.Checkpoint = GetOrCreateCheckpoint(locator, attemptStat.Mission, checkpointSub.PreviousCheckpointIdx);
            checkpointAttemptStat.Start = checkpointSub.Start;
            checkpointAttemptStat.End = checkpointSub.End;
            checkpointAttemptStat.TimeToComplete = checkpointSub.TimeToComplete;
            checkpointAttemptStat.Comment = checkpointSub.UserComment;
            checkpointAttemptStat.Ignore = false;
            locator.Save(checkpointAttemptStat);

            checkpointSub.Id = checkpointAttemptStat.Id;
            return checkpointSub;
        }

        /// <summary>
        /// Update mission checkpoint attempt information.
        /// </summary>
        public void UpdateCheckpointAttempt(String sessionId, String missionAttemptId, String checkpointAttemptId, PlayModel.CheckpointAttemptSubmission checkpointSub)
        {
            ExecuteCommand(locator => AddCheckpointAttemptCommand(locator, Int64.Parse(sessionId), Int64.Parse(missionAttemptId), Int64.Parse(checkpointAttemptId), checkpointSub));
        }
        
        /// <summary>
        /// 
        /// </summary>
        private void AddCheckpointAttemptCommand(IRepositoryLocator locator, long sessionId, long missionAttemptId, long checkpointAttemptId, PlayModel.CheckpointAttemptSubmission attemptSub)
        {
            // Make sure the ids match.
            if (checkpointAttemptId != attemptSub.Id)
            {
                throw new ArgumentException(String.Format(
                    "Checkpoint attempt id used in url doesn't match the checkpoint attempt id in the posted object.  {0} vs {1}.",
                    checkpointAttemptId, attemptSub.Id));
            }

            // Make sure the playthrough session exists.
            PlaythroughSession dbSession = locator.GetById<PlaythroughSession>(sessionId);
            if (dbSession == null)
            {
                throw new ArgumentException(String.Format(
                    "Session with id {0} doesn't exist in the database.", sessionId));
            }

            // Make sure the mission attempt exists.
            PlaythroughMissionAttempt missionAttemptStat = locator.GetById<PlaythroughMissionAttempt>(missionAttemptId);
            if (missionAttemptStat == null)
            {
                throw new ArgumentException(String.Format(
                    "Mission attempt with id {0} doesn't exist in the database.", missionAttemptId));
            }

            PlaythroughCheckpointAttempt checkpointAttemptStat = locator.GetById<PlaythroughCheckpointAttempt>(checkpointAttemptId);
            if (checkpointAttemptStat == null)
            {
                throw new ArgumentException(String.Format(
                    "Checkpoint attempt with id {0} doesn't exist in the database.", checkpointAttemptId));
            }

            checkpointAttemptStat.Ignore = attemptSub.Ignore;
            locator.Save(checkpointAttemptStat);
        }

        /// <summary>
        /// Adds a generic comment to a particular playthrough session.
        /// </summary>
        public void AddGenericComment(String sessionId, PlayModel.CommentSubmission comment)
        {
            ExecuteCommand(locator => AddGenericCommentCommand(locator, Int64.Parse(sessionId), comment));
        }

        /// <summary>
        /// 
        /// </summary>
        private void AddGenericCommentCommand(IRepositoryLocator locator, long sessionId, PlayModel.CommentSubmission commentSub)
        {
            // Make sure the playthrough session exists.
            PlaythroughSession dbSession = locator.GetById<PlaythroughSession>(sessionId);
            if (dbSession == null)
            {
                throw new ArgumentException(String.Format(
                    "Session with id {0} doesn't exist in the database.", sessionId));
            }

            // Make sure that the context is valid.
            bool contextValid = commentSub.Context == null ||
                String.Compare(commentSub.Context, "Mission", true) == 0 ||
                String.Compare(commentSub.Context, "Weapon", true) == 0 ||
                String.Compare(commentSub.Context, "Vehicle", true) == 0;
            if (!contextValid)
            {
                throw new ArgumentOutOfRangeException("Invalid comment context provided.  Must be one of: null, Mission, Vehicle or Weapon.");
            }

            if (commentSub.Comment == null)
            {
                throw new ArgumentNullException("Comment must be provided.");
            }

            if (commentSub.Context != null && commentSub.ContextIdentifier == null)
            {
                throw new ArgumentNullException("If a context type is supplied, the context identifier cannot be null.");
            }

            // All input parameters have been validated, so try and create the comment.
            PlaythroughComment comment = new PlaythroughComment();
            comment.PlaythroughSession = dbSession;
            comment.Comment = commentSub.Comment;

            if (String.Compare(commentSub.Context, "Mission", true) == 0)
            {
                IMissionRepository missionRepo = (IMissionRepository)locator.GetRepository<RSG.Statistics.Domain.Entities.GameAssets.Mission>();
                RSG.Statistics.Domain.Entities.GameAssets.Mission mission = missionRepo.GetByMissionId(commentSub.ContextIdentifier);
                if (mission == null)
                {
                    throw new ArgumentException(String.Format(
                       "Mission with mission id {0} doesn't exist in the database.", commentSub.ContextIdentifier));
                }

                comment.Mission = mission;
            }
            else if (String.Compare(commentSub.Context, "Weapon", true) == 0)
            {
                comment.Weapon = locator.GetAssetByIdentifier<RSG.Statistics.Domain.Entities.GameAssets.Weapon>(commentSub.ContextIdentifier);
            }
            else if (String.Compare(commentSub.Context, "Vehicle", true) == 0)
            {
                comment.Vehicle = locator.GetAssetByIdentifier<RSG.Statistics.Domain.Entities.GameAssets.Vehicle>(commentSub.ContextIdentifier);
            }

            locator.Save(comment);
        }
        #endregion // IPlaythroughService Implementation

        #region Private Methods
        /// <summary>
        /// Retrieve or create a user.
        /// </summary>
        private PlaythroughUser GetOrCreateUser(IRepositoryLocator locator, String userName)
        {
            DetachedCriteria criteria = DetachedCriteria.For<PlaythroughUser>().Add(Expression.Eq("Name", userName));
            PlaythroughUser user = locator.FindFirst<PlaythroughUser>(criteria);

            if (user == null)
            {
                user = new PlaythroughUser();
                user.Name = userName;
                locator.Save(user);
            }

            return user;
        }

        /// <summary>
        /// 
        /// </summary>
        private DomainAssets.Mission GetOrCreateMission(IRepositoryLocator locator, String missionName)
        {
            // Retrieve the mission.
            uint missionHash = RSG.ManagedRage.StringHashUtil.atStringHash(missionName, 0);
            DetachedCriteria criteria = DetachedCriteria.For<DomainAssets.Mission>()
                .Add(Restrictions.Or(
                    Restrictions.Eq("AltIdentifier", missionHash.ToString()),
                    Restrictions.Eq("Identifier", missionHash.ToString())));
            DomainAssets.Mission mission = locator.FindFirst<DomainAssets.Mission>(criteria);

            if (mission == null)
            {
                RSG.Base.Logging.Log.Log__Message("Mission with identifier '{0}' doesn't exist in the database.  Creating a new mission.", missionName);
                mission = new DomainAssets.Mission();
                mission.Name = String.Format("Unknown Mission ({0})", missionName);
                mission.Identifier = "";
                mission.Description = "";
                mission.ScriptName = missionName;
                mission.AltIdentifier = RSG.ManagedRage.StringHashUtil.atStringHash(missionName, 0).ToString();
                mission.GameType = locator.GetEnumReferenceByValue(GameType.Singleplayer);
                locator.Save(mission);
            }

            return mission;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="missionIdentifier"></param>
        /// <param name="checkpoint"></param>
        /// <returns></returns>
        private DomainAssets.MissionCheckpoint GetOrCreateCheckpoint(IRepositoryLocator locator, DomainAssets.Mission mission, uint checkpointIdx)
        {
            // Retrieve the checkpoint.
            DetachedCriteria criteria = DetachedCriteria.For<DomainAssets.MissionCheckpoint>()
                .Add(Expression.Eq("Mission", mission))
                .Add(Expression.Eq("Index", checkpointIdx));
            DomainAssets.MissionCheckpoint checkpoint = locator.FindFirst<DomainAssets.MissionCheckpoint>(criteria);

            if (checkpoint == null)
            {
                RSG.Base.Logging.Log.Log__Message("Checkpoint {0} doesn't exist for mission {1}.  Creating a new checkpoint.", checkpointIdx, mission.Name);
                checkpoint = new DomainAssets.MissionCheckpoint();
                checkpoint.Name = "Unknown Checkpoint";
                checkpoint.Identifier = "";
                checkpoint.Mission = mission;
                checkpoint.Index = checkpointIdx;
                checkpoint.InBugstar = false;
                locator.Save(checkpoint);
            }

            return checkpoint;
        }
        #endregion // Private Methods
    } // PlaythroughService
}
