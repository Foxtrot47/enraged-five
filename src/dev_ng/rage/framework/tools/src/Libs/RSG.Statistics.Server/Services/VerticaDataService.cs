﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Text.RegularExpressions;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.SqlCommand;
using NHibernate.Transform;
using RSG.Base.Configuration.Services;
using RSG.Base.Extensions;
using RSG.ROS;
using RSG.Statistics.Client.Vertica.Client;
using RSG.Statistics.Common;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Model.VerticaData;
using RSG.Statistics.Common.ServiceContract;
using RSG.Statistics.Common.Vertica.Dto;
using RSG.Statistics.Domain.Entities;
using RSG.Statistics.Domain.Entities.Vertica;
using RSG.Statistics.Server.Repository;

namespace RSG.Statistics.Server.Services
{
    /// <summary>
    /// 
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class VerticaDataService : ServiceBase, IVerticaDataService
    {
        #region Constructor(s)
        /// <summary>
        /// Constructor taking a server configuration object.
        /// </summary>
        /// <param name="server"></param>
        public VerticaDataService(IServiceHostConfig config)
            : base(config)
        {
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// TODO FIX!
        /// </summary>
        private IStatsServer Server
        {
            get { return Config as IStatsServer; }
        }
        #endregion // Properties

        #region Clans
        /// <summary>
        /// Searches for a clan based on a partial name.
        /// </summary>
        public List<Clan> SearchForClan(String searchName)
        {
            // Make sure the user provided a search name that is at least 3 characters long.
            if (String.IsNullOrEmpty(searchName) || searchName.Length < 3)
            {
                throw new ArgumentException("Search name must be at least 3 characters long.");
            }

            // Retrieve the data from Vertica.
            using (DataClient client = new DataClient(Server.VerticaServer))
            {
                // HACK. We need to manually set the outgoing request method because WCF changes it to POST for some bizarre reason...
                // http://social.msdn.microsoft.com/Forums/en-US/wcf/thread/017fa465-5873-45b3-a12a-6228bf4a7cc6/
                using (OperationContextScope scope = new OperationContextScope(client.InnerChannel))
                {
                    WebOperationContext.Current.OutgoingRequest.Method = "GET";
                    return client.SearchForClan(searchName);
                }
            }
        }

        /// <summary>
        /// Returns the list of gamers that are members of a particular clan.
        /// </summary>
        public List<Gamer> GetClanMembers(String id)
        {
            // Retrieve the data from Vertica.
            using (DataClient client = new DataClient(Server.VerticaServer))
            {
                // HACK. We need to manually set the outgoing request method because WCF changes it to POST for some bizarre reason...
                // http://social.msdn.microsoft.com/Forums/en-US/wcf/thread/017fa465-5873-45b3-a12a-6228bf4a7cc6/
                using (OperationContextScope scope = new OperationContextScope(client.InnerChannel))
                {
                    WebOperationContext.Current.OutgoingRequest.Method = "GET";
                    return client.GetClanMembers(id);
                }
            }
        }
        #endregion // Clans

        #region Gamers
        /// <summary>
        /// Updates the list of websites.
        /// </summary>
        public List<Gamer> SearchForGamer(String searchName, String platform)
        {
            // Make sure the user provided a search name that is at least 3 characters long.
            if (String.IsNullOrEmpty(searchName) || searchName.Length < 3)
            {
                throw new ArgumentException("Search name must be at least 3 characters long.");
            }

            // Determine which platforms we should be searching through.
            IList<ROSPlatform> platforms = new List<ROSPlatform>();
            if (!String.IsNullOrEmpty(platform))
            {
                platforms.Add((ROSPlatform)Enum.Parse(typeof(ROSPlatform), platform));
            }
            else
            {
                platforms.AddRange(ROSPlatformUtils.GetValidPlatforms());
            }

            // Retrieve the data from Vertica.
            using (DataClient client = new DataClient(Server.VerticaServer))
            {
                return client.SearchForGamer(searchName, platforms);
            }
        }

        /// <summary>
        /// Validates a list of gamers returning only the valid ones.
        /// </summary>
        public List<Gamer> ValidateGamerList(List<Gamer> gamers)
        {
            using (DataClient client = new DataClient(Server.VerticaServer))
            {
                return client.ValidateGamerList(gamers);
            }
        }

        /// <summary>
        /// Retrieves the list of all gamer groups.
        /// </summary>
        public List<GamerGroup> GetGamerGroups()
        {
            return ExecuteCommand(locator => GetGamerGroupsCommand(locator));
        }

        /// <summary>
        /// Intermediate class for the results of the below criteria.
        /// NOTE: Couldn't convince nhibernate to do the query in a single one.
        /// </summary>
        private class GroupGamerTuple
        {
            public String GroupName { get; set; }
            public String GamerHandle { get; set; }
            public ROSPlatform Platform { get; set; }
        }

        /// <summary>
        /// As called from GetGamerGroups().
        /// </summary>
        private List<GamerGroup> GetGamerGroupsCommand(IRepositoryLocator locator)
        {
            ICriteria criteria =
                locator.CreateCriteria<VerticaGamerGroup>()
                    .SetFetchMode("Gamers", FetchMode.Join)
                    .CreateAlias("Gamers", "g", JoinType.LeftOuterJoin)
                    .CreateAlias("g.Platform", "p", JoinType.LeftOuterJoin)
                    .SetProjection(Projections.ProjectionList()
                        .Add(Projections.Property("Name"), "GroupName")
                        .Add(Projections.Property("g.Name"), "GamerHandle")
                        .Add(Projections.Property("p.Value"), "Platform"))
                    .SetResultTransformer(Transformers.AliasToBean(typeof(GroupGamerTuple)));

            // Convert the db groups to dto groups.
            List<GamerGroup> groupResults = new List<GamerGroup>();
            foreach (IGrouping<String, GroupGamerTuple> group in criteria.List<GroupGamerTuple>().GroupBy(item => item.GroupName))
            {
                GamerGroup groupResult = new GamerGroup(group.Key);

                foreach (GroupGamerTuple gamer in group)
                {
                    if (gamer.GamerHandle != null)
                    {
                        Gamer gamerResult = new Gamer
                        {
                            GamerHandle = gamer.GamerHandle,
                            Platform = gamer.Platform
                        };
                        groupResult.Gamers.Add(gamerResult);
                    }
                }
                groupResults.Add(groupResult);
            }
            return groupResults;
        }

        /// <summary>
        /// Creates a new gamer group.
        /// </summary>
        public List<String> CreateGamerGroup(GamerGroup group)
        {
            return ExecuteCommand(locator => CreateGamerGroupCommand(locator, group));
        }

        /// <summary>
        /// As called from CreateGamerGroup.
        /// </summary>
        private List<String> CreateGamerGroupCommand(IRepositoryLocator locator, GamerGroup groupSub)
        {
            // Make sure a group with this name doesn't already exist.
            VerticaGamerGroup group = GetGamerGroupByName(locator, groupSub.Name);
            if (group != null)
            {
                throw new ArgumentException(String.Format("Gamer group with name {0} already exists in the database.", groupSub.Name));
            }

            if (Regex.IsMatch(groupSub.Name, @"[<>*%&:\\\?]"))
            {
                throw new ArgumentException("Group name is invalid.  The name must not contain any of the following characters: <, >, *, %, &, :, \\ or ?.");
            }

            // Create the new group.
            group = new VerticaGamerGroup(groupSub.Name);
            List<String> messages = AddGamersToGroup(locator, group, groupSub.Gamers);
            locator.Save(group);

            return messages;
        }

        /// <summary>
        /// 
        /// </summary>
        private List<String> AddGamersToGroup(IRepositoryLocator locator, VerticaGamerGroup group, List<Gamer> gamers)
        {
            List<String> messages = new List<String>();

            if (gamers.Any())
            {
                // Get the list of valid gamers first.
                List<Gamer> validGamers;
                using (DataClient client = new DataClient(Server.VerticaServer))
                {
                    validGamers = client.ValidateGamerList(gamers);
                }

                // Create the message list based on the difference between the submitted gamers and the ones that were valid.
                foreach (Gamer invalidGamer in gamers.Except(validGamers))
                {
                    messages.Add(String.Format("The following gamer wasn't found in Vertica: {0} ({1}).",
                        invalidGamer.GamerHandle, invalidGamer.Platform.GetFriendlyNameValue()));
                }

                foreach (Gamer gamerSub in validGamers)
                {
                    VerticaGamer gamer = GetOrCreateGamer(locator, gamerSub.GamerHandle, gamerSub.Platform);
                    group.Gamers.Add(gamer);
                }
            }

            return messages;
        }

        /// <summary>
        /// Updates an existing gamer group.
        /// </summary>
        public List<String> UpdateGamerGroup(GamerGroup group, String name)
        {
            return ExecuteCommand(locator => UpdateGamerGroupCommand(locator, group, name));
        }

        /// <summary>
        /// As called from UpdateGamerGroup.
        /// </summary>
        private List<String> UpdateGamerGroupCommand(IRepositoryLocator locator, GamerGroup groupSub, String name)
        {
            if (groupSub.Name != name)
            {
                throw new ArgumentException(String.Format("Group name passed in dto object doesn't match URL name.  {0} vs {1}.", groupSub.Name, name));
            }

            VerticaGamerGroup group = GetGamerGroupByName(locator, name);
            if (group == null)
            {
                throw new ArgumentException(String.Format("Gamer group with name {0} doesn't exist in the database.", groupSub.Name));
            }

            // Update the gamer list.
            IEnumRepository<ROSPlatform> enumRepo = (IEnumRepository<ROSPlatform>)locator.GetRepository<EnumReference<ROSPlatform>>();

            group.Gamers.Clear();
            List<String> messages = AddGamersToGroup(locator, group, groupSub.Gamers);
            locator.Save(group);

            return messages;
        }

        /// <summary>
        /// Deletes an existing gamer group.
        /// </summary>
        public void DeleteGamerGroup(String name)
        {
            ExecuteCommand(locator => DeleteGamerGroupCommand(locator, name));
        }

        /// <summary>
        /// As called from DeleteGamerGroup.
        /// </summary>
        private void DeleteGamerGroupCommand(IRepositoryLocator locator, String name)
        {
            VerticaGamerGroup group = GetGamerGroupByName(locator, name);
            if (group == null)
            {
                throw new ArgumentException(String.Format("Gamer group with name {0} doesn't exist in the database.", name));
            }

            locator.Delete(group);
        }

        /// <summary>
        /// Retrieves a gamer group from the db based off of it's name.
        /// </summary>
        private VerticaGamerGroup GetGamerGroupByName(IRepositoryLocator locator, String name)
        {
            ICriteria criteria = locator.CreateCriteria<VerticaGamerGroup>().Add(Restrictions.Eq("Name", name));
            return criteria.UniqueResult<VerticaGamerGroup>();
        }

        /// <summary>
        /// Retrieves a gamer based off of his name/platform.
        /// </summary>
        private VerticaGamer GetOrCreateGamer(IRepositoryLocator locator, String name, ROSPlatform platform)
        {
            IEnumRepository<ROSPlatform> enumRepo = (IEnumRepository<ROSPlatform>)locator.GetRepository<EnumReference<ROSPlatform>>();
            EnumReference<ROSPlatform> rosPlatform = enumRepo.GetByValue(platform);

            ICriteria criteria = locator.CreateCriteria<VerticaGamer>().Add(Restrictions.Eq("Name", name)).Add(Restrictions.Eq("Platform", rosPlatform));
            VerticaGamer gamer = criteria.UniqueResult<VerticaGamer>();
            if (gamer == null)
            {
                gamer = new VerticaGamer();
                gamer.Name = name;
                gamer.Platform = rosPlatform;
                locator.Save(gamer);
            }
            return gamer;
        }
        #endregion // Gamers

        #region Missions
        /// <summary>
        /// Searches for a UGC mission user with the particular name.
        /// </summary>
        public List<UGCMission> SearchForMission(String searchName, String matchType, String missionCategory, String published)
        {
            // Make sure the user provided a search name that is at least 3 characters long.
            if (String.IsNullOrEmpty(searchName) || searchName.Length < 3)
            {
                throw new ArgumentException("Search name must be at least 3 characters long.");
            }

            // Create the post object.
            MissionSearchFilters filters = new MissionSearchFilters();

            // Validate the provided match type.
            if (matchType != null)
            {
                MatchType matchTypeOut;
                if (Enum.TryParse(matchType, out matchTypeOut))
                {
                    filters.MatchType = matchTypeOut;
                }
            }

            // Validate the provided mission category.
            if (missionCategory != null)
            {
                FreemodeMissionCategory missionCategoryOut;
                if (Enum.TryParse(missionCategory, out missionCategoryOut))
                {
                    filters.MissionCategory = missionCategoryOut;
                }
            }

            // Check whether the published flag should be taken into consideration.
            if (published != null)
            {
                bool publishedOut;
                if (Boolean.TryParse(published, out publishedOut))
                {
                    filters.PublishedOnly = publishedOut;
                }
            }

            using (DataClient client = new DataClient(Server.VerticaServer))
            {
                return client.SearchForMission(searchName, filters);
            }
        }
        #endregion // Missions

        #region Social Club Users
        /// <summary>
        /// Updates the list of websites.
        /// </summary>
        public List<String> SearchForUser(String searchName)
        {
            // Make sure the user provided a search name that is at least 3 characters long.
            if (String.IsNullOrEmpty(searchName) || searchName.Length < 3)
            {
                throw new ArgumentException("Search name must be at least 3 characters long.");
            }

            using (DataClient client = new DataClient(Server.VerticaServer))
            {
                // HACK. We need to manually set the outgoing request method because WCF changes it to POST for some bizarre reason...
                // http://social.msdn.microsoft.com/Forums/en-US/wcf/thread/017fa465-5873-45b3-a12a-6228bf4a7cc6/
                using (OperationContextScope scope = new OperationContextScope(client.InnerChannel))
                {
                    WebOperationContext.Current.OutgoingRequest.Method = "GET";
                    return client.SearchForUser(searchName);
                }
            }
        }

        /// <summary>
        /// Validates a list of users returning only the valid ones.
        /// </summary>
        public List<String> ValidateUserList(List<String> users)
        {
            using (DataClient client = new DataClient(Server.VerticaServer))
            {
                return client.ValidateUserList(users);
            }
        }

        /// <summary>
        /// Retrieves the list of all user groups.
        /// </summary>
        public List<UserGroup> GetUserGroups()
        {
            return ExecuteCommand(locator => GetUserGroupsCommand(locator));
        }

        /// <summary>
        /// Intermediate class for the results of the below criteria.
        /// NOTE: Couldn't convince nhibernate to do the query in a single one.
        /// </summary>
        private class GroupUserTuple
        {
            public String GroupName { get; set; }
            public String UserName { get; set; }
        }

        /// <summary>
        /// As called by GetUserGroups.
        /// </summary>
        private List<UserGroup> GetUserGroupsCommand(IRepositoryLocator locator)
        {
            ICriteria criteria =
                locator.CreateCriteria<VerticaUserGroup>()
                    .SetFetchMode("Users", FetchMode.Join)
                    .CreateAlias("Users", "u", JoinType.LeftOuterJoin)
                    .SetProjection(Projections.ProjectionList()
                        .Add(Projections.Property("Name"), "GroupName")
                        .Add(Projections.Property("u.Name"), "UserName"))
                    .SetResultTransformer(Transformers.AliasToBean(typeof(GroupUserTuple)));

            // Convert the db groups to dto groups.
            List<UserGroup> groupResults = new List<UserGroup>();
            foreach (IGrouping<String, GroupUserTuple> group in criteria.List<GroupUserTuple>().GroupBy(item => item.GroupName))
            {
                UserGroup groupResult = new UserGroup(group.Key);
                groupResult.Users.AddRange(group.Where(item => item.UserName != null).Select(item => item.UserName));
                groupResults.Add(groupResult);
            }
            return groupResults;
        }

        /// <summary>
        /// Creates a new user group.
        /// </summary>
        public List<String> CreateUserGroup(UserGroup group)
        {
            return ExecuteCommand(locator => CreateUserGroupCommand(locator, group));
        }

        /// <summary>
        /// As called by CreateUserGroup.
        /// </summary>
        private List<String> CreateUserGroupCommand(IRepositoryLocator locator, UserGroup groupSub)
        {
            // Make sure a group with this name doesn't already exist.
            VerticaUserGroup group = GetUserGroupByName(locator, groupSub.Name);
            if (group != null)
            {
                throw new ArgumentException(String.Format("Social club user group with name {0} already exists in the database.", groupSub.Name));
            }

            if (Regex.IsMatch(groupSub.Name, @"[<>*%&:\\\?]"))
            {
                throw new ArgumentException("Group name is invalid.  The name must not contain any of the following characters: <, >, *, %, &, :, \\ or ?.");
            }

            // Create the new group.
            group = new VerticaUserGroup(groupSub.Name);
            List<String> messages = AddUsersToGroup(locator, group, groupSub.Users);
            locator.Save(group);

            return messages;
        }

        /// <summary>
        /// 
        /// </summary>
        private List<String> AddUsersToGroup(IRepositoryLocator locator, VerticaUserGroup group, List<String> users)
        {
            List<String> messages = new List<String>();

            if (users.Any())
            {
                // Get the list of valid gamers first.
                List<String> validUsers;
                using (DataClient client = new DataClient(Server.VerticaServer))
                {
                    validUsers = client.ValidateUserList(users);
                }

                // Create the message list based on the difference between the submitted gamers and the ones that were valid.
                foreach (String invalidUser in users.Except(validUsers))
                {
                    messages.Add(String.Format("The following user wasn't found in Vertica: {0}.", invalidUser));
                }

                foreach (String userSub in validUsers)
                {
                    VerticaUser user = GetOrCreateUser(locator, userSub);
                    group.Users.Add(user);
                }
            }

            return messages;
        }

        /// <summary>
        /// Updates an existing user group.
        /// </summary>
        public List<String> UpdateUserGroup(UserGroup group, String name)
        {
            return ExecuteCommand(locator => UpdateUserGroupCommand(locator, group, name));
        }

        /// <summary>
        /// As called by UpdateUserGroup.
        /// </summary>
        private List<String> UpdateUserGroupCommand(IRepositoryLocator locator, UserGroup groupSub, String name)
        {
            if (groupSub.Name != name)
            {
                throw new ArgumentException(String.Format("Group name passed in dto object doesn't match URL name.  {0} vs {1}.", groupSub.Name, name));
            }

            VerticaUserGroup group = GetUserGroupByName(locator, name);
            if (group == null)
            {
                throw new ArgumentException(String.Format("Social club user group with name {0} doesn't exist in the database.", groupSub.Name));
            }

            // Update the gamer list.
            group.Users.Clear();
            List<String> messages = AddUsersToGroup(locator, group, groupSub.Users);
            locator.Save(group);

            return messages;
        }

        /// <summary>
        /// Deletes an existing user group.
        /// </summary>
        public void DeleteUserGroup(String name)
        {
            ExecuteCommand(locator => DeleteUserGroupCommand(locator, name));
        }

        /// <summary>
        /// As called by DeleteUserGroup.
        /// </summary>
        private void DeleteUserGroupCommand(IRepositoryLocator locator, String name)
        {
            VerticaUserGroup group = GetUserGroupByName(locator, name);
            if (group == null)
            {
                throw new ArgumentException(String.Format("Social club user group with name {0} doesn't exist in the database.", name));
            }

            locator.Delete(group);
        }

        /// <summary>
        /// Retrieves a social club user group from the db based off of it's name.
        /// </summary>
        private VerticaUserGroup GetUserGroupByName(IRepositoryLocator locator, String name)
        {
            ICriteria criteria = locator.CreateCriteria<VerticaUserGroup>().Add(Restrictions.Eq("Name", name));
            return criteria.UniqueResult<VerticaUserGroup>();
        }

        /// <summary>
        /// Retrieves a social club user based off of his name.
        /// </summary>
        private VerticaUser GetOrCreateUser(IRepositoryLocator locator, String name)
        {
            ICriteria criteria = locator.CreateCriteria<VerticaUser>().Add(Restrictions.Eq("Name", name));
            VerticaUser user = criteria.UniqueResult<VerticaUser>();
            if (user == null)
            {
                user = new VerticaUser();
                user.Name = name;
                locator.Save(user);
            }
            return user;
        }
        #endregion // Social Club Users
    } // VerticaDataService
}
