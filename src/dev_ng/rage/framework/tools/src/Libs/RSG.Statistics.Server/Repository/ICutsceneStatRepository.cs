﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common.Animation;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Domain.Entities.GameAssetStats.Animation;

namespace RSG.Statistics.Server.Repository
{
    /// <summary>
    /// Cutscene stat repository.
    /// </summary>
    internal interface ICutsceneStatRepository : IGameAssetStatRepository<CutsceneStat, Cutscene>
    {
        #region Methods
        /// <summary>
        /// Creates a cutscene collection for the specified build.
        /// </summary>
        /// <param name="build"></param>
        /// <returns></returns>
        ICutsceneCollection CreateCollectionForBuild(Build build);
        #endregion // Methods
    } // ICutsceneStatRepository
}
