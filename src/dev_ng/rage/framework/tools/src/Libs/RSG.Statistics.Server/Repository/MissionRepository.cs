﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Criterion;
using RSG.Statistics.Domain.Entities.GameAssets;

namespace RSG.Statistics.Server.Repository
{
    /// <summary>
    /// 
    /// </summary>
    internal class MissionRepository : GameAssetRepository<Mission>, IMissionRepository
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public MissionRepository(ISession session)
            : base(session)
        {
        }
        #endregion // Constructor(s)

        #region IMissionRepository Methods
        /// <summary>
        /// Retrieves a mission by it's ID (e.g. ARM1, CAR3, etc...).
        /// </summary>
        public Mission GetByMissionId(String missionId)
        {
            if (missionId == null)
            {
                return null;
            }

            ICriteria criteria = Session.CreateCriteria<Mission>().Add(Restrictions.Eq("MissionId", missionId)).SetMaxResults(1);
            return criteria.List<Mission>().FirstOrDefault();
        }

        /// <summary>
        /// Creates a hash to asset lookup for all assets.
        /// </summary>
        /// <returns></returns>
        public override IDictionary<uint, Mission> CreateAssetLookup()
        {
            ICriteria criteria = Session.CreateCriteria<Mission>();

            IDictionary<uint, Mission> lookup = new Dictionary<uint, Mission>();
            foreach (Mission mission in criteria.List<Mission>().Where(item => !String.IsNullOrEmpty(item.Identifier)))
            {
                lookup[UInt32.Parse(mission.Identifier)] = mission;
            }
            return lookup;
        }

        /// <summary>
        /// Creates a hash to name lookup for all assets.
        /// </summary>
        /// <returns></returns>
        public override IDictionary<uint, String> CreateAssetNameLookup()
        {
            ICriteria criteria = Session.CreateCriteria<Mission>();

            IDictionary<uint, String> lookup = new Dictionary<uint, String>();
            foreach (Mission mission in criteria.List<Mission>().Where(item => !String.IsNullOrEmpty(item.Identifier)))
            {
                lookup[UInt32.Parse(mission.Identifier)] = mission.Name;
            }
            return lookup;
        }

        /// <summary>
        /// Creates a hash to asset lookup for all assets.
        /// </summary>
        /// <returns></returns>
        public IDictionary<uint, Mission> CreateAlternativeAssetLookup()
        {
            ICriteria criteria = Session.CreateCriteria<Mission>();

            IDictionary<uint, Mission> lookup = new Dictionary<uint, Mission>();
            foreach (Mission mission in criteria.List<Mission>().Where(item => !String.IsNullOrEmpty(item.AltIdentifier)))
            {
                lookup[UInt32.Parse(mission.AltIdentifier)] = mission;
            }
            return lookup;
        }

        /// <summary>
        /// Creates a hash to name lookup for all assets based off of the alternative identifier.
        /// </summary>
        /// <returns></returns>
        public IDictionary<uint, String> CreateAlternativeAssetNameLookup()
        {
            ICriteria criteria = Session.CreateCriteria<Mission>();

            IDictionary<uint, String> lookup = new Dictionary<uint, String>();
            foreach (Mission mission in criteria.List<Mission>().Where(item => !String.IsNullOrEmpty(item.AltIdentifier)))
            {
                lookup[UInt32.Parse(mission.AltIdentifier)] = mission.Name;
            }
            return lookup;
        }
        #endregion // IMissionRepository Methods
    } // MissionRepository
}
