﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common.Dto;
using RSG.Statistics.Server.Repository;
using RSG.Statistics.Server.TransManager;
using AutoMapper;
using RSG.Statistics.Domain.Entities;
using System.ComponentModel.Composition;
using RSG.Statistics.Server.Context;
using NHibernate.Criterion;
using System.ServiceModel.Web;
using System.Net;
using RSG.Statistics.Common.Async;
using RSG.Base.Tasks;
using NHibernate.Metadata;
using RSG.Statistics.Server.Extensions;
using RSG.Base.Logging;
using RSG.Statistics.Common.Config;
using RSG.Services.Common.Services;
using RSG.Base.Configuration.Services;

namespace RSG.Statistics.Server.Services
{
    /// <summary>
    /// Base class from which all services that interact with a database should inherit from.
    /// </summary>
    public abstract class ServiceBase : ConfigAwareService
    {
        #region Static Data
        /// <summary>
        /// Transaction manager factory.
        /// </summary>
        public static ITransManagerFactory TransManagerFactory { get; private set; }
        #endregion // Static Data

        #region Member Data
        /// <summary>
        /// Private log object.
        /// </summary>
        protected ILog Log
        {
            get { return _log; }
        }

        /// <summary>
        /// Need to keep track of transaction managers for asynchronous operations.
        /// </summary>
        private IDictionary<Guid, ITransManager> AsyncOperations
        {
            get
            {
                if (m_asyncOperations == null)
                {
                    m_asyncOperations = new Dictionary<Guid, ITransManager>();
                }
                return m_asyncOperations;
            }
        }
        private IDictionary<Guid, ITransManager> m_asyncOperations;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ServiceBase(IServiceHostConfig config)
            : base(config)
        {
        }
        #endregion // Constructor(s)

        #region Protected Methods
        /// <summary>
        /// Executes a synchronously command through the transaction manager
        /// </summary>
        /// <param name="command"></param>
        protected void ExecuteCommand(Action<IRepositoryLocator> command, bool readOnly = false)
        {
            using (ITransManager manager = GlobalContext.Instance.TransManagerFactory.CreateManager(readOnly))
            {
                manager.ExecuteCommand(command);
            }
        }

        /// <summary>
        /// Convenience method for executing a readonly command.
        /// </summary>
        protected void ExecuteReadOnlyCommand(Action<IRepositoryLocator> command)
        {
            ExecuteCommand(command, true);
        }

        /// <summary>
        /// Executes a command synchronously through the transaction manager
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="command"></param>
        /// <returns></returns>
        protected T ExecuteCommand<T>(Func<IRepositoryLocator, T> command, bool readOnly = false)
        {
            using (ITransManager manager = GlobalContext.Instance.TransManagerFactory.CreateManager(readOnly))
            {
                return manager.ExecuteCommand(command);
            }
        }

        /// <summary>
        /// Convenience method for executing a readonly command.
        /// </summary>
        protected T ExecuteReadOnlyCommand<T>(Func<IRepositoryLocator, T> command)
        {
            return ExecuteCommand(command, true);
        }

        /// <summary>
        /// Executes a command asynchronously
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="command"></param>
        /// <returns></returns>
        protected IAsyncCommandResult ExecuteAsyncCommand<T>(Func<IRepositoryLocator, IAsyncCommandResult, T> command, bool readOnly = false)
        {
            ITransManager manager = GlobalContext.Instance.TransManagerFactory.CreateManager(readOnly);
            IAsyncCommandResult result = manager.ExecuteAsyncCommand(command);

            // Keep track of the async operation so that we can properly dispose of the transaction manager.
            AsyncOperations[result.TaskIdentifier] = manager;

            return result;
        }

        /// <summary>
        /// Queries the progress of an asynchronous operation based on the tasks identifier.
        /// </summary>
        /// <param name="taskIdentifier"></param>
        /// <returns></returns>
        public IAsyncCommandResult QueryAsyncOperation(Guid taskIdentifier)
        {
            if (!AsyncOperations.ContainsKey(taskIdentifier))
            {
                throw new WebFaultException<string>(String.Format("Task with identifier '{0}' doesn't exist.", taskIdentifier), HttpStatusCode.BadRequest);
            }

            ITransManager manager = AsyncOperations[taskIdentifier];
            IAsyncCommandResult results = manager.AsyncTaskResults;

            // If the task has completed we no longer need to keep track of it.
            if (results.Progress == 1.0)
            {
                AsyncOperations.Remove(taskIdentifier);
                manager.Dispose();
            }

            return results;
        }

        /// <summary>
        /// Helper method for getting an db enum reference from it's corresponding enum value.
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="locator"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        [Obsolete()]
        protected EnumReference<TEnum> GetEnumReferenceByValue<TEnum>(IRepositoryLocator locator, TEnum value) where TEnum : struct
        {
            return locator.GetEnumReferenceByValue<TEnum>(value);
        }

        /// <summary>
        ///  Helper method for retrieving a game asset by it's identifier.
        /// </summary>
        /// <param name="identifier"></param>
        /// <returns></returns>
        [Obsolete()]
        protected T GetAssetByIdentifier<T>(IRepositoryLocator locator, string identifier, bool throwIfNull = true) where T : class, IDomainEntity
        {
            return locator.GetAssetByIdentifier<T>(identifier, throwIfNull);
        }

        /// <summary>
        /// Helper method for enumerating all nhibernate mapped db properties for a particular type.
        /// </summary>
        /// <param name="dbType"></param>
        /// <returns></returns>
        [Obsolete()]
        protected ProjectionList CreateProjectionListForAllProperties<T>(IRepositoryLocator locator) where T : IDomainEntity
        {
            ProjectionList projList = Projections.ProjectionList();

            IClassMetadata metadata = locator.Session.SessionFactory.GetClassMetadata(typeof(T));
            foreach (String name in metadata.PropertyNames)
            {
                projList.Add(Projections.Property(name), name);
            }

            return projList;
        }
        #endregion // Protected Methods
    } // ServiceBase
}
