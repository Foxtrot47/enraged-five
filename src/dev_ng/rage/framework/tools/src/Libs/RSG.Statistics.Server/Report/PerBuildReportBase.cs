﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Server.Report
{
    /// <summary>
    /// Base class for reports that work on a single build's worth of data.
    /// </summary>
    internal abstract class PerBuildReportBase<TResult> : StatsReportBase<TResult>
        where TResult : class
    {
        #region Properties
        /// <summary>
        /// Build that we wish to retrieve data for.
        /// </summary>
        [ReportParameter]
        public String BuildIdentifier { get; set; }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        internal PerBuildReportBase(String name)
            : base(name)
        {
        }
        #endregion // Constructor(s)
    } // PerBuildReportBase<TResult>
}
