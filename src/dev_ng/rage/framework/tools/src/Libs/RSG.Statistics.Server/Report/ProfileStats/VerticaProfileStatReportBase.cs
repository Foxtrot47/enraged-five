﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Criterion;
using RSG.Base.Extensions;
using RSG.ROS;
using RSG.Statistics.Common.Dto.ProfileStats;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Domain.Entities.Profile;
using RSG.Statistics.Server.Report.Util;
using RSG.Statistics.Server.Repository;

namespace RSG.Statistics.Server.Report.ProfileStats
{
    /// <summary>
    /// 
    /// </summary>
    internal abstract class VerticaProfileStatReportBase<RParam> : VerticaReportBase<RParam>
        where RParam : class
    {
        #region Properties
        /// <summary>
        /// Names of the platforms we wish to see the data for.
        /// </summary>
        [ReportParameter]
        public List<String> PlatformNames { get; set; }

        /// <summary>
        /// Convenience property that transforms the platform names to platform enum values.
        /// </summary>
        public IList<RSG.Platform.Platform> Platforms
        {
            get
            {
                IList<RSG.Platform.Platform> platforms = new List<RSG.Platform.Platform>();
                foreach (String platformName in PlatformNames)
                {
                    RSG.Platform.Platform platform;
                    if (Enum.TryParse(platformName, out platform))
                    {
                        platforms.Add(platform);
                    }
                }
                return platforms;
            }
        }

        /// <summary>
        /// Optional end date.
        /// </summary>
        [ReportParameter("ValidateEndDate", typeof(Validation))]
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Optional lower age boundary.
        /// </summary>
        [ReportParameter]
        public int? AgeMin { get; set; }

        /// <summary>
        /// Optional upper age boundary.
        /// </summary>
        [ReportParameter]
        public int? AgeMax { get; set; }

        /// <summary>
        /// Optional country codes where the user is registered.
        /// </summary>
        [ReportParameter]
        public List<String> CountryCodes { get; set; }

        /// <summary>
        /// Optional list of social club users to restrict the results to.
        /// </summary>
        [ReportParameter]
        public List<String> SocialClubUserNames { get; set; }

        /// <summary>
        /// Optional list of social club groups to restrict the results to.
        /// </summary>
        [ReportParameter]
        public List<String> SocialClubUserGroups { get; set; }

        /// <summary>
        /// Names of the gamer handles we wish to see the data for.
        /// </summary>
        [ReportParameter]
        public List<Tuple<String, String>> GamerHandlePlatformPairs { get; set; }

        /// <summary>
        /// List of gamer handle groups for which we wish to see data.
        /// </summary>
        [ReportParameter]
        public List<String> GamerGroups { get; set; }

        /// <summary>
        /// Size of each bucket that will be returned.  If null it means we want a single bucket.
        /// </summary>
        [ReportParameter]
        public double? BucketSize { get; set; }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public VerticaProfileStatReportBase(String name)
            : base(name)
        {
        }
        #endregion // Constructor(s)

        #region Protected Methods
        /// <summary>
        /// Sets parameters common to all telemetry based reports.
        /// </summary>
        protected void SetDefaultParams(IRepositoryLocator locator, ProfileStatParams parameters)
        {
            parameters.EndDate = this.EndDate;
            parameters.AgeMin = this.AgeMin;
            parameters.AgeMax = this.AgeMax;
            parameters.CountryCodes = this.CountryCodes.Distinct().ToList();
            parameters.BucketSize = this.BucketSize;

            ISet<String> scUserNames = new HashSet<String>();
            if (SocialClubUserNames.Any())
            {
                scUserNames.AddRange(SocialClubUserNames);
            }
            if (SocialClubUserGroups.Any())
            {
                scUserNames.AddRange(ConvertUserGroups(locator, SocialClubUserGroups));
            }
            parameters.SocialClubUsernames = scUserNames.ToList();

            ISet<Tuple<String, ROSPlatform>> gamerTuples = new HashSet<Tuple<String, ROSPlatform>>();
            if (GamerHandlePlatformPairs.Any())
            {
                gamerTuples.AddRange(ConvertGamerPlatformTuples(GamerHandlePlatformPairs));
            }
            if (GamerGroups.Any())
            {
                gamerTuples.AddRange(ConvertGamerGroups(locator, GamerGroups));
            }
            parameters.GamerHandlePlatformPairs = gamerTuples.ToList();

            if (Platforms.Any())
            {
                ISet<ROSPlatform> rosPlatforms = new HashSet<ROSPlatform>();
                foreach (RSG.Platform.Platform platform in Platforms)
                {
                    ROSPlatform rosPlatform;
                    if (TryConvertPlatformToROSPlatform(platform, out rosPlatform))
                    {
                        rosPlatforms.Add(rosPlatform);
                    }
                }
                parameters.Platforms = rosPlatforms.ToList();
            }
            else
            {
                parameters.Platforms = ROSPlatformUtils.GetValidPlatforms().ToList();
            }
        }

        /// <summary>
        /// Retrieves list of default values associated with the passed in stat names.
        /// </summary>
        /// <returns></returns>
        protected IList<Tuple<String, String>> GetDefaultStatValues(IRepositoryLocator locator, IList<String> statNames)
        {
            ICriteria criteria =
                locator.CreateCriteria<ProfileStatDefinition>()
                    .Add(Restrictions.In("Name", statNames.ToArray()));
            IList<ProfileStatDefinition> defs = criteria.List<ProfileStatDefinition>();

            return defs.Select(item => Tuple.Create(item.Name, item.DefaultValue)).ToList();
        }
        #endregion // Protected Methods
    } // VerticaProfileStatReportBase
}
