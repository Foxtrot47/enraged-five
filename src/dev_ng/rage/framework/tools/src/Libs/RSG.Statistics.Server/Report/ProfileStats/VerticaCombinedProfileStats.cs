﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Linq;
using System.Text;
using RSG.Base.Logging;
using RSG.ROS;
using RSG.Statistics.Client.Vertica.Client;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Dto.ProfileStats;
using RSG.Statistics.Common.Report;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.Server.Repository;

namespace RSG.Statistics.Server.Report.ProfileStats
{
    /// <summary>
    /// Report based off of a combination of profile stats.
    /// </summary>
    [Export(typeof(IStatsReport))]
    internal class VerticaCombinedProfileStats : VerticaProfileStatReportBase<List<ProfileStatDto>>
    {
        #region Constants
        /// <summary>
        /// Name of the report.
        /// </summary>
        private const String c_name = "VerticaCombinedProfileStats";
        #endregion // Constants
        
        #region Properties
        /// <summary>
        /// Names of the stats we wish to see combined data for.
        /// </summary>
        [ReportParameter(Required = true)]
        public List<String> StatNames { get; set; }

        /// <summary>
        /// Flag that indicates whether we wish to add stats for gamers that haven't changed from the default.
        /// </summary>
        [ReportParameter]
        public bool? IgnoreDefaultValues { get; set; }

        /// <summary>
        /// Minimum allowed value (allows us to filter out invalid values).
        /// </summary>
        [ReportParameter]
        public float? MinValue { get; set; }

        /// <summary>
        /// Maximum allowed value (allows us to filter out invalid values).
        /// </summary>
        [ReportParameter]
        public float? MaxValue { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public VerticaCombinedProfileStats()
            : base(c_name)
        {
        }
        #endregion // Constructor(s)
        
        #region IReportDataProvider Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        /// <returns></returns>
        protected override List<ProfileStatDto> Generate(IRepositoryLocator locator, ILog log, IStatsServer server)
        {
            Debug.Assert(StatNames.Any(), "No stat names provided.");

            // Convert the parameters to something vertica can work with.
            CombinedProfileStatParams queryParams = new CombinedProfileStatParams();
            queryParams.StatNames = GetDefaultStatValues(locator, StatNames);
            queryParams.IgnoreDefaultValues = (IgnoreDefaultValues == true);
            queryParams.MinValue = MinValue;
            queryParams.MaxValue = MaxValue ?? 1000000;
            SetDefaultParams(locator, queryParams);

            // Contact vertica for the report data.
            using (ReportClient client = new ReportClient(server.VerticaServer))
            {
                return (List<ProfileStatDto>)client.RunReport(ReportNames.CombinedProfileStatReport, queryParams);
            }
        }
        #endregion // IReportDataProvider Implementation
    } // VerticaCombinedProfileStats
}
