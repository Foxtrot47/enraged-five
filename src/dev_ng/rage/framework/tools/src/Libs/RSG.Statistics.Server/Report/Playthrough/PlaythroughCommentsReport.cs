﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using NHibernate.SqlCommand;
using RSG.Base.Logging;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Report;
using RSG.Statistics.Domain.Entities.Playthrough;
using RSG.Statistics.Server.Repository;
using PlayModel = RSG.Statistics.Common.Model.Playthrough;

namespace RSG.Statistics.Server.Report.Playthrough
{
    /// <summary>
    /// Report that returns all generic comments that have been added.
    /// </summary>
    [Export(typeof(IStatsReport))]
    internal class PlaythroughCommentsReport : PlaythroughReportBase<List<PlayModel.PlaythroughComment>>
    {
        #region Constants
        /// <summary>
        /// Name of the report.
        /// </summary>
        private const String c_name = "PlaythroughComments";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Optional user to restrict the results to.
        /// </summary>
        [ReportParameter]
        public List<String> Users { get; set; }

        /// <summary>
        /// Flag indicating whether we wish to exclude users instead of include them.
        /// </summary>
        [ReportParameter]
        public bool ExcludeUsers { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public PlaythroughCommentsReport()
            : base(c_name)
        {
        }
        #endregion // Constructor(s)

        #region IStatsReport Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        protected override List<PlayModel.PlaythroughComment> Generate(IRepositoryLocator locator, ILog log, IStatsServer server)
        {
            return GetGenericComments(locator);
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void AddRestrictions(DetachedCriteria criteria, IRepositoryLocator locator, String sessionAlias = "")
        {
            base.AddRestrictions(criteria, locator, sessionAlias);

            String alias = sessionAlias;
            if (sessionAlias != "")
            {
                alias = sessionAlias + ".";
            }

            if (Users.Any())
            {
                List<PlaythroughUser> users = new List<PlaythroughUser>();
                foreach (String userName in Users)
                {
                    DetachedCriteria userCriteria = DetachedCriteria.For<PlaythroughUser>().Add(Expression.Eq("Name", userName));
                    PlaythroughUser user = locator.FindFirst<PlaythroughUser>(userCriteria);
                    if (user == null)
                    {
                        throw new ArgumentException("Playthrough user doesn't exist in the database.");
                    }
                    users.Add(user);
                }

                if (ExcludeUsers)
                {
                    criteria.Add(Restrictions.Not(Restrictions.In(alias + "User", users)));
                }
                else
                {
                    criteria.Add(Restrictions.In(alias + "User", users));
                }
            }
        }
        #endregion // IStatsReport Implementation
    } // PlaythroughCommentsReport
}
