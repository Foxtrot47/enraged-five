﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Linq;
using System.Text;
using RSG.Base.Logging;
using RSG.Statistics.Client.Vertica.Client;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Dto.ProfileStats;
using RSG.Statistics.Common.Report;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.Server.Repository;

namespace RSG.Statistics.Server.Report.ProfileStats
{
    /// <summary>
    /// Report based off of a combination of profile stats.
    /// </summary>
    [Export(typeof(IStatsReport))]
    internal class VerticaCombinedDiffReport : VerticaProfileStatReportBase<List<ProfileStatDto>>
    {
        #region Constants
        /// <summary>
        /// Name of the report.
        /// </summary>
        private const String c_name = "VerticaCombinedDiffProfileStats";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Optional end date.
        /// </summary>
        [ReportParameter(Required = true)]
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Names of the stats we wish to see combined data for.
        /// </summary>
        [ReportParameter(Required = true)]
        public List<String> StatNames { get; set; }

        /// <summary>
        /// Maximum difference between two consecutively submitted values (allows us to filter out absurdly large changes).
        /// </summary>
        [ReportParameter]
        public float? MaxDifference { get; set; }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public VerticaCombinedDiffReport()
            : base(c_name)
        {
        }
        #endregion // Constructor(s)
        
        #region IReportDataProvider Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        /// <returns></returns>
        protected override List<ProfileStatDto> Generate(IRepositoryLocator locator, ILog log, IStatsServer server)
        {
            Debug.Assert(StatNames.Any(), "No stat names provided.");

            // Convert the parameters to something vertica can work with.
            CombinedProfileStatParams queryParams = new CombinedProfileStatParams();
            queryParams.StartDate = this.StartDate;
            queryParams.StatNames = GetDefaultStatValues(locator, StatNames);
            queryParams.MaxValue = MaxDifference ?? 1000000;
            SetDefaultParams(locator, queryParams);

            if (queryParams.StatNames.Any())
            {
                // Contact vertica for the report data.
                using (ReportClient client = new ReportClient(server.VerticaServer))
                {
                    return (List<ProfileStatDto>)client.RunReport(ReportNames.CombinedDiffProfileStatReport, queryParams);
                }
            }
            else
            {
                return null;
            }
        }
        #endregion // IReportDataProvider Implementation
    } // VerticaCombinedDiffReport
}
