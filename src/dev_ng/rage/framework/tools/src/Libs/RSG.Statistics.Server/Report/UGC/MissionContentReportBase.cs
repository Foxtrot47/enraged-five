﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Extensions;
using RSG.ROS;
using RSG.Statistics.Common;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Server.Repository;

namespace RSG.Statistics.Server.Report.UGC
{
    /// <summary>
    /// Base class for mission content based reports.
    /// </summary>
    internal abstract class MissionContentReportBase<TResult> : VerticaReportBase<TResult>, IUGCMissionReport
    {
        #region Properties
        /// <summary>
        /// List of mission category names that we wish to restrict the results to.
        /// </summary>
        [ReportParameter]
        public List<String> MissionCategoryNames { get; set; }

        /// <summary>
        /// Convenience property that transforms the mission category names to mission category enum values.
        /// </summary>
        public IList<FreemodeMissionCategory> MissionCategories
        {
            get
            {
                IList<FreemodeMissionCategory> categories = new List<FreemodeMissionCategory>();
                foreach (String categoryName in MissionCategoryNames)
                {
                    FreemodeMissionCategory category;
                    if (Enum.TryParse(categoryName, out category))
                    {
                        categories.Add(category);
                    }
                }
                return categories;
            }
        }

        /// <summary>
        /// Start date for when the match was created.
        /// </summary>
        [ReportParameter]
        public DateTime? MissionCreatedStart { get; set; }

        /// <summary>
        /// End date for when the match was created.
        /// </summary>
        [ReportParameter]
        public DateTime? MissionCreatedEnd { get; set; }

        /// <summary>
        /// Names of the gamer handles of mission creators we wish to see the data for.
        /// </summary>
        [ReportParameter]
        public List<Tuple<String, String>> MissionCreatorUserIdPlatformPairs { get; set; }

        /// <summary>
        /// Optional flag to restrict the results to only published or non-published missions.
        /// </summary>
        [ReportParameter]
        public bool? MissionPublishedOnly { get; set; }

        /// <summary>
        /// Whether we should return only the latest versions of a particular piece of content.
        /// </summary>
        [ReportParameter]
        public bool? LatestVersionsOnly { get; set; }

        /// <summary>
        /// Whether we should included deleted missions in the report.
        /// </summary>
        [ReportParameter]
        public bool? IncludeDeleted { get; set; }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public MissionContentReportBase(String name)
            : base(name)
        {
        }
        #endregion // Constructor(s)

        #region Protected Methods
        /// <summary>
        /// Sets parameters common to all misson based reports.
        /// </summary>
        protected void SetDefaultParams(IRepositoryLocator locator, MissionContentParams parameters)
        {
            parameters.MissionCategories = MissionCategories;
            parameters.MissionCreatedStart = MissionCreatedStart;
            parameters.MissionCreatedEnd = MissionCreatedEnd;
            parameters.MissionPublishedOnly = MissionPublishedOnly;
            parameters.IncludeDeleted = IncludeDeleted;
            parameters.LatestVersionsOnly = LatestVersionsOnly;

            ISet<Tuple<String, ROSPlatform>> creatorTuples = new HashSet<Tuple<String, ROSPlatform>>();
            if (MissionCreatorUserIdPlatformPairs.Any())
            {
                creatorTuples.AddRange(ConvertGamerPlatformTuples(MissionCreatorUserIdPlatformPairs));
            }
            parameters.MissionCreatorUserIdPlatformPairs = creatorTuples.ToList();
        }
        #endregion // Protected Methods
    } // MissionContentReportBase<TResult>
}
