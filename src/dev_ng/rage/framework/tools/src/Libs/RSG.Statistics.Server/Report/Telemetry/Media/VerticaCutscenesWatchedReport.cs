﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using RSG.Base.Logging;
using RSG.ROS;
using RSG.Statistics.Client.Vertica.Client;
using RSG.Statistics.Common;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Model.Telemetry;
using RSG.Statistics.Common.Report;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Dto.ReportResults;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Server.Repository;

namespace RSG.Statistics.Server.Report.Telemetry.Media
{
    /// <summary>
    /// Report that provides cutscene view statistics.
    /// </summary>
    [Export(typeof(IStatsReport))]
    internal class VerticaCutscenesWatchedReport : VerticaTelemetryReportBase<List<CutsceneMissionStat>>
    {
        #region Constants
        /// <summary>
        /// Name of the report.
        /// </summary>
        private const String c_name = "VerticaCutscenesWatched";
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public VerticaCutscenesWatchedReport()
            : base(c_name)
        {
        }
        #endregion // Constructor(s)

        #region IReportDataProvider Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        /// <returns></returns>
        protected override List<CutsceneMissionStat> Generate(IRepositoryLocator locator, ILog log, IStatsServer server)
        {
            // Convert the parameters to something vertica can work with.
            CutscenesWatchedParams queryParams = new CutscenesWatchedParams();
            SetDefaultParams(locator, queryParams);

            // Contact vertica for the report data.
            List<CutsceneViewResult> intermediateResults;
            using (ReportClient client = new ReportClient(server.VerticaServer))
            {
                intermediateResults = (List<CutsceneViewResult>)client.RunReport(ReportNames.CutscenesWatchedReport, queryParams);
            }

            // Patch up the cutscene names/durations.
            IDictionary<String, CutsceneMissionStat> missionStatLookup = new SortedDictionary<String, CutsceneMissionStat>();
            if (intermediateResults != null)
            {
                IGameAssetRepository<Cutscene> cutsceneRepo = (IGameAssetRepository<Cutscene>)locator.GetRepository<Cutscene>();
                IDictionary<uint, Cutscene> cutsceneLookup = cutsceneRepo.CreateAssetLookup();

                // Force all the missions to be loaded.
                IMissionRepository missionRepo = (IMissionRepository)locator.GetRepository<Mission>();
                missionRepo.FindAll();

                foreach (CutsceneViewResult result in intermediateResults)
                {
                    Cutscene cutscene;
                    if (cutsceneLookup.TryGetValue(result.CutsceneHash, out cutscene))
                    {
                        Mission mission = cutscene.Mission;
                        String missionId = (mission != null ? mission.MissionId : null);
                        String missionName = (mission != null ? mission.Name : "Uncategorised");
                        
                        CutsceneMissionStat missionStat;
                        if (!missionStatLookup.TryGetValue(missionName, out missionStat))
                        {
                            missionStat = new CutsceneMissionStat(missionName, missionId);
                            missionStatLookup[missionName] = missionStat;
                        }

                        missionStat.CutsceneViews.Add(new CutsceneViewStat
                            {
                                CutsceneName = cutscene.Name,
                                CutsceneFriendlyName = cutscene.FriendlyName,
                                CutsceneDuration = cutscene.Duration,
                                CutsceneHasBranches = cutscene.HasBranch,
                                AverageTimeWatched = result.AverageTimeWatched,
                                Started = result.Started,
                                Skipped = result.Skipped,
                                NumGamers = result.NumGamers,
                                PlayersWatchedToCompletion = result.PlayersWatchedToCompletion
                            });
                    }
                    else
                    {
                        log.Warning("Unknown cutscene hash encountered in data returned from Vertica. Hash: {0}", result.CutsceneHash);
                    }
                }
            }
            return missionStatLookup.Values.ToList();
        }
        #endregion // IReportDataProvider Implementation
    } // VerticaCutscenesWatchedReport
}
