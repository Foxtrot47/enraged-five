﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Criterion;
using RSG.Base.Security.Cryptography;
using RSG.Statistics.Domain.Entities.GameAssets;

namespace RSG.Statistics.Server.Repository
{
    /// <summary>
    /// Game asset repository.
    /// </summary>
    /// <typeparam name="TAsset"></typeparam>
    internal class GameAssetRepository<TAsset> : Repository<TAsset>, IGameAssetRepository<TAsset>
        where TAsset : class, IGameAsset, new()
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public GameAssetRepository(ISession session)
            : base(session)
        {
        }
        #endregion // Constructor(s)

        #region IGameAssetRepository<TAsset> Methods
        /// <summary>
        /// Retrieves a game asset by it's hash.
        /// </summary>
        public TAsset GetByHash(uint hash)
        {
            return GetByIdentifier(hash.ToString());
        }

        /// <summary>
        /// Retrieves a game asset by the string version of it's identifier.
        /// NOTE: This should eventually be made obsolete in favor of the former method.  Still need to 
        /// make the appropriate modifications to the game asset class though.
        /// </summary>
        public TAsset GetByIdentifier(String identifier)
        {
            ICriteria criteria = Session.CreateCriteria<TAsset>().Add(Restrictions.Eq("Identifier", identifier)).SetMaxResults(1);
            return criteria.List<TAsset>().FirstOrDefault();
        }

        /// <summary>
        /// Retrieves a game asset by it's name.
        /// </summary>
        public TAsset GetByName(String name)
        {
            return GetByHash(OneAtATime.ComputeHash(name));
        }

        /// <summary>
        /// Attempts to retrieve a game asset by it's hash and creating a new one if it doesn't exist.
        /// </summary>
        public TAsset GetOrCreateByName(String name)
        {
            uint hash = OneAtATime.ComputeHash(name);

            TAsset asset = GetByHash(hash);
            if (asset == null)
            {
                asset = new TAsset();
                asset.Name = name;
                asset.Identifier = hash.ToString();
                Save(asset);
            }

            return asset;
        }

        /// <summary>
        /// Creates a hash to asset lookup for all assets.
        /// NOTE: That this will only work with assets that have hashes stored in their identifiers.
        /// </summary>
        /// <returns></returns>
        public virtual IDictionary<uint, TAsset> CreateAssetLookup()
        {
            ICriteria criteria = Session.CreateCriteria<TAsset>();
            return criteria.List<TAsset>().ToDictionary(item => UInt32.Parse(item.Identifier));
        }

        /// <summary>
        /// Creates a hash to name lookup for all assets.
        /// </summary>
        /// <returns></returns>
        public virtual IDictionary<uint, String> CreateAssetNameLookup()
        {
            ICriteria criteria = Session.CreateCriteria<TAsset>();
            return criteria.List<TAsset>().ToDictionary(item => UInt32.Parse(item.Identifier), item => item.Name);
        }
        #endregion // Methods
    } // GameAssetRepository<TAsset>
}
