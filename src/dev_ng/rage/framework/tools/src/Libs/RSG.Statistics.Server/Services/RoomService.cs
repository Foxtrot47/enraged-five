﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Common.Dto.GameAssets;
using RSG.Statistics.Common.ServiceContract;
using System.ServiceModel;
using RSG.Statistics.Common.Config;
using RSG.Base.Configuration.Services;

namespace RSG.Statistics.Server.Services
{
    /// <summary>
    /// 
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class RoomService : GameAssetServiceBase<Room, RoomDto, RoomDtos>, IRoomService
    {
        #region Constructor(s)
        /// <summary>
        /// Constructor taking a server configuration object.
        /// </summary>
        /// <param name="server"></param>
        public RoomService(IServiceHostConfig config)
            : base(config)
        {
        }
        #endregion // Constructor(s)
    } // RoomService
}
