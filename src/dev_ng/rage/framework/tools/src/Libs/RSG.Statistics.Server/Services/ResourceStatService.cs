﻿using System;
using System.Collections.Generic;
using RSG.Statistics.Common.ServiceContract;
using System.ServiceModel;
using RSG.Statistics.Server.Repository;
using NHibernate.Criterion;
using RSG.Statistics.Domain.Entities;
using RSG.Platform;
using RSG.Model.Statistics.Platform;
using RSG.Statistics.Domain.Entities.ResourceStats;
using RSG.Statistics.Common.Config;
using RSG.Base.Configuration.Services;

namespace RSG.Statistics.Server.Services
{
    /// <summary>
    /// Service for resource stats
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class ResourceStatService : ServiceBase, IResourceStatService
    {
        #region Constructor(s)
        /// <summary>
        /// Constructor taking a server configuration object.
        /// </summary>
        /// <param name="server"></param>
        public ResourceStatService(IServiceHostConfig config)
            : base(config)
        {
        }
        #endregion // Constructor(s)
        
        #region Public Methods
        /// <summary>
        /// Concrete entry point of endpoint
        /// </summary>
        /// <param name="resourceStats"></param>
        public void CreateResourceStats(List<RSG.Model.Statistics.Platform.ResourceStat> resourceStats)
        {
            // Grab the insertion timestamp NOW, not as we go...
            DateTime timeOfInsertion = DateTime.Now;

            foreach (RSG.Model.Statistics.Platform.ResourceStat resourceStat in resourceStats)
            {
                ExecuteCommand(locator => CreateResourceStatCommand(locator, resourceStat, timeOfInsertion));
            }
        }

        #endregion // Public Methods

        #region Private Methods

        /// <summary>
        /// Insert into the database the list of resourceStats model
        /// </summary>
        private void CreateResourceStatCommand(IRepositoryLocator locator, RSG.Model.Statistics.Platform.ResourceStat resourceStat, DateTime timeOfInsertion)
        {
            IGameAssetRepository<ResourceName> resouceNameRepo = (IGameAssetRepository<ResourceName>)locator.GetRepository<ResourceName>();

            // Read data
            EnumReference<RSG.Platform.Platform> platform = GetEnumReferenceByValue<RSG.Platform.Platform>(locator, PlatformUtils.PlatformFromString(resourceStat.Platform.ToString()));
            EnumReference<RSG.Platform.FileType> fileType = GetEnumReferenceByValue<RSG.Platform.FileType>(locator, resourceStat.FileType);

            // Create new resource stat
            RSG.Statistics.Domain.Entities.ResourceStats.ResourceStat resourceStatDomain = new RSG.Statistics.Domain.Entities.ResourceStats.ResourceStat();
            resourceStatDomain.ResourceName = resouceNameRepo.GetOrCreateByName(resourceStat.ResourceName);
            resourceStatDomain.SourceFilename = resourceStat.SourceFilename;
            resourceStatDomain.DestinationFilename = resourceStat.DestinationFilename;
            resourceStatDomain.Platform = platform;
            resourceStatDomain.FileType = fileType;
            resourceStatDomain.Timestamp = timeOfInsertion; // we can change this if it ever becomes an issue, the Timestamp is only really a ballpark figure for now.
            resourceStatDomain.Buckets = new List<RSG.Statistics.Domain.Entities.ResourceStats.ResourceBucket>();

            // Create domain objects for each bucket, and point them back to parent
            foreach (KeyValuePair<ResourceBucketType, ICollection<RSG.Model.Statistics.Platform.ResourceBucket>> kvp in resourceStat.Buckets)
            {
                ICollection<RSG.Model.Statistics.Platform.ResourceBucket> buckets = kvp.Value;
                EnumReference<ResourceBucketType> bucketType = GetEnumReferenceByValue<ResourceBucketType>(locator, kvp.Key);

                foreach (RSG.Model.Statistics.Platform.ResourceBucket bucket in buckets)
                {
                    RSG.Statistics.Domain.Entities.ResourceStats.ResourceBucket bucketDomain = new RSG.Statistics.Domain.Entities.ResourceStats.ResourceBucket();

                    bucketDomain.BucketType = bucketType;
                    bucketDomain.Capacity = bucket.Capacity;
                    bucketDomain.BucketId = bucket.ID;
                    bucketDomain.Size = bucket.Size;
                    bucketDomain.ResourceStat = resourceStatDomain;

                    resourceStatDomain.Buckets.Add(bucketDomain);
                }
            }

            locator.Save(resourceStatDomain);
        }
        #endregion // Private Methods
    } // ReportStatService
}
