﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Xml;
using RSG.Base.Logging;

namespace RSG.Statistics.Server.Extensions
{
    /// <summary>
    /// File based cache for datacontract serializable objects.
    /// </summary>
    /// <typeparam name="TKey">Key to use for looking up/inserting items into the cache.</typeparam>
    /// <typeparam name="TValue">Type of value that will be stored in the cache.</typeparam>
    public class BinaryFileCache<TKey>
    {
        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        private String m_directory;
        #endregion // Member Data

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public object this[TKey key]
        {
            get
            {
                if (!ContainsKey(key))
                {
                    throw new KeyNotFoundException(String.Format("Key '{0}' not found in cache located at '{1}'."));
                }
                return RetrieveItemFromCache(key);
            }
            set
            {
                AddItemToCache(key, value);
            }
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="directory"></param>
        public BinaryFileCache(String directory)
        {
            m_directory = directory;

            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// Checks whether a particular key exists in the cache.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool ContainsKey(TKey key)
        {
            return File.Exists(GetFilenameForKey(key));
        }

        /// <summary>
        /// Adds an item to the cache.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void Add(TKey key, object value)
        {
            if (ContainsKey(key))
            {
                throw new ArgumentException(String.Format("Key with value {0} already exists in the cache.", key));
            }

            AddItemToCache(key, value);
        }

        /// <summary>
        /// Removes a particular item from the cache.
        /// </summary>
        /// <param name="key"></param>
        public void Remove(TKey key)
        {
            File.Delete(GetFilenameForKey(key));
        }

        /// <summary>
        /// Attempts to retrieve the value associated with a particular.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value">Gets set to the cached value if it exists in the cache.</param>
        /// <returns>True if it exists in the cache, otherwise false.</returns>
        public bool TryGetValue(TKey key, out object value)
        {
            if (ContainsKey(key))
            {
                value = RetrieveItemFromCache(key);
                return true;
            }
            else
            {
                value = null;
                return false;
            }
        }

        /// <summary>
        /// Clears out all values that are stored in the cache.
        /// </summary>
        public void Clear()
        {
            foreach (String file in Directory.GetFiles(m_directory))
            {
                File.Delete(file);
            }
        }
        #endregion // Public Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        private String GetFilenameForKey(TKey key)
        {
            return Path.Combine(m_directory, key.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        private void AddItemToCache(TKey key, object value)
        {
            using (FileStream stream = new FileStream(GetFilenameForKey(key), FileMode.Create))
            {
                BinaryFormatter bFormatter = new BinaryFormatter();
                bFormatter.Serialize(stream, value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        private object RetrieveItemFromCache(TKey key)
        {
            try
            {
                using (FileStream stream = new FileStream(GetFilenameForKey(key), FileMode.Open))
                {
                    BinaryFormatter bFormatter = new BinaryFormatter();
                    object deserializedValue = bFormatter.Deserialize(stream);
                    return deserializedValue;
                }
            }
            catch (System.Exception ex)
            {
                Log.Log__Exception(ex, "Exception while deserialising item from report cache.");
                Remove(key);
                return null;
            }
        }
        #endregion // Private Methods
    } // ReportCache
}
