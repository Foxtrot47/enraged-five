﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using RSG.Statistics.Domain.Entities.GameAssets;

namespace RSG.Statistics.Server.Repository
{
    /// <summary>
    /// Custom repository for missions.
    /// </summary>
    internal interface IMissionRepository : IGameAssetRepository<Mission>
    {
        #region Methods
        /// <summary>
        /// Retrieves a mission by it's ID (e.g. ARM1, CAR3, etc...).
        /// </summary>
        Mission GetByMissionId(String missionId);

        /// <summary>
        /// Creates a hash to asset lookup for all assets based off of the alternative identifier.
        /// </summary>
        /// <returns></returns>
        IDictionary<uint, Mission> CreateAlternativeAssetLookup();

        /// <summary>
        /// Creates a hash to name lookup for all assets based off of the alternative identifier.
        /// </summary>
        /// <returns></returns>
        IDictionary<uint, String> CreateAlternativeAssetNameLookup();
        #endregion // Methods
    } // IMissionRepository
}
