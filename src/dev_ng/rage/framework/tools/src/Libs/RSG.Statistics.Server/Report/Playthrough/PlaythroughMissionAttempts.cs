﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using NHibernate.Transform;
using RSG.Platform;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Server.Repository;
using RSG.Statistics.Server.Extensions;
using RSG.Statistics.Domain.Entities.Playthrough;
using RSG.Statistics.Domain.Entities;
using RSG.Statistics.Domain.Entities.GameAssetStats;
using RSG.Statistics.Common.Report;
using NHibernate;
using PlayModel = RSG.Statistics.Common.Model.Playthrough;
using RSG.Base.Logging;
using RSG.Model.Common.Mission;
using NHibernate.SqlCommand;
using RSG.Statistics.Common.Config;

namespace RSG.Statistics.Server.Report.Playthrough
{
    /// <summary>
    /// Report that provides mission attempt summary information.
    /// </summary>
    [Export(typeof(IStatsReport))]
    internal class PlaythroughMissionAttempts : PlaythroughReportBase<List<PlayModel.MissionAttemptStatCategory>>
    {
        #region Constants
        /// <summary>
        /// Name of the report.
        /// </summary>
        private const String c_name = "PlaythroughMissionAttempts";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Optional user to restrict the results to.
        /// </summary>
        [ReportParameter]
        public List<String> Users { get; set; }

        /// <summary>
        /// Flag indicating whether we wish to exclude users instead of include them.
        /// </summary>
        [ReportParameter]
        public bool ExcludeUsers { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public PlaythroughMissionAttempts()
            : base(c_name)
        {
        }
        #endregion // Constructor(s)

        #region IStatsReport Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        /// <returns></returns>
        protected override List<PlayModel.MissionAttemptStatCategory> Generate(IRepositoryLocator locator, ILog log, IStatsServer server)
        {
            // To get the information we are after we need 4 different db queries.
            // 1) Retrieve grouped mission attempts.
            // 2) Retrieve all comments associated with the mission attempts.
            // 3) Retrieve grouped checkpoint attempts.
            // 4) Retrieve all comments associated with the checkpoint attempts.
            IList<PlayModel.MissionAttemptStat> attemptStats = GatherMissionAttemptInformation(locator);
            IList<PlayModel.CheckpointAttemptStat> checkpointResults = GatherCheckpointAttemptInformation(locator);

            // Add the checkpoint attempts to the appropriate mission attempt.
            Dictionary<long, PlayModel.MissionAttemptStat> missionAttemptLookup =
                attemptStats.ToDictionary(item => item.MissionId);
            foreach (PlayModel.CheckpointAttemptStat checkpointResult in checkpointResults)
            {
                if (missionAttemptLookup.ContainsKey(checkpointResult.MissionId))
                {
                    missionAttemptLookup[checkpointResult.MissionId].CheckpointAttemptStats.Add(checkpointResult);
                }
            }

            // Group the results.
            List<PlayModel.MissionAttemptStatCategory> categorisedStats = new List<PlayModel.MissionAttemptStatCategory>();
            foreach (IGrouping<String, PlayModel.MissionAttemptStat> group in attemptStats.GroupBy(item => item.MissionCategory))
            {
                PlayModel.MissionAttemptStatCategory categoryStat = new PlayModel.MissionAttemptStatCategory();
                categoryStat.CategoryName = group.Key;
                categoryStat.MissionAttemptStats.AddRange(group);
                categorisedStats.Add(categoryStat);
            }
            
            return categorisedStats;
        }
        #endregion // IStatsReport Implementation

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <returns></returns>
        private IList<PlayModel.MissionAttemptStat> GatherMissionAttemptInformation(IRepositoryLocator locator)
        {
            // ARGHH!!! NHibernate is being a twat again and doing the joins in the wrong order in the query that retrieves
            // the playthrough attempt information.  Ideally the Mission would be RIGHT OUTER JOINed after the PlaythroughSession
            // is INNER JOINed.  This would mean we get a result for all of the missions.
            // Instead NHibernate doesn't use the order that you specify in the criteria for doing the joins.  Instead it uses
            // the order that the properties that are being joined on appear in the hibernate mapping file.

            // So to get around this I first query for all the story missions and then fill in the details in a separate query.
            DetachedCriteria missionCriteria =
                DetachedCriteria.For<RSG.Statistics.Domain.Entities.GameAssets.Mission>()
                    .CreateAlias("Category", "c")
                    .SetProjection(Projections.ProjectionList()
                        .Add(Projections.Property("Id"), "MissionId")
                        .Add(Projections.Property("Name"), "MissionName")
                        .Add(Projections.Property("c.Name"), "MissionCategory")
                        .Add(Projections.Property("ProjectedAttempts"), "ProjectedAttempts")
                        .Add(Projections.Property("ProjectedAttemptsMin"), "ProjectedAttemptsMin")
                        .Add(Projections.Property("ProjectedAttemptsMax"), "ProjectedAttemptsMax"))
                    .Add(Restrictions.Eq("InBugstar", true))
                    .AddOrder(Order.Asc("Index"))
                    .SetResultTransformer(Transformers.AliasToBean(typeof(PlayModel.MissionAttemptStat)));
            IList<PlayModel.MissionAttemptStat> missionAttempts =
                locator.FindAll<PlaythroughMissionAttempt, PlayModel.MissionAttemptStat>(missionCriteria).ToList();
            
            // Create a mission attempt lookup for the following two queries.
            IDictionary<long, PlayModel.MissionAttemptStat> missionAttemptLookup = missionAttempts.ToDictionary(item => item.MissionId);

            // Create the criteria to retrieve the attempt information.
            DetachedCriteria attemptCriteria =
                DetachedCriteria.For<PlaythroughMissionAttempt>()
                    .CreateAlias("PlaythroughSession", "ps")
                    .CreateAlias("Mission", "m")
                    .SetProjection(Projections.ProjectionList()
                        .Add(Projections.Property("m.Id"), "MissionId")
                        .Add(Projections.CountDistinct("Id"), "NumberOfAttempts")
                        .Add(Projections.CountDistinct("ps.User"), "PlayersAttempted")
                        .Add(Projections.Min("TimeToComplete"), "MinMissionTime")
                        .Add(Projections.Avg("TimeToComplete"), "AvgMissionTime")
                        .Add(Projections.Max("TimeToComplete"), "MaxMissionTime")
                        .Add(Projections.GroupProperty("Mission")))
                    .Add(Restrictions.Eq("Ignore", false))
                    .SetResultTransformer(Transformers.AliasToBean(typeof(PlayModel.MissionAttemptStat)));
            AddRestrictions(attemptCriteria, locator, "ps");
            IList<PlayModel.MissionAttemptStat> attemptResults = locator.FindAll<PlaythroughMissionAttempt, PlayModel.MissionAttemptStat>(attemptCriteria);

            // Patch in the attempt/timing information.
            foreach (PlayModel.MissionAttemptStat attemptResult in attemptResults)
            {
                if (missionAttemptLookup.ContainsKey(attemptResult.MissionId))
                {
                    PlayModel.MissionAttemptStat mainAttempt = missionAttemptLookup[attemptResult.MissionId];
                    mainAttempt.NumberOfAttempts = attemptResult.NumberOfAttempts;
                    mainAttempt.PlayersAttempted = attemptResult.PlayersAttempted;
                    mainAttempt.MinMissionTime = attemptResult.MinMissionTime;
                    mainAttempt.AvgMissionTime = attemptResult.AvgMissionTime;
                    mainAttempt.MaxMissionTime = attemptResult.MaxMissionTime;
                }
            }

            // Create the criteria for retrieving the mission attempt comments.
            DetachedCriteria commentCriteria =
                DetachedCriteria.For<PlaythroughMissionAttempt>()
                    .CreateAlias("PlaythroughSession", "ps")
                    .CreateAlias("Mission", "m")
                    .CreateAlias("ps.User", "u")
                    .SetProjection(Projections.ProjectionList()
                        .Add(Projections.Property("m.Id"), "ParentId")
                        .Add(Projections.Property("u.Name"), "Username")
                        .Add(Projections.Property("Comment"), "Comment")
                        .Add(Projections.Property("Start"), "Timestamp"))
                    .Add(Restrictions.IsNotNull("Comment"))
                    .Add(Restrictions.Eq("Ignore", false))
                    .AddOrder(Order.Asc("u.Name"))
                    .SetResultTransformer(Transformers.AliasToBean(typeof(PlayModel.PlaythroughComment)));
            AddRestrictions(commentCriteria, locator, "ps");
            IList<PlayModel.PlaythroughComment> commentResults = locator.FindAll<PlaythroughMissionAttempt, PlayModel.PlaythroughComment>(commentCriteria);

            // Add the comments to the appropriate mission details.
            foreach (PlayModel.PlaythroughComment commentResult in commentResults)
            {
                if (missionAttemptLookup.ContainsKey(commentResult.ParentId))
                {
                    missionAttemptLookup[commentResult.ParentId].Comments.Add(commentResult);
                }
            }

            return missionAttempts;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <returns></returns>
        private IList<PlayModel.CheckpointAttemptStat> GatherCheckpointAttemptInformation(IRepositoryLocator locator)
        {
            // ARGHH!!! NHibernate is being a twat again and doing the joins in the wrong order in the query that retrieves
            // the playthrough attempt information.  Ideally the Mission would be RIGHT OUTER JOINed after the PlaythroughSession
            // is INNER JOINed.  This would mean we get a result for all of the missions.
            // Instead NHibernate doesn't use the order that you specify in the criteria for doing the joins.  Instead it uses
            // the order that the properties that are being joined on appear in the hibernate mapping file.

            // So to get around this I first query for all the story missions and then fill in the details in a separate query.
            DetachedCriteria checkpointCriteria =
                DetachedCriteria.For<RSG.Statistics.Domain.Entities.GameAssets.MissionCheckpoint>()
                    .CreateAlias("Mission", "m")
                    .SetProjection(Projections.ProjectionList()
                        .Add(Projections.Property("m.Id"), "MissionId")
                        .Add(Projections.Property("Id"), "CheckpointId")
                        .Add(Projections.Property("Name"), "CheckpointName")
                        .Add(Projections.Property("Index"), "CheckpointIndex")
                        .Add(Projections.Property("ProjectedAttempts"), "ProjectedAttempts")
                        .Add(Projections.Property("ProjectedAttemptsMin"), "ProjectedAttemptsMin")
                        .Add(Projections.Property("ProjectedAttemptsMax"), "ProjectedAttemptsMax"))
                    .Add(Restrictions.Eq("InBugstar", true))
                    .AddOrder(Order.Asc("Index"))
                    .SetResultTransformer(Transformers.AliasToBean(typeof(PlayModel.CheckpointAttemptStat)));
            IList<PlayModel.CheckpointAttemptStat> checkpointAttempts =
                locator.FindAll<PlaythroughMissionAttempt, PlayModel.CheckpointAttemptStat>(checkpointCriteria).ToList();

            // Create a checkpoint attempt lookup for the following two queries.
            IDictionary<long, PlayModel.CheckpointAttemptStat> checkpointAttemptLookup = checkpointAttempts.ToDictionary(item => item.CheckpointId);

            // Create the criteria to get the checkpoint attempt info.
            DetachedCriteria attemptCriteria =
                DetachedCriteria.For<PlaythroughCheckpointAttempt>()
                    .CreateAlias("PlaythroughMissionAttempt", "ma")
                    .CreateAlias("ma.PlaythroughSession", "ps")
                    .CreateAlias("Checkpoint", "c")
                    .SetProjection(Projections.ProjectionList()
                        .Add(Projections.Property("c.Id"), "CheckpointId")
                        .Add(Projections.CountDistinct("ps.User"), "PlayersAttempted")
                        .Add(Projections.Count("Id"), "NumberOfAttempts")
                        .Add(Projections.Min("TimeToComplete"), "MinCheckpointTime")
                        .Add(Projections.Avg("TimeToComplete"), "AvgCheckpointTime")
                        .Add(Projections.Max("TimeToComplete"), "MaxCheckpointTime")
                        .Add(Projections.GroupProperty("Checkpoint")))
                    .Add(Restrictions.Eq("Ignore", false))
                    .Add(Restrictions.Eq("ma.Ignore", false))
                    .SetResultTransformer(Transformers.AliasToBean(typeof(PlayModel.CheckpointAttemptStat)));
            AddRestrictions(attemptCriteria, locator, "ps");

            // Add the summary info to the checkpoint attempt info we already retrieved.
            IList<PlayModel.CheckpointAttemptStat> checkpointInfos = locator.FindAll<PlaythroughCheckpointAttempt, PlayModel.CheckpointAttemptStat>(attemptCriteria);
            foreach (PlayModel.CheckpointAttemptStat checkpointInfo in checkpointInfos)
            {
                if (checkpointAttemptLookup.ContainsKey(checkpointInfo.CheckpointId))
                {
                    PlayModel.CheckpointAttemptStat mainAttempt = checkpointAttemptLookup[checkpointInfo.CheckpointId];
                    mainAttempt.PlayersAttempted = checkpointInfo.PlayersAttempted;
                    mainAttempt.NumberOfAttempts = checkpointInfo.NumberOfAttempts;
                    mainAttempt.MinCheckpointTime = checkpointInfo.MinCheckpointTime;
                    mainAttempt.AvgCheckpointTime = checkpointInfo.AvgCheckpointTime;
                    mainAttempt.MaxCheckpointTime = checkpointInfo.MaxCheckpointTime;
                }
            }

            // Get the checkpoint attempt results and associate them with the mission attempt model objects we retrieved above.

            // Create the criteria for retrieving the checkpoint attempt comments.
            DetachedCriteria commentCriteria =
                DetachedCriteria.For<PlaythroughCheckpointAttempt>()
                    .CreateAlias("PlaythroughMissionAttempt", "ma")
                    .CreateAlias("ma.PlaythroughSession", "ps")
                    .CreateAlias("ps.User", "u")
                    .CreateAlias("Checkpoint", "c")
                    .SetProjection(Projections.ProjectionList()
                        .Add(Projections.Property("c.Id"), "ParentId")
                        .Add(Projections.Property("u.Name"), "Username")
                        .Add(Projections.Property("Comment"), "Comment")
                        .Add(Projections.Property("Start"), "Timestamp")
                        .Add(Projections.Property("TimeToComplete"), "Duration"))
                    .Add(Restrictions.IsNotNull("Comment"))
                    .Add(Restrictions.Eq("Ignore", false))
                    .Add(Restrictions.Eq("ma.Ignore", false))
                    .AddOrder(Order.Asc("u.Name"))
                    .SetResultTransformer(Transformers.AliasToBean(typeof(PlayModel.PlaythroughComment)));
            AddRestrictions(commentCriteria, locator, "ps");

            // Add the comments the checkpoint attempt info we already retrieved.
            IList<PlayModel.PlaythroughComment> commentResults = locator.FindAll<PlaythroughCheckpointAttempt, PlayModel.PlaythroughComment>(commentCriteria);
            foreach (PlayModel.PlaythroughComment commentResult in commentResults)
            {
                if (checkpointAttemptLookup.ContainsKey(commentResult.ParentId))
                {
                    checkpointAttemptLookup[commentResult.ParentId].Comments.Add(commentResult);
                }
            }

            return checkpointAttempts;
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void AddRestrictions(DetachedCriteria criteria, IRepositoryLocator locator, String sessionAlias = "")
        {
            base.AddRestrictions(criteria, locator, sessionAlias);

            String alias = sessionAlias;
            if (sessionAlias != "")
            {
                alias = sessionAlias + ".";
            }

            if (Users.Any())
            {
                List<PlaythroughUser> users = new List<PlaythroughUser>();
                foreach (String userName in Users)
                {
                    DetachedCriteria userCriteria = DetachedCriteria.For<PlaythroughUser>().Add(Expression.Eq("Name", userName));
                    PlaythroughUser user = locator.FindFirst<PlaythroughUser>(userCriteria);
                    if (user == null)
                    {
                        throw new ArgumentException("Playthrough user doesn't exist in the database.");
                    }
                    users.Add(user);
                }

                if (ExcludeUsers)
                {
                    criteria.Add(Restrictions.Not(Restrictions.In(alias + "User", users)));
                }
                else
                {
                    criteria.Add(Restrictions.In(alias + "User", users));
                }
            }
        }
        #endregion // Private Methods
    } // PlaythroughMissionAttempts
}
