﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Server.Repository;
using RSG.Statistics.Common.Dto;

namespace RSG.Statistics.Server.TransManager
{
    public abstract class TransManagerBase
        : ITransManager
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        protected bool IsInTranx
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public IRepositoryLocator Locator
        {
            get;
            set;
        }
        #endregion // Properties

        #region ITransManager Members
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="command"></param>
        /// <returns></returns>
        public T ExecuteCommand<T>(Func<Repository.IRepositoryLocator, T> command) where T : class, IDto
        {
            try
            {
                BeginTransaction();
                T result = command.Invoke(Locator);
                CommitTransaction();
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual void BeginTransaction()
        {
            IsInTranx = true;
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual void CommitTransaction()
        {
            IsInTranx = false;
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual void Rollback()
        {
            IsInTranx = false;
        }
        #endregion

        #region IDisposable Implementation
        /// <summary>
        /// 
        /// </summary>
        protected bool IsDisposed
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposing)
            {
                return;
            }

            // free managed resources
            if (!IsDisposed && IsInTranx)
            {
                Rollback();
            }
            Locator = null;
            IsDisposed = true;
        }
        #endregion // IDisposable Implementation
    } // TransManagerBase
}
