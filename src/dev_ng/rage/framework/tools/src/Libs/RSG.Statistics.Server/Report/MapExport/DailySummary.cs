﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Report;
using RSG.Statistics.Domain.Entities.ExportStats;
using RSG.Statistics.Server.Repository;
using ExpoModel = RSG.Statistics.Common.Model.MapExport;

namespace RSG.Statistics.Server.Report.MapExport
{
    /// <summary>
    /// Report that provides daily export summary information.
    /// </summary>
    [Export(typeof(IStatsReport))]
    internal class DailySummary : MapExportReportBase<ExpoModel.DailySummary>
    {
        #region Constants
        /// <summary>
        /// Name of the report.
        /// </summary>
        private const String c_name = "MapExportsDailySummary";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// The date to get the summary information for.
        /// </summary>
        [ReportParameter(Required = true)]
        public DateTime Date { get; set; }

        /// <summary>
        /// Hour offset to use when running the report (used to get san diego's exports included in the results).
        /// </summary>
        [ReportParameter]
        public uint HourOffset { get; set; }
        #endregion // Properties

        #region Private Classes
        /// <summary>
        /// Temp class for nhibernate query results.
        /// </summary>
        private class BuildTimeResult
        {
            public long ExportId { get; set; }
            public ulong BuildTime { get; set; }
        }

        /// <summary>
        /// Temp class for nhibernate query results.
        /// </summary>
        private class BuildSectionResult
        {
            public long ExportId { get; set; }
            public String SectionName { get; set; }
        }
        #endregion // Private Classes

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public DailySummary()
            : base(c_name)
        {
        }
        #endregion // Constructor(s)

        #region IReportDataProvider Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        /// <returns></returns>
        protected override ExpoModel.DailySummary Generate(IRepositoryLocator locator, ILog log, IStatsServer server)
        {
            ExpoModel.DailySummary summary = new ExpoModel.DailySummary();
            DateTime startDate = Date.Date.AddHours(HourOffset);
            DateTime endDate = Date.Date.AddDays(1).AddHours(HourOffset);

            // We report on 3 different things.
            // 1) Users
            // 2) Sections
            // 3) Individual Exports
            summary.UserStats.AddRange(GetUserSummaryInfo(locator, startDate, endDate));
            summary.SectionStats.AddRange(GetSectionSummaryInfo(locator, startDate, endDate));
            summary.ExportStats.AddRange(GetExportDetails(locator, startDate, endDate));
            return summary;
        }
        #endregion // IReportDataProvider Implementation

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private IList<ExpoModel.UserSummary> GetUserSummaryInfo(IRepositoryLocator locator, DateTime startDate, DateTime endDate)
        {
            IProjection exportTimeProj =
                Projections.SqlFunction("TIMESTAMPDIFF", NHibernateUtil.Int32, Projections.Property("Start"), Projections.Property("End"));

            DetachedCriteria usersCriteria =
                DetachedCriteria.For<MapExportStat>()
                                .SetProjection(Projections.ProjectionList()
                                    .Add(Projections.Property("Username"), "Username")
                                    .Add(Projections.Count("Id"), "TotalCount")
                                    .Add(Projections.Sum(Projections.Cast(NHibernateUtil.Int32, Projections.Property("Success"))), "SuccessfulCount")
                                    .Add(Projections.Sum(exportTimeProj), "TotalTime")
                                    .Add(Projections.Max(exportTimeProj), "MaxTime")
                                    .Add(Projections.GroupProperty("Username")))
                                .Add(Restrictions.IsNotNull("Start"))
                                .Add(Restrictions.Between("Start", startDate, endDate))
                                .SetResultTransformer(Transformers.AliasToBean(typeof(ExpoModel.UserSummary)));
            return locator.FindAll<MapExportStat, ExpoModel.UserSummary>(usersCriteria);
        }
        #endregion // Private Methods
    } // DailySummary
}
