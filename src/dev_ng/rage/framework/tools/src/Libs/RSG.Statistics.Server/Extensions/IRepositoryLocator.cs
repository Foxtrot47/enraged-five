﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Server.Repository;
using NHibernate.Criterion;
using RSG.Statistics.Domain.Entities;
using System.Net;
using System.ServiceModel.Web;
using System.Diagnostics;

namespace RSG.Statistics.Server.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    public static class IRepositoryLocatorExtensions
    {
        /// <summary>
        /// Enums don't change while the application is running, so this is an optimisation for storing
        /// enum values -> db id's for quicker lookup.
        /// </summary>
        public static IDictionary<Type, IDictionary<object, long>> s_enumLookup
            = new Dictionary<Type, IDictionary<object, long>>();

        /// <summary>
        /// Assets won't change while the application is running, so this is an optimisation for storing
        /// asset identifiers -> db id's for quicker lookup.
        /// </summary>
        public static IDictionary<Type, IDictionary<String, long>> s_assetLookup
            = new Dictionary<Type, IDictionary<String, long>>();

        /// <summary>
        /// Gets an asset using its name ( which is converted to it's identifier)
        /// </summary>
        /// <typeparam name="TDomain"></typeparam>
        /// <param name="locator"></param>
        /// <param name="name"></param>
        /// <param name="throwIfNull"></param>
        /// <returns></returns>
        public static TDomain GetAssetByName<TDomain>(this IRepositoryLocator locator, String name, bool throwIfNull = true) where TDomain : class, IDomainEntity
        {
            string identifier = RSG.ManagedRage.StringHashUtil.atStringHash(name, 0).ToString();
            return GetAssetByIdentifier<TDomain>(locator, identifier, throwIfNull);
        }

        /// <summary>
        /// Gets an asset using its identifier
        /// </summary>
        /// <param name="values"></param>
        /// <returns></returns>
        public static TDomain GetAssetByIdentifier<TDomain>(this IRepositoryLocator locator, String identifier, bool throwIfNull = true) where TDomain : class, IDomainEntity
        {
            if (identifier == null)
            {
                if (throwIfNull)
                {
                    throw new ArgumentNullException("identifier");
                }

                return default(TDomain);
            }

            Type assetType = typeof(TDomain);

            if (!s_assetLookup.ContainsKey(assetType))
            {
                s_assetLookup[assetType] = new Dictionary<String, long>();
            }

            if (!s_assetLookup[assetType].ContainsKey(identifier))
            {
                DetachedCriteria query = DetachedCriteria.For<TDomain>().Add(Expression.Eq("Identifier", identifier));
                TDomain asset = locator.FindFirst<TDomain>(query);
                
                if (asset != null)
                {
                    s_assetLookup[assetType][identifier] = asset.Id;
                }
                else if (throwIfNull)
                {
                    throw new WebFaultException<String>(String.Format("Asset of type '{0}' with identifier '{1}' doesn't exist in the database.", typeof(TDomain).Name, identifier), HttpStatusCode.BadRequest);
                }
                return asset;
            }
            else
            {
                return locator.GetById<TDomain>(s_assetLookup[assetType][identifier]);
            }
        }

        /// <summary>
        /// Helper method for getting an db enum reference from it's corresponding enum value.
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="locator"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static EnumReference<TEnum> GetEnumReferenceByValue<TEnum>(this IRepositoryLocator locator, TEnum value, bool throwIfNull = true) where TEnum : struct
        {
            Type enumType = typeof(TEnum);

            if (!s_enumLookup.ContainsKey(enumType))
            {
                s_enumLookup[enumType] = new Dictionary<object, long>();
            }

            if (!s_enumLookup[enumType].ContainsKey(value))
            {
                DetachedCriteria query = DetachedCriteria.For<EnumReference<TEnum>>().Add(Expression.Eq("Name", value.ToString()));
                EnumReference<TEnum> asset = locator.FindFirst<EnumReference<TEnum>>(query);

                if (asset != null)
                {
                    s_enumLookup[enumType][value] = asset.Id;
                }
                else if (throwIfNull)
                {
                    Debug.Assert(asset != null, "Requesting enum value that doesn't exist in the db.  Type: {0}, Value: {1}.", enumType.FullName, value);
                    throw new WebFaultException<String>(String.Format("EnumReference of type '{0}' with value '{1}' doesn't exist in the database.", typeof(TEnum).Name, value), HttpStatusCode.BadRequest);
                }

                return asset;
            }
            else
            {
                return locator.GetById<EnumReference<TEnum>>(s_enumLookup[enumType][value]);
            }
        }
    } // IRepositoryLocatorExtensions
}
