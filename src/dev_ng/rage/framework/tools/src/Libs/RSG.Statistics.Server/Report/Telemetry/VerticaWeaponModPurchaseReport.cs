﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Security.Cryptography;
using RSG.Statistics.Client.Vertica.Client;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Model.Telemetry;
using RSG.Statistics.Common.Report;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Dto.ReportResults;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Server.Repository;

namespace RSG.Statistics.Server.Report.Telemetry
{
    /// <summary>
    /// Report that provides weapon mod purchase information.
    /// </summary>
    [Export(typeof(IStatsReport))]
    internal class VerticaWeaponModPurchaseReport : VerticaTelemetryReportBase<List<WeaponModPurchaseStat>>
    {
        #region Constants
        /// <summary>
        /// Name of the report.
        /// </summary>
        private const String c_name = "VerticaWeaponModPurchases";

        /// <summary>
        /// Weapon mod config file
        /// </summary>
        private const String c_weaponModConfigFile = @"$(toolsconfig)\config\statistics\weaponmods.xml";
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public VerticaWeaponModPurchaseReport()
            : base(c_name)
        {
        }
        #endregion // Constructor(s)

        #region StatsReportBase Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        /// <returns></returns>
        protected override List<WeaponModPurchaseStat> Generate(IRepositoryLocator locator, ILog log, IStatsServer server)
        {
            // Parse the weapon mod file to get the list of weapon mods that can be applied to which weapon.
            List<WeaponModPurchaseStat> results = ParseWeaponModConfigFile(locator, server.Config.Branch);

            // Convert the parameters to something vertica can work with.
            WeaponModPurchaseParams queryParams = new WeaponModPurchaseParams();
            queryParams.WeaponModHashes = results.SelectMany(item => item.WeaponMods).Select(item => item.WeaponModHash).Distinct().ToList();
            SetDefaultParams(locator, queryParams);

            // Contact vertica for the report data.
            List<WeaponModPurchaseResult> intermediateResults;
            using (ReportClient client = new ReportClient(server.VerticaServer))
            {
                intermediateResults = (List<WeaponModPurchaseResult>)client.RunReport(ReportNames.WeaponModPurchaseReport, queryParams);
            }

            // Populate the result object with the data from Vertica.
            foreach (WeaponModPurchaseResult interResult in intermediateResults)
            {
                WeaponModPurchaseStat weaponStat = results.FirstOrDefault(item => item.WeaponHash == interResult.WeaponHash);
                if (weaponStat != null)
                {
                    WeaponModPurchaseSubStat weaponModStat = weaponStat.WeaponMods.FirstOrDefault(item => item.WeaponModHash == interResult.WeaponModHash);
                    if (weaponModStat != null)
                    {
                        weaponModStat.TimesPurchased = interResult.TimesPurchased;
                        weaponModStat.TotalSpent = interResult.TotalSpent;
                        weaponModStat.UniqueGamers = interResult.UniqueGamers;
                    }
                }
            }

            return results;
        }
        #endregion // StatsReportBase Implementation

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <returns></returns>
        private List<WeaponModPurchaseStat> ParseWeaponModConfigFile(IRepositoryLocator locator, IBranch branch)
        {
            // Parse the file for the appropriate information.
            String filename = branch.Environment.Subst(c_weaponModConfigFile);
            if (!File.Exists(filename))
            {
                throw new FileNotFoundException("Unable to run report as the file containing the weapon mod information doesn't exist on the machine.", filename);
            }

            // Create a weapon name lookup for the weapon names.
            IList<Weapon> allWeapons = locator.CreateCriteria<Weapon>().List<Weapon>();
            IDictionary<uint, String> weaponNameLookup =
                allWeapons.ToDictionary(item => UInt32.Parse(item.Identifier), item => item.FriendlyName);

            // Create a shop item lookup for the weapon mod names.
            IGameAssetRepository<ShopItem> shopItemRepo = (IGameAssetRepository<ShopItem>)locator.GetRepository<ShopItem>();
            IDictionary<uint, String> shopItemLookup = shopItemRepo.CreateAssetNameLookup();
            
            List<WeaponModPurchaseStat> stats = new List<WeaponModPurchaseStat>();

            // Parse the document.
            XDocument xDoc = XDocument.Load(filename);
            foreach (XElement weaponElem in xDoc.XPathSelectElements("/weaponmods/weapon"))
            {
                XAttribute hashAtt = weaponElem.Attribute("hash");
                if (hashAtt != null)
                {
                    String weaponName = hashAtt.Value;
                    uint weaponHash = OneAtATime.ComputeHash(weaponName);
                    String weaponFriendlyName = null;
                    if (weaponNameLookup.ContainsKey(weaponHash))
                    {
                        weaponFriendlyName = weaponNameLookup[weaponHash];
                    }

                    WeaponModPurchaseStat stat = new WeaponModPurchaseStat
                        {
                            WeaponName = weaponName,
                            WeaponHash = weaponHash,
                            WeaponFriendlyName = weaponFriendlyName
                        };

                    foreach (XElement modElem in weaponElem.Elements("weaponmod"))
                    {
                        XAttribute modHashAtt = modElem.Attribute("hash");
                        if (modHashAtt != null)
                        {
                            String weaponModName = modHashAtt.Value;
                            uint weaponModHash = OneAtATime.ComputeHash(weaponModName);
                            String weaponModFriendlyName = null;
                            if (shopItemLookup.ContainsKey(weaponModHash))
                            {
                                weaponModFriendlyName = shopItemLookup[weaponModHash];
                            }

                            stat.WeaponMods.Add(new WeaponModPurchaseSubStat
                                {
                                    WeaponModName = weaponModName,
                                    WeaponModHash = weaponModHash,
                                    WeaponModFriendlyName = weaponModFriendlyName
                                });
                        }
                    }

                    if (stat.WeaponMods.Any())
                    {
                        stats.Add(stat);
                    }
                }
            }

            return stats;
        }
        #endregion // Private Methods
    } // VerticaWeaponModPurchaseReport
}
