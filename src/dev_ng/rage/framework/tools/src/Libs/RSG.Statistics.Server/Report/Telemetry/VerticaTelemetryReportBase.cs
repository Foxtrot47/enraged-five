﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Extensions;
using RSG.ROS;
using RSG.Statistics.Common;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Server.Report.Util;
using RSG.Statistics.Server.Repository;

namespace RSG.Statistics.Server.Report.Telemetry
{
    /// <summary>
    /// 
    /// </summary>
    internal abstract class VerticaTelemetryReportBase<TResult> : VerticaReportBase<TResult>
    {
        #region Properties
        /// <summary>
        /// Names of the gamer handles we wish to see the data for.
        /// </summary>
        [ReportParameter]
        public List<Tuple<String, String>> GamerHandlePlatformPairs { get; set; }

        /// <summary>
        /// List of gamer handle groups for which we wish to see data.
        /// </summary>
        [ReportParameter]
        public List<String> GamerGroups { get; set; }

        /// <summary>
        /// Names of the platforms we wish to see the data for.
        /// </summary>
        [ReportParameter]
        public List<String> PlatformNames { get; set; }

        /// <summary>
        /// Convenience property that transforms the platform names to platform enum values.
        /// </summary>
        public IList<RSG.Platform.Platform> Platforms
        {
            get
            {
                IList<RSG.Platform.Platform> platforms = new List<RSG.Platform.Platform>();
                if (PlatformNames != null)
                {
                    foreach (String platformName in PlatformNames)
                    {
                        RSG.Platform.Platform platform;
                        if (Enum.TryParse(platformName, out platform))
                        {
                            platforms.Add(platform);
                        }
                    }
                }
                return platforms;
            }
        }

        /// <summary>
        /// Optional start date.
        /// </summary>
        [ReportParameter]
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Optional end date.
        /// </summary>
        [ReportParameter("ValidateEndDate", typeof(Validation))]
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Optional lower age boundary.
        /// </summary>
        [ReportParameter]
        public int? AgeMin { get; set; }

        /// <summary>
        /// Optional upper age boundary.
        /// </summary>
        [ReportParameter]
        public int? AgeMax { get; set; }

        /// <summary>
        /// Optional country codes where the user is registered.
        /// </summary>
        [ReportParameter]
        public List<String> CountryCodes { get; set; }

        /// <summary>
        /// Optional list of social club actors to restrict the results to.
        /// </summary>
        [ReportParameter]
        public List<String> SocialClubUserNames { get; set; }

        /// <summary>
        /// Optional list of social club groups to restrict the results to.
        /// </summary>
        [ReportParameter]
        public List<String> SocialClubUserGroups { get; set; }

        /// <summary>
        /// Names of the game types we wish to filter the data by.
        /// </summary>
        [ReportParameter]
        public List<String> GameTypeNames { get; set; }

        /// <summary>
        /// Convenience property that transforms the game type names to game type enum values.
        /// </summary>
        public IList<GameType> GameTypes
        {
            get
            {
                IList<GameType> gameTypes = new List<GameType>();
                if (GameTypeNames != null)
                {
                    foreach (String gameTypeName in GameTypeNames)
                    {
                        GameType gameType;
                        if (Enum.TryParse(gameTypeName, out gameType))
                        {
                            gameTypes.Add(gameType);
                        }
                    }
                }
                return gameTypes;
            }
        }

        /// <summary>
        /// Optional build to restrict the results to.
        /// </summary>
        [ReportParameter]
        public List<String> BuildIdentifiers { get; set; }

        /// <summary>
        /// Optional companywide playtest to restrict the results to.
        /// </summary>
        [ReportParameter]
        public uint? CompanywidePlaytestId { get; set; }

        /// <summary>
        /// Optional character slot to restrict the results to.
        /// </summary>
        [ReportParameter]
        public uint? CharacterSlot { get; set; }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public VerticaTelemetryReportBase(String name)
            : base(name)
        {
        }
        #endregion // Constructor(s)

        #region Protected Methods
        /// <summary>
        /// Sets parameters common to all telemetry based reports.
        /// </summary>
        protected void SetDefaultParams(IRepositoryLocator locator, TelemetryParams parameters)
        {
            parameters.StartDate = this.StartDate;
            parameters.EndDate = this.EndDate;
            parameters.AgeMin = this.AgeMin;
            parameters.AgeMax = this.AgeMax;
            parameters.CountryCodes = (this.CountryCodes != null ? this.CountryCodes.Distinct().ToList() : new List<String>());
            parameters.Builds = (BuildIdentifiers != null ? BuildIdentifiers.Select(item => ConvertBuildIdentifierToVersion(item)).ToList() : new List<uint>());
            parameters.CompanywidePlaytestId = this.CompanywidePlaytestId;
            parameters.CharacterSlot = this.CharacterSlot;

            ISet<String> scUserNames = new HashSet<String>();
            if (SocialClubUserNames != null && SocialClubUserNames.Any())
            {
                scUserNames.AddRange(SocialClubUserNames);
            }
            if (SocialClubUserGroups != null && SocialClubUserGroups.Any())
            {
                scUserNames.AddRange(ConvertUserGroups(locator, SocialClubUserGroups));
            }
            parameters.SocialClubUsernames = scUserNames.ToList();

            ISet<Tuple<String, ROSPlatform>> gamerTuples = new HashSet<Tuple<String, ROSPlatform>>();
            if (GamerHandlePlatformPairs != null && GamerHandlePlatformPairs.Any())
            {
                gamerTuples.AddRange(ConvertGamerPlatformTuples(GamerHandlePlatformPairs));
            }
            if (GamerGroups != null && GamerGroups.Any())
            {
                gamerTuples.AddRange(ConvertGamerGroups(locator, GamerGroups));
            }
            parameters.GamerHandlePlatformPairs = gamerTuples.ToList();

            if (Platforms.Any())
            {
                ISet<ROSPlatform> rosPlatforms = new HashSet<ROSPlatform>();
                foreach (RSG.Platform.Platform platform in Platforms)
                {
                    ROSPlatform rosPlatform;
                    if (TryConvertPlatformToROSPlatform(platform, out rosPlatform))
                    {
                        rosPlatforms.Add(rosPlatform);
                    }
                }
                parameters.Platforms = rosPlatforms.ToList();
            }
            else
            {
                parameters.Platforms = ROSPlatformUtils.GetValidPlatforms().ToList();
            }

            if (GameTypes.Count == 1)
            {
                parameters.GameTypeRestriction = GameTypes.First();
            }
        }
        #endregion // Protected Methods
    } // VerticaTelemetryReportBase
}
