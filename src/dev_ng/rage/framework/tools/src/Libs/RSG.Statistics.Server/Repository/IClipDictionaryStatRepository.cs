﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Domain.Entities.GameAssetStats.Animation;

namespace RSG.Statistics.Server.Repository
{
    /// <summary>
    /// Clip dictionary stat repository.
    /// </summary>
    internal interface IClipDictionaryStatRepository : IGameAssetStatRepository<ClipDictionaryStat, ClipDictionary>
    {
        #region Methods
        /// <summary>
        /// Returns all clip dictionary stats associated with the specified build optionally retrieving clips and anim data.
        /// </summary>
        IEnumerable<ClipDictionaryStat> GetStatsForBuild(Build build, bool retrieveClips, bool retrieveAnims);
        #endregion // Methods
    } // IClipDictionaryStatRepository
}
