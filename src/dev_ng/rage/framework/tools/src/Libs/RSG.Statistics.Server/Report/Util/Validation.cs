﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Server.Report.Util
{
    /// <summary>
    /// Common report parameter validation methods.
    /// </summary>
    internal static class Validation
    {
        /// <summary>
        /// Clamps the end date so that it is the current time.
        /// </summary>
        public static object ValidateEndDate(object obj)
        {
            DateTime? endDate = (DateTime?)obj;
            if (endDate == null || endDate > DateTime.Now)
            {
                endDate = DateTime.UtcNow;
            }
            return endDate;
        }
    } // Validation
}
