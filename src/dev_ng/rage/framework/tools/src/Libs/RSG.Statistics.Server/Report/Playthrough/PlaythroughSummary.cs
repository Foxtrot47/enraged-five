﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Domain.Entities.Playthrough;
using RSG.Statistics.Server.Repository;
using RSG.Statistics.Server.Extensions;
using RSG.Platform;
using RSG.Statistics.Domain.Entities;
using RSG.Statistics.Domain.Entities.GameAssetStats;
using RSG.Statistics.Common.Report;
using RSG.Base.Logging;
using PlayModel = RSG.Statistics.Common.Model.Playthrough;
using NHibernate.Dialect.Function;
using RSG.Statistics.Common.Config;

namespace RSG.Statistics.Server.Report.Playthrough
{
    /// <summary>
    /// Report that provides summary information about the playthroughs in the db.
    /// </summary>
    [Export(typeof(IStatsReport))]
    internal class PlaythroughSummary : PlaythroughReportBase<List<PlayModel.UserSessionSummary>>
    {
        #region Constants
        /// <summary>
        /// Name of the report.
        /// </summary>
        private const String c_name = "PlaythroughSummary";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Optional user to restrict the results to.
        /// </summary>
        [ReportParameter]
        public List<String> Users { get; set; }

        /// <summary>
        /// Flag indicating whether we wish to exclude users instead of include them.
        /// </summary>
        [ReportParameter]
        public bool ExcludeUsers { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public PlaythroughSummary()
            : base(c_name)
        {
        }
        #endregion // Constructor(s)

        #region IStatsReport Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        protected override List<PlayModel.UserSessionSummary> Generate(IRepositoryLocator locator, ILog log, IStatsServer server)
        {
            ISQLFunction sqlDiv = new VarArgsSQLFunction("(", "/", ")");
            ISQLFunction sqlMul = new VarArgsSQLFunction("(", "*", ")");

            DetachedCriteria criteria =
                DetachedCriteria.For<PlaythroughCheckpointAttempt>()
                    .CreateAlias("PlaythroughMissionAttempt", "ma")
                    .CreateAlias("ma.PlaythroughSession", "ps")
                    .CreateAlias("ps.User", "pu")
                    .SetProjection(Projections.ProjectionList()
                        .Add(Projections.Property("pu.Name"), "UserName")
                        .Add(Projections.CountDistinct("ma.PlaythroughSession"), "SessionCount")
                        .Add(Projections.SqlFunction(sqlDiv, NHibernateUtil.Int32,
                            Projections.SqlFunction(sqlMul, NHibernateUtil.Int32,
                                Projections.Sum("ps.TimeSpentPlaying"),
                                Projections.CountDistinct("ps.Id")),
                            Projections.Count("Id")), "TimeSpentPlaying")
                        .Add(Projections.SqlFunction(sqlDiv, NHibernateUtil.Int32,
                            Projections.SqlFunction(sqlMul, NHibernateUtil.Int32,
                                Projections.Count("ma.Id"),
                                Projections.CountDistinct("ma.Id")),
                            Projections.Count("Id")), "TotalMissionAttempts")
                        .Add(Projections.CountDistinct("PlaythroughMissionAttempt"), "UniqueMissionAttempts")
                        .Add(Projections.Count("Checkpoint"), "TotalCheckpointAttempts")
                        .Add(Projections.CountDistinct("Checkpoint"), "UniqueCheckpointAttempts")
                        .Add(Projections.GroupProperty("ps.User")))
                    .AddOrder(Order.Asc("pu.Name"))
                    .SetResultTransformer(Transformers.AliasToBean(typeof(PlayModel.UserSessionSummary)));
            AddRestrictions(criteria, locator, "ps");

            IEnumerable<PlayModel.UserSessionSummary> summaryData =
                locator.FindAll<PlaythroughCheckpointAttempt, PlayModel.UserSessionSummary>(criteria);
            return summaryData.ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void AddRestrictions(DetachedCriteria criteria, IRepositoryLocator locator, String sessionAlias = "")
        {
            base.AddRestrictions(criteria, locator, sessionAlias);

            String alias = sessionAlias;
            if (sessionAlias != "")
            {
                alias = sessionAlias + ".";
            }

            if (Users.Any())
            {
                List<PlaythroughUser> users = new List<PlaythroughUser>();
                foreach (String userName in Users)
                {
                    DetachedCriteria userCriteria = DetachedCriteria.For<PlaythroughUser>().Add(Expression.Eq("Name", userName));
                    PlaythroughUser user = locator.FindFirst<PlaythroughUser>(userCriteria);
                    if (user == null)
                    {
                        throw new ArgumentException("Playthrough user doesn't exist in the database.");
                    }
                    users.Add(user);
                }

                if (ExcludeUsers)
                {
                    criteria.Add(Restrictions.Not(Restrictions.In(alias + "User", users)));
                }
                else
                {
                    criteria.Add(Restrictions.In(alias + "User", users));
                }
            }
        }
        #endregion // IStatsReport Implementation
    } // PlaythroughSummary
}
