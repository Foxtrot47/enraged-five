﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Reflection;
using System.ServiceModel;
using System.ServiceModel.Web;
using AutoMapper;
using GisSharpBlog.NetTopologySuite.Geometries;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.SqlCommand;
using NHibernate.Transform;
using RSG.Base.Configuration.Services;
using RSG.Base.Logging;
using RSG.Base.Math;
using RSG.Model.Common;
using RSG.Model.Common.Map;
using RSG.Platform;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Dto.Enums;
using RSG.Statistics.Common.Dto.GameAssets;
using RSG.Statistics.Common.Dto.GameAssets.Bundles;
using RSG.Statistics.Common.Dto.GameAssetStats;
using RSG.Statistics.Common.Dto.GameAssetStats.Bundles;
using RSG.Statistics.Common.ServiceContract;
using RSG.Statistics.Domain.Entities;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Domain.Entities.GameAssetStats;
using RSG.Statistics.Domain.Entities.GameAssetStats.Animation;
using RSG.Statistics.Server.Extensions;
using RSG.Statistics.Server.Repository;
using CommonAnim = RSG.Model.Common.Animation;

namespace RSG.Statistics.Server.Services
{
    /// <summary>
    /// 
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class BuildService : GameAssetServiceBase<Build, BuildDto, BuildDtos>, IBuildService
    {
        #region Constants
        /// <summary>
        /// Types of archetype stats.
        /// </summary>
        private static readonly Type[] c_archetypeDbTypes =
            new Type[]
            {
                typeof(GameAssetStatBase),
                typeof(ArchetypeStat),
                typeof(SimpleMapArchetypeStat),
                typeof(DrawableArchetypeStat),
                typeof(FragmentArchetypeStat),
                typeof(InteriorArchetypeStat),
                typeof(StatedAnimArchetypeStat)
            };
        #endregion // Constants

        #region Internal Classes
        /// <summary>
        /// 
        /// </summary>
        private class ArchetypeView
        {
            public String Name { get; set; }
            public Char Type { get; set; }
            public long ReferencedStatId { get; set; }
        } // ArchetypeView

        /// <summary>
        /// 
        /// </summary>
        private class EntityView
        {
            public String Name { get; set; }
            public String ArchetypeName { get; set; }
            public long ReferencedStatId { get; set; }
        } // EntityView
        #endregion // Internal Classes

        #region Constructor(s)
        /// <summary>
        /// Constructor taking a server configuration object.
        /// </summary>
        /// <param name="server"></param>
        public BuildService(IServiceHostConfig config)
            : base(config)
        {
        }
        #endregion // Constructor(s)
    
        #region IBuildService Implementation
        #region Animations
        /// <summary>
        /// Creates per build stats for clip dictionaries.
        /// </summary>
        public void CreateClipDictionaryStats(String buildIdentifier, CommonAnim.IClipDictionaryCollection clipDictionarySubs)
        {
            long buildId = ExecuteCommand(locator => GetBuildIdCommand(locator, buildIdentifier));
            ExecuteCommand(locator => DeleteExistingClipDictionaryStatsCommand(locator, buildId));

            // Now create a stat for each of the clip dictionaries in the submission.
            foreach (CommonAnim.IClipDictionary clipDictionarySub in clipDictionarySubs)
            {
                ExecuteCommand(locator => CreateClipDictionaryStatsCommand(locator, buildId, clipDictionarySub));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private long GetBuildIdCommand(IRepositoryLocator locator, String buildIdentifier)
        {
            // Get the build we are wanting to tie these stats to.
            Build build = locator.GetAssetByIdentifier<Build>(buildIdentifier);
            return build.Id;
        }

        /// <summary>
        /// 
        /// </summary>
        private void DeleteExistingClipDictionaryStatsCommand(IRepositoryLocator locator, long buildId)
        {
            Build build = locator.GetById<Build>(buildId);

            // Delete any existing clip dictionary stats we have associated with this build.
            DetachedCriteria criteria =
                DetachedCriteria.For<ClipDictionaryStat>()
                                .Add(Restrictions.Eq("Build", build));
            IList<ClipDictionaryStat> dictionaryStats = locator.FindAll<ClipDictionaryStat>(criteria);
            foreach (ClipDictionaryStat dictionaryStat in dictionaryStats)
            {
                locator.Delete(dictionaryStat);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void CreateClipDictionaryStatsCommand(IRepositoryLocator locator, long buildId, CommonAnim.IClipDictionary clipDictionarySub)
        {
            // Get the repos we need for this.
            IGameAssetRepository<ClipDictionary> clipDictRepo = (IGameAssetRepository<ClipDictionary>)locator.GetRepository<ClipDictionary>();
            IGameAssetRepository<Clip> clipRepo = (IGameAssetRepository<Clip>)locator.GetRepository<Clip>();
            IGameAssetRepository<Animation> animRepo = (IGameAssetRepository<Animation>)locator.GetRepository<Animation>();
            IEnumRepository<CommonAnim.ClipDictionaryCategory> clipDictCategoryRepo =
                (IEnumRepository<CommonAnim.ClipDictionaryCategory>)locator.GetRepository<EnumReference<CommonAnim.ClipDictionaryCategory>>();

            // Get the build
            Build build = locator.GetById<Build>(buildId);

            // Start creating the stats.
            ClipDictionary clipDict = clipDictRepo.GetOrCreateByName(clipDictionarySub.Name);
            clipDict.Category = clipDictCategoryRepo.GetByValue(clipDictionarySub.Category);
            locator.Save(clipDict);

            ClipDictionaryStat dictStat = new ClipDictionaryStat();
            dictStat.Build = build;
            dictStat.Asset = clipDict;
            dictStat.Category = locator.GetEnumReferenceByValue(clipDictionarySub.Category);

            foreach (CommonAnim.IClip clipSub in clipDictionarySub.Clips)
            {
                ClipStat clipStat = new ClipStat();
                clipStat.Build = build;
                clipStat.Asset = clipRepo.GetOrCreateByName(clipSub.Name);
                clipStat.ClipDictionaryStat = dictStat;

                foreach (CommonAnim.IAnimation animSub in clipSub.Animations)
                {
                    AnimationStat animStat = new AnimationStat();
                    animStat.Build = build;
                    animStat.Asset = animRepo.GetOrCreateByName(animSub.Name);
                    animStat.ClipStat = clipStat;

                    clipStat.AnimationStats.Add(animStat);
                }

                dictStat.ClipStats.Add(clipStat);
            }

            // Saves all the objects in the hierarchy.
            locator.Save(dictStat);
        }
        #endregion // Animations

        #region Builds
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public BuildDto GetLatest()
        {
            return ExecuteCommand(locator => GetLatestCommand(locator));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        public BuildDtos GetLatestBuilds(uint count)
        {
            return ExecuteCommand(locator => GetLatestCommand(locator, count));
        }

        /// <summary>
        /// 
        /// </summary>
        public void UpdateBuildFeatures()
        {
            ExecuteCommand(locator => UpdateBuildFeaturesCommand(locator));
        }
        #endregion // Builds

        #region Characters
        /// <summary>
        /// 
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <returns></returns>
        public CharacterDtos GetAllCharactersForBuild(string buildIdentifier)
        {
            return ExecuteCommand(locator => GetCharactersForBuildCommand(locator, buildIdentifier));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="characterHash"></param>
        /// <returns></returns>
        public CharacterDto GetCharacterForBuild(string buildIdentifier, string characterHash)
        {
            return ExecuteCommand(locator => GetCharacterForBuildCommand(locator, buildIdentifier, characterHash));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="characterHash"></param>
        /// <returns></returns>
        public CharacterStatBundleDto GetCharacterStat(string buildIdentifier, string characterHash)
        {
            return ExecuteCommand(locator => GetCharacterStatCommand(locator, buildIdentifier, characterHash));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="characterHash"></param>
        /// <param name="characterStatBundle"></param>
        /// <returns></returns>
        public void CreateCharacterStat(string buildIdentifier, string characterHash, CharacterStatBundleDto characterStatBundle)
        {
            ExecuteCommand(locator => CreateCharacterStat(locator, buildIdentifier, characterHash, characterStatBundle));
        }
        #endregion // Characters

        #region Cutscenes
        /// <summary>
        /// Creates per build stats for cutscenes.
        /// </summary>
        public void CreateCutsceneStats(String buildIdentifier, CommonAnim.ICutsceneCollection cutscenes)
        {
            long buildId = ExecuteCommand(locator => GetBuildIdCommand(locator, buildIdentifier));
            ExecuteCommand(locator => DeleteExistingCutsceneStatsCommand(locator, buildId));

            // Now create a stat for each of the cutscenes in the submission.
            foreach (CommonAnim.ICutscene cutscene in cutscenes)
            {
                ExecuteCommand(locator => CreateCutsceneStatCommand(locator, buildId, cutscene));
            }
        }

        /// <summary>
        /// Deletes any cutscene stats associated with a particular build of the game.
        /// </summary>
        private void DeleteExistingCutsceneStatsCommand(IRepositoryLocator locator, long buildId)
        {
            Build build = locator.GetById<Build>(buildId);

            // Delete any existing clip dictionary stats we have associated with this build.
            DetachedCriteria criteria =
                DetachedCriteria.For<CutsceneStat>()
                                .Add(Restrictions.Eq("Build", build));
            IList<CutsceneStat> cutsceneStats = locator.FindAll<CutsceneStat>(criteria);
            foreach (CutsceneStat cutsceneStat in cutsceneStats)
            {
                locator.Delete(cutsceneStat);
            }
        }

        /// <summary>
        /// Creates a new per build stat for a particular cutscene.
        /// </summary>
        private void CreateCutsceneStatCommand(IRepositoryLocator locator, long buildId, CommonAnim.ICutscene cutsceneSub)
        {
            // Get the repos we need for this operation.
            IGameAssetRepository<Cutscene> cutsceneRepo = (IGameAssetRepository<Cutscene>)locator.GetRepository<Cutscene>();
            IEnumRepository<RSG.Platform.Platform> platformRepo = (IEnumRepository<RSG.Platform.Platform>)locator.GetRepository<EnumReference<RSG.Platform.Platform>>();

            // Get the build
            Build build = locator.GetById<Build>(buildId);

            // Construct the stat we require for this cutscene.
            CutsceneStat cutsceneStat = new CutsceneStat();
            cutsceneStat.Build = build;
            cutsceneStat.Asset = cutsceneRepo.GetOrCreateByName(cutsceneSub.Name);
            cutsceneStat.ExportZipFilepath = cutsceneSub.ExportZipFilepath;
            cutsceneStat.IsConcat = cutsceneSub.IsConcat;
            cutsceneStat.HasBranch = cutsceneSub.HasBranch;

            foreach (CommonAnim.ICutscenePart partSub in cutsceneSub.Parts)
            {
                CutscenePartStat partStat = new CutscenePartStat();
                partStat.Name = partSub.Name;
                partStat.CutsceneStat = cutsceneStat;
                partStat.CutFile = partSub.CutFile;
                partStat.Duration = partSub.Duration;

                cutsceneStat.PartStats.Add(partStat);
            }

            foreach (KeyValuePair<RSG.Platform.Platform, CommonAnim.ICutscenePlatformStat> platformStatPair in cutsceneSub.PlatformStats)
            {
                foreach (CommonAnim.ICutscenePlatformSectionStat sectionStatSub in platformStatPair.Value.Sections)
                {
                    CutscenePlatformSectionStat sectionStat = new CutscenePlatformSectionStat();
                    sectionStat.AssetStat = cutsceneStat;
                    sectionStat.Platform = platformRepo.GetByValue(platformStatPair.Key);
                    sectionStat.PhysicalSize = sectionStatSub.PhysicalSize;
                    sectionStat.VirtualSize = sectionStatSub.VirtualSize;
                    sectionStat.DataPath = platformStatPair.Value.PlatformDataPath;
                    sectionStat.Index = sectionStatSub.Index;

                    cutsceneStat.PlatformSectionStats.Add(sectionStat);
                }
            }

            // Saves all the objects in the hierarchy.
            locator.Save(cutsceneStat);
        }

        /// <summary>
        /// Gets the list of cutscenes that are associated with the specified build.
        /// </summary>
        public CommonAnim.ICutsceneCollection GetCutsceneStats(String buildIdentifier)
        {
            return ExecuteCommand(locator => GetCutsceneStatsCommand(locator, buildIdentifier));
        }

        /// <summary>
        /// As called by GetCutsceneStats.
        /// </summary>
        private CommonAnim.ICutsceneCollection GetCutsceneStatsCommand(IRepositoryLocator locator, String buildIdentifier)
        {
            ICutsceneStatRepository cutsceneStatRepo = (ICutsceneStatRepository)locator.GetRepository<CutsceneStat>();
            Build build = locator.GetAssetByIdentifier<Build>(buildIdentifier);
            return cutsceneStatRepo.CreateCollectionForBuild(build);
        }
        #endregion // Cutscenes

        #region Levels
        /// <summary>
        /// 
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <returns></returns>
        public LevelDtos GetAllLevelsForBuild(string buildIdentifier)
        {
            return ExecuteCommand(locator => GetLevelsForBuildCommand(locator, buildIdentifier));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <returns></returns>
        public LevelDto GetLevelForBuild(string buildIdentifier, string levelHash)
        {
            return ExecuteCommand(locator => GetLevelForBuildCommand(locator, buildIdentifier, levelHash));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="levelStatBundle"></param>
        /// <returns></returns>
        public void CreateLevelStat(string buildIdentifier, string levelHash, LevelStatBundleDto levelStatBundle)
        {
            ExecuteCommand(locator => CreateLevelStatCommand(locator, buildIdentifier, levelHash, levelStatBundle));
        }

        /// <summary>
        /// Retrieves a specific sub set of stats for a particular set of levels.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="fetchDto"></param>
        /// <returns></returns>
        public List<LevelStatDto> GetMultipleLevelStats(String buildIdentifier, StreamableStatFetchDto fetchDto)
        {
            return ExecuteCommand(locator => GetMultipleLevelStatsCommand(locator, buildIdentifier, fetchDto));
        }
        #endregion // Levels

        #region Map Hierarchy
        /// <summary>
        /// Retrieves the map hierarchy along with some very basic information for a particular build/level.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <returns></returns>
        public MapHierarchyBundleDto GetMapHierarchy(String buildIdentifier, String levelHash)
        {
            return ExecuteCommand(locator => GetMapHierarchyCommand(locator, buildIdentifier, levelHash));
        }

        /// <summary>
        /// Creates a new map hierarchy for a particular build/level.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="hierarchyBundle"></param>
        public void CreateMapHierarchy(String buildIdentifier, String levelHash, MapHierarchyBundleDto hierarchyBundle)
        {
            ExecuteCommand(locator => CreateMapHierarchyCommand(locator, buildIdentifier, levelHash, hierarchyBundle));
        }

        /// <summary>
        /// Creates a new map area stat for the specified build and map area.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="mapAreaHash"></param>
        /// <param name="mapAreaStatBundle"></param>
        public void UpdateMapAreaStat(String buildIdentifier, String levelHash, String mapAreaHash, MapAreaStatBundleDto mapAreaStatBundle)
        {
            ExecuteCommand(locator => UpdateMapAreaStatCommand(locator, buildIdentifier, levelHash, mapAreaHash, mapAreaStatBundle));
        }

        /// <summary>
        /// Creates a new map section stat for the specified build and map section.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="mapAreaHash"></param>
        /// <param name="mapAreaStatBundle"></param>
        public void UpdateMapSectionStat(String buildIdentifier, String levelHash, String mapSectionHash, MapSectionStatBundleDto mapSectionStatBundle)
        {
            ExecuteCommand(locator => UpdateMapSectionStatCommand(locator, buildIdentifier, levelHash, mapSectionHash, mapSectionStatBundle));
        }

        /// <summary>
        /// Creates a new map section stat for the specified build, level and map section.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="mapAreaHash"></param>
        /// <param name="mapAreaStatBundle"></param>
        public void CreateArchetypeStats(String buildIdentifier, String levelHash, String mapSectionHash, ArchetypeStatBundles archetypeStatBundles)
        {
            foreach (ArchetypeStatBundle bundle in archetypeStatBundles.Items)
            {
                ExecuteCommand(locator => CreateArchetypeStatCommand(locator, buildIdentifier, levelHash, mapSectionHash, bundle));
            }
        }

        /// <summary>
        /// Creates entity stats for a particular map section.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="mapAreaHash"></param>
        /// <param name="mapAreaStatBundle"></param>
        public void CreateEntityStats(String buildIdentifier, String levelHash, String mapSectionHash, EntityStatBundles entityStatBundles)
        {
            foreach (EntityStatBundle bundle in entityStatBundles.Items)
            {
                ExecuteCommand(locator => CreateEntityStatCommand(locator, buildIdentifier, levelHash, mapSectionHash, bundle));
            }
        }

        /// <summary>
        /// Creates a new car gen stat for the specified build, level and map section.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="mapSectionHash"></param>
        /// <param name="mapSectionStatBundle"></param>
        public void CreateCarGenStats(String buildIdentifier, String levelHash, String mapSectionHash, CarGenStatDtos dtos)
        {
            foreach (CarGenStatDto dto in dtos.Items)
            {
                ExecuteCommand(locator => CreateCarGenStatCommand(locator, buildIdentifier, levelHash, mapSectionHash, dto));
            }
        }

        /// <summary>
        /// Creates a new spawn point stat for the specified build and level.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="dtos"></param>
        public void CreateSpawnPointStats(String buildIdentifier, String levelHash, SpawnPointStatDtos dtos)
        {
            foreach (SpawnPointStatDto dto in dtos.Items)
            {
                ExecuteCommand(locator => CreateSpawnPointStatCommand(locator, buildIdentifier, levelHash, dto));
            }
        }

        /// <summary>
        /// Retrieves a specific sub set of stats for a particular set map section.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="fetchDto"></param>
        /// <returns></returns>
        public List<MapSectionStatDto> GetMultipleMapSectionStats(String buildIdentifier, String levelHash, StreamableStatFetchDto fetchDto)
        {
            return ExecuteCommand(locator => GetMultipleMapSectionStatsCommand(locator, buildIdentifier, levelHash, fetchDto));
        }
        /// <summary>
        /// Retrieves a specific sub set of stats for a particular set of archetypes.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="fetchDto"></param>
        /// <returns></returns>
        public List<ArchetypeStatDto> GetMultipleArchetypeStats(String buildIdentifier, String levelHash, StreamableStatFetchDto fetchDto)
        {
            return ExecuteCommand(locator => GetMultipleArchetypeStatsCommand(locator, buildIdentifier, levelHash, fetchDto));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="fetchDto"></param>
        public List<EntityStatDto> GetMultipleEntityStats(string buildIdentifier, String levelHash, StreamableStatFetchDto fetchDto)
        {
            return ExecuteCommand(locator => GetMultipleEntityStatsCommand(locator, buildIdentifier, levelHash, fetchDto));
        }

        /// <summary>
        /// Retrieves a specific sub set of stats for a particular set of room.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="fetchDto"></param>
        /// <returns></returns>
        public List<RoomStatDto> GetMultipleRoomStats(String buildIdentifier, String levelHash, StreamableStatFetchDto fetchDto)
        {
            return ExecuteCommand(locator => GetMultipleRoomStatsCommand(locator, buildIdentifier, levelHash, fetchDto));
        }
        #endregion // Map Hierarchy

        #region Vehicles
        /// <summary>
        /// 
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <returns></returns>
        public VehicleDtos GetAllVehiclesForBuild(string buildIdentifier)
        {
            return ExecuteCommand(locator => GetVehiclesForBuildCommand(locator, buildIdentifier));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="vehicleHash"></param>
        /// <returns></returns>
        public VehicleDto GetVehicleForBuild(string buildIdentifier, string vehicleHash)
        {
            return ExecuteCommand(locator => GetVehicleForBuildCommand(locator, buildIdentifier, vehicleHash));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="vehicleHash"></param>
        /// <returns></returns>
        public VehicleStatBundleDto GetVehicleStat(string buildIdentifier, string vehicleHash)
        {
            return ExecuteCommand(locator => GetVehicleStatCommand(locator, buildIdentifier, vehicleHash));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="vehicleHash"></param>
        /// <param name="vehicleStatBundle"></param>
        /// <returns></returns>
        public void CreateVehicleStat(string buildIdentifier, string vehicleHash, VehicleStatBundleDto vehicleStatBundle)
        {
            ExecuteCommand(locator => CreateVehicleStat(locator, buildIdentifier, vehicleHash, vehicleStatBundle));
        }
        #endregion // Vehicles

        #region Weapons
        /// <summary>
        /// Gets a list of weapons that are associated with the specified build.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <returns></returns>
        public WeaponDtos GetAllWeaponsForBuild(string buildIdentifier)
        {
            return ExecuteCommand(locator => GetWeaponsForBuildCommand(locator, buildIdentifier));
        }

        /// <summary>
        /// Gets an individual weapon for a particular build based on the weapon's hash.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="weaponHash">Hash of the weapon's name.</param>
        /// <returns></returns>
        public WeaponDto GetWeaponForBuild(string buildIdentifier, string weaponHash)
        {
            return ExecuteCommand(locator => GetWeaponForBuildCommand(locator, buildIdentifier, weaponHash));
        }

        /// <summary>
        /// Gets an individual weapon statistic for a particular build and weapon.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="weaponHash">Hash of the weapon's name.</param>
        /// <returns></returns>
        public WeaponStatBundleDto GetWeaponStat(string buildIdentifier, string weaponHash)
        {
            return ExecuteCommand(locator => GetWeaponStatCommand(locator, buildIdentifier, weaponHash));
        }

        /// <summary>
        /// Creates a new weapon stat for the specified build and weapon.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="weaponHash">Hash of the weapon's name.</param>
        /// <param name="weaponStatBundle">Data transfer object containing all the information relating to a weapon stat.</param>
        /// <returns></returns>
        public void CreateWeaponStat(string buildIdentifier, string weaponHash, WeaponStatBundleDto weaponStatBundle)
        {
            ExecuteCommand(locator => CreateWeaponStat(locator, buildIdentifier, weaponHash, weaponStatBundle));
        }
        #endregion // Weapons

        #region Data Deletion
        /// <summary>
        /// 
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <returns></returns>
        public void DeleteAssetStats(string buildIdentifier)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buildIdentifier"></param>
        public void DeleteTelemetryStats(string buildIdentifier)
        {
            throw new NotImplementedException();
        }
        #endregion // Data Deletion
        #endregion // IBuildService Implementation

        #region Private Methods
        #region Builds
        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <returns></returns>
        protected override BuildDtos GetAllCommand(IRepositoryLocator locator)
        {
            DetachedCriteria query = DetachedCriteria.For<Build>().Add(Restrictions.Eq("Hidden", false));

            BuildDtos result = new BuildDtos();
            result.Items = Mapper.Map<IList<Build>, List<BuildDto>>(locator.FindAll<Build>(query));
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <returns></returns>
        private BuildDto GetLatestCommand(IRepositoryLocator locator)
        {
            DetachedCriteria query = DetachedCriteria.For<Build>().Add(Restrictions.Eq("Hidden", false));
            Build build = locator.FindFirst<Build>(query, new Order("Identifier", false));

            Debug.Assert(build != null, "No builds exist in the database.");
            if (build == null)
            {
                throw new ArgumentException("No builds exist in the database.");
            }

            return Mapper.Map<Build, BuildDto>(build);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        private BuildDtos GetLatestCommand(IRepositoryLocator locator, uint count)
        {
            // Select the latest 'count' builds.
            DetachedCriteria query =
                DetachedCriteria.For<Build>()
                                .Add(Restrictions.Eq("Hidden", false))
                                .SetFirstResult(0)
                                .SetMaxResults((int)count)
                                .AddOrder(Order.Desc("Identifier"));

            BuildDtos result = new BuildDtos();
            result.Items = Mapper.Map<IEnumerable<Build>, List<BuildDto>>(locator.FindAll<Build>(query));
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        private void UpdateBuildFeaturesCommand(IRepositoryLocator locator)
        {
            // Get the build features from Vertica.
            IList<RSG.Statistics.Common.Vertica.Dto.BuildFeatures> features;
            using (RSG.Statistics.Client.Vertica.Client.DataClient client = new RSG.Statistics.Client.Vertica.Client.DataClient((Config as IStatsServer).VerticaServer))
            {
                // HACK. We need to manually set the outgoing request method because WCF changes it to POST for some bizarre reason...
                // http://social.msdn.microsoft.com/Forums/en-US/wcf/thread/017fa465-5873-45b3-a12a-6228bf4a7cc6/
                using (OperationContextScope scope = new OperationContextScope(client.InnerChannel))
                {
                    WebOperationContext.Current.OutgoingRequest.Method = "GET";
                    features = client.GetAllBuildFeatures();
                }
            }

            // Get the list of builds that our local server is aware of.
            DetachedCriteria criteria = DetachedCriteria.For<Build>();
            IList<Build> allBuilds = locator.FindAll<Build>(criteria);
            IDictionary<String, Build> buildLookup = allBuilds.ToDictionary(item => item.Identifier);

            // Go over all the features 
            foreach (RSG.Statistics.Common.Vertica.Dto.BuildFeatures feature in features)
            {
                uint major = feature.BuildVersion >> 4;
                uint minor = feature.BuildVersion - (major << 4);
                String verticaBuildIdentifier = (minor == 0 ? major.ToString() : String.Format("{0}.{1}", major, minor));

                Build build;
                if (!buildLookup.TryGetValue(verticaBuildIdentifier, out build))
                {
                    build = new Build();
                    build.Identifier = verticaBuildIdentifier;
                }

                build.GameVersion = feature.BuildVersion;
                build.HasAutomatedMapOnlyStats = feature.HasMapOnlyStats;
                build.HasAutomatedEverythingStats = feature.HasEverythingStats;
                build.HasAutomatedNighttimeMapOnlyStats = feature.HasNightTimeMapOnlyStats;
                build.HasMemShortfallStats = feature.HasMemShortfallStats;
                build.HasShapeTestStats = feature.HasPhysicsShapeCostStats;
                locator.SaveOrUpdate(build);
            }
        }
        #endregion // Builds

        #region Characters
        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="buildIdentifier"></param>
        /// <returns></returns>
        private CharacterDtos GetCharactersForBuildCommand(IRepositoryLocator locator, string buildIdentifier)
        {
            Build build = GetAssetByIdentifier<Build>(locator, buildIdentifier);

            // Get all the characters for this build
            DetachedCriteria query =
                DetachedCriteria.For<CharacterStat>()
                                .CreateAlias("Character", "c")
                                .Add(Restrictions.Eq("Build", build));

            CharacterDtos result = new CharacterDtos();
            foreach (CharacterStat stat in locator.FindAll<CharacterStat>(query))
            {
                result.Items.Add(Mapper.Map<Character, CharacterDto>(stat.Character));
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="buildIdentifier"></param>
        /// <param name="characterHash"></param>
        /// <returns></returns>
        private CharacterDto GetCharacterForBuildCommand(IRepositoryLocator locator, string buildIdentifier, string characterHash)
        {
            Build build = GetAssetByIdentifier<Build>(locator, buildIdentifier);
            Character character = GetAssetByIdentifier<Character>(locator, characterHash);

            // Ensure that a character stat exists for the two
            DetachedCriteria query = DetachedCriteria.For<CharacterStat>()
                                                     .Add(Expression.Eq("Build", build))
                                                     .Add(Expression.Eq("Character", character));
            CharacterStat characterStat = locator.FindFirst<CharacterStat>(query);
            if (characterStat == null)
            {
                throw new WebFaultException<string>("Character doesn't have a stat in build.", HttpStatusCode.BadRequest);
            }

            return Mapper.Map<Character, CharacterDto>(character);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="buildIdentifier"></param>
        /// <param name="characterHash"></param>
        /// <returns></returns>
        private CharacterStatBundleDto GetCharacterStatCommand(IRepositoryLocator locator, string buildIdentifier, string characterHash)
        {
            Build build = GetAssetByIdentifier<Build>(locator, buildIdentifier);
            Character character = GetAssetByIdentifier<Character>(locator, characterHash);

            // Ensure that a character stat exists for the two
            DetachedCriteria query = DetachedCriteria.For<CharacterStat>()
                                                     .Add(Expression.Eq("Build", build))
                                                     .Add(Expression.Eq("Character", character));
            CharacterStat characterStat = locator.FindFirst<CharacterStat>(query);
            if (characterStat == null)
            {
                throw new WebFaultException<string>("Character doesn't have a stat in build.", HttpStatusCode.BadRequest);
            }

            return Mapper.Map<CharacterStat, CharacterStatBundleDto>(characterStat);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="buildIdentifier"></param>
        /// <param name="characterHash"></param>
        /// <returns></returns>
        private void CreateCharacterStat(IRepositoryLocator locator, string buildIdentifier, string characterHash, CharacterStatBundleDto characterStatBundle)
        {
            Build build = GetAssetByIdentifier<Build>(locator, buildIdentifier);
            Character character = GetAssetByIdentifier<Character>(locator, characterHash);

            DetachedCriteria query = DetachedCriteria.For<CharacterStat>()
                                                     .Add(Expression.Eq("Build", build))
                                                     .Add(Expression.Eq("Character", character));
            CharacterStat characterStat = locator.FindFirst<CharacterStat>(query);
            if (characterStat != null)
            {
                // Delete the stat if it exists
                locator.Delete(characterStat);
            }

            // Create any shaders/textures this bundle contains.
            CreateShadersAndTextures(locator, characterStatBundle);

            // Create the new character stat
            CharacterStat stat = Mapper.Map<CharacterStatBundleDto, CharacterStat>(characterStatBundle);
            stat.Build = GetAssetByIdentifier<Build>(locator, buildIdentifier);
            stat.Character = GetAssetByIdentifier<Character>(locator, characterHash);
            
            // Create the platform stats
            stat.CharacterPlatformStats = new List<CharacterPlatformStat>();
            foreach (CharacterPlatformStatBundleDto platformStatDto in characterStatBundle.CharacterPlatformStats)
            {
                CharacterPlatformStat platformStat = Mapper.Map<CharacterPlatformStatBundleDto, CharacterPlatformStat>(platformStatDto);
                platformStat.CharacterStat = stat;
                platformStat.Platform = GetEnumReferenceByValue<RSG.Platform.Platform>(locator, platformStatDto.Platform.Value);
                stat.CharacterPlatformStats.Add(platformStat);
            }

            // Create/link the shader stats
            LinkShaderStatsToGameAssetStat(locator, stat, characterStatBundle);

            locator.Save(stat);
        }
        #endregion // Characters
        
        #region Levels
        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="buildIdentifier"></param>
        /// <returns></returns>
        private LevelDtos GetLevelsForBuildCommand(IRepositoryLocator locator, string buildIdentifier)
        {
            Build build = GetAssetByIdentifier<Build>(locator, buildIdentifier);

            // Get all the characters for this build
            DetachedCriteria query = DetachedCriteria.For<LevelStat>()
                .CreateAlias("Level", "l")
                .Add(Expression.Eq("Build", build));

            LevelDtos result = new LevelDtos();
            foreach (LevelStat stat in locator.FindAll<LevelStat>(query))
            {
                result.Items.Add(Mapper.Map<Level, LevelDto>(stat.Level));
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="buildIdentifier"></param>
        /// <param name="characterHash"></param>
        /// <returns></returns>
        private LevelDto GetLevelForBuildCommand(IRepositoryLocator locator, string buildIdentifier, string levelHash)
        {
            Build build = GetAssetByIdentifier<Build>(locator, buildIdentifier);
            Level level = GetAssetByIdentifier<Level>(locator, levelHash);

            // Ensure that a character stat exists for the two
            DetachedCriteria query = DetachedCriteria.For<CharacterStat>()
                                                     .Add(Expression.Eq("Build", build))
                                                     .Add(Expression.Eq("Level", level));
            LevelStat levelStat = locator.FindFirst<LevelStat>(query);
            if (levelStat == null)
            {
                throw new WebFaultException<string>("Level doesn't have a stat in build.", HttpStatusCode.BadRequest);
            }

            return Mapper.Map<Level, LevelDto>(level);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <returns></returns>
        private void CreateLevelStatCommand(IRepositoryLocator locator, string buildIdentifier, string levelHash, LevelStatBundleDto levelStatBundle)
        {
            Build build = GetAssetByIdentifier<Build>(locator, buildIdentifier);
            Level level = GetAssetByIdentifier<Level>(locator, levelHash);

            // Attempt to retrieve an existing stat
            DetachedCriteria query = DetachedCriteria.For<LevelStat>()
                                                     .Add(Expression.Eq("Build", build))
                                                     .Add(Expression.Eq("Level", level));
            LevelStat levelStat = locator.FindFirst<LevelStat>(query);
            if (levelStat != null)
            {
                // Delete the stat if it exists
                locator.Delete(levelStat);
            }

            // Check whether we need to create a new level image for this stat
            query = DetachedCriteria.For<LevelImage>()
                                    .Add(Expression.Eq("BugstarId", levelStatBundle.ImageBugstarId));
            LevelImage levelImage = locator.FindFirst<LevelImage>(query);

            if (levelImage == null)
            {
                levelImage = new LevelImage();
                levelImage.BugstarId = levelStatBundle.ImageBugstarId;
                levelImage.ImageData = levelStatBundle.ImageData;
                locator.SaveOrUpdate(levelImage);
            }

            levelStat = Mapper.Map<LevelStatBundleDto, LevelStat>(levelStatBundle);
            levelStat.Build = build;
            levelStat.Level = level;
            levelStat.BackgroundImage = levelImage;
            if (levelStatBundle.LowerLeft != null)
                levelStat.LowerLeft = new Point(new Coordinate(levelStatBundle.LowerLeft.X, levelStatBundle.LowerLeft.Y));
            if (levelStatBundle.UpperRight != null)
                levelStat.UpperRight = new Point(new Coordinate(levelStatBundle.UpperRight.X, levelStatBundle.UpperRight.Y));
            locator.SaveOrUpdate(levelStat);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="fetchDto"></param>
        /// <returns></returns>
        private List<LevelStatDto> GetMultipleLevelStatsCommand(IRepositoryLocator locator, String buildIdentifier, StreamableStatFetchDto fetchDto)
        {
            // Get db objects for the passed in items
            Build build = GetAssetByIdentifier<Build>(locator, buildIdentifier);

            //
            DetachedCriteria criteria = DetachedCriteria.For<LevelStat>()
                                                        .CreateAlias("Level", "l")
                                                        .Add(Expression.Eq("Build", build))
                                                        .Add(Restrictions.In("l.Identifier", fetchDto.AssetIdentifiers));
            IList<LevelStat> stats = locator.FindAll<LevelStat>(criteria);

            // Convert the db objects to dto objects
            List<LevelStatDto> dtos = new List<LevelStatDto>();

            foreach (LevelStat stat in stats)
            {
                LevelStatDto dto = new LevelStatDto();
                dto.IdentifierString = stat.Level.Identifier;
                dto.Id = stat.Id;
                CopySpecificStats(stat, dto, fetchDto.RequestedStats);

                if (fetchDto.RequestedStats.Contains(StreamableLevelStat.ImageBounds))
                {
                    BoundingBox2f bbox = new BoundingBox2f();
                    if (stat.LowerLeft != null && stat.UpperRight != null)
                    {
                        bbox.Min = new Vector2f((float)stat.LowerLeft.X, (float)stat.LowerLeft.Y);
                        bbox.Max = new Vector2f((float)stat.UpperRight.X, (float)stat.UpperRight.Y);
                    }
                    dto.ImageBounds = bbox;
                }
                if (fetchDto.RequestedStats.Contains(StreamableLevelStat.BackgroundImage))
                {
                    dto.ImageData = stat.BackgroundImage.ImageData;
                }

                dtos.Add(dto);
            }

            // Were any complex stat types requested?
            IDictionary<StreamableStat, PropertyInfo> dbLookup = StreamableStatUtils.GetPropertyInfoLookup(typeof(RoomStat));

            foreach (StreamableStat stat in fetchDto.RequestedStats)
            {
                if (dbLookup.ContainsKey(stat))
                {
                    if (stat.DbToDtoRequiresProcessing)
                    {
                        ProcessComplexLevelStats(locator, dtos, stat);
                    }
                }
            }

            return dtos;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="dtos"></param>
        /// <param name="stat"></param>
        private void ProcessComplexLevelStats(IRepositoryLocator locator, IList<LevelStatDto> dtos, StreamableStat stat)
        {
            // Create a map of section stat ids to dtos
            IDictionary<long, LevelStatDto> dtoLookup = dtos.ToDictionary(item => item.Id);

            // Check what type of streamable stat was requested.
            if (stat == StreamableLevelStat.SpawnPoints)
            {
                ProcessLevelSpawnPointStats(locator, dtoLookup);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="dtoLookup"></param>
        private void ProcessLevelSpawnPointStats(IRepositoryLocator locator, IDictionary<long, LevelStatDto> dtoLookup)
        {
            // Initialise all the dtos.
            foreach (LevelStatDto dto in dtoLookup.Values)
            {
                dto.SpawnPoints = new List<SpawnPointStatDto>();
            }

            // Get the data from the db
            DetachedCriteria criteria = DetachedCriteria.For<SpawnPointStat>()
                                                     .CreateAlias("LevelStat", "ls")
                                                     .Add(Restrictions.In("ls.Id", dtoLookup.Keys.ToList()));
            IList<SpawnPointStat> spawnPointStats = locator.FindAll<SpawnPointStat>(criteria);

            // Convert the data to dtos
            foreach (SpawnPointStat spawnPoint in spawnPointStats)
            {
                if (dtoLookup.ContainsKey(spawnPoint.ArchetypeStat.Id))
                {
                    LevelStatDto dto = dtoLookup[spawnPoint.LevelStat.Id];

                    SpawnPointStatDto spawnPointDto = Mapper.Map<SpawnPointStat, SpawnPointStatDto>(spawnPoint);
                    spawnPointDto.Position = new Vector3f((float)spawnPoint.Position.X, (float)spawnPoint.Position.Y, (float)spawnPoint.Position.Z);
                    spawnPointDto.AvailabilityMode = (spawnPoint.AvailabilityMode != null ? spawnPoint.AvailabilityMode.Value : null);
                    dto.SpawnPoints.Add(spawnPointDto);
                }
            }
        }
        #endregion // Levels

        #region Map Hierarchy
        #region Data Submission
        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="hierarchyBundle"></param>
        private void CreateMapHierarchyCommand(IRepositoryLocator locator, String buildIdentifier, String levelHash, MapHierarchyBundleDto hierarchyBundle)
        {
            Build build = GetAssetByIdentifier<Build>(locator, buildIdentifier);
            Level level = GetAssetByIdentifier<Level>(locator, levelHash);

            // Check whether any stats already exist
            DetachedCriteria query = DetachedCriteria.For<MapNodeStatBase>()
                                                     .Add(Expression.Eq("Build", build))
                                                     .Add(Expression.Eq("Level", level))
                                                     .Add(Expression.Eq("ParentAreaStat", null));
            IEnumerable<MapNodeStatBase> mapNodeStats = locator.FindAll<MapNodeStatBase>(query);

            // Delete stats that exist
            if (mapNodeStats.Any())
            {
                foreach (MapNodeStatBase nodeStat in mapNodeStats)
                {
                    locator.Delete(nodeStat);
                }
            }

            // Add a stat for each root level entry
            foreach (BasicMapNodeStatBaseDto dto in hierarchyBundle.ChildNodes)
            {
                MapNodeStatBase stat = Mapper.Map<BasicMapNodeStatBaseDto, MapNodeStatBase>(dto);
                stat.Build = build;
                stat.Level = level;
                stat.ParentAreaStat = null;

                if (stat is MapSectionStat)
                {
                    MapSectionStat msStat = (MapSectionStat)stat;
                    msStat.MapSection = GetAssetByIdentifier<MapSection>(locator, dto.IdentifierString);

                    // Handle the outline specially.
                    BasicMapSectionStatDto sectionDto = (BasicMapSectionStatDto)dto;
                    if (sectionDto.VectorMapPoints != null && sectionDto.VectorMapPoints.Any())
                    {
                        // Duplicate the last coordinate as nhibernate spatial/geoAPI expect the points to form a closed loop.
                        IList<Coordinate> coordinates = sectionDto.VectorMapPoints.Select(item => new Coordinate(item.X, item.Y)).ToList();
                        coordinates.Add(new Coordinate(sectionDto.VectorMapPoints[0].X, sectionDto.VectorMapPoints[0].Y));

                        msStat.VectorMapPoints = new Polygon(new LinearRing(coordinates.ToArray()));
                    }

                    locator.SaveOrUpdate(stat);
                }
                else if (stat is MapAreaStat)
                {
                    MapAreaStat maStat = (MapAreaStat)stat;
                    maStat.MapArea = GetAssetByIdentifier<MapArea>(locator, dto.IdentifierString);
                    locator.SaveOrUpdate(stat);

                    // Parent area needs to be saved before we can access the Id for child stats
                    AddMapAreaChildren(locator, build, level, maStat, (BasicMapAreaStatDto)dto);
                }
            }
        }

        /// <summary>
        /// Recursive helper function for adding child stats to a parent area
        /// </summary>
        /// <param name="parentArea"></param>
        /// <param name="dto"></param>
        private void AddMapAreaChildren(IRepositoryLocator locator, Build build, Level level, MapAreaStat parentArea, BasicMapAreaStatDto dto)
        {
            foreach (BasicMapNodeStatBaseDto childDto in dto.ChildNodes)
            {
                MapNodeStatBase stat = Mapper.Map<BasicMapNodeStatBaseDto, MapNodeStatBase>(childDto);
                stat.Build = build;
                stat.Level = level;
                stat.ParentAreaStat = parentArea;

                if (stat is MapSectionStat)
                {
                    MapSectionStat msStat = (MapSectionStat)stat;
                    msStat.MapSection = GetAssetByIdentifier<MapSection>(locator, childDto.IdentifierString);

                    // Handle the outline specially.
                    BasicMapSectionStatDto sectionDto = (BasicMapSectionStatDto)childDto;
                    if (sectionDto.VectorMapPoints != null && sectionDto.VectorMapPoints.Any())
                    {
                        // Duplicate the last coordinate as nhibernate spatial/geoAPI expect the points to form a closed loop.
                        IList<Coordinate> coordinates = sectionDto.VectorMapPoints.Select(item => new Coordinate(item.X, item.Y)).ToList();
                        coordinates.Add(new Coordinate(sectionDto.VectorMapPoints[0].X, sectionDto.VectorMapPoints[0].Y));

                        msStat.VectorMapPoints = new Polygon(new LinearRing(coordinates.ToArray()));
                    }

                    locator.SaveOrUpdate(stat);
                }
                else if (stat is MapAreaStat)
                {
                    MapAreaStat maStat = (MapAreaStat)stat;
                    maStat.MapArea = GetAssetByIdentifier<MapArea>(locator, childDto.IdentifierString);
                    locator.SaveOrUpdate(stat);

                    // Parent area needs to be saved before we can access the Id for child stats
                    AddMapAreaChildren(locator, build, level, maStat, (BasicMapAreaStatDto)childDto);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="mapAreaHash"></param>
        /// <param name="mapAreaStatBundle"></param>
        private void UpdateMapAreaStatCommand(IRepositoryLocator locator, String buildIdentifier, String levelHash, String mapAreaHash, MapAreaStatBundleDto mapAreaStatBundle)
        {
            // Get db objects for the passed in items
            Build build = GetAssetByIdentifier<Build>(locator, buildIdentifier);
            Level level = GetAssetByIdentifier<Level>(locator, levelHash);
            MapArea mapArea = GetAssetByIdentifier<MapArea>(locator, mapAreaHash);

            // Get the map area stat for the filter combination.
            DetachedCriteria query = DetachedCriteria.For<MapAreaStat>();
            query.Add(Expression.Eq("Build", build));
            query.Add(Expression.Eq("Level", level));
            query.Add(Expression.Eq("MapArea", mapArea));
            MapAreaStat mapAreaStat = locator.FindFirst<MapAreaStat>(query);

            // Make sure the stat exists.  It should have been created via the CreateMapHierarchy endpoint prior to this being called.
            if (mapAreaStat == null)
            {
                throw new WebFaultException<String>(String.Format("Map area stat for build '{0}', level '{1}' and map area '{2}' doesn't yet exist.  " + 
                    "Has the map hierarchy been created?", build.Identifier, level.Name, mapArea.Name), HttpStatusCode.BadRequest);
            }

            // Update and save the data.
            mapAreaStat.Update(mapAreaStatBundle);
            locator.Save(mapAreaStat);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="mapSectionHash"></param>
        /// <param name="mapSectionStatBundle"></param>
        private void UpdateMapSectionStatCommand(IRepositoryLocator locator, String buildIdentifier, String levelHash, String mapSectionHash, MapSectionStatBundleDto mapSectionStatBundle)
        {
            // Get db objects for the passed in items
            Build build = GetAssetByIdentifier<Build>(locator, buildIdentifier);
            Level level = GetAssetByIdentifier<Level>(locator, levelHash);
            MapSection mapSection = GetAssetByIdentifier<MapSection>(locator, mapSectionHash);

            // Get the map section stat for the filter combination and make sure it exists.
            // It should have been created via the CreateMapHierarchy endpoint prior to this being called.
            MapSectionStat mapSectionStat = GetMapSectionStat(locator, build, level, mapSection);
            if (mapSectionStat == null)
            {
                throw new WebFaultException<String>(String.Format("Map section stat for build '{0}', level '{1}' and map section '{2}' doesn't yet exist.  " +
                    "Has the map hierarchy been created?", build.Identifier, level.Name, mapSection.Name), HttpStatusCode.BadRequest);
            }

            // Update and save the data.
            mapSectionStat.Update(mapSectionStatBundle);

            // The following is done externally from the update method as it requires the repository locator which isn't available in the RSG.Statistics.Domain assembly.
            List<MapSectionPlatformStat> platformStats = new List<MapSectionPlatformStat>();
            foreach (MapSectionPlatformStatDto platformStatDto in mapSectionStatBundle.PlatformStats)
            {
                EnumReference<RSG.Platform.Platform> platform = GetEnumReferenceByValue(locator, platformStatDto.Platform.Value);

                foreach (MapSectionMemoryStatDto memoryStatDto in platformStatDto.FileTypeSizes)
                {
                    MapSectionPlatformStat memoryStat = new MapSectionPlatformStat();
                    memoryStat.Platform = platform;
                    memoryStat.FileType = GetEnumReferenceByValue(locator, memoryStatDto.FileType.Value);
                    memoryStat.MapSectionStat = mapSectionStat;
                    memoryStat.PhysicalSize = memoryStatDto.MemorySat.PhysicalSize;
                    memoryStat.VirtualSize = memoryStatDto.MemorySat.VirtualSize;
                    platformStats.Add(memoryStat);
                }
            }
            mapSectionStat.MapSectionPlatformStats = platformStats;
            
            // Handle aggregate stats next
            List<MapSectionAggregateStat> aggregateStats = new List<MapSectionAggregateStat>();
            foreach (MapSectionAggregateStatDto aggregateStatDto in mapSectionStatBundle.AggregatedStats)
            {
                EnumReference<StatLodLevel> lodLevel = GetEnumReferenceByValue(locator, aggregateStatDto.LodLevel);
                EnumReference<StatGroup> group = GetEnumReferenceByValue(locator, aggregateStatDto.Group);
                EnumReference<StatEntityType> entityType = GetEnumReferenceByValue(locator, aggregateStatDto.EntityType);

                MapSectionAggregateStat aggregateStat = Mapper.Map<MapSectionAggregateStatDto, MapSectionAggregateStat>(aggregateStatDto);
                aggregateStat.MapSectionStat = mapSectionStat;
                aggregateStat.LodLevel = lodLevel;
                aggregateStat.Group = group;
                aggregateStat.EntityType = entityType;

                // Collision Types
                aggregateStat.CollisionTypePolygonCounts = new List<CollisionPolyStat<MapSectionAggregateStat>>();
                foreach (CollisionPolyStatDto polyStatDto in aggregateStatDto.CollisionTypePolygonCounts)
                {
                    aggregateStat.CollisionTypePolygonCounts.Add(new CollisionPolyStat<MapSectionAggregateStat>(aggregateStat, polyStatDto.CollisionFlags, polyStatDto.PolygonCount));
                }
                aggregateStats.Add(aggregateStat);
            }
            mapSectionStat.AggregatedStats = aggregateStats;

            locator.Save(mapSectionStat);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="build"></param>
        /// <param name="level"></param>
        /// <param name="section"></param>
        /// <returns></returns>
        private MapSectionStat GetMapSectionStat(IRepositoryLocator locator, Build build, Level level, MapSection section)
        {
            DetachedCriteria query = DetachedCriteria.For<MapSectionStat>();
            query.Add(Expression.Eq("Build", build));
            query.Add(Expression.Eq("Level", level));
            query.Add(Expression.Eq("MapSection", section));
            return locator.FindFirst<MapSectionStat>(query);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="mapSectionHash"></param>
        /// <param name="bundle"></param>
        private void CreateArchetypeStatCommand(IRepositoryLocator locator, String buildIdentifier, String levelHash, String mapSectionHash, ArchetypeStatBundle bundle)
        {
            Build build = GetAssetByIdentifier<Build>(locator, buildIdentifier);
            Level level = GetAssetByIdentifier<Level>(locator, levelHash);
            MapSection mapSection = GetAssetByIdentifier<MapSection>(locator, mapSectionHash);

            // This is slightly awkward due to the possibility of having archetypes with the same name but different types
            Archetype archetype = null;
            if (bundle is DrawableArchetypeStatBundle)
            {
                archetype = GetAssetByIdentifier<DrawableArchetype>(locator, bundle.ArchetypeIdentifier);
            }
            else if (bundle is FragmentArchetypeStatBundle)
            {
                archetype = GetAssetByIdentifier<FragmentArchetype>(locator, bundle.ArchetypeIdentifier);
            }
            else if (bundle is InteriorArchetypeStatBundle)
            {
                archetype = GetAssetByIdentifier<InteriorArchetype>(locator, bundle.ArchetypeIdentifier);
            }
            else if (bundle is StatedAnimArchetypeStatBundle)
            {
                archetype = GetAssetByIdentifier<StatedAnimArchetype>(locator, bundle.ArchetypeIdentifier);
            }
            else
            {
                Debug.Assert(false, "Unknown archetype stat bundle!");
                throw new WebFaultException<String>(String.Format("Unknown archetype stat bundle type '{0}' encountered while creating an archetype stat.", bundle.GetType().FullName), HttpStatusCode.BadRequest);
            }

            // Get the map section stat these archetype stats will be associated with.
            DetachedCriteria query = DetachedCriteria.For<MapSectionStat>();
            query.Add(Expression.Eq("Build", build));
            query.Add(Expression.Eq("Level", level));
            query.Add(Expression.Eq("MapSection", mapSection));
            MapSectionStat mapSectionStat = locator.FindFirst<MapSectionStat>(query);

            // Make sure the stat exists.  It should have been created via the CreateMapHierarchy endpoint prior to this being called.
            if (mapSectionStat == null)
            {
                throw new WebFaultException<String>(String.Format("Map section stat for build '{0}', level '{1}' and map section '{2}' doesn't yet exist.  " +
                    "Has the map hierarchy been created?", build.Identifier, level.Name, mapSection.Name), HttpStatusCode.BadRequest);
            }

            // Check whether the archetype stat already exists
            query = DetachedCriteria.For<ArchetypeStat>();
            query.Add(Expression.Eq("Build", build));
            query.Add(Expression.Eq("MapSectionStat", mapSectionStat));
            query.Add(Expression.Eq("Archetype", archetype));
            ArchetypeStat archetypeStat = locator.FindFirst<ArchetypeStat>(query);
            if (archetypeStat != null)
            {
                // Delete the stat if it exists
                locator.Delete(archetypeStat);
            }

            // Create any shaders/textures this bundle contains.
            CreateShadersAndTextures(locator, bundle);

            // Create the new archetype stat based on the type of archetype we have.
            ArchetypeStat stat = Mapper.Map<ArchetypeStatBundle, ArchetypeStat>(bundle);
            stat.Build = build;
            stat.Archetype = archetype;
            stat.MapSectionStat = mapSectionStat;
            stat.BoundingBoxMin = new Point(new Coordinate(bundle.BoundingBox.Min.X, bundle.BoundingBox.Min.Y, bundle.BoundingBox.Min.Z));
            stat.BoundingBoxMax = new Point(new Coordinate(bundle.BoundingBox.Max.X, bundle.BoundingBox.Max.Y, bundle.BoundingBox.Max.Z));

            // Txd sizes
            stat.TxdExportSizes = new List<TxdExportSizeStat<ArchetypeStat>>();
            foreach (TxdExportStatDto txdStatDto in bundle.TxdExportSizes)
            {
                stat.TxdExportSizes.Add(new TxdExportSizeStat<ArchetypeStat>(stat, txdStatDto.Name, txdStatDto.Size));
            }

            // Collision Types
            stat.CollisionTypePolygonCounts = new List<CollisionPolyStat<ArchetypeStat>>();
            foreach (CollisionPolyStatDto polyStatDto in bundle.CollisionTypePolygonCounts)
            {
                stat.CollisionTypePolygonCounts.Add(new CollisionPolyStat<ArchetypeStat>(stat, polyStatDto.CollisionFlags, polyStatDto.PolygonCount));
            }

            // Do some additional processing for the various types.
            if (bundle is SimpleArchetypeStatBundle)
            {
                SimpleArchetypeStatBundle simpleBundle = bundle as SimpleArchetypeStatBundle;
                SimpleMapArchetypeStat simpleStat = stat as SimpleMapArchetypeStat;

                simpleStat.DrawableLods = new List<DrawableLodStat>();
                foreach (DrawableLodStatDto lodStatDto in simpleBundle.DrawableLods)
                {
                    DrawableLodStat lodStat = Mapper.Map<DrawableLodStatDto, DrawableLodStat>(lodStatDto);
                    lodStat.ArchetypeStat = simpleStat;
                    lodStat.LodLevel = GetEnumReferenceByValue(locator, lodStatDto.LodLevel);
                    simpleStat.DrawableLods.Add(lodStat);
                }
            }
            else if (bundle is InteriorArchetypeStatBundle)
            {
                InteriorArchetypeStatBundle interiorBundle = bundle as InteriorArchetypeStatBundle;
                InteriorArchetypeStat interiorStat = stat as InteriorArchetypeStat;

                // Handle rooms first keeping track of those we created for linking portals.
                IDictionary<string, RoomStat> roomStatsMap = new Dictionary<string, RoomStat>();

                interiorStat.RoomStats = new List<RoomStat>();
                foreach (RoomStatBundle roomBundle in interiorBundle.Rooms)
                {
                    RoomStat roomStat = Mapper.Map<RoomStatBundle, RoomStat>(roomBundle);
                    roomStat.Build = build;
                    roomStat.InteriorStat = interiorStat;
                    roomStat.Room = GetAssetByIdentifier<Room>(locator, roomBundle.RoomIdentifier);
                    roomStat.BoundingBoxMin = new Point(new Coordinate(roomBundle.BoundingBox.Min.X, roomBundle.BoundingBox.Min.Y, roomBundle.BoundingBox.Min.Z));
                    roomStat.BoundingBoxMax = new Point(new Coordinate(roomBundle.BoundingBox.Max.X, roomBundle.BoundingBox.Max.Y, roomBundle.BoundingBox.Max.Z));

                    // Create any shaders/textures this bundle contains
                    // (technically the archetype should contain all of these already).
                    CreateShadersAndTextures(locator, roomBundle);

                    // Txd sizes
                    roomStat.TxdExportSizes = new List<TxdExportSizeStat<RoomStat>>();
                    foreach (TxdExportStatDto txdStatDto in bundle.TxdExportSizes)
                    {
                        roomStat.TxdExportSizes.Add(new TxdExportSizeStat<RoomStat>(roomStat, txdStatDto.Name, txdStatDto.Size));
                    }

                    // Collision Types
                    roomStat.CollisionTypePolygonCounts = new List<CollisionPolyStat<RoomStat>>();
                    foreach (CollisionPolyStatDto polyStatDto in bundle.CollisionTypePolygonCounts)
                    {
                        roomStat.CollisionTypePolygonCounts.Add(new CollisionPolyStat<RoomStat>(roomStat, polyStatDto.CollisionFlags, polyStatDto.PolygonCount));
                    }

                    // Create/link the shader stats
                    LinkShaderStatsToGameAssetStat(locator, roomStat, roomBundle);

                    interiorStat.RoomStats.Add(roomStat);

                    // Keep track of the new room stat for the portals.
                    roomStatsMap.Add(roomBundle.RoomIdentifier, roomStat);
                }

                // Handle portals next.
                interiorStat.PortalStats = new List<PortalStat>();
                foreach (OldPortalStatDto portalDto in interiorBundle.Portals)
                {
                    PortalStat portalStat = new PortalStat();
                    portalStat.Name = portalDto.Name;
                    portalStat.InteriorStat = interiorStat;
                    if (!String.IsNullOrEmpty(portalDto.RoomAIdentifier) && roomStatsMap.ContainsKey(portalDto.RoomAIdentifier))
                    {
                        portalStat.RoomStatA = roomStatsMap[portalDto.RoomAIdentifier];
                    }
                    if (!String.IsNullOrEmpty(portalDto.RoomBIdentifier) && roomStatsMap.ContainsKey(portalDto.RoomBIdentifier))
                    {
                        portalStat.RoomStatB = roomStatsMap[portalDto.RoomBIdentifier];
                    }

                    interiorStat.PortalStats.Add(portalStat);
                }
            }
            else if (bundle is StatedAnimArchetypeStatBundle)
            {
                StatedAnimArchetypeStatBundle statedAnimBundle = bundle as StatedAnimArchetypeStatBundle;
                StatedAnimArchetypeStat statedAnimStat = stat as StatedAnimArchetypeStat;

                statedAnimStat.ComponentArchetypeStats = new List<ArchetypeStat>();
                foreach (string archetypeIdentifier in statedAnimBundle.ComponentArchetypeIdentifiers)
                {
                    // Hmmm...will this work...
                    Archetype componentArchetype = GetAssetByIdentifier<Archetype>(locator, archetypeIdentifier);

                    query = DetachedCriteria.For<ArchetypeStat>();
                    query.Add(Expression.Eq("Build", build));
                    query.Add(Expression.Eq("Archetype", componentArchetype));
                    ArchetypeStat componentStat = locator.FindFirst<ArchetypeStat>(query);

                    if (componentStat != null)
                    {
                        statedAnimStat.ComponentArchetypeStats.Add(componentStat);
                    }
                }
            }

            // Create/link the shader stats
            LinkShaderStatsToGameAssetStat(locator, stat, bundle);

            // Finally save out the stat (this will in turn save any linked objects set to cascade).
            locator.Save(stat);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="mapSectionHash"></param>
        /// <param name="bundle"></param>
        private void CreateEntityStatCommand(IRepositoryLocator locator, String buildIdentifier, String levelHash, String mapSectionHash, EntityStatBundle bundle)
        {
            Build build = GetAssetByIdentifier<Build>(locator, buildIdentifier);
            Level level = GetAssetByIdentifier<Level>(locator, levelHash);
            MapSection mapSection = GetAssetByIdentifier<MapSection>(locator, mapSectionHash);
            Entity entity = GetAssetByIdentifier<Entity>(locator, bundle.EntityIdentifier);

            // This is slightly awkward due to the possibility of having archetypes with the same name/identifier but different types
            Archetype archetype = null;
            if (bundle.ArchetypeType == typeof(DrawableArchetype).Name)
            {
                archetype = GetAssetByIdentifier<DrawableArchetype>(locator, bundle.ArchetypeIdentifier);
            }
            else if (bundle.ArchetypeType == typeof(FragmentArchetype).Name)
            {
                archetype = GetAssetByIdentifier<FragmentArchetype>(locator, bundle.ArchetypeIdentifier);
            }
            else if (bundle.ArchetypeType == typeof(InteriorArchetype).Name)
            {
                archetype = GetAssetByIdentifier<InteriorArchetype>(locator, bundle.ArchetypeIdentifier);
            }
            else if (bundle.ArchetypeType == typeof(StatedAnimArchetype).Name)
            {
                archetype = GetAssetByIdentifier<StatedAnimArchetype>(locator, bundle.ArchetypeIdentifier);
            }
            else
            {
                Debug.Assert(false, "Unknown archetype type in bundle!");
                throw new WebFaultException<String>(String.Format("Unknown archetype type '{0}' encountered in bundle while creating an entity stat.", bundle.ArchetypeType), HttpStatusCode.BadRequest);
            }

            // Get the map section stat these archetype stats will be associated with.
            DetachedCriteria query = DetachedCriteria.For<ArchetypeStat>();
            query.Add(Expression.Eq("Build", build));
            query.Add(Expression.Eq("Archetype", archetype));
            ArchetypeStat archetypeStat = locator.FindFirst<ArchetypeStat>(query);

            if (archetypeStat == null)
            {
                throw new WebFaultException<String>(String.Format("Archetype stat for build '{0}', map section '{1}' and archetype '{2}' doesn't yet exist.  " +
                    "Have the archetypes been synced?", build.Identifier, mapSection.Name, archetype.Name), HttpStatusCode.BadRequest);
            }

            // Get the parent stat object
            MapSectionStat parentSectionStat = null;
            InteriorArchetypeStat parentInteriorStat = null;
            RoomStat parentRoomStat = null;

            if (bundle.ParentType == "MapSection")
            {
                query = DetachedCriteria.For<MapSectionStat>();
                query.Add(Expression.Eq("Build", build));
                query.Add(Expression.Eq("Level", level));
                query.Add(Expression.Eq("MapSection", mapSection));
                parentSectionStat = locator.FindFirst<MapSectionStat>(query);

                // Make sure the stat exists.  It should have been created via the CreateMapHierarchy endpoint prior to this being called.
                if (parentSectionStat == null)
                {
                    throw new WebFaultException<String>(String.Format("Map section stat for build '{0}' and map section '{1}' doesn't yet exist.  " +
                        "Has the map hierarchy been created?", build.Identifier, mapSection.Name), HttpStatusCode.BadRequest);
                }
            }
            else if (bundle.ParentType == "InteriorArchetype")
            {
                InteriorArchetype interiorArchetype = GetAssetByIdentifier<InteriorArchetype>(locator, bundle.ParentIdentifier);

                query = DetachedCriteria.For<ArchetypeStat>();
                query.Add(Expression.Eq("Build", build));
                query.Add(Expression.Eq("Archetype", interiorArchetype));
                parentInteriorStat = locator.FindFirst<InteriorArchetypeStat>(query);

                if (parentInteriorStat == null)
                {
                    throw new WebFaultException<String>(String.Format("Interior archetype stat for build '{0}' and interior archetype '{1}' doesn't yet exist.  " +
                        "Have the archetypes stats been created yet?", build.Identifier, interiorArchetype.Name), HttpStatusCode.BadRequest);
                }
            }
            else if (bundle.ParentType == "Room")
            {
                Room room = GetAssetByIdentifier<Room>(locator, bundle.ParentIdentifier);

                query = DetachedCriteria.For<RoomStat>();
                query.Add(Expression.Eq("Build", build));
                query.Add(Expression.Eq("Room", room));
                parentRoomStat = locator.FindFirst<RoomStat>(query);

                if (parentRoomStat == null)
                {
                    throw new WebFaultException<String>(String.Format("Interior archetype stat for build '{0}' and room '{1}' doesn't yet exist.  " +
                        "Have the archetypes stats been created yet?", build.Identifier, room.Name), HttpStatusCode.BadRequest);
                }
            }
            else
            {
                Debug.Assert(false, "Unknown parent type in bundle!");
                throw new WebFaultException<String>(String.Format("Unknown parent type '{0}' encountered in bundle while creating an entity stat.", bundle.ParentType), HttpStatusCode.BadRequest);
            }

            // Check whether the entity stat already exists
            query = DetachedCriteria.For<EntityStat>();
            query.Add(Expression.Eq("Entity", entity));
            query.Add(Expression.Eq("ArchetypeStat", archetypeStat));
            query.Add(Expression.Eq("MapSectionStat", parentSectionStat));
            query.Add(Expression.Eq("InteriorStat", parentInteriorStat));
            query.Add(Expression.Eq("RoomStat", parentRoomStat));
            EntityStat entityStat = locator.FindFirst<EntityStat>(query);
            if (entityStat != null)
            {
                // Delete the stat if it exists
                locator.Delete(entityStat);
            }

            // Create the new entity stat
            EntityStat stat = Mapper.Map<EntityStatBundle, EntityStat>(bundle);
            stat.Build = build;
            stat.Entity = entity;
            stat.ArchetypeStat = archetypeStat;
            stat.MapSectionStat = parentSectionStat;
            stat.InteriorStat = parentInteriorStat;
            stat.RoomStat = parentRoomStat;
            stat.Location = new Point(new Coordinate(bundle.Position.X, bundle.Position.Y, bundle.Position.Z));
            if (bundle.BoundingBox != null)
            {
                stat.BoundingBoxMin = new Point(new Coordinate(bundle.BoundingBox.Min.X, bundle.BoundingBox.Min.Y, bundle.BoundingBox.Min.Z));
                stat.BoundingBoxMax = new Point(new Coordinate(bundle.BoundingBox.Max.X, bundle.BoundingBox.Max.Y, bundle.BoundingBox.Max.Z));
            }
            stat.LodLevel = GetEnumReferenceByValue<RSG.Model.Common.Map.LodLevel>(locator, bundle.LodLevel.Value);

            // Get the parent entity stat
            EntityStat parentEntityStat = null;
            if (!String.IsNullOrEmpty(bundle.LodParentEntityIdentifier))
            {
                Entity parentEntity = GetAssetByIdentifier<Entity>(locator, bundle.LodParentEntityIdentifier);

                query = DetachedCriteria.For<EntityStat>();
                query.Add(Expression.Eq("Entity", parentEntity));
                // Don't need to check the archetype stat at this point. 
                query.Add(Expression.Eq("MapSectionStat", parentSectionStat));
                query.Add(Expression.Eq("InteriorStat", parentInteriorStat));
                query.Add(Expression.Eq("RoomStat", parentRoomStat));
                parentEntityStat = locator.FindFirst<EntityStat>(query);
            }
            stat.LodParent = parentEntityStat;
            
            // Finally save out the stat
            locator.Save(stat);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="mapSectionHash"></param>
        /// <param name="carGenDto"></param>
        private void CreateCarGenStatCommand(IRepositoryLocator locator, String buildIdentifier, String levelHash, String mapSectionHash, CarGenStatDto carGenDto)
        {
            // Get db objects for the passed in items
            Build build = GetAssetByIdentifier<Build>(locator, buildIdentifier);
            Level level = GetAssetByIdentifier<Level>(locator, levelHash);
            MapSection mapSection = GetAssetByIdentifier<MapSection>(locator, mapSectionHash);
            
            // Get the map section stat for the filter combination.
            DetachedCriteria query = DetachedCriteria.For<MapSectionStat>();
            query.Add(Expression.Eq("Build", build));
            query.Add(Expression.Eq("Level", level));
            query.Add(Expression.Eq("MapSection", mapSection));
            MapSectionStat mapSectionStat = locator.FindFirst<MapSectionStat>(query);

            // Make sure the stat exists.  It should have been created via the CreateMapHierarchy endpoint prior to this being called.
            if (mapSectionStat == null)
            {
                throw new WebFaultException<String>(String.Format("Map section stat for build '{0}', level '{1}' and map section '{2}' doesn't yet exist.  " +
                    "Has the map hierarchy been created?", build.Identifier, level.Name, mapSection.Name), HttpStatusCode.BadRequest);
            }

            // Create the new car gen stat and save it to the database.
            CarGenStat carGenStat = Mapper.Map<CarGenStatDto, CarGenStat>(carGenDto);
            if (carGenDto.Position != null)
                carGenStat.Position = new Point(new Coordinate(carGenDto.Position.X, carGenDto.Position.Y, carGenDto.Position.Z));
            carGenStat.MapSectionStat = mapSectionStat;
            locator.Save(carGenStat);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="dto"></param>
        private void CreateSpawnPointStatCommand(IRepositoryLocator locator, String buildIdentifier, String levelHash, SpawnPointStatDto dto)
        {
            Debug.Assert(dto.ArchetypeIdentifier != null || dto.LevelIdentifier != null, "Neither archetype nor level identifier provided when attempting to create a spawn point stat.");
            if (dto.ArchetypeIdentifier == null && dto.LevelIdentifier == null)
            {
                throw new WebFaultException<String>("Neither the archetype nor level identifier was provided when attempting to create a spawn point stat.", HttpStatusCode.BadRequest);
            }

            // Get db objects for the passed in items
            Build build = GetAssetByIdentifier<Build>(locator, buildIdentifier);

            // Get the stat that this will be associated with
            LevelStat levelStat = null;
            ArchetypeStat archetypeStat = null;

            if (dto.ArchetypeIdentifier != null)
            {
                Archetype archetype = GetAssetByIdentifier<Archetype>(locator, dto.ArchetypeIdentifier);

                DetachedCriteria query = DetachedCriteria.For<ArchetypeStat>();
                query.Add(Expression.Eq("Build", build));
                query.Add(Expression.Eq("Archetype", archetype));
                archetypeStat = locator.FindFirst<ArchetypeStat>(query);

                if (archetypeStat == null)
                {
                    throw new WebFaultException<String>(String.Format("Archetype stat for build '{0}' and archetype '{1}' doesn't yet exist.  " +
                        "Have the archetype stats been populated?", build.Identifier, archetype.Name), HttpStatusCode.BadRequest);
                }
            }
            else if (dto.LevelIdentifier != null)
            {
                Level level = GetAssetByIdentifier<Level>(locator, levelHash);

                DetachedCriteria query = DetachedCriteria.For<LevelStat>();
                query.Add(Expression.Eq("Build", build));
                query.Add(Expression.Eq("Level", level));
                levelStat = locator.FindFirst<LevelStat>(query);

                if (levelStat == null)
                {
                    throw new WebFaultException<String>(String.Format("Level stat for build '{0}' and level '{1}' doesn't yet exist.  " +
                        "Have the level stats been populated?", build.Identifier, level.Name), HttpStatusCode.BadRequest);
                }
            }

            SpawnPointStat stat = Mapper.Map<SpawnPointStatDto, SpawnPointStat>(dto);
            stat.ArchetypeStat = archetypeStat;
            stat.LevelStat = levelStat;
            if (dto.Position != null)
            {
                stat.Position = new Point(new Coordinate(dto.Position.X, dto.Position.Y, dto.Position.Z));
            }
            if (dto.AvailabilityMode != null)
            {
                stat.AvailabilityMode = GetEnumReferenceByValue(locator, (SpawnPointAvailableModes)dto.AvailabilityMode);
            }
            locator.Save(stat);
        }
        #endregion // Data Submission

        #region Data Retrieval
        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <returns></returns>
        private MapHierarchyBundleDto GetMapHierarchyCommand(IRepositoryLocator locator, String buildIdentifier, String levelHash)
        {
            Build build = GetAssetByIdentifier<Build>(locator, buildIdentifier);
            Level level = GetAssetByIdentifier<Level>(locator, levelHash);

            // Do 2 queries. One for map areas and one for map sections.
            DetachedCriteria areaQuery = DetachedCriteria.For<MapAreaStat>()
                                                         .CreateAlias("MapArea", "ma")
                                                         .CreateAlias("ParentAreaStat", "pas", JoinType.LeftOuterJoin)
                                                         .SetProjection(Projections.ProjectionList()
                                                            .Add(Projections.Property("ma.Name"), "Name")
                                                            .Add(Projections.Property("ma.Identifier"), "IdentifierString")
                                                            .Add(Projections.Property("Id"), "Id")
                                                            .Add(Projections.Property("pas.Id"), "ParentId")
                                                         )
                                                         .Add(Expression.Eq("Build", build))
                                                         .Add(Expression.Eq("Level", level))
                                                         .SetResultTransformer(Transformers.AliasToBean(typeof(BasicMapAreaStatDto)));
            IList<BasicMapAreaStatDto> mapAreaDtos = locator.FindAll<MapAreaStat, BasicMapAreaStatDto>(areaQuery);

            // Retrieve the section stats.
            DetachedCriteria sectionQuery = DetachedCriteria.For<MapSectionStat>()
                                                            .SetFetchMode("MapSection", FetchMode.Join)
                                                            .Add(Expression.Eq("Build", build))
                                                            .Add(Expression.Eq("Level", level));
            IList<MapSectionStat> mapSectionStats = locator.FindAll<MapSectionStat>(sectionQuery);

            // Convert he map section stats to dtos
            IList<BasicMapSectionStatDto> mapSectionDtos = new List<BasicMapSectionStatDto>();

            foreach (MapSectionStat mapSectionStat in mapSectionStats)
            {
                BasicMapSectionStatDto mapSectionDto = Mapper.Map<MapSectionStat, BasicMapSectionStatDto>(mapSectionStat);
                mapSectionDto.ParentId = (mapSectionStat.ParentAreaStat != null ? (long?)mapSectionStat.ParentAreaStat.Id : null);

                if (mapSectionStat.VectorMapPoints != null)
                {
                    mapSectionDto.VectorMapPoints = mapSectionStat.VectorMapPoints.Shell.Coordinates.Select(item => new Vector2f((float)item.X, (float)item.Y)).ToList();

                    // Remove the last point as its a dupe of the first one.
                    if (mapSectionDto.VectorMapPoints.Any())
                    {
                        mapSectionDto.VectorMapPoints.RemoveAt(mapSectionDto.VectorMapPoints.Count - 1);
                    }
                }
                else
                {
                    mapSectionDto.VectorMapPoints = new List<Vector2f>();
                }

                mapSectionDtos.Add(mapSectionDto);
            }

            // Build the hierarchy from the retrieved data
            MapHierarchyBundleDto bundle = new MapHierarchyBundleDto();
            //bundle.ChildNodes = new List<BasicMapNodeStatBaseDto>();
            bundle.ChildNodes.AddRange(mapAreaDtos.Where(item => item.ParentId == null));
            bundle.ChildNodes.AddRange(mapSectionDtos.Where(item => item.ParentId == null));

            IDictionary<long, BasicMapAreaStatDto> mapAreaDtoLookup = mapAreaDtos.ToDictionary(item => item.Id);

            foreach (BasicMapAreaStatDto mapAreaDto in mapAreaDtos.Where(item => item.ParentId != null))
            {
                if (mapAreaDtoLookup.ContainsKey(mapAreaDto.ParentId.Value))
                {
                    mapAreaDtoLookup[mapAreaDto.ParentId.Value].ChildNodes.Add(mapAreaDto);
                }
                else
                {
                    Debug.Assert(false, "Parent map area stat dto doesn't exist in the lookup!");
                }
            }

            foreach (BasicMapSectionStatDto mapSectionDto in mapSectionDtos.Where(item => item.ParentId != null))
            {
                if (mapAreaDtoLookup.ContainsKey(mapSectionDto.ParentId.Value))
                {
                    mapAreaDtoLookup[mapSectionDto.ParentId.Value].ChildNodes.Add(mapSectionDto);
                }
                else
                {
                    Debug.Assert(false, "Parent map area stat dto doesn't exist in the lookup!");
                }
            }

            return bundle;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="fetchDto"></param>
        private List<MapSectionStatDto> GetMultipleMapSectionStatsCommand(IRepositoryLocator locator, String buildIdentifier, String levelHash, StreamableStatFetchDto fetchDto)
        {
            // Get db objects for the passed in items
            Build build = GetAssetByIdentifier<Build>(locator, buildIdentifier);
            Level level = GetAssetByIdentifier<Level>(locator, levelHash);

            // Generate the projection list dynamically based off of the requested stats.
            ProjectionList projList = CreateProjectionListFromRequestedStats(typeof(MapSectionStat), typeof(MapSectionStatDto), fetchDto.RequestedStats);
            projList = projList.Add(Projections.Property("section.Identifier"), "IdentifierString")
                               .Add(Projections.Property("Id"), "Id");

            DetachedCriteria criteria = DetachedCriteria.For<MapSectionStat>()
                                                         .CreateAlias("MapSection", "section")
                                                         .SetProjection(projList)
                                                         .Add(Expression.Eq("Build", build))
                                                         .Add(Expression.Eq("Level", level))
                                                         .Add(Restrictions.In("section.Identifier", fetchDto.AssetIdentifiers))
                                                         .SetResultTransformer(Transformers.AliasToBean(typeof(MapSectionStatDto)));
            IList<MapSectionStatDto> dtos = locator.FindAll<MapSectionStat, MapSectionStatDto>(criteria);

            // Were any complex stat types requested?
            IDictionary<StreamableStat, PropertyInfo> dbLookup = StreamableStatUtils.GetPropertyInfoLookup(typeof(MapSectionStat));

            foreach (StreamableStat stat in fetchDto.RequestedStats)
            {
                if (dbLookup.ContainsKey(stat))
                {
                    if (stat.DbToDtoRequiresProcessing)
                    {
                        ProcessComplexMapSectionStat(locator, dtos, stat);
                    }
                }
            }

            return dtos.ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="fetchDto"></param>
        /// <returns></returns>
        private List<ArchetypeStatDto> GetMultipleArchetypeStatsCommand(IRepositoryLocator locator, String buildIdentifier, String levelHash, StreamableStatFetchDto fetchDto)
        {
            // Get db objects for the passed in items
            Build build = GetAssetByIdentifier<Build>(locator, buildIdentifier);
            Level level = GetAssetByIdentifier<Level>(locator, levelHash);

            //
            DetachedCriteria criteria = DetachedCriteria.For<ArchetypeStat>()
                                                        .CreateAlias("Archetype", "arch")
                                                        .CreateAlias("MapSectionStat", "mss")
                                                        .Add(Expression.Eq("Build", build))
                                                        .Add(Expression.Eq("mss.Level", level))
                                                        .Add(Restrictions.In("arch.Identifier", fetchDto.AssetIdentifiers));
            IList<ArchetypeStat> stats = locator.FindAll<ArchetypeStat>(criteria);

            // Convert the retrieved stats to dtos.
            List<ArchetypeStatDto> dtos = new List<ArchetypeStatDto>();

            foreach (ArchetypeStat stat in stats)
            {
                MapArchetypeStatDto dto;

                // Add the appropriate type of dto.
                if (stat is DrawableArchetypeStat)
                {
                    dto = new DrawableArchetypeStatDto();
                }
                else if (stat is FragmentArchetypeStat)
                {
                    dto = new FragmentArchetypeStatDto();
                }
                else if (stat is InteriorArchetypeStat)
                {
                    dto = new InteriorArchetypeStatDto();
                }
                else if (stat is StatedAnimArchetypeStat)
                {
                    dto = new StatedAnimArchetypeStatDto();
                }
                else
                {
                    throw new ArgumentException("Unknown type encountered while retrieving archetype stats.");
                }

                // Copy across the requested data.
                dto.Id = stat.Id;
                dto.IdentifierString = stat.Archetype.Identifier;
                CopySpecificStats(stat, dto, fetchDto.RequestedStats);

                if (fetchDto.RequestedStats.Contains(StreamableMapArchetypeStat.BoundingBox))
                {
                    Vector3f min = new Vector3f((float)stat.BoundingBoxMin.X, (float)stat.BoundingBoxMin.Y, (float)stat.BoundingBoxMin.Z);
                    Vector3f max = new Vector3f((float)stat.BoundingBoxMax.X, (float)stat.BoundingBoxMax.Y, (float)stat.BoundingBoxMax.Z);
                    dto.BoundingBox = new BoundingBox3f(min, max);
                }

                // Add the dto to the list.
                dtos.Add(dto);
            }

            // Were any complex stat types requested?
            IDictionary<StreamableStat, PropertyInfo> dbLookup = CreateArchetypeDbPropertyInfoLookup();

            foreach (StreamableStat stat in fetchDto.RequestedStats)
            {
                if (dbLookup.ContainsKey(stat))
                {
                    if (stat.DbToDtoRequiresProcessing)
                    {
                        ProcessComplexArchetypeStats(locator, dtos, stat);
                    }
                }
            }

            return dtos;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="fetchDto"></param>
        /// <returns></returns>
        private List<EntityStatDto> GetMultipleEntityStatsCommand(IRepositoryLocator locator, String buildIdentifier, String levelHash, StreamableStatFetchDto fetchDto)
        {
            // Get db objects for the passed in items
            Build build = GetAssetByIdentifier<Build>(locator, buildIdentifier);
            Level level = GetAssetByIdentifier<Level>(locator, levelHash);

            DetachedCriteria criteria = DetachedCriteria.For<EntityStat>()
                                                         .CreateAlias("Entity", "ent")
                                                         .CreateAlias("MapSectionStat", "mss")
                                                         .Add(Expression.Eq("Build", build))
                                                         .Add(Expression.Eq("mss.Level", level))
                                                         .Add(Restrictions.In("ent.Identifier", fetchDto.AssetIdentifiers));
            IList<EntityStat> stats = locator.FindAll<EntityStat>(criteria);

            // Convert the db objects to domain objects
            List<EntityStatDto> dtos = new List<EntityStatDto>();

            foreach (EntityStat stat in stats)
            {
                EntityStatDto dto = new EntityStatDto();
                dto.IdentifierString = stat.Entity.Identifier;
                CopySpecificStats(stat, dto, fetchDto.RequestedStats);
                ProcessComplexEntityStats(stat, dto, fetchDto.RequestedStats);
                dtos.Add(dto);
            }

            return dtos;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="fetchDto"></param>
        /// <returns></returns>
        private List<RoomStatDto> GetMultipleRoomStatsCommand(IRepositoryLocator locator, String buildIdentifier, String levelHash, StreamableStatFetchDto fetchDto)
        {
            // Get db objects for the passed in items
            Build build = GetAssetByIdentifier<Build>(locator, buildIdentifier);
            Level level = GetAssetByIdentifier<Level>(locator, levelHash);

            DetachedCriteria criteria = DetachedCriteria.For<RoomStat>()
                                                         .CreateAlias("Room", "room")
                                                         .CreateAlias("InteriorStat", "is")
                                                         .CreateAlias("is.MapSectionStat", "mss")
                                                         .Add(Expression.Eq("Build", build))
                                                         .Add(Expression.Eq("mss.Level", level))
                                                         .Add(Restrictions.In("room.Identifier", fetchDto.AssetIdentifiers));
            IList<RoomStat> stats = locator.FindAll<RoomStat>(criteria);

            // Convert the db objects to domain objects
            List<RoomStatDto> dtos = new List<RoomStatDto>();

            foreach (RoomStat stat in stats)
            {
                RoomStatDto dto = new RoomStatDto();
                dto.IdentifierString = stat.Room.Identifier;
                dto.Id = stat.Id;
                CopySpecificStats(stat, dto, fetchDto.RequestedStats);

                if (fetchDto.RequestedStats.Contains(StreamableRoomStat.BoundingBox))
                {
                    Vector3f min = new Vector3f((float)stat.BoundingBoxMin.X, (float)stat.BoundingBoxMin.Y, (float)stat.BoundingBoxMin.Z);
                    Vector3f max = new Vector3f((float)stat.BoundingBoxMax.X, (float)stat.BoundingBoxMax.Y, (float)stat.BoundingBoxMax.Z);
                    dto.BoundingBox = new BoundingBox3f(min, max);
                }

                dtos.Add(dto);
            }

            // Were any complex stat types requested?
            IDictionary<StreamableStat, PropertyInfo> dbLookup = StreamableStatUtils.GetPropertyInfoLookup(typeof(RoomStat));

            foreach (StreamableStat stat in fetchDto.RequestedStats)
            {
                if (dbLookup.ContainsKey(stat))
                {
                    if (stat.DbToDtoRequiresProcessing)
                    {
                        ProcessComplexRoomStats(locator, dtos, stat);
                    }
                }
            }

            return dtos;
        }

        /// <summary>
        /// Creates a projection list based off of the requested simple stats.
        /// </summary>
        /// <param name="dbType"></param>
        /// <param name="dtoType"></param>
        /// <param name="requestedStats"></param>
        /// <returns></returns>
        private ProjectionList CreateProjectionListFromRequestedStats(Type dbType, Type dtoType, IList<StreamableStat> requestedStats)
        {
            ProjectionList projList = Projections.ProjectionList();

            IDictionary<StreamableStat, PropertyInfo> dbLookup = StreamableStatUtils.GetPropertyInfoLookup(dbType);
            IDictionary<StreamableStat, PropertyInfo> dtoLookup = StreamableStatUtils.GetPropertyInfoLookup(dtoType);

            foreach (StreamableStat stat in requestedStats)
            {
                if (dtoLookup.ContainsKey(stat) && dbLookup.ContainsKey(stat))
                {
                    // Ignore complex stats.
                    if (!stat.DbToDtoRequiresProcessing)
                    {
                        projList = projList.Add(Projections.Property(dbLookup[stat].Name), dtoLookup[stat].Name);
                    }
                }
                else
                {
#warning Readd assert once everything is marked up.
                    //Debug.Assert(false, String.Format("Either the '{0}' or '{1}' class doesn't have the '{1}' stat marked up.", dbType.Name, dtoType.Name, stat));
                    Log.Error(String.Format("Either the '{0}' or '{1}' class doesn't have the '{1}' stat marked up.", dbType.Name, dtoType.Name, stat));
                }
            }

            return projList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mapSectionStat"></param>
        /// <param name="dto"></param>
        /// <param name="requestedStats"></param>
        private void CopySpecificStats<TDomain, TDto>(TDomain entityStat, TDto entityDto, IList<StreamableStat> requestedStats)
        {
            // Create a mapping of streamable stats to properties for both db and dto objects.
            IDictionary<StreamableStat, PropertyInfo> dbLookup = StreamableStatUtils.GetPropertyInfoLookup(entityStat.GetType());
            IDictionary<StreamableStat, PropertyInfo> dtoLookup = StreamableStatUtils.GetPropertyInfoLookup(entityDto.GetType());

            // Map the properties we are after.
            foreach (StreamableStat stat in requestedStats)
            {
                if (dtoLookup.ContainsKey(stat) && dbLookup.ContainsKey(stat))
                {
                    // Ignore complex stats.
                    if (!stat.DbToDtoRequiresProcessing)
                    {
                        object value = dbLookup[stat].GetValue(entityStat, null);
                        dtoLookup[stat].SetValue(entityDto, value, null);
                    }
                }
                else
                {
#warning Readd assert once everything is marked up.
                    //Debug.Assert(false, String.Format("One of the MapSectionStat dto class or the db class doesn't have the '{0}' stat marked up.", stat));
                    Log.Error(String.Format("One of the MapSectionStat dto class or the db class doesn't have the '{0}' stat marked up.", stat));
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="dtoLookup"></param>
        /// <param name="stat"></param>
        private void ProcessComplexMapSectionStat(IRepositoryLocator locator, IList<MapSectionStatDto> dtos, StreamableStat stat)
        {
            // Create a map of section stat ids to dtos
            IDictionary<long, MapSectionStatDto> dtoLookup = dtos.ToDictionary(item => item.Id);

            // Check what type of streamable stat was requested.
            if (stat == StreamableMapSectionStat.Archetypes)
            {
                ProcessMapSectionArchetypeStats(locator, dtoLookup);
            }
            else if (stat == StreamableMapSectionStat.Entities)
            {
                ProcessMapSectionEntityStats(locator, dtoLookup);
            }
            else if (stat == StreamableMapSectionStat.CarGens)
            {
                ProcessMapSectionCarGenStats(locator, dtoLookup);
            }
            else if (stat == StreamableMapSectionStat.ContainerAttributes)
            {
                ProcessMapSectionContainerAttributeStats(locator, dtoLookup);
            }
            else if (stat == StreamableMapSectionStat.CollisionPolygonCountBreakdown)
            {
                ProcessMapSectionCollisionPolyCountStats(locator, dtoLookup);
            }
            else if (stat == StreamableMapSectionStat.PlatformStats)
            {
                ProcessMapSectionPlatformStats(locator, dtoLookup);
            }
            else if (stat == StreamableMapSectionStat.AggregatedStats)
            {
                ProcessMapSectionAggregateStats(locator, dtoLookup);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="dtoLookup"></param>
        private void ProcessMapSectionArchetypeStats(IRepositoryLocator locator, IDictionary<long, MapSectionStatDto> dtoLookup)
        {
            // Initialise all the dtos.
            foreach (MapSectionStatDto dto in dtoLookup.Values)
            {
                dto.Archetypes = new List<BasicArchetypeStatDto>();
            }
            
            // Get the data from the db
            DetachedCriteria criteria = DetachedCriteria.For<ArchetypeStat>()
                                                     .CreateAlias("Archetype", "arch")
                                                     .SetProjection(Projections.ProjectionList()
                                                        .Add(Projections.Property("arch.Name"), "Name")
                                                        .Add(Projections.Property("arch.class"), "Type")
                                                        .Add(Projections.Property("MapSectionStat.Id"), "ReferencedStatId"))
                                                     .Add(Restrictions.In("MapSectionStat.Id", dtoLookup.Keys.ToList()))
                                                     .SetResultTransformer(Transformers.AliasToBean(typeof(ArchetypeView)));
            IList<ArchetypeView> archetypeViews = locator.FindAll<ArchetypeStat, ArchetypeView>(criteria);

            // Convert the data to dtos
            foreach (ArchetypeView view in archetypeViews)
            {
                if (dtoLookup.ContainsKey(view.ReferencedStatId))
                {
                    MapSectionStatDto dto = dtoLookup[view.ReferencedStatId];
                    
                    // Add the appropriate type of dto.
                    if (view.Type == 'd')
                    {
                        dto.Archetypes.Add(new BasicDrawableArchetypeStatDto(view.Name));
                    }
                    else if (view.Type == 'f')
                    {
                        dto.Archetypes.Add(new BasicFragmentArchetypeStatDto(view.Name));
                    }
                    else if (view.Type == 'i')
                    {
                        dto.Archetypes.Add(new BasicInteriorArchetypeStatDto(view.Name));
                    }
                    else if (view.Type == 'a')
                    {
                        dto.Archetypes.Add(new BasicStatedAnimArchetypeStatDto(view.Name));
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="dtoLookup"></param>
        private void ProcessMapSectionEntityStats(IRepositoryLocator locator, IDictionary<long, MapSectionStatDto> dtoLookup)
        {
            // Initialise all the dtos.
            foreach (MapSectionStatDto dto in dtoLookup.Values)
            {
                dto.Entities = new List<BasicEntityStatDto>();
            }

            // Get the data from the db
            DetachedCriteria criteria = DetachedCriteria.For<EntityStat>()
                                                     .CreateAlias("Entity", "ent")
                                                     .CreateAlias("ArchetypeStat", "archs")
                                                     .CreateAlias("archs.Archetype", "arch")
                                                     .SetProjection(Projections.ProjectionList()
                                                        .Add(Projections.Property("ent.Name"), "Name")
                                                        .Add(Projections.Property("arch.Name"), "ArchetypeName")
                                                        .Add(Projections.Property("MapSectionStat.Id"), "ReferencedStatId"))
                                                     .Add(Restrictions.In("MapSectionStat.Id", dtoLookup.Keys.ToList()))
                                                     .SetResultTransformer(Transformers.AliasToBean(typeof(EntityView)));
            IList<EntityView> entityViews = locator.FindAll<EntityStat, EntityView>(criteria);

            // Convert the data to dtos
            foreach (EntityView view in entityViews)
            {
                if (dtoLookup.ContainsKey(view.ReferencedStatId))
                {
                    MapSectionStatDto dto = dtoLookup[view.ReferencedStatId];
                    dto.Entities.Add(new BasicEntityStatDto(view.Name, view.ArchetypeName));
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="dtoLookup"></param>
        private void ProcessMapSectionCarGenStats(IRepositoryLocator locator, IDictionary<long, MapSectionStatDto> dtoLookup)
        {
            // Initialise all the dtos.
            foreach (MapSectionStatDto dto in dtoLookup.Values)
            {
                dto.CarGens = new List<CarGenStatDto>();
            }

            // Get the data from the db
            DetachedCriteria criteria = DetachedCriteria.For<CarGenStat>()
                                                     .Add(Restrictions.In("MapSectionStat.Id", dtoLookup.Keys.ToList()));
            IList<CarGenStat> carGens = locator.FindAll<CarGenStat>(criteria);

            // Convert the data to dtos
            foreach (CarGenStat carGen in carGens)
            {
                if (dtoLookup.ContainsKey(carGen.MapSectionStat.Id))
                {
                    MapSectionStatDto dto = dtoLookup[carGen.MapSectionStat.Id];

                    CarGenStatDto carGenDto = Mapper.Map<CarGenStat, CarGenStatDto>(carGen);
                    carGenDto.Position = new Vector3f((float)carGen.Position.X, (float)carGen.Position.Y, (float)carGen.Position.Z);
                    dto.CarGens.Add(carGenDto);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="dtoLookup"></param>
        private void ProcessMapSectionContainerAttributeStats(IRepositoryLocator locator, IDictionary<long, MapSectionStatDto> dtoLookup)
        {
            // Initialise all the dtos.
            foreach (MapSectionStatDto dto in dtoLookup.Values)
            {
                dto.ContainerAttributes = new List<MapSectionAttributeStatDto>();
            }

            // Get the data from the db
            DetachedCriteria criteria = DetachedCriteria.For<MapSectionAttributeStat>()
                                                     .Add(Restrictions.In("MapSectionStat.Id", dtoLookup.Keys.ToList()));
            IList<MapSectionAttributeStat> attributes = locator.FindAll<MapSectionAttributeStat>(criteria);

            // Convert the data to dtos
            foreach (MapSectionAttributeStat attribute in attributes)
            {
                if (dtoLookup.ContainsKey(attribute.MapSectionStat.Id))
                {
                    MapSectionStatDto dto = dtoLookup[attribute.MapSectionStat.Id];
                    dto.ContainerAttributes.Add(Mapper.Map<MapSectionAttributeStat, MapSectionAttributeStatDto>(attribute));
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="dtoLookup"></param>
        private void ProcessMapSectionCollisionPolyCountStats(IRepositoryLocator locator, IDictionary<long, MapSectionStatDto> dtoLookup)
        {
            // Initialise all the dtos.
            foreach (MapSectionStatDto dto in dtoLookup.Values)
            {
                dto.CollisionPolygonCountBreakdown = new List<CollisionPolyStatDto>();
            }

            // Get the data from the db
            DetachedCriteria criteria = DetachedCriteria.For<CollisionPolyStat<MapSectionStat>>()
                                                     .Add(Restrictions.In("ReferencedStat.Id", dtoLookup.Keys.ToList()));
            IList<CollisionPolyStat<MapSectionStat>> stats = locator.FindAll<CollisionPolyStat<MapSectionStat>>(criteria);

            // Convert the data to dtos
            foreach (CollisionPolyStat<MapSectionStat> stat in stats)
            {
                if (dtoLookup.ContainsKey(stat.ReferencedStat.Id))
                {
                    MapSectionStatDto dto = dtoLookup[stat.ReferencedStat.Id];

                    CollisionPolyStatDto statDto = new CollisionPolyStatDto(stat.CollisionFlags, stat.PolygonCount);
                    statDto.PrimitiveType = (stat.PrimitiveType != null ? stat.PrimitiveType.Value : null);
                    dto.CollisionPolygonCountBreakdown.Add(statDto);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="dtoLookup"></param>
        private void ProcessMapSectionPlatformStats(IRepositoryLocator locator, IDictionary<long, MapSectionStatDto> dtoLookup)
        {
            // Initialise all the dtos.
            foreach (MapSectionStatDto dto in dtoLookup.Values)
            {
                dto.PlatformStats = new List<MapSectionPlatformStatDto>();
            }

            // Get the data from the db
            DetachedCriteria criteria = DetachedCriteria.For<MapSectionPlatformStat>()
                                                     .Add(Restrictions.In("MapSectionStat.Id", dtoLookup.Keys.ToList()));
            IList<MapSectionPlatformStat> stats = locator.FindAll<MapSectionPlatformStat>(criteria);

            // Convert the data to dtos
            foreach (MapSectionPlatformStat stat in stats)
            {
                if (dtoLookup.ContainsKey(stat.MapSectionStat.Id))
                {
                    MapSectionStatDto dto = dtoLookup[stat.MapSectionStat.Id];

                    //
                    MapSectionPlatformStatDto platformDto = dto.PlatformStats.FirstOrDefault(item => item.Platform.Value == stat.Platform.Value);
                    if (platformDto == null)
                    {
                        platformDto = new MapSectionPlatformStatDto();
                        platformDto.Platform = new PlatformDto((RSG.Platform.Platform)stat.Platform.Value, stat.Platform.Name);
                        platformDto.FileTypeSizes = new List<MapSectionMemoryStatDto>();
                        dto.PlatformStats.Add(platformDto);
                    }

                    MapSectionMemoryStatDto fileTypeDto = platformDto.FileTypeSizes.FirstOrDefault(item => item.FileType.Value == stat.FileType.Value);
                    if (fileTypeDto == null)
                    {
                        fileTypeDto = new MapSectionMemoryStatDto();
                        fileTypeDto.FileType = new FileTypeDto((FileType)stat.FileType.Value, stat.FileType.Name);
                        fileTypeDto.MemorySat = new MemoryStatDto(stat.PhysicalSize, stat.VirtualSize);
                        platformDto.FileTypeSizes.Add(fileTypeDto);
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="dtoLookup"></param>
        private void ProcessMapSectionAggregateStats(IRepositoryLocator locator, IDictionary<long, MapSectionStatDto> dtoLookup)
        {
            // Initialise all the dtos.
            foreach (MapSectionStatDto dto in dtoLookup.Values)
            {
                dto.AggregatedStats = new List<MapSectionAggregateStatDto>();
            }

            // Get the data from the db
            DetachedCriteria criteria = DetachedCriteria.For<MapSectionAggregateStat>()
                                                     .Add(Restrictions.In("MapSectionStat.Id", dtoLookup.Keys.ToList()));
            IList<MapSectionAggregateStat> aggregateStats = locator.FindAll<MapSectionAggregateStat>(criteria);

            // Convert the data to dtos
            foreach (MapSectionAggregateStat stat in aggregateStats)
            {
                if (dtoLookup.ContainsKey(stat.MapSectionStat.Id))
                {
                    MapSectionStatDto dto = dtoLookup[stat.MapSectionStat.Id];

                    MapSectionAggregateStatDto aggregateStatDto = Mapper.Map<MapSectionAggregateStat, MapSectionAggregateStatDto>(stat);
                    aggregateStatDto.Group = (StatGroup)stat.Group.Value;
                    aggregateStatDto.LodLevel = (StatLodLevel)stat.LodLevel.Value;
                    aggregateStatDto.EntityType = (StatEntityType)stat.EntityType.Value;
                    aggregateStatDto.CollisionTypePolygonCounts = new List<CollisionPolyStatDto>();

                    dto.AggregatedStats.Add(aggregateStatDto);
                }
            }

            // Create a mapping of aggregate stat ids to dtos
            IDictionary<long, MapSectionAggregateStatDto> aggregateStatDtoLookup = dtoLookup.Values.SelectMany(item => item.AggregatedStats).ToDictionary(item => item.Id);
            
            // Get the collision poly count info next
            criteria = DetachedCriteria.For<CollisionPolyStat<MapSectionAggregateStat>>()
                                       .Add(Restrictions.In("ReferencedStat.Id", aggregateStatDtoLookup.Keys.ToList()));
            IList<CollisionPolyStat<MapSectionAggregateStat>> collisionStats = locator.FindAll<CollisionPolyStat<MapSectionAggregateStat>>(criteria);

            // Convert the data to dtos and add them to the aggregate stat dtos.
            foreach (CollisionPolyStat<MapSectionAggregateStat> stat in collisionStats)
            {
                if (aggregateStatDtoLookup.ContainsKey(stat.ReferencedStat.Id))
                {
                    MapSectionAggregateStatDto dto = aggregateStatDtoLookup[stat.ReferencedStat.Id];

                    CollisionPolyStatDto statDto = new CollisionPolyStatDto(stat.CollisionFlags, stat.PolygonCount);
                    statDto.PrimitiveType = (stat.PrimitiveType != null ? stat.PrimitiveType.Value : null);
                    dto.CollisionTypePolygonCounts.Add(statDto);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="dtos"></param>
        /// <param name="type"></param>
        private IDictionary<StreamableStat, PropertyInfo> CreateArchetypeDbPropertyInfoLookup()
        {
            IDictionary<StreamableStat, PropertyInfo> dbLookup = new Dictionary<StreamableStat, PropertyInfo>();

            foreach (Type type in c_archetypeDbTypes)
            {
                foreach (KeyValuePair<StreamableStat, PropertyInfo> pair in StreamableStatUtils.GetPropertyInfoLookup(type))
                {
                    if (!dbLookup.ContainsKey(pair.Key))
                    {
                        dbLookup.Add(pair);
                    }
                }
            }

            return dbLookup;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="archetypeStat"></param>
        /// <param name="archetypeDto"></param>
        /// <param name="requestedStats"></param>
        private void ProcessComplexArchetypeStats(IRepositoryLocator locator, IList<ArchetypeStatDto> dtos, StreamableStat stat)
        {
            // Create a map of section stat ids to dtos
            IDictionary<long, MapArchetypeStatDto> dtoLookup = dtos.OfType<MapArchetypeStatDto>().ToDictionary(item => item.Id, item => item);
            IDictionary<long, SimpleMapArchetypeStatDto> simpleDtoLookup = dtos.OfType<SimpleMapArchetypeStatDto>().ToDictionary(item => item.Id, item => item);
            IDictionary<long, InteriorArchetypeStatDto> interiorDtoLookup = dtos.OfType<InteriorArchetypeStatDto>().ToDictionary(item => item.Id);

            // Check what type of streamable stat was requested.
            if (stat == StreamableMapArchetypeStat.Shaders)
            {
                ProcessArchetypeShaderStats(locator, dtoLookup);
            }
            else if (stat == StreamableMapArchetypeStat.TxdExportSizes)
            {
                ProcessTxdExportSizeStats(locator, dtoLookup);
            }
            else if (stat == StreamableMapArchetypeStat.CollisionTypePolygonCounts)
            {
                ProcessArchetypeCollisionPolyCountStats(locator, dtoLookup);
            }
            else if (stat == StreamableSimpleMapArchetypeStat.SpawnPoints)
            {
                ProcessArchetypeSpawnPointStats(locator, simpleDtoLookup);
            }
            else if (stat == StreamableSimpleMapArchetypeStat.DrawableLODs)
            {
                ProcessArchetypeDrawableLodStats(locator, simpleDtoLookup);
            }
            else if (stat == StreamableInteriorArchetypeStat.Rooms)
            {
                ProcessRoomStats(locator, interiorDtoLookup);
            }
            else if (stat == StreamableInteriorArchetypeStat.Portals)
            {
                ProcessPortalStats(locator, interiorDtoLookup);
            }
            else if (stat == StreamableInteriorArchetypeStat.Entities)
            {
                ProcessInteriorEntityStats(locator, interiorDtoLookup);
            }
            else if (stat == StreamableStatedAnimArchetypeStat.ComponentArchetypes)
            {
                IDictionary<long, StatedAnimArchetypeStatDto> statedAnimDtoLookup = dtos.OfType<StatedAnimArchetypeStatDto>().ToDictionary(item => item.Id);
                ProcessComponentArchetypeStats(locator, statedAnimDtoLookup);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="dtoLookup"></param>
        private void ProcessArchetypeShaderStats(IRepositoryLocator locator, IDictionary<long, MapArchetypeStatDto> dtoLookup)
        {
            // Initialise all the dtos.
            foreach (MapArchetypeStatDto dto in dtoLookup.Values)
            {
                dto.Shaders = new List<RSG.Statistics.Common.Dto.GameAssetStats.ShaderStatDto>();
            }

            // Get the data from the db
            // Get the list of shader stats assoicated with the archetype.
            DetachedCriteria criteria = DetachedCriteria.For<ShaderStat>()
                                                        .CreateAlias("Shader", "s")
                                                        .SetProjection(Projections.ProjectionList()
                                                            .Add(Projections.Property("Id"), "Id")
                                                            .Add(Projections.Property("s.Name"), "Name")
                                                            .Add(Projections.Property("s.Identifier"), "IdentifierString")
                                                            .Add(Projections.Property("GameAssetStatBase.Id"), "GameAssetStatId"))
                                                        .Add(Restrictions.In("GameAssetStatBase.Id", dtoLookup.Keys.ToList()))
                                                        .SetResultTransformer(Transformers.AliasToBean(typeof(RSG.Statistics.Common.Dto.GameAssetStats.ShaderStatDto)));
            IList<RSG.Statistics.Common.Dto.GameAssetStats.ShaderStatDto> shaderStatDtos =
                locator.FindAll<ShaderStat, RSG.Statistics.Common.Dto.GameAssetStats.ShaderStatDto>(criteria);

            // Link the shader stats with the archetype stats.
            foreach (RSG.Statistics.Common.Dto.GameAssetStats.ShaderStatDto shaderStatDto in shaderStatDtos)
            {
                if (dtoLookup.ContainsKey(shaderStatDto.GameAssetStatId))
                {
                    MapArchetypeStatDto archetypeDto = dtoLookup[shaderStatDto.GameAssetStatId];
                    archetypeDto.Shaders.Add(shaderStatDto);
                }
            }

            // Get the list of texture stats for the shader stats we retrieved in the previous step.
            criteria = DetachedCriteria.For<TextureStat>()
                                       .CreateAlias("Texture", "t")
                                       .SetProjection(Projections.ProjectionList()
                                           .Add(Projections.Property("ShaderStat.Id"), "ShaderStatId")
                                           .Add(Projections.Property("t.Name"), "Name")
                                           .Add(Projections.Property("t.Identifier"), "IdentifierString")
                                           .Add(Projections.Property("t.Filename"), "Filename")
                                           .Add(Projections.Property("t.AlphaFilename"), "AlphaFilename")
                                           .Add(Projections.Property("t.TextureType"), "TextureType"))
                                       .Add(Restrictions.In("ShaderStat.Id", shaderStatDtos.Select(item => item.Id).ToList()))
                                       .SetResultTransformer(Transformers.AliasToBean(typeof(RSG.Statistics.Common.Dto.GameAssetStats.TextureStatDto)));
            IList<RSG.Statistics.Common.Dto.GameAssetStats.TextureStatDto> textureStatDtos =
                locator.FindAll<TextureStat, RSG.Statistics.Common.Dto.GameAssetStats.TextureStatDto>(criteria);

            // Link the texture stats dto up with the shader stat dtos.
            IDictionary<long, RSG.Statistics.Common.Dto.GameAssetStats.ShaderStatDto> shaderStatLookup =
                dtoLookup.Values.Cast<MapArchetypeStatDto>().SelectMany(item => item.Shaders).ToDictionary(shader => shader.Id);

            foreach (RSG.Statistics.Common.Dto.GameAssetStats.TextureStatDto textureStatDto in textureStatDtos)
            {
                shaderStatLookup[textureStatDto.ShaderStatId].TextureStats.Add(textureStatDto);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="dtoLookup"></param>
        private void ProcessTxdExportSizeStats(IRepositoryLocator locator, IDictionary<long, MapArchetypeStatDto> dtoLookup)
        {
            // Initialise all the dtos.
            foreach (MapArchetypeStatDto dto in dtoLookup.Values)
            {
                dto.TxdExportSizes = new List<TxdExportStatDto>();
            }

            // Get the data from the db
            DetachedCriteria criteria = DetachedCriteria.For<TxdExportSizeStat<ArchetypeStat>>()
                                                     .Add(Restrictions.In("ReferencedStat.Id", dtoLookup.Keys.ToList()));
            IList<TxdExportSizeStat<ArchetypeStat>> stats = locator.FindAll<TxdExportSizeStat<ArchetypeStat>>(criteria);

            // Convert the data to dtos
            foreach (TxdExportSizeStat<ArchetypeStat> stat in stats)
            {
                if (dtoLookup.ContainsKey(stat.ReferencedStat.Id))
                {
                    MapArchetypeStatDto dto = dtoLookup[stat.ReferencedStat.Id];
                    dto.TxdExportSizes.Add(new TxdExportStatDto(stat.TxdName, stat.Size));
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="dtoLookup"></param>
        private void ProcessArchetypeCollisionPolyCountStats(IRepositoryLocator locator, IDictionary<long, MapArchetypeStatDto> dtoLookup)
        {
            // Initialise all the dtos.
            foreach (MapArchetypeStatDto dto in dtoLookup.Values)
            {
                dto.CollisionTypePolygonCounts = new List<CollisionPolyStatDto>();
            }

            // Get the data from the db
            DetachedCriteria criteria = DetachedCriteria.For<CollisionPolyStat<ArchetypeStat>>()
                                                     .Add(Restrictions.In("ReferencedStat.Id", dtoLookup.Keys.ToList()));
            IList<CollisionPolyStat<ArchetypeStat>> stats = locator.FindAll<CollisionPolyStat<ArchetypeStat>>(criteria);

            // Convert the data to dtos
            foreach (CollisionPolyStat<ArchetypeStat> stat in stats)
            {
                if (dtoLookup.ContainsKey(stat.ReferencedStat.Id))
                {
                    MapArchetypeStatDto dto = dtoLookup[stat.ReferencedStat.Id];

                    CollisionPolyStatDto statDto = new CollisionPolyStatDto(stat.CollisionFlags, stat.PolygonCount);
                    statDto.PrimitiveType = (stat.PrimitiveType != null ? stat.PrimitiveType.Value : null);
                    dto.CollisionTypePolygonCounts.Add(statDto);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="dtoLookup"></param>
        private void ProcessArchetypeSpawnPointStats(IRepositoryLocator locator, IDictionary<long, SimpleMapArchetypeStatDto> dtoLookup)
        {
            // Initialise all the dtos.
            foreach (SimpleMapArchetypeStatDto dto in dtoLookup.Values)
            {
                dto.SpawnPoints = new List<SpawnPointStatDto>();
            }

            // Get the data from the db
            DetachedCriteria criteria = DetachedCriteria.For<SpawnPointStat>()
                                                     .Add(Restrictions.In("ArchetypeStat.Id", dtoLookup.Keys.ToList()));
            IList<SpawnPointStat> spawnPointStats = locator.FindAll<SpawnPointStat>(criteria);

            // Convert the data to dtos
            foreach (SpawnPointStat spawnPoint in spawnPointStats)
            {
                if (dtoLookup.ContainsKey(spawnPoint.ArchetypeStat.Id))
                {
                    SimpleMapArchetypeStatDto dto = dtoLookup[spawnPoint.ArchetypeStat.Id];

                    SpawnPointStatDto spawnPointDto = Mapper.Map<SpawnPointStat, SpawnPointStatDto>(spawnPoint);
                    spawnPointDto.Position = new Vector3f((float)spawnPoint.Position.X, (float)spawnPoint.Position.Y, (float)spawnPoint.Position.Z);
                    spawnPointDto.AvailabilityMode = (spawnPoint.AvailabilityMode != null ? spawnPoint.AvailabilityMode.Value : null);
                    dto.SpawnPoints.Add(spawnPointDto);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="dtoLookup"></param>
        private void ProcessArchetypeDrawableLodStats(IRepositoryLocator locator, IDictionary<long, SimpleMapArchetypeStatDto> dtoLookup)
        {
            // Initialise all the dtos.
            foreach (SimpleMapArchetypeStatDto dto in dtoLookup.Values)
            {
                dto.DrawableLods = new List<DrawableLodStatDto>();
            }

            // Get the data from the db
            DetachedCriteria criteria = DetachedCriteria.For<DrawableLodStat>()
                                                     .Add(Restrictions.In("ArchetypeStat.Id", dtoLookup.Keys.ToList()));
            IList<DrawableLodStat> lodStats = locator.FindAll<DrawableLodStat>(criteria);

            // Convert the data to dtos
            foreach (DrawableLodStat lodStat in lodStats)
            {
                if (dtoLookup.ContainsKey(lodStat.ArchetypeStat.Id))
                {
                    SimpleMapArchetypeStatDto dto = dtoLookup[lodStat.ArchetypeStat.Id];

                    DrawableLodStatDto lodStatDto = Mapper.Map<DrawableLodStat, DrawableLodStatDto>(lodStat);
                    lodStatDto.LodLevel = (MapDrawableLODLevel)lodStat.LodLevel.Value;
                    dto.DrawableLods.Add(lodStatDto);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="dtoLookup"></param>
        private void ProcessRoomStats(IRepositoryLocator locator, IDictionary<long, InteriorArchetypeStatDto> dtoLookup)
        {
            // Initialise all the dtos.
            foreach (InteriorArchetypeStatDto dto in dtoLookup.Values)
            {
                dto.RoomStats = new List<BasicRoomStatDto>();
            }

            // Get the data from the db
            DetachedCriteria criteria = DetachedCriteria.For<RoomStat>()
                                                     .CreateAlias("Room", "r")
                                                     .SetProjection(Projections.ProjectionList()
                                                        .Add(Projections.Property("r.Name"), "Name")
                                                        .Add(Projections.Property("InteriorStat.Id"), "InteriorStatId"))
                                                     .Add(Restrictions.In("InteriorStat.Id", dtoLookup.Keys.ToList()))
                                                     .SetResultTransformer(Transformers.AliasToBean(typeof(BasicRoomStatDto)));
            IList<BasicRoomStatDto> roomDtos = locator.FindAll<RoomStat, BasicRoomStatDto>(criteria);

            // Convert the data to dtos
            foreach (BasicRoomStatDto roomDto in roomDtos)
            {
                if (dtoLookup.ContainsKey(roomDto.InteriorStatId))
                {
                    InteriorArchetypeStatDto dto = dtoLookup[roomDto.InteriorStatId];
                    dto.RoomStats.Add(roomDto);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="dtoLookup"></param>
        private void ProcessPortalStats(IRepositoryLocator locator, IDictionary<long, InteriorArchetypeStatDto> dtoLookup)
        {
            // Initialise all the dtos.
            foreach (InteriorArchetypeStatDto dto in dtoLookup.Values)
            {
                dto.PortalStats = new List<PortalStatDto>();
            }

            // Get the data from the db
            DetachedCriteria criteria = DetachedCriteria.For<PortalStat>()
                                                     .CreateAlias("RoomStatA", "rsa")
                                                     .CreateAlias("rsa.Room", "rsar")
                                                     .CreateAlias("RoomStatB", "rsb")
                                                     .CreateAlias("rsb.Room", "rsbr")
                                                     .SetProjection(Projections.ProjectionList()
                                                        .Add(Projections.Property("Name"), "Name")
                                                        .Add(Projections.Property("rsar.Identifier"), "RoomAIdentifierString")
                                                        .Add(Projections.Property("rsbr.Identifier"), "RoomBIdentifierString")
                                                        .Add(Projections.Property("InteriorStat.Id"), "InteriorStatId"))
                                                     .Add(Restrictions.In("InteriorStat.Id", dtoLookup.Keys.ToList()))
                                                     .SetResultTransformer(Transformers.AliasToBean(typeof(PortalStatDto)));
            IList<PortalStatDto> portalDtos = locator.FindAll<RoomStat, PortalStatDto>(criteria);

            // Convert the data to dtos
            foreach (PortalStatDto portalDto in portalDtos)
            {
                if (dtoLookup.ContainsKey(portalDto.InteriorStatId))
                {
                    InteriorArchetypeStatDto dto = dtoLookup[portalDto.InteriorStatId];
                    dto.PortalStats.Add(portalDto);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="dtoLookup"></param>
        private void ProcessInteriorEntityStats(IRepositoryLocator locator, IDictionary<long, InteriorArchetypeStatDto> dtoLookup)
        {
            // Initialise all the dtos.
            foreach (InteriorArchetypeStatDto dto in dtoLookup.Values)
            {
                dto.Entities = new List<BasicEntityStatDto>();
            }

            // Get the data from the db
            DetachedCriteria criteria = DetachedCriteria.For<EntityStat>()
                                                     .CreateAlias("Entity", "ent")
                                                     .CreateAlias("ArchetypeStat", "archs")
                                                     .CreateAlias("archs.Archetype", "arch")
                                                     .SetProjection(Projections.ProjectionList()
                                                        .Add(Projections.Property("ent.Name"), "Name")
                                                        .Add(Projections.Property("arch.Name"), "ArchetypeName")
                                                        .Add(Projections.Property("InteriorStat.Id"), "ReferencedStatId"))
                                                     .Add(Restrictions.In("InteriorStat.Id", dtoLookup.Keys.ToList()))
                                                     .SetResultTransformer(Transformers.AliasToBean(typeof(EntityView)));
            IList<EntityView> entityViews = locator.FindAll<EntityStat, EntityView>(criteria);

            // Convert the data to dtos
            foreach (EntityView view in entityViews)
            {
                if (dtoLookup.ContainsKey(view.ReferencedStatId))
                {
                    InteriorArchetypeStatDto dto = dtoLookup[view.ReferencedStatId];
                    dto.Entities.Add(new BasicEntityStatDto(view.Name, view.ArchetypeName));
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="dtoLookup"></param>
        private void ProcessComponentArchetypeStats(IRepositoryLocator locator, IDictionary<long, StatedAnimArchetypeStatDto> dtoLookup)
        {
            // Initialise all the dtos.
            foreach (StatedAnimArchetypeStatDto dto in dtoLookup.Values)
            {
                dto.ComponentArchetypeStats = new List<BasicArchetypeStatDto>();
            }

            // Get the data from the db
            DetachedCriteria criteria = DetachedCriteria.For<StatedAnimArchetypeStat>()
                                                        .CreateAlias("ComponentArchetypeStats", "cas")
                                                        .CreateAlias("cas.Archetype", "arch")
                                                        .SetProjection(Projections.ProjectionList()
                                                            .Add(Projections.Property("arch.Name"), "Name")
                                                            .Add(Projections.Property("arch.class"), "Type")
                                                            .Add(Projections.Property("Id"), "ReferencedStatId"))
                                                        .Add(Restrictions.In("Id", dtoLookup.Keys.ToList()))
                                                        .SetResultTransformer(Transformers.AliasToBean(typeof(ArchetypeView)));
            IList<ArchetypeView> archetypeViews = locator.FindAll<StatedAnimArchetypeStat, ArchetypeView>(criteria);

            // Convert the data to dtos
            foreach (ArchetypeView view in archetypeViews)
            {
                if (dtoLookup.ContainsKey(view.ReferencedStatId))
                {
                    StatedAnimArchetypeStatDto dto = dtoLookup[view.ReferencedStatId];

                    // Add the appropriate type of dto.
                    if (view.Type == 'd')
                    {
                        dto.ComponentArchetypeStats.Add(new BasicDrawableArchetypeStatDto(view.Name));
                    }
                    else if (view.Type == 'f')
                    {
                        dto.ComponentArchetypeStats.Add(new BasicFragmentArchetypeStatDto(view.Name));
                    }
                    else if (view.Type == 'i')
                    {
                        dto.ComponentArchetypeStats.Add(new BasicInteriorArchetypeStatDto(view.Name));
                    }
                    else if (view.Type == 'a')
                    {
                        dto.ComponentArchetypeStats.Add(new BasicStatedAnimArchetypeStatDto(view.Name));
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stat"></param>
        /// <param name="dto"></param>
        private void ProcessComplexEntityStats(EntityStat entityStat, EntityStatDto entityDto, IList<StreamableStat> requestedStats)
        {
            foreach (StreamableStat stat in requestedStats)
            {
                if (stat == StreamableEntityStat.Position)
                {
                    entityDto.Location = new Vector3f((float)entityStat.Location.X, (float)entityStat.Location.Y, (float)entityStat.Location.Z);
                }
                else if (stat == StreamableEntityStat.BoundingBox)
                {
                    Vector3f min = new Vector3f((float)entityStat.BoundingBoxMin.X, (float)entityStat.BoundingBoxMin.Y, (float)entityStat.BoundingBoxMin.Z);
                    Vector3f max = new Vector3f((float)entityStat.BoundingBoxMax.X, (float)entityStat.BoundingBoxMax.Y, (float)entityStat.BoundingBoxMax.Z);
                    entityDto.BoundingBox = new BoundingBox3f(min, max);
                }
                else if (stat == StreamableEntityStat.LodLevel)
                {
                    entityDto.LodLevel = entityStat.LodLevel.Value;
                }
                else if (stat == StreamableEntityStat.LodParent)
                {
                    // This could be quite an expensive statement!
                    if (entityStat.LodParent != null)
                    {
                        entityDto.LodParent = new BasicEntityStatDto(entityStat.LodParent.Entity.Name, null);
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="dtos"></param>
        /// <param name="stat"></param>
        private void ProcessComplexRoomStats(IRepositoryLocator locator, IList<RoomStatDto> dtos, StreamableStat stat)
        {
            // Create a map of section stat ids to dtos
            IDictionary<long, RoomStatDto> dtoLookup = dtos.ToDictionary(item => item.Id);

            // Check what type of streamable stat was requested.
            if (stat == StreamableRoomStat.Shaders)
            {
                ProcessRoomShaderStats(locator, dtoLookup);
            }
            else if (stat == StreamableRoomStat.TxdExportSizes)
            {
                ProcessRoomTxdExportSizeStats(locator, dtoLookup);
            }
            else if (stat == StreamableRoomStat.CollisionTypePolygonCounts)
            {
                ProcessRoomCollisionPolyCountStats(locator, dtoLookup);
            }
            else if (stat == StreamableRoomStat.Entities)
            {
                ProcessRoomEntityStats(locator, dtoLookup);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="dtoLookup"></param>
        private void ProcessRoomShaderStats(IRepositoryLocator locator, IDictionary<long, RoomStatDto> dtoLookup)
        {
            // Initialise all the dtos.
            foreach (RoomStatDto dto in dtoLookup.Values)
            {
                dto.ShaderStats = new List<RSG.Statistics.Common.Dto.GameAssetStats.ShaderStatDto>();
            }

            // Get the data from the db
            // Get the list of shader stats assoicated with the archetype.
            DetachedCriteria criteria = DetachedCriteria.For<ShaderStat>()
                                                        .CreateAlias("Shader", "s")
                                                        .SetProjection(Projections.ProjectionList()
                                                            .Add(Projections.Property("Id"), "Id")
                                                            .Add(Projections.Property("s.Name"), "Name")
                                                            .Add(Projections.Property("s.Identifier"), "IdentifierString")
                                                            .Add(Projections.Property("GameAssetStatBase.Id"), "GameAssetStatId"))
                                                        .Add(Restrictions.In("GameAssetStatBase.Id", dtoLookup.Keys.ToList()))
                                                        .SetResultTransformer(Transformers.AliasToBean(typeof(RSG.Statistics.Common.Dto.GameAssetStats.ShaderStatDto)));
            IList<RSG.Statistics.Common.Dto.GameAssetStats.ShaderStatDto> shaderStatDtos =
                locator.FindAll<ShaderStat, RSG.Statistics.Common.Dto.GameAssetStats.ShaderStatDto>(criteria);

            // Link the shader stats with the archetype stats.
            foreach (RSG.Statistics.Common.Dto.GameAssetStats.ShaderStatDto shaderStatDto in shaderStatDtos)
            {
                if (dtoLookup.ContainsKey(shaderStatDto.GameAssetStatId))
                {
                    RoomStatDto roomDto = dtoLookup[shaderStatDto.GameAssetStatId];
                    roomDto.ShaderStats.Add(shaderStatDto);
                }
            }

            // Get the list of texture stats for the shader stats we retrieved in the previous step.
            criteria = DetachedCriteria.For<TextureStat>()
                                       .CreateAlias("Texture", "t")
                                       .SetProjection(Projections.ProjectionList()
                                           .Add(Projections.Property("ShaderStat.Id"), "ShaderStatId")
                                           .Add(Projections.Property("t.Name"), "Name")
                                           .Add(Projections.Property("t.Identifier"), "IdentifierString")
                                           .Add(Projections.Property("t.Filename"), "Filename")
                                           .Add(Projections.Property("t.AlphaFilename"), "AlphaFilename")
                                           .Add(Projections.Property("t.TextureType"), "TextureType"))
                                       .Add(Restrictions.In("ShaderStat.Id", shaderStatDtos.Select(item => item.Id).ToList()))
                                       .SetResultTransformer(Transformers.AliasToBean(typeof(RSG.Statistics.Common.Dto.GameAssetStats.TextureStatDto)));
            IList<RSG.Statistics.Common.Dto.GameAssetStats.TextureStatDto> textureStatDtos =
                locator.FindAll<TextureStat, RSG.Statistics.Common.Dto.GameAssetStats.TextureStatDto>(criteria);

            // Link the texture stats dto up with the shader stat dtos.
            IDictionary<long, RSG.Statistics.Common.Dto.GameAssetStats.ShaderStatDto> shaderStatLookup =
                dtoLookup.Values.SelectMany(item => item.ShaderStats).ToDictionary(shader => shader.Id);

            foreach (RSG.Statistics.Common.Dto.GameAssetStats.TextureStatDto textureStatDto in textureStatDtos)
            {
                shaderStatLookup[textureStatDto.ShaderStatId].TextureStats.Add(textureStatDto);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="dtoLookup"></param>
        private void ProcessRoomTxdExportSizeStats(IRepositoryLocator locator, IDictionary<long, RoomStatDto> dtoLookup)
        {
            // Initialise all the dtos.
            foreach (RoomStatDto dto in dtoLookup.Values)
            {
                dto.TxdExportSizes = new List<TxdExportStatDto>();
            }

            // Get the data from the db
            DetachedCriteria criteria = DetachedCriteria.For<TxdExportSizeStat<RoomStat>>()
                                                     .Add(Restrictions.In("ReferencedStat.Id", dtoLookup.Keys.ToList()));
            IList<TxdExportSizeStat<RoomStat>> stats = locator.FindAll<TxdExportSizeStat<RoomStat>>(criteria);

            // Convert the data to dtos
            foreach (TxdExportSizeStat<RoomStat> stat in stats)
            {
                if (dtoLookup.ContainsKey(stat.ReferencedStat.Id))
                {
                    RoomStatDto dto = dtoLookup[stat.ReferencedStat.Id];
                    dto.TxdExportSizes.Add(new TxdExportStatDto(stat.TxdName, stat.Size));
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="dtoLookup"></param>
        private void ProcessRoomCollisionPolyCountStats(IRepositoryLocator locator, IDictionary<long, RoomStatDto> dtoLookup)
        {
            // Initialise all the dtos.
            foreach (RoomStatDto dto in dtoLookup.Values)
            {
                dto.CollisionPolygonCountBreakdown = new List<CollisionPolyStatDto>();
            }

            // Get the data from the db
            DetachedCriteria criteria = DetachedCriteria.For<CollisionPolyStat<RoomStat>>()
                                                     .Add(Restrictions.In("ReferencedStat.Id", dtoLookup.Keys.ToList()));
            IList<CollisionPolyStat<RoomStat>> stats = locator.FindAll<CollisionPolyStat<RoomStat>>(criteria);

            // Convert the data to dtos
            foreach (CollisionPolyStat<RoomStat> stat in stats)
            {
                if (dtoLookup.ContainsKey(stat.ReferencedStat.Id))
                {
                    RoomStatDto dto = dtoLookup[stat.ReferencedStat.Id];

                    CollisionPolyStatDto statDto = new CollisionPolyStatDto(stat.CollisionFlags, stat.PolygonCount);
                    statDto.PrimitiveType = (stat.PrimitiveType != null ? stat.PrimitiveType.Value : null);
                    dto.CollisionPolygonCountBreakdown.Add(statDto);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="dtoLookup"></param>
        private void ProcessRoomEntityStats(IRepositoryLocator locator, IDictionary<long, RoomStatDto> dtoLookup)
        {
            // Initialise all the dtos.
            foreach (RoomStatDto dto in dtoLookup.Values)
            {
                dto.Entities = new List<BasicEntityStatDto>();
            }

            // Get the data from the db
            DetachedCriteria criteria = DetachedCriteria.For<EntityStat>()
                                                     .CreateAlias("Entity", "ent")
                                                     .CreateAlias("ArchetypeStat", "archs")
                                                     .CreateAlias("archs.Archetype", "arch")
                                                     .SetProjection(Projections.ProjectionList()
                                                        .Add(Projections.Property("ent.Name"), "Name")
                                                        .Add(Projections.Property("arch.Name"), "ArchetypeName")
                                                        .Add(Projections.Property("RoomStat.Id"), "ReferencedStatId"))
                                                     .Add(Restrictions.In("RoomStat.Id", dtoLookup.Keys.ToList()))
                                                     .SetResultTransformer(Transformers.AliasToBean(typeof(EntityView)));
            IList<EntityView> entityViews = locator.FindAll<EntityStat, EntityView>(criteria);

            // Convert the data to dtos
            foreach (EntityView view in entityViews)
            {
                if (dtoLookup.ContainsKey(view.ReferencedStatId))
                {
                    RoomStatDto dto = dtoLookup[view.ReferencedStatId];
                    dto.Entities.Add(new BasicEntityStatDto(view.Name, view.ArchetypeName));
                }
            }
        }
        #endregion // Data Retrieval
        #endregion // Map Hierarchy

        #region Vehicles
        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="buildIdentifier"></param>
        /// <returns></returns>
        private VehicleDtos GetVehiclesForBuildCommand(IRepositoryLocator locator, string buildIdentifier)
        {
            Build build = GetAssetByIdentifier<Build>(locator, buildIdentifier);

            // Get all the vehicles for this build
            DetachedCriteria query = DetachedCriteria.For<VehicleStat>();
            query.Add(Expression.Eq("Build", build));

            VehicleDtos result = new VehicleDtos();
            foreach (VehicleStat stat in locator.FindAll<VehicleStat>(query))
            {
                result.Items.Add(Mapper.Map<Vehicle, VehicleDto>(stat.Vehicle));
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="buildIdentifier"></param>
        /// <param name="vehicleHash"></param>
        /// <returns></returns>
        private VehicleDto GetVehicleForBuildCommand(IRepositoryLocator locator, string buildIdentifier, string vehicleHash)
        {
            Build build = GetAssetByIdentifier<Build>(locator, buildIdentifier);
            Vehicle vehicle = GetAssetByIdentifier<Vehicle>(locator, vehicleHash);

            // Ensure that a vehicle stat exists for the two
            DetachedCriteria query = DetachedCriteria.For<VehicleStat>()
                                                     .Add(Expression.Eq("Build", build))
                                                     .Add(Expression.Eq("Vehicle", vehicle));
            VehicleStat vehicleStat = locator.FindFirst<VehicleStat>(query);
            if (vehicleStat == null)
            {
                throw new WebFaultException<string>("Vehicle doesn't have a stat in build.", HttpStatusCode.BadRequest);
            }

            return Mapper.Map<Vehicle, VehicleDto>(vehicle);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="buildIdentifier"></param>
        /// <param name="vehicleHash"></param>
        /// <returns></returns>
        private VehicleStatBundleDto GetVehicleStatCommand(IRepositoryLocator locator, string buildIdentifier, string vehicleHash)
        {
            Build build = GetAssetByIdentifier<Build>(locator, buildIdentifier);
            Vehicle vehicle = GetAssetByIdentifier<Vehicle>(locator, vehicleHash);

            // Ensure that a vehicle stat exists for the two
            DetachedCriteria query = DetachedCriteria.For<VehicleStat>()
                                                     .Add(Expression.Eq("Build", build))
                                                     .Add(Expression.Eq("Vehicle", vehicle));
            VehicleStat vehicleStat = locator.FindFirst<VehicleStat>(query);
            if (vehicleStat == null)
            {
                throw new WebFaultException<string>("Vehicle doesn't have a stat in build.", HttpStatusCode.BadRequest);
            }

            return Mapper.Map<VehicleStat, VehicleStatBundleDto>(vehicleStat);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="buildIdentifier"></param>
        /// <param name="vehicleHash"></param>
        /// <returns></returns>
        private void CreateVehicleStat(IRepositoryLocator locator, string buildIdentifier, string vehicleHash, VehicleStatBundleDto vehicleStatBundle)
        {
            Build build = GetAssetByIdentifier<Build>(locator, buildIdentifier);
            Vehicle vehicle = GetAssetByIdentifier<Vehicle>(locator, vehicleHash);
            
            DetachedCriteria query = DetachedCriteria.For<VehicleStat>()
                                                     .Add(Expression.Eq("Build", build))
                                                     .Add(Expression.Eq("Vehicle", vehicle));
            VehicleStat vehicleStat = locator.FindFirst<VehicleStat>(query);
            if (vehicleStat != null)
            {
                // Delete the stat if it exists
                locator.Delete(vehicleStat);
            }

            // Create any shaders/textures this bundle contains.
            CreateShadersAndTextures(locator, vehicleStatBundle);

            // Create the new vehicle stat
            VehicleStat stat = Mapper.Map<VehicleStatBundleDto, VehicleStat>(vehicleStatBundle);
            stat.Build = GetAssetByIdentifier<Build>(locator, buildIdentifier);
            stat.Vehicle = GetAssetByIdentifier<Vehicle>(locator, vehicleHash);

            // Create the platform stats
            stat.VehiclePlatformStats = new List<VehiclePlatformStat>();
            foreach (VehiclePlatformStatBundleDto platformStatDto in vehicleStatBundle.VehiclePlatformStats)
            {
                VehiclePlatformStat platformStat = Mapper.Map<VehiclePlatformStatBundleDto, VehiclePlatformStat>(platformStatDto);
                platformStat.VehicleStat = stat;
                platformStat.Platform = GetEnumReferenceByValue<RSG.Platform.Platform>(locator, platformStatDto.Platform.Value);
                stat.VehiclePlatformStats.Add(platformStat);
            }

            // Create/link the shader stats
            LinkShaderStatsToGameAssetStat(locator, stat, vehicleStatBundle);

            locator.Save(stat);
        }
        #endregion // Vehicles

        #region Weapons
        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="buildIdentifier"></param>
        /// <returns></returns>
        private WeaponDtos GetWeaponsForBuildCommand(IRepositoryLocator locator, string buildIdentifier)
        {
            Build build = GetAssetByIdentifier<Build>(locator, buildIdentifier);

            // Get all the weapons for this build
            DetachedCriteria query = DetachedCriteria.For<WeaponStat>();
            query.Add(Expression.Eq("Build", build));

            WeaponDtos result = new WeaponDtos();
            foreach (WeaponStat stat in locator.FindAll<WeaponStat>(query))
            {
                result.Items.Add(Mapper.Map<Weapon, WeaponDto>(stat.Weapon));
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="buildIdentifier"></param>
        /// <param name="weaponHash"></param>
        /// <returns></returns>
        private WeaponDto GetWeaponForBuildCommand(IRepositoryLocator locator, string buildIdentifier, string weaponHash)
        {
            Build build = GetAssetByIdentifier<Build>(locator, buildIdentifier);
            Weapon weapon = GetAssetByIdentifier<Weapon>(locator, weaponHash);

            // Ensure that a weapon stat exists for the two
            DetachedCriteria query = DetachedCriteria.For<WeaponStat>()
                                                     .Add(Expression.Eq("Build", build))
                                                     .Add(Expression.Eq("Weapon", weapon));
            WeaponStat weaponStat = locator.FindFirst<WeaponStat>(query);
            if (weaponStat == null)
            {
                throw new WebFaultException<string>("Weapon doesn't have a stat in build.", HttpStatusCode.BadRequest);
            }

            return Mapper.Map<Weapon, WeaponDto>(weapon);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="buildIdentifier"></param>
        /// <param name="weaponHash"></param>
        /// <returns></returns>
        private WeaponStatBundleDto GetWeaponStatCommand(IRepositoryLocator locator, string buildIdentifier, string weaponHash)
        {
            Build build = GetAssetByIdentifier<Build>(locator, buildIdentifier);
            Weapon weapon = GetAssetByIdentifier<Weapon>(locator, weaponHash);

            // Ensure that a weapon stat exists for the two
            DetachedCriteria query = DetachedCriteria.For<WeaponStat>()
                                                     .Add(Expression.Eq("Build", build))
                                                     .Add(Expression.Eq("Weapon", weapon));
            WeaponStat weaponStat = locator.FindFirst<WeaponStat>(query);
            if (weaponStat == null)
            {
                throw new WebFaultException<string>("Weapon doesn't have a stat in build.", HttpStatusCode.BadRequest);
            }

            return Mapper.Map<WeaponStat, WeaponStatBundleDto>(weaponStat);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="buildIdentifier"></param>
        /// <param name="weaponHash"></param>
        /// <returns></returns>
        private void CreateWeaponStat(IRepositoryLocator locator, string buildIdentifier, string weaponHash, WeaponStatBundleDto weaponStatBundle)
        {
            Build build = GetAssetByIdentifier<Build>(locator, buildIdentifier);
            Weapon weapon = GetAssetByIdentifier<Weapon>(locator, weaponHash);

            DetachedCriteria query = DetachedCriteria.For<WeaponStat>()
                                                     .Add(Expression.Eq("Build", build))
                                                     .Add(Expression.Eq("Weapon", weapon));
            WeaponStat weaponStat = locator.FindFirst<WeaponStat>(query);
            if (weaponStat != null)
            {
                // Delete the stat if it exists
                locator.Delete(weaponStat);
            }

            // Create any shaders/textures this bundle contains.
            CreateShadersAndTextures(locator, weaponStatBundle);

            // Create the new weapon stat
            WeaponStat stat = Mapper.Map<WeaponStatBundleDto, WeaponStat>(weaponStatBundle);
            stat.Build = GetAssetByIdentifier<Build>(locator, buildIdentifier);
            stat.Weapon = GetAssetByIdentifier<Weapon>(locator, weaponHash);

            // Create the platform stats
            stat.WeaponPlatformStats = new List<WeaponPlatformStat>();
            foreach (WeaponPlatformStatBundleDto platformStatDto in weaponStatBundle.WeaponPlatformStats)
            {
                WeaponPlatformStat platformStat = Mapper.Map<WeaponPlatformStatBundleDto, WeaponPlatformStat>(platformStatDto);
                platformStat.WeaponStat = stat;
                platformStat.Platform = GetEnumReferenceByValue<RSG.Platform.Platform>(locator, platformStatDto.Platform.Value);
                stat.WeaponPlatformStats.Add(platformStat);
            }

            // Create/link the shader stats
            LinkShaderStatsToGameAssetStat(locator, stat, weaponStatBundle);

            locator.Save(stat);
        }
        #endregion // Weapons

        #region Shared Methods
        /// <summary>
        /// Helper function for creating all shaders/textures that exist in a game asset stat bundle.
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="gameAssetStatBundle"></param>
        private void CreateShadersAndTextures(IRepositoryLocator locator, GameAssetStatBundleDtoBase bundle)
        {
            // Create new shaders
            foreach (ShaderBundleDto shaderDto in bundle.Shaders)
            {
                Shader shader = GetAssetByIdentifier<Shader>(locator, shaderDto.Hash.ToString(), false);
                if (shader != null)
                {
                    shader.Name = shaderDto.Name;
                }
                else
                {
                    shader = Mapper.Map<ShaderBundleDto, Shader>(shaderDto);
                    shader.Id = 0;
                }
                locator.SaveOrUpdate(shader);
            }

            // Create new textures
            foreach (TextureBundleDto textureDto in bundle.Textures)
            {
                Texture texture = GetAssetByIdentifier<Texture>(locator, textureDto.Hash.ToString(), false);
                if (texture != null)
                {
                    texture.Name = textureDto.Name;
                    texture.Filename = textureDto.Filename;
                    texture.AlphaFilename = textureDto.AlphaFilename;
                    texture.TextureType = textureDto.TextureType;
                }
                else
                {
                    texture = Mapper.Map<TextureBundleDto, Texture>(textureDto);
                    texture.Id = 0;
                }
                locator.SaveOrUpdate(texture);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="stat"></param>
        /// <param name="bundle"></param>
        private void LinkShaderStatsToGameAssetStat(IRepositoryLocator locator, GameAssetStatBase stat, GameAssetStatBundleDtoBase bundle)
        {
            // Create the shader stats and link them to the stat.
            stat.ShaderStats = new List<ShaderStat>();

            foreach (ShaderStatBundleDto shaderStatDto in bundle.ShaderStats)
            {
                ShaderStat shaderStat = new ShaderStat();
                shaderStat.GameAssetStatBase = stat;
                shaderStat.Shader = GetAssetByIdentifier<Shader>(locator, shaderStatDto.ShaderHash.ToString());

                // Create the texture stats for this shader
                shaderStat.TextureStats = new List<TextureStat>();

                foreach (long textureHash in shaderStatDto.Textures)
                {
                    TextureStat textureStat = new TextureStat();
                    textureStat.ShaderStat = shaderStat;
                    textureStat.Texture = GetAssetByIdentifier<Texture>(locator, textureHash.ToString());
                    shaderStat.TextureStats.Add(textureStat);
                }

                stat.ShaderStats.Add(shaderStat);
            }
        }
        #endregion // Shared Methods
        #endregion // Private Methods
    } // BuildService
}
