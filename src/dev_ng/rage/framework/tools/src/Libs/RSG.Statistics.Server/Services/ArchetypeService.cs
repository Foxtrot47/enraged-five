﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common.ServiceContract;
using RSG.Statistics.Common.Dto.GameAssets;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Server.Repository;
using AutoMapper;
using System.ServiceModel.Web;
using System.Net;
using System.Diagnostics;
using RSG.Statistics.Common.Config;
using System.ServiceModel;
using RSG.Base.Configuration.Services;

namespace RSG.Statistics.Server.Services
{
    /// <summary>
    /// 
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class ArchetypeService : ServiceBase, IArchetypeService
    {
        #region Constructor(s)
        /// <summary>
        /// Constructor taking a server configuration object.
        /// </summary>
        /// <param name="server"></param>
        public ArchetypeService(IServiceHostConfig config)
            : base(config)
        {
        }
        #endregion // Constructor(s)

        #region IArchetypeService Implementation
        #region Generic
        /// <summary>
        /// Returns a list of all archetypes.
        /// </summary>
        /// <returns></returns>
        public ArchetypeDtos GetAll()
        {
            return ExecuteCommand(locator => GetAllCommand(locator));
        }
        #endregion // Generic

        #region Drawables
        /// <summary>
        /// Returns a list of all drawables.
        /// </summary>
        /// <returns></returns>
        public ArchetypeDtos GetAllDrawables()
        {
            return ExecuteCommand(locator => GetAllCommand<DrawableArchetype>(locator));
        }

        /// <summary>
        /// Updates or creates a list of drawable archetypes.
        /// </summary>
        /// <param name="dtos"></param>
        public void PutDrawables(ArchetypeDtos dtos)
        {
            ExecuteCommand(locator => PutArchetypesCommand<DrawableArchetype, DrawableArchetypeDto>(locator, dtos));
        }

        /// <summary>
        /// Updates or creates a drawable archetype based on its identifier.
        /// </summary>
        /// <param name="assetId"></param>
        /// <param name="asset"></param>
        /// <returns></returns>
        public DrawableArchetypeDto PutDrawable(string identifier, DrawableArchetypeDto dto)
        {
            return ExecuteCommand(locator => PutArchetypeCommand<DrawableArchetype, DrawableArchetypeDto>(locator, identifier, dto));
        }
        #endregion // Drawables

        #region Fragments
        /// <summary>
        /// Returns a list of all fragments.
        /// </summary>
        /// <returns></returns>
        public ArchetypeDtos GetAllFragments()
        {
            return ExecuteCommand(locator => GetAllCommand<FragmentArchetype>(locator));
        }

        /// <summary>
        /// Updates or creates a list of fragment archetypes.
        /// </summary>
        /// <param name="dtos"></param>
        public void PutFragments(ArchetypeDtos dtos)
        {
            ExecuteCommand(locator => PutArchetypesCommand<FragmentArchetype, FragmentArchetypeDto>(locator, dtos));
        }

        /// <summary>
        /// Updates or creates a fragment archetype based on its identifier.
        /// </summary>
        /// <param name="assetId"></param>
        /// <param name="asset"></param>
        /// <returns></returns>
        public FragmentArchetypeDto PutFragment(string identifier, FragmentArchetypeDto dto)
        {
            return ExecuteCommand(locator => PutArchetypeCommand<FragmentArchetype, FragmentArchetypeDto>(locator, identifier, dto));
        }
        #endregion // Fragments

        #region Stated Anims
        /// <summary>
        /// Returns a list of all stated anims.
        /// </summary>
        /// <returns></returns>
        public ArchetypeDtos GetAllStatedAnims()
        {
            return ExecuteCommand(locator => GetAllCommand<StatedAnimArchetype>(locator));
        }

        /// <summary>
        /// Updates or creates a list of stated anim archetypes.
        /// </summary>
        /// <param name="dtos"></param>
        public void PutStatedAnims(ArchetypeDtos dtos)
        {
            ExecuteCommand(locator => PutArchetypesCommand<StatedAnimArchetype, StatedAnimArchetypeDto>(locator, dtos));
        }

        /// <summary>
        /// Updates or creates a stated anim archetype based on its identifier
        /// </summary>
        /// <param name="assetId"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        public StatedAnimArchetypeDto PutStatedAnim(string identifier, StatedAnimArchetypeDto dto)
        {
            return ExecuteCommand(locator => PutArchetypeCommand<StatedAnimArchetype, StatedAnimArchetypeDto>(locator, identifier, dto));
        }
        #endregion // Stated Anims

        #region Interiors
        /// <summary>
        /// Returns a list of all interiors.
        /// </summary>
        /// <returns></returns>
        public ArchetypeDtos GetAllInteriors()
        {
            return ExecuteCommand(locator => GetAllCommand<InteriorArchetype>(locator));
        }

        /// <summary>
        /// Updates or creates a list of interior archetypes.
        /// </summary>
        /// <param name="dtos"></param>
        public void PutInteriors(ArchetypeDtos dtos)
        {
            ExecuteCommand(locator => PutArchetypesCommand<InteriorArchetype, InteriorArchetypeDto>(locator, dtos));
        }

        /// <summary>
        /// Updates or creates an interior archetype based on its identifier.
        /// </summary>
        /// <param name="assetId"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        public InteriorArchetypeDto PutInterior(string identifier, InteriorArchetypeDto dto)
        {
            return ExecuteCommand(locator => PutArchetypeCommand<InteriorArchetype, InteriorArchetypeDto>(locator, identifier, dto));
        }
        #endregion // Interiors
        #endregion // IArchetypeService Implementation

        #region Private Methods
        #region Generic
        /// <summary>
        /// Returns a list of all archetypes.
        /// </summary>
        /// <returns></returns>
        public ArchetypeDtos GetAllCommand(IRepositoryLocator locator)
        {
            ArchetypeDtos result = new ArchetypeDtos();
            result.Items = Mapper.Map<IQueryable<Archetype>, List<ArchetypeDto>>(locator.FindAll<Archetype>());
            return result;
        }

        /// <summary>
        /// Returns a list of all archetypes of a particular type.
        /// </summary>
        /// <returns></returns>
        public ArchetypeDtos GetAllCommand<TArchetype>(IRepositoryLocator locator) where TArchetype : Archetype
        {
            ArchetypeDtos result = new ArchetypeDtos();
            result.Items = Mapper.Map<IQueryable<TArchetype>, List<ArchetypeDto>>(locator.FindAll<TArchetype>());
            return result;
        }

        /// <summary>
        /// Creates new archetypes as required.
        /// </summary>
        /// <typeparam name="TArchetype"></typeparam>
        /// <param name="locator"></param>
        /// <param name="dtos"></param>
        public void PutArchetypesCommand<TArchetype, TArchetypeDto>(IRepositoryLocator locator, ArchetypeDtos dtos)
            where TArchetype : Archetype, new()
            where TArchetypeDto : ArchetypeDto
        {
            foreach (ArchetypeDto dto in dtos.Items)
            {
                Debug.Assert(dto is TArchetypeDto, "Invalid type of archetype passed through in archetype list.");

                if (dto is TArchetypeDto)
                {
                    PutArchetypeCommand<TArchetype, TArchetypeDto>(locator, dto.Identifier, (TArchetypeDto)dto);
                }
            }
        }

        /// <summary>
        /// Updates or creates an archetype based on its identifier.
        /// </summary>
        /// <param name="assetId"></param>
        /// <param name="asset"></param>
        /// <returns></returns>
        public TArchetypeDto PutArchetypeCommand<TArchetype, TArchetypeDto>(IRepositoryLocator locator, string identifier, TArchetypeDto dto)
            where TArchetype : Archetype, new()
            where TArchetypeDto : ArchetypeDto
        {
            if (identifier != dto.Identifier)
            {
                throw new WebFaultException<string>("Archetype identifier specified in the Uri doesn't match the dto.", HttpStatusCode.BadRequest);
            }

            // Get the asset from the database
            TArchetype archetype = GetAssetByIdentifier<TArchetype>(locator, identifier, false);

            // If it doesn't exist, create a new one
            if (archetype == null)
            {
                archetype = new TArchetype();
                archetype.Id = 0;
            }

            // Update the asset, save it to the db and then return a dto version of it
            archetype.Name = dto.Name;
            archetype.Identifier = dto.Identifier;
            locator.SaveOrUpdate(archetype);
            return Mapper.Map<TArchetype, TArchetypeDto>(archetype);
        }
        #endregion // Generic
        #endregion // Private Methods
    } // ArchetypeService
}
