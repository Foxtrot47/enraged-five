﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Statistics.Client.Vertica.Client;
using AssetsModel = RSG.Statistics.Common.Model.GameAssets;
using RSG.Statistics.Common.Model.Telemetry;
using RSG.Statistics.Common.Model.Telemetry.PerUser;
using RSG.Statistics.Common.Report;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Server.Repository;
using NHibernate;
using RSG.Statistics.Domain.Entities.SocialClub;
using NHibernate.Criterion;
using RSG.ROS;
using RSG.ROS.UGC;
using RSG.Base.Configuration.ROS;
using RSG.Base.Configuration;
using RSG.Statistics.Server.Report.Util;
using RSG.Statistics.Common.Config;

namespace RSG.Statistics.Server.Report.Telemetry.PerUser
{
    /// <summary>
    /// Report that provides a player's multiplayer cash summary.
    /// </summary>
    [Export(typeof(IStatsReport))]
    internal class VerticaPerUserCashReport : VerticaReportBase<PerUserHourlyCashStat>
    {
        #region Constants
        /// <summary>
        /// Name of the report.
        /// </summary>
        private const String c_name = "VerticaPerUserCashReport";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Account to retrieve information for.
        /// </summary>
        [ReportParameter(Required = true)]
        public uint AccountId { get; set; }

        /// <summary>
        /// Platform that the gamerhandle belongs to.
        /// </summary>
        [ReportParameter(Required = true)]
        public String PlatformIdentifier { get; set; }

        /// <summary>
        /// Convenience accessor for the platform.
        /// </summary>
        public ROSPlatform Platform
        {
            get
            {
                ROSPlatform platform;
                if (!Enum.TryParse(PlatformIdentifier, out platform))
                {
                    platform = ROSPlatform.Unknown;
                }
                return platform;
            }
        }

        /// <summary>
        /// Character slot we wish to view data for.
        /// </summary>
        [ReportParameter(Required = true)]
        public uint CharacterSlot { get; set; }

        /// <summary>
        /// Character id we wish to view data for.
        /// </summary>
        [ReportParameter(Required = true)]
        public uint CharacterId { get; set; }

        /// <summary>
        /// Optional build identifier to restrict the results to.
        /// </summary>
        [ReportParameter]
        public List<String> BuildIdentifiers { get; set; }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public VerticaPerUserCashReport()
            : base(c_name)
        {
        }
        #endregion // Constructor(s)
        
        #region IReportDataProvider Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        /// <returns></returns>
        protected override PerUserHourlyCashStat Generate(IRepositoryLocator locator, ILog log, IStatsServer server)
        {
            // Convert the parameters to something vertica can work with.
            MPUserHourlyParams queryParams = new MPUserHourlyParams();
            queryParams.AccountId = AccountId;
            queryParams.Platform = Platform;
            queryParams.CharacterSlot = CharacterSlot;
            queryParams.CharacterId = CharacterId;
            queryParams.Builds = BuildIdentifiers.Select(item => ConvertBuildIdentifierToVersion(item)).ToList();

            // Get the results from the vertica service.
            PerUserHourlyCashStat result;
            using (ReportClient client = new ReportClient(server.VerticaServer))
            {
                result = (PerUserHourlyCashStat)client.RunReport(ReportNames.PerUserMPHourlyCashReport, queryParams);
            }

            // Set the rank and unlock data.
            SetXPRankAndUnlockInfo(result.Data, locator);
            ResolveXPHashes(result.Data, locator, log);
            return result;
        }
        #endregion // IReportDataProvider Implementation

        #region Private Methods
        /// <summary>
        /// Sets the rank, xp and unlock information for the list of hourly stats.
        /// </summary>
        /// <param name="stats"></param>
        /// <param name="locator"></param>
        private void SetXPRankAndUnlockInfo(List<HourlyCashStat> stats, IRepositoryLocator locator)
        {
            // Get the list of ranks/unlocks.
            IList<Rank> allRanks = locator.FindAll<Rank>().OrderBy(item => item.Level).ToList();
            IList<Unlock> allUnlocks = locator.FindAll<Unlock>().OrderBy(item => item.Rank).ToList();

            // Update the ranks/unlocks based on the experience.
            int previousRank = -1;
            decimal totalXP = 0;

            for (int i = 0; i < stats.Count(); ++i)
            {
                HourlyCashStat hourlyStat = stats[i];

                // Set the rank based on total xp.
                totalXP += hourlyStat.XPEarned;

                if (hourlyStat.Rank == 0)
                {
                    hourlyStat.Rank = GetRankForExperience(totalXP, allRanks);
                }

                // Check whether the user would have received any unlocks.
                if (hourlyStat.Rank > previousRank)
                {
                    // Determine which unlocks the user has acquired.
                    hourlyStat.Unlocks.AddRange(GetUnlocksForBetweenRanks((int)previousRank, (uint)hourlyStat.Rank, allUnlocks));
                }
                previousRank = (int)hourlyStat.Rank;
            }
        }

        /// <summary>
        /// Returns the rank for a particular experience value.
        /// </summary>
        private uint GetRankForExperience(decimal xp, IList<Rank> allRanks)
        {
            Rank highestRank = allRanks.Where(item => item.Experience <= xp).OrderBy(item => item.Experience).LastOrDefault();
            return (highestRank == null ? 0 : highestRank.Level);
        }

        /// <summary>
        /// Returns the list of unlocks that the user 
        /// </summary>
        /// <param name="previousRank"></param>
        /// <param name="newRank"></param>
        /// <param name="allUnlocks"></param>
        /// <returns></returns>
        private IEnumerable<AssetsModel.Unlock> GetUnlocksForBetweenRanks(int previousRank, uint newRank, IList<Unlock> allUnlocks)
        {
            IEnumerable<Unlock> unlocksAcquired = allUnlocks.Where(item => item.Rank > previousRank && item.Rank <= newRank);

            // Convert the unlocks to "dto" objects.
            foreach (Unlock unlock in unlocksAcquired)
            {
                yield return new AssetsModel.Unlock
                {
                    Name = unlock.Name,
                    Rank = unlock.Rank,
                    UnlockType = unlock.UnlockType.Value.Value
                };
            }
        }

        /// <summary>
        /// Resolves the xp detail's hashes into friendlier strings.
        /// </summary>
        private void ResolveXPHashes(List<HourlyCashStat> stats, IRepositoryLocator locator, ILog log)
        {
            IGameAssetRepository<XPCategory> categoryRepo = (IGameAssetRepository<XPCategory>)locator.GetRepository<XPCategory>();
            IGameAssetRepository<XPType> typeRepo = (IGameAssetRepository<XPType>)locator.GetRepository<XPType>();
            IDictionary<uint, XPCategory> categoryLookup = categoryRepo.CreateAssetLookup();
            IDictionary<uint, XPType> typeLookup = typeRepo.CreateAssetLookup();

            foreach (HourlyCashStat stat in stats)
            {
                foreach (HourlyCashStatDetail detail in stat.XPEarnedDetails)
                {
                    // Resolve the category first.
                    XPCategory category;
                    if (categoryLookup.TryGetValue(UInt32.Parse(detail.Category), out category))
                    {
                        detail.Category = category.FriendlyName;
                    }
                    else
                    {
                        log.Warning("Unknown xp category hash encountered in data returned from Vertica. Hash: {0}", detail.Category);
                    }

                    // Resolve the type next.
                    XPType type;
                    if (typeLookup.TryGetValue(UInt32.Parse(detail.Action), out type))
                    {
                        detail.Action = type.FriendlyName;
                    }
                    else
                    {
                        log.Warning("Unknown xp type hash encountered in data returned from Vertica. Hash: {0}", detail.Action);
                    }
                }
            }
        }
        #endregion // Private Methods
    } // VerticaMPCashReport
}
