﻿using System.ServiceModel;
using RSG.Base.Configuration.Services;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Dto.GameAssets;
using RSG.Statistics.Common.ServiceContract;
using RSG.Statistics.Domain.Entities.GameAssets;

namespace RSG.Statistics.Server.Services
{
    /// <summary>
    /// 
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class CutsceneService : GameAssetServiceBase<Cutscene, CutsceneDto, CutsceneDtos>, ICutsceneService
    {
        #region Constructor(s)
        /// <summary>
        /// Constructor taking a server configuration object.
        /// </summary>
        /// <param name="server"></param>
        public CutsceneService(IServiceHostConfig config)
            : base(config)
        {
        }
        #endregion // Constructor(s)
    } // CutsceneService
}
