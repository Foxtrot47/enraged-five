﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Server.Report
{
    /// <summary>
    /// 
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class ReportParameterAttribute : Attribute
    {
        #region Properties
        /// <summary>
        /// Whether the user must provide the parameter.
        /// </summary>
        public bool Required { get; set; }

        /// <summary>
        /// Name of the method to use to determine whether a particular parameters value is valid.
        /// </summary>
        public String ValidationMethodName { get; private set; }

        /// <summary>
        /// Type of the object that the validation method belongs to.
        /// </summary>
        public Type ValidationDeclaringType { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public ReportParameterAttribute()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="methodName"></param>
        /// <param name="declaringType"></param>
        public ReportParameterAttribute(String validationMethodName, Type validationDeclaringType)
        {
            ValidationMethodName = validationMethodName;
            ValidationDeclaringType = validationDeclaringType;
        }
        #endregion // Constructor(s)
    } // ReportParameterAttribute
}
