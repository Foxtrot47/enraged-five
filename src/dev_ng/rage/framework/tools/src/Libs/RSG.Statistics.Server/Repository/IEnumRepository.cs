﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Domain.Entities;

namespace RSG.Statistics.Server.Repository
{
    /// <summary>
    /// 
    /// </summary>
    internal interface IEnumRepository<TEnum> : IRepository<EnumReference<TEnum>>
        where TEnum : struct
    {
        #region Methods
        /// <summary>
        /// Returns a enum ref object from the db for the requested value.
        /// </summary>
        EnumReference<TEnum> GetByValue(TEnum value);
        #endregion // Methods
    } // IEnumRepository<TDomain, TEnum>
}
