﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Common.Dto.GameAssets;
using RSG.Statistics.Common.ServiceContract;
using RSG.Statistics.Common.Config;
using RSG.Base.Configuration.Services;

namespace RSG.Statistics.Server.Services
{
    /// <summary>
    /// 
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class WeaponService : GameAssetServiceBase<Weapon, WeaponDto, WeaponDtos>, IWeaponService
    {
        #region Constructor(s)
        /// <summary>
        /// Constructor taking a server configuration object.
        /// </summary>
        /// <param name="server"></param>
        public WeaponService(IServiceHostConfig config)
            : base(config)
        {
        }
        #endregion // Constructor(s)
    } // VehicleService
}
