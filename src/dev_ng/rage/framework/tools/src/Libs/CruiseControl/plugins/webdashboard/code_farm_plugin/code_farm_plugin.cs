﻿using Exortech.NetReflector;
using ThoughtWorks.CruiseControl.WebDashboard.Dashboard;
using ThoughtWorks.CruiseControl.WebDashboard.IO;
using ThoughtWorks.CruiseControl.WebDashboard.MVC;
using ThoughtWorks.CruiseControl.WebDashboard.MVC.Cruise;
using ThoughtWorks.CruiseControl.WebDashboard.Dashboard.Actions;
using ThoughtWorks.CruiseControl.WebDashboard.Dashboard.GenericPlugins;

namespace code_farm_plugin
{
    [ReflectorType("codeFarmReportFarmPlugin")]
    public class CodeFarmReportFarmPlugin : ICruiseAction, IPlugin
    {
        public static readonly string ACTION_NAME = "ViewCodeFarmReport";

        private readonly IProjectGridAction projectGridAction;

        public CodeFarmReportFarmPlugin(IProjectGridAction projectGridAction, IActionInstantiator actionInstantiator)
        {
            this.projectGridAction = projectGridAction;
        }

        public IResponse Execute(ICruiseRequest request)
        {
            return projectGridAction.Execute(ACTION_NAME, request.Request);
        }

        public string LinkDescription
        {
            get { return "Core Code Farm Report"; }
        }

        public INamedAction[] NamedActions
        {
            get { return new INamedAction[] { new ImmutableNamedAction(ACTION_NAME, this) }; }
        }
    }
}
