﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIO = System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;

using RSG.Base.Math;
using RSG.Base.Configuration;
using RSG.Pipeline.Content;
using RSG.Pipeline.Core;
using RSG.Pipeline.Services.Metadata;
using RSG.Pipeline.Services.InstancePlacement;
using RSG.Base.Logging.Universal;
using RSG.SceneXml;

namespace RSG.Pipeline.InstancePlacementProcessor
{
    public class TextureSplicer
    {
        #region Constants
        private static readonly String LOG_CTX = "Instance Placement Processor";
        #endregion Constants

        #region Properties
        private static String TextureMetadataPath;
        private static TextureMetadata TextureMetadata;
        private static IBranch Branch;
        private static IUniversalLog Log;
        private static IContentTree ContentTree;
        private static ContentTreeHelper ContentHelper;
        private static IProcessorCollection Processors;
        #endregion

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="tree"></param>
        /// <param name="contentHelper"></param>
        /// <param name="log"></param>
        public TextureSplicer(IBranch branch, IContentTree tree, ContentTreeHelper contentHelper,
            IUniversalLog log)
        {
            Branch = branch;
            ContentTree = tree;
            ContentHelper = contentHelper;
            Log = log;

            TextureMetadataPath = SIO.Path.Combine(branch.Project.Config.ToolsConfig,
                "processors", "RSG.Pipeline.InstancePlacementProcessor.xml");
            TextureMetadata = TextureMetadata.Load(TextureMetadataPath, branch, log);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Takes the input texture and sorts out everything required to splice the image into its smaller sub-section
        /// </summary>
        /// <param name="inputTexture"></param>
        /// <param name="textureOutput"></param>
        /// <returns></returns>
        public bool SpliceTextures(String inputTexture, String boundsManifestXML, IEnumerable<InstancePlacementTextureSerialiserOutput> textureOutput)
        {
            if (!SIO.File.Exists(boundsManifestXML))
            {
                Log.ErrorCtx(LOG_CTX, "Could not find bounds manifest file: {0}", boundsManifestXML);
                return false;
            }

            if (!SIO.File.Exists(inputTexture))
            {
                Log.ErrorCtx(LOG_CTX, "Could not find input texture to splice: {0}", inputTexture);
                return false;
            }

            SIO.FileStream texStream = new SIO.FileStream(inputTexture, SIO.FileMode.Open, SIO.FileAccess.Read);
            Bitmap texMap = new Bitmap(texStream);

            if (texMap == null)
            {
                Log.ErrorCtx(LOG_CTX, "Problem loading input texture: {0}", inputTexture);
                return false;
            }

            IEnumerable<BoundsEntry> boundsEntries = BoundsEntry.LoadBoundsManifest(boundsManifestXML);

            foreach (InstancePlacementTextureSerialiserOutput output in textureOutput)
            {
                String collisionPath = output.CollisionPath;
                String textureFilename = output.TextureFilename;

                // Shouldn't be a lot of entries so this shouldn't really matter doing it this way.
                BoundsEntry boundsEntry = boundsEntries.Where(entry => String.Equals(entry.CollisionPath, collisionPath, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();

                if (boundsEntry == null)
                {
                    Log.ErrorCtx(LOG_CTX, "Could not find collision in manifest file to calculate texture bounds: {0}", collisionPath);
                    return false;
                }

                BoundingBox3f worldExtents = new BoundingBox3f(new Vector3f(TextureMetadata.Bounds.MinBounds.X, TextureMetadata.Bounds.MinBounds.Y, 0.0f),
                                                                new Vector3f(TextureMetadata.Bounds.MaxBounds.X, TextureMetadata.Bounds.MaxBounds.Y, 0.0f));

                if (!GenerateSplicedTexture(textureFilename, worldExtents, boundsEntry.BoundingBox, ref texMap))
                {
                    Log.ErrorCtx(LOG_CTX, String.Format("Failed to process bounds {0}.  Aborting instance placement.", collisionPath));
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Calculates the position in the texture based on the bounds and creates a new texture from that region.
        /// </summary>
        /// <param name="textureFilename"></param>
        /// <param name="worldExtents"></param>
        /// <param name="boundExtents"></param>
        /// <returns></returns>
        private bool GenerateSplicedTexture(String textureFilename, BoundingBox3f worldExtents, BoundingBox3f boundExtents, ref Bitmap sourceImage)
        {
            // Find the width/height of our container
            int height = (int)Math.Round(Math.Abs(boundExtents.Max.Y - boundExtents.Min.Y));
            int width = (int)Math.Round(Math.Abs(boundExtents.Max.X - boundExtents.Min.X));

            // Find our position in the world
            int minX = (int)Math.Abs(boundExtents.Min.X - worldExtents.Min.X);
            int minY = (int)Math.Abs(worldExtents.Max.Y - boundExtents.Max.Y);

            // If the edge of the world doesn't match the image width properly, we'll overflow.
            // Catch the case and clamp it to the maximum texture size otherwise we crap out.
            if (minX + width >= sourceImage.Width)
                width = (sourceImage.Width - minX) - 1;
            if (minY + height > sourceImage.Height)
                height = (sourceImage.Height - minY) - 1;

            Bitmap carvedBitmap = sourceImage.Clone(new Rectangle(minX, minY, width, height), sourceImage.PixelFormat);
            String outputDirectory = SIO.Path.GetDirectoryName(textureFilename);

            if (!System.IO.Directory.Exists(outputDirectory))
                System.IO.Directory.CreateDirectory(outputDirectory);

            carvedBitmap.Save(textureFilename, ImageFormat.Bmp);
            Log.MessageCtx(LOG_CTX, "Generated split texture: {0}", textureFilename);

            return true;
        }
        #endregion
    }
}
