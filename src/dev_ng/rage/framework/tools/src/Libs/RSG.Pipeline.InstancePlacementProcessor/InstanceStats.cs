﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Xml;
using System.Xml.Linq;

using System.Drawing;
using System.Drawing.Imaging;

using RSG.Base.Math;
using RSG.SceneXml.MapExport;
using RSG.Pipeline.Services;
using RSG.Pipeline.Services.Metadata;
using RSG.Pipeline.Services.InstancePlacement;

namespace RSG.Pipeline.InstancePlacementProcessor
{
    /// <summary>
    /// Holds all the stats for the instance placement processor.
    /// Saves off batch information as well as a vector representation of the batch layout.
    /// </summary>
    public class InstanceStats
    {
        /// <summary>
        /// Stores all the instance lists (batches) stats.
        /// </summary>
        private InstancePlacementContainerStat containerStats;

        /// <summary>
        /// World extents for the current container.
        /// Needed for image size and positional calculation.
        /// </summary>
        private BoundingBox3f worldExtents;

        /// <summary>
        /// Instance placement options.
        /// </summary>
        private InstancePlacementSerialiserOptions placementOptions;

        /// <summary>
        /// Initialize the lists.
        /// </summary>
        public InstanceStats(BoundingBox3f extents, InstancePlacementSerialiserOptions options, string container, string region)
        {
            containerStats = new InstancePlacementContainerStat(container, region);
            worldExtents = extents;
            placementOptions = options;
        }

        /// <summary>
        /// Add batch lists to the stats.
        /// </summary>
        /// <param name="instanceList"></param>
        public void AddBatchList(List<InstancedEntityList> batchList)
        {
            foreach (InstancedEntityList list in batchList)
            {
                String archetypeName = list[0].ArchetypeName;
                int instanceCount = list.Count;
                BoundingBox3f boundingBox = list.BoundingBox;

                Vector3f boundsExtents = list.BoundingBox.Max - list.BoundingBox.Min;

                float volume = boundsExtents.X * boundsExtents.Y * boundsExtents.Z;
                float occupancy = instanceCount / volume;

                InstancePlacementArchetypeStat archetypeStat = new InstancePlacementArchetypeStat(archetypeName);
                archetypeStat.ListColour = list.ListColour;
                archetypeStat.AddInstanceBatch(instanceCount, occupancy, volume, boundingBox);

                containerStats.AddBatchStats(archetypeName, archetypeStat);
            }
        }

        /// <summary>
        /// Save off the stats which will include the rough stats as well as a vector
        /// image that will coincide with the data.
        /// </summary>
        /// <param name="statsFilename"></param>
        public void SaveStats(string statsFilename, string imageFilename)
        {
            InstancePlacementStatsUtil.SaveContainerStatsFile(statsFilename, imageFilename, worldExtents, containerStats);
        }
    }
}
