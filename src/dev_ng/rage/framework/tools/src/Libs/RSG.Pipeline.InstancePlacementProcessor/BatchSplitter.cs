﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Base.Math;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Services.Metadata;
using RSG.SceneXml.MapExport;

namespace RSG.Pipeline.InstancePlacementProcessor
{
    /// <summary>
    /// Will split up the entity lists into seperate lists based on specified criteria.
    /// </summary>
    public class BatchSplitter
    {
        #region Statics
        private static Vector3f ZeroVector = new Vector3f(0.0f, 0.0f, 0.0f);
        private static BoundingBox3f ZeroBoundingBox = new BoundingBox3f(ZeroVector, ZeroVector);
        #endregion

        #region Variables
        private InstancePlacementSerialiserOptions Options;
        private IUniversalLog Log;
        #endregion

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="options"></param>
        public BatchSplitter(InstancePlacementSerialiserOptions options, IUniversalLog log)
        {
            Options = options;
            Log = log;
        }
        #endregion

        /// <summary>
        /// This does the bulk of the work for splitting. We start with a list of entities
        /// and use the bounding box and continue to split based on the longest dimension.
        /// We have a set of criteria which comes from the options to determine when the splitting
        /// is done.
        /// </summary>
        /// <param name="entityList"></param>
        /// <param name="validEntityLists"></param>
        public void Split(InstancedEntityList entityList, ref List<InstancedEntityList> validEntityLists)
        {
            // Check for a degenerate batch, we don't want to keep them.
            if (CheckForDegenerateBatch(entityList))
            {
                Log.Warning("Removed degenerate batch with ({0}) entities", entityList.Count);
                return;
            }

            // If we've already met the criteria right off the bat, don't split it up.
            if (CheckForStopCriteria(entityList))
            {
                validEntityLists.Add(entityList);
                return;
            }

            float width = entityList.BoundingBox.Max.X - entityList.BoundingBox.Min.X;
            float height = entityList.BoundingBox.Max.Y - entityList.BoundingBox.Min.Y;

            // Figure out the dimension we want to split on.
            if (width > height)
            {
                //Splitting on the X axis. Find our average X point for optimal splitting.
                float splitX = 0;
                if (Options.UseAveragePositionSplitting)
                {
                    float positionAverage = 0;
                    int averageCount = 0;
                    foreach (InstancedEntity entity in entityList)
                    {
                        positionAverage += entity.Position.X;
                        averageCount++;
                    }

                    float averageX = positionAverage / averageCount;
                    splitX = averageX - entityList.BoundingBox.Min.X;
                }
                else
                {
                    splitX = width / 2;
                }

                // Create our split boxes.
                Vector3f leftMax = new Vector3f(entityList.BoundingBox.Min.X + splitX, entityList.BoundingBox.Max.Y, entityList.BoundingBox.Max.Z);
                Vector3f rightMin = new Vector3f(entityList.BoundingBox.Min.X + splitX, entityList.BoundingBox.Min.Y, entityList.BoundingBox.Min.Z);

                BoundingBox3f left = new BoundingBox3f(entityList.BoundingBox.Min, leftMax);
                BoundingBox3f right = new BoundingBox3f(rightMin, entityList.BoundingBox.Max);

                InstancedEntityList leftList = new InstancedEntityList();
                InstancedEntityList rightList = new InstancedEntityList();

                // Determine which side they are in.
                foreach (InstancedEntity entity in entityList)
                {
                    if (left.Contains(entity.Position))
                        leftList.Add(entity);
                    else if (right.Contains(entity.Position))
                        rightList.Add(entity);
                }

                leftList.Process(ZeroBoundingBox, -1.0f, 0.0f, 0.0f, 0.0f, ZeroVector, 0);
                rightList.Process(ZeroBoundingBox, -1.0f, 0.0f, 0.0f, 0.0f, ZeroVector, 0);

                leftList.ListColour = entityList.ListColour;
                rightList.ListColour = entityList.ListColour;

                if (rightList.Count > 0 && !CheckForDegenerateBatch(rightList))
                {
                    if (CheckForStopCriteria(rightList))
                        validEntityLists.Add(rightList);
                    else
                        Split(rightList, ref validEntityLists);
                }
                else
                {
                    Log.Warning("Removed degenerate batch with ({0}) entities", rightList.Count);
                }

                if (leftList.Count > 0 && !CheckForDegenerateBatch(leftList))
                {
                    if (CheckForStopCriteria(leftList))
                        validEntityLists.Add(leftList);
                    else
                        Split(leftList, ref validEntityLists);
                }
                else
                {
                    Log.Warning("Removed degenerate batch with ({0}) entities", leftList.Count);
                }
            }
            else
            {
                //Splitting on the Y axis. Find our average Y point for optimal splitting.
                float splitY = 0;
                if (Options.UseAveragePositionSplitting)
                {
                    float positionAverage = 0;
                    int averageCount = 0;
                    foreach (InstancedEntity entity in entityList)
                    {
                        positionAverage += entity.Position.Y;
                        averageCount++;
                    }

                    float averageY = positionAverage / averageCount;
                    splitY = averageY - entityList.BoundingBox.Min.Y;
                }
                else
                {
                    splitY = height / 2;
                }

                // Create our split boxes.
                Vector3f topMin = new Vector3f(entityList.BoundingBox.Min.X, entityList.BoundingBox.Min.Y + splitY, entityList.BoundingBox.Min.Z);
                Vector3f bottomMax = new Vector3f(entityList.BoundingBox.Max.X, entityList.BoundingBox.Min.Y + splitY, entityList.BoundingBox.Max.Z);

                BoundingBox3f top = new BoundingBox3f(topMin, entityList.BoundingBox.Max);
                BoundingBox3f bottom = new BoundingBox3f(entityList.BoundingBox.Min, bottomMax);

                InstancedEntityList topList = new InstancedEntityList();
                InstancedEntityList bottomList = new InstancedEntityList();

                // Determine which side they are in.
                foreach (InstancedEntity entity in entityList)
                {
                    if (top.Contains(entity.Position))
                        topList.Add(entity);
                    else if (bottom.Contains(entity.Position))
                        bottomList.Add(entity);
                }

                topList.Process(ZeroBoundingBox, -1.0f, 0.0f, 0.0f, 0.0f, ZeroVector, 0);
                bottomList.Process(ZeroBoundingBox, -1.0f, 0.0f, 0.0f, 0.0f, ZeroVector, 0);

                topList.ListColour = entityList.ListColour;
                bottomList.ListColour = entityList.ListColour;

                if (topList.Count > 0 && !CheckForDegenerateBatch(topList))
                {
                    if (CheckForStopCriteria(topList))
                        validEntityLists.Add(topList);
                    else
                        Split(topList, ref validEntityLists);
                }
                else
                {
                    Log.Warning("Removed degenerate batch with ({0}) entities", topList.Count);
                }


                if (bottomList.Count > 0 && !CheckForDegenerateBatch(bottomList))
                {
                    if (CheckForStopCriteria(bottomList))
                        validEntityLists.Add(bottomList);
                    else
                        Split(bottomList, ref validEntityLists);
                }
                else
                {
                    Log.Warning("Removed degenerate batch with ({0}) entities", bottomList.Count);
                }
            }
        }

        /// <summary>
        /// Checks a handful of criteria to see if we should stop splitting the batches
        /// </summary>
        /// <param name="entityList"></param>
        /// <returns></returns>
        private bool CheckForStopCriteria(InstancedEntityList entityList)
        {
            Vector3f boundsExtents = entityList.BoundingBox.Max - entityList.BoundingBox.Min;

            float volume = boundsExtents.X * boundsExtents.Y * boundsExtents.Z;
            float occupancy = entityList.Count / volume;

            float width = entityList.BoundingBox.Max.X - entityList.BoundingBox.Min.X;
            float height = entityList.BoundingBox.Max.Y - entityList.BoundingBox.Min.Y;

            if (Options.MaxInstancesPerBatch == -1 || entityList.Count() < Options.MaxInstancesPerBatch)
            {
                if (width < Options.MaxBoundLength && height < Options.MaxBoundLength)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Returns true if we have a degenerate batch and don't want to even process it.
        /// </summary>
        /// <param name="entityList"></param>
        /// <returns></returns>
        private bool CheckForDegenerateBatch(InstancedEntityList entityList)
        {
            if (entityList.Count() < Options.MinInstancesPerBatch)
                return true;

            return false;
        }
    }
}
