﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

using RSG.Base.Math;
using RSG.Pipeline.Services;
using RSG.Pipeline.Services.Metadata;
using RSG.SceneXml.MapExport;

namespace RSG.Pipeline.InstancePlacementProcessor
{
    /// <summary>
    /// k-dimensional tree node. 
    /// </summary>
    public class KDTreeNodeEntity
    {
        public Vector3f Location;
        public KDTreeNodeEntity LeftChild;
        public KDTreeNodeEntity RightChild;
        public InstancedEntityList Entities;

        public KDTreeNodeEntity(InstancedEntityList entities)
        {
            Entities = entities;
        }

        public void Visit(ref List<InstancedEntityList> lists)
        {
            if (LeftChild == null && RightChild == null)
                lists.Add(Entities);

            if (LeftChild != null)
                LeftChild.Visit(ref lists);
            if (RightChild != null)
                RightChild.Visit(ref lists);
        }
    }

    public class KDTreeNodeBatch
    {
        public Vector3f Location;
        public KDTreeNodeBatch LeftChild;
        public KDTreeNodeBatch RightChild;
        public List<InstancedEntityList> Batches;

        public KDTreeNodeBatch(List<InstancedEntityList> batches)
        {
            Batches = batches;
        }

        public void Visit(ref List<List<InstancedEntityList>> lists)
        {
            if (LeftChild == null && RightChild == null)
                lists.Add(Batches);

            if (LeftChild != null)
                LeftChild.Visit(ref lists);
            if (RightChild != null)
                RightChild.Visit(ref lists);
        }
    }

    /// <summary>
    /// k-dimensional tree used for spatial partitioning a set of instance objects.
    /// </summary>
    public class KDTree
    {
        #region Properties
        /// <summary>
        /// Number of dimensions to process in the tree.
        /// </summary>
        public int NumDimensions
        {
            get;
            set;
        }

        /// <summary>
        /// The total list of entities to spatially partition.
        /// </summary>
        public InstancedEntityList InstanceEntities
        {
            get;
            set;
        }

        /// <summary>
        /// The total list of batches to spatially partition.
        /// </summary>
        public List<InstancedEntityList> InstanceLists
        {
            get;
            set;
        }

        /// <summary>
        /// Options to specify batch size, instance count, etc.
        /// </summary>
        public InstancePlacementSerialiserOptions Options
        {
            get;
            private set;
        }

        /// <summary>
        /// The top entity node of the tree.
        /// </summary>
        public KDTreeNodeEntity RootEntityNode
        {
            get;
            private set;
        }
        private KDTreeNodeEntity m_RootEntityNode;

        /// <summary>
        /// The top batch node of the tree.
        /// </summary>
        public KDTreeNodeBatch RootBatchNode
        {
            get;
            private set;
        }
        private KDTreeNodeBatch m_RootBatchNode;

        #endregion

        #region Constructors
        public KDTree(int dimensions, InstancedEntityList entities, InstancePlacementSerialiserOptions options)
        {
            Debug.Assert(dimensions > 0 && dimensions < 4);
            NumDimensions = dimensions;
            InstanceEntities = entities;
            Options = options;
        }

        public KDTree(int dimensions, List<InstancedEntityList> batches, InstancePlacementSerialiserOptions options)
        {
            Debug.Assert(dimensions > 0 && dimensions < 4);
            NumDimensions = dimensions;
            InstanceLists = batches;
            Options = options;
        }
        #endregion Constructors

        #region Public Methods
        /// <summary>
        /// Process start function for entities.
        /// </summary>
        public void ProcessEntities()
        {
            m_RootEntityNode = Process(InstanceEntities, 0);
        }

        /// <summary>
        /// Process start function for batches.
        /// </summary>
        public void ProcessBatches()
        {
            m_RootBatchNode = Process(InstanceLists, 0);
        }

        /// <summary>
        /// Acquires a list of all entity lists at the leaves in the tree.
        /// </summary>
        /// <returns></returns>
        public List<InstancedEntityList> GetEntityLeaves()
        {
            List<InstancedEntityList> leafLists = new List<InstancedEntityList>();

            if (m_RootEntityNode != null)
            {
                m_RootEntityNode.Visit(ref leafLists);
            }
            
            return leafLists;
        }

        /// <summary>
        /// Acquires a list of all entity lists at the leaves in the tree.
        /// </summary>
        /// <returns></returns>
        public List<List<InstancedEntityList>> GetBatchLeaves()
        {
            List<List<InstancedEntityList>> leafLists = new List<List<InstancedEntityList>>();

            if (m_RootBatchNode != null)
            {
                m_RootBatchNode.Visit(ref leafLists);
            }

            return leafLists;
        }
        #endregion Public Methods

        #region Private Methods
        /// <summary>
        /// Recursive process function, ordering by a given axes.
        /// Will stop breaking the list into nodes based on the optimal size.
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="depth"></param>
        /// <returns></returns>
        private KDTreeNodeEntity Process(IEnumerable<InstancedEntity> entities, int depth)
        {
            // If we have no entities or too little entities, dump the batch.
            if (entities.Count() == 0)
                return null;

            int axis = depth % NumDimensions;

            IEnumerable<InstancedEntity> orderedList = null;
            if (axis == 0)
                orderedList = entities.OrderBy(instanceEntity => instanceEntity.Position.X);
            else if (axis == 1)
                orderedList = entities.OrderBy(instanceEntity => instanceEntity.Position.Y);
            else if (axis == 2)
                orderedList = entities.OrderBy(instanceEntity => instanceEntity.Position.Z);

            //Find the median value.
            int median = orderedList.Count() / 2;
            InstancedEntity medianEntity = orderedList.ElementAt(median);

            //Create a new tree node.
            InstancedEntityList entityList = new InstancedEntityList();
            entityList.ListColour = InstanceEntities.ListColour;
            entityList.AddRange(entities);
            Vector3f zeroVector = new Vector3f(0.0f, 0.0f, 0.0f);
            BoundingBox3f boundingBox = new BoundingBox3f(zeroVector, zeroVector);
            float orientToTerrain = 0.0f;
            Vector3f zeroScale = new Vector3f(0.0f, 0.0f, 0.0f);
            entityList.Process(boundingBox, -1.0f, 0.0f, 0.0f, orientToTerrain, zeroScale, 0);

            KDTreeNodeEntity newNode = new KDTreeNodeEntity(entityList);
            newNode.Location = medianEntity.Position;

            float width = (entityList.BoundingBox.Max.X - entityList.BoundingBox.Min.X);
            float length = (entityList.BoundingBox.Max.Y - entityList.BoundingBox.Min.Y);
            float zOffset = (entityList.BoundingBox.Max.Z - entityList.BoundingBox.Min.Z);

            int listCount = orderedList.Count();
            
            if (listCount < Options.MaxInstancesPerBatch
                && (Options.MaxBoundLength < 0 || (width < Options.MaxBoundLength && length < Options.MaxBoundLength && zOffset < Options.MaxBoundLength)))
            {
                newNode.LeftChild = null;
                newNode.RightChild = null;
            }
            else
            {
                List<InstancedEntity> list = orderedList.ToList();
                IEnumerable<InstancedEntity> leftChildren = list.GetRange(0, median);
                int leftCount = leftChildren.Count();
                IEnumerable<InstancedEntity> rightChildren = list.GetRange(median, orderedList.Count() - median);
                int rightCount = rightChildren.Count();
                newNode.LeftChild = Process(leftChildren, depth + 1);
                newNode.RightChild = Process(rightChildren, depth + 1);
            }

            return newNode;
        }

        /// <summary>
        /// Recursive process function, ordering by a given axes.
        /// Will stop breaking the list into nodes based on the optimal size.
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="depth"></param>
        /// <returns></returns>
        private KDTreeNodeBatch Process(IEnumerable<InstancedEntityList> batches, int depth)
        {
            // If we have no entities or too little entities, dump the batch.
            if (batches.Count() == 0)
                return null;

            int axis = depth % NumDimensions;

            IEnumerable<InstancedEntityList> orderedList = null;
            if (axis == 0)
                orderedList = batches.OrderBy(batch => batch.BoundingBox.Centre().X);
            else if (axis == 1)
                orderedList = batches.OrderBy(batch => batch.BoundingBox.Centre().Y);
            else if (axis == 2)
                orderedList = batches.OrderBy(batch => batch.BoundingBox.Centre().Z);

            //Find the median value.
            int median = orderedList.Count() / 2;
            InstancedEntityList medianBatch = orderedList.ElementAt(median);

            //Create a new tree node.
            List<InstancedEntityList> batchList = new List<InstancedEntityList>();
            batchList.AddRange(batches);

            KDTreeNodeBatch newNode = new KDTreeNodeBatch(batchList);
            newNode.Location = medianBatch.BoundingBox.Centre();

            if (orderedList.Count() < Options.MaxBatchesPerImap)
            {
                newNode.LeftChild = null;
                newNode.RightChild = null;
            }
            else
            {
                List<InstancedEntityList> batch = orderedList.ToList();
                IEnumerable<InstancedEntityList> leftChildren = batch.GetRange(0, median);
                int leftCount = leftChildren.Count();
                IEnumerable<InstancedEntityList> rightChildren = batch.GetRange(median, orderedList.Count() - median);
                int rightCount = rightChildren.Count();
                newNode.LeftChild = Process(leftChildren, depth + 1);
                newNode.RightChild = Process(rightChildren, depth + 1);
            }

            return newNode;
        }
        #endregion
    }
}
