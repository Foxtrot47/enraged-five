﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Xml.XPath;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.Math;
using RSG.Pipeline.Content.Algorithm;
using RSG.Pipeline.Core;
using RSG.Pipeline.Services;
using RSG.Pipeline.Services.InstancePlacement;
using RSG.Pipeline.Services.Metadata;
using RSG.Platform;
using RSG.SceneXml;
using RSG.SceneXml.MapExport;
using RSG.SceneXml.MapExport.Project.GTA5_NG; // Instance placement is next gen only
using RSG.SceneXml.MapExport.Project.GTA5_NG.Collections;
using RSG.SceneXml.MapExport.Util;

namespace RSG.Pipeline.InstancePlacementProcessor
{
    class PlacementTask
    {
        public PlacementTask(float xCoordinate, float yCoordinate) { x = xCoordinate; y = yCoordinate; }

        public float x;
        public float y;
    }

    /// <summary>
    /// Keeps track of the instance that was rejected along with their 
    /// object name and position.
    /// </summary>
    class InstanceRejection
    {
        public Vector2f Position;
        public String InstanceDefinitionName;

        public InstanceRejection(String name, Vector2f position)
        {
            Position = position;
            InstanceDefinitionName = name;
        }
    }

    class InstancePlacementProcessor
    {
        #region Constants
        /// <summary>
        /// Log context static.
        /// </summary>
        private static readonly String LOG_CTX = "Instance Placement Processor";
        #endregion Constants

        #region Properties
        /// <summary>
        /// Serialisation options for instance placement.
        /// </summary>
        private InstancePlacementSerialiserOptions Options;

        /// <summary>
        /// Instance placement serialisation task.
        /// </summary>
        private InstancePlacementSerialiserTask InstancePlacementTask;

        /// <summary>
        /// Extents for the collision within the container being processed.
        /// </summary>
        private BoundingBox3f ContainerExtents;

        /// <summary>
        /// Partitioned object for accessing/testing against bounds within the container.
        /// </summary>
        private BoundsPartition Partition;

        /// <summary>
        /// Bool to signify is this is a preview export
        /// </summary>
        private bool Preview;

        /// <summary>
        /// Bool to determine if instacne placement should also serialise manifest data.
        /// </summary>
        private bool SerialiseManifestData;

        /// <summary>
        /// Keep a list of all the manifest dependencies for this process.
        /// </summary>
        public List<ManifestDependency> ManifestDependencies;

        /// <summary>
        /// Branch property ensures tool is branch safe.
        /// </summary>
        private static IBranch Branch;

        /// <summary>
        /// Universal log object.
        /// </summary>
        private static IUniversalLog Log;

        /// <summary>
        /// 
        /// </summary>
        private static String TextureMetadataPath;

        /// <summary>
        /// 
        /// </summary>
        private static TextureMetadata TextureMetadata;

        /// <summary>
        /// 
        /// </summary>
        private static String CacheDirectory;

        /// <summary>
        /// 
        /// </summary>
        private static IContentTree ContentTree;

        /// <summary>
        /// 
        /// </summary>
        private static RSG.Pipeline.Content.ContentTreeHelper ContentHelper;

        /// <summary>
        /// 
        /// </summary>
        private static SceneCollection SceneCollection;
        
        /// <summary>
        /// Platform list array split by console generation
        /// </summary>
        private static String[] platformLists = new String[] { "xenon|ps3|x64|orbis|durango|prospero|scarlett" };

        /// <summary>
        /// Path to our instance placement (metadata) XML filename.
        /// </summary>
        private static readonly String InstancePlacementXmlFilename = "$(assets)/metadata/terrain/InstancePlacement.xml";

        /// <summary>
        /// Cached version of bounds' extents.
        /// </summary>
        private static Dictionary<String, BoundingBox3f> ContainerExtentsDictionary;

        /// <summary>
        /// Cached version of the bounds partition.
        /// </summary>
        private static Dictionary<String, BoundsPartition> BoundsPartitionDictionary;
       
        #endregion Properties

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="options"></param>
        /// <param name="task"></param>
        /// <param name="preview"></param>
        /// <param name="serialiseManifestData"></param>
        public InstancePlacementProcessor(InstancePlacementSerialiserOptions options, InstancePlacementSerialiserTask task, bool preview, bool serialiseManifestData)
        {
            this.Options = options;
            this.InstancePlacementTask = task;
            this.Preview = preview;
            this.SerialiseManifestData = serialiseManifestData;
            this.ManifestDependencies = new List<ManifestDependency>();
        }
        #endregion Constructors

        /// <summary>
        /// Loads configuration and metadata related to this processor.
        /// </summary>
        /// <returns></returns>
        public static bool Load(IBranch branch, IContentTree tree, RSG.Pipeline.Content.ContentTreeHelper contentHelper, IUniversalLog log)
        {
            InstancePlacementProcessor.Log = log;
            InstancePlacementProcessor.Branch = branch;
            InstancePlacementProcessor.ContentTree = tree;
            InstancePlacementProcessor.ContentHelper = contentHelper;
            InstancePlacementProcessor.SceneCollection = new SceneCollection(branch, tree, SceneCollectionLoadOption.None);
            InstancePlacementProcessor.CacheDirectory = Path.Combine(Branch.Project.Cache, "maps", Branch.Name, "_instance_placement");

            if (branch.Project.IsDLC)
            {
                InstancePlacementProcessor.TextureMetadataPath = Branch.Environment.Subst(InstancePlacementXmlFilename);
            }
            else
            {
                InstancePlacementProcessor.TextureMetadataPath = Path.Combine(branch.Project.Config.ToolsConfig,
                    "processors\\RSG.Pipeline.InstancePlacementProcessor.xml");
            }
            InstancePlacementProcessor.TextureMetadata = TextureMetadata.Load(TextureMetadataPath, branch, log);
            
            InstancePlacementProcessor.ContainerExtentsDictionary = new Dictionary<String, BoundingBox3f>();
            InstancePlacementProcessor.BoundsPartitionDictionary = new Dictionary<String, BoundsPartition>();

            return true;
        }

        /// <summary>
        /// Main processing loop for instance placement.
        /// </summary>
        /// <returns></returns>
        public bool Process(bool generateMapDataInfo)
        {
            // Generate the mapDataInfo.xml file.
            if (generateMapDataInfo)
            {
                return GenerateMapDataInfo();
            }

            //Select the specified category.
            if (!InstancePlacementProcessor.TextureMetadata.SetSelectedCategory(InstancePlacementTask.PlacementType))
            {
                Log.ErrorCtx(LOG_CTX, String.Format("Unable to find placement type with the name {0}.", InstancePlacementTask.PlacementType));
                return false;
            }

            Log.MessageCtx(LOG_CTX, String.Format("Processing task {0}...", InstancePlacementTask.Name));

            //Transform the output.
            String containerName = this.InstancePlacementTask.Name;
            InstancePlacementSerialiserOutput outputInfo = this.InstancePlacementTask.Outputs.First();
            String outputDir = null;
            if (outputInfo.OutputDirectory == null)
            {
                outputDir = Path.GetDirectoryName(outputInfo.OutputFile);
            }
            else
            {
                outputDir = outputInfo.OutputDirectory;
            }

            if (!Directory.Exists(outputDir))
                Directory.CreateDirectory(outputDir);

            //Clear any old data since the pipeline doesn't know about exact outputs.
			//Ensure that stale data is removed.
            String[] files = Directory.GetFiles(outputInfo.OutputDirectory, String.Format("{0}_{1}*.imap", containerName, this.InstancePlacementTask.PlacementType.ToString().ToLower()));
            foreach(String file in files)
            {
                try
                { 
                    File.Delete(file);
                }
                catch(Exception)
                {
                    Log.ErrorCtx(LOG_CTX, "Unable to delete file '{0}'.", file);
                }
            }

            ContainerExtents = new BoundingBox3f();

            if (BoundsPartitionDictionary.ContainsKey(containerName))
            {
                this.ContainerExtents = ContainerExtentsDictionary[containerName];
                this.Partition = BoundsPartitionDictionary[containerName];
            }
            else
            {
                ICollection<OptimisedBound> optimisedBounds = new List<OptimisedBound>();

                BoundsProcessing.ProcessBounds(this.InstancePlacementTask.Inputs.First().CollisionPath, optimisedBounds, this.ContainerExtents, InstancePlacementProcessor.Log);

                /* T.L. - Not using these on V so commenting out for now.
                if (TextureMetadata.LoadAdjacentBounds == true)
                {
                    // This will load the bounds for the containers that overlap the current input.
                    foreach (InstancePlacementIntersectionInput input in InstancePlacementTask.IntersectionInputs)
                    {
                        String collisionZipFile = input.CollisionPath;

                        ICollection<OptimisedBound> intersectionBounds = new List<OptimisedBound>();
                        BoundingBox3f intersectionContainerExtents = new BoundingBox3f();

                        ProcessBounds(collisionZipFile, ref intersectionBounds, ref intersectionContainerExtents);

                        BoundsPartition intersectionPartition = new BoundsPartition(intersectionContainerExtents);
                        intersectionPartition.AddBoundPrimitives(intersectionBounds);
                        intersectionPartition.SortPartitions();

                        intersectingBoundsParititions.Add(intersectionPartition);
                    }
                }
                */

                //Round the bounding box to the nearest integer.  Floating point imprecision
                //could cause the area where this texture is allocated to be incorrect.
                this.ContainerExtents.Min.X = BoundsProcessing.RoundNearest(ContainerExtents.Min.X);
                this.ContainerExtents.Min.Y = BoundsProcessing.RoundNearest(ContainerExtents.Min.Y);
                this.ContainerExtents.Max.X = BoundsProcessing.RoundNearest(ContainerExtents.Max.X);
                this.ContainerExtents.Max.Y = BoundsProcessing.RoundNearest(ContainerExtents.Max.Y);

                Log.MessageCtx(LOG_CTX, "Container Minimum Bounds: ({0}, {1}) Maximum Bounds: ({2}, {3})", this.ContainerExtents.Min.X, this.ContainerExtents.Min.Y, this.ContainerExtents.Max.X, this.ContainerExtents.Max.Y);

                this.Partition = new BoundsPartition(this.ContainerExtents, TextureMetadata.EnableAccurateBoundsRounding);
                this.Partition.AddBoundPrimitives(optimisedBounds, TextureMetadata.EnableAccurateBoundsRounding);

                /* T.L. - Not using these on V so commenting out for now.
                foreach (InstancePlacementRejectionInput input in InstancePlacementTask.RejectionInputs)
                {
                    String collisionZipFile = input.CollisionPath;

                    ICollection<OptimisedBound> rejectionBounds = new List<OptimisedBound>();
                    BoundingBox3f rejectionBoundsExtents = new BoundingBox3f();
                    ProcessRejectionBounds(collisionZipFile, ref rejectionBounds, ref rejectionBoundsExtents);

                    this.Partition.AddBoundPrimitives(rejectionBounds, true);
                }
                */

                this.Partition.SortPartitions();

                ContainerExtentsDictionary.Add(containerName, ContainerExtents);
                BoundsPartitionDictionary.Add(containerName, this.Partition);
            }

            //Load the textures for the bounds.
            if (!TextureMetadata.LoadTextures(this.InstancePlacementTask.InputTextures, this.ContainerExtents))
            {
                return false;
            }
          
            //Find extents of x- and y- coordinates to iterate only through the coordinates we need
            //to process.
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            ParallelOptions options = new ParallelOptions();
            options.MaxDegreeOfParallelism = options.MaxDegreeOfParallelism; //Set to -1 for maximum threads; set to 1 to debug a single thread at a time.

            IEnumerable<PlacementTask> tasks = InstancePlacementProcessor.CreatePlacementTasks(this.ContainerExtents.Min.X, this.ContainerExtents.Max.X, this.ContainerExtents.Min.Y, this.ContainerExtents.Max.Y, InstancePlacementProcessor.TextureMetadata.Resolution);

            InstancePlacementProcessor.Log.ProfileCtx(LOG_CTX, "Processing instance placement tasks...");
            InstanceObject[] instanceObjects = ProcessTasks(tasks, options, this.Partition);

            InstancePlacementProcessor.Log.ProfileEnd();
            InstancePlacementProcessor.Log.ProfileCtx(LOG_CTX, String.Format("{0} objects have been placed.", instanceObjects.Length));

            if (this.InstancePlacementTask.PlacementType == InstancePlacementType.Grass)
            {
                foreach (InstancePlacementSerialiserInput input in this.InstancePlacementTask.Inputs)
                {
                    if (!File.Exists(InstancePlacementTask.Outputs.First().SceneXml))
                    {
                        File.Copy(input.SceneFilename, this.InstancePlacementTask.Outputs.First().SceneXml);
                        // If this is writeable the engine can't clean it up.
                        FileInfo fileInfo = new FileInfo(this.InstancePlacementTask.Outputs.First().SceneXml);
                        fileInfo.IsReadOnly = false;
                        fileInfo.Refresh();
                        break;
                    }
                }
                this.ProcessGrassInstances(instanceObjects, this.Partition);
            }
            else
            {
                if (!this.ProcessPropInstances(instanceObjects))
                    return false;
            }
            InstancePlacementProcessor.Log.ProfileEnd();

            stopwatch.Stop();
            Console.WriteLine(stopwatch.Elapsed.TotalSeconds);

            if (Preview == true)
            {
                Scene scene = InstancePlacementProcessor.SceneCollection.LoadScene(this.InstancePlacementTask.Inputs.First().SceneFilename);

                //Serialise the data to the target file.
                List<Scene> dummySceneList = new List<Scene>();
                InteriorContainerNameMap interiorNameMap = new InteriorContainerNameMap();
                SceneSerialiserIMAP imapSerialiser = new SceneSerialiserIMAP(Branch, dummySceneList.ToArray(), null, null, "", "", interiorNameMap, null);
            
                IMAPPreview.GenerateIMAPsForPreview_GTA5_NG(Branch, outputDir, ContentTree, scene, imapSerialiser, false);
            }

            return true;
        }

        /// <summary>
        /// Generates the MapDataInfo.xml file.
        /// This generates a file with all the containers in the world along with
        /// extra information for each specific container.
        /// </summary>
        /// <returns></returns>
        private bool GenerateMapDataInfo()
        {
            List<MapDataInfo> mapDataInfoList = new List<MapDataInfo>();

            List<Scene> loadedScenes = new List<Scene>();
            IContentNode[] mapExportNodes = ContentHelper.GetAllMapExportNodes();
            foreach (IContentNode mapExportNode in mapExportNodes)
            {
                IEnumerable<IProcess> processes = InstancePlacementProcessor.ContentTree.FindProcessesWithInput(mapExportNode);
                IProcess exportProcess = processes.FirstOrDefault(p =>
                    String.Equals(p.ProcessorClassName, "RSG.Pipeline.Processor.Common.Dcc3dsmaxExportProcessor"));
                if (null != exportProcess)
                {
                    IEnumerable<IContentNode> outputs = exportProcess.Outputs;
                    RSG.Pipeline.Content.File outputFile = outputs.OfType<RSG.Pipeline.Content.File>().
                        FirstOrDefault(f => String.Equals(".zip", f.Extension));

                    RSG.Pipeline.Content.File maxFile = mapExportNode as RSG.Pipeline.Content.File;
                    SceneType sceneType = Scene.GetSceneType(ContentTree, maxFile.AbsolutePath);

                    if (sceneType == SceneType.EnvironmentContainer)
                    {
                        String sceneXmlFilename = System.IO.Path.ChangeExtension(outputFile.AbsolutePath, ".xml");
                        if (File.Exists(sceneXmlFilename))
                        {
                            Scene loadedScene = InstancePlacementProcessor.SceneCollection.LoadScene(sceneXmlFilename);
                            loadedScenes.Add(loadedScene);
                        }
                    }
                }
            }


            foreach (Scene loadedScene in loadedScenes)
            {
                String sceneFileName = Path.ChangeExtension(loadedScene.Filename, ".imap");
                List<Scene> sceneList = new List<Scene>();
                sceneList.Add(loadedScene);
                InteriorContainerNameMap interiorNameMap2 = new InteriorContainerNameMap();
                SceneSerialiserIMAP imapSerialiserTest = new SceneSerialiserIMAP(Branch,
                        sceneList.ToArray(), null, null,
                        sceneFileName, Branch.Build, interiorNameMap2, null);

                imapSerialiserTest.Process(sceneFileName);
                mapDataInfoList.Add(imapSerialiserTest.GetMapDataInfo());
            }

            StreamWriter mapInfoWriter = new StreamWriter(InstancePlacementProcessor.TextureMetadata.MapDataInfoFilename);
            XmlSerializer serialiser = new XmlSerializer(typeof(List<MapDataInfo>));
            serialiser.Serialize(mapInfoWriter, mapDataInfoList);
            mapInfoWriter.Close();

            return true;
        }



        /// <summary>
        /// Processes all tasks, in parallel if specified.
        /// </summary>
        /// <param name="tasks"></param>
        /// <param name="options"></param>
        /// <returns>A list of instance objects placed.</returns>
        private InstanceObject[] ProcessTasks(IEnumerable<PlacementTask> tasks, ParallelOptions options, 
            BoundsPartition partition)
        {
            if (InstancePlacementProcessor.TextureMetadata.EnableTexturePlacement == false)
            {
                InstanceObject[] dummyObjectList = new InstanceObject[0];
                return dummyObjectList;
            }

            ConcurrentStack<InstanceObject> results = new ConcurrentStack<InstanceObject>();
            ConcurrentStack<InstanceRejection> rejections = new ConcurrentStack<InstanceRejection>();
            Parallel.ForEach<PlacementTask>(tasks, options, task =>
            {
                float x = task.x;
                float y = task.y;

                Random random = new Random((InstancePlacementProcessor.TextureMetadata.SelectedCategory.RandomSeed * (int)x) + (int)y);

                Vector3f currentPoint = new Vector3f(x, y, float.MinValue);

                // Get all the vegetation pixels at this position.
                ColorReference[] imageColors = InstancePlacementProcessor.TextureMetadata.GetColorFromImage(currentPoint.X, currentPoint.Y);
                if (imageColors == null)
                {
                    return;
                }

                foreach (ColorReference colorReference in imageColors)
                {
                    // Get the InstanceDefinition from the given pixel.
                    InstanceDefinition[] definitions = InstancePlacementProcessor.TextureMetadata.GetObjectsFromPixel(colorReference.Color);

                    foreach(InstanceDefinition definition in definitions)
                    {
                        // Decide whether to use the global or local data texture depending on the XML definition.
                        Color dataColor = Color.Blue;
                        if (definition.UseLocalDataTexture)
                            dataColor = InstancePlacementProcessor.TextureMetadata.GetPixelData(x, y, colorReference.TextureIndex);
                        else
                            dataColor = InstancePlacementProcessor.TextureMetadata.GetPixelData(x, y, -1);

                        // Decide whether to use the global or local tint texture depending on the XML definition.
                        Color tintColor = Color.White;
                        if (definition.UseLocalTint)
                            tintColor = definition.CalculateTintIntensityVariation(random, InstancePlacementProcessor.TextureMetadata.GetPixelTint(x, y, colorReference.TextureIndex));
                        else
                            tintColor = definition.CalculateTintIntensityVariation(random, InstancePlacementProcessor.TextureMetadata.GetPixelTint(x, y, -1));

                        OptimisedPrimitive tri = null;

                        if (InstancePlacementTask.PlacementType != InstancePlacementType.Grass)
                        {
                            currentPoint.X += (Convert.ToSingle(random.NextDouble()) * InstancePlacementProcessor.TextureMetadata.Bounds.PixelResolution) - InstancePlacementProcessor.TextureMetadata.MaxPositionOffset;
                            currentPoint.Y += (Convert.ToSingle(random.NextDouble()) * InstancePlacementProcessor.TextureMetadata.Bounds.PixelResolution) - InstancePlacementProcessor.TextureMetadata.MaxPositionOffset;

                            if (partition.TestCollision(currentPoint, out tri))
                            {
                                // Are we on a specific material we shouldn't be dropping on based on the exclusion type.
                                if (InstancePlacementProcessor.TextureMetadata.CheckForExclusion(definition.ExclusionType, tri.MaterialName))
                                    continue;

                                if (partition.TestForRejection(definition.MaxSlope, tri))
                                {
                                    // Fudge factor to move the instance down a bit.
                                    currentPoint.Z -= definition.HeightCompensation;

                                    InstanceObject instance = InstancePlacementProcessor.TextureMetadata.CreateInstance(definition, currentPoint, tri.Plane, tri.Color, dataColor, tintColor, true);
                                    results.Push(instance);
                                }
                            }
                        }
                        else
                        {
                            if (partition.TestCollision(currentPoint, out tri))
                            {
                                float density = definition.CalculateDensity((float)dataColor.G);

                                float radian = (float)(System.Math.PI * 2.0f) / density;
                                float radius = definition.Radius;

                                float rotationOffset = Convert.ToSingle(random.NextDouble() * System.Math.PI);

                                for (int objectIndex = 0; objectIndex < density; ++objectIndex)
                                {
                                    Vector3f forward = new Vector3f(radius, 0, 0);
                                    forward.RotateAboutAxis(rotationOffset + (radian * objectIndex), 'z');
                                    Vector3f newPosition = currentPoint + forward;

                                    newPosition.X += (Convert.ToSingle(random.NextDouble()) * InstancePlacementProcessor.TextureMetadata.Bounds.PixelResolution) - InstancePlacementProcessor.TextureMetadata.MaxPositionOffset;
                                    newPosition.Y += (Convert.ToSingle(random.NextDouble()) * InstancePlacementProcessor.TextureMetadata.Bounds.PixelResolution) - InstancePlacementProcessor.TextureMetadata.MaxPositionOffset;

                                    if (partition.TestCollision(newPosition, out tri))
                                    {
                                        // Are we on a specific material we shouldn't be dropping on based on the exclusion type.
                                        if (InstancePlacementProcessor.TextureMetadata.CheckForExclusion(definition.ExclusionType, tri.MaterialName))
                                            continue;

                                        if (partition.TestForRejection(definition.MaxSlope, tri))
                                        {
                                            // Fudge factor to move the grass down a bit.
                                            newPosition.Z -= definition.HeightCompensation;

                                            InstanceObject instance = InstancePlacementProcessor.TextureMetadata.CreateInstance(definition, newPosition, tri.Plane, tri.Color, dataColor, tintColor);
                                            results.Push(instance);
                                        }
                                        else
                                        {
                                            InstanceRejection rejection = new InstanceRejection(definition.Name, new Vector2f(newPosition.X, newPosition.Y));
                                            rejections.Push(rejection);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            });

            //Create a list of InstanceObjects that will be dropped.
            InstanceObject[] instanceObjects = new InstanceObject[results.Count];
            if (results.Count > 0)
                results.TryPopRange(instanceObjects);

            // Currently just spitting out the count but might want to use the info and spit it out to a file
            // with the name and position of the rejection in case someone wants to analyze the results.
            if (rejections.Count > 0)
                Log.WarningCtx(LOG_CTX, "{0} instances were rejected due to bad placements", rejections.Count);

            return instanceObjects;
        }

        /// <summary>
        /// DEBUG FUNCTION
        /// If you want to visualize the drops that are happening in relation to a max container you can call this
        /// function with a stream file for every drop in the instance placement processor. This will generate
        /// a test maxscript file that will create a 1m X 1m box for every position and then just execute the
        /// maxscript in Max to create all the boxes and see where the placements are happening.
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="position"></param>
        /// <param name="boxCount"></param>
        private void GenerateMaxscriptHit(StreamWriter writer, Vector3f position, int boxCount)
        {
            string boxName = "IPBox" + boxCount.ToString();

            writer.WriteLine(boxName + " = box length:0.5 width:0.5 height:0.5");
            writer.WriteLine(boxName + ".pos = [{0}, {1}, {2}]", position.X, position.Y, 0.0);
            writer.WriteLine("");
        }

        /// <summary>
        /// After the positions are compressed, re-scan the collision at the compressed position to get the  
        /// new Z position and calculate the Z-Bias.
        /// </summary>
        /// <param name="entityLists"></param>
        /// <param name="partition"></param>
        private void RecalculateZPosition(ref List<InstancedEntityList> entityLists, BoundsPartition partition, float maxSlope)
        {
            foreach (InstancedEntityList list in entityLists)
            {
                Vector3f extents = (list.BoundingBox.Max - list.BoundingBox.Min);

                foreach (InstancedEntity entity in list)
                {
                    Vector3f compressedPosition = new Vector3f(entity.CompressedX, entity.CompressedY, entity.CompressedZ);
                    compressedPosition /= UInt16.MaxValue;

                    Vector3f uncompressedPosition = new Vector3f(compressedPosition.X * extents.X, compressedPosition.Y * extents.Y, compressedPosition.Z * extents.Z);
                    uncompressedPosition += list.BoundingBox.Min;

                    OptimisedPrimitive primitive;
                    partition.TestCollision(uncompressedPosition, out primitive);

                    // Make sure our newly calculated position is still sitting in a good position.
                    if (primitive != null && partition.TestForRejection(maxSlope, primitive))
                    {
                        // Recalculate the new Z position based on the unpacked X/Y
                        float newZ = primitive.Plane.GetZCoordinate(uncompressedPosition.X, uncompressedPosition.Y);

                        // Save the new Z, update the position and re-compress.
                        uncompressedPosition.Z = newZ;
                        entity.Position = uncompressedPosition;
                    }
                }

                // We need to reprocess the list because the positions have changed so the bounding box will need to change.
                list.Process(list.ArchetypeBoundingBox, list.LODDistance, list.LODFadeStartDistance, list.LODInstFadeRange,
                    list.OrientToTerrain, new Vector3f(list.MinScale, list.MaxScale, list.RandomScale), list.Brightness);
            }
        }

        /// <summary>
        /// Breaks up the given list of entities into a list of lists, based on the optimal batch size
        /// for that category.
        /// </summary>
        /// <param name="entityList"></param>
        /// <param name="optimalBatchSize"></param>
        /// <returns></returns>
        private List<InstancedEntityList> OptimiseBatchLists(InstancedEntityList entityList, InstancePlacementSerialiserOptions options, bool reorderLists = false)
        {
            //Spatially partition these instances.  
            KDTree tree = new KDTree(2, entityList, options);
            tree.ProcessEntities();
            List<InstancedEntityList> optimisedLists = tree.GetEntityLeaves();

            // Order the list to get a better "random" look for the instance batches.
            if (reorderLists)
                return OrderEntityLists(optimisedLists);
            else
                return optimisedLists;
        }

        /// <summary>
        /// Takes all the batch lists we have and break them down spatially based on our options.
        /// </summary>
        /// <param name="batchLists"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        private List<List<InstancedEntityList>> GenerateSpatialBatchLists(List<InstancedEntityList> batchLists, InstancePlacementSerialiserOptions options)
        {
            //Spatially partition these batches.
            KDTree tree = new KDTree(2, batchLists, options);
            tree.ProcessBatches();
            List<List<InstancedEntityList>> optimisedLists = tree.GetBatchLeaves();

            return optimisedLists;
        }

        /// <summary>
        /// Takes in an entity to start from and finds the furthest entity in the list away from it.
        /// </summary>
        /// <param name="startEntity"></param>
        /// <param name="foundEntity"></param>
        /// <param name="currentList"></param>
        /// <param name="entityList"></param>
        /// <returns></returns>
        private void FindFurthestEntity(ref InstancedEntity startEntity, ref InstancedEntity foundEntity, ref InstancedEntityList currentList, InstancedEntityList entityList)
        {
            double distance = 0;

            // If there is only 1 entity make sure it becomes the "foundEntity".
            if (entityList.Count == 1)
                foundEntity = startEntity;

            foreach (InstancedEntity entity in entityList)
            {
                // If we're the exact same position, we're probably the same entity so skip it.
                // Also, if we're already in the list then ignore it, we don't want to double up instances.
                if (startEntity.Position == entity.Position || currentList.Contains(entity))
                    continue;

                double distFromStart = startEntity.Position.Dist(entity.Position);

                if (distFromStart > distance)
                {
                    distance = distFromStart;
                    foundEntity = entity;
                }
            }

            // Fallback case to make sure we have a "foundEntity" set.
            if (foundEntity == null)
                foundEntity = startEntity;
        }

        /// <summary>
        /// Goes through an instanced entity list and re-orders the lists based on distances furthest away from each entity.
        /// </summary>
        /// <param name="instancedEntityList"></param>
        /// <returns></returns>
        private List<InstancedEntityList> OrderEntityLists(List<InstancedEntityList> instancedEntityList)
        {
            List<InstancedEntityList> orderedList = new List<InstancedEntityList>();

            foreach (InstancedEntityList list in instancedEntityList)
            {
                InstancedEntity startEntity = null;
                double closestDistance = 100000;
                InstancedEntityList orderedEntityList = new InstancedEntityList();
                orderedEntityList.ListColour = list.ListColour;

                if (TextureMetadata.EnableFarthestEntityShuffling)
                {
                    // Find the closest entity to the bounding boxes min position to start from.
                    foreach (InstancedEntity entity in list)
                    {
                        double distFromMinBB = entity.Position.Dist(list.BoundingBox.Min);
                        if (distFromMinBB < closestDistance)
                        {
                            startEntity = entity;
                            closestDistance = distFromMinBB;
                        }
                    }

                    if (startEntity == null)
                    {
                        Log.ErrorCtx(LOG_CTX, "Could not find an entity close to the bounding box min point.");
                        return orderedList;
                    }

                    // Build up a new list by going through and finding the furthest entity each time.
                    foreach (InstancedEntity entity in list)
                    {
                        InstancedEntity furthestEntity = null;
                        FindFurthestEntity(ref startEntity, ref furthestEntity, ref orderedEntityList, list);
                        orderedEntityList.Add(furthestEntity);
                        startEntity = furthestEntity;
                    }
                }
                else
                {
                    //Split the entities into 4 quadrants.
                    const int NumQuadrants = 4;
                    Dictionary<BoundingBox3f, InstancedEntityList> quadrants = new Dictionary<BoundingBox3f, InstancedEntityList>();

                    BoundingBox3f mainBoundingBox = list.GetBoundingBox(new BoundingBox3f(Vector3f.Zero, Vector3f.Zero));
                    float halfWidth = mainBoundingBox.Min.X + (mainBoundingBox.Max.X - mainBoundingBox.Min.X) / 2;
                    float halfHeight = mainBoundingBox.Min.Y + (mainBoundingBox.Max.Y - mainBoundingBox.Min.Y) / 2;

                    BoundingBox3f upperLeft = new BoundingBox3f();
                    upperLeft.Min.X = mainBoundingBox.Min.X;
                    upperLeft.Min.Y = mainBoundingBox.Min.Y;
                    upperLeft.Max.X = halfWidth;
                    upperLeft.Max.Y = halfHeight;
                    upperLeft.Min.Z = mainBoundingBox.Min.Z;
                    upperLeft.Max.Z = mainBoundingBox.Max.Z;
                    quadrants.Add(upperLeft, new InstancedEntityList());

                    BoundingBox3f upperRight = new BoundingBox3f();
                    upperRight.Min.X = halfWidth;
                    upperRight.Min.Y = mainBoundingBox.Min.Y;
                    upperRight.Max.X = mainBoundingBox.Max.X;
                    upperRight.Max.Y = halfHeight;
                    upperRight.Min.Z = mainBoundingBox.Min.Z;
                    upperRight.Max.Z = mainBoundingBox.Max.Z;
                    quadrants.Add(upperRight, new InstancedEntityList());

                    BoundingBox3f lowerLeft = new BoundingBox3f();
                    lowerLeft.Min.X = mainBoundingBox.Min.X;
                    lowerLeft.Min.Y = halfHeight;
                    lowerLeft.Max.X = halfWidth;
                    lowerLeft.Max.Y = mainBoundingBox.Max.Y;
                    lowerLeft.Min.Z = mainBoundingBox.Min.Z;
                    lowerLeft.Max.Z = mainBoundingBox.Max.Z;
                    quadrants.Add(lowerLeft, new InstancedEntityList());

                    BoundingBox3f lowerRight = new BoundingBox3f();
                    lowerRight.Min.X = halfWidth;
                    lowerRight.Min.Y = halfHeight;
                    lowerRight.Max.X = mainBoundingBox.Max.X;
                    lowerRight.Max.Y = mainBoundingBox.Max.Y;
                    lowerRight.Min.Z = mainBoundingBox.Min.Z;
                    lowerRight.Max.Z = mainBoundingBox.Max.Z;
                    quadrants.Add(lowerRight, new InstancedEntityList());

                    //Slot the entity into their quadrants
                    foreach (InstancedEntity entity in list)
                    {
                        foreach (KeyValuePair<BoundingBox3f, InstancedEntityList> pair in quadrants)
                        {
                            if (pair.Key.Contains(entity.Position))
                            {
                                quadrants[pair.Key].Add(entity);
                            }
                        }
                    }

                    foreach (KeyValuePair<BoundingBox3f, InstancedEntityList> pair in quadrants)
                    {
                        quadrants[pair.Key].Shuffle();
                    }

                    //Randomize the order of the entity list.
                    int randomCount = 0;
                    while (randomCount < list.Count)
                    {
                        foreach (KeyValuePair<BoundingBox3f, InstancedEntityList> pair in quadrants)
                        {
                            if (pair.Value.Count > 0)
                            {
                                orderedEntityList.Add(pair.Value[0]);
                                pair.Value.RemoveAt(0);
                                randomCount++;
                            }
                        }
                    }
                }

                // Make sure the new list has its values (bounding box) calculated properly.
                orderedEntityList.Process(new BoundingBox3f(Vector3f.Zero, Vector3f.Zero), list.LODDistance, list.LODFadeStartDistance, list.LODInstFadeRange, list.OrientToTerrain,
                                            new Vector3f(list.MinScale, list.MaxScale, list.RandomScale), list.Brightness);

                orderedList.Add(orderedEntityList);
            }

            return orderedList;
        }

        /// <summary>
        /// Performs any processing necessary for the entity lists.
        /// This includes properly sizing the bounding boxes.
        /// </summary>
        /// <param name="boundingBoxes"></param>
        /// <param name="entityLists"></param>
        private void ProcessBatchLists(Dictionary<String, TargetObjectDef> archetypes, List<InstancedEntityList> entityLists)
        {
            foreach (InstancedEntityList entityList in entityLists)
            {
                KeyValuePair<String, TargetObjectDef> pair = archetypes.Where(entry => String.Compare(entityList[0].ArchetypeName, entry.Key, true) == 0).FirstOrDefault();

                String entityName = pair.Key;
                InstanceDefinition definition = InstancePlacementProcessor.TextureMetadata.GetDefinition(entityName);
                TargetObjectDef archetype = pair.Value;
                if (archetype == null)
                {
                    Log.ErrorCtx(LOG_CTX, String.Format("Archetype {0} can not be found.", entityList[0].ArchetypeName));
                    continue;
                }

                if (Branch.Project.IsDLC && archetype.IsNewDLCObject())
                {
                    // Here's where the suckage begins.
                    string dlcAsset = MapAsset.AppendMapPrefixIfRequired(Branch.Project, archetype);
                    entityList.UpdateArchetypeNames(dlcAsset);
                }

                float lodDistance = archetype.GetLODDistance();
                if (definition.LODDistance > 0)
                {
                    lodDistance = definition.LODDistance;
                }

                BoundingBox3f localBoundingBox = archetype.GetLocalBoundingBox();
                float orientToTerrain = Math.Min(1.0f, definition.OrientToTerrain);

                entityList.Process(localBoundingBox, lodDistance, definition.LODFadeStartDistance, definition.LODInstFadeRange, orientToTerrain, new Vector3f(definition.MinScale, definition.MaxScale, definition.RandomScale), definition.Brightness);
            }
        }

        /// <summary>
        /// Optimised the given grass instances into lists; add them to the IMAP container.
        /// </summary>
        /// <param name="instanceObjects"></param>
        /// <param name="instanceContainer"></param>
        private void ProcessGrassInstances(InstanceObject[] instanceObjects, BoundsPartition partition)
        {
            // Allow the imap generation to still go through even with no instances.
            // This was causing crashes because the RPF was empty when an area doesn't have IP data.
            if (instanceObjects.Count() == 0)
            {
                Log.WarningCtx(LOG_CTX, String.Format("No instance objects created, will create an emtpy .imap file."));
            }

            List<String> itypDependencies = new List<String>();
            Dictionary<InstanceDefinition, InstancedEntityList> entitiesDictionary = new Dictionary<InstanceDefinition, InstancedEntityList>();
            foreach (InstanceObject instance in instanceObjects)
            {
                if (!entitiesDictionary.ContainsKey(instance.Definition))
                {
                    InstancedEntityList newGrassEntityList = new InstancedEntityList();
                    newGrassEntityList.ListColour = instance.Definition.Color;
                    entitiesDictionary.Add(instance.Definition, newGrassEntityList);
                }

                InstancedGrassEntity treeEntity = new InstancedGrassEntity(instance.Definition.Name, instance.Definition.HighDetailName, instance.Position, instance.Rotation, instance.Scale, instance.Scale, instance.Plane, instance.Color, instance.Tint, instance.AO);
                entitiesDictionary[instance.Definition].Add(treeEntity);
            }

            Dictionary<String, TargetObjectDef> archetypes = new Dictionary<String, TargetObjectDef>();

            IEnumerable<String> additionalDependencies = TextureMetadata.SelectedCategory.Dependencies;
            additionalDependencies = additionalDependencies.Select(path => path = Branch.Environment.Subst(path));

            List<Scene> scenes = new List<Scene>();
            foreach (String dependency in additionalDependencies)
            {
                Scene objectScene = SceneCollection.LoadScene(dependency);
                scenes.Add(objectScene);
            }

            List<String> archetypeNames = new List<String>();

            foreach (InstanceDefinition definition in entitiesDictionary.Keys)
            {
                TargetObjectDef foundObjectDef = null;

                foreach (Scene scene in scenes)
                {
                    TargetObjectDef objectDef = scene.FindObject(definition.Name);
                    if (objectDef != null)
                    {
                        foundObjectDef = objectDef;
                        if (!itypDependencies.Contains(Path.GetFileNameWithoutExtension(scene.Filename)))
                        {
                            if (Branch.Project.IsDLC && scene.Filename.StartsWith(Branch.Project.Root))
                            {
                                itypDependencies.Add(MapAsset.AppendMapPrefixIfRequired(Branch.Project, Path.GetFileNameWithoutExtension(scene.Filename)));
                            }
                            else
                            {
                                itypDependencies.Add(Path.GetFileNameWithoutExtension(scene.Filename));
                            }
                        }
                    }

                    if (foundObjectDef != null)
                        break;
                }

                if (foundObjectDef == null)
                {
                    Log.WarningCtx(LOG_CTX, String.Format("Archetype \"{0}\" could not be found.", definition.Name));
                }
                else
                {
                    archetypeNames.Add(definition.Name);
                    archetypes.Add(definition.Name, foundObjectDef);
                }
            }

            using (ProfileContext ctx = new ProfileContext(Log, LOG_CTX, "Processing/optimising grass lists..."))
            {
                List<InstancedEntityList> optimisedLists = new List<InstancedEntityList>();
                InstanceCategory category = TextureMetadata.SelectedCategory;
                BatchSplitter splitter = new BatchSplitter(this.Options, InstancePlacementProcessor.Log);

                foreach (KeyValuePair<InstanceDefinition, InstancedEntityList> grassEntry in entitiesDictionary)
                {
                    InstancedEntityList grassEntities = grassEntry.Value;

                    Vector3f ZeroVector = new Vector3f(0.0f, 0.0f, 0.0f);
                    BoundingBox3f ZeroBoundingBox = new BoundingBox3f(ZeroVector, ZeroVector);

                    grassEntities.Process(ZeroBoundingBox, -1.0f, 0.0f, 0.0f, 0.0f, ZeroVector, 0);

                    // Separate this list into the lists containing the optimal batch count.
                    List<InstancedEntityList> optimisedGrassEntityLists = new List<InstancedEntityList>();
                    splitter.Split(grassEntities, ref optimisedGrassEntityLists);

                    // Order the entity list.
                    optimisedGrassEntityLists = OrderEntityLists(optimisedGrassEntityLists);

                    ProcessBatchLists(archetypes, optimisedGrassEntityLists);
                    RecalculateZPosition(ref optimisedGrassEntityLists, partition, grassEntry.Key.MaxSlope);

                    foreach (InstancedEntityList list in optimisedGrassEntityLists)
                        optimisedLists.Add(list);
                }

                int batchCount = 0;
                List<List<InstancedEntityList>> spatialBatchLists = GenerateSpatialBatchLists(optimisedLists, this.Options);

                List<Scene> dummySceneList = new List<Scene>();
                InteriorContainerNameMap interiorNameMap = new InteriorContainerNameMap();

                foreach (List<InstancedEntityList> batch in spatialBatchLists)
                {
                    SceneSerialiserIMAP imapSerialiser = new SceneSerialiserIMAP(Branch, dummySceneList.ToArray(), null, null, "", "", interiorNameMap, null);
                    String containerName = String.Format("{0}", InstancePlacementTask.Name);
                    IMAPContainer instanceContainer = new IMAPContainer(containerName);

                    foreach (String additionalDependency in additionalDependencies)
                        instanceContainer.AddPhysicsDictionary(Path.GetFileNameWithoutExtension(additionalDependency));

                    String outputImapFile = Path.Combine(this.InstancePlacementTask.Outputs.First().OutputDirectory, String.Format("{0}_{1}_{2}.imap", this.InstancePlacementTask.Name, this.InstancePlacementTask.PlacementType.ToString().ToLower(), batchCount));
                    Log.MessageCtx(LOG_CTX, String.Format("Writing {0}...", outputImapFile));

                    foreach (String platform in platformLists)
                    {
                        foreach (InstancedEntityList optimisedGrassEntityList in batch)
                            instanceContainer.AddGrassEntities(platform, optimisedGrassEntityList);
                    }

                    ProcessIMAPContainer(instanceContainer);
                    imapSerialiser.WriteInstanceMapDataContainer(instanceContainer, outputImapFile);

                    if (this.SerialiseManifestData)
                    {
                        ManifestDependency manifestDependency = new ManifestDependency(outputImapFile, itypDependencies.ToArray(), false);
                        ManifestDependencies.Add(manifestDependency);
                    }

                    if (this.Options.GenerateDebugStats)
                    {
                        // Grab the region. Using the directory where the SceneXML exists.
                        InstancePlacementSerialiserInput input = this.InstancePlacementTask.Inputs.FirstOrDefault();
                        string sceneXMLPath = Path.GetDirectoryName(input.SceneFilename);
                        string[] folderArray = sceneXMLPath.Split(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
                        string regionDirectory = folderArray[folderArray.Length - 1];

                        InstanceStats instanceStats = new InstanceStats(this.ContainerExtents, this.Options, this.InstancePlacementTask.Name, regionDirectory);
                        instanceStats.AddBatchList(batch);

                        string statsOutputDirectory = InstancePlacementProcessor.Branch.Environment.Subst("$(assets)/reports/InstancePlacement");
                        statsOutputDirectory = Path.Combine(statsOutputDirectory, regionDirectory);

                        if (!Directory.Exists(statsOutputDirectory))
                            Directory.CreateDirectory(statsOutputDirectory);

                        string statsFilename = Path.Combine(statsOutputDirectory, String.Format("{0}_{1}_stats.xml", this.InstancePlacementTask.Name, batchCount));
                        string imageStatsFilename = Path.Combine(statsOutputDirectory, String.Format("{0}_{1}_stats.bmp", this.InstancePlacementTask.Name, batchCount));

                        instanceStats.SaveStats(statsFilename, imageStatsFilename);
                    }

                    batchCount++;
                }
            }
        }

        /// <summary>
        /// Process all prop instances.  This will generate a high detail entity IMAP container as well as break up the
        /// instances objects into optimised list.
        /// </summary>
        /// <param name="instanceObjects"></param>
        private bool ProcessPropInstances(InstanceObject[] instanceObjects)
        {
            Dictionary<String, InstancedEntityList> entitiesDictionary = new Dictionary<String, InstancedEntityList>();
            foreach (InstanceObject instance in instanceObjects)
            {
                string definitionName = instance.Definition.Name.ToLower();
                string definitionHighDetailName = instance.Definition.HighDetailName.ToLower();
                String key = definitionName + ":" + definitionHighDetailName;

                if (!entitiesDictionary.ContainsKey(key))
                {
                    InstancedEntityList newPropEntityList = new InstancedEntityList();
                    entitiesDictionary.Add(key, newPropEntityList);
                }

                InstancedPropEntity treeEntity = new InstancedPropEntity(definitionName, definitionHighDetailName, instance.Plane, instance.Position, instance.Rotation, instance.Scale, instance.Scale);
                entitiesDictionary[key].Add(treeEntity);
            }

            //Add any instanced entities coming from the scene's SceneXml file (Max file).
            List<String> additionalMaxDependencies = new List<String>();
            InstancedEntityList maxInstancedEntities = GetInstancedEntitiesFromInputs(this.InstancePlacementTask.Inputs, this.InstancePlacementTask.Outputs.First(), out additionalMaxDependencies);
            foreach (InstancedPropEntity instance in maxInstancedEntities)
            {
                bool foundKey = false;
                string keyName = "";

                string instanceArchetypeName = instance.ArchetypeName.ToLower();

                foreach (String archetypeInfo in entitiesDictionary.Keys)
                {
                    string archetypeName = archetypeInfo.Split(':')[0];

                    if (instanceArchetypeName == archetypeName)
                    {
                        foundKey = true;
                        keyName = archetypeInfo;
                        break;
                    }
                }

                if (foundKey)
                {
                    entitiesDictionary[keyName].Add(instance);
                }
                else
                {
                    if (!entitiesDictionary.ContainsKey(instanceArchetypeName))
                    {
                        InstancedEntityList newPropEntityList = new InstancedEntityList();
                        entitiesDictionary.Add(instanceArchetypeName, newPropEntityList);
                    }

                    entitiesDictionary[instanceArchetypeName].Add(instance);
                }
            }

            Log.MessageCtx(LOG_CTX, String.Format("{0} objects from the scene file have been placed.", maxInstancedEntities.Count()));

            if (Log.HasErrors == true) return false;
    
            //For every entity we've found, let's grab its archetype.
            Dictionary<String, TargetObjectDef> archetypes = new Dictionary<String, TargetObjectDef>();
            Dictionary<String, TargetObjectDef> highDetailArchetypes = new Dictionary<String, TargetObjectDef>();

            IEnumerable<String> additionalDependencies = InstancePlacementProcessor.TextureMetadata.SelectedCategory.Dependencies;
            additionalDependencies = additionalDependencies.Concat(additionalMaxDependencies);
            additionalDependencies = additionalDependencies.Select(path => path = Branch.Environment.Subst(path));

            List<Scene> scenes = new List<Scene>();
            foreach (String dependency in additionalDependencies)
            {
                Scene objectScene = SceneCollection.LoadScene(dependency);
                if (scenes.Contains(objectScene) == false)
                    scenes.Add(objectScene);
            }

            List<String> itypDependencies = new List<String>();

            foreach (String archetypeInfo in entitiesDictionary.Keys)
            {
                string[] splitInfo = archetypeInfo.Split(':');

                string archetypeName = "";
                string highDetailArchetypeName = "";

                if (splitInfo.Length > 1)
                {
                    archetypeName = splitInfo[0];
                    highDetailArchetypeName = splitInfo[1];
                }
                else
                {
                    archetypeName = archetypeInfo;
                    highDetailArchetypeName = "";
                }

                TargetObjectDef foundObjectDef = null;
                TargetObjectDef foundHighDetailObjectDef = null;
                InstanceDefinition definition = String.IsNullOrEmpty(highDetailArchetypeName) ? InstancePlacementProcessor.TextureMetadata.GetDefinition(archetypeName) : InstancePlacementProcessor.TextureMetadata.GetDefinition(archetypeName, highDetailArchetypeName);

                if (definition == null)
                {
                    //If a definition does not exist, create a default definition with the
                    //high detail entity being the same as the initial archetype.
                    definition = new InstanceDefinition(archetypeName, archetypeName);
                    InstancePlacementProcessor.TextureMetadata.SelectedCategory.Definitions.Add(definition);
                }

                foreach (Scene scene in scenes)
                {
                    TargetObjectDef objectDef = scene.FindObject(archetypeName);
                    if (objectDef != null)
                    {
                        foundObjectDef = objectDef;
                    }

                    TargetObjectDef highDetailObjectDef = scene.FindObject(definition.HighDetailName);
                    if (highDetailObjectDef != null)
                    {
                        foundHighDetailObjectDef = highDetailObjectDef;
                    }

                    if (foundObjectDef != null && foundHighDetailObjectDef != null)
                    {
                        if (!itypDependencies.Contains(Path.GetFileNameWithoutExtension(scene.Filename)))
                            itypDependencies.Add(Path.GetFileNameWithoutExtension(scene.Filename));
                        break;
                    }
                }

                string definitionName = definition.Name.ToLower();

                if (foundObjectDef == null)
                {
                    Log.WarningCtx(LOG_CTX, String.Format("Archetype \"{0}\" could not be found.", definition.Name));
                }
                else
                {
                    if (!archetypes.ContainsKey(definitionName))
                        archetypes.Add(definitionName, foundObjectDef);
                }

                if (foundHighDetailObjectDef == null)
                {
                    Log.WarningCtx(LOG_CTX, String.Format("High detail archetype \"{0}\" could not be found.", definition.HighDetailName));
                }
                else
                {
                    string definitionHighDetailName = definition.HighDetailName.ToLower();

                    if (!highDetailArchetypes.ContainsKey(definitionHighDetailName))
                        highDetailArchetypes.Add(definitionName + ":" + definitionHighDetailName, foundHighDetailObjectDef);
                }
            }

            using (ProfileContext ctx = new ProfileContext(Log, LOG_CTX, "Processing/optimising tree lists..."))
            {
                InstanceCategory category = TextureMetadata.SelectedCategory;

                ICollection<List<InstancedEntityList>> optimisedTreeEntityListsCollection = new List<List<InstancedEntityList>>();
                ParallelOptions options = new ParallelOptions();
                options.MaxDegreeOfParallelism = this.Options.DegreeOfParallelism; //Set to -1 for maximum threads; set to 1 to debug a single thread at a time.
                Parallel.ForEach<InstancedEntityList>(entitiesDictionary.Values, options, task =>
                {
                    InstancedEntityList treeEntities = task;

                    //Separate this list into the lists containing the optimal batch count.
                    List<InstancedEntityList> optimisedTreeEntityLists = OptimiseBatchLists(treeEntities, this.Options);

                    optimisedTreeEntityListsCollection.Add(optimisedTreeEntityLists);
                });

                if (Log.HasErrors) return false;
                if (optimisedTreeEntityListsCollection.Count == 0) return true; //Nothing to write out.

                //Compute all bounding box information for the lists.
                BoundingBox3f containerBoundingBox = new BoundingBox3f();
                foreach (List<InstancedEntityList> optimisedTreeEntityLists in optimisedTreeEntityListsCollection)
                {
                    ProcessBatchLists(archetypes, optimisedTreeEntityLists);

                    foreach (InstancedEntityList optimisedTreeEntityList in optimisedTreeEntityLists)
                    {
                        containerBoundingBox.Expand(optimisedTreeEntityList.PositionBoundingBox);
                    }
                }

                if (TextureMetadata.EnableEntityPlacement)
                {
                    IMAPContainer imapContainer = new IMAPContainer();
                    Entity.Branch = Branch;
                    
                    float fullRotRadians = 360 * ((float)System.Math.PI / 180.0f);
                    Random random = new Random(InstancePlacementProcessor.TextureMetadata.SelectedCategory.RandomSeed);

                    foreach (List<InstancedEntityList> optimisedTreeEntityLists in optimisedTreeEntityListsCollection)
                    {
                        foreach (InstancedEntityList optimisedTreeEntityList in optimisedTreeEntityLists)
                        {
                            InstancedEntity entity = optimisedTreeEntityList.FirstOrDefault();
                            InstanceDefinition definition = InstancePlacementProcessor.TextureMetadata.GetDefinition(entity.ArchetypeName);

                            foreach (InstancedEntity treeEntity in optimisedTreeEntityList)
                            {
                                TargetObjectDef objectDef = archetypes[treeEntity.ArchetypeName];
                                Entity newEntity = new Entity(objectDef);

                                newEntity.LODDistance = optimisedTreeEntityList.LODDistance;
                                newEntity.IsInstancedObject = true;

                                float zRotation = (float)random.NextDouble() * fullRotRadians;

                                Matrix34f rotationMatrix = new Matrix34f();
                                if (optimisedTreeEntityList.OrientToTerrain != 0.0f)
                                {
                                    Vector3f normal = treeEntity.GroundPlane.GetNormal();
                                    normal.Normalise();
                                    rotationMatrix.C = normal;
                                    rotationMatrix.B = Vector3f.Cross(normal, new Vector3f(1.0f, 0.0f, 0.0f));
                                    rotationMatrix.B.Normalise();
                                    rotationMatrix.A = Vector3f.Cross(rotationMatrix.B, rotationMatrix.C);

                                    Quaternionf from = new Quaternionf();
                                    Quaternionf to = Quaternionf.FromMatrix34(rotationMatrix);

                                    from.PrepareSlerp(to);
                                    from.Slerp(definition.OrientToTerrain, from, to);

                                    rotationMatrix.FromQuaternionf(from);
                                    rotationMatrix.RotateUnitAxis(treeEntity.GroundPlane.GetNormal(), zRotation);
                                }

                                Quaternionf rotQuat = Quaternionf.FromMatrix34(rotationMatrix);
                                rotQuat.Normalise();

                                newEntity.WorldTransformOverride = new Matrix34f(rotQuat);
                                newEntity.WorldTransformOverride.Translation = treeEntity.Position;
                                newEntity.Object.WorldBoundingBox = new BoundingBox3f(objectDef.LocalBoundingBox);
                                newEntity.Object.WorldBoundingBox.Transform(newEntity.WorldTransformOverride);
                                newEntity.ScaleOverride = new Vector3f(treeEntity.ScaleXY, treeEntity.ScaleXY, treeEntity.ScaleZ);

                                imapContainer.AddEntity(newEntity);
                            }
                        }
                    }

                    List<Scene> dummySceneList = new List<Scene>();
                    InteriorContainerNameMap interiorNameMap = new InteriorContainerNameMap();
                    SceneSerialiserIMAP imapSerialiser = new SceneSerialiserIMAP(Branch, dummySceneList.ToArray(), null, null, "", "", interiorNameMap, null);

                    InstancePlacementSerialiserOutput output = InstancePlacementTask.Outputs.First();
                    String outputImapFile = Path.Combine(output.OutputDirectory, String.Format("{0}_{1}.imap", InstancePlacementTask.Name, InstancePlacementTask.PlacementType.ToString().ToLower(), ".imap"));

                    Log.MessageCtx(LOG_CTX, String.Format("Writing {0}...", outputImapFile));
                    imapSerialiser.Write(outputImapFile, imapContainer);

                    if (this.SerialiseManifestData)
                    {
                        ManifestDependency manifestDependency = new ManifestDependency(outputImapFile, itypDependencies.ToArray(), false);
                        ManifestDependencies.Add(manifestDependency);
                    }
                }
                else
                {
                    //Serialise the imap files for props.
                    InstancePlacementSerialiserOutput output = InstancePlacementTask.Outputs.First();
                    float containerWidth = containerBoundingBox.Max.X - containerBoundingBox.Min.X;
                    float containerHeight = containerBoundingBox.Max.Y - containerBoundingBox.Min.Y;

                    int boundLength = TextureMetadata.SelectedCategory.MaximumBoundLength > 0 ? TextureMetadata.SelectedCategory.MaximumBoundLength : int.MaxValue;
                    int rows = (int)(containerWidth / (float)boundLength) + 1;
                    int columns = (int)(containerHeight / (float)boundLength) + 1;

                    IMAPContainer[][] imapContainers = new IMAPContainer[rows][];
                    for (int rowIndex = 0; rowIndex < rows; rowIndex++)
                    {
                        imapContainers[rowIndex] = new IMAPContainer[columns];
                    }

                    foreach (List<InstancedEntityList> optimisedTreeEntityLists in optimisedTreeEntityListsCollection)
                    {
                        foreach (InstancedEntityList optimisedTreeEntityList in optimisedTreeEntityLists)
                        {
                            //Use the center of the bounding box to determine which IMAP container to be added to.
                            Vector3f listCenter = optimisedTreeEntityList.PositionBoundingBox.Centre();

                            int row = (int)(listCenter.X - containerBoundingBox.Min.X) / boundLength;
                            int col = (int)(listCenter.Y - containerBoundingBox.Min.Y) / boundLength;

                            if (imapContainers[row][col] == null)
                            {
                                String containerName = String.Format("{0}_{1}_{2}", InstancePlacementTask.Name, row, col);
                                imapContainers[row][col] = new IMAPContainer(containerName);

                                foreach (String additionalDependency in additionalDependencies)
                                    imapContainers[row][col].AddPhysicsDictionary(Path.GetFileNameWithoutExtension(additionalDependency));
                            }

                            foreach (String platform in platformLists)
                            {
                                imapContainers[row][col].AddTreeEntities(platform, optimisedTreeEntityList as InstancedEntityList);
                            }
                        }
                    }

                    List<Scene> dummySceneList = new List<Scene>();
                    InteriorContainerNameMap interiorNameMap = new InteriorContainerNameMap();
                    SceneSerialiserIMAP imapSerialiser = new SceneSerialiserIMAP(Branch, dummySceneList.ToArray(), null, null, "", "", interiorNameMap, null);

                    int imapCount = 0;
                    for (int rowIndex = 0; rowIndex < rows; ++rowIndex)
                    {
                        for (int colIndex = 0; colIndex < columns; ++colIndex)
                        {
                            if (imapContainers[rowIndex][colIndex] == null) continue;

                            ProcessIMAPContainer(imapContainers[rowIndex][colIndex]);

                            String outputImapFile = Path.Combine(output.OutputDirectory, String.Format("{0}_{1}_{2}.imap", InstancePlacementTask.Name, InstancePlacementTask.PlacementType.ToString().ToLower(), imapCount, ".imap"));

                            if (TextureMetadata.EnableHighDefEntityGeneration == true)
                            {
                                String highDetailOutputImapFile = Path.Combine(output.OutputDirectory, String.Format("{0}_{1}_{2}_hi.imap", InstancePlacementTask.Name, InstancePlacementTask.PlacementType.ToString().ToLower(), imapCount, ".imap"));
                                String highDetailEntityIMAP = Path.GetFileNameWithoutExtension(highDetailOutputImapFile);
                                imapContainers[rowIndex][colIndex].SetInstancedDataIMAPLink(highDetailEntityIMAP);

                                Log.MessageCtx(LOG_CTX, String.Format("Writing {0}...", highDetailOutputImapFile));
                                WriteHighDetailEntityIMAP(highDetailArchetypes, imapContainers[rowIndex][colIndex], highDetailEntityIMAP, highDetailOutputImapFile);
                            }

                            Log.MessageCtx(LOG_CTX, String.Format("Writing {0}...", outputImapFile));
                            imapSerialiser.WriteInstanceMapDataContainer(imapContainers[rowIndex][colIndex], outputImapFile);

                            if (this.SerialiseManifestData)
                            {
                                ManifestDependency manifestDependency = new ManifestDependency(outputImapFile, itypDependencies.ToArray(), false);
                                ManifestDependencies.Add(manifestDependency);
                            }

                            imapCount++;
                        }
                    }
                }

            }

            return true;
        }

        /// <summary>
        /// Iterate through all instance entities within the IMAP container to assign their index to the high-definition entity.
        /// </summary>
        /// <param name="container"></param>
        private void ProcessIMAPContainer(IMAPContainer container)
        {
            BoundingBox3f entityBoundingBox = new BoundingBox3f();

            String platform = container.InstanceEntityData.TreeEntitiesList.Keys.FirstOrDefault();

            int unique_index = 0;
            float maxLODDistance = 0.0f;

            if (!String.IsNullOrEmpty(platform) && container.InstanceEntityData.TreeEntitiesList.ContainsKey(platform))
            {
                foreach (InstancedEntityList entityList in container.InstanceEntityData.TreeEntitiesList[platform])
                {
                    if (TextureMetadata.EnableHighDefEntityGeneration)
                    {
                        foreach (InstancedPropEntity propEntity in entityList)
                        {
                            propEntity.Index = unique_index;

                            unique_index++;
                        }
                    }

                    // Even though we do the list for each platform, the lists have the same positions so it shouldn't affect the bounding box size.
                    entityBoundingBox.Expand(entityList.BoundingBox);

                    if (entityList.LODDistance > maxLODDistance)
                    {
                        maxLODDistance = entityList.LODDistance;
                    }
                }
            }

            platform = container.InstanceEntityData.GrassEntitiesList.Keys.FirstOrDefault();

            if (!String.IsNullOrEmpty(platform) && container.InstanceEntityData.GrassEntitiesList.ContainsKey(platform))
            {
                foreach (InstancedEntityList entityList in container.InstanceEntityData.GrassEntitiesList[platform])
                {
                    // Even though we do the list for each platform, the lists have the same positions so it shouldn't affect the bounding box size.
                    entityBoundingBox.Expand(entityList.BoundingBox);

                    if (entityList.LODDistance > maxLODDistance)
                    {
                        maxLODDistance = entityList.LODDistance;
                    }
                }
            }

            // When we end up having to create an empty IMAP file, make sure we set the bounding box to zero but only if
            // we didn't touch the bounding box (meaning no entity lists were created).
            if (entityBoundingBox.IsEmpty)
            {
                entityBoundingBox.Min = new Vector3f(0.0f, 0.0f, 0.0f);
                entityBoundingBox.Max = new Vector3f(0.0f, 0.0f, 0.0f);
            }

            BoundingBox3f streamingBoundingBox = new BoundingBox3f(entityBoundingBox);
            streamingBoundingBox.Min.X -= maxLODDistance + TextureMetadata.SelectedCategory.StreamingBoundSizeBuffer;
            streamingBoundingBox.Max.X += maxLODDistance + TextureMetadata.SelectedCategory.StreamingBoundSizeBuffer;
            streamingBoundingBox.Min.Y -= maxLODDistance + TextureMetadata.SelectedCategory.StreamingBoundSizeBuffer;
            streamingBoundingBox.Max.Y += maxLODDistance + TextureMetadata.SelectedCategory.StreamingBoundSizeBuffer;

            container.EntityBoundingBox = entityBoundingBox;
            container.StreamingBoundingBox = streamingBoundingBox;
        }

        /// <summary>
        /// Writes an IMAP file containing a list of high-definition entity objects.
        /// </summary>
        /// <param name="archetypes"></param>
        /// <param name="instancedEntityContainer"></param>
        private void WriteHighDetailEntityIMAP(Dictionary<String, TargetObjectDef> archetypes, IMAPContainer instancedEntityContainer, string containerName, String outputFile)
        {
            BoundingBox3f initBoundingBox = new BoundingBox3f();
            IMAPContainer highEntityContainer = new IMAPContainer(containerName, initBoundingBox, initBoundingBox);
            highEntityContainer.AddPhysicsDictionaryRange(instancedEntityContainer.PhysicsDictionaries);
            highEntityContainer.DisableEntityDuplicationCheck = true;

            // We shouldn't be iterating through each platform on the high detail props so just grab the first one in the list.
            String basePlatform = instancedEntityContainer.InstanceEntityData.TreeEntitiesList.Keys.First();

            foreach (InstancedEntityList propEntityList in instancedEntityContainer.InstanceEntityData.TreeEntitiesList[basePlatform])
            {
                foreach (InstancedPropEntity propEntity in propEntityList)
                {
                    string key = propEntity.ArchetypeName.ToLower() + ":" + propEntity.HighDetailArchetypeName.ToLower();
                    string secondaryKey = propEntity.ArchetypeName.ToLower() + ":" + propEntity.ArchetypeName.ToLower();

                    Entity entity = null;
                    TargetObjectDef objectDef = null;

                    if (archetypes.ContainsKey(key))
                    {
                        objectDef = archetypes[key];
                        entity = new Entity(objectDef);
                    }
                    else if (archetypes.ContainsKey(secondaryKey))
                    {
                        objectDef = archetypes[secondaryKey];
                        entity = new Entity(objectDef);
                    }
                    else
                    {
                        continue;
                    }

                    InstanceDefinition definition = null;
                    if (!String.IsNullOrEmpty(propEntity.HighDetailArchetypeName))
                        definition = InstancePlacementProcessor.TextureMetadata.GetDefinition(propEntity.ArchetypeName, propEntity.HighDetailArchetypeName);
                    else
                        definition = InstancePlacementProcessor.TextureMetadata.GetDefinition(propEntity.ArchetypeName);

                    if (definition != null && definition.HighDetailLODDistance > 0.0f)
                        entity.LODDistance = definition.HighDetailLODDistance;

                    entity.WorldTransformOverride = new Matrix34f(propEntity.Rotation);
                    entity.WorldTransformOverride.Translation = propEntity.Position;
                    entity.Object.WorldBoundingBox = new BoundingBox3f(objectDef.LocalBoundingBox);
                    entity.Object.WorldBoundingBox.Transform(entity.WorldTransformOverride);
                    entity.ScaleOverride = new Vector3f(propEntity.ScaleXY, propEntity.ScaleXY, propEntity.ScaleZ);

                    highEntityContainer.AddEntity(entity);
                }
            }

            highEntityContainer.DisableEntityDuplicationCheck = false;

            Log.MessageCtx(LOG_CTX, String.Format("Writing {0}...", outputFile));
            List<Scene> dummyHighEntitySceneList = new List<Scene>();
            InteriorContainerNameMap highEntityInteriorNameMap = new InteriorContainerNameMap();
            SceneSerialiserIMAP highEntityImapSerialiser = new SceneSerialiserIMAP(Branch, dummyHighEntitySceneList.ToArray(), null, null, "", "", highEntityInteriorNameMap, null);
            highEntityImapSerialiser.WriteInstanceMapDataContainer(highEntityContainer, outputFile);
        }

        /// <summary>
        /// Gets all instanced entities from the inputs.
        /// </summary>
        /// <param name="inputs"></param>
        /// <returns></returns>
        private InstancedEntityList GetInstancedEntitiesFromInputs(IEnumerable<InstancePlacementSerialiserInput> inputs, InstancePlacementSerialiserOutput output, out List<String> additionalDependencies)
        {
            additionalDependencies = new List<String>();
            InstancedEntityList maxInstancedEntities = new InstancedEntityList();

            if (InstancePlacementProcessor.TextureMetadata.SelectedCategory.Shaders.Count == 0)  //No instances from Max will be read in.
                return maxInstancedEntities;

            foreach (InstancePlacementSerialiserInput input in inputs)
            {
                if (String.IsNullOrWhiteSpace(input.SceneFilename) == true)
                    continue;  //No SceneXML in this scene.

                //Load the Max scene; find any instance objects in there.  Add them to the list.
                Scene inputScene = SceneCollection.LoadScene(input.SceneFilename);

                //Walk through the scene looking for all entities with a material used.
                Dictionary<Scene, List<TargetObjectDef>> exportedFiles = new Dictionary<Scene, List<TargetObjectDef>>();
                foreach (TargetObjectDef objectDef in inputScene.Walk(Scene.WalkMode.DepthFirst))
                {
                    string maxFile = objectDef.RefFile;
                    if (maxFile != null)
                    {
                        string sceneName = Path.GetFileNameWithoutExtension(maxFile);

                        IEnumerable<String> validDependencies = InstancePlacementProcessor.TextureMetadata.SelectedCategory.Dependencies.Where(dependency => String.Compare(Path.GetFileNameWithoutExtension(dependency), sceneName, true) == 0);
                        if (!validDependencies.Any())
                            continue;

                        Scene scene = SceneCollection.GetScene(sceneName);
                        if (scene == null)
                        {
                            Log.WarningCtx(LOG_CTX, String.Format("Unable to find external reference \"{0}\" in file \"{1}\".", objectDef.Name, objectDef.RefFile));
                            continue;
                        }

                        String exportFile = Path.ChangeExtension(scene.Filename, "zip");

                        if (!exportedFiles.ContainsKey(scene))
                            exportedFiles.Add(scene, new List<TargetObjectDef>());

                        exportedFiles[scene].Add(objectDef);
                    }
                }

                List<System.Guid> objectsToExclude = new List<System.Guid>();
                String drawableZipExtension = "." + FileType.Drawable.GetExportExtension() + "." + FileType.ZipArchive.GetExportExtension();
                String fragmentZipExtension = "." + FileType.Fragment.GetExportExtension() + "." + FileType.ZipArchive.GetExportExtension();
                const String entityTypeFileName = "entity.type";

                foreach (KeyValuePair<Scene, List<TargetObjectDef>> exportFile in exportedFiles)
                {
                    String exportZipFile = Path.ChangeExtension(exportFile.Key.Filename, "zip");
                    String outputCacheDirectory = Path.Combine(CacheDirectory, this.InstancePlacementTask.Name, Path.GetFileNameWithoutExtension(exportFile.Key.Filename));
                    if (!Directory.Exists(outputCacheDirectory))
                        Directory.CreateDirectory(outputCacheDirectory);

                    Dictionary<String, RSG.Rage.TypeFile.FragmentType> parsedTypeFiles = new Dictionary<String, RSG.Rage.TypeFile.FragmentType>();
                    foreach (TargetObjectDef objectDef in exportFile.Value)
                    {
                        TargetObjectDef sourceObjectDef = exportFile.Key.FindObject(objectDef.RefName);  //The source file's object definition.

                        if (sourceObjectDef == null || sourceObjectDef.IsFragment() || sourceObjectDef.IsMiloTri()) continue;

                        RSG.Rage.TypeFile.FragmentType typeFile = null;
                        if (!parsedTypeFiles.ContainsKey(objectDef.RefName))
                        {
                            String objectZipPath = objectDef.RefName;
                            if (sourceObjectDef.IsFragment())
                            {
                                objectZipPath += fragmentZipExtension;
                            }
                            else
                            {
                                objectZipPath += drawableZipExtension;
                            }

                            if (!File.Exists(exportZipFile))
                            {
                                Log.WarningCtx(LOG_CTX, String.Format("Unable to find zip archive {0}.", exportZipFile));
                                continue;  //Silently fail; we'll continue on
                            }

                            MemoryStream stream = Zip.ExtractFile(exportZipFile, objectZipPath);

                            if (stream == null)
                            {
                                Log.WarningCtx(LOG_CTX, String.Format("Unable to find file {0} in zip archive.", objectZipPath));
                                continue;
                            }

                            stream = Zip.ExtractFile(stream, entityTypeFileName);
                            if (stream == null)
                            {
                                Log.WarningCtx(LOG_CTX, String.Format("Unable to find file {0} in zip archive.", objectDef.RefName + drawableZipExtension));
                                continue;
                            }

                            String typefileName = Path.GetFileNameWithoutExtension(objectZipPath) + ".type";
                            String entityTypeFile = Path.Combine(outputCacheDirectory, typefileName);
                            FileStream fileStream = new FileStream(entityTypeFile, FileMode.Create);
                            stream.WriteTo(fileStream);
                            fileStream.Close();

                            typeFile = new RSG.Rage.TypeFile.FragmentType(Log);

                            if (!typeFile.LoadFromFile(entityTypeFile, ""))
                            {
                                Log.Error(String.Format("Unable to load {0} to gather material information.", entityTypeFile));
                                continue;
                            }

                            parsedTypeFiles.Add(objectDef.RefName, typeFile);

                            File.Delete(entityTypeFile);
                        }
                        else
                        {
                            typeFile = parsedTypeFiles[objectDef.RefName];
                        }

                        for (int shaderIndex = 0; shaderIndex < typeFile.ShadingGroup.Shaders.Count(); ++shaderIndex)
                        {
                            RSG.Rage.Entity.Shader shader = typeFile.ShadingGroup.Shaders.ElementAt(shaderIndex);

                            IEnumerable<String> acceptedShaders = InstancePlacementProcessor.TextureMetadata.SelectedCategory.Shaders.Where(shaderName => String.Compare(shader.PresetName, shaderName, true) == 0);
                            if (!acceptedShaders.Any()) continue;

                            Matrix34f mtxParent = Matrix34f.Identity;
                            if (null != objectDef.Parent)
                                mtxParent = objectDef.Parent.NodeTransform.Inverse();

                            Vector3f pos = objectDef.NodeTransform.Translation;
                            Quaternionf rot = new Quaternionf(mtxParent * objectDef.NodeTransform);
                            Vector3f scale = objectDef.NodeScale;

                            InstancedPropEntity entity = new InstancedPropEntity(objectDef.RefName, "", new Plane(), pos, rot, scale.X, scale.Z);
                            maxInstancedEntities.Add(entity);

                            objectsToExclude.Add(objectDef.AttributeGuid);

                            //Register this file's ITYP as a dependency; supply the SceneXML.
                            if (!additionalDependencies.Contains(exportFile.Key.Filename))
                                additionalDependencies.Add(exportFile.Key.Filename);
                        }

                    }
                }

                //Append a list of excluded objects to the SceneXML.
                string outputDirectory = Path.GetDirectoryName(output.SceneXml);
                if (!Directory.Exists(outputDirectory))
                    Directory.CreateDirectory(outputDirectory);

                if (!File.Exists(output.SceneXml))
                {
                    File.Copy(input.SceneFilename, output.SceneXml);
                    // If this is writeable, the engine can't clean it up.
                    FileInfo fileInfo = new FileInfo(output.SceneXml);
                    fileInfo.IsReadOnly = false;
                    fileInfo.Refresh();
                }

                File.SetAttributes(output.SceneXml, FileAttributes.Normal);

                if (objectsToExclude.Count > 0)
                {
                    XDocument sceneDocument = XDocument.Load(output.SceneXml);

                    XElement excludedListElement = sceneDocument.XPathSelectElement("/scene/excluded_objects");
                    if (excludedListElement == null)
                        excludedListElement = new XElement("excluded_objects");

                    foreach (Guid guid in objectsToExclude)
                    {
                        XElement excludedObject = new XElement("excluded_object");
                        XAttribute attribute = new XAttribute("guid", guid);
                        excludedObject.Add(attribute);
                        excludedListElement.Add(excludedObject);
                    }

                    XElement sceneElement = sceneDocument.XPathSelectElement("/scene");
                    sceneElement.Add(excludedListElement);
                    sceneDocument.Save(output.SceneXml);
                }
            }

            return maxInstancedEntities;
        }
        
        #region Utility Functions
        /// <summary>
        /// Creates all tasks needing to be performed to place objects in this world.
        /// </summary>
        /// <param name="minX"></param>
        /// <param name="maxX"></param>
        /// <param name="minY"></param>
        /// <param name="maxY"></param>
        /// <param name="resolution"></param>
        /// <returns></returns>
        public static IEnumerable<PlacementTask> CreatePlacementTasks(float minX, float maxX, float minY, float maxY, float resolution)
        {
             for (float x = minX; x < maxX; x += resolution)
                 for (float y = minY; y < maxY; y += resolution)
                        yield return new PlacementTask(x, y);
        }

        /// <summary>
        /// In order to preview, create a method to generate the configuration file from an input zip file.
        /// </summary>
        /// <param name="inputZipFile"></param>
        /// <returns></returns>
        public static InstancePlacementSerialiserConfig CreatePreviewConfiguration(String inputZipFile, String inputTexture)
        {
            String sceneName = Path.GetFileNameWithoutExtension(inputZipFile);
            String collisionZipFileName = sceneName + "_collision.zip";
            String cacheDirectory = Path.Combine(Branch.Project.Cache, "maps", Branch.Name, sceneName);
            String collisionCacheDirectory = Path.Combine(cacheDirectory, Path.GetFileNameWithoutExtension(collisionZipFileName));
            String collisionZipFilePath = Path.Combine(cacheDirectory, collisionZipFileName);
            
            String inputSceneXmlFile = Path.ChangeExtension(inputZipFile, ".xml");
            String outputSceneXmlFile = Path.Combine(cacheDirectory, Path.GetFileName(inputSceneXmlFile));

            String outputManifestFile = Path.Combine(cacheDirectory, "InstancePlacementAdditions.xml");

            IEnumerable<String> extractedFiles;
            MemoryStream collisionZipStream = Zip.ExtractFile(inputZipFile, collisionZipFileName);

            if (collisionZipStream == null)
            {
                //Log.ErrorCtx(LOG_CTX, String.Format("Unable to find file {0} in zip archive {1}.", collisionZipFileName, inputZipFile));
                return null;
            }

            if (!Directory.Exists(collisionCacheDirectory))
                Directory.CreateDirectory(collisionCacheDirectory);

            FileStream fileStream = new FileStream(collisionZipFilePath, FileMode.Create);
            collisionZipStream.WriteTo(fileStream);
            fileStream.Close();

            bool extractResult = Zip.ExtractAll(collisionZipFilePath, collisionCacheDirectory, true, null, out extractedFiles);

            InstancePlacementSerialiserOptions options = new InstancePlacementSerialiserOptions();

            List<InstancePlacementSerialiserTask> tasks = new List<InstancePlacementSerialiserTask>();
            foreach(InstanceCategory category in TextureMetadata.Categories)
            {
                InstancePlacementType placementType = InstancePlacementType.Unknown;
                if (Enum.TryParse<InstancePlacementType>(category.Name, true, out placementType) == false)
                {
                    Log.WarningCtx(LOG_CTX, String.Format("Unable to find instance placement type {0}.  Skipping...", category.Name));
                    continue;
                }

                //Generate the input textures.
                String inputTexturePath = inputTexture.Replace("$(instance_category)", category.Name);
                List<InstancePlacementSerialiserInputTexture> inputTextures = new List<InstancePlacementSerialiserInputTexture>();

                if (inputTexturePath.Contains("*"))
                {
                    String directory = Path.GetDirectoryName(inputTexturePath);
                    String wildcard = Path.GetFileName(inputTexturePath);

                    string[] textureFiles = Directory.GetFiles(directory, wildcard);

                    foreach(string texturePath in textureFiles)
                        inputTextures.Add(new InstancePlacementSerialiserInputTexture(texturePath));
                }
                else
                {
                    inputTextures.Add(new InstancePlacementSerialiserInputTexture(inputTexturePath));
                }

                //Generate the SceneXML inputs.
                List<InstancePlacementSerialiserInput> inputs = new List<InstancePlacementSerialiserInput>();
                InstancePlacementSerialiserInput input = new InstancePlacementSerialiserInput(inputSceneXmlFile, collisionZipFilePath);
                inputs.Add(input);

                //Determine any additional outputs (e.g. prop high-detail entities).
                String outputDirectory = System.IO.Path.Combine(cacheDirectory, "instance_placement");
                
                List<InstancePlacementSerialiserOutput> outputs = new List<InstancePlacementSerialiserOutput>();
                InstancePlacementSerialiserOutput output = new InstancePlacementSerialiserOutput(outputDirectory, null, null, outputSceneXmlFile, outputManifestFile);
                outputs.Add(output);

                // Add empty intersection inputs for now. Not sure how we'll be able to handle this for previewing, might need to build this up internally
                // using the MapInfo.xml file similar to how the pipeline does it.
                List<InstancePlacementIntersectionInput> intersectionInputs = new List<InstancePlacementIntersectionInput>();

                List<String> sceneXmlDependencies = new List<String>();
                InstancePlacementSerialiserTask task = new InstancePlacementSerialiserTask(sceneName, inputs, intersectionInputs, inputTextures, outputs, sceneXmlDependencies, placementType);
                tasks.Add(task);
            }
           
            InstancePlacementSerialiserConfig config = new InstancePlacementSerialiserConfig(options, tasks);
            return config;
        }
        #endregion Utility Functions
    }
}
