﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.OS;
using RSG.Pipeline.Core;
using RSG.Pipeline.Content;
using RSG.Pipeline.Services.Metadata;
using RSG.SceneXml;
using RSG.SceneXml.MapExport;
using RSG.SceneXml.MapExport.Project.GTA5_NG;

namespace RSG.Pipeline.InstancePlacementProcessor
{
    class Program
    {
        #region Constants
        private static readonly String LOG_CTX = "Instance Placement";
        private static readonly String OPT_CONFIG = "config";
        private static readonly String OPT_REBUILD = "rebuild";
        private static readonly String OPT_PREVIEW = "preview";
        private static readonly String OPT_INPUT_ZIP = "zip";
        private static readonly String OPT_INPUT_TEXTURE = "texture";
        private static readonly String OPT_GENERATE_MAP_DATA_INFO = "generateMapDataInfo";
        private static readonly String OPT_SERIALISE_MANIFEST_DATA = "serialiseManifest";
        private static readonly String OPT_EXTRACT_INSTANCE_DATA = "extract";
        private static readonly String OPT_SPLIT_TEXTURES = "splitTextures";
        private static readonly String OPT_BOUNDS_MANIFEST = "boundsManifest";
        #endregion // Constants

        static int Main(string[] args)
        {
            int result = Constants.Exit_Success;
            LongOption[] lopts = new LongOption[] {
                    new LongOption(OPT_CONFIG, LongOption.ArgType.Required,
                        "Instance placement configuration XML filename."),
                    new LongOption(OPT_REBUILD, LongOption.ArgType.None,
                        "Force rebuild flag."),
                    new LongOption(OPT_PREVIEW, LongOption.ArgType.None,
                        "Preview."),
                    new LongOption(OPT_INPUT_ZIP, LongOption.ArgType.Required,
                        "Input zip used for previewing."),
                    new LongOption(OPT_INPUT_TEXTURE, LongOption.ArgType.Required,
                        "Input texture used for previewing."),
                    new LongOption(OPT_GENERATE_MAP_DATA_INFO, LongOption.ArgType.None,
                        "Generate map data info file."),
                    new LongOption(OPT_SERIALISE_MANIFEST_DATA, LongOption.ArgType.None,
                        "Serialise the manifest data to the cache."),
                    new LongOption(OPT_EXTRACT_INSTANCE_DATA, LongOption.ArgType.None, 
                        "Extracts all instance definitions from SceneXMLs."),
                    new LongOption(OPT_SPLIT_TEXTURES, LongOption.ArgType.None,
                        "Splits up a global texture into its region specific texture."),
                    new LongOption(OPT_BOUNDS_MANIFEST, LongOption.ArgType.None,
                        "Generates a bounds manifest file for the supplied area.")
                };
            CommandOptions options = new CommandOptions(args, lopts);
            IUniversalLog log = LogFactory.CreateUniversalLog(LOG_CTX);
            IUniversalLogTarget logFile = LogFactory.CreateUniversalLogFile(log);
            LogFactory.CreateApplicationConsoleLogTarget();

            try
            {
                if (options.ShowHelp)
                {
                    log.MessageCtx(LOG_CTX, options.GetUsage());
                    return (result);
                }

                // Load Content-Tree.  This is required for the SceneCollection.
                IContentTree tree = Factory.CreateTree(options.Branch);
                ContentTreeHelper contentHelper = new ContentTreeHelper(tree);

                //Load metadata.
                InstancePlacementProcessor.Load(options.Branch, tree, contentHelper, log);

                //Parse configuration.
                bool preview = options.ContainsOption(OPT_PREVIEW);
                bool extract = options.ContainsOption(OPT_EXTRACT_INSTANCE_DATA);
                bool splitTextures = options.ContainsOption(OPT_SPLIT_TEXTURES);
                bool boundsManifest = options.ContainsOption(OPT_BOUNDS_MANIFEST);

                InstancePlacementSerialiserConfig config = null;
                if (preview)
                {
                    if (!options.ContainsOption(OPT_INPUT_ZIP))
                    {
                        log.ErrorCtx(LOG_CTX, "Input zip file was not specified.  Unable to preview.");
                        result = Constants.Exit_Failure;
                    }

                    if (!options.ContainsOption(OPT_INPUT_TEXTURE))
                    {
                        log.ErrorCtx(LOG_CTX, "Input texture file was not specified.  Unable to preview.");
                        result = Constants.Exit_Failure;
                    }

                    if (result == Constants.Exit_Failure)
                    {
                        log.MessageCtx(LOG_CTX, options.GetUsage());
                        return Constants.Exit_Failure;
                    }

                    String inputZip = options[OPT_INPUT_ZIP] as String;
                    String inputTexture = options[OPT_INPUT_TEXTURE] as String;

                    //Create the configuration
                    config = InstancePlacementProcessor.CreatePreviewConfiguration(inputZip, inputTexture);
                }
                else if (extract)
                {
                    String configFile = options[OPT_CONFIG] as String;
                    config = new InstancePlacementSerialiserConfig(configFile);

                    Extractor extractor = new Extractor(options.Branch, tree, contentHelper, log);
                    foreach(InstancePlacementSerialiserTask task in config.Tasks)
                    {
                        //NOTE:  This is really a two-pass process, but we plan on seldom regenerating this information.
                        //Just note that if you do use this process that you will want to generate the archetype list,
                        //copy the result into the InstancePlacementProcessor.xml and then run the extractor's
                        //Process function to generate the proper images.
                        //
                        extractor.GenerateArchetypeList(task.PlacementType);
                        extractor.Process(task.PlacementType);
                    }

                    return Constants.Exit_Success;
                }
                else if (splitTextures)
                {
                    String configFile = options[OPT_CONFIG] as String;
                    InstancePlacementTextureSerialiserConfig textureConfig = new InstancePlacementTextureSerialiserConfig(configFile);

                    TextureSplicer splicer = new TextureSplicer(options.Branch, tree, contentHelper, log);

                    foreach (InstancePlacementTextureSerialiserTask task in textureConfig.Tasks)
                    {
                        if (!splicer.SpliceTextures(task.InputTexture.TextureFilename, task.BoundsManifestXML, task.Output))
                            return Constants.Exit_Failure;
                    }

                    return Constants.Exit_Success;
                }
                else if (boundsManifest)
                {
                    String configFile = options[OPT_CONFIG] as String;
                    config = new InstancePlacementSerialiserConfig(configFile);

                    BoundsManifest manifest = new BoundsManifest(options.Branch, tree, contentHelper, log);
                    if (!manifest.GenerateBoundsManifest(config.Tasks.FirstOrDefault()))
                        return Constants.Exit_Failure;

                    return Constants.Exit_Success;
                }
                else
                {
                    if (options.ShowHelp || !options.ContainsOption(OPT_CONFIG))
                    {
                        log.MessageCtx(LOG_CTX, options.GetUsage());
                        return (result);
                    }

                    String configFile = options[OPT_CONFIG] as String;
                    config = new InstancePlacementSerialiserConfig(configFile);
                }

                bool generateMapDataInfo = false;
                if (options.ContainsOption(OPT_GENERATE_MAP_DATA_INFO))
                    generateMapDataInfo = true;

                bool serialiseManifest = false;
                if (options.ContainsOption(OPT_SERIALISE_MANIFEST_DATA))
                    serialiseManifest = true;

                List<String> sceneList = new List<String>();
                List<ManifestDependency> manifestDependencies = new List<ManifestDependency>();
                string manifestFile = "";
                if (serialiseManifest)
                    manifestFile = config.Tasks.First().Outputs.First().ManifestFile;

                foreach (InstancePlacementSerialiserTask task in config.Tasks)
                {
                    IEnumerable<String> inputScenes = task.Inputs.Select(input => input.SceneFilename);

                    foreach (String inputScene in inputScenes)
                    {
                        if (!sceneList.Contains(inputScene))
                            sceneList.Add(inputScene);
                    }

                    InstancePlacementProcessor processor = new InstancePlacementProcessor(config.Options, task, preview, serialiseManifest);
                    processor.Process(generateMapDataInfo);

                    // Grab all the manifest dependencies now that the task is complete.
                    manifestDependencies.AddRange(processor.ManifestDependencies);
                }

                if (serialiseManifest)
                {
                    List<Scene> dummySceneList = new List<Scene>();
                    InteriorContainerNameMap interiorNameMap = new InteriorContainerNameMap();
                    SceneSerialiserIMAP imapSerialiser = new SceneSerialiserIMAP(options.Branch, dummySceneList.ToArray(), null, null, "", "", interiorNameMap, null);

                    XDocument xmlManifestAdditions = Manifest.CreateManifestFile(sceneList);
                    imapSerialiser.WriteManifestData(xmlManifestAdditions, manifestDependencies);
                    Manifest.SaveManifestFile(xmlManifestAdditions, manifestFile);
                }

                if (LogFactory.HasError())
                    result = Constants.Exit_Failure;
            }
            catch (Exception ex)
            {
                log.ToolExceptionCtx(LOG_CTX, ex,
                    "Unhandled exception during Instance Placement.");
                result = Constants.Exit_Failure;
            }

            return (result);
        }
    }
}
