﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIO = System.IO;
using System.Xml.Linq;

using RSG.Base.Math;
using RSG.Base.Configuration;
using RSG.Pipeline.Content;
using RSG.Pipeline.Core;
using RSG.Pipeline.Services.Metadata;
using RSG.Pipeline.Services.InstancePlacement;
using RSG.Base.Logging.Universal;
using RSG.SceneXml;

namespace RSG.Pipeline.InstancePlacementProcessor
{
    /// <summary>
    /// Generates a bounds manifest file that lists all the collision containers for a given area
    /// and their bounds extents. This is used so we don't need to load up bounds for every texture
    /// process we have instead it does it all up front and then passes the manifest to the texture
    /// processing function.
    /// </summary>
    public class BoundsManifest
    {
        #region Constants
        private static readonly String LOG_CTX = "Instance Placement Processor";
        #endregion

        #region Properties
        private static IBranch Branch;
        private static IUniversalLog Log;
        private static IContentTree ContentTree;
        private static ContentTreeHelper ContentHelper;
        #endregion

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="tree"></param>
        /// <param name="contentHelper"></param>
        /// <param name="log"></param>
        public BoundsManifest(IBranch branch, IContentTree tree, ContentTreeHelper contentHelper,
            IUniversalLog log)
        {
            Branch = branch;
            ContentTree = tree;
            ContentHelper = contentHelper;
            Log = log;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Takes all the bounds zip files specified in our task and generates a manifest file
        /// with all the bounds extents. This file will be used for later instance placement
        /// processing tasks (texture processing).
        /// </summary>
        /// <param name="task"></param>
        /// <returns></returns>
        public bool GenerateBoundsManifest(InstancePlacementSerialiserTask task)
        {
            IEnumerable<String> collisionPaths = task.Inputs.Select(taskInput => taskInput.CollisionPath);
            List<BoundsEntry> boundsEntries = new List<BoundsEntry>();

            foreach (String collisionPath in collisionPaths)
            {
                ICollection<OptimisedBound> optimisedBounds = new List<OptimisedBound>();
                BoundingBox3f boundsExtents = new BoundingBox3f();
                if (!BoundsProcessing.ProcessBounds(collisionPath, optimisedBounds, boundsExtents, Log))
                {
                    Log.ErrorCtx(LOG_CTX, String.Format("Failed to process bounds {0}.  Aborting instance placement.", collisionPath));
                    return false;
                }

                // Make sure we actually have bounds extents from processing otherwise things will go haywire later.
                if (boundsExtents.IsEmpty)
                {
                    Log.ErrorCtx(LOG_CTX, String.Format("Failed to process bounds {0} as it is reporting a bad bounds extents.", collisionPath));
                    return false;
                }

                //Round the bounding box to the nearest integer.  Floating point imprecision
                //could cause the area where this texture is allocated to be incorrect.
                boundsExtents.Min.X = BoundsProcessing.RoundNearest(boundsExtents.Min.X);
                boundsExtents.Min.Y = BoundsProcessing.RoundNearest(boundsExtents.Min.Y);
                boundsExtents.Min.Z = BoundsProcessing.RoundNearest(boundsExtents.Min.Z);
                boundsExtents.Max.X = BoundsProcessing.RoundNearest(boundsExtents.Max.X);
                boundsExtents.Max.Y = BoundsProcessing.RoundNearest(boundsExtents.Max.Y);
                boundsExtents.Max.Z = BoundsProcessing.RoundNearest(boundsExtents.Max.Z);

                BoundsEntry newEntry = new BoundsEntry(collisionPath, boundsExtents);
                boundsEntries.Add(newEntry);
            }

            // Generate our manifest file.
            InstancePlacementSerialiserOutput output = task.Outputs.FirstOrDefault();
            String boundsManifestXML = output.OutputFile;

            XDocument document = new XDocument(
                    new XElement("BoundsManifestData",
                        boundsEntries.Select(boundsEntry => boundsEntry.ToXElement("BoundsEntry"))));

            document.Save(boundsManifestXML);

            return true;
        }
        #endregion
    }
}
