﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

using RSG.Base.Configuration;
using RSG.Pipeline.Content;
using RSG.Pipeline.Core;
using RSG.Pipeline.Services.Metadata;
using RSG.Pipeline.Services.InstancePlacement;
using RSG.Base.Logging.Universal;
using RSG.SceneXml;

namespace RSG.Pipeline.InstancePlacementProcessor
{
    public class Extractor
    {
        #region Constants
        private static readonly String LOG_CTX = "Instance Placement Processor";
        #endregion Constants

        #region Properties
        private static String TextureMetadataPath;
        private static TextureMetadata TextureMetadata;
        private static IBranch Branch;
        private static IUniversalLog Log;
        private static IContentTree ContentTree;
        private static ContentTreeHelper ContentHelper;
        private static SceneCollection SceneCollection;
        #endregion

        #region Constructors
        /// <summary>
        /// Main constructor.
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="tree"></param>
        /// <param name="contentHelper"></param>
        /// <param name="processors"></param>
        /// <param name="log"></param>
        public Extractor(IBranch branch, IContentTree tree, ContentTreeHelper contentHelper,
            IUniversalLog log)
        {
            Log = log;
            Branch = branch;
            ContentTree = tree;
            ContentHelper = contentHelper;
            TextureMetadataPath = Path.Combine(branch.Project.Config.ToolsConfig, 
                "processors", "RSG.Pipeline.InstancePlacementProcessor.xml");
            TextureMetadata = TextureMetadata.Load(TextureMetadataPath, branch, log);
            SceneCollection = new SceneCollection(Branch, ContentTree, SceneCollectionLoadOption.None);
            
            foreach(IContentNode exportNode in ContentHelper.GetAllMapExportNodes())
            {
                Content.File exportFileNode = (Content.File) exportNode;
                String sceneName = Path.GetFileNameWithoutExtension(exportFileNode.AbsolutePath);

                if (sceneName.Contains("_vegetation"))
                    SceneCollection.GetScene(sceneName);
            }
        }
        #endregion

        /// <summary>
        /// Processes all instances of a type; pushing this data into an image.
        /// </summary>
        /// <param name="placementType"></param>
        /// <returns></returns>
        public bool Process(InstancePlacementType placementType)
        {
            TextureMetadata.SetSelectedCategory(placementType);

            InstanceDefinition[] definitions = TextureMetadata.GetDefinitions();

            int textureWidth = (int) Math.Ceiling(TextureMetadata.Bounds.MaxBounds.X - TextureMetadata.Bounds.MinBounds.X);
            int textureHeight = (int) Math.Ceiling(TextureMetadata.Bounds.MaxBounds.Y - TextureMetadata.Bounds.MinBounds.Y);
            Bitmap bitmap = new Bitmap(textureWidth, textureHeight);
            for (int x = 0; x < textureWidth; x++)
            {
                for (int y = 0; y < textureHeight; y++)
                {
                    bitmap.SetPixel(x, y, Color.White);
                }
            }

            List<String> usedArchetypes = new List<String>();
            foreach (Scene scene in SceneCollection)
            {
                if (scene.SceneType == SceneType.EnvironmentProps) continue;

                Log.MessageCtx(LOG_CTX, String.Format("Processing scene \"{0}\"...", Path.GetFileNameWithoutExtension(scene.Filename)));
                foreach (TargetObjectDef targetObjectDef in scene.Walk(Scene.WalkMode.DepthFirst))
                {
                    foreach(InstanceDefinition definition in definitions)
                    {
                        if ( String.Compare(definition.HighDetailName, targetObjectDef.ObjectDef.RefName, true) == 0)
                        {
                            //Set this definition's pixel onto the texture.
                            int x = (int) Math.Round(targetObjectDef.ObjectDef.NodeTransform.Translation.X - TextureMetadata.Bounds.MinBounds.X);
                            int y = (int)Math.Round(targetObjectDef.ObjectDef.NodeTransform.Translation.Y - TextureMetadata.Bounds.MinBounds.Y);
                            y = (int)bitmap.Height - y - 1;   //Bitmap coordinate y-axis is top-down.

                            if (x < 0 || x >= bitmap.Width || y < 0 || y >= bitmap.Height)
                            {
                                continue;
                            }

                            bitmap.SetPixel(x, y, definition.Color);

                            if (usedArchetypes.Contains(definition.HighDetailName) == false)
                            {
                                usedArchetypes.Add(definition.HighDetailName);
                            }
                        }
                    }
                }
            }
            bitmap.Save(Path.Combine(Branch.Metadata, "terrain", placementType + ".bmp"), System.Drawing.Imaging.ImageFormat.Bmp);

            //Register only the archetypes that the images use.
            StreamWriter writer = new StreamWriter(Path.Combine(Branch.Assets, "usedArchetypes.txt"));
            foreach (String usedArchetype in usedArchetypes)
            {
                writer.WriteLine(usedArchetype);
            }
            writer.Close();

            PopulateDefinitions(placementType, usedArchetypes);
            return true;
        }

        /// <summary>
        /// This is a project-specific function for extracting objects with particular attributes (in the
        /// initial implementation only object with tree shaders are extracted).  
        /// The collection of archetypes are then shuttled into a partial XML file to then be copied into
        /// the pre-existing InstancePlacementProcessor.xml so the output image via the Process() call
        /// has correct input values.
        /// </summary>
        public void GenerateArchetypeList(InstancePlacementType placementType)
        {
            String TextureMetadataPath = Path.Combine(Branch.Project.Config.ToolsConfig,
                "processors", "RSG.Pipeline.InstancePlacementProcessor.xml");
            TextureMetadata metadata = TextureMetadata.Load(TextureMetadataPath, Branch, Log);

            metadata.SetSelectedCategory(placementType);

            SceneCollection propSceneCollection = new SceneCollection(Branch, ContentTree, SceneCollectionLoadOption.None);

            IEnumerable<String> additionalDependencies = metadata.SelectedCategory.Dependencies;
            additionalDependencies = additionalDependencies.Select(path => path = Branch.Environment.Subst(path));

            foreach (String inputFile in additionalDependencies)
            {
                String sceneName = Path.GetFileNameWithoutExtension(inputFile);
                propSceneCollection.GetScene(sceneName);
            }

            //This is *not* the Texture Metadata's shader listing because we have some instances
            //of trees that are using non-instance materials; Art would like to convert these over
            //as part of the extraction process.
            //
            List<String> validPresetTypes = new List<String>();
            validPresetTypes.Add("trees_");  
            
            List<String> archetypeNames = new List<String>();
            foreach (Scene scene in propSceneCollection)
            {
                 if (scene == null || scene.SceneType != SceneType.EnvironmentProps) continue;

                foreach(TargetObjectDef objectDef in scene.Walk(Scene.WalkMode.DepthFirst))
                {
                    if (objectDef.Material == Guid.Empty) continue;

                    SceneXml.Material.MaterialDef material = scene.MaterialLookup[objectDef.Material];

                    if (material.MaterialType == SceneXml.Material.MaterialTypes.Rage)
                    {
                        foreach (String validPreset in validPresetTypes)
                        {
                            if (material.Preset.Contains(validPreset) == true)
                            {
                                if (archetypeNames.Contains(objectDef.Name) == false)
                                    archetypeNames.Add(objectDef.Name);
                            }
                        }
                    }
                    else if (material.MaterialType == SceneXml.Material.MaterialTypes.MultiMaterial)
                    {
                        if (material.SubMaterials == null) continue;

                        foreach (SceneXml.Material.MaterialDef subMaterial in material.SubMaterials)
                        {
                            if (subMaterial.MaterialType == SceneXml.Material.MaterialTypes.Rage)
                            {
                                foreach (String validPreset in validPresetTypes)
                                {
                                    if (subMaterial.Preset.Contains(validPreset) == true)
                                    {
                                        if (archetypeNames.Contains(objectDef.Name) == false)
                                            archetypeNames.Add(objectDef.Name);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            PopulateDefinitions(placementType, archetypeNames);
        }

        /// <summary>
        /// Utility function for generating instance placement definitions from a list of archetypes.
        /// </summary>
        /// <param name="archetypeNames"></param>
        private void PopulateDefinitions(InstancePlacementType placementType, List<String> archetypeNames)
        {
            StreamWriter writer = new StreamWriter( Path.Combine(Branch.Project.Config.ToolsConfig, "processors", placementType + ".xml"));

            List<String> usedColors = new List<String>();
            Random random = new Random(123456789);
            foreach (string archetypeName in archetypeNames)
            {
                bool reassign = true;
                string htmlColor = null;
                while (reassign == true)
                {
                    Color color = Color.FromArgb(random.Next() % 255, random.Next() % 255, random.Next() % 255);
                    htmlColor = ColorTranslator.ToHtml(color);
                    if (usedColors.Contains(htmlColor) == false)
                    {
                        usedColors.Add(htmlColor);
                        reassign = false;
                    }
                }

                writer.WriteLine(String.Format("<Definition Name=\"{0}\" HighDetailName=\"{0}\" LODDistance=\"1000\" HighDetailLODDistance=\"120\">", archetypeName));
                writer.WriteLine(String.Format("\t<Color Value=\"{0}\" />", htmlColor));
                writer.WriteLine("</Definition>");
            }

            writer.Close();
        }
    }
}
