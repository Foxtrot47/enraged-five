﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace RSG.Pipeline.Processor.Map.AssetCombine
{
    internal class TextureDictionaryInput
    {
        internal TextureDictionaryInput(XElement element)
        {
            Name = element.Attribute("name").Value;
            Type = element.Attribute("type").Value;
            CacheDirectory = element.Attribute("cachedir").Value;
            SourceZipPathname = element.Attribute("source").Value;
            XElement requiredItemsElement = element.Element("RequiredItems");
            if (requiredItemsElement != null)
                RequiredItems = requiredItemsElement.Elements("Item").Select(itemElement => itemElement.Value).ToArray();
            else
                RequiredItems = new String[0];

            bool isNewDLC;
            bool.TryParse(element.Attribute("is_new_dlc").Value, out isNewDLC);
            IsNewDLC = isNewDLC; // isNewDLC is always set by TryParse. Magic.
        }

        internal String Name { get; private set; }
        internal String Type { get; private set; }
        internal String CacheDirectory { get; private set; }
        internal String SourceZipPathname { get; private set; }
        internal IEnumerable<String> RequiredItems { get; private set; }
        internal bool IsNewDLC { get; private set; }
    }
}
