﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using SIO = System.IO;
using System.Linq;
using System.Text;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using XGE = RSG.Interop.Incredibuild.XGE;
using RSG.Pipeline.Content;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Attributes;
using RSG.Pipeline.Services;
using RSG.Pipeline.Services.Metadata;

namespace RSG.Pipeline.Processor.Map
{
    /// <summary>
    /// Instance placement processor.
    /// </summary>
    [Export(typeof(IProcessor))]
    [ProcessorFlags(ProcessorFlags.Compatibility)]
    public class InstancePlacementProcessor :
        ProcessorBase,
        IProcessor
    {
        #region Constants
        /// <summary>
        /// Processor description.
        /// </summary>
        private static readonly String DESCRIPTION = "Instance Placement Processor";

        /// <summary>
        /// Processor log context.
        /// </summary>
        private static readonly String LOG_CTX = "Instance Placement Processor";

        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public InstancePlacementProcessor()
            : base(DESCRIPTION)
        {

        }
        #endregion // Constructor(s)

        #region IProcessor Interface Methods
        /// <summary>
        /// Prebuild stage; contains simple validation of passed process.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="process"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="syncDependencies"></param>
        /// <param name="resultantProcesses"></param>
        /// <returns></returns>
        public override bool Prebuild(IEngineParameters param,
            IProcess process, IProcessorCollection processors,
            IContentTree owner, out IEnumerable<IContentNode> syncDependencies,
            out IEnumerable<IProcess> resultantProcesses)
        {
            // Process verification.
            Debug.Assert(process.Inputs.Count() > 0);
            Debug.Assert(process.Outputs.Count() > 0);

            this.LoadParameters(param);

            bool result = true;
            ICollection<IProcess> processes = new List<IProcess>();
            process.State = ProcessState.Prebuilt;
            processes.Add(process);

            syncDependencies = new List<IContentNode>();
            resultantProcesses = processes;
            return (result);
        }

        /// <summary>
        /// Prepare a build for the processes; populating the XGE project with
        /// the tools, environments and tasks required.  Including setting up
        /// the task dependency chain.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="processes"></param>
        /// <param name="tools"></param>
        /// <param name="tasks"></param>
        /// <returns></returns>
        public override bool Prepare(IEngineParameters param, IEnumerable<IProcess> processes,
            out IEnumerable<XGE.ITool> tools, out IEnumerable<XGE.ITask> tasks)
        {
            bool xgeAllowRemote = GetXGEAllowRemote(param, true);

            XGE.ITaskJobGroup taskGroup = new XGE.TaskGroup(DESCRIPTION);
            XGE.ITool tool = XGEFactory.GetMapProcessorTool(param.Log, param.Branch.Project.Config, XGEFactory.MapToolType.InstancePlacementProcessor, DESCRIPTION, xgeAllowRemote);
            ICollection<XGE.ITool> convertTools = new List<XGE.ITool>();
            convertTools.Add(tool);

            bool result = true;
            InstancePlacementSerialiserOptions options = new InstancePlacementSerialiserOptions();
            
            int index = 0;
            List<IProcess> processList = processes.ToList();
            const int maxProcessesPerTask = 8;
            int maxBuckets = (int)Math.Ceiling((double)processList.Count() / (double)maxProcessesPerTask);
            ICollection<XGE.ITask> convertTasks = new List<XGE.ITask>();

            for (int bucketIndex = 0; bucketIndex < maxBuckets; ++bucketIndex)
            {
                int startBucketIndex = bucketIndex*maxProcessesPerTask;
                List<IProcess> bucketProcesses = processList.GetRange(startBucketIndex, Math.Min(maxProcessesPerTask, processList.Count() - startBucketIndex));

                String caption = String.Format("{0} {1}", DESCRIPTION, index);
                XGE.TaskJob xgeTask = new XGE.TaskJob(caption);

                //Create the configuration options file for this process.
                ICollection<InstancePlacementSerialiserTask> tasksData = new List<InstancePlacementSerialiserTask>();

                foreach (IProcess process in bucketProcesses)
                {
                    Content.File inputNode = process.Inputs.First() as Content.File;

                    IEnumerable<IContentNode> inputTextureNodes = (IEnumerable<IContentNode>)process.Parameters[Consts.InstancePlacement_InputTextures];
                    
                    List<InstancePlacementSerialiserInputTexture> inputTextures = new List<InstancePlacementSerialiserInputTexture>();

                    String inputDestination = GetSourceZipAssetCacheDir(param, process, inputNode);

                    String collisionProcessInputPathname;
                    IEnumerable<String> zipFiles = SIO.Directory.GetFiles(inputDestination, "*");
                    if (!Collision.GetUnprocessedCollisionPathname(zipFiles, out collisionProcessInputPathname))
                    {
                        param.Log.ErrorCtx(LOG_CTX, String.Format("Unable to determine collision input path for process {0}.", inputNode.Name));
                        continue;
                    }

                    xgeTask.InputFiles.Add(collisionProcessInputPathname);

                    Content.File sceneXmlFile = null;
                    List<String> sceneXmlDependencies = null;
                    IList<IContentNode> metadataInputs = (IList<IContentNode>)process.Parameters[Consts.MetadataSerialiser_InputToProcessParameterPrefix];

                    if (metadataInputs.Count() > 0)
                    {
                        //Add the SceneXML from the Max file and all export zip dependencies
                        //to be used by the instance placement processor.
                        sceneXmlFile = process.Outputs.ElementAt(0) as Content.File;

                        sceneXmlDependencies = new List<String>();
                        ICollection<IContentNode> sceneXmlDependenciesCollection = (ICollection<IContentNode>)process.Parameters[Consts.InstancePlacement_AdditionalSceneXMLDependencies];
                        foreach (IContentNode sceneXmlDependency in sceneXmlDependenciesCollection)
                        {
                            Content.File sceneXmlDependencyFile = sceneXmlDependency as Content.File;
                            sceneXmlDependencies.Add(sceneXmlDependencyFile.AbsolutePath);
                        }
                    }

                    //Add a task for every type to be placed.
                    InstancePlacementType placementType = (InstancePlacementType)process.Parameters[Consts.InstancePlacement_PlacementType];
                    List<InstancePlacementSerialiserInput> inputs = new List<InstancePlacementSerialiserInput>();

                    //Find all texture inputs.
                    foreach (IContentNode inputTextureNode in inputTextureNodes)
                    {
                        Content.File inputTextureFileNode = inputTextureNode as Content.File;
                        xgeTask.InputFiles.Add(inputTextureFileNode.AbsolutePath);

                        InstancePlacementTextureType textureType = InstancePlacementTextureType.Default;
                        if (inputTextureFileNode.Parameters.ContainsKey(Consts.InstancePlacement_GlobalDataTexture))
                            textureType = InstancePlacementTextureType.Global_Data;
                        if (inputTextureFileNode.Parameters.ContainsKey(Consts.InstancePlacement_GlobalTintTexture))
                            textureType = InstancePlacementTextureType.Global_Tint;

                        InstancePlacementSerialiserInputTexture inputTexture = new InstancePlacementSerialiserInputTexture(inputTextureFileNode.AbsolutePath, textureType);
                        inputTextures.Add(inputTexture);
                    }

                    //Acquire all SceneXML inputs.
                    foreach (IContentNode metadataInput in metadataInputs)
                    {
                        Content.File metadataFileInput = metadataInput as Content.File;
                        xgeTask.InputFiles.Add(metadataFileInput.AbsolutePath);
                        InstancePlacementSerialiserInput input = new InstancePlacementSerialiserInput(metadataFileInput.AbsolutePath, collisionProcessInputPathname);
                        inputs.Add(input);
                    }

                    List<InstancePlacementIntersectionInput> intersectionInputs = new List<InstancePlacementIntersectionInput>();

                    //Find all intersection inputs
                    if (process.Parameters.ContainsKey(Consts.InstancePlacement_OverlappingContainerDependencies))
                    {
                        IList<String> intersectionContainers = (IList<String>)process.Parameters[Consts.InstancePlacement_OverlappingContainerDependencies];
                        foreach (String intersectionInput in intersectionContainers)
                        {
                            String containerName = SIO.Path.GetFileName(intersectionInput);

                            IEnumerable<String> intersectionInputFiles = SIO.Directory.GetFiles(intersectionInput, "*");
                            string intersectionZipFile;

                            if (!Collision.GetUnprocessedCollisionPathname(intersectionInputFiles, out intersectionZipFile))
                            {
                                // TODO - This should be an error because we need to make sure the collision exists for all overlapping containers.
                                // RDR doesn't seem to have all sections with collision setup yet though.
                                param.Log.WarningCtx(LOG_CTX, String.Format("Unable to determine collision input path for overlapped container {0}.", containerName));
                                continue;
                            }

                            String collisionPath = intersectionZipFile;

                            InstancePlacementIntersectionInput input = new InstancePlacementIntersectionInput(containerName, collisionPath);
                            intersectionInputs.Add(input);
                        }
                    }

                    //Acquire all outputs.
                    Content.Directory outputDirectory = process.Outputs.ElementAt(1) as Content.Directory;
                    List<InstancePlacementSerialiserOutput> outputs = new List<InstancePlacementSerialiserOutput>();
                    InstancePlacementSerialiserOutput output = new InstancePlacementSerialiserOutput(outputDirectory.AbsolutePath, null, null, sceneXmlFile.AbsolutePath, null);
                    outputs.Add(output);

                    InstancePlacementSerialiserTask task = new InstancePlacementSerialiserTask(inputNode.Name, inputs, intersectionInputs, inputTextures, outputs,
                            sceneXmlDependencies, placementType);
                    tasksData.Add(task);

                    //Create the XGE task.
                    IContentNode contentNode = process.Inputs.First();
                    String inputPath = null;
                    if (contentNode is Content.File)
                    {
                        Content.File contentFile = contentNode as Content.File;
                        inputPath = contentFile.AbsolutePath;
                    }
                    else if (contentNode is Content.Asset)
                    {
                        Content.Asset contentAsset = contentNode as Content.Asset;
                        inputPath = contentAsset.AbsolutePath;
                    }
                    else if (contentNode is Content.Directory)
                    {
                        Content.Directory contentDir = contentNode as Content.Directory;
                        inputPath = contentDir.AbsolutePath;
                    }

                    process.Parameters.Add(Constants.ProcessXGE_Task, xgeTask);
                    process.Parameters.Add(Constants.ProcessXGE_TaskName, caption);
                }

                InstancePlacementSerialiserConfig config = new InstancePlacementSerialiserConfig(options, tasksData);
                String cacheDirectory = (String)processes.First().Parameters[Consts.MapProcess_ProcessedMapCacheDirectory];
                cacheDirectory = param.Branch.Environment.Subst(cacheDirectory);
                String configDataFilename = String.Format("instance_placement_{0}.xml", index);
                String configDataPathname = SIO.Path.Combine(cacheDirectory, configDataFilename);
                configDataPathname = SIO.Path.GetFullPath(configDataPathname);
                config.Serialise(param.Log, configDataPathname);

                StringBuilder paramSb = new StringBuilder();
                if (param.Flags.HasFlag(EngineFlags.Rebuild))
                    paramSb.Append("-rebuild ");
                paramSb.AppendFormat("-config \"{0}\" ", configDataPathname);

                index++;

                xgeTask.Parameters = paramSb.ToString();
                xgeTask.SourceFile = configDataPathname;
                xgeTask.InputFiles.Add(configDataPathname);
                xgeTask.Caption = caption;
                xgeTask.Tool = tool;

                convertTasks.Add(xgeTask);
            }

            foreach (XGE.ITaskJob taskJob in convertTasks)
                taskGroup.Tasks.Add(taskJob);

            tools = convertTools;
            tasks = new List<XGE.ITask>(new XGE.ITask[] { taskGroup });
            return (result);
        }
        #endregion // IProcessor Interface Methods

        #region Private Methods
        /// <summary>
        /// Return source map cache directory for an asset.
        /// </summary>
        /// <param name="asset"></param>
        /// <returns></returns>
        private String GetSourceZipAssetCacheDir(IEngineParameters param, IProcess process, Content.File asset)
        {
            Debug.Assert(process.Parameters.ContainsKey(Consts.MapProcess_SourceMapCacheDirectory),
                "Cache directory not found in parameters!  Internal error.");
            if (!process.Parameters.ContainsKey(Consts.MapProcess_SourceMapCacheDirectory))
            {
                param.Log.ErrorCtx(LOG_CTX, "Cache directory not found in parameters!  Internal error.");
                throw (new NotSupportedException("Cache directory not found in parameters!  Internal error."));
            }

            String cacheDirectory = (String)process.Parameters[Consts.MapProcess_SourceMapCacheDirectory];
            String cacheRoot = SIO.Path.GetFullPath(param.Branch.Environment.Subst(cacheDirectory));
            return (SIO.Path.Combine(cacheRoot, asset.Name));
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Processor.Map namespace
