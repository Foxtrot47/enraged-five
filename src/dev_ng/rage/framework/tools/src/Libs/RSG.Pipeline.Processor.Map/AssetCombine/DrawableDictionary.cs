﻿using System.Linq;
using System.Xml.Linq;

namespace RSG.Pipeline.Processor.Map.AssetCombine
{
    internal class DrawableDictionary
    {
        internal DrawableDictionary(XElement element)
        {
            Name = element.Attribute("name").Value;
            TextureDictionaryName = element.Attribute("txd").Value;
            LOD = element.Attribute("lod").Value;
            bool isNewDlc;
            bool.TryParse(element.Attribute("isNewDlc").Value, out isNewDlc);
            IsNewDlc = isNewDlc;

            Inputs = element.Element("Inputs").Elements("Input").
                Select(input => new DrawableDictionaryInput(input)).ToArray();

        }

        internal string Name { get; private set; }
        internal string TextureDictionaryName { get; private set; }
        internal string LOD { get; private set; }
        internal bool IsNewDlc { get; private set; }
        internal DrawableDictionaryInput[] Inputs { get; private set; }
    }
}
