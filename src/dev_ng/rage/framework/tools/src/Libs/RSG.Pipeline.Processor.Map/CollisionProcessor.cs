﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using XGE = RSG.Interop.Incredibuild.XGE;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Attributes;
using RSG.Pipeline.Content;
using RSG.Pipeline.Services;
using RSG.Pipeline.Services.CollisionProcessor;

namespace RSG.Pipeline.Processor.Map
{
    
    /// <summary>
    /// Map Collision Processor (MKII).
    /// </summary>
    /// Map Collision Processor (MKII) is the wrapper around the RSG.Pipeline.CollisionProcessor
    /// pipeline executable; providing a single executable/processor for collision transformations.
    /// 
    [Export(typeof(IProcessor))]
    [ProcessorFlags(ProcessorFlags.Compatibility)]
    class CollisionProcessor : 
        MapProcessorBase,
        IProcessor
    {
        #region Constants
        /// <summary>
        /// Processor description string.
        /// </summary>
        private static readonly String _description = "Map Collision Processor (MKII)";
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public CollisionProcessor()
            : base(_description)
        {
        }
        #endregion // Constructor(s)

        #region IProcessor Interface Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="process"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="syncDependencies"></param>
        /// <param name="resultantProcesses"></param>
        /// <returns></returns>
        public override bool Prebuild(IEngineParameters param,
            IProcess process, IProcessorCollection processors,
            IContentTree owner, out IEnumerable<IContentNode> syncDependencies,
            out IEnumerable<IProcess> resultantProcesses)
        {
            bool result = true;

            // Verify process.
            Debug.Assert(process.Inputs.OfType<Content.File>().Any(),
                "CollisionProcessor requires one or more input files.");
            if (0 == process.Inputs.OfType<Content.File>().Count())
            {
                param.Log.Error("CollisionProcessor requires one or more input files.");
                result = false;
            }

            Debug.Assert(process.Outputs.First() is Content.Directory,
                "CollisionProcessor requires an output directory.");
            if (!(process.Outputs.First() is Content.Directory))
            {
                param.Log.Error("CollisionProcessor requires an output directory.");
                result = false;
            }

            Debug.Assert(process.Parameters.ContainsKey(Constants.Map_CollisionProcessor_Operation),
                String.Format("CollisionProcessor requires an '{0}' parameter.", Constants.Map_CollisionProcessor_Operation));
            if (!process.Parameters.ContainsKey(Constants.Map_CollisionProcessor_Operation))
            {
                param.Log.Error("CollisionProcessor requires an '{0}' parameter.", Constants.Map_CollisionProcessor_Operation);
                result = false;
            }
            else
            {
                String opString = (String)process.Parameters[Constants.Map_CollisionProcessor_Operation];
                Operation op;
                if (!Enum.TryParse(opString, true, out op))
                {
                    param.Log.Error("Invalid CollisionProcessor operation: {0}.",
                        process.Parameters[Constants.Map_CollisionProcessor_Operation]);
                    result = false;
                }
            }

            Debug.Assert(process.Parameters.ContainsKey(Constants.Map_CollisionProcessor_Configuration),
                "CollisionProcessor requires a configuration metadata file.");
            if (!process.Parameters.ContainsKey(Constants.Map_CollisionProcessor_Configuration))
            {
                param.Log.Error("CollisionProcessor requires a configuration metadata file.");
                result = false;
            }

            ICollection<IProcess> processes = new List<IProcess>();
            process.State = ProcessState.Prebuilt;
            processes.Add(process);

            syncDependencies = new List<IContentNode>();
            resultantProcesses = processes;

            return (result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processes"></param>
        /// <param name="tools"></param>
        /// <param name="tasks"></param>
        /// <returns></returns>
        public override bool Prepare(IEngineParameters param, IEnumerable<IProcess> processes,
            out IEnumerable<XGE.ITool> tools, out IEnumerable<XGE.ITask> tasks)
        {
            bool result = true;
            bool allow_remote = GetXGEAllowRemote(param, true);
            XGE.ITool serialiserTool = XGEFactory.GetMapProcessorTool(param.Log,
                param.Branch.Project.Config, XGEFactory.MapToolType.CollisionProcessor,
                "Collision Processor", allow_remote);
            XGE.ITaskJobGroup taskGroup = new XGE.TaskGroup("Collision Processor");

            int processCounter = 0;
            foreach (IProcess process in processes)
            {
                IEnumerable<String> inputFiles = process.Inputs.OfType<Content.File>().Select(i => i.AbsolutePath);
                Content.Directory outputDirectory = process.Outputs.OfType<Content.Directory>().First();
                
                // Build the config for the bounds processor
                String configFilename = Path.Combine("$(cache)", "maps", param.Branch.Name, 
                    String.Format("CollisionProcessor_Task{0}.xml", processCounter));
                configFilename = param.Branch.Environment.Subst(configFilename);
                
                String opString = (String)process.Parameters[Constants.Map_CollisionProcessor_Operation];
                Operation op = (Operation)Enum.Parse(typeof(Operation), opString);
                switch (op)
                {
                    case Operation.AmbientScanner:
                        String scannerConfigFilename = (String)process.Parameters[Constants.Map_CollisionProcessor_Configuration];
                        CollisionProcessorTask procTask = new CollisionProcessorTask(param.Branch, op, scannerConfigFilename, outputDirectory.AbsolutePath);
                        CollisionProcessorConfig config = new CollisionProcessorConfig(inputFiles, new CollisionProcessorTask[] { procTask });
                        config.Save(configFilename);
                        break;
                    default:
                        param.Log.Error("Operation not currently supported: '{0}'.", opString);
                        result = false;
                        break;
                }

                String taskName = String.Format("Collision Processor {0} [{1}]", processCounter++, opString);
                String flatTaskName = taskName.Replace(" ", "_");
                XGE.ITaskJob task = new XGE.TaskJob(taskName);
                task.Tool = serialiserTool;
                task.Caption = taskName;
                task.InputFiles.AddRange(inputFiles);
                task.OutputFiles.Add(outputDirectory.AbsolutePath);
                task.Parameters = String.Format("--taskname {0} --config {1}", flatTaskName, configFilename);
                task.SourceFile = inputFiles.First();

                process.Parameters.Add(Constants.ProcessXGE_Task, task);
                process.Parameters.Add(Constants.ProcessXGE_TaskName, flatTaskName);
                taskGroup.Tasks.Add(task);
            }

            // Assign method output.
            tools = new XGE.ITool[] { serialiserTool };
            tasks = new XGE.ITask[] { taskGroup };

            return (true);
        }
        #endregion // IProcessor Interface Methods
    }

} // RSG.Pipeline.Processor.Map namespace
