﻿using System.Linq;
using System.Xml.Linq;

namespace RSG.Pipeline.Processor.Map.AssetCombine
{
    internal class TextureDictionary
    {
        internal TextureDictionary(XElement element)
        {
            Name = element.Attribute("name").Value;
            
            bool isNewDlc;
            bool.TryParse(element.Attribute("isNewDlc").Value, out isNewDlc);
            IsNewDlc = isNewDlc;
            
            bool isNewTxd;
            bool.TryParse(element.Attribute("isNewTxd").Value, out isNewTxd);
            IsNewTxd = isNewTxd;

            Inputs = element.Element("Inputs").Elements("Input").
                Select(input => new TextureDictionaryInput(input)).ToArray();
        }

        internal string Name { get; private set; }
        internal  bool IsNewDlc { get; private set; }
        internal bool IsNewTxd { get; private set; }
        internal TextureDictionaryInput[] Inputs { get; private set; }
    }
}
