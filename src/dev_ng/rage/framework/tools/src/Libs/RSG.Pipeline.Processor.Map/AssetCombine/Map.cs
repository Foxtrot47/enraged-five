﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.Pipeline.Processor.Map.AssetCombine
{

    /// <summary>
    /// Class that reads and stores Asset Combine executable document output.
    /// </summary>
    internal class Map
    {
        internal Map(XElement element)
        {
            Name = element.Attribute("name").Value;
            CacheDirectory = element.Attribute("cachedir").Value;
            DrawableDictionaries = element.Elements("DrawableDictionary").
                Select(drawableDictionaryElement => new DrawableDictionary(drawableDictionaryElement)).ToArray();
            TextureDictionaries = element.Elements("TextureDictionary").
                Select(textureDictionaryElement => new TextureDictionary(textureDictionaryElement)).ToArray();
        }

        internal String Name { get; private set; }
        internal String CacheDirectory { get; private set; }
        internal DrawableDictionary[] DrawableDictionaries { get; private set; }
        internal TextureDictionary[] TextureDictionaries { get; private set; }
    }

} // RSG.Pipeline.Processor.Map.AssetCombine namespace

