﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using SIO = System.IO;
using System.Linq;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using XGE = RSG.Interop.Incredibuild.XGE;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Util;
using RSG.Pipeline.Content;
using RSG.Pipeline.Services;
using RSG.Pipeline.Services.AssetCombine;
using RSG.Pipeline.Services.AssetPack;

namespace RSG.Pipeline.Processor.Map
{

    /// <summary>
    /// Asset Combine PreProcess; this processor determines all of the Asset Combine 
    /// dependencies and creates the necessary Asset Pack Processor processes.
    /// </summary>
    /// This replaces the Asset Combine coordinator singleton that is used in the
    /// Map PreProcess (MKI).
    /// 
    [Export(typeof(IProcessor))]
    class AssetCombinePreProcess : 
        MapProcessorBase,
        IProcessor
    {
        #region Constants
        private static readonly String DESCRIPTION = "Asset Combine PreProcessor";
        private static readonly String LOG_CTX = "Asset Combine PreProcess";
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public AssetCombinePreProcess()
            : base(DESCRIPTION)
        {
        }
        #endregion // Constructor(s)

        #region IProcessor Interface Methods
        /// <summary>
        /// Asset Combine PreProcess prebuild.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="process"></param>
        /// <param name="processors"></param>
        /// <param name="targetContentTree"></param>
        /// <param name="syncDependencies"></param>
        /// <param name="resultantProcesses"></param>
        /// <returns></returns>
        /// For any given map; we construct the following processes:
        ///  1. A map metadata process
        ///  2. A collision process
        ///  3. Where required, an asset pack process for merged content (e.g. dt1_01 => downtown)
        ///  4. An asset pack process for the processed content
        ///  5. A rage process to convert processed intermediate data to platform data
        ///
        public override bool Prebuild(IEngineParameters param,
            IProcess process, IProcessorCollection processors,
            IContentTree targetContentTree, out IEnumerable<IContentNode> syncDependencies,
            out IEnumerable<IProcess> resultantProcesses)
        {
            using (ProfileContext ctx = new ProfileContext(param.Log, "Asset Combine PreProcess: Prebuild"))
            {
                return (PrebuildImpl(param, process, processors, targetContentTree,
                    out syncDependencies, out resultantProcesses));
            }
        }

        /// <summary>
        /// Prepare; this should never get called.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processes"></param>
        /// <param name="tools"></param>
        /// <param name="tasks"></param>
        /// <returns></returns>
        public override bool Prepare(IEngineParameters param, IEnumerable<IProcess> processes,
            out IEnumerable<XGE.ITool> tools, out IEnumerable<XGE.ITask> tasks)
        {
            // This should never get hit; the preprocess just creates other
            // processes.
            throw (new NotImplementedException());
        }
        #endregion // IProcessor Interface Methods

        #region Private Methods
        /// <summary>
        /// Prebuild implementation.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="process"></param>
        /// <param name="processors"></param>
        /// <param name="targetContentTree"></param>
        /// <param name="syncDependencies"></param>
        /// <param name="resultantProcesses"></param>
        /// <returns></returns>
        private bool PrebuildImpl(IEngineParameters param, IProcess process, 
            IProcessorCollection processors, IContentTree targetContentTree, 
            out IEnumerable<IContentNode> syncDependencies,
            out IEnumerable<IProcess> resultantProcesses)
        {
            resultantProcesses = new IProcess[0];
            syncDependencies = new IContentNode[0];

            if (!ValidateProcess(param, process, processors))
                return (false);
            this.LoadParameters(param);

            bool result = true;
            ContentTreeHelper treeHelper = new ContentTreeHelper(targetContentTree, processors);
            IContentNode combineNode = process.Outputs.First();
            IContentNode combineDocumentNode = process.Outputs.ElementAt(1);

            IFilesystemNode combineOutput = (IFilesystemNode)(process.Outputs.First());
            IFilesystemNode combineDocumentOutput = (IFilesystemNode)(process.Outputs.ElementAt(1));
            
            String mapCacheDirectoryParamValue = (String)this.Parameters[Consts.MapProcess_SourceMapCacheDirectory];
            String mapCacheDirectory = SIO.Path.GetFullPath(param.Branch.Environment.Subst(mapCacheDirectoryParamValue));
            
            String combineZipName = SIO.Path.GetFileNameWithoutExtension(combineOutput.AbsolutePath);
            String assetCombineConfigFilename = SIO.Path.Combine(mapCacheDirectory,
                String.Format("asset_combine_{0}_config.xml", combineZipName));

            // Generate configuration data.
            if (!GenerateAssetCombineConfig(param, process, processors, mapCacheDirectory,
                assetCombineConfigFilename, combineZipName, combineDocumentOutput.AbsolutePath))
            {
                param.Log.ErrorCtx(LOG_CTX, "Asset Combine configuration data creation failed.");
                return (false);
            }

            // Run utility.
            if (!RunAssetCombineExecutable(param, mapCacheDirectory, assetCombineConfigFilename,
                combineZipName, combineDocumentOutput.AbsolutePath))
            {
                param.Log.ErrorCtx(LOG_CTX, "Asset Combine executable failed.");
                return (false);
            }

            // Load document to create resultant processes.
            AssetCombine.Document document = AssetCombine.Document.Load(combineDocumentOutput.AbsolutePath);
            AssetCombine.CustomRBSManager.Instance.CreateCustomRBSFiles(document.Map.CacheDirectory);
            AssetPackScheduleBuilder assetPackBuilder = new AssetPackScheduleBuilder();
            foreach (AssetCombine.DrawableDictionary dictionary in document.Map.DrawableDictionaries)
            {
                String dictionaryFilename = SIO.Path.Combine(document.Map.CacheDirectory, 
                    String.Format("{0}.idd.zip", dictionary.Name));
                AssetPackZipArchive dictZip = assetPackBuilder.AddZipArchive(dictionaryFilename);
                foreach (AssetCombine.DrawableDictionaryInput input in dictionary.Inputs)
                {
                    String path = SIO.Path.Combine(input.CacheDirectory, input.Name);
                    dictZip.AddDirectory(path, input.Name, "*.*");
                }

#if false 
                String customPathname = CustomRBSManager.Instance.GetCustomRBSPathname(dictionary.LOD, hasEmbeddedTextures);
                inputMap.Add(customPathname, "");

                String customFinishPathname = CustomRBSManager.Instance.GetCustomFinishRBSPathname(hasEmbeddedTextures);
                inputMap.Add(customFinishPathname, "");
#endif
            }
            foreach (AssetCombine.TextureDictionary dictionary in document.Map.TextureDictionaries)
            {
                String dictionaryFilename = SIO.Path.Combine(document.Map.CacheDirectory,
                    String.Format("{0}.itd.zip", dictionary.Name));
                AssetPackZipArchive dictZip = assetPackBuilder.AddZipArchive(dictionaryFilename);
                foreach (AssetCombine.TextureDictionaryInput input in dictionary.Inputs)
                {
                    String path = SIO.Path.Combine(input.CacheDirectory, input.Name);
                    dictZip.AddDirectory(path, input.Name, "*.*");
                }
            }

            ICollection<IProcess> outputProcesses = new List<IProcess>();
            IDictionary<IProcess, IEnumerable<IContentNode>> syncDeps = new Dictionary<IProcess, IEnumerable<IContentNode>>();

            String assetPackScheduleFilename = SIO.Path.Combine(mapCacheDirectory,
                String.Format("asset_pack_{0}_combine.xml", combineZipName));
            assetPackBuilder.Save(assetPackScheduleFilename);
            ProcessBuilder assetPackPb = new ProcessBuilder("RSG.Pipeline.Processor.Common.AssetPackProcessor",
                 processors, targetContentTree);
            assetPackPb.Inputs.Add(targetContentTree.CreateFile(assetPackScheduleFilename));

            // Queue our process builder objects.
            outputProcesses.Add(assetPackPb.ToProcess());
            
            // Assign outputs.
            process.State = ProcessState.Discard;
            resultantProcesses = outputProcesses;
            syncDependencies = new List<IContentNode>();
            
            return (result);
        }

        /// <summary>
        /// Validate the process for Asset Combine.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="process"></param>
        /// <param name="processors"></param>
        /// <returns></returns>
        private bool ValidateProcess(IEngineParameters param, IProcess process, 
            IProcessorCollection processors)
        {
            bool result = true;
            ContentTreeHelper treeHelper = new ContentTreeHelper(process.Inputs.First().Owner, processors);
            if (process.Inputs.Count() < 1)
            {
                param.Log.ErrorCtx(LOG_CTX, "Asset Combine PreProcess requires one or more inputs.");
                result = false;
            }

            // Verify we're not trying to do something odd with output.
            IContentNode combineOutput = treeHelper.GetCombineOutputForNodes(process.Inputs);
            if (null == combineOutput)
            {
                param.Log.ErrorCtx(LOG_CTX, "Asset Combine output node failure.  Mis-configured content-tree?");
                result = false;
            }
            if (combineOutput != process.Outputs.First())
            {
                param.Log.ErrorCtx(LOG_CTX, "Asset Combine output node must be first set output.");
                result = false;
            }

            return (result);
        }

        /// <summary>
        /// Create Asset Combine configuration XML.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="process"></param>
        /// <param name="processors"></param>
        /// <param name="mapCacheDirectory"></param>
        /// <param name="assetCombineConfigFilename"></param>
        /// <param name="combineZipName"></param>
        /// <param name="outputFilename"></param>
        /// <returns></returns>
        private bool GenerateAssetCombineConfig(IEngineParameters param,
            IProcess process, IProcessorCollection processors,
            String mapCacheDirectory, String assetCombineConfigFilename, 
            String combineZipName, String outputFilename)
        {
            // Build the config
            if (!SIO.Directory.Exists(mapCacheDirectory))
                SIO.Directory.CreateDirectory(mapCacheDirectory);

            IEnumerable<IContentNode> combineInputs = process.Inputs;
            ContentTreeHelper treeHelper = new ContentTreeHelper(combineInputs.First().Owner, processors);
            IFilesystemNode combineOutput = (IFilesystemNode)(process.Outputs.First());
            
            List<AssetCombineInput> assetCombineInputs = new List<AssetCombineInput>();
            foreach (Content.Asset combineInput in combineInputs.OfType<Content.Asset>())
            {
                String sceneType = treeHelper.GetSceneTypeForExportNode(combineInput);
                String zipPathname = combineInput.AbsolutePath;
                String sceneXmlPathname = SIO.Path.ChangeExtension(zipPathname, ".xml");
                assetCombineInputs.Add(new AssetCombineInput(sceneXmlPathname, zipPathname, sceneType));
            }

            AssetCombineOutput assetCombineOutput = new AssetCombineOutput(mapCacheDirectory, outputFilename);
            AssetCombineTask task = new AssetCombineTask(combineZipName, assetCombineInputs, assetCombineOutput);
            AssetCombineConfig assetCombineConfig = new AssetCombineConfig(new AssetCombineTask[1] { task });
            assetCombineConfig.Save(assetCombineConfigFilename);

            return (true);
        }

        /// <summary>
        /// Run Asset Combine executable with config; generating XML document specifying 
        /// assets to build.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="mapCacheDirectory"></param>
        /// <param name="assetCombineConfigFilename"></param>
        /// <param name="combineZipName"></param>
        /// <param name="outputFilename"></param>
        /// <returns></returns>
        private bool RunAssetCombineExecutable(IEngineParameters param, String mapCacheDirectory,
            String assetCombineConfigFilename, String combineZipName, String outputFilename)
        {
            bool result = true;

            // Create tool.
            XGE.ITool assetCombineTool = XGEFactory.GetMapProcessorTool(
                param.Log, param.Branch.Project.Config,
                XGEFactory.MapToolType.AssetCombineProcessor, "Asset Combine");
            String assetCombineExecutablePathname = assetCombineTool.Path;
            String assetCombineArguments = String.Format("--config {0}", assetCombineConfigFilename);

            // Run the combine processor
            Command assetCombineProcessorCommand = new Command(assetCombineExecutablePathname, assetCombineArguments);
            param.Log.MessageCtx(LOG_CTX, "Running '{0} {1}'", assetCombineExecutablePathname, assetCombineArguments);
            param.Log.MessageCtx(LOG_CTX, "Determining Asset Combine requirements for '{0}'", combineZipName);
            int assetCombineExitCode = assetCombineProcessorCommand.Run(param.Log);
            if (assetCombineExitCode != 0)
            {
                param.Log.ErrorCtx(LOG_CTX, "Asset Combine executable failed.");
                return (false);
            }

            return (result);
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Processor.Map namespace
