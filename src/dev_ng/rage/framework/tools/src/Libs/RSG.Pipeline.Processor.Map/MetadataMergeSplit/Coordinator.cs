﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using RSG.Pipeline.Content;
using RSG.Pipeline.Content.Algorithm;
using RSG.Pipeline.Core;
using RSG.Pipeline.Services.AssetPack;
using RSG.Pipeline.Services.Metadata;

using RSG.SceneXml;

namespace RSG.Pipeline.Processor.Map.MetadataMergeSplit
{
    internal class Coordinator
    {
        #region Singleton Implementation
        internal static Coordinator Instance
        {
            get
            {
                if (instance_ == null)
                    instance_ = new Coordinator();

                return instance_;
            }
        }

        private static Coordinator instance_;
        #endregion Singleton Implementation

        #region Constructor(s)
        private Coordinator()
        {
            processedMetadataMergeNodes_ = new List<IContentNode>();
            IsMetadataTargetRebuildRequired = false;
        }
        #endregion Constructor(s)

        #region Internal Properties
        internal bool IsMetadataTargetRebuildRequired { get; private set; }
        #endregion

        #region Internal Methods
        internal void Reset()
        {
            IsMetadataTargetRebuildRequired = false;

            processedMetadataMergeNodes_.Clear();
        }

        internal bool HasMergeNodeBeenConsidered(IContentNode metadataMergeNode)
        {
            return processedMetadataMergeNodes_.Contains(metadataMergeNode);
        }

        internal MergeSplitDetails GetMergeDetails(IContentNode metadataMergeNode, 
                                              IEngineParameters engineParameters, 
                                              ContentTreeHelper contentTreeHelper)
        {
            IEnumerable<IContentNode> inputSceneXmlNodes = contentTreeHelper.GetAllInputsForNode(metadataMergeNode);
            Content.File[] inputSceneXmlFileNodes = inputSceneXmlNodes.Cast<Content.File>().ToArray();
            List<Content.File> actualInputSceneXmlFileNodes = new List<Content.File>();

            // First we get all of the xml file dependencies from ref objects in the scenes we are going to process
            HashSet<IContentNode> refMaxFileNodesProcessed = new HashSet<IContentNode>();// use this cache to speed things up
            HashSet<IContentNode> sceneXmlNodeDependencies = new HashSet<IContentNode>();
            foreach (Content.File inputSceneXmlFileNode in inputSceneXmlFileNodes)
            {
				String sceneXmlFilename = inputSceneXmlFileNode.AbsolutePath;
                if (engineParameters.Branch.Project.IsDLC && !System.IO.File.Exists(inputSceneXmlFileNode.AbsolutePath))
                {
                    // For DLC we add the DLC SceneXml if it exists; otherwise fallback to core project.
                    sceneXmlFilename = inputSceneXmlFileNode.AbsolutePath.Replace(
                        engineParameters.Branch.Export.ToLower(), engineParameters.CoreBranch.Export.ToLower());
                    actualInputSceneXmlFileNodes.Add((Content.File)inputSceneXmlFileNode.Owner.CreateFile(sceneXmlFilename));
                }
                else
                {
                    // Default case; just add the current SceneXml filename.
                    actualInputSceneXmlFileNodes.Add(inputSceneXmlFileNode);
                }

                Scene scene = new Scene(contentTreeHelper.ContentTree, contentTreeHelper, sceneXmlFilename, RSG.SceneXml.LoadOptions.Objects, true);

                foreach (TargetObjectDef obj in scene.Walk(Scene.WalkMode.DepthFirst))
                {
                    if (obj.IsRefObject())
                    {
                        if (obj.RefFile == null)
                            continue;

                        IContentNode refMaxFileNode = contentTreeHelper.CreateFile(obj.RefFile);
                        if (refMaxFileNodesProcessed.Add(refMaxFileNode))
                        {
                            IContentNode exportZipNode = contentTreeHelper.GetExportZipNodeFromMaxFileNode(refMaxFileNode);
                            if (exportZipNode != null)
                            {
                                IContentNode sceneXmlNode = contentTreeHelper.GetSceneXmlNodeFromExportZipNode(exportZipNode);
                                if (sceneXmlNode != null)
                                {
                                    sceneXmlNodeDependencies.Add(sceneXmlNode);
                                }
                            }
                        }
                    }
                }
            }

            processedMetadataMergeNodes_.Add(metadataMergeNode);

            String mapExportAdditionsPathname = System.IO.Path.Combine(engineParameters.Branch.Project.Cache, engineParameters.Branch.Name, "manifest", metadataMergeNode.Name, "MapExportAdditions.xml");

            // If the map export additions pathname doesn't exist then we need are assuming that this is the
            // first rebuild of the merged metadata.  We set a flag here and force Rage and processed rebuild
            // in Map.PreProcess if this is set.
            if (!System.IO.File.Exists(mapExportAdditionsPathname))
                IsMetadataTargetRebuildRequired = true;

            return new MergeSplitDetails(metadataMergeNode, actualInputSceneXmlFileNodes, sceneXmlNodeDependencies, mapExportAdditionsPathname);
        }

        /// <summary>
        /// Gets all of the processes (Asset Pack and Rage) required for the merge
        /// </summary>
        internal IProcess[] BuildMergedContentProcesses(
            MergeSplitDetails mergeSplitDetails, Content.Directory metadataOutputDirectoryNode, String mapCacheDirectory, IEnumerable<MapMetadataSerialiserManifestDependency> additionalManifestDependencies,
            IProcessorCollection processors, IContentTree sourceOwner, IContentTree targetOwner, bool buildingProcessedZips)
        {
            List<IProcess> processes = new List<IProcess>();

            Debug.Assert(mergeSplitDetails.MetadataMergeSplitNode is Content.Asset);
            Content.Asset ouputAsset = (Content.Asset)mergeSplitDetails.MetadataMergeSplitNode;

            // Build the asset pack process
            if (buildingProcessedZips)
            {
                AssetPackScheduleBuilder assetPackScheduleBuilder = new AssetPackScheduleBuilder();
                AssetPackZipArchive outputZipArchive = assetPackScheduleBuilder.AddZipArchive(ouputAsset.AbsolutePath);
                outputZipArchive.AddDirectory(metadataOutputDirectoryNode.AbsolutePath);
                outputZipArchive.AddFile(mergeSplitDetails.MapExportAdditionsPathname);

                foreach (MapMetadataSerialiserManifestDependency additionalManifestDependency in additionalManifestDependencies)
                {
                    outputZipArchive.AddFile(additionalManifestDependency.IMAPPath);
                }

                String assetPackSchedulePathname = System.IO.Path.Combine(mapCacheDirectory, mergeSplitDetails.MetadataMergeSplitNode.Name + "_asset_pack_schedule.xml");
                assetPackScheduleBuilder.Save(assetPackSchedulePathname);

                ProcessBuilder assetPackProcessBuilder;
                CreateAssetPackProcessBuilder(processors, targetOwner, out assetPackProcessBuilder);
                IContentNode assetPackScheduleNode = targetOwner.CreateFile(assetPackSchedulePathname);
                assetPackProcessBuilder.Inputs.Add(assetPackScheduleNode);
                assetPackProcessBuilder.Outputs.Add(ouputAsset);

                processes.Add(assetPackProcessBuilder.ToProcess());
            }

            // Build the Rage process
            ProcessBuilder rageProcessBuilder;
            CreateRAGEProcessBuilder(processors, sourceOwner, targetOwner, ouputAsset, out rageProcessBuilder);
            rageProcessBuilder.Inputs.Add(metadataOutputDirectoryNode);

            foreach (MapMetadataSerialiserManifestDependency additionalManifestDependency in additionalManifestDependencies)
            {
                rageProcessBuilder.Inputs.Add(targetOwner.CreateFile(additionalManifestDependency.IMAPPath));
            }

            rageProcessBuilder.Parameters.Add(Core.Constants.RageProcess_DelayedDirectoryEvaluationParameterName, true);
            rageProcessBuilder.Parameters.Add(Core.Constants.ManifestProcess_MapExportAdditions, mergeSplitDetails.MapExportAdditionsPathname);
            processes.Add(rageProcessBuilder.ToProcess());

            return processes.ToArray();
        }
        #endregion Internal Methods

        #region Private Methods
        // TODO - this is duplicated in PreProcess.CreateAssetPackProcess
        private bool CreateAssetPackProcessBuilder(IProcessorCollection processors,
                                                   IContentTree owner,
                                                   out ProcessBuilder assetPackProcessBuilder)
        {
            IProcessor assetPackProcessor = processors.Where(p =>
                0 == String.Compare("RSG.Pipeline.Processor.Common.AssetPackProcessor", p.Name)).FirstOrDefault();
            Debug.Assert(null != assetPackProcessor, "Asset Pack processor not defined.");
            if (null == assetPackProcessor)
            {
                assetPackProcessBuilder = null;
                return false;
            }

            assetPackProcessBuilder = new ProcessBuilder(assetPackProcessor, owner);
            return true;
        }

        // TODO - this is duplicated in PreProcess.CreateRAGEProcessBuilder
        private bool CreateRAGEProcessBuilder(IProcessorCollection processors,
                                              IContentTree sourceOwner,
                                              IContentTree targetOwner,
                                              IContentNode processedNode,
                                              out ProcessBuilder rageProcessBuilder)
        {
            IProcessor rageProcessor = processors.FirstOrDefault(p =>
                String.Equals("RSG.Pipeline.Processor.Platform.Rage", p.Name));
            Debug.Assert(null != rageProcessor, "Rage processor not defined.");
            if (null == rageProcessor)
            {
                rageProcessBuilder = null;
                return false;
            }

            rageProcessBuilder = new ProcessBuilder(rageProcessor, targetOwner);
            foreach (IProcess rageProcessWithInput in sourceOwner.FindProcessesWithInput(processedNode).Where(p => p.ProcessorClassName.Equals(rageProcessor.Name)))
            {
                Debug.Assert(rageProcessWithInput.Outputs.Count() == 1);
                rageProcessBuilder.Outputs.Add(rageProcessWithInput.Outputs.First());
            }

            return true;
        }
        #endregion Private Methods

        #region Private Data
        List<IContentNode> processedMetadataMergeNodes_;
        #endregion Private Data
    }
}
