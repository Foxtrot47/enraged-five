﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using Ionic.Zip;
using RSG.Base.Extensions;
using RSG.Pipeline.Content;
using RSG.Pipeline.Content.Algorithm;
using RSG.Pipeline.Core;
using RSG.Pipeline.Services;
using RSG.Pipeline.Services.AssetPack;
using RSG.Pipeline.Services.Map.AssetCombine;
using RSG.SceneXml.MapExport;
using XGE = RSG.Interop.Incredibuild.XGE;

namespace RSG.Pipeline.Processor.Map.AssetCombine
{
    /// <summary>
    /// Responsible for understanding how drawable and texture dictionaries are combined.
    /// Understands complications such as the fact that many export->processed conversion may rely on the same combined data
    /// Caches data related to asset combination to improve performance
    /// </summary>
    public class Coordinator
    {
        #region Singleton Implementation
        public static Coordinator Instance
        {
            get
            {
                if (instance_ == null)
                    instance_ = new Coordinator();

                return instance_;
            }
        }

        private static Coordinator instance_;
        #endregion Singleton Implementation

        #region Constructor(s)
        private Coordinator()
        {
            cachedCombineDocuments_ = new Dictionary<IContentNode, Document>();
            inputNodeToCombineDocumentMap_ = new Dictionary<IContentNode, string>();
            nodesProcessesHaveBeenBuiltFor_ = new SortedSet<Guid>();
            pathnameToNodeMap_ = new Dictionary<string, IContentNode>();
            filesToExcludeMap_ = new Dictionary<IContentNode, List<string>>();
        }
        #endregion Constructor(s)

        #region Internal Methods

        public void Reset()
        {
            cachedCombineDocuments_.Clear();
            inputNodeToCombineDocumentMap_.Clear();
            nodesProcessesHaveBeenBuiltFor_.Clear();
            pathnameToNodeMap_.Clear();
            filesToExcludeMap_.Clear();
        }

        /// <summary>
        /// Takes an enumeration of export zips and builds any combined data required
        /// </summary>
        /// <returns>True if the combine processing was successful (or there was nothing to do)</returns>
        internal bool ConsiderInputs(IEngineParameters engineParameters, 
                                     IContentTree contentTree,
                                     ContentTreeHelper contentTreeHelper,
                                     IEnumerable<IContentNode> mapPreProcessInputs,
                                     String mapCacheDirectory,
                                     out String combineDocumentPathname)
        {
            combineDocumentPathname = null;

            // Find all the combine outputs
            IContentNode combineOutput = contentTreeHelper.GetCombineOutputForNodes(mapPreProcessInputs);

            // Process the combine output
            if (combineOutput != null)
            {
                // Is the data cached?
                if (cachedCombineDocuments_.ContainsKey(combineOutput))
                {
                    Debug.Assert(inputNodeToCombineDocumentMap_.ContainsKey(mapPreProcessInputs.First()));
                    combineDocumentPathname = inputNodeToCombineDocumentMap_[mapPreProcessInputs.First()];
                }
                else
                {
                    // Here we build add to an input node to combine document map
                    // It isn't trivial and it's required by the map metadata serialiser (and possibly others in future)
                    combineDocumentPathname = System.IO.Path.Combine(mapCacheDirectory, String.Format("asset_combine_{0}.meta", combineOutput.Name));
                    HashSet<IContentNode> combineInputs = contentTreeHelper.GetAllInputsForNode(combineOutput);
                    foreach (IContentNode combineInput in combineInputs)
                    {
                        inputNodeToCombineDocumentMap_.Add(combineInput, combineDocumentPathname);
                    }
                    
                    AssetCombine.Document combineDocument = null;
                    if (!RunAssetCombineProcess(engineParameters, contentTreeHelper, mapCacheDirectory, combineInputs, 
                                                combineOutput.Name, combineDocumentPathname, out combineDocument))
                        return false;

                    cachedCombineDocuments_.Add(combineOutput, combineDocument);
                    BuildDataFromDocument(engineParameters, contentTree, combineDocument);
                }
            }

            return true;
        }

        /// <summary>
        /// Get the pathnames that are being combined for a given export zip node
        /// </summary>
        internal String[] GetFilesMarkedForExclude(IContentNode exportZipNode)
        {
            if (filesToExcludeMap_.ContainsKey(exportZipNode))
            {
                return filesToExcludeMap_[exportZipNode].ToArray();
            }

            return new String[0];
        }

        /// <summary>
        /// Gets all of the processes (Asset Pack and Rage) required for the combine
        /// </summary>
        internal IProcess[] BuildMergedContentProcesses(IEngineParameters engineParameters, IProcessorCollection processors, IContentTree sourceOwner, IContentTree targetOwner)
        {
            List<IProcess> processes = new List<IProcess>();

            foreach (KeyValuePair<IContentNode, Document> outputNodeDocumentPair in cachedCombineDocuments_)
            {
                if (nodesProcessesHaveBeenBuiltFor_.Contains(outputNodeDocumentPair.Key.ID))
                    continue;// Only build processes once

                // Build the asset pack process
                Debug.Assert(outputNodeDocumentPair.Key is Content.Asset);
                Content.Asset ouputAsset = (Content.Asset)outputNodeDocumentPair.Key;
                Document combineDocument = outputNodeDocumentPair.Value;
                AssetPackScheduleBuilder assetPackScheduleBuilder = new AssetPackScheduleBuilder();
                AssetPackZipArchive outputZipArchive = assetPackScheduleBuilder.AddZipArchive(ouputAsset.AbsolutePath);
                foreach (DrawableDictionary drawableDictionary in combineDocument.Map.DrawableDictionaries)
                {
                    String drawableDictionaryPathname = System.IO.Path.Combine(combineDocument.Map.CacheDirectory, drawableDictionary.Name + ".idd.zip");
                    outputZipArchive.AddFile(drawableDictionaryPathname);
                }
                foreach (TextureDictionary textureDictionary in combineDocument.Map.TextureDictionaries)
                {
                    String textureDictionaryPathname = System.IO.Path.Combine(combineDocument.Map.CacheDirectory, textureDictionary.Name + ".itd.zip");
                    outputZipArchive.AddFile(textureDictionaryPathname);
                }

                String assetPackSchedulePathname = System.IO.Path.Combine(combineDocument.Map.CacheDirectory, "asset_pack_schedule.xml");
                assetPackScheduleBuilder.Save(assetPackSchedulePathname);

                ProcessBuilder assetPackProcessBuilder;
                CreateAssetPackProcessBuilder(processors, targetOwner, out assetPackProcessBuilder);
                IContentNode assetPackScheduleNode = targetOwner.CreateFile(assetPackSchedulePathname);
                assetPackProcessBuilder.Inputs.Add(assetPackScheduleNode);
                // TEMP - disabled processed zip creation because of packed TIF sizes
                //if(buildingProcessedZips)
                //    processes.Add(assetPackProcessBuilder.ToProcess());

                // Build the Rage process
                ProcessBuilder rageProcessBuilder;
                CreateRAGEProcessBuilder(processors, sourceOwner, targetOwner, ouputAsset, out rageProcessBuilder);
                foreach (DrawableDictionary drawableDictionary in combineDocument.Map.DrawableDictionaries)
                {
                    if (engineParameters.Branch.Project.IsDLC && !drawableDictionary.IsNewDlc)
                        continue;

                    String drawableDictionaryPathname = System.IO.Path.Combine(combineDocument.Map.CacheDirectory, drawableDictionary.Name + ".idd.zip");
                    IContentNode drawableDictionaryNode = targetOwner.CreateFile(drawableDictionaryPathname);
                    rageProcessBuilder.Inputs.Add(drawableDictionaryNode);
                }
                foreach (TextureDictionary textureDictionary in combineDocument.Map.TextureDictionaries)
                {
                    if (engineParameters.Branch.Project.IsDLC && (!textureDictionary.IsNewDlc || (textureDictionary.IsNewDlc && !textureDictionary.IsNewTxd)))
                        continue;

                    String textureDictionaryPathname = System.IO.Path.Combine(combineDocument.Map.CacheDirectory, textureDictionary.Name + ".itd.zip");
                    IContentNode textureDictionaryNode = targetOwner.CreateFile(textureDictionaryPathname);
                    rageProcessBuilder.Inputs.Add(textureDictionaryNode);
                }

                // We only add the process if we have defined inputs; for new districts
                // we may not have any LOD/SLOD objects to merge so this will have
                // no inputs.
                if (rageProcessBuilder.Inputs.Count > 0)
                    processes.Add(rageProcessBuilder.ToProcess());

                nodesProcessesHaveBeenBuiltFor_.Add(outputNodeDocumentPair.Key.ID);// Only build processes once
            }

            return processes.ToArray();
        }
        #endregion Internal Methods

        #region Private Methods
        private bool RunAssetCombineProcess(IEngineParameters engineParameters, 
                                            ContentTreeHelper contentTreeHelper,
                                            String mapCacheDirectory, 
                                            HashSet<IContentNode> combineInputs, 
                                            String combinedZipName, 
                                            String assetCombineOutputPathname,
                                            out Document document)
        {
            // Default assignment for out parameters
            document = null;

            // Build the config
            if (!System.IO.Directory.Exists(mapCacheDirectory))
                System.IO.Directory.CreateDirectory(mapCacheDirectory);

            List<AssetCombineConfigInput> assetCombineInputs = new List<AssetCombineConfigInput>();
            foreach (Content.Asset combineInput in combineInputs.OfType<Content.Asset>())
            {
                if (engineParameters.Branch.Project.IsDLC && System.IO.File.Exists(combineInput.AbsolutePath))
                {
                    // DLC.
                    String sceneType = contentTreeHelper.GetSceneTypeForExportNode(combineInput);
                    String zipPathname = combineInput.AbsolutePath;
                    String sceneXmlPathname = System.IO.Path.ChangeExtension(zipPathname, ".xml");
                    assetCombineInputs.Add(new AssetCombineConfigInput(sceneXmlPathname, zipPathname, sceneType, true));
                }

                // Always add core for combine.
                String coreSceneType = contentTreeHelper.GetSceneTypeForExportNode(combineInput);
                String coreZipPathname = combineInput.AbsolutePath.Replace(engineParameters.Branch.Export, engineParameters.CoreBranch.Export, StringComparison.OrdinalIgnoreCase);
                String coreSceneXmlPathname = System.IO.Path.ChangeExtension(coreZipPathname, ".xml");
                assetCombineInputs.Add(new AssetCombineConfigInput(coreSceneXmlPathname, coreZipPathname, coreSceneType, false));
            }

#warning Flo/DHM/DLC Hack/Whatever: force syncing dependencies for DLC as we MIGHT need files not on disc yet
            using (AssetCombineSyncer dlcSyncer = new AssetCombineSyncer(engineParameters))
            {
                if (engineParameters.Branch.Project.IsDLC)
                {
                    IEnumerable<string> sceneXmlDependencies = assetCombineInputs.Select(input => input.SceneXmlPathname).Distinct().ToList();
                    if (sceneXmlDependencies.Any())
                    {
                        dlcSyncer.SyncDependencies(engineParameters, sceneXmlDependencies);
                    }

                    IEnumerable<string> zipDependencies = assetCombineInputs.Select(input => input.ZipPathname).Distinct().ToList();
                    if (zipDependencies.Any())
                    {
                        dlcSyncer.SyncDependencies(engineParameters, zipDependencies);
                    }
                }
            }

            String assetCombineConfigPathname = System.IO.Path.Combine(mapCacheDirectory, String.Format("asset_combine_{0}_config.meta", combinedZipName));
            AssetCombineConfigOutput assetCombineOutput = new AssetCombineConfigOutput(mapCacheDirectory, assetCombineOutputPathname);

            AssetCombineConfigTask task = new AssetCombineConfigTask(combinedZipName, assetCombineInputs, assetCombineOutput);
            AssetCombineConfig assetCombineConfig = new AssetCombineConfig(new AssetCombineConfigTask[1] { task });
            assetCombineConfig.Save(assetCombineConfigPathname);

            // Define important tokens
            XGE.ITool assetCombineTool = XGEFactory.GetMapProcessorTool(
                engineParameters.Log, engineParameters.Branch.Project.Config,
                XGEFactory.MapToolType.AssetCombineProcessor, "Asset Combine");
            String assetCombineExecutablePathname = assetCombineTool.Path;
            StringBuilder assetCombineArgumentsSb = new StringBuilder();
            assetCombineArgumentsSb.AppendFormat("--branch {0} ", engineParameters.Branch.Name);
            
            assetCombineArgumentsSb.AppendFormat("--config {0} ", assetCombineConfigPathname);
            if (engineParameters.Branch.Project.IsDLC)
                assetCombineArgumentsSb.AppendFormat("--dlc {0}", engineParameters.Branch.Project.Name);
            
            // Run the combine processor
            Command assetCombineProcessorCommand = new Command(assetCombineExecutablePathname, assetCombineArgumentsSb.ToString());
            engineParameters.Log.Message("Running '{0} {1}'", assetCombineExecutablePathname, assetCombineArgumentsSb.ToString());
            engineParameters.Log.Message("Determining Asset Combine requirements for '{0}'", combinedZipName);
            int assetCombineExitCode = assetCombineProcessorCommand.Run(engineParameters.Log);
            if (assetCombineExitCode != 0)
                return false;

            // Load the XML file and process it
            document = AssetCombine.Document.Load(assetCombineOutputPathname);

            return true;
        }




        private void BuildDataFromDocument(IEngineParameters engineParameters, IContentTree contentTree, AssetCombine.Document combineDocument)
        {
            CustomRBSManager.Instance.CreateCustomRBSFiles(combineDocument.Map.CacheDirectory);

            foreach (DrawableDictionary drawableDictionary in combineDocument.Map.DrawableDictionaries)
            {
                if(engineParameters.Branch.Project.IsDLC && !drawableDictionary.IsNewDlc)
                    continue;

                BuildDrawableDictionary(drawableDictionary,
                    combineDocument.Map.CacheDirectory,
                    engineParameters.Log,
                    engineParameters.Flags.HasFlag(EngineFlags.Rebuild),
                    contentTree);
            }
            foreach (TextureDictionary textureDictionary in combineDocument.Map.TextureDictionaries)
            {
                // 
                if (engineParameters.Branch.Project.IsDLC && (!textureDictionary.IsNewDlc || (textureDictionary.IsNewDlc && !textureDictionary.IsNewTxd)))
                    continue;

                BuildTextureDictionary(textureDictionary,
                    combineDocument.Map.CacheDirectory,
                    engineParameters.Log,
                    engineParameters.Flags.HasFlag(EngineFlags.Rebuild),
                    contentTree);
            }
        }

        // !WARNING!
        // This method is called from within a Parallel.ForEach loop.
        private void BuildDrawableDictionary(DrawableDictionary drawableDictionary, 
                                             String cacheDirectory, 
                                             Base.Logging.Universal.IUniversalLog log,
                                             bool rebuild,
                                             IContentTree contentTree)
        {
            // Determine whether or not we need to build
            String drawableDictionaryPathname = System.IO.Path.Combine(cacheDirectory, drawableDictionary.Name + ".idd.zip");

            bool buildRequired = (rebuild || BuildRequired(drawableDictionary, drawableDictionaryPathname));

            foreach (AssetCombine.DrawableDictionaryInput input in drawableDictionary.Inputs)
            {
                String fileToExclude = input.Name + ".idr.zip";

                // To prevent unnecessary calls to IContentTree.CreateFile we keep a local pathname to node map
                IContentNode sourceZipNode;
                if (pathnameToNodeMap_.ContainsKey(input.SourceZipPathname))
                {
                    sourceZipNode = pathnameToNodeMap_[input.SourceZipPathname];
                }
                else
                {
                    sourceZipNode = contentTree.CreateFile(input.SourceZipPathname);

                    pathnameToNodeMap_.Add(input.SourceZipPathname, sourceZipNode);
                }

                if (!filesToExcludeMap_.ContainsKey(sourceZipNode))
                {
                    filesToExcludeMap_.Add(sourceZipNode, new List<String>());
                }

                filesToExcludeMap_[sourceZipNode].Add(fileToExclude);
            }

            // DHM TODO: this could be done in memory?! 
            if (buildRequired)
            {
                log.Message("Building Drawable Dictionary '{0}'", drawableDictionary.Name);

                string drawableDictionaryDirectory = System.IO.Path.GetDirectoryName(drawableDictionaryPathname);
                if (!System.IO.Directory.Exists(drawableDictionaryDirectory))
                    System.IO.Directory.CreateDirectory(drawableDictionaryDirectory);
                Dictionary<String, String> inputMap = new Dictionary<String, String>();
                bool hasEmbeddedTextures = false;

                foreach (AssetCombine.DrawableDictionaryInput input in drawableDictionary.Inputs)
                {
                    String inputFilename = input.Name + ".idr.zip";
                    String inputPathname = System.IO.Path.Combine(input.CacheDirectory, inputFilename);
                    String drawableDirectory = input.Name;
                    if (input.IsNewDLC)
                        drawableDirectory = MapAsset.AppendMapPrefixIfRequired(contentTree.Branch.Project, input.Name);

                    using (ZipFile sourceZipFile = ZipFile.Read(input.SourceZipPathname))
                    {
                        ZipEntry sourceZipEntry = sourceZipFile.Entries.FirstOrDefault(
                            entry => String.Equals(entry.FileName, inputFilename, StringComparison.OrdinalIgnoreCase));
                        if (sourceZipEntry != null)
                        {
                            if (!System.IO.Directory.Exists(input.CacheDirectory))
                                System.IO.Directory.CreateDirectory(input.CacheDirectory);
                            sourceZipEntry.Extract(input.CacheDirectory, ExtractExistingFileAction.OverwriteSilently);

                            String drawableExtractDirectory = System.IO.Path.Combine(cacheDirectory, drawableDictionary.Name, input.Name);
                            if (!System.IO.Directory.Exists(drawableExtractDirectory))
                                System.IO.Directory.CreateDirectory(drawableExtractDirectory);

                            using (ZipFile drawableZipFile = ZipFile.Read(inputPathname))
                            {
                                foreach (ZipEntry drawableZipEntry in drawableZipFile)
                                {
                                    drawableZipEntry.Extract(drawableExtractDirectory, ExtractExistingFileAction.OverwriteSilently);
                                    string drFileExtension = Path.GetExtension(drawableZipEntry.FileName).ToLower();
                                    if ( drFileExtension== ".dds" || drFileExtension == ".tif")
                                    {
                                        hasEmbeddedTextures = true;
                                    }

#warning Flo "patching" TCLs on the fly to replace ${RS_ASSETS} with $(assets) to avoid Ragebuilder fucking up everything at resource stage
                                    if (drFileExtension == ".tcl")
                                    {
                                        string tclFilepath = Path.Combine(drawableExtractDirectory, drawableZipEntry.FileName);
                                        PatchExtractedTCL(tclFilepath, input.IsNewDLC);
                                    }

                                    string extractedCacheFilePathname = Path.Combine(drawableExtractDirectory, drawableZipEntry.FileName);
                                    inputMap.Add(extractedCacheFilePathname, drawableDirectory);
                                }
                            }
                        }
                        else
                        {
                            log.Error("Input map zip '{0}' does not include required drawable '{1}'.", input.SourceZipPathname, inputFilename);
                        }
                    }
                }

                String customPathname = CustomRBSManager.Instance.GetCustomRBSPathname(drawableDictionary.LOD, hasEmbeddedTextures);
                inputMap.Add(customPathname, "");

                String customFinishPathname = CustomRBSManager.Instance.GetCustomFinishRBSPathname(hasEmbeddedTextures);
                inputMap.Add(customFinishPathname, "");

                if (System.IO.File.Exists(drawableDictionaryPathname))
                    System.IO.File.Delete(drawableDictionaryPathname);

                using (ZipFile drawableDictionaryZipFile = new ZipFile(drawableDictionaryPathname))
                {
                    foreach (KeyValuePair<String, String> inputPair in inputMap)
                    {
                        drawableDictionaryZipFile.AddFile(inputPair.Key, inputPair.Value);
                    }
                    drawableDictionaryZipFile.Save();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tclFilepath"></param>
        /// Flo: we have to deal with ${RS_ASSETS} data in DLC files. Those files need to be patched before AssetCombine happens,
        /// because Ragebuilder considers ${RS_ASSETS} as $(core_assets) while we consider it as $(assets) in the pipeline
        private void PatchExtractedTCL(string tclFilepath, bool isNewDLC)
        {
            if (isNewDLC)
                return;

            // open, read all, patch lines, rewrite extracted file. Simple.
            string[] allLines = System.IO.File.ReadAllLines(tclFilepath);

            for (int i = 0; i < allLines.Length; i++)
            {
                // If the file is in DLC, leave.
                // If the file comes from core, replace.
                // Replace RS_ASSETS
                allLines[i] = allLines[i].Replace("$(assets)", "$(core_assets)", StringComparison.OrdinalIgnoreCase);
                allLines[i] = allLines[i].Replace("${RS_ASSETS}", "$(core_assets)", StringComparison.OrdinalIgnoreCase);
            }

            // hand crafted "WriteAllLines" because WriteAllLines() method hads a CRLF at the end of each line, 
            // meaning we'll have a empty line at the end; and I don't want to mess with TCLs (more than that)
            System.IO.File.Delete(tclFilepath);
            using (var stream = System.IO.File.OpenWrite(tclFilepath))
            {
                using (StreamWriter writer = new StreamWriter(stream))
                {
                    if (allLines.Length > 0)
                    {
                        for (int i = 0; i < allLines.Length - 1; i++)
                        {
                            writer.WriteLine(allLines[i]);
                        }
                        writer.Write(allLines[allLines.Length - 1]);
                    }
                }
            }
        }

        // TODO - we need to factor out common parts of AssetCombine.DrawableDictionary and AssetCombine.DrawableDictionaryInput to avoid duplication here
        private static bool BuildRequired(AssetCombine.DrawableDictionary drawableDictionary, String drawableDictionaryPathname)
        {
            if (!System.IO.File.Exists(drawableDictionaryPathname))
                return true;

            System.IO.FileInfo targetFileInfo = new System.IO.FileInfo(drawableDictionaryPathname);
            foreach (AssetCombine.DrawableDictionaryInput input in drawableDictionary.Inputs)
            {
                if (!System.IO.File.Exists(input.SourceZipPathname))
                    return true;

                System.IO.FileInfo sourceFileInfo = new System.IO.FileInfo(input.SourceZipPathname);
                if (sourceFileInfo.LastWriteTimeUtc > targetFileInfo.LastWriteTimeUtc)
                    return true;
            }

            return false;
        }

        // !WARNING!
        // This method is called from within a Parallel.ForEach loop.
        private void BuildTextureDictionary(AssetCombine.TextureDictionary textureDictionary,
                                            String cacheDirectory,
                                            Base.Logging.Universal.IUniversalLog log,
                                            bool rebuild,
                                            IContentTree contentTree)
        {
            string textureDictionaryPathname = System.IO.Path.Combine(cacheDirectory, textureDictionary.Name + ".itd.zip");

            bool buildRequired = (rebuild || BuildRequired(textureDictionary, textureDictionaryPathname));

            foreach (AssetCombine.TextureDictionaryInput input in textureDictionary.Inputs)
            {
                String fileToExclude = input.Name + ".itd.zip";

                // To prevent unnecessary calls to IContentTree.CreateFile we keep a local pathname to node map
                IContentNode sourceZipNode;
                if (pathnameToNodeMap_.ContainsKey(input.SourceZipPathname))
                {
                    sourceZipNode = pathnameToNodeMap_[input.SourceZipPathname];
                }
                else
                {
                    sourceZipNode = contentTree.CreateFile(input.SourceZipPathname);
                    pathnameToNodeMap_.Add(input.SourceZipPathname, sourceZipNode);
                }

                if (!filesToExcludeMap_.ContainsKey(sourceZipNode))
                {
                    filesToExcludeMap_.Add(sourceZipNode, new List<String>());
                }

                filesToExcludeMap_[sourceZipNode].Add(fileToExclude);
            }

            if (buildRequired)
            {
                log.Message("Building Texture Dictionary '{0}'", textureDictionary.Name);

                string textureDictionaryDirectory = System.IO.Path.GetDirectoryName(textureDictionaryPathname);
                if (!System.IO.Directory.Exists(textureDictionaryDirectory))
                    System.IO.Directory.CreateDirectory(textureDictionaryDirectory);

                List<String> inputPathnames = new List<String>();
                SortedSet<String> inputFilenames = new SortedSet<String>();
                foreach (AssetCombine.TextureDictionaryInput input in textureDictionary.Inputs)
                {
                    String inputFilename = input.Name + ".itd.zip";
                    using (ZipFile sourceZipFile = ZipFile.Read(input.SourceZipPathname))
                    {
                        ZipEntry sourceZipEntry = sourceZipFile.Entries.FirstOrDefault(entry => entry.FileName.Equals(inputFilename, StringComparison.OrdinalIgnoreCase));
                        if (sourceZipEntry != null)
                        {
                            if (!System.IO.Directory.Exists(input.CacheDirectory))
                                System.IO.Directory.CreateDirectory(input.CacheDirectory);
                            sourceZipEntry.Extract(input.CacheDirectory, ExtractExistingFileAction.OverwriteSilently);
                            String extractedInputPathname = System.IO.Path.Combine(input.CacheDirectory, inputFilename);

                            using (ZipFile inputZipFile = ZipFile.Read(extractedInputPathname))
                            {
                                foreach (ZipEntry inputZipEntry in inputZipFile)
                                {
                                    // Is this item required?
                                    // We use String.StartsWith() here to ensure that partial matches are included (required for alpha textures)
                                    string sourceBasename = System.IO.Path.GetFileNameWithoutExtension(inputZipEntry.FileName);
                                    bool sourceIsRequired = !input.RequiredItems.Any();// special case - take everything
                                    foreach (String requiredItem in input.RequiredItems)
                                        sourceIsRequired |= sourceBasename.ToLower().StartsWith(requiredItem.ToLower());

                                    if (sourceIsRequired)
                                    {
                                        inputZipEntry.Extract(input.CacheDirectory, ExtractExistingFileAction.OverwriteSilently);
                                        string extractedCacheFilePathname = System.IO.Path.Combine(input.CacheDirectory, inputZipEntry.FileName);
                                        if (!inputFilenames.Contains(inputZipEntry.FileName.ToLower()))// prevent duplicate files being added
                                        {
                                            inputPathnames.Add(extractedCacheFilePathname);
                                            inputFilenames.Add(inputZipEntry.FileName.ToLower());
                                        }

#warning Flo "patching" TCLs on the fly to replace ${RS_ASSETS} with $(core_assets) to make combined core assets from current gen point to their actual path
                                        string drFileExtension = Path.GetExtension(extractedCacheFilePathname).ToLower();
                                        if (drFileExtension == ".tcl")
                                        {
                                            PatchExtractedTCL(extractedCacheFilePathname, input.IsNewDLC);
                                        }
                                    }
                                    else
                                    {
                                        log.Message("Didn't take '{0}' as it isn't required", inputZipEntry.FileName);
                                    }
                                }
                            }
                        }
                        else
                        {
                            log.Warning("Input map zip '{0}' does not include required texture dictionary '{1}'.", input.SourceZipPathname, inputFilename);
                        }
                    }
                }

                if (System.IO.File.Exists(textureDictionaryPathname))
                    System.IO.File.Delete(textureDictionaryPathname);

                using (ZipFile mergedTextureDictionaryZipFile = new ZipFile(textureDictionaryPathname))
                {
                    foreach (String inputPathname in inputPathnames)
                    {
                        mergedTextureDictionaryZipFile.AddFile(inputPathname, "");
                    }
                    mergedTextureDictionaryZipFile.Save();
                }
            }
        }

        // TODO - we need to factor out common parts of AssetCombine.TextureDictionary and AssetCombine.TextureDictionaryInput to avoid duplication here
        private static bool BuildRequired(AssetCombine.TextureDictionary textureDictionary, String textureDictionaryPathname)
        {
            if (!System.IO.File.Exists(textureDictionaryPathname))
                return true;

            System.IO.FileInfo targetFileInfo = new System.IO.FileInfo(textureDictionaryPathname);
            foreach (AssetCombine.TextureDictionaryInput input in textureDictionary.Inputs)
            {
                if (!System.IO.File.Exists(input.SourceZipPathname))
                    return true;

                System.IO.FileInfo sourceFileInfo = new System.IO.FileInfo(input.SourceZipPathname);
                if (sourceFileInfo.LastWriteTimeUtc > targetFileInfo.LastWriteTimeUtc)
                    return true;
            }

            return false;
        }

        // TODO - this is duplicated in PreProcess.CreateAssetPackProcess
        private bool CreateAssetPackProcessBuilder(IProcessorCollection processors, 
                                                   IContentTree owner,
                                                   out ProcessBuilder assetPackProcessBuilder)
        {
            IProcessor assetPackProcessor = processors.FirstOrDefault(p => p.Name == "RSG.Pipeline.Processor.Common.AssetPackProcessor");
            Debug.Assert(null != assetPackProcessor, "Asset Pack processor not defined.");
            if (null == assetPackProcessor)
            {
                assetPackProcessBuilder = null;
                return false;
            }

            assetPackProcessBuilder = new ProcessBuilder(assetPackProcessor, owner);
            return true;
        }

        // TODO - this is duplicated in PreProcess.CreateRAGEProcessBuilder
        private bool CreateRAGEProcessBuilder(IProcessorCollection processors, 
                                              IContentTree sourceOwner, 
                                              IContentTree targetOwner, 
                                              IContentNode processedNode, 
                                              out ProcessBuilder rageProcessBuilder)
        {
            IProcessor rageProcessor = processors.FirstOrDefault(p => p.Name == "RSG.Pipeline.Processor.Platform.Rage");
            Debug.Assert(null != rageProcessor, "Rage processor not defined.");
            if (null == rageProcessor)
            {
                rageProcessBuilder = null;
                return false;
            }

            rageProcessBuilder = new ProcessBuilder(rageProcessor, targetOwner);
            foreach (IProcess rageProcessWithInput in sourceOwner.FindProcessesWithInput(processedNode).Where(p => p.ProcessorClassName.Equals(rageProcessor.Name)))
            {
                Debug.Assert(rageProcessWithInput.Outputs.Count() == 1);
                rageProcessBuilder.Outputs.Add(rageProcessWithInput.Outputs.First());
            }

            return true;
        }
        #endregion Private Methods

        #region Private Data

        // Map of processed zip node to cached asset combine document
        private Dictionary<IContentNode, Document> cachedCombineDocuments_;

        // Map of export zip node to asset combine documnt pathname
        private Dictionary<IContentNode, String> inputNodeToCombineDocumentMap_;

        // Set of guids for processed zip nodes that combine processes (Asset Pack and Rage) have been built for
        private SortedSet<Guid> nodesProcessesHaveBeenBuiltFor_;

        // Map of combine asset input (e.g. IDR) to content node for extra-quick lookup
        private Dictionary<String, IContentNode> pathnameToNodeMap_;

        // Map of pathnames of files to exclude for each processed zip node
        private Dictionary<IContentNode, List<String>> filesToExcludeMap_;

        #endregion Private Data
    }
}
