﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

using RSG.Pipeline.Content;
using RSG.Pipeline.Content.Algorithm;
using RSG.Pipeline.Core;
using RSG.Pipeline.Services;

namespace RSG.Pipeline.Processor.Map.InterContainerLODDependencies
{
    internal class Coordinator
    {
        #region Singleton Implementation
        internal static Coordinator Instance
        {
            get
            {
                if (instance_ == null)
                    instance_ = new Coordinator();

                return instance_;
            }
        }

        private static Coordinator instance_;
        #endregion Singleton Implementation

        #region Constructor(s)
        private Coordinator()
        {
            processesConsidered_ = new HashSet<IProcess>();
            processesAddedAsDependencies_ = new HashSet<IProcess>();
            processDependencyMap_ = new Dictionary<IProcess, HashSet<IProcess>>();
        }
        #endregion Constructor(s)

        #region Internal Methods

        internal void Reset()
        {
            processesConsidered_.Clear();
            processesAddedAsDependencies_.Clear();
            processDependencyMap_.Clear();
        }

        internal bool HasProcessBeenConsidered(IProcess process)
        {
            return processesConsidered_.Contains(process);
        }

        internal bool ProcessHasDependencies(IEngineParameters engineParameters,
                                             ContentTreeHelper contentTreeHelper,
                                             IProcess process)
        {
            processesConsidered_.Add(process);// Make a note to prevent any processes being processed twice

            if (processesAddedAsDependencies_.Contains(process))
                return false;

            IProcess[] siblingPreProcesses = contentTreeHelper.GetMapPreProcessCombineSiblings(process);
            if (siblingPreProcesses.Any())
            {
                processDependencyMap_.Add(process, new HashSet<IProcess>());
                foreach (IProcess siblingPreProcess in siblingPreProcesses)
                {
                    if (!processesConsidered_.Contains(siblingPreProcess) && !processesAddedAsDependencies_.Contains(siblingPreProcess))
                    {
                        processesAddedAsDependencies_.Add(siblingPreProcess);
                        processDependencyMap_[process].Add(siblingPreProcess);
                    }
                }
            }

            return siblingPreProcesses.Any();
        }

        internal IProcess[] GetDependentProcesses(IProcess process)
        {
            Debug.Assert(processDependencyMap_.ContainsKey(process));

            return processDependencyMap_[process].ToArray();
        }
        #endregion Internal Methods

        #region Private Methods
        #endregion Private Methods

        #region Private Data
        private HashSet<IProcess> processesConsidered_;
        private HashSet<IProcess> processesAddedAsDependencies_;
        private Dictionary<IProcess, HashSet<IProcess>> processDependencyMap_;
        #endregion Private Data
    }
}
