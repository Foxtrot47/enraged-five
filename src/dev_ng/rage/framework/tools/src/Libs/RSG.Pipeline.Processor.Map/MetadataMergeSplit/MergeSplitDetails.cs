﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Pipeline.Core;

namespace RSG.Pipeline.Processor.Map.MetadataMergeSplit
{
    internal class MergeSplitDetails
    {
        internal MergeSplitDetails(IContentNode metadataMergeSplitNode,
                              IEnumerable<IContentNode> inputSceneXmlNodes,
                              IEnumerable<IContentNode> sceneXmlNodeDependencies,
                              String mapExportAdditionsPathname)
        {
            MetadataMergeSplitNode = metadataMergeSplitNode;
            InputSceneXmlNodes = inputSceneXmlNodes.ToArray();
            SceneXmlNodeDependencies = sceneXmlNodeDependencies.ToArray();
            MapExportAdditionsPathname = mapExportAdditionsPathname;
        }

        internal IContentNode MetadataMergeSplitNode { get; private set; }
        internal IContentNode[] InputSceneXmlNodes { get; private set; }
        internal IContentNode[] SceneXmlNodeDependencies { get; private set; }
        internal String MapExportAdditionsPathname { get; private set; }
    }
}
