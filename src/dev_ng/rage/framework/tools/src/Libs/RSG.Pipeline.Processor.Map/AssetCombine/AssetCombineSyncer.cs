﻿using System;
using System.Collections.Generic;
using System.Linq;
using P4API;
using RSG.Base.Extensions;
using RSG.Pipeline.Core;
using RSG.SourceControl.Perforce;

namespace RSG.Pipeline.Processor.Map.AssetCombine
{
    /// <summary>
    /// Flo:
    /// This whole class is an implementation of the CopyAndPaste pattern from Engine.cs
    /// for AssetCombine Coordinator to force sync file before the engine does it for DLC core dependencies prior to combine.
    /// I feel dirty.
    /// </summary>
    internal class AssetCombineSyncer : IDisposable
    {
        private const string LOG_CTX = "AssetCombine Coordinator";

        private readonly P4 p4;

        public AssetCombineSyncer(IEngineParameters param)
        {
            p4 = param.Branch.Project.SCMConnect();
        }

        /// <summary>
        /// Sync dependencies for the process list; ensuring that we handle
        /// process dependencies and sync in reverse order.
        /// </summary>
        /// <param name="param">Engine parameters for current build.</param>
        /// <param name="syncDependencies">dependencies files to sync.</param>
        public bool SyncDependencies(IEngineParameters param, IEnumerable<string> syncDependencies)
        {
            bool result = true;

            param.Log.ProfileCtx(LOG_CTX, "Syncing DLC dependencies for AssetCombine.");

            if (p4 == null)
            {
                // TODO Flo: whenever SCMConnect() is droppped (it's marked as Obsolete), make sure this is still relevant, and update accordingly
                param.Log.ErrorCtx(LOG_CTX, "P4 object was null. Connection is not valid.");
                return true;
            }

            // Calculate recursive dependencies; we need this information
            // to control how we sync dependencies to ensure their modified
            // times do not affect their build required state.  We sync
            // dependencies in reverse order; grouping by processor.
            try
            {
                var fileDependencies = syncDependencies;
                var dirDependencies = new string[0];

                // This uses Perforce where (FileMapping) to determine the
                // Perforce server location of the files; we should use depot
                // paths as they will sync even if the user doesn't have
                // those directories locally (local path wouldn't sync!).
                FileMapping[] fileMappings = new FileMapping[] {};
                if (fileDependencies.Any())
                {
                    fileMappings = FileMapping.Create(p4, fileDependencies.ToArray());
                }

                IEnumerable<FileMapping> missingFiles = fileMappings.Where(fm => !fm.Mapped);
                if (missingFiles.Any())
                {
                    param.Log.ErrorCtx(LOG_CTX, "There are build dependency files that are not mapped into your Perforce workspace:");
                    missingFiles.ForEachWithIndex((fm, index) => param.Log.ErrorCtx(LOG_CTX, "\t{0}: {1}", index, fm.DepotFilename));
                    result = false;
                }
                fileMappings = fileMappings.Where(fm => fm.Mapped).ToArray();

                FileMapping[] dirMappings = new FileMapping[] {};
                if (dirDependencies.Any())
                {
                    dirMappings = FileMapping.Create(p4, dirDependencies.ToArray());
                }

                IEnumerable<FileMapping> missingDirs = dirMappings.Where(fm => !fm.Mapped);
                if (missingDirs.Any())
                {
                    param.Log.ErrorCtx(LOG_CTX, "There are build dependency directories that are not mapped into your Perforce workspace:");
                    missingDirs.ForEachWithIndex((fm, index) => param.Log.ErrorCtx(LOG_CTX, "\t{0}: {1}", index.ToString(), fm.DepotFilename));
                    result = false;
                }

                dirMappings = dirMappings.Where(fm => fm.Mapped).ToArray();

                // Determine what needs syncing and perform the syncs.
                result &= TrySyncPerforceFiles(param, p4, fileMappings.Select(fm => fm.DepotFilename));
                result &= TrySyncPerforceFiles(param, p4, dirMappings.Select(fm => fm.DepotFilename));
            }
            catch (P4API.Exceptions.PerforceInitializationError ex)
            {
                param.Log.ToolExceptionCtx(LOG_CTX, ex, "Perforce connection error: {0} as {1}, client: {2}, current directory: {3}",
                    p4.Port, p4.User, p4.Client, System.IO.Directory.GetCurrentDirectory());
                result = false;
            }
            catch (Exception ex)
            {
                param.Log.ToolExceptionCtx(LOG_CTX, ex, "Exception syncing dependencies.");
                result = false;
            }
            finally
            {
                param.Log.ProfileEnd();
            }

            // Clear our cache of modified times; in case Perforce has touched an entry.
            //this.ContentObjectCacheManager.ClearNodeModifiedTimeEntries();
            return (result);
        }

        /// <summary>
        /// Sync a set of files from Perforce to #head revision; the list
        /// is filtered for only those files requiring syncing.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="p4"></param>
        /// <param name="depotFilenames"></param>
        /// <returns></returns>
        private bool TrySyncPerforceFiles(IEngineParameters param, P4 p4, IEnumerable<String> depotFilenames)
        {
            if (!depotFilenames.Any())
                return (true);

            bool result = true;
            try
            {
                // "preview" sync-method which is much quicker; especially for
                // remote studios.
                List<String> syncArgs = new List<String> {"-n"};
                syncArgs.AddRange(depotFilenames);
                P4RecordSet previewResult = p4.Run("sync", false, syncArgs.ToArray());
                List<String> oldDepotFilenames = previewResult.Records.
                    Where(record => record.Fields.ContainsKey("depotFile")).
                    Select(record => record.Fields["depotFile"]).
                    ToList();

                int oldDepotFilenamesCount = oldDepotFilenames.Count;
                param.Log.MessageCtx(LOG_CTX, "Syncing {0} out-of-date dependencies from Perforce.", oldDepotFilenamesCount.ToString("D"));

                if (oldDepotFilenamesCount > 0)
                {
                    param.Log.MessageCtx(LOG_CTX, "Syncing {0}, ...", String.Join(", ", oldDepotFilenames.Take(Math.Min(oldDepotFilenamesCount, 5))));

                    List<String> errors = new List<String>();
                    P4RecordSet fileSyncRecords = null;


                    fileSyncRecords = p4.Run("sync", false, oldDepotFilenames.ToArray());
                    errors.AddRange(fileSyncRecords.Errors);
                }

                previewResult = p4.Run("sync", false, syncArgs.ToArray());
                oldDepotFilenames = previewResult.Records.
                    Where(record => record.Fields.ContainsKey("depotFile")).
                    Select(record => record.Fields["depotFile"]).
                    ToList();

                if (oldDepotFilenames.Any())
                {
                    param.Log.WarningCtx(LOG_CTX, "{0} files still out-of-date!", oldDepotFilenames.Count);
                }
            }
            catch (P4API.Exceptions.P4APIExceptions ex)
            {
                param.Log.ExceptionCtx(LOG_CTX, ex, "Perforce exception");
                result = false;
            }
            catch (Exception ex)
            {
                param.Log.ExceptionCtx(LOG_CTX, ex, "Unhandled exception");
                result = false;
            }
            return (result);
        }

        public void Dispose()
        {
            if (p4 != null)
            {
                p4.Disconnect();
            }
        }
    }
}