﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.Pipeline.Processor.Map.AssetCombine
{

    /// <summary>
    /// 
    /// </summary>
    internal class DrawableDictionaryInput
    {
        internal DrawableDictionaryInput(XElement element)
        {
            Name = element.Attribute("name").Value;
            Type = element.Attribute("type").Value;
            CacheDirectory = element.Attribute("cachedir").Value;
            SourceZipPathname = element.Attribute("source").Value;
            
            bool isNewDLC = false;
            if (bool.TryParse(element.Attribute("is_new_dlc").Value, out isNewDLC))
                IsNewDLC = isNewDLC;
            else
                IsNewDLC = false;
        }

        internal string Name { get; private set; }
        internal string Type { get; private set; }
        internal string CacheDirectory { get; private set; }
        internal string SourceZipPathname { get; private set; }
        internal bool IsNewDLC { get; private set; }
    }

} // RSG.Pipeline.Processor.Map.AssetCombine namespace
