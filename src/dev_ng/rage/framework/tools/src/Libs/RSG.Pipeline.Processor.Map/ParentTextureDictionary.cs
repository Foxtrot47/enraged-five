﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using Ionic.Zip;
using RSG.Pipeline.Content;
using RSG.Pipeline.Services;
using RSG.Model.GlobalTXD;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Attributes;
using XGE = RSG.Interop.Incredibuild.XGE;

namespace RSG.Pipeline.Processor.Map
{
    using RSG.Platform;
    using File = RSG.Pipeline.Content.File;

    /// <summary>
    /// Global parent texture dictionary processor.
    /// </summary>
    [Export(typeof(IProcessor))]
    [ProcessorFlags(ProcessorFlags.Compatibility)]
    public class ParentTextureDictionary :
        MapProcessorBase,
        IProcessor
    {
        #region Constants
        private static readonly String DESCRIPTION = "Parent Texture Dictionary Processor";
        private static readonly String LOG_CTX = "ParentTxd PreProcess";

        private static GlobalRoot GtxdRoot;
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ParentTextureDictionary()
            : base(DESCRIPTION)
        {
        }
        #endregion // Constructor(s)

        #region IProcessor Interface Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="process"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="syncDependencies"></param>
        /// <param name="resultantProcesses"></param>
        /// <returns></returns>
        public override bool Prebuild(
            IEngineParameters param,
            IProcess process,
            IProcessorCollection processors,
            IContentTree owner,
            out IEnumerable<IContentNode> syncDependencies,
            out IEnumerable<IProcess> resultantProcesses)
        {
            ICollection<IProcess> processes = new List<IProcess>();
            ICollection<IContentNode> dependencies = new List<IContentNode>();

            bool ret = true;

            // get the gtxd node from the process' parameters
            IFilesystemNode gtxdNode = process.GetParameter(Consts.Textures_ParentTextureDictionaryModel, (IFilesystemNode) null);
            if (gtxdNode != null)
            {
                if (process.State == ProcessState.Initialised)
                {
                    process.State = ProcessState.Prebuilding;
                    processes.Add(process);

                    dependencies.Add((IContentNode)gtxdNode);

                    syncDependencies = dependencies;
                    resultantProcesses = processes;
                }
                else if (process.State == ProcessState.Prebuilding)
                {
                    process.State = ProcessState.Discard;

                    // we can proceed to the stripping of textures
                    if (System.IO.File.Exists(gtxdNode.AbsolutePath))
                    {
                        if (GtxdRoot == null)
                        {
                            GtxdRoot = new GlobalRoot(gtxdNode.AbsolutePath, Path.Combine(param.Branch.Assets, "maps", "Textures"), ValidationOptions.DontValidate, true);
                        }

                        if (GtxdRoot != null)
                        {
                            ProcessDictionaryTextures(param, process, GtxdRoot);
                        }
                    }
                    else
                    {
                        param.Log.ErrorCtx(LOG_CTX, string.Format("Process {0} parameter's file doest not exists: {1}", Consts.Textures_ParentTextureDictionaryModel, gtxdNode.AbsolutePath));
                        ret = false;
                    }
                }
            }
            else
            {
                param.Log.ErrorCtx(LOG_CTX, string.Format("Process does not contain the {0} parameter", Consts.Textures_ParentTextureDictionaryModel));
                ret = false;
            }

            syncDependencies = dependencies;
            resultantProcesses = processes;

            return ret;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processes"></param>
        /// <param name="tools"></param>
        /// <param name="tasks"></param>
        /// <returns></returns>
        public override bool Prepare(IEngineParameters param, IEnumerable<IProcess> processes,
            out IEnumerable<XGE.ITool> tools, out IEnumerable<XGE.ITask> tasks)
        {
            tools = new List<XGE.ITool>();
            tasks = new List<XGE.ITask>();
            return (false);
        }
        #endregion // IProcessor Interface Methods

        /// <summary>
        /// If enabled, omits all textures within a texture dictionary based on
        /// what has been promoted into the global texture dictionary
        /// </summary>
        /// <param name="param"></param>
        /// <param name="asset"></param>
        /// <param name="gtxdRoot"></param>
        /// <param name="textureDictionaryZip"></param>
        /// <returns></returns>
        private bool ProcessDictionaryTextures(IEngineParameters param, IProcess process, GlobalRoot gtxdRoot)
        {
            Content.Asset asset = process.Inputs.First() as Content.Asset;
            Content.Asset output = process.Outputs.First() as Content.Asset;
            String inputTextureDictionaryZip = asset.AbsolutePath;
            String txdName = inputTextureDictionaryZip.Substring(0, inputTextureDictionaryZip.IndexOf("."));
            String txdDirectory = Path.Combine(Path.GetDirectoryName(output.AbsolutePath), Path.GetFileName(txdName));

            if (!System.IO.Directory.Exists(txdDirectory))
                System.IO.Directory.CreateDirectory(txdDirectory);

            IEnumerable<String> extractedTxdFiles;
            bool extractResult = true;
            if (process.Rebuild == RebuildType.Yes || param.Flags.HasFlag(EngineFlags.Rebuild))
                extractResult = Zip.ExtractAll(inputTextureDictionaryZip, txdDirectory, false, null, out extractedTxdFiles);
            else
                extractResult = Zip.ExtractNewer(inputTextureDictionaryZip, txdDirectory, null, out extractedTxdFiles);

            if (!extractResult)
            {
                param.Log.ErrorCtx(LOG_CTX, "Extracting '{0}' to '{1}' failed.",
                    inputTextureDictionaryZip, txdDirectory);
                return false;
            }

            //Go through all textures in this dictionary to validate them against the GTXD.
            //If it exists in the GTXD, remove them and re-pack the .itd.zip.
            String baseTxdName = Filename.GetBasename(inputTextureDictionaryZip);
            String mapSectionName = (String)process.Parameters[Consts.MapProcess_MapSectionName];
            GlobalTextureDictionary sourceTextureDictionary = GlobalRoot.GetSourceDictionaryParent(baseTxdName, mapSectionName, gtxdRoot);

            IList<String> globalTextures = new List<String>();

            if (sourceTextureDictionary != null) //The GTXD has this map section as a source.
            {
                GlobalRoot.GetAllPromotedTexturesFromBase(sourceTextureDictionary, ref globalTextures);

                List<String> packedTxdFiles = new List<String>();
                foreach (String extractedTxdFile in extractedTxdFiles)
                {
                    if (Path.GetExtension(extractedTxdFile) == ".tcl"
                        || Path.GetExtension(extractedTxdFile) == ".tif"
                        || Path.GetExtension(extractedTxdFile) == ".dds")
                    {
                        string txdFileName = Path.GetFileNameWithoutExtension(extractedTxdFile);
                        IEnumerable<String> foundTextures = globalTextures.Where(globalTexture => String.Compare(txdFileName, globalTexture, true) == 0);
                        if (foundTextures.Count() == 0)
                        {
                            //Add this texture to the TXD.  Otherwise, it will be omitted
                            //because it's already in the global texture dictionary.
                            packedTxdFiles.Add(extractedTxdFile);
                        }
                        else
                        {
                            param.Log.WarningCtx(LOG_CTX, String.Format("'{0}' will be omitted from '{1}/{2}' because it has been promoted to the global texture dictionary.", txdFileName, mapSectionName, baseTxdName));
                        }
                    }
                    else
                    {
                        packedTxdFiles.Add(extractedTxdFile);
                    }
                }

                //Package up the .zip file again.
                if (System.IO.File.Exists(output.AbsolutePath))
                    System.IO.File.Delete(output.AbsolutePath);
                Zip.Create(param.Branch, output.AbsolutePath, packedTxdFiles);
            }

            return true;
        }

    }

}   // RSG.Pipeline.Processor.Map
