﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Linq;
using System.Xml.Linq;

using RSG.Base.Configuration;
using RSG.Base.Extensions;
using XGE = RSG.Interop.Incredibuild.XGE;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Attributes;
using RSG.Pipeline.Content;
using RSG.Pipeline.Services;
using RSG.SceneXml;
using RSG.SceneXml.MapExport;

namespace RSG.Pipeline.Processor.Map
{

    /// <summary>
    /// Map collision processor.
    /// </summary>
    [Export(typeof(IProcessor))]
    [ProcessorFlags(ProcessorFlags.Compatibility)]
    public class CollisionPreview :
        ProcessorBase,
        IProcessor
    {
        #region Constants
        private static readonly String description_ = "Map Collision Preview Processor";
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public CollisionPreview()
            : base(description_)
        {
        }
        #endregion // Constructor(s)

        #region Static Methods
        #endregion

        #region IProcessor Interface Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="process"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="syncDependencies"></param>
        /// <param name="resultantProcesses"></param>
        /// <returns></returns>
        public override bool Prebuild(IEngineParameters param,
            IProcess process, IProcessorCollection processors,
            IContentTree owner, out IEnumerable<IContentNode> syncDependencies,
            out IEnumerable<IProcess> resultantProcesses)
        {
            bool result = true;

            Debug.Assert(process.Inputs.OfType<Content.File>().Any());
            Debug.Assert(process.Outputs.Any());
            Debug.Assert(process.Outputs.First() is Content.IFilesystemNode);

            List<IProcess> resultantProcessesList = new List<IProcess>();

            ContentTreeHelper contentTreeHelper = new ContentTreeHelper(process.Owner);
            Content.File[] unprocessedCollisionZipFiles = process.Inputs.OfType<Content.File>().ToArray();
            Content.IFilesystemNode outputNode = (Content.IFilesystemNode)process.Outputs.First();

            foreach (Content.File unprocessedCollisionZipFile in unprocessedCollisionZipFiles)
            {
                // Create collision process
                String manifestAdditionsPathname = System.IO.Path.Combine(param.Branch.Project.Cache, param.Branch.Name, "manifest", process.Outputs.First().Name, "BoundsProcessorAdditions.xml");
                IProcessor collisionProcessor = processors.FirstOrDefault(p => "RSG.Pipeline.Processor.Map.Collision".Equals(p.Name, StringComparison.OrdinalIgnoreCase));
                ProcessBuilder collisionProcessBuilder = new ProcessBuilder(collisionProcessor, owner);

                // setup the inputs
                IContentNode exportZipNode = contentTreeHelper.GetExportZipNodeFromUnprocessedCollisionNode(unprocessedCollisionZipFile);
                IContentNode sceneXmlNode = contentTreeHelper.GetSceneXmlNodeFromExportZipNode(exportZipNode);

                int inputIndex = 0;
                collisionProcessBuilder.Inputs.Add(unprocessedCollisionZipFile);
                String unprocessedBoundsZipParameterName = String.Format("{0}{1}", Consts.Collision_UnprocessedBoundsZipParameterPrefix, inputIndex);
                collisionProcessBuilder.Parameters.Add(unprocessedBoundsZipParameterName, unprocessedCollisionZipFile.AbsolutePath);

                collisionProcessBuilder.Inputs.Add(sceneXmlNode);
                String sceneXmlParameterName = String.Format("{0}{1}", Consts.Collision_SceneXmlParameterPrefix, inputIndex);
                collisionProcessBuilder.Parameters.Add(sceneXmlParameterName, ((IFilesystemNode)sceneXmlNode).AbsolutePath);
                ++inputIndex;

                // also get the scene xml siblings
                IContentNode processedZipNode = contentTreeHelper.GetProcessedZipNodeFromExportZipNode(exportZipNode);
                IContentNode[] exportZipSiblingNodes = contentTreeHelper.GetAllInputsForNode(processedZipNode).Where(node => node != exportZipNode).ToArray();
                foreach (IContentNode exportZipSiblingNode in exportZipSiblingNodes)
                {
                    IContentNode sceneXmlSiblingNode = contentTreeHelper.GetSceneXmlNodeFromExportZipNode(exportZipSiblingNode);
                    collisionProcessBuilder.Inputs.Add(sceneXmlSiblingNode);
                    String sceneXmlSiblingParameterName = String.Format("{0}{1}", Consts.Collision_SceneXmlParameterPrefix, inputIndex++);
                    collisionProcessBuilder.Parameters.Add(sceneXmlSiblingParameterName, ((IFilesystemNode)sceneXmlSiblingNode).AbsolutePath);
                }

                // setup the outputs
                if (!System.IO.Directory.Exists(outputNode.AbsolutePath))
                    System.IO.Directory.CreateDirectory(outputNode.AbsolutePath);
                Content.Directory outputDirectoryNode = (Content.Directory)owner.CreateDirectory(outputNode.AbsolutePath);
                collisionProcessBuilder.Outputs.Add(outputDirectoryNode);

                // Set up manifest additions data
                collisionProcessBuilder.Parameters.Add(Consts.Collision_ManifestAdditionsPathnameParameterName, manifestAdditionsPathname);
                collisionProcessBuilder.Outputs.Add(owner.CreateFile(manifestAdditionsPathname));

                resultantProcessesList.Add(collisionProcessBuilder.ToProcess());
            }

            syncDependencies = new List<IContentNode>();
            resultantProcesses = resultantProcessesList;
            process.State = ProcessState.Discard;

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processes"></param>
        /// <param name="tools"></param>
        /// <param name="tasks"></param>
        /// <returns></returns>
        public override bool Prepare(IEngineParameters param, IEnumerable<IProcess> processes,
            out IEnumerable<XGE.ITool> tools, out IEnumerable<XGE.ITask> tasks)
        {
            // This should never get hit; the preprocess just creates other
            // processes.
            throw (new NotImplementedException());
        }
        #endregion // IProcessor Interface Methods

        #region Private Methods
        #endregion // Private Methods
    }

} // RSG.Pipeline.Processor.Map namespace
