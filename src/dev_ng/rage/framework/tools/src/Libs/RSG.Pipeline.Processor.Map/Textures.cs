﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using XGE = RSG.Interop.Incredibuild.XGE;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Attributes;
using RSG.Pipeline.Content;
using RSG.SceneXml;
using RSG.SceneXml.MapExport;
using System.Diagnostics;
using RSG.Model.GlobalTXD;
using RSG.Base.Configuration;
using System.IO;
using Ionic.Zip;
using RSG.Base.Logging;
using RSG.Base.Extensions;

namespace RSG.Pipeline.Processor.Map
{

    /// <summary>
    /// Map data texture processor; repackages map texture dictionaries.
    /// </summary>
    /// The map data texture processor handles the repackaging of map texture
    /// dictionaries.  This includes the parented textures using the Workbench
    /// and drawable/fragment-packed textures.
    /// 
    /// This is currently used to verify that the global txd zip/xml files match and
    /// that the contents of the map export zips are valid in relation to the parent txd.
    /// 
    [Export(typeof(IProcessor))]
    [ProcessorFlags(ProcessorFlags.Compatibility)]
    public class Textures :
        MapProcessorBase,
        IProcessor
    {
        #region Constants
        private static readonly String DESCRIPTION = "Map Texture Processor";
        private static readonly String LOG_CTX = "Textures PreProcess";
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public Textures()
            : base(DESCRIPTION)
        {
        }
        #endregion // Constructor(s)

        #region IProcessor Interface Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="process"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="syncDependencies"></param>
        /// <param name="resultantProcesses"></param>
        /// <returns></returns>
        public override bool Prebuild(IEngineParameters param,
            IProcess process, IProcessorCollection processors,
            IContentTree owner, out IEnumerable<IContentNode> syncDependencies, 
            out IEnumerable<IProcess> resultantProcesses)
        {
            syncDependencies = new List<IContentNode>();
            resultantProcesses = new List<IProcess>();

            // We should have 3 inputs.
            // 1 - Map section export node.
            // 2 - Parent txd xml file.
            // 2 - Parent txd zip file.
            Debug.Assert(process.Inputs.Count() == 3, "Incorrect number of input nodes for.");
            if (process.Inputs.Count() != 3)
            {
                throw new ArgumentException("Incorrect number of input nodes provided.");
            }

            bool retVal = true;

            // Get the paths that we are interested in.
            IFilesystemNode sectionExportNode;
            IFilesystemNode parentTxdXmlNode;
            IFilesystemNode parentTxdZipNode;
            if (ExtractNodes(process.Inputs, out sectionExportNode, out parentTxdXmlNode, out parentTxdZipNode))
            {
                String parentTxdXml = parentTxdXmlNode.AbsolutePath;
                String parentTxdZip = parentTxdZipNode.AbsolutePath;

                // Load the global txd file into our model and then extract the txds we'll need to check.
                GlobalRoot parentTxdModel;

                if (parentTxdXmlNode.Parameters.ContainsKey(Consts.Textures_ParentTextureDictionaryModel))
                {
                    parentTxdModel = (GlobalRoot)parentTxdXmlNode.Parameters[Consts.Textures_ParentTextureDictionaryModel];
                }
                else
                {
                    param.Log.ProfileCtx(LOG_CTX, "GlobalTXD: Creating global txd model");
                    parentTxdModel = new GlobalRoot(parentTxdXml, Path.Combine(param.Branch.Assets, "maps", "Textures"), ValidationOptions.DontValidate, false);
                    parentTxdXmlNode.Parameters[Consts.Textures_ParentTextureDictionaryModel] = parentTxdModel;
                    param.Log.ProfileEnd();
                }

                // Check for duped textures (i.e. ones that appear in both the global textures and in an .itd.zip).
                retVal &= ValidateExportZipAgainstGlobalTxd(sectionExportNode, parentTxdModel, param.Log);
            }
            else
            {
                param.Log.ErrorCtx(LOG_CTX, "GlobalTXD: Failed to decipher input nodes into section export node, parent txd xml node and parent txd zip node.");
                retVal = false;
            }

            process.State = ProcessState.Prebuilt;
            return retVal;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processes"></param>
        /// <param name="tools"></param>
        /// <param name="tasks"></param>
        /// <returns></returns>
        public override bool Prepare(IEngineParameters param, IEnumerable<IProcess> processes,
            out IEnumerable<XGE.ITool> tools, out IEnumerable<XGE.ITask> tasks)
        {
            tools = new List<XGE.ITool>();
            tasks = new List<XGE.ITask>();
            return (false);
        }
        #endregion // IProcessor Interface Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="inputs"></param>
        /// <param name="sectionExportZip"></param>
        /// <param name="parentTxdXml"></param>
        /// <param name="parentTxdZip"></param>
        /// <returns></returns>
        private bool ExtractNodes(IEnumerable<IContentNode> inputs, out IFilesystemNode sectionExportNode, out IFilesystemNode ptxdXmlNode, out IFilesystemNode ptxdZipNode)
        {
            sectionExportNode = null;
            ptxdXmlNode = null;
            ptxdZipNode = null;

            foreach (IContentNode inputNode in inputs)
            {
                if (inputNode.Parameters.ContainsKey("gtxdxml"))
                {
                    ptxdXmlNode = inputNode as IFilesystemNode;
                }
                else if (inputNode.Parameters.ContainsKey("gtxdzip"))
                {
                    ptxdZipNode = inputNode as IFilesystemNode;
                }
                else
                {
                    sectionExportNode = inputNode as IFilesystemNode;
                }
            }

            Debug.Assert(sectionExportNode != null, "Section export input node not found.");
            Debug.Assert(ptxdXmlNode != null, "Parent txd xml input node not found.");
            Debug.Assert(ptxdZipNode != null, "Parent txd zip input node not found.");
            return (sectionExportNode != null && ptxdXmlNode != null && ptxdZipNode != null);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool ValidateExportZipAgainstGlobalTxd(IFilesystemNode sectionExportNode, GlobalRoot rootModel, ILog log)
        {
            try
            {
                using (ZipFile gtxdZipFile = ZipFile.Read(sectionExportNode.AbsolutePath))
                {
                    // Go over all the entries in the gtxd file that are texture dictionaries.
                    foreach (ZipEntry entry in gtxdZipFile.Entries.Where(item => item.FileName.EndsWith(".itd.zip")))
                    {
                        // Get all ancestor parent txds for each .itd.zip file that exists.
                        String txdName = StripAllExtensions(entry.FileName);

                        // Does this txd appear as a source dictionary in the model?
                        IList<SourceTextureDictionary> sourceTxds = FindSourceDictionaries(rootModel, txdName).ToList();
                        IList<String> globalTextureNames = new List<String>();

                        foreach (SourceTextureDictionary sourceTxd in sourceTxds)
                        {
                            // Get the list of parented textures.
                            IList<GlobalTexture> globalTextures = GetGlobalTexturesForSourceTxd(sourceTxd);
                            globalTextureNames.AddRange(globalTextures.Select(item => item.StreamName));
                        }

                        // Check whether any of the global texture names appear in the zip's txd.
                        if (globalTextureNames.Any())
                        {
                            using (MemoryStream txdZipStream = new MemoryStream())
                            {
                                entry.Extract(txdZipStream);
                                txdZipStream.Position = 0;

                                CheckForDuplicateTextures(txdZipStream, globalTextureNames, sectionExportNode.Name, txdName, log);
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "GlobalTXD: Section {0} encountered an exception while parsing the '{1}' zip file.",
                    sectionExportNode.Name, sectionExportNode.AbsolutePath);
                return false;
            }

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        private String StripAllExtensions(String filename)
        {
            // Strip the off all extensionts from the entries file name.
            filename = filename.ToLower();
            while (!String.IsNullOrEmpty(Path.GetExtension(filename)))
            {
                filename = Path.GetFileNameWithoutExtension(filename);
            }
            return filename;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private IEnumerable<SourceTextureDictionary> FindSourceDictionaries(IDictionaryContainer modelRoot, String txdName)
        {
            if (modelRoot.GlobalTextureDictionaries != null)
            {
                foreach (GlobalTextureDictionary globalTxd in modelRoot.GlobalTextureDictionaries.Values)
                {
                    foreach (SourceTextureDictionary sourceTxd in FindSourceDictionaries(globalTxd, txdName))
                    {
                        yield return sourceTxd;
                    }
                }
            }

            if (modelRoot.SourceTextureDictionaries != null)
            {
                foreach (SourceTextureDictionary sourceTxd in modelRoot.SourceTextureDictionaries.Values)
                {
                    if (sourceTxd.Name.ToLower() == txdName)
                    {
                        yield return sourceTxd;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rootModel"></param>
        /// <param name="sourceTxdName"></param>
        /// <returns></returns>
        private IList<GlobalTexture> GetGlobalTexturesForSourceTxd(SourceTextureDictionary sourceTxd)
        {
            IList<GlobalTexture> textures = new List<GlobalTexture>();

            // Iterate up through all parent dictionary containers
            IDictionaryContainer parentContainer = sourceTxd.Parent;
            while (parentContainer != null)
            {
                if (parentContainer is GlobalTextureDictionary)
                {
                    GlobalTextureDictionary globalTxdContainer = (GlobalTextureDictionary)parentContainer;
                    textures.AddRange(globalTxdContainer.Textures.Values.Cast<GlobalTexture>());
                }

                parentContainer = parentContainer.Parent;
            }

            return textures;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="txdZipStream"></param>
        /// <param name="globalTextureNames"></param>
        private void CheckForDuplicateTextures(Stream txdZipStream, IList<String> globalTextureNames, String sectionName, String txdName, ILog log)
        {
            using (ZipFile txdZipFile = ZipFile.Read(txdZipStream))
            {
                foreach (ZipEntry entry in txdZipFile.Entries.Where(item => item.FileName.EndsWith(".tcl")))
                {
                    // Get all ancestor parent txds for each .itd.zip file that exists.
                    String textureName = StripAllExtensions(entry.FileName);

                    if (globalTextureNames.Contains(textureName))
                    {
                        log.ErrorCtx(LOG_CTX, "GlobalTXD: Section {0}, TXD {1}, Texture {2} is present in both the parent txd xml file and in the txd zip.",
                            sectionName, txdName, textureName);
                    }
                }
            }
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Processor.Map namespace
