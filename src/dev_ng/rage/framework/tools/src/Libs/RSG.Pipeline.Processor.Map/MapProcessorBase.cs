﻿using System;
using System.Collections.Generic;
using SIO = System.IO;
using XGE = RSG.Interop.Incredibuild.XGE;
using RSG.Pipeline.Core;
using RSG.Pipeline.Services;

namespace RSG.Pipeline.Processor.Map
{

    /// <summary>
    /// Base class for all map processors; mainly to override the parameters
    /// filename in a single place so all map processors share the same set
    /// of parameters.
    /// </summary>
    public abstract class MapProcessorBase :
        ProcessorBase,
        IProcessor
    {
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public MapProcessorBase(String description)
            : base(description)
        {
        }
        #endregion // Constructor(s)

        #region IProcessor Interface Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="process"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="syncDependencies"></param>
        /// <param name="resultantProcesses"></param>
        /// <returns></returns>
        public override bool Prebuild(IEngineParameters param,
            IProcess process, IProcessorCollection processors,
            IContentTree owner, out IEnumerable<IContentNode> syncDependencies, 
            out IEnumerable<IProcess> resultantProcesses)
        {
            throw (new NotImplementedException());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processes"></param>
        /// <param name="tools"></param>
        /// <param name="tasks"></param>
        /// <returns></returns>
        public override bool Prepare(IEngineParameters param, IEnumerable<IProcess> processes,
            out IEnumerable<XGE.ITool> tools, out IEnumerable<XGE.ITask> tasks)
        {
            throw (new NotImplementedException());
        }
        #endregion // IProcessor Interface Methods

        #region ProcessorBase Overridden Methods
        /// <summary>
        /// Load parameters from defined XML file.
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        /// We override this method to load parameters from a common map parameter
        /// XML file; and then processor-specific parameter file.
        protected override void LoadParameters(IEngineParameters param)
        {
            if (this.ParametersLoaded)
                return;

            String commonFilename = GetCommonMapParametersFilename(param);
            if (SIO.File.Exists(commonFilename))
            {
                if (!RSG.Base.Xml.Parameters.Load(commonFilename, ref this._parameters))
                    param.Log.Error("Common Map Parameter load failed: {0}.", commonFilename);
            }

            String mapFilename = GetParametersFilename(param);
            param.Log.Message("Loading parameters from {0}.", mapFilename);
            if (SIO.File.Exists(mapFilename))
            {
                RSG.Base.Xml.Parameters.Load(mapFilename, ref this._parameters);
            }

            String projectFilename = GetProjectParametersFilename(param);
            param.Log.Message("Loading parameters from {0}.", projectFilename);
            if (SIO.File.Exists(projectFilename))
            {
                RSG.Base.Xml.Parameters.Load(projectFilename, ref this._parameters);

                // Attempt to load a "<filename>.local.xml" file; for local overrides.
                String extension = String.Format("local{0}", SIO.Path.GetExtension(projectFilename));
                String localFilename = SIO.Path.ChangeExtension(projectFilename, extension);
                if (SIO.File.Exists(localFilename))
                    RSG.Base.Xml.Parameters.Load(localFilename, ref this._parameters);
            }

            this.ParametersLoaded = true;
        }

        /// <summary>
        /// Return common map parameters XML filename.
        /// </summary>
        /// <returns></returns>
        /// Overridden in base class so that all map processors get the same
        /// parameters XML.
        /// 
        protected virtual String GetCommonMapParametersFilename(IEngineParameters param)
        {
            return (System.IO.Path.Combine(param.Branch.Project.Config.ToolsConfig,
                "processors", String.Format("RSG.Pipeline.Processor.Map.xml")));
        }
        #endregion // ProcessorBase Overridden Methods
    }

} // RSG.Pipeline.Processor.Map namespace
