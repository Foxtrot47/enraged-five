﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.Pipeline.Processor.Map.AssetCombine
{

    /// <summary>
    /// Class that reads the Asset Combine executable output XML document.
    /// </summary>
    internal class Document
    {
        private Document(XDocument document)
        {
            IEnumerable<XElement> mapElements = document.Root.Elements("Map");
            Debug.Assert(mapElements.Count() == 1);

            Map = new Map(mapElements.First());
        }

        internal static Document Load(string pathname)
        {
            XDocument document = XDocument.Load(pathname);
            return new Document(document);
        }

        internal Map Map { get; private set; }
    }

} // RSG.Pipeline.Processor.Map.AssetCombine namespace
