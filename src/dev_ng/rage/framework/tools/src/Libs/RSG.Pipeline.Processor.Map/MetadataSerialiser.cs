﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using RSG.SceneXml.MapExport;
using SIO = System.IO;
using System.Linq;
using System.Text;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using XGE = RSG.Interop.Incredibuild.XGE;
using RSG.Pipeline.Content;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Attributes;
using RSG.Pipeline.Services;
using RSG.Pipeline.Services.Metadata;
using RSG.Platform;
using RSG.SceneXml.MapExport;

namespace RSG.Pipeline.Processor.Map
{

    /// <summary>
    /// Map metadata serialiser processor.
    /// </summary>
    [Export(typeof(IProcessor))]
    [ProcessorFlags(ProcessorFlags.Compatibility)]
    public class MetadataSerialiser :
        ProcessorBase,
        IProcessor
    {
        #region Constants
        /// <summary>
        /// Processor description.
        /// </summary>
        private static readonly String DESCRIPTION = "Map Metadata Serialiser";
        
        /// <summary>
        /// Processor log context.
        /// </summary>
        private static readonly String LOG_CTX = "Map Metadata Serialiser";
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public MetadataSerialiser()
            : base(DESCRIPTION)
        {
        }
        #endregion // Constructor(s)

        #region IProcessor Interface Methods
        /// <summary>
        /// Prebuild stage; contains simple validation of passed process.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="process"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="syncDependencies"></param>
        /// <param name="resultantProcesses"></param>
        /// <returns></returns>
        public override bool Prebuild(IEngineParameters param,
            IProcess process, IProcessorCollection processors,
            IContentTree owner, out IEnumerable<IContentNode> syncDependencies, 
            out IEnumerable<IProcess> resultantProcesses)
        {
            // Process verification.
            Debug.Assert(process.Inputs.Count() > 0);
            Debug.Assert(process.Outputs.Count() > 0);
            Debug.Assert(process.Outputs.First() is Content.Directory);

            bool result = true;

            IContentNode imapStreamDirNode = GetIMAPStreamDirOutput(param, process);
            Debug.Assert(null != imapStreamDirNode);
            result &= (null != imapStreamDirNode);

            ICollection<IProcess> processes = new List<IProcess>();
            process.State = ProcessState.Prebuilt;
            processes.Add(process);

            syncDependencies = new List<IContentNode>();
            resultantProcesses = processes;
            return (result);
        }

        /// <summary>
        /// Prepare a build for the processes; populating the XGE project with
        /// the tools, environments and tasks required.  Including setting up
        /// the task dependency chain.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="processes"></param>
        /// <param name="tools"></param>
        /// <param name="tasks"></param>
        /// <returns></returns>
        public override bool Prepare(IEngineParameters param, IEnumerable<IProcess> processes,
            out IEnumerable<XGE.ITool> tools, out IEnumerable<XGE.ITask> tasks)
        {
            this.LoadParameters(param);
            int maxTasksPerJob = this.Parameters.ContainsKey(Consts.MetadataSerialiser_MaxTasksPerJobParameterName) ?
                (int)this.Parameters[Consts.MetadataSerialiser_MaxTasksPerJobParameterName] : defaultMaxTasksPerJob_;
            int lodTaskWeight = this.Parameters.ContainsKey(Consts.MetadataSerialiser_LODTaskWeightParameterName) ?
                (int)this.Parameters[Consts.MetadataSerialiser_LODTaskWeightParameterName] : defaultLODTaskWeight_;
            String nonSerialisedTimeCycleTypesText = this.Parameters.ContainsKey(Consts.MetadataSerialiser_NonSerialisedTimeCycleTypesParameterName) ?
                (String)this.Parameters[Consts.MetadataSerialiser_NonSerialisedTimeCycleTypesParameterName] : String.Empty;

            bool result = true;
            bool allow_remote = GetXGEAllowRemote(param, false);
            XGE.ITool serialiserTool = XGEFactory.GetMapProcessorTool(param.Log,
                param.Branch.Project.Config, XGEFactory.MapToolType.MetadataSerialiser2,
                "Map Metadata Serialiser", allow_remote);
            XGE.ITaskJobGroup taskGroup = new XGE.TaskGroup("Map Metadata Serialiser");

            XGE.ITaskJob[] taskJobs;
            result &= CreateTaskJobsFromProcesses(param, processes, serialiserTool, maxTasksPerJob, lodTaskWeight, nonSerialisedTimeCycleTypesText, out taskJobs);

            foreach (XGE.ITaskJob taskJob in taskJobs)
                taskGroup.Tasks.Add(taskJob);

            // Assign method output.
            tools = new List<XGE.ITool>(new XGE.ITool[] { serialiserTool });
            tasks = new List<XGE.ITask>(new XGE.ITask[] { taskGroup });

            return (result);
        }
        #endregion // IProcessor Interface Methods

        #region Private Methods
        /// <summary>
        /// Return IMAP stream directory output from IProcess; or null (and log error).
        /// </summary>
        /// <param name="param"></param>
        /// <param name="process"></param>
        /// <returns></returns>
        private IContentNode GetIMAPStreamDirOutput(IEngineParameters param, IProcess process)
        {
            IContentNode imapStreamDir = process.Outputs.FirstOrDefault(n => n is Directory);
            if (null == imapStreamDir)
                param.Log.ErrorCtx(LOG_CTX, "No IMAP stream directory node attached to process.  Aborting.");
            return (imapStreamDir);
        }

        /// <summary>
        /// Given a collection of processes, create a collection of ITaskJob objects
        /// </summary>
        private bool CreateTaskJobsFromProcesses(IEngineParameters param, 
            IEnumerable<IProcess> processes, 
            XGE.ITool serialiserTool, 
            int maxTasksPerJob, 
            int lodTaskWeight, 
            String nonSerialisedTimeCycleTypesText,
            out XGE.ITaskJob[] taskJobs)
        {
            bool result = true;
            List<XGE.ITaskJob> taskJobsList = new List<XGE.ITaskJob>();

            // Here we deal with ITYP merge tasks and other tasks separately.
            List<IProcess> itypMergeProcesses = new List<IProcess>();
            List<IProcess> regularProcesses = new List<IProcess>();

            String audioCollisionMapFilename = String.Empty;
            String sceneOverridesDirectory = String.Empty;

            foreach (IProcess process in processes)
            {
                if (process.Parameters.ContainsKey(Consts.MetadataSerialiser_AudioCollisionMapPathnameParameterName))
                    audioCollisionMapFilename = (String)process.Parameters[Consts.MetadataSerialiser_AudioCollisionMapPathnameParameterName];
                if (process.Parameters.ContainsKey(Consts.MetadataSerialiser_SceneOverridesDirectoryParameterName))
                    sceneOverridesDirectory = (String)process.Parameters[Consts.MetadataSerialiser_SceneOverridesDirectoryParameterName];

                bool itypMerge = false;
                if (process.Parameters.ContainsKey(Consts.MetadataSerialiser_ITYPMergeParameterName))
                    itypMerge = (bool)process.Parameters[Consts.MetadataSerialiser_ITYPMergeParameterName];

                if (itypMerge)
                    itypMergeProcesses.Add(process);
                else
                    regularProcesses.Add(process);
            }

            // create the options object
            String[] NonSerialiserTimeCycleTypes = nonSerialisedTimeCycleTypesText.Split(';');
            MapMetadataSerialiserOptions options = new MapMetadataSerialiserOptions(
                audioCollisionMapFilename, sceneOverridesDirectory, NonSerialiserTimeCycleTypes);

            // Consider any regular processes
            if (regularProcesses.Any())
            {
                int bucketIndex = 0;
                Dictionary<String, IEnumerable<IProcess>> processBucketMap = SplitProcessesIntoBuckets(regularProcesses, maxTasksPerJob, lodTaskWeight);
                foreach (KeyValuePair<String, IEnumerable<IProcess>> processBucketPair in processBucketMap)
                {
                    String xgeTaskName = processBucketPair.Key;
                    XGE.ITaskJob task = new XGE.TaskJob(xgeTaskName);
                    int taskCounter = 0;
                    ICollection<MapMetadataSerialiserTask> tasksData =
                        new List<MapMetadataSerialiserTask>();
                    foreach (IProcess process in processBucketPair.Value)
                    {
                        // All SceneXml File inputs.
                        IEnumerable<String> allInputSceneXmlFiles = process.Inputs.
                            OfType<File>().
                            Where(input => input.AbsolutePath.EndsWith(".xml", StringComparison.OrdinalIgnoreCase)).
                            Select(input => input.AbsolutePath);

                        // SceneXml files inputs.
                        List<String> inputSceneXmlFiles = process.Parameters.
                            Where(parameter => parameter.Key.StartsWith(Consts.MetadataSerialiser_InputToProcessParameterPrefix)).
                            Select(parameter => parameter.Value).
                            OfType<File>().
                            Select(input => input.AbsolutePath).
                            ToList();

                        // Dependency SceneXml file inputs.
                        IEnumerable<String> dependencySceneXmlFiles = allInputSceneXmlFiles.Except(inputSceneXmlFiles);

                        // Determine what we are exporting.
                        bool exportArchetypes = process.Parameters.ContainsKey(Constants.ParamMap_ExportArchetypes) ?
                            (bool)process.Parameters[Constants.ParamMap_ExportArchetypes] : false;
                        bool exportEntities = process.Parameters.ContainsKey(Constants.ParamMap_ExportEntities) ?
                            (bool)process.Parameters[Constants.ParamMap_ExportEntities] : false;

                        IEnumerable<string> forcedItypDependencies = process.Parameters.ContainsKey(Constants.ParamMap_ForceItypDependency) ? (IEnumerable<string>)process.Parameters[Constants.ParamMap_ForceItypDependency] : null;

                        Content.Directory output = (Content.Directory)process.Outputs.First();
                        String processedMapName = System.IO.Directory.GetParent(output.AbsolutePath).Name;
                        String taskName = String.Format("Map Metadata {0} [{1}]",
                            taskCounter++, processedMapName);

                        // Create task structure for our new metadata serialiser.
                        String assetCombineFilename = String.Empty;
                        if (process.Parameters.ContainsKey(Consts.MetadataSerialiser_AssetCombineDocumentPathnameParameterName))
                            assetCombineFilename = (String)process.Parameters[Consts.MetadataSerialiser_AssetCombineDocumentPathnameParameterName];
                        String mapExportAdditionsFilename = String.Empty;
                        if (process.Parameters.ContainsKey(Consts.MetadataSerialiser_MapExportAdditionsPathnameParameterName))
                            mapExportAdditionsFilename = (String)process.Parameters[Consts.MetadataSerialiser_MapExportAdditionsPathnameParameterName];
                        List<MapMetadataSerialiserManifestDependency> additionalManifestDependencies = new List<MapMetadataSerialiserManifestDependency>();
                        if (process.Parameters.ContainsKey(Consts.MetadataSerialiser_AdditionalManifestDependenciesParameterName))
                            additionalManifestDependencies = (List<MapMetadataSerialiserManifestDependency>)process.Parameters[Consts.MetadataSerialiser_AdditionalManifestDependenciesParameterName];

                        List<MapMetadataSerialiserCoreGameMetadataZipDependency> mapMetadataZips = new List<MapMetadataSerialiserCoreGameMetadataZipDependency>();
                        String metadataZipFilename = (String)process.GetParameter(Constants.ParamMap_CoreMetadataZip, String.Empty);
                        if (!String.IsNullOrEmpty(metadataZipFilename))
                        {
                            mapMetadataZips.Add(new MapMetadataSerialiserCoreGameMetadataZipDependency(metadataZipFilename));
                        }

                        IEnumerable<MapMetadataSerialiserInput> inputData = inputSceneXmlFiles.
                            Select(scene => new MapMetadataSerialiserInput(scene));
                        List<MapMetadataSerialiserSceneXmlDependency> dependencyData = dependencySceneXmlFiles.
                            Select(scene => new MapMetadataSerialiserSceneXmlDependency(scene)).ToList();

                        // For props merging, we add the inputs as dependency for the output. B*1777237
                        //if (forcedItypDependencies.Any())
                        //{
                        //    foreach (string file in inputSceneXmlFiles)
                        //    {
                        //        dependencyData.Add(new MapMetadataSerialiserDependency(file));
                        //    }
                        //}

                        List<MapMetadataSerialiserOutput> outputData = new List<MapMetadataSerialiserOutput>();

                        // Create our base platform which is for backwards compatibility with the current .ityp and imap files.
                        MapMetadataSerialiserOutput baseEntry = new MapMetadataSerialiserOutput(
                                processedMapName, output.AbsolutePath, exportArchetypes, exportEntities, forcedItypDependencies,
                                param.Flags.HasFlag(EngineFlags.Preview), "independent");
                        outputData.Add(baseEntry);

                        /* Disabled temporarily until we want to create platform specific .ityp and .imap files.
                        // Create outputs for all the platforms enabled.
                        IEnumerable<ITarget> enabledPlatforms = param.Branch.Targets.Where(t => t.Value.Enabled).Select(t => t.Value);
                        foreach (ITarget target in enabledPlatforms)
                        {
                            String platformString = PlatformUtils.PlatformToRagebuilderPlatform(target.Platform);
                            String platformOutputPath = SIO.Path.Combine(output.AbsolutePath, platformString);

                            MapMetadataSerialiserOutput outputEntry = new MapMetadataSerialiserOutput(
                                processedMapName, platformOutputPath, exportArchetypes, exportEntities,
                                param.Flags.HasFlag(EngineFlags.Preview), platformString);

                            outputData.Add(outputEntry);
                        }
                        */

                        MapMetadataSerialiserTask taskData = new MapMetadataSerialiserTask(
                            taskName, inputData,
                            dependencyData, additionalManifestDependencies, mapMetadataZips,
                            outputData,
                            assetCombineFilename, mapExportAdditionsFilename);
                        tasksData.Add(taskData);

                        // Set XGE parameters for dependency tracking.
                        process.Parameters.Add(Constants.ProcessXGE_Task, task);
                        process.Parameters.Add(Constants.ProcessXGE_TaskName,
                            xgeTaskName.Replace(" ", "_"));
                    }

                    String cacheDirectory = (String)this.Parameters[Consts.MapProcess_ProcessedMapCacheDirectory];
                    cacheDirectory = param.Branch.Environment.Subst(cacheDirectory);
                    if (!SIO.Directory.Exists(cacheDirectory))
                        SIO.Directory.CreateDirectory(cacheDirectory);

                    String configDataFilename = String.Format("map_metadata_serialiser_{0}.xml", bucketIndex++);
                    String configDataPathname = SIO.Path.Combine(cacheDirectory, configDataFilename);
                    configDataPathname = SIO.Path.GetFullPath(configDataPathname);
                    MapMetadataSerialiserConfig configData = new MapMetadataSerialiserConfig(options, tasksData);
                    result &= configData.Serialise(param.Log, configDataPathname);

                    // Create XGE task.
                    task.Tool = serialiserTool;
                    task.Caption = xgeTaskName;
                    task.InputFiles.Add(configDataPathname);
                    if (param.Branch.Project.IsDLC)
                        task.Parameters = String.Format("--config {0} --branch {1} --dlc {2}", configDataPathname, param.Branch.Name, param.Branch.Project.Name);
                    else
                        task.Parameters = String.Format("--config {0} --branch {1}", configDataPathname, param.Branch.Name);
                    task.SourceFile = configDataPathname;
                    taskJobsList.Add(task);
                }
            }

            // Consider any ITYP merge processes
            foreach (IProcess itypMergeProcess in itypMergeProcesses)
            {
                Content.Directory outputDirectory = (Content.Directory)itypMergeProcess.Outputs.First();
                String xgeTaskName = System.IO.Directory.GetParent(outputDirectory.AbsolutePath).Name;
                if (param.Branch.Project.IsDLC)
                    xgeTaskName = MapAsset.AppendMapPrefixIfRequired(param.Branch.Project, xgeTaskName);

                XGE.ITaskJob task = new XGE.TaskJob(xgeTaskName);

                // All SceneXml File inputs.
                IEnumerable<String> allInputSceneXmlFiles = itypMergeProcess.Inputs.
                    Where(input => input is File).
                    Cast<Content.File>().Select(input => input.AbsolutePath);

                // SceneXml files inputs.
                List<MapMetadataSerialiserMergeInputGroup> inputGroups = new List<MapMetadataSerialiserMergeInputGroup>();

                List<String> nonDependencySceneXmlFiles = new List<String>();// to seperate *real* inputs and dependencies
                foreach(KeyValuePair<String, object> inputParameterPair in itypMergeProcess.Parameters
                    .Where(parameter => parameter.Key.StartsWith(Consts.MetadataSerialiser_InputToProcessParameterPrefix)))
                {
                    Debug.Assert(inputParameterPair.Value is IEnumerable<IContentNode>);
                    IEnumerable<IContentNode> sceneXmlInputNodes = (IEnumerable<IContentNode>)inputParameterPair.Value;
                    IEnumerable<String> sceneXmlInputPathnames = 
                        sceneXmlInputNodes.Cast<Content.File>()
                                          .Select(sceneXmlInputFileNode => sceneXmlInputFileNode.AbsolutePath);
                    nonDependencySceneXmlFiles.AddRange(sceneXmlInputPathnames);
                    String[] parameterKeyTokens = inputParameterPair.Key.Split(':');// format is "input_to_process_:<prefix>"
                    Debug.Assert(parameterKeyTokens.Length == 2);
                    inputGroups.Add(new MapMetadataSerialiserMergeInputGroup(parameterKeyTokens[1], sceneXmlInputPathnames));
                }

                // Dependency SceneXml file inputs.
                IEnumerable<String> dependencySceneXmlFiles = allInputSceneXmlFiles.Except(nonDependencySceneXmlFiles);

                // Determine what we are exporting.
                bool exportArchetypes = itypMergeProcess.Parameters.ContainsKey(Constants.ParamMap_ExportArchetypes) ?
                    (bool)itypMergeProcess.Parameters[Constants.ParamMap_ExportArchetypes] : false;
                bool exportEntities = itypMergeProcess.Parameters.ContainsKey(Constants.ParamMap_ExportEntities) ?
                    (bool)itypMergeProcess.Parameters[Constants.ParamMap_ExportEntities] : false;

                IEnumerable<string> forcedItypDependencies = itypMergeProcess.Parameters.ContainsKey(Constants.ParamMap_ForceItypDependency) ? (IEnumerable<string>)itypMergeProcess.Parameters[Constants.ParamMap_ForceItypDependency] : null;

                String taskName = String.Format("Map Metadata [{0}]", xgeTaskName);

                // Create task structure for our new metadata serialiser.
                String assetCombineFilename = String.Empty;
                if (itypMergeProcess.Parameters.ContainsKey(Consts.MetadataSerialiser_AssetCombineDocumentPathnameParameterName))
                    assetCombineFilename = (String)itypMergeProcess.Parameters[Consts.MetadataSerialiser_AssetCombineDocumentPathnameParameterName];
                String mapExportAdditionsFilename = String.Empty;
                if (itypMergeProcess.Parameters.ContainsKey(Consts.MetadataSerialiser_MapExportAdditionsPathnameParameterName))
                    mapExportAdditionsFilename = (String)itypMergeProcess.Parameters[Consts.MetadataSerialiser_MapExportAdditionsPathnameParameterName];

                List<MapMetadataSerialiserManifestDependency> additionalManifestDependencies = new List<MapMetadataSerialiserManifestDependency>();
                if (itypMergeProcess.Parameters.ContainsKey(Consts.MetadataSerialiser_AdditionalManifestDependenciesParameterName))
                    additionalManifestDependencies = (List<MapMetadataSerialiserManifestDependency>)itypMergeProcess.Parameters[Consts.MetadataSerialiser_AdditionalManifestDependenciesParameterName];

                IEnumerable<MapMetadataSerialiserSceneXmlDependency> dependencyData = dependencySceneXmlFiles.
                    Select(scene => new MapMetadataSerialiserSceneXmlDependency(scene));
                
                List<MapMetadataSerialiserOutput> outputData = new List<MapMetadataSerialiserOutput>();

                // Create our base platform which is for backwards compatibility with the current .ityp and imap files.
                MapMetadataSerialiserOutput baseEntry = new MapMetadataSerialiserOutput(
                        xgeTaskName, outputDirectory.AbsolutePath, exportArchetypes, exportEntities, forcedItypDependencies,
                        param.Flags.HasFlag(EngineFlags.Preview), "independent");
                outputData.Add(baseEntry);

                /* Disabled temporarily until we want to create platform specific .ityp and .imap files.
                // Create outputs for all the platforms enabled.
                IEnumerable<ITarget> enabledPlatforms = param.Branch.Targets.Where(t => t.Value.Enabled).Select(t => t.Value);
                foreach (ITarget target in enabledPlatforms)
                {
                    String platformString = PlatformUtils.PlatformToRagebuilderPlatform(target.Platform);
                    String platformOutputPath = SIO.Path.Combine(outputDirectory.AbsolutePath, platformString);

                    MapMetadataSerialiserOutput outputEntry = new MapMetadataSerialiserOutput(
                        xgeTaskName, platformOutputPath, exportArchetypes, exportEntities,
                        param.Flags.HasFlag(EngineFlags.Preview), platformString);

                    outputData.Add(outputEntry);
                }
                */

                List<MapMetadataSerialiserCoreGameMetadataZipDependency> mapMetadataZips = new List<MapMetadataSerialiserCoreGameMetadataZipDependency>();
                String metadataZipFilename = (String)itypMergeProcess.GetParameter(Constants.ParamMap_CoreMetadataZip, String.Empty);
                if (!String.IsNullOrEmpty(metadataZipFilename))
                {
                    mapMetadataZips.Add(new MapMetadataSerialiserCoreGameMetadataZipDependency(metadataZipFilename));
                }

                MapMetadataSerialiserMergeTask mergeTask = new MapMetadataSerialiserMergeTask(
                    xgeTaskName, 
                    inputGroups,
                    dependencyData, additionalManifestDependencies, mapMetadataZips,
                    outputData,
                    assetCombineFilename, mapExportAdditionsFilename);

                // Set XGE parameters for dependency tracking.
                itypMergeProcess.Parameters.Add(Constants.ProcessXGE_Task, task);
                itypMergeProcess.Parameters.Add(Constants.ProcessXGE_TaskName,
                    xgeTaskName.Replace(" ", "_"));

                String cacheDirectory = (String)this.Parameters[Consts.MapProcess_ProcessedMapCacheDirectory];
                cacheDirectory = param.Branch.Environment.Subst(cacheDirectory);
                String configDataFilename = String.Format("map_metadata_serialiser_{0}.xml", xgeTaskName);
                String configDataPathname = SIO.Path.Combine(cacheDirectory, configDataFilename);
                configDataPathname = SIO.Path.GetFullPath(configDataPathname);
                MapMetadataSerialiserConfig configData = new MapMetadataSerialiserConfig(
                    options,
                    new MapMetadataSerialiserMergeTask[1] { mergeTask });
                result &= configData.Serialise(param.Log, configDataPathname);

                // Create XGE task.
                task.Tool = serialiserTool;
                task.Caption = taskName;
                task.InputFiles.Add(configDataPathname);
                if (param.Branch.Project.IsDLC)
                    task.Parameters = String.Format("--config {0} --branch {1} --dlc {2}", configDataPathname, param.Branch.Name, param.Branch.Project.Name);
                else
                    task.Parameters = String.Format("--config {0} --branch {1}", configDataPathname, param.Branch.Name);
                task.SourceFile = configDataPathname;
                taskJobsList.Add(task);
            }

            taskJobs = taskJobsList.ToArray();
            return result;
        }

        /// <summary>
        /// Splits a collection of IProcess instances
        /// </summary>
        private Dictionary<String, IEnumerable<IProcess>> SplitProcessesIntoBuckets(
            IEnumerable<IProcess> processes, int maxTasksPerJob, int lodTaskWeight)
        {
            // This is convoluted as a result of having to support ITYP merge and old school combine based merging.
            // TODO - tidy this up once the ITYP merge is live

            Dictionary<String, IEnumerable<IProcess>> processBucketMap = new Dictionary<String, IEnumerable<IProcess>>();

            // First we determine if we need to split.  We split if
            // 1) We are processing more than one asset combine section
            // 2) We are processing too much stuff (as defined by maxTasksPerJob_)
            SortedSet<String> assetCombineSectionsInvolved = new SortedSet<String>();
            int totalTaskCost = 0;
            bool itypMergeProcessesPresent = false;
            foreach (IProcess process in processes)
            {
                if (process.Parameters.ContainsKey(Consts.MetadataSerialiser_ITYPMergeParameterName))
                {
                    itypMergeProcessesPresent |= (bool)process.Parameters[Consts.MetadataSerialiser_ITYPMergeParameterName];
                }

                if (process.Parameters.ContainsKey(Consts.MetadataSerialiser_AssetCombineDocumentPathnameParameterName))
                    assetCombineSectionsInvolved.Add((String)process.Parameters[Consts.MetadataSerialiser_AssetCombineDocumentPathnameParameterName]);

                if (process.Parameters.ContainsKey(Consts.MetadataSerialiser_IsLODParameterName) == false 
                    || (bool)process.Parameters[Consts.MetadataSerialiser_IsLODParameterName] == false)
                    ++totalTaskCost;
                else
                    totalTaskCost += lodTaskWeight;
            }

            int itypMergeIndex = 1;
            if (itypMergeProcessesPresent || assetCombineSectionsInvolved.Count > 1 || totalTaskCost > maxTasksPerJob)
            {
                 // pass one - split based on asset combine input
                Dictionary<String, List<IProcess>> assetCombineBucketMap = new Dictionary<String, List<IProcess>>();
                foreach (IProcess process in processes)
                {
                    String assetCombineName = "";
                    if (process.Parameters.ContainsKey(Consts.MetadataSerialiser_AssetCombineDocumentPathnameParameterName))
                        assetCombineName = (String)process.Parameters[Consts.MetadataSerialiser_AssetCombineDocumentPathnameParameterName];

                    // We process ITYP merge tasks in one go
                    if (process.Parameters.ContainsKey(Consts.MetadataSerialiser_ITYPMergeParameterName))
                    {
                        bool itypMerge = (bool)process.Parameters[Consts.MetadataSerialiser_ITYPMergeParameterName];
                        if (itypMerge)
                            assetCombineName = String.Format("ITYP Merge Job {0}", itypMergeIndex++);
                    }

                    if (!assetCombineBucketMap.ContainsKey(assetCombineName))
                        assetCombineBucketMap.Add(assetCombineName, new List<IProcess>());

                    assetCombineBucketMap[assetCombineName].Add(process);
                }

                // pass two - split further based on task count (if necessary)
                int bucketIndex = 1;
                foreach (KeyValuePair<String, List<IProcess>> assetCombineBucketPair in assetCombineBucketMap)
                {
                    int bucketSize = 0;
                    List<IProcess> currentBucket = new List<IProcess>();

                    // Consider LOD processes first
                    IEnumerable<IProcess> lodProcesses = assetCombineBucketPair.Value.Where(
                        process => (bool)process.Parameters[Consts.MetadataSerialiser_IsLODParameterName]);
                    foreach (IProcess lodProcess in lodProcesses)
                        currentBucket.Add(lodProcess);
                    if (lodProcesses.Count() > 0)
                        bucketSize += lodTaskWeight + lodProcesses.Count() - 1;

                    // now add regular processes, splitting whenever we need to
                    IEnumerable<IProcess> regularProcesses = assetCombineBucketPair.Value.Where(
                        process => !(bool)process.Parameters[Consts.MetadataSerialiser_IsLODParameterName]);
                    foreach (IProcess regularProcess in regularProcesses)
                    {
                        if (bucketSize >= maxTasksPerJob)
                        {
                            String jobName = String.Format("Map Metadata Serialiser {0}", bucketIndex++);
                            processBucketMap.Add(jobName, currentBucket);
                            bucketSize = 0;
                            currentBucket = new List<IProcess>();
                        }

                        currentBucket.Add(regularProcess);
                        ++bucketSize;
                    }

                    // add any 'stragglers'
                    if (currentBucket.Count > 0)
                    {
                        String jobName = String.Format("Map Metadata Serialiser {0}", bucketIndex++);
                        processBucketMap.Add(jobName, currentBucket);
                    }
                }
            }
            else
            {
                processBucketMap.Add("Map Metadata Serialiser", processes);
            }

            return processBucketMap;
        }
        #endregion // Private Methods

        #region Private Data
        private static readonly int defaultMaxTasksPerJob_ = 25;
        private static readonly int defaultLODTaskWeight_ = 10;
        #endregion Private Data
    }

} // RSG.Pipeline.Processor.Map namespace
