﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.IO;
using System.Linq;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Pipeline.Content;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Attributes;
using RSG.Pipeline.Services;
using RSG.Pipeline.Services.BoundsProcessor;
using Directory = System.IO.Directory;
using XGE = RSG.Interop.Incredibuild.XGE;

namespace RSG.Pipeline.Processor.Map
{

    /// <summary>
    /// Map collision processor.
    /// </summary>
    [Export(typeof(IProcessor))]
    [ProcessorFlags(ProcessorFlags.Compatibility)]
    public class Collision :
        ProcessorBase
    {
        #region Constants
        private static readonly String[] unprocessedFilenameSuffixes_ = { "_collision.zip", ".ibr.zip", "_static_bounds.ibn.zip" };
        private const String CollisionDescription = "Map Collision Processor";
        private const string TransformedDirectory = "transformed_drawables";
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public Collision()
            : base(CollisionDescription)
        {
        }
        #endregion // Constructor(s)

        #region Static Methods
        /// <summary>
        /// Given
        /// </summary>
        internal static bool GetUnprocessedCollisionPathname(IEnumerable<String> pathnames, out String unprocessedCollisionPathname)
        {
            unprocessedCollisionPathname = null;

            // Flo 06.03.14: changing the order of the foreach, so we can actually iterate on the string we want to check first, not on the extensions
            foreach (String pathname in pathnames)
            {
                foreach (String unprocessedCollisionFilenameSuffix in unprocessedFilenameSuffixes_)
                {
                    if (pathname.EndsWith(unprocessedCollisionFilenameSuffix, StringComparison.OrdinalIgnoreCase))
                    {
                        unprocessedCollisionPathname = pathname;
                        return true;
                    }
                }
            }

            return false;
        }
        #endregion

        #region IProcessor Interface Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="process"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="syncDependencies"></param>
        /// <param name="resultantProcesses"></param>
        /// <returns></returns>
        public override bool Prebuild(IEngineParameters param,
            IProcess process, IProcessorCollection processors,
            IContentTree owner, out IEnumerable<IContentNode> syncDependencies, 
            out IEnumerable<IProcess> resultantProcesses)
        {
            Debug.Assert(process.Inputs.OfType<Content.File>().Any());
            Debug.Assert(process.Outputs.First() is Content.Directory);

            ICollection<IProcess> processes = new List<IProcess>();
            process.State = ProcessState.Prebuilt;
            processes.Add(process);

            syncDependencies = new List<IContentNode>();
            resultantProcesses = processes;

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processes"></param>
        /// <param name="tools"></param>
        /// <param name="tasks"></param>
        /// <returns></returns>
        public override bool Prepare(IEngineParameters param, IEnumerable<IProcess> processes,
            out IEnumerable<XGE.ITool> tools, out IEnumerable<XGE.ITask> tasks)
        {
            Map.PreProcess.ResetStaticData();

            bool allow_remote = GetXGEAllowRemote(param, true);
            XGE.ITool serialiserTool = XGEFactory.GetMapProcessorTool(param.Log,
                param.Branch.Project.Config, XGEFactory.MapToolType.BoundsProcessor,
                "Bounds Processor", allow_remote);
            XGE.ITaskJobGroup taskGroup = new XGE.TaskGroup("Bounds Processor");

            int taskCounter = 0;
            foreach (IProcess process in processes)
            {
                // Build the config for the bounds processor
                string configPathname = BuildBoundsProcessorConfig(process);

                Content.Directory output = (Content.Directory)process.Outputs.First();
                string processedMapName = Directory.GetParent(output.AbsolutePath).Name;

                // todo flo : logfile is not used
                String logfile = System.IO.Path.Combine(
                    param.Branch.Project.Config.ToolsLogs,
                    String.Format("bounds_processor_{0}.ulog", processedMapName));

                //string sceneName = Path.GetFileNameWithoutExtension(sourceFile);
                IEnumerable<String> inputPathnames = process.Inputs.OfType<Content.File>().Select(input => input.AbsolutePath);
                IEnumerable<String> inputDrawables = process.Inputs.OfType<Content.Asset>().Select(inputDrawable => inputDrawable.AbsolutePath);
                IEnumerable<String> outputDrawables = process.Outputs.OfType<Content.Asset>().Select(outputDrawable => outputDrawable.AbsolutePath);

                String taskName = String.Format("Bounds Processor {0} [{1}]", taskCounter++, processedMapName);
                XGE.ITaskJob task = new XGE.TaskJob(taskName);
                task.Tool = serialiserTool;
                task.Caption = taskName;
                task.InputFiles.AddRange(inputPathnames);
                task.InputFiles.AddRange(inputDrawables);
                task.OutputFiles.AddRange(outputDrawables);
                task.SourceFile = inputPathnames.First();

                if (param.Branch.Project.IsDLC)
                    task.Parameters = String.Format("--config {0} --branch {1} --dlc {2} --logdir {3}", configPathname, param.Branch.Name, param.Branch.Project.Name, LogFactory.ParentProcessLogDirectory);
                else
                    task.Parameters = String.Format("--config {0} --branch {1} --logdir {2}", configPathname, param.Branch.Name, LogFactory.ParentProcessLogDirectory);

                process.Parameters.Add(Constants.ProcessXGE_Task, task);
                process.Parameters.Add(Constants.ProcessXGE_TaskName, taskName.Replace(" ", "_"));
                taskGroup.Tasks.Add(task);
            }

            // Assign method output.
            tools = new List<XGE.ITool>(new XGE.ITool[] { serialiserTool });
            tasks = new List<XGE.ITask>(new XGE.ITask[] { taskGroup });

            return (true);
        }
        #endregion // IProcessor Interface Methods

        #region Private Methods
        private String BuildBoundsProcessorConfig(IProcess process)
        {
            List<String> additionalImaps = new List<String>();

            process.Inputs.ForEach(input =>
            {
                Asset inputAsset = input as Asset;

                if (inputAsset != null && inputAsset.AssetType == Platform.FileType.IMAP)
                {
                    additionalImaps.Add(inputAsset.AbsolutePath);
                }

            });

            Content.Directory firstDirectoryOutput = (Content.Directory)process.Outputs.First();
            string outputDirectory = firstDirectoryOutput.AbsolutePath;
            DirectoryInfo parentDirectoryInfo = Directory.GetParent(outputDirectory);

            List<BoundsProcessorTask> tasks = new List<BoundsProcessorTask>();
            String manifestAdditionsPathname = (String)process.Parameters[Consts.Collision_ManifestAdditionsPathnameParameterName];
            BoundsProcessorOutput output = null;

            bool propsMerging = (bool)process.Parameters[Consts.Collision_AllInputsAreProps];
            List<BoundsProcessorInput> inputs = new List<BoundsProcessorInput>();

            // Before we construct the xml document we need to determine the input elements and output element
            bool doneParsingInputs = false;
            int inputIndex = 0;
            while (!doneParsingInputs)
            {
                // overrides for the regular packing. Those will be changed in case of props packing
                string name = parentDirectoryInfo.Name;
                string outputDir = outputDirectory;

                String sceneXmlParameterName = String.Format("{0}{1}", Consts.Collision_SceneXmlParameterPrefix, inputIndex);
                if (process.Parameters.ContainsKey(sceneXmlParameterName))
                {
                    String sceneXmlPathname = (String)process.Parameters[sceneXmlParameterName];
                    String sceneTypeParameterName = String.Format("{0}{1}", Consts.Collision_SceneTypeParameterPrefix, inputIndex);
                    String sceneType = (String)process.Parameters[sceneTypeParameterName];
                    String unprocessedBoundsPathnameParameterName = String.Format("{0}{1}", Consts.Collision_UnprocessedBoundsZipParameterPrefix, inputIndex);

                    String drawableListParameterName = String.Format("{0}{1}", Consts.Collision_DrawableListParameterPrefix, inputIndex);
                    String drawableList = String.Empty;
                    String inputDir = String.Empty;
                    string boundPathName = "";

                    if (process.Parameters.ContainsKey(drawableListParameterName))
                    {
                        drawableList = (String)process.Parameters[drawableListParameterName];
                        inputDir = parentDirectoryInfo.FullName;
                    }

                    if (process.Parameters.ContainsKey(unprocessedBoundsPathnameParameterName))
                    {
                        String unprocessedBoundsPathname = (String)process.Parameters[unprocessedBoundsPathnameParameterName];
                        boundPathName = unprocessedBoundsPathname;

                        // B*1699820 merge props for DLC in one RPF.
                        // Flo: if there are drawables to pack it's because props are packed
                        // so I needed to hack that
                        // sorry future me (or anyone else) :(
                        if (drawableList != "")
                        {
                            DirectoryInfo propDirectory = Directory.GetParent(unprocessedBoundsPathname);
                            inputDir = propDirectory.FullName;
                            name = propDirectory.Name;
                            //outputDir = Path.Combine(outputDirectory, name);
                        }
                    }
                    inputs.Add(new BoundsProcessorInput(sceneXmlPathname, sceneType, boundPathName, inputDir, drawableList, additionalImaps));

                    ++inputIndex;
                }
                else
                {
                    doneParsingInputs = true;
                }

                // B*1699820 merge props for DLC in one RPF.
                // one task per source prop
                if (propsMerging && inputs.Any())
                {
                    output = new BoundsProcessorOutput(name, outputDir, manifestAdditionsPathname);
                    tasks.Add(new BoundsProcessorTask(inputs, output));

                    // resetting the inputs list to generate tasks for every input
                    inputs = new List<BoundsProcessorInput>(); 
                }
            }

            // RELATED TO
            // B*1699820 merge props for DLC in one RPF.
            // Here we test the opposite as just above. If we're not in a prop merging case, we need to pack all inputs in one task
            if (!propsMerging && inputs.Any())
            {
                output = new BoundsProcessorOutput(parentDirectoryInfo.Name, outputDirectory, manifestAdditionsPathname);
                tasks.Add(new BoundsProcessorTask(inputs, output));
            }

            BoundsProcessorOptions defaultOptions = new BoundsProcessorOptions();

            string configPathname = Path.Combine(parentDirectoryInfo.FullName, "bounds_processor.xml");
            BoundsProcessorConfig config = new BoundsProcessorConfig(defaultOptions, tasks);
            config.Save(configPathname);

            return configPathname;
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Processor.Map namespace
