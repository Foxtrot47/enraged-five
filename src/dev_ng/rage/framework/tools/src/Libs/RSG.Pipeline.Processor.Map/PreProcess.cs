﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.IO;
using System.Linq;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Model.MaterialPresets;
using RSG.Pipeline.Content;
using RSG.Pipeline.Content.Algorithm;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Attributes;
using RSG.Pipeline.Services;
using RSG.Pipeline.Services.AssetPack;
using RSG.Pipeline.Services.Metadata;
using RSG.Platform;
using RSG.SceneXml;
using RSG.SceneXml.MapExport;
using RSG.SceneXml.Material;
using File = RSG.Pipeline.Content.File;
using XGE = RSG.Interop.Incredibuild.XGE;

namespace RSG.Pipeline.Processor.Map
{
    /// <summary>
    /// Map preprocessor; this processor determines all of the map dependencies.
    /// </summary>
    [Export(typeof(IProcessor))]
    [ProcessorFlags(ProcessorFlags.Compatibility)]
    public class PreProcess :
        MapProcessorBase,
        IProcessor
    {
        #region Constants
        private static readonly String description_ = "Map Preprocessor";
        private static readonly String logContext_ = "Map PreProcess";
        private static readonly String animationWorkingDirectoryName_ = "map_animation";
        private static readonly String animationGroupDirectoryName_ = "group_2";
        private static readonly String animationCompressedDirectoryName_ = "compressed";
        private static readonly String metadataDirectoryName_ = "map_metadata";
        private static readonly String instancePlacementDirectoryName_ = "instance_placement";
        private static readonly String processedBoundsDirectoryName_ = "processed_bounds";
        private static readonly String transformedDrawablesDirectoryName_ = "transformed_drawables";
        private static readonly String processedMapAssetPackScheduleFilename_ = "map_asset_pack_schedule.xml";
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public PreProcess()
            : base(description_)
        {
        }
        #endregion // Constructor(s)

        #region IProcessor Interface Methods
        /// <summary>
        /// Map preprocess prebuild.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="process"></param>
        /// <param name="processors"></param>
        /// <param name="targetContentTree"></param>
        /// <param name="syncDependencies"></param>
        /// <param name="resultantProcesses"></param>
        /// <returns></returns>
        /// For any given map; we construct the following processes:
        ///  1. A map metadata process
        ///  2. A collision process
        ///  3. Where required, an asset pack process for merged content (e.g. dt1_01 => downtown)
        ///  4. An asset pack process for the processed content
        ///  5. A rage process to convert processed intermediate data to platform data
        ///
        public override bool Prebuild(IEngineParameters param,
            IProcess process, IProcessorCollection processors,
            IContentTree targetContentTree, out IEnumerable<IContentNode> syncDependencies,
            out IEnumerable<IProcess> resultantProcesses)
        {
            HashSet<IContentNode> syncDependenciesCollection = new HashSet<IContentNode>();
            Stopwatch prebuildStopwatch = Stopwatch.StartNew();

            // Set trivial out parameters to allow
            resultantProcesses = new IProcess[0];
            syncDependencies = new IContentNode[0];

            if (!ValidateProcess(param, process))
                return false;

            this.LoadParameters(param);

            bool buildingProcessedZips = BuildingProcessedZips(param);

            // If the inputs this process has have already been considered, we just ignore it
            if (InterContainerLODDependencies.Coordinator.Instance.HasProcessBeenConsidered(process))
            {
                process.State = ProcessState.Discard;
                return true;
            }

            Content.Asset outputAsset = (Content.Asset)process.Outputs.First(); // This assumption is validated in ValidateProcess()
            String ouputPathname = param.Branch.Environment.Subst(outputAsset.AbsolutePath);

			ContentTreeHelper contentTreeHelper = new ContentTreeHelper(process.Owner);
            IContentNode[] sceneXmlInputNodes = contentTreeHelper.GetSceneXmlNodesFromExportZipNodes(process.Inputs);
            IContentNode metadataMergeSplitNode = contentTreeHelper.GetMetadataMergeNodeFromSceneXmlNodes(sceneXmlInputNodes);
            MetadataMergeSplit.MergeSplitDetails metadataMergeSplitDetails = null;

            String mapExportAdditionsPathname = Path.Combine(param.Branch.Project.Cache, param.Branch.Name, "manifest", process.Outputs.First().Name, "MapExportAdditions.xml");
            String collisionManifestAdditionsPathname = Path.Combine(param.Branch.Project.Cache, param.Branch.Name, "manifest", process.Outputs.First().Name, "BoundsProcessorAdditions.xml");
            String audioCollisionMapPathname = Path.Combine(param.CoreBranch.Assets, "maps", "ModelAudioCollisionList.csv");
            String sceneOverridesDirectory = Path.Combine(param.CoreBranch.Assets, "maps", "scene_overrides");

            // Create Metadata Serialiser Process
            // If we are merging this metadata then we leave the process builder as null to prevent any further work being done
            ProcessBuilder metadataProcessBuilder = null;
            if (metadataMergeSplitNode == null)
            {
                if (!CreateMetadataSerialiserProcessBuilder(
                    param, process, processors, targetContentTree,
                    syncDependenciesCollection,
                    mapExportAdditionsPathname, audioCollisionMapPathname, sceneOverridesDirectory,
                    out metadataProcessBuilder))
                {
                    param.Log.ErrorCtx(logContext_, "Failed to create metadata serialiser process.");
                    return false;
                }
            }
            else
            {
                mapExportAdditionsPathname = String.Empty;
                if (!MetadataMergeSplit.Coordinator.Instance.HasMergeNodeBeenConsidered(metadataMergeSplitNode))
                    metadataMergeSplitDetails = MetadataMergeSplit.Coordinator.Instance.GetMergeDetails(metadataMergeSplitNode, param, contentTreeHelper);
            }

            // Forward on our inputs to the inter container LOD dependency coordinator
            bool haveInterContainerLODDependencies = InterContainerLODDependencies.Coordinator.Instance.ProcessHasDependencies(param, contentTreeHelper, process);

            // Forward on our inputs to the asset combine coordinator
            String mapCacheDirectoryParamValue = (String)this.Parameters[Consts.MapProcess_SourceMapCacheDirectory];
            String mapCacheDirectory = System.IO.Path.GetFullPath(param.Branch.Environment.Subst(mapCacheDirectoryParamValue));
            String combineDocumentPathname;
            if (!AssetCombine.Coordinator.Instance.ConsiderInputs(
                param, targetContentTree, contentTreeHelper,
                process.Inputs, mapCacheDirectory,
                out combineDocumentPathname))
            {
                param.Log.ErrorCtx(logContext_, "Asset Combine failed");
                return false;
            }

            // Set the asset combine parameter
            if (metadataProcessBuilder != null)
            {
                if (!String.IsNullOrEmpty(combineDocumentPathname))
                {
                    metadataProcessBuilder.Parameters.Add(
                        Consts.MetadataSerialiser_AssetCombineDocumentPathnameParameterName,
                        combineDocumentPathname);
                }

                // Set the is_lod parameter
                bool isLODContainer = false;
                foreach (IContentNode inputNode in process.Inputs)
                    isLODContainer |= contentTreeHelper.ExportNodeIsLODContainer(inputNode);
                metadataProcessBuilder.Parameters.Add(Consts.MetadataSerialiser_IsLODParameterName, isLODContainer);
            }

            // Create Collision Process for map.
            ProcessBuilder collisionProcessBuilder = null;
            ProcessBuilder collisionPackProcessBuilder = null;

            bool processCollision = true;
            foreach (IContentNode inputNode in process.Inputs)
                processCollision = processCollision ? inputNode.GetParameter(Constants.Map_Process_Collision, true) : processCollision;

            if (processCollision)
            {
                if (!CreateCollisionProcessBuilder(param, process, processors, targetContentTree,
                    collisionManifestAdditionsPathname, mapCacheDirectory,
                    buildingProcessedZips,
                    out collisionProcessBuilder, out collisionPackProcessBuilder))
                {
                    param.Log.ErrorCtx(logContext_, "Failed to create collision process.");
                    return false;
                }
            }
            else
            {
                param.Log.MessageCtx(logContext_, "Container content has been tagged to exclude collision.  No collision will be generated.");
            }

            ICollection<ProcessBuilder> materialPresetProcessBuilders = new List<ProcessBuilder>();

            // Loop through each of our inputs; these represent map zips.
            List<IContentNode> outputFiles = new List<IContentNode>();
            List<String> outputDirectories = new List<String>();
            List<IContentNode> metadataInputsToProcess = new List<IContentNode>();
            List<IContentNode> unprocessedCollisionInputNodes = new List<IContentNode>();
            List<IContentNode> clipDictionaryNodes = new List<IContentNode>();
            List<IContentNode> parentTextureNodes = new List<IContentNode>();
            HashSet<IContentNode> exportXMLDependencies = new HashSet<IContentNode>();
            this.ExtractAndProcessInputs(param,
                process,
                processors,
                contentTreeHelper,
                targetContentTree,
                metadataProcessBuilder,
                collisionProcessBuilder,
                materialPresetProcessBuilders,
                syncDependenciesCollection,
                outputFiles,
                outputDirectories,
                exportXMLDependencies,
                metadataInputsToProcess,
                unprocessedCollisionInputNodes,
                clipDictionaryNodes,
				parentTextureNodes);

            if (process.State == ProcessState.Discard)
            {
                return true;
            }

            // Create Instance Placement Process.
            ProcessBuilder instanceMetadataProcessBuilder = null;
            List<ProcessBuilder> instancePlacementProcessBuilders = null;
            List<MapMetadataSerialiserManifestDependency> additionalManifestDependencies = new List<MapMetadataSerialiserManifestDependency>();

            this.PrepareInstancePlacement(param, process, processors, targetContentTree, contentTreeHelper, exportXMLDependencies,
                collisionProcessBuilder, metadataProcessBuilder, instanceMetadataProcessBuilder, additionalManifestDependencies, outputFiles,
                outputDirectories, metadataMergeSplitDetails, mapExportAdditionsPathname, ref metadataInputsToProcess, ref instancePlacementProcessBuilders);

            //Process the parent texture dictionary, if enabled.
            List<IProcess> parentTextureDictionaryProcesses = null;

            if (this.IsMapGtxdPipelineEnabled(param.Branch))
            {
                CreateParentTextureDictionaryProcessBuilder(param, process, processors,
                    targetContentTree, contentTreeHelper, parentTextureNodes, out parentTextureDictionaryProcesses);
            }

            // Process any clip dictionaries
            List<ProcessBuilder> clipProcessBuilders = new List<ProcessBuilder>();
            List<IContentNode> processedClipDictionaryFileNodes = new List<IContentNode>();
            ProcessClipDictionaryInputs(
                param, processors, targetContentTree,
                clipDictionaryNodes.OfType<Content.File>(), clipProcessBuilders, processedClipDictionaryFileNodes);

            // Add LOD link dependencies
            if (metadataProcessBuilder != null)
            {
                IContentNode[] lodLinkDependencyNodes = GetLODLinkDependencyNodes(process, contentTreeHelper);
                if (lodLinkDependencyNodes.Length > 0)
                    metadataProcessBuilder.Inputs.AddRange(lodLinkDependencyNodes);

                // Setup metadata serialiser input parameters
                for (int metadataInputIndex = 0; metadataInputIndex < metadataInputsToProcess.Count; ++metadataInputIndex)
                {
                    String inputToProcessKey = String.Format("{0}{1}", Consts.MetadataSerialiser_InputToProcessParameterPrefix, metadataInputIndex);
                    metadataProcessBuilder.Parameters.Add(inputToProcessKey, metadataInputsToProcess[metadataInputIndex]);
                }
            }

            foreach (IContentNode unprocessedCollisionInputNode in unprocessedCollisionInputNodes)
                collisionProcessBuilder.Inputs.Add(unprocessedCollisionInputNode);

            // Create Asset Pack Process for processed map zip.
            ProcessBuilder assetPackProcessBuilder;
            if (!CreateAssetPackProcessBuilder(param, process, processors, targetContentTree, out assetPackProcessBuilder))
            {
                param.Log.ErrorCtx(logContext_, "Failed to create asset pack process.");
                return false;
            }

            // add RAGE processors to kick the next stage off
            ProcessBuilder rageProcessBuilder;
            String rpfSortMetadataDirectory = String.Empty;
            if (null != metadataMergeSplitNode)
            {
                rpfSortMetadataDirectory = GetProcessedZipAssetCacheDir(param, metadataMergeSplitNode as Content.File);
                rpfSortMetadataDirectory = Path.Combine(rpfSortMetadataDirectory, "map_metadata");
            }
            else
            {
                rpfSortMetadataDirectory = GetProcessedZipAssetCacheDir(param, process.Outputs.First() as Content.File);
                // Since the metadata split we've not been sorting prop RPF data according to their ITYP files.
                // The new Map PreProcess sorts this by storing these things in an object!
                rpfSortMetadataDirectory = Path.Combine(rpfSortMetadataDirectory, "map_metadata");
            }

            if (!CreateRAGEProcessBuilder(param, process, processors, targetContentTree,
                process.Outputs.First(), rpfSortMetadataDirectory,
                mapExportAdditionsPathname,
                collisionManifestAdditionsPathname,
                out rageProcessBuilder))
            {
                param.Log.ErrorCtx(logContext_, "Failed to create RAGE process.");
                return false;
            }

            String processedDestination = GetProcessedZipAssetCacheDir(param, outputAsset);
            if (System.IO.Directory.Exists(processedDestination) == false)
            {
                System.IO.Directory.CreateDirectory(processedDestination);
            }
            String metadataDirectory = System.IO.Path.Combine(processedDestination, metadataDirectoryName_);
            String boundsDirectory = System.IO.Path.Combine(processedDestination, processedBoundsDirectoryName_);
            String assetPackSchedulePathname = System.IO.Path.Combine(processedDestination, processedMapAssetPackScheduleFilename_);
            if (metadataMergeSplitNode == null)
                outputDirectories.Add(metadataDirectory);

            // Set up the outputs for the asset pack and the rage process
            AssetPackScheduleBuilder assetPackScheduleBuilder = new AssetPackScheduleBuilder();
            AssetPackZipArchive outputZipArchive = assetPackScheduleBuilder.AddZipArchive(ouputPathname);

            foreach (IContentNode outputFile in outputFiles)
            {
                outputZipArchive.AddFile((outputFile as IFilesystemNode).AbsolutePath);
                rageProcessBuilder.Inputs.Add(outputFile);
            }

            foreach (String outputDirectory in outputDirectories)
            {
                outputZipArchive.AddDirectory(outputDirectory);
                rageProcessBuilder.Inputs.Add(targetContentTree.CreateDirectory(outputDirectory));
            }

            outputZipArchive.AddDirectory(boundsDirectory, null, "*.ibn.zip");
            outputZipArchive.AddDirectory(boundsDirectory, null, "*.ibd.zip");
            // B* 1229899 - we don't send IBD.ZIPs to Rage any more
            rageProcessBuilder.Inputs.Add(targetContentTree.CreateDirectory(boundsDirectory, "*.ibn.zip"));

            // Add any processed clip dictionaries to the processed zip and the rage process
            foreach (Content.File processedClipDictionaryFileNode in processedClipDictionaryFileNodes.OfType<Content.File>())
            {
                outputZipArchive.AddFile(processedClipDictionaryFileNode.AbsolutePath);
                rageProcessBuilder.Inputs.Add(processedClipDictionaryFileNode);
            }

            assetPackScheduleBuilder.Save(assetPackSchedulePathname);
            IContentNode assetPackScheduleNode = targetContentTree.CreateFile(assetPackSchedulePathname);
            assetPackProcessBuilder.Inputs.Add(assetPackScheduleNode);

            ICollection<IProcess> processCollection = new List<IProcess>();

            if (instancePlacementProcessBuilders != null)
            {
                foreach (ProcessBuilder instancePlacementProcessBuilder in instancePlacementProcessBuilders)
                    processCollection.Add(instancePlacementProcessBuilder.ToProcess());
            }

            if (instanceMetadataProcessBuilder != null)
            {
                processCollection.Add(instanceMetadataProcessBuilder.ToProcess());
            }

            if (parentTextureDictionaryProcesses != null)
            {
                processCollection.AddRange(parentTextureDictionaryProcesses);
            }

            // Material Preset processes.
            if (materialPresetProcessBuilders.Count > 0)
                materialPresetProcessBuilders.ForEach(materialProcess => processCollection.Add(materialProcess.ToProcess()));

            if (metadataProcessBuilder != null)
            {
                //Add instance placement files to the asset package.
                if (additionalManifestDependencies.Any())
                {
                    metadataProcessBuilder.Parameters.Add(Consts.MetadataSerialiser_AdditionalManifestDependenciesParameterName, additionalManifestDependencies);
                }

                processCollection.Add(metadataProcessBuilder.ToProcess());
            }
            if (metadataMergeSplitDetails != null)
            {
                // Create a metadata processor for the merge/split
                ProcessBuilder metadataMergeSplitProcessBuilder;
                Content.Directory metadataOutputDirectoryNode;
                if (!CreateMetadataSerialiserProcessBuilderForMergeSplit(
                    param, processors, contentTreeHelper, targetContentTree,
                    syncDependenciesCollection,
                    metadataMergeSplitDetails, combineDocumentPathname, audioCollisionMapPathname, sceneOverridesDirectory,
                    out metadataMergeSplitProcessBuilder, out metadataOutputDirectoryNode))
                {
                    param.Log.ErrorCtx(logContext_, "Failed to create metadata serialiser process for merge.");
                    return false;
                }

                //Add instance placement files to the asset package.
                List<MapMetadataSerialiserManifestDependency> mergedManifestDependencies = new List<MapMetadataSerialiserManifestDependency>();
                if (additionalManifestDependencies.Any() && false) //NOTE:  Turning this off as we don't want instance data to be in the map metadata RPF at the moment.
                {
                    mergedManifestDependencies = additionalManifestDependencies;
                    metadataMergeSplitProcessBuilder.Parameters.Add(Consts.MetadataSerialiser_AdditionalManifestDependenciesParameterName, mergedManifestDependencies);
                }

                processCollection.Add(metadataMergeSplitProcessBuilder.ToProcess());

                // Get the processes resulting from any merge
                IProcess[] mergedMetadataContentProcesses = MetadataMergeSplit.Coordinator.Instance.BuildMergedContentProcesses(
                    metadataMergeSplitDetails, metadataOutputDirectoryNode, mapCacheDirectory, mergedManifestDependencies,
                    processors, process.Owner, targetContentTree, buildingProcessedZips);
                processCollection.AddRange(mergedMetadataContentProcesses);
            }

            if (processCollision)
            {
                // collision processes
                processCollection.Add(collisionProcessBuilder.ToProcess());
                if (null != collisionPackProcessBuilder)
                    processCollection.Add(collisionPackProcessBuilder.ToProcess());
            }

            // processed data asset pack processes
            bool hasPropContainerInput = process.Inputs.Any(input => contentTreeHelper.ExportNodeIsPropContainer(input));
            // TEMP - disabled processed zip creation because of packed TIF sizes
            //if (buildingProcessedZips || hasPropContainerInput)
            if (hasPropContainerInput)
                processCollection.Add(assetPackProcessBuilder.ToProcess());

            // ragebuilder process
            processCollection.Add(rageProcessBuilder.ToProcess());

            // clip processes
            foreach (ProcessBuilder clipProcessBuilder in clipProcessBuilders)
                processCollection.Add(clipProcessBuilder.ToProcess());

            // Get the processes resulting from any Asset Combines
            IProcess[] mergedContentProcesses = AssetCombine.Coordinator.Instance.BuildMergedContentProcesses(param, processors, process.Owner, targetContentTree);
            processCollection.AddRange(mergedContentProcesses);

            // Get the processes resulting from any inter container LOD dependencies (of required)
            if (haveInterContainerLODDependencies)
            {
                IProcess[] dependentProcesses = InterContainerLODDependencies.Coordinator.Instance.GetDependentProcesses(process);
                processCollection.AddRange(dependentProcesses);
            }

            // As the MSS has a fallback in place to load all prop and interior data, we need to sync it all here.
            // This makes Jonny very sad.
            IContentNode[] propSceneXmlNodes = contentTreeHelper.GetAllPropSceneXmlNodes();
            syncDependenciesCollection.AddRange(propSceneXmlNodes);
            IContentNode[] interiorSceneXmlNodes = contentTreeHelper.GetAllInteriorSceneXmlNodes();
            syncDependenciesCollection.AddRange(interiorSceneXmlNodes);

            // Additional sync dependencies
            IContentNode audioCollisionMapFileNode = targetContentTree.CreateFile(audioCollisionMapPathname);
            IContentNode sceneOverridesDirectoryNode = targetContentTree.CreateDirectory(sceneOverridesDirectory);
            syncDependenciesCollection.Add(audioCollisionMapFileNode);
            syncDependenciesCollection.Add(sceneOverridesDirectoryNode);

            // Add scene metadata to the dependencies to force a metadata sync before any process
            syncDependenciesCollection.Add(targetContentTree.CreateDirectory(Path.Combine(param.CoreBranch.Metadata, "definitions/game/scene"), "*.*", true));
            syncDependenciesCollection.Add(targetContentTree.CreateDirectory(Path.Combine(param.CoreBranch.Metadata, "definitions/rage/framework/src/fwscene"), "*.*", true));

            process.State = ProcessState.Discard;
            resultantProcesses = processCollection;
            syncDependencies = syncDependenciesCollection;

            prebuildStopwatch.Stop();
            string contentName = System.IO.Path.GetFileNameWithoutExtension(outputAsset.AbsolutePath);
            param.Log.MessageCtx(logContext_, "Prebuild for '{0}' took {1} ms", contentName, prebuildStopwatch.ElapsedMilliseconds);

            return true;
        }

        /// <summary>
        /// Prepare; this should never get called.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processes"></param>
        /// <param name="tools"></param>
        /// <param name="tasks"></param>
        /// <returns></returns>
        public override bool Prepare(IEngineParameters param, IEnumerable<IProcess> processes,
            out IEnumerable<XGE.ITool> tools, out IEnumerable<XGE.ITask> tasks)
        {
            // This should never get hit; the preprocess just creates other
            // processes.
            throw (new NotImplementedException());
        }
        #endregion // IProcessor Interface Methods

        #region Private Methods

        #region MAP Gtxd Enabled Methods
        /// <summary>
        /// Checks if the global parameter is activated or the current branch is activated
        /// </summary>
        /// <param name="branch">branch to check</param>
        /// <returns>>True is the MAP Gtxd is enabled</returns>
        private bool IsMapGtxdPipelineEnabled(IBranch branch)
        {
            // check if the global parameter is enabled
            if (this.GetParameter(Consts.MapProcess_MapGTXDPipelineEnabled, false))
            {
                return true;
            }

            // verify that the current branch is in the list of available branches and projects
            if (IsMapGtxdPipelineEnabledForBranch(branch))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Checks if the branch and its project are activated
        /// </summary>
        /// <param name="branch">branch to check</param>
        /// <returns>True is the MAP Gtxd is enabled for the branch</returns>
        private bool IsMapGtxdPipelineEnabledForBranch(IBranch branch)
        {
            string branchesParameter = this.GetParameter(Consts.MapProcess_MapGTXDPipelineEnabledPerBranch, string.Empty);
            if (string.IsNullOrWhiteSpace(branchesParameter))
            {
                return false;
            }

            ISet<string> branches = branchesParameter.Split(';').ToSet();
            if (!branches.Contains(branch.Name))
            {
                return false;
            }

            // check if we are restricting the current branch to a set of projects, use case is that we want to process with only a selected set of DLCs
            if (IsMapGtxdPipelineEnabledForProject(branch.Project))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Checks is the project's name is in the list of activated projects
        /// </summary>
        /// <param name="project">project to check</param>
        /// <returns>true if the project is in the list or if there is no list</returns>
        /// <remarks>this function is to be used from IsMapGtxdPipelineEnabledForBranch, all other use cases are not really safe</remarks>
        private bool IsMapGtxdPipelineEnabledForProject(IProject project)
        {
            string projectParameter = this.GetParameter(Consts.MapProcess_MapGTXDPipelineEnabledPerProject, string.Empty);
            if (string.IsNullOrWhiteSpace(projectParameter))
            {
                return true;
            }

            ISet<string> projects = projectParameter.Split(';').ToSet();
            if (projects.Contains(project.Name))
            {
                return true;
            }

            return false;
        }
        #endregion // MAP Gtxd Enabled Methods

        #region Dependency Methods
        private IContentNode[] GetLODLinkDependencyNodes(IProcess process, ContentTreeHelper contentTreeHelper)
        {
            HashSet<IContentNode> lodLinkDependencyNodes = new HashSet<IContentNode>();

            IContentNode[] lodContainerInputs = process.Inputs.Where(
                input => contentTreeHelper.ExportNodeIsLODContainer(input)).ToArray();
            IContentNode[] mapContainerInputs = process.Inputs.Where(
                input => contentTreeHelper.ExportNodeIsMapContainer(input)).ToArray();

            if (lodContainerInputs.Length > 0)
            {
                // Get all export nodes that share a combine output and are "map_container"
                IContentNode combineOutputNode = contentTreeHelper.GetCombineOutputForNodes(lodContainerInputs);
                if (combineOutputNode != null)
                {
                    HashSet<IContentNode> combineInputs = contentTreeHelper.GetAllInputsForNode(combineOutputNode);
                    IContentNode[] mapContainerSiblingNodes = combineInputs.Where(
                        input => contentTreeHelper.ExportNodeIsMapContainer(input)).ToArray();
                    foreach (Content.IFilesystemNode mapContainerSiblingNode in mapContainerSiblingNodes.OfType<Content.IFilesystemNode>())
                    {
                        String sceneXmlPathname = System.IO.Path.ChangeExtension(mapContainerSiblingNode.AbsolutePath, "xml");
                        IContentNode sceneXmlContentNode = contentTreeHelper.CreateAsset(sceneXmlPathname, Platform.Platform.Independent);
                        lodLinkDependencyNodes.Add(sceneXmlContentNode);
                    }
                }
            }
            else if (mapContainerInputs.Length > 0)
            {
                // Get all export nodes that share a combine output and are "lod_container"
                IContentNode combineOutputNode = contentTreeHelper.GetCombineOutputForNodes(mapContainerInputs);
                if (combineOutputNode != null)
                {
                    HashSet<IContentNode> combineInputs = contentTreeHelper.GetAllInputsForNode(combineOutputNode);
                    IContentNode[] mapContainerSiblingNodes = combineInputs.Where(
                        input => contentTreeHelper.ExportNodeIsLODContainer(input)).ToArray();
                    foreach (Content.IFilesystemNode mapContainerSiblingNode in mapContainerSiblingNodes.OfType<Content.IFilesystemNode>())
                    {
                        String sceneXmlPathname = System.IO.Path.ChangeExtension(mapContainerSiblingNode.AbsolutePath, "xml");
                        IContentNode sceneXmlContentNode = contentTreeHelper.CreateAsset(sceneXmlPathname, Platform.Platform.Independent);
                        lodLinkDependencyNodes.Add(sceneXmlContentNode);
                    }
                }
            }

            return lodLinkDependencyNodes.ToArray();
        }

        /// <summary>
        /// Walk the SceneXml scene gathering dependencies.
        /// </summary>
        /// <param name="scene"></param>
        /// <param name="process"></param>
        /// <param name="maxFileDependencies"></param>
        /// <param name="exportXmlFileDependencies"></param>
        /// <param name="processedZipDependencies"></param>
        /// <param name="materialDependencies"></param>
        /// <param name="exportZipFileDependencies"></param>
        private void GetSceneDependencies(Scene scene,
            IProcess process,
            ISet<IContentNode> exportXmlFileDependencies,
            ISet<IContentNode> processedZipDependencies,
            ISet<IContentNode> materialDependencies)
        {
            ISet<IContentNode> maxFileDependencies = new HashSet<IContentNode>();

            // Loop through Scene objects; pulling out dependencies.
            foreach (TargetObjectDef obj in scene.Walk(Scene.WalkMode.DepthFirst))
            {
                if (obj.IsRefObject())
                {
                    if (obj.RefFile == null)
                        continue;

                    IContentNode refFileNode = process.Owner.CreateFile(obj.RefFile);

                    // no point going further if we've already added this dependency
                    if (!maxFileDependencies.Add(refFileNode))
                    {
                        continue;
                    }

                    // we don't care about the bounds of interiors, only props (for baking)
                    if (obj.IsMiloTri())
                    {
                        continue;
                    }

                    // Flo: Moved that from before the IsMiloTri() test to after. The exportZipFileDependencies was not used in later code
                    // (whether here or outside, that's why i moved it here in the first place
                    IEnumerable<IProcess> processesWithRefFileInput = process.Owner.FindProcessesWithInput(refFileNode);
                    IEnumerable<File> exportZipFiles = processesWithRefFileInput.
                        SelectMany(p => p.Outputs).
                        OfType<File>().
                        Where(n => n.AbsolutePath.EndsWith(".zip"));

                    foreach (File file in exportZipFiles)
                    {
                        String xmlPathname = Path.ChangeExtension(file.AbsolutePath, "xml");
                        exportXmlFileDependencies.Add(process.Owner.CreateFile(xmlPathname));

                        IEnumerable<IProcess> processesWithExportZipInput = process.Owner.FindProcessesWithInput(file);
                        IEnumerable<File> processedZipFiles = processesWithExportZipInput.
                            SelectMany(p => p.Outputs).
                            OfType<File>().
                            Where(n => n.AbsolutePath.EndsWith(".zip"));
                        processedZipDependencies.AddRange(processedZipFiles);
                    }
                }
            }

            // DHM TODO: for full source texture export/blackness/Parent TXD
            // support we will need to know the source textures.
#if false
    // Loop through materials gathering source texture filenames.
			GetSceneMaterialDependencies(owner, scene.Materials, materialDependencies);
#endif
        }

        /// <summary>
        /// Walk the scene to find any dynamic objects
        /// </summary>
        /// <param name="scene"></param>
        /// <param name="objectList"></param>
        private void GetSceneDynamicObjects(Scene scene, ICollection<String> objectList)
        {
            foreach (TargetObjectDef obj in scene.Walk(Scene.WalkMode.DepthFirst))
            {
                // B* 1336672 - IsPartOfAnEntitySet() is non-trivial so we must avoid doing this where possible
                if (obj.IsObject() && (obj.IsDynObject() || obj.IsPartOfAnEntitySet()))
                {
                    objectList.Add(obj.Name.ToLower());
                }
            }
        }

        /// <summary>
        /// Walk the material definition list.
        /// </summary>
        /// <param name="materials"></param>
        /// <param name="dependencies"></param>
        private void GetSceneMaterialDependencies(IContentTree owner, IEnumerable<MaterialDef> materials, ISet<IContentNode> dependencies)
        {
            foreach (MaterialDef material in materials)
            {
                if (material.HasSubMaterials)
                    GetSceneMaterialDependencies(owner, material.SubMaterials, dependencies);
                else
                {
                    dependencies.AddRange(material.Textures.Select(texture =>
                        owner.CreateFile(texture.FilePath)));
                }
            }
        }
        #endregion // Dependency Methods

        /// <summary>
        /// 
        /// </summary>
        private bool PrepareInstancePlacement(IEngineParameters param,
            IProcess process, IProcessorCollection processors, IContentTree targetContentTree,
            ContentTreeHelper contentTreeHelper, HashSet<IContentNode> exportXMLDependencies,
            ProcessBuilder collisionProcessBuilder, ProcessBuilder metadataProcessBuilder,
            ProcessBuilder instanceMetadataProcessBuilder,
            List<MapMetadataSerialiserManifestDependency> additionalManifestDependencies,
            List<IContentNode> outputFiles, List<String> outputDirectories,
            MetadataMergeSplit.MergeSplitDetails metadataMergeSplitDetails,
            String mapExportAdditionsPathname,
            ref List<IContentNode> metadataInputsToProcess,
            ref List<ProcessBuilder> instancePlacementProcessBuilders)
        {
            if (this.Parameters.ContainsKey(Consts.MapProcess_MapInstancePlacementPipelineEnabled)
                && ((bool)this.Parameters[Consts.MapProcess_MapInstancePlacementPipelineEnabled] == true))
            {
                List<IContentNode> metadataOutputsToProcess = new List<IContentNode>();
                if (CreateInstancePlacementProcessBuilder(param, process, processors,
                    targetContentTree, contentTreeHelper, metadataInputsToProcess, exportXMLDependencies,
                    out metadataOutputsToProcess, out instancePlacementProcessBuilders))
                {
                    //Add the instance placement outputs into the bound processor.
                    foreach (ProcessBuilder instancePlacementProcessBuilder in instancePlacementProcessBuilders)
                        collisionProcessBuilder.Inputs.AddRange(instancePlacementProcessBuilder.Outputs);

                    if (metadataProcessBuilder != null)
                    {
                        //Add the instance placement outputs to the map metadata serialiser.
                        foreach (ProcessBuilder instancePlacementProcessBuilder in instancePlacementProcessBuilders)
                            metadataProcessBuilder.Inputs.AddRange(instancePlacementProcessBuilder.Outputs);
                    }

                    //All outputs from the instance placement processor should be inputs to
                    //the map metadata serialiser.
                    metadataInputsToProcess = metadataOutputsToProcess;

                    foreach (ProcessBuilder instancePlacementProcessBuilder in instancePlacementProcessBuilders)
                    {
                        List<IContentNode> additionalManifestDependenciesList = instancePlacementProcessBuilder.Parameters[Consts.InstancePlacement_AdditionalManifestDependenciesParameter] as List<IContentNode>;
                        additionalManifestDependenciesList.ForEach(output =>
                        {
                            MapMetadataSerialiserManifestDependency manifestDependency = null;
                            String outputPath = null;
                            if (output is Content.File)
                            {
                                Content.File instancePlacementOutput = output as Content.File;
                                outputPath = instancePlacementOutput.AbsolutePath;

                                outputFiles.Add(output);
                                manifestDependency = new MapMetadataSerialiserManifestDependency(outputPath, false);
                            }
                            else if (output is Content.Directory)
                            {
                                Content.Directory instancePlacementOutput = output as Content.Directory;
                                outputPath = instancePlacementOutput.AbsolutePath;

                                IEnumerable<MapMetadataSerialiserManifestDependency> duplicates = additionalManifestDependencies.Where(dependency => dependency.IMAPPath == outputPath);
                                if (!duplicates.Any())
                                {
                                    outputDirectories.Add(outputPath);
                                    manifestDependency = new MapMetadataSerialiserManifestDependency(outputPath, "*.imap", false);
                                }
                            }

                            if (manifestDependency != null)
                                additionalManifestDependencies.Add(manifestDependency);
                        });
                    }

                    if (metadataMergeSplitDetails != null)
                    {
                        //Replace the node.
                        foreach (Content.File instancePlacementMetadata in metadataOutputsToProcess)
                        {
                            String instancePlacementSceneXmlFile = Path.GetFileName(instancePlacementMetadata.AbsolutePath);
                            for (int nodeIndex = 0; nodeIndex < metadataMergeSplitDetails.InputSceneXmlNodes.Count(); ++nodeIndex)
                            {
                                String fileName = Path.GetFileName((metadataMergeSplitDetails.InputSceneXmlNodes[nodeIndex] as Content.File).AbsolutePath);
                                if (String.Compare(fileName, instancePlacementSceneXmlFile, true) == 0)
                                {
                                    instancePlacementMetadata.Parameters[Consts.MetadataSerialiser_SourceInputParameter] = metadataMergeSplitDetails.InputSceneXmlNodes[nodeIndex];

                                    metadataMergeSplitDetails.InputSceneXmlNodes[nodeIndex] = instancePlacementMetadata;
                                }
                            }
                        }

                        mapExportAdditionsPathname = Path.Combine(param.Branch.Project.Cache, param.Branch.Name, "manifest", process.Outputs.First().Name, "MapExportAdditions.xml");

                        if (!CreateInstanceMetadataSerialiserProcessBuilder(
                            param, process, processors, targetContentTree, instancePlacementProcessBuilders.Select(pb => pb.Outputs),
                            mapExportAdditionsPathname, out instanceMetadataProcessBuilder))
                        {
                            param.Log.ErrorCtx(logContext_, "Failed to create metadata serialiser process.");
                            return false;
                        }
                        instanceMetadataProcessBuilder.Parameters.Add(Consts.MetadataSerialiser_AdditionalManifestDependenciesParameterName, additionalManifestDependencies);
                    }
                }
            }
            return true;
        }

        private bool ValidateProcess(IEngineParameters param, IProcess process)
        {
            bool result = true;

            if (!process.Inputs.Any())
            {
                param.Log.ErrorCtx(logContext_, "Map PreProcess requires one or more input nodes.");
                result = false;
            }

            if (process.Outputs.Count() != 1)
            {
                param.Log.ErrorCtx(logContext_, "Map PreProcess requires one or more input nodes.");
                return false;
            }

            IContentNode output = process.Outputs.First();
            if (!(output is Asset))
            {
                param.Log.ErrorCtx(logContext_, "Map PreProcess output must be of type Asset.");
                return false;
            }

            Asset outputAsset = (Asset)output;
            if (outputAsset.AssetType != RSG.Platform.FileType.ZipArchive)
            {
                param.Log.ErrorCtx(logContext_, "Map PreProcess output Asset must have an AssetType of ZipArchive.");
                return false;
            }

            return result;
        }

		/// <summary>
		/// Extract input zips and cache data about them (from SceneXml data).
		/// </summary>
		/// <param name="param"></param>
		/// <param name="process"></param>
		/// <param name="processors"></param>
		/// <param name="contentTreeHelper"></param>
		/// <param name="targetContentTree"></param>
		/// <param name="mapMetadataProcessBuilder"></param>
		/// <param name="collisionProcessBuilder"></param>
		/// <param name="materialPresetProcessBuilders"></param>
		/// <param name="syncDependenciesCollection"></param>
		/// <param name="outputFiles"></param>
		/// <param name="outputDirectories"></param>
		/// <param name="exportXmlFileDependencies"></param>
		/// <param name="metadataInputsToProcess"></param>
		/// <param name="unprocessedCollisionInputNodes"></param>
		/// <param name="clipDictionaryNodes"></param>
		/// <param name="parentTextureDictionaryNodes"></param>
		private void ExtractAndProcessInputs(IEngineParameters param,
											 IProcess process,
											 IProcessorCollection processors,
											 ContentTreeHelper contentTreeHelper,
											 IContentTree targetContentTree,
											 ProcessBuilder mapMetadataProcessBuilder,
											 ProcessBuilder collisionProcessBuilder,
											 ICollection<ProcessBuilder> materialPresetProcessBuilders,
											 ICollection<IContentNode> syncDependenciesCollection,
                                             ICollection<IContentNode> outputFiles,
											 ICollection<String> outputDirectories,
											 ISet<IContentNode> exportXmlFileDependencies,
											 ICollection<IContentNode> metadataInputsToProcess,
											 ICollection<IContentNode> unprocessedCollisionInputNodes,
											 ICollection<IContentNode> clipDictionaryNodes,
											 ICollection<IContentNode> parentTextureDictionaryNodes)
		{
			int inputIndex = 0;
            // Pre-pass to flesh out whether to discard the dependent process

            if (param.Branch.Project.IsDLC && !param.Branch.Project.ForceFlags.AllNewContent)
            {
                bool discard = true;
                foreach (IContentNode input in process.Inputs)
                {
                    // Ensure we only handle the ZipArchive inputs here.
                    if (!(input is Asset))
                        continue;
                    if (RSG.Platform.FileType.ZipArchive != ((Asset)input).AssetType)
                        continue;

                    Content.Asset asset = (Asset)input;
                    String assetFilename = param.Branch.Environment.Subst(asset.AbsolutePath);

                    // For DLC we need to check if dependent processes have any inputs in place.  If even one exists
                    // let's assume the intention is to include it.
                    if (System.IO.File.Exists(assetFilename))
                    {
                        discard = false;
                        break;
                    }
                }

                if (true == discard)
                {
                    process.State = ProcessState.Discard;
                    return;
                }
            }

            // B*1699820 merge props for DLC in one RPF.
            bool allInputsAreProps = process.Inputs.All(contentTreeHelper.ExportNodeIsPropContainer);
            if (collisionProcessBuilder != null)
                collisionProcessBuilder.SetParameter(Consts.Collision_AllInputsAreProps, allInputsAreProps);

            foreach (IContentNode input in process.Inputs)
            {
                // Ensure we only handle the ZipArchive inputs here.
                if (!(input is Asset))
                    continue;
                if (RSG.Platform.FileType.ZipArchive != ((Asset)input).AssetType)
                    continue;

                Content.Asset asset = (Asset)input;
                String assetFilename = param.Branch.Environment.Subst(asset.AbsolutePath);
                String inputDestination = GetSourceZipAssetCacheDir(param, asset);
                if (!System.IO.File.Exists(assetFilename))
                {
                    param.Log.Error("Input file: '{0}' does not exist.", assetFilename);
                    continue;
                }
                System.IO.Directory.CreateDirectory(inputDestination);
                if (!System.IO.Directory.Exists(inputDestination))
                {
                    param.Log.Error("Destination directory: '{0}' does not exist and could not be created.  Is it locked?",
                        inputDestination);
                    continue;
                }

                // Extract map zip.
                IEnumerable<String> extractedPathnames;
                bool extractOk = true;
                if (process.Rebuild == RebuildType.Yes || param.Flags.HasFlag(EngineFlags.Rebuild))
                    extractOk = Zip.ExtractAll(assetFilename, inputDestination, true, null, out extractedPathnames);
                else
                    extractOk = Zip.ExtractNewer(assetFilename, inputDestination, null, out extractedPathnames);

                if (!extractOk)
                {
                    param.Log.Error("Extracting '{0}' to '{1}' failed.",
                        asset.AbsolutePath, inputDestination);
                    continue;
                }

                // List of drawables for bounds processor.
                List<String> drawableFiles = new List<String>();
                List<IContentNode> inputDrawables = new List<IContentNode>();
                List<IContentNode> transformedDrawables = new List<IContentNode>();
                bool isPropContainer = contentTreeHelper.ExportNodeIsPropContainer(input);

				String sceneFilename = Path.ChangeExtension(assetFilename, ".xml");
                Scene scene = new Scene(contentTreeHelper.ContentTree, contentTreeHelper, sceneFilename, LoadOptions.All, true);

                // Check for dynamic objects in non-prop containers because we'll need to pack their collision in the drawables.
                List<String> dynamicObjects = new List<String>();
                if (!isPropContainer)
                    GetSceneDynamicObjects(scene, dynamicObjects);

                string transformedDirectory = Path.Combine(inputDestination, transformedDrawablesDirectoryName_);

                // Consider specific types of file
                foreach (String extractedPathname in extractedPathnames)
                {
                    String[] extensions = Filename.GetExtensions(extractedPathname);
                    FileType[] fileTypes = Filename.GetFileTypes(extensions);
                    IContentNode targetFileNode = targetContentTree.CreateFile(extractedPathname);

                    if (fileTypes.Length == 1)
                    {
                        if (fileTypes[0] == FileType.IMAP || fileTypes[0] == FileType.ITYP)
                        {
                            // HACK  - do nothing (to prevent old map metadata from source data being included)
                        }
                        else
                        {
                            outputFiles.Add(targetFileNode);
                        }
                    }
                    else if (fileTypes.Length > 1)
                    {
                        if (fileTypes[0] == FileType.ZipArchive
                            && fileTypes[1] == FileType.ClipDictionary)
                        {
                            clipDictionaryNodes.Add(targetContentTree.CreateFile(extractedPathname));
                        }
                        else if (isPropContainer
                                 && fileTypes[0] == FileType.ZipArchive
                                 && fileTypes[1] == FileType.Drawable)
                        {
                            string drawableFilename = Path.GetFileName(extractedPathname);
                            drawableFiles.Add(drawableFilename);

                            string transformedDrawable = Path.Combine(transformedDirectory, drawableFilename);

                            transformedDrawables.Add(targetContentTree.CreateFile(transformedDrawable));
                            inputDrawables.Add(targetFileNode);

                            CopyToTransformedDirectory(targetContentTree, outputFiles, transformedDirectory, extractedPathname, transformedDrawable);

                            ProcessMaterialPresets(param, targetContentTree, processors, asset, transformedDrawable, ref materialPresetProcessBuilders);
                        }
                        else if (fileTypes[0] == FileType.ZipArchive
                                 && fileTypes[1] == FileType.Drawable)
                        {
                            parentTextureDictionaryNodes.Add(targetContentTree.CreateFile(extractedPathname));

                            string drawableName = "";
                            string drawableFilename = Path.GetFileName(extractedPathname);
                            string transformedDrawable = Path.Combine(transformedDirectory, drawableFilename);

                            int iPosOfFirstDot = drawableFilename.IndexOf(".");
                            if (iPosOfFirstDot != -1)
                                drawableName = drawableFilename.Substring(0, iPosOfFirstDot);

                            // Some map containers still have props so see if we have any to pack collision for.
                            if (dynamicObjects.Contains(drawableName))
                            {
                                drawableFiles.Add(drawableFilename);

                                transformedDrawables.Add(targetContentTree.CreateFile(transformedDrawable));
                                inputDrawables.Add(targetContentTree.CreateFile(extractedPathname));
                            }

                            CopyToTransformedDirectory(targetContentTree, outputFiles, transformedDirectory, extractedPathname, transformedDrawable);

                            ProcessMaterialPresets(param, targetContentTree, processors, asset, transformedDrawable, ref materialPresetProcessBuilders);
                        }
                        else if (fileTypes[0] == FileType.ZipArchive
                                 && fileTypes[1] == FileType.TextureDictionary)
                        {
                            parentTextureDictionaryNodes.Add(targetContentTree.CreateFile(extractedPathname));

                            string textureDictionaryFilename = Path.GetFileName(extractedPathname);
                            string transformedTextureDictionary = Path.Combine(transformedDirectory, textureDictionaryFilename);

                            CopyToTransformedDirectory(targetContentTree, outputFiles, transformedDirectory, extractedPathname, transformedTextureDictionary);
                        }
                        else if (fileTypes[0] == FileType.ZipArchive
                                 && fileTypes[1] == FileType.Fragment)
                        {
		                    string drawableFilename = Path.GetFileName(extractedPathname);
		                    string drawableName = "";
		                    int iPosOfFirstDot = drawableFilename.IndexOf(".");
		                    if (iPosOfFirstDot != -1)
		                        drawableName = drawableFilename.Substring(0, iPosOfFirstDot);

                            TargetObjectDef obj = scene.FindObject(drawableName);
                            if(obj!=null && !obj.GetAttribute(AttrNames.OBJ_IS_CLOTH, false))
                                targetFileNode.SetParameter(Constants.ParamMap_NeedsTuning, true);
                            outputFiles.Add(targetFileNode);
                        }
                        else
                        {
                            outputFiles.Add(targetFileNode);
                        }
                    }
                }

                ISet<IContentNode> processedZipFileDependencies = new HashSet<IContentNode>();
                ISet<IContentNode> materialDependencies = new HashSet<IContentNode>();
                GetSceneDependencies(scene, process, exportXmlFileDependencies, processedZipFileDependencies, materialDependencies);
                syncDependenciesCollection.AddRange(exportXmlFileDependencies);
                syncDependenciesCollection.AddRange(processedZipFileDependencies);

                IContentNode sceneFile = targetContentTree.CreateFile(sceneFilename);
                IContentNode sceneDirectory = targetContentTree.CreateDirectory(inputDestination); // TODO : never used

                // Remove any merged inputs
                foreach (String fileMarkedForExclusion in AssetCombine.Coordinator.Instance.GetFilesMarkedForExclude(input))
                {
                    IContentNode fileToRemovePathname = outputFiles.FirstOrDefault(
                        outputFile => fileMarkedForExclusion.Equals(Path.GetFileName((outputFile as IFilesystemNode).AbsolutePath), StringComparison.OrdinalIgnoreCase));

                    if (fileToRemovePathname != null)
                        outputFiles.Remove(fileToRemovePathname);
                }

                // Handle metadata serialisation process inputs/outputs.
                if (mapMetadataProcessBuilder != null)
                {
                    mapMetadataProcessBuilder.Inputs.Add(sceneFile);
                    metadataInputsToProcess.Add(sceneFile);
                    mapMetadataProcessBuilder.Inputs.AddRange(exportXmlFileDependencies);
                }

                // Handle collision process inputs.
                String collisionProcessInputPathname;
                if (Collision.GetUnprocessedCollisionPathname(extractedPathnames, out collisionProcessInputPathname))
                {
                    outputFiles.Remove(outputFiles.First(outputFile => (outputFile as IFilesystemNode).AbsolutePath.Equals(collisionProcessInputPathname, StringComparison.OrdinalIgnoreCase)));
                }
                if (collisionProcessBuilder != null)
                {
                    // Handle collision process inputs.
                    if (Collision.GetUnprocessedCollisionPathname(extractedPathnames, out collisionProcessInputPathname))
                    {
                        unprocessedCollisionInputNodes.Add(targetContentTree.CreateFile(collisionProcessInputPathname));
                        String collisionProcessInputParameterName = String.Format("{0}{1}", Consts.Collision_UnprocessedBoundsZipParameterPrefix, inputIndex);
#warning DHM FIX ME: the nice thing with the parameters is that they are Objects... use a List!
                        collisionProcessBuilder.Parameters.Add(collisionProcessInputParameterName, collisionProcessInputPathname);
                    }

                    // Handle packed bound files inside drawables.
                    if (drawableFiles.Count > 0)
                    {
                        String drawableListParameterName = String.Format("{0}{1}", Consts.Collision_DrawableListParameterPrefix, inputIndex);
                        String drawableList = String.Join(";", drawableFiles.Select(i => i.ToString()).ToArray());
#warning DHM FIX ME: the nice thing with the parameters is that they are Objects... use a List!
                        collisionProcessBuilder.Parameters.Add(drawableListParameterName, drawableList);
                    }

#warning DHM FIX ME: the nice thing with the parameters is that they are Objects... use a List!
                    String sceneXmlParameterName = String.Format("{0}{1}", Consts.Collision_SceneXmlParameterPrefix, inputIndex);
                    collisionProcessBuilder.Parameters.Add(sceneXmlParameterName, sceneFilename);

                    String sceneType = contentTreeHelper.GetSceneTypeForExportNode(input);
#warning DHM FIX ME: the nice thing with the parameters is that they are Objects... use a List!
                    String sceneTypeParameterName = String.Format("{0}{1}", Consts.Collision_SceneTypeParameterPrefix, inputIndex);
                    collisionProcessBuilder.Parameters.Add(sceneTypeParameterName, sceneType);

                    collisionProcessBuilder.Inputs.Add(targetContentTree.CreateFile(sceneFilename));
                    collisionProcessBuilder.Inputs.AddRange(exportXmlFileDependencies);
                    collisionProcessBuilder.Inputs.AddRange(processedZipFileDependencies);
                    collisionProcessBuilder.Inputs.AddRange(inputDrawables);
                    collisionProcessBuilder.Outputs.AddRange(transformedDrawables);
                }
                // Remove any occlusion data (if necessary)
                if (contentTreeHelper.ExportNodeIsOcclusionContainer(input))
                {
                    String occlusionXmlFilename = String.Format("{0}.occl.zip", input.Name);
                    IContentNode fileToRemovePathname = outputFiles.FirstOrDefault(
                        outputFile => occlusionXmlFilename.Equals(Path.GetFileName((outputFile as IFilesystemNode).AbsolutePath), StringComparison.OrdinalIgnoreCase));

                    if (fileToRemovePathname != null)
                        outputFiles.Remove(fileToRemovePathname);
                }

                ++inputIndex;
            }
        }

        private static void CopyToTransformedDirectory(IContentTree targetContentTree, ICollection<IContentNode> outputFiles, string transformedDirectory, string extractedPathname, string transformedDrawable)
        {
            if (!System.IO.Directory.Exists(transformedDirectory))
                System.IO.Directory.CreateDirectory(transformedDirectory);

            System.IO.File.Copy(extractedPathname, transformedDrawable, true);

            IContentNode contentNode = targetContentTree.CreateFile(transformedDrawable);

            outputFiles.Add(contentNode);
        }


        /// <summary>
        /// Processes all material preset dependencies and processes.
        /// This will extract the .zip file, determine if a processor is needed.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="contentTreeHelper"></param>
        /// <param name="asset"></param>
        /// <param name="drawableZip"></param>
        /// <returns></returns>
        private bool ProcessMaterialPresets(IEngineParameters param,
            IContentTree targetContentTree,
            IProcessorCollection processors,
            Content.File asset,
            String drawableZip,
            ref ICollection<ProcessBuilder> materialPresetProcesses)
        {
            String drawableName = drawableZip.Substring(0, drawableZip.IndexOf("."));
            String inputDestination = GetSourceZipAssetCacheDir(param, asset);
            String drawableDirectory = Path.Combine(inputDestination, drawableName);

            if (!System.IO.Directory.Exists(drawableDirectory))
                System.IO.Directory.CreateDirectory(drawableDirectory);

            IEnumerable<String> extractedFiles;
            bool extractResult = Zip.ExtractAll(drawableZip, drawableDirectory, true, null, out extractedFiles);

            if (!extractResult)
            {
                param.Log.ErrorCtx(logContext_, "Extracting '{0}' to '{1}' failed.",
                    drawableZip, drawableDirectory);
                return false;
            }

            List<string> templateFiles = new List<string>();

            //Find any .mpo or .mpi files.  If any exist, create a processor for them to be converted.
            //TODO: Currently supporting packed .mpo files but these will probably be removed soon.
            templateFiles.AddRange(System.IO.Directory.GetFiles(drawableDirectory, "*" + MaterialPreset.MaterialObjectExtension));
            templateFiles.AddRange(System.IO.Directory.GetFiles(drawableDirectory, "*" + MaterialPreset.MaterialTemplateExportExtension));

            if (templateFiles.Count > 0)
            {
                ProcessBuilder materialPresetProcessBuilder = new ProcessBuilder("RSG.Pipeline.Processor.Material.MaterialPresetProcessor", processors, targetContentTree);

                // TODO: outputAsset is not used in any path
                Content.Asset outputAsset = (Asset)asset;
                materialPresetProcessBuilder.Inputs.Add(targetContentTree.CreateDirectory(drawableDirectory));

                materialPresetProcessBuilder.Outputs.Add(targetContentTree.CreateFile(drawableZip));
                materialPresetProcesses.Add(materialPresetProcessBuilder);
            }

            return true;
        }

        private void ProcessClipDictionaryInputs(IEngineParameters param,
            IProcessorCollection processors,
            IContentTree targetContentTree,
            IEnumerable<Content.File> clipDictionaryFiles,
            ICollection<ProcessBuilder> clipProcessBuilders,
            ICollection<IContentNode> processedClipDictionaryFileNodes)
        {
            String defaultSkeletonPathname = GetDefaultSkeletonPathname(param);
            IContentNode defaultSkeletonFileNode = targetContentTree.CreateFile(defaultSkeletonPathname);

            foreach (Content.File clipDictionaryFile in clipDictionaryFiles)
            {
                String clipDictionaryBasename = System.IO.Path.GetFileNameWithoutExtension(clipDictionaryFile.AbsolutePath);
                String clipDictionaryFilename = System.IO.Path.GetFileName(clipDictionaryFile.AbsolutePath);
                clipDictionaryFilename = MapAsset.AppendMapPrefixIfRequired(param.Branch.Project, clipDictionaryFilename);

                String clipDictionaryDirectory = System.IO.Path.GetDirectoryName(clipDictionaryFile.AbsolutePath);
                String animationWorkingDirectory = System.IO.Path.Combine(clipDictionaryDirectory, animationWorkingDirectoryName_, clipDictionaryBasename);
                String animationGroupDirectory = System.IO.Path.Combine(animationWorkingDirectory, animationGroupDirectoryName_);
                String animationCompressedDirectory = System.IO.Path.Combine(animationWorkingDirectory, animationCompressedDirectoryName_);
                String animationPackSchedulePathname = System.IO.Path.Combine(animationWorkingDirectory, clipDictionaryFilename + ".xml");
                String animationPackPathname = System.IO.Path.Combine(animationWorkingDirectory, clipDictionaryFilename);

                System.IO.Directory.CreateDirectory(animationGroupDirectory);
                System.IO.Directory.CreateDirectory(animationCompressedDirectory);

                IContentNode animationGroupDirectoryNode = targetContentTree.CreateDirectory(animationGroupDirectory, "*.*");
                IContentNode animationCompressedDirectoryNode = targetContentTree.CreateDirectory(animationCompressedDirectory, "*.*");
                IContentNode animationPackScheduleFileNode = targetContentTree.CreateFile(animationPackSchedulePathname);
                IContentNode animationPackFileNode = targetContentTree.CreateFile(animationPackPathname);

                ProcessBuilder animationGroupProcessBuilder = new ProcessBuilder(
                    "RSG.Pipeline.Processor.Animation.Common.ClipDictionaryProcessor", processors, targetContentTree);
                animationGroupProcessBuilder.Inputs.Add(clipDictionaryFile);
                animationGroupProcessBuilder.Outputs.Add(animationGroupDirectoryNode);
                animationGroupProcessBuilder.Parameters.Add("Grouping", true);
                if (!String.IsNullOrEmpty(param.Branch.Project.AssetPrefix.MapPrefix))
                    animationGroupProcessBuilder.Parameters.Add("DLCPrefix", param.Branch.Project.AssetPrefix.MapPrefix);

                clipProcessBuilders.Add(animationGroupProcessBuilder);

                ProcessBuilder animationCompressProcessBuilder = new ProcessBuilder(
                    "RSG.Pipeline.Processor.Animation.Common.Compression", processors, targetContentTree);
                animationCompressProcessBuilder.AdditionalDependentContent.Add(defaultSkeletonFileNode);
                animationCompressProcessBuilder.Inputs.Add(animationGroupDirectoryNode);
                animationCompressProcessBuilder.Outputs.Add(animationCompressedDirectoryNode);
                clipProcessBuilders.Add(animationCompressProcessBuilder);

                AssetPackScheduleBuilder animationPackScheduleBuilder = new AssetPackScheduleBuilder();
                AssetPackZipArchive animationZipArchive = animationPackScheduleBuilder.AddZipArchive(animationPackPathname);
                animationZipArchive.AddDirectory(animationCompressedDirectory);
                animationPackScheduleBuilder.Save(animationPackSchedulePathname);

                ProcessBuilder animationPackProcessBuilder = new ProcessBuilder(
                    "RSG.Pipeline.Processor.Common.AssetPackProcessor", processors, targetContentTree);
                animationPackProcessBuilder.Inputs.Add(animationPackScheduleFileNode);
                clipProcessBuilders.Add(animationPackProcessBuilder);

                processedClipDictionaryFileNodes.Add(animationPackFileNode);
            }
        }

        /// <summary>
        /// Create the instance placement process.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="process"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="metadataProcess"></param>
        /// <returns></returns>
        private bool CreateInstancePlacementProcessBuilder(IEngineParameters param,
            IProcess process, IProcessorCollection processors, IContentTree owner, ContentTreeHelper contentTreeHelper,
            List<IContentNode> metadataInputs, HashSet<IContentNode> exportXMLDependencies,
            out List<IContentNode> metadataOutputs, out List<ProcessBuilder> instancePlacementProcessBuilders)
        {
            instancePlacementProcessBuilders = null;
            metadataOutputs = null;

            Content.Asset outputAsset = (Asset)process.Outputs.First();

            IEnumerable<IProcess> processes = null;
            DependencyWalker.OutputsFrom(outputAsset, out processes);
            if (processes.Count() == 0)
                return false; //No processes.

            IProcessor instancePlacementProcessor = processors.Where(p =>
                0 == String.Compare("RSG.Pipeline.Processor.Map.InstancePlacementProcessor", p.Name)).FirstOrDefault();
            Debug.Assert(null != instancePlacementProcessor, "Instance placement processor not defined.");

            String processedDestination = GetProcessedZipAssetCacheDir(param, outputAsset);
            String placementDirectory = System.IO.Path.Combine(processedDestination, instancePlacementDirectoryName_);
            String processedBasename = outputAsset.Basename;

            Asset inputNode = process.Inputs.First() as Asset;

            bool checkContainerOverlaps = false;
            List<String> overlappedOutputPaths = new List<string>();

            if (this.Parameters.ContainsKey(Consts.MapProcess_MapInstancePlacementIntersectionEnabled) &&
                (bool)this.Parameters[Consts.MapProcess_MapInstancePlacementIntersectionEnabled] == true)
            {
                checkContainerOverlaps = true;
            }

            if (checkContainerOverlaps)
            {
                List<MapDataInfo> mapDataInfoList = new List<MapDataInfo>();
                String MapDataFilename = Path.Combine(param.Branch.Metadata, "terrain", "MapInfo.xml");

                if (!System.IO.File.Exists(MapDataFilename))
                {
                    param.Log.ErrorCtx(logContext_, String.Format("Unable to find map info file: {0}", MapDataFilename));
                    return false;
                }

                // Deserialize the MapInfo.xml file 
                System.IO.StreamReader reader = new System.IO.StreamReader(MapDataFilename);
                System.Xml.Serialization.XmlSerializer serialiser = new System.Xml.Serialization.XmlSerializer(typeof(List<MapDataInfo>));
                mapDataInfoList = (List<MapDataInfo>)serialiser.Deserialize(reader);
                reader.Close();

                // Get our overlapping containers.
                List<MapDataInfo> overlappingContainers = new List<MapDataInfo>();

                if (!MapDataInfo.CheckMapIntersections(inputNode.Basename, ref mapDataInfoList, ref overlappingContainers))
                    param.Log.ErrorCtx(logContext_, String.Format("Could not gather overlapping dependencies for input {0}", inputNode.Basename));

                List<IContentNode> overlappedZipNodes = new List<IContentNode>();

                // Go through and add inputs to the processor from the overlapping container export zip nodes.
                if (overlappingContainers.Count > 0)
                {
                    IContentNode[] maxNodes = contentTreeHelper.GetAllMapContainerMaxNodes();

                    foreach (IContentNode maxNode in maxNodes)
                    {
                        foreach (MapDataInfo mapDataInfo in overlappingContainers)
                        {
                            if (String.Compare(mapDataInfo.Name, maxNode.Name, true) == 0)
                            {
                                IContentNode exportZipNode = contentTreeHelper.GetExportZipNodeFromMaxFileNode(maxNode);
                                if (!overlappedZipNodes.Contains(exportZipNode))
                                    overlappedZipNodes.Add(exportZipNode);
                            }
                        }
                    }
                }

                // Go through and extract all the dependendant zip files so we can use the extracted collision zip file.
                if (overlappedZipNodes.Count > 0)
                {
                    foreach (IContentNode zipFileNode in overlappedZipNodes)
                    {
                        Content.File exportZipFile = zipFileNode as Content.File;

                        String exportZipFilename = exportZipFile.AbsolutePath;
                        String cacheDir = GetProcessedZipAssetCacheDir(param, exportZipFile);

                        IEnumerable<String> extractedPathnames;
                        bool extractOk = true;
                        if (process.Rebuild == RebuildType.Yes || param.Flags.HasFlag(EngineFlags.Rebuild))
                            extractOk = Zip.ExtractAll(exportZipFilename, cacheDir, true, null, out extractedPathnames);
                        else
                            extractOk = Zip.ExtractNewer(exportZipFilename, cacheDir, null, out extractedPathnames);

                        if (!extractOk)
                        {
                            param.Log.ErrorCtx(logContext_, "Extracting '{0}' to '{1}' failed.",
                                exportZipFilename, cacheDir);
                            continue;
                        }

                        // Store the output directory, this is the final input passed to the instance placement process.
                        if (extractedPathnames.Count() > 0)
                            overlappedOutputPaths.Add(cacheDir);
                    }
                }
            }

            String collisionProcessInputPathname;
            IEnumerable<String> zipFiles = null;
            Zip.GetFileList(inputNode.AbsolutePath, out zipFiles);
            if (!Collision.GetUnprocessedCollisionPathname(zipFiles, out collisionProcessInputPathname))
            {
                return false; //No collision; ignore this process.
            }

            String inputDestination = GetSourceZipAssetCacheDir(param, inputNode);
            collisionProcessInputPathname = Path.Combine(inputDestination, collisionProcessInputPathname);

            //The Instance Placement Processor may transform the SceneXML from Max so 
            //any objects qualifying for instancing are omitted from the normal ITYP/IMAP files.
            //If there is a Max file associated with this process, then ensure the map metadata
            //serialiser uses the updated version.
            List<IContentNode> inputMetadataNodes = new List<IContentNode>();
            foreach (IContentNode inputSceneXmlNode in process.Inputs)
            {
                if (inputSceneXmlNode is Asset)
                {
                    Asset inputFileNode = inputSceneXmlNode as Asset;
                    String sceneXML = Path.ChangeExtension(inputFileNode.AbsolutePath, "xml");
                    String exclusionFile = System.IO.Path.Combine(placementDirectory, String.Format("{0}.xml", inputNode.Name));
                    bool sceneXmlExists = System.IO.File.Exists(sceneXML);
                    if (sceneXmlExists)
                    {
                        inputMetadataNodes.Add(owner.CreateFile(sceneXML));
                        metadataOutputs = new List<IContentNode>();
                        metadataOutputs.Add(owner.CreateFile(exclusionFile));
                    }
                }
            }

            if (inputMetadataNodes.Count() == 0)
            {
                //No transformation will take place.
                metadataOutputs = metadataInputs;
            }

            List<IContentNode> additionalManifestDependencies = new List<IContentNode>();
            IContentNode instancePlacementDirNode = owner.CreateDirectory(placementDirectory);
            additionalManifestDependencies.Add(instancePlacementDirNode);

            List<object> enabledPlacementTypes = this.Parameters[Consts.InstancePlacement_EnabledPlacementTypes] as List<object>;
            List<InstancePlacementType> instancePlacementTypes = new List<InstancePlacementType>();

            foreach (object enabledPlacementTypeObj in enabledPlacementTypes)
            {
                String enabledPlacementType = (String)enabledPlacementTypeObj;
                InstancePlacementType placementType;
                if (!Enum.TryParse<InstancePlacementType>(enabledPlacementType, out placementType))
                {
                    param.Log.ErrorCtx(logContext_, String.Format("Unable to resolve instance placement type {0}.", enabledPlacementType));
                    continue;
                }

                List<IContentNode> inputTextureFileNodes = new List<IContentNode>();
                if (this.Parameters.ContainsKey(Consts.InstancePlacement_PlaceOnMapContainers)
                    && (bool)this.Parameters[Consts.InstancePlacement_PlaceOnMapContainers] == true)
                {
                    if (this.Parameters.ContainsKey(Consts.InstancePlacement_GlobalTexture))
                    {
                        String globalTexturePathParam = (String)this.Parameters[Consts.InstancePlacement_GlobalTexture];
                        String globalTexturePath = param.Branch.Environment.Subst(globalTexturePathParam);
                        globalTexturePath = globalTexturePath.Replace("$(instance_category)", placementType.ToString());

                        String[] globalTextures = System.IO.Directory.GetFiles(Path.GetDirectoryName(globalTexturePath), Path.GetFileNameWithoutExtension(globalTexturePath) + "*", SearchOption.TopDirectoryOnly);
                        foreach (String globalTexture in globalTextures)
                        {
                            IContentNode textureInput = owner.CreateFile(globalTexture);
                            inputTextureFileNodes.Add(textureInput);
                        }
                    }
                    else
                    {
                        return false; //No global texture specified.
                    }

                    if (!contentTreeHelper.ExportNodeIsMapContainer(process.Inputs.First()))
                    {
                        return false; //Not a map container.
                    }
                }
                else
                {
                    IEnumerable<IProcess> instancePlacementProcesses = processes.Where(p =>
                        p.ProcessorClassName.Equals("RSG.Pipeline.Processor.Map.InstancePlacementProcessor"));

                    if (!instancePlacementProcesses.Any())
                        return false; //No instance placement process.

                    IProcess instancePlacementProcess = instancePlacementProcesses.FirstOrDefault();
                    foreach (IContentNode inputTextureNode in instancePlacementProcess.Inputs)
                    {
                        Content.File inputTextureFileNode = inputTextureNode as Content.File;
                        string texturePath = inputTextureFileNode.AbsolutePath.Replace("$(instance_category)", placementType.ToString());

                        String[] textureFiles = System.IO.Directory.GetFiles(Path.GetDirectoryName(texturePath), Path.GetFileNameWithoutExtension(texturePath) + "*", SearchOption.TopDirectoryOnly);

                        foreach (String texture in textureFiles)
                        {
                            inputTextureFileNodes.Add(owner.CreateFile(texture));
                        }
                    }
                }

                if (inputTextureFileNodes.Count() > 0)
                {
                    ProcessBuilder instancePlacementProcessBuilder = new ProcessBuilder(instancePlacementProcessor, owner);
                    if (instancePlacementProcessBuilders == null)
                    {
                        instancePlacementProcessBuilders = new List<ProcessBuilder>();
                    }
                    instancePlacementProcessBuilders.Add(instancePlacementProcessBuilder);

                    instancePlacementTypes.Add(placementType);

                    if (inputMetadataNodes.Count() > 0)
                    {
                        instancePlacementProcessBuilder.Inputs.AddRange(inputMetadataNodes.ToList());

                        //Add the scene XML file excluded objects in it.
                        instancePlacementProcessBuilder.Outputs.AddRange(metadataOutputs.ToList());
                    }

                    instancePlacementProcessBuilder.Inputs.AddRange(inputTextureFileNodes);
                    instancePlacementProcessBuilder.Outputs.Add(instancePlacementDirNode);

                    if (!instancePlacementProcessBuilder.Parameters.ContainsKey(Consts.InstancePlacement_InputTextures))
                        instancePlacementProcessBuilder.Parameters.Add(Consts.InstancePlacement_InputTextures, inputTextureFileNodes);

                    String cacheDirectory = (String)this.Parameters[Consts.MapProcess_ProcessedMapCacheDirectory];
                    if (!instancePlacementProcessBuilder.Parameters.ContainsKey(Consts.MapProcess_ProcessedMapCacheDirectory))
                        instancePlacementProcessBuilder.Parameters.Add(Consts.MapProcess_ProcessedMapCacheDirectory, cacheDirectory);

                    String sourceCacheDirectory = (String)this.Parameters[Consts.MapProcess_SourceMapCacheDirectory];
                    if (!instancePlacementProcessBuilder.Parameters.ContainsKey(Consts.MapProcess_SourceMapCacheDirectory))
                        instancePlacementProcessBuilder.Parameters.Add(Consts.MapProcess_SourceMapCacheDirectory, sourceCacheDirectory);

                    instancePlacementProcessBuilder.Parameters.Add(Consts.MetadataSerialiser_InputToProcessParameterPrefix, inputMetadataNodes);
                    instancePlacementProcessBuilder.Parameters.Add(Consts.InstancePlacement_AdditionalManifestDependenciesParameter, additionalManifestDependencies);

                    // Add our overlapping containers so we can extract their collision.
                    if (checkContainerOverlaps)
                    {
                        instancePlacementProcessBuilder.Parameters.Add(Consts.InstancePlacement_OverlappingContainerDependencies, overlappedOutputPaths);
                    }

                    //Add to the process builder what types are being placed.
                    instancePlacementProcessBuilder.Parameters.Add(Consts.InstancePlacement_PlacementType, placementType);

                    //Sets all SceneXML dependencies for this process.
                    instancePlacementProcessBuilder.Parameters.Add(Consts.InstancePlacement_AdditionalSceneXMLDependencies, exportXMLDependencies);

                }
            }

            if (instancePlacementProcessBuilders != null)
                return true;

            return false;
        }

        /// <summary>
        /// Create the parent texture dictionary processes.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="process"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="metadataProcess"></param>
        /// <returns></returns>
        private bool CreateParentTextureDictionaryProcessBuilder(IEngineParameters param,
            IProcess process, IProcessorCollection processors, IContentTree owner,
            ContentTreeHelper contentTreeHelper, ICollection<IContentNode> parentTextureNodes,
            out List<IProcess> parentTextureDictionaryProcesses)
        {
            parentTextureDictionaryProcesses = null;

            IContentNode gtxdNode = contentTreeHelper.GetParentTXDFileNode();
            if (gtxdNode != null)
            {
                Content.Asset outputAsset = (Asset)process.Outputs.First();

                IProcessor parentTextureDictionaryProcessor = processors.Where(p =>
                    0 == String.Compare("RSG.Pipeline.Processor.Map.ParentTextureDictionary", p.Name)).FirstOrDefault();
                Debug.Assert(null != parentTextureDictionaryProcessor, "Parent texture dictionary processor not defined.");

                parentTextureDictionaryProcesses = new List<IProcess>();
                foreach (IContentNode parentTextureNode in parentTextureNodes)
                {
                    ProcessBuilder parentTextureDictionaryProcessBuilder = new ProcessBuilder(parentTextureDictionaryProcessor, owner);
                    parentTextureDictionaryProcessBuilder.Inputs.Add(parentTextureNode);

                    Content.File parentTextureFileNode = parentTextureNode as Content.File;
                    String outputTextureFile = Path.GetFileName(parentTextureFileNode.AbsolutePath);
                    String outputTexturePath = Path.Combine(Path.GetDirectoryName(parentTextureFileNode.AbsolutePath), transformedDrawablesDirectoryName_, outputTextureFile);

                    parentTextureDictionaryProcessBuilder.Outputs.Add(owner.CreateFile(outputTexturePath));

                    String cacheDirectory = (String)this.Parameters[Consts.MapProcess_ProcessedMapCacheDirectory];
                    if (!parentTextureDictionaryProcessBuilder.Parameters.ContainsKey(Consts.MapProcess_ProcessedMapCacheDirectory))
                        parentTextureDictionaryProcessBuilder.Parameters.Add(Consts.MapProcess_ProcessedMapCacheDirectory, cacheDirectory);

                    String sourceCacheDirectory = (String)this.Parameters[Consts.MapProcess_SourceMapCacheDirectory];
                    if (!parentTextureDictionaryProcessBuilder.Parameters.ContainsKey(Consts.MapProcess_SourceMapCacheDirectory))
                        parentTextureDictionaryProcessBuilder.Parameters.Add(Consts.MapProcess_SourceMapCacheDirectory, sourceCacheDirectory);

                    IFilesystemNode gtxdInput = gtxdNode as IFilesystemNode;
                    if (!parentTextureDictionaryProcessBuilder.Parameters.ContainsKey(Consts.Textures_ParentTextureDictionaryModel))
                        parentTextureDictionaryProcessBuilder.Parameters.Add(Consts.Textures_ParentTextureDictionaryModel, gtxdInput);

                    if (!parentTextureDictionaryProcessBuilder.Parameters.ContainsKey(Consts.MapProcess_MapSectionName))
                        parentTextureDictionaryProcessBuilder.Parameters.Add(Consts.MapProcess_MapSectionName, outputAsset.Name);

                    parentTextureDictionaryProcesses.Add(parentTextureDictionaryProcessBuilder.ToProcess());
                }
            }
            else
            {
                //No GTXD, then this processor is not necessary to execute.
                return false;
            }

            return true;
        }

        /// <summary>
        /// Create the metadata serialiser process.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="process"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="metadataProcess"></param>
        /// <returns></returns>
        private bool CreateMetadataSerialiserProcessBuilder(IEngineParameters param,
            IProcess process,
            IProcessorCollection processors,
            IContentTree owner,
            ICollection<IContentNode> syncDependencies,
            String mapExportAdditionsPathname,
            String audioCollisionMapPathname,
            String sceneOverridesDirectory,
            out ProcessBuilder metadataPb)
        {
            Content.Asset outputAsset = (Asset)process.Outputs.First();
            String processedDestination = GetProcessedZipAssetCacheDir(param, outputAsset);
            String metadataDirectory = System.IO.Path.Combine(processedDestination, metadataDirectoryName_);
            String processedBasename = outputAsset.Basename;
            String processedITYP = Path.Combine(metadataDirectory, processedBasename + ".ityp");
            String processedIMAP = Path.Combine(metadataDirectory, processedBasename + ".imap");

            // Find process and input for this zip; so we can read flags such
            // as whether we need archetype/entity export support.  These
            // are required by other map processors.
            List<IProcess> dccExportProcesses = process.Inputs.SelectMany(input => input.Owner.FindProcessesWithOutput(input)).ToList();
            Debug.Assert(dccExportProcesses.Count() == process.Inputs.Count(),
                "Inconsistent content-tree: missing DCC export process for one or more maps.");
            if (dccExportProcesses.Count() != process.Inputs.Count())
            {
                param.Log.ErrorCtx(logContext_, "Inconsistent content-tree; missing DCC export process for one or more maps.");
                metadataPb = null;
                return (false);
            }
            List<IContentNode> dccSourceContent = dccExportProcesses.Select(p => p.Inputs.First()).ToList();
            Debug.Assert(dccSourceContent.Count() == dccExportProcesses.Count(),
                "Inconsistent content-tree: missing DCC source content for one or more maps.");
            if (dccSourceContent.Count() != dccExportProcesses.Count())
            {
                param.Log.ErrorCtx(logContext_, "Inconsistent content-tree: missing DCC source content for one or more maps.");
                metadataPb = null;
                return (false);
            }

            // Determine whether we are exporting archetypes and or entities
            // for this map process based on source map parameters.
            bool exportArchetypes = dccSourceContent.
                Where(n => n.Parameters.ContainsKey(Constants.ParamMap_ExportArchetypes)).
                Select(n => n.Parameters[Constants.ParamMap_ExportArchetypes]).
                Cast<bool>().Any(b => true == b);
            bool exportEntities = dccSourceContent.
                Where(n => n.Parameters.ContainsKey(Constants.ParamMap_ExportEntities)).
                Select(n => n.Parameters[Constants.ParamMap_ExportEntities]).
                Cast<bool>().Any(b => true == b);
            IEnumerable<string> forceDependency = dccSourceContent.
                Where(n => n.Parameters.ContainsKey(Constants.ParamMap_ForceItypDependency)).
                Select(n => (string)n.Parameters[Constants.ParamMap_ForceItypDependency]).
                Distinct().ToList();

            // Create Metadata Serialisation Process for map.
            String cacheDirectory = (String)this.Parameters[Consts.MapProcess_ProcessedMapCacheDirectory];
            IProcessor metadataProcessor = processors.GetProcessor("RSG.Pipeline.Processor.Map.MetadataSerialiser");
            Debug.Assert(null != metadataProcessor, "Map Metadata Serialiser processor not defined.");
            if (!metadataProcessor.Parameters.ContainsKey(Consts.MapProcess_ProcessedMapCacheDirectory))
                metadataProcessor.Parameters.Add(Consts.MapProcess_ProcessedMapCacheDirectory, cacheDirectory);
            metadataPb = new ProcessBuilder(metadataProcessor, owner);
            metadataPb.SetParameter(Constants.ParamMap_ExportArchetypes, exportArchetypes);
            metadataPb.SetParameter(Constants.ParamMap_ExportEntities, exportEntities);
            metadataPb.SetParameter(Constants.ParamMap_ForceItypDependency, forceDependency);
            metadataPb.SetParameter(Consts.MetadataSerialiser_MapExportAdditionsPathnameParameterName,
                mapExportAdditionsPathname);
            metadataPb.SetParameter(Consts.MetadataSerialiser_AudioCollisionMapPathnameParameterName,
                audioCollisionMapPathname);
            metadataPb.SetParameter(Consts.MetadataSerialiser_SceneOverridesDirectoryParameterName,
                sceneOverridesDirectory);

            // If we are converting DLC then we add the parameter to mark up the Core Metadata Zip.
            // We only add the parameter if we are exporting entities; its a poor criteria but saves
            // errors for DLC interiors and props.
            if (param.Branch.Project.IsDLC && exportEntities && !param.Branch.Project.ForceFlags.AllNewContent)
            {
                String metadataZipFilename = outputAsset.AbsolutePath;
                String coreMetadataZipFilename = metadataZipFilename.Replace(param.Branch.Processed,
                    param.CoreBranch.Processed, StringComparison.OrdinalIgnoreCase);
                metadataPb.SetParameter(Constants.ParamMap_CoreMetadataZip, coreMetadataZipFilename);
                syncDependencies.Add(owner.CreateFile(coreMetadataZipFilename));
            }

            IContentNode audioCollisionMapFileNode = owner.CreateFile(audioCollisionMapPathname);
            IContentNode sceneOverridesDirectoryNode = owner.CreateDirectory(sceneOverridesDirectory);
            metadataPb.Inputs.Add(audioCollisionMapFileNode);
            metadataPb.Inputs.Add(sceneOverridesDirectoryNode);

            metadataPb.Outputs.Add(owner.CreateDirectory(metadataDirectory));
            if (exportArchetypes)
                metadataPb.Outputs.Add(owner.CreateAsset(processedITYP, Platform.Platform.Independent));
            if (exportEntities)
                metadataPb.Outputs.Add(owner.CreateAsset(processedIMAP, Platform.Platform.Independent));
            metadataPb.Outputs.Add(owner.CreateFile(mapExportAdditionsPathname));

            return (true);
        }

        private bool CreateMetadataSerialiserProcessBuilderForMergeSplit(
            IEngineParameters param,
            IProcessorCollection processors,
            ContentTreeHelper sourceContentTreeHelper,
            IContentTree owner,
            ICollection<IContentNode> syncDependencies,
            MetadataMergeSplit.MergeSplitDetails metadataMergeSplitDetails,
            String combineDocumentPathname, String audioCollisionMapPathname, String sceneOverridesDirectory,
            out ProcessBuilder metadataProcessBuilder, out Content.Directory metadataOutputDirectoryNode)
        {
            Content.Asset outputAsset = (Content.Asset)metadataMergeSplitDetails.MetadataMergeSplitNode;
            String processedDestination = GetProcessedZipAssetCacheDir(param, outputAsset);
            String metadataOutputDirectory = System.IO.Path.Combine(processedDestination, metadataDirectoryName_);
            String processedBasename = outputAsset.Basename;

            String cacheDirectory = (String)this.Parameters[Consts.MapProcess_ProcessedMapCacheDirectory];
            IProcessor metadataProcessor = processors.GetProcessor("RSG.Pipeline.Processor.Map.MetadataSerialiser");
            Debug.Assert(null != metadataProcessor, "Map Metadata Serialiser processor not defined.");
            if (!metadataProcessor.Parameters.ContainsKey(Consts.MapProcess_ProcessedMapCacheDirectory))
                metadataProcessor.Parameters.Add(Consts.MapProcess_ProcessedMapCacheDirectory, cacheDirectory);
            metadataProcessBuilder = new ProcessBuilder(metadataProcessor, owner);
            metadataProcessBuilder.SetParameter(Constants.ParamMap_ExportArchetypes, true);
            metadataProcessBuilder.SetParameter(Constants.ParamMap_ExportEntities, true);
            metadataProcessBuilder.SetParameter(Consts.MetadataSerialiser_IsLODParameterName, false);
            metadataProcessBuilder.SetParameter(Consts.MetadataSerialiser_MapExportAdditionsPathnameParameterName,
                metadataMergeSplitDetails.MapExportAdditionsPathname);
            metadataProcessBuilder.SetParameter(Consts.MetadataSerialiser_AudioCollisionMapPathnameParameterName,
                audioCollisionMapPathname);
            metadataProcessBuilder.SetParameter(Consts.MetadataSerialiser_SceneOverridesDirectoryParameterName,
                sceneOverridesDirectory);
            metadataProcessBuilder.SetParameter(Consts.MetadataSerialiser_ITYPMergeParameterName, true);

            // If we are converting DLC then we add the parameter to mark up the Core Metadata Zip.
            if (param.Branch.Project.IsDLC && !param.Branch.Project.ForceFlags.AllNewContent)
            {
                String metadataZipFilename = outputAsset.AbsolutePath;
                String coreMetadataZipFilename = metadataZipFilename.Replace(param.Branch.Processed,
                    param.CoreBranch.Processed, StringComparison.OrdinalIgnoreCase);
                metadataProcessBuilder.SetParameter(Constants.ParamMap_CoreMetadataZip, coreMetadataZipFilename);
                syncDependencies.Add(owner.CreateFile(coreMetadataZipFilename));
            }

            if (combineDocumentPathname != null)
            {
                metadataProcessBuilder.SetParameter(
                    Consts.MetadataSerialiser_AssetCombineDocumentPathnameParameterName,
                    combineDocumentPathname);
            }

            // As runtime want IMAP merge to stay as is, but ITYP merge to be global,
            // we have fairly complex information to the metadata serialiser.
            // We need to go back to the associated zips to find the IMAP merge (based on common processed output) rules.
            Dictionary<IContentNode, List<IContentNode>> processedToXmlInputsMap = new Dictionary<IContentNode, List<IContentNode>>();
            foreach (IContentNode inputSceneXmlNode in metadataMergeSplitDetails.InputSceneXmlNodes)
            {
                if (param.Branch.Project.IsDLC && !param.Branch.Project.ForceFlags.AllNewContent)
                {
                    // For DLC we add the DLC SceneXml if it exists; otherwise fallback to core project.
                    IFilesystemNode inputSceneXmlFile = (IFilesystemNode)inputSceneXmlNode;
                    if (System.IO.File.Exists(inputSceneXmlFile.AbsolutePath))
                        metadataProcessBuilder.Inputs.Add(inputSceneXmlNode);
                    else
                    {
#warning DHM FIX ME: find out where the lowercase filename came from (use mpMapWorking DLC).
                        String coreSceneXmlFilename = inputSceneXmlFile.AbsolutePath.ToLower();
                        coreSceneXmlFilename = coreSceneXmlFilename.Replace(param.Branch.Export.ToLower(), param.CoreBranch.Export.ToLower());
                        metadataProcessBuilder.Inputs.Add(owner.CreateFile(coreSceneXmlFilename));
                    }
                }
                else
                {
                    // Default case; just add the current SceneXml filename.
                    metadataProcessBuilder.Inputs.Add(inputSceneXmlNode);
                }


                IContentNode sourceInputSceneXmlNode = inputSceneXmlNode;
                if (inputSceneXmlNode.Parameters.ContainsKey(Consts.MetadataSerialiser_SourceInputParameter))
                    sourceInputSceneXmlNode = inputSceneXmlNode.Parameters[Consts.MetadataSerialiser_SourceInputParameter] as IContentNode;

                IContentNode exportZipNode = sourceContentTreeHelper.GetExportZipNodeFromSceneXmlNode(sourceInputSceneXmlNode);
                IContentNode processedZipNode = sourceContentTreeHelper.GetProcessedZipNodeFromExportZipNode(exportZipNode);
                if (null == processedZipNode)
                {
                    param.Log.ErrorCtx(logContext_, "Map Export Node: '{0}' does not have Map Process Node.", exportZipNode.Name);
                }
                else
                {
                    if (!processedToXmlInputsMap.ContainsKey(processedZipNode))
                        processedToXmlInputsMap.Add(processedZipNode, new List<IContentNode>());
                    processedToXmlInputsMap[processedZipNode].Add(inputSceneXmlNode);
                }
            }

            foreach (KeyValuePair<IContentNode, List<IContentNode>> processedToXmlInputsPair in processedToXmlInputsMap)
            {
                String inputToProcessKey = String.Format("{0}:{1}", Consts.MetadataSerialiser_InputToProcessParameterPrefix,
                    processedToXmlInputsPair.Key.Name);
                if (!metadataProcessBuilder.Parameters.ContainsKey(inputToProcessKey))
                    metadataProcessBuilder.Parameters.Add(inputToProcessKey, processedToXmlInputsPair.Value);
                else
                    param.Log.MessageCtx(logContext_, "Attempted to add metadata processor input twice: {0}", processedToXmlInputsPair.Value);

            }

            IContentNode audioCollisionMapFileNode = owner.CreateFile(audioCollisionMapPathname);
            IContentNode sceneOverridesDirectoryNode = owner.CreateDirectory(sceneOverridesDirectory);
            metadataProcessBuilder.Inputs.Add(audioCollisionMapFileNode);
            metadataProcessBuilder.Inputs.Add(sceneOverridesDirectoryNode);

            foreach (IContentNode sceneXmlDependencyNode in metadataMergeSplitDetails.SceneXmlNodeDependencies)
                metadataProcessBuilder.Inputs.Add(sceneXmlDependencyNode);

            // Setup metadata serialiser output parameters
            metadataOutputDirectoryNode = (Content.Directory)owner.CreateDirectory(metadataOutputDirectory);
            metadataProcessBuilder.Outputs.Add(metadataOutputDirectoryNode);
            metadataProcessBuilder.Outputs.Add(owner.CreateFile(metadataMergeSplitDetails.MapExportAdditionsPathname));

            return true;
        }

        /// <summary>
        /// Create the metadata serialiser process used by the instance placement so that IMAP 
        /// added to the RPFs have their dependencies properly registered in the manifest.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="process"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="metadataProcess"></param>
        /// <returns></returns>
        private bool CreateInstanceMetadataSerialiserProcessBuilder(IEngineParameters param,
            IProcess process, IProcessorCollection processors, IContentTree owner, IEnumerable<ISet<IContentNode>> inputNodeSets,
            String mapExportAdditionsPathname, out ProcessBuilder metadataPb)
        {
            Content.Asset outputAsset = (Asset)process.Outputs.First();
            String processedDestination = GetProcessedZipAssetCacheDir(param, outputAsset);
            String metadataDirectory = System.IO.Path.Combine(processedDestination, metadataDirectoryName_);

            // Create Metadata Serialisation Process for map.
            String cacheDirectory = (String)this.GetParameter(Consts.MapProcess_ProcessedMapCacheDirectory, String.Empty);
            IProcessor metadataProcessor = processors.GetProcessor("RSG.Pipeline.Processor.Map.MetadataSerialiser");
            Debug.Assert(null != metadataProcessor, "Map Metadata Serialiser processor not defined.");
            if (!metadataProcessor.Parameters.ContainsKey(Consts.MapProcess_ProcessedMapCacheDirectory))
                metadataProcessor.SetParameter(Consts.MapProcess_ProcessedMapCacheDirectory, cacheDirectory);
            metadataPb = new ProcessBuilder(metadataProcessor, owner);
            metadataPb.SetParameter(Constants.ParamMap_ExportArchetypes, false);
            metadataPb.SetParameter(Constants.ParamMap_ExportEntities, true);
            metadataPb.SetParameter(Consts.MetadataSerialiser_MapExportAdditionsPathnameParameterName,
                mapExportAdditionsPathname);

            foreach (IEnumerable<IContentNode> inputNodes in inputNodeSets)
            {
                foreach (IContentNode inputNode in inputNodes)
                {
                    if (inputNode is Content.File)
                    {
                        Content.File inputFileNode = inputNode as Content.File;
                        String[] extensions = RSG.Platform.Filename.GetExtensions(inputFileNode.AbsolutePath);
                        RSG.Platform.FileType[] fileTypes = RSG.Platform.Filename.GetFileTypes(extensions);
                        if (fileTypes[0] == RSG.Platform.FileType.IMAP)
                        {
                            metadataPb.Inputs.Add(inputFileNode);
                        }
                    }
                    else if (inputNode is Content.Directory)
                    {
                        Content.Directory inputDirNode = inputNode as Content.Directory;
                        metadataPb.Inputs.Add(inputDirNode);
                    }
                }
            }

            metadataPb.Outputs.Add(owner.CreateDirectory(metadataDirectory));
            metadataPb.Outputs.Add(owner.CreateFile(mapExportAdditionsPathname));

            return (true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <param name="process"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="manifestAdditionsPathname"></param>
        /// <param name="mapCacheDir"></param>
        /// <param name="buildingProcessedZips"></param>
        /// <param name="collisionProcessBuilder"></param>
        /// <param name="collisionPackBuilder"></param>
        /// <returns></returns>
        private bool CreateCollisionProcessBuilder(IEngineParameters param,
            IProcess process,
            IProcessorCollection processors,
            IContentTree owner,
            String manifestAdditionsPathname,
            String mapCacheDir,
            bool buildingProcessedZips,
            out ProcessBuilder collisionProcessBuilder,
            out ProcessBuilder collisionPackBuilder)
        {
            IProcessor collisionProcessor = processors.FirstOrDefault(p => p.Name == "RSG.Pipeline.Processor.Map.Collision");
            Debug.Assert(null != collisionProcessor, "Map Collision processor not defined.");
            if (null == collisionProcessor)
            {
                collisionProcessBuilder = null;
                collisionPackBuilder = null;
                return (false);
            }

            collisionProcessBuilder = new ProcessBuilder(collisionProcessor, owner);
            collisionPackBuilder = null; // Optionally created below.

            Asset outputAsset = (Asset)process.Outputs.First();
            String processedDestination = GetProcessedZipAssetCacheDir(param, outputAsset);
            String processedBoundsDirectory = Path.Combine(processedDestination, processedBoundsDirectoryName_);
            // B* 1229899 - we don't send IBD.ZIPs to Rage any more
            collisionProcessBuilder.Outputs.Add(owner.CreateDirectory(processedBoundsDirectory, "*.ibn.zip"));

            // Set up manifest additions data
            collisionProcessBuilder.Parameters.Add(Consts.Collision_ManifestAdditionsPathnameParameterName, manifestAdditionsPathname);
            collisionProcessBuilder.Outputs.Add(owner.CreateFile(manifestAdditionsPathname));

            // If we're building processed zips then create an Asset Pack process for it.
            if (buildingProcessedZips)
            {
                IProcessor assetPackProcessor = processors.FirstOrDefault(p => p.Name == "RSG.Pipeline.Processor.Common.AssetPackProcessor");
                if (null == assetPackProcessor)
                {
                    param.Log.Error("Failed to find Asset Pack Processor for packing processed bounds.");
                    return (false);
                }

                // Create XML input file.
                File processedZipNode = (File)process.Outputs.First();
                String processedZipDirectory = Path.GetDirectoryName(processedZipNode.AbsolutePath);
                String processedZipFilename = Path.Combine(processedZipDirectory, processedZipNode.Basename + "_collision.zip");
                String scheduleFilename = Path.Combine(mapCacheDir, processedZipNode.Basename + "_processed_collision_asset_pack_schedule.xml");
                AssetPackScheduleBuilder scheduleBuilder = new AssetPackScheduleBuilder();
                AssetPackZipArchive processedCollZip = scheduleBuilder.AddZipArchive(processedZipFilename);
                processedCollZip.AddDirectory(processedBoundsDirectory, null, "*.ibd.zip");
                processedCollZip.AddDirectory(processedBoundsDirectory, null, "*.ibn.zip");
                scheduleBuilder.Save(scheduleFilename);

                // Create Process.
                collisionPackBuilder = new ProcessBuilder(assetPackProcessor, owner);
                collisionPackBuilder.Inputs.Add(owner.CreateFile(scheduleFilename));
            }

            return (true);
        }

        /// <summary>
        /// Create the Asset Pack process.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="process"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="assetPackProcess"></param>
        /// <returns></returns>
        /// TODO : param and process are not used
        private bool CreateAssetPackProcessBuilder(IEngineParameters param,
            IProcess process,
            IProcessorCollection processors,
            IContentTree owner,
            out ProcessBuilder assetPackProcess)
        {
            IProcessor assetPackProcessor = processors.FirstOrDefault(p => p.Name == "RSG.Pipeline.Processor.Common.AssetPackProcessor");
            Debug.Assert(null != assetPackProcessor, "Asset Pack processor not defined.");
            if (null == assetPackProcessor)
            {
                assetPackProcess = null;
                return (false);
            }

            assetPackProcess = new ProcessBuilder(assetPackProcessor, owner);
            return (true);
        }

        /// <summary>
        /// Create the RAGE platform conversion processes.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="process"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="input"></param>
        /// <param name="metadataDirectory"></param>
        /// <param name="mapExportAdditionsPathname"></param>
        /// <param name="collisionManifestAdditionsPathname"></param>
        /// <param name="rageProcessBuilder"></param>
        /// <returns></returns>
#warning DHM FIX ME: should use PlatformProcessBuilder for constant cache directories etc.
        private bool CreateRAGEProcessBuilder(IEngineParameters param,
            IProcess process, IProcessorCollection processors, IContentTree owner, IContentNode input,
            String metadataDirectory, String mapExportAdditionsPathname, String collisionManifestAdditionsPathname,
            out ProcessBuilder rageProcessBuilder)
        {
            IProcessor rageProcessor = processors.FirstOrDefault(p => p.Name == "RSG.Pipeline.Processor.Platform.Rage");
            Debug.Assert(null != rageProcessor, "Asset Pack processor not defined.");
            if (null == rageProcessor)
            {
                rageProcessBuilder = null;
                return (false);
            }

            rageProcessBuilder = new ProcessBuilder(rageProcessor, owner);
            foreach (IProcess rageProcessWithInput in process.Owner.FindProcessesWithInput(input).Where(p => p.ProcessorClassName.Equals("RSG.Pipeline.Processor.Platform.Rage")))
            {
                Debug.Assert(rageProcessWithInput.Outputs.Count() == 1);
                IFilesystemNode outputFromInitialTree = rageProcessWithInput.Outputs.First() as IFilesystemNode;
                IContentNode output;
                if (outputFromInitialTree == null)
                {
                    output = rageProcessWithInput.Outputs.First();
                }
                else
                {
                    output = owner.CreateFile(outputFromInitialTree.AbsolutePath);
                    output.CopyParametersFrom(outputFromInitialTree);
                }

                rageProcessBuilder.Outputs.Add(output);
            }
            rageProcessBuilder.Parameters.Add(Core.Constants.RageProcess_DelayedDirectoryEvaluationParameterName, true);
            if (!String.IsNullOrEmpty(metadataDirectory))
                rageProcessBuilder.Parameters.Add(Core.Constants.RpfProcess_MetadataDirectory, metadataDirectory);
            if (!String.IsNullOrEmpty(mapExportAdditionsPathname))
                rageProcessBuilder.Parameters.Add(Core.Constants.ManifestProcess_MapExportAdditions, mapExportAdditionsPathname);
            if (!String.IsNullOrEmpty(collisionManifestAdditionsPathname))
                rageProcessBuilder.Parameters.Add(Core.Constants.ManifestProcess_BoundsProcessorAdditions, collisionManifestAdditionsPathname);

            // We add a prefix parameter to the Rage Process (specifically for DLC, but
            // its supported in all projects).
            String prefix = MapAsset.GetMapProjectPrefix(param.Branch.Project);
            if (!String.IsNullOrEmpty(prefix))
                rageProcessBuilder.Parameters.Add(Constants.RageProcess_ResourcePrefix, prefix);

            return (true);
        }

        /// <summary>
        /// Return source map cache directory for an asset.
        /// </summary>
        /// <param name="asset"></param>
        /// <returns></returns>
        private String GetSourceZipAssetCacheDir(IEngineParameters param, Content.File asset)
        {
            Debug.Assert(this.Parameters.ContainsKey(Consts.MapProcess_SourceMapCacheDirectory),
                "Cache directory not found in parameters!  Internal error.");
            if (!this.Parameters.ContainsKey(Consts.MapProcess_SourceMapCacheDirectory))
            {
                param.Log.ErrorCtx(logContext_, "Cache directory not found in parameters!  Internal error.");
                throw (new NotSupportedException("Cache directory not found in parameters!  Internal error."));
            }

            String cacheDirectory = (String)this.Parameters[Consts.MapProcess_SourceMapCacheDirectory];
            String cacheRoot = String.Empty;
#warning DHM FIX ME: think these will now return the same thing.
            if (param.Branch.Project.IsDLC && !param.Branch.Project.ForceFlags.AllNewContent)
                cacheRoot = Path.GetFullPath(param.Branch.Environment.Subst(cacheDirectory));
            else
                cacheRoot = Path.GetFullPath(param.CoreBranch.Environment.Subst(cacheDirectory));

            return (Path.Combine(cacheRoot, asset.Name));
        }

        /// <summary>
        /// Return processed map cache directory for an asset.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="asset"></param>
        /// <returns></returns>
        private String GetProcessedZipAssetCacheDir(IEngineParameters param, Content.File asset)
        {
            Debug.Assert(this.Parameters.ContainsKey(Consts.MapProcess_ProcessedMapCacheDirectory),
                "Cache directory not found in parameters!  Internal error.");
            if (!this.Parameters.ContainsKey(Consts.MapProcess_ProcessedMapCacheDirectory))
            {
                param.Log.ErrorCtx(logContext_, "Cache directory not found in parameters!  Internal error.");
                throw (new NotSupportedException("Cache directory not found in parameters!  Internal error."));
            }

            String cacheDirectory = (String)this.Parameters[Consts.MapProcess_ProcessedMapCacheDirectory];
            String cacheRoot = String.Empty;
#warning DHM FIX ME: think these will now return the same thing.
            if (param.Branch.Project.IsDLC && !param.Branch.Project.ForceFlags.AllNewContent)
                cacheRoot = Path.GetFullPath(param.Branch.Environment.Subst(cacheDirectory));
            else
                cacheRoot = Path.GetFullPath(param.CoreBranch.Environment.Subst(cacheDirectory));

            return (Path.Combine(cacheRoot, asset.Name));
        }

        private String GetDefaultSkeletonPathname(IEngineParameters param)
        {
            Debug.Assert(this.Parameters.ContainsKey(Consts.MapProcess_DefaultSkeletonParameterName),
                "Default skeleton pathname not found in parameters!  Internal error.");
            if (!this.Parameters.ContainsKey(Consts.MapProcess_DefaultSkeletonParameterName))
            {
                param.Log.ErrorCtx(logContext_, "Default skeleton pathname not found in parameters!  Internal error.");
                throw (new NotSupportedException("Default skeleton pathname not found in parameters!  Internal error."));
            }

            String defaultSkeletonPathname = (String)this.Parameters[Consts.MapProcess_DefaultSkeletonParameterName];
            return defaultSkeletonPathname;
        }

        private bool BuildingProcessedZips(IEngineParameters param)
        {
            IConfig config = param.Branch.Project.Config;
            IUsertype builderClientUsertype = config.Usertypes.FirstOrDefault(
                usertype => "builder_client".Equals(usertype.Name, StringComparison.OrdinalIgnoreCase));
            IUsertype builderServerUsertype = config.Usertypes.FirstOrDefault(
                usertype => "builder_server".Equals(usertype.Name, StringComparison.OrdinalIgnoreCase));
            bool userRequiresProcessedZipBuilds = ((builderClientUsertype != null) && ((config.Usertype & builderClientUsertype.Flags) != 0)) ||
                                                  ((builderServerUsertype != null) && ((config.Usertype & builderServerUsertype.Flags) != 0));
            bool overrideRequiresProcessedZipBuilds = true;
            if (this.Parameters.ContainsKey(Consts.MapProcess_ForceProcessedMapZipCreation))
                overrideRequiresProcessedZipBuilds = (bool)this.Parameters[Consts.MapProcess_ForceProcessedMapZipCreation];

            return (userRequiresProcessedZipBuilds || overrideRequiresProcessedZipBuilds);
        }
        #endregion // Private Methods

        #region Static Public Methods
        /// <summary>
        /// Resets static shite.
        /// - Needs reviewed - processor should not be reliant on static data?!
        /// - Made a public method to call the internal reset that is required here, and this will be called externally.
        /// </summary>
        public static void ResetStaticData() // for want of a better method name?
        {
            // B* 1070319
            // JWR - this is horrendous, but is unfortunately required for now.  When we move to considering all
            // map pre-processes at once we can remove this.
            //
            // Any map component would would be a reasonable place to do this.  
            // There's no specific reason for it to be Collision
            AssetCombine.Coordinator.Instance.Reset();
            InterContainerLODDependencies.Coordinator.Instance.Reset();
            MetadataMergeSplit.Coordinator.Instance.Reset();
        }
        #endregion // Static Public Methods
    }

} // RSG.Pipeline.Processor.Map namespace
