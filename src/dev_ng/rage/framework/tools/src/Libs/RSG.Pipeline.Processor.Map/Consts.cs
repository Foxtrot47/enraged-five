﻿using System;

namespace RSG.Pipeline.Processor.Map
{

    /// <summary>
    /// Map processor(s) constants.
    /// </summary>
    public class Consts
    {
        /// <summary>
        /// Map cache directory for source map extraction.
        /// </summary>
        internal static readonly String MapProcess_SourceMapCacheDirectory = 
            "Source Map Cache Directory";

        /// <summary>
        /// Map cache directory for processed map construction.
        /// </summary>
        internal static readonly String MapProcess_ProcessedMapCacheDirectory =
            "Processed Map Cache Directory";

        /// <summary>
        /// Map section name; used for gathering parent texture dictionray information.
        /// </summary>
        internal static readonly String MapProcess_MapSectionName =
            "Map Section Name";

        /// <summary>
        /// Map cache directory for processed map construction.
        /// </summary>
        internal static readonly String MapProcess_DefaultSkeletonParameterName =
            "Default Skeleton";

        /// <summary>
        /// Map cache directory for processed map construction.
        /// </summary>
        internal static readonly String MapProcess_ForceProcessedMapZipCreation =
            "Force Processed Map Zip Creation";

        /// <summary>
        /// Option to enable the processing/pruning of shared textures in the map pipeline.
        /// </summary>
        internal static readonly String MapProcess_MapGTXDPipelineEnabled =
            "Enable Map GTXD Pipeline Process";

        /// <summary>
        /// Option to enable the processing/pruning of shared textures in the map pipeline per branch, branches are separated with a ';'.
        /// </summary>
        internal static readonly String MapProcess_MapGTXDPipelineEnabledPerBranch =
            "Enable Map GTXD Pipeline Process Per Branch";

        /// <summary>
        /// Option to enable the processing/pruning of shared textures in the map pipeline per project, projects are separated with a ';'.
        /// </summary>
        internal static readonly String MapProcess_MapGTXDPipelineEnabledPerProject =
            "Enable Map GTXD Pipeline Process Per Project";

        /// <summary>
        /// Option to enable the processing of instance placement.
        /// </summary>
        internal static readonly String MapProcess_MapInstancePlacementPipelineEnabled =
            "Enable Instance Placement Pipeline Process";

        /// <summary>
        /// Option to enable the processing of instance placement.
        /// </summary>
        internal static readonly String MapProcess_MapInstancePlacementIntersectionEnabled =
            "Enable Instance Placement Intersection Checks";

        /// <summary>
        /// Dictates the source input file.  Used when map metadata is overridden.
        /// </summary>
        internal static readonly String MetadataSerialiser_SourceInputParameter =
            "source_input";

        /// <summary>
        /// Prefix for a parameter that specifies an input to be processed by the metadata serialiser
        /// </summary>
        internal static readonly String MetadataSerialiser_InputToProcessParameterPrefix =
            "input_to_process_";

        /// <summary>
        /// An optional parameter that specifies the asset combine document pathname
        /// </summary>
        internal static readonly String MetadataSerialiser_AssetCombineDocumentPathnameParameterName =
            "asset_combine_document_pathname";

        /// <summary>
        /// A parameter that specifies the map export additions document pathname
        /// </summary>
        internal static readonly String MetadataSerialiser_MapExportAdditionsPathnameParameterName =
            "map_export_additions_pathname";

        /// <summary>
        /// A parameter that specifies the audio collision mapping csv pathname
        /// </summary>
        internal static readonly String MetadataSerialiser_AudioCollisionMapPathnameParameterName =
            "audio_collision_map_pathname";

        /// <summary>
        /// A parameter that specifies the scene override data directory
        /// </summary>
        internal static readonly String MetadataSerialiser_SceneOverridesDirectoryParameterName =
            "scene_overrides_directory";

        /// <summary>
        /// A parameter that specifies additional imap files to be added to the manifest file.
        /// </summary>
        internal static readonly String MetadataSerialiser_AdditionalManifestDependenciesParameterName =
            "additional_manifest_dependencies";

        /// <summary>
        /// A parameter that specifies whether this processor will build metadata for a LOD container
        /// </summary>
        internal static readonly String MetadataSerialiser_IsLODParameterName =
            "is_lod";

        /// <summary>
        /// A parameter that specifies the map export additions document pathname
        /// </summary>
        internal static readonly String MetadataSerialiser_ITYPMergeParameterName =
            "ityp_merge";

        /// <summary>
        /// Maximum number of metadata conversion tasks per job
        /// </summary>
        internal static readonly String MetadataSerialiser_MaxTasksPerJobParameterName =
            "Max Tasks Per Job";

        /// <summary>
        /// Relative cost of metadata conversion for a LOD container
        /// </summary>
        internal static readonly String MetadataSerialiser_LODTaskWeightParameterName =
            "LOD Task Weight";

        /// <summary>
        /// Semi-colon delimited string of which time cycle types to exclude from serialisation
        /// </summary>
        internal static readonly String MetadataSerialiser_NonSerialisedTimeCycleTypesParameterName =
            "Non Serialised TimeCycle Types";

        /// <summary>
        /// A parameter that specifies the map export additions document pathname
        /// </summary>
        internal static readonly String Collision_ManifestAdditionsPathnameParameterName =
            "manifest_additions_pathname";

        /// <summary>
        /// Prefix for a parameter that specifies a bounds data input to be processed by the bounds processor
        /// </summary>
        internal static readonly String Collision_UnprocessedBoundsZipParameterPrefix =
            "unprocessed_bounds_zip_";

        /// <summary>
        /// Prefix for a parameter that specifies a scene xml input to be processed by the bounds processor
        /// </summary>
        internal static readonly String Collision_SceneXmlParameterPrefix =
            "scene_xml_";

        /// <summary>
        /// Prefix for a parameter that specifies the scene type of an input to be processed by the bounds processor
        /// </summary>
        internal static readonly String Collision_SceneTypeParameterPrefix =
            "scene_type_";

        /// <summary>
        /// Prefix for a parameter that specifies the list of drawables we need to pack bounds into.
        /// </summary>
        internal static readonly String Collision_DrawableListParameterPrefix =
            "prop_drawables_";

        /// <summary>
        /// Branching conditionnal used to create a different Bound Processor Config for props merge packing and regular exports
        /// </summary>
        internal static readonly string Collision_AllInputsAreProps = 
            "AllInputsAreProps";

        /// <summary>
        /// Filepath for the parent txd xml file.
        /// </summary>
        public static readonly String Textures_ParentTextureDictionaryModel =
            "parent_txd_model";

        /// <summary>
        /// Parameter for specifying any additional manifest dependencies to the metadata serialiser.
        /// </summary>
        internal static readonly String InstancePlacement_AdditionalManifestDependenciesParameter =
            "additional_manifest_dependencies";

        /// <summary>
        /// List of all of the enabled instance placement types in the pipeline.
        /// </summary>
        internal static readonly String InstancePlacement_EnabledPlacementTypes =
            "Enabled Instance Placement Types";

        /// <summary>
        /// Specifies a global texture to use in the pipeline for placing instances.
        /// </summary>
        internal static readonly String InstancePlacement_GlobalTexture =
            "Global Instance Placement Texture";

        /// <summary>
        /// Circumvents the content system and instead allows the pipeline to drop instances on
        /// all map_container nodes.
        /// </summary>
        internal static readonly String InstancePlacement_PlaceOnMapContainers =
            "Place Instances on All Map Containers";

        /// <summary>
        /// Type of object to drop..
        /// </summary>
        internal static readonly String InstancePlacement_PlacementType =
            "instance_placement_type";

        /// <summary>
        /// Type of object to drop..
        /// </summary>
        internal static readonly String InstancePlacement_InputTextures =
            "instance_placement_input_textures";

        /// <summary>
        /// Path to our global data texture to be passed to the tool.
        /// </summary>
        internal static readonly String InstancePlacement_GlobalDataTexture =
            "Global Data Texture";

        /// <summary>
        /// Path to our global data texture to be passed to the tool.
        /// </summary>
        internal static readonly String InstancePlacement_GlobalTintTexture =
            "Global Tint Texture";

        /// <summary>
        /// Determines whether this process has a 3D Studio Max file that needs to be processed.
        /// </summary>
        internal static readonly String InstancePlacement_AdditionalSceneXMLDependencies =
            "additional_scene_xml_dependencies";

        /// <summary>
        /// Overlapping container nodes.
        /// </summary>
        internal static readonly String InstancePlacement_OverlappingContainerDependencies =
            "instance_placement_overlapping_containers";

    }

} // RSG.Pipeline.Processor.Map namespace
