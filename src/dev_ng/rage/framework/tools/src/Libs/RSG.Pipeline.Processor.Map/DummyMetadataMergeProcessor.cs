﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using RSG.Base.Configuration;
using XGE = RSG.Interop.Incredibuild.XGE;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Attributes;
using RSG.Pipeline.Services;

namespace RSG.Pipeline.Processor.Map
{
    /// <summary>
    /// Dummy pipeline processor; used only for marking up metadata merge in the content tree.
    /// </summary>
    [Export(typeof(IProcessor))]
    [ProcessorFlags(ProcessorFlags.Compatibility)]
    public class DummyMetadataMergeProcessor :
        ProcessorBase,
        IProcessor
    {
        #region Constants
        private static readonly String DESCRIPTION = "Dummy Metadata Merge Processor";
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public DummyMetadataMergeProcessor()
            : base(DESCRIPTION)
        {
        }
        #endregion // Constructor(s)

        #region IProcessor Interface Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="process"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="resultantProcesses"></param>
        /// <returns></returns>
        public override bool Prebuild(IEngineParameters param,
            IProcess process, IProcessorCollection processors,
            IContentTree owner, out IEnumerable<IContentNode> syncDependencies,
            out IEnumerable<IProcess> resultantProcesses)
        {
            process.State = ProcessState.Discard;
            syncDependencies = new List<IContentNode>();
            resultantProcesses = new List<IProcess>();

            return true;
        }

        /// <summary>
        /// Prepare a build for the processes; populating the XGE project with
        /// the tools, environments and tasks required.  Including setting up
        /// the task dependency chain.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="processes"></param>
        /// <param name="tools"></param>
        /// <param name="tasks"></param>
        /// <returns></returns>
        public override bool Prepare(IEngineParameters param, IEnumerable<IProcess> processes,
            out IEnumerable<XGE.ITool> tools, out IEnumerable<XGE.ITask> tasks)
        {
            // We are discarded in prebuild so we should never get here
            throw (new NotImplementedException());
        }
        #endregion // IProcessor Interface Methods
    }

} // RSG.Pipeline.Processor.Map namespace
