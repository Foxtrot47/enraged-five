﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.ServiceModel.Web;
using RSG.Services.Common.ServiceModelExtensions;
using RSG.Pipeline.Automation.Database.Domain.Entities.Jobs;
using RSG.Pipeline.Automation.Database.Domain.Entities;
using RSG.Pipeline.Automation.Database.Domain.Entities.JobResults;
using RSG.Pipeline.Automation.Database.Domain.Entities.Stats;
using RSG.Pipeline.Automation.Database.Domain.Entities.Logs;

namespace RSG.Pipeline.Automation.Database.Common.ServiceContract
{
    [ServiceContract]
    public interface IJobDatabaseService
    {
        #region Data Submission
        /// <summary>
        /// Endpoint for creating a job
        /// </summary>
        /// <param name="session"></param>
        [OperationContract]
        [CorsEnabled]
        [WebInvoke(Method = "PUT", UriTemplate = "CreateJob")]
        bool CreateJob(JobModel job);

        /// <summary>
        /// Endpoint for updating a job
        /// </summary>
        /// <param name="session"></param>
        [OperationContract]
        [CorsEnabled]
        [WebInvoke(Method = "PUT", UriTemplate = "UpdateJob")]
        bool UpdateJob(JobModel job);


        /// <summary>
        /// Endpoint for creating a job result 
        /// </summary>
        /// <param name="session"></param>
        [OperationContract]
        [CorsEnabled]
        [WebInvoke(Method = "PUT", UriTemplate = "CreateJobResult")]
        bool CreateJobResult(JobResultModel jobResult);

        /// <summary>
        /// Endpoint for updating a job result 
        /// </summary>
        /// <param name="session"></param>
        [OperationContract]
        [CorsEnabled]
        [WebInvoke(Method = "PUT", UriTemplate = "UpdateJobResult")]
        bool UpdateJobResult(JobResultModel jobResult);

        /// <summary>
        /// Endpoint for creating a log message
        /// </summary>
        /// <param name="session"></param>
        [OperationContract]
        [CorsEnabled]
        [WebInvoke(Method = "PUT", UriTemplate = "CreateLogMessage")]
        bool CreateLogMessage(MessageModel logMessage);


        /// <summary>
        /// Endpoint for creating a log message
        /// </summary>
        /// <param name="session"></param>
        [OperationContract]
        [CorsEnabled]
        [WebInvoke(Method = "PUT", UriTemplate = "CreateLogMessageFromList")]
        bool CreateLogMessageFromList(IEnumerable<MessageModel> logMessages);
        #endregion // Data Submission

        #region Data Retrieval
        /// <summary>
        /// Endpoint for getting all jobs. Limit and offset are optional parameters which allow us to paginate 
        /// the data on the portal. This will result in faster queries.
        /// </summary>
        /// <param name="session"></param>
        [OperationContract]
        [CorsEnabled]
        [WebInvoke(Method = "GET", UriTemplate = "AllJobs?server={server}&limit={limit}&offset={offset}")]
        IList<JobModel> GetAllJobs(String server = "", int limit = -1, int offset = -1);

        /// <summary>
        /// Endpoint for getting all jobs in a date range. Limit and offset are optional parameters which allow us to paginate 
        /// the data on the portal. This will result in faster queries.
        /// </summary>
        /// <param name="session"></param>
        [OperationContract]
        [CorsEnabled]
        [WebInvoke(Method = "GET", UriTemplate = "AllJobsByDate?from={from}&to={to}&server={server}&limit={limit}&offset={offset}")]
        IList<JobModel> GetAllJobsByDate(String from, String to, String server = "", int limit = -1, int offset = -1);

        /// <summary>
        /// Endpoint for getting all jobs. Limit and offset are optional parameters which allow us to paginate 
        /// the data on the portal. This will result in faster queries.
        /// </summary>
        /// <param name="session"></param>
        [OperationContract]
        [CorsEnabled]
        [WebInvoke(Method = "GET", UriTemplate = "AllJobsByType/{type}?server={server}&limit={limit}&offset={offset}")]
        IList<JobModel> GetAllJobsByType(string type, String server = "", int limit = -1, int offset = -1);

        /// <summary>
        /// Endpoint for getting all jobs in a date range. Limit and offset are optional parameters which allow us to paginate 
        /// the data on the portal. This will result in faster queries.
        /// </summary>
        /// <param name="session"></param>
        [OperationContract]
        [CorsEnabled]
        [WebInvoke(Method = "GET", UriTemplate = "AllJobsByDateAndType/{type}?from={from}&to={to}&server={server}&limit={limit}&offset={offset}")]
        IList<JobModel> GetAllJobsByDateAndType(String type, String from, String to, String server = "", int limit = -1, int offset = -1);

        /// <summary>
        /// Endpoint for getting all jobs for a single processing host. Limit and offset are optional parameters which allow us to paginate 
        /// the data on the portal. This will result in faster queries.
        /// </summary>
        /// <param name="session"></param>
        [OperationContract]
        [CorsEnabled]
        [WebInvoke(Method = "GET", UriTemplate = "AllJobsByProcessingHost/{host}?limit={limit}&offset={offset}")]
        IList<JobModel> GetAllJobsForProcessingHost(string host, int limit = -1, int offset = -1);

        /// <summary>
        /// Endpoint for getting all changes for a job
        /// </summary>
        /// <param name="session"></param>
        [OperationContract]
        [CorsEnabled]
        [WebInvoke(Method = "GET", UriTemplate = "GetAllChangesForJob/{id}")]
        IEnumerable<Change> GetAllChangesForJob(string id);

        /// <summary>
        /// Endpoint for getting a job
        /// </summary>
        /// <param name="session"></param>
        [OperationContract]
        [CorsEnabled]
        [WebInvoke(Method = "GET", UriTemplate = "Job/{id}")]
        JobModel GetJob(String id);

        /// <summary>
        /// Endpoint for getting a job result
        /// </summary>
        /// <param name="session"></param>
        [OperationContract]
        [CorsEnabled]
        [WebInvoke(Method = "GET", UriTemplate = "JobResult/{id}")]
        JobResultModel GetJobResult(String id);


        /// <summary>
        /// Get a breakdown of state for a server
        /// </summary>
        /// <param name="session"></param>
        [OperationContract]
        [CorsEnabled]
        [WebInvoke(Method = "GET", UriTemplate = "JobStats?server={server}&type={type}")]
        ServerSummary GetJobStats(String server, String type);

        /// <summary>
        /// Gets a list of errors for a job
        /// </summary>
        /// <param name="session"></param>
        [OperationContract]
        [CorsEnabled]
        [WebInvoke(Method = "GET", UriTemplate = "Errors/{id}")]
        IEnumerable<MessageModel> GetErrorsForJob(String id);

        #endregion // Data Retrieval
    }
}
