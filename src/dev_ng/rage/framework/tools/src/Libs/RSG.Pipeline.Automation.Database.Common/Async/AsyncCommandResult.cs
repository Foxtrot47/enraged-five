﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Pipeline.Automation.Database.Common.Async
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class AsyncCommandResult : IAsyncCommandResult
    {
        #region Properties
        /// <summary>
        /// Unique identifier given to the task that is executing.
        /// This is what is used to query the state of the task.
        /// </summary>
        [DataMember]
        public Guid TaskIdentifier
        {
            get;
            protected set;
        }

        /// <summary>
        /// Operation's current progress (0-1).
        /// </summary>
        [DataMember]
        public double Progress
        {
            get;
            set;
        }

        /// <summary>
        /// Message containing information about what the server is currently doing.
        /// </summary>
        [DataMember]
        public string Message
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the exception that was thrown by the asynchronous operation, if any
        /// </summary>
        [DataMember]
        public string Exception
        {
            get;
            set;
        }

        /// <summary>
        /// Result of the operation.
        /// </summary>
        [DataMember]
        public object Result
        {
            get { return m_result; }
            set
            {
                if (m_result != value)
                {
                    CheckResultType(value);
                    m_result = value;
                }
            }
        }
        private object m_result;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public AsyncCommandResult(Guid taskIdentifier)
        {
            TaskIdentifier = taskIdentifier;
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        [Conditional("DEBUG")]
        private void CheckResultType(object value)
        {
            bool isKnownType = GetKnownTypes(null).Any(item => item == value.GetType());
            //Debug.Assert(isKnownType, "Trying to set the result to an 'unknown type' which will cause its serialisation to fail.  Have you forgotten to add it to the AsyncCommandResult.GetKnownTypes() method?");
        }
        #endregion // Private Methods

        #region Static Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        public static IEnumerable<Type> GetKnownTypes(ICustomAttributeProvider provider)
        {
            yield return typeof(AsyncCommandResult);
        }
        #endregion // Static Methods
    } // AsyncCommandResultDto
}
