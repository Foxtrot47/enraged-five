﻿using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Database.Domain.Entities.Jobs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Pipeline.Automation.Database.Common.Extensions
{
    public static class CodeBuilderJobModelExtension
    {
        public static CodeBuilder2Job Convert(this CodeBuilderJobModel model)
        {
            return new CodeBuilder2Job(
                // IJob
                (CapabilityType)Enum.Parse(typeof(CapabilityType), model.Role),
                model.CompletedAt.HasValue ? model.CompletedAt.Value.ToUniversalTime() : default(DateTime),
                model.ConsumedByJobID,
                model.CreatedAt.HasValue ? model.CreatedAt.Value.ToUniversalTime() : default(DateTime),
                model.ID,
                model.PostbuildCommands,
                model.PrebuildCommands,
                (JobPriority)Enum.Parse(typeof(JobPriority), model.Priority),
                model.ProcessedAt.HasValue ? model.ProcessedAt.Value.ToUniversalTime() : default(DateTime),
                model.ProcessingHost,
                model.ProcessingTime,
                (JobState)Enum.Parse(typeof(JobState), model.State),
                model.Trigger.Convert(),
                model.UserData,

                // CodeBuilderJob
                model.SolutionFilename,
                model.TargetDir,
                model.PublishDir,
                model.BuildConfig,
                model.Platform,
                model.Tool,
                model.Rebuild,
                model.SkipConsume,
                model.RequiresPostJobNotification
            );
        }

        public static CodeBuilderJobModel Convert(this CodeBuilder2Job model)
        {
            return new CodeBuilderJobModel()
            {
                // JobModel
                CompletedAt = model.CompletedAt.ToUniversalTime(),
                ConsumedByJobID = model.ConsumedByJobID,
                CreatedAt = model.CreatedAt.ToUniversalTime(),
                ID = model.ID,
                PostbuildCommands = model.PostbuildCommands,
                PrebuildCommands = model.PrebuildCommands,
                Priority = model.Priority.ToString(),
                ProcessedAt = model.ProcessedAt.ToUniversalTime(),
                ProcessingHost = model.ProcessingHost,
                ProcessingTime = model.ProcessingTime,
                Role = model.Role.ToString(),
                State = model.State.ToString(),
                Trigger = model.Trigger.Convert(),
                UserData = model.UserData,


                // CodeBuilderJob
                SolutionFilename = model.SolutionFilename,
                TargetDir = model.TargetDir,
                PublishDir = model.PublishDir,
                BuildConfig = model.BuildConfig,
                Platform = model.Platform,
                Tool = model.Tool,
                Rebuild = model.Rebuild,
                SkipConsume = model.SkipConsume,
                RequiresPostJobNotification = model.RequiresPostJobNotification
            };
        }
    }
}
