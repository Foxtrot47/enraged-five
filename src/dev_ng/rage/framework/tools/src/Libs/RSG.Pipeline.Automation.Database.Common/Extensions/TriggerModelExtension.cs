﻿using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Pipeline.Automation.Database.Domain.Entities.Triggers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Pipeline.Automation.Database.Common.Extensions
{
    public static class TriggerModelExtension
    {
        public static ITrigger Convert(this TriggerModel model)
        {
            if (model is ChangelistTriggerModel)
            {
                ChangelistTriggerModel clTrigger = (ChangelistTriggerModel)model;
                return clTrigger.Convert();
            }
            else if (model is UserRequestTriggerModel)
            {

            }

            return null;
        }

        public static TriggerModel Convert(this ITrigger model)
        {
            if (model is ChangelistTrigger)
            {
                ChangelistTrigger trigger = (ChangelistTrigger)model;
                return trigger.Convert();
            }

            return new TriggerModel()
            {
                TriggerId = model.TriggerId,
                TriggeredAt = model.TriggeredAt
            };
        }
    }
}
