﻿using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Core;
using RSG.Pipeline.Automation.Database.Domain.Entities.Jobs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Pipeline.Automation.Database.Common.Extensions
{
    public static class AssetBuilderJobModelExtension
    {
        public static AssetBuilderJob Convert(this AssetBuilderJobModel model)
        {
            return new AssetBuilderJob(
                // IJob
                (CapabilityType)Enum.Parse(typeof(CapabilityType), model.Role),
                model.CompletedAt.HasValue ? model.CompletedAt.Value.ToUniversalTime() : default(DateTime),
                model.ConsumedByJobID,
                model.CreatedAt.HasValue ? model.CreatedAt.Value.ToUniversalTime() : default(DateTime),
                model.ID,
                model.PostbuildCommands,
                model.PrebuildCommands,
                (JobPriority)Enum.Parse(typeof(JobPriority), model.Priority),
                model.ProcessedAt.HasValue ? model.ProcessedAt.Value.ToUniversalTime() : default(DateTime),
                model.ProcessingHost,
                model.ProcessingTime,
                (JobState)Enum.Parse(typeof(JobState), model.State),
                model.Trigger.Convert(),
                model.UserData,

                // AssetBuilderJob
                (EngineFlags)Enum.Parse(typeof(EngineFlags), model.EngineFlags)
            );
        }

        public static AssetBuilderJobModel Convert(this AssetBuilderJob model)
        {
            return new AssetBuilderJobModel()
            {
                // JobModel
                CompletedAt = model.CompletedAt.ToUniversalTime(),
                ConsumedByJobID = model.ConsumedByJobID,
                CreatedAt = model.CreatedAt.ToUniversalTime(),
                ID = model.ID,
                PostbuildCommands = model.PostbuildCommands,
                PrebuildCommands = model.PrebuildCommands,
                Priority = model.Priority.ToString(),
                ProcessedAt = model.ProcessedAt.ToUniversalTime(),
                ProcessingHost = model.ProcessingHost,
                ProcessingTime = model.ProcessingTime,
                Role = model.Role.ToString(),
                State = model.State.ToString(),
                Trigger = model.Trigger.Convert(),
                UserData = model.UserData,


                // AssetBuilderJobModel
                EngineFlags =  model.EngineFlags.ToString(),
            };
        }
    }
}
