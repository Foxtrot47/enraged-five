﻿using RSG.Base.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.Configuration.Services;

namespace RSG.Pipeline.Automation.Database.Common
{
    /// <summary>
    /// This is the main configuration object for the system. This will contain all the
    /// individual server definitions and also contain such information as default server.
    /// </summary>
    public interface IJobConfig : IEnumerable<IServer>
    {
        /// <summary>
        /// Main game config.
        /// </summary>
        IBranch Branch { get; }

        /// <summary>
        /// Collection of servers for this project.
        /// </summary>
        IDictionary<String, IServer> Servers { get; }

        /// <summary>
        /// Name of the default server to connect to.
        /// </summary>
        string DefaultServerName { get; }

        /// <summary>
        /// Default server to connect to.
        /// </summary>
        IServer DefaultServer { get; }

        /// <summary>
        /// Reload all of the configuration data.
        /// </summary>
        void Reload();
    }
}
