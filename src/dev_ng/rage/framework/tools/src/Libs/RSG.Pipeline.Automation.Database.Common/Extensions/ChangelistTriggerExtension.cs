﻿using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Pipeline.Automation.Database.Domain.Entities.Triggers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Pipeline.Automation.Database.Common.Extensions
{
    public static class ChangelistTriggerExtension
    {
        public static ChangelistTrigger Convert(this ChangelistTriggerModel model)
        {
            return new ChangelistTrigger(model.Changelist, model.Description, model.Client, model.Files, model.MonitoredPaths, model.Username, model.TriggerId, model.TriggeredAt);
        }

        public static ChangelistTriggerModel Convert(this ChangelistTrigger trigger)
        {
            return new ChangelistTriggerModel()
            {
                TriggerId = trigger.TriggerId,
                TriggeredAt = trigger.TriggeredAt,
                Changelist = trigger.Changelist,
                Client = trigger.Client,
                Description = trigger.Description,
                Files = trigger.Files,
                MonitoredPaths = trigger.MonitoredPaths,
                Username = trigger.Username
            };
        }
    }
}
