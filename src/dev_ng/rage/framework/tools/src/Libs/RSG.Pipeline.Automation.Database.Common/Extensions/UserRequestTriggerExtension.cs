﻿using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Pipeline.Automation.Database.Domain.Entities.Triggers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Pipeline.Automation.Database.Common.Extensions
{
    public static class UserRequestTriggerExtension
    {
        public static UserRequestTrigger Convert(this UserRequestTriggerModel model)
        {
            return new UserRequestTrigger(model.Username, model.Files, model.TriggerId, model.TriggeredAt, model.Description);
        }

        public static UserRequestTriggerModel Convert(this UserRequestTrigger trigger)
        {
            return new UserRequestTriggerModel()
            {
                // TriggerModel
                TriggerId = trigger.TriggerId,
                TriggeredAt = trigger.TriggeredAt,

                // UserRequestTriggerModel
                Files = trigger.Files,
                FormattedCommand = trigger.Command,
                Username = trigger.Username,
                Description = trigger.Description
            };
        }
    }
}
