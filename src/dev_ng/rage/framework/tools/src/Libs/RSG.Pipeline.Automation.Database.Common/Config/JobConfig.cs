﻿using RSG.Base.Configuration;
using RSG.Base.Logging;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Configuration.Services;

namespace RSG.Pipeline.Automation.Database.Common
{
    /// <summary>
    /// This is the main configuration object for the system. This will contain all the
    /// individual server definitions and also contain such information as default server.
    /// </summary>
    public class JobConfig : IJobConfig, IEnumerable<IServer>
    {
        #region Constants
        internal static readonly String ELEM_JOBDATABASES = "jobdatabases";

        internal static readonly String ELEM_SERVERS = "servers";
        internal static readonly String ELEM_SERVER = "server";

        internal static readonly String ATTR_SERVER_DEFAULT = "default";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Main game config.
        /// </summary>
        public IBranch Branch { get; private set; }

        /// <summary>
        /// XML configuration filename.
        /// </summary>
        public String Filename { get; private set; }


        /// <summary>
        /// Name of the default server to connect to.
        /// </summary>
        public string DefaultServerName { get; private set; }

        /// <summary>
        /// Collection of servers for this project.
        /// </summary>
        public IDictionary<String, IServer> Servers { get; private set; }

        /// <summary>
        /// Default server to connect to.
        /// </summary>
        public IServer DefaultServer { get; private set; }

        #endregion
       
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        [Obsolete("Only here to support other legacy requirements")]
        public JobConfig()
        {
            Branch = ConfigFactory.CreateConfig().Project.DefaultBranch;
            Reload();
        }

        public JobConfig(IBranch branch)
        {
            Branch = branch;
            Reload();
        }
        #endregion

        #region Controller Methods
        /// <summary>
        /// Reload all of the configuration data.
        /// </summary>
        public void Reload()
        {
            Servers = new Dictionary<string, IServer>();
            // Get the file that we wish to load and ensure that it exists
            Filename = Path.Combine(Branch.Project.Config.ToolsConfig, "jobdatabase.xml");
            Debug.Assert(File.Exists(Filename), String.Format("Job Database Xml '{0}' does not exist.", Filename));
            if (!File.Exists(Filename))
            {
                throw new ConfigurationException(String.Format("Job Database Xml '{0}' does not exist.", Filename));
            }
            String localFilename = Path.Combine(Branch.Project.Config.ToolsConfig, "local", "jobdatabase_local.xml");


            // Load and parse the config file
            XDocument configDoc = XDocument.Load(Filename);
            XDocument localDoc = null;
            if (File.Exists(localFilename))
            {
                localDoc = XDocument.Load(localFilename);
            }

            // Parse the normal server configuration next.
            ParseServers(configDoc);
            if (localDoc != null)
            {
                ParseServers(localDoc);
            }

        }
        #endregion

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private void ParseServers(XDocument xmlDoc)
        {
            // Parse the servers.
            XElement xmlServersElem = xmlDoc.XPathSelectElement(String.Format("{0}/{1}",ELEM_JOBDATABASES, ELEM_SERVERS));
            if (xmlServersElem != null)
            {
                foreach (XElement xmlServerElem in xmlServersElem.XPathSelectElements("server"))
                {
                    IServer server = new Server(xmlServerElem);
                    if (Servers.ContainsKey(server.Name))
                    {
                        Log.Log__Warning("Detected existing server config with name {0}.  Overwriting previous instance with the new one.", server.Name);
                    }
                    Servers[server.Name] = server;
                }

                SetupDefaultServer(xmlServersElem);
            }
        }

        /// <summary>
        /// Setup default branch properties.
        /// </summary>
        /// <param name="xmlServersElem"></param>
        private void SetupDefaultServer(XElement xmlServersElem)
        {
            XAttribute xmlDefaultServerAttr = xmlServersElem.Attributes().Where(
                attr => (0 == String.Compare(ATTR_SERVER_DEFAULT, attr.Name.LocalName))).FirstOrDefault();
            if (null != xmlDefaultServerAttr)
            {
                this.DefaultServerName = xmlDefaultServerAttr.Value.Trim();
                this.DefaultServer = this.Servers[this.DefaultServerName];
            }
            else
            {
                this.DefaultServerName = String.Empty;
                this.DefaultServer = null;
                Log.Log__Warning("Default server not set if config data.");
            }
        }
        #endregion

        #region IEnumerable<IServer> Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerator<IServer> GetEnumerator()
        {
            foreach (IServer server in Servers.Values)
                yield return server;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return (GetEnumerator());
        }
        #endregion // IEnumerable<IServer> Methods
    }
}