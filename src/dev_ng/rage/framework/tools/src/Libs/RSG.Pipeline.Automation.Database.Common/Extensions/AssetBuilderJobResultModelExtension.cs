﻿using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Database.Domain.Entities.JobResults;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Pipeline.Automation.Database.Common.Extensions
{
    public static class AssetBuilderJobResultModelExtension
    {
        public static AssetBuilderJobResult Convert(this AssetBuilderJobResultModel model)
        {
            return new AssetBuilderJobResult(model.JobId, model.Succeeded, model.SubmittedChangelistNumber);
        }

        public static AssetBuilderJobResultModel Convert(this AssetBuilderJobResult model)
        {
            return new AssetBuilderJobResultModel()
            {
                Succeeded = model.Succeeded,
                JobId = model.JobId,
                SubmittedChangelistNumber = model.SubmittedChangelistNumber
            };
        }
    }
}
