﻿using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Database.Domain.Entities.JobResults;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Pipeline.Automation.Database.Common.Extensions
{
    public static class JobResultModelExtension
    {
        public static IJobResult Convert(this JobResultModel model)
        {
            if (model is AssetBuilderJobResultModel)
            {
                AssetBuilderJobResultModel abResult = (AssetBuilderJobResultModel) model;
                return abResult.Convert();
            }
            return new JobResult(model.JobId, model.Succeeded);
        }

        public static JobResultModel Convert(this IJobResult model)
        {
            if (model is AssetBuilderJobResult)
            {
                AssetBuilderJobResult abResult = (AssetBuilderJobResult)model;
                return abResult.Convert();
            }

            return new JobResultModel()
            {
                Succeeded = model.Succeeded,
                JobId = model.JobId
            };
        }
    }
}
