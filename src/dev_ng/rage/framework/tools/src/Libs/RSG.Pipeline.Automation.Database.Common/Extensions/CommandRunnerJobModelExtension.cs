﻿using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Jobs.JobData;
using RSG.Pipeline.Automation.Database.Domain.Entities.Jobs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Pipeline.Automation.Database.Common.Extensions
{
    public static class CommandRunnerJobModelExtension
    {
        public static CommandRunnerJob Convert(this CommandRunnerJobModel model)
        {
            return new CommandRunnerJob(
                // IJob
                (CapabilityType)Enum.Parse(typeof(CapabilityType), model.Role),
                model.CompletedAt.HasValue ? model.CompletedAt.Value.ToUniversalTime() : default(DateTime),
                model.ConsumedByJobID,
                model.CreatedAt.HasValue ? model.CreatedAt.Value.ToUniversalTime() : default(DateTime),
                model.ID,
                model.PostbuildCommands,
                model.PrebuildCommands,
                (JobPriority)Enum.Parse(typeof(JobPriority), model.Priority),
                model.ProcessedAt.HasValue ? model.ProcessedAt.Value.ToUniversalTime() : default(DateTime),
                model.ProcessingHost,
                model.ProcessingTime,
                (JobState)Enum.Parse(typeof(JobState), model.State),
                model.Trigger.Convert(),
                model.UserData,

                // CommandRunnerJob
                model.Commands.Select(c => c.Convert()),
                model.CommandName,
                model.RequiresPostJobNotification,
                model.BranchName,
                model.TargetDir,
                model.TargetRegex,
                model.PublishDir
            );
        }

        public static CommandRunnerJobModel Convert(this CommandRunnerJob model)
        {
            return new CommandRunnerJobModel()
            {
                CompletedAt = model.CompletedAt,
                ConsumedByJobID = model.ConsumedByJobID,
                CreatedAt = model.CreatedAt,
                ID = model.ID,
                PostbuildCommands = model.PostbuildCommands,
                PrebuildCommands = model.PrebuildCommands,
                Priority = model.Priority.ToString(),
                ProcessedAt = model.ProcessedAt,
                ProcessingHost = model.ProcessingHost,
                ProcessingTime = model.ProcessingTime,
                Role = model.Role.ToString(),
                State = model.State.ToString(),
                Trigger = model.Trigger.Convert(),
                UserData = model.UserData,


                // CommandRunnerJob
                Commands = model.Commands.Select(c => c.Convert()),
                CommandName = model.CommandName,
                RequiresPostJobNotification = model.RequiresPostJobNotification,
                BranchName = model.BranchName,
                TargetDir = model.TargetDir,
                TargetRegex = model.TargetRegex,
                PublishDir = model.PublishDir
            };
        }

        public static CommandRunnerJobInfoModel Convert(this CommandRunnerJobInfo jobInfo)
        {

            return new CommandRunnerJobInfoModel()
            {
                ID = jobInfo.ID,
                Command = jobInfo.Command,
                ErrorRegex = jobInfo.ErrorRegex,
                WarningRegex = jobInfo.WarningRegex
            };
        }

        public static CommandRunnerJobInfo Convert(this CommandRunnerJobInfoModel model)
        {
            return new CommandRunnerJobInfo(model.ID, model.Command, model.Args, model.ErrorRegex, model.WarningRegex);
        }
    }
}
