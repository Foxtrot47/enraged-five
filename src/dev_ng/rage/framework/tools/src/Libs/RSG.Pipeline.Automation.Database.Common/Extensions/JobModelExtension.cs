﻿using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Database.Domain.Entities;
using RSG.Pipeline.Automation.Database.Domain.Entities.Jobs;
using RSG.Pipeline.Automation.Database.Domain.Entities.Triggers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Pipeline.Automation.Database.Common.Extensions
{
    public static class JobModelExtension
    {
        public static IJob Convert(this JobModel model)
        {
            // This is to get around the issue of using extensions to converting.
            // Extensions are decided at compile time so this makes it possible
            // to use extensions for types that extend from the base.

            if (model is AssetBuilderJobModel)
            {
                AssetBuilderJobModel m = (AssetBuilderJobModel)model;
                return m.Convert();

            }
            else if (model is CodeBuilderJobModel)
            {
                CodeBuilderJobModel m = (CodeBuilderJobModel)model;
                return m.Convert();
            }
            else if (model is CommandRunnerJobModel)
            {
                CommandRunnerJobModel m = (CommandRunnerJobModel)model;
                return m.Convert();
            }
            else if (model is ScriptBuilderJobModel)
            {
                ScriptBuilderJobModel m = (ScriptBuilderJobModel)model;
                return m.Convert();
            }
            else
            {
                return new Job((CapabilityType)Enum.Parse(typeof(CapabilityType), model.Role),
                    model.CompletedAt.HasValue ? model.CompletedAt.Value.ToUniversalTime() : default(DateTime),
                    model.ConsumedByJobID,
                    model.CreatedAt.HasValue ? model.CreatedAt.Value.ToUniversalTime() : default(DateTime),
                    model.ID,
                    model.PostbuildCommands,
                    model.PrebuildCommands,
                    (JobPriority)Enum.Parse(typeof(JobPriority), model.Priority),
                    model.ProcessedAt.HasValue ? model.ProcessedAt.Value.ToUniversalTime() : default(DateTime),
                    model.ProcessingHost,
                    model.ProcessingTime,
                  
                    (JobState)Enum.Parse(typeof(JobState), model.State),
                    model.Trigger.Convert(),
                    model.UserData
                );
            }
        }

        public static JobModel Convert(this IJob model)
        {
            // This is to get around the issue of using extensions to converting.
            // Extensions are decided at compile time so this makes it possible
            // to use extensions for types that extend from the base.

            if (model is AssetBuilderJob)
            {
                AssetBuilderJob m = (AssetBuilderJob)model;
                return m.Convert();
            }
            else if (model is CodeBuilder2Job)
            {
                CodeBuilder2Job m = (CodeBuilder2Job)model;
                return m.Convert();
            }
            else if (model is CommandRunnerJob)
            {
                CommandRunnerJob m = (CommandRunnerJob)model;
                return m.Convert();
            }
            else if (model is ScriptBuilderJob)
            {
                ScriptBuilderJob m = (ScriptBuilderJob)model;
                return m.Convert();
            }
            else
            {
                return new JobModel()
                {
                    CompletedAt = model.CompletedAt.ToUniversalTime(),
                    ConsumedByJobID = model.ConsumedByJobID,
                    CreatedAt = model.CreatedAt.ToUniversalTime(),
                    ID = model.ID,
                    PostbuildCommands = model.PostbuildCommands,
                    PrebuildCommands = model.PrebuildCommands,
                    Priority = model.Priority.ToString(),
                    ProcessedAt = model.ProcessedAt.ToUniversalTime(),
                    ProcessingHost = model.ProcessingHost,
                    ProcessingTime = model.ProcessingTime,
                    Role = model.Role.ToString(),
                    State = model.State.ToString(),
                    Trigger = model.Trigger.Convert(),
                    UserData = model.UserData
                };
            }
        }

        public static List<Change> DetailedCompare<T>(this T previous, T newValue)
        {
            List<Change> variances = new List<Change>();
            PropertyInfo[] fi = previous.GetType().GetProperties();
            foreach (PropertyInfo f in fi)
            {
                Change v = new Change();
                v.ID = Guid.NewGuid();
                v.Property = f.Name;
                v.PreviousValue = f.GetValue(previous, null);
                v.NewValue = f.GetValue(newValue,null);
                v.ChangeTime = DateTime.UtcNow;

                if (v.PreviousValue is IEnumerable<Change> || v.NewValue is IEnumerable<Change> || v.PreviousValue is TriggerModel)
                {
                    continue;
                }

                // Helps with different storages of miliseconds. 
                if (v.PreviousValue is DateTime)
                {
                    DateTime pDT = (DateTime)v.PreviousValue;
                    DateTime nDT = (DateTime)v.NewValue;

                    v.PreviousValue = new DateTime(pDT.Year, pDT.Month, pDT.Day, pDT.Hour, pDT.Minute, pDT.Second, DateTimeKind.Utc);
                    v.NewValue = new DateTime(nDT.Year, nDT.Month, nDT.Day, nDT.Hour, nDT.Minute, nDT.Second, DateTimeKind.Utc);

                    if (pDT.Year == 9999)
                    {
                        v.PreviousValue = String.Empty;
                    }

                    if (pDT.Year == 9999)
                    {
                        v.NewValue = String.Empty;
                    }
                }

                if (!(v.PreviousValue is String) && v.PreviousValue is IEnumerable)
                {
                    IEnumerable<object> enumerablePrevious = (IEnumerable<object>)v.PreviousValue;
                    IEnumerable<object> enumerableNext = (IEnumerable<object>)v.NewValue;
                    if (enumerableNext != null && !enumerablePrevious.SequenceEqual(enumerableNext))
                    {

                    }

                    continue;
                }

                if (v.PreviousValue != null && !v.PreviousValue.Equals(v.NewValue))
                {
                    // We won't be needing these and to make them easier to read convert them into strings
                    v.PreviousValue = v.PreviousValue.ToString();
                    v.NewValue = v.NewValue.ToString();
                    variances.Add(v);
                }
            }

            return variances;
        }
    }
}
