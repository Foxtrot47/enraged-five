using System;

namespace RSG.Pipeline.Core
{

	/// <summary>
	/// Static class full of constants; these need to be documented.
	/// </summary>
	public static class Constants
	{
		#region Executable Return Codes
		/// <summary>
		/// Console application successful exit return code.
		/// </summary>
		public static readonly int Exit_Success = 0;

		/// <summary>
		/// Console application failure exit return code.
		/// </summary>
        public static readonly int Exit_Failure = 1;

        /// <summary>
        /// Bit flag to indicate the application wishes to resume after exit.
        /// </summary>
        public static readonly int Exit_Resume = (1 << 30);

        /// <summary>
        /// Bit flag to indicate the application wishes to sync to head of tools after exit.
        /// </summary>
        public static readonly int Exit_SyncHead = (1 << 29);

        /// <summary>
        /// Bit flag to indicate the application wishes to sync to labelled tools after exit.
        /// </summary>
        public static readonly int Exit_SyncLabel = (1 << 28);

        /// <summary>
        /// Bit flag to indicate the application wishes to sync to configuration data after exit.
        /// </summary>
        public static readonly int Exit_SyncConfig = (1 << 27);

        /// <summary>
        /// Bit flag to indicate the application wishes to sync to content data after exit.
        /// </summary>
        public static readonly int Exit_SyncContent = (1 << 26);

        /// <summary>
        /// Bit flag to indicate the application wishes to clean the content from the cache folder after exit.
        /// </summary>
        public static readonly int Exit_CleanCache = (1 << 25);

		#endregion // Executable Return Codes

		#region Content-Node Parameter Constants
		/// <summary>
		/// Asset content-node parameter for encoding the RPF asset pathname
		/// the asset data should be written to.
		/// </summary>
		/// Defaults to the asset filename (no folder information).
		/// 
		/// Used by the RPF Create Processor.
		/// 
		public static readonly String Asset_RPFPath = "Asset RPF Path";

		/// <summary>
		/// Asset content-node parameter for encoding the Zip asset pathname
		/// the asset data should be written to.
		/// </summary>
		/// Defaults to the asset filename (no folder information).
		/// 
		/// Used by the Asset Pack Processor.
		/// 
		public static readonly String Asset_ZipPath = "Asset Zip Path";

		/// <summary>
		/// Asset content-node parameter for adding a reference to an HD TXD;
		/// as determined by the Rage processor's Prebuild.  This is used for
		/// manifest construction.
		/// </summary>
		public static readonly String Asset_HDTXD = "Asset HD TXD";

		/// <summary>
		/// Content tree parameter to mark content that has been removed from the content tree.
		/// </summary>
		public static readonly String Removed_Content = "Removed Content";

		/// <summary>
		/// Content-node parameter specifying its state; whether its
		/// changed or been deleted.  Allows the Asset Builder to query this
		/// information after a build.
		/// </summary>
		public static readonly String Content_State = "Content State";

        /// <summary>
		/// Content-node parameter specifying if we can have a resulting output file with no input files.
        /// Allows the asset builder to build out files regardless of their inputs.
		/// </summary>
        public static readonly String Force_Create_If_Empty = "Force Create If Empty";

        /// <summary>
        /// Content-node parameter for preserving the folder structure of a built asset.
        /// </summary>
        public static readonly String Preserve_Folder_Structure = "Preserve Folder Structure";

        /// <summary>
        /// Content-node parameter for storing the original filenames that were used to kick off a build.
        /// Allows us to determine the filenames that started a build but were resolved into directories.
        /// </summary>
        /// This is used particularly with the new EngineFlags.BuildSelected mode for quicker
        /// build iteration for directory content.
        /// 
        public static readonly String Convert_Filenames = "Convert Filenames";
		#endregion // Content-Node Parameter Constants

		#region Process Parameter Constants
		/// <summary>
		/// Process parameter for XGE Task name; used for setting up dependencies
		/// between IProcess objects.
		/// </summary>
		public static readonly String ProcessXGE_TaskName = "Process XGE TaskName";

		/// <summary>
		/// Process parameter for XGE task object; used for setting up 
		/// dependencies between IProcess objects.
		/// </summary>
		public static readonly String ProcessXGE_Task = "Process XGE Task";

		/// <summary>
		/// Process parameter for telling Rage processor's Prepare that its
		/// converting a Manifest file; so that it filters it out and doesn't
		/// get a circular dependency during XGE packet construction.
		/// </summary>
		public static readonly String ProcessXGE_ManifestConvert = "Process Manifest Convert";

		/// <summary>
		/// Process parameter for telling XGE to remotely execute the specified
		/// process executable (default: true).
		/// </summary>
		public static readonly String ProcessXGE_AllowRemote = "XGE Allow Remote";

        /// <summary>
        /// Processs parameter used for per platform override of IContent nodes.
        /// </summary>
        public static readonly String Platform_Override = "Platform Override";
		#endregion // Process Parameter Constants

        #region Asset Processor Constants
        /// <summary>
        /// AssetProcessor parameter used to pass Operations data and elements
        /// </summary>
	    public static readonly string AssetProcessor_Operations = "AssetProcessor_Operations";
        #endregion

        #region Platform Processor Constants
        /// <summary>
		/// XGE packet file limit (count).
		/// </summary>
		public static readonly String RageProcess_XGEFileLimit =
			"XGE File Limit";

		/// <summary>
		/// XGE maximum packet size (kilobytes).
		/// </summary>
		public static readonly String RageProcess_XGEPacketSize =
			"XGE Packet Size";

		/// <summary>
		/// Resource cache parameter constant.
		/// </summary>
		public static readonly String RageProcess_ResourceCache =
			"Cache Directory";

		/// <summary>
		/// Capture resource binning statistics from Ragebuilder output.
		/// </summary>
		public static readonly String RageProcess_CaptureResourceStats =
			"Capture Resource Statistics";

        /// <summary>
        /// Capture resource binning statistics rolling file size byte limit.
        /// </summary>
        public static readonly String RageProcess_CaptureResourceStatsSizeLimit =
            "Resource Statistics Size";

		/// <summary>
		/// Directory inputs are to be evaluated at the point they are converted rather than in prebuild
		/// </summary>
		public static readonly String RageProcess_DelayedDirectoryEvaluationParameterName =
			"Delayed Directory Evaluation";
        
		/// <summary>
		/// Process parameter for RpfCreate/ManifestCreate processes for map 
		/// RPFs; the metadata serialiser export additions filename for picking 
		/// up ITYP/IMAP file dependencies.
		/// </summary>
		public static readonly String ManifestProcess_MapExportAdditions =
			"Map BuildSelected Additions";

		/// <summary>
		/// Process parameter for RpfCreate/ManifestCreate processes for map 
		/// RPFs; the bounds processor export additions filename for picking 
		/// up various dependencies.
		/// </summary>
		public static readonly String ManifestProcess_BoundsProcessorAdditions =
			"Bounds Processor Additions";

        /// <summary>
        /// Process parameter for RpfCreate/ManifestCreate processes for map 
        /// RPFs; the metadata serialiser export additions filename for picking 
        /// up ITYP/IMAP file dependencies.
        /// </summary>
        public static readonly String ManifestProcess_InstancePlacementProcessorAdditions =
            "Instance Placement Processor Additions";

        /// <summary>
        /// Content node paramater for RPF Create process, to avoid generating the manifest
        /// e.g. for non DLC patches.
        /// </summary>
        public static readonly string RpfCreate_GenerateManifest = "generate_manifest";

		/// <summary>
		/// Content-node parameter for RPF processes to tell it an input should
		/// not be packed; e.g. manifest files that are handled as special cases.
		/// </summary>
		public static readonly String RpfProcess_DoNotPack = "Do Not Pack";

        /// <summary>
        /// Process parameter for RPF processes to tell it a metadata directory
        /// for RPF Sorting (ITYP files picked up to order RPF content).
        /// </summary>
        public static readonly String RpfProcess_MetadataDirectory = "Metadata Directory";

		/// <summary>
		/// Content-process parameter for RAGE processes to tell it an input should
		/// not be converted; e.g. ragdoll XML files that are dependencies but not
		/// converted.
		/// </summary>
		public static readonly String RageProcess_DoNotConvert = "Do Not Convert";

		/// <summary>
		/// Content-process parameter for RAGE processes to tell it not to extract 
		/// the zip to be converted; this is an optimisation for zips containing lots
		/// of tiny files (e.g. carrecs) that fall outside our normal file count and
		/// data size parameters.
		/// </summary>
		public static readonly String RageProcess_DoNotExtract = "RageProcess_DoNotExtract";

        /// <summary>
        /// Resource-prefix String.  This is used for map DLC where we need to ensure
        /// that a prefix is allocated to all resources (if not already defined).
        /// </summary>
        public static readonly String RageProcess_ResourcePrefix = "RageProcess_ResourcePrefix";

        /// <summary>
        /// Resource cache parameter constant.
        /// </summary>
        public static readonly String RageProcess_WarnForMissingTuneFiles =
            "Warn For Missing Tune Files";

        #endregion // Platform Processor Constants

        #region Character Constants
        /// <summary>
        /// Character Outfit DLC name; used in Character Processor.
        /// </summary>
        public static readonly String Character_DLCOutfit = "outfit_name";

        /// <summary>
        /// Should Character Drawable Dictionaries be considered for bvh generation; used in Character Processor.
        /// </summary>
        public static readonly String No_BVH_Generation = "no_bvh_generation";
        #endregion // Character Constants

        #region Map Constants
        /// <summary>
        /// Parameter (string) type of map container/file.
        /// </summary>
        public static readonly String ParamMap_3dsmaxType = "max_type";

        /// <summary>
        /// Parameter (int) type for map hash seed.
        /// </summary>
        public static readonly String ParamMap_Seed = "map_seed";

        /// <summary>
        /// Parameter (bool) whether map exports archetypes (ITYP).
        /// </summary>
        public static readonly String ParamMap_ExportArchetypes = "archetypes";

        /// <summary>
        /// Parameter (bool) whether map exports entities (IMAPs).
        /// </summary>
        public static readonly String ParamMap_ExportEntities = "entities";

        /// <summary>
        /// Parameter (string) object export prefix.
        /// </summary>
        public static readonly String ParamMap_ExportPrefix = "prefix";

        /// <summary>
        /// Parameter (string) set from code to pass through necessity of tune file existence.
        /// </summary>
        public static readonly String ParamMap_NeedsTuning = "NeedsTuning";

        /// <summary>
        /// Parameter (bool) for whether container has SLOD2 link (for 
        /// validation purposes only).
        /// </summary>
        public static readonly String ParamMap_SLOD2Link = "slod2link";

        /// <summary>
        /// Parameter (String) to tell the metadata serialiser where to
        /// find the core game metadata zip file (for reading ITYP dependencies).
        /// </summary>
        public static readonly String ParamMap_CoreMetadataZip = "Core Metadata Zip";

        /// <summary>
        /// Parameter (string) to determine for props merging if we force the Ityp dependency during serialization.
        /// </summary>
        public static readonly String ParamMap_ForceItypDependency = "forced_ityp_dependencies";

        /// <summary>
        /// Whether the map can be auto-exported (default: false).
        /// </summary>
        public static readonly String Map_AutoExport = "map_auto_export";

        /// <summary>
        /// Map Collision Processor (MKII) parameters.
        /// </summary>
        public static readonly String Map_CollisionProcessor_Operation = "operation";
        public static readonly String Map_CollisionProcessor_Configuration = "configuration";

        /// <summary>
        /// Whether to generate collision for this container.
        /// </summary>
        public static readonly String Map_Export_Collision = "export_collision";

        /// <summary>
        /// Whether to generate collision for this container.
        /// </summary>
        public static readonly String Map_Process_Collision = "process_collision";
        #endregion // Map Constants

        #region Cutscene Constants

        /// <summary>
		/// Process parameter for cutscene concatenation for section method.
		/// </summary>
		public static readonly String Cutscene_Concat_SectionMethod = "SectionMethod";

		/// <summary>
		/// Process parameter for cutscene concatenation for section duration.
		/// </summary>
		public static readonly String Cutscene_Concat_SectionDuration = "SectionDuration";

		/// <summary>
		/// Process parameter for cutscene concatenation for section duration.
		/// </summary>
		public static readonly String Cutscene_Concat_Audio = "Audio";

		#endregion

        #region Vehicle Constants
        /// <summary>
        /// Specify whether to build the multiplayer vehicle RPF - used in Vehicle Processor.
        /// </summary>
        public static readonly String Vehicle_BuildMPRPF = "build_mp_rpf";
        #endregion // Vehicle Constants

        #region Scaleform Constants
	    public static readonly string Supported_Platforms = "supported_platforms";
	    #endregion
	}

} // RSG.Pipeline.Core namespace
