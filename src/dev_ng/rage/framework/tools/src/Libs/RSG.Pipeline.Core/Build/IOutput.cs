﻿using System;
using System.Collections.Generic;

namespace RSG.Pipeline.Core.Build
{

    /// <summary>
    /// Abstraction of the engine build output; recording engine output and a
    /// Build time.
    /// </summary>
    public interface IOutput
    {
        /// <summary>
        /// Time taken for this build to complete.
        /// </summary>
        TimeSpan BuildTime { get; }

        /// <summary>
        /// IContentNode objects describing what has been built.
        /// </summary>
        IEnumerable<IContentNode> Items { get; }

        /// <summary>
        /// Errors generated whilst the output was created.
        /// </summary>
        IEnumerable<String> Errors { get; }

        /// <summary>
        /// Warnings generated whilst the output was created.
        /// </summary>
        IEnumerable<String> Warnings { get; }
    }

} // RSG.Pipeline.Core.Build namespace
