﻿//---------------------------------------------------------------------------------------------
// <copyright file="ParameterTypeAttribute.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Core.Attributes
{
    using System;

    /// <summary>
    /// Attribute to define pipeline parameter types (for validation assistance).
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
    public class ParameterTypeAttribute : Attribute
    {
        #region Properties
        /// <summary>
        /// The Type associated with the Parameter Value.
        /// </summary>
        public Type ParameterValueType
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameterValueType"></param>
        public ParameterTypeAttribute(Type parameterValueType)
        {
            this.ParameterValueType = parameterValueType;
        }
        #endregion // Constructor(s)
    }

} // RSG.Pipeline.Core.Attributes namespace
