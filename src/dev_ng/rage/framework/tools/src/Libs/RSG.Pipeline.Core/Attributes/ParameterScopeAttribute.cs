﻿//---------------------------------------------------------------------------------------------
// <copyright file="ParameterScopeAttribute.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Core.Attributes
{
    using System;

    /// <summary>
    /// Attribute to define pipeline parameter scope (for validation assistance).
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
    public class ParameterScopeAttribute : Attribute
    {
        #region Properties
        /// <summary>
        /// The scope associated with the Parameter Value.
        /// </summary>
        public ParameterScope Scope
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="parameterScope"></param>
        public ParameterScopeAttribute(ParameterScope parameterScope)
        {
            this.Scope = parameterScope;
        }
        #endregion // Constructor(s)
    }

} // RSG.Pipeline.Core.Attributes namespace
