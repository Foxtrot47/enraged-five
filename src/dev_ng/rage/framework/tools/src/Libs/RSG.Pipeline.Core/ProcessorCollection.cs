﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Diagnostics;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.IO;
using System.Linq;
using System.Reflection;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Core;
using RSG.Base.Configuration;

namespace RSG.Pipeline.Core
{

    /// <summary>
    /// Abstraction of a collection of IProcessors.  This is the core collection
    /// of processors and how the pipeline interacts with them.
    /// </summary>
    /// Under normal pipeline operation the default constructor should be used.
    /// Under very special circumstances a predecate may be specified limiting
    /// the IProcessor objects within the collection.
    /// 
    /// Public access to the IProcessor objects is available through the 
    /// IEnumerable of IProcessor implementation (i.e. foreach sequential access).
    /// 
    /// Note: random access is not currently allowed (although it may in future
    /// regarding processors to convert particular pieces of content say).
    /// 
    public sealed class ProcessorCollection :
        IProcessorCollection,
        IEnumerable<IProcessor>,
        IPartImportsSatisfiedNotification
    {
        #region Constants
        /// <summary>
        /// Log context string for the processor collection.
        /// </summary>
        private static readonly String LOG_CTX = "Processor Collection";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Number of processors available.
        /// </summary>
        public int Count
        {
            get { return Processors.Count(); }
        }

        /// <summary>
        /// Pipeline configuration object.
        /// </summary>
        private IConfig Config
        {
            get;
            set;
        }

        /// <summary>
        /// Private collection of IProcessor objects; created internally by
        /// filtering AllProcessor collection using user-defined predicate.
        /// </summary>
        private IDictionary<String, IProcessor> Processors
        {
            get;
            set;
        }

        /// <summary>
        /// Private collection of IProcessor objects; created internally using
        /// MEF composition.  Predicate handles Processor container above.
        /// </summary>
        [ImportMany(typeof(IProcessor))]
        private ICollection<IProcessor> AllProcessors
        {
            get;
            set;
        }

        /// <summary>
        /// Processor MEF composition container.
        /// </summary>
        private CompositionContainer Container
        {
            get;
            set;
        }

        /// <summary>
        /// Processor Collection log.
        /// </summary>
        private static IUniversalLog Log
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="ProcessorCollection"/> class
        /// using the provided config object to determine where to load the processors
        /// from.
        /// </summary>
        /// <param name="config">Pipeline configuration object.</param>
        public ProcessorCollection(IConfig config)
        {
            this.Config = config;
            Initialise(processor => true);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ProcessorCollection"/> class
        /// using the provided config object and prediate to determine where to load
        /// the processors that satisfy the predicate from.
        /// </summary>
        /// <param name="config">Pipeline configuration object.</param>
        /// <param name="predicate"></param>
        public ProcessorCollection(IConfig config, Func<IProcessor, bool> predicate)
        {
            this.Config = config;
            Initialise(predicate);
        }

        /// <summary>
        /// Static constructor; setup log.
        /// </summary>
        static ProcessorCollection()
        {
            LogFactory.Initialize();
            ProcessorCollection.Log = LogFactory.CreateUniversalLog("RSG.Pipeline.Core.ProcessorCollection");
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Return processor with specified name.
        /// </summary>
        /// <param name="processorClassName"></param>
        /// <returns></returns>
        public IProcessor GetProcessor(String processorClassName)
        {
            if (this.Processors.ContainsKey(processorClassName))
                return (this.Processors[processorClassName]);

            throw (new ArgumentException(String.Format("Invalid processor: '{0}'.", processorClassName)));
        }
        #endregion // Controller Methods

        #region IEnumerable<IProcessor> Interface
        public IEnumerator<IProcessor> GetEnumerator()
        {
            return (this.AllProcessors.GetEnumerator());
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        #endregion // IEnumerable<IProcessor> Interface

        #region IPartImportsSatisfiedNotification Methods
        /// <summary>
        /// MEF composition completion callback.
        /// </summary>
        public void OnImportsSatisfied()
        {
            ProcessorCollection.Log.MessageCtx(LOG_CTX, "Processor Collection MEF composition complete.");
        }
        #endregion // IPartImportsSatisfiedNotification Methods

        #region Static Controller Methods
        /// <summary>
        /// CreateEmptyTree a read-only collection of IProcessor objects.
        /// </summary>
        /// <param name="config">Pipeline configuration object.</param>
        /// <returns></returns>
        public static ReadOnlyCollection<IProcessor> Create(IConfig config)
        {
            return (ProcessorCollection.Create(config, processor => true));
        }

        /// <summary>
        /// CreateEmptyTree a read-only collection of a specific set of IProcessor objects.
        /// </summary>
        /// <param name="config">Pipeline configuration object.</param>
        /// <param name="predecate"></param>
        /// <returns></returns>
        public static ReadOnlyCollection<IProcessor> Create(IConfig config, Func<IProcessor, bool> predecate)
        {
            ProcessorCollection collection = new ProcessorCollection(config, predecate);
            IList<IProcessor> list = collection.ToList();
            return (new ReadOnlyCollection<IProcessor>(list));
        }
        #endregion // Static Controller Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        private void AssemblyLoadHandler(Object sender, AssemblyLoadEventArgs e)
        {
            ProcessorCollection.Log.MessageCtx(LOG_CTX, "Loading Assembly: {0}.", e.LoadedAssembly.FullName);
        }

        /// <summary>
        /// Initialisation; populating the internal list of IProcessor objects.
        /// </summary>
        /// <param name="predicate"></param>
        private void Initialise(Func<IProcessor, bool> predicate)
        {
            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.AssemblyLoad += AssemblyLoadHandler;

            this.Processors = new Dictionary<String, IProcessor>();
            this.AllProcessors = new List<IProcessor>();

            String pluginPath = Path.Combine(this.Config.ToolsRoot, "ironlib", "lib", "Processors");
            ProcessorCollection.Log.ProfileCtx(LOG_CTX, "Processor Collection Initialisation");

            AggregateCatalog catalog = new AggregateCatalog();
            DirectoryCatalog dirCatalog = new DirectoryCatalog(pluginPath, "RSG.Pipeline.Processor.*.dll");
            //catalog.Catalogs.Add(new AssemblyCatalog(Assembly.GetExecutingAssembly()));
            catalog.Catalogs.Add(dirCatalog);

            this.Container = new CompositionContainer(catalog);
            try
            {
                this.Container.ComposeParts(this);
            }
            catch (CompositionException ex)
            {
                ProcessorCollection.Log.ToolExceptionCtx(LOG_CTX, ex, "Processor MEF Composition Exception");
                foreach (CompositionError error in ex.Errors)
                    ProcessorCollection.Log.ToolExceptionCtx(LOG_CTX, error.Exception, error.Description);
            }
            catch (ReflectionTypeLoadException ex)
            {
                ProcessorCollection.Log.ToolExceptionCtx(LOG_CTX, ex, "Processor Reflection Type Load Exception");
                foreach (Exception loadException in ex.LoaderExceptions)
                    ProcessorCollection.Log.ToolExceptionCtx(LOG_CTX, loadException, loadException.Message);
            }

            // Filter the processor list.
            foreach (IProcessor processor in this.AllProcessors)
            {
                if (!predicate.Invoke(processor))
                    continue;

                String processorClassName = String.Intern(processor.Name);
                this.Processors.Add(processorClassName, processor);
                ProcessorCollection.Log.MessageCtx(LOG_CTX, "Registered Processor: {0}.", processorClassName);
            }
            ProcessorCollection.Log.ProfileEnd();

            currentDomain.AssemblyLoad -= AssemblyLoadHandler;
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Engine namespace
