﻿using System;
using System.Collections.Generic;

namespace RSG.Pipeline.Core.Build
{

    /// <summary>
    /// Abstraction of the engine build unit; allowing future extension or
    /// additional properties.  We may run out of bit flags or need special
    /// features in the future.
    /// </summary>
    public interface IInput
    {
        #region Properties
        /// <summary>
        /// Build type to be done.
        /// </summary>
        BuildType Types { get; }

        /// <summary>
        /// Raw content-tree root; raw processes and nodes are owned by this
        /// content-tree.
        /// </summary>
        IContentTree RawTree { get; }

        /// <summary>
        /// Raw content-tree; requires preparation.
        /// </summary>
        ICollection<IProcess> RawProcesses { get; }

        /// <summary>
        /// Prepared content-tree root; prepared processes and nodes are owned
        /// by this content-tree.
        /// </summary>
        IContentTree PrebuiltTree { get; }

        /// <summary>
        /// Processes to be built (preprocessed in engine).
        /// </summary>
        ICollection<IProcess> PrebuiltProcesses { get; }
        #endregion // Properties
    }

} // RSG.Pipeline.Core.Build namespace
