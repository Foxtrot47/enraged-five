﻿//---------------------------------------------------------------------------------------------
// <copyright file="IHasParameters.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Core
{
    /// <summary>
    /// Provides an interface to implement SetParameters and GetParameters to avoid exposing the parameters dictionary.
    /// </summary>
    public interface IHasParameters
    {
        /// <summary>
        /// Determine whether the parameter exists.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        bool HasParameter(string name);

        /// <summary>
        /// Use this to perform a ContainsKey/Add to Parameters
        /// </summary>
        /// <param name="name">Parameter's name</param>
        /// <param name="value">Parameter's value</param>
        void SetParameter<T>(string name, T value);
        void SetParameter(string name, object value);

        /// <summary>
        /// Use this to perform a ContainsKey/Get to Parameters
        /// </summary>
        /// <param name="name">Parameter's name we try to retrieve the value.</param>
        /// <param name="defaultValue">If we can't retrieve the value, the default value will be used as return value.</param>
        /// <returns></returns>
        T GetParameter<T>(string name, T defaultValue);
        object GetParameter(string name, object defaultValue);
    }

} // RSG.Pipeline.Core namespace
