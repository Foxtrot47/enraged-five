﻿//---------------------------------------------------------------------------------------------
// <copyright file="IProcessorWithLocalBuild.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Core.Extensions
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The IProcessorAvecBuildLocale interface; extension to IProcessor that adds an 
    /// explicit method for producing local data.
    /// </summary>
    /// Note: if you want to use this or think you need to use this please discuss.
    /// The preferred approach is to parallelise through XGE in Prepare rather than 
    /// using this.
    /// 
    /// This interface should be implemented to abstract out any data creation/build 
    /// steps currently done in Prebuild IProcessor's.
    /// 
    public interface IProcessorWithLocalBuild : IProcessor
    {
        #region Methods
        /// <summary>
        /// Build local content prior to parallel execution; first build pass
        /// used to build local content before XGE (or other) parallel execution.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="processes">Processes to build.</param>
        /// <returns>true iff successfully built; false otherwise</returns>
        /// This should be used where previously we would have created data
        /// in Prebuild.
        /// 
        /// Note: please carefully consider the use of this interface.  The processor
        /// will be suboptimal - if you can refactor the top-level algorithm to
        /// remove this use the general pipeline will be quicker.
        /// 
        bool BuildLocal(IEngineParameters param, IEnumerable<IProcess> processes);
        #endregion // Methods
    }

} // RSG.Pipeline.Core namespace
