﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProcessState.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2012-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Core
{

    /// <summary>
    /// Process state enumeration; for tracking a process through a build 
    /// engine.
    /// </summary>
    /// Note: hoped to have the state internally managed by the engines; however
    /// it was felt that this would limit us later in how processes were
    /// created and used within processors.
    /// 
    public enum ProcessState
    {
        /// <summary>
        /// Process constructed.
        /// </summary>
        Initialised,

        /// <summary>
        /// Process is recursing through Prebuild; syncing dependencies inbetween.
        /// </summary>
        Prebuilding,
        
        /// <summary>
        /// Process has been prebuilt by the Engine.
        /// </summary>
        Prebuilt,

        /// <summary>
        /// Process has been built by the Engine.
        /// </summary>
        Built,

        /// <summary>
        /// Process to be discarded by the Engine; flagged by IProcessor Prebuild.
        /// </summary>
        Discard,
    }

} // RSG.Pipeline.Core namespace
