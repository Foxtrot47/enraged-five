﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RSG.Pipeline.Core
{

    /// <summary>
    /// Abstraction of a collection of IProcessors.  This is the core collection
    /// of processors and how the pipeline interacts with them.
    /// </summary>
    /// Under normal pipeline operation the default constructor should be used.
    /// Under very special circumstances a predecate may be specified limiting
    /// the IProcessor objects within the collection.
    /// 
    /// Public access to the IProcessor objects is available through the 
    /// IEnumerable of IProcessor implementation (i.e. foreach sequential access).
    /// 
    /// Note: random access is not currently allowed (although it may in future
    /// regarding processors to convert particular pieces of content say).
    /// 
    public interface IProcessorCollection : IEnumerable<IProcessor>
    {       
        #region Properties
        /// <summary>
        /// Number of processors available.
        /// </summary>
        int Count { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Return processor with specified name.
        /// </summary>
        /// <param name="procname"></param>
        /// <returns></returns>
        IProcessor GetProcessor(String procname);
        #endregion // Methods
    }

} // RSG.Pipeline.Core namespace
