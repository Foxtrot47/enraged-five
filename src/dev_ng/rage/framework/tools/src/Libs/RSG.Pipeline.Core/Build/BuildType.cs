﻿using System;

namespace RSG.Pipeline.Core.Build
{

    /// <summary>
    /// Abstraction of the work that the build engine will do; broken down into
    /// asset types.  Rather than be processor specific lets drive it from the
    /// data types; in that one processor may be concerned with multiple asset
    /// types.
    /// </summary>
    /// This will allow an Engine instance to build just texture data, or
    /// fragments.  It is up to the processors to interpret this correctly for
    /// the work it would normally do.
    [Flags]
    public enum BuildType : uint
    {
        None = 0x00000000,
        Geometry = (1 << 0),
        Textures = (1 << 1),
        Fragments = (1 << 2),
        Collision = (1 << 3),
        Metadata = (1 << 4),
        Cloth = (1 << 5),
        Animation = (1 << 6),
        Expressions = (1 << 7),

        // ...

        PlatformConversion = (1 << 30),

        Default = 0xFFFFFFFF,
        All = 0xFFFFFFFF
    }

} // RSG.Pipeline.Core.Build namespace
