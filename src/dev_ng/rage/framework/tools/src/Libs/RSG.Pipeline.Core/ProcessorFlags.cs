﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProcessorFlags.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Core
{
    using System;

    /// <summary>
    /// Processor flags that the Engine MKII recognises.  These were not used in the original
    /// Engine implementation; please discuss before adding any flags as there is likely a 
    /// better way.
    /// </summary>
    /// These allow the Engine to handle the migration and different use-cases of N-passes
    /// versus the optimal approach of a single parallel build step.
    [Flags]
    public enum ProcessorFlags
    {
        /// <summary>
        /// No special ProcessorFlags.
        /// </summary>
        None = 0,

        /// <summary>
        /// Processor is ignored by the Engine.
        /// </summary>
        Skipped = 1,

        /// <summary>
        /// Processor is given a separate Prebuild/LocalBuild/Build pass.
        /// </summary>
        SeparatePass = 2,

        // Aliases below.

        /// <summary>
        /// Default alias.
        /// </summary>
        Default = None | SeparatePass,

        /// <summary>
        /// Compatible default alias (with old single-pass Engine).
        /// </summary>
        Compatibility = None
    }

} // RSG.Pipeline.Core namespace
