﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.Core
{

    /// <summary>
    /// Core conversion engine operating flags.
    /// </summary>
    [Flags]
    public enum EngineFlags : uint
    {
        /// <summary>
        /// None.  Really not useful.
        /// </summary>
        None = 0x00000000,

        /// <summary>
        /// Rebuild flag; ignore timestamp checks and does clean.
        /// </summary>
        Rebuild = 0x00000001,

        /// <summary>
        /// Preview flag; quick turnaround, resource output redirected to preview folder.
        /// </summary>
        Preview = 0x00000002,

        /// <summary>
        /// Clean processes to be built only.
        /// </summary>
        CleanForBuild = 0x00000004,

        /// <summary>
        /// Load content-tree (not required for simple resource-only builds).
        /// </summary>
        LoadContent = 0x00000008,

        /// <summary>
        /// Use content-tree caching.
        /// </summary>
        CacheContent = 0x00000010,

        /// <summary>
        /// Engine locking mechanism (preventing multiple instances).
        /// </summary>
        Lock = 0x00000020,

        /// <summary>
        /// Engine dependency Perforce sync.
        /// </summary>
        SyncDependencies = 0x00000040,

        /// <summary>
        /// Clean output from erroneous processes.
        /// </summary>
        CleanErroredOutputs = 0x00000080,

        /// <summary>
        /// Allow popup dialogs.
        /// </summary>
        Popups = 0x00000100,

        /// <summary>
        /// Use default resource-only resolver (if no content-process defined).
        /// </summary>
        DefaultResolverFallback = 0x00000200,

        /// <summary>
        /// Export data build; quicker iteration that doesn't build all directory content.
        /// </summary>
        Selected = 0x00000400,

        /// <summary>
        /// Engine verbosity log level (really tell user what is happening).
        /// </summary>
        Verbose = 0x00000800,

        /// <summary>
        /// [Statistic] Calculate total input, output and additional dependencies sizes 
        /// (in bytes) and output this to the log at the end of a build.
        /// </summary>
        StatisticsFileSizes = 0x00010000,

        /// <summary>
        /// Xoreax XGE build support (currently required as no local path).
        /// </summary>
        XGE = 0x80000000,

        /// <summary>
        /// Define useful default mask.
        /// </summary>
        Default = XGE | StatisticsFileSizes | DefaultResolverFallback | Popups | 
            CleanErroredOutputs | SyncDependencies | Lock | CacheContent | LoadContent | 
            CleanForBuild | 
            None,

        /// <summary>
        /// Define sensible minimal mask.
        /// </summary>
        Minimal = Lock | None,
    }

} // RSG.Pipeline.Core namespace
