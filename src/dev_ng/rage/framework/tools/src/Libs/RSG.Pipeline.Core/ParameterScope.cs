﻿//---------------------------------------------------------------------------------------------
// <copyright file="ParameterScope.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Core
{
    using System;

    /// <summary>
    /// Enumeration to better define the scope of a parameter.
    /// </summary>
    /// This together with the <see cref="ParameterScopeAttribute"/> and <see cref="ParameterTypeAttribute"/>
    /// will help us document, verify and control processor, process and engine parameter
    /// usage throughout the pipeline.
    /// 
    [Flags]
    public enum ParameterScope
    {
        /// <summary>
        /// Parameter can be associated with an IContentNode.
        /// </summary>
        /// These can be read from the content-tree or setup through code in an
        /// IProcessor.
        Node,

        /// <summary>
        /// Parameter can be associated with an IProcess.
        /// </summary>
        /// These can be read from the content-tree or setup through code in an
        /// IProcessor.
        Process,

        /// <summary>
        /// Parameter can be associated with an IProcessor (typically read from
        /// parCodeGen metadata file).
        /// </summary>
        Processor,

        /// <summary>
        /// Parameter can be associated with the Engine (typically read from
        /// parCodeGen metadata file).
        /// </summary>
        Engine,
    }

} // RSG.Pipeline.Core namespace
