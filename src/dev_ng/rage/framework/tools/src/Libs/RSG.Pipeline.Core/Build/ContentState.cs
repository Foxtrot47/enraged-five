﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.Core.Build
{

    /// <summary>
    /// Content state enumeration; allowing Engine to specify the state of assets
    /// to the Asset Builders.
    /// </summary>
    public enum ContentState
    {
        Modified, // Asset has been modified.

        Deleted, // Asset is deleted.
    }

} // RSG.Pipeline.Core.Build namespace
