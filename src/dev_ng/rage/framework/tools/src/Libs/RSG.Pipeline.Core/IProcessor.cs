﻿//---------------------------------------------------------------------------------------------
// <copyright file="IProcessor.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Core
{
    using System;
    using System.Collections.Generic;
    using RSG.Base.Configuration;
    using RSG.Base.Logging.Universal;
    using RSG.Pipeline.Core.Attributes;
    using XGE = RSG.Interop.Incredibuild.XGE;
    
    /// <summary>
    /// The IProcessor interface; the heavy lifting processors that convert
    /// data.  E.g. RAGE, map etc.
    /// </summary>
    /// Note: in the medium term this interface is the fallback for all 
    /// processors.  It *must* be implemented so that the ProcessorCollection 
    /// picks up the processor instance and also so that users without Xoreax 
    /// XGE installed can still run the pipeline.
    /// 
    public interface IProcessor : IHasParameters
    {
        #region Properties
        /// <summary>
        /// Processor's name (matches the processor nodes in content tree).
        /// </summary>
        String Name { get; }

        /// <summary>
        /// Processor's description.
        /// </summary>
        String Description { get; }

        /// <summary>
        /// User-defined options for processor; processor-specific key, value
        /// pairs.  Allow injection of processor options from content-tree.
        /// </summary>
        [Obsolete("Please use IHasParameters interface methods instead.")]
        IDictionary<String, Object> Parameters { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Prepare content; first pass of the on-disk content-tree.  Returns a
        /// set of IProcess objects after determining all required inputs, 
        /// outputs and whether the process' needs to change (this allows the 
        /// concept of 'preprocessors').
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="process">Process to prepare.</param>
        /// <param name="processors">Processor collection to reference for new resultant proceses.</param>
        /// <param name="owner">Owning IContentTree.</param>
        /// <param name="syncDependencies">Dependencies to sync.</param>
        /// <param name="resultantProcesses">RawProcesses that will actually be built.</param>
        /// <returns>true iff successfully prebuilt; false otherwise</returns>
        /// The resultant processes are used by the pipeline engine after these
        /// calls.
        /// 
        /// New resultant processes can have their state updated internally to
        /// prevent the engine re-prebuilding them.
        /// 
        bool Prebuild(IEngineParameters param, IProcess process, 
            IProcessorCollection processors, IContentTree owner, 
            out IEnumerable<IContentNode> syncDependencies,
            out IEnumerable<IProcess> resultantProcesses);

        /// <summary>
        /// Prepare content; first pass of the on-disk content-tree.  Returns a
        /// set of IProcess objects after determining all required inputs, 
        /// outputs and whether the process' needs to change (this allows the 
        /// concept of 'preprocessors').
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="processes">Processes to prepare.</param>
        /// <param name="processors">Processor collection to reference for new resultant proceses.</param>
        /// <param name="owner">Owning IContentTree.</param>
        /// <param name="syncDependencies">Dependencies to sync.</param>
        /// <param name="resultantProcesses">RawProcesses that will actually be built.</param>
        /// <returns>true iff successfully prebuilt; false otherwise</returns>
        /// The resultant processes are used by the pipeline engine after these
        /// calls.
        /// 
        /// New resultant processes can have their state updated internally to
        /// prevent the engine re-prebuilding them.
        /// 
        bool Prebuild(IEngineParameters param, IEnumerable<IProcess> processes,
            IProcessorCollection processors, IContentTree owner,
            out IDictionary<IProcess, IEnumerable<IContentNode>> syncDependencies,
            out IEnumerable<IProcess> resultantProcesses);

        /// <summary>
        /// Prepare a build for the processes; populating the XGE project with
        /// the tools, environments and tasks required.  Including setting up
        /// the task dependency chain.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="processes"></param>
        /// <param name="tools"></param>
        /// <param name="tasks"></param>
        /// <returns></returns>
        bool Prepare(IEngineParameters param, IEnumerable<IProcess> processes,
            out IEnumerable<XGE.ITool> tools, out IEnumerable<XGE.ITask> tasks);
        
        /// <summary>
        /// Clean output content for a IProcess.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="process"></param>
        /// <returns></returns>
        bool Clean(IEngineParameters param, IProcess process);

        /// <summary>
        /// Clean output content for a collection of IProcess objects.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="processes"></param>
        /// <returns></returns>
        bool Clean(IEngineParameters param, IEnumerable<IProcess> processes);

        /// <summary>
        /// Callback for when the build process starts.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        void Started(IEngineParameters param);

        /// <summary>
        /// Callback for when the build process completes.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        void Finished(IEngineParameters param);

        /// <summary>
        /// Parse log information; processors are only passed log data for their
        /// respective IProcess output.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="taskLogData">Log string data.</param>
        /// <returns>true for successful process; false for error/failure.</returns>
        bool ParseLog(IEngineParameters param, IEnumerable<String> taskLogData);
        #endregion // Methods
    }

    /// <summary>
    /// IProcessor extension methods.
    /// </summary>
    public static class ProcessorExtensions
    {
        /// <summary>
        /// Return ProcessorFlags for the IProcessor.
        /// </summary>
        /// <param name="processor"></param>
        /// <returns></returns>
        public static ProcessorFlags GetFlags(this IProcessor processor)
        {
            ProcessorFlagsAttribute[] attributes =
                processor.GetType().GetCustomAttributes(typeof(ProcessorFlagsAttribute), false) as ProcessorFlagsAttribute[];

            if (1 == attributes.Length)
                return (attributes[0].Flags);
            else
                return (ProcessorFlags.Default);
        }
    }

} // RSG.Pipeline.Core namespace
