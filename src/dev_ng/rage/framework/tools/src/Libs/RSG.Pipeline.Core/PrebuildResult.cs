﻿//---------------------------------------------------------------------------------------------
// <copyright file="PrebuildResult.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Core
{

    /// <summary>
    /// Enumeration to indicate internally the result of an IProcessor's Prebuild method.
    /// </summary>
    /// This enumeration is designed to help developers write IProcessor implementations
    /// correctly.
    /// 
    /// Note: a future version of the IProcessor interface may support this directly
    /// so we can get support and validation from the pipeline Engine.
    /// 
    /// Note: [DHM] hope that this together with an IProcessor template or pattern would
    /// allow us to drop ProcessState.Prebuilding.
    /// 
    /// <seealso cref="IProcessor" />
    /// <seealso cref="IProcess" />
    /// <seealso cref="ProcessState" />
    /// 
    public enum PrebuildResult
    {
        /// <summary>
        /// Prebuild is running normally and can continue execution.
        /// </summary>
        OK,

        /// <summary>
        /// Errors have been encountered and should abort when returned.
        /// </summary>
        Errors,

        /// <summary>
        /// Prebuild requires another pass; maybe to get the engine to sync assets.
        /// </summary>
        /// When this occurs the IProcessor has typically determined that assets
        /// are needing synced.  We need to return to the Engine to sync the assets
        /// and keep the IProcess into the Initialised state and ensure it gets
        /// pushed into the resultant process list (in Prebuild).
        /// 
        /// Note: this does not mean reentrant as in thread reentrancy.  Only that
        /// IProcessor.Prebuild is expected to be invoked again on the same IProcess
        /// object.
        /// 
        Reentrant,
    }

} // RSG.Pipeline.Core namespace
