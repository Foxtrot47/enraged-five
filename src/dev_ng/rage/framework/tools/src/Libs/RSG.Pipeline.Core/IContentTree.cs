﻿using System;
using System.Collections.Generic;
using RSG.Base.Configuration;

namespace RSG.Pipeline.Core
{

    /// <summary>
    /// The core content-tree interface; representing a set of assets and their
    /// transitions (processes) in the build pipeline.
    /// </summary>
    public interface IContentTree
    {
        #region Properties
        /// <summary>
        /// Associated project branch; link to pipeline configuration data.
        /// </summary>
        IBranch Branch { get; }

        /// <summary>
        /// Enumerable of IProcess objects; the edges between nodes.
        /// </summary>
        IEnumerable<IProcess> Processes { get; }

        /// <summary>
        /// Enumerable of IContentNode objects; all of the nodes in the graph.
        /// </summary>
        IEnumerable<IContentNode> Nodes { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Return whether a path is a File content-node.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        bool IsFile(String path);

        /// <summary>
        /// Return whether a path is a Asset content-node.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        bool IsAsset(String path);

        /// <summary>
        /// Return whether a path is a Directory content-node.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        bool IsDirectory(String path);
        
        /// <summary>
        /// Return whether a path has a parent Directory content-node. 
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        bool HasParentDirectory(String path);

        /// <summary>
        /// Will use the wildcard from the parent directories and try to find
        /// a match to the filename. Will return null if we dont find a match.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        IContentNode GetParentDirectoryFromWildcard(String path);

        /// <summary>
        /// Return Directory content-node if it exists (null otherwise).
        /// </summary>
        /// <param name="path"></param>
        /// <param name="wildcard"></param>
        /// <returns></returns>
        IContentNode GetParentDirectory(String path, String wildcard);

        /// <summary>
        /// Return all Directory content-nodes if it exists (empty IEnumerable otherwise).
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        IEnumerable<IContentNode> GetParentDirectories(String path);

        /// <summary>
        /// Return whether a path is an ArchiveFile content-node.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        bool IsArchiveFile(String path);

        /// <summary>
        /// Return whether a path is an ArchiveDirectory content-node.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        bool IsArchiveDirectory(String path);

        /// <summary>
        /// Create a file-based content-node for filename; may return a cached
        /// node if its already part of this content tree.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        /// May also return an independent platform Asset object.
        /// 
        IContentNode CreateFile(String filename);

        /// <summary>
        /// Create an Asset content-node for filename and platform; may return a
        /// cached node if its already part of this content.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="platform"></param>
        /// <returns></returns>
        /// If no known asset type is registered for the filename extension then
        /// this method should return null.
        /// 
        IContentNode CreateAsset(String filename, RSG.Platform.Platform platform);

        /// <summary>
        /// Create a directory-based content node; may return a cached node if
        /// its already part of this content tree.
        /// </summary>
        /// <param name="directory"></param>
        /// <returns></returns>
        IContentNode CreateDirectory(String directory);

        /// <summary>
        /// Create a directory-based content node; may return a cached node if
        /// its already part of this content tree.
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="wildcard"></param>
        /// <param name="recursive"></param>
        /// <returns></returns>
        IContentNode CreateDirectory(String directory, String wildcard, bool recursive = false);

        /// <summary>
        /// Get a list of all the directories associated with a specified directory in the content-tree.
        /// We can have a base directory with multiple wildcards so return all of them.
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="wildcard"></param>
        /// <param name="recursive"></param>
        /// <returns></returns>
        IEnumerable<IContentNode> GetDirectories(String directory);

        /// <summary>
        /// Create a static directory-based content node; may return a cached 
        /// node if its already part of this content tree.
        /// </summary>
        /// <param name="directory"></param>
        /// <returns></returns>
        IContentNode CreateStaticDirectory(String directory);

        /// <summary>
        /// Create a static directory from a directory node.
        /// </summary>
        /// <param name="directory"></param>
        /// <returns></returns>
        IContentNode CreateStaticDirectoryFromDirectory(IContentNode directory);

        /// <summary>
        /// Create a regular expression-based directory.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="wildcard"></param>
        /// <param name="regex"></param>
        /// <param name="recursive"></param>
        /// <returns></returns>
        IContentNode CreateRegexDirectory(String path, String wildcard, String regex, bool recursive);

        /// <summary>
        /// Create an archive file content-node; may return a cached node if its
        /// already part of this content tree.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        IContentNode CreateArchiveFile(String path);

        /// <summary>
        /// Create an archive file content-node; may return a cached node if its
        /// already part of this content tree.
        /// </summary>
        /// <param name="paths"></param>
        /// <returns></returns>
        IContentNode CreateArchiveFile(IEnumerable<String> paths);

        /// <summary>
        /// Create an archive directory content-node; may return a cached node 
        /// if its already part of this content tree.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        IContentNode CreateArchiveDirectory(String path);

        /// <summary>
        /// Create an archive directory content-node; may return a cached node 
        /// if its already part of this content tree.
        /// </summary>
        /// <param name="paths"></param>
        /// <returns></returns>
        IContentNode CreateArchiveDirectory(IEnumerable<String> paths);

        /// <summary>
        /// Create a process within this content-tree (with processor classname validation).
        /// </summary>
        /// <param name="processorClassName"></param>
        /// <param name="procCollection"></param>
        /// <param name="inputs"></param>
        /// <param name="outputs"></param>
        /// <param name="additionalDependentContent"></param>
        /// <param name="allow_remote"></param>
        /// <param name="rebuild"></param>
        /// <returns></returns>
        IProcess CreateProcess(String processorClassName, IProcessorCollection procCollection,
            IEnumerable<IContentNode> inputs, IEnumerable<IContentNode> outputs,
            IEnumerable<IContentNode> additionalDependentContent = null,
            AllowRemoteType allowRemote = AllowRemoteType.Yes,
            RebuildType rebuild = RebuildType.No);

        /// <summary>
        /// Create a process within this content-tree (without processor classname validation).
        /// </summary>
        /// <param name="processorClassName"></param>
        /// <param name="inputs"></param>
        /// <param name="outputs"></param>
        /// <param name="additionalDependentContent"></param>
        /// <param name="allow_remote"></param>
        /// <param name="rebuild"></param>
        /// <returns></returns>
        IProcess CreateProcess(String processorClassName, IEnumerable<IContentNode> inputs, 
            IEnumerable<IContentNode> outputs, IEnumerable<IContentNode> additionalDependentContent = null,
            AllowRemoteType allowRemote = AllowRemoteType.Yes, RebuildType rebuild = RebuildType.No);

        /// <summary>
        /// Create a process within this content-tree.
        /// </summary>
        /// <param name="processor"></param>
        /// <param name="inputs"></param>
        /// <param name="outputs"></param>
        /// <param name="additionalDependentContent"></param>
        /// <param name="allow_remote"></param>
        /// <param name="rebuild"></param>
        /// <returns></returns>
        IProcess CreateProcess(IProcessor processor, IEnumerable<IContentNode> inputs, 
            IEnumerable<IContentNode> outputs, 
            IEnumerable<IContentNode> additionalDependentContent = null,
            AllowRemoteType allowRemote = AllowRemoteType.Yes,
            RebuildType rebuild = RebuildType.No);

        /// <summary>
        /// Merge another content-tree into this one; used for merging a core
        /// Project's content-tree into a DLC-content-tree.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        void Merge(IContentTree other);

        /// <summary>
        /// Load content-tree from a local cache file.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        bool LoadCache(String filename);

        /// <summary>
        /// Save content-tree to a local cache file.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        bool SaveCache(String filename);
        #endregion // Methods
    }

} // RSG.Pipeline.Core namespace
