﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProcessorFlagsAttribute.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Core.Attributes
{
    using System;

    /// <summary>
    /// Attribute used to markup IProcessor classes to tell the Engine2 with flags
    /// to inform the Engine MKII how to utilise it.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public class ProcessorFlagsAttribute : Attribute
    {
        #region Properties
        /// <summary>
        /// IProcessor flags.
        /// </summary>
        public ProcessorFlags Flags
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="flags"></param>
        public ProcessorFlagsAttribute(ProcessorFlags flags)
        {
            this.Flags = flags;
        }
        #endregion // Constructor(s)
    }
    
} // RSG.Pipeline.Core.Attributes namespace
