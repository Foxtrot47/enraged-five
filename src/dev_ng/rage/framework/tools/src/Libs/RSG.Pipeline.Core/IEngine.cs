﻿using System;
using System.Collections.Generic;
using RSG.Base.Configuration;
using RSG.Pipeline.Core.Build;

namespace RSG.Pipeline.Core
{
        
    /// <summary>
    /// Core conversion engine interface.
    /// </summary>
    public interface IEngine
    {
        #region Properties
        /// <summary>
        /// Pipeline configuration object.
        /// </summary>
        IConfig Config { get; }

        /// <summary>
        /// Project branch object.
        /// </summary>
        IBranch Branch { get; }

        /// <summary>
        /// Current engine operation flags.
        /// </summary>
        EngineFlags Flags { get; }

        /// <summary>
        /// Available processors.
        /// </summary>
        IProcessorCollection Processors { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Prebuild content; prebuild the passed in input filenames invoking
        /// the Processor's prebuild as required.  This interface allows external
        /// code to utilise the IProcessor dependency content-tree transforms.
        /// </summary>
        /// <param name="filenames"></param>
        /// <param name="outputs"></param>
        /// <param name="prebuild_time"></param>
        /// <returns></returns>
        /// Once content-tree caching is implemented this will also be able to
        /// used by external code to update the content-tree caches.
        /// 
        bool Prebuild(IEnumerable<String> filenames, out IEnumerable<Build.IOutput> outputs,
            out TimeSpan prebuild_time);

        /// <summary>
        /// Prebuild content; prebuild the passed in input invoking the 
        /// Processor's prebuild as required.  This interface allows external
        /// code to utilise the IProcessor dependency content-tree transforms.
        /// </summary>
        /// <param name="input"></param>
        /// <param name="outputs"></param>
        /// <param name="prebuild_time"></param>
        /// <returns></returns>
        /// Once content-tree caching is implemented this will also be able to
        /// used by external code to update the content-tree caches.
        /// 
        bool Prebuild(ref Build.IInput input, out IEnumerable<Build.IOutput> outputs,
            out TimeSpan prebuild_time);

        /// <summary>
        /// Build content; build the passed in input filenames invoking the
        /// Processors as required.
        /// </summary>
        /// <param name="filenames"></param>
        /// <param name="outputs"></param>
        /// <param name="build_time"></param>
        /// <returns></returns>
        bool Build(IEnumerable<String> filenames, out IEnumerable<Build.IOutput> outputs,
            out TimeSpan build_time);

        /// <summary>
        /// Build content; build the passed in input filenames invoking the
        /// Processors as required.
        /// </summary>
        /// <param name="filenames"></param>
        /// <param name="types"></param>
        /// <param name="outputs"></param>
        /// <param name="build_time"></param>
        /// <returns></returns>
        bool Build(IEnumerable<String> filenames, BuildType types, 
            out IEnumerable<Build.IOutput> outputs, out TimeSpan build_time);

        /// <summary>
        /// Build content; build the passed input content data, invoking the
        /// processors as required.
        /// </summary>
        /// <param name="input"></param>
        /// <param name="outputs"></param>
        /// <param name="build_time">Build time.</param>
        /// <returns>true if no errors; false otherwise</returns>
        /// 
        bool Build(ref Build.IInput input, out IEnumerable<Build.IOutput> outputs, 
            out TimeSpan build_time);

        /// <summary>
        /// Clean content; run through the input content data cleaning all 
        /// output files as required.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        bool Clean(Build.IInput input, out TimeSpan clean_time);
        #endregion // Methods
    }

} // RSG.Pipeline.Core namespace
