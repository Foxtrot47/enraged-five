﻿//---------------------------------------------------------------------------------------------
// <copyright file="IProcess.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2012-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Core
{
    using System;
    using System.Collections.Generic;
    using RSG.Base.Threading;
    
    /// <summary>
    /// The IProcess interface; the transition between two content nodes and a
    /// references, and some parameters, to an IProcessor.
    /// </summary>
    /// IContentNode's are dumb; IProcess objects are the cunning bastards of
    /// Asset Pipeline 3.0.
    /// 
    /// After an IProcess returns from Prebuild its frozen.  This ensure our
    /// dependency caching mechanism works.
    /// 
    public interface IProcess : 
        ICloneable,
        IHasParameters
    {
        #region Properties
        /// <summary>
        /// Process' processor classname to utilise for the data transition.
        /// </summary>
        String ProcessorClassName { get; }

        /// <summary>
        /// Owning IContentTree object.
        /// </summary>
        IContentTree Owner { get; }

        /// <summary>
        /// Collection of process input IContentNode's.
        /// </summary>
        IEnumerable<IContentNode> Inputs { get; }

        /// <summary>
        /// Collection of process output IContentNode's.
        /// </summary>
        IEnumerable<IContentNode> Outputs { get; }

        /// <summary>
        /// Set of our dependent nodes (need to be synced prior to this being 
        /// executed).  This allows a secondary set of nodes that are dependencies
        /// but not explicitly process inputs (e.g. ITYP file for RPF sorting).
        /// </summary>
        IEnumerable<IContentNode> AdditionalDependentContent { get; }

        /// <summary>
        /// Current process state; used by engines to track process state through
        /// the recursive prebuild and build steps.
        /// </summary>
        ProcessState State { get; set; }

        /// <summary>
        /// Process' rebuild flag; used to force data through even if it would 
        /// normally not require building.
        /// </summary>
        RebuildType Rebuild { get; set; }

        /// <summary>
        /// Process' remote flag; indicates to the system whether the processor 
        /// can run on a remote host (default: true).
        /// </summary>
        AllowRemoteType AllowRemote { get; }

        /// <summary>
        /// Generic parameters for the processor for this transition; read from
        /// the content-tree XML and saves us having to explicitly add parameters
        /// and definitions for each processor.
        /// </summary>
        [Obsolete("Use GetParameter/SetParameter IHasParameters interface methods instead.")]
        IDictionary<String, Object> Parameters { get; }
        #endregion // Properties
    }

} // RSG.Pipeline.Core namespace
