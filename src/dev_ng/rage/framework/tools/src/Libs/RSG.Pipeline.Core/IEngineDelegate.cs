﻿using System;
using System.Collections.Generic;
using RSG.Base.Configuration;

namespace RSG.Pipeline.Core
{
    #region Delegates
    /// <summary>
    /// Engine delegate for a readonly list of content.
    /// </summary>
    /// <typeparam name="USERDATA"></typeparam>
    /// <param name="branch"></param>
    /// <param name="contentList"></param>
    /// <param name="userData"></param>
    public delegate bool ContentCollectionDelegate(IBranch branch, IEnumerable<IContentNode> contentList, Object userData);
    #endregion // Delegates

    /// <summary>
    /// Interface for engine callback
    /// </summary>
    public interface IEngineDelegate
    {
        /// <summary>
        /// User-data object for PreBuild callback.
        /// </summary>
        Object PreBuildUserData { get; set; }

        /// <summary>
        /// User-data object for Build callback.
        /// </summary>
        Object PostBuildUserData { get; set; }

        /// <summary>
        /// Content collection callback for prebuild.  This is invoked AFTER prebuild has run.
        /// </summary>
        ContentCollectionDelegate PreBuildCallback { get; set; }

        /// <summary>
        /// Content collection callback for build. This is invoked AFTER build has run.
        /// </summary>
        ContentCollectionDelegate PostBuildCallback { get; set; }
    }

} // RSG.Pipeline.Core namespace
