﻿using System;
using System.Collections;
using System.Collections.Generic;
using RSG.Base.Configuration;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Core.Build;

namespace RSG.Pipeline.Core
{
    /// <summary>
    /// Engine parameters interface; provides an interface to engine parameter
    /// objects that are used when passing engine build parameters to each
    /// IProcessor.
    /// </summary>
    /// This saves the IProcessor interface being updated as additional parameters
    /// are required.
    /// 
    public interface IEngineParameters
    {
        /// <summary>
        /// Project branch object.
        /// </summary>
        IBranch Branch { get; }

        /// <summary>
        /// Core project's branch object.
        /// </summary>
        IBranch CoreBranch { get; }
        
        /// <summary>
        /// Platforms to use (defaults to installer enabled targets).
        /// </summary>
        IEnumerable<RSG.Platform.Platform> Platforms { get; }

        /// <summary>
        /// Current engine operation flags.
        /// </summary>
        EngineFlags Flags { get; }

        /// <summary>
        /// Additional build type for current build.
        /// </summary>
        BuildType Types { get; }

        /// <summary>
        /// Engine log; for processor logging.
        /// </summary>
        /// Having a central log allows the engine to detect any errors in the
        /// entire process and report accordingly.
        /// 
        /// Processors should use the context where applicable.
        /// 
        IUniversalLog Log { get; }

        /// <summary>
        /// Callback for operating on data output from the processors
        /// </summary>
        IEngineDelegate EngineDelegate { get; }
    }

} // RSG.Pipeline.Core namespace
