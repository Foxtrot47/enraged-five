﻿using System;
using System.Collections.Generic;

namespace RSG.Pipeline.Core
{
    
    /// <summary>
    /// The core content-node interface; representing an asset of the 
    /// build pipeline.
    /// </summary>
    /// The content-tree is now very much process focused; each IContentNode
    /// has no knowledge of its input nor output.  Its just a file, an asset,
    /// and dumb.  Think of something dumb, content-nodes are dumber.
    /// 
    public interface IContentNode : IHasParameters
    {
        #region Properties
        /// <summary>
        /// Content node name.
        /// </summary>
        String Name { get; }

        /// <summary>
        /// Owning IContentTree object.
        /// </summary>
        IContentTree Owner { get; }

        /// <summary>
        /// Content node unique identifier; used for caching.
        /// </summary>
        Guid ID { get; }

        /// <summary>
        /// Generic parameters for this IContentNode; can be used to stuff additional
        /// data into nodes as they are being passed through the pipeline.
        /// </summary>
        IDictionary<String, Object> Parameters { get; }

        /// <summary>
        /// Copy parameters from a source node into this parameters
        /// </summary>
        /// <param name="fromNode"></param>
        void CopyParametersFrom(IContentNode fromNode);
        #endregion // Properties
    }

} // RSG.Pipeline.Core
