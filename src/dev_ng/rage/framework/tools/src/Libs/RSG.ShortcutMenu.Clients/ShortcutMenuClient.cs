﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using RSG.ShortcutMenu.Common;

namespace RSG.ShortcutMenu.Clients
{
    /// <summary>
    /// 
    /// </summary>
    public class ShortcutMenuClient : ClientBase<IShortcutMenuService>, IShortcutMenuService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="binding"></param>
        /// <param name="remoteAddress"></param>
        public ShortcutMenuClient(Binding binding, EndpointAddress remoteAddress)
            : base(binding, remoteAddress)
        {
        }

        /// <summary>
        /// Invokes a particular shortcut item based on it's guid.
        /// </summary>
        /// <param name="guid"></param>
        /// <returns>Whether an item with the specified guid exists.</returns>
        public bool InvokeShortcutItem(Guid guid)
        {
            return base.Channel.InvokeShortcutItem(guid);
        }
    }
}
