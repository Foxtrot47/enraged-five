﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using RSG.Rag.Contracts.Data;
using RSG.Rag.Contracts.Services;

namespace RSG.Rag.Clients
{
    /// <summary>
    ///  WCF client for communicating with a service that implements the <see cref="IConsoleService"/>
    /// interface.
    /// </summary>
    public class ConsoleClient : ClientBase<IConsoleService>, IConsoleService
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="ConsoleClient"/> class using the 
        /// provided binding and endpoint information.
        /// </summary>
        /// <param name="binding"></param>
        /// <param name="remoteAddress"></param>
        public ConsoleClient(Binding binding, EndpointAddress remoteAddress)
            : base(binding, remoteAddress)
        {
        }
        #endregion

        #region IConsoleService Implementation
        /// <summary>
        /// Executes a console command.
        /// </summary>
        /// <param name="fullCommand">Command to execute.</param>
        /// <returns>Results of executing the command.</returns>
        public ConsoleCommandResponse ExecuteCommand(string fullCommand)
        {
            return base.Channel.ExecuteCommand(fullCommand);
        }
        #endregion
    }
}
