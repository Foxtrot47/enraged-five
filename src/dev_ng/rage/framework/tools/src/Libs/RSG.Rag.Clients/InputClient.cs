﻿// ---------------------------------------------------------------------------------------------
// <copyright file="InputClient.cs" company="Rockstar Games">
//      Copyright © Rockstar Games 2016. All rights reserved
// </copyright>
// ---------------------------------------------------------------------------------------------

namespace RSG.Rag.Clients
{
    using System.ServiceModel;
    using System.ServiceModel.Channels;
    using System.Windows.Input;
    using Contracts.Data;
    using Contracts.Services;

    /// <summary>
    /// Initialises a new instance of the <see cref="InputClient" /> class using the
    /// provided binding and endpoint information.
    /// </summary>
    public class InputClient : ClientBase<IInputService>, IInputService
    {
        /// <summary>
        /// Creates new instance of <see cref="InputClient"/>.
        /// </summary>
        /// <param name="binding"></param>
        /// <param name="remoteAddress"></param>
        public InputClient(Binding binding, EndpointAddress remoteAddress) : base(binding, remoteAddress)
        {
        }

        #region Methods
        /// <inheritdoc />
        public void QueueKeyboardEvent(KeyboardEvent evt, Key key)
        {
            this.Channel.QueueKeyboardEvent(evt, key);
        }

        /// <inheritdoc />
        public void QueueMouseEvent(MouseEvent evt, double x, double y, int wheelDelta, double width, double height)
        {
            this.Channel.QueueMouseEvent(evt, x, y, wheelDelta, width, height);
        }
        #endregion
    }
}