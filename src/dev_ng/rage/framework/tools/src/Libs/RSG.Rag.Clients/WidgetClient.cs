﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.Collections;
using RSG.Base.Math;
using RSG.Rag.Contracts.Data;
using RSG.Rag.Contracts.Services;

namespace RSG.Rag.Clients
{
    /// <summary>
    /// WCF client for communicating with a service that implements the <see cref="IWidgetService"/>
    /// interface.
    /// </summary>
    public class WidgetClient : ClientBase<IWidgetService>, IWidgetService
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="WidgetClient"/> class using the 
        /// provided binding and endpoint information.
        /// </summary>
        /// <param name="binding"></param>
        /// <param name="remoteAddress"></param>
        public WidgetClient(Binding binding, EndpointAddress remoteAddress)
            : base(binding, remoteAddress)
        {
        }
        #endregion

        #region IWidgetService Implementation
        #region General Operations
        /// <summary>
        /// Returns whether a particular widget exists.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        public bool WidgetExists(String widgetPath)
        {
            return base.Channel.WidgetExists(widgetPath);
        }

        /// <summary>
        /// Returns whether a particular widget exists.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        public bool WidgetExists(uint widgetId)
        {
            return base.Channel.WidgetExists(widgetId);
        }

        /// <summary>
        /// Retrieves a particular widgets id.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        public uint GetWidgetId(String widgetPath)
        {
            return base.Channel.GetWidgetId(widgetPath);
        }

        /// <summary>
        /// Retrieves a particular widgets path.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        public String GetWidgetPath(uint widgetId)
        {
            return base.Channel.GetWidgetPath(widgetId);
        }

        /// <summary>
        /// Adds a reference to the specified bank/group widget indicating that it is
        /// actively in use and to ensure that we receive value updates for child widgets.
        /// </summary>
        /// <param name="bankPath"></param>
        /// <returns></returns>
        public void AddBankReference(String bankPath)
        {
            base.Channel.AddBankReference(bankPath);
        }

        /// <summary>
        /// Adds a reference to the specified bank/group widget indicating that it is
        /// actively in use and to ensure that we receive value updates for child widgets.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        public void AddBankReference(uint widgetId)
        {
            base.Channel.AddBankReference(widgetId);
        }

        /// <summary>
        /// Removes a reference to the specified bank/group widget.
        /// </summary>
        /// <param name="bankPath"></param>
        /// <returns></returns>
        public void RemoveBankReference(String bankPath)
        {
            base.Channel.RemoveBankReference(bankPath);
        }

        /// <summary>
        /// Removes a reference to the specified bank/group widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        public void RemoveBankReference(uint widgetId)
        {
            base.Channel.RemoveBankReference(widgetId);
        }

        /// <summary>
        /// A special command that will block until all currently pending bank packets sent
        /// from the game have been processed. This is extremely useful when many widgets
        /// are being changed that may be dependent on each other.
        /// </summary>
        /// <param name="timeout">
        /// Optional timeout (in ms) for how long we should wait before aborting the sync
        /// command.
        /// </param>
        public void SendSyncCommand(uint timeout = 0)
        {
            base.Channel.SendSyncCommand(timeout);
        }

        /// <summary>
        /// Presses the specified widget as a button.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        public void PressButton(String widgetPath)
        {
            base.Channel.PressButton(widgetPath);
        }

        /// <summary>
        /// Presses the specified widget as a button.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        public void PressButton(uint widgetId)
        {
            base.Channel.PressButton(widgetId);
        }

        /// <summary>
        /// Presses the specified widget as a button.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        public void PressVCRButton(String widgetPath, VCRButton button)
        {
            base.Channel.PressVCRButton(widgetPath, button);
        }

        /// <summary>
        /// Presses the specified widget as a button.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        public void PressVCRButton(uint widgetId, VCRButton button)
        {
            base.Channel.PressVCRButton(widgetId, button);
        }
        #endregion

        #region Read Operations
        /// <summary>
        /// Reads a boolean value from the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        public bool ReadBoolWidget(String widgetPath)
        {
            return base.Channel.ReadBoolWidget(widgetPath);
        }

        /// <summary>
        /// Reads a boolean value from the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        public bool ReadBoolWidget(uint widgetId)
        {
            return base.Channel.ReadBoolWidget(widgetId);
        }

        /// <summary>
        /// Reads a float value from the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        public float ReadFloatWidget(String widgetPath)
        {
            return base.Channel.ReadFloatWidget(widgetPath);
        }

        /// <summary>
        /// Reads a float value from the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        public float ReadFloatWidget(uint widgetId)
        {
            return base.Channel.ReadFloatWidget(widgetId);
        }

        /// <summary>
        /// Retrieves a widgets floating point limits.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        public Range<float> ReadFloatWidgetLimits(String widgetPath)
        {
            return base.Channel.ReadFloatWidgetLimits(widgetPath);
        }

        /// <summary>
        /// Retrieves a widgets floating point limits.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        public Range<float> ReadFloatWidgetLimits(uint widgetId)
        {
            return base.Channel.ReadFloatWidgetLimits(widgetId);
        }

        /// <summary>
        /// Reads an integer value from the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        public int ReadIntWidget(String widgetPath)
        {
            return base.Channel.ReadIntWidget(widgetPath);
        }

        /// <summary>
        /// Reads an integer value from the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        public int ReadIntWidget(uint widgetId)
        {
            return base.Channel.ReadIntWidget(widgetId);
        }

        /// <summary>
        /// Reads a string value from the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        public String ReadStringWidget(String widgetPath)
        {
            return base.Channel.ReadStringWidget(widgetPath);
        }

        /// <summary>
        /// Reads a string value from the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        public String ReadStringWidget(uint widgetId)
        {
            return base.Channel.ReadStringWidget(widgetId);
        }

        /// <summary>
        /// Reads a vector2 value from the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        public Vector2f ReadVector2Widget(String widgetPath)
        {
            return base.Channel.ReadVector2Widget(widgetPath);
        }

        /// <summary>
        /// Reads a vector2 value from the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        public Vector2f ReadVector2Widget(uint widgetId)
        {
            return base.Channel.ReadVector2Widget(widgetId);
        }

        /// <summary>
        /// Reads a vector3 value from the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        public Vector3f ReadVector3Widget(String widgetPath)
        {
            return base.Channel.ReadVector3Widget(widgetPath);
        }

        /// <summary>
        /// Reads a vector3 value from the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        public Vector3f ReadVector3Widget(uint widgetId)
        {
            return base.Channel.ReadVector3Widget(widgetId);
        }

        /// <summary>
        /// Reads a vector4 value from the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        public Vector4f ReadVector4Widget(String widgetPath)
        {
            return base.Channel.ReadVector4Widget(widgetPath);
        }

        /// <summary>
        /// Reads a vector4 value from the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        public Vector4f ReadVector4Widget(uint widgetId)
        {
            return base.Channel.ReadVector4Widget(widgetId);
        }

        /// <summary>
        /// Reads a matrix33 value from the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        public Matrix33f ReadMatrix33Widget(String widgetPath)
        {
            return base.Channel.ReadMatrix33Widget(widgetPath);
        }

        /// <summary>
        /// Reads a matrix33 value from the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        public Matrix33f ReadMatrix33Widget(uint widgetId)
        {
            return base.Channel.ReadMatrix33Widget(widgetId);
        }

        /// <summary>
        /// Reads a matrix34 value from the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        public Matrix34f ReadMatrix34Widget(String widgetPath)
        {
            return base.Channel.ReadMatrix34Widget(widgetPath);
        }

        /// <summary>
        /// Reads a matrix34 value from the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        public Matrix34f ReadMatrix34Widget(uint widgetId)
        {
            return base.Channel.ReadMatrix34Widget(widgetId);
        }

        /// <summary>
        /// Reads a matrix44 value from the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        public Matrix44f ReadMatrix44Widget(String widgetPath)
        {
            return base.Channel.ReadMatrix44Widget(widgetPath);
        }

        /// <summary>
        /// Reads a matrix44 value from the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        public Matrix44f ReadMatrix44Widget(uint widgetId)
        {
            return base.Channel.ReadMatrix44Widget(widgetId);
        }

        /// <summary>
        /// Reads a byte array value from the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        public byte[] ReadDataWidget(String widgetPath)
        {
            return base.Channel.ReadDataWidget(widgetPath);
        }

        /// <summary>
        /// Reads a byte array value from the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        public byte[] ReadDataWidget(uint widgetId)
        {
            return base.Channel.ReadDataWidget(widgetId);
        }
        #endregion

        #region Write Operations
        /// <summary>
        /// Writes a boolean value to the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <param name="value"></param>
        public void WriteBoolWidget(String widgetPath, bool value)
        {
            base.Channel.WriteBoolWidget(widgetPath, value);
        }

        /// <summary>
        /// Writes a boolean value to the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="value"></param>
        public void WriteBoolWidget(uint widgetId, bool value)
        {
            base.Channel.WriteBoolWidget(widgetId, value);
        }

        /// <summary>
        /// Writes a float value to the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public void WriteFloatWidget(String widgetPath, float value)
        {
            base.Channel.WriteFloatWidget(widgetPath, value);
        }

        /// <summary>
        /// Writes a float value to the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public void WriteFloatWidget(uint widgetId, float value)
        {
            base.Channel.WriteFloatWidget(widgetId, value);
        }

        /// <summary>
        /// Writes an integer value to the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public void WriteIntWidget(String widgetPath, int value)
        {
            base.Channel.WriteIntWidget(widgetPath, value);
        }

        /// <summary>
        /// Writes an integer value to the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public void WriteIntWidget(uint widgetId, int value)
        {
            base.Channel.WriteIntWidget(widgetId, value);
        }

        /// <summary>
        /// Writes a string value to the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public void WriteStringWidget(String widgetPath, String value)
        {
            base.Channel.WriteStringWidget(widgetPath, value);
        }

        /// <summary>
        /// Writes a string value to the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public void WriteStringWidget(uint widgetId, String value)
        {
            base.Channel.WriteStringWidget(widgetId, value);
        }

        /// <summary>
        /// Writes a vector2 value to the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <param name="value"></param>
        public void WriteVector2Widget(String widgetPath, Vector2f value)
        {
            base.Channel.WriteVector2Widget(widgetPath, value);
        }

        /// <summary>
        /// Writes a vector2 value to the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="value"></param>
        public void WriteVector2Widget(uint widgetId, Vector2f value)
        {
            base.Channel.WriteVector2Widget(widgetId, value);
        }

        /// <summary>
        /// Writes a vector3 value to the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <param name="value"></param>
        public void WriteVector3Widget(String widgetPath, Vector3f value)
        {
            base.Channel.WriteVector3Widget(widgetPath, value);
        }

        /// <summary>
        /// Writes a vector3 value to the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="value"></param>
        public void WriteVector3Widget(uint widgetId, Vector3f value)
        {
            base.Channel.WriteVector3Widget(widgetId, value);
        }

        /// <summary>
        /// Writes a vector4 value to the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <param name="value"></param>
        public void WriteVector4Widget(String widgetPath, Vector4f value)
        {
            base.Channel.WriteVector4Widget(widgetPath, value);
        }

        /// <summary>
        /// Writes a vector4 value to the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="value"></param>
        public void WriteVector4Widget(uint widgetId, Vector4f value)
        {
            base.Channel.WriteVector4Widget(widgetId, value);
        }

        /// <summary>
        /// Writes a matrix33 value to the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <param name="value"></param>
        public void WriteMatrix33Widget(String widgetPath, Matrix33f value)
        {
            base.Channel.WriteMatrix33Widget(widgetPath, value);
        }

        /// <summary>
        /// Writes a matrix33 value to the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="value"></param>
        public void WriteMatrix33Widget(uint widgetId, Matrix33f value)
        {
            base.Channel.WriteMatrix33Widget(widgetId, value);
        }

        /// <summary>
        /// Writes a matrix34 value to the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <param name="value"></param>
        public void WriteMatrix34Widget(String widgetPath, Matrix34f value)
        {
            base.Channel.WriteMatrix34Widget(widgetPath, value);
        }

        /// <summary>
        /// Writes a matrix34 value to the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="value"></param>
        public void WriteMatrix34Widget(uint widgetId, Matrix34f value)
        {
            base.Channel.WriteMatrix34Widget(widgetId, value);
        }

        /// <summary>
        /// Writes a matrix44 value to the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <param name="value"></param>
        public void WriteMatrix44Widget(String widgetPath, Matrix44f value)
        {
            base.Channel.WriteMatrix44Widget(widgetPath, value);
        }

        /// <summary>
        /// Writes a matrix44 value to the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="value"></param>
        public void WriteMatrix44Widget(uint widgetId, Matrix44f value)
        {
            base.Channel.WriteMatrix44Widget(widgetId, value);
        }

        /// <summary>
        /// Writes a byte array to the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <param name="value"></param>
        public void WriteDataWidget(String widgetPath, byte[] value)
        {
            base.Channel.WriteDataWidget(widgetPath, value);
        }

        /// <summary>
        /// Writes a byte array to the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="value"></param>
        public void WriteDataWidget(uint widgetId, byte[] value)
        {
            base.Channel.WriteDataWidget(widgetId, value);
        }
        #endregion
        #endregion
    }
}
