﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using RSG.Rag.Contracts.Services;

namespace RSG.Rag.Clients
{
    /// <summary>
    /// Duplex client for communicating with a service that implements the
    /// <see cref="IConnectionSubscriptionService"/> contract.
    /// </summary>
    public class ConnectionSubscriptionClient : DuplexClientBase<IConnectionSubscriptionService>, IConnectionSubscriptionService
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ConnectionSubscriptionClient"/>
        /// class using the specified callback object, binding and service endpoint address.
        /// </summary>
        /// <param name="callbackInstance">
        /// An object used to create the instance context that associates the callback
        /// object with the channel to the service.
        /// </param>
        /// <param name="binding">
        /// The binding with which to call the service.
        /// </param>
        /// <param name="remoteAddress">
        /// The service endpoint address to use.
        /// </param>
        public ConnectionSubscriptionClient(IConnectionEvents callbackInstance, Binding binding, EndpointAddress remoteAddress)
            : base(callbackInstance, binding, remoteAddress)
        {
        }
        #endregion

        #region IConnectionSubscriptionService Implementation
        /// <summary>
        /// Allows clients to subscribe to receive events.
        /// </summary>
        public void Subscribe()
        {
            base.Channel.Subscribe();
        }

        /// <summary>
        /// Allows clients to unsubscribe from any further events.
        /// </summary>
        public void Unsubscribe()
        {
            base.Channel.Unsubscribe();
        }
        #endregion
    }
}
