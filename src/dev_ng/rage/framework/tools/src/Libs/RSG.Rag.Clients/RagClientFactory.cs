﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using RSG.Base.Extensions;
using RSG.Rag.Contracts.Data;
using RSG.Rag.Contracts.Services;

namespace RSG.Rag.Clients
{
    /// <summary>
    /// Static class for retrieving game connection information as well as creating clients
    /// for consuming the various RAG services based on a particular game connection.
    /// </summary>
    public static class RagClientFactory
    {
        #region Fields
        /// <summary>
        /// IP address of the local machine.
        /// </summary>
        private static readonly IPAddress _localIP;
        #endregion

        #region Constrcutors
        /// <summary>
        /// Initialises the static fields for the <see cref="RagClientFactory"/> class.
        /// </summary>
        static RagClientFactory()
        {
            _localIP = IPAddressExtensions.GetLocalIPAddress();
        }
        #endregion

        #region Methods
        /// <summary>
        /// Retrieves a list of game connections that are exposed by the RAG proxy running on the local machine.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<GameConnection> GetGameConnections()
        {
            return GetGameConnections(IPAddress.Loopback);
        }

        /// <summary>
        /// Retrieves the list of game connections that are exposed by the RAG proxy hosted at the specified address.
        /// </summary>
        /// <param name="proxyAddress"></param>
        /// <returns></returns>
        public static IEnumerable<GameConnection> GetGameConnections(String proxyAddress)
        {
            if (proxyAddress == null)
            {
                throw new ArgumentNullException("proxyAddress");
            }

            String[] components = proxyAddress.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
            if (components.Length != 4)
            {
                throw new ArgumentException("proxyAddress is not a valid IP address.");
            }

            byte[] byteComponents = new byte[4];
            for (int i = 0; i < 4; ++i)
            {
                byteComponents[i] = Byte.Parse(components[i]);
            }

            return GetGameConnections(new IPAddress(byteComponents));
        }

        /// <summary>
        /// Retrieves the list of game connections that are exposed by the RAG proxy hosted at the specified address.
        /// </summary>
        /// <param name="proxyAddress"></param>
        /// <returns></returns>
        public static IEnumerable<GameConnection> GetGameConnections(IPAddress proxyAddress)
        {
            EndpointAddress address = CreateEndpointAddress(proxyAddress, 15050, "ConnectionService");
            Binding binding = CreateDefaultBinding(proxyAddress);
            ConnectionClient client = null;

            try
            {
                client = new ConnectionClient(binding, address);
                return client.GetGameConnections();
            }
            catch (Exception ex)
            {
                //TODO: Logging.
                //Log.ToolExceptionCtx(CommandLogCtx, ex, "Error connecting to shortcut menu service.");
                throw ex;
            }
            finally
            {
                if (client.State == CommunicationState.Faulted)
                {
                    client.Abort();
                }
                else
                {
                    client.Close();
                }
            }
        }

        /// <summary>
        /// Creates a subscription connection client for receiving connection events from the proxy running on the local machine.
        /// </summary>
        /// <param name="proxyAddress"></param>
        /// <param name="callbackInstance"></param>
        /// <returns></returns>
        public static ConnectionSubscriptionClient CreateConnectionSubscriptionClient(IConnectionEvents callbackInstance)
        {
            return CreateConnectionSubscriptionClient(IPAddress.Loopback, callbackInstance);
        }

        /// <summary>
        /// Creates a subscription connection client for receiving connection events from the proxy running on the specified machine.
        /// </summary>
        /// <param name="proxyAddress"></param>
        /// <param name="callbackInstance"></param>
        /// <returns></returns>
        public static ConnectionSubscriptionClient CreateConnectionSubscriptionClient(IPAddress proxyAddress, IConnectionEvents callbackInstance)
        {
            EndpointAddress address = CreateEndpointAddress(proxyAddress, 15050, "ConnectionSubscriptionService");
            Binding binding = CreateInfiniteBinding(proxyAddress);
            return new ConnectionSubscriptionClient(callbackInstance, binding, address);
        }

        /// <summary>
        /// Creates a widget client for the specified game connection.
        /// </summary>
        /// <param name="connection"></param>
        /// <returns></returns>
        public static WidgetClient CreateWidgetClient(GameConnection connection)
        {
            EndpointAddress address = CreateRagServiceEndpointAddress(connection.RagProxyIP, connection.ServicesPort, "WidgetService");
            Binding binding = CreateDefaultBinding(connection.RagProxyIP);
            return new WidgetClient(binding, address);
        }

        /// <summary>
        /// Creates a console client for the specified game connection.
        /// </summary>
        /// <param name="connection"></param>
        /// <returns></returns>
        public static ConsoleClient CreateConsoleClient(GameConnection connection)
        {
            EndpointAddress address = CreateRagServiceEndpointAddress(connection.RagProxyIP, connection.ServicesPort, "ConsoleService");
            Binding binding = CreateDefaultBinding(connection.RagProxyIP);
            return new ConsoleClient(binding, address);
        }

        public static InputClient CreateInputClient(GameConnection connection)
        {
            EndpointAddress address = CreateRagServiceEndpointAddress(connection.RagProxyIP, connection.ServicesPort, "InputService");
            Binding binding = CreateDefaultBinding(connection.RagProxyIP);
            return new InputClient(binding, address);
        }
        
        /// <summary>
        /// Utility method that creates either a TCP or IPC endpoint address based on the IP address
        /// of the proxy that is passed in.
        /// </summary>
        /// <param name="proxyAddress"></param>
        /// <param name="tcpPort"></param>
        /// <param name="service"></param>
        /// <returns></returns>
        private static EndpointAddress CreateEndpointAddress(IPAddress proxyAddress, int tcpPort, String service)
        {
            if (proxyAddress.Equals(IPAddress.Loopback) || proxyAddress.Equals(_localIP))
            {
                return new EndpointAddress(String.Format("net.pipe://{0}/{1}.svc", proxyAddress, service));
            }
            else
            {
                return new EndpointAddress(String.Format("net.tcp://{0}:{1}/{2}.svc", proxyAddress, tcpPort, service));
            }
        }

        /// <summary>
        /// Utility method that creates either a TCP or IPC endpoint address based on the IP address
        /// of the proxy that is passed in.
        /// </summary>
        /// <param name="proxyAddress"></param>
        /// <param name="port"></param>
        /// <param name="service"></param>
        /// <returns></returns>
        private static EndpointAddress CreateRagServiceEndpointAddress(IPAddress proxyAddress, int port, String service)
        {
            if (proxyAddress.Equals(IPAddress.Loopback) || proxyAddress.Equals(_localIP))
            {
                return new EndpointAddress(String.Format("net.pipe://{0}/{1}/{2}.svc", proxyAddress, port, service));
            }
            else
            {
                return new EndpointAddress(String.Format("net.tcp://{0}:{1}/{2}.svc", proxyAddress, port, service));
            }
        }

        /// <summary>
        /// Utility method that creates the appropriate WCF binding configuration based on the IP address
        /// of the proxy that is passed in.
        /// </summary>
        /// <param name="proxyAddress"></param>
        /// <returns></returns>
        private static Binding CreateDefaultBinding(IPAddress proxyAddress)
        {
            if (proxyAddress.Equals(IPAddress.Loopback) || proxyAddress.Equals(_localIP))
            {
                return new NetNamedPipeBinding();
            }
            else
            {
                return new NetTcpBinding();
            }
        }

        /// <summary>
        /// Utility method that creates the appropriate WCF binding configuration based on the IP address
        /// of the proxy that is passed in.
        /// </summary>
        /// <param name="proxyAddress"></param>
        /// <returns></returns>
        private static Binding CreateInfiniteBinding(IPAddress proxyAddress)
        {
            TimeSpan eternity = TimeSpan.MaxValue;

            if (proxyAddress.Equals(IPAddress.Loopback) || proxyAddress.Equals(_localIP))
            {
                NetNamedPipeBinding namedPipeBinding = new NetNamedPipeBinding();
                namedPipeBinding.ReceiveTimeout = eternity;
                return namedPipeBinding;
            }
            else
            {
                NetTcpBinding tcpBinding = new NetTcpBinding();
                tcpBinding.ReceiveTimeout = eternity;
                tcpBinding.ReliableSession.InactivityTimeout = eternity;
                return tcpBinding;
            }
        }
        #endregion
    }
}
