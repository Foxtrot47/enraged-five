﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using RSG.Rag.Contracts.Data;
using RSG.Rag.Contracts.Services;

namespace RSG.Rag.Clients
{
    /// <summary>
    /// WCF client for communicating with a service that implements the <see cref="IConnectionService"/>
    /// interface.
    /// </summary>
    public class ConnectionClient : ClientBase<IConnectionService>, IConnectionService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="binding"></param>
        /// <param name="remoteAddress"></param>
        public ConnectionClient(Binding binding, EndpointAddress remoteAddress)
            : base(binding, remoteAddress)
        {
        }
        
        /// <summary>
        /// Retrieves the list of game connections the proxy currently has.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<GameConnection> GetGameConnections()
        {
            return base.Channel.GetGameConnections();
        }
    }
}
