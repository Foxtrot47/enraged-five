﻿//---------------------------------------------------------------------------------------------
// <copyright file="EngineProgressMonitoringServiceCallbackClient.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Services.Engine.Clients
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel;
    using System.Xml;
    using RSG.Pipeline.Services.Engine;

    /// <summary>
    /// Engine Progress Monitoring Service WCF client (consumers should be used externally).
    /// </summary>
    internal sealed class EngineProgressMonitoringServiceCallbackClient
        : ClientBase<IEngineProgressMonitoringServiceCallback>,
        IEngineProgressMonitoringServiceCallback
    {
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="binding"></param>
        /// <param name="remoteAddress"></param>    
        public EngineProgressMonitoringServiceCallbackClient(System.ServiceModel.Channels.Binding binding, EndpointAddress remoteAddress)
            : base(binding, remoteAddress)
        {
        }
        #endregion // Constructor(s)

        #region IEngineProgressMonitoringService Interface Methods
        /// <summary>
        /// Method callback executed when Engine starts a build.
        /// </summary>
        /// <param name="e"></param>
        public void NotifyMonitorBuildStarted(BuildStartedEventArgs e)
        {
            base.Channel.NotifyMonitorBuildStarted(e);
        }

        /// <summary>
        /// Method callback executed when Engine completes a build.
        /// </summary>
        /// <param name="e"></param>
        public void NotifyMonitorBuildComplete(BuildCompleteEventArgs e)
        {
            base.Channel.NotifyMonitorBuildComplete(e);
        }

        /// <summary>
        /// Method callback executed when Engine aborts a build.
        /// </summary>
        /// <param name="e"></param>
        public void NotifyMonitorBuildAborted(BuildAbortedEventArgs e)
        {
            base.Channel.NotifyMonitorBuildAborted(e);
        }

        /// <summary>
        /// Method callback executed when Engine stage changes.
        /// </summary>
        /// <param name="newStage"></param>
        public void NotifyMonitorBuildStageChange(AssetBuildStage newStage)
        {
            base.Channel.NotifyMonitorBuildStageChange(newStage);
        }

        /// <summary>
        /// Method callback executed when Engine step progress changes.
        /// </summary>
        /// <param name="progress"></param>
        public void NotifyMonitorEstimateOfStepProgressChange(Tuple<int, int> progress)
        {
            base.Channel.NotifyMonitorEstimateOfStepProgressChange(progress);
        }

        /// <summary>
        /// Method callback executed when Engine invokes log profile call.  This
        /// can be used to get back micro-managed messages about what the engine
        /// is doing.
        /// </summary>
        /// <param name="message"></param>
        public void NotifyMonitorProfileMessage(String message)
        {
            base.Channel.NotifyMonitorProfileMessage(message);
        }
        #endregion // IEngineProgressMonitoringService Interface Methods
    }

} // RSG.Pipeline.Services.Engine.Clients namespace
