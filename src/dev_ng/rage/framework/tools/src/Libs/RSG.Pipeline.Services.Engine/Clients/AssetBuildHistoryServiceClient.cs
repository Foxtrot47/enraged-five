﻿//---------------------------------------------------------------------------------------------
// <copyright file="AssetBuildHistoryServiceClient.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Services.Engine.Clients
{
    using System;
    using System.Collections.Generic;
    using System.ServiceModel;

    /// <summary>
    /// Asset Build History Service WCF client (consumers should be used externally).
    /// </summary>
    internal sealed class AssetBuildHistoryServiceClient : 
        ClientBase<IAssetBuildHistoryService>,
        IAssetBuildHistoryService
    {
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="binding"></param>
        /// <param name="remoteAddress"></param>    
        public AssetBuildHistoryServiceClient(System.ServiceModel.Channels.Binding binding, EndpointAddress remoteAddress)
            : base(binding, remoteAddress)
        {
        }
        #endregion // Constructor(s)

        #region IAssetBuildHistoryService Interface Methods
        /// <summary>
        /// Return information about client Build History.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<AssetBuildItem> GetBuildHistory()
        {
            return (base.Channel.GetBuildHistory());
        }
        
        /// <summary>
        /// Clear client Build History.
        /// </summary>
        public void ClearBuildHistory()
        {
            base.Channel.ClearBuildHistory();
        }
        #endregion // IAssetBuildHistoryService Interface Methods
    }

} // RSG.Pipeline.Services.Engine.Clients namespace
