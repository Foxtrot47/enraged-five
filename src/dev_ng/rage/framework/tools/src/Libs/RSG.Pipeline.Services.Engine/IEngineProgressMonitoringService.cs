﻿//---------------------------------------------------------------------------------------------
// <copyright file="IEngineProgressMonitoringService.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Services.Engine
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ServiceModel;
    using System.Threading.Tasks;

    /// <summary>
    /// Engine Progress Callback Service; designed to allow progress information displays
    /// from the pipeline Engine.
    /// </summary>
    [ServiceContract]
    public interface IEngineProgressMonitoringService
    {
        /// <summary>
        /// Register for callback invocations.
        /// </summary>
        /// <param name="listenerConnection"></param>
        [OperationContract(Action = "http://tempuri.org/IEngineProgressMonitoringService/RegisterListener",
            ReplyAction = "http://tempuri.org/IEngineProgressMonitoringService/RegisterListener")]
        [Description("Register for callback invocations.")]
        Guid RegisterListener(String listenerConnection);

        /// <summary>
        /// Unregister for callback invocations.
        /// </summary>
        /// <param name="id"></param>
        [OperationContract]
        [Description("Stop callback invocations.")]
        void UnregisterListener(Guid id);

        /// <summary>
        /// Determine if a the caller is registered for monitoring.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [OperationContract(Action = "http://tempuri.org/IEngineProgressMonitoringService/IsRegistered",
            ReplyAction = "http://tempuri.org/IEngineProgressMonitoringService/IsRegistered")]
        [Description("Determine if a callback ID is registered.")]
        bool IsRegistered(Guid id);
    }

} // RSG.Pipeline.Services.Engine namespace
