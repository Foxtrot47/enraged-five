﻿//---------------------------------------------------------------------------------------------
// <copyright file="IEngineProgressMonitoringServiceCallback.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Services.Engine
{
    using System;
    using System.ComponentModel;
    using System.ServiceModel;

    /// <summary>
    /// Engine Progress Callback Service; designed to allow progress information displays
    /// from the pipeline Engine.
    /// </summary>
    [ServiceContract]
    public interface IEngineProgressMonitoringServiceCallback
    {
        /// <summary>
        /// Method callback executed when Engine starts a build.
        /// </summary>
        /// <param name="e"></param>
        [OperationContract]
        [Description("Method callback executed when Engine starts a build.")]
        void NotifyMonitorBuildStarted(BuildStartedEventArgs e);
        
        /// <summary>
        /// Method callback executed when Engine completes a build.
        /// </summary>
        /// <param name="e"></param>
        [OperationContract]
        [Description("Method callback executed when Engine completes a build.")]
        void NotifyMonitorBuildComplete(BuildCompleteEventArgs e);

        /// <summary>
        /// Method callback executed when Engine aborts a build.
        /// </summary>
        /// <param name="e"></param>
        [OperationContract]
        [Description("Method callback executed when Engine aborts a build.")]
        void NotifyMonitorBuildAborted(BuildAbortedEventArgs e);

        /// <summary>
        /// Method callback executed when Engine stage changes.
        /// </summary>
        /// <param name="newStage"></param>
        [OperationContract]
        [Description("Method callback executed when Engine stage changes.")]
        void NotifyMonitorBuildStageChange(AssetBuildStage newStage);

        /// <summary>
        /// Method callback executed when Engine step progress changes.
        /// </summary>
        /// <param name="progress"></param>
        [OperationContract]
        [Description("Method callback executed when Engine step progress changes.")]
        void NotifyMonitorEstimateOfStepProgressChange(Tuple<int, int> progress);

        /// <summary>
        /// Method callback executed when Engine invokes log profile call.  This
        /// can be used to get back micro-managed messages about what the engine
        /// is doing.
        /// </summary>
        /// <param name="message"></param>
        [OperationContract]
        [Description("Method callback executed when Engine invokes log profile call.")]
        void NotifyMonitorProfileMessage(String message);
    }

} // RSG.Pipeline.Services.Engine namespace
