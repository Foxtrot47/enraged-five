﻿//---------------------------------------------------------------------------------------------
// <copyright file="EngineProgressMonitoringServiceClient.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Services.Engine.Clients
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel;
    using System.Threading.Tasks;
    using System.Xml;
    using RSG.Pipeline.Services.Engine;

    /// <summary>
    /// Engine Progress Monitoring Service WCF client (consumers should be used externally).
    /// </summary>
    internal sealed class EngineProgressMonitoringServiceClient
        : ClientBase<IEngineProgressMonitoringService>,
        IEngineProgressMonitoringService
    {
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="binding"></param>
        /// <param name="remoteAddress"></param>    
        public EngineProgressMonitoringServiceClient(System.ServiceModel.Channels.Binding binding, EndpointAddress remoteAddress)
            : base(binding, remoteAddress)
        {
        }
        #endregion // Constructor(s)
        
        #region IEngineProgressMonitoringService Interface Methods
        /// <summary>
        /// Register for callback invocations.
        /// </summary>
        /// <param name="listenerConnection"></param>
        /// <returns></returns>
        public Guid RegisterListener(String listenerConnection)
        {
            return (base.Channel.RegisterListener(listenerConnection));
        }
        
#if false
        /// <summary>
        /// Register for callback invocations.
        /// </summary>
        /// <param name="listenerConnection"></param>
        /// <returns></returns>
        public async Task<Guid> RegisterListenerAsync(String listenerConnection)
        {
            return (await base.Channel.RegisterListenerAsync(listenerConnection));
        }
#endif

        /// <summary>
        /// Unregister for callback invocations.
        /// </summary>
        /// <param name="id"></param>
        public void UnregisterListener(Guid id)
        {
            base.Channel.UnregisterListener(id);
        }

        /// <summary>
        /// Determine if a callee is registered for monitoring.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool IsRegistered(Guid id)
        {
            return (base.Channel.IsRegistered(id));
        }
        
#if false
        /// <summary>
        /// Determine if a callee is registered for monitoring.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<bool> IsRegisteredAsync(Guid id)
        {
            return (await base.Channel.IsRegisteredAsync(id));
        }
#endif

        #endregion // IEngineProgressMonitoringService Interface Methods
    }

} // RSG.Pipeline.Services.Engine.Clients namespace
