﻿//---------------------------------------------------------------------------------------------
// <copyright file="EngineProgressServiceCallbackConsumer.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Services.Engine.Consumers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel;
    using RSG.Base.Logging.Universal;
    
    /// <summary>
    /// 
    /// </summary>
    public class EngineProgressMonitoringServiceConsumer : 
        EngineConsumerBase
    {
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="service"></param>
        public EngineProgressMonitoringServiceConsumer(IUniversalLog log, Uri service)
            : base(log, service)
        {
        }
        #endregion // Constructor(s)

        // Controller methods here wrap the IEngineProgressMonitoringService methods; 
        // hiding the details of the WCF service.  This allows our WCF service 
        // channels to be short-lived.
        #region Controller Methods
        /// <summary>
        /// Register for callback invocations.
        /// </summary>
        /// <param name="listenerConnection"></param>
        public Guid RegisterListener(String listenerConnection)
        {
            this._log.Debug("EngineProgressMonitoringServiceConsumer::RegisterListener()");
            
            Clients.EngineProgressMonitoringServiceClient client = null;
            try
            {
                client = this.ConnectMonitoringService();
                return (client.RegisterListener(listenerConnection));
            }
            catch (System.ServiceModel.CommunicationException ex)
            {
                throw (new EngineProgressConsumerException("RegisterListener failed.", ex.InnerException));
            }
            catch (Exception ex)
            {
                throw (new EngineProgressConsumerException("Failed to connect to EngineProgressMonitoringService.", ex));
            }
            finally
            {
                if (null != client)
                {
                    if (client.State == CommunicationState.Faulted)
                        client.Abort();
                    else
                        client.Close();
                }
            }
        }

        /// <summary>
        /// Unregister for callback invocations.
        /// </summary>
        /// <param name="id"></param>
        public void UnregisterListener(Guid id)
        {
            this._log.Debug("EngineProgressMonitoringServiceConsumer::UnregisterListener()");

            Clients.EngineProgressMonitoringServiceClient client = null;
            try
            {
                client = this.ConnectMonitoringService();
                client.UnregisterListener(id);
            }
            catch (System.ServiceModel.CommunicationException ex)
            {
                throw (new EngineProgressConsumerException("UnregisterListener failed.", ex.InnerException));
            }
            catch (Exception ex)
            {
                throw (new EngineProgressConsumerException("Failed to connect to EngineProgressMonitoringService.", ex));
            }
            finally
            {
                if (null != client)
                {
                    if (client.State == CommunicationState.Faulted)
                        client.Abort();
                    else
                        client.Close();
                }
            }
        }

        /// <summary>
        /// Determine if a the caller is registered for monitoring.
        /// </summary>
        /// <returns></returns>
        public bool IsRegistered(Guid id)
        {
            this._log.Debug("EngineProgressMonitoringServiceConsumer::IsRegistered()");

            Clients.EngineProgressMonitoringServiceClient client = null;
            try
            {
                client = this.ConnectMonitoringService();
                return (client.IsRegistered(id));
            }
            catch (System.ServiceModel.CommunicationException ex)
            {
                throw (new EngineProgressConsumerException("IsRegistered failed.", ex.InnerException));
            }
            catch (Exception ex)
            {
                throw (new EngineProgressConsumerException("Failed to connect to EngineProgressMonitoringService.", ex));
            }
            finally
            {
                if (null != client)
                {
                    if (client.State == CommunicationState.Faulted)
                        client.Abort();
                    else
                        client.Close();
                }
            }
        }

#if false
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        public void NotifyMonitorBuildStarted(BuildStartedEventArgs e)
        {
            try
            {
                using (Clients.EngineProgressMonitoringServiceCallbackClient client =
                    this.ConnectMonitoringCallbackService())
                {
                    try
                    {
                        client.NotifyMonitorBuildStarted(e);
                    }
                    catch (Exception ex)
                    {
                        throw (new EngineProgressConsumerException("Failed to invoke NotifyMonitorBuildStarted.", ex));
                    }
                }
            }
            catch (Exception ex)
            {
                throw (new EngineProgressConsumerException("Failed to connect to EngineProgressMonitoringServiceCallback.", ex));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        public void NotifyMonitorBuildComplete(BuildCompleteEventArgs e)
        {
            try
            {
                using (Clients.EngineProgressMonitoringServiceCallbackClient client =
                    this.ConnectMonitoringCallbackService())
                {
                    try
                    {
                        client.NotifyMonitorBuildComplete(e);
                    }
                    catch (Exception ex)
                    {
                        throw (new EngineProgressConsumerException("Failed to invoke NotifyMonitorBuildComplete.", ex));
                    }
                }
            }
            catch (Exception ex)
            {
                throw (new EngineProgressConsumerException("Failed to connect to EngineProgressMonitoringServiceCallback.", ex));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        public void NotifyMonitorBuildAborted(BuildAbortedEventArgs e)
        {
            try
            {
                using (Clients.EngineProgressMonitoringServiceCallbackClient client =
                    this.ConnectMonitoringCallbackService())
                {
                    try
                    {
                        client.NotifyMonitorBuildAborted(e);
                    }
                    catch (Exception ex)
                    {
                        throw (new EngineProgressConsumerException("Failed to invoke NotifyMonitorBuildComplete.", ex));
                    }
                }
            }
            catch (Exception ex)
            {
                throw (new EngineProgressConsumerException("Failed to connect to EngineProgressMonitoringServiceCallback.", ex));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stage"></param>
        public void NotifyMonitorBuildStageChange(AssetBuildStage stage)
        {
            try
            {
                using (Clients.EngineProgressMonitoringServiceCallbackClient client =
                    this.ConnectMonitoringCallbackService())
                {
                    try
                    {
                        client.NotifyMonitorBuildStageChange(stage);
                    }
                    catch (Exception ex)
                    {
                        throw (new EngineProgressConsumerException("Failed to invoke NotifyMonitorBuildStageChange.", ex));
                    }
                }
            }
            catch (Exception ex)
            {
                throw (new EngineProgressConsumerException("Failed to connect to EngineProgressMonitoringServiceCallback.", ex));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="progress"></param>
        public void NotifyMonitorEstimateOfStepProgressChange(Tuple<int, int> progress)
        {
            try
            {
                using (Clients.EngineProgressMonitoringServiceCallbackClient client =
                    this.ConnectMonitoringCallbackService())
                {
                    try
                    {
                        client.NotifyMonitorEstimateOfStepProgressChange(progress);
                    }
                    catch (Exception ex)
                    {
                        throw (new EngineProgressConsumerException("Failed to invoke NotifyMonitorEstimateOfStepProgressChange.", ex));
                    }
                }
            }
            catch (Exception ex)
            {
                throw (new EngineProgressConsumerException("Failed to connect to EngineProgressMonitoringServiceCallback.", ex));
            }
        }
#endif
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private Clients.EngineProgressMonitoringServiceClient ConnectMonitoringService()
        {
            // Connect to automation service.
            this._log.Message("Connecting: {0}.", this._unresolvedUri);
            EndpointAddress address = new EndpointAddress(this._resolvedUri);
            NetTcpBinding binding = new NetTcpBinding();
            binding.ReceiveTimeout = new TimeSpan(0, 30, 0);
            binding.SendTimeout = new TimeSpan(0, 30, 0);
            binding.OpenTimeout = new TimeSpan(0, 0, 30);
            binding.CloseTimeout = new TimeSpan(0, 0, 30);
            binding.MaxReceivedMessageSize = 100 * 1024 * 1024;
            binding.MaxBufferPoolSize = 100 * 1024 * 1024;
            binding.ReaderQuotas.MaxStringContentLength = 100 * 1024 * 1024;
            binding.ReliableSession.Enabled = true;
            binding.ReliableSession.InactivityTimeout = TimeSpan.MaxValue;

            Clients.EngineProgressMonitoringServiceClient client = null;
            try
            {
                client = new Clients.EngineProgressMonitoringServiceClient(binding, address);
                client.Open();
                return (client);
            }
            catch (EndpointNotFoundException ex)
            {
                throw (new EngineProgressConsumerException("Could not connect to the end point.", ex));
            }
            catch (Exception ex)
            {
                throw (new EngineProgressConsumerException("Error connecting to EngineProgressNotificationService.", ex));
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private Clients.EngineProgressMonitoringServiceCallbackClient ConnectMonitoringCallbackService()
        {
            // Connect to automation service.
            this._log.Message("Connecting: {0}.", this._unresolvedUri);
            EndpointAddress address = new EndpointAddress(this._resolvedUri);
            NetTcpBinding binding = new NetTcpBinding();
            binding.ReceiveTimeout = new TimeSpan(0, 30, 0);
            binding.SendTimeout = new TimeSpan(0, 30, 0);
            binding.OpenTimeout = new TimeSpan(0, 0, 30);
            binding.CloseTimeout = new TimeSpan(0, 0, 30);
            binding.MaxReceivedMessageSize = 100 * 1024 * 1024;
            binding.MaxBufferPoolSize = 100 * 1024 * 1024;
            binding.ReaderQuotas.MaxStringContentLength = 100 * 1024 * 1024;
            binding.ReliableSession.Enabled = true;
            binding.ReliableSession.InactivityTimeout = TimeSpan.MaxValue;

            Clients.EngineProgressMonitoringServiceCallbackClient client = null;
            try
            {
                client = new Clients.EngineProgressMonitoringServiceCallbackClient(binding, address);
                client.Open();
                return (client);
            }
            catch (EndpointNotFoundException ex)
            {
                throw (new EngineProgressConsumerException("Could not connect to the end point.", ex));
            }
            catch (Exception ex)
            {
                throw (new EngineProgressConsumerException("Error connecting to EngineProgressNotificationService.", ex));
            }
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Services.Engine.Consumers namespace
