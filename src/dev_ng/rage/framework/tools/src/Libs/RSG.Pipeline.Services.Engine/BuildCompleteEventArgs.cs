﻿//---------------------------------------------------------------------------------------------
// <copyright file="BuildCompleteEventArgs.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Services.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;

    /// <summary>
    /// Build complete callback event arguments.
    /// </summary>
    [DataContract]
    public class BuildCompleteEventArgs
    {
        #region Properties
        /// <summary>
        /// Build completed successfully?
        /// </summary>
        [DataMember]
        public bool Successful
        {
            get;
            private set;
        }

        /// <summary>
        /// Build complete time (UTC).
        /// </summary>
        [DataMember]
        public DateTime CompleteTime
        {
            get;
            set;
        }

        /// <summary>
        /// Filenames the build produced.
        /// </summary>
        [DataMember]
        public String[] Filenames
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public BuildCompleteEventArgs()
        {
            this.Successful = false;
            this.CompleteTime = DateTime.MaxValue;
            this.Filenames = new String[0];
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="success"></param>
        /// <param name="completeTime"></param>
        /// <param name="filenames"></param>
        public BuildCompleteEventArgs(bool success, DateTime completeTime, IEnumerable<String> filenames)
        {
            this.Successful = success;
            this.CompleteTime = completeTime;
            this.Filenames = filenames.ToArray();
        }
        #endregion // Constructor(s)
    }

} // RSG.Pipeline.Services.Engine namespace
