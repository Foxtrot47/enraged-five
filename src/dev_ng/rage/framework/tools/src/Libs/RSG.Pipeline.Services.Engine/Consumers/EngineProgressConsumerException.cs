﻿//---------------------------------------------------------------------------------------------
// <copyright file="EngineProgressMonitoringServiceClient.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Services.Engine.Consumers
{
    using System;

    /// <summary>
    /// Engine Progress Consumer Exception class.
    /// </summary>
    public class EngineProgressConsumerException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the System.Exception class.
        /// </summary>
        public EngineProgressConsumerException()
            : base()
        {        
        }

        /// <summary>
        /// Initializes a new instance of the System.Exception class with a specified 
        /// error message.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public EngineProgressConsumerException(String message)
            : base(message)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="innerException"></param>
        public EngineProgressConsumerException(String message, Exception innerException)
            : base(message, innerException)
        {
        }
    }

} // RSG.Pipeline.Services.Engine.Consumers namespace
