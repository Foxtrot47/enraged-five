﻿//---------------------------------------------------------------------------------------------
// <copyright file="AssetBuildState.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Services.Engine
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Enumeration documenting a build state.
    /// </summary>
    [DataContract]
    public enum AssetBuildState
    {
        /// <summary>
        /// No builds pending (machine is idle).
        /// </summary>
        [EnumMember]
        Idle,

        /// <summary>
        /// Build is pending (waiting on XGE resource).
        /// </summary>
        [EnumMember]
        Pending,

        /// <summary>
        /// Build is currently active.
        /// </summary>
        [EnumMember]
        Building,

        /// <summary>
        /// Build is complete (and successful).
        /// </summary>
        [EnumMember]
        Complete,

        /// <summary>
        /// Build failed.
        /// </summary>
        [EnumMember]
        Failed,

        /// <summary>
        /// Build was aborted (by user).
        /// </summary>
        [EnumMember]
        Aborted,
    }

} // RSG.Pipeline.Services.Engine namespace
