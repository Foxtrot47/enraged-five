﻿//---------------------------------------------------------------------------------------------
// <copyright file="AssetBuildItem.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Services.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Xml.Linq;

    /// <summary>
    /// Asset Build item class; represents a single asset build run onces its complete.
    /// </summary>
    [DataContract]
    public sealed class AssetBuildItem
    {
        #region Properties
        /// <summary>
        /// Gets the associated project.
        /// </summary>
        [DataMember]
        public String ProjectName
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the associated branch.
        /// </summary>
        [DataMember]
        public String BranchName
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the build type.
        /// </summary>
        [DataMember]
        public AssetBuildType BuildType
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the associated start time (UTC).
        /// </summary>
        [DataMember]
        public DateTime StartTime
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the current build duration.
        /// </summary>
        [DataMember]
        public TimeSpan Duration
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the current build state.
        /// </summary>
        [DataMember]
        public AssetBuildState State
        {
            get;
            private set;
        }

        /// <summary>
        /// Machine host the build was initiated on.
        /// </summary>
        [DataMember]
        public String Host
        {
            get;
            private set;
        }

        /// <summary>
        /// User who initiated the build.
        /// </summary>
        [DataMember]
        public String Username
        {
            get;
            private set;
        }

        /// <summary>
        /// Source absolute filenames that were used to kick the build.
        /// </summary>
        [DataMember]
        public IEnumerable<String> SourceFilenames
        {
            get;
            set;
        }

        /// <summary>
        /// Output absolute filenames that were output during build.
        /// </summary>
        [DataMember]
        public IEnumerable<String> OutputFilenames
        {
            get;
            set;
        }

        /// <summary>
        /// Universal Log absolute filename.
        /// </summary>
        [DataMember]
        public String LogFilename
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public AssetBuildItem()
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="projectName"></param>
        /// <param name="branchName"></param>
        /// <param name="buildType"></param>
        /// <param name="sourceFilenames"></param>
        /// <param name="outputFilenames"></param>
        /// <param name="logFilename"></param>
        public AssetBuildItem(String projectName, String branchName, AssetBuildType buildType,
            AssetBuildState state, String host, String user, DateTime startTime, TimeSpan duration,
            IEnumerable<String> sourceFilenames, IEnumerable<String> outputFilenames,
            String logFilename)
        {
            this.ProjectName = projectName;
            this.BranchName = branchName;
            this.BuildType = buildType;
            this.State = state;
            this.Host = host;
            this.Username = user;
            this.StartTime = startTime;
            this.Duration = duration;
            this.SourceFilenames = sourceFilenames;
            this.OutputFilenames = outputFilenames;
            this.LogFilename = logFilename;
        }

        /// <summary>
        /// Constructor; from XElement.
        /// </summary>
        /// <param name="xmlElem"></param>
        public AssetBuildItem(XElement xmlElem)
        {
            this.ProjectName = xmlElem.Element("ProjectName").Value;
            this.BranchName = xmlElem.Element("BranchName").Value;
            this.BuildType = (AssetBuildType)Enum.Parse(typeof(AssetBuildType), xmlElem.Element("BuildType").Value);
            this.State = (AssetBuildState)Enum.Parse(typeof(AssetBuildState), xmlElem.Element("State").Value);
            this.Host = xmlElem.Element("Host").Value;
            this.Username = xmlElem.Element("Username").Value;

            // StartTime is always in UTC; so we ensure its set correctly on load.
            DateTime startTimeFromTicks = new DateTime(long.Parse(xmlElem.Element("StartTime").Attribute("value").Value));
            this.StartTime = DateTime.SpecifyKind(startTimeFromTicks, DateTimeKind.Utc);

            this.Duration = TimeSpan.FromMilliseconds(double.Parse(xmlElem.Element("Duration").Attribute("value").Value));
            this.SourceFilenames = xmlElem.Element("SourceFilenames").Elements("Item").Select(i => i.Value);
            this.OutputFilenames = xmlElem.Element("OutputFilenames").Elements("Item").Select(o => o.Value);
            this.LogFilename = xmlElem.Element("LogFilename").Value;

            Debug.Assert(this.StartTime.Kind.Equals(DateTimeKind.Utc));
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Serialise object to an XElement.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public XElement ToXElement(String name)
        {
            XElement xmlAssetBuildItem = new XElement(name,
                new XAttribute("type", "AssetBuildItem"),
                new XElement("ProjectName", this.ProjectName),
                new XElement("BranchName", this.BranchName),
                new XElement("BuildType", this.BuildType.ToString()),
                new XElement("State", this.State.ToString()),
                new XElement("Host", this.Host),
                new XElement("Username", this.Username),
                new XElement("StartTime", new XAttribute("value", this.StartTime.Ticks)),
                new XElement("Duration", new XAttribute("value", this.Duration.TotalMilliseconds)),
                new XElement("SourceFilenames", this.SourceFilenames.Select(f => new XElement("Item", f))),
                new XElement("OutputFilenames", this.OutputFilenames.Select(f => new XElement("Item", f))),
                new XElement("LogFilename", this.LogFilename)
            );
            return (xmlAssetBuildItem);
        }
        #endregion // Controller Methods
    }

} // RSG.Pipeline.Services.Engine namespace
