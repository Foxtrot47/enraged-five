﻿//---------------------------------------------------------------------------------------------
// <copyright file="AssetBuildItem.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Services.Engine
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Enumeration for the different types of builds.
    /// </summary>
    [DataContract]
    public enum AssetBuildType
    {
        /// <summary>
        /// Incremental build (default).
        /// </summary>
        [EnumMember]
        Build,

        /// <summary>
        /// Rebuild only.
        /// </summary>
        [EnumMember]
        Rebuild
    }

} // RSG.Pipeline.Services.Engine namespace
