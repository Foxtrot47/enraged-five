﻿//---------------------------------------------------------------------------------------------
// <copyright file="AssetBuildHistoryConsumerException.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Services.Engine.Consumers
{
    using System;

    /// <summary>
    /// Asset Build History Consumer Exception class.
    /// </summary>
    public class AssetBuildHistoryConsumerException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the System.Exception class.
        /// </summary>
        public AssetBuildHistoryConsumerException()
            : base()
        {        
        }

        /// <summary>
        /// Initializes a new instance of the System.Exception class with a specified 
        /// error message.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public AssetBuildHistoryConsumerException(String message)
            : base(message)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="innerException"></param>
        public AssetBuildHistoryConsumerException(String message, Exception innerException)
            : base(message, innerException)
        {
        }
    }

} // RSG.Pipeline.Services.Engine.Consumers namespace
