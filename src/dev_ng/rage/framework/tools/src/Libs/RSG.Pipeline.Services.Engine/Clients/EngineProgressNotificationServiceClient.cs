﻿//---------------------------------------------------------------------------------------------
// <copyright file="EngineProgressNotificationServiceClient.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Services.Engine.Clients
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel;
    using System.Xml;
    using RSG.Pipeline.Services.Engine;

    /// <summary>
    /// Engine Progress Notification Service WCF client (consumers should be used externally).
    /// </summary>
    internal sealed class EngineProgressNotificationServiceClient
        : ClientBase<IEngineProgressNotificationService>,
        IEngineProgressNotificationService
    {
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="binding"></param>
        /// <param name="remoteAddress"></param>    
        public EngineProgressNotificationServiceClient(System.ServiceModel.Channels.Binding binding, EndpointAddress remoteAddress)
            : base(binding, remoteAddress)
        {
        }
        #endregion // Constructor(s)
                
        #region IEngineProgressNotificationService Interface Methods
        /// <summary>
        /// Post notification that a build has started.
        /// </summary>
        /// <param name="e"></param>
        public void NotifyBuildStarted(BuildStartedEventArgs e)
        {
            base.Channel.NotifyBuildStarted(e);
        }

        /// <summary>
        /// Post notification that a build has completed.
        /// </summary>
        /// <param name="e"></param>
        public void NotifyBuildCompleted(BuildCompleteEventArgs e)
        {
            base.Channel.NotifyBuildCompleted(e);
        }

        /// <summary>
        /// Post notification that a build was aborted.
        /// </summary>
        /// <param name="e"></param>
        public void NotifyBuildAborted(BuildAbortedEventArgs e)
        {
            base.Channel.NotifyBuildAborted(e);
        }

        /// <summary>
        /// Post notification that the build stage has changed.
        /// </summary>
        /// <param name="stage"></param>
        public void NotifyBuildStageChange(AssetBuildStage stage)
        {
            base.Channel.NotifyBuildStageChange(stage);
        }

        /// <summary>
        /// Post notification that the progress has changed.
        /// </summary>
        /// <param name="progress"></param>
        public void NotifyEstimateProgressChange(Tuple<int, int> progress)
        {
            base.Channel.NotifyEstimateProgressChange(progress);
        }

        /// <summary>
        /// Post notification of the profile message.
        /// </summary>
        /// <param name="message"></param>
        public void NotifyProfileMessage(String message)
        {
            base.Channel.NotifyProfileMessage(message);
        }
        #endregion // IEngineProgressNotificationService Interface Methods
    }

} // RSG.Pipeline.Services.Engine.Clients namespace
