﻿//---------------------------------------------------------------------------------------------
// <copyright file="EngineProgressNotificationServiceConsumer.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Services.Engine.Consumers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel;
    using RSG.Base.Logging.Universal;

    /// <summary>
    /// Engine Progress Service consumer; created by the Engine implementations 
    /// to send notifications to the Tools Service the engine progress.
    /// </summary>
    public class EngineProgressNotificationServiceConsumer :
        EngineConsumerBase
    {
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="serviceConnection"></param>
        public EngineProgressNotificationServiceConsumer(IUniversalLog log, Uri serviceConnection)
            : base(log, serviceConnection)
        {
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Post notification that a build has started.
        /// </summary>
        /// <param name="e"></param>
        public void NotifyBuildStarted(BuildStartedEventArgs e)
        {
            this._log.Debug("EngineProgressNotificationServiceConsumer::NotifyBuildStarted()");

            Clients.EngineProgressNotificationServiceClient client = null;
            try
            {
                client = this.ConnectNotificationService();
                client.NotifyBuildStarted(e);
            }
            catch (System.ServiceModel.CommunicationException ex)
            {
                throw (new EngineProgressConsumerException("NotifyProfileMessage failed.", ex.InnerException));
            }
            catch (Exception ex)
            {
                throw (new EngineProgressConsumerException("Failed to connect to EngineProgressNotificationService.", ex));
            }
        }

        /// <summary>
        /// Post notification that a build has completed.
        /// </summary>
        /// <param name="e"></param>
        public void NotifyBuildCompleted(BuildCompleteEventArgs e)
        {
            this._log.Debug("EngineProgressNotificationServiceConsumer::NotifyBuildCompleted()");

            Clients.EngineProgressNotificationServiceClient client = null;
            try
            {
                client = this.ConnectNotificationService();
                client.NotifyBuildCompleted(e);
            }
            catch (System.ServiceModel.CommunicationException ex)
            {
                throw (new EngineProgressConsumerException("NotifyProfileMessage failed.", ex.InnerException));
            }
            catch (Exception ex)
            {
                throw (new EngineProgressConsumerException("Failed to connect to EngineProgressNotificationService.", ex));
            }
        }

        /// <summary>
        /// Post notification that a build was aborted.
        /// </summary>
        /// <param name="e"></param>
        public void NotifyBuildAborted(BuildAbortedEventArgs e)
        {
            this._log.Debug("EngineProgressNotificationServiceConsumer::NotifyBuildAborted()");

            Clients.EngineProgressNotificationServiceClient client = null;
            try
            {
                client = this.ConnectNotificationService();
                client.NotifyBuildAborted(e);
            }
            catch (System.ServiceModel.CommunicationException ex)
            {
                throw (new EngineProgressConsumerException("NotifyProfileMessage failed.", ex.InnerException));
            }
            catch (Exception ex)
            {
                throw (new EngineProgressConsumerException("Failed to connect to EngineProgressNotificationService.", ex));
            }
        }

        /// <summary>
        /// Post notification that the build stage has changed.
        /// </summary>
        /// <param name="stage"></param>
        public void NotifyBuildStageChange(AssetBuildStage stage)
        {
            this._log.Debug("EngineProgressNotificationServiceConsumer::NotifyBuildStageChange()");

            Clients.EngineProgressNotificationServiceClient client = null;
            try
            {
                client = this.ConnectNotificationService();
                client.NotifyBuildStageChange(stage);
            }
            catch (System.ServiceModel.CommunicationException ex)
            {
                throw (new EngineProgressConsumerException("NotifyProfileMessage failed.", ex.InnerException));
            }
            catch (Exception ex)
            {
                throw (new EngineProgressConsumerException("Failed to connect to EngineProgressNotificationService.", ex));
            }
        }

        /// <summary>
        /// Post notification that the progress has changed.
        /// </summary>
        /// <param name="progress"></param>
        public void NotifyEstimateProgressChange(Tuple<int, int> progress)
        {
            this._log.Debug("EngineProgressNotificationServiceConsumer::NotifyEstimateProgressChange()");

            Clients.EngineProgressNotificationServiceClient client = null;
            try
            {
                client = this.ConnectNotificationService();
                client.NotifyEstimateProgressChange(progress);
            }
            catch (System.ServiceModel.CommunicationException ex)
            {
                throw (new EngineProgressConsumerException("NotifyProfileMessage failed.", ex.InnerException));
            }
            catch (Exception ex)
            {
                throw (new EngineProgressConsumerException("Failed to connect to EngineProgressNotificationService.", ex));
            }
        }

        /// <summary>
        /// Post notification that the profile message has changed.
        /// </summary>
        /// <param name="message"></param>
        public void NotifyProfileMessage(String message)
        {
            this._log.Debug("EngineProgressNotificationServiceConsumer::NotifyProfileMessage()");

            Clients.EngineProgressNotificationServiceClient client = null;
            try
            {
                client = this.ConnectNotificationService();
                client.NotifyProfileMessage(message);
            }
            catch (System.ServiceModel.CommunicationException ex)
            {
                throw (new EngineProgressConsumerException("NotifyProfileMessage failed.", ex.InnerException));
            }
            catch (Exception ex)
            {
                throw (new EngineProgressConsumerException("Failed to connect to EngineProgressNotificationService.", ex));
            }
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private Clients.EngineProgressNotificationServiceClient ConnectNotificationService()
        {
            // Connect to automation service.
            this._log.Message("Connecting: {0}.", this._unresolvedUri);
            EndpointAddress address = new EndpointAddress(this._resolvedUri);
            NetTcpBinding binding = new NetTcpBinding();
            binding.ReceiveTimeout = new TimeSpan(0, 30, 0);
            binding.SendTimeout = new TimeSpan(0, 30, 0);
            binding.OpenTimeout = new TimeSpan(0, 0, 30);
            binding.CloseTimeout = new TimeSpan(0, 0, 30);
            binding.MaxReceivedMessageSize = 100 * 1024 * 1024;
            binding.MaxBufferPoolSize = 100 * 1024 * 1024;
            binding.ReaderQuotas.MaxStringContentLength = 100 * 1024 * 1024;
            binding.ReliableSession.Enabled = true;
            binding.ReliableSession.InactivityTimeout = TimeSpan.MaxValue;

            Clients.EngineProgressNotificationServiceClient client = null;
            try
            {
                client = new Clients.EngineProgressNotificationServiceClient(binding, address);
                client.Open();
                return (client);
            }
            catch (EndpointNotFoundException ex)
            {
                throw (new EngineProgressConsumerException("Could not connect to the end point.", ex));
            }
            catch (Exception ex)
            {
                throw (new EngineProgressConsumerException("Error connecting to EngineProgressNotificationService.", ex));
            }
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Services.Engine.Consumers namespace
