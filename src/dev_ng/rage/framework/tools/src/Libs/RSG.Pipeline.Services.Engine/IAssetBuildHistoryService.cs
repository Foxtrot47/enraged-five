﻿//---------------------------------------------------------------------------------------------
// <copyright file="IAssetBuildHistoryService.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Services.Engine
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.ServiceModel;

    /// <summary>
    /// 
    /// </summary>
    [ServiceContract]
    public interface IAssetBuildHistoryService
    {        
        /// <summary>
        /// Return information about client Build History.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [Description("Return information about client Build History.")]
        IEnumerable<AssetBuildItem> GetBuildHistory();
        
        /// <summary>
        /// Clear the client Build History.
        /// </summary>
        [OperationContract]
        [Description("Clear the client Build History.")]
        void ClearBuildHistory();
    }

} // RSG.Pipeline.Services.Engine namespace
