﻿//---------------------------------------------------------------------------------------------
// <copyright file="BuildAbortedEventArgs.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Services.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// Build aborted callback event arguments.
    /// </summary>
    [DataContract]
    public class BuildAbortedEventArgs
    {
        #region Properties
        /// <summary>
        /// Build aborted time (UTC).
        /// </summary>
        [DataMember]
        public DateTime AbortTime
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public BuildAbortedEventArgs()
        {
            this.AbortTime = DateTime.MaxValue;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="abortedTime"></param>
        public BuildAbortedEventArgs(DateTime abortedTime)
        {
            this.AbortTime = abortedTime;
        }
        #endregion // Constructor(s)
    }

} // RSG.Pipeline.Services.Engine namespace
