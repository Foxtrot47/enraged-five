﻿//---------------------------------------------------------------------------------------------
// <copyright file="BuildHistoryManager.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Services.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.ServiceModel;
    using System.Xml.Linq;

    /// <summary>
    /// Asset Build History Manager class; this is used to maintain build history and 
    /// its persistence on-disk.
    /// </summary>
    public class BuildHistoryManager
    {
        #region Properties
        /// <summary>
        /// Get persistent build history.
        /// </summary>
        public IEnumerable<AssetBuildItem> BuildHistory
        {
            get { return _buildHistory; }
        }

        /// <summary>
        /// Filename of our local build history metadata.
        /// </summary>
        public static String LocalBuildHistoryFilename
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Storage for our persistent build history.
        /// </summary>
        private List<AssetBuildItem> _buildHistory;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor; localhost.
        /// </summary>
        public BuildHistoryManager()
        {
            this._buildHistory = new List<AssetBuildItem>();
        }

        /// <summary>
        /// Constructor; loading history from file.
        /// </summary>
        /// <param name="filename"></param>
        public BuildHistoryManager(String filename)
            : this()
        {
            if (System.IO.File.Exists(filename))
                this.Load(filename);    
        }

        /// <summary>
        /// Static constructor.
        /// </summary>
        static BuildHistoryManager()
        {
            String folder = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            LocalBuildHistoryFilename = System.IO.Path.Combine(folder, 
                "Rockstar Games", "Asset Pipeline", "BuildHistory.meta");
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Add details of a new build.
        /// </summary>
        /// <param name="newBuild"></param>
        public void AddBuild(AssetBuildItem newBuild)
        {
            this._buildHistory.Add(newBuild);
        }

        /// <summary>
        /// Clear all builds from history; must be saved to be persistent.
        /// </summary>
        public void Clear()
        {
            this._buildHistory.Clear();
        }

        /// <summary>
        /// Load build history from parCodeGen metadata file.
        /// </summary>
        public void Load(String filename)
        {
            XDocument xmlDoc = XDocument.Load(filename);
            this._buildHistory.AddRange(xmlDoc.Root.Element("BuildHistory").Elements("Item").
                Select(i => new AssetBuildItem(i)));
        }

        /// <summary>
        /// Save build history to parCodeGen metadata file.
        /// </summary>
        /// <param name="filename"></param>
        public void Save(String filename)
        {
            XDocument xmlDoc = new XDocument(
                new XDeclaration("1.0", "utf-8", "yes"),
                new XElement("BuildHistoryManager",
                    new XElement("BuildHistory",  
                        this._buildHistory.Select(bi => bi.ToXElement("Item"))
                    ) // BuildHistory
                ) // BuildHistory
            );

            String directory = System.IO.Path.GetDirectoryName(filename);
            if (!System.IO.Directory.Exists(directory))
                System.IO.Directory.CreateDirectory(directory);
            xmlDoc.Save(filename);
        }
        #endregion // Controller Methods
    }

} // RSG.Pipeline.Services.Engine namespace
