﻿//---------------------------------------------------------------------------------------------
// <copyright file="EngineConsumerBase.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Services.Engine.Consumers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using RSG.Base.Logging;
    using RSG.Base.Logging.Universal;

    /// <summary>
    /// Abstract base class for Engine service consumers.
    /// </summary>
    public abstract class EngineConsumerBase
    {
        #region Properties
        /// <summary>
        /// Service connection string (e.g. "tcp.net://ediw-dmuir3:7001/service.svc")
        /// </summary>
        public Uri ServiceConnection
        {
            get { return _unresolvedUri; }
        }

        /// <summary>
        /// Service identifier (generated automatically).
        /// </summary>
        public Guid ID
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Uri where the host name hasn't been resolved to an IP address.
        /// </summary>
        protected readonly Uri _unresolvedUri;

        /// <summary>
        /// Uri where the host name has been resolved to an IP address.
        /// </summary>
        protected readonly Uri _resolvedUri;

        /// <summary>
        /// Consumer log.
        /// </summary>
        protected readonly IUniversalLog _log;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="service"></param>
        public EngineConsumerBase(IUniversalLog log, Uri service)
        {
            this.ID = Guid.NewGuid();
            this._unresolvedUri = service;
            this._resolvedUri = ResolveUri(this._unresolvedUri);
            this._log = log;
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// Resolves the host of a uri and converts it to an IP address via a DNS lookup.
        /// </summary>
        /// <param name="uri"></param>
        /// <returns></returns>
        private Uri ResolveUri(Uri uri)
        {
            System.Net.IPAddress[] addresslist = 
                System.Net.Dns.GetHostAddresses(uri.DnsSafeHost);
            if (addresslist.Length < 1)
            {
                throw new ArgumentException("Unable to resolve the service connection's IP.");
            }

            UriBuilder builder = new UriBuilder(uri);
            builder.Host = addresslist[0].ToString();
            return builder.Uri;
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Services.Engine.Consumers namespace
