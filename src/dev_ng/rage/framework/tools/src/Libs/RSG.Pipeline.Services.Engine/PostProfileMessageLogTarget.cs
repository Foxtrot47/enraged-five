﻿//---------------------------------------------------------------------------------------------
// <copyright file="EngineProgressNotificationServiceConsumer.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Services.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using RSG.Base.Logging;
    using RSG.Base.Logging.Universal;
    using RSG.Pipeline.Services.Engine.Consumers;

    /// <summary>
    /// Engine Profiling Message progress posting target (for Build Monitor support).
    /// </summary>
    public class PostProfileMessageLogTarget : 
        ThreadedLogTargetBase,
        IUniversalLogTarget,
        ILogTarget
    {
        #region Member Data
        /// <summary>
        /// Progress Consumer.
        /// </summary>
        private EngineProgressNotificationServiceConsumer _consumer;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="consumer"></param>
        public PostProfileMessageLogTarget(EngineProgressNotificationServiceConsumer consumer)
        {
            this._consumer = consumer;
        }
        #endregion // Constructor(s)
        
        #region ILogFileTarget Interface Methods
        /// <summary>
        /// Flush the target.
        /// </summary>
        public override void Flush()
        {
            // Do nothing.
        }
        #endregion // ILogFileTarget Interface Methods

        #region ThreadedLogTargetBase Methods
        /// <summary>
        /// Invoked by worker thread to actually handle the messages.
        /// </summary>
        /// <param name="args"></param>
        protected override void LogMessage(LogMessageEventArgs args)
        {
            if (args.Level != LogLevel.Profile)
                return;

            this._consumer.NotifyProfileMessage(args.Message);
        }
        #endregion // ThreadedLogTargetBase Methods
    }

} // RSG.Pipeline.Services.Engine namespace
