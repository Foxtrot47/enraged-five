﻿//---------------------------------------------------------------------------------------------
// <copyright file="BuildStartedEventArgs.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Services.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;

    /// <summary>
    /// Build started callback event arguments.
    /// </summary>
    [DataContract]
    public class BuildStartedEventArgs
    {
        #region Properties
        /// <summary>
        /// Project name.
        /// </summary>
        [DataMember]
        public String ProjectName
        {
            get;
            set;
        }

        /// <summary>
        /// Branch name.
        /// </summary>
        [DataMember]
        public String BranchName
        {
            get;
            set;
        }

        /// <summary>
        /// Build initiated on host.
        /// </summary>
        [DataMember]
        public String Hostname
        {
            get;
            set;
        }

        /// <summary>
        /// Build initiated by user.
        /// </summary>
        [DataMember]
        public String Username
        {
            get;
            set;
        }

        /// <summary>
        /// Build type.
        /// </summary>
        [DataMember]
        public AssetBuildType BuildType
        {
            get;
            set;
        }

        /// <summary>
        /// Build start time (UTC).
        /// </summary>
        [DataMember]
        public DateTime StartTime
        {
            get;
            set;
        }

        /// <summary>
        /// Filenames the build was started with.
        /// </summary>
        [DataMember]
        public String[] Filenames
        {
            get;
            set;
        }

        /// <summary>
        /// Log filename.
        /// </summary>
        [DataMember]
        public String LogFilename
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public BuildStartedEventArgs()
        {
            this.StartTime = DateTime.MaxValue;
            this.Filenames = new String[0];
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="projectName"></param>
        /// <param name="branchName"></param>
        /// <param name="startTime"></param>
        /// <param name="filenames"></param>
        /// <param name="logFilename"></param>
        public BuildStartedEventArgs(String projectName, String branchName, DateTime startTime, 
            IEnumerable<String> filenames, String logFilename)
        {
            this.ProjectName = projectName;
            this.BranchName = branchName;
            this.Hostname = Environment.MachineName;
            this.Username = Environment.UserName;
            this.StartTime = startTime;
            this.Filenames = filenames.ToArray();
            this.LogFilename = logFilename;
        }
        #endregion // Constructor(s)
    }

} // RSG.Pipeline.Services.Engine namespace
