﻿//---------------------------------------------------------------------------------------------
// <copyright file="AssetBuildHistoryServiceConsumer.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Services.Engine.Consumers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel;
    using System.Text;
    using RSG.Base.Logging.Universal;

    /// <summary>
    /// 
    /// </summary>
    public sealed class AssetBuildHistoryServiceConsumer :
        EngineConsumerBase
    {
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="service"></param>
        public AssetBuildHistoryServiceConsumer(IUniversalLog log, Uri service)
            : base(log, service)
        {
        }
        #endregion // Constructor(s)

        /// <summary>
        /// Return information about client Build History.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<AssetBuildItem> GetBuildHistory()
        {
            this._log.Debug("AssetBuildHistoryServiceConsumer::GetBuildHistory()");

            Clients.AssetBuildHistoryServiceClient client = null;
            try
            {
                client = this.Connect();
                return (client.GetBuildHistory());
            }
            catch (System.ServiceModel.CommunicationException ex)
            {
                throw (new AssetBuildHistoryConsumerException("GetBuildHistory failed.", ex.InnerException));
            }
            catch (Exception ex)
            {
                throw (new AssetBuildHistoryConsumerException("Failed to connect to AssetBuildHistoryService.", ex));
            }
            finally
            {
                if (null != client)
                {
                    if (client.State == CommunicationState.Faulted)
                        client.Abort();
                    else
                        client.Close();
                }
            }
        }

        /// <summary>
        /// Clear client Build History.
        /// </summary>
        public void ClearBuildHistory()
        {
            this._log.Debug("AssetBuildHistoryServiceConsumer::ClearBuildHistory()");

            Clients.AssetBuildHistoryServiceClient client = null;
            try
            {
                client = this.Connect();
                client.ClearBuildHistory();
            }
            catch (System.ServiceModel.CommunicationException ex)
            {
                throw (new AssetBuildHistoryConsumerException("GetBuildHistory failed.", ex.InnerException));
            }
            catch (Exception ex)
            {
                throw (new AssetBuildHistoryConsumerException("Failed to connect to AssetBuildHistoryService.", ex));
            }
            finally
            {
                if (null != client)
                {
                    if (client.State == CommunicationState.Faulted)
                        client.Abort();
                    else
                        client.Close();
                }
            }
        }
        
        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private Clients.AssetBuildHistoryServiceClient Connect()
        {
            // Connect to automation service.
            this._log.Message("Connecting: {0}.", this._unresolvedUri);
            EndpointAddress address = new EndpointAddress(this._resolvedUri);
            NetTcpBinding binding = new NetTcpBinding();
            binding.ReceiveTimeout = new TimeSpan(0, 30, 0);
            binding.SendTimeout = new TimeSpan(0, 30, 0);
            binding.OpenTimeout = new TimeSpan(0, 0, 30);
            binding.CloseTimeout = new TimeSpan(0, 0, 30);
            binding.MaxReceivedMessageSize = 100 * 1024 * 1024;
            binding.MaxBufferPoolSize = 100 * 1024 * 1024;
            binding.ReaderQuotas.MaxStringContentLength = 100 * 1024 * 1024;
            binding.ReliableSession.Enabled = true;
            binding.ReliableSession.InactivityTimeout = TimeSpan.MaxValue;

            Clients.AssetBuildHistoryServiceClient client = null;
            try
            {
                client = new Clients.AssetBuildHistoryServiceClient(binding, address);
                client.Open();
                return (client);
            }
            catch (EndpointNotFoundException ex)
            {
                throw (new AssetBuildHistoryConsumerException("Could not connect to the end point.", ex));
            }
            catch (Exception ex)
            {
                throw (new AssetBuildHistoryConsumerException("Error connecting to AssetBuildHistoryService.", ex));
            }
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Services.Engine.Consumers namespace
