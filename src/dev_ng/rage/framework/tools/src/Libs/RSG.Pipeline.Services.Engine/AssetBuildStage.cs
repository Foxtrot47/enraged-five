﻿//---------------------------------------------------------------------------------------------
// <copyright file="AssetBuildStage.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Services.Engine
{
    using System.Runtime.Serialization;
    using RSG.Base.Attributes;

    /// <summary>
    /// Enumeration of different Asset Build stages that we report on from the Engine.
    /// </summary>
    [DataContract]
    public enum AssetBuildStage
    {
        /// <summary>
        /// Engine is current idle; or not started.
        /// </summary>
        [EnumMember]
        [FieldDisplayName("Idle")]
        Idle,

        /// <summary>
        /// Engine is current analysing what to build.
        /// </summary>
        [EnumMember]
        [FieldDisplayName("Analysing Inputs and Processes")]
        Prebuild,

        /// <summary>
        /// Engine is syncing data required for the build.
        /// </summary>
        [EnumMember]
        [FieldDisplayName("Syncing Data")]
        Syncing,

        /// <summary>
        /// Engine is building data (locally).
        /// </summary>
        [EnumMember]
        [FieldDisplayName("Building Local")]
        BuildLocal,

        /// <summary>
        /// Engine is building data.
        /// </summary>
        [EnumMember]
        [FieldDisplayName("Building")]
        Build,
    }

} // RSG.Pipeline.Services.Engine namespace
