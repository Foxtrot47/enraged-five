﻿//---------------------------------------------------------------------------------------------
// <copyright file="IEngineProgressNotificationService.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Services.Engine
{
    using System;
    using System.ServiceModel;

    /// <summary>
    /// Engine Progress Service; designed to allow Engine progress information to be
    /// posted from an IEngine implementation.
    /// </summary>
    /// It is expected that IEngine implementation connect to this service to post
    /// asset build progress information.
    [ServiceContract]
    public interface IEngineProgressNotificationService
    {
        /// <summary>
        /// Post notification that a build has started.
        /// </summary>
        /// <param name="e"></param>
        [OperationContract]
        void NotifyBuildStarted(BuildStartedEventArgs e);

        /// <summary>
        /// Post notification that a build has completed.
        /// </summary>
        /// <param name="e"></param>
        [OperationContract]
        void NotifyBuildCompleted(BuildCompleteEventArgs e);

        /// <summary>
        /// Post notification that a build was aborted.
        /// </summary>
        /// <param name="e"></param>
        [OperationContract]
        void NotifyBuildAborted(BuildAbortedEventArgs e);

        /// <summary>
        /// Post notification that the build stage has changed.
        /// </summary>
        /// <param name="stage"></param>
        [OperationContract]
        void NotifyBuildStageChange(AssetBuildStage stage);

        /// <summary>
        /// Post notification that the progress has changed.
        /// </summary>
        /// <param name="progress"></param>
        [OperationContract]
        void NotifyEstimateProgressChange(Tuple<int, int> progress);

        /// <summary>
        /// Post notification that the profile message has changed.
        /// </summary>
        /// <param name="message"></param>
        [OperationContract]
        void NotifyProfileMessage(String message);
    }

} // RSG.Pipeline.Services.Engine namespace
