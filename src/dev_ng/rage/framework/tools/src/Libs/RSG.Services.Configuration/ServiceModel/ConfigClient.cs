﻿//---------------------------------------------------------------------------------------------
// <copyright file="ConfigClient.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace RSG.Services.Configuration.ServiceModel
{
    /// <summary>
    /// Custom WCF client that obtains it's configuration data from the server
    /// configuration data.
    /// </summary>
    /// <typeparam name="TChannel"></typeparam>
    public abstract class ConfigClient<TChannel> : ClientBase<TChannel> where TChannel : class
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="AutoConfiguredClient"/> using
        /// the specified server configuration object and uri scheme.
        /// </summary>
        public ConfigClient(IServer server, string uriScheme)
            : this(GetServiceEndpoint(server, uriScheme))
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="AutoConfiguredClient"/> using
        /// the specified service endpoint configuration object.
        /// </summary>
        private ConfigClient(ServiceEndpoint endpoint)
            : base(endpoint.Binding, endpoint.Address)
        {
        }
        #endregion

        #region Methods
        /// <summary>
        /// Helper method to obtain the service endpoint configuration data for the
        /// client from the server configuration object.
        /// </summary>
        /// <param name="service"></param>
        /// <param name="uriScheme"></param>
        /// <returns></returns>
        private static ServiceEndpoint GetServiceEndpoint(IServer server, string uriScheme)
        {
            IService service = server.GetService<TChannel>();
            if (service == null)
            {
                throw new KeyNotFoundException("Unable to retrieve the service configuration object for " + typeof(TChannel).Name);
            }

            ServiceEndpoint endpoint = service.GetEndpoint(uriScheme);
            if (endpoint == null)
            {
                throw new EndpointNotFoundException("Unable to retrieve the endpoint configuration object for the " + uriScheme + " uri scheme for the " + typeof(TChannel).Name + "service contract.");
            }

            return endpoint;
        }
        #endregion
    }
}
