﻿//---------------------------------------------------------------------------------------------
// <copyright file="LiveEditConfig.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System.IO;
using System.Xml.Linq;
using RSG.Configuration;

namespace RSG.Services.Configuration.LiveEdit
{
    /// <summary>
    /// LiveEdit services configuration.
    /// </summary>
    public class LiveEditConfig : ServicesConfig<IServer>
    {
        #region Constants
        /// <summary>
        /// Name of the statistics configuration file.
        /// </summary>
        private const string _configFilename = @"liveedit.xml";
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the LiveEdit configuration from the specified file.
        /// </summary>
        /// <param name="filepath"></param>
        public LiveEditConfig(string filepath)
            : base(filepath)
        {
        }

        /// <summary>
        /// Initialises a new instance of the LiveEdit configuration for the specified
        /// project.
        /// </summary>
        /// <param name="filepath"></param>
        public LiveEditConfig(ToolsConfig config)
            : this(Path.Combine(config.ToolsConfigDirectory, "services", _configFilename))
        {
        }

        /// <summary>
        /// Initialises a new instance of the LiveEdit configuration for the specified
        /// project.
        /// </summary>
        /// <param name="config"></param>
        public LiveEditConfig(IConfig config)
            : this(Path.Combine(config.ToolsConfigDirectory, "services", _configFilename))
        {
        }
        #endregion

        #region Methods
        protected override IServer CreateServerInstance(XElement serverElement, XElement servicesElement)
        {
            return new Server(serverElement, servicesElement, this);
        }

        #endregion
    }
}
