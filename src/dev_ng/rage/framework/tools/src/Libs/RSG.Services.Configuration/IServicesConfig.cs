﻿//---------------------------------------------------------------------------------------------
// <copyright file="IServicesConfig.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System.Collections.Generic;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;

namespace RSG.Services.Configuration
{
    /// <summary>
    /// Services configuration.
    /// </summary>
    public interface IServicesConfig<out T> where T : IServer
    {
        #region Properties
        /// <summary>
        /// Gets the default server to connect to.
        /// </summary>
        T DefaultServer { get; }

        /// <summary>
        /// Gets the list of servers that are available to connect to.
        /// </summary>
        IEnumerable<T> Servers { get; }
        #endregion

        #region Methods
        /// <summary>
        /// Retrieves the binding for the specified binding configuration and type.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        Binding GetBinding(string type, string configuration);

        /// <summary>
        /// Retrieves the list of endpoint behaviours for the specified configuration.
        /// </summary>
        /// <param name="configuration"></param>
        /// <returns></returns>
        IEnumerable<IEndpointBehavior> GetEndpointBehaviours(string configuration);

        /// <summary>
        /// Retrieves a server by name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        T GetServerByName(string name);

        /// <summary>
        /// Retrieves the list of service behaviours for the specified configuration.
        /// </summary>
        /// <param name="configuration"></param>
        /// <returns></returns>
        IEnumerable<IServiceBehavior> GetServiceBehaviours(string configuration);
        #endregion
    }
}
