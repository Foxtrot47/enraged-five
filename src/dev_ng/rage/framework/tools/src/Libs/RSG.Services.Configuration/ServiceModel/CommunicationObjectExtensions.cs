﻿//---------------------------------------------------------------------------------------------
// <copyright file="ClientBaseExtensions.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System.ServiceModel;
using System.ServiceModel.Channels;

namespace RSG.Services.Configuration.ServiceModel
{
    /// <summary>
    /// Extension methods for communication objects (i.e. WCF service hosts or clients).
    /// </summary>
    public static class CommunicationObjectExtensions
    {
        /// <summary>
        /// Closes or aborts a the communication object based on it's current state.
        /// </summary>
        /// <param name="obj"></param>
        public static void CloseOrAbort(this ICommunicationObject obj)
        {
            if (obj.State != CommunicationState.Faulted)
            {
                obj.Close();
            }
            else
            {
                obj.Abort();
            }
        }
    }
}
