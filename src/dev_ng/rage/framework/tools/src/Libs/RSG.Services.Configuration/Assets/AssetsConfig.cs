﻿//---------------------------------------------------------------------------------------------
// <copyright file="AssetsConfig.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System.IO;
using System.Xml.Linq;
using RSG.Configuration;

namespace RSG.Services.Configuration.Assets
{
    /// <summary>
    /// Assets services configuration.
    /// </summary>
    public class AssetsConfig : ServicesConfig<IDatabaseServer>
    {
        #region Constants
        /// <summary>
        /// Name of the statistics configuration file.
        /// </summary>
        private const string _configFilename = @"assets.xml";
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the assets configuration from the specified file.
        /// </summary>
        /// <param name="filepath"></param>
        public AssetsConfig(string filepath)
            : base(filepath)
        {
        }

        /// <summary>
        /// Initialises a new instance of the assets configuration for the specified
        /// project.
        /// </summary>
        /// <param name="filepath"></param>
        public AssetsConfig(ToolsConfig config)
            : this(Path.Combine(config.ToolsConfigDirectory, "services", _configFilename))
        {
        }

        /// <summary>
        /// Initialises a new instance of the assets configuration for the specified
        /// project.
        /// </summary>
        /// <param name="config"></param>
        public AssetsConfig(IConfig config)
            : this(Path.Combine(config.ToolsConfigDirectory, "services", _configFilename))
        {
        }
        #endregion

        #region Methods
        /// <summary>
        /// Creates a new server instance from server and services xml elements.
        /// </summary>
        /// <param name="serverElement"></param>
        /// <param name="servicesElement"></param>
        /// <returns></returns>
        protected override IDatabaseServer CreateServerInstance(XElement serverElement, XElement servicesElement)
        {
            return new DatabaseServer(serverElement, servicesElement, this);
        }
        #endregion
    }
}
