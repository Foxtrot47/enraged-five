﻿//---------------------------------------------------------------------------------------------
// <copyright file="Server.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Xml.Linq;
using RSG.Configuration;

namespace RSG.Services.Configuration
{
    /// <summary>
    /// Server configuration.
    /// </summary>
    public class Server : IServer
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="Environment"/> property.
        /// </summary>
        private readonly IEnvironment _environment;

        /// <summary>
        /// Private field for the <see cref="Host"/> property.
        /// </summary>
        private readonly string _host;

        /// <summary>
        /// Private field for the <see cref="Name"/> property.
        /// </summary>
        private readonly string _name;

        /// <summary>
        /// Private field for the <see cref="Services"/> property.
        /// </summary>
        private readonly IList<IService> _services;

        /// <summary>
        /// Private field for the <see cref="UsingIIS"/> property.
        /// </summary>
        private readonly bool _usingIIS;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="Server"/> class using the
        /// provided server and services xml elements along with the pre-created binding,
        /// endpoint behaviours and service behaviours.
        /// </summary>
        /// <param name="serverElement"></param>
        /// <param name="servicesElement"></param>
        /// <param name="servicesConfig"></param>
        public Server(
            XElement serverElement,
            XElement servicesElement,
            IServicesConfig<IServer> servicesConfig)
        {
            _services = new List<IService>();

            // Parse the server element first.
            _name = GetSubElementValue<string>(serverElement, "Name");
            _host = GetSubElementValue<string>(serverElement, "Host");
            _usingIIS = GetSubElementValue<bool>(serverElement, "UsingIIS");

            // Get the environment variables we should use when parsing the services.
            _environment = ConfigFactory.CreateEnvironment();
            _environment.Add("server_name", _name);
            _environment.Add("server_host", _host);

            XElement servicesEnvironmentElement = serverElement.Element("ServicesEnvironment");
            foreach (XElement environmentElement in servicesEnvironmentElement.Elements())
            {
                _environment.Add(environmentElement.Name.LocalName, environmentElement.Value);
            }

            // Parse the services next making use of all the information we've gathered so far.
            foreach (XElement serviceElement in servicesElement.Elements("Service"))
            {
                Service service = new Service(
                    serviceElement,
                    this,
                    servicesConfig);
                _services.Add(service);
            }
        }
        #endregion

        #region Properties
        /// <summary>
        /// Environment for this server.
        /// </summary>
        public IEnvironment Environment
        {
            get { return _environment; }
        }

        /// <summary>
        /// Gets the host where this server is hosted.
        /// </summary>
        public string Host
        {
            get { return _host; }
        }

        /// <summary>
        /// Gets the name used to identify this server.
        /// </summary>
        public string Name
        {
            get { return _name; }
        }

        /// <summary>
        /// List of services this server exposes.
        /// </summary>
        public IEnumerable<IService> Services
        {
            get { return _services; }
        }

        /// <summary>
        /// Gets a flag indicating whether the server is being hosted under IIS.
        /// </summary>
        public bool UsingIIS
        {
            get { return _usingIIS; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Read attribute of a particular type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="elem"></param>
        /// <param name="attr"></param>
        protected T GetAttributeValue<T>(XElement element, string attributeName)
        {
            XAttribute attribute = element.Attribute(attributeName);
            if (attribute == null)
            {
                ConfigurationErrorsException exception = new ConfigurationErrorsException("Server element is missing a required attribute.");
                exception.Data.Add("ServerName", _name);
                exception.Data.Add("ElementName", element.Name);
                exception.Data.Add("AttributeName", attributeName);
                throw exception;
            }

            return (T)Convert.ChangeType(attribute.Value, typeof(T));
        }

        /// <summary>
        /// Retreives a service of type <typeparamref name="T"/>.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public IService GetService<T>()
        {
            return GetService(typeof(T));
        }

        /// <summary>
        /// Retrieves a service for the specified contract type.
        /// </summary>
        /// <param name="contractType"></param>
        /// <returns></returns>
        public IService GetService(Type contractType)
        {
            IService service = null;

            Type contractTypeToCheck = contractType;
            while (contractTypeToCheck != null)
            {
                service = _services.FirstOrDefault(item => item.ContractType == contractTypeToCheck);
                if (service != null)
                {
                    break;
                }

                // Probably safe...
                contractTypeToCheck = contractTypeToCheck.GetInterfaces().FirstOrDefault();
            }

            return service;
        }

        /// <summary>
        /// Read attribute of a particular type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="elem"></param>
        /// <param name="attr"></param>
        protected T GetSubElementValue<T>(XElement element, string subElementName)
        {
            XElement subElement = element.Element(subElementName);
            if (subElement == null)
            {
                ConfigurationErrorsException exception = new ConfigurationErrorsException("Server element is missing a required sub-element.");
                exception.Data.Add("ServerName", _name);
                exception.Data.Add("ElementName", element.Name);
                exception.Data.Add("SubElementName", subElementName);
                throw exception;
            }

            return (T)Convert.ChangeType(subElement.Value, typeof(T));
        }
        #endregion
    }
}
