﻿//---------------------------------------------------------------------------------------------
// <copyright file="IServer.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using RSG.Configuration;

namespace RSG.Services.Configuration
{
    /// <summary>
    /// Configuration information about a single server.
    /// </summary>
    public interface IServer
    {
        #region Properties
        /// <summary>
        /// Environment for this server.
        /// </summary>
        IEnvironment Environment { get; }

        /// <summary>
        /// Gets the host where this server is hosted.
        /// </summary>
        string Host { get; }

        /// <summary>
        /// Gets the name used to identify this server.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets a list of services this server exposes.
        /// </summary>
        IEnumerable<IService> Services { get; }

        /// <summary>
        /// Gets a flag indicating whether the server is being hosted under IIS.
        /// </summary>
        bool UsingIIS { get; }
        #endregion

        #region Methods
        /// <summary>
        /// Retreives a service of type <typeparamref name="T"/>.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        IService GetService<T>();

        /// <summary>
        /// Retrieves a service for the specified contract type.
        /// </summary>
        /// <param name="contractType"></param>
        /// <returns></returns>
        IService GetService(Type contractType);
        #endregion
    }
}
