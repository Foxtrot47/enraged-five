﻿//---------------------------------------------------------------------------------------------
// <copyright file="ConfigInstanceProvider.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

namespace RSG.Services.Configuration.ServiceModel
{
    /// <summary>
    /// Instance provider which allows for the injection of the server configuration
    /// object into a newly created WCF service.
    /// </summary>
    public class ConfigInstanceProvider : IInstanceProvider, IContractBehavior
    {
        #region Fields
        /// <summary>
        /// Reference to the server configuration object.
        /// </summary>
        private readonly IServer _server;

        /// <summary>
        /// Type of service we should create.
        /// </summary>
        private readonly Type _serviceType;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ConfigInstanceProvider"/> class
        /// using the specified server configuration object and service type.
        /// </summary>
        /// <param name="server"></param>
        /// <param name="serviceType"></param>
        public ConfigInstanceProvider(IServer server, Type serviceType)
        {
            Debug.Assert(server != null, "Server config parameter is null.");
            if (server == null)
            {
                throw new ArgumentNullException("server");
            }

            _server = server;
            _serviceType = serviceType;
        }
        #endregion

        #region IInstanceProvider Implementation
        /// <summary>
        /// Returns a service object given the specified <see cref="InstanceContext"/>
        /// object.
        /// </summary>
        /// <param name="instanceContext"></param>
        /// <param name="message"></param>
        public object GetInstance(InstanceContext instanceContext, Message message)
        {
            return GetInstance(instanceContext);
        }

        /// <summary>
        /// Creates the new service instance passing in the server config object.
        /// </summary>
        /// <param name="instanceContext"></param>
        public object GetInstance(InstanceContext instanceContext)
        {
            return Activator.CreateInstance(_serviceType, _server);
        }

        /// <summary>
        /// Called when an <see cref="InstanceContext"/> object recycles a service
        /// object.
        /// </summary>
        /// <param name="instanceContext"></param>
        /// <param name="instance"></param>
        public void ReleaseInstance(InstanceContext instanceContext, object instance)
        {
        }
        #endregion

        #region IContractBehavior Implementation
        /// <summary>
        /// Configures any binding elements to support the contract behavior.
        /// </summary>
        /// <param name="contractDescription"></param>
        /// <param name="endpoint"></param>
        /// <param name="bindingParameters"></param>
        public void AddBindingParameters(ContractDescription contractDescription, ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
        }

        /// <summary>
        /// Implements a modification or extension of the client across a contract.
        /// </summary>
        /// <param name="contractDescription"></param>
        /// <param name="endpoint"></param>
        /// <param name="clientRuntime"></param>
        public void ApplyClientBehavior(ContractDescription contractDescription, ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
        }

        /// <summary>
        /// Tells the dispatch runtime that this object is in charge of creating service instances.
        /// </summary>
        /// <param name="contractDescription"></param>
        /// <param name="endpoint"></param>
        /// <param name="dispatchRuntime"></param>
        public void ApplyDispatchBehavior(ContractDescription contractDescription, ServiceEndpoint endpoint, DispatchRuntime dispatchRuntime)
        {
            dispatchRuntime.InstanceProvider = this;
        }

        /// <summary>
        /// Implement to confirm that the contract and endpoint can support the contract
        /// behaviour.
        /// </summary>
        /// <param name="contractDescription"></param>
        /// <param name="endpoint"></param>
        public void Validate(ContractDescription contractDescription, ServiceEndpoint endpoint)
        {
        }
        #endregion
    }
}
