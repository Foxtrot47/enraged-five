﻿//---------------------------------------------------------------------------------------------
// <copyright file="ConfigServiceHostFactory.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Configuration;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Activation;
using RSG.Base.Logging;

namespace RSG.Services.Configuration.ServiceModel
{
    /// <summary>
    /// Custom service host factory that injects the server configuration object into
    /// services created via IIS.
    /// </summary>
    public class ConfigServiceHostFactory<T> : ServiceHostFactory where T : IServer
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="Log"/> property.
        /// </summary>
        private readonly ILog _log;

        /// <summary>
        /// Private field for the <see cref="Server"/> property.
        /// </summary>
        private readonly T _server;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ConfigServiceHostFactory"/>
        /// class using the specified server configuration object.
        /// </summary>
        /// <param name="server"></param>
        protected ConfigServiceHostFactory(T server)
        {
            _log = LogFactory.CreateUniversalLog("ServiceHostFactory");
            _server = server;
        }

        /// <summary>
        /// Initialises the log factory as this is the entry point for services hosted
        /// using IIS.
        /// </summary>
        static ConfigServiceHostFactory()
        {
            LogFactory.Initialize(null, false);
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets a reference to the service host factory's log object.
        /// </summary>
        protected ILog Log
        {
            get { return _log; }
        }

        /// <summary>
        /// Gets a reference to the server configuration object to use when creating
        /// service hosts.
        /// </summary>
        protected T Server
        {
            get { return _server; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Creates a <see cref="ServiceHost"/> for a specified type of service with a
        /// specific base address.
        /// </summary>
        /// <param name="serviceType"></param>
        /// <param name="baseAddresses"></param>
        /// <returns></returns>
        protected override ServiceHost CreateServiceHost(Type serviceType, Uri[] baseAddresses)
        {
            _log.Message("Creating new instance of the '{0}' service for the '{1}' server.", serviceType.Name, _server.Name);
            return new ConfigServiceHost(_server, serviceType);
        }

        /// <summary>
        /// Obtains the server to use from the application settings.
        /// </summary>
        /// <param name="servicesConfig"></param>
        /// <returns></returns>
        protected static T GetServerFromAppSettings(IServicesConfig<T> servicesConfig)
        {
            if (System.Web.HttpRuntime.AppDomainAppId == null)
            {
                Debug.Fail(
                    "Trying to retrieve the default server from the application configuration data while not running under IIS. " +
                    "This method should not be used when self hosting services. Instead make sure you're passing the server to " +
                    "connect to into the custom service host factory manually.");
                throw new NotSupportedException("Using the application config to determine the default server is not supported in non-IIS applications.");
            }

            // Determine the correct server config item to use.
            T server;

            String serverName = ConfigurationManager.AppSettings["server"];
            if (serverName != null)
            {
                server = servicesConfig.GetServerByName(serverName);
            }
            else
            {
                server = servicesConfig.DefaultServer;
            }

            // Make sure the config is set up to use IIS.
            Debug.Assert(server.UsingIIS, "Trying to host a server in IIS that isn't flagged as using IIS.");
            if (!server.UsingIIS)
            {
                throw new ArgumentException("Trying to host a server in IIS that isn't flagged as using IIS.");
            }

            return server;
        }
        #endregion
    }
}
