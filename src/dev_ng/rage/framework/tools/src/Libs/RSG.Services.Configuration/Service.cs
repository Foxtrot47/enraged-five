﻿//---------------------------------------------------------------------------------------------
// <copyright file="Service.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Reflection;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Configuration;

namespace RSG.Services.Configuration
{
    /// <summary>
    /// Service configuration.
    /// </summary>
    public class Service : IService
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="Behaviours"/> property.
        /// </summary>
        private readonly IEnumerable<IServiceBehavior> _serviceBehaviours;

        /// <summary>
        /// Private field for the <see cref="ContractDescription"/> property.
        /// </summary>
        private readonly ContractDescription _contractDescription;

        /// <summary>
        /// Private field for the <see cref="ContractType"/> property.
        /// </summary>
        private readonly Type _contractType;

        /// <summary>
        /// Private field for the <see cref="Endpoints"/> property.
        /// </summary>
        private readonly IList<ServiceEndpoint> _endpoints;

        /// <summary>
        /// Private field for the <see cref="Name"/> property.
        /// </summary>
        private readonly string _name;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="Service"/> class using the
        /// specified xml element, environment and wcf configuration objects.
        /// </summary>
        /// <param name="serviceElement"></param>
        /// <param name="server"></param>
        /// <param name="servicesConfig"></param>
        public Service(
            XElement serviceElement,
            IServer server,
            IServicesConfig<IServer> servicesConfig)
        {
            _name = serviceElement.Element("Name").Value;
            string contractName = serviceElement.Element("Contract").Value;
            string assemblyName = serviceElement.Element("Assembly").Value;

            string typeAsString = String.Format("{0},{1}", contractName, assemblyName);
            _contractType = Type.GetType(typeAsString);
            System.Diagnostics.Debug.Assert(_contractType != null, string.Format(" Cannot find type {0}.", typeAsString));
            _contractDescription = ContractDescription.GetContract(_contractType);
            
            // Determine which service behaviour configuration to use.
            string serviceBehaviourConfiguration = "";
            XElement behaviourConfigurationElement = serviceElement.Element("BehaviourConfiguration");
            if (behaviourConfigurationElement != null)
            {
                serviceBehaviourConfiguration = behaviourConfigurationElement.Value;
            }
            _serviceBehaviours = servicesConfig.GetServiceBehaviours(serviceBehaviourConfiguration);

            // Parse all the endpoints.
            IEnvironment serviceEnvironment = server.Environment;
            serviceEnvironment.Push();
            serviceEnvironment.Add("service_name", _name);

            _endpoints = new List<ServiceEndpoint>();
            foreach (XElement endpointElement in serviceElement.XPathSelectElements("Endpoints/Endpoint"))
            {
                _endpoints.Add(CreateServiceEndpoint(endpointElement, serviceEnvironment, servicesConfig));
            }

            serviceEnvironment.Pop();
        }
        #endregion

        #region Properties
        /// <summary>
        /// Behaviors that this service makes use of.
        /// </summary>
        public IEnumerable<IServiceBehavior> ServiceBehaviours
        {
            get { return _serviceBehaviours; }
        }

        /// <summary>
        /// Service contract type that this service implements.
        /// </summary>
        public Type ContractType
        {
            get { return _contractType; }
        }

        /// <summary>
        /// Endpoints that this service exposes.
        /// </summary>
        public IEnumerable<ServiceEndpoint> Endpoints
        {
            get { return _endpoints; }
        }

        /// <summary>
        /// Name of this service.
        /// </summary>
        public string Name
        {
            get { return _name; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Creates a service endpoint based off an XElement object.
        /// </summary>
        /// <param name="endpointElement"></param>
        /// <param name="servicesEnvironment"></param>
        /// <param name="servicesConfig"></param>
        /// <returns></returns>
        private ServiceEndpoint CreateServiceEndpoint(
            XElement endpointElement,
            IEnvironment servicesEnvironment,
            IServicesConfig<IServer> servicesConfig)
        {
            // Resolve the address.
            XElement addressElement = endpointElement.Element("Address");
            string address = servicesEnvironment.Subst(addressElement.Value);
            
            // Get the binding to use.
            XElement bindingElement = endpointElement.Element("Binding");
            string bindingConfiguration = "";
            XElement bindingConfigurationElement = endpointElement.Element("BindingConfiguration");
            if (bindingConfigurationElement != null)
            {
                bindingConfiguration = bindingConfigurationElement.Value;
            }
            Binding binding = servicesConfig.GetBinding(bindingElement.Value, bindingConfiguration);

            // Get any endpoint behaviours to apply.
            string endpointBehaviourConfiguration = "";
            XElement endpointBehaviourElement = endpointElement.Element("BehaviourConfiguration");
            if (endpointBehaviourElement != null)
            {
                endpointBehaviourConfiguration = endpointBehaviourElement.Value;
            }
            IEnumerable<IEndpointBehavior> endpointBehaviours = servicesConfig.GetEndpointBehaviours(endpointBehaviourConfiguration);

            // Create the endpoint and apply the endpoint behaviours.
            EndpointAddress endpointAddress = new EndpointAddress(address);
            ServiceEndpoint serviceEndpoint = new ServiceEndpoint(_contractDescription, binding, endpointAddress);

            foreach (IEndpointBehavior endpointBehaviour in endpointBehaviours)
            {
                serviceEndpoint.EndpointBehaviors.Add(endpointBehaviour);
            }

            return serviceEndpoint;
        }

        /// <summary>
        /// Retrieve the first endpoint that implements the supplied uri scheme.
        /// </summary>
        /// <param name="uriScheme"></param>
        /// <returns></returns>
        public ServiceEndpoint GetEndpoint(string uriScheme)
        {
            foreach (ServiceEndpoint serviceEndpoint in _endpoints)
            {
                if (serviceEndpoint.ListenUri.Scheme == uriScheme)
                {
                    return serviceEndpoint;
                }
            }

            throw new ArgumentException("uriScheme not found");
        }
        #endregion
    }
}
