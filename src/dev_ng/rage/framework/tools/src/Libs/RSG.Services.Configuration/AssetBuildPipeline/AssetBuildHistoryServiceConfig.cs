﻿//---------------------------------------------------------------------------------------------
// <copyright file="AssetBuildHistoryServiceConfig.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Services.Configuration.AssetBuildPipeline
{
    using System;
    using System.IO;
    using System.Xml.Linq;
    using RSG.Configuration;

    /// <summary>
    /// Asset Build Pipeline Config.
    /// </summary>
    public class AssetBuildHistoryServiceConfig : ServicesConfig<IDatabaseServer>
    {
        #region Constants
        /// <summary>
        /// Name of the configuration file.
        /// </summary>
        private const String _configFilename = @"services\AssetBuildHistory.xml";
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the statistics configuration from the specified file.
        /// </summary>
        /// <param name="filepath"></param>
        public AssetBuildHistoryServiceConfig(String filepath)
            : base(filepath)
        {
        }

        /// <summary>
        /// Initialises a new instance of the statistics configuration for the specified
        /// project.
        /// </summary>
        /// <param name="filepath"></param>
        public AssetBuildHistoryServiceConfig(ToolsConfig config)
            : this(Path.Combine(config.ToolsConfigDirectory, _configFilename))
        {
        }

        /// <summary>
        /// Initialises a new instance of the statistics configuration for the specified
        /// project.
        /// </summary>
        /// <param name="config"></param>
        public AssetBuildHistoryServiceConfig(IConfig config)
            : this(Path.Combine(config.ToolsConfigDirectory, _configFilename))
        {
        }
        #endregion // Constructor(s)

        #region Methods
        /// <summary>
        /// Creates a new server instance from server and services xml elements.
        /// </summary>
        /// <param name="serverElement"></param>
        /// <param name="servicesElement"></param>
        /// <returns></returns>
        protected override IDatabaseServer CreateServerInstance(XElement serverElement,
            XElement servicesElement)
        {
            return new DatabaseServer(serverElement, servicesElement, this);
        }
        #endregion // Methods
    }

} // RSG.Services.Configuration.AssetBuildPipeline namespace
