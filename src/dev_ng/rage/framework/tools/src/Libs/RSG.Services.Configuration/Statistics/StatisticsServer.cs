﻿//---------------------------------------------------------------------------------------------
// <copyright file="StatisticsServer.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System.Xml.Linq;

namespace RSG.Services.Configuration.Statistics
{
    /// <summary>
    /// Configuration information for a single statistics server.
    /// </summary>
    public class StatisticsServer : DatabaseServer, IStatisticsServer
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="Server"/> class using the
        /// provided server and services xml elements along with the pre-created binding,
        /// endpoint behaviours and service behaviours.
        /// </summary>
        /// <param name="serverElement"></param>
        /// <param name="servicesElement"></param>
        /// <param name="statisticsConfig"></param>
        public StatisticsServer(
            XElement serverElement,
            XElement servicesElement,
            StatisticsConfig servicesConfig)
            : base(serverElement, servicesElement, servicesConfig)
        {
        }
        #endregion
    }
}
