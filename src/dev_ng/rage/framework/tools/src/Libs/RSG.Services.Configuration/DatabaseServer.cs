﻿//---------------------------------------------------------------------------------------------
// <copyright file="DatabaseServer.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System.Xml.Linq;

namespace RSG.Services.Configuration
{
    /// <summary>
    /// Configuration information for a single database server.
    /// </summary>
    public class DatabaseServer : Server, IDatabaseServer
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="CommandTimeout"/> property.
        /// </summary>
        private readonly uint _commandTimeout;

        /// <summary>
        /// Private field for the <see cref="ConnectionString"/> property.
        /// </summary>
        private readonly string _connectionString;

        /// <summary>
        /// Private field for the <see cref="DebugMode"/> property.
        /// </summary>
        private readonly bool _debugMode;

        /// <summary>
        /// Private field for the <see cref="ReadOnly"/> property.
        /// </summary>
        private readonly bool _readOnly;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="Server"/> class using the
        /// provided server and services xml elements along with the pre-created binding,
        /// endpoint behaviours and service behaviours.
        /// </summary>
        /// <param name="serverElement"></param>
        /// <param name="servicesElement"></param>
        /// <param name="statisticsConfig"></param>
        public DatabaseServer(
            XElement serverElement,
            XElement servicesElement,
            IServicesConfig<IDatabaseServer> servicesConfig)
            : base(serverElement, servicesElement, servicesConfig)
        {
            // Read in the database configuration data.
            _commandTimeout = GetSubElementValue<uint>(serverElement, "CommandTimeout");
            _connectionString = GetSubElementValue<string>(serverElement, "ConnectionString");
            _debugMode = GetSubElementValue<bool>(serverElement, "DebugMode");
            _readOnly = GetSubElementValue<bool>(serverElement, "ReadOnly");
        }
        #endregion

        #region Properties
        /// <summary>
        /// How long SQL commands can run on the db before timing out.  Set to 0 for an infinite timeout.
        /// </summary>
        public uint CommandTimeout
        {
            get { return _commandTimeout; }
        }

        /// <summary>
        /// Database connection string.
        /// </summary>
        public string ConnectionString
        {
            get { return _connectionString; }
        }

        /// <summary>
        /// Gets a value indicating whether we wish to run the server in debug mode.
        /// Debug mode enable additional server/query logging.
        /// </summary>
        public bool DebugMode
        {
            get { return _debugMode; }
        }

        /// <summary>
        /// Gets a flag indicating whether we should only allow read operations against the db.
        /// </summary>
        public bool ReadOnly
        {
            get { return _readOnly; }
        }
        #endregion
    }
}
