﻿//---------------------------------------------------------------------------------------------
// <copyright file="BehaviourExtensions.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Reflection;
using System.ServiceModel.Configuration;

namespace RSG.Services.Configuration.ServiceModel
{
    /// <summary>
    /// Extension methods for behaviour and related classes.
    /// </summary>
    public static class BehaviourExtensions
    {
        /// <summary>
        /// Uses reflection to create a behaviour from a configuration element.
        /// </summary>
        /// <param name="extensionElement"></param>
        /// <returns></returns>
        /// <remarks>
        /// This relies on using reflection to get at the protected/internal
        /// CreateBehavior method.  This is an unfortunate necessity, but has been
        /// guarded against as best I can to make sure we verify the behaviour when the
        /// assembly gets upgraded to newer .net framework versions in the future.
        /// </remarks>
        public static object CreateBehavior(this BehaviorExtensionElement extensionElement)
        {
#if NETFX4_5
            Type type = extensionElement.GetType();
            MethodInfo methodInfo = type.GetMethod(
                "CreateBehavior",
                BindingFlags.Instance | BindingFlags.NonPublic);

            return methodInfo.Invoke(extensionElement, new object[0] { });
#else
#error Make sure the above code works (and is still needed) prior to updating the #if check to the appropriate framework version.
#endif
        }
    }
}
