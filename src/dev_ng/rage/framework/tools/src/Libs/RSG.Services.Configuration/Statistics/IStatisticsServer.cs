﻿//---------------------------------------------------------------------------------------------
// <copyright file="IStatisticsServer.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Services.Configuration.Statistics
{
    /// <summary>
    /// Configuration information for a single satistics server.
    /// </summary>
    public interface IStatisticsServer : IDatabaseServer
    {
    }
}
