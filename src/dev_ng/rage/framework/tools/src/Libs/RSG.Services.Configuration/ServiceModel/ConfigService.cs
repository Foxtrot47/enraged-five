﻿//---------------------------------------------------------------------------------------------
// <copyright file="ConfigClient.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using RSG.Base.Logging;

namespace RSG.Services.Configuration
{
    /// <summary>
    /// Base class for WCF services that exposes the server configuration element to the
    /// service.
    /// </summary>
    public abstract class ConfigService
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="Log"/> property.
        /// </summary>
        private readonly ILog _log;

        /// <summary>
        /// Private field for the <see cref="Server"/> property.
        /// </summary>
        private readonly IServer _server;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ServiceBase"/> class using the
        /// specified server configuration.
        /// </summary>
        /// <param name="server"></param>
        protected ConfigService(IServer server)
        {
            _log = LogFactory.CreateUniversalLog(GetType().FullName);
            _server = server;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Service level log object.
        /// </summary>
        protected ILog Log
        {
            get { return _log; }
        }

        /// <summary>
        /// Server configuration element that this service is using.
        /// </summary>
        public IServer Server
        {
            get { return _server; }
        }
        #endregion
    }
}
