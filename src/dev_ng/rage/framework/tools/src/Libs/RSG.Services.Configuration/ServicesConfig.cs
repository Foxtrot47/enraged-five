﻿//---------------------------------------------------------------------------------------------
// <copyright file="ServicesConfig.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel.Channels;
using System.ServiceModel.Configuration;
using System.ServiceModel.Description;
using System.Web;
using System.Xml.Linq;
using RSG.Services.Configuration.ServiceModel;

namespace RSG.Services.Configuration
{
    /// <summary>
    /// Base configuration for a service based server application.
    /// </summary>
    public abstract class ServicesConfig<T> : IServicesConfig<T> where T : IServer
    {
        #region Fields
        /// <summary>
        /// Binding collections keyed by the binding type.
        /// </summary>
        private readonly IDictionary<string, IList<Binding>> _bindings;

        /// <summary>
        /// Private field for the <see cref="DefaultServer"/> property.
        /// </summary>
        private readonly T _defaultServer;

        /// <summary>
        /// Endpoint behaviour collections keyed by their configuration name.
        /// </summary>
        private readonly IDictionary<string, IList<IEndpointBehavior>> _endpointBehaviours;

        /// <summary>
        /// Private field for the <see cref="Servers"/> property.
        /// </summary>
        private readonly IDictionary<string, T> _servers;

        /// <summary>
        /// Service behaviour collections keyed by their configuration name.
        /// </summary>
        private readonly IDictionary<string, IList<IServiceBehavior>> _serviceBehaviours;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the statistics configuration from the specified file.
        /// </summary>
        /// <param name="filepath"></param>
        public ServicesConfig(string filepath)
        {
            XDocument doc = XDocument.Load(filepath);

            // Parse the bindings and behaviours configurations first.
            _bindings = ParseBindings(doc.Root.Element("bindings"));
            ParseBehaviours(doc.Root.Element("behaviors"), out _endpointBehaviours, out _serviceBehaviours);

            // Parse the servers/services next.
            _servers = new Dictionary<string, T>();

            XElement servicesElement = doc.Root.Element("Services");
            XElement serversElement = doc.Root.Element("Servers");
            foreach (XElement serverElement in serversElement.Elements("Server"))
            {
                IServer server = CreateServerInstance(serverElement, servicesElement);
                _servers.Add(server.Name, (T)server);
            }

            XAttribute defaultServerAttribute = serversElement.Attribute("default");
            _defaultServer = _servers[defaultServerAttribute.Value];
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the default server to connect to.
        /// </summary>
        public T DefaultServer
        {
            get { return _defaultServer; }
        }

        /// <summary>
        /// Gets the list of servers that are available to connect to.
        /// </summary>
        public IEnumerable<T> Servers
        {
            get { return _servers.Values; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Creates a new server instance from server and services xml elements.
        /// </summary>
        /// <param name="serverElement"></param>
        /// <param name="servicesElement"></param>
        /// <returns></returns>
        protected abstract T CreateServerInstance(XElement serverElement, XElement servicesElement);

        /// <summary>
        /// Attempts to retrieve a binding with name <paramref name="configuration"/> of
        /// type <paramref name="type"/>.
        /// </summary>
        /// <param name="bindingType">
        /// Type of binding to retrieve (i.e. netTcpBinding, webHttpBinding, etc...).
        /// </param>
        /// <param name="configuration">
        /// Name of the configuration relating to the requested binding type.
        /// </param>
        /// <returns></returns>
        /// <remarks>
        /// This method throws if either the <paramref name="type"/> or
        /// <paramref name="configuration"/> aren't found in the list of available
        /// bindings.
        /// </remarks>
        public Binding GetBinding(string type, string configuration)
        {
            IList<Binding> bindingsOfRequestedType = _bindings[type];
            return bindingsOfRequestedType.First(item => item.Name == configuration);
        }

        /// <summary>
        /// Attempts to retrieve the list of endpoint behaviours to apply for the
        /// specified configuration.
        /// </summary>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public IEnumerable<IEndpointBehavior> GetEndpointBehaviours(string configuration)
        {
            IList<IEndpointBehavior> behaviours;
            if (!_endpointBehaviours.TryGetValue(configuration, out behaviours))
            {
                if (String.IsNullOrEmpty(configuration))
                {
                    return Enumerable.Empty<IEndpointBehavior>();
                }
                else
                {
                    throw new KeyNotFoundException("configuration");
                }
            }

            return behaviours;
        }

        /// <summary>
        /// Retrieves a server by name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public T GetServerByName(string name)
        {
            T server;
            _servers.TryGetValue(name, out server);
            return server;
        }

        /// <summary>
        /// Attempts to retrieve the list of service behaviours to apply for the
        /// specified configuration.
        /// </summary>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public IEnumerable<IServiceBehavior> GetServiceBehaviours(string configuration)
        {
            IList<IServiceBehavior> behaviours;
            if (!_serviceBehaviours.TryGetValue(configuration, out behaviours))
            {
                if (String.IsNullOrEmpty(configuration))
                {
                    return Enumerable.Empty<IServiceBehavior>();
                }
                else
                {
                    throw new KeyNotFoundException("configuration");
                }
            }

            return behaviours;
        }
        #endregion

        #region Parse Methods
        /// <summary>
        /// Opens the app.config or web.config file based on whether we are self hosting
        /// or hosting under IIS.
        /// </summary>
        /// <returns></returns>
        private System.Configuration.Configuration OpenApplicationConfiguration()
        {
            if (!String.IsNullOrEmpty(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath))
            {
                return System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~");
            }
            else
            {
                return ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            }
        }

        /// <summary>
        /// Creates binding objects from the specified element.
        /// </summary>
        /// <param name="elem"></param>
        /// <returns></returns>
        private IDictionary<string, IList<Binding>> ParseBindings(XElement elem)
        {
            System.Configuration.Configuration configuration = OpenApplicationConfiguration();
            BindingsSection bindingSection = BindingsSection.GetSection(configuration);
            if (bindingSection == null)
            {
                bindingSection = new BindingsSection();
                configuration.Sections.Add("bindings", bindingSection);
            }
            bindingSection.SectionInformation.SetRawXml(elem.ToString());

            // Parse all the bindings.
            IDictionary<string, IList<Binding>> bindings = new Dictionary<string, IList<Binding>>();
            foreach (BindingCollectionElement bindingCollectionElement in bindingSection.BindingCollections)
            {
                IList<Binding> bindingList = new List<Binding>();
                foreach (IBindingConfigurationElement bindingConfigurationElement in bindingCollectionElement.ConfiguredBindings)
                {
                    Binding binding = (Binding)Activator.CreateInstance(bindingCollectionElement.BindingType);
                    bindingConfigurationElement.ApplyConfiguration(binding);
                    binding.Name = bindingConfigurationElement.Name;
                    bindingList.Add(binding);
                }

                if (bindingList.Any())
                {
                    bindings.Add(bindingCollectionElement.BindingName, bindingList);
                }
            }

            return bindings;
        }

        /// <summary>
        /// Creates endpoint and service behaviour objects from the specified element.
        /// </summary>
        /// <param name="elem"></param>
        /// <param name="endpointBehaviours"></param>
        /// <param name="serviceBehaviours"></param>
        private void ParseBehaviours(
            XElement elem,
            out IDictionary<string, IList<IEndpointBehavior>> endpointBehaviours,
            out IDictionary<string, IList<IServiceBehavior>> serviceBehaviours)
        {
            System.Configuration.Configuration configuration = OpenApplicationConfiguration();
            BehaviorsSection behavioursSection = (BehaviorsSection)ConfigurationManager.GetSection("behaviors");
            if (behavioursSection == null)
            {
                behavioursSection = new BehaviorsSection();
                configuration.Sections.Add("behaviors", behavioursSection);
            }
            behavioursSection.SectionInformation.SetRawXml(elem.ToString());

            // Parse the endpoint behaviours first.
            endpointBehaviours = new Dictionary<string, IList<IEndpointBehavior>>();
            foreach (EndpointBehaviorElement endpointBehaviourElement in behavioursSection.EndpointBehaviors)
            {
                IList<IEndpointBehavior> enpointBehaviourList = new List<IEndpointBehavior>();
                foreach (BehaviorExtensionElement behaviorElement in endpointBehaviourElement)
                {
                    IEndpointBehavior endpointBehaviour = (IEndpointBehavior)behaviorElement.CreateBehavior();
                    enpointBehaviourList.Add(endpointBehaviour);
                }
                endpointBehaviours.Add(endpointBehaviourElement.Name, enpointBehaviourList);
            }

            // Parse the service behaviours next.
            serviceBehaviours = new Dictionary<string, IList<IServiceBehavior>>();
            foreach (ServiceBehaviorElement serviceBehaviourElement in behavioursSection.ServiceBehaviors)
            {
                IList<IServiceBehavior> serviceBehaviourList = new List<IServiceBehavior>();
                foreach (BehaviorExtensionElement behaviorElement in serviceBehaviourElement)
                {
                    IServiceBehavior serviceBehaviour = (IServiceBehavior)behaviorElement.CreateBehavior();
                    serviceBehaviourList.Add(serviceBehaviour);
                }
                serviceBehaviours.Add(serviceBehaviourElement.Name, serviceBehaviourList);
            }
        }
        #endregion
    }
}
