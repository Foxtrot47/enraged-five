﻿//---------------------------------------------------------------------------------------------
// <copyright file="ConfigServiceHost.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace RSG.Services.Configuration.ServiceModel
{
    /// <summary>
    /// Custom service host that obtains its configuration data from the server
    /// configuration data.
    /// </summary>
    public class ConfigServiceHost : ServiceHost
    {
        #region Fields
        /// <summary>
        /// Reference to the server configuration.
        /// </summary>
        private readonly IServer _server;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ConfigServiceHost"/> class using
        /// the specified server configuration and service singleton instance.
        /// </summary>
        /// <param name="server"></param>
        /// <param name="singletonInstance"></param>
        public ConfigServiceHost(IServer server, object singletonInstance)
            : base(singletonInstance)
        {
            _server = server;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ConfigServiceHost"/> class using
        /// the specified server configuration and service type.
        /// </summary>
        /// <param name="server"></param>
        /// <param name="serviceType"></param>
        public ConfigServiceHost(IServer server, Type serviceType)
            : base(serviceType)
        {
            _server = server;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Invoked during the transition of a communication object into the opening
        /// state.
        /// </summary>
        protected override void OnOpening()
        {
            // Determine which services we need to add endpoints for.
            foreach (ContractDescription contractDescription in ImplementedContracts.Values)
            {
                IService serviceConfig = _server.GetService(contractDescription.ContractType);
                if (serviceConfig == null)
                {
                    ConfigurationErrorsException exception = new ConfigurationErrorsException("Missing service configuration.");
                    exception.Data.Add("ContractType", contractDescription.ContractType);
                    throw exception;
                }

                // Add the config instance provider behaviour.
                // Note: I think we only need to add it to one of the endpoint contracts
                // behaviours but adding it to the implemented contracts as well just in
                // case. This is partly because the service endpoints have a different
                // ContractDescription instance to the service host.
                contractDescription.Behaviors.Add(new ConfigInstanceProvider(_server, Description.ServiceType));

                // Add all the service endpoints.
                foreach (ServiceEndpoint endpoint in serviceConfig.Endpoints)
                {
                    AddServiceEndpoint(endpoint);

                    IContractBehavior existingBehaviour = endpoint.Contract.Behaviors.Find<ConfigInstanceProvider>();
                    if (existingBehaviour == null)
                    {
                        endpoint.Contract.Behaviors.Add(new ConfigInstanceProvider(_server, Description.ServiceType));
                    }
                }

                // Configure the service behaviours.
                foreach (IServiceBehavior serviceBehaviour in serviceConfig.ServiceBehaviours)
                {
                    IServiceBehavior existingBehaviour = Description.Behaviors.FirstOrDefault(item => item.GetType() == serviceBehaviour.GetType());
                    if (existingBehaviour != null)
                    {
                        Description.Behaviors.Remove(existingBehaviour);
                    }

                    Description.Behaviors.Add(serviceBehaviour);
                }
            }

            // Continue with the opening of the service.
            base.OnOpening();
        }
        #endregion
    }
}
