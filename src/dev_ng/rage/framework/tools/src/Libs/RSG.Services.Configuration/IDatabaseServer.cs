﻿//---------------------------------------------------------------------------------------------
// <copyright file="IDatabaseServer.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Services.Configuration
{
    /// <summary>
    /// Augments the server configuration interface with database connection information.
    /// </summary>
    public interface IDatabaseServer : IServer
    {
        /// <summary>
        /// How long SQL commands can run on the db before timing out.  Set to 0 for an
        /// infinite timeout.
        /// </summary>
        uint CommandTimeout { get; }

        /// <summary>
        /// Database connection string.
        /// </summary>
        string ConnectionString { get; }

        /// <summary>
        /// Gets a value indicating whether we wish to run the server in debug mode.
        /// Debug mode enable additional server/query logging.
        /// </summary>
        bool DebugMode { get; }

        /// <summary>
        /// Gets a flag indicating whether we should only allow read operations against
        /// the db.
        /// </summary>
        bool ReadOnly { get; }
    }
}
