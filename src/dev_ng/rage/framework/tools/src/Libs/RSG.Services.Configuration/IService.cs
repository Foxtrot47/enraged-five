﻿//---------------------------------------------------------------------------------------------
// <copyright file="IService.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ServiceModel.Description;

namespace RSG.Services.Configuration
{
    /// <summary>
    /// Configuration information about a single service.
    /// </summary>
    public interface IService
    {
        #region Properties
        /// <summary>
        /// Service contract type that this service implements.
        /// </summary>
        Type ContractType { get; }

        /// <summary>
        /// Endpoints that this service exposes.
        /// </summary>
        IEnumerable<ServiceEndpoint> Endpoints { get; }

        /// <summary>
        /// Name of this service.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Behaviors that this service makes use of.
        /// </summary>
        IEnumerable<IServiceBehavior> ServiceBehaviours { get; }
        #endregion

        #region Methods
        /// <summary>
        /// Retrieve the first endpoint that implements the supplied uri scheme.
        /// </summary>
        /// <param name="uriScheme"></param>
        /// <returns></returns>
        ServiceEndpoint GetEndpoint(string uriScheme);
        #endregion
    }
}
