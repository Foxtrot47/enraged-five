﻿//---------------------------------------------------------------------------------------------
// <copyright file="AssetPipelineService.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Services.Configuration.GameConnection
{
    using System.IO;
    using System.Xml.Linq;
    using RSG.Configuration;

    /// <summary>
    /// Game Connection services configuration class.
    /// </summary>
    public class GameConnectionConfig : ServicesConfig<IServer>
    {
        #region Constants
        /// <summary>
        /// Name of the Game Connection configuration file.
        /// </summary>
        private const string _configFilename = @"GameConnection.xml";
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the LiveEdit configuration from the specified file.
        /// </summary>
        /// <param name="filepath"></param>
        public GameConnectionConfig(string filepath)
            : base(filepath)
        {
        }

        /// <summary>
        /// Initialises a new instance of the LiveEdit configuration for the specified
        /// project.
        /// </summary>
        /// <param name="filepath"></param>
        public GameConnectionConfig(ToolsConfig config)
            : this(Path.Combine(config.ToolsConfigDirectory, "services", _configFilename))
        {
        }

        /// <summary>
        /// Initialises a new instance of the LiveEdit configuration for the specified
        /// project.
        /// </summary>
        /// <param name="config"></param>
        public GameConnectionConfig(IConfig config)
            : this(Path.Combine(config.ToolsConfigDirectory, "services", _configFilename))
        {
        }
        #endregion // Constructor(s)

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="serverElement"></param>
        /// <param name="servicesElement"></param>
        /// <returns></returns>
        protected override IServer CreateServerInstance(XElement serverElement, 
            XElement servicesElement)
        {
            return (new Server(serverElement, servicesElement, this));
        }
        #endregion // Methods
    }

} // RSG.Services.Configuration.GameConnection namespace
