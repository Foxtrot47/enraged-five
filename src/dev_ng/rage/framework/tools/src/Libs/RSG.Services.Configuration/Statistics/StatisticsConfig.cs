﻿//---------------------------------------------------------------------------------------------
// <copyright file="StatisticsConfig.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System.IO;
using System.Xml.Linq;
using RSG.Configuration;

namespace RSG.Services.Configuration.Statistics
{
    /// <summary>
    /// Statistics configuration.
    /// </summary>
    public class StatisticsConfig : ServicesConfig<IStatisticsServer>
    {
        #region Constants
        /// <summary>
        /// Name of the statistics configuration file.
        /// </summary>
        private const string _configFilename = @"statistics.xml";
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the statistics configuration from the specified file.
        /// </summary>
        /// <param name="filepath"></param>
        public StatisticsConfig(string filepath)
            : base(filepath)
        {
        }

        /// <summary>
        /// Initialises a new instance of the statistics configuration for the specified
        /// project.
        /// </summary>
        /// <param name="filepath"></param>
        public StatisticsConfig(ToolsConfig config)
            : this(Path.Combine(config.ToolsConfigDirectory, "services", _configFilename))
        {
        }

        /// <summary>
        /// Initialises a new instance of the statistics configuration for the specified
        /// project.
        /// </summary>
        /// <param name="config"></param>
        public StatisticsConfig(IConfig config)
            : this(Path.Combine(config.ToolsConfigDirectory, "services", _configFilename))
        {
        }
        #endregion

        #region Methods
        /// <summary>
        /// Creates a new server instance from server and services xml elements.
        /// </summary>
        /// <param name="serverElement"></param>
        /// <param name="servicesElement"></param>
        /// <returns></returns>
        protected override IStatisticsServer CreateServerInstance(XElement serverElement, XElement servicesElement)
        {
            return new StatisticsServer(serverElement, servicesElement, this);
        }
        #endregion
    }
}
