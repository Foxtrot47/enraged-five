﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Editor;

namespace RSG.ShortcutMenu.Tasks.ViewModels
{
    /// <summary>
    /// View model for the <see cref="View.InfoDisplayWindow"/>.
    /// </summary>
    public class InfoDisplayViewModel : NotifyPropertyChangedBase
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="Lines"/> property.
        /// </summary>
        private readonly ObservableCollection<String> _lines = new ObservableCollection<String>();
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="InfoDisplayViewModel"/> class.
        /// </summary>
        public InfoDisplayViewModel()
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// List of text lines to display.
        /// </summary>
        public IEnumerable<String> Lines
        {
            get { return _lines; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Adds a line of text to the info display view.
        /// </summary>
        /// <param name="line"></param>
        public void AddLine(String line)
        {
            _lines.Add(line);
        }
        #endregion
    }
}
