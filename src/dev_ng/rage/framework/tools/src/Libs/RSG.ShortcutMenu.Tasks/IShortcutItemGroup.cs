﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.ShortcutMenu.Tasks
{
    /// <summary>
    /// 
    /// </summary>
    public interface IShortcutItemGroup
    {
        #region Properties
        /// <summary>
        /// Child items that this group contains.
        /// </summary>
        IReadOnlyCollection<IShortcutItem> Items { get; }
        #endregion // Properties
    } // IShortcutItemGroup

}
