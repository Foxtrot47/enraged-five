﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml.Linq;
using RSG.Base.Configuration;
using RSG.Editor;
using RSG.Rag.Clients;
using RSG.Rag.Contracts.Data;
using RSG.ShortcutMenu.Tasks.View;

namespace RSG.ShortcutMenu.Tasks
{
    /// <summary>
    /// Shortcut item task that executes a particular 
    /// </summary>
    public class ExecutableTask : ShortcutItemBase, IShortcutItemTask
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="Command"/> property.
        /// </summary>
        private readonly String _command;

        /// <summary>
        /// Private field for the <see cref="Arguments"/> property.
        /// </summary>
        private readonly String _arguments;

        /// <summary>
        /// Private field for the <see cref="UseUniversalLog"/> property.
        /// </summary>
        private readonly bool _useUniversalLog;

        /// <summary>
        /// Private field for the <see cref="CheckForRunningGame"/> property.
        /// </summary>
        private readonly bool _checkForRunningGame;

        /// <summary>
        /// Private field for the <see cref="RunAsStandalone"/> property.
        /// </summary>
        private readonly bool _runAsStandalone;

        /// <summary>
        /// HACK: Reference to the most recently passed in task invoke args.
        /// This is so that we can make use of the args in the event handler
        /// that gets invoked when attached taskshave compelted running.
        /// </summary>
        protected TaskInvokeArgs _taskArgs;
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="ExecutableTask"/> class
        /// based on an xml element.
        /// </summary>
        /// <param name="elem"></param>
        public ExecutableTask(XElement elem)
            : base(elem)
        {
            _command = GetAttributeValue<String>(elem, "command");
            _arguments = GetAttributeValue<String>(elem, "args", false);
            _useUniversalLog = GetAttributeValue<bool>(elem, "use_ulog", false);
            _checkForRunningGame = GetAttributeValue<bool>(elem, "check_game", false);
            _runAsStandalone = GetAttributeValue<bool>(elem, "standalone", false, true);
        }
        #endregion

        #region Properties
        /// <summary>
        /// Command that will be executed when this item is invoked.
        /// </summary>
        public String Command
        {
            get { return _command; }
        }

        /// <summary>
        /// Arguments to supply the command when this item is invoked.
        /// </summary>
        public String Arguments
        {
            get { return _arguments; }
        }

        /// <summary>
        /// Flag indicating whether the output from the task should be output
        /// to an universal log file that is shown when the task is completed.
        /// </summary>
        public bool UseUniversalLog
        {
            get { return _useUniversalLog; }
        }

        /// <summary>
        /// Flag to indicate that the game needs to be killed prior to executing this task.
        /// </summary>
        public bool CheckForRunningGame
        {
            get { return _checkForRunningGame; }
        }

        /// <summary>
        /// Flag indicating that the application will run as a standalone
        /// application.
        /// </summary>
        public bool RunAsStandalone
        {
            get { return _runAsStandalone; }
        }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Kicks off the task.
        /// </summary>
        /// <param name="args"></param>
        public void Invoke(TaskInvokeArgs args)
        {
            IMessageBoxService msgService = args.ServiceProvider.GetService<IMessageBoxService>();
            if (msgService == null)
            {
                throw new ArgumentNullException("IMessageBoxService");
            }

            _taskArgs = args;
            if (CheckForRunningGame)
            {
                //TODO: Should we change the call to check for running rag games to an async operation?
                if (!RunTaskAfterRunningGameCheck(args, msgService))
                {
                    return;
                }
            }

            if (RunAsStandalone)
            {
                RunAsStandaloneTask(args, msgService);
            }
            else
            {
                RunAsAttachedTask(args, msgService);
            }
        }

        /// <summary>
        /// Checks whether the game is running, prompting the user whether they wish to continue if it is.
        /// </summary>
        /// <param name="args"></param>
        /// <param name="msgService"></param>
        /// <returns>Whether it's ok to run execute the task.</returns>
        private bool RunTaskAfterRunningGameCheck(TaskInvokeArgs args, IMessageBoxService msgService)
        {
            bool runProcess = true;

            try
            {
                IEnumerable<GameConnection> connections = RagClientFactory.GetGameConnections();

                if (connections.Any())
                {
                    MessageBoxResult result =
                        msgService.Show(
                            "You are running the game while starting a task that will shutdown the game! Do you still want to launch the new process?",
                            "Game is Currently Running!",
                            MessageBoxButton.YesNo);

                    if (result == MessageBoxResult.No)
                    {
                        runProcess = false;
                    }
                }
            }
            catch (EndpointNotFoundException)
            {
                args.Log.Warning("Endpoint not found while checking for running games.  This usually indicates that RAG wasn't running.");
            }
            catch (Exception ex)
            {
                args.Log.ToolException(ex, "Exception while querying RAG proxy.");
            }
            return runProcess;
        }

        /// <summary>
        /// Runs a task as a standalone process.
        /// </summary>
        /// <param name="args"></param>
        /// <param name="msgService"></param>
        /// <returns>Whether the task was successfully started.</returns>
        private bool RunAsStandaloneTask(TaskInvokeArgs args, IMessageBoxService msgService)
        {
            Process process = new Process();
            process.StartInfo.FileName = args.Branch.Environment.Subst(Command);
            process.StartInfo.Arguments = args.Branch.Environment.Subst(Arguments);
            process.StartInfo.WorkingDirectory = Path.GetDirectoryName(Command);

            try
            {
                process.Start();
                return true;
            }
            catch (System.Exception ex)
            {
                String taskName = args.Branch.Environment.Subst(Name);
                args.Log.ToolException(ex, "Unable to start the '{0}' task.", taskName);

                String message = String.Format("An error occurred while trying to start the '{0}' task.  Please check the configuration for this item.", taskName);
                msgService.Show(message, null, MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
        }

        /// <summary>
        /// Runs a task as an attached process displaying the output in an info display window.
        /// </summary>
        /// <param name="args"></param>
        /// <param name="msgService"></param>
        /// <returns>Whether the task was successfully started.</returns>
        private bool RunAsAttachedTask(TaskInvokeArgs args, IMessageBoxService msgService)
        {
            String command = args.Branch.Environment.Subst(Command);
            if (!File.Exists(command))
            {
                // Display a message to the user.
                String taskName = args.Branch.Environment.Subst(Name);
                args.Log.Error("Unable to start the '{0}' task because it doesn't exist on disk.", taskName);

                String message = String.Format("Unable to run the '{0}' task because the executable file doesn't exist on disk.  Please check the configuration for this item.", taskName);
                msgService.Show(message, null, MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }

            InfoDisplayWindow window = new InfoDisplayWindow(this, args.Branch.Environment);
            window.Closed += AttachedTask_Completed;
            window.Show();
            return true;
        }

        /// <summary>
        /// Event handler for when an attached task has finished running.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void AttachedTask_Completed(object sender, EventArgs args)
        {
            InfoDisplayWindow window = sender as InfoDisplayWindow;
            if (window != null)
            {
                OnAttachedTaskCompleted(_taskArgs, !window.ProcessKilled);
            }
        }

        /// <summary>
        /// Method that gets called when an attached task has stopped running.
        /// </summary>
        /// <param name="args"></param>
        /// <param name="ranToCompletion"></param>
        protected virtual void OnAttachedTaskCompleted(TaskInvokeArgs args, bool ranToCompletion)
        {
        }
        #endregion
    }
}
