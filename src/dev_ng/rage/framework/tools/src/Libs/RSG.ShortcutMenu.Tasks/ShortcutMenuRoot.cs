﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using RSG.Base.Logging;
using RSG.Base.Xml;

namespace RSG.ShortcutMenu.Tasks
{
    /// <summary>
    /// 
    /// </summary>
    public class ShortcutMenuRoot : ItemGroup
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="ShortcutMenuRoot"/> class
        /// based on an xml element.
        /// </summary>
        /// <param name="elem"></param>
        /// <param name="factory"></param>
        private ShortcutMenuRoot(XElement elem, ShortcutItemFactory factory)
            : base(elem, factory)
        {
        }
        #endregion // Constructor(s)

        #region Static Creation
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="factory"></param>
        /// <param name="log"></param>
        /// <returns></returns>
        public static ShortcutMenuRoot Load(String filename, ShortcutItemFactory factory, ILog log)
        {
            using (Mvp.Xml.XInclude.XIncludingReader xmlReader = new Mvp.Xml.XInclude.XIncludingReader(filename))
            {
                XDocument doc = XDocument.Load(xmlReader);
                return new ShortcutMenuRoot(doc.Root, factory);
            }
        }
        #endregion // Static Creation
    } // ShortcutMenuRoot
}
