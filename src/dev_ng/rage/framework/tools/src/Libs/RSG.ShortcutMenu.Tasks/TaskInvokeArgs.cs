﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Editor;

namespace RSG.ShortcutMenu.Tasks
{
    /// <summary>
    /// Arguments to provide a task when it's being invoked.
    /// </summary>
    public class TaskInvokeArgs
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="ServiceProvider"/> property.
        /// </summary>
        private readonly ICommandServiceProvider _serviceProvider;

        /// <summary>
        /// Private field for the <see cref="Branch"/> property.
        /// </summary>
        private readonly IBranch _branch;

        /// <summary>
        /// Private field for the <see cref="Log"/> property.
        /// </summary>
        private readonly ILog _log;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="TaskInvokeArgs"/> class using the
        /// provided parameters.
        /// </summary>
        /// <param name="serviceProvider"></param>
        /// <param name="branch"></param>
        /// <param name="log"></param>
        public TaskInvokeArgs(ICommandServiceProvider serviceProvider, IBranch branch, ILog log)
        {
            _serviceProvider = serviceProvider;
            _branch = branch;
            _log = log;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Reference to the service provider object.
        /// </summary>
        public ICommandServiceProvider ServiceProvider
        {
            get { return _serviceProvider; }
        }

        /// <summary>
        /// Reference to the branch the tool is currently running with.
        /// </summary>
        public IBranch Branch
        {
            get { return _branch; }
        }

        /// <summary>
        /// Reference to a log object.
        /// </summary>
        public ILog Log
        {
            get { return _log; }
        }
        #endregion
    }
}
