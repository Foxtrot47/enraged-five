﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.Configuration;

namespace RSG.ShortcutMenu.Tasks
{
    /// <summary>
    /// Shortcut item that implements a particular task (e.g. invoking an application,
    /// or syncing a build).
    /// </summary>
    public interface IShortcutItemTask : IShortcutItem
    {
        #region Methods
        /// <summary>
        /// Kicks off the task.
        /// </summary>
        /// <param name="args"></param>
        void Invoke(TaskInvokeArgs args);
        #endregion
    }
}
