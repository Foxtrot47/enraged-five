﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.ShortcutMenu.Tasks.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public static class ShortcutMenuCommandIds
    {
        #region Fields
        /// <summary>
        /// The private instance used for the <see cref="ContextMenu"/> property.
        /// </summary>
        private static Guid? _contextMenu;

        /// <summary>
        /// A private instance to a generic object used to support thread access.
        /// </summary>
        private static object _syncRoot = new object();
        #endregion

        #region Properties
        /// <summary>
        /// Gets the global identifier used for the context menu for the individual items in
        /// the InfoDisplayWindow list.
        /// </summary>
        public static Guid ContextMenu
        {
            get
            {
                if (!_contextMenu.HasValue)
                {
                    lock (_syncRoot)
                    {
                        if (!_contextMenu.HasValue)
                        {
                            _contextMenu = new Guid("F6B85017-B3EE-4314-B1B4-BEAF7067C7B8");
                        }
                    }
                }

                return _contextMenu.Value;
            }
        }
        #endregion
    }
}
