﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Editor;
using RSG.Editor.Controls;
using RSG.Editor.SharedCommands;
using RSG.ShortcutMenu.Tasks;
using RSG.ShortcutMenu.Tasks.Commands;
using RSG.ShortcutMenu.Tasks.ViewModels;

namespace RSG.ShortcutMenu.Tasks.View
{
    /// <summary>
    /// Interaction logic for InfoDisplayWindow.xaml
    /// </summary>
    public partial class InfoDisplayWindow : RsWindow
    {
        #region Constants
        /// <summary>
        /// Location of the universal log viewer executable.
        /// </summary>
        private const String _universalLogViewerExe = @"$(toolsbin)\UniversalLogViewer\UniversalLogViewer.exe";
        #endregion

        #region Fields
        /// <summary>
        /// Task to execute.
        /// </summary>
        private readonly ExecutableTask _task;

        /// <summary>
        /// Environment for resolving task parameters.
        /// </summary>
        private readonly IEnvironment _environment;

        /// <summary>
        /// The process that is running.
        /// </summary>
        private Process _process;

        /// <summary>
        /// Flag indicating whether the process was killed before running to completion.
        /// </summary>
        private bool _processKilled;

        /// <summary>
        /// Universal log object for the process.
        /// </summary>
        private readonly IUniversalLog _log;

        /// <summary>
        /// Log file target.
        /// TODO: Change this to an ILogTarget once the relevant Flush code has made it's way over from V.
        /// </summary>
        private readonly UniversalLogFile _target;

        /// <summary>
        /// Path of the log file.
        /// </summary>
        private readonly String _logFilename;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="InfoDisplayWindow"/> class.
        /// </summary>
        /// <param name="task"></param>
        /// <param name="environment"></param>
        public InfoDisplayWindow(ExecutableTask task, IEnvironment environment)
        {
            InitializeComponent();
            DataContext = new InfoDisplayViewModel();

            Loaded += InfoDisplayWindow_Loaded;

            _task = task;
            _environment = environment;

            if (_task.UseUniversalLog)
            {
                // TODO: Get the log to output to the tools log directory.
                _logFilename = Path.GetTempFileName();
                if (File.Exists(_logFilename))
                {
                    try { File.Delete(_logFilename); }
                    catch (Exception) { }
                }

                _log = new UniversalLog("ShortcutMenu");
                _target = new UniversalLogFile(_logFilename, FileMode.Create, FlushMode.Default, _log);
            }

            // Attach the command bindings.
            new CopyAction(this.SelectedItemsResolver).AddBinding(RockstarCommands.Copy, this);
        }

        /// <summary>
        /// Sets up the command instances for the lines listbox context menu.
        /// </summary>
        static InfoDisplayWindow()
        {
            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Copy,
                0,
                false,
                "Copy",
                new Guid("63CB25D6-507D-4FAD-A852-D97085B10F0B"),
                ShortcutMenuCommandIds.ContextMenu);
        }
        #endregion

        #region Properties
        /// <summary>
        /// Flag indicating whether the user let the process run to completion.
        /// </summary>
        public bool ProcessKilled
        {
            get { return _processKilled; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Called when the window is closed to determine whether it was a user invoked close or not.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(EventArgs e)
        {
            // Check whether the user invoked the close or not.
            if (_process != null && !_process.HasExited)
            {
                _processKilled = true;
                _process.KillProcessTree();
            }

            base.OnClosed(e);
        }

        /// <summary>
        /// Asynchronously starts the process to run the task.  Once the process has
        /// exited it will optionally display the universal log window with the output
        /// generated by the application.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void InfoDisplayWindow_Loaded(object sender, RoutedEventArgs e)
        {
            // Should we be using the return code in some way?
            bool success = await RunProcessAsync();
            Close();

            if (_task.UseUniversalLog && !_processKilled)
            {
                _target.Flush();

                Process ulogViewer = new Process();
                ulogViewer.StartInfo.FileName = _environment.Subst(_universalLogViewerExe);
                ulogViewer.StartInfo.Arguments = _logFilename;
                ulogViewer.Start();
            }
        }

        /// <summary>
        /// Returns a task that runs a process asynchronously returning once the process
        /// has exited.
        /// </summary>
        /// <returns></returns>
        private Task<bool> RunProcessAsync()
        {
            TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool>();

            _process = new Process();
            _process.StartInfo.FileName = _environment.Subst(_task.Command);
            _process.StartInfo.Arguments = _environment.Subst(_task.Arguments);
            _process.StartInfo.WorkingDirectory = Path.GetDirectoryName(_process.StartInfo.FileName);
            _process.StartInfo.CreateNoWindow = _task.UseUniversalLog;
            _process.StartInfo.UseShellExecute = false;

            if (_task.UseUniversalLog)
            {
                _process.StartInfo.RedirectStandardOutput = true;
                _process.OutputDataReceived +=
                    (sender, args) =>
                    {
                        Process_OutputDataReceived(sender, args);
                    };

                _process.StartInfo.RedirectStandardError = true;
                _process.ErrorDataReceived +=
                    (sender, args) =>
                    {
                        Process_ErrorDataReceived(sender, args);
                    };
            }

            _process.EnableRaisingEvents = true;
            _process.Exited +=
                (sender, args) =>
                {
                    tcs.SetResult(_process.ExitCode == 0);
                };

            _process.Start();

            if (_task.UseUniversalLog)
            {
                _process.BeginOutputReadLine();
                _process.BeginErrorReadLine();
            }

            return tcs.Task;
        }

        /// <summary>
        /// Event handler for parsing standard output received from an executing process.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Process_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            String line = e.Data;
            if (line == null)
            {
                return;
            }

            if (_task.UseUniversalLog)
            {
                if (line.StartsWith("error:", StringComparison.InvariantCultureIgnoreCase) == true)
                {
                    //TODO:  A better mechanism for filtering out false-positive errors.
                    if (line.Contains("up-to-date") == true)
                    {
                        line = line.Replace("error:", "info:");
                        _log.Message(line);
                    }
                    else
                    {
                        _log.Error(line);
                    }
                }
                else if (line.StartsWith("warning:", StringComparison.InvariantCultureIgnoreCase) == true)
                {
                    _log.Warning(line);
                }
                else
                {
                    _log.Message(line);
                }
            }

            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => AddLineToViewModel(line));
                return;
            }
            else
            {
                AddLineToViewModel(line);
            }
        }

        /// <summary>
        /// Adds a single line of output to the info display view model.
        /// </summary>
        /// <param name="line"></param>
        private void AddLineToViewModel(String line)
        {
            InfoDisplayViewModel vm = DataContext as InfoDisplayViewModel;
            if (vm != null)
            {
                vm.AddLine(line);

                if (LinesBox.Items.Count > 0 && LinesBox.SelectedItems.Count == 0)
                {
                    LinesBox.ScrollIntoView(LinesBox.Items[LinesBox.Items.Count - 1]);
                }
            }
        }

        /// <summary>
        /// Event handler for parsing error output received from an executing process.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Process_ErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            String line = e.Data;
            if (line == null)
            {
                return;
            }

            if (_task.UseUniversalLog)
            {
                if (line.Contains("up-to-date") == true)
                {
                    line = line.Replace("error:", "info:");
                }
                _log.Error(line);
            }

            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => AddLineToViewModel(line));
                return;
            }
            else
            {
                AddLineToViewModel(line);
            }
        }

        /// <summary>
        /// Resolver for getting at the selected listbox items.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public IEnumerable<String> SelectedItemsResolver(CommandData data)
        {
            return LinesBox.SelectedItems.OfType<String>();
        }
        #endregion
    }
}
