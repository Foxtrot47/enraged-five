﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace RSG.ShortcutMenu.Tasks
{
    /// <summary>
    /// 
    /// </summary>
    public class SeparatorItem : ShortcutItemBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="elem"></param>
        public SeparatorItem(XElement elem)
            : base(elem)
        {
        }
    }
}
