﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using RSG.Base.OS;
using RSG.Editor.Controls;

namespace RSG.ShortcutMenu.Tasks
{
    /// <summary>
    /// Task for restarting the shortcut menu application.
    /// </summary>
    public sealed class RestartTask : ShortcutItemBase, IShortcutItemTask
    {
        #region Constants
        /// <summary>
        /// Unique guid for this task.
        /// </summary>
        private static readonly Guid _restartGuid = new Guid("CE0CE5FB-4039-455A-BE4C-32DCB0A4DF96");
        #endregion
        
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="RestartTask"/> class
        /// based on the provided parameters.
        /// </summary>
        public RestartTask()
            : base(_restartGuid, "Restart (Files Updated)", Color.FromArgb(127, 128, 0, 0))
        {
        }
        #endregion
        
        #region Methods
        /// <summary>
        /// Kicks off the task.
        /// </summary>
        /// <param name="args"></param>
        public void Invoke(TaskInvokeArgs args)
        {
            RsApplication app = Application.Current as RsApplication;
            if (app != null)
            {
                app.Shutdown(ExitCode.Restart);
            }
        }
        #endregion
    }
}
