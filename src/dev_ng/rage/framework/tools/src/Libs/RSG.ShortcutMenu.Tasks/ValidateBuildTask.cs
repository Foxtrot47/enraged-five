﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace RSG.ShortcutMenu.Tasks
{
    /// <summary>
    /// 
    /// </summary>
    public class ValidateBuildTask : ExecutableTask
    {
        #region Fields
        /// <summary>
        /// Platform to sync.
        /// </summary>
        private readonly RSG.Platform.Platform? _platform;
        #endregion // Fields

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="elem"></param>
        public ValidateBuildTask(XElement elem)
            : base(elem)
        {
            _platform = GetAttributeValue<RSG.Platform.Platform?>(elem, "platform", false);
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public RSG.Platform.Platform? Platform
        {
            get { return _platform; }
        }
        #endregion // Properties
    }
}
