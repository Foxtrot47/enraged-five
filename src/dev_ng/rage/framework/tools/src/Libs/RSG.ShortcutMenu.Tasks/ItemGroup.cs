﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.XPath;

namespace RSG.ShortcutMenu.Tasks
{
    /// <summary>
    /// An <see cref="ItemGroup"/> object is one that can contain other 
    /// shortcut items.
    /// </summary>
    public class ItemGroup : ShortcutItemBase, IShortcutItemGroup
    {
        #region Fields

        /// <summary>
        /// Child items that this group contains.
        /// </summary>
        private readonly IList<IShortcutItem> _items = new List<IShortcutItem>();
        #endregion // Fields

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="ItemGroup"/> class
        /// based on an xml element.
        /// </summary>
        /// <param name="elem"></param>
        /// <param name="factory"></param>
        public ItemGroup(XElement elem, ShortcutItemFactory factory)
            : base(elem)
        {
            foreach (XElement childElem in elem.XPathSelectElements("Items/*"))
            {
                IShortcutItem childItem = factory.CreateShortcutItem(childElem.Name.ToString(), childElem);
                if (childItem != null)
                {
                    _items.Add(childItem);
                }
            }
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// Child items that this group contains.
        /// </summary>
        public IReadOnlyCollection<IShortcutItem> Items
        {
            get { return (IReadOnlyCollection<IShortcutItem>)_items; }
        }
        #endregion // Properties
    } // ItemGroup
}
