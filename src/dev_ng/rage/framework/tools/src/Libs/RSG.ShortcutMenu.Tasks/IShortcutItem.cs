﻿using System;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using RSG.Base.Configuration;

namespace RSG.ShortcutMenu.Tasks
{
    /// <summary>
    /// 
    /// </summary>
    public interface IShortcutItem
    {
        /// <summary>
        /// Unique guid for this item.
        /// </summary>
        Guid Guid { get; }

        /// <summary>
        /// Name of the item as it appears in the menu.
        /// </summary>
        String Name { get; }

        /// <summary>
        /// Flag indicating whether this item should be visible in the menu.
        /// </summary>
        bool Visible { get; }

        /// <summary>
        /// Color of the item's foreground.
        /// </summary>
        Color ForegroundColor { get; }

        /// <summary>
        /// Color of the item's background.
        /// </summary>
        Color BackgroundColor { get; }

        /// <summary>
        /// Location of the item's icon.
        /// </summary>
        String Icon { get; }

        /// <summary>
        /// Image for this item.
        /// </summary>
        BitmapSource GetImage(IEnvironment env);
    }
}
