﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using RSG.Base.Logging;

namespace RSG.ShortcutMenu.Tasks
{
    /// <summary>
    /// Used for creating shortcut items based on their name.
    /// This class self registers item types by reflecting over the local assembly
    /// looking for non-abstract types that implement the IShortcutItem interface.
    /// </summary>
    public class ShortcutItemFactory
    {
        #region Constants
        /// <summary>
        /// Log context.
        /// </summary>
        private const String _factoryLogContext = "Shortcut Item Factory";
        #endregion // Constants

        #region Member Data
        /// <summary>
        /// Log object.
        /// </summary>
        private readonly ILog _log;

        /// <summary>
        /// Registered types that we know can be parsed.
        /// </summary>
        private readonly IDictionary<String, Type> _shortcutItemTypes = new Dictionary<String, Type>();
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public ShortcutItemFactory(ILog log)
        {
            _log = log;
            _log.ProfileCtx(_factoryLogContext, "Discovering types.");

            // Iterate over the types in the current assemblies.
            Assembly currentAssembley = Assembly.GetExecutingAssembly();
            Type iShortcutItemType = typeof(IShortcutItem);

            // Iterate over all types in the assembly that are classes and not abstract.
            foreach (Type type in currentAssembley.GetTypes().Where(type => type.IsClass && !type.IsAbstract))
            {
                if (iShortcutItemType.IsAssignableFrom(type))
                {
                    _shortcutItemTypes.Add(type.Name, type);
                    _log.MessageCtx(_factoryLogContext, "Registered the '{0}' shortcut item type.", type.Name);
                }
            }

            _log.ProfileEnd();
        }
        #endregion // Constructor(s)

        #region Factory Methods
        /// <summary>
        /// Creates an instance of a shortcut item based on it's type name.
        /// </summary>
        /// <param name="name">Name of the type of item we wish to create.</param>
        /// <param name="elem">XElement containing the data for the new instance to .</param>
        /// <returns></returns>
        public IShortcutItem CreateShortcutItem(String name, XElement elem)
        {
            Type itemType;
            if (!_shortcutItemTypes.TryGetValue(name, out itemType))
            {
                _log.ErrorCtx(_factoryLogContext, "'{0}' is not a registered shortcut item.", name);
                return null;
            }

            // Check if we are creating a group or an item.
            bool isGroupType = typeof(IShortcutItemGroup).IsAssignableFrom(itemType);
            if (isGroupType)
            {
                return (IShortcutItem)Activator.CreateInstance(itemType, elem, this);
            }
            else
            {
                return (IShortcutItem)Activator.CreateInstance(itemType, elem);
            }
        }

        /// <summary>
        /// Returns whether the passed in type name is known by the factory.
        /// </summary>
        /// <param name="name"></param>
        /// <returns>True if the particular name is registered, otherwise false.</returns>
        public bool IsRegistered(String name)
        {
            return _shortcutItemTypes.ContainsKey(name);
        }
        #endregion // Factory Methods
    } // ShortcutItemFactory
}
