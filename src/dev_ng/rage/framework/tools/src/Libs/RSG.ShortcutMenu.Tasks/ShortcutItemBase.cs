﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using SDraw = System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Xml.Linq;
using RSG.Base.Configuration;
using System.Windows.Media.Imaging;

namespace RSG.ShortcutMenu.Tasks
{
    /// <summary>
    /// Base class from which shortcut items inherit from.
    /// </summary>
    public abstract class ShortcutItemBase : IShortcutItem
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="Guid"/> property.
        /// </summary>
        private readonly Guid _guid;

        /// <summary>
        /// Private field for the <see cref="Name"/> property.
        /// </summary>
        private readonly String _name;

        /// <summary>
        /// Private field for the <see cref="Visible"/> property.
        /// </summary>
        private bool _visible;

        /// <summary>
        /// Private field for the <see cref="BackgroundColor"/> property.
        /// </summary>
        private readonly Color _backgroundColor = Colors.Transparent;

        /// <summary>
        /// Private field for the <see cref="ForegroundColor"/> property.
        /// </summary>
        private readonly Color _foregroundColor = Colors.Transparent;

        /// <summary>
        /// Private field for the <see cref="Icon"/> property.
        /// </summary>
        private String _icon;

        /// <summary>
        /// Image object for this item.
        /// </summary>
        private BitmapSource _imageSource;
        #endregion // Fields

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="ShortcutItemBase"/> class
        /// based on the provided guid, name and background color.
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="name"></param>
        /// <param name="bgColor"></param>
        public ShortcutItemBase(Guid guid, string name, Color bgColor)
        {
            _guid = guid;
            _name = name;
            _backgroundColor = bgColor;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ShortcutItemBase"/> class
        /// based on an xml element.
        /// </summary>
        /// <param name="elem"></param>
        public ShortcutItemBase(XElement elem)
        {
            _guid = GetAttributeValue<Guid>(elem, "guid", false);
            _name = GetAttributeValue<String>(elem, "name", false);
            _visible = GetAttributeValue<bool>(elem, "visible", false, true);

            XAttribute bgColorAtt = elem.Attribute("background_color");
            if (bgColorAtt != null)
            {
                _backgroundColor = (Color)ColorConverter.ConvertFromString(bgColorAtt.Value);
            }

            XAttribute fgColorAtt = elem.Attribute("foreground_color");
            if (fgColorAtt != null)
            {
                _foregroundColor = (Color)ColorConverter.ConvertFromString(fgColorAtt.Value);
            }

            _icon = GetAttributeValue<String>(elem, "icon", false);
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// Unique guid for this item.
        /// </summary>
        public Guid Guid
        {
            get { return _guid; }
        }

        /// <summary>
        /// Name of the item as it appears in the menu.
        /// </summary>
        public String Name
        {
            get { return _name; }
        }

        /// <summary>
        /// Flag indicating whether this item should be visible in the menu.
        /// </summary>
        public bool Visible
        {
            get { return _visible; }
            set { _visible = value; }
        }

        /// <summary>
        /// Color of the item's foreground.
        /// </summary>
        public Color ForegroundColor
        {
            get { return _foregroundColor; }
        }
        
        /// <summary>
        /// Color of the item's background.
        /// </summary>
        public Color BackgroundColor
        {
            get { return _backgroundColor; }
        }

        /// <summary>
        /// Location of the item's icon.
        /// </summary>
        public String Icon
        {
            get { return _icon; }
        }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Image for this item.
        /// </summary>
        public BitmapSource GetImage(IEnvironment env)
        {
            if (_imageSource == null && _icon != null)
            {
                try
                {
                    _imageSource = new BitmapImage(new Uri(env.Subst(_icon)));
                }
                catch (Exception)
                {
                    _icon = null;
                }
            }
            return _imageSource;
        }

        /// <summary>
        /// Read attribute of a particular type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="elem"></param>
        /// <param name="attribute">Name of the attribute to get the value from.</param>
        /// <param name="required">Flag indicating whether the attribute needs to be present.  If true this method throws an exception if it doesn't find it.</param>
        /// <param name="defaultValue">Default value to use if the attribute isn't required and not present.</param>
        /// <returns>The requested attributed casted to the right type, or the default value of T if the attribute isn't present and not required.</returns>
        protected T GetAttributeValue<T>(XElement elem, String attribute, bool required = true, T defaultValue = default(T))
        {
            // Make sure the attribute exists.
            XAttribute att = elem.Attribute(attribute);
            if (att == null)
            {
                if (required)
                {
                    throw new ArgumentNullException(String.Format("Unable to read the '{0}' attribute under the '{1}' element.", attribute, elem.Name));
                }
                else
                {
                    return defaultValue;
                }
            }

            // Convert the attribute to the right type.
            //return (T)Convert.ChangeType(att.Value, typeof(T));
            return (T)TypeDescriptor.GetConverter(typeof(T)).ConvertFromInvariantString(att.Value);
        }
        #endregion // Methods
    } // ShortcutItemBase
}
