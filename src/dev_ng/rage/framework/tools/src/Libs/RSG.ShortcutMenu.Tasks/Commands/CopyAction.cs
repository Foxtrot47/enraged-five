﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using RSG.Editor;

namespace RSG.ShortcutMenu.Tasks.Commands
{
    /// <summary>
    /// Action for copying string data.
    /// </summary>
    public class CopyAction : ButtonAction<IEnumerable<String>>
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="CopyAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public CopyAction(ParameterResolverDelegate<IEnumerable<String>> resolver)
            : base(resolver)
        {
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// Override to set logic against the can execute command handler.
        /// </summary>
        /// <param name="messages"></param>
        /// <returns></returns>
        public override bool CanExecute(IEnumerable<String> messages)
        {
            return messages.Any();
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="messages">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(IEnumerable<String> messages)
        {
            Clipboard.SetText(String.Join("\n", messages));
        }
        #endregion // Overrides
    } // CopyAction
}
