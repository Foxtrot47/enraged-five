﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;

namespace RSG.Pipeline.Services.Platform.Manifest
{

    /// <summary>
    /// Bounds Additions XML file reader.
    /// </summary>
    /// The 'BoundsAdditions' XML file is output by the bounds processor
    /// and is a major input to the RPF manifest constructor.
    /// 
    public class BoundsAdditions
    {
        #region Properties
        /// <summary>
        /// Filename.
        /// </summary>
        public String Filename
        {
            get;
            private set;
        }

        /// <summary>
        /// Collection of ITYP streaming dependencies.
        /// </summary>
        public IEnumerable<InteriorBounds> InteriorBounds
        {
            get;
            private set;
        }

        /// <summary>
        /// Collection of ITYP streaming dependencies.
        /// </summary>
        public IEnumerable<IMAPGroupBounds> IMAPGroupBounds
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor; from filename.
        /// </summary>
        /// <param name="filename"></param>
        public BoundsAdditions(String filename)
        {
            Load(filename);
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// Load XML data.
        /// </summary>
        /// <param name="filename"></param>
        private void Load(String filename)
        {
            this.Filename = filename;
            List<InteriorBounds> interiorBounds = new List<InteriorBounds>();
            List<IMAPGroupBounds> imapGroupBounds = new List<IMAPGroupBounds>();
            XDocument document = XDocument.Load(filename);

            IEnumerable<XElement> interiorBoundsElements = document.XPathSelectElements("/ManifestData/Interior");
            foreach (XElement interiorBoundsElement in interiorBoundsElements)
            {
                interiorBounds.Add(Manifest.InteriorBounds.FromBoundsAdditions(interiorBoundsElement));
            }

            IEnumerable<XElement> imapGroupBoundsElements = document.XPathSelectElements("/ManifestData/IMAPGroup");
            foreach (XElement imapGroupBoundsElement in imapGroupBoundsElements)
            {
                imapGroupBounds.Add(Manifest.IMAPGroupBounds.FromBoundsAdditions(imapGroupBoundsElement));
            }

            this.InteriorBounds = interiorBounds;
            this.IMAPGroupBounds = imapGroupBounds;
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Services.Platform.Manifest namespace
