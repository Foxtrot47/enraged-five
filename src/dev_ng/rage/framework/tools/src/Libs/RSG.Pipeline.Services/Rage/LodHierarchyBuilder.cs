﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using RSG.Rage;
using RSG.Rage.TypeFile;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Rage.Entity;
using MR = RSG.ManagedRage;

namespace RSG.Pipeline.Services.Rage
{
    /// <summary>
    /// 
    /// </summary>
    public class LodHierarchyBuilder
    {
        #region Constants
        private static string LOG_CTX = "LOD Hiearchy Builder";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Description/map for how we turn our input data into a single LOD hiearchy with associated
        /// type and mesh data correctly updated to reflect this.
        /// </summary>
        public ILodDescription LodDescription { get; set; }

        /// <summary>
        /// Location of source data to be merged.
        /// </summary>
        public String SourceDirectory { get; set; }

        /// <summary>
        /// Output location for merged data.
        /// </summary>
        public String DestinationDirectory { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public FragmentType Fragment { get; set; }

        /// <summary>
        /// Universal log object.
        /// </summary>
        public IUniversalLog Log { get; set; }
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// Lone constructor.
        /// </summary>
        public LodHierarchyBuilder(ILodDescription lodDesc, String sourceDir, String destinationDir, IUniversalLog log)
        {
            this.LodDescription = lodDesc;
            this.SourceDirectory = sourceDir;
            this.DestinationDirectory = destinationDir;
            this.Log = log;
            this.Fragment = new FragmentType(this.Log);
        }
        #endregion // Constructors

        #region Controller Methods
        /// <summary>
        /// T
        /// </summary>
        /// <returns></returns>
        public bool Fabricate()
        {
            bool result = true;
            
            FragmentType medFrag = new FragmentType(this.Log);
            FragmentType lowFrag = new FragmentType(this.Log);
            FragmentType vlowFrag = new FragmentType(this.Log);

            Dictionary<String, String> existingFiles = new Dictionary<String, String>();
            Dictionary<String, String> tcsFiles = new Dictionary<String, String>();
            Dictionary<String, String> animFiles = new Dictionary<String, String>();

            if (!Directory.Exists(this.DestinationDirectory))
                Directory.CreateDirectory(this.DestinationDirectory);

            String texDestDir = Path.Combine(this.DestinationDirectory, "textures");
            if (!Directory.Exists(texDestDir))
                Directory.CreateDirectory(texDestDir);

            String animDestDir = Path.Combine(this.DestinationDirectory, "anim");
            if (!Directory.Exists(animDestDir))
                Directory.CreateDirectory(animDestDir);

#warning LPXO: This is a little hacky, we're passing self's fragment to merge as the high lod level.  It's nice and convenient but needs to be handled better or we could introduce bugs with non-vehicle frags (my test type).
            if (!this.CollectAndMergeData(LodLevel.High, this.Fragment, ref existingFiles, ref tcsFiles, ref animFiles))
            {
                this.Log.ErrorCtx(LOG_CTX, "No high detail LOD description for {0}.  Abandoning LOD fabrication process.", this.SourceDirectory);
                return false;
            }
            this.CollectAndMergeData(LodLevel.Medium, medFrag, ref existingFiles, ref tcsFiles, ref animFiles);
            this.CollectAndMergeData(LodLevel.Low, lowFrag, ref existingFiles, ref tcsFiles, ref animFiles);
            this.CollectAndMergeData(LodLevel.VeryLow, vlowFrag, ref existingFiles, ref tcsFiles, ref animFiles);
           
            this.Fragment.SaveToFile(String.Format("{0}/entity", this.DestinationDirectory), "type");
            this.Fragment.SaveAllMeshes(this.DestinationDirectory);

            Dictionary<String, String> outputFiles = new Dictionary<String, String>();
            foreach (String file in Directory.GetFiles(this.DestinationDirectory).ToList())
            {
                outputFiles.Add(Path.GetFileName(file), file);
            }

            foreach (KeyValuePair<String,String> kvp in existingFiles)
            {
                if (!outputFiles.ContainsKey(kvp.Key))
                {
                    String dest = Path.Combine(this.DestinationDirectory, kvp.Key);
                    File.Copy(kvp.Value, dest, true);
                }
            }

            foreach (KeyValuePair<String, String> kvp in tcsFiles)
            {
                String dest = Path.Combine(texDestDir, kvp.Key);
                File.Copy(kvp.Value, dest, true);
            }

            foreach (KeyValuePair<String, String> kvp in animFiles)
            {
                String dest = Path.Combine(animDestDir, kvp.Key);
                File.Copy(kvp.Value, dest, true);
            }

            return (result);
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="lodLevel"></param>
        /// <param name="existingFiles"></param>
        /// <param name="frag"></param>
        /// <returns></returns>
        bool CollectAndMergeData(LodLevel lodLevel, FragmentType frag, ref Dictionary<String, String> fileList, ref Dictionary<String, String> tcsList, ref Dictionary<String, String> animList)
        {
            bool hasLod = false;
            if ((this.LodDescription.LodLevels as Dictionary<LodLevel, String>).ContainsKey(lodLevel))
            {
                String detailDirectory = Path.Combine(this.SourceDirectory, (this.LodDescription.LodLevels as Dictionary<LodLevel, String>)[lodLevel]);
                String textureDirectory = Path.Combine(detailDirectory, "textures");
                String genericTextureDirectory = Path.Combine(this.SourceDirectory, "textures");
                String animDirectory = Path.Combine(this.SourceDirectory, "anim");
                String detailEntity = Path.Combine(detailDirectory, "entity");

                frag.ShadingGroup.LoadSvaFilesFrom(detailDirectory);

                foreach (String file in Directory.GetFiles(detailDirectory))
                {
                    if (!fileList.ContainsKey(Path.GetFileName(file)))
                        fileList.Add(Path.GetFileName(file), file);
                }

                if (Directory.Exists(textureDirectory))
                {
                    foreach (String file in Directory.GetFiles(textureDirectory))
                    {
                        if (!tcsList.ContainsKey(Path.GetFileName(file)))
                            tcsList.Add(Path.GetFileName(file), file);
                    }
                }

                if (Directory.Exists(genericTextureDirectory))
                {
                    foreach (String file in Directory.GetFiles(genericTextureDirectory))
                    {
                        if (!tcsList.ContainsKey(Path.GetFileName(file)))
                            tcsList.Add(Path.GetFileName(file), file);
                    }
                }

                if (Directory.Exists(animDirectory))
                {
                    foreach (String file in Directory.GetFiles(animDirectory))
                    {
                        if (!animList.ContainsKey(Path.GetFileName(file)))
                            animList.Add(Path.GetFileName(file), file);
                    }
                }

                if (frag.LoadFromFile(detailEntity, "type"))
                {
                    this.Fragment.SetLodFromFragType(frag, lodLevel, detailDirectory);
                    hasLod = true;
                }
                else
                {
                    this.Log.ErrorCtx(LOG_CTX, "Failed to load {0} entity {1}.  Abandoning LOD fabrication process.", lodLevel.ToString(), detailEntity);
                    return false;
                }

                foreach (String file in Directory.GetFiles(this.SourceDirectory))
                {
                    string filename = Path.GetFileName(file);
                    string extension = Path.GetExtension(file);
                    if (extension == ".rbs" && !fileList.ContainsKey(filename))
                        fileList.Add(filename, file);
                }
            }
            
            return hasLod;
        }
        #endregion // Private Methods
    }
}
