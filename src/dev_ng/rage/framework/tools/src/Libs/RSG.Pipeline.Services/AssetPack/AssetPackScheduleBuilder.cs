﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.Pipeline.Services.AssetPack
{
    /// <summary>
    /// The AssetPackScheduleBuilder is used to build input XML for the AssetPackProcessor
    /// </summary>
    public class AssetPackScheduleBuilder
    {
        public AssetPackScheduleBuilder()
        {
            zipArchives_ = new List<AssetPackZipArchive>();
        }

        public AssetPackZipArchive AddZipArchive(string pathname, DuplicateBehaviour dupeBehaviour = DuplicateBehaviour.Error)
        {
            AssetPackZipArchive newZipArchive = new AssetPackZipArchive(pathname, dupeBehaviour);
            zipArchives_.Add(newZipArchive);

            return newZipArchive;
        }

        public void Save(string pathname)
        {
            XDocument document = new XDocument(
                new XElement("Assets", zipArchives_.Select(zipArchive => zipArchive.ToXElement())));

            String directoryName = System.IO.Path.GetDirectoryName(pathname);
            if (!System.IO.Directory.Exists(directoryName))
                System.IO.Directory.CreateDirectory(directoryName);

            document.Save(pathname);
        }

        private List<AssetPackZipArchive> zipArchives_;
    }

} // RSG.Pipeline.Services.AssetPack namespace
