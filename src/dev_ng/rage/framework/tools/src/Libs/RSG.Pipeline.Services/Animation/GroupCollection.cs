﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.Services.Animation
{
    /// <summary>
    /// Wrapper for a collection of Group objects
    /// </summary>
    public class GroupCollection : IEnumerable<Group>
    {
        #region Fields
        /// <summary>
        /// The collection of clip groups
        /// </summary>
        private List<Group> _Items = new List<Group>();
        #endregion // Fields

        #region Methods
        /// <summary>
        /// Determine whether the Item list already contains a ClipGroup.
        /// </summary>
        /// <param name="inClipGroup"></param>
        public bool Contains(ref Group inClipGroup)
        {
            foreach (Group clipGroup in this._Items)
            {
                if (clipGroup.Additive == inClipGroup.Additive &&
                    clipGroup.CompressionTemplate == inClipGroup.CompressionTemplate &&
                    clipGroup.CompressionTemplate == inClipGroup.CompressionTemplate)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Add a ClipGroup if it doesnt already exist.
        /// </summary>
        /// <param name="inClipGroup"></param>
        public bool AddIfUnique(ref Group inClipGroup)
        {
            if (!this.Contains(inClipGroup))
            {
                _Items.Add(inClipGroup);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Add a ClipGroup if it doesnt already exist, otherwise merge it's file list with an existing ClipGroup.
        /// </summary>
        /// <param name="inClipGroup"></param>
        public void AddOrMerge(ref Group inClipGroup)
        {
            bool added = false;
            foreach (Group clipGroup in this._Items)
            {
                if (clipGroup.Additive == inClipGroup.Additive &&
                    clipGroup.CompressionTemplate == inClipGroup.CompressionTemplate &&
                    clipGroup.SkeletonFile == inClipGroup.SkeletonFile)
                {
                    clipGroup.ClipFiles.AddRange(inClipGroup.ClipFiles);
                    added = true;
                }
            }
            if (!added)
            {
                this._Items.Add(inClipGroup);
            }
        }
        #endregion

        #region IEnumerable<ClipGroup> Members
        public IEnumerator<Group> GetEnumerator()
        {
            return _Items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        #endregion
    }
}
