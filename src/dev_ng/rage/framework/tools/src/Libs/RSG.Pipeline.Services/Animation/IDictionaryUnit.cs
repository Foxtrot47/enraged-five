﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.Services.Animation
{
    /// <summary>
    /// Dictionary Unit interface
    /// </summary>
    public interface IDictionaryUnit
    {
        #region Properties
        /// <summary>
        /// Dictionary unit name.  This is used to generate outputname
        /// </summary>
        string Rootname { get; }

        /// <summary>
        /// Dictionary unit name.  This is used to generate outputname
        /// </summary>
        string DictionaryName { get; }

        /// <summary>
        /// Path to the main dictionary for the dictionary unit (if there is one).
        /// </summary>
        string MainDictionaryPath { get; }

        /// <summary>
        /// Additional files to be included with the data from the main dictionary.
        /// </summary>
        IEnumerable<DictionaryItem> Inclusions { get; } 

        /// <summary>
        /// Files to be exlcuded from the main dictionary.
        /// </summary>
        IEnumerable<string> Exclusions { get; }
        #endregion

        #region Methods
        /// <summary>
        /// Add additional file and alias.
        /// </summary>
        void AddInclusion(DictionaryItem dictionaryItem);

        /// <summary>
        /// Add an exclusion by alias.
        /// </summary>
        void AddExclusion(string alias);
        #endregion
    }
}
