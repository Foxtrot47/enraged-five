﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using SIO = System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Core;

namespace RSG.Pipeline.Services.AssetPack
{
    /// <summary>
    /// LPXO: DO NOT just use this to hide what may be a legitimate error.
    /// 
    /// Decides how duplicate files are handled when encountered.  This is required because the asset pack schedule 
    /// file can include directories the requesting process may not be able to determine if duplicates will be present up front.
    /// </summary>
    public enum DuplicateBehaviour
    {
        Error, 
        AcceptFirst,
        AcceptLast,    
    }

    /// <summary>
    /// Asset Pack File data structure; parser for reading asset packing XML
    /// as documented at https://devstar.rockstargames.com/wiki/index.php/Dev:Asset_Pipeline3_Asset_Pack.
    /// </summary>
    /// <see cref="https://devstar.rockstargames.com/wiki/index.php/Dev:Asset_Pipeline3_Asset_Pack"/>
    ///
    public class AssetPackScheduleFile
    {
        #region Constants
        /// <summary>
        /// Log context string.
        /// </summary>
        private static readonly String LOG_CTX = "Asset Pack Schedule";

        /// <summary>
        /// XML attribute representaiton of DuplicateBehaviour.Error
        /// </summary>
        private static readonly String DUPE_BEHAVIOUR_ATTR_ERROR = "error";

        /// <summary>
        /// XML attribute representaiton of DuplicateBehaviour.AcceptFirst
        /// </summary>
        private static readonly String DUPE_BEHAVIOUR_ATTR_ACCEPTFIRST = "acceptfirst";

        /// <summary>
        /// XML attribute representaiton of DuplicateBehaviour.AcceptLast
        /// </summary>
        private static readonly String DUPE_BEHAVIOUR_ATTR_ACCEPTLAST = "acceptlast";

        #endregion // Constants

        #region Properties
        /// <summary>
        /// Filename describing the assets to pack.
        /// </summary>
        public String Filename
        {
            get;
            private set;
        }

        /// <summary>
        /// Owning content tree for assets content-nodes.
        /// </summary>
        public IContentTree Owner
        {
            get;
            private set;
        }

        /// <summary>
        /// Dictionary of assets (keys are outputs, values lists of inputs).
        /// </summary>
        public IDictionary<IContentNode, Tuple<IEnumerable<IContentNode>, DuplicateBehaviour>> Assets
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Universal log.
        /// </summary>
        private static IUniversalLog Log { get; set; }
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor; loading asset declarations from XML file.
        /// </summary>
        /// <param name="filename"></param>
        public AssetPackScheduleFile(IContentTree owner, String filename)
        {
            this.Filename = filename;
            this.Owner = owner;
            this.Assets = new Dictionary<IContentNode, Tuple<IEnumerable<IContentNode>, DuplicateBehaviour>>();
            this.Parse();
        }

        /// <summary>
        /// Constructor; programatically defining assets to pack.
        /// </summary>
        /// <param name="assets"></param>
        public AssetPackScheduleFile(IContentTree owner, IDictionary<IContentNode, Tuple<IEnumerable<IContentNode>, DuplicateBehaviour>> assets)
        {
            this.Owner = owner;
            this.Assets = assets;
        }

        /// <summary>
        /// Static constructor.
        /// </summary>
        static AssetPackScheduleFile()
        {
            AssetPackScheduleFile.Log = LogFactory.CreateUniversalLog("Asset Pack File");
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// Parse XML to populate our asset dictionary.
        /// </summary>
        private void Parse()
        {
            if (!SIO.File.Exists(this.Filename))
                throw new SIO.FileNotFoundException("Asset Pack XML not found.", this.Filename);

            Log.MessageCtx(LOG_CTX, "Parsing Asset Pack Schedule: {0}.", this.Filename);
            XDocument xmlDoc = XDocument.Load(this.Filename, 
                LoadOptions.SetLineInfo | LoadOptions.SetBaseUri);
            IEnumerable<XElement> xmlZipArchiveElems =
                xmlDoc.XPathSelectElements("/Assets/ZipArchive");
            foreach (XElement xmlZipArchiveElem in xmlZipArchiveElems)
            {
                XAttribute xmlPathAttr = xmlZipArchiveElem.Attribute("path");
                XAttribute xmlForceCreateAttr = xmlZipArchiveElem.Attribute("forceCreateIfEmpty");
                XAttribute xmlDupeBehaviourAttr = xmlZipArchiveElem.Attribute("dupeBehaviour");

                IEnumerable<XElement> xmlFileElems =
                    xmlZipArchiveElem.XPathSelectElements("File");
                IEnumerable<XElement> xmlDirectoryElems = 
                    xmlZipArchiveElem.XPathSelectElements("Directory");

                RSG.Base.Configuration.IEnvironment env = this.Owner.Branch.Environment;
                DuplicateBehaviour dupeBehaviour = DuplicateBehaviour.Error;

                // Create output.
                IContentNode output = this.Owner.CreateAsset(env.Subst(xmlPathAttr.Value),
                    RSG.Platform.Platform.Independent);

                if (xmlForceCreateAttr != null)
                    output.Parameters.Add(Constants.Force_Create_If_Empty, true);

                if (xmlDupeBehaviourAttr != null)
                    dupeBehaviour = ResolveDupeBehaviourString(xmlDupeBehaviourAttr.Value as String);

                ICollection<IContentNode> entries = new List<IContentNode>();

                // Sort out file inputs.
                foreach (XElement xmlFileElem in xmlFileElems)
                {
                    XAttribute xmlFilePathAttr = xmlFileElem.Attribute("path");
                    XAttribute xmlFileDestAttr = xmlFileElem.Attribute("destination");

                    // Verify we have the required 'path' attribute.
                    Debug.Assert(null != xmlFilePathAttr, "No 'path' attribute specified for File.");
                    if (null == xmlFilePathAttr)
                    {
                        Log.Error(xmlFileElem, "No 'path' attribute specified for File.");
                        continue;
                    }

                    String filename = env.Subst(xmlFilePathAttr.Value);
                    String destination = String.Empty;
                    IContentNode fileInput = this.Owner.CreateFile(filename);
                    if (null != xmlFileDestAttr)
                    {
                        destination = xmlFileDestAttr.Value;

                        if (String.IsNullOrEmpty(destination))
                        {
                            Log.WarningCtx(LOG_CTX, "Destination parameter is empty for Filename: {0}", filename);
                        }
                        else
                        {
                            if (fileInput.Parameters.ContainsKey(Constants.Asset_ZipPath))
                            {
                                String zipPath = (String)fileInput.Parameters[Constants.Asset_ZipPath];
                                if (0 != String.Compare(destination, zipPath))
                                    Log.Error(xmlFileDestAttr, "File input: {0}, has multiple destinations: {1}, {2}.",
                                        (fileInput as Content.File).AbsolutePath, zipPath, destination);
                            }
                            else
                            {
                                fileInput.Parameters.Add(Constants.Asset_ZipPath, destination);
                            }
                        }
                    }
                    entries.Add(fileInput);
                }

                // Sort out directory inputs.
                foreach (XElement xmlDirectoryElem in xmlDirectoryElems)
                {
                    XAttribute xmlDirectoryPathAttr = xmlDirectoryElem.Attribute("path");
                    XAttribute xmlDirectoryDestAttr = xmlDirectoryElem.Attribute("destination");
                    XAttribute xmlDirectoryWildcardAttr = xmlDirectoryElem.Attribute("wildcard");
                    XAttribute xmlDirectoryRecursiveAttr = xmlDirectoryElem.Attribute("recursive");

                    // Verify we have the required 'path' attribute.
                    Debug.Assert(null != xmlDirectoryPathAttr, "No 'path' attribute specified for Directory.");
                    if (null == xmlDirectoryPathAttr)
                    {
                        Log.Error(xmlDirectoryElem, "No 'path' attribute specified for Directory.");
                        continue;
                    }

                    String filename = env.Subst(xmlDirectoryPathAttr.Value);
                    String destination = String.Empty;
                    String recursive = String.Empty;
                    IContentNode dirInput = null;
                    if(null != xmlDirectoryWildcardAttr)
                        dirInput = this.Owner.CreateDirectory(filename, xmlDirectoryWildcardAttr.Value, (xmlDirectoryRecursiveAttr != null));
                    else
                        dirInput = this.Owner.CreateDirectory(filename, "*.*", (xmlDirectoryRecursiveAttr != null));

                    if (null != xmlDirectoryDestAttr)
                    {
                        destination = xmlDirectoryDestAttr.Value;

                        if (String.IsNullOrEmpty(destination))
                        {
                            Log.WarningCtx(LOG_CTX, "Destination parameter is empty for Directory: {0}", filename);
                        }
                        else
                        {
                            if (dirInput.Parameters.ContainsKey(Constants.Asset_ZipPath))
                            {
                                String zipPath = (String)dirInput.Parameters[Constants.Asset_ZipPath];
                                if (0 != String.Compare(destination, zipPath))
                                    Log.Error(xmlDirectoryDestAttr, "Directory input: {0}, has multiple destinations: {1}, {2}.",
                                        (dirInput as Content.Directory).AbsolutePath, zipPath, destination);
                            }
                            else
                            {
                                dirInput.Parameters.Add(Constants.Asset_ZipPath, destination);
                            }
                        }
                    }

                    entries.Add(dirInput);
                }

                // Add into the assets dictionary.
                this.Assets.Add(output, new Tuple<IEnumerable<IContentNode>, DuplicateBehaviour>(entries, dupeBehaviour));
            }
        }
        #endregion // Private Methods

        #region Public Static Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dupeBeh"></param>
        /// <returns></returns>
        public static string ResolveDupeBehaviourEnum(DuplicateBehaviour dupeBeh)
        {
            if (dupeBeh == DuplicateBehaviour.AcceptFirst)
                return DUPE_BEHAVIOUR_ATTR_ACCEPTFIRST;
            else if (dupeBeh == DuplicateBehaviour.AcceptLast)
                return DUPE_BEHAVIOUR_ATTR_ACCEPTLAST;
            else
                return DUPE_BEHAVIOUR_ATTR_ERROR;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dupeBeh"></param>
        /// <returns></returns>
        public static DuplicateBehaviour ResolveDupeBehaviourString(String dupeBeh)
        {
            if (dupeBeh == DUPE_BEHAVIOUR_ATTR_ACCEPTFIRST)
                return DuplicateBehaviour.AcceptFirst;
            else if (dupeBeh == DUPE_BEHAVIOUR_ATTR_ACCEPTLAST)
                return DuplicateBehaviour.AcceptLast;
            else
                return DuplicateBehaviour.Error;
        }
        #endregion // Public Static Methods
    }

} // RSG.Pipeline.Services.AssetPack namespace
