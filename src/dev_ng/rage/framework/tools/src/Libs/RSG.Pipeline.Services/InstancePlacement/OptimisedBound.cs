﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

using RSG.Base;
using RSG.Base.Math;
using RSG.Bounds;
using RSG.Base.Math;

namespace RSG.Pipeline.Services.InstancePlacement
{
    /// <summary>
    /// A helper class for storing bounds primitive information.
    /// This flattens information about the RSG.Bound object so we don't have to 
    /// constantly reallocate memory throughout the instance placement process.
    /// </summary>
    public class OptimisedPrimitive
    {
        #region Public Members
        public BoundingBox3f BoundingBox;
        public Vector3f[] Vertices;
        public Plane Plane;
        public Vector4i Color;
        public String MaterialName;
        #endregion

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="boundingBox"></param>
        /// <param name="vertices"></param>
        /// <param name="color"></param>
        public OptimisedPrimitive(BoundingBox3f boundingBox, Vector3f[] vertices, Vector4i color, String materialName)
        {
            BoundingBox = boundingBox;
            Vertices = vertices;
            Plane = new Plane(vertices[0], vertices[1], vertices[2]);
            Color = color;
            MaterialName = materialName;
        }
        #endregion
    }

    /// <summary>
    /// A helper class to store bounds information from a BVH.  
    /// </summary>
    public class OptimisedBound
    {
        #region Public Properties
        public String SourceFilename;
        public BVH Bound;
        public List<OptimisedPrimitive> Primitives;
        #endregion

        #region Constructors
        public OptimisedBound(String sourceFilename, BVH bound)
        {
            SourceFilename = sourceFilename;
            Bound = bound;
            Primitives = new List<OptimisedPrimitive>();

            for (int index = 0; index < bound.Primitives.Count(); ++index)
            {
                //Only support triangle primitives since we're processing only terrain meshes.
                Debug.Assert(bound.Primitives[index] is BVHTriangle);
                BVHTriangle tri = bound.Primitives[index] as BVHTriangle;
                if (tri == null) continue; //Support only normal triangles (for now).

                BoundingBox3f boundingBox = tri.GetBoundingBox(bound.Verts);

                Vector3f[] vertices = new Vector3f[3];
                vertices[2] = new Vector3f(BoundsProcessing.RoundNearest(bound.Verts[tri.Index0].Position.X), BoundsProcessing.RoundNearest(bound.Verts[tri.Index0].Position.Y), BoundsProcessing.RoundNearest(bound.Verts[tri.Index0].Position.Z));
                vertices[1] = new Vector3f(BoundsProcessing.RoundNearest(bound.Verts[tri.Index1].Position.X), BoundsProcessing.RoundNearest(bound.Verts[tri.Index1].Position.Y), BoundsProcessing.RoundNearest(bound.Verts[tri.Index1].Position.Z));
                vertices[0] = new Vector3f(BoundsProcessing.RoundNearest(bound.Verts[tri.Index2].Position.X), BoundsProcessing.RoundNearest(bound.Verts[tri.Index2].Position.Y), BoundsProcessing.RoundNearest(bound.Verts[tri.Index2].Position.Z));

                string materialName = Bound.Materials[tri.MaterialIndex];
                BVHMaterial material = new BVHMaterial(materialName);
                Vector4i materialColor = new Vector4i(255, 255, 255, 255);
                if (material.PaletteIndex > 0 && Bound.MaterialColours.Count() > 0)
                    materialColor = Bound.MaterialColours[material.PaletteIndex - 1];

                // Use the material name for a "rejection" style system.
                string[] materialNameSplit = materialName.Split('|');
                if (materialNameSplit.Length > 0)
                    materialName = materialNameSplit[0].ToLower();

                OptimisedPrimitive primitive = new OptimisedPrimitive(boundingBox, vertices, materialColor, materialName);

                //The bounds can generate degenerate polygons and duplicate vertices.  
                //Guard against it until the exporter has been fixed.
                if (primitive.Plane.X == 0.0f && primitive.Plane.Y == 0.0f
                    && primitive.Plane.Z == 0.0f && primitive.Plane.W == 0.0f)
                {
                    //Ignore.  This is a degenerate polygon.
                }
                else
                {
                    Primitives.Add(primitive);
                }

            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Determines if the point exist within the 2D bounds.
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public bool Intersect(Vector3f point)
        {
            bool xInRange = (Bound.Bounds.Min.X <= point.X) && (point.X <= Bound.Bounds.Max.X);
            bool yInRange = (Bound.Bounds.Min.Y <= point.Y) && (point.Y <= Bound.Bounds.Max.Y);

            return (xInRange && yInRange);
        }
        #endregion
    }
}
