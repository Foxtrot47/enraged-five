﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.Pipeline.Services.AssetProcessor
{
    /// <summary>
    /// Class representing a single Asset Processor operation.
    /// </summary>
    public class AssetProcessorTask
    {
        #region Constants
        private const string CONFIG_OP = "Operation";
        private const string OperationNameAttribute = "name";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Operation field.
        /// </summary>
        public Operation Operation
        {
            get;
            private set;
        }

        public IEnumerable<Uri> Elements { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        public AssetProcessorTask(Operation operation, IEnumerable<Uri> elements = null )
        {
            Operation = operation;
            Elements = elements ?? new Uri[0];
        }

        /// <summary>
        /// AssetProcessorOperation ctor, with no sub elements
        /// </summary>
        /// <param name="operation"></param>
        public AssetProcessorTask(XElement operation)
        {
            AssetProcessorSetup(operation, new Uri[0]);
        }

        /// <summary>
        /// AssetProcessorOperation ctor with sub elements
        /// </summary>
        /// <param name="operation">Operation type</param>
        /// <param name="elements">Associated elements for this operation</param>
        public AssetProcessorTask(XElement operation, IEnumerable<Uri> elements)
        {
            AssetProcessorSetup(operation, elements);
        }

        /// <summary>
        /// Generic initialization code for constructors
        /// </summary>
        /// <param name="operationElement"></param>
        /// <param name="elements"></param>
        private void AssetProcessorSetup(XElement operationElement, IEnumerable<Uri> elements) 
        {
            // the name of the operation should match the Enum.Operation elements
            string operationString = operationElement.Attribute(OperationNameAttribute).Value;

            Operation operation;
            if (Enum.TryParse(operationString, out operation))
                Operation = operation;
            else
                throw new ArgumentException("Invalid value for attribute {0}. Check the AssetProcessorConfig file.", OperationNameAttribute); // TODO ILog stuff ?

            // no sub elements supplied, so ensure an empty collection here
            Elements = elements;
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Serialise the task object to an XElement.
        /// </summary>
        /// <returns></returns>
        public XElement ToXElement()
        {
            XElement operation =
                new XElement(CONFIG_OP, new XAttribute(OperationNameAttribute, Operation.ToString()),
                    new XElement("Elements",
                        Elements.Select(e => new XElement("Element", e.ToString()))));
            
            return operation;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendFormat("Operation: {0}, on elements:", Operation);
            foreach (Uri element in Elements)
            {
                sb.AppendFormat("\n\t{0}", element);
            }

            return sb.ToString();
        }
        #endregion // Controller Methods
    }

} // RSG.Pipeline.Services.AssetProcessor namespace
