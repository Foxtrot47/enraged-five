﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.Pipeline.Services.AssetPack
{

    /// <summary>
    /// 
    /// </summary>
    internal class AssetPackDirectory
    {
        internal AssetPackDirectory(string directoryName, string destination = null, string wildcard = null, bool recursive = false)
        {
            DirectoryName = directoryName;
            Destination = destination;
            Wildcard = wildcard;
            Recursive = recursive;
        }

        internal XElement ToXElement()
        {
            XElement directoryElement = new XElement("Directory", new XAttribute("path", DirectoryName));
            if(Destination != null)
                directoryElement.Add(new XAttribute("destination", Destination));
            if (Wildcard != null)
                directoryElement.Add(new XAttribute("wildcard", Wildcard));
            if (Recursive != false)
                directoryElement.Add(new XAttribute("recursive", Recursive));

            return directoryElement;
        }

        internal string DirectoryName { get; private set; }
        internal string Destination { get; private set; }
        internal string Wildcard { get; private set; }
        internal bool Recursive { get; private set; }
    }

} // RSG.Pipeline.Services.AssetPack namespace
