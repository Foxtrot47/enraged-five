﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Text.RegularExpressions;

namespace RSG.Pipeline.Services
{
    public sealed class Email
    {
        #region Public Static methods
        /// <summary>
        /// Validate an email address (does not authenticate).
        /// http://msdn.microsoft.com/en-us/library/01escwtf.aspx with the timeout exceptions removed (unsupported in .NET 4.0).
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <returns></returns>
        public static bool IsValidEmail(String strIn)
        {
            if (String.IsNullOrEmpty(strIn))
                return false;

            // Use IdnMapping class to convert Unicode domain names. 
            strIn = Regex.Replace(strIn, @"(@)(.+)$", Email.DomainMapper,
                                      RegexOptions.None);

            // Return true if strIn is in valid e-mail format.  
            return Regex.IsMatch(strIn,
                    @"^(?("")(""[^""]+?""@)|(([0-9a-z\*]((\.(?!\.))|[-!#\$%&'\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                    @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9]{2,17}))$",
                    RegexOptions.IgnoreCase);
        }
        #endregion // Public Static Methods

        #region Private Static Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="match"></param>
        /// <returns></returns>
        private static string DomainMapper(Match match)
        {
            // IdnMapping class with default property values.
            IdnMapping idn = new IdnMapping();

            string domainName = match.Groups[2].Value;
            domainName = idn.GetAscii(domainName);
           
            return match.Groups[1].Value + domainName;
        }
        #endregion

    }
}
