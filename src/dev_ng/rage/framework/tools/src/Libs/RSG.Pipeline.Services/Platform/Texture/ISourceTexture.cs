﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.Services.Platform.Texture
{

    /// <summary>
    /// Source texture (BMP, TGA, etc).
    /// </summary>
    public interface ISourceTexture
    {
        #region Properties
        /// <summary>
        /// Absolute path to source texture file.
        /// </summary>
        String AbsolutePath { get; }
        #endregion // Properties
    }

} // RSG.Pipeline.Services.Platform.Texture namespace
