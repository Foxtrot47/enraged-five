﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Content;
using RSG.Pipeline.Core;
using XGE = RSG.Interop.Incredibuild.XGE;


namespace RSG.Pipeline.Services
{
    /// <summary>
    /// Bits and bobs required by animation processors, need to be moved.
    /// </summary>
    public static class XGEUtil
    {
        public enum ToolType
        {
            TYPE_CLIP_GROUP,
            TYPE_COALESCE,
            TYPE_COMPRESS,
        }

        #region Constants
        private static readonly String LOG_CTX = "Animation:Utility";

        private static readonly String TOOL_COALESCE = "$(toolsbin)/anim/clipcoalesce.exe";
        private static readonly String TOOL_RENAME = "$(toolsbin)/anim/cliprename.exe";
        private static readonly String TOOL_COMPRESS = "$(toolsbin)/anim/animcompress.exe";
        private static readonly String TOOL_COMBINE = "$(toolsbin)/anim/animcombine.exe";
        private static readonly String TOOL_CONCAT = "$(toolsbin)/anim/animconcat.exe";
        private static readonly String TOOL_EDIT = "$(toolsbin)/anim/animedit.exe";
        private static readonly String TOOL_QUERY = "$(toolsbin)/anim/animquery.exe";
        private static readonly String TOOL_RUBY = "$(toolsbin)/ruby/bin/ruby.exe";
        private static readonly String TOOL_IRON_RUBY = "$(toolsbin)/ironruby/bin/ir.exe";

        private static readonly String TOOL_CUTSCENE_LIGHTMERGE = "$(toolsbin)/anim/cutscene/cutflightmerge.exe";
        private static readonly String TOOL_CUTSCENE_ANIMCOMBINE = "$(toolsbin)/anim/cutscene/cutfanimcombine.exe";
        private static readonly String TOOL_CUTSCENE_ANIMSECTION = "$(toolsbin)/anim/cutscene/cutfanimsection.exe";
        private static readonly String TOOL_CUTSCENE_SUBFIX = "$(toolsbin)/anim/cutscene/cutfsubfix.exe";
        private static readonly String TOOL_CUTSCENE_ANIMEDIT = "$(toolsbin)/cutscene/animedit.exe";
        private static readonly String TOOL_CUTSCENE_CLIPEDIT = "$(toolsbin)/cutscene/clipedit.exe";
        private static readonly String TOOL_CUTSCENE_CLIPXMLEDIT = "$(toolsbin)/anim/cutscene/clipxmledit.exe";
        private static readonly String TOOL_CUTSCENE_SECTION = "$(toolsbin)/anim/cutscene/cutfsection.exe";
        private static readonly String TOOL_CUTSCENE_CONCAT = "$(toolsbin)/anim/cutscene/cutfconcat.exe";
        private static readonly String TOOL_CUTSCENE_CLIPCONCAT = "$(toolsbin)/anim/cutscene/cutfclipconcat.exe";
        private static readonly String TOOL_CUTSCENE_MERGEEXTERNAL = "$(toolsbin)/anim/cutscene/cutfmergeexternal.exe";
        private static readonly String TOOL_CUTSCENE_REBUILD = "$(toolsbin)/anim/cutscene/cutfrebuild.exe";
        private static readonly String TOOL_CUTSCENE_MERGE = "$(toolsbin)/anim/cutscene/cutfmerge.exe";

        private static readonly String PATH_SKELETON = "$(toolsconfig)/config/anim/skeletons";
        private static readonly String PATH_TEMPLATE = "$(toolsconfig)/config/anim/compression_templates";

        private static readonly String FILE_SKEL_DOF = "$(toolsroot)/etc/config/anim/dof_injection_skeletons.xml";

        private const String CONTEXT_PREPARATION = @"Unpacking (?<src>[:\.\\/\w]*) to (?<dst>[:\.\\/\w]*)";
        private const String CONTEXT_COALESCE = @"Coalescing (?<src>[:\.\\/\w]*) to (?<dst>[:\.\\/\w]*)";
        private const String CONTEXT_COMPRESS = @"Compressing (?<src>[:\.\\/\w]*) to (?<dst>[:\.\\/\w]*)";
        private const String CONTEXT_POSTPROCESS = @"Packing (?<src>[:\.\\/\w]*) to (?<dst>[:\.\\/\w]*)";
        #endregion // Constants

        #region Static Member Data
        /// <summary>
        /// int storing packet counts for conversion tasks.
        /// </summary>
        private static int m_TaskCount = 0;

        private static readonly Regex REGEX_PREPARATION = new Regex(CONTEXT_PREPARATION, RegexOptions.Singleline | RegexOptions.IgnoreCase);
        private static readonly Regex REGEX_COALESCE    = new Regex(CONTEXT_COALESCE, RegexOptions.Singleline | RegexOptions.IgnoreCase);
        private static readonly Regex REGEX_COMPRESS    = new Regex(CONTEXT_COMPRESS, RegexOptions.Singleline | RegexOptions.IgnoreCase);
        private static readonly Regex REGEX_POSTPROCESS = new Regex(CONTEXT_POSTPROCESS, RegexOptions.Singleline | RegexOptions.IgnoreCase);
        private static readonly Regex REGEX_ERROR       = new Regex("Error: (.*)", RegexOptions.Singleline | RegexOptions.IgnoreCase);
        private static readonly Regex REGEX_WARN        = new Regex("Warning: (.*)", RegexOptions.Singleline | RegexOptions.IgnoreCase);
        private static readonly Regex REGEX_ULOG_ERROR = new Regex("\\[error] (.*)|\\[tool error] (.*)", RegexOptions.Singleline | RegexOptions.IgnoreCase);
        private static readonly Regex REGEX_ULOG_WARN = new Regex("\\[warn] (.*)", RegexOptions.Singleline | RegexOptions.IgnoreCase);
        private static readonly Regex REGEX_ULOG_EXCEPTION = new Regex("\\[tool exception] (.*)", RegexOptions.Singleline | RegexOptions.IgnoreCase);

        #endregion // Static Member Data

        #region Static Controller Methods

        public static void ResetTaskCounter()
        {
            m_TaskCount = 0;
        }

        #region CutsceneToolParser
        /// <summary>
        /// Parse Cutscene output errors, warnings and context into a log.
        /// </summary>
        /// <param name="log">Target log.</param>
        /// <param name="logData">log filename.</param>
        /// <param name="logCtx"></param>
        /// <param name="hasErrors"></param>
        public static void ParseCutsceneProcessorToolOutput(IUniversalLog log,
            IEnumerable<String> logData, String logCtx, out bool hasErrors)
        {
            hasErrors = false;
            try
            {
                foreach (String line in logData)
                {
                    Match match = REGEX_ULOG_ERROR.Match(line);
                    if (match.Success)
                    {
                        log.ErrorCtx(logCtx, match.Value);
                        hasErrors = true;
                        continue;
                    }
                    match = REGEX_ULOG_EXCEPTION.Match(line);
                    if (match.Success)
                    {
                        log.ErrorCtx(logCtx, "Unhandled Exception: {0}", match.Value);
                        hasErrors = true;
                        continue;
                    }
                    match = REGEX_ERROR.Match(line);
                    if (match.Success)
                    {
                        log.ErrorCtx(logCtx, match.Value);
                        hasErrors = true;
                        continue;
                    }
                    match = REGEX_ULOG_WARN.Match(line);
                    if (match.Success)
                    {
                        log.WarningCtx(logCtx, match.Value);
                        continue;
                    }
                    match = REGEX_WARN.Match(line);
                    if (match.Success)
                    {
                        log.WarningCtx(logCtx, match.Value);
                        continue;
                    }
                }
            }
            catch (Exception ex)
            {
                log.ToolExceptionCtx(LOG_CTX, ex, "Exception parsing cutscene tool output.");
            }
        }
        #endregion

        #region Grouping / Preparation
        /// <summary>
        /// Parse Preparation output errors, warnings and context into a log.
        /// </summary>
        /// <param name="log">Target log.</param>
        /// <param name="logfile">Preparation log filename.</param>
        public static void ParsePreparationToolOutput(IUniversalLog log,
            IEnumerable<String> logData, String logCtx, out bool hasErrors)
        {
            hasErrors = false;
            try
            {
                String contextSource = String.Empty;
                String contextDest = String.Empty;
                foreach (String line in logData)
                {
                    Match match = REGEX_PREPARATION.Match(line);
                    if (match.Success)
                    {
                        contextSource = match.Groups["src"].Value;
                        contextDest = match.Groups["dst"].Value;
                        continue;
                    }

                    match = REGEX_ERROR.Match(line);
                    if (match.Success)
                    {
                        log.ErrorCtx(LOG_CTX, "Error preparing asset: {0} to {1}.",
                            contextSource, contextDest);
                        log.ErrorCtx(LOG_CTX, match.Value);
                        hasErrors = true;
                        continue;
                    }

                    match = REGEX_WARN.Match(line);
                    if (match.Success)
                    {
                        log.WarningCtx(LOG_CTX, "Warning preparing asset: {0} to {1}.",
                            contextSource, contextDest);
                        log.WarningCtx(LOG_CTX, match.Value);
                        hasErrors = true;
                        continue;
                    }
                }
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception parsing preparation output.");
            }
        }

        /// <summary>
        /// Create an XGE task object for a grouping conversion process.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="processes"></param>
        /// <param name="tool"></param>
        /// <returns></returns>
        public static XGE.ITask GetGroupingTask(IUniversalLog log, IEnvironment environment,
            IProcess process, XGE.ITool tool)
        {
            String taskName = String.Format("Clip Group Packet [{0}]",
                m_TaskCount++);
            String safeTaskName = taskName.Replace(' ', '_');

            XGE.ITaskJob task = new XGE.TaskJob(taskName);
            //task.StopOnErrors = true;
            try
            {
                IFilesystemNode input = (IFilesystemNode)process.Inputs.ElementAt(0);
                IFilesystemNode output = (IFilesystemNode)process.Outputs.First();
                String inputPathStr = input.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                String outputPathStr = output.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                String modDir = String.Empty;
                String defaultSkel = String.Empty;
                if (process.AdditionalDependentContent.Any())
                {
                    IFilesystemNode inputMods = (IFilesystemNode)process.AdditionalDependentContent.ElementAt(0);
                    modDir = inputMods.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                    if (process.AdditionalDependentContent.Count() > 1)
                    {
                        IFilesystemNode inputSkeleton = (IFilesystemNode)process.AdditionalDependentContent.ElementAt(1);
                        defaultSkel = inputSkeleton.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                    }
                }

                String tool_rename = environment.Subst(TOOL_RENAME);
                String path_template = environment.Subst(PATH_TEMPLATE);
                String path_skel = environment.Subst(PATH_SKELETON);

                tool_rename = Path.GetFullPath(tool_rename).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                path_template = Path.GetFullPath(path_template).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                path_skel = Path.GetFullPath(path_skel).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                // Define the XGE task for the IProcess.
                process.Parameters.Add(Constants.ProcessXGE_TaskName, safeTaskName);
                process.Parameters.Add(Constants.ProcessXGE_Task, task);

                task.Caption = taskName;
                task.Parameters = String.Format("-zip {0} -outdir {1} -templatedir {2} -skeldir {3} -cliprename {4} -taskname {5}",
                        inputPathStr, outputPathStr, path_template, path_skel, tool_rename, safeTaskName);

                if (modDir != String.Empty)
                {
                    task.Parameters += String.Format(" -moddir {0}", modDir);
                }

                if (defaultSkel != String.Empty)
                {
                    task.Parameters += String.Format(" -defaultskeleton {0}", defaultSkel);
                }

                task.InheritParams = true;
                task.Tool = tool;
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception creating XGE task: {0}.", taskName);
            }
            return (task);
        }
        #endregion // Grouping / Preparation

        #region Coalesce

        /// <summary>
        /// Parse Coalesce output errors, warnings and context into a log.
        /// </summary>
        /// <param name="log">Target log.</param>
        /// <param name="logfile">Coalesce log filename.</param>
        public static void ParseCoalesceToolOutput(IUniversalLog log,
            IEnumerable<String> logData, String logCtx, out bool hasErrors)
        {
            hasErrors = false;
            try
            {
                String contextSource = String.Empty;
                String contextDest = String.Empty;

                foreach (String line in logData)
                {
                    Match match = REGEX_COALESCE.Match(line);
                    if (match.Success)
                    {
                        contextSource = match.Groups["src"].Value;
                        contextDest = match.Groups["dst"].Value;
                        continue;
                    }
                    match = REGEX_ERROR.Match(line);
                    if (match.Success)
                    {
                        log.ErrorCtx(LOG_CTX, "Error coalescing assets: {0} to {1}.",
                            contextSource, contextDest);
                        log.ErrorCtx(LOG_CTX, match.Value);
                        hasErrors = true;
                        continue;
                    }
                    match = REGEX_WARN.Match(line);
                    if (match.Success)
                    {
                        log.WarningCtx(LOG_CTX, "Warning coalescing assets: {0} to {1}.",
                            contextSource, contextDest);
                        log.WarningCtx(LOG_CTX, match.Value);
                        continue;
                    }
                }
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception parsing coalesce output.");
            }
        }

        /// <summary>
        /// Create an XGE task object for a grouping conversion process.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="processes"></param>
        /// <param name="tool"></param>
        /// <returns></returns>
        public static XGE.ITask GetCoalesceTask(IUniversalLog log, IEnvironment environment,
            IProcess process, XGE.ITool tool, IDictionary<String, Object> parameters)
        {
            String taskName = String.Format("Clip Coalesce Packet [{0}]",
                m_TaskCount++);
            String safeTaskName = taskName.Replace(' ', '_');

            XGE.ITaskJob task = new XGE.TaskJob(taskName);
            try
            {

                IFilesystemNode input = (IFilesystemNode)process.Inputs.First();
                IFilesystemNode output = (IFilesystemNode)process.Outputs.First();
                String inputPathStr = input.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                String outputPathStr = output.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                // Define the XGE task for the IProcess.
                process.Parameters.Add(Constants.ProcessXGE_TaskName, safeTaskName);
                process.Parameters.Add(Constants.ProcessXGE_Task, task);

                task.Caption = taskName;

                String tool_coalesce = environment.Subst(TOOL_COALESCE);
                String tool_edit = environment.Subst(TOOL_EDIT);
                String tool_query = environment.Subst(TOOL_QUERY);
                String tool_combine = environment.Subst(TOOL_COMBINE);
                String tool_dofinjection = environment.Subst(FILE_SKEL_DOF);

                tool_coalesce = Path.GetFullPath(tool_coalesce).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                tool_edit = Path.GetFullPath(tool_edit).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                tool_query = Path.GetFullPath(tool_query).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                tool_combine = Path.GetFullPath(tool_combine).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                tool_dofinjection = Path.GetFullPath(tool_dofinjection).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                if (!System.IO.Directory.Exists(outputPathStr))
                {
                    System.IO.Directory.CreateDirectory(outputPathStr);
                }

                if (parameters.Count == 0)
                {
                    task.Parameters = String.Format("-coalesce {0} -combine {1} -edit {2} -query {3} -inputdir {4} -outputdir {5} -framelimit {6} -dofinjection {7} -taskname {8} -memory",
                     tool_coalesce, tool_combine, tool_edit, tool_query, inputPathStr, outputPathStr, 2048, tool_dofinjection, safeTaskName);
                }
                else
                {
                    task.Parameters = String.Format("-coalesce {0} -combine {1} -edit {2} -query {3} -inputdir {4} -outputdir {5} -taskname {6}",
                     tool_coalesce, tool_combine, tool_edit, tool_query, inputPathStr, outputPathStr, safeTaskName);

                    foreach (KeyValuePair<string, Object> parameter in parameters)
                    {
                        task.Parameters += String.Format(" -{0} {1}", parameter.Key, environment.Subst(parameter.Value.ToString()));
                    }
                }

                task.InheritParams = false;
                task.Tool = tool;
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception creating XGE task: {0}.", taskName);
            }
            return (task);
        }
        #endregion // Coalesce

        #region Compression
        /// <summary>
        /// Parse Compression output errors, warnings and context into a log.
        /// </summary>
        /// <param name="log">Target log.</param>
        /// <param name="logData">Compression log filename.</param>
        /// <param name="logCtx"></param>
        /// <param name="hasErrors"></param>
        public static void ParseCompressionToolOutput(IUniversalLog log,
            IEnumerable<String> logData, String logCtx, out bool hasErrors)
        {
            hasErrors = false;
            try
            {
                String contextSource = String.Empty;
                String contextDest = String.Empty;

                foreach (String line in logData)
                {
                    Match match = REGEX_COMPRESS.Match(line);
                    if (match.Success)
                    {
                        contextSource = match.Groups["src"].Value;
                        contextDest = match.Groups["dst"].Value;
                        continue;
                    }

                    match = REGEX_ERROR.Match(line);
                    if (match.Success)
                    {
                        log.ErrorCtx(LOG_CTX, "Error compressing assets: {0} to {1}.",
                            contextSource, contextDest);
                        log.ErrorCtx(LOG_CTX, match.Value);
                        hasErrors = true;
                        continue;
                    }

                    match = REGEX_WARN.Match(line);
                    if (match.Success)
                    {
                        log.WarningCtx(LOG_CTX, "Warning compressing assets: {0} to {1}.",
                            contextSource, contextDest);
                        log.WarningCtx(LOG_CTX, match.Value);
                        continue;
                    }
                }
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception parsing compression output.");
            }
        }

        /// <summary>
        /// Create an XGE task object for a compression process.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="processes"></param>
        /// <param name="tool"></param>
        /// <returns></returns>
        public static XGE.ITask GetCompressionTask(IUniversalLog log, IEnvironment environment,
            IProcess process, XGE.ITool tool, IDictionary<String, Object> parameters)
        {
            String taskName = String.Format("Clip Compression Packet [{0}]",
                m_TaskCount++);
            String safeTaskName = taskName.Replace(' ', '_');

            XGE.ITaskJob task = new XGE.TaskJob(taskName);
            try
            {

                IFilesystemNode input = (IFilesystemNode)process.Inputs.First();
                IFilesystemNode output = (IFilesystemNode)process.Outputs.First();
                String inputPathStr = input.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                String outputPathStr = output.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                // Define the XGE task for the IProcess.
                process.Parameters.Add(Constants.ProcessXGE_TaskName, safeTaskName);
                process.Parameters.Add(Constants.ProcessXGE_Task, task);

                task.Caption = taskName;

                //String script_compress = environment.Subst(SCRIPT_COMPRESS);
                String tool_compress = environment.Subst(TOOL_COMPRESS);
                String path_skeleton = environment.Subst(PATH_SKELETON);
                String path_template = environment.Subst(PATH_TEMPLATE);

                //script_compress = Path.GetFullPath(script_compress).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                tool_compress = Path.GetFullPath(tool_compress).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                path_skeleton = Path.GetFullPath(path_skeleton).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                path_template = Path.GetFullPath(path_template).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                String defaultSkeleton = String.Empty;
                if ((process.AdditionalDependentContent).Any())
                {
                    IFilesystemNode inputMods = (IFilesystemNode)process.AdditionalDependentContent.ElementAt(0);
                    defaultSkeleton = inputMods.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                    // Overload the per platform compression - drop back to the default for ingame since its currently not setup
                    if ((process.AdditionalDependentContent).Count() > 1)
                    {
                        IFilesystemNode inputPlatform = (IFilesystemNode)process.AdditionalDependentContent.ElementAt(1);
                        path_template = inputPlatform.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                        path_template = Path.GetFullPath(path_template).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                    }
                }

                //System.IO.Directory.2CreateDirectory(outputPathStr);

                //task.SourceFile = script_compress;
                if (parameters.Count == 0)
                {
                    const int MAX_BLOCK_SIZE = 96 * 1024; //This has to match the CR_DEFAULT_MAX_BLOCK_SIZE define in animtolerance.h in RAGE.
                    task.Parameters = String.Format("-compress {0} -templatepath {1} -skeletonpath {2} -inputdir {3} -outputdir {4} -defaultskeleton {5} -taskname {6} -maxblocksize {7}",
                        tool_compress, path_template, path_skeleton, inputPathStr, outputPathStr, defaultSkeleton, safeTaskName, MAX_BLOCK_SIZE);
                }
                else
                {
                    task.Parameters = String.Format("-compress {0} -templatepath {1} -inputdir {2} -outputdir {3} -defaultskeleton {4} -taskname {5}",
                        tool_compress, path_template, inputPathStr, outputPathStr, defaultSkeleton, safeTaskName);

                    foreach(KeyValuePair<string, Object> parameter in parameters)
                    {
                        task.Parameters += String.Format(" -{0} {1}", parameter.Key, environment.Subst(parameter.Value.ToString()));
                    }
                }

                task.InheritParams = false;
                task.Tool = tool;
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception creating XGE task: {0}.", taskName);
            }
            return (task);
        }
        #endregion // Compression

        #region Clip Dictionary Processor
        /// <summary>
        /// Parse Compression output errors, warnings and context into a log.
        /// </summary>
        /// <param name="log">Target log.</param>
        /// <param name="logData">Compression log filename.</param>
        /// <param name="logCtx"></param>
        /// <param name="hasErrors"></param>
        public static void ParseClipDictionaryProcessorToolOutput(IUniversalLog log,
            IEnumerable<String> logData, String logCtx, out bool hasErrors)
        {
            hasErrors = false;
            try
            {
                String contextSource = String.Empty;
                String contextDest = String.Empty;

                foreach (String line in logData)
                {
                    Match match = REGEX_COMPRESS.Match(line);
                    if (match.Success)
                    {
                        contextSource = match.Groups["src"].Value;
                        contextDest = match.Groups["dst"].Value;
                        continue;
                    }

                    match = REGEX_ERROR.Match(line);
                    if (match.Success)
                    {
                        log.ErrorCtx(LOG_CTX, "Error compressing assets: {0} to {1}.",
                            contextSource, contextDest);
                        log.ErrorCtx(LOG_CTX, match.Value);
                        hasErrors = true;
                        continue;
                    }

                    match = REGEX_WARN.Match(line);
                    if (match.Success)
                    {
                        log.WarningCtx(LOG_CTX, "Warning compressing assets: {0} to {1}.",
                            contextSource, contextDest);
                        log.WarningCtx(LOG_CTX, match.Value);
                    }
                }
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception parsing compression output.");
            }
        }

        /// <summary>
        /// Create an XGE task object for a compression process.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="processes"></param>
        /// <param name="tool"></param>
        /// <returns></returns>
        public static XGE.ITask GetClipDictionaryProcessorTask(IUniversalLog log, IEnvironment environment,
            IProcess process, XGE.ITool tool, IDictionary<String, Object> parameters)
        {
            String taskName = String.Format("Clip Dictionary Processor Packet [{0}]",
                m_TaskCount++);
            String safeTaskName = taskName.Replace(' ', '_');

            XGE.ITaskJob task = new XGE.TaskJob(taskName);
            try
            {

                IFilesystemNode input = (IFilesystemNode)process.Inputs.First();
                IFilesystemNode output = (IFilesystemNode)process.Outputs.First();
                String inputPathStr = input.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                String outputPathStr = output.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                String outputZipStr = String.Empty;
                if (process.Outputs.Count() > 1)
                {
                    IFilesystemNode outputZip = (IFilesystemNode)process.Outputs.ElementAt(1);
                    outputZipStr = outputZip.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                }
                

                // Define the XGE task for the IProcess.
                process.Parameters.Add(Constants.ProcessXGE_TaskName, safeTaskName);
                process.Parameters.Add(Constants.ProcessXGE_Task, task);

                task.Caption = taskName;

                String tool_clipxmledit = environment.Subst(TOOL_CUTSCENE_CLIPXMLEDIT);
                String tool_rename = environment.Subst(TOOL_RENAME);
                String path_template = environment.Subst(PATH_TEMPLATE);
                String path_skel = environment.Subst(PATH_SKELETON);

                tool_clipxmledit = Path.GetFullPath(tool_clipxmledit).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                String modDir = String.Empty;
                String defaultSkel = String.Empty;
                if (process.AdditionalDependentContent.Any())
                {
                    IFilesystemNode inputMods = (IFilesystemNode)process.AdditionalDependentContent.ElementAt(0);
                    modDir = inputMods.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                    if ((process.AdditionalDependentContent).Count() > 1)
                    {
                        IFilesystemNode inputSkeleton = (IFilesystemNode)process.AdditionalDependentContent.ElementAt(1);
                        defaultSkel = inputSkeleton.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                    }
                }
                
                task.Parameters = String.Format("-clipxmledit {0} -inputzip {1} -outputdir {2} -templatedir {3} -skeldir {4} -cliprename {5} -taskname {6}",
                                        tool_clipxmledit, inputPathStr, outputPathStr, path_template, path_skel, tool_rename, safeTaskName);

                if (process.Parameters.ContainsKey("Grouping"))
                {
                    if((bool)process.Parameters["Grouping"] == true)
                        task.Parameters += " -group";
                }

                if (process.Parameters.ContainsKey("DLCPrefix"))
                {
                    task.Parameters += String.Format(" -dlcprefix {0}", process.Parameters["DLCPrefix"]);
                }

                if (outputZipStr != String.Empty)
                {
                    task.Parameters += String.Format(" -outputzip {0}", outputZipStr);
                }

                if (modDir != String.Empty)
                {
                    task.Parameters += String.Format(" -moddir {0}", modDir);
                }

                if (defaultSkel != String.Empty)
                {
                    task.Parameters += String.Format(" -defaultskeleton {0}", defaultSkel);
                }

                task.InheritParams = false;
                task.Tool = tool;
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception creating XGE task: {0}.", taskName);
            }
            return (task);
        }
        #endregion // Clip Dictionary

        #region Extract

        /// <summary>
        /// Parse Coalesce output errors, warnings and context into a log.
        /// </summary>
        /// <param name="log">Target log.</param>
        /// <param name="logfile">Coalesce log filename.</param>
        public static void ParseExtractToolOutput(IUniversalLog log,
            IEnumerable<String> logData, String logCtx, out bool hasErrors)
        {
            hasErrors = false;
            try
            {
                String contextSource = String.Empty;
                String contextDest = String.Empty;

                foreach (String line in logData)
                {
                    Match match = REGEX_COALESCE.Match(line);
                    if (match.Success)
                    {
                        contextSource = match.Groups["src"].Value;
                        contextDest = match.Groups["dst"].Value;
                        continue;
                    }
                    match = REGEX_ERROR.Match(line);
                    if (match.Success)
                    {
                        log.ErrorCtx(LOG_CTX, "Error coalescing assets: {0} to {1}.",
                            contextSource, contextDest);
                        log.ErrorCtx(LOG_CTX, match.Value);
                        hasErrors = true;
                        continue;
                    }
                    match = REGEX_WARN.Match(line);
                    if (match.Success)
                    {
                        log.WarningCtx(LOG_CTX, "Warning coalescing assets: {0} to {1}.",
                            contextSource, contextDest);
                        log.WarningCtx(LOG_CTX, match.Value);
                        continue;
                    }
                }
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception parsing coalesce output.");
            }
        }

        /// <summary>
        /// Create an XGE task object for asset extraction process.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="processes"></param>
        /// <param name="tool"></param>
        /// <returns></returns>
        public static XGE.ITask GetExtractTask(IUniversalLog log, IEnvironment environment,
            IProcess process, XGE.ITool tool)
        {
            IFilesystemNode input = ((IFilesystemNode)process.Inputs.First());
            IFilesystemNode output = ((IFilesystemNode)process.Outputs.First());
            String taskName = String.Format("Asset Extract Packet {0} [{1}]",
                m_TaskCount++, input.Name);
            String safeTaskName = taskName.Replace(' ', '_');

            XGE.ITaskJob task = new XGE.TaskJob(taskName);
            try
            {                
                String inputPathStr = input.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                String outputPathStr = output.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                // Define the XGE task for the IProcess.
                process.Parameters.Add(Constants.ProcessXGE_TaskName, safeTaskName);
                process.Parameters.Add(Constants.ProcessXGE_Task, task);

                task.Caption = taskName;
                task.InputFiles.AddRange(process.Inputs.OfType<Content.File>().Select(i => i.AbsolutePath));
                task.OutputFiles.AddRange(process.Outputs.OfType<Content.File>().Select(o => o.AbsolutePath));
                task.SourceFile = task.InputFiles.First();

                if (!System.IO.Directory.Exists(outputPathStr))
                {
                    System.IO.Directory.CreateDirectory(outputPathStr);
                }

                task.Parameters = String.Format("-outputdir {0} -taskname {1} -extractall -forceoverwrite -useparenttimestamp {2}",
                   outputPathStr, safeTaskName, inputPathStr);

                task.InheritParams = false;
                task.Tool = tool;
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception creating XGE task: {0}.", taskName);
            }
            return (task);
        }
        #endregion // Coalesce

        #region Post Process
        /// <summary>
        /// Parse Compression output errors, warnings and context into a log.
        /// </summary>
        /// <param name="log">Target log.</param>
        /// <param name="logfile">Compression log filename.</param>
        /// <param name="hasErrors"></param>
        public static void ParsePostProcessToolOutput(IUniversalLog log,
            IEnumerable<String> logData, String logCtx, out bool hasErrors)
        {
            hasErrors = false;
            try
            {
                String contextSource = String.Empty;
                String contextDest = String.Empty;

                foreach (String line in logData)
                {
                    Match match = REGEX_POSTPROCESS.Match(line);
                    if (match.Success)
                    {
                        contextSource = match.Groups["src"].Value;
                        contextDest = match.Groups["dst"].Value;
                        continue;
                    }
                    match = REGEX_ERROR.Match(line);
                    if (match.Success)
                    {
                        log.ErrorCtx(LOG_CTX, "Error packing assets: {0} to {1}.",
                            contextSource, contextDest);
                        log.ErrorCtx(LOG_CTX, match.Value);
                        hasErrors = true;
                        continue;
                    }
                    match = REGEX_WARN.Match(line);
                    if (match.Success)
                    {
                        log.WarningCtx(LOG_CTX, "Warning packing assets: {0} to {1}.",
                            contextSource, contextDest);
                        log.WarningCtx(LOG_CTX, match.Value);
                        continue;
                    }
                }
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception parsing postprocess output.");
            }
        }

        /// <summary>
        /// Create an XGE task object for the post process.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="processes"></param>
        /// <param name="tool"></param>
        /// <returns></returns>
        public static XGE.ITask GetPostProcessTask(IUniversalLog log, IEnvironment environment,
            IProcess process, XGE.ITool tool)
        {
            String taskName = String.Format("Post Process Packet [{0}]",
                m_TaskCount++);
            String safeTaskName = taskName.Replace(' ', '_');

            XGE.ITaskJob task = new XGE.TaskJob(taskName);
            try
            {

                IFilesystemNode input = ((IFilesystemNode)process.Inputs.First());
                IFilesystemNode output = ((IFilesystemNode)process.Outputs.First());
                String inputPathStr = input.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                String outputFile = output.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                process.Parameters.Add(Constants.ProcessXGE_TaskName, safeTaskName);
                process.Parameters.Add(Constants.ProcessXGE_Task, task);

                task.Caption = taskName;

                System.IO.Directory.CreateDirectory(Path.GetDirectoryName(outputFile));

                task.Parameters = String.Format("-inputdir {0} -outputfile {1} -taskname {2}",
                    inputPathStr, outputFile, safeTaskName);
                task.InheritParams = false;
                task.Tool = tool;
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception creating XGE task: {0}.", taskName);
            }
            return (task);
        }
        #endregion // Post Process

        #region Dictionary Build
        /// <summary>
        /// Parse Compression output errors, warnings and context into a log.
        /// </summary>
        /// <param name="log">Target log.</param>
        /// <param name="logfile">Compression log filename.</param>
        /// <param name="hasErrors"></param>
        public static void ParseDictionaryMetadataToolOutput(IUniversalLog log,
            IEnumerable<String> logData, String logCtx, out bool hasErrors)
        {
            hasErrors = false;
            try
            {
                String contextSource = String.Empty;
                String contextDest = String.Empty;

                foreach (String line in logData)
                {
                    Match match = REGEX_POSTPROCESS.Match(line);
                    if (match.Success)
                    {
                        contextSource = match.Groups["src"].Value;
                        contextDest = match.Groups["dst"].Value;
                        continue;
                    }
                    match = REGEX_ERROR.Match(line);
                    if (match.Success)
                    {
                        log.ErrorCtx(LOG_CTX, "Error packing assets: {0} to {1}.",
                            contextSource, contextDest);
                        log.ErrorCtx(LOG_CTX, match.Value);
                        hasErrors = true;
                        continue;
                    }
                    match = REGEX_WARN.Match(line);
                    if (match.Success)
                    {
                        log.WarningCtx(LOG_CTX, "Warning packing assets: {0} to {1}.",
                            contextSource, contextDest);
                        log.WarningCtx(LOG_CTX, match.Value);
                        continue;
                    }
                }
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception parsing postprocess output.");
            }
        }

        /// <summary>
        /// Create an XGE task object for the post process.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="processes"></param>
        /// <param name="tool"></param>
        /// <returns></returns>
        public static XGE.ITask GetDictionaryMetadataTask(IUniversalLog log, IEnvironment environment,
            IProcess process, XGE.ITool tool)
        {
            String taskName = String.Format("Dictionary Build Packet [{0}]",
                m_TaskCount++);
            String safeTaskName = taskName.Replace(' ', '_');

            XGE.ITaskJob task = new XGE.TaskJob(taskName);
            try
            {
                IFilesystemNode input = ((IFilesystemNode)process.Inputs.First());
                IFilesystemNode inputProcessed = ((IFilesystemNode)process.Inputs.ElementAt(1));
                IFilesystemNode output = ((IFilesystemNode)process.Outputs.First());
                String inputPathStr = input.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                String inputProcessedPathStr = inputProcessed.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                String outputFile = output.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                process.Parameters.Add(Constants.ProcessXGE_TaskName, safeTaskName);
                process.Parameters.Add(Constants.ProcessXGE_Task, task);

                task.Caption = taskName;

                System.IO.Directory.CreateDirectory(Path.GetDirectoryName(outputFile));

                task.Parameters = String.Format("-sourcedir {0} -processeddir {1} -outputfile {2} -taskname {3}",
                    inputPathStr, inputProcessedPathStr, outputFile, safeTaskName);
                task.InheritParams = false;
                task.Tool = tool;
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception creating XGE task: {0}.", taskName);
            }
            return (task);
        }
        #endregion // Post Process

        #region Cutscene Light Merge

        /// <summary>
        /// Create an XGE task object for a compression process.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="processes"></param>
        /// <param name="tool"></param>
        /// <returns></returns>
        public static XGE.ITask GetCutsceneLightMergeTask(IUniversalLog log, IEnvironment environment,
            IProcess process, XGE.ITool tool, IDictionary<String, Object> parameters)
        {
            String taskName = String.Format("Cutscene Light Merge Packet [{0}]",
                m_TaskCount++);
            String safeTaskName = taskName.Replace(' ', '_');

            XGE.ITaskJob task = new XGE.TaskJob(taskName);
            try
            {
                IFilesystemNode input = ((IFilesystemNode)process.Inputs.First());
                IFilesystemNode inputExtraction = ((IFilesystemNode)process.Inputs.ElementAt(1));
                IFilesystemNode output = ((IFilesystemNode)process.Outputs.First());
                String inputPathStr = input.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                String inputExtractionPathStr = inputExtraction.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                String outputPathStr = output.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                // Define the XGE task for the IProcess.
                process.Parameters.Add(Constants.ProcessXGE_TaskName, safeTaskName);
                process.Parameters.Add(Constants.ProcessXGE_Task, task);

                task.Caption = taskName;

                String tool_lightmerge = environment.Subst(TOOL_CUTSCENE_LIGHTMERGE);

                tool_lightmerge = Path.GetFullPath(tool_lightmerge).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                //System.IO.Directory.CreateDirectory(outputPathStr);

                task.Parameters = String.Format("-cutmerge {0} -inputfile {1} -outputfile {2} -lightfile {3} -taskname {4}",
                   tool_lightmerge, Path.Combine(inputPathStr, "data_stream.cutxml"), Path.Combine(outputPathStr, "data_stream.cutxml"), Path.Combine(inputExtractionPathStr, "data.lightxml"), safeTaskName);

                object removeDupeLights = false;
                if (parameters.TryGetValue("RemoveDupes", out removeDupeLights))
                {
                    if((bool)removeDupeLights == true)
                        task.Parameters += " -removedupes";
                }
                
                task.InheritParams = false;
                task.Tool = tool;
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception creating XGE task: {0}.", taskName);
            }
            return (task);
        }
        #endregion // Compression

        #region Cutscene Facial Merge

        /// <summary>
        /// Create an XGE task object for a compression process.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="processes"></param>
        /// <param name="tool"></param>
        /// <returns></returns>
        public static XGE.ITask GetCutsceneFacialMergeTask(IUniversalLog log, IEnvironment environment,
            IProcess process, XGE.ITool tool)
        {
            String taskName = String.Format("Cutscene Facial Merge Packet [{0}]",
                m_TaskCount++);
            String safeTaskName = taskName.Replace(' ', '_');

            XGE.ITaskJob task = new XGE.TaskJob(taskName);
            try
            {
                IFilesystemNode input = ((IFilesystemNode)process.Inputs.First());
                IFilesystemNode output = ((IFilesystemNode)process.Outputs.First());
                String inputPathStr = input.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                String outputPathStr = output.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                // Define the XGE task for the IProcess.
                process.Parameters.Add(Constants.ProcessXGE_TaskName, safeTaskName);
                process.Parameters.Add(Constants.ProcessXGE_Task, task);

                task.Caption = taskName;

                String tool_lightmerge = environment.Subst(TOOL_CUTSCENE_LIGHTMERGE);
                String tool_cutfanimcombine = environment.Subst(TOOL_CUTSCENE_ANIMCOMBINE);
                String tool_animcombine = environment.Subst(TOOL_COMBINE);
                String tool_animedit = environment.Subst(TOOL_CUTSCENE_ANIMEDIT);
                String tool_clipedit = environment.Subst(TOOL_CUTSCENE_CLIPEDIT);

                tool_lightmerge = Path.GetFullPath(tool_lightmerge).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                tool_cutfanimcombine = Path.GetFullPath(tool_cutfanimcombine).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                tool_animcombine = Path.GetFullPath(tool_animcombine).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                tool_animedit = Path.GetFullPath(tool_animedit).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                tool_clipedit = Path.GetFullPath(tool_clipedit).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                //System.IO.Directory.CreateDirectory(outputPathStr);

                task.Parameters = String.Format("-inputdir {0} -outputdir {1} -cutfile {2} -cutfanimcombine {3} -animcombine {4} -animedit {5} -clipedit {6} -taskname {7}",
                   inputPathStr, outputPathStr, Path.Combine(inputPathStr, "data.cutxml"), tool_cutfanimcombine, tool_animcombine, tool_animedit, tool_clipedit, safeTaskName);
                task.InheritParams = false;
                task.Tool = tool;
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception creating XGE task: {0}.", taskName);
            }
            return (task);
        }
        #endregion // Compression

        #region Cutscene Section

        /// <summary>
        /// Create an XGE task object for a cutscene section process.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="branch"></param>
        /// <param name="process"></param>
        /// <param name="tool"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static XGE.ITask GetCutsceneSectionTask(IUniversalLog log, IBranch branch,
            IProcess process, XGE.ITool tool, IDictionary<String, Object> parameters)
        {
            String taskName = String.Format("Cutscene Section Packet [{0}]",
                m_TaskCount++);
            String safeTaskName = taskName.Replace(' ', '_');

            IEnvironment environment = branch.Environment;
            XGE.ITaskJob task = new XGE.TaskJob(taskName);
            try
            {
                IFilesystemNode input = ((IFilesystemNode)process.Inputs.First());
                IFilesystemNode inputFace = ((IFilesystemNode)process.Inputs.ElementAt(1));
                IFilesystemNode output = ((IFilesystemNode)process.Outputs.First());
                String inputPathStr = input.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                String inputFacePathStr = inputFace.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                String outputPathStr = output.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                // Define the XGE task for the IProcess.
                process.Parameters.Add(Constants.ProcessXGE_TaskName, safeTaskName);
                process.Parameters.Add(Constants.ProcessXGE_Task, task);

                task.Caption = taskName;

                String tool_cutfanimsection = environment.Subst(TOOL_CUTSCENE_ANIMSECTION);
                String tool_animedit = environment.Subst(TOOL_CUTSCENE_ANIMEDIT);
                String tool_clipedit = environment.Subst(TOOL_CUTSCENE_CLIPEDIT);

                tool_cutfanimsection = Path.GetFullPath(tool_cutfanimsection).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                tool_animedit = Path.GetFullPath(tool_animedit).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                tool_clipedit = Path.GetFullPath(tool_clipedit).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                //System.IO.Directory.CreateDirectory(outputPathStr);

                task.Parameters = String.Format("-inputdir {0} -outputdir {1} -inputfacedir {2} -inputlightdir {3} -cutfanimsection {4} -animedit {5} -clipedit {6} -taskname {7}",
                   inputPathStr, outputPathStr, inputFacePathStr, inputFacePathStr, tool_cutfanimsection, tool_animedit, tool_clipedit, safeTaskName);

                object multiThreaded = false;
                parameters.TryGetValue("multithreaded", out multiThreaded);
                task.Parameters += String.Format(" -multithreaded {0}", (multiThreaded != null) ? multiThreaded.ToString() : "false");

                object projects;
                if (parameters.TryGetValue("UseOldMinimumSectionLengthForProjects", out projects))
                {
                    string[] projectsUsingOldMinimum = projects.ToString().Split(',');
                    if (projectsUsingOldMinimum.Contains(branch.Project.Name, StringComparer.OrdinalIgnoreCase))
                    {
                        task.Parameters += " -useOldMinSectionTime";
                    }
                }

                task.InheritParams = false;
                task.Tool = tool;
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception creating XGE task: {0}.", taskName);
            }
            return (task);
        }
        #endregion // Compression

        #region Cutscene Finalise

        /// <summary>
        /// Create an XGE task object for a compression process.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="processes"></param>
        /// <param name="tool"></param>
        /// <returns></returns>
        public static XGE.ITask GetCutsceneFinaliseTask(IEngineParameters param,
            IProcess process, XGE.ITool tool, IDictionary<String, Object> parameters)
        {
            String taskName = String.Format("Cutscene Finalise Packet [{0}]",
                m_TaskCount++);
            String safeTaskName = taskName.Replace(' ', '_');
            IUniversalLog log = param.Log;
            IEnvironment environment = param.Branch.Environment;

            XGE.ITaskJob task = new XGE.TaskJob(taskName);
            try
            {
                IFilesystemNode input = ((IFilesystemNode)process.Inputs.First());
                IFilesystemNode inputSceneName = ((IFilesystemNode)process.Inputs.ElementAt(1));
                IFilesystemNode inputFaceDir = ((IFilesystemNode)process.Inputs.ElementAt(2));
                IFilesystemNode output = ((IFilesystemNode)process.Outputs.First());
                String inputPathStr = input.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                String inputFacePathStr = inputFaceDir.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                String inputSceneNameStr = Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(inputSceneName.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar)));
                String outputPathStr = output.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                // Define the XGE task for the IProcess.
                process.Parameters.Add(Constants.ProcessXGE_TaskName, safeTaskName);
                process.Parameters.Add(Constants.ProcessXGE_Task, task);

                task.Caption = taskName;

                String tool_cutfsection = environment.Subst(TOOL_CUTSCENE_SECTION);
                
                tool_cutfsection = Path.GetFullPath(tool_cutfsection).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                //System.IO.Directory.CreateDirectory(outputPathStr);
                
                task.Parameters = String.Format("-inputdir {0} -inputfacedir {1}/faces -outputdir {2} -scenename {3} -cutfsection {4} -taskname {5}",
                   inputPathStr, inputFacePathStr, outputPathStr, inputSceneNameStr, tool_cutfsection, safeTaskName);

				task.Parameters += String.Format(" -branch {0}", param.Branch.Name);
                if (param.Branch.Project.IsDLC)
                {
                    task.Parameters += String.Format(" -dlc {0}", param.Branch.Project.Name);
                }

                bool enforceCameraCuts = true;
                object projects;
                if (parameters.TryGetValue("DontEnforceCameraCutForProjects", out projects))
                {
                    string[] projectsNotEnforced = projects.ToString().Split(',');
                    if (projectsNotEnforced.Contains(param.Branch.Project.Name, StringComparer.OrdinalIgnoreCase))
                    {
                        enforceCameraCuts = false;
                    }
                }

                if (enforceCameraCuts)
                {
                    task.Parameters += " -enforceAtLeastOneCameraCut";
                }

                task.InheritParams = false;
                task.Tool = tool;
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception creating XGE task: {0}.", taskName);
            }
            return (task);
        }
        #endregion // Compression

        #region Cutscene Post Process

        /// <summary>
        /// Create an XGE task object for a compression process.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="processes"></param>
        /// <param name="tool"></param>
        /// <returns></returns>
        public static XGE.ITask GetCutscenePostProcessTask(IUniversalLog log, IEnvironment environment,
            IProcess process, XGE.ITool tool)
        {
            String taskName = String.Format("Cutscene Post Process Packet [{0}]",
                m_TaskCount++);
            String safeTaskName = taskName.Replace(' ', '_');

            XGE.ITaskJob task = new XGE.TaskJob(taskName);
            try
            {
                IFilesystemNode input = ((IFilesystemNode)process.Inputs.First());
                IFilesystemNode inputSceneName = ((IFilesystemNode)process.Inputs.ElementAt(1));
                IFilesystemNode inputFinalise = ((IFilesystemNode)process.Inputs.ElementAt(2));
                IFilesystemNode output = ((IFilesystemNode)process.Outputs.First());
                String inputPathStr = input.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                String inputSceneNameStr = Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(inputSceneName.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar)));
                String inputFinalisePathStr = inputFinalise.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                String outputPathStr = output.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                // Define the XGE task for the IProcess.
                process.Parameters.Add(Constants.ProcessXGE_TaskName, safeTaskName);
                process.Parameters.Add(Constants.ProcessXGE_Task, task);

                task.Caption = taskName;

                System.IO.Directory.CreateDirectory(outputPathStr);

                task.Parameters = String.Format("-inputdir {0} -outputdir {1} -finalisedir {2} -scenename {3} -taskname {4}",
                   inputPathStr, outputPathStr, inputFinalisePathStr, inputSceneNameStr, safeTaskName);
                task.InheritParams = false;
                task.Tool = tool;
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception creating XGE task: {0}.", taskName);
            }
            return (task);
        }
        #endregion // Compression

        #region Cutscene Concatenation

        /// <summary>
        /// Create an XGE task object for a compression process.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="processes"></param>
        /// <param name="tool"></param>
        /// <returns></returns>
        public static XGE.ITask GetCutsceneConcatenationTask(IUniversalLog log, IEnvironment environment,
            IProcess process, XGE.ITool tool)
        {
            String taskName = String.Format("Cutscene Concatenation Packet [{0}]",
                m_TaskCount++);
            String safeTaskName = taskName.Replace(' ', '_');

            XGE.ITaskJob task = new XGE.TaskJob(taskName);
            try
            {
                String inputDirs = String.Empty;

                var enumerator = process.Inputs.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    IFilesystemNode input = ((IFilesystemNode)enumerator.Current);

                    inputDirs += input.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar) + ",";
                }

                //IFilesystemNode input = ((IFilesystemNode)process.Inputs.First());
                IFilesystemNode output = ((IFilesystemNode)process.Outputs.First());
                //String inputPathStr = input.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                String outputPathStr = output.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                // Define the XGE task for the IProcess.
                process.Parameters.Add(Constants.ProcessXGE_TaskName, safeTaskName);
                process.Parameters.Add(Constants.ProcessXGE_Task, task);

                task.Caption = taskName;

                String tool_animconcat = environment.Subst(TOOL_CONCAT);
                String tool_cutfclipconcat = environment.Subst(TOOL_CUTSCENE_CLIPCONCAT);
                String tool_cutfconcat = environment.Subst(TOOL_CUTSCENE_CONCAT);

                tool_animconcat = Path.GetFullPath(tool_animconcat).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                tool_cutfclipconcat = Path.GetFullPath(tool_cutfclipconcat).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                tool_cutfconcat = Path.GetFullPath(tool_cutfconcat).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                String sectionMethod = "0";
                String sectionDuration = "0";
                String audio = String.Empty;

                if (process.Parameters.ContainsKey(RSG.Pipeline.Core.Constants.Cutscene_Concat_Audio))
                {
                    audio = process.Parameters[RSG.Pipeline.Core.Constants.Cutscene_Concat_Audio].ToString();
                }

                if (process.Parameters.ContainsKey(RSG.Pipeline.Core.Constants.Cutscene_Concat_SectionMethod))
                {
                    sectionMethod = process.Parameters[RSG.Pipeline.Core.Constants.Cutscene_Concat_SectionMethod].ToString();
                }

                if (process.Parameters.ContainsKey(RSG.Pipeline.Core.Constants.Cutscene_Concat_SectionDuration))
                {
                    sectionDuration = process.Parameters[RSG.Pipeline.Core.Constants.Cutscene_Concat_SectionDuration].ToString();
                }

                //System.IO.Directory.CreateDirectory(outputPathStr);

                task.Parameters = String.Format("-inputdir {0} -outputdir {1} -audio {2} -sectionmethod {3} -sectionduration {4} -animconcat {5} -clipconcat {6} -cutconcat {7} -taskname {8}",
                   inputDirs, outputPathStr, audio, sectionMethod, sectionDuration, tool_animconcat, tool_cutfclipconcat, tool_cutfconcat, safeTaskName);
                task.InheritParams = false;
                task.Tool = tool;
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception creating XGE task: {0}.", taskName);
            }
            return (task);
        }
        #endregion // Compression

        #region Cutscene Merge

        /// <summary>
        /// Create an XGE task object for a merge process.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="processes"></param>
        /// <param name="tool"></param>
        /// <returns></returns>
        public static XGE.ITask GetCutsceneMergeTask(IUniversalLog log, IEnvironment environment,
            IProcess process, XGE.ITool tool)
        {
            String taskName = String.Format("Cutscene Merge Packet [{0}]",
                m_TaskCount++);
            String safeTaskName = taskName.Replace(' ', '_');

            XGE.ITaskJob task = new XGE.TaskJob(taskName);
            try
            {
                String inputDirs = String.Empty;

                var enumerator = process.Inputs.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    Content.IFilesystemNode input = ((Content.IFilesystemNode)enumerator.Current);

                    inputDirs += input.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar) + ",";
                }

                Content.IFilesystemNode output = ((Content.IFilesystemNode)process.Outputs.First());
                String outputPathStr = output.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                // Define the XGE task for the IProcess.
                process.Parameters.Add(Constants.ProcessXGE_TaskName, safeTaskName);
                process.Parameters.Add(Constants.ProcessXGE_Task, task);

                task.Caption = taskName;

                String tool_cutscenemerge = environment.Subst(TOOL_CUTSCENE_MERGE);

                tool_cutscenemerge = Path.GetFullPath(tool_cutscenemerge).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                
                //System.IO.Directory.CreateDirectory(outputPathStr);

                task.Parameters = String.Format("-inputdir {0} -outputdir {1} -cutfmerge {2} -taskname {3}",
                   inputDirs, outputPathStr, tool_cutscenemerge, safeTaskName);
                task.InheritParams = false;
                task.Tool = tool;
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception creating XGE task: {0}.", taskName);
            }
            return (task);
        }
        #endregion // Compression

        #region Cutscene Concatenation Lighting

        /// <summary>
        /// Create an XGE task object for a compression process.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="processes"></param>
        /// <param name="tool"></param>
        /// <returns></returns>
        public static XGE.ITask GetCutsceneConcatenationLightingTask(IUniversalLog log, IEnvironment environment,
            IProcess process, XGE.ITool tool)
        {
            String taskName = String.Format("Cutscene Concatenation Lighting Packet [{0}]",
                m_TaskCount++);
            String safeTaskName = taskName.Replace(' ', '_');

            XGE.ITaskJob task = new XGE.TaskJob(taskName);
            try
            {
                String inputDirs = String.Empty;

                var enumerator = process.Inputs.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    IFilesystemNode input = ((IFilesystemNode)enumerator.Current);

                    inputDirs += input.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar) + ",";
                }

                //IFilesystemNode input = ((IFilesystemNode)process.Inputs.First());
                IFilesystemNode output = ((IFilesystemNode)process.Outputs.First());
                //String inputPathStr = input.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                String outputPathStr = output.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                // Define the XGE task for the IProcess.
                process.Parameters.Add(Constants.ProcessXGE_TaskName, safeTaskName);
                process.Parameters.Add(Constants.ProcessXGE_Task, task);

                task.Caption = taskName;

                String tool_animconcat = environment.Subst(TOOL_CONCAT);
                String tool_cutfclipconcat = environment.Subst(TOOL_CUTSCENE_CLIPCONCAT);
                String tool_cutfconcat = environment.Subst(TOOL_CUTSCENE_CONCAT);
                String tool_cutflightmerge = environment.Subst(TOOL_CUTSCENE_LIGHTMERGE);

                tool_animconcat = Path.GetFullPath(tool_animconcat).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                tool_cutfclipconcat = Path.GetFullPath(tool_cutfclipconcat).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                tool_cutfconcat = Path.GetFullPath(tool_cutfconcat).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                tool_cutflightmerge = Path.GetFullPath(tool_cutflightmerge).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                //System.IO.Directory.CreateDirectory(outputPathStr);

                task.Parameters = String.Format("-inputdir {0} -outputdir {1} -animconcat {2} -clipconcat {3} -cutconcat {4} -cutlightmerge {5} -taskname {6}",
                   inputDirs, outputPathStr, tool_animconcat, tool_cutfclipconcat, tool_cutfconcat, tool_cutflightmerge, safeTaskName);
                task.InheritParams = false;
                task.Tool = tool;
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception creating XGE task: {0}.", taskName);
            }
            return (task);
        }
        #endregion // Compression

        #region Cutscene Merge Subtitles

        /// <summary>
        /// Create an XGE task object for a compression process.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="processes"></param>
        /// <param name="tool"></param>
        /// <returns></returns>
        public static XGE.ITask GetCutsceneMergeSubtitlesTask(IUniversalLog log, IEnvironment environment,
            IProcess process, XGE.ITool tool)
        {
            String taskName = String.Format("Cutscene Merge Subtitles Packet [{0}]",
                m_TaskCount++);
            String safeTaskName = taskName.Replace(' ', '_');
            
            XGE.ITaskJob task = new XGE.TaskJob(taskName);
            try
            {
                IFilesystemNode input = ((IFilesystemNode)process.Inputs.First());
                IFilesystemNode inputSceneName = ((IFilesystemNode)process.Inputs.ElementAt(1));
                IFilesystemNode output = ((IFilesystemNode)process.Outputs.First());
                String inputPathStr = input.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                String inputSceneNameStr = Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(inputSceneName.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar)));
                String outputPathStr = output.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                // Define the XGE task for the IProcess.
                process.Parameters.Add(Constants.ProcessXGE_TaskName, safeTaskName);
                process.Parameters.Add(Constants.ProcessXGE_Task, task);

                task.Caption = taskName;

                String tool_subfix = environment.Subst(TOOL_CUTSCENE_SUBFIX);

                tool_subfix = Path.GetFullPath(tool_subfix).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                //System.IO.Directory.CreateDirectory(outputPathStr);

                task.Parameters = String.Format("-cutfsubfix {0} -inputfile {1} -outputfile {2} -scenename {3} -taskname {4} -assets {5}",
                   tool_subfix, Path.Combine(inputPathStr, "data_stream.cutxml"), Path.Combine(outputPathStr, "data_stream.cutxml"), inputSceneNameStr, safeTaskName, environment.Subst("$(assets)"));
                task.InheritParams = false;
                task.Tool = tool;
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception creating XGE task: {0}.", taskName);
            }
            return (task);
        }
        #endregion // Merge Subtitles

        #region Cutscene Merge External

        /// <summary>
        /// Create an XGE task object for a compression process.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="processes"></param>
        /// <param name="tool"></param>
        /// <returns></returns>
        public static XGE.ITask GetCutsceneMergeExternalTask(IUniversalLog log, IEnvironment environment,
            IProcess process, XGE.ITool tool, IDictionary<String, Object> parameters)
        {
            String taskName = String.Format("Cutscene Merge External Packet [{0}]",
                m_TaskCount++);
            String safeTaskName = taskName.Replace(' ', '_');

            XGE.ITaskJob task = new XGE.TaskJob(taskName);
            try
            {
                IFilesystemNode input = ((IFilesystemNode)process.Inputs.First());
                IFilesystemNode inputSceneName = ((IFilesystemNode)process.Inputs.ElementAt(1));
                IFilesystemNode output = ((IFilesystemNode)process.Outputs.First());
                String inputPathStr = input.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                String inputSceneNameStr = Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(inputSceneName.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar)));
                String outputPathStr = output.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                // Define the XGE task for the IProcess.
                process.Parameters.Add(Constants.ProcessXGE_TaskName, safeTaskName);
                process.Parameters.Add(Constants.ProcessXGE_Task, task);

                task.Caption = taskName;

                String tool_mergeexternal = environment.Subst(TOOL_CUTSCENE_MERGEEXTERNAL);

                tool_mergeexternal = Path.GetFullPath(tool_mergeexternal).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                //System.IO.Directory.CreateDirectory(outputPathStr);

                task.Parameters = String.Format("-cutfmergeexternal {0} -inputfile {1} -outputfile {2} -scenename {3} -taskname {4} -assets {5}",
                tool_mergeexternal, Path.Combine(inputPathStr, "data.cutxml"), Path.Combine(outputPathStr, "data_stream.cutxml"), inputSceneNameStr, safeTaskName, environment.Subst("$(assets)"));

                object errorCheck;
                if (parameters.TryGetValue("errorcheck", out errorCheck))
                {
                    if ((bool)errorCheck == true)
                    {
                        task.Parameters += " -errorcheck";
                    }
                }
                
                task.InheritParams = false;
                task.Tool = tool;
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception creating XGE task: {0}.", taskName);
            }
            return (task);
        }
        #endregion // Merge Subtitles

        #region Cutscene Inject Dof

        /// <summary>
        /// Create an XGE task object for a compression process.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="processes"></param>
        /// <param name="tool"></param>
        /// <returns></returns>
        public static XGE.ITask GetCutsceneInjectDofTask(IUniversalLog log, IEnvironment environment,
            IProcess process, XGE.ITool tool)
        {
            String taskName = String.Format("Cutscene Inject Dof Packet [{0}]",
                m_TaskCount++);
            String safeTaskName = taskName.Replace(' ', '_');

            XGE.ITaskJob task = new XGE.TaskJob(taskName);
            try
            {
                IFilesystemNode input = ((IFilesystemNode)process.Inputs.First());
                IFilesystemNode output = ((IFilesystemNode)process.Outputs.First());
                String inputPathStr = input.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                String outputPathStr = output.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                // Define the XGE task for the IProcess.
                process.Parameters.Add(Constants.ProcessXGE_TaskName, safeTaskName);
                process.Parameters.Add(Constants.ProcessXGE_Task, task);

                task.Caption = taskName;

                String tool_animedit = environment.Subst(TOOL_CUTSCENE_ANIMEDIT);

                tool_animedit = Path.GetFullPath(tool_animedit).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                //System.IO.Directory.CreateDirectory(outputPathStr);

                task.Parameters = String.Format("-inputdir {0} -outputdir {1} -animedit {2} -taskname {3}",
                   inputPathStr, outputPathStr, tool_animedit, safeTaskName);
                task.InheritParams = false;
                task.Tool = tool;
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception creating XGE task: {0}.", taskName);
            }
            return (task);
        }
        #endregion // Merge Subtitles

        #region Cutscene Slice

        /// <summary>
        /// Create an XGE task object for a compression process.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="processes"></param>
        /// <param name="tool"></param>
        /// <returns></returns>
        public static XGE.ITask GetCutsceneSliceTask(IUniversalLog log, IEnvironment environment,
            IProcess process, XGE.ITool tool)
        {
            String taskName = String.Format("Cutscene Slice Packet [{0}]",
                m_TaskCount++);
            String safeTaskName = taskName.Replace(' ', '_');

            XGE.ITaskJob task = new XGE.TaskJob(taskName);
            try
            {
                IFilesystemNode input = ((IFilesystemNode)process.Inputs.First());
                IFilesystemNode inputSceneName = ((IFilesystemNode)process.Inputs.ElementAt(1));
                IFilesystemNode output = ((IFilesystemNode)process.Outputs.First());
                String inputPathStr = input.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                String outputPathStr = output.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                String inputSceneNameStr = Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(inputSceneName.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar)));
                String inputMissionNameStr = Path.GetFileName(Path.GetDirectoryName(inputSceneName.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar)));
                

                // Define the XGE task for the IProcess.
                process.Parameters.Add(Constants.ProcessXGE_TaskName, safeTaskName);
                process.Parameters.Add(Constants.ProcessXGE_Task, task);

                task.Caption = taskName;

                String tool_animedit = environment.Subst(TOOL_CUTSCENE_ANIMEDIT);
                String tool_rebuild = environment.Subst(TOOL_CUTSCENE_REBUILD);
                String tool_clipedit = environment.Subst(TOOL_CUTSCENE_CLIPEDIT);
                String tool_clipconcat = environment.Subst(TOOL_CUTSCENE_CLIPCONCAT);
                String tool_animconcat = environment.Subst(TOOL_CONCAT);
                String tool_cutfconcat = environment.Subst(TOOL_CUTSCENE_CONCAT);

                tool_animedit = Path.GetFullPath(tool_animedit).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                tool_rebuild = Path.GetFullPath(tool_rebuild).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                tool_clipedit = Path.GetFullPath(tool_clipedit).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                tool_clipconcat = Path.GetFullPath(tool_clipconcat).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                tool_animconcat = Path.GetFullPath(tool_animconcat).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                tool_cutfconcat = Path.GetFullPath(tool_cutfconcat).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                //System.IO.Directory.CreateDirectory(outputPathStr);

                task.Parameters = String.Format("-inputdir {0} -outputdir {1} -animedit {2} -clipedit {3} -clipconcat {4} -animconcat {5} -cutfconcat {6} -cutfrebuild {7} -scenename {8} -missionname {9} -taskname {10}",
                   inputPathStr, outputPathStr, tool_animedit, tool_clipedit, tool_clipconcat, tool_animconcat, tool_cutfconcat, tool_rebuild, inputSceneNameStr, inputMissionNameStr, safeTaskName);
                task.InheritParams = false;
                task.Tool = tool;
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception creating XGE task: {0}.", taskName);
            }
            return (task);
        }
        #endregion // Merge Subtitles

        #region Inject Prefix

        /// <summary>
        /// Create an XGE task object for a compression process.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="processes"></param>
        /// <param name="tool"></param>
        /// <returns></returns>
        public static XGE.ITask GetCutsceneInjectPrefixTask(IUniversalLog log, IEnvironment environment,
            IProcess process, XGE.ITool tool)
        {
            String taskName = String.Format("Cutscene Inject Prefix Packet [{0}]",
                m_TaskCount++);
            String safeTaskName = taskName.Replace(' ', '_');

            XGE.ITaskJob task = new XGE.TaskJob(taskName);
            try
            {
                IFilesystemNode inputAnims = ((IFilesystemNode)process.Inputs.First());
                IFilesystemNode inputMeta = ((IFilesystemNode)process.Inputs.ElementAt(1));
                IFilesystemNode output = ((IFilesystemNode)process.Outputs.First());
                String inputAnimPathStr = inputAnims.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                String inputMetaPathStr = inputMeta.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                String outputPathStr = output.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                // Define the XGE task for the IProcess.
                process.Parameters.Add(Constants.ProcessXGE_TaskName, safeTaskName);
                process.Parameters.Add(Constants.ProcessXGE_Task, task);

                task.Caption = taskName;

                task.Parameters = String.Format("-inputanimdir {0} -inputmetadir {1} -outputdir {2} -taskname {3}",
                   inputAnimPathStr, inputMetaPathStr, outputPathStr, safeTaskName);

                if (process.Parameters.ContainsKey("DLCName"))
                {
                    task.Parameters += String.Format(" -dlc {0}", process.Parameters["DLCName"]);
                }

                task.InheritParams = false;
                task.Tool = tool;
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception creating XGE task: {0}.", taskName);
            }
            return (task);
        }
        #endregion // Prefix

        /// <summary>
        /// Create output cache directory.
        /// </summary>
        /// <param name="process"></param>
        /// <returns></returns>
        public static void CreateCacheOutputDirectory(IProcess process)
        {
            if (process.Outputs.FirstOrDefault() != null)
            {
                RSG.Pipeline.Content.Directory dirContent = process.Outputs.First() as RSG.Pipeline.Content.Directory;

                if (!System.IO.Directory.Exists(dirContent.AbsolutePath))
                    System.IO.Directory.CreateDirectory(dirContent.AbsolutePath);
            }
        }

        #endregion // Static Controller Methods

        #region Private Static Methods
        /// <summary>
        /// Add the process input and output filenames to the XGE task.
        /// </summary>
        /// <param name="process"></param>
        /// <param name="task"></param>
        private static void AddProcessInputsAndOutputsToTask(IProcess process, ref XGE.ITaskJob task)
        {
            foreach (IContentNode node in process.Inputs)
            {
                if (!(node is IFilesystemNode))
                    continue;
                IFilesystemNode fsNode = (node as IFilesystemNode);
                if (fsNode is RSG.Pipeline.Content.Directory)
                {
                    task.InputFiles.Add(Path.Combine(fsNode.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar), (fsNode as RSG.Pipeline.Content.Directory).Wildcard));
                }
                else
                {
                    task.InputFiles.Add(fsNode.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar));
                }
            }
            foreach (IContentNode node in process.Outputs)
            {
                if (!(node is IFilesystemNode))
                    continue;
                IFilesystemNode fsNode = (node as IFilesystemNode);
                if (fsNode is RSG.Pipeline.Content.Directory)
                {
                    task.OutputFiles.Add(Path.Combine(fsNode.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar), (fsNode as RSG.Pipeline.Content.Directory).Wildcard));
                }
                else
                {
                    task.OutputFiles.Add(fsNode.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar));
                }
            }
        }
        #endregion // Private Static Methods
    }
}
