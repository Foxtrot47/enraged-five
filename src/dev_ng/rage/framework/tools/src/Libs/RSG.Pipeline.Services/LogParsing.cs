﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using RSG.Base.Logging.Universal;

namespace RSG.Pipeline.Services
{

    /// <summary>
    /// Log parsing methods.
    /// </summary>
    public static class LogParsing
    {
        #region Constants
        private static readonly Regex REGEX_RAGE_FATAL_ERROR = new Regex("Fatal Error: (.*)",
            RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase);
        private static readonly Regex REGEX_RAGE_ERROR = new Regex("Error: (.*)",
            RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase);
        private static readonly Regex REGEX_RAGE_WARN = new Regex("Warning: (.*)",
            RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase);
        
        private static readonly Regex REGEX_DOTNET_ERROR = new Regex(@".* \[error\] (.*)",
            RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase);
        private static readonly Regex REGEX_DOTNET_WARN = new Regex(@".* \[warn\] (.*)",
            RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase);
        #endregion // Constants

        #region Controller Methods
        /// <summary>
        /// Parse output errors, warnings and context into a log.
        /// </summary>
        /// <param name="log">Target log.</param>
        /// <param name="logCtx"></param>
        /// <param name="logfile">Log filename.</param>
        public static void ParseRageToolOutput(IUniversalLog log, String logCtx, String logfile)
        {
            log.ProfileCtx(logCtx, "Parsing: {0}.", logfile);
            try
            {
                if (!File.Exists(logfile))
                {
                    log.WarningCtx(logCtx, "Log file {0} does not exist for parsing.",
                        logfile);
                    return;
                }

                using (StreamReader fs = new StreamReader(logfile))
                {
                    String line = String.Empty;
                    while (null != (line = fs.ReadLine()))
                    {
                        ParseRageToolOutputLine(log, logCtx, line);
                    }
                }
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(logCtx, ex, "Exception parsing log: {0}.", logfile);
            }
            finally
            {
                log.ProfileEnd();
            }
        }

        /// <summary>
        /// Parse output errors, warnings and context into a log.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="logCtx"></param>
        /// <param name="logData"></param>
        /// <param name="hasErrors"></param>
        public static void ParseRageToolOutput(IUniversalLog log, String logCtx, 
            IEnumerable<String> logData, out bool hasErrors)
        {
            log.ProfileCtx(logCtx, "Parsing Rage tool output ({0} lines).", 
                logData.Count());
            hasErrors = false;
            try
            {
                bool outputStatus = true;
                foreach (String line in logData)
                    outputStatus &= ParseRageToolOutputLine(log, logCtx, line);
                hasErrors = !outputStatus;
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(logCtx, ex, "Exception parsing log output.");
            }
            finally
            {
                log.ProfileEnd();
            }
        }

        /// <summary>
        /// Parse .Net Log/UniversalLog Console output into a log (from a 
        /// separate process).
        /// </summary>
        /// <param name="log"></param>
        /// <param name="logCtx"></param>
        /// <param name="logData"></param>
        public static void ParseConsoleLogOutput(IUniversalLog log, String logCtx, 
            IEnumerable<String> logData, out bool hasErrors)
        {
            log.ProfileCtx(logCtx, "Parsing Rage tool output ({0} lines).",
                logData.Count());
            hasErrors = false;
            try
            {
                bool outputStatus = true;
                foreach (String line in logData)
                    outputStatus &= ParseConsoleLogOutputLine(log, logCtx, line);
                hasErrors = !outputStatus;
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(logCtx, ex, "Exception parsing log output.");
            }
            finally
            {
                log.ProfileEnd();
            }
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Parse a single line of a Rage tool's output.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="ctx"></param>
        /// <param name="line"></param>
        /// <returns>true for no errors; false otherwise.</returns>
        private static bool ParseRageToolOutputLine(IUniversalLog log, 
            String logCtx, String line)
        {
            Match match = REGEX_RAGE_FATAL_ERROR.Match(line);
            if (match.Success)
            {
                log.ErrorCtx(logCtx, match.Value);
                return (false);
            }                
            match = REGEX_RAGE_ERROR.Match(line);
            if (match.Success)
            {
                log.ErrorCtx(logCtx, match.Value);
                return (false);
            }
            match = REGEX_RAGE_WARN.Match(line);
            if (match.Success)
            {
                log.WarningCtx(logCtx, match.Value);
                return (true);
            }
            return (true);
        }

        /// <summary>
        /// Parse a single line of .Net Log/UniversalLog Console output.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="logCtx"></param>
        /// <param name="line"></param>
        /// <returns>true for no errors; false otherwise.</returns>
        private static bool ParseConsoleLogOutputLine(IUniversalLog log, 
            String logCtx, String line)
        {
            Match match = REGEX_DOTNET_ERROR.Match(line);
            if (match.Success)
            {
                log.ErrorCtx(logCtx, match.Value);
                return (false);
            }                
            match = REGEX_DOTNET_WARN.Match(line);
            if (match.Success)
            {
                log.WarningCtx(logCtx, match.Value);
                return (true);
            }
            return (true);
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Services namespace
