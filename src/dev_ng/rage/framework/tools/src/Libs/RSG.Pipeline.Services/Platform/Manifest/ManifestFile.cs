﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;

namespace RSG.Pipeline.Services.Platform.Manifest
{

    /// <summary>
    /// RPF manifest file container.
    /// </summary>
    public class ManifestFile
    {
        #region Constants
        /// <summary>
        /// Log context.
        /// </summary>
        internal static String LOG_CTX = "RPF Manifest";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Asset bindings collection.
        /// </summary>
        public ICollection<HDTXDAssetBinding> HDTXDAssetBindings
        {
            get;
            private set;
        }

        /// <summary>
        /// Collection of ITYP streaming dependencies (old format).
        /// </summary>
        public ICollection<IMAPDependency> IMAPDependencies
        {
            get;
            private set;
        }

        /// <summary>
        /// Collection of ITYP streaming dependencies.
        /// </summary>
        public ICollection<ITYPDependency2> ITYPDependencies2
        {
            get;
            private set;
        }

        /// <summary>
        /// Collection of ITYP streaming dependencies.
        /// </summary>
        public ICollection<IMAPDependency2> IMAPDependencies2
        {
            get;
            private set;
        }

        /// <summary>
        /// Collection of Interior bounds streaming dependencies.
        /// </summary>
        public ICollection<InteriorBounds> InteriorBounds
        {
            get;
            private set;
        }

        /// <summary>
        /// Log.
        /// </summary>
        private IUniversalLog Log
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="filename"></param>
        /// 
        public ManifestFile(IUniversalLog log, String filename)
        {
            this.Log = log;
            this.HDTXDAssetBindings = new List<HDTXDAssetBinding>();
            this.IMAPDependencies = new List<IMAPDependency>();
            this.ITYPDependencies2 = new List<ITYPDependency2>();
            this.IMAPDependencies2 = new List<IMAPDependency2>();
            this.InteriorBounds = new List<InteriorBounds>();

            imapGroupMap_ = new Dictionary<string, IMAPGroup>();

            this.Load(filename);
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Return whether there is data within this manifest object.
        /// </summary>
        /// <returns></returns>
        public bool HasData()
        {
            return (HDTXDAssetBindings.Count > 0 ||
                imapGroupMap_.Count > 0 ||
                IMAPDependencies.Count > 0 ||
                ITYPDependencies2.Count > 0 ||
                IMAPDependencies2.Count > 0 ||
                InteriorBounds.Count > 0);
        }

        /// <summary>
        /// Add map dependencies from a MapExportAdditions XML file.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public bool AddMapDependencies(String filename)
        {
            try
            {
                MapExportAdditions mapAdditions = new MapExportAdditions(filename);
                this.IMAPDependencies.AddRange(mapAdditions.IMAPDependencies);
                this.IMAPDependencies2.AddRange(mapAdditions.IMAPDependencies2);
                this.ITYPDependencies2.AddRange(mapAdditions.ITYPDependencies2);
                foreach (IMAPGroupMapData item in mapAdditions.IMAPGroupData)
                {
                    if (!imapGroupMap_.ContainsKey(item.Name))
                        imapGroupMap_.Add(item.Name, new IMAPGroup(item.Name));

                    imapGroupMap_[item.Name].Flags.AddRange(item.ActivationTypes);
                    imapGroupMap_[item.Name].WeatherTypes.AddRange(item.ActiveWeatherTypes);
                    imapGroupMap_[item.Name].ActiveHours = item.ActiveHours;
                }
            }
            catch (Exception ex)
            {
                this.Log.ExceptionCtx(LOG_CTX, ex, "Error add map dependencies from: {0}.",
                    filename);
                return (false);
            }
            return (true);
        }

        /// <summary>
        /// Add map dependencies from a MapExportAdditions XML file.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public bool AddBoundsDependencies(String filename)
        {
            try
            {
                BoundsAdditions boundsAdditions = new BoundsAdditions(filename);
                foreach (IMAPGroupBounds item in boundsAdditions.IMAPGroupBounds)
                {
                    if (!imapGroupMap_.ContainsKey(item.Name))
                        imapGroupMap_.Add(item.Name, new IMAPGroup(item.Name));

                    imapGroupMap_[item.Name].Bounds.AddRange(item.BoundsNames);
                }
                this.InteriorBounds.AddRange(boundsAdditions.InteriorBounds);
            }
            catch (Exception ex)
            {
                this.Log.ExceptionCtx(LOG_CTX, ex, "Error add map dependencies from: {0}.",
                    filename);
                return (false);
            }
            return (true);
        }

        /// <summary>
        /// Deserialise the manifest file.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public bool Load(String filename)
        {
            bool result = true;
            try
            {
                XDocument xmlDoc = XDocument.Load(filename);

                // Map Data Group Elements
                IEnumerable<XElement> mapDataGroupElements =
                    xmlDoc.Root.XPathSelectElements("/CPackFileMetaData/MapDataGroups/Item");
                foreach (XElement mapDataGroupElement in mapDataGroupElements)
                {
                    IMAPGroup imapGroup = IMAPGroup.FromManifest(mapDataGroupElement);
                    this.imapGroupMap_.Add(imapGroup.GroupName, imapGroup);
                }

                // HD TXD Asset Binding Elements
                IEnumerable<XElement> xmlHDTXDAssetBindingElems =
                    xmlDoc.Root.XPathSelectElements("/CPackFileMetaData/HDTxdBindingArray/Item");
                foreach (XElement xmlHDTXDAssetBindingElem in xmlHDTXDAssetBindingElems)
                {
                    this.HDTXDAssetBindings.Add(new HDTXDAssetBinding(xmlHDTXDAssetBindingElem));
                }

                // IMAP Dependency
                IEnumerable<XElement> xmlIMAPDependElems =
                    xmlDoc.Root.XPathSelectElements("/CPackFileMetaData/imapDependencies/Item");
                foreach (XElement xmlIMAPDependency in xmlIMAPDependElems)
                {
                    this.IMAPDependencies.Add(IMAPDependency.FromManifest(xmlIMAPDependency));
                }

                // IMAP Dependencies 2
                IEnumerable<XElement> xmlIMAPDependencyElems =
                    xmlDoc.Root.XPathSelectElements("/CPackFileMetaData/imapDependencies_2/Item");
                foreach (XElement xmlIMAPDependency in xmlIMAPDependencyElems)
                {
                    this.IMAPDependencies2.Add(IMAPDependency2.FromManifest(xmlIMAPDependency));
                }

                // ITYP Dependencies 2
                IEnumerable<XElement> xmlITYPDependencyElems =
                    xmlDoc.Root.XPathSelectElements("/CPackFileMetaData/itypDependencies_2/Item");
                foreach (XElement xmlITYPDependency in xmlITYPDependencyElems)
                {
                    this.ITYPDependencies2.Add(ITYPDependency2.FromManifest(xmlITYPDependency));
                }

                // Interiors
                IEnumerable<XElement> xmlInteriorElems =
                    xmlDoc.Root.XPathSelectElements("/CPackFileMetaData/Interiors/Item");
                foreach (XElement xmlInteriorElem in xmlInteriorElems)
                {
                    this.InteriorBounds.Add(RSG.Pipeline.Services.Platform.Manifest.InteriorBounds.FromManifest(xmlInteriorElem));
                }
            }
            catch (Exception)
            {
                result = false;
            }
            return (result);
        }

        /// <summary>
        /// Serialise the manifest file.
        /// </summary>
        /// <param name="filename"></param>
        public bool Save(String filename)
        {
            try
            {
                // B* 892520 - static interior bounds must be enabled here

                XDocument xmlDoc = new XDocument(
                    new XDeclaration("1.0", "utf-8", "yes"),
                    new XElement("CPackFileMetaData",
                        new XElement("MapDataGroups",
                            this.imapGroupMap_.Values.Select(imapGroup => imapGroup.AsCMapDataGroup("Item"))
                        ), // MapDataGroups
                        new XElement("HDTxdBindingArray",
                            this.HDTXDAssetBindings.Select(Binding => Binding.AsCHDTxdAssetBinding("Item"))
                        ), // HDTXDBindingArray
                        new XElement("imapDependencies",
                            this.IMAPDependencies.Select(dep => dep.AsCImapDependency("Item"))
                        ), // imapDependencies
                        new XElement("itypDependencies_2",
                            this.ITYPDependencies2.Select(dep => dep.AsCItypDependencies("Item"))
                        ), // itypDependencies2
                        new XElement("imapDependencies_2",
                            this.IMAPDependencies2.Select(dep => dep.AsCImapDependencies("Item"))
                        ), // imapDependencies2
                        new XElement("Interiors",
                            this.InteriorBounds.Select(interiorBounds => interiorBounds.AsCInteriorBoundsFile("Item"))
                        ) // Interiors
                    ) // CPackFileMetaData
                );

                xmlDoc.Save(filename);
            }
            catch (Exception ex)
            {
                this.Log.ExceptionCtx(LOG_CTX, ex, "Exception serialising RPF manifest: '{0}'.",
                    filename);
                return (false);
            }
            return (true);
        }
        #endregion // Controller Methods

        #region Private Data
        private Dictionary<String, IMAPGroup> imapGroupMap_;
        #endregion Private Data
    }

} // RSG.Pipeline.Services.Platform.Manifest namespace
