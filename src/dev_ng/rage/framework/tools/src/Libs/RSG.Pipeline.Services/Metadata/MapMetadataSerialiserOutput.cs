﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Xml.Linq;

namespace RSG.Pipeline.Services.Metadata
{

    /// <summary>
    /// Map Metadata Serialiser task output object.
    /// </summary>
    public class MapMetadataSerialiserOutput
    {
        #region Statics
        private static readonly char[] SplitChar = {';'};
        #endregion

        #region Properties
        /// <summary>
        /// Output prefix string.
        /// </summary>
        public String Prefix
        {
            get;
            private set;
        }

        /// <summary>
        /// Output directory absolute path.
        /// </summary>
        public String OutputDirectory
        {
            get;
            private set;
        }

        /// <summary>
        /// Whether we are generating archetype metadata (ITYP).
        /// </summary>
        public bool ExportArchetypes
        {
            get;
            private set;
        }

        /// <summary>
        /// Whether we are generating entity metadata (IMAP).
        /// </summary>
        public bool ExportEntities
        {
            get;
            private set;
        }

        /// <summary>
        /// Whether we need to force the ITYP dependency (used in props merging)
        /// </summary>
        public IEnumerable<string> ForcedDependencies
        {
            get; 
            private set;
        }

        /// <summary>
        /// Output preview mode.
        /// </summary>
        public bool Preview
        {
            get;
            private set;
        }

        /// <summary>
        /// What platform we are building this for.
        /// </summary>
        public String Platform
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public MapMetadataSerialiserOutput(String prefix, String outputDirectory,
            bool archetypes, bool entities, IEnumerable<string> forcedDependencies,bool preview, String platform)
        {
            this.Prefix = prefix;
            this.OutputDirectory = outputDirectory;
            this.ExportArchetypes = archetypes;
            this.ExportEntities = entities;
            this.ForcedDependencies = forcedDependencies;
            this.Preview = preview;
            this.Platform = platform;
        }

        /// <summary>
        /// Constructor; from XML element.
        /// </summary>
        /// <param name="xmlOutputElem"></param>
        public MapMetadataSerialiserOutput(XElement xmlOutputElem)
        {
            Debug.Assert(0 == String.Compare(xmlOutputElem.Name.LocalName, "Output"),
                "Invalid 'Output' element.");

            this.Prefix = xmlOutputElem.Attribute("name").Value;
            this.OutputDirectory = xmlOutputElem.Attribute("directory").Value;
            this.ExportArchetypes = bool.Parse(xmlOutputElem.Attribute("export_archetypes").Value);
            this.ExportEntities = bool.Parse(xmlOutputElem.Attribute("export_entities").Value);
            this.Preview = bool.Parse(xmlOutputElem.Attribute("preview").Value);
            this.Platform = xmlOutputElem.Attribute("platform").Value;

            var dependencies = xmlOutputElem.Attribute("forced_ityp_dependencies");
            this.ForcedDependencies = dependencies == null ? new string[0] : dependencies.Value.Split(SplitChar).Distinct();
        }
        #endregion // Constructor(s)

        #region Internal Methods
        /// <summary>
        /// Serialise output to an XElement.
        /// </summary>
        /// <returns></returns>
        internal XElement Serialise()
        {
            XElement xmlOutputElem = new XElement("Output",
                new XAttribute("name", this.Prefix),
                new XAttribute("directory", this.OutputDirectory),
                new XAttribute("export_archetypes", this.ExportArchetypes.ToString()),
                new XAttribute("export_entities", this.ExportEntities.ToString()),
                new XAttribute("preview", this.Preview.ToString()),
                new XAttribute("platform", this.Platform),
                new XAttribute("forced_ityp_dependencies", ForcedDependencies == null ? "" : string.Join(";", ForcedDependencies))
            ); // Output
            return (xmlOutputElem);
        }
        #endregion // Internal Methods
    }
    
} // RSG.Pipeline.Services.Metadata namespace
