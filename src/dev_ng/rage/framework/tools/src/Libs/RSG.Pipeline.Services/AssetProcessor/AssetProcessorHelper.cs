﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using RSG.Pipeline.Core;

namespace RSG.Pipeline.Services.AssetProcessor
{
    /// <summary>
    /// This static class provides helper functions to generate valid operation parameters for AssetProcessor task creation.
    /// </summary>
    public static class AssetProcessorHelper
    {
        /// <summary>
        /// Creates a valid Copy operation KVP for this Processor
        /// </summary>
        public static KeyValuePair<Operation, IEnumerable<IContentNode>> CreateOperationCopy()
        {
            return GetAssetProcessorOperation(Operation.Copy, null);
        }

        /// <summary>
        /// Creates a valid DrawableMerge operation KVP for this Processor
        /// </summary>
        public static KeyValuePair<Operation, IEnumerable<IContentNode>> CreateOperationDrawableMerge()
        {
            return GetAssetProcessorOperation(Operation.DrawableMerge, null);
        }

        /// <summary>
        /// Creates a valid RenameClip operation KVP for this Processor
        /// </summary>
        public static KeyValuePair<Operation, IEnumerable<IContentNode>> CreateOperationRenameAnimation(IEnumerable<IContentNode> elements)
        {
            return GetAssetProcessorOperation(Operation.RenameAnimation, elements);
        }

        /// <summary>
        /// Creates a valid ModulariseSkeleton operation KVP for this Processor
        /// </summary>
        public static KeyValuePair<Operation, IEnumerable<IContentNode>> CreateOperationModulariseSkeleton(IEnumerable<IContentNode> elements)
        {
            return GetAssetProcessorOperation(Operation.ModulariseSkeleton, elements);
        }

        /// <summary>
        /// Creates a valid AddFiles operation KVP for this Processor
        /// </summary>
        public static KeyValuePair<Operation, IEnumerable<IContentNode>> CreateOperationAddFiles(IEnumerable<IContentNode> elements)
        {
            return GetAssetProcessorOperation(Operation.AddFiles, elements);
        }

        /// <summary>
        /// Creates a valid MergeFiles operation KVP for this Processor
        /// </summary>
        public static KeyValuePair<Operation, IEnumerable<IContentNode>> CreateOperationMergeFiles(IEnumerable<IContentNode> elements)
        {
            return GetAssetProcessorOperation(Operation.MergeFiles, elements);
        }

        /// <summary>
        /// Creates a valid RemoveFiles operation KVP for this Processor
        /// </summary>
        public static KeyValuePair<Operation, IEnumerable<IContentNode>> CreateOperationRemoveFiles(IEnumerable<IContentNode> elements)
        {
            return GetAssetProcessorOperation(Operation.RemoveFiles, elements);
        }

        /// <summary>
        /// Creates a valid MaterialPreset operation KVP for this Processor
        /// </summary>
        public static KeyValuePair<Operation, IEnumerable<IContentNode>> CreateOperationMaterialPreset(IEnumerable<IContentNode> elements)
        {
            return GetAssetProcessorOperation(Operation.MaterialPreset, elements);
        }

        /// <summary>
        /// Generates a valid KVP for the given operation, with associated elements it should operate on.
        /// Copy and DrawableMerge are special cases, as they dont run on elements but directly on the inputs.
        /// Thus, their associated elements is an empty collection.
        /// </summary>
        /// <param name="operationType">Operation key</param>
        /// <param name="elements">The elements the AssetProcessor operation will run on.</param>
        /// <returns>A valid KVP for the operation provided and the elements it runs on.</returns>
        /// <exception cref="ArgumentException">If the operationType provided is not processed here, it will throw an ArgumentException.</exception>
        /// <exception cref="InvalidDataException">If the elements collection is null or empty, for the regular operations (apart from Copy and DrawableMerge).</exception>
        private static KeyValuePair<Operation, IEnumerable<IContentNode>> GetAssetProcessorOperation(Operation operationType, IEnumerable<IContentNode> elements)
        {
            switch (operationType)
            {
                case Operation.Copy:
                    return new KeyValuePair<Operation, IEnumerable<IContentNode>>(Operation.Copy, new IContentNode[0]);
                case Operation.DrawableMerge:
                    return new KeyValuePair<Operation, IEnumerable<IContentNode>>(Operation.DrawableMerge, new IContentNode[0]);
                case Operation.RenameAnimation:
                case Operation.AddFiles:
                case Operation.MergeFiles:
                case Operation.RemoveFiles:
                case Operation.MaterialPreset:
                case Operation.ModulariseSkeleton:
                    if (elements == null || !elements.Any())
                        throw new InvalidDataException(string.Format("Empty element collection supplied;this operation can't be created; {0}", operationType));
                    return new KeyValuePair<Operation, IEnumerable<IContentNode>>(operationType, elements);
                default:
                    throw new ArgumentException(string.Format("Unknown Operation type: {0}.", operationType));
            }
        }
    }
}
