﻿//---------------------------------------------------------------------------------------------
// <copyright file="MapMetadataSerialiserInput.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Services.Metadata
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Xml.Linq;
    
    /// <summary>
    /// Map Metadata Serialiser input object (for non-merge tasks).
    /// </summary>
    public class MapMetadataSerialiserInput
    {
        #region Properties
        /// <summary>
        /// SceneXml absolute filename.
        /// </summary>
        public String SceneFilename
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="sceneFilename"></param>
        public MapMetadataSerialiserInput(String sceneFilename)
        {
            this.SceneFilename = sceneFilename;
        }

        /// <summary>
        /// Constructor; from XML element.
        /// </summary>
        /// <param name="xmlInputElem"></param>
        public MapMetadataSerialiserInput(XElement xmlInputElem)
        {
            Debug.Assert(0 == String.Compare(xmlInputElem.Name.LocalName, "Input"),
                "Invalid 'Input' element.");

            this.SceneFilename = xmlInputElem.Attribute("scenexml").Value;
        }
        #endregion // Constructor(s)

        #region Internal Methods
        /// <summary>
        /// Serialise input to an XElement.
        /// </summary>
        /// <returns></returns>
        internal XElement Serialise()
        {
            XElement xmlInputElem = new XElement("Input",
                new XAttribute("scenexml", this.SceneFilename)
            ); // Input
            return (xmlInputElem);
        }
        #endregion // Internal Methods
    }

} // RSG.Pipeline.Services.Metadata namespace
