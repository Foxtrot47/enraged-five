﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;

namespace RSG.Pipeline.Services.Metadata
{

    /// <summary>
    /// Map Metadata Serialiser task base class; shared data between regular
    /// and merge tasks.
    /// </summary>
    public abstract class MapMetadataSerialiserTaskBase
    {
        #region Properties
        /// <summary>
        /// Task name.
        /// </summary>
        public String Name
        {
            get;
            private set;
        }

        /// <summary>
        /// Task dependencies.
        /// </summary>
        /// DHM TODO: consider collapsing dependencies together -identifiable via class?
        public IEnumerable<MapMetadataSerialiserSceneXmlDependency> Dependencies
        {
            get;
            private set;
        }

        /// <summary>
        /// Additional manifest dependencies.
        /// </summary>
        /// DHM TODO: consider collapsing dependencies together -identifiable via class?
        public IEnumerable<MapMetadataSerialiserManifestDependency> AdditionalManifestDependencies
        {
            get;
            private set;
        }

        /// <summary>
        /// Task Core Game Metadata Zip dependencies (for DLC-only).
        /// </summary>
        /// DHM TODO: consider collapsing dependencies together -identifiable via class?
        public IEnumerable<MapMetadataSerialiserCoreGameMetadataZipDependency> CoreGameMetadataZipDependencies
        {
            get;
            private set;
        }

        /// <summary>
        /// Asset Combine data filename.
        /// </summary>
        public String AssetCombineFilename
        {
            get;
            private set;
        }

        /// <summary>
        /// Map Export Additions filename.
        /// </summary>
        public String MapExportAdditionsFilename
        {
            get;
            private set;
        }

        /// <summary>
        /// Task output(s).
        /// </summary>
        public IEnumerable<MapMetadataSerialiserOutput> Output
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="dependencies"></param>
        /// <param name="additionalManifestDependencies"></param>
        public MapMetadataSerialiserTaskBase(String name,
            IEnumerable<MapMetadataSerialiserSceneXmlDependency> dependencies,
            IEnumerable<MapMetadataSerialiserManifestDependency> additionalManifestDependencies,
            IEnumerable<MapMetadataSerialiserCoreGameMetadataZipDependency> metadataDependencies,
            IEnumerable<MapMetadataSerialiserOutput> output, 
            String assetCombineFilename,
            String mapExportAdditionsFilename)
        {
            this.Name = name;
            this.Dependencies = dependencies;
            this.AdditionalManifestDependencies = additionalManifestDependencies;
            this.CoreGameMetadataZipDependencies = metadataDependencies;
            this.Output = output;
            this.AssetCombineFilename = assetCombineFilename;
            this.MapExportAdditionsFilename = mapExportAdditionsFilename;
        }

        /// <summary>
        /// Constructor; from serialised XML configuration file.
        /// </summary>
        /// <param name="xmlTaskElem"></param>
        public MapMetadataSerialiserTaskBase(XElement xmlTaskElem)
        {
            this.Name = xmlTaskElem.Attribute("name").Value;
            this.AssetCombineFilename = xmlTaskElem.Attribute("asset_combine").Value;
            this.MapExportAdditionsFilename = xmlTaskElem.Attribute("map_export_additions").Value;

            this.Dependencies = xmlTaskElem.XPathSelectElements(MapMetadataSerialiserSceneXmlDependency.DEPENDENCY_SCENEXML).
                Select(xmlDependencyElem => new MapMetadataSerialiserSceneXmlDependency(xmlDependencyElem));
            this.AdditionalManifestDependencies = xmlTaskElem.XPathSelectElements(MapMetadataSerialiserManifestDependency.DEPENDENCY_MANFIEST).
                Select(xmlAdditionalManifestDep => new MapMetadataSerialiserManifestDependency(xmlAdditionalManifestDep));
            this.CoreGameMetadataZipDependencies = xmlTaskElem.XPathSelectElements(MapMetadataSerialiserCoreGameMetadataZipDependency.DEPENDENCY_COREGAMEMETADATAZIP).
                Select(xmlDependencyElem => new MapMetadataSerialiserCoreGameMetadataZipDependency(xmlDependencyElem));
                        
            this.Output = xmlTaskElem.XPathSelectElements("Output").
                Select(xmlOutputElem => new MapMetadataSerialiserOutput(xmlOutputElem));
        }
        #endregion // Constructor(s)

        #region Abstract Methods
        /// <summary>
        /// Serialise task to an XElement.
        /// </summary>
        /// <returns></returns>
        public abstract XElement Serialise();
        #endregion // Abstract Methods

        #region Internal Methods
        /// <summary>
        /// Serialise task to an XElement.
        /// </summary>
        /// <param name="elementName"></param>
        /// <param name="inputsNode"></param>
        /// <returns></returns>
        protected XElement Serialise(String elementName, IEnumerable<XElement> inputsNodes)
        {
            XElement xmlTaskElem = new XElement(elementName,
                new XAttribute("name", this.Name),
                new XAttribute("asset_combine", this.AssetCombineFilename),
                new XAttribute("map_export_additions", this.MapExportAdditionsFilename),
                this.Dependencies.Select(dependency => dependency.Serialise()),
                this.AdditionalManifestDependencies.Select(manifestDependency => manifestDependency.Serialise()),
                this.CoreGameMetadataZipDependencies.Select(dep => dep.Serialise()),
                inputsNodes,
                this.Output.Select(output => output.Serialise())
            ); // Task
            return (xmlTaskElem);
        }
        #endregion // Internal Methods
    }

} // RSG.Pipeline.Services.Metadata namespace
