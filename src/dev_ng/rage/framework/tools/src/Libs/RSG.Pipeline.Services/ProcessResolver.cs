﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using SIO = System.IO;
using System.Linq;
using RSG.Base.Extensions;
using RSG.Pipeline.Content;
using RSG.Pipeline.Content.Algorithm;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Build;
using RSG.Platform;

namespace RSG.Pipeline.Services
{

    /// <summary>
    /// Asset Pipeline Process Resolver.
    /// </summary>
    public static class ProcessResolver
    {
        #region Constants
        /// <summary>
        /// Default IProcessor fully-qualified class name.
        /// </summary>
        private static readonly String DEFAULT_PROCESSOR = "RSG.Pipeline.Processor.Platform.Rage";

        /// <summary>
        /// Log context.
        /// </summary>
        private static readonly String LOG_CTX = "Process Resolver";

        /// <summary>
        /// Invalid export extensions; error and do not create default processes for.
        /// </summary>
        private static readonly String[] DEFAULT_INVALID_EXTENSIONS = new String[] { ".rpf" };
        #endregion // Constants
        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <param name="tree"></param>
        /// <param name="processors"></param>
        /// <param name="filenames"></param>
        /// <param name="assetBuilderOverride">THIS IS ONLY FOR ASSET BUILDER USE. DO NOT USE IF YOU ARE NOT AN ASSET BUILDER.</param>
        /// <returns></returns>
        public static IEnumerable<IProcess> ResolveProcessRecursive(IEngineParameters param,
            IContentTree tree, IEnumerable<String> filenames, bool assetBuilderOverride = false)
        {
            // Initial resolve on filenames.
            IEnumerable<IProcess> tempResolved = ProcessResolver.Resolve(param, tree, filenames);
            List<IProcess> resolvedProcesses = new List<IProcess>(tempResolved);

#warning Flo: HACK. This flag prevents any recursive resolve for other processor than OutfitPreProcess. Because. (from 1 to 10 in term of ugliness, this one goes to 11)
            bool allowRecursiveResolve = resolvedProcesses.Any(p => p.ProcessorClassName == "RSG.Pipeline.Processor.Character.OutfitPreProcess");

#warning Flo/DHM: Hack; assetBuilderOverride is here to allow assetbuilders to quickly get the results of inputs, to delete unnecessary RPFs. (12 on the ugly-hack-scale)
            while (tempResolved.Any() && (allowRecursiveResolve || assetBuilderOverride))
            {
                // TODO Hack : maybe use $(export)/$(processed) check on inputs rather than $(target) check on output?
                IEnumerable<IContentNode> outputs = tempResolved.SelectMany(p => p.Outputs);
                IEnumerable<String> outputFilenames = outputs.OfType<IFilesystemNode>().
                    Where(n => !n.AbsolutePath.StartsWith("$(target)")).
                    Select(n => n.AbsolutePath);

                tempResolved = ProcessResolver.Resolve(param, tree, outputFilenames);
                resolvedProcesses.AddRange(tempResolved);
            }

            return (resolvedProcesses);
        }

        /// <summary>
        /// Resolve a set of input filenames to a collection of IProcess objects 
        /// so that the asset pipeline can build them.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="tree"></param>
        /// <param name="processors"></param>
        /// <param name="filenames"></param>
        /// <returns></returns>
        /// <see cref="ResolveDefault" />
        /// 
        /// This has been abstracted into a service so that other pipeline 
        /// applications can utilise its functionality (e.g. Asset Builder).
        /// 
        /// Note: if the tree is empty then we should return a 'Rage' converter
        /// process (see ResolveDefault).
        /// 
        public static IEnumerable<IProcess> Resolve(IEngineParameters param, 
            IContentTree tree, IEnumerable<String> filenames)
        {
            ICollection<String> unresolvedFilenames = new List<String>();
            ICollection<IProcess> resolvedProcesses = new List<IProcess>();
            ICollection<IContentNode> additionalInputs = new List<IContentNode>();
            foreach (String filename in filenames)
            {
                param.Log.Message(LOG_CTX, "Input filename: {0}.", filename);
                ICollection<IContentNode> nodes = new List<IContentNode>();
                if (tree.HasParentDirectory(filename))
                {
                    // First see if we have a wildcard specific directory node, otherwise grab all of them.
                    IContentNode parentDir = tree.GetParentDirectoryFromWildcard(filename);

                    if (parentDir != null)
                    {
                        nodes.Add(parentDir);

                        if (!parentDir.Parameters.ContainsKey(Constants.Convert_Filenames))
                            parentDir.Parameters.Add(Constants.Convert_Filenames, new List<String>());
                        ((List<String>)parentDir.Parameters[Constants.Convert_Filenames]).Add(filename);
                    }
                    else
                        nodes.AddRange(tree.GetParentDirectories(filename));

                    if (param.Flags.HasFlag(EngineFlags.Preview))
                    {
                        // If we already have a static directory; just add more
                        // files.  This allows us to preview multiple content 
                        // files from the same directory (e.g. vehicles).
                        IContentNode first = nodes.First();
                        if (first is StaticDirectory)
                        {
                            (first as StaticDirectory).StaticFiles.Add(tree.CreateFile(filename));
                        }
                        else
                        {
                            StaticDirectory staticDir = tree.CreateStaticDirectoryFromDirectory(first as Directory) as StaticDirectory;
                            staticDir.StaticFiles.Add(tree.CreateFile(filename));
                            nodes.Clear();
                            nodes.Add(staticDir);
                        }
                    }
                }
                else if (tree.IsDirectory(filename))
                {
                    IEnumerable<IContentNode> nodeDirectories = tree.GetDirectories(filename);

                    if (nodeDirectories != null)
                    {
                        foreach (IContentNode dirNode in nodeDirectories.ToList<IContentNode>())
                        {
                            if (!SIO.Directory.Exists(filename) && !dirNode.Parameters.ContainsKey(Constants.Content_State))
                                dirNode.Parameters.Add(Constants.Content_State, ContentState.Deleted);
                            else if (!dirNode.Parameters.ContainsKey(Constants.Content_State))
                                dirNode.Parameters.Add(Constants.Content_State, ContentState.Modified);

                            additionalInputs.Add(dirNode);
                        }
                    }
                    else
                    {
                        IContentNode node = tree.CreateDirectory(filename);
                        if (!SIO.Directory.Exists(filename) && !node.Parameters.ContainsKey(Constants.Content_State))
                            node.Parameters.Add(Constants.Content_State, ContentState.Deleted);
                        else if (!node.Parameters.ContainsKey(Constants.Content_State))
                            node.Parameters.Add(Constants.Content_State, ContentState.Modified);
                        nodes.Add(node);
                    }
                }
                else
                {
                    IContentNode node = tree.CreateFile(filename);
                    if (!SIO.File.Exists(filename) && !node.Parameters.ContainsKey(Constants.Content_State))
                        node.Parameters.Add(Constants.Content_State, ContentState.Deleted);
                    else if (!node.Parameters.ContainsKey(Constants.Content_State))
                        node.Parameters.Add(Constants.Content_State, ContentState.Modified);
                    nodes.Add(node);
                }

                foreach (IContentNode node in nodes)
                {
                    IEnumerable<IProcess> processes = tree.FindProcessesWithInput(node);
                    if (processes.Any())
                        resolvedProcesses.AddRange(processes);
                    else
                        unresolvedFilenames.Add(filename);
                }

                foreach (IContentNode additionalNode in additionalInputs)
                {
                    IEnumerable<IProcess> additionalProcesses = tree.FindProcessesWithInput(additionalNode);
                    if (additionalProcesses.Any())
                        resolvedProcesses.AddRange(additionalProcesses);
                    else
                        unresolvedFilenames.Add(filename);
                }
            }

            // Fallback for simple platform conversion (Rage) processes.  This
            // will get hit for nodes not within the content-tree.  Note: we
            // now need the DefaultResolverFallback flag.
            if (param.Flags.HasFlag(EngineFlags.DefaultResolverFallback) && unresolvedFilenames.Any())
            {
                IEnumerable<IProcess> defaultProcesses =
                    ProcessResolver.ResolveDefault(param, tree, unresolvedFilenames);
                resolvedProcesses.AddRange(defaultProcesses);
            }
            else if (unresolvedFilenames.Any())
            {
                param.Log.Warning("Unresolved files ({0}) but Default Resolver is disabled.", 
                    unresolvedFilenames.Count);
            }
            else
            {
                param.Log.Message("All filenames resolved to {0} processes.", 
                    resolvedProcesses.Count);
            }

            return (resolvedProcesses);
        }

        /// <summary>
        /// Resolve a set of input filenames to a collection of IProcess objects
        /// that do the default Platform (RAGE) conversion.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="tree"></param>
        /// <param name="processors"></param>
        /// <param name="filenames"></param>
        /// <returns></returns>
        /// <see cref="Resolve" />
        /// 
        public static IEnumerable<IProcess> ResolveDefault(IEngineParameters param,
            IContentTree tree, IEnumerable<String> filenames)
        {
            // Setup a Rage Platform process for the data mapping it into 
            // the target directories appropriately.
            ICollection<IProcess> resolvedProcesses = new List<IProcess>();

            // Preprocess our filenames; if we have directories then we expand
            // this using a single-level file search (non-recursive).
            List<String> normalisedFilenames = new List<String>();
            foreach (String filename in filenames)
            {
                String normalisedFilename = SIO.Path.GetFullPath(filename).ToLower();
                if (SIO.Directory.Exists(normalisedFilename))
                {
                    String[] directoryFilenames = SIO.Directory.GetFiles(normalisedFilename, "*.*",
                        SIO.SearchOption.TopDirectoryOnly);
                    normalisedFilenames.AddRange(directoryFilenames.Select(f => SIO.Path.GetFullPath(f).ToLower()));
                }
                else
                {
                    // Regular file; we just add it provided its not marked as invalid.
                    String extension = SIO.Path.GetExtension(normalisedFilename);
                    if (DEFAULT_INVALID_EXTENSIONS.Contains(extension))
                    {
                        param.Log.Error("Cannot resolve build path for '{0}'.  It is not a valid independent asset.",
                            normalisedFilename);
                        continue;
                    }
                    else
                    {
                        normalisedFilenames.Add(normalisedFilename);
                    }
                }
            }

            foreach (String normalisedFilename in normalisedFilenames)
            {
                if (param.Flags.HasFlag(EngineFlags.Preview))
                {
                    // Handle preview; we can platform convert any file into
                    // the preview directory.
                    Content.Asset input = tree.CreateAsset(normalisedFilename,
                        RSG.Platform.Platform.Independent) as Content.Asset;
                    String outputFilename = SIO.Path.Combine("$(preview)",
                        String.Format("{0}.{1}", input.Basename, input.Extension));
                    IContentNode output = tree.CreateAsset(outputFilename,
                        RSG.Platform.Platform.Independent);
                    ProcessBuilder pb = new ProcessBuilder(DEFAULT_PROCESSOR, tree);
                    pb.Inputs.Add(input);
                    pb.Outputs.Add(output);
                    IProcess process = pb.ToProcess();
                    resolvedProcesses.Add(process);
                }
                else
                {
                    // Not previewing data so we need to ensure that the
                    // file is under $(processed) or $(export).
                    Debug.Assert(normalisedFilename.StartsWith(param.Branch.Export, StringComparison.OrdinalIgnoreCase) ||
                        normalisedFilename.StartsWith(param.Branch.Processed, StringComparison.OrdinalIgnoreCase),
                        String.Format("Input file is not under export or processed root path; cannot convert.  Filename: {0}.", normalisedFilename));
                    if (!(normalisedFilename.StartsWith(param.Branch.Export, StringComparison.OrdinalIgnoreCase) ||
                          normalisedFilename.StartsWith(param.Branch.Processed, StringComparison.OrdinalIgnoreCase)))
                    {
                        param.Log.ErrorCtx(LOG_CTX, "Input file is not under export or processed root path; cannot convert.  Filename: {0}.  Processed root path: {1}.  Export path: {2}.", normalisedFilename, param.Branch.Processed, param.Branch.Export);
                        continue;
                    }

                    // Create input content-node; tag up depending on if the
                    // input file is deleted.
                    Content.Asset input = tree.CreateAsset(normalisedFilename,
                        RSG.Platform.Platform.Independent) as Content.Asset;
                    if (!(input is Content.Asset))
                    {
                        param.Log.WarningCtx(LOG_CTX, "Input file does not resolve to Content.Asset content type.  It will be ignored.  Filename: {0}.", normalisedFilename);
                        continue;
                    }
                    if (!SIO.File.Exists(normalisedFilename) && !input.Parameters.ContainsKey(Constants.Content_State))
                        input.Parameters.Add(Constants.Content_State, ContentState.Deleted);
                    else if (!input.Parameters.ContainsKey(Constants.Content_State))
                        input.Parameters.Add(Constants.Content_State, ContentState.Modified);

#warning DHM FIX ME: temporary fix for GTA5 Bug #831245 - will be replaced with common path conversion methods real soon!
                    String normalisedExport = SIO.Path.GetFullPath(param.Branch.Export).ToLower();
                    String normalisedProcessed = SIO.Path.GetFullPath(param.Branch.Processed).ToLower();
                    String outputFilename = "$(target)" + normalisedFilename.Replace(normalisedExport, "").Replace(normalisedProcessed, "");
                    if (outputFilename.EndsWith(".zip"))
                        outputFilename = SIO.Path.ChangeExtension(outputFilename, ".rpf");

                    IContentNode output = tree.CreateAsset(outputFilename,
                        RSG.Platform.Platform.Independent);
                    ProcessBuilder pb = new ProcessBuilder(DEFAULT_PROCESSOR, tree);
                    pb.Inputs.Add(input);
                    pb.Outputs.Add(output);
                    IProcess process = pb.ToProcess();
                    resolvedProcesses.Add(process);
                }
            }
            return (resolvedProcesses);
        }
    }

} // RSG.Pipeline.Services
