﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace RSG.Pipeline.Services.Platform.Texture
{

    /// <summary>
    /// Cached Texture pipeline specification object; recording the modified
    /// timestamp of the file.
    /// </summary>
    internal class CachedSpecification
    {
        #region Properties
        /// <summary>
        /// Specification filename modified time (used to determine when to
        /// drop and re-parse).  Required for long-running processes such as
        /// the Asset Builders.
        /// </summary>
        public DateTime ModifiedTime
        {
            get;
            private set;
        }

        /// <summary>
        /// Specification object.
        /// </summary>
        public ISpecification Specification
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="specificationFilename"></param>
        public CachedSpecification(ISpecification spec, DateTime mtime)
        {
            this.Specification = spec;
            this.ModifiedTime = mtime;
        }
        #endregion // Constructor(s)
    }

} // RSG.Pipeline.Services.Platform.Texture namespace
