﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using RSG.Base.Logging.Universal;

namespace RSG.Pipeline.Services.AssetProcessor
{

    /// <summary>
    /// Asset Processor configuration object; loads/saves inputs, outputs
    /// and operation information for Asset Processor executable.
    /// </summary>
    /// Some combinations of inputs and output are incompatible.
    ///     
    /// see //depot/docs/tools/specs/AP3 - Asset Processor Design.docx for more info
    /// The AssetProcessorConfig is used to setup the asset processor itself
    /// it defines the operations that will be run (AssetProcessorTask), the Inputs that will be used as a source for it
    /// and the final output of the processor.
    /// 
    /// An operation can have sub-elements (like specific files to process, add or remove).
    /// Those sub elements path can be absolute or relative to the inputs
    /// 
    /// Inputs are absolute path to the files needed to perform the operations
    /// Output is an absolute path to the file that will be written out
    /// 
    /// There's a lot of code involved here in validating the instance before it's created.
    /// An invalid AssetProcessorConfig file should never happen.
    /// If invalid arguments are provided, the Validate method will throw exceptions all over the place.
    public sealed class AssetProcessorConfig
    {
        #region Constants
        private const string ConfigRoot                 = "Configurations";
        private const string ConfigAssetProcessorConfig = "AssetProcessorConfig";

        private const string ConfigOperations           = "Operations";
        private const string ConfigOperation            = "Operation";
        private const string ConfigElements             = "Elements";
        private const string ConfigElement              = "Element";

        private const string ConfigInputs               = "Inputs";
        private const string ConfigInput                = "Input";
        private const string ConfigOutput               = "Output";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Operations with operations to run for this Asset Processor instance.
        /// </summary>
        public IEnumerable<AssetProcessorTask> Operations { get; private set; }

        /// <summary>
        /// Inputs asset absolute filenames. 
        /// </summary>
        public IEnumerable<Uri> Inputs { get; private set; }

        /// <summary>
        /// Output asset absolute filename.
        /// </summary>
        public Uri Output { get; private set; }
        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// Constructor; from a collection of operations, inputs, and output.
        /// </summary>
        /// <param name="operations">Collection of operations that will be performed by the AssetProcessor.</param>
        /// <param name="inputs">Current config's inputs.</param>
        /// <param name="output">Current config's output.</param>
        /// <param name="log">Log instance, for reporting.</param>
        public AssetProcessorConfig(IEnumerable<AssetProcessorTask> operations,
            IEnumerable<Uri> inputs,
            Uri output,
            IUniversalLog log)
        {
            ValidateConfig(ref operations, inputs, output, log);

            Operations = operations;
            Inputs = inputs;
            Output = output;
        }

        #endregion // Constructor(s)

        #region Static Controller Methods
        /// <summary>
        /// Construction; from absolute filename.
        /// </summary>
        /// <param name="filename">AssetProcessorConfig's filename that needs to be loaded.</param>
        /// <param name="log">Current AssetProcessor's log instance, for reporting.</param>
        /// <returns>A collection of AssetProcessorConfig, validated as they are parsed.</returns>
        public static IEnumerable<AssetProcessorConfig> Load(string filename, IUniversalLog log)
        {
            if (!File.Exists(filename))
                throw new FileNotFoundException("AssetProcessorConfig not found.", filename);

            XDocument xmlDoc = XDocument.Load(filename);
            XElement root = xmlDoc.Root;
            if (root == null)
            {
                log.ErrorCtx(ConfigRoot, "AssetProcessor's {0} root not found. Verify that the xml is valid.", ConfigRoot);
                throw new XmlException(string.Format("AssetProcessor's {0} root not found. Verify that the xml is valid.", ConfigRoot));
            }

            List<AssetProcessorConfig> assetProcessorConfigs = new List<AssetProcessorConfig>();

            IEnumerable<XElement> assetProcessors = root.Elements(ConfigAssetProcessorConfig);
            if (assetProcessors.Any())
            {
                foreach (XElement assetProcessor in assetProcessors)
                {
                    IEnumerable<AssetProcessorTask> operations;
                    IEnumerable<Uri> inputs;
                    Uri output;

                    ParseAssetProcessor(assetProcessor, log, out operations, out inputs, out output);

                    AssetProcessorConfig config = new AssetProcessorConfig(operations, inputs, output, log);

                    assetProcessorConfigs.Add(config);
                }
            }
            else
            {
                log.ErrorCtx(ConfigAssetProcessorConfig, "AssetProcessor's {0} xml tag not found. Verify that the xml is valid.", ConfigAssetProcessorConfig);
                throw new XmlException(string.Format("AssetProcessor's {0} xml tag not found. Verify that the xml is valid.", ConfigAssetProcessorConfig));
            }
            return assetProcessorConfigs;
        }

        /// <summary>
        /// Parser from filename.  Use 'Load' static method instead.
        /// This parsing method assigns its out parameters as null if any error is found;
        /// this is used later on in the ValidateConfig() method to determine if the config is a valid one
        /// </summary>
        /// <param name="assetProcessor"></param>
        /// <param name="log"></param>
        /// <param name="operations"></param>
        /// <param name="inputs"></param>
        /// <param name="output"></param>
        private static void ParseAssetProcessor(XElement assetProcessor, IUniversalLog log, out IEnumerable<AssetProcessorTask> operations, out IEnumerable<Uri> inputs, out Uri output)
        {
            // Read Operations and Elements
            XElement operationsRoot = assetProcessor.Element(ConfigOperations);
            if (operationsRoot == null)
            {
                operations = null;
            }
            else
            {
                List<AssetProcessorTask> ops = new List<AssetProcessorTask>();

                IEnumerable<XElement> operationsNodes = operationsRoot.Elements(ConfigOperation);

                foreach (XElement operation in operationsNodes)
                {
                    XElement elementsRoot = operation.Element(ConfigElements);

                    if (elementsRoot == null)
                    {
                        // no sub elements, we just add the operation (probably a copy or a drawable merge)
                        ops.Add(new AssetProcessorTask(operation));
                    }
                    else
                    {
                        // TODO Flo:
                        // Here we might want to generate absolute URI based on the value of elem, and a base URI.
                        // that base URI can be specified as an option for certain operations.
                        // for example, AddFiles only support absolute URI because of File.Exists(path)
                        // then we would be able to combine the Uri with the provided base, and the elem.Value.
                        // (or we dont care and enforce that in the config when we build it anyway).

                        List<Uri> elements = elementsRoot.Elements(ConfigElement).Select(elem => new Uri(elem.Value, UriKind.RelativeOrAbsolute)).ToList();
                        ops.Add(new AssetProcessorTask(operation, elements));
                    }
                }

                operations = ops;
            }

            // Read Inputs
            // In case of invalid node, just assign null. The validation will throw an exception
            XElement inputsRoot = assetProcessor.Element(ConfigInputs);
            inputs = (inputsRoot == null) ? null : inputsRoot.Elements(ConfigInput).Select(input => new Uri(input.Value, UriKind.RelativeOrAbsolute)).ToList();

            // Read Output
            XElement outputElement = assetProcessor.Element(ConfigOutput);
            output = (outputElement == null) ? null : new Uri(outputElement.Value, UriKind.RelativeOrAbsolute);
        }

        /// <summary>
        /// Validates supplied operations, inputs and output for a given AssetProcessorConfig
        /// </summary>
        /// <param name="operations">Collection of operations performed for this config.
        /// This collection is modified during the validation, to order the operations.</param>
        /// <param name="inputs">Current config's inputs</param>
        /// <param name="output">Current config's output</param>
        /// <param name="log"></param>
        private static void ValidateConfig(ref IEnumerable<AssetProcessorTask> operations,
            IEnumerable<Uri> inputs,
            Uri output,
            IUniversalLog log)
        {
            // Check if we actually have operation
            if (operations == null)
            {
                log.ErrorCtx(ConfigRoot, "Error while loading AssetProcessor's config file on Operations parsing.\nVerify AssetProcessor config's file.");
                throw new ArgumentNullException("operations", "Error while creating AssetProcesorConfig object. Argument is null.");
            }

            // Sorting according to the enumeration. Enum integer value ensure that Copy or DrawableMerge are first in line
            // replace operations argument with a sorted version of itself
            // done here instead of inside ParseConfigFile if we want to build an AssetProcessorConfig by code
            operations = operations.OrderBy(op => op.Operation).ToList();

            // Check that the first operation is a Copy or a DrawableMerge
            AssetProcessorTask firstOperation = operations.First();
            switch (firstOperation.Operation)
            {
                case Operation.Copy:
                case Operation.DrawableMerge:
				case Operation.MergeFiles:
                case Operation.RenameAnimation:
                case Operation.ModulariseSkeleton:
                    break;

                    // falling into the case that the first one is not a Copy or a DrawableMerge. Log Error and return
                default:
                    log.ErrorCtx(ConfigRoot, "The first task in the AssetProcessor has to be a Copy, a DrawableMerge, MergeFiles or a RenameAnimation. Found {0} for {1} instead. Aborting.", firstOperation.Operation, firstOperation);
                    throw new InvalidDataException("Copy, a DrawableMerge, MergeFiles or a RenameAnimation operation not found. Verifiy the AssetProcessorConfig file.");
            }

            // We have both a drawablemerge and a copy and a . This is wrong. It's one or the other
            if (TooManyMajorOperation(operations))
            {
                log.ErrorCtx(ConfigRoot, "AssetProcessorConfig does not support both Copy, DrawableMerge or MergeFiles at the same time.");
                throw new InvalidDataException("Copy and DrawableMerge found. Verify the AssetProcessorConfig file.");
            }

            // Checking if inputs are valid
            if (inputs == null)
            {
                log.ErrorCtx(ConfigRoot, "Error while loading AssetProcessor's config file on Inputs parsing.\nVerify AssetProcessor config's file.");
                throw new ArgumentNullException("inputs", "Error while creating AssetProcesorConfig object. Argument is null.");
            }

            // check that all inputs actually exists as files
            foreach (Uri input in inputs)
            {
                if (!input.IsAbsoluteUri)
                {
                    log.ErrorCtx(ConfigRoot, "Input invalid. This input is not an absolute path.\n\t{0}", input.AbsolutePath);
                    throw new UriFormatException("AbsoluteUri expected, RelativeUri provided.");
                }
            }

            // Checking if output is valid
            if (output == null)
            {
                log.ErrorCtx(ConfigRoot, "Error while loading AssetProcessor's config file on Output parsing.\nVerify AssetProcessor config's file.");
                throw new ArgumentNullException("output", "Error while creating AssetProcesorConfig object. Argument is null.");
            }

            if (!output.IsAbsoluteUri)
            {
                throw new UriFormatException("AbsoluteUri expected, RelativeUri provided.");
            }
        }

        /// <summary>
        /// Ugly function to basically count the number of major operations in the collection.
        /// I may add some sort of flag on Operation one day to prevent doing that kind of shitty workaround
        /// </summary>
        /// <param name="operations">Current config's collection of operations.</param>
        /// <returns>True if more than one Major Operation is found. False otherwise.</returns>
        private static bool TooManyMajorOperation(IEnumerable<AssetProcessorTask> operations)
        {
            int weHaveCopy            = operations.Any(o => o.Operation == Operation.Copy) ? 1 : 0;
            int weHaveDrawableMerge   = operations.Any(o => o.Operation == Operation.DrawableMerge) ? 1 : 0;
			int weHaveMergeFiles      = operations.Any(o => o.Operation == Operation.MergeFiles) ? 1 : 0;
            int weHaveRenameAnimation = operations.Any(o => o.Operation == Operation.RenameAnimation) ? 1 : 0;
            int weHaveModulariseSkeleton = operations.Any(o => o.Operation == Operation.ModulariseSkeleton) ? 1 : 0;

            int numberOfMajorOps = weHaveCopy + weHaveDrawableMerge + weHaveMergeFiles + weHaveRenameAnimation + weHaveModulariseSkeleton;

            return numberOfMajorOps > 1;
        }
        #endregion // Static Controller Methods

        #region Controller Methods
        /// <summary>
        /// Serialise the Asset Processor configuration to XML file.
        /// </summary>
        /// <param name="filename">Full file path and name where we want to save the Configs.</param>
        /// <param name="configs">Collection of valid configs that will be serialised out.</param>
        public static void Save(string filename, IEnumerable<AssetProcessorConfig> configs)
        {
            XDocument xmlDoc = new XDocument(new XDeclaration("1.0", "utf-8", "yes"));

            XElement root = new XElement(ConfigRoot);
            foreach (AssetProcessorConfig config in configs)
            {
                root.Add(config.ToXElement());
            }
            
            xmlDoc.Add(root);
            xmlDoc.Save(filename);
        }

        public XElement ToXElement()
        {
            return new XElement(ConfigAssetProcessorConfig,
                new XElement(ConfigOperations, Operations.Select(task => task.ToXElement())),
                new XElement(ConfigInputs, Inputs.Select(input => new XElement("Input", input))),
                new XElement(ConfigOutput, Output));
        }
        #endregion // Controller Methods
    }

} // RSG.Pipeline.Services.AssetProcessor namespace
