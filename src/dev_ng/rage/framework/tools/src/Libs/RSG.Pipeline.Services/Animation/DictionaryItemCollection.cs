﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.Services.Animation
{
    /// <summary>
    /// Wrapper for a collection DictionaryItem objects
    /// </summary>
    public sealed class DictionaryItemCollection 
        : IEnumerable<DictionaryItem>
    {
        #region Fields
        /// <summary>
        /// The collection of DictionaryItem objects
        /// </summary>
        private List<DictionaryItem> _Items = new List<DictionaryItem>();
        #endregion // Fields

        #region Methods
        /// <summary>
        /// Determine whether the Item list already contains a DictionaryItem.
        /// </summary>
        /// <param name="inClipGroup"></param>
        public bool Contains(ref DictionaryItem inUnit)
        {
            foreach (DictionaryItem unit in this._Items)
            {
                if (unit.Equals(inUnit))
                {
                    return true;
                }
            }
            return false;
        }
        #endregion // Methods

        #region IEnumerable<DictionaryItem> Members
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerator<DictionaryItem> GetEnumerator()
        {
            return _Items.GetEnumerator();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        #endregion // IEnumerable<DictionaryUnit> Members
    }


}
