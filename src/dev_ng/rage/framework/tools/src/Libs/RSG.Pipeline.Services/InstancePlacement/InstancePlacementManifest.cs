﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.Pipeline.Services.InstancePlacement
{
    public class InstancePlacementManifest
    {
        #region Constants
        private static readonly String MANIFEST = "Manifest";
        #endregion

        #region Properties
        /// <summary>
        /// List of all the manifest additions from the instance placement processor.
        /// </summary>
        public IEnumerable<String> InstancePlacementManifestAdditions
        {
            get;
            private set;
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Default Constructor
        /// </summary>
        /// <param name="instancePlacementManifestAdditions"></param>
        public InstancePlacementManifest(IEnumerable<String> instancePlacementManifestAdditions)
        {
            InstancePlacementManifestAdditions = instancePlacementManifestAdditions;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Serialize the list of manifest additions to an XML file.
        /// </summary>
        /// <param name="filename"></param>
        public void Serialize(String filename)
        {
            List<XElement> manifests = new List<XElement>();
            foreach(String manifestAdditions in InstancePlacementManifestAdditions)
                manifests.Add(new XElement(MANIFEST, manifestAdditions));

            XDocument document = new XDocument(
                   new XElement("InstancePlacementManifests",
                       manifests.ToArray()));

            document.Save(filename);
        }
        #endregion

        #region Static Methods
        /// <summary>
        /// Deserialize the list of manifest additions from an XML file.
        /// </summary>
        /// <param name="filename"></param>
        public static IEnumerable<String> Deserialize(String filename)
        {
            XDocument document = XDocument.Load(filename);
            return document.Root.Elements(MANIFEST).Select(element => element.Value);
        }
        #endregion
    }
}
