﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Base.Math;
using RSG.Pipeline.Services.InstancePlacement;

namespace RSG.Pipeline.Services.InstancePlacement
{
    public class BoundsPartition
    {
        #region Properties
        private BoundingBox3f BoundingBox;
        private BoundingBox2i RoundedBoundingBox;

        private int Width;
        private int Length;
        private List<OptimisedPrimitive>[][] Partitions;

        private static readonly float MAX_ANGLE = 85.0f;
        #endregion

        #region Constructors
        public BoundsPartition(BoundingBox3f box, bool accurateRounding)
        {
            BoundingBox = box;

            // NOTE: Floor is correct (i.e., accurate), but causes data discrepancies with the base game
            int minX, minY;
            if (accurateRounding)
            {
                minX = (int)Math.Floor(BoundingBox.Min.X);
                minY = (int)Math.Floor(BoundingBox.Min.Y);
            }
            else
            {
                minX = (int)Math.Truncate(BoundingBox.Min.X);
                minY = (int)Math.Truncate(BoundingBox.Min.Y);
            }

            int maxX = (int)Math.Ceiling(BoundingBox.Max.X);
            int maxY = (int)Math.Ceiling(BoundingBox.Max.Y);
            RoundedBoundingBox = new BoundingBox2i(new Vector2i(minX, minY), new Vector2i(maxX, maxY));

            Width = maxX - minX + 1;  //Plus one to include the outer bound edge.
            Length = maxY - minY + 1; //Plus one to include the outer bound edge.

            Partitions = new List<OptimisedPrimitive>[Width][];
            for (int widthIndex = 0; widthIndex < Width; widthIndex++)
            {
                Partitions[widthIndex] = new List<OptimisedPrimitive>[Length];

                for (int lengthIndex = 0; lengthIndex < Length; lengthIndex++)
                {
                    Partitions[widthIndex][lengthIndex] = new List<OptimisedPrimitive>();
                }
            }
        }
        #endregion

        #region Public Members
        /// <summary>
        /// Adds all bounds to each partition it overlaps.
        /// </summary>
        /// <param name="bounds"></param>
        public void AddBoundPrimitives(ICollection<OptimisedBound> bounds, bool accurateRounding)
        {
            int xRange = RoundedBoundingBox.Max.X - RoundedBoundingBox.Min.X;
            int yRange = RoundedBoundingBox.Max.Y - RoundedBoundingBox.Min.Y;

            foreach (OptimisedBound optimisedBound in bounds)
            {
                foreach (OptimisedPrimitive primitive in optimisedBound.Primitives)
                {
                    // NOTE: Floor is correct (i.e., accurate), but causes data discrepancies with the base game
                    int minX, minY;
                    if (accurateRounding)
                    {
                        minX = (int)Math.Max(Math.Floor(primitive.BoundingBox.Min.X) - RoundedBoundingBox.Min.X, 0);
                        minY = (int)Math.Max(Math.Floor(primitive.BoundingBox.Min.Y) - RoundedBoundingBox.Min.Y, 0);
                    }
                    else
                    {
                        minX = (int)Math.Max(Math.Truncate(primitive.BoundingBox.Min.X) - RoundedBoundingBox.Min.X, 0);
                        minY = (int)Math.Max(Math.Truncate(primitive.BoundingBox.Min.Y) - RoundedBoundingBox.Min.Y, 0);
                    }

                    int maxX = (int)Math.Min(Math.Ceiling(primitive.BoundingBox.Max.X) - RoundedBoundingBox.Min.X, xRange);
                    int maxY = (int)Math.Min(Math.Ceiling(primitive.BoundingBox.Max.Y) - RoundedBoundingBox.Min.Y, yRange);

                    for (int x = minX; x <= maxX; ++x)
                    {
                        for (int y = minY; y <= maxY; ++y)
                        {
                            Partitions[x][y].Add(primitive);
                        }
                    }
                }
            }

        }

        /// <summary>
        /// Sorts all partitions by the z coordinate.
        /// </summary>
        public void SortPartitions()
        {
            int xMaxIndex = RoundedBoundingBox.Max.X - RoundedBoundingBox.Min.X;
            int yMaxIndex = RoundedBoundingBox.Max.Y - RoundedBoundingBox.Min.Y;

            for (int x = 0; x <= xMaxIndex; ++x)
            {
                for (int y = 0; y <= yMaxIndex; ++y)
                {
                    Partitions[x][y] = Partitions[x][y].OrderByDescending(primitive => primitive.BoundingBox.Max.Z).ToList();
                }
            }
        }

        /// <summary>
        /// Collision test algorithm; takes a 2D point and a list of spatially ordered bounds and
        /// returns the triangle collided and populates the Z-axis in the point.
        /// </summary>
        /// <param name="point"></param>
        /// <param name="tri"></param>
        /// <returns></returns>
        public bool TestCollision(Vector3f point, out OptimisedPrimitive tri)
        {
            int xIndex = (int)(point.X - RoundedBoundingBox.Min.X);
            int yIndex = (int)(point.Y - RoundedBoundingBox.Min.Y);

            if (xIndex < 0 || yIndex < 0 || xIndex >= Width || yIndex >= Length)
            {
                tri = null;
                return false;
            }

            List<OptimisedPrimitive> hitPolys = new List<OptimisedPrimitive>();

            foreach (OptimisedPrimitive primitive in Partitions[xIndex][yIndex])
            {
                if (primitive.BoundingBox.Contains(point))
                {
                    if (IsPointInPolygon(point, primitive.Vertices) == true)
                    {
                        hitPolys.Add(primitive);
                    }
                }
            }

            tri = null;
            float maxZ = float.MinValue;

            foreach (OptimisedPrimitive prim in hitPolys)
            {
                float z = prim.Plane.GetZCoordinate(point.X, point.Y);
                if (z > maxZ)
                {
                    tri = prim;
                    maxZ = z;
                }
            }

            if (tri != null)
            {
                point.Z = maxZ;
                return true;
            }

            tri = null;
            return false;
        }

        /// <summary>
        /// Tests a collision tri to see if it passes all the criteria to place the instance.
        /// This includes slope of the triangle and more as we tighten up the system.
        /// </summary>
        /// <param name="definition"></param>
        /// <param name="tri"></param>
        /// <returns></returns>
        public bool TestForRejection(float maxSlope, OptimisedPrimitive tri)
        {
            bool result = false;

            // Check the slope angle.
            double angle = Math.Acos((double)tri.Plane.Z);
            double degree = angle * 180 / Math.PI;

            if (degree > MAX_ANGLE)
                return false;

            if (maxSlope == -1.0f || degree <= maxSlope)
                result = true;

            return result;
        }
        #endregion

        #region Private Members
        /// <summary>
        /// Determines if the given point is within the polygon using the even-odd algorithm.
        /// </summary>
        /// <param name="p"></param>
        /// <param name="poly"></param>
        /// <returns></returns>
        private bool IsPointInPolygon(Vector3f p, Vector3f[] poly)
        {
            Vector3f p1, p2;
            bool inside = false;

            if (poly.Length < 3)
            {
                return inside;
            }

            var oldPoint = new Vector3f(poly[poly.Length - 1].X, poly[poly.Length - 1].Y, 0.0f);

            for (int i = 0; i < poly.Length; i++)
            {
                var newPoint = new Vector3f(poly[i].X, poly[i].Y, 0.0f);

                if (newPoint.X > oldPoint.X)
                {
                    p1 = oldPoint;
                    p2 = newPoint;
                }
                else
                {
                    p1 = newPoint;
                    p2 = oldPoint;
                }

                if ((newPoint.X < p.X) == (p.X <= oldPoint.X)
                    && (p.Y - (long)p1.Y) * (p2.X - p1.X)
                    < (p2.Y - (long)p1.Y) * (p.X - p1.X))
                {
                    inside = !inside;
                }

                //Verify that this point is on the line. 
                if (p2.X == p1.X)
                {
                    //Vertical line; no slope.
                    if (p.X == p1.X)
                    {
                        if (p2.Y > p1.Y)
                        {
                            if (p.Y >= p1.Y && p.Y <= p2.Y)
                                return true; //On the line.
                        }
                        else
                        {
                            if (p.Y >= p2.Y && p.Y <= p1.Y)
                                return true; //On the line.
                        }
                    }
                }
                else
                {
                    float lineSlope = (p2.Y - p1.Y) / (p2.X - p1.X);
                    if (lineSlope == 0.0f)
                    {
                        //Horizontal line.
                        if (p.Y == p1.Y)
                        {
                            if (p.X >= p1.X)
                            {
                                if (p.X >= p1.X && p.X <= p2.X)
                                    return true;  //On the line.
                            }
                            else
                            {
                                if (p.X >= p2.X && p.X <= p1.X)
                                    return true;  //On the line.
                            }
                            return true;
                        }
                    }
                    else
                    {
                        if (p2.Y > p1.Y)
                        {
                            float yIntersection = (lineSlope * (p.X - p1.X)) + p1.Y;
                            if (yIntersection == p.Y && yIntersection >= p1.Y && yIntersection <= p2.Y)
                                return true; //On the line.
                        }
                        else
                        {
                            float yIntersection = (lineSlope * (p.X - p2.X)) + p2.Y;
                            if (yIntersection == p.Y && yIntersection >= p2.Y && yIntersection <= p1.Y)
                                return true; //On the line.
                        }
                    }
                }

                oldPoint = newPoint;
            }



            return inside;
        }
        #endregion
    }
}


