﻿using System;
using SW = System.Windows;
using RSG.Base.Windows.Dialogs;

namespace RSG.Pipeline.Services
{

    /// <summary>
    /// Core pipeline CustomMessageBox service.
    /// </summary>
    /// Use this service to display message boxes for user attention.  Its 
    /// important this service is used throughout the pipeline so that we handle
    /// common user parameters in the same way (e.g. -nopopups).
    /// 
    public static class MessageBox
    {
        #region Properties
        /// <summary>
        /// Caption; used for all message and dialog boxes.
        /// </summary>
        public static String Caption
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        static MessageBox()
        {
            MessageBox.Caption = Resources.MessageBoxCaption;
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Display a custom dialog.
        /// </summary>
        /// <param name="dialog"></param>
        /// <returns></returns>
        public static bool? ShowCustomDialog(SW.Window dialog)
        {
            dialog.Title = MessageBox.Caption;
            return (dialog.ShowDialog());
        }

        /// <summary>
        /// Show a simple message box.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static SW.MessageBoxResult Show(String message, params Object[] args)
        {
            String msg = String.Format(message, args);
            return (SW.MessageBox.Show(msg, MessageBox.Caption));
        }

        /// <summary>
        /// Show a message box with custom buttons.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="button"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static SW.MessageBoxResult Show(String message, SW.MessageBoxButton button, 
            params Object[] args)
        {
            String msg = String.Format(message, args);
            return (SW.MessageBox.Show(msg, MessageBox.Caption, button));
        }

        /// <summary>
        /// Show a message box with custom buttons and an icon.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="button"></param>
        /// <param name="icon"></param>
        /// <param name="defaultResult"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static SW.MessageBoxResult Show(String message, SW.MessageBoxButton button, 
            SW.MessageBoxImage icon, params Object[] args)
        {
            String msg = String.Format(message, args);
            return (SW.MessageBox.Show(msg, MessageBox.Caption, button, icon));
        }

        /// <summary>
        /// Show a message box with custom buttons, an icon, with a default result.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="button"></param>
        /// <param name="icon"></param>
        /// <param name="defaultResult"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static SW.MessageBoxResult Show(String message, SW.MessageBoxButton button, 
            SW.MessageBoxImage icon, SW.MessageBoxResult defaultResult, params Object[] args)
        {
            String msg = String.Format(message, args);
            return (SW.MessageBox.Show(msg, MessageBox.Caption, button, icon, defaultResult));
        }

        /// <summary>
        /// Show a message box with custom buttons, an icon, a default result and
        /// custom options.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="button"></param>
        /// <param name="icon"></param>
        /// <param name="defaultResult"></param>
        /// <param name="options"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static SW.MessageBoxResult Show(String message, SW.MessageBoxButton button, 
            SW.MessageBoxImage icon, SW.MessageBoxResult defaultResult, 
            SW.MessageBoxOptions options, params Object[] args)
        {
            String msg = String.Format(message, args);
            return (SW.MessageBox.Show(msg, MessageBox.Caption, button, icon, defaultResult, options));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="messgae"></param>
        /// <param name="button"></param>
        /// <param name="icon"></param>
        /// <param name="defaultResult"></param>
        /// <param name="timeout"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static SW.MessageBoxResult Show(String message, SW.MessageBoxButton button, 
            SW.MessageBoxImage icon, SW.MessageBoxResult defaultResult, 
            uint timeout, params Object[] args)
        {
            String msg = String.Format(message, args);
            return (CustomMessageBox.Show(msg, MessageBox.Caption, button, icon, defaultResult, timeout));

        }
        #endregion // Controller Methods
    }

} // RSG.Pipeline.Services namespace
