﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using RSG.Base.Collections;
using RSG.Pipeline.Content;
using RSG.Platform;

namespace RSG.Pipeline.Services.Platform
{

    /// <summary>
    /// Asset comparer class; used for RPF sorting without ITYP metadata.
    /// </summary>
    internal class RpfSortComparer : 
        IComparer<Pair<String, String>>,
        IComparer<Asset>
    {
        #region IComparer<Pair<String, String>> Interface Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public int Compare(Pair<String, String> a, Pair<String, String> b)
        {
            String basenameA = Filename.GetBasename(a.First);
            String basenameB = Filename.GetBasename(b.First);

            return (basenameA.CompareTo(basenameB));

#if false
            FileType filetypeA = FileTypeUtils.ConvertExtensionToFileType(
                Path.GetExtension(a.Second));
            FileType filetypeB = FileTypeUtils.ConvertExtensionToFileType(
                Path.GetExtension(b.Second));

            int abComp = basenameA.CompareTo(basenameB);
            if (0 == abComp)
            {
                // Same core asset name; we need to sort appropriately.
                if ((FileType.TextureDictionary == filetypeA) &&
                    (FileType.TextureDictionary == filetypeB))
                {
                    // Both TXDs so we want to have the HD TXD second.
                    if (basenameA.Contains("+"))
                        return (1); // TXD 'b' before HD TXD 'a'.
                    else
                        return (-1);
                }
                else if (FileType.TextureDictionary == filetypeA)
                {
                    return (-1); // TXD 'a' before other asset 'b'. 
                }
                else if (FileType.TextureDictionary == filetypeB)
                {
                    return (1); // TXD 'b' before other asset 'a'.
                }
                else
                {
                    return (0); // Don't care ordering.
                }
            }
            else
            {
                // Different core asset name; sort based on basename result.
                return (abComp);
            }
#endif
        }
        #endregion // IComparer<Pair<String, String>> Interface Methods

        #region IComparer<Asset> Interface Methods
        /// <summary>
        /// Compare two assets; for RPF sorting without ITYP data.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public int Compare(Asset a, Asset b)
        {
            return (a.Basename.CompareTo(b.Basename));

#if false
            int abComp = a.Basename.CompareTo(b.Basename);
            if (0 == abComp)
            {
                // Same core asset name; we need to sort appropriately.
                if ((FileType.TextureDictionary == a.AssetType) && 
                    (FileType.TextureDictionary == b.AssetType))
                {
                    // Both TXDs so we want to have the HD TXD second.
                    if (a.Basename.Contains("+"))
                        return (1); // TXD 'b' before HD TXD 'a'.
                    else
                        return (-1);
                }
                else if (FileType.TextureDictionary == a.AssetType)
                {
                    return (-1); // TXD 'a' before other asset 'b'. 
                }
                else if (FileType.TextureDictionary == b.AssetType)
                {
                    return (1); // TXD 'b' before other asset 'a'.
                }
                else
                {
                    return (0); // Don't care ordering.
                }
            }
            else
            {
                // Different core asset name; sort based on basename result.
                return (abComp);
            }
#endif
        }
        #endregion // IComparer<Asset> Interface Methods
    }

} // RSG.Pipeline.Services.Platform namespace
