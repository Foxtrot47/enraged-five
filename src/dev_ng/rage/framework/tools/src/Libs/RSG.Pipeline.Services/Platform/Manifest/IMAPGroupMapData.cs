﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.Pipeline.Services.Platform.Manifest
{
    public class IMAPGroupMapData
    {
        public static IMAPGroupMapData FromMapExportAdditions(XElement imapGroupElement)
        {
            String name = imapGroupElement.Attribute("name").Value.ToString();
            String[] activationTypes = imapGroupElement.Elements("ActivationType").Select(element => element.Attribute("value").Value).ToArray();
            String[] activeWeatherTypes = imapGroupElement.Elements("ActiveWeatherType").Select(element => element.Attribute("value").Value).ToArray();
            uint activeHours = 0;
            XElement activeHoursElement = imapGroupElement.Element("ActiveHours");
            if (activeHoursElement != null)
                UInt32.TryParse(activeHoursElement.Attribute("value").Value, out activeHours);

            return new IMAPGroupMapData(name, activationTypes, activeWeatherTypes, activeHours);
        }

        private IMAPGroupMapData(String name, IEnumerable<String> activationTypes, IEnumerable<String> activeWeatherTypes, uint activeHours)
        {
            Name = name;
            ActivationTypes = activationTypes;
            ActiveWeatherTypes = activeWeatherTypes;
            ActiveHours = activeHours;
        }

        internal String Name { get;  private set;}
        internal IEnumerable<String> ActivationTypes { get; private set; }
        internal IEnumerable<String> ActiveWeatherTypes { get; private set; }
        internal uint ActiveHours { get; private set; }
    }
}
