﻿using System;

namespace RSG.Pipeline.Services.CollisionProcessor
{

    /// <summary>
    /// Collision Processor operations.
    /// </summary>
    public enum Operation
    {
        /// <summary>
        /// Map Bounds Processor; raw export bounds data to processed bounds.
        /// </summary>
        BoundsProcessor,

        /// <summary>
        /// Ambient Scanner; collision analysis for ambient metadata.
        /// </summary>
        AmbientScanner,

        /// <summary>
        /// Instance Placement; collision and texture analysis and output IMAP entity data.
        /// </summary>
        InstancePlacement,
    }

} // RSG.Pipeline.Services.CollisionProcessor namespace
