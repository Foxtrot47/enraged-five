﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.Pipeline.Services.Metadata
{

    /// <summary>
    /// Map Metadata Serialiser options file.
    /// </summary>
    public class MapMetadataSerialiserOptions
    {
        #region Properties
        /// <summary>
        /// Audio Collision mappings filename.
        /// </summary>
        public String AudioCollisionMapFilename
        {
            get;
            private set;
        }

        /// <summary>
        /// Scene Overrides directory
        /// </summary>
        public String SceneOverridesDirectory
        {
            get;
            private set;
        }

        /// <summary>
        /// Scene Overrides directory
        /// </summary>
        public String[] NonSerialisedTimeCycleTypes
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        public MapMetadataSerialiserOptions(String audioCollisionMapFilename,
                                            String sceneOverridesDirectory,
                                            IEnumerable<String> nonSerialisedTimeCycleTypes)
        {
            AudioCollisionMapFilename = audioCollisionMapFilename;
            SceneOverridesDirectory = sceneOverridesDirectory;
            NonSerialisedTimeCycleTypes = nonSerialisedTimeCycleTypes.ToArray();
        }

        internal MapMetadataSerialiserOptions(XElement xmlTaskElem)
        {
            this.AudioCollisionMapFilename = xmlTaskElem.Element("AudioCollisionMap").Value;
            this.SceneOverridesDirectory = xmlTaskElem.Element("SceneOverrides").Value;
            this.NonSerialisedTimeCycleTypes = xmlTaskElem.Elements("NonSerialisedTimeCycleType").Select(elem => elem.Value).ToArray();
        }
        #endregion // Constructor(s)

        #region Internal Methods
        /// <summary>
        /// Serialise to an XElement.
        /// </summary>
        /// <returns></returns>
        internal XElement Serialise()
        {
            XElement xmlTaskElem = new XElement("Options",
                new XElement("AudioCollisionMap", this.AudioCollisionMapFilename),
                new XElement("SceneOverrides", this.SceneOverridesDirectory),
                NonSerialisedTimeCycleTypes.Select(type => new XElement("NonSerialisedTimeCycleType", type))
            ); // Options
            return (xmlTaskElem);
        }
        #endregion // Internal Methods
    }

} // RSG.Pipeline.Services.Metadata namespace
