﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using RSG.Base.Collections;
using RSG.Base.Extensions;
using RSG.ManagedRage;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using XGE = RSG.Interop.Incredibuild.XGE;
using RSG.Pipeline.Core;

namespace RSG.Pipeline.Services.Platform
{

    /// <summary>
    /// RPF file construction and extraction utility functions.
    /// </summary>
    /// RPF read and extraction support uses RSG.ManagedRage.
    /// Write support invokes Ragebuilder as per the old Ruby pipeline.
    /// 
    public static class Rpf
    {
        #region Constants
        /// <summary>
        /// Nameheap byte limit count for RPF7.
        /// </summary>
        public static readonly uint RPF7_NAMEHEAP_LIMIT = 16384;

        /// <summary>
        /// Ratio of nameheap limit to generate a warning for.
        /// </summary>
        public static readonly float RPF_NAMEHEAP_WARNING_RATIO = 0.95f;

        /// <summary>
        /// Ratio of approx. RPF size to disk available space before we output warning.
        /// </summary>
        /// This is for idiots who ignore Windows telling them their drives are
        /// filling up.
        public static readonly float RPF_DISK_SIZE_WARNING_RATIO = 1.2f;

        /// <summary>
        /// Log context.
        /// </summary>
        private static readonly String LOG_CTX = "RPF";

        /// <summary>
        /// Zip create temporary file location
        /// </summary>
        private static readonly String PATH_RPF_CREATE_TMP = "$(toolstemp)\\rpf_create";
        #endregion // Constants

        #region Static Member Data
        /// <summary>
        /// Dictionary storing packet counts for RPF packing tasks.
        /// </summary>
        static IDictionary<ITarget, int> m_TargetRpfPackTaskCount = new Dictionary<ITarget, int>();
        #endregion // Static Member Data

        #region RPF File Query Methods
        /// <summary>
        /// Determine whether a file is an RPF.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="checkMagic"></param>
        /// <returns></returns>
        public static bool IsRpf(String filename, bool checkMagic = false)
        {
            String extension = Path.GetExtension(filename);
            if ("rpf".Equals(extension, StringComparison.OrdinalIgnoreCase))
            {
                if (checkMagic)
                    return (IsRpfHeader(filename));
                else
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Determine whether the file has an RPF header.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static bool IsRpfHeader(String filename)
        {
            bool isRpf = false;
            if (File.Exists(filename))
            {
                using (FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read))
                {
                    Byte[] magic = new Byte[4];
                    fs.Read(magic, 0, 4);
                    if ('R' == magic[0] && 'P' == magic[1] && 'F' == magic[2])
                        isRpf = true;
                }
            }
            return (isRpf);
        }
        #endregion // RPF Query Methods

        #region RPF File Construction Methods
        /// <summary>
        /// Create a RPF file (no folder support).
        /// </summary>
        /// <param name="target">Target platform.</param>
        /// <param name="filename">Destination filename.</param>
        /// <param name="files">Enumerable of filenames to pack.</param>
        /// <returns>true iff successful; false otherwise.</returns>
        public static bool Create(IUniversalLog log, String filename, IEnumerable<String> files, ITarget target)
        {
            List<Pair<String, String>> file_list = new List<Pair<String, String>>();
            foreach (String fileEntry in files)
                file_list.Add(new Pair<String, String>(fileEntry, Path.GetFileName(fileEntry)));

            return (Rpf.Create(log, filename, file_list, target));
        }

        /// <summary>
        /// Create a RPF file; defining the destination files (folder support).
        /// </summary>
        /// <param name="filename">Destination filename.</param>
        /// <param name="files">Enumerable of Pairs of source/destination files to pack.</param>
        /// <param name="target">Target platform.</param>
        /// <returns>true iff successful; false otherwise.</returns>
        public static bool Create(IUniversalLog log, String filename, IEnumerable<Pair<String, String>> files, ITarget target)
        {
            return (Rpf.Create(log, filename, files, target, String.Empty, 
                String.Empty));
        }

        /// <summary>
        /// Create a RPF file; with an ITYP file for dependency sorting.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="filename">Destination filename.</param>
        /// <param name="files">Enumerable of filenames to pack.</param>
        /// <param name="target">Target platform.</param>
        /// <param name="itypFilename"></param>
        /// <param name="manifestFilename"></param>
        /// <returns></returns>
        public static bool Create(IUniversalLog log, String filename, 
            IEnumerable<String> files, ITarget target, String itypFilename,
            String manifestFilename)
        {
            List<Pair<String, String>> file_list = new List<Pair<String, String>>();
            foreach (String fileEntry in files)
                file_list.Add(new Pair<String, String>(fileEntry, Path.GetFileName(fileEntry)));

            return (Rpf.Create(log, filename, file_list, target, itypFilename,
                manifestFilename));
        }

        /// <summary>
        /// Create a RPF file; defining the destination files (folder support)
        /// and an ITYP file for dependency sorting.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="filename"></param>
        /// <param name="files"></param>
        /// <param name="target"></param>
        /// <param name="itypFilename"></param>
        /// <returns></returns>
        public static bool Create(IUniversalLog log, String filename,
            IEnumerable<Pair<String, String>> files, ITarget target,
            String metadataDirectory, String manifestFilename)
        {
            uint nameheapSize = GetNameheapSize(files);
            if (nameheapSize > RPF7_NAMEHEAP_LIMIT)
            {
                log.ErrorCtx(LOG_CTX, "RPF7 nameheap limit exceeded for '{0}': {1} bytes with {2} files (limit: {3}).",
                    filename, nameheapSize, files.Count(), RPF7_NAMEHEAP_LIMIT);
            }
            else if (nameheapSize > (RPF_NAMEHEAP_WARNING_RATIO * RPF7_NAMEHEAP_LIMIT))
            {
                log.WarningCtx(LOG_CTX, "Nearing RPF7 nameheap limit for '{0}': {1} bytes with {2} files (limit: {3}).",
                    filename, nameheapSize, files.Count(), RPF7_NAMEHEAP_LIMIT);
            }

            // Sort file list.
            IEnumerable<Pair<String, String>> sorted_files;
            RpfSort.SortFileList(log, files, out sorted_files, metadataDirectory);

            // Verify free disk space.  ***Sigh***.
            long approxRpfSize = GetApproximateRpfSize(sorted_files);
            long availableSpace = GetAvailableDiskSpace(filename);
            if ((RPF_DISK_SIZE_WARNING_RATIO * approxRpfSize) > availableSpace)
                log.WarningCtx(LOG_CTX, "Low on disk space for output RPF; {0}KiB available.", availableSpace);

            log.MessageCtx(LOG_CTX, "Creating RPF: {0} with {1} files.", filename, files.Count());
            return (0 == PackRpf(log, target, filename, sorted_files, manifestFilename, metadataDirectory));
        }
        #endregion // RPF File Construction Methods

        #region RPF File Construction (XGE Packet) Methods
        /// <summary>
        /// Create an XGE task object for a RPF creation process.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="process"></param>
        /// <param name="target"></param>
        /// <param name="filename"></param>
        /// <param name="files"></param>
        /// <param name="tool"></param>
        /// <returns></returns>
        public static XGE.ITask GetXgeRpfPackTask(IUniversalLog log,
            IProcess process, ITarget target, String filename,
            IEnumerable<Pair<String, String>> files, XGE.ITool tool)
        {
            if (!m_TargetRpfPackTaskCount.ContainsKey(target))
                m_TargetRpfPackTaskCount.Add(target, 0);

            String taskName = String.Format("RPF Create Packet {0} [{1}]",
                m_TargetRpfPackTaskCount[target]++, target.Platform);
            XGE.ITaskJob task = new XGE.TaskJob(taskName);

            try
            {
                String manifestFilename = String.Empty;
                Content.Asset manifestNode = process.AdditionalDependentContent.
                    OfType<Content.Asset>().
                    Where(asset => asset.AssetType.Equals(RSG.Platform.FileType.Manifest) &&
                        asset.Platform.Equals(RSG.Platform.Platform.Independent)).
                    FirstOrDefault();
                if (null != manifestNode)
                    manifestFilename = manifestNode.AbsolutePath;

                // RPF Sort; pass through metadata directory if it is defined.
                String metadataDirectory = String.Empty;
                if (process.Parameters.ContainsKey(Constants.RpfProcess_MetadataDirectory))
                    metadataDirectory = (String)process.Parameters[Constants.RpfProcess_MetadataDirectory];
                
                String logDirectory = Path.Combine(XGEFactory.GetTempDirectory(target.Branch),
                    "RagebuilderLogs");
                String logFilename = Path.Combine(logDirectory,
                    String.Format("RpfCreate_{0}_Ragebuilder.log", taskName.Replace(' ', '_')));
                String arguments = GetRpfCreateExecutableArguments(target,
                    filename, files, manifestFilename, metadataDirectory, logFilename, taskName);

                task.Caption = taskName;
                task.Parameters = arguments;
                task.InputFiles.AddRange(files.Select(p => p.First));
                task.OutputFiles.Add(filename);
                task.InheritParams = true;
                task.Tool = tool;

                // Define the XGE task for the IProcess.
                process.Parameters.Add(Constants.ProcessXGE_TaskName, taskName.Replace(' ', '_'));
                process.Parameters.Add(Constants.ProcessXGE_Task, task);
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception creating XGE task: {0}.", taskName);
            }

            return (task);
        }
        #endregion // RPF File Construction (XGE Packet) Methods

        #region RPF File Extraction Methods
        /// <summary>
        /// Extract all files from RPF.
        /// </summary>
        /// <param name="log">Log.</param>
        /// <param name="rpf">Rpf packfile</param>
        /// <param name="outputDirectory">outputdirectory for extraction</param>
        /// <param name="rpfFilename">the RPF packfile filename, as PackFile class does not have a name property.</param>
        /// <param name="overwrite"></param>
        /// <returns></returns>
        public static bool ExtractAll(IUniversalLog log, Packfile rpf, string outputDirectory, string rpfFilename, bool overwrite)
        {
            bool result = true;
            foreach (PackEntry packEntry in rpf.Entries)
            {
                string entryName = Path.Combine(packEntry.Path, packEntry.Name);
                string entryOutputPath = Path.Combine(outputDirectory, rpfFilename, packEntry.Path);

                if (!Directory.Exists(entryOutputPath))
                    Directory.CreateDirectory(entryOutputPath);

                string outputFilename = Path.Combine(entryOutputPath, packEntry.Name);
                try
                {
                    if (!overwrite && File.Exists(outputFilename))
                    {
                        log.Error("File already exists: {0}. Skipping.", outputFilename);
                        continue;
                    }

                    Byte[] buffer = rpf.Extract(entryName);
                    using (FileStream fs = File.OpenWrite(outputFilename))
                    {
                        log.Message("Extracting: {0}.", outputFilename);
                        fs.Write(buffer, 0, buffer.Length);
                    }
                }
                catch
                {
                    log.Error("Failed to create {0}.", outputFilename);
                    result = false;
                }
            }

            return result;
        }

        /// <summary>
        /// Extract all files from RPF.
        /// </summary>
        /// <param name="log">Log.</param>
        /// <param name="filename">RPF filename.</param>
        /// <param name="outputDir">Output directory.</param>
        /// <param name="overwrite">Overwrite force flag.</param>
        /// <returns></returns>
        public static bool ExtractAll(IUniversalLog log, String filename, String outputDir, bool overwrite)
        {
            Packfile packfile = new Packfile();
            packfile.Load(filename);
            string rpfFilename = Path.GetFileNameWithoutExtension(filename);

            bool result = ExtractAll(log, packfile, outputDir, rpfFilename, overwrite);

            packfile.Close();
            return result;
        }

        /// <summary>
        /// Read list of files in the zip archive.
        /// </summary>
        /// <param name="filename">Zipfile filename</param>
        /// <param name="filelist">Output list of files.</param>
        /// <returns></returns>
        public static bool GetFileList(String filename, out IEnumerable<String> filelist)
        {
            Debug.Assert(IsRpf(filename, true),
                String.Format("Not an RPF file: '{0}'.", filename));
            if (!IsRpf(filename))
            {
                filelist = new String[] { };
                return (false);
            }

            Packfile rpf = new Packfile();
            if (!rpf.Load(filename))
            {
                filelist = new String[] { };
                return (false);
            }

            List<String> entries = rpf.Entries.Select(entry => Path.Combine(entry.Path, entry.Name)).ToList();
            filelist = entries;

            return (true);
        }
        #endregion // RPF File Extraction Methods

        #region Utility Methods
        /// <summary>
        /// Calculate the nameheap size for a set of files.
        /// </summary>
        /// <param name="files">Input file pairs, source => RPF destination.</param>
        /// <returns>Nameheap size in bytes.</returns>
        /// 
        public static uint GetNameheapSize(IEnumerable<Pair<String, String>> files)
        {
            IEnumerable<String> destinationFiles = files.Select(f => f.Second.ToLower());
            uint size = 0;
            List<String> nameheap = new List<String>();
            foreach (String filename in destinationFiles)
            {
                if (filename.Contains(Path.DirectorySeparatorChar) ||
                    filename.Contains(Path.AltDirectorySeparatorChar))
                {
                    String[] parts = filename.Split(new char[] { 
                        Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar });
                    for (int i = 0; i < parts.Length; ++i)
                    {
                        if (i == parts.Length - 1)
                            nameheap.Add(parts[i]); // Last part; always add.
                        else if (!nameheap.Contains(parts[i]))
                        {
                            // Only add if not already in heap.
                            nameheap.Add(parts[i]);
                            size += (uint)parts[i].Length + 1; // For '0'.
                        }
                    }
                }
                else
                {
                    nameheap.Add(filename);
                    size += (uint)filename.Length + 1; // For '0'.
                }
            }

            return size;
        }

        /// <summary>
        /// Return approximate resultant RPF size.
        /// </summary>
        /// <param name="files"></param>
        /// <returns></returns>
        public static long GetApproximateRpfSize(IEnumerable<Pair<String, String>> files)
        {
            long fileSize = 2048 * 1024; // 2MB overhead for header crap.
            foreach (Pair<String, String> p in files)
            {
                FileInfo fi = new FileInfo(p.First);
                fileSize += fi.Length;
            }
            return (fileSize);
        }

        /// <summary>
        /// Return number of bytes free for output disk.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="files"></param>
        /// <returns></returns>
        public static long GetAvailableDiskSpace(String output)
        {
            long driveFreeSpace = 0L;
            foreach (DriveInfo di in DriveInfo.GetDrives())
            {
                if (di.Name[0] != output[0])
                    continue;

                driveFreeSpace = di.AvailableFreeSpace;
                break;
            }
            return (driveFreeSpace);
        }

        /// <summary>
        /// Return the rpf creation temporary directory.
        /// </summary>
        /// <returns></returns>
        public static String GetRpfCreateTempDir(IBranch branch)
        {
            return (branch.Environment.Subst(PATH_RPF_CREATE_TMP));
        }
        #endregion // Utility Methods

        #region Private Methods
        /// <summary>
        /// Return RpfCreate executable absolute filename.
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        private static String GetRpfCreateExecutable(ITarget target)
        {
            return (Path.Combine(target.Branch.Project.Config.ToolsRoot,
                "ironlib\\lib", "RSG.Pipeline.RpfCreate.exe"));
        }

        /// <summary>
        /// Return RpfCreate executable arguments.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="rpfFilename"></param>
        /// <param name="files"></param>
        /// <param name="manifestFilename"></param>
        /// <param name="metadataDirectory"></param>
        /// <param name="taskname"></param>
        /// <param name="logFilename"></param>
        /// <returns></returns>
        private static String GetRpfCreateExecutableArguments(ITarget target,
            String rpfFilename, IEnumerable<Pair<String, String>> files,
            String manifestFilename, String metadataDirectory, String logFilename, 
            String taskname = "")
        {
            // Write file list to a Ragebuilder input file.
            String tmp = String.Format("{0}\\{1}", GetRpfCreateTempDir(target.Branch), target.Platform);
            if (!Directory.Exists(tmp))
                Directory.CreateDirectory(tmp);

            IConfig config = target.Branch.Project.Config;
            // Support same named rpf in different build locations
            String fileName = rpfFilename.Replace("/", "\\").Replace(config.Project.DefaultBranch.Build, "").Replace("\\", "_");

            String filelistFilename = String.Format("{0}\\{1}.txt", tmp, Path.GetFileNameWithoutExtension(fileName));
            using (FileStream fs = new FileStream(filelistFilename, FileMode.Create, FileAccess.Write))
            {
                ISet<String> sourceSet = new HashSet<String>();
                sourceSet.AddRange(files.Select(p => p.First));
                Debug.Assert(sourceSet.Count() == files.Count());

                using (StreamWriter sw = new StreamWriter(fs, System.Text.Encoding.ASCII))
                {
                    foreach (Pair<String, String> filePair in files)
                        sw.WriteLine("{0}\t{1}", filePair.First, filePair.Second);
                }
            }

            StringBuilder arguments = new StringBuilder();
            if (!String.IsNullOrWhiteSpace(taskname))
                arguments.AppendFormat("-taskname {0} ", taskname.Replace(' ', '_'));
            arguments.AppendFormat("-branch {0} ", target.Branch.Name);
            arguments.AppendFormat("-platform {0} ", target.Platform);
            arguments.AppendFormat("-filelist {0} ", filelistFilename);
            arguments.AppendFormat("-output {0} ", rpfFilename);
            if (!String.IsNullOrWhiteSpace(manifestFilename))
                arguments.AppendFormat("-manifest {0} ", manifestFilename);
            if (!String.IsNullOrEmpty(metadataDirectory))
                arguments.AppendFormat("-metadata {0} ", metadataDirectory);
            arguments.AppendFormat("-logfile {0} ", logFilename);

            return (arguments.ToString());
        }

        /// <summary>
        /// Invoke RSG.Pipeline.RpfCreate; locally creating an RPF file for the 
        /// specified target platform (includes manifest support).
        /// </summary>
        /// <param name="log"></param>
        /// <param name="target"></param>
        /// <param name="rpfFilename"></param>
        /// <param name="files"></param>
        /// <param name="manifestFilename"></param>
        /// <param name="metadataDirectory"></param>
        /// <returns></returns>
        private static int PackRpf(IUniversalLog log, ITarget target,
            String rpfFilename, IEnumerable<Pair<String, String>> files,
            String manifestFilename, String metadataDirectory)
        {
            String logDirectory = Path.Combine(XGEFactory.GetTempDirectory(target.Branch),
                "RagebuilderLogs");
            String logFilename = Path.Combine(logDirectory, String.Format("RpfCreate_Ragebuilder.log"));
            String exe = GetRpfCreateExecutable(target);
            String arguments = GetRpfCreateExecutableArguments(target,
                rpfFilename, files, manifestFilename, metadataDirectory, logFilename);
            Command process = new Command(exe, arguments);

            log.DebugCtx(LOG_CTX, "Command line: \"{0} {1}\".", exe, arguments);
            int result = process.Run(log);
            bool hasErrors = false;
            Ragebuilder.ParseRagebuilderOutput(target.Branch, log, logFilename, 
                false, 0, out hasErrors);

            return (result);
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Services.Platform namespace
