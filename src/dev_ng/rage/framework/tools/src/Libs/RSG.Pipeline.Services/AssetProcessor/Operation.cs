﻿
namespace RSG.Pipeline.Services.AssetProcessor
{

    /// <summary>
    /// Asset Processor operations.
    /// </summary>
    /// <remarks>The order of those operation is used to sort them for processing.
    /// <see cref="AssetProcessorConfig"/>
    /// Copy, DrawableMerge, RenameAnimation or MergeFiles must be the top level operations in that enum.
    /// Try to keep the order meaningful (or fear my wrath)</remarks>
    public enum Operation
    {
        /// <summary>
        /// Copy asset; copy all input files into the output asset.
        /// </summary>
        /// This is typically used to pre-populate the output asset.
        /// <remarks>THIS IS A MAJOR Operation</remarks>
        Copy,

        /// <summary>
        /// Drawable merge; multiple drawables to a Drawable Dictionary.
        /// </summary>
        /// <remarks>THIS IS A MAJOR Operation</remarks>
        DrawableMerge,

        /// <summary>
        /// Rename animation to workaround Ragebuilder 
        /// </summary>
        /// <remarks>THIS IS A MAJOR Operation</remarks>
        RenameAnimation,

        /// <summary>
        /// Merge files together, with no regards to the source directory
        /// </summary>
        /// This operation differs from DrawableMerge. 
        /// DrawableMerge keeps source order in the form of folders. 
        /// Merge just takes every files in the source and puts it in the destination.
        /// <remarks>THIS IS A MAJOR Operation</remarks>
        MergeFiles,
        
        /// <summary>
        /// AddFiles to the drawables
        /// Example: Drawable bounds packing; packing bound data into drawables (typically for prop drawables).
        /// </summary>
        AddFiles,

        /// <summary>
        /// Remove files from the drawable
        /// Example: Texture strip; parented textures removed from drawables and texture dictionaries.
        /// </summary>
        RemoveFiles,

        /// <summary>
        /// Material presets; material preset transform to SVA files.
        /// </summary>
        MaterialPreset,

        /// <summary>
        /// Modularise the skeleton xml meta into seperate skeleton files.
        /// </summary>
        /// <remarks>THIS IS A MAJOR Operation</remarks>
        ModulariseSkeleton,
    }

} // RSG.Pipeline.Services.AssetProcessor namespace
