﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;

namespace RSG.Pipeline.Services.Platform.Manifest
{

    /// <summary>
    /// ITYP dependencies for another ITYP file.
    /// </summary>
    public class ITYPDependency2 : MetadataDependency
    {
        #region Properties
        /// <summary>
        /// ITYP basename.
        /// </summary>
        public String ITYPBasename
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ITYPDependency2()
        {
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Return XElement representing a CItypDependencies structure.
        /// </summary>
        /// <returns></returns>
        public XElement AsCItypDependencies(String name)
        {
            XElement xmlItem = new XElement(name,
                new XElement("itypName", this.ITYPBasename),
                new XElement("manifestFlags", this.IsInterior ? "INTERIOR_DATA" : String.Empty),
                new XElement("itypDepArray",
                    this.ITYPDependencyFiles.Select(ityp => new XElement("Item", ityp))));
            return (xmlItem);
        }
        #endregion // Controller Methods

        #region Static Controller Methods
        /// <summary>
        /// Create from manifest file XML element.
        /// </summary>
        /// <param name="xmlElem"></param>
        /// <returns></returns>
        public static ITYPDependency2 FromManifest(XElement xmlElem)
        {
            ITYPDependency2 dependency = new ITYPDependency2();
            dependency.ITYPBasename = xmlElem.XPathSelectElement("itypName").Value;
            dependency.IsInterior = xmlElem.XPathSelectElement("manifestFlags").Value.Contains("INTERIOR_DATA");
            dependency.ITYPDependencyFiles = xmlElem.XPathSelectElements("itypDepArray/Item").Select(s => s.Value);

            return (dependency);
        }

        /// <summary>
        /// Create from map export additions XML element (which is awkwardly
        /// different to how to serialise out for the manifest).
        /// </summary>
        /// <param name="xmlItem"></param>
        /// <returns></returns>
        public static ITYPDependency2 FromMapExportAdditions(XElement xmlElem)
        {
            Debug.Assert(null != xmlElem.Attributes("isInterior"),
                "Invalid XML element; no 'isInterior' attribute.");
            if (null == xmlElem.Attributes("isInterior"))
                throw new NotSupportedException("Invalid XML element; no 'isInterior' attribute.");

            Debug.Assert(null != xmlElem.Attributes("itypNames"),
                "Invalid XML element; no 'itypNames' attribute.");
            if (null == xmlElem.Attributes("itypNames"))
                throw new NotSupportedException("Invalid XML element; no 'itypNames' attribute.");

            ITYPDependency2 dependency = new ITYPDependency2();
            String[] itypNames = xmlElem.Attribute("itypNames").Value.Split(new char[] { ';' });

            dependency.IsInterior = bool.Parse(xmlElem.Attribute("isInterior").Value);
            dependency.ITYPDependencyFiles = itypNames;

            Debug.Assert(null != xmlElem.Attributes("itypName"),
                "Invalid XML element; no 'itypName' attribute.");
            if (null == xmlElem.Attributes("itypName"))
                throw new NotSupportedException("Invalid XML element; no 'itypName' attribute.");

            dependency.ITYPBasename = xmlElem.Attribute("itypName").Value;

            return (dependency);
        }
        #endregion // Static Controller Methods
    }

    /// <summary>
    /// ITYP dependencies for an IMAP file.
    /// </summary>
    public class IMAPDependency2 : MetadataDependency
    {
        #region Properties
        /// <summary>
        /// ITYP basename.
        /// </summary>
        public String IMAPBasename
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public IMAPDependency2()
        {
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Return XElement representing a CImapDependencies structure.
        /// </summary>
        /// <returns></returns>
        public XElement AsCImapDependencies(String name)
        {
            XElement xmlItem = new XElement(name,
                new XElement("imapName", this.IMAPBasename),
                new XElement("manifestFlags", this.IsInterior ? "INTERIOR_DATA" : String.Empty),
                new XElement("itypDepArray",
                    this.ITYPDependencyFiles.Select(ityp => new XElement("Item", ityp))));
            return (xmlItem);
        }
        #endregion // Controller Methods

        #region Static Controller Methods
        /// <summary>
        /// Create from manifest file XML element.
        /// </summary>
        /// <param name="xmlElem"></param>
        /// <returns></returns>
        public static IMAPDependency2 FromManifest(XElement xmlElem)
        {
            IMAPDependency2 dependency = new IMAPDependency2();
            dependency.IMAPBasename = xmlElem.XPathSelectElement("imapName").Value;
            dependency.IsInterior = xmlElem.XPathSelectElement("manifestFlags").Value.Contains("INTERIOR_DATA");
            dependency.ITYPDependencyFiles = xmlElem.XPathSelectElements("itypDepArray/Item").Select(s => s.Value);

            return (dependency);
        }

        /// <summary>
        /// Create from map export additions XML element (which is awkwardly
        /// different to how to serialise out for the manifest).
        /// </summary>
        /// <param name="xmlItem"></param>
        /// <returns></returns>
        public static IMAPDependency2 FromMapExportAdditions(XElement xmlElem)
        {
            Debug.Assert(null != xmlElem.Attributes("isInterior"),
                "Invalid XML element; no 'isInterior' attribute.");
            if (null == xmlElem.Attributes("isInterior"))
                throw new NotSupportedException("Invalid XML element; no 'isInterior' attribute.");

            Debug.Assert(null != xmlElem.Attributes("itypNames"),
                "Invalid XML element; no 'itypNames' attribute.");
            if (null == xmlElem.Attributes("itypNames"))
                throw new NotSupportedException("Invalid XML element; no 'itypNames' attribute.");

            IMAPDependency2 dependency = new IMAPDependency2();
            String[] itypNames = xmlElem.Attribute("itypNames").Value.Split(new char[] { ';' });

            dependency.IsInterior = bool.Parse(xmlElem.Attribute("isInterior").Value);
            dependency.ITYPDependencyFiles = itypNames;

            Debug.Assert(null != xmlElem.Attributes("imapName"),
                "Invalid XML element; no 'imapName' attribute.");
            if (null == xmlElem.Attributes("imapName"))
                throw new NotSupportedException("Invalid XML element; no 'imapName' attribute.");

            dependency.IMAPBasename = xmlElem.Attribute("imapName").Value;

            return (dependency);
        }
        #endregion // Static Controller Methods
    }

    /// <summary>
    /// (OLD) ITYP dependency for an IMAP file (singular, old).
    /// </summary>
    public class IMAPDependency : MetadataDependency
    {
        #region Properties
        /// <summary>
        /// ITYP basename.
        /// </summary>
        public String IMAPBasename
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public IMAPDependency()
        {
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Return XElement representing a CImapDependencies structure.
        /// </summary>
        /// <returns></returns>
        public XElement AsCImapDependency(String name)
        {
            XElement xmlItem = new XElement(name,
                new XElement("imapName", this.IMAPBasename),
                new XElement("itypName", this.ITYPDependencyFiles.First()));
            return (xmlItem);
        }
        #endregion // Controller Methods

        #region Static Controller Methods
        /// <summary>
        /// Create from manifest file XML element.
        /// </summary>
        /// <param name="xmlElem"></param>
        /// <returns></returns>
        public static IMAPDependency FromManifest(XElement xmlElem)
        {
            IMAPDependency dependency = new IMAPDependency();
            dependency.IMAPBasename = xmlElem.XPathSelectElement("imapName").Value;            
            dependency.ITYPDependencyFiles = xmlElem.XPathSelectElements("itypName").Select(s => s.Value);

            return (dependency);
        }

        /// <summary>
        /// Create from map export additions XML element (which is awkwardly
        /// different to how to serialise out for the manifest).
        /// </summary>
        /// <param name="xmlItem"></param>
        /// <returns></returns>
        public static IMAPDependency FromMapExportAdditions(XElement xmlElem)
        {
            Debug.Assert(null != xmlElem.Attributes("imapName"),
                "Invalid XML element; no 'imapName' attribute.");
            if (null == xmlElem.Attributes("imapName"))
                throw new NotSupportedException("Invalid XML element; no 'imapName' attribute.");

            Debug.Assert(null != xmlElem.Attributes("interiorFilename"),
                "Invalid XML element; no 'interiorFilename' attribute.");
            if (null == xmlElem.Attributes("interiorFilename"))
                throw new NotSupportedException("Invalid XML element; no 'interiorFilename' attribute.");

            ICollection<String> itypNames = new List<String>();
            IMAPDependency dependency = new IMAPDependency();
            dependency.IMAPBasename = xmlElem.Attribute("imapName").Value;
            itypNames.Add(xmlElem.Attribute("interiorFilename").Value);
            dependency.ITYPDependencyFiles = itypNames;
            
            return (dependency);
        }
        #endregion // Static Controller Methods
    }

    /// <summary>
    /// Abstract base class for metadata dependency information.
    /// </summary>
    public abstract class MetadataDependency
    {
        #region Properties
        /// <summary>
        /// Flag whether this is an interior.
        /// </summary>
        public bool IsInterior
        {
            get;
            set;
        }

        /// <summary>
        /// IMAP basenames that are dependencies.
        /// </summary>
        public IEnumerable<String> ITYPDependencyFiles
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public MetadataDependency()
        {
        }
        #endregion // Constructor(s)
    }

} // RSG.Pipeline.Services.Platform.Manifest namespace
