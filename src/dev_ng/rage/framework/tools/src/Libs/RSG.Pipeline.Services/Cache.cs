﻿using RSG.Base.Logging.Universal;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.Services
{
    /// <summary>
    /// Helper class for operations on the cache.
    /// </summary>
    public static class Cache
    {
        /// <summary>
        /// Recursive directory delete
        /// </summary>
        /// <param name="log"></param>
        /// <param name="directory"></param>
        /// <returns></returns>
        public static bool ClearDirectory(IUniversalLog log, String directory)
        {
            // We specifically don't do "di.Delete(true)" as that destroys the
            // top-level folder and its metadata.
            bool result = true;
            DirectoryInfo di = new DirectoryInfo(directory);
            foreach (FileInfo fi in di.GetFiles())
            {
                try
                {
                    Console.Write(".");
                    fi.IsReadOnly = false;
                    fi.Delete();
                }
                catch (IOException)
                {
                    log.Error("Failed to delete file: {0}", fi.FullName);
                    result = false;
                }
            }

            foreach (DirectoryInfo sdi in di.GetDirectories())
            {
                Console.WriteLine();
                Console.Write("[{0}] ", sdi.FullName);
                result &= ClearDirectory(log, sdi.FullName);
                sdi.Delete();
            }

            return (result);
        }
    }
}
