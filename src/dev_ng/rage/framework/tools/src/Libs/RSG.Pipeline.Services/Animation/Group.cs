﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace RSG.Pipeline.Services.Animation
{
    /// <summary>
    /// A object containing clips with shared clip properties
    /// </summary>
    public class Group
    {
        #region Properties
        public List<string> ClipFiles
        {
            get { return _ClipFiles; }
            set { _ClipFiles = value; }
        }
        List<string> _ClipFiles;

        public string CompressionTemplate
        {
            get { return _CompressionTemplate; }
            set { _CompressionTemplate = value; }
        }
        string _CompressionTemplate;

        public string SkeletonFile
        {
            get { return _SkeletonFile; }
            set { _SkeletonFile = value; }
        }
        string _SkeletonFile;

        public bool Additive
        {
            get { return _Additive; }
            set { _Additive = value; }
        }
        bool _Additive;
        #endregion // Properties

        #region Methods
        public void WriteClipList(string outputDir)
        {
            // #{compression_key}@#{skeleton_key}@#{additive_key}.cliplist
            string fileName = string.Format("{0}@{1}@{2}.cliplist", Path.GetFileNameWithoutExtension(_CompressionTemplate), Path.GetFileNameWithoutExtension(_SkeletonFile), Path.GetFileNameWithoutExtension(_Additive == true ? "1" : "0"));
            string filePath = Path.Combine(outputDir, fileName);
            using (StreamWriter sw = new StreamWriter(filePath))
            {
                foreach (string clipFile in _ClipFiles)
                {
                    sw.WriteLine(clipFile);
                }
            }
        }
        #endregion // Methods

        #region Constructor(s)
        public Group()
        {
            _ClipFiles = new List<string>();
        }
        #endregion
    }
}
