﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using SIO = System.IO;
using RSG.Base.Logging.Universal;

namespace RSG.Pipeline.Services.Metadata
{
    //TODO:  Move this into another place?
    public enum InstancePlacementType
    {
        Unknown,
        Grass,
        Debris,
        Trees
    }

    public enum InstancePlacementTextureType
    {
        Default,
        Global_Tint,
        Global_Data
    }

    /// <summary>
    /// Instance Placement Serialiser input object.
    /// </summary>
    public class InstancePlacementSerialiserInput
    {
        #region Properties
        /// <summary>
        /// SceneXml absolute filename.
        /// </summary>
        public String SceneFilename
        {
            get;
            private set;
        }

        /// <summary>
        /// Collision input path.
        /// </summary>
        public String CollisionPath
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="sceneFilename"></param>
        public InstancePlacementSerialiserInput(String sceneFilename, String collisionPath)
        {
            this.SceneFilename = sceneFilename;
            this.CollisionPath = collisionPath;
        }

        /// <summary>
        /// Constructor; from XML element.
        /// </summary>
        /// <param name="xmlInputElem"></param>
        public InstancePlacementSerialiserInput(XElement xmlInputElem)
        {
            Debug.Assert(0 == String.Compare(xmlInputElem.Name.LocalName, "Input"),
                "Invalid 'Input' element.");

            this.SceneFilename = xmlInputElem.Attribute("scenexml").Value;
            this.CollisionPath = xmlInputElem.Attribute("collision").Value;
        }
        #endregion // Constructor(s)

        #region Internal Methods
        /// <summary>
        /// Serialise input to an XElement.
        /// </summary>
        /// <returns></returns>
        internal XElement Serialise()
        {
            XElement xmlInputElem = new XElement("Input",
                new XAttribute("scenexml", this.SceneFilename),
                new XAttribute("collision", this.CollisionPath)
            ); // Input
            return (xmlInputElem);
        }
        #endregion // Internal Methods
    }

    /// <summary>
    /// Instance Placement Serialiser Intersection Input object.
    /// </summary>
    public class InstancePlacementIntersectionInput
    {
        #region Properties
        /// <summary>
        /// Container name
        /// </summary>
        public String ContainerName
        {
            get;
            private set;
        }

        /// <summary>
        /// Collision input path.
        /// </summary>
        public String CollisionPath
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="sceneFilename"></param>
        public InstancePlacementIntersectionInput(String containerName, String collisionPath)
        {
            this.ContainerName = containerName;
            this.CollisionPath = collisionPath;
        }

        /// <summary>
        /// Constructor; from XML element.
        /// </summary>
        /// <param name="xmlInputElem"></param>
        public InstancePlacementIntersectionInput(XElement xmlInputElem)
        {
            Debug.Assert(0 == String.Compare(xmlInputElem.Name.LocalName, "IntersectionInput"),
                "Invalid 'IntersectionInput' element.");

            this.ContainerName = xmlInputElem.Attribute("container").Value;
            this.CollisionPath = xmlInputElem.Attribute("collision").Value;
        }
        #endregion // Constructor(s)

        #region Internal Methods
        /// <summary>
        /// Serialise input to an XElement.
        /// </summary>
        /// <returns></returns>
        internal XElement Serialise()
        {
            XElement xmlInputElem = new XElement("IntersectionInput",
                new XAttribute("container", this.ContainerName),
                new XAttribute("collision", this.CollisionPath)
            ); // Input
            return (xmlInputElem);
        }
        #endregion // Internal Methods
    }

    /// <summary>
    /// Instance Placement Serialiser input texture object.
    /// </summary>
    public class InstancePlacementSerialiserInputTexture
    {
        #region Properties

        /// <summary>
        /// Input texture absolute file path.
        /// </summary>
        public String TextureFilename
        {
            get;
            private set;
        }

        public InstancePlacementTextureType Type
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="sceneFilename"></param>
        public InstancePlacementSerialiserInputTexture(String textureFilename, InstancePlacementTextureType type = InstancePlacementTextureType.Default)
        {
            this.TextureFilename = textureFilename;
            this.Type = type;
        }

        /// <summary>
        /// Constructor; from XML element.
        /// </summary>
        /// <param name="xmlInputElem"></param>
        public InstancePlacementSerialiserInputTexture(XElement xmlInputElem)
        {
            Debug.Assert(0 == String.Compare(xmlInputElem.Name.LocalName, "InputTexture"),
                "Invalid 'InputTexture' element.");

            this.TextureFilename = xmlInputElem.Attribute("texture").Value;
            this.Type = (InstancePlacementTextureType)Enum.Parse(typeof(InstancePlacementTextureType), xmlInputElem.Attribute("texture_type").Value);
        }
        #endregion // Constructor(s)

        #region Internal Methods
        /// <summary>
        /// Serialise input to an XElement.
        /// </summary>
        /// <returns></returns>
        internal XElement Serialise()
        {
            XElement xmlInputElem = new XElement("InputTexture",
                new XAttribute("texture", this.TextureFilename == null ? "" : this.TextureFilename),
                new XAttribute("texture_type", this.Type.ToString())
            ); // Input
            return (xmlInputElem);
        }
        #endregion // Internal Methods
    }

    /// <summary>
    /// Instance Placement Serialiser Texture output object.
    /// </summary>
    public class InstancePlacementTextureSerialiserOutput
    {
        #region Properties

        /// <summary>
        /// Output texture absolute file path.
        /// </summary>
        public String TextureFilename
        {
            get;
            private set;
        }

        /// <summary>
        /// Input collision absolute file path.
        /// </summary>
        public String CollisionPath
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="sceneFilename"></param>
        public InstancePlacementTextureSerialiserOutput(String textureFilename, String collisionPath)
        {
            this.TextureFilename = textureFilename;
            this.CollisionPath = collisionPath;
        }

        /// <summary>
        /// Constructor; from XML element.
        /// </summary>
        /// <param name="xmlInputElem"></param>
        public InstancePlacementTextureSerialiserOutput(XElement xmlInputElem)
        {
            Debug.Assert(0 == String.Compare(xmlInputElem.Name.LocalName, "Output"),
                "Invalid 'Output' element.");

            this.TextureFilename = xmlInputElem.Attribute("texture").Value;
            this.CollisionPath = xmlInputElem.Attribute("collision").Value;
        }
        #endregion // Constructor(s)

        #region Internal Methods
        /// <summary>
        /// Serialise input to an XElement.
        /// </summary>
        /// <returns></returns>
        internal XElement Serialise()
        {
            XElement xmlInputElem = new XElement("Output",
                new XAttribute("texture", this.TextureFilename == null ? "" : this.TextureFilename),
                new XAttribute("collision", this.CollisionPath == null ? "" : this.CollisionPath)
            ); // Input
            return (xmlInputElem);
        }
        #endregion // Internal Methods
    }

    /// <summary>
    /// Instance Placement Serialiser output object.
    /// </summary>
    public class InstancePlacementSerialiserOutput
    {
        #region Properties

        /// <summary>
        /// SceneXml absolute filename.
        /// </summary>
        public String OutputDirectory
        {
            get;
            private set;
        }

        /// <summary>
        /// SceneXml absolute filename.
        /// </summary>
        public String OutputFile
        {
            get;
            set;
        }

        /// <summary>
        /// High detail input path.
        /// </summary>
        public String HighDetailOutputFile
        {
            get;
            set;
        }

        /// <summary>
        /// Scene XML output path.
        /// </summary>
        public String SceneXml
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public String ManifestFile { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="sceneFilename"></param>
        public InstancePlacementSerialiserOutput(String outputDirectory, String outputFile, String highDetailOutputFile, String sceneXmlOutputFile, String manifestFile)
        {
            this.OutputDirectory = outputDirectory;
            this.OutputFile = outputFile;
            this.HighDetailOutputFile = highDetailOutputFile;
            this.SceneXml = sceneXmlOutputFile;
            this.ManifestFile = manifestFile;
        }

        /// <summary>
        /// Constructor; from XML element.
        /// </summary>
        /// <param name="xmlInputElem"></param>
        public InstancePlacementSerialiserOutput(XElement xmlOutputElem)
        {
            Debug.Assert(0 == String.Compare(xmlOutputElem.Name.LocalName, "Output"),
                "Invalid 'Output' element.");

            this.OutputDirectory = xmlOutputElem.Attribute("output_directory").Value;
            this.OutputFile = xmlOutputElem.Attribute("output_file").Value;
            this.HighDetailOutputFile = xmlOutputElem.Attribute("high_detail_output_file").Value;
            this.SceneXml = xmlOutputElem.Attribute("scenexml").Value;
            this.ManifestFile = xmlOutputElem.Attribute("manifest_file").Value;
        }
        #endregion // Constructor(s)

        #region Internal Methods
        /// <summary>
        /// Serialise output to an XElement.
        /// </summary>
        /// <returns></returns>
        internal XElement Serialise()
        {
            XElement xmlInputElem = new XElement("Output",
                new XAttribute("output_directory", this.OutputDirectory == null ? "" : this.OutputDirectory),
                new XAttribute("output_file", this.OutputFile == null ? "" : this.OutputFile),
                new XAttribute("high_detail_output_file", this.HighDetailOutputFile == null ? "" : this.HighDetailOutputFile),
                new XAttribute("scenexml", this.SceneXml == null ? "" : this.SceneXml),
                new XAttribute("manifest_file", this.ManifestFile == null ? "" : this.ManifestFile)
            ); // Input
            return (xmlInputElem);
        }
        #endregion // Internal Methods
    }

    /// <summary>
    /// Instance Placement Texture Serialiser task.
    /// </summary>
    public class InstancePlacementTextureSerialiserTask
    {
        #region Properties
        /// <summary>
        /// Task name.
        /// </summary>
        public String Name
        {
            get;
            private set;
        }

        /// <summary>
        /// Bounds Manifest XML filename
        /// </summary>
        public String BoundsManifestXML
        {
            get;
            private set;
        }

        /// <summary>
        /// Input Texture
        /// </summary>
        public InstancePlacementSerialiserInputTexture InputTexture
        {
            get;
            private set;
        }

        /// <summary>
        /// Output Textures
        /// </summary>
        public IEnumerable<InstancePlacementTextureSerialiserOutput> Output
        {
            get;
            private set;
        }
        #endregion

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="inputTexture"></param>
        /// <param name="output"></param>
        public InstancePlacementTextureSerialiserTask(String name, String boundsManifestXML, InstancePlacementSerialiserInputTexture inputTexture, IEnumerable<InstancePlacementTextureSerialiserOutput> output)
        {
            this.Name = name;
            this.BoundsManifestXML = boundsManifestXML;
            this.InputTexture = inputTexture;
            this.Output = output;
        }

        /// <summary>
        /// Constructor; from serialised XML configuration file.
        /// </summary>
        /// <param name="xmlTaskElem"></param>
        public InstancePlacementTextureSerialiserTask(XElement xmlTaskElem)
        {
            Debug.Assert(0 == String.Compare(xmlTaskElem.Name.LocalName, "Task"),
                "Invalid 'Task' element.");

            this.Name = xmlTaskElem.Attribute("name").Value;
            this.BoundsManifestXML = xmlTaskElem.Attribute("boundsManifest").Value;
            this.InputTexture = xmlTaskElem.XPathSelectElements("InputTexture").
                Select(xmlInputElem => new InstancePlacementSerialiserInputTexture(xmlInputElem)).First();
            this.Output = xmlTaskElem.XPathSelectElements("Output").
                Select(xmlOutputElem => new InstancePlacementTextureSerialiserOutput(xmlOutputElem));
        }
        #endregion

        #region Internal Methods
        /// <summary>
        /// Serialise task to an XElement.
        /// </summary>
        /// <returns></returns>
        internal XElement Serialise()
        {
            XElement xmlTaskElem = new XElement("Task",
                new XAttribute("name", this.Name),
                new XAttribute("boundsManifest", this.BoundsManifestXML),
                this.InputTexture.Serialise(),
                this.Output.Select(output => output.Serialise())
            ); // Task
            return (xmlTaskElem);
        }
        #endregion // Internal Methods
    }

    /// <summary>
    /// Instance Placement Serialiser task.
    /// </summary>
    public class InstancePlacementSerialiserTask
    {
        #region Properties
        /// <summary>
        /// Task name.
        /// </summary>
        public String Name
        {
            get;
            private set;
        }

        public IEnumerable<InstancePlacementSerialiserInput> Inputs
        {
            get;
            private set;
        }

        public IEnumerable<InstancePlacementIntersectionInput> IntersectionInputs
        {
            get;
            private set;
        }

        public IEnumerable<InstancePlacementSerialiserInputTexture> InputTextures
        {
            get;
            private set;
        }

        public IEnumerable<InstancePlacementSerialiserOutput> Outputs
        {
            get;
            private set;
        }

        public IEnumerable<String> SceneXmlDependencies
        {
            get;
            private set;
        }

        public InstancePlacementType PlacementType
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="inputs"></param>
        /// <param name="dependencies"></param>
        /// <param name="output"></param>
        /// <param name="assetCombineFilename"></param>
        public InstancePlacementSerialiserTask(String name, 
            IEnumerable<InstancePlacementSerialiserInput> inputs,
            IEnumerable<InstancePlacementIntersectionInput> intersectionInputs,
            IEnumerable<InstancePlacementSerialiserInputTexture> inputTextures,
            IEnumerable<InstancePlacementSerialiserOutput> outputs,
            IEnumerable<String> sceneXmlDependencies,
            InstancePlacementType placementType)
        {
            this.Name = name;
            this.Inputs = inputs;
            this.IntersectionInputs = intersectionInputs;
            this.InputTextures = inputTextures;
            this.Outputs = outputs;
            this.SceneXmlDependencies = sceneXmlDependencies;
            this.PlacementType = placementType;
        }

        /// <summary>
        /// Constructor; from serialised XML configuration file.
        /// </summary>
        /// <param name="xmlTaskElem"></param>
        public InstancePlacementSerialiserTask(XElement xmlTaskElem)
        {
            Debug.Assert(0 == String.Compare(xmlTaskElem.Name.LocalName, "Task"),
                "Invalid 'Task' element.");

            this.Name = xmlTaskElem.Attribute("name").Value;
            this.Inputs = xmlTaskElem.XPathSelectElements("Input").
                Select(xmlInputElem => new InstancePlacementSerialiserInput(xmlInputElem));
            this.IntersectionInputs = xmlTaskElem.XPathSelectElements("IntersectionInput").
                Select(xmlInputElem => new InstancePlacementIntersectionInput(xmlInputElem));
            this.InputTextures = xmlTaskElem.XPathSelectElements("InputTexture").
                Select(xmlInputTextureElem => new InstancePlacementSerialiserInputTexture(xmlInputTextureElem));
            this.Outputs = xmlTaskElem.XPathSelectElements("Output").
                Select(xmlOutputElem => new InstancePlacementSerialiserOutput(xmlOutputElem));
            this.SceneXmlDependencies = xmlTaskElem.XPathSelectElements("SceneXMLDependency").
                Select(xmlSceneDependencyElem => xmlSceneDependencyElem.Attribute("file").Value);
            this.PlacementType = (InstancePlacementType)Enum.Parse(typeof(InstancePlacementType), xmlTaskElem.Attribute("placement_type").Value);
        }
        #endregion // Constructor(s)

        #region Internal Methods
        /// <summary>
        /// Serialise task to an XElement.
        /// </summary>
        /// <returns></returns>
        internal XElement Serialise()
        {
            XElement xmlTaskElem = new XElement("Task",
                new XAttribute("name", this.Name),
                new XAttribute("placement_type", this.PlacementType),
                this.Inputs.Select(input => input.Serialise()),
                this.IntersectionInputs.Select(intersectionInput => intersectionInput.Serialise()),
                this.InputTextures.Select(inputTexture => inputTexture.Serialise()),
                this.SceneXmlDependencies.Select(sceneXml => new XElement("SceneXMLDependency",
                    new XAttribute("file", sceneXml))),
                this.Outputs.Select(output => output.Serialise())
            ); // Task
            return (xmlTaskElem);
        }
        #endregion // Internal Methods
    }

    public class InstancePlacementSerialiserOptions
    {
        #region Constructor(s)
        public InstancePlacementSerialiserOptions()
        {
            DegreeOfParallelism = defaultMaxDegreesOfParallelism_;
            MaxBatchesPerImap = defaultMaxBatchesPerIMAP_;
            MinBatchesPerImap = defaultMinBatchesPerIMAP_;
            MaxInstancesPerBatch = defaultMaxInstancesPerBatch_;
            MinInstancesPerBatch = defaultMinInstancesPerBatch_;
            MaxBoundLength = defaultMaxBoundLength_;
            Volume = defaultVolume_;
            Occupancy = defaultOccupancy_;
            GenerateDebugStats = defaultGenerateDebugStats_;
            UseAveragePositionSplitting = defaultAveragePositionSplitting_;
        }

        /// <summary>
        /// Parameters that come from the pipeline in the processors XML file.
        /// </summary>
        /// <param name="parameters"></param>
        public InstancePlacementSerialiserOptions(IDictionary<String, object> parameters)
        {
            this.DegreeOfParallelism = parameters.ContainsKey(MaxDegreesOfParallelism_Tag) ?
                (int)parameters[MaxDegreesOfParallelism_Tag] : defaultMaxDegreesOfParallelism_;

            this.MaxBatchesPerImap = parameters.ContainsKey(MaxBatchesPerIMAP_Tag) ?
                (int)parameters[MaxBatchesPerIMAP_Tag] : defaultMaxBatchesPerIMAP_;

            this.MinBatchesPerImap = parameters.ContainsKey(MinBatchesPerIMAP_Tag) ?
                (int)parameters[MinBatchesPerIMAP_Tag] : defaultMinBatchesPerIMAP_;

            this.MaxInstancesPerBatch = parameters.ContainsKey(MaxInstancesPerBatch_Tag) ?
                (int)parameters[MaxInstancesPerBatch_Tag] : defaultMaxInstancesPerBatch_;

            this.MinInstancesPerBatch = parameters.ContainsKey(MinInstancesPerBatch_Tag) ?
                (int)parameters[MinInstancesPerBatch_Tag] : defaultMinInstancesPerBatch_;

            this.MaxBoundLength = parameters.ContainsKey(MaxBoundLength_Tag) ?
                (int)parameters[MaxBoundLength_Tag] : defaultMaxBoundLength_;

            this.Volume = parameters.ContainsKey(Volume_Tag) ?
                (int)parameters[Volume_Tag] : defaultVolume_;

            this.Occupancy = parameters.ContainsKey(Occupancy_Tag) ?
                (float)parameters[Occupancy_Tag] : defaultOccupancy_;

            this.GenerateDebugStats = parameters.ContainsKey(GenerateDebugStats_Tag) ?
                (bool)parameters[GenerateDebugStats_Tag] : defaultGenerateDebugStats_;

            this.UseAveragePositionSplitting = parameters.ContainsKey(UseAveragePositionSplitting_Tag) ?
                (bool)parameters[UseAveragePositionSplitting_Tag] : defaultAveragePositionSplitting_;
        }

        internal InstancePlacementSerialiserOptions(XElement xmlTaskElem)
        {
            this.DegreeOfParallelism = xmlTaskElem.Element(MaxDegreesOfParallelism_Tag) != null ? 
                int.Parse(xmlTaskElem.Element(MaxDegreesOfParallelism_Tag).Value) : defaultMaxDegreesOfParallelism_;

            this.MaxBatchesPerImap = xmlTaskElem.Element(MaxBatchesPerIMAP_Tag) != null ?
                int.Parse(xmlTaskElem.Element(MaxBatchesPerIMAP_Tag).Value) : defaultMaxBatchesPerIMAP_;

            this.MinBatchesPerImap = xmlTaskElem.Element(MinBatchesPerIMAP_Tag) != null ?
                int.Parse(xmlTaskElem.Element(MinBatchesPerIMAP_Tag).Value) : defaultMinBatchesPerIMAP_;

            this.MaxInstancesPerBatch = xmlTaskElem.Element(MaxInstancesPerBatch_Tag) != null ?
                int.Parse(xmlTaskElem.Element(MaxInstancesPerBatch_Tag).Value) : defaultMaxInstancesPerBatch_;

            this.MinInstancesPerBatch = xmlTaskElem.Element(MinInstancesPerBatch_Tag) != null ?
                int.Parse(xmlTaskElem.Element(MinInstancesPerBatch_Tag).Value) : defaultMinInstancesPerBatch_;

            this.MaxBoundLength = xmlTaskElem.Element(MaxBoundLength_Tag) != null ?
                int.Parse(xmlTaskElem.Element(MaxBoundLength_Tag).Value) : defaultMaxBoundLength_;

            this.Volume = xmlTaskElem.Element(Volume_Tag) != null ?
                int.Parse(xmlTaskElem.Element(Volume_Tag).Value) : defaultVolume_;

            this.Occupancy = xmlTaskElem.Element(Occupancy_Tag) != null ?
                float.Parse(xmlTaskElem.Element(Occupancy_Tag).Value) : defaultOccupancy_;

            this.GenerateDebugStats = xmlTaskElem.Element(GenerateDebugStats_Tag) != null ?
                bool.Parse(xmlTaskElem.Element(GenerateDebugStats_Tag).Value) : defaultGenerateDebugStats_;

            this.UseAveragePositionSplitting = xmlTaskElem.Element(UseAveragePositionSplitting_Tag) != null ?
                bool.Parse(xmlTaskElem.Element(UseAveragePositionSplitting_Tag).Value) : defaultAveragePositionSplitting_;
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// Number of threads to spawn within the process.
        /// </summary>
        public int DegreeOfParallelism
        {
            get;
            private set;
        }

        /// <summary>
        /// Maximum number of batches that can exist in an IMAP
        /// </summary>
        public int MaxBatchesPerImap
        {
            get;
            private set;
        }

        /// <summary>
        /// Minimum number of batches that can exist in an IMAP
        /// </summary>
        public int MinBatchesPerImap
        {
            get;
            private set;
        }

        /// <summary>
        /// Maximum number of instances that can exist in a batch.
        /// </summary>
        public int MaxInstancesPerBatch
        {
            get;
            private set;
        }

        /// <summary>
        /// Minimum number of instances that can exist in a batch.
        /// </summary>
        public int MinInstancesPerBatch
        {
            get;
            private set;
        }

        /// <summary>
        /// Maximum bound size of a batch (max X max)
        /// </summary>
        public int MaxBoundLength
        {
            get;
            private set;
        }

        /// <summary>
        /// Volume of instances.
        /// </summary>
        public int Volume
        {
            get;
            private set;
        }

        /// <summary>
        /// Occupancy in batches.
        /// </summary>
        public float Occupancy
        {
            get;
            private set;
        }

        /// <summary>
        /// Generate debug stats that include batch/instance information.
        /// Will also generate a vector map with the batch/instance/bound information.
        /// </summary>
        public bool GenerateDebugStats
        {
            get;
            private set;
        }

        /// <summary>
        /// When splitting the bounding boxes, this will average the positions
        /// to find the best split instead of splitting down the middle.
        /// </summary>
        public bool UseAveragePositionSplitting
        {
            get;
            private set;
        }

        #endregion

        #region Internal Methods
        /// <summary>
        /// Serialise to an XElement.
        /// </summary>
        /// <returns></returns>
        internal XElement Serialise()
        {
            XElement xmlTaskElem = new XElement("Options",
                new XElement(MaxDegreesOfParallelism_Tag, this.DegreeOfParallelism),
                new XElement(MaxBatchesPerIMAP_Tag, this.MaxBatchesPerImap),
                new XElement(MinBatchesPerIMAP_Tag, this.MinBatchesPerImap),
                new XElement(MaxInstancesPerBatch_Tag, this.MaxInstancesPerBatch),
                new XElement(MinInstancesPerBatch_Tag, this.MinInstancesPerBatch),
                new XElement(MaxBoundLength_Tag, this.MaxBoundLength),
                new XElement(Volume_Tag, this.Volume),
                new XElement(Occupancy_Tag, this.Occupancy),
                new XElement(GenerateDebugStats_Tag, this.GenerateDebugStats),
                new XElement(UseAveragePositionSplitting_Tag, this.UseAveragePositionSplitting)
            ); // Options
            return (xmlTaskElem);
        }
        #endregion // Internal Methods

        #region Constants
        private static readonly int     defaultMaxDegreesOfParallelism_ = -1;
        private static readonly int     defaultMaxBatchesPerIMAP_ = 250;
        private static readonly int     defaultMinBatchesPerIMAP_ = 2;
        private static readonly int     defaultMaxInstancesPerBatch_ = 200;
        private static readonly int     defaultMinInstancesPerBatch_ = 2;
        private static readonly int     defaultMaxBoundLength_ = 512;
        private static readonly int     defaultVolume_ = 50;
        private static readonly float   defaultOccupancy_ = 0.10f;
        private static readonly bool    defaultGenerateDebugStats_ = true;
        private static readonly bool    defaultAveragePositionSplitting_ = true;

        private static readonly string  MaxDegreesOfParallelism_Tag = "DegreeOfParallelism";
        private static readonly string  MaxBatchesPerIMAP_Tag = "MaxBatchesPerIMAP";
        private static readonly string  MinBatchesPerIMAP_Tag = "MinBatchesPerIMAP";
        private static readonly string  MaxInstancesPerBatch_Tag = "MaxInstancesPerBatch";
        private static readonly string  MinInstancesPerBatch_Tag = "MinInstancesPerBatch";
        private static readonly string  MaxBoundLength_Tag = "MaxBoundLength";
        private static readonly string  Volume_Tag = "Volume";
        private static readonly string  Occupancy_Tag = "Occupancy";
        private static readonly string  GenerateDebugStats_Tag = "GenerateDebugStats";
        private static readonly string UseAveragePositionSplitting_Tag = "AveragePositionSplitting";
        #endregion
    }

    /// <summary>
    /// Instance Placement Serialiser configuration data; loaded from XML.
    /// </summary>
    public class InstancePlacementSerialiserConfig
    {
        #region Properties
        public InstancePlacementSerialiserOptions Options
        {
            get;
            private set;
        }

        /// <summary>
        /// Instance Placement Serialiser tasks.
        /// </summary>
        public IEnumerable<InstancePlacementSerialiserTask> Tasks
        {
            get;
            private set;
        }

        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public InstancePlacementSerialiserConfig(InstancePlacementSerialiserOptions options,
                                           IEnumerable<InstancePlacementSerialiserTask> tasks)
        {
            this.Options = options;
            this.Tasks = tasks.ToArray();
        }

        /// <summary>
        /// Constructor; loading configuration data from XML document.
        /// </summary>
        /// <param name="filename"></param>
        public InstancePlacementSerialiserConfig(String filename)
        {
            XDocument xmlDoc = XDocument.Load(filename);
            InstancePlacementSerialiserOptions options = new InstancePlacementSerialiserOptions(xmlDoc.Root.Element("Options"));
            IEnumerable<InstancePlacementSerialiserTask> tasks =
                xmlDoc.XPathSelectElements("/InstancePlacementSerialiser/Tasks/Task").
                Select(xmlTaskElem => new InstancePlacementSerialiserTask(xmlTaskElem));

            this.Options = options;
            this.Tasks = tasks.ToArray();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Serialise the configuration data to an XML file.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
        public bool Serialise(IUniversalLog log, String filename)
        {
            bool result = true;
            try
            {
                XDocument xmlDoc = new XDocument(
                    new XDeclaration("1.0", "UTF-8", "yes"),
                    new XElement("InstancePlacementSerialiser",
                        this.Options.Serialise(),
                        new XElement("Tasks",
                            this.Tasks.Select(t => t.Serialise())
                        ) // Tasks
                    ) // InstancePlacementSerialiser
                ); // xmlDoc
                if (!SIO.Directory.Exists(SIO.Path.GetDirectoryName(filename)))
                    SIO.Directory.CreateDirectory(SIO.Path.GetDirectoryName(filename));

                xmlDoc.Save(filename);
            }
            catch (Exception ex)
            {
                log.Exception(ex, "Error serialising Instance Placement Configuration data.");
                result = false;
            }
            return (result);
        }
        #endregion // Controller Methods
    }

    public class InstancePlacementTextureSerialiserConfig
    {
        #region Properties
        /// <summary>
        /// Instance Placement Serialiser tasks.
        /// </summary>
        public IEnumerable<InstancePlacementTextureSerialiserTask> Tasks
        {
            get;
            private set;
        }
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public InstancePlacementTextureSerialiserConfig(IEnumerable<InstancePlacementTextureSerialiserTask> tasks)
        {
            this.Tasks = tasks.ToArray();
        }

        /// <summary>
        /// Constructor; loading configuration data from XML document.
        /// </summary>
        /// <param name="filename"></param>
        public InstancePlacementTextureSerialiserConfig(String filename)
        {
            XDocument xmlDoc = XDocument.Load(filename);
            IEnumerable<InstancePlacementTextureSerialiserTask> tasks =
                xmlDoc.XPathSelectElements("/InstancePlacementTextureSerialiser/Tasks/Task").
                Select(xmlTaskElem => new InstancePlacementTextureSerialiserTask(xmlTaskElem));

            this.Tasks = tasks.ToArray();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Serialise the configuration data to an XML file.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
        public bool Serialise(IUniversalLog log, String filename)
        {
            bool result = true;
            try
            {
                XDocument xmlDoc = new XDocument(
                    new XDeclaration("1.0", "UTF-8", "yes"),
                    new XElement("InstancePlacementTextureSerialiser",
                        new XElement("Tasks",
                            this.Tasks.Select(t => t.Serialise())
                        ) // Tasks
                    ) // InstancePlacementSerialiser
                ); // xmlDoc
                if (!SIO.Directory.Exists(SIO.Path.GetDirectoryName(filename)))
                    SIO.Directory.CreateDirectory(SIO.Path.GetDirectoryName(filename));

                xmlDoc.Save(filename);
            }
            catch (Exception ex)
            {
                log.Exception(ex, "Error serialising Instance Placement Texture Configuration data.");
                result = false;
            }
            return (result);
        }
        #endregion // Controller Methods
    }

} // // RSG.Pipeline.Services.Metadata namespace
