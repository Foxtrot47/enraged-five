﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Xml.Linq;

namespace RSG.Pipeline.Services.Metadata
{
    /// <summary>
    /// Map Metadata Serialiser base abstract dependency.
    /// </summary>
    public abstract class MapMetadataSerialiserDependency
    {
        /// <summary>
        /// Serialise dependency to an XElement.
        /// </summary>
        /// <returns></returns>
        internal abstract XElement Serialise();

        /// <summary>
        /// Factory method to create dependency objects.
        /// </summary>
        /// <param name="xmlElem"></param>
        /// <returns></returns>
        public static MapMetadataSerialiserDependency Create(XElement xmlElem)
        {
            if (String.Equals(xmlElem.Name.LocalName,
                MapMetadataSerialiserSceneXmlDependency.DEPENDENCY_SCENEXML))
            {
                return (new MapMetadataSerialiserSceneXmlDependency(xmlElem));
            }
            else if (String.Equals(xmlElem.Name.LocalName, 
                MapMetadataSerialiserCoreGameMetadataZipDependency.DEPENDENCY_COREGAMEMETADATAZIP))
            {
                return (new MapMetadataSerialiserCoreGameMetadataZipDependency(xmlElem));
            }
            else if (String.Equals(xmlElem.Name.LocalName,
                MapMetadataSerialiserManifestDependency.DEPENDENCY_MANFIEST))
            {
                return (new MapMetadataSerialiserManifestDependency(xmlElem));
            }
            return (null);
        }
    }

    /// <summary>
    /// Map Metadata Serialiser SceneXml dependency object.
    /// </summary>
    public class MapMetadataSerialiserSceneXmlDependency :
        MapMetadataSerialiserDependency
    {
        #region Constants
        internal static readonly String DEPENDENCY_SCENEXML = "DependencySceneXml";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// SceneXml absolute filename.
        /// </summary>
        public String SceneFilename
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="sceneFilename"></param>
        public MapMetadataSerialiserSceneXmlDependency(String sceneFilename)
        {
            this.SceneFilename = sceneFilename;
        }

        /// <summary>
        /// Constructor; from XML element.
        /// </summary>
        /// <param name="xmlDependencyElem"></param>
        public MapMetadataSerialiserSceneXmlDependency(XElement xmlDependencyElem)
        {
            Debug.Assert(String.Equals(xmlDependencyElem.Name.LocalName, DEPENDENCY_SCENEXML),
                String.Format("Invalid '{0}' element.", DEPENDENCY_SCENEXML));
            SceneFilename = xmlDependencyElem.Value;
        }
        #endregion // Constructor(s)

        #region Internal Methods
        /// <summary>
        /// Serialise dependency to an XElement.
        /// </summary>
        /// <returns></returns>
        internal override XElement Serialise()
        {
            return (new XElement(DEPENDENCY_SCENEXML, this.SceneFilename));
        }
        #endregion // Internal Methods
    }

    /// <summary>
    /// Map Metadata Serialiser Core-Game Metadata Zip dependency object.
    /// </summary>
    public class MapMetadataSerialiserCoreGameMetadataZipDependency :
        MapMetadataSerialiserDependency
    {
        #region Constants
        internal static readonly String DEPENDENCY_COREGAMEMETADATAZIP = "DependencyCoreGameMetadataZip";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Core-Game Metadata Zip absolute filename.
        /// </summary>
        public String CoreGameMetadataZipFilename
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="metadataZipFilename"></param>
        public MapMetadataSerialiserCoreGameMetadataZipDependency(String metadataZipFilename)
        {
            this.CoreGameMetadataZipFilename = metadataZipFilename;
        }

        /// <summary>
        /// Constructor; from XML element.
        /// </summary>
        /// <param name="xmlDependencyElem"></param>
        public MapMetadataSerialiserCoreGameMetadataZipDependency(XElement xmlDependencyElem)
        {
            Debug.Assert(String.Equals(xmlDependencyElem.Name.LocalName, DEPENDENCY_COREGAMEMETADATAZIP),
                String.Format("Invalid '{0}' element.", DEPENDENCY_COREGAMEMETADATAZIP));
            this.CoreGameMetadataZipFilename = xmlDependencyElem.Value;
        }
        #endregion // Constructor(s)

        #region Internal Methods
        /// <summary>
        /// Serialise dependency to an XElement.
        /// </summary>
        /// <returns></returns>
        internal override XElement Serialise()
        {
            return (new XElement(DEPENDENCY_COREGAMEMETADATAZIP, this.CoreGameMetadataZipFilename));
        }
        #endregion // Internal Methods
    }

    /// <summary>
    /// Map Metadata Serialiser Manifest Dependency object.
    /// </summary>
    public class MapMetadataSerialiserManifestDependency :
        MapMetadataSerialiserDependency
    {
        #region Constants
        internal static readonly String DEPENDENCY_MANFIEST = "DependencyManifest";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// IMAP absolute filename.
        /// </summary>
        public String IMAPPath
        {
            get;
            private set;
        }

        /// <summary>
        /// IMAP absolute filename.
        /// </summary>
        public String InputWildcard
        {
            get;
            private set;
        }

        /// <summary>
        /// ITYP filename dependencies.
        /// </summary>
        public String[] ITYPDependencies
        {
            get;
            private set;
        }

        /// <summary>
        /// Is Interior
        /// </summary>
        public bool IsInterior
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor for imap with no known ityp dependencies.
        /// </summary>
        /// <param name="sceneFilename"></param>
        public MapMetadataSerialiserManifestDependency(String imapFilename, bool isInterior)
        {
            this.IMAPPath = imapFilename;
            this.InputWildcard = null;
            this.ITYPDependencies = new string[0];
            this.IsInterior = isInterior;
        }

        /// <summary>
        /// Constructor for imap with no known ityp dependencies.
        /// </summary>
        /// <param name="sceneFilename"></param>
        public MapMetadataSerialiserManifestDependency(String directory, String wildcard, bool isInterior)
        {
            this.IMAPPath = directory;
            this.InputWildcard = wildcard;
            this.ITYPDependencies = new string[0];
            this.IsInterior = isInterior;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="sceneFilename"></param>
        public MapMetadataSerialiserManifestDependency(String imapFilename, String[] itypDependencies, bool isInterior)
        {
            this.IMAPPath = imapFilename;
            this.InputWildcard = null;
            this.ITYPDependencies = itypDependencies;
            this.IsInterior = isInterior;
        }

        /// <summary>
        /// Constructor; from XML element.
        /// </summary>
        /// <param name="xmlDependencyElem"></param>
        public MapMetadataSerialiserManifestDependency(XElement xmlDependencyElem)
        {
            Debug.Assert(String.Equals(xmlDependencyElem.Name.LocalName, DEPENDENCY_MANFIEST),
                String.Format("Invalid '{0}' element.", DEPENDENCY_MANFIEST));

            this.IMAPPath = xmlDependencyElem.Attribute("IMAPPath").Value;
            this.InputWildcard = xmlDependencyElem.Attribute("Wildcard").Value;
            this.IsInterior = Convert.ToBoolean(xmlDependencyElem.Attribute("IsInterior").Value);
            this.ITYPDependencies = xmlDependencyElem.Elements("ITYPDependency").Select(dependencyElement => dependencyElement.Attribute("name").Value).ToArray();
        }
        #endregion // Constructor(s)

        #region Internal Methods
        /// <summary>
        /// Serialise input to an XElement.
        /// </summary>
        /// <returns></returns>
        internal override XElement Serialise()
        {
            XElement xmlInputElem = new XElement(DEPENDENCY_MANFIEST,
                new XAttribute("IMAPPath", this.IMAPPath),
                new XAttribute("Wildcard", this.InputWildcard == null ? "" : this.InputWildcard),
                new XAttribute("IsInterior", this.IsInterior),
                this.ITYPDependencies.Select(itypDependency => { return new XElement("ITYPDependency", new XAttribute("name", itypDependency)); })
            ); // Input
            return (xmlInputElem);
        }
        #endregion // Internal Methods
    }

} // RSG.Pipeline.Services.Metadata namespace
