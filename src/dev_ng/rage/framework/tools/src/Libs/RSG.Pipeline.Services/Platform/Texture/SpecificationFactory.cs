﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Platform;

namespace RSG.Pipeline.Services.Platform.Texture
{

    /// <summary>
    /// Texture specification factory methods; for creating and manipulating
    /// TCS/TCL data.
    /// </summary>
    public static class SpecificationFactory
    {
        #region Static Member Data
        /// <summary>
        /// Cached dictionary of raw specifications.  These specifications are
        /// not recursively processed up the chain.
        /// </summary>
        private static IDictionary<RSG.Platform.Platform, IDictionary<String, CachedSpecification>>
            ms_CachedSpecifications;
        #endregion // Static Member Data

        #region Constructor(s)
        /// <summary>
        /// Static constructor; initialises our cached specification data.
        /// </summary>
        static SpecificationFactory()
        {
            ms_CachedSpecifications = new Dictionary<RSG.Platform.Platform, IDictionary<String, CachedSpecification>>();
        }
        #endregion // Constructor(s)

        #region Static Controller Methods
        /// <summary>
        /// Clear the Specifications cache; generally shouldn't be required
        /// as the specification file modified timestamps are used in caching.
        /// </summary>
        public static void ClearCache()
        {
            ms_CachedSpecifications.Clear();
        }

        /// <summary>
        /// Load an existing texture pipeline specification file.  The loaded
        /// specifications are cached.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="specificationFilename"></param>
        /// <returns></returns>
        public static ISpecification Load(ITarget target, String specificationFilename)
        {
            IDictionary<String, CachedSpecification> cachedSpecifications =
                GetTargetCache(target);

            String normalisedFilename = specificationFilename.ToLower();
            ISpecification specification = null;
            DateTime templateModTime = DateTime.MinValue;
            if (File.Exists(specificationFilename))
                templateModTime = File.GetLastWriteTimeUtc(specificationFilename);

            if (cachedSpecifications.ContainsKey(normalisedFilename))
            {
                // Specification is cached; determine if the on-disk file is
                // modified from the cached variant.
                CachedSpecification cs = cachedSpecifications[normalisedFilename];
                
                // Use the cached specification.
                if (templateModTime.IsLaterThan(cs.ModifiedTime))
                {
#if DEBUG
                    Specification.Log.Warning("Flushing texture pipeline specification: {0}.",
                        specificationFilename);
#endif
                    cachedSpecifications.Remove(normalisedFilename);
                }
                else
                {
                    specification = cs.Specification;
                }
            }

            // Either we didn't have a cached specification or we need to update
            // the cached specification.
            if (null == specification)
            {
#if DEBUG
                Specification.Log.Debug("Loading texture pipeline specification: {0}.", 
                    specificationFilename);
#endif
                specification = new Specification(target, specificationFilename);
                CachedSpecification cs = new CachedSpecification(specification, templateModTime);
                cachedSpecifications.Add(normalisedFilename, cs);
            }
            return (specification);
        }

        /// <summary>
        /// Load a texture pipeline specification from a stream; the loaded
        /// Specification isn't cached but any referenced parent-templates are.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static ISpecification Load(ITarget target, System.IO.Stream stream)
        {
            ISpecification specification = new Specification(target, stream);
            return (specification);
        }

        /// <summary>
        /// Create a new TCS specification file.
        /// </summary>
        /// <param name="specificationFilename"></param>
        /// <param name="templateFilename"></param>
        /// <param name="sourceTextureFilenames"></param>
        /// <param name="resourceTextureFilename"></param>
        /// <param name="skipProcessing"></param>
        public static void Create(String specificationFilename, String templateFilename,
            IEnumerable<String> sourceTextureFilenames, String resourceTextureFilename,
            bool skipProcessing)
        {
            XDocument xmlDoc = new XDocument(
                new XDeclaration("1.0", "UTF-8", "yes"),
                new XElement("rage__CTextureConversionSpecification",
                    new XElement("skipProcessing",
                        new XAttribute("value", skipProcessing)
                    ), // skipProcessing
                    new XElement("parent", templateFilename),
                    new XElement("resourceTextures",
                        new XElement("Item", new XAttribute("type", "rage__CTextureConversionResourceTexture"),
                            new XElement("pathname", resourceTextureFilename),
                            new XElement("usage", "usage_Default"),
                            new XElement("sourceTextures", 
                                sourceTextureFilenames.Select(sourceTextureFilename => 
                                    new XElement("Item",
                                        new XAttribute("type", "rage__CTextureConversionSourceTexture"),
                                        new XElement("pathname", sourceTextureFilename))
                                )
                            ) // sourceTextures
                        ) //Item
                    ) // resourceTextures
                ) // rage__CTextureConversionSpecification
            ); // xmlDoc
            
            xmlDoc.Save(specificationFilename);
        }

        /// <summary>
        /// Update an existing TCS specification file with the resourceTextures chunk.
        /// </summary>
        /// <param name="specificationFilename"></param>
        /// <param name="sourceTextureFilenames"></param>
        /// <param name="resourceTextureFilename"></param>
        /// This includes some data migration; notably:
        ///  1. Remove root sourceTexture element; migrate to new element if no new source specified.
        public static void UpdateResourceTextures(String specificationFilename, 
            IEnumerable<String> sourceTextureFilenames, String resourceTextureFilename)
        {
            XDocument xmlDoc = XDocument.Load(specificationFilename);

            try
            {
                FileInfo fi = new FileInfo(specificationFilename);

                if (fi.IsReadOnly)
                {
                    Log.Log__Message("File wasn't writable, setting writable now: {0}.", specificationFilename);
                    fi.IsReadOnly = false;
                }
            }
            catch (Exception ex)
            {
                Log.Log__Exception(ex, "Exception while trying to set file writable ({0}).", specificationFilename);
                return;
            }

            // Clean out the old "sourceTexture" root node.
            String oldSourceTexture = null;
            XElement xmlSourceTextureElem = xmlDoc.Root.Element("sourceTexture");
            if (null != xmlSourceTextureElem)
            {
                oldSourceTexture = xmlSourceTextureElem.Value;
                xmlSourceTextureElem.Remove();
            }
                
            // Remove the existing resourceTextures element.
            XElement xmlResourceTexturesElem = xmlDoc.Root.Element("resourceTextures");
            if (null != xmlResourceTexturesElem)
                xmlResourceTexturesElem.Remove();

            List<String> allSourceTextures = new List<String>(sourceTextureFilenames);
            if (!String.IsNullOrEmpty(oldSourceTexture) && (0 == sourceTextureFilenames.Count()))
                allSourceTextures.Add(oldSourceTexture);
            
            // Create the resourceTextures chunk as it doesn't currently exist.
            xmlResourceTexturesElem =
                new XElement("resourceTextures",
                    new XElement("Item", new XAttribute("type", "rage__CTextureConversionResourceTexture"),
                        new XElement("pathname", resourceTextureFilename),
                        new XElement("usage", "usage_Default"),
                          new XElement("sourceTextures",
                                allSourceTextures.Distinct().Select(sourceTextureFilename => 
                                    new XElement("Item", 
                                        new XAttribute("type", "rage__CTextureConversionSourceTexture"),
                                        new XElement("pathname", sourceTextureFilename)
                                    ) // Item
                                )
                            ) // sourceTextures
                        ) // Item
                    ); // resourceTextures
            xmlDoc.Root.Add(xmlResourceTexturesElem);
            xmlDoc.Save(specificationFilename);
        }

        /// <summary>
        /// Create resource and source textures link data in specification.
        /// </summary>
        /// <param name="specificationFilename"></param>
        /// <param name="sourceTextureFilenames"></param>
        /// <param name="resourceTextureFilename"></param>
        [Obsolete("Use UpdateResourceTextures as its tested.", true)]
        public static void CreateTextureLinkData(String specificationFilename,
            IEnumerable<String> sourceTextureFilenames, String resourceTextureFilename)
        {
            XDocument document = XDocument.Load(specificationFilename);

            // Remove existing elements that we will replace
            XElement sourceTextureElement = document.Root.Element("sourceTexture");
            if (sourceTextureElement != null)
                sourceTextureElement.Remove();

            XElement resourceTexturesElement = document.Root.Element("resourceTextures");
            if (resourceTexturesElement != null)
                resourceTexturesElement.Remove();

            // Add the new data
            var newSourceTextureElementsQuery = from sourceTexturePathname in sourceTextureFilenames
                                                select new XElement("Item", new XAttribute("type", "rage__CTextureConversionSourceTexture"),
                                                           new XElement("pathname", sourceTexturePathname));

            XElement newResourceTexturesElement =
                new XElement("resourceTextures",
                    new XElement("Item", new XAttribute("type", "rage__CTextureConversionResourceTexture"),
                        new XElement("pathname", resourceTextureFilename),
                        new XElement("usage", "usage_Default"),
                        new XElement("sourceTextures", newSourceTextureElementsQuery.ToArray())));
            document.Root.Add(newResourceTexturesElement);

            document.Save(specificationFilename);
        }

        /// <summary>
        /// Create a new TCL specification file.
        /// </summary>
        /// <param name="specificationFilename"></param>
        /// <param name="templateFilename"></param>
        public static void CreateLink(String specificationFilename, String templateFilename)
        {
            XDocument xmlDoc = new XDocument(
                new XDeclaration("1.0", "UTF-8", "yes"),
                new XElement("rage__CTextureConversionSpecification",
                    new XElement("parent", templateFilename)
                ) // rage__CTextureConversionSpecification
            ); // xmlDoc

            xmlDoc.Save(specificationFilename);
        }

        /// <summary>
        /// Return the specification 'parent'.
        /// </summary>
        /// <param name="specificationFilename"></param>
        /// <returns></returns>
        public static String ReadParent(String specificationFilename)
        {
            XDocument xmlDoc = XDocument.Load(specificationFilename);
            XElement xmlParentElem = xmlDoc.Root.Element("parent");
            return (xmlParentElem.Value);
        }

        /// <summary>
        /// Return whether there are resource textures specified.
        /// </summary>
        /// <param name="specificationFilename"></param>
        /// <returns></returns>
        public static bool HasResourceTexturesData(String specificationFilename)
        {
            try
            {
                XDocument document = XDocument.Load(specificationFilename);
                if (document.Root.Name == "rage__CTextureConversionSpecification")
                {
                    XElement resourceTexturesElement = document.Root.Element("resourceTextures");
                    return (resourceTexturesElement != null);
                }

                return (false);
            }
            catch (System.IO.FileNotFoundException)
            {
                return (false);
            }
            catch (System.Xml.XmlException)
            {
                return (false);
            }
        }
        #endregion // Static Controller Methods

        #region Private Methods
        /// <summary>
        /// Return target specification cache.
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        private static IDictionary<String, CachedSpecification> GetTargetCache(ITarget target)
        {
            IDictionary<String, CachedSpecification> cachedSpecifications = null;
            if (ms_CachedSpecifications.ContainsKey(target.Platform))
            {
                cachedSpecifications = ms_CachedSpecifications[target.Platform];
            }
            else
            {
                cachedSpecifications = new Dictionary<String, CachedSpecification>();
                ms_CachedSpecifications.Add(target.Platform, cachedSpecifications);
            }
            return (cachedSpecifications);
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Services.Platform.Texture
