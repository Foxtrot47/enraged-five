﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using RSG.Base.Configuration;

namespace RSG.Pipeline.Services.Materials
{

    /// <summary>
    /// Shader preset (SPS) abstraction; use the Create method to initialise 
    /// a collection.
    /// </summary>
    public class ShaderPreset
    {
        #region Constants
        /// <summary>
        /// Default draw bucket.
        /// </summary>
        private static int DRAWBUCKET_DEFAULT = 0;

        /// <summary>
        /// Alpha preset draw bucket.
        /// </summary>
        private static int DRAWBUCKET_ALPHA = 1;

        /// <summary>
        /// Decal preset draw bucket.
        /// </summary>
        private static int DRAWBUCKET_DECAL = 2;

        /// <summary>
        /// Cutout preset draw bucket.
        /// </summary>
        private static int DRAWBUCKET_CUTOUT = 3;

        /// <summary>
        /// Shader name regex.
        /// </summary>
        private static readonly Regex REGEX_SHADER_NAME = new Regex("shader (?<shader>[A-Z0-9_]+)",
            RegexOptions.Compiled | RegexOptions.IgnoreCase);

        /// <summary>
        /// Draw bucket regex.
        /// </summary>
        private static readonly Regex REGEX_DRAW_BUCKET_START = new Regex("__rage_drawbucket {",
            RegexOptions.Compiled | RegexOptions.IgnoreCase);
        private static readonly Regex REGEX_DRAW_BUCKET_STOP = new Regex("}",
            RegexOptions.Compiled | RegexOptions.IgnoreCase);
        private static readonly Regex REGEX_DRAW_BUCKET = new Regex("[ \t]+int[ \t]+(?<bucket>[0-9]+)",
            RegexOptions.Compiled | RegexOptions.IgnoreCase);

        /// <summary>
        /// Shader preset file wildcard.
        /// </summary>
        private static String SHADER_PRESET_WILDCARD = "*.sps";
        #endregion // Constants

        #region Enumerations
        /// <summary>
        /// Parser state.
        /// </summary>
        private enum ParserState
        {
            Init,
            DrawBucket,
        }
        #endregion // Enumerations

        #region Properties
        /// <summary>
        /// Shader preset name.
        /// </summary>
        public String Name
        {
            get;
            private set;
        }

        /// <summary>
        /// Shader preset filename (SPS).
        /// </summary>
        public String Filename
        {
            get;
            private set;
        }

        /// <summary>
        /// Shader name.
        /// </summary>
        public String ShaderName
        {
            get;
            private set;
        }

        /// <summary>
        /// Shader preset draw bucket.
        /// </summary>
        public int DrawBucket
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="filename"></param>
        public ShaderPreset(String filename)
        {
            this.Name = Path.GetFileName(filename);
            this.Filename = filename;
            Parse();
        }
        #endregion // Constructor(s)

        #region Static Controller Methods
        /// <summary>
        /// Create dictionary of available shader presets.
        /// </summary>
        /// <param name="branch"></param>
        /// <returns></returns>
        public static IDictionary<String, ShaderPreset> Create(IBranch branch)
        {
            IDictionary<String, ShaderPreset> presets = new Dictionary<String, ShaderPreset>();
            String presetDirectory = Path.Combine(branch.Shaders, "db");
            String[] shaderPresets = Directory.GetFiles(presetDirectory, 
                SHADER_PRESET_WILDCARD, SearchOption.TopDirectoryOnly);

            foreach (String shaderPreset in shaderPresets)
            {
                ShaderPreset preset = new ShaderPreset(shaderPreset);
                presets.Add(preset.Name, new ShaderPreset(shaderPreset));
            }

            return (presets);
        }
        #endregion // Static Controller Methods

        #region Controller Methods
        /// <summary>
        /// Determine whether this is an alpha preset.
        /// </summary>
        /// <returns></returns>
        public bool IsAlphaPreset()
        {
            return (DRAWBUCKET_ALPHA == this.DrawBucket);
        }

        /// <summary>
        /// Determine whether this is a cutout preset.
        /// </summary>
        /// <returns></returns>
        public bool IsCutoutPreset()
        {
            return (DRAWBUCKET_CUTOUT == this.DrawBucket);
        }

        /// <summary>
        /// Determine whether this is a decal preset.
        /// </summary>
        /// <returns></returns>
        public bool IsDecalPreset()
        {
            return (DRAWBUCKET_DECAL == this.DrawBucket);
        }

        /// <summary>
        /// Determine whether on export we merge the diffuse and alpha maps
        /// for materials of this type.
        /// </summary>
        /// <returns></returns>
        public bool IsDiffuseAlphaTexturesMerged()
        {
            return (this.DrawBucket > DRAWBUCKET_DEFAULT);
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Parse the SPS file; we only parse this partially for the info required.
        /// </summary>
        private void Parse()
        {
            ParserState state = ParserState.Init;
            FileInfo fi = new FileInfo(this.Filename);
            using (StreamReader reader = fi.OpenText())
            {
                String line;
                
                while (null != (line = reader.ReadLine()))
                {
                    if (ParserState.Init == state)
                    {
                        Match m = REGEX_SHADER_NAME.Match(line);
                        if (m.Success)
                        {
                            this.ShaderName = m.Groups["shader"].Value;
                        }
                        m = REGEX_DRAW_BUCKET_START.Match(line);
                        {
                            state = ParserState.DrawBucket;
                        }
                    }
                    else if (ParserState.DrawBucket == state)
                    {
                        Match m = REGEX_DRAW_BUCKET.Match(line);
                        if (m.Success)
                        {
                            this.DrawBucket = int.Parse(m.Groups["bucket"].Value);
                        }
                        m = REGEX_DRAW_BUCKET_STOP.Match(line);
                        if (m.Success)
                        {
                            state = ParserState.Init;
                        }
                    }
                }
            }
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Services.Materials namespace
