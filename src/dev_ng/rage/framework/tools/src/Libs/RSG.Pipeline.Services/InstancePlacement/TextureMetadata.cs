﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

using RSG.Base.Math;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Services.Metadata;
using RSG.Base.Configuration;

namespace RSG.Pipeline.Services.InstancePlacement
{
    #region Helper Structures
    /// <summary>
    /// Helper class for serializing a BoundVector
    /// TODO: Replace this with Vector2f?
    /// </summary>
    public class BoundsVector
    {
        [XmlAttribute]
        public float X;

        [XmlAttribute]
        public float Y;

        public BoundsVector() { }
    }

    /// <summary>
    /// Helper class for serialising and deserialising colors in hex code.
    /// </summary>
    public class XmlColor
    {
        private Color color_ = Color.Black;

        public XmlColor() { }
        public XmlColor(Color c) { color_ = c; }

        public Color ToColor()
        {
            return color_;
        }

        public void FromColor(Color c)
        {
            color_ = c;
        }

        public static implicit operator Color(XmlColor x)
        {
            return x.ToColor();
        }

        public static implicit operator XmlColor(Color c)
        {
            return new XmlColor(c);
        }

        [XmlAttribute]
        public string Value
        {
            get 
            {
                return ColorTranslator.ToHtml(color_);
            }
            set
            {
                try
                {
                    color_ = ColorTranslator.FromHtml(value);
                }
                catch (Exception)
                {
                    color_ = Color.Black;
                }
            }
        }
    }

    /// <summary>
    /// Structure holding data on every texture such as its bounds, location and resolution.
    /// </summary>
    public class TextureBounds
    {
        [XmlAttribute]
        public float TextureWidth;

        [XmlAttribute]
        public float TextureHeight;

        [XmlAttribute]
        public float PixelResolution;

        public BoundsVector MinBounds;  //The bounds in world-coordinates for the texture.
        public BoundsVector MaxBounds;

        [XmlIgnore]
        public int MaxRowIndex;

        [XmlIgnore]
        public int MaxColIndex;

        public TextureBounds()
        {
            MinBounds = new BoundsVector();
            MaxBounds = new BoundsVector();
            TextureWidth = 512.0f;
            TextureHeight = 512.0f;
            PixelResolution = 1.0f;
            MaxRowIndex = -1;
            MaxColIndex = -1;
        }
    }

    /// <summary>
    /// Structure to hold a group of InstanceDefinitions.
    /// </summary>
    public class InstanceGroup
    {
        [XmlAttribute]
        public string Name;

        [XmlAttribute]
        public String GroupColor;

        [XmlIgnore]
        public Color Color;

        [XmlArray("Definitions")]
        [XmlArrayItem("Definition")]
        public List<InstanceDefinition> Group;

        public InstanceGroup()
        {
            Name = "";
            Color = Color.Black;
            GroupColor = "";
            Group = new List<InstanceDefinition>();
        }

        public void SetXmlColor()
        {
            XmlColor xmlColor = new XmlColor();
            xmlColor.Value = GroupColor;
            Color = xmlColor.ToColor();
        }
    }

    /// <summary>
    /// A definition of an instance, equating to a particular color within the texture.
    /// </summary>
    public class InstanceDefinition
    {
        [XmlAttribute]
        public string Name;

        [XmlAttribute]
        public string HighDetailName;

        [XmlAttribute]
        public float MinScale;

        [XmlAttribute]
        public float MaxScale;

        [XmlAttribute]
        public float RandomScale;

        [XmlAttribute]
        public float Radius;

        [XmlAttribute]
        public float Density;

        [XmlAttribute]
        public float LODDistance;

        [XmlAttribute]
        public float HighDetailLODDistance;

        [XmlAttribute]
        public float LODFadeStartDistance;

        [XmlAttribute]
        public float LODInstFadeRange;

        [XmlAttribute]
        public float MaxSlope;

        [XmlAttribute]
        public float HeightCompensation;

        [XmlAttribute]
        public float OrientToTerrain;

        [XmlAttribute]
        public int Brightness;

        [XmlAttribute]
        public bool UseLocalTint;

        [XmlAttribute]
        public bool UseLocalDataTexture;

        [XmlAttribute]
        public string ExclusionType;

        [XmlAttribute]
        public float IntensityVariation;

        [XmlElement(typeof(XmlColor))]
        public Color Color;
        
        /// <summary>
        /// Constructor.
        /// </summary>
        public InstanceDefinition()
        {
            Name = null;
            HighDetailName = null;
            Color = System.Drawing.Color.White;
            MinScale = 1.0f;
            MaxScale = 1.0f;
            RandomScale = 0.0f;
            Radius = 1.0f;
            Density = 1.0f;
            LODDistance = -1;
            HighDetailLODDistance = -1;
            LODFadeStartDistance = 8191.0f;
            LODInstFadeRange = 1.0f;
            MaxSlope = -1.0f;
            HeightCompensation = 0.0f;
            OrientToTerrain = 0.0f;
            Brightness = 0;
            UseLocalTint = true;
            UseLocalDataTexture = true;
            ExclusionType = "";
            IntensityVariation = 0.0f;
        }

        /// <summary>
        /// Constructor for creating temporary definitions.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="highDetailName"></param>
        public InstanceDefinition(String name, String highDetailName)
        {
            Name = name;
            HighDetailName = highDetailName;
            Color = System.Drawing.Color.White;
            MinScale = 1.0f;
            MaxScale = 1.0f;
            RandomScale = 0.0f;
            Radius = 1.0f;
            Density = 1.0f;
            LODDistance = -1;
            HighDetailLODDistance = -1;
            LODFadeStartDistance = 8191.0f;
            LODInstFadeRange = 1.0f;
            MaxSlope = -1.0f;
            HeightCompensation = 0.0f;
            OrientToTerrain = 0.0f;
            Brightness = 0;
            UseLocalTint = true;
            UseLocalDataTexture = true;
            ExclusionType = "";
            IntensityVariation = 0.0f;
        }

        public float CalculateDensity(float value)
        {
            float density = value;

            // We are probably coming in as a colour value so divide by 255;
            if (value >= 0.0f && value > 1.0f)
                density = (value / 255.0f);

            // Set the minimum density to 1.
            float defDensity = Math.Max(1, Density);

            return (float)Math.Round((1 * (1 - density) + defDensity * density));
        }

        public float CalculateScale(float value)
        {
            float scale = value;
            
            // We are probably coming in as a colour value so divide by 255;
            if (value >= 0.0f && value > 1.0f)
                scale = (value / 255.0f);

            return ((MinScale * (1 - scale) + MaxScale * scale));
        }

        public Color CalculateTintIntensityVariation(Random randomizer, Color baseColour)
        {
            if (IntensityVariation == 0.0f)
                return baseColour;

            // Make sure we're between 0-1.
            float tintIntensityVariation = Math.Min(1.0f, Math.Max(0.0f, IntensityVariation));
            int percentage = (int)Math.Round(255 * tintIntensityVariation);

            int randomPercentage = (int)(randomizer.NextDouble() * percentage);

            // Choose either positive or negative random tint
            if (randomizer.NextDouble() > 0.5)
                randomPercentage *= -1;

            int R = Math.Min(255, Math.Max(0, baseColour.R + randomPercentage));
            int G = Math.Min(255, Math.Max(0, baseColour.G + randomPercentage));
            int B = Math.Min(255, Math.Max(0, baseColour.B + randomPercentage));

            return Color.FromArgb(255, R, G, B);
        }
    }

    /// <summary>
    /// A simple container object that will cache a particular entry in an accessible format for MaxScript.
    /// </summary>
    public class InstanceObject
    {
        public InstanceDefinition Definition;
        public Vector3f Position;
        public Quaternionf Rotation;
        public Plane Plane;
        public Vector4i Color;
        public Vector4i Tint;
        public float Scale;
        public float Density;
        public byte AO;

        public InstanceObject(InstanceDefinition definition, Vector3f position, Quaternionf rotation, 
            Plane plane, Vector4i color, Color tint, float scale, float density, byte ao)
        {
            Definition = definition;
            Position = position;
            Rotation = rotation;
            Plane = plane;
            Color = color;
            Tint = new Vector4i(tint.R, tint.G, tint.B, tint.A);
            Scale = scale;
            Density = density;
            AO = ao;
        }
    }


    /// <summary>
    /// Structure defining a category of instances to drop.  Categories are arbitrary but 
    /// can be used to specify object sets defined within a set of textures (e.g. a set of trees
    /// and a set of grass will be separate texture groups).
    /// </summary>
    public class InstanceCategory
    {
        public InstanceCategory()
        {
            Name = null;
            StreamingBoundSizeBuffer = 0.0f;
            OptimalBatchSize = 512;
            MaximumBoundLength = -1;
            Randomize = false;
            RandomSeed = -1;
            MaxSlope = -1.0f;
            CategoryInstanceTexture = null;
            Dependencies = new List<String>();
            Shaders = new List<String>();
            Definitions = new List<InstanceDefinition>();
            Groups = new List<InstanceGroup>();
        }

        [XmlAttribute]
        public string Name;

        [XmlAttribute]
        public float StreamingBoundSizeBuffer;
        
        [XmlAttribute]
        public int OptimalBatchSize;

        [XmlAttribute]
        public int MaximumBoundLength;

        [XmlAttribute]
        public bool Randomize;
        
        [XmlAttribute]
        public int RandomSeed;

        [XmlAttribute]
        public float MaxSlope;

        [XmlArray("Dependencies")]
        [XmlArrayItem("Dependency")]
        public List<String> Dependencies;

        [XmlArray("Shaders")]
        [XmlArrayItem("Shader")]
        public List<String> Shaders;

        [XmlArray("Definitions")]
        [XmlArrayItem("Definition")]
        public List<InstanceDefinition> Definitions;

        [XmlArray("Groups")]
        [XmlArrayItem("Group")]
        public List<InstanceGroup> Groups;

        [XmlIgnore]
        public InstanceTexture CategoryInstanceTexture;

        /// <summary>
        /// Fixes the group's color, converting from an HTML tag to a color object.
        /// </summary>
        public void FixGroupColours()
        {
            if (Groups != null)
            {
                for (int i = 0; i < Groups.Count; i++)
                {
                    Groups[i].SetXmlColor();
                }
            }
        }

        /// <summary>
        /// Propagated the category's max slope to each definition should it not be initialized.
        /// </summary>
        public void SetMaxSlopes()
        {
            if (Groups != null)
            {
                for (int i = 0; i < Groups.Count; i++)
                {
                    foreach (InstanceDefinition definition in Groups[i].Group)
                    {
                        if (definition.MaxSlope == -1.0f)
                        {
                            definition.MaxSlope = this.MaxSlope;
                        }
                    }
                }

                foreach (InstanceDefinition definition in Definitions)
                {
                    if (definition.MaxSlope == -1.0f)
                    {
                        definition.MaxSlope = this.MaxSlope;
                    }
                }
            }
        }
    }

    /// <summary>
    /// Structure for holding the different exclusion lists.
    /// This is used so that we can figure out if we are able to place an instance on a specific collision
    /// triangle based on whether it's an excluded type.
    /// </summary>
    public class InstanceBoundsExclusionList
    {
        public InstanceBoundsExclusionList()
        {
            Name = "";
            Parent = "";
            ExclusionList = new List<string>();
        }

        [XmlAttribute]
        public String Name;

        [XmlAttribute]
        public String Parent;

        [XmlArray("Exclusions")]
        [XmlArrayItem("Exclude")]
        public List<String> ExclusionList;
    }
    #endregion Helper Structures
 
    /// <summary>
    /// General class for holding texture metadata.
    /// TODO:  Investigate turning this into a PSC-formatted and/or
    /// using native RSG C# types.
    /// </summary>
    public class TextureMetadata
    {
        #region Constants
        private static readonly String LOG_CTX = "Texture Metadata";
        #endregion Constants

        #region Properties
        [XmlAttribute("Version")]
        public int m_Version;
        private int m_CurrentVersion = 1;

        /// <summary>
        /// Changes bounds processing rounding to be more accurate.
        /// NOTE: More accurate bounds processing was needed for certain DLC but caused discrepancies with base game.
        ///       This must be disabled for the base game and only enabled for specific DLC that requires it.
        ///       Original bug: url:bugstar:6804673 - cant figure out why grass isn't dropping here
        /// </summary>
        [XmlElement("EnableAccurateBoundsRounding")]
        public bool EnableAccurateBoundsRounding;

        /// <summary>
        /// Allows for instance placement from source textures.
        /// </summary>
        [XmlElement("EnableTexturePlacement")]
        public bool EnableTexturePlacement;

        /// <summary>
        /// Enables for farthest entity shuffling; currently toggled off by default.
        /// </summary>
        [XmlElement("EnableFarthestEntityShuffling")]
        public bool EnableFarthestEntityShuffling;

        /// <summary>
        /// Enables the high definition entity generation for prop instances.
        /// </summary>
        [XmlElement("EnableHighDefEntityGeneration")]
        public bool EnableHighDefEntityGeneration;

        [XmlElement("MapDataInfoFilename")]
        public String MapDataInfoFilename;

        [XmlElement("GlobalTintTexture")]
        public String GlobalTintTexture;

        [XmlElement("GlobalDataTexture")]
        public String GlobalDataTexture;

        [XmlElement("EnableEntityPlacement")]
        public bool EnableEntityPlacement;

        [XmlIgnore]
        public float MaxPositionOffset;

        public TextureBounds Bounds;

        [XmlArray("Categories")]
        [XmlArrayItem("Category")]
        public List<InstanceCategory> Categories;

        [XmlArray("BoundsMaterialExclusion")]
        [XmlArrayItem("ExclusionList")]
        public List<InstanceBoundsExclusionList> ExclusionList;

        [XmlIgnore]
        private static IUniversalLog Log;

        [XmlIgnore]
        public InstanceCategory SelectedCategory
        {
            get;
            private set;
        }

        [XmlIgnore]
        public Dictionary<String, InstanceBoundsExclusionList> ExclusionDictionary;
        #endregion Properties

        #region Constructors
        public TextureMetadata() 
        {
            EnableTexturePlacement = true;
            EnableAccurateBoundsRounding = false;
            EnableEntityPlacement = false;
            Bounds = new TextureBounds();
            Categories = new List<InstanceCategory>();
            ExclusionList = new List<InstanceBoundsExclusionList>();

            GlobalTintTexture = "";
            GlobalDataTexture = "";
        }
        #endregion Constructors

        #region Get / Set Accessors
        public int GetVersion() { return m_Version; }
        public int GetCurrentVersion() { return m_CurrentVersion; }
        public float Resolution { get { return Bounds.PixelResolution; } }

        /// <summary>
        /// Sets the selected category.
        /// </summary>
        /// <param name="categoryIndex"></param>
        public bool SetSelectedCategory(InstancePlacementType categoryName)
        {
            IEnumerable<InstanceCategory> categories = Categories.Where(category => (String.Compare(category.Name, categoryName.ToString(), true) == 0));
            if (categories.Count() > 0)
            {
                SelectedCategory = categories.First();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Returns the list of categories in an array.
        /// </summary>
        /// <returns></returns>
        public string[] GetCategories()  
        {
            List<string> categories = new List<string>();
            foreach (InstanceCategory category in Categories)
            {
                categories.Add(category.Name);
            }

            return categories.ToArray();
        }
        #endregion Get/Set Accessors

        #region Loading Functions
        /// <summary>
        /// Deserializes the texture metadata file.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static TextureMetadata Load(string filename, IBranch branch, IUniversalLog log)
        {
            StreamReader reader = null;
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(TextureMetadata));
                reader = new StreamReader(filename);
                TextureMetadata data = (TextureMetadata) serializer.Deserialize(reader);
                reader.Close();

                if (data.Bounds.TextureWidth <= 0) 
                {
                    log.ErrorCtx(LOG_CTX, "Invalid bounds texture width.  Must be greater than zero!");
                    return null;
                }

                if (data.Bounds.TextureHeight <= 0)
                {
                    log.ErrorCtx(LOG_CTX, "Invalid bounds texture height.  Must be greater than zero!");
                    return null;
                }

                data.InitializeData(branch, log);
           
                return data;
            }
            catch(Exception e)
            {
                log.ErrorCtx(LOG_CTX, String.Format("Unable to deserialise texture metadata file: {0}", e.Message));
                reader.Close();
                return null;
            }
        }

        /// <summary>
        /// Initializes the exclusion dictionary and flattens the list based on parents
        /// </summary>
        private void BuildExclusionDictionary()
        {
            ExclusionDictionary = new Dictionary<String, InstanceBoundsExclusionList>();

            for (int i = 0; i < ExclusionList.Count; i++)
            {
                InstanceBoundsExclusionList list = ExclusionList[i];
                String name = list.Name.ToLower();

                // Go through and make the list lowercase before storing it so we can match it up easier later.
                for (int j = 0; j < list.ExclusionList.Count; j++)
                    list.ExclusionList[j] = list.ExclusionList[j].ToLower();

                if (!ExclusionDictionary.ContainsKey(name))
                {
                    ExclusionDictionary.Add(name, list);
                }
                else
                {
                    ExclusionDictionary[name].ExclusionList.AddRange(list.ExclusionList);
                }
            }

            // Go through a second time now that our keys are setup and collapse the parent structures into the list.
            foreach (InstanceBoundsExclusionList list in ExclusionList)
            {
                String key = list.Name.ToLower();
                String parent = list.Parent.ToLower();

                while (!String.IsNullOrEmpty(parent))
                {
                    if (ExclusionDictionary.ContainsKey(parent))
                    {
                        ExclusionDictionary[key].ExclusionList.AddRange(ExclusionDictionary[parent].ExclusionList);
                        parent = ExclusionDictionary[parent].Parent.ToLower();
                    }
                    else
                    {
                        // Bail out, the parent structure is hosed.
                        Log.WarningCtx(LOG_CTX, String.Format("Parent exclusion list does not exist: {0}", parent));
                        parent = null;
                    }
                }
            }
        }

        /// <summary>
        /// Check based on a given exclusion type and see if the material.
        /// Returns true if the placement should be rejected.
        /// </summary>
        public bool CheckForExclusion(string exclusionType, string materialName)
        {
            exclusionType = exclusionType.ToLower();

            if (ExclusionDictionary.ContainsKey(exclusionType))
            {
                List<String> exclusionTypes = ExclusionDictionary[exclusionType].ExclusionList;
                return exclusionTypes.Contains(materialName.ToLower());
            }

            return false;
        }

        /// <summary>
        /// Fixes all categories' group colors.
        /// </summary>
        public void FixGroupColours()
        {
            for (int i = 0; i < Categories.Count; i++)
            {
                Categories[i].FixGroupColours();
            }
        }

        /// <summary>
        /// Propagates
        /// </summary>
        public void SetMaxSlopes()
        {
            for (int i = 0; i < Categories.Count; i++)
            {
                Categories[i].SetMaxSlopes();
            }
        }

        /// <summary>
        /// Initializes the internal VegetationData structure after the XML serialization process.
        /// </summary>
        private void InitializeData(IBranch branch, IUniversalLog log)
        {
            float xLength = Bounds.MaxBounds.X - Bounds.MinBounds.X;
            float yLength = Bounds.MaxBounds.Y - Bounds.MinBounds.Y;

            int maxXIndex = (int)(xLength / Bounds.TextureWidth);
            int maxYIndex = (int)(yLength / Bounds.TextureHeight);
            Bounds.MaxRowIndex = maxXIndex;
            Bounds.MaxColIndex = maxYIndex;

            MaxPositionOffset = (Bounds.PixelResolution / 2.0f);

            MapDataInfoFilename = branch.Environment.Subst(MapDataInfoFilename);
            GlobalTintTexture = branch.Environment.Subst(GlobalTintTexture);
            GlobalDataTexture = branch.Environment.Subst(GlobalDataTexture);

            SelectedCategory = null;
            Log = log;

            BuildExclusionDictionary();
            FixGroupColours();
            SetMaxSlopes();
        }


        /// <summary>
        /// Loads the specified vegetation texture with the given row/column index.
        /// </summary>
        /// <param name="textureRowIndex"></param>
        /// <param name="textureColIndex"></param>
        /// <returns></returns>
        public bool LoadTextures(IEnumerable<InstancePlacementSerialiserInputTexture> inputTextures, BoundingBox3f containerExtents)
        {
            foreach (InstancePlacementSerialiserInputTexture inputTexture in inputTextures)
            {
                String texturePath = inputTexture.TextureFilename;

                if (inputTexture.Type == InstancePlacementTextureType.Global_Data)
                {
                    GlobalDataTexture = inputTexture.TextureFilename;
                    continue;
                }

                if (inputTexture.Type == InstancePlacementTextureType.Global_Tint)
                {
                    GlobalTintTexture = inputTexture.TextureFilename;
                    continue;
                }

                if (File.Exists(texturePath) == false)
                {
                    Log.ErrorCtx(LOG_CTX, String.Format("Unable to find input texture file {0}.", texturePath));
                    return false;
                }
                else
                {
                    Log.MessageCtx(LOG_CTX, String.Format("Loading texture {0}...", texturePath));
                }
            }

            if (!String.IsNullOrEmpty(GlobalTintTexture) && !File.Exists(GlobalTintTexture))
            {
                Log.ErrorCtx(LOG_CTX, String.Format("Unable to find global tint texture {0}", GlobalTintTexture));
                return false;
            }
            else if (!String.IsNullOrEmpty(GlobalTintTexture) && File.Exists(GlobalTintTexture))
            {
                Log.MessageCtx(LOG_CTX, String.Format("Loading global tint texture: {0}", GlobalTintTexture));
            }

            if (!String.IsNullOrEmpty(GlobalDataTexture) && !File.Exists(GlobalDataTexture))
            {
                Log.ErrorCtx(LOG_CTX, String.Format("Unable to find global data texture {0}", GlobalDataTexture));
                return false;
            }
            else if (!String.IsNullOrEmpty(GlobalDataTexture) && File.Exists(GlobalDataTexture))
            {
                Log.MessageCtx(LOG_CTX, String.Format("Loading global data texture: {0}", GlobalDataTexture));
            }

            String[] texturePaths = inputTextures.Select(inputTexture => inputTexture.TextureFilename).ToArray();

            SelectedCategory.CategoryInstanceTexture = new InstanceTexture(Bounds, containerExtents);
            return SelectedCategory.CategoryInstanceTexture.LoadTextures(Log, texturePaths, GlobalTintTexture, GlobalDataTexture);
        }
        #endregion Loading Functions

        #region Placement Functions

        /// <summary>
        /// Creates an instance object.
        /// </summary>
        /// <param name="definition"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <param name="plane"></param>
        /// <param name="materialColor">The color of the ground.</param>
        /// <param name="pixel"></param>
        /// <param name="secondPixel"></param>
        /// <returns></returns>
        public InstanceObject CreateInstance(InstanceDefinition definition, Vector3f position, 
            Plane plane, Vector4i materialColor, Color colorData, Color tintData, bool calculateScale = false)
        {
            Quaternionf quaternion = new Quaternionf();

            // Scale is calculated in-game using min/max values passed into the batch header but only for grass.
            // Trees need to calculate scale because the scale is used to build up the transform matrix.
            float scaleValue = (float)colorData.R;
            if (calculateScale)
                scaleValue = definition.CalculateScale((float)colorData.R);

            // Calculate density between our Min and Max ranges.
            float densityValue = definition.CalculateDensity((float)colorData.G);

            // AO data comes directly from the blue channel of the data colour.
            byte AO = colorData.B;

            return new InstanceObject(definition, position, quaternion, plane, materialColor, tintData, scaleValue, densityValue, AO);
        }

        public void GetPixelPosition(float x, float y, ref int pX, ref int pY)
        {
            InstanceTexture texture = SelectedCategory.CategoryInstanceTexture;

            pX = (int)Math.Round(x - texture.ContainerBounds.Min.X);
            pY = (int)Math.Round(y - texture.ContainerBounds.Min.Y);
        }

        /// <summary>
        /// Specifies a world-coordinate 2D point and determines the pixel in the world this point best
        /// maps to.  This algorithm rounds to the nearest integer to find the texture pixel.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public ColorReference[] GetColorFromImage(float x, float y)
        {
            int pX = 0;
            int pY = 0;
            GetPixelPosition(x, y, ref pX, ref pY);

            InstanceTexture texture = SelectedCategory.CategoryInstanceTexture;

            //NOTE:  Texture coordinates consider (0,0) as the upper-left corner of the texture.
            //In our coordinate system, we assume that (0,0) is the bottom-left corner.
            //Flip the y-coordinate then.
            return texture.GetVegetationPixel(pX, (texture.Height - pY - 1));
        }

        /// <summary>
        /// Specifies a world-coordinate 2D point and determines the data pixel in the world this point best
        /// maps to.  This algorithm rounds to the nearest integer to find the texture pixel.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public Color GetPixelData(float x, float y, int textureIndex)
        {
            int pX = 0;
            int pY = 0;
            GetPixelPosition(x, y, ref pX, ref pY);

            InstanceTexture texture = SelectedCategory.CategoryInstanceTexture;

            //NOTE:  Texture coordinates consider (0,0) as the upper-left corner of the texture.
            //In our coordinate system, we assume that (0,0) is the bottom-left corner.
            //Flip the y-coordinate then.
            return texture.GetPixelData(pX, (texture.Height - pY - 1), textureIndex);
        }

        /// <summary>
        /// Specifies a world-coordinate 2D point and determines the tint pixel in the world this point best
        /// maps to.  This algorithm rounds to the nearest integer to find the texture pixel.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public Color GetPixelTint(float x, float y, int textureIndex)
        {
            int pX = 0;
            int pY = 0;
            GetPixelPosition(x, y, ref pX, ref pY);

            InstanceTexture texture = SelectedCategory.CategoryInstanceTexture;

            //NOTE:  Texture coordinates consider (0,0) as the upper-left corner of the texture.
            //In our coordinate system, we assume that (0,0) is the bottom-left corner.
            //Flip the y-coordinate then.
            return texture.GetPixelTint(pX, (texture.Height - pY - 1), textureIndex);
        }

        /// <summary>
        /// Specifies an array of pixel colors and returns the corresponding procedural objects.
        /// </summary>
        /// <param name="pixel"></param>
        /// <returns></returns>
        public InstanceDefinition[] GetObjectFromPixel(ColorReference[] pixels)
        {
            List<InstanceDefinition> definitions = new List<InstanceDefinition>();
            foreach (ColorReference colReference in pixels)
            {
                Color pixel = colReference.Color;

                foreach (InstanceDefinition definition in SelectedCategory.Definitions)
                {
                    if (definition.Color.A == pixel.A && definition.Color.R == pixel.R && definition.Color.G == pixel.G && definition.Color.B == pixel.B)
                    {
                        definitions.Add(definition);
                    }
                }
            }
            
            return definitions.ToArray();
        }

        /// <summary>
        /// Specifies a pixel color and returns the corresponding procedural object.
        /// </summary>
        /// <param name="pixel"></param>
        /// <returns></returns>
        public InstanceDefinition GetObjectFromPixel(Color pixel)
        {
            foreach (InstanceDefinition definition in SelectedCategory.Definitions)
            {
                if (definition.Color.A == pixel.A && definition.Color.R == pixel.R && definition.Color.G == pixel.G && definition.Color.B == pixel.B)
                {
                    return definition;
                }
            }

            return null;
        }

        /// <summary>
        /// Specifies a pixel color and returns the corresponding procedural objects.
        /// </summary>
        /// <param name="pixel"></param>
        /// <returns></returns>
        public InstanceDefinition[] GetObjectsFromPixel(Color pixel)
        {
            List<InstanceDefinition> definitions = new List<InstanceDefinition>();

            foreach (InstanceDefinition definition in SelectedCategory.Definitions)
            {
                if (definition.Color.A == pixel.A && definition.Color.R == pixel.R && definition.Color.G == pixel.G && definition.Color.B == pixel.B)
                {
                    definitions.Add(definition);
                }
            }

            foreach (InstanceGroup group in SelectedCategory.Groups)
            {
                if (group.Color.A == pixel.A && group.Color.R == pixel.R && group.Color.G == pixel.G && group.Color.B == pixel.B)
                {
                    for (int i = 0; i < group.Group.Count; i++)
                    {
                        definitions.Add(group.Group[i]);
                    }
                }
            }

            return definitions.ToArray();
        }

        /// <summary>
        /// Returns the color based on the definition name.
        /// </summary>
        /// <param name="definitionName"></param>
        /// <returns></returns>
        public Color GetColorFromDefinition(InstanceCategory category, string definitionName)
        {
            foreach (InstanceDefinition definition in category.Definitions)
            {
                if (String.Compare(definition.Name, definitionName, true) == 0)
                {
                    return definition.Color;
                }
            }

            return Color.White;
        }

        /// <summary>
        /// Returns the first definition found through the name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public InstanceDefinition GetDefinition(String name)
        {
            return SelectedCategory.Definitions.Where(definition => String.Compare(name, definition.Name, true) == 0).FirstOrDefault();
        }

        /// <summary>
        /// Returns the first definition found through the name and high detail name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public InstanceDefinition GetDefinition(String name, string highDetailName)
        {
            return SelectedCategory.Definitions.Where(definition => String.Compare(name, definition.Name, true) == 0 && String.Compare(highDetailName, definition.HighDetailName, true) == 0).FirstOrDefault();
        }

        /// <summary>
        /// Return an array version of the current definitions.
        /// </summary>
        /// <returns></returns>
        public InstanceDefinition[] GetDefinitions()
        {
            return SelectedCategory.Definitions.ToArray();
        }

        #endregion Placement Functions

        #region Test Functions
        /// <summary>
        /// Creates a basic Vegetation XML file based on the XML serializer.
        /// Useful for figuring out the syntax of the Vegetation XML or to ensure
        /// the serializer is writing out data as expected.
        /// </summary>
        /// <param name="filename"></param>
        public void Save(string filename)
        {
            /*try
            {
                Bounds.MinBounds.X = 4000.0f;
                Bounds.MinBounds.Y = 4000.0f;
                Bounds.MaxBounds.X = 4000.0f;
                Bounds.MaxBounds.Y = 4000.0f;

                InstanceDefinition def = new InstanceDefinition();
                def.Name = "TestObject_01";
                def.Color = new VegetationColor(255, 128, 64);
                Definitions.Add(def);
                Definitions.Add(def);
                Definitions.Add(def);
                Definitions.Add(def);

                XmlSerializer serializer = new XmlSerializer(typeof(TextureMetadata));
                StreamWriter writer = new StreamWriter(filename);
                serializer.Serialize(writer, this);
                writer.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }*/
        }
        #endregion
    }
}


