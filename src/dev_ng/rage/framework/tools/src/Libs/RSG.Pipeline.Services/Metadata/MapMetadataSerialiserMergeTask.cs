﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;

namespace RSG.Pipeline.Services.Metadata
{

    /// <summary>
    /// Map Metadata Serialiser merge task class for defining parameters to a
    /// ITYP/IMAP merge process.
    /// </summary>
    public class MapMetadataSerialiserMergeTask :
        MapMetadataSerialiserTaskBase
    {
        #region Properties
        /// <summary>
        /// Task inputs.
        /// </summary>
        public IEnumerable<MapMetadataSerialiserMergeInputGroup> InputGroups
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public MapMetadataSerialiserMergeTask(String name,
            IEnumerable<MapMetadataSerialiserMergeInputGroup> inputGroups,
            IEnumerable<MapMetadataSerialiserSceneXmlDependency> dependencies,
            IEnumerable<MapMetadataSerialiserManifestDependency> additionalManifestDependencies,
            IEnumerable<MapMetadataSerialiserCoreGameMetadataZipDependency> metadataDependencies,
            IEnumerable<MapMetadataSerialiserOutput> output,
            String assetCombineFilename,
            String mapExportAdditionsFilename)
            : base(name, dependencies, additionalManifestDependencies, metadataDependencies, output, assetCombineFilename, mapExportAdditionsFilename)
        {
            this.InputGroups = inputGroups;
        }

        /// <summary>
        /// Constructor; from serialised XML configuration file.
        /// </summary>
        /// <param name="xmlTaskElem"></param>
        public MapMetadataSerialiserMergeTask(XElement xmlTaskElem)
            : base(xmlTaskElem)
        {
            Debug.Assert(String.Equals(xmlTaskElem.Name.LocalName, "MergeTask"),
                "Invalid 'MergeTask' element.");

            this.InputGroups = xmlTaskElem.XPathSelectElements("InputGroups").
                Select(xmlInputGroupElem => new MapMetadataSerialiserMergeInputGroup(xmlInputGroupElem));
        }
        #endregion // Constructor(s)

        #region MapMetadataSerialiserTaskBase Methods
        /// <summary>
        /// Serialise task to an XElement.
        /// </summary>
        /// <returns></returns>
        public override XElement Serialise()
        {
            XElement xmlTaskElem = base.Serialise("MergeTask",
                this.InputGroups.Select(inputGroup => inputGroup.Serialise()));
            return (xmlTaskElem);
        }
        #endregion // MapMetadataSerialiserTaskBase Methods
    }

} // RSG.Pipeline.Services.Metadata namespace
