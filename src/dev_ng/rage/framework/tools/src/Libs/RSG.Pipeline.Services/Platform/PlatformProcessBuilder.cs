﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using SIO = System.IO;
using System.Linq;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Pipeline.Content;
using RSG.Pipeline.Core;

namespace RSG.Pipeline.Services.Platform
{

    /// <summary>
    /// Helper methods to create platform conversion processes without requiring
    /// the Rage platform processor to prebuild them; this allows other processors
    /// to create platform-processes and link to their outputs.
    /// </summary>
    /// This abstracts the installer-enabled targets etc.
    /// 
    public static class PlatformProcessBuilder
    {
        #region Constants
        private static readonly String LOG_CTX = "Platform Process Builder";
        private static readonly String RAGE_PROCESSOR = "RSG.Pipeline.Processor.Platform.Rage";
        private static readonly String MANIFEST_CONVERT_PROCESSOR = "RSG.Pipeline.Processor.Platform.ManifestConvert";
        #endregion // Constants

        #region Member Data
        /// <summary>
        /// Platform processor parameters.
        /// </summary>
        private static IDictionary<String, Object> Parameters;

        /// <summary>
        /// Platform processor classnames.
        /// </summary>
        private static String[] PlatformProcessors = new String[] {
            RAGE_PROCESSOR,
            "RSG.Pipeline.Processor.Platform.RpfCreateProcessor",
            "RSG.Pipeline.Processor.Platform.ManifestCreateProcessor"
        };
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Static constructor; loads platform process parameters.
        /// </summary>
        static PlatformProcessBuilder()
        {
            Parameters = new Dictionary<String, Object>();
            LoadParameters();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Create a manifest conversion process; to convert an independent
        /// manifest into a platform resource (same directory).
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        public static ProcessBuilder CreateManifestConvertProcessor(IEngineParameters param,
           IProcessorCollection processors, IContentTree owner,
           IContentNode input, ITarget target)
        {
            Content.File inputFile = (input as Content.File);
            String directory = SIO.Path.GetDirectoryName(inputFile.AbsolutePath);

            String outputFilename = PlatformPathConversion.ConvertAndRemapFilenameToDirectory(
                target, inputFile.AbsolutePath, directory, false);
            IContentNode output = owner.CreateAsset(outputFilename, target.Platform);
            IContentNode resourceTool = owner.CreateFile(target.GetRagebuilderExecutable());

            ProcessBuilder pb = new ProcessBuilder(MANIFEST_CONVERT_PROCESSOR, owner);
            pb.Inputs.Add(input);
            pb.Outputs.Add(output);
            pb.AdditionalDependentContent.Add(resourceTool);

            return (pb);
        }

        /// <summary>
        /// Create a simple platform conversion process; to convert an independent
        /// resource/resource-zip into a platform resource (same directory) for
        /// a single target.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="input"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public static ProcessBuilder CreateSimpleResource(IEngineParameters param, 
            IProcessorCollection processors, IContentTree owner, 
            IContentNode input, ITarget target)
        {
            Content.File inputFile = (input as Content.File);
            String directory = SIO.Path.GetDirectoryName(inputFile.AbsolutePath);

            String outputFilename = PlatformPathConversion.ConvertAndRemapFilenameToDirectory(
                target, inputFile.AbsolutePath, directory, false);
            IContentNode output = owner.CreateAsset(outputFilename, target.Platform);
            IContentNode resourceTool = owner.CreateFile(target.GetRagebuilderExecutable());

            ProcessBuilder pb = new ProcessBuilder(RAGE_PROCESSOR, owner);
            pb.Inputs.Add(input);
            pb.Outputs.Add(output);
            pb.AdditionalDependentContent.Add(resourceTool);

            return (pb);
        }

        /// <summary>
        /// Create a simple platform conversion process; to convert an indpendent
        /// directory into a set of platform resources (same directory) for a
        /// single target.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        public static ProcessBuilder CreateSimpleResourcesForDirectory(
            IEngineParameters param, IProcessorCollection processors,
            IContentTree owner, Content.Directory input, ITarget target)
        {
            ProcessBuilder directoryConversionRageProcessBuilder = new ProcessBuilder(
                RAGE_PROCESSOR, owner);
            directoryConversionRageProcessBuilder.Inputs.Add(input);

            String outputDirectory = String.Empty;
            if (param.Flags.HasFlag(EngineFlags.Preview))
                outputDirectory = target.Branch.Preview;
            else
                outputDirectory = SIO.Path.Combine(input.AbsolutePath, target.Platform.ToString());

            directoryConversionRageProcessBuilder.Outputs.Add(owner.CreateDirectory(outputDirectory));

            return (directoryConversionRageProcessBuilder);
        }

        /// <summary>
        /// Create a simple platform conversion process; to convert an independent
        /// resource/resource-zip into a platform resource (same directory).
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        public static IEnumerable<ProcessBuilder> CreateSimpleResource(
            IEngineParameters param, IProcessorCollection processors,
            IContentTree owner, IContentNode input)
        {
            Debug.Assert(input is Content.File, "Input needs to be a Content.File.");
            if (!(input is Content.File))
                throw (new ArgumentException("Input needs to be a Content.File", "input"));

            Content.File inputFile = (input as Content.File);
            String resourceCacheRoot = GetResourceCacheRootDir(param);
            IEnumerable<ITarget> enabledTargets = GetEnabledTargets(param);
            ICollection<ProcessBuilder> processes = new List<ProcessBuilder>();
            foreach (ITarget target in enabledTargets)
            {
                processes.Add(CreateSimpleResource(param, processors,
                    owner, input, target));
            }

            return (processes);
        }

        /// <summary>
        /// Create a simple platform conversion process; to convert an indpendent
        /// directory into a set of platform resources (same directory) for all
        /// enabled targets.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        public static IEnumerable<ProcessBuilder> CreateSimpleResourcesForDirectory(
            IEngineParameters param, IProcessorCollection processors,
            IContentTree owner, Content.Directory input)
        {
            IEnumerable<ITarget> enabledTargets = GetEnabledTargets(param);
            ICollection<ProcessBuilder> processes = new List<ProcessBuilder>();
            foreach (ITarget target in enabledTargets)
            {
                processes.Add(CreateSimpleResourcesForDirectory(param, processors,
                    owner, input, target));
            }

            return (processes);
        }

        /// <summary>
        /// Create a simple platform conversion process; to convert an independent 
        /// resource/resource-zip into a platform resource (within the cache) for 
        /// a single target.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="input"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public static ProcessBuilder CreateSimpleResourceInCache(
            IEngineParameters param, IProcessorCollection processors,
            IContentTree owner, IContentNode input, ITarget target)
        {
            Content.File inputFile = (input as Content.File);
            String resourceCacheRoot = GetResourceCacheRootDir(param);
           
#warning DHM FIX ME: consider adding preview support.
            String outputFilename = PlatformPathConversion.ConvertAndRemapFilenameToDirectory(
                target, inputFile.AbsolutePath, resourceCacheRoot, false);
            IContentNode output = owner.CreateAsset(outputFilename, target.Platform);
            IContentNode resourceTool = owner.CreateFile(target.GetRagebuilderExecutable());

            ProcessBuilder pb = new ProcessBuilder(RAGE_PROCESSOR, owner);
            pb.Inputs.Add(input);
            pb.Outputs.Add(output);
            pb.AdditionalDependentContent.Add(resourceTool);

            return (pb);
        }

        /// <summary>
        /// Create a simple platform conversion process; to convert an indpendent
        /// directory into a set of platform resources (resource cache) for a
        /// single target.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="input"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public static ProcessBuilder CreateSimpleResourcesInCacheForDirectory(
            IEngineParameters param, IProcessorCollection processors,
            IContentTree owner, Content.Directory input, ITarget target)
        {
            ProcessBuilder directoryConversionRageProcessBuilder = new ProcessBuilder(
                RAGE_PROCESSOR, owner);
            directoryConversionRageProcessBuilder.Inputs.Add(input);

            String resourceCacheRoot = GetResourceCacheRootDir(param);
            String outputDirectory = PlatformPathConversion.ConvertAndRemapDirectoryToDirectory(
                    target, input.AbsolutePath, resourceCacheRoot, true);
            
            directoryConversionRageProcessBuilder.Outputs.Add(owner.CreateDirectory(outputDirectory));
            directoryConversionRageProcessBuilder.Parameters.Add("platform", target.Platform);

            return (directoryConversionRageProcessBuilder);
        }

        /// <summary>
        /// Create a simple platform conversion process; to convert an indpendent
        /// directory into a set of platform resources (resource cache) for all
        /// enabled targets.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        public static IEnumerable<ProcessBuilder> CreateSimpleResourcesInCacheForDirectory(
            IEngineParameters param, IProcessorCollection processors,
            IContentTree owner, Content.Directory input)
        {
            IEnumerable<ITarget> enabledTargets = GetEnabledTargets(param);
            ICollection<ProcessBuilder> processes = new List<ProcessBuilder>();
            foreach (ITarget target in enabledTargets)
            {
                processes.Add(CreateSimpleResourcesInCacheForDirectory(param, processors,
                    owner, input, target));
            }

            return (processes);
        }

        /// <summary>
        /// Create a set of simple platform conversion processes; to convert a
        /// independent resource-zip into a platform resource (within the cache).
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        public static IEnumerable<ProcessBuilder> CreateSimpleResourceInCache(
            IEngineParameters param, IProcessorCollection processors,
            IContentTree owner, IContentNode input)
        {
            Debug.Assert(input is Content.File, "Input needs to be a Content.File.");
            if (!(input is Content.File))
                throw (new ArgumentException("Input needs to be a Content.File", "input"));

            Content.File inputFile = (input as Content.File);
            String resourceCacheRoot = GetResourceCacheRootDir(param);
            IEnumerable<ITarget> enabledTargets = GetEnabledTargets(param);
            ICollection<ProcessBuilder> processes = new List<ProcessBuilder>();
            foreach (ITarget target in enabledTargets)
            {
                processes.Add(CreateSimpleResourceInCache(param, processors, 
                    owner, input, target));
            }
            
            return (processes);
        }
        
        /// <summary>
        /// Return engine-parameter enabled target platforms.
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public static IEnumerable<ITarget> GetEnabledTargets(IEngineParameters param)
        {
            List<ITarget> targets = new List<ITarget>();
            foreach (RSG.Platform.Platform platform in param.Platforms)
            {
                if (param.Branch.Targets.ContainsKey(platform))
                    targets.Add(param.Branch.Targets[platform]);
            }
            return (targets);
        }

        /// <summary>
        /// Return installer-enabled target platforms.
        /// </summary>
        /// <param name="branch"></param>
        /// <returns></returns>
        public static IEnumerable<ITarget> GetInstallerEnabledTargets(IBranch branch)
        {
            return (branch.Targets.Where(t => t.Value.Enabled).Select(t => t.Value));
        }

        /// <summary>
        /// Return installer-enabled platforms.
        /// </summary>
        /// <param name="branch"></param>
        /// <returns></returns>
        public static IEnumerable<RSG.Platform.Platform> GetInstallerEnabledPlatforms(IBranch branch)
        {
            return (branch.Targets.Where(t => t.Value.Enabled).Select(t => t.Value).Select(t => t.Platform));
        }

        /// <summary>
        /// Return resource cache root directory.
        /// </summary>
        /// <param name="branch"></param>
        /// <returns></returns>
        public static String GetResourceCacheRootDir(IEngineParameters param)
        {
            Debug.Assert(Parameters.ContainsKey(Constants.RageProcess_ResourceCache),
                "Cache directory not found in parameters!  Internal error.");
            if (!Parameters.ContainsKey(Constants.RageProcess_ResourceCache))
            {
                param.Log.ErrorCtx(LOG_CTX, "Cache directory not found in parameters!  Internal error.");
                throw (new NotSupportedException("Cache directory not found in parameters!  Internal error."));
            }

            String cacheDirectory = (String)Parameters[Constants.RageProcess_ResourceCache];
            String cacheRoot = SIO.Path.GetFullPath(param.Branch.Environment.Subst(cacheDirectory));
            return (cacheRoot);
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Load platform processor parameters from XML files.
        /// </summary>
        private static void LoadParameters()
        {
            Parameters.Clear();
                String toolsroot = Environment.GetEnvironmentVariable("RS_TOOLSROOT");
            foreach (String platformProcessor in PlatformProcessors)
            {
                String filename = SIO.Path.Combine(toolsroot, "etc", "processors",
                    String.Format("{0}.xml", platformProcessor));
                if (!SIO.File.Exists(filename))
                    continue; // Skip missing parameter files.

                if (!RSG.Base.Xml.Parameters.Load(filename, ref Parameters, false, true))
                    Log.Log__ErrorCtx(LOG_CTX, 
                        "Failed to load platform processor parameters from: {0}",
                        filename);
            }
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Services.Platform namespace
