﻿using System;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;

namespace RSG.Pipeline.Services.Platform.Manifest
{

    /// <summary>
    /// 
    /// </summary>
    public enum HDTXDBindingType
    {
        Drawable,
        Fragment,
        DrawableDictionary,
        TextureDictionary,
    }
        
    /// <summary>
    /// HD TXD asset binding abstraction.
    /// </summary>
    public class HDTXDAssetBinding
    {
        #region Constants
        private static readonly String ELEM_TYPE = "assetType";
        private static readonly String ELEM_TARGET = "targetAsset";
        private static readonly String ELEM_DEPENDENCY = "HDTxd";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// HD TXD binding type.
        /// </summary>
        public HDTXDBindingType Type 
        { 
            get; 
            private set; 
        }

        /// <summary>
        /// Target asset name.
        /// </summary>
        public String TargetAsset 
        { 
            get; 
            private set; 
        }
        
        /// <summary>
        /// Dependent asset name.
        /// </summary>
        public String DependencyAsset 
        { 
            get; 
            private set; 
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="target"></param>
        /// <param name="dependency"></param>
        public HDTXDAssetBinding(HDTXDBindingType type, String target, String dependency)
        {
            this.Type = type;
            this.TargetAsset = target;
            this.DependencyAsset = dependency;
        }

        /// <summary>
        /// Constructor; from XML data.
        /// </summary>
        /// <param name="xmlElem"></param>
        public HDTXDAssetBinding(XElement xmlElem)
        {
            XElement xmlType = xmlElem.XPathSelectElement(ELEM_TYPE);
            XElement xmlTargetAsset = xmlElem.XPathSelectElement(ELEM_TARGET);
            XElement xmlDependency = xmlElem.XPathSelectElement(ELEM_DEPENDENCY);

            if (xmlType.Value.Equals("AT_DRB"))
                this.Type = HDTXDBindingType.Drawable;
            else if (xmlType.Value.Equals("AT_DWD"))
                this.Type = HDTXDBindingType.DrawableDictionary;
            else if (xmlType.Value.Equals("AT_FRG"))
                this.Type = HDTXDBindingType.Fragment;
            else if (xmlType.Value.Equals("AT_TXD"))
                this.Type = HDTXDBindingType.TextureDictionary;
            else
                throw new NotSupportedException(String.Format("Unsupported HD TXD Binding Type: {0}.", xmlType.Value));

            this.TargetAsset = xmlTargetAsset.Value;
            this.DependencyAsset = xmlDependency.Value;
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Serialise this asset binding to XML.
        /// </summary>
        /// <returns></returns>
        public XElement AsCHDTxdAssetBinding(String name)
        {
            XElement xmlType = null;
            switch (this.Type)
            {
                case HDTXDBindingType.Drawable:
                    xmlType = new XElement(ELEM_TYPE, "AT_DRB");
                    break;
                case HDTXDBindingType.DrawableDictionary:
                    xmlType = new XElement(ELEM_TYPE, "AT_DWD");
                    break;
                case HDTXDBindingType.Fragment:
                    xmlType = new XElement(ELEM_TYPE, "AT_FRG");
                    break;
                case HDTXDBindingType.TextureDictionary:
                    xmlType = new XElement(ELEM_TYPE, "AT_TXD");
                    break;
            }

            XElement xmlElem = new XElement(name,
                xmlType,
                new XElement(ELEM_TARGET, this.TargetAsset),
                new XElement(ELEM_DEPENDENCY, this.DependencyAsset));
            return (xmlElem);
        }
        #endregion // Controller Methods
    }

} // RSG.Pipeline.Services.Platform.Manifest namespace
