﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProcessorBase2.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2012-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Services
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using RSG.Base.Extensions;
    using RSG.Base.Xml;
    using RSG.Pipeline.Content;
    using RSG.Pipeline.Core;
    using SIO = System.IO;
    using XGE = RSG.Interop.Incredibuild.XGE;

    /// <summary>
    /// Abstract base class for an IProcessor; takes some of the grunt work out
    /// of implementing the IProcessor interface.  Properties and enumerable 
    /// IContentNode methods are pre-implemented but virtual for local 
    /// optimisations if required.
    /// </summary>
    /// Note: main differences with the original ProcessorBase are:
    ///   1) Prebuild for single IProcess is implemented rather than the
    ///      IEnumerable variant (for optimisation).
    ///   2) the use of parCodeGen metadata parameter files.
    /// 
    /// Parameter file is expected to be a ParameterCollection file that would
    /// contain a ParameterDictionary with the processor name as a map entry.
    /// This is then available as the _parameters member.
    /// 
    public abstract class ProcessorBase2 : 
        IProcessor
    {
        #region Constants
        /// <summary>
        /// Log context.
        /// </summary>
        private static readonly String LOG_CTX = "Core";

        /// <summary>
        /// All cache (temporary) locations for data.  
        /// </summary>
        private static readonly String[] m_CachedLocations = new String[] { "$(cache)" };
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Processor's name (matches the processor nodes in content tree).
        /// </summary>
        public String Name
        {
            get { return this.GetType().ToString(); }
        }

        /// <summary>
        /// Processor's description.
        /// </summary>
        public String Description
        {
            get;
            private set;
        }

        /// <summary>
        /// Whether the parameter XML has already been loaded.
        /// </summary>
        protected bool ParametersLoaded
        {
            get;
            set;
        }
        
        /// <summary>
        /// User-defined options for processor; processor-specific key, value
        /// pairs.  Allow injection of processor options from content-tree.
        /// </summary>
        [Obsolete("Please use IHasParameters interface methods instead.", true)]
        public IDictionary<String, Object> Parameters { get { return null; } }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Parameters dictionary; to support IHasParameters.
        /// </summary>
        protected ParameterDictionary _parameters;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="description"></param>
        public ProcessorBase2(String description)
        {
            this.Description = description;
            this._parameters = new ParameterDictionary();
            this.ParametersLoaded = false;
        }
        #endregion // Constructor(s)

        #region Abstract/Virtual Controller Methods
        /// <summary>
        /// Prepare content; first pass of the on-disk content-tree.  Returns a
        /// set of IProcess objects after determining all required inputs, 
        /// outputs and whether the process' needs to change (this allows the 
        /// concept of 'preprocessors').
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="process">Process to prebuild.</param>
        /// <param name="processors">Processor collection to reference for new resultant proceses.</param>
        /// <param name="owner">Owning IContentTree.</param>
        /// <param name="syncDependencies">Dependencies to sync.</param>
        /// <param name="resultantProcesses">RawProcesses that will actually be built.</param>
        /// <returns>true iff successful; false otherwise</returns>
        /// The resultant processes are used by the pipeline engine after these
        /// calls.
        /// 
        public virtual bool Prebuild(IEngineParameters param,
            IProcess process, IProcessorCollection processors,
            IContentTree owner, out IEnumerable<IContentNode> syncDependencies,
            out IEnumerable<IProcess> resultantProcesses)
        {
            IDictionary<IProcess, IEnumerable<IContentNode>> deps = 
                new Dictionary<IProcess, IEnumerable<IContentNode>>();
            
            bool result = this.Prebuild(param, new IProcess[]{process}, processors, owner,
                out deps, out resultantProcesses);
            syncDependencies = deps[process];
            return (result);
        }

        /// <summary>
        /// Prepare content; first pass of the on-disk content-tree.  Returns a
        /// set of IProcess objects after determining all required inputs, 
        /// outputs and whether the process' needs to change (this allows the 
        /// concept of 'preprocessors').
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="processes">RawProcesses to prebuild.</param>
        /// <param name="processors">Processor collection to reference for new resultant proceses.</param>
        /// <param name="owner">Owning IContentTree.</param>
        /// <param name="syncDependencies">Dependencies to sync.</param>
        /// <param name="resultantProcesses">RawProcesses that will actually be built.</param>
        /// <returns>true iff successful; false otherwise</returns>
        /// The resultant processes are used by the pipeline engine after these
        /// calls.
        /// 
        public abstract bool Prebuild(
            IEngineParameters param,
            IEnumerable<IProcess> processes,
            IProcessorCollection processors,
            IContentTree owner,
            out IDictionary<IProcess, IEnumerable<IContentNode>> syncDependencies,
            out IEnumerable<IProcess> resultantProcesses);

        /// <summary>
        /// Prepare a build for the processes; populating the XGE project with
        /// the tools, environments and tasks required.  Including setting up
        /// the task dependency chain.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="processes"></param>
        /// <param name="tools"></param>
        /// <param name="tasks"></param>
        /// <returns></returns>
        public abstract bool Prepare(IEngineParameters param,
            IEnumerable<IProcess> processes, out IEnumerable<XGE.ITool> tools,
            out IEnumerable<XGE.ITask> tasks);

        /// <summary>
        /// Clean output content for a IProcess.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="process"></param>
        /// <returns></returns>
        public virtual bool Clean(IEngineParameters param, IProcess process)
        {
            bool result = true;
            try
            {
                foreach (IContentNode output in process.Outputs)
                {
                    if (!(output is IFilesystemNode))
                        continue; // Skip non-filesystem nodes.

                    IFilesystemNode fsNode = (IFilesystemNode)output;
                    if (fsNode is File)
                    {
                        result &= TryCleanFile(param, fsNode.AbsolutePath);
                    }
                    else if ((fsNode is IInputEvaluator) && fsNode.Exists())
                    {
                        foreach (IContentNode input in ((IInputEvaluator)fsNode).EvaluateInputs())
                        {
                            if (!(input is IFilesystemNode))
                                continue; // Skip non-filesystem nodes.

                            IFilesystemNode fsInput = (IFilesystemNode)input;
                            if (fsInput is File)
                                result &= TryCleanFile(param, fsInput.AbsolutePath);
                        }
                        result &= TryCleanDirectory(param, fsNode.AbsolutePath);
                    }
                }
            }
            catch (Exception ex)
            {
                result = false;
                param.Log.ToolExceptionCtx(LOG_CTX, ex, "Exception during ProcessorBase::Clean.");
            }

            return (result);
        }

        /// <summary>
        /// Clean output content for a collection of IProcess objects.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="processes"></param>
        /// <returns></returns>
        public virtual bool Clean(IEngineParameters param, IEnumerable<IProcess> processes)
        {
            bool result = true;
            foreach (IProcess process in processes)
            {
                result &= Clean(param, process);
            }
            return (result);
        }

        /// <summary>
        /// Callback for when the build process starts.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        public virtual void Started(IEngineParameters param)
        {
            // No base implementation.
        }

        /// <summary>
        /// Callback for when the build process completes.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        public virtual void Finished(IEngineParameters param)
        {
            // No base implementation.
        }

        /// <summary>
        /// Parse log information; processors are only passed log data for their
        /// respective IProcess output.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="taskLogData">Log string data.</param>
        /// <returns>true for successful process; false for error/failure.</returns>
        public virtual bool ParseLog(IEngineParameters param, IEnumerable<String> taskLogData)
        {
            // Signal to engine that we were successful.
            return (true);
        }

        /// <summary>
        /// Determine whether the parameter exists.
        /// </summary>
        /// <param name="name">Parameter's name</param>
        /// <returns></returns>
        public bool HasParameter(string name)
        {
            return (this._parameters.HasParameter(name));
        }

        /// <summary>
        /// Encapsulation of the Parameters Dictionary access.
        /// Use this to perform a ContainsKey/Add to Parameters
        /// </summary>
        /// <param name="name">Parameter's name</param>
        /// <param name="value">Parameter's value</param>
        public void SetParameter<T>(string name, T value)
        {
            this._parameters.SetParameter(name, value);
        }

        /// <summary>
        /// Encapsulation of the Parameters Dictionary access.
        /// Use this to perform a ContainsKey/Add to Parameters
        /// </summary>
        /// <param name="name">Parameter's name</param>
        /// <param name="value">Parameter's value</param>
        public void SetParameter(string name, object value)
        {
            SetParameter<object>(name, value);
        }

        /// <summary>
        /// Encapsulation of the Parameters Dictionary access.
        /// Use this to perform a ContainsKey/Get to Parameters
        /// </summary>
        /// <param name="name">Parameter's name we try to retrieve the value.</param>
        /// <param name="defaultValue">If we can't retrieve the value, the default value will be used as return value.</param>
        /// <returns></returns>
        public T GetParameter<T>(string name, T defaultValue)
        {
            return (this._parameters.GetParameter<T>(name, defaultValue));
        }

        /// <summary>
        /// Encapsulation of the Parameters Dictionary access.
        /// Use this to perform a ContainsKey/Get to Parameters
        /// </summary>
        /// <param name="name">Parameter's name we try to retrieve the value.</param>
        /// <param name="defaultValue">If we can't retrieve the value, the default value will be used as return value.</param>
        /// <returns></returns>
        public object GetParameter(string name, object defaultValue)
        {
            return GetParameter<object>(name, defaultValue);
        }

        /// <summary>
        /// Validate that a parameter is defined (not null).
        /// </summary>
        /// <param name="param">Engine parameters.</param>
        /// <param name="obj">Object to validate.</param>
        /// <param name="paramKey">Parameter key to check exists.</param>
        /// <returns></returns>
        protected virtual bool ValidateHasParameter(IEngineParameters param, IHasParameters obj, String paramKey)
        {
            Debug.Assert(obj.HasParameter(paramKey),
                String.Format("Parameter '{0}' not defined.", paramKey));
            Object paramValue = obj.GetParameter(paramKey, null);
            if (null == paramValue)
            {
                param.Log.ToolError("Parameter '{0}' not defined.", paramKey);
                return (false);
            }
            return (true);
        }

        /// <summary>
        /// Return "XGE Allow Remote" common parameter value.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        protected virtual bool GetXGEAllowRemote(IEngineParameters param, bool defaultValue)
        {
            this.LoadParameters(param);
            return (this.GetParameter(Constants.ProcessXGE_AllowRemote, defaultValue));
        }

        /// <summary>
        /// Load parameters from defined XML file.
        /// </summary>
        protected virtual void LoadParameters(IEngineParameters param)
        {
            if (this.ParametersLoaded)
                return;

            String filename = GetParametersFilename(param);
            if (!SIO.File.Exists(filename))
                return;

            param.Log.MessageCtx(LOG_CTX, "Loading parCodeGen parameters from {0}.", filename);
            ParameterCollection collection = ParameterCollection.Load(filename);
            this._parameters = collection.FirstOrDefault(d => d.Name.Equals(this.Name));
            if (null == this._parameters)
            {
                param.Log.WarningCtx(LOG_CTX, "ParameterCollection does not have a key for '{0}' processor.",
                    this.Name); 
                this._parameters = new ParameterDictionary();
            }

            this.ParametersLoaded = true;
        }

        /// <summary>
        /// Clear parameters.
        /// </summary>
        protected virtual void ClearParameters(IEngineParameters param)
        {
            param.Log.MessageCtx(LOG_CTX, "Clearing parameters.");
            this._parameters.Clear();
            this.ParametersLoaded = false;
        }

        /// <summary>
        /// Return parameters XML filename.
        /// </summary>
        /// <returns></returns>
        /// This is virtual allowing you to override it if required; although
        /// it's not recommended.
        /// 
        protected virtual String GetParametersFilename(IEngineParameters param)
        {
            return (System.IO.Path.Combine(param.Branch.Project.Config.ToolsConfig,
                "processors", String.Format("{0}.meta", this.GetType().FullName)));
        }

        /// <summary>
        /// Return parameters XML filename.
        /// </summary>
        /// <returns></returns>
        /// This is virtual allowing you to override it if required; although
        /// it's not recommended.
        /// 
        protected virtual String GetProjectParametersFilename(IEngineParameters param)
        {
            return (System.IO.Path.Combine(param.Branch.Project.Config.ToolsConfig,
                "processors", "RSG.Pipeline.Project.xml"));
        }

        /// <summary>
        /// Attempt to delete a file; catching IO exceptions and raising errors
        /// as required with filename information.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="filename"></param>
        protected bool TryCleanFile(IEngineParameters param, String filename)
        {
            bool result = true;
            try
            {
                // DHM FIX ME: filename.StartsWith will never work here because location
                // hasn't been resolved!
                if (SIO.File.Exists(filename))
                {
                    SIO.FileAttributes attributes = SIO.File.GetAttributes(filename);

                    foreach (string location in m_CachedLocations)
                    {
                        if (attributes.HasFlag(SIO.FileAttributes.ReadOnly) && filename.StartsWith(location))
                        {
                            SIO.File.SetAttributes(filename, attributes ^ SIO.FileAttributes.ReadOnly);
                            break;
                        }
                    }

                    SIO.File.Delete(filename);
                }
            }
            catch (System.IO.IOException ex)
            {
                result = false;
                if (ex.IsFileLocked())
                {
                    param.Log.ErrorCtx(LOG_CTX, "File {0} is locked.  Do you have the game running?",
                        filename);
                }
                else
                {
                    param.Log.ExceptionCtx(LOG_CTX, ex, "Exception deleting file {0}.  Do you have the game running?",
                        filename);
                }
            }
            return result;
        }

        /// <summary>
        /// Attempt to delete a file; catching IO exceptions and raising errors
        /// as required with filename information.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="directory"></param>
        protected bool TryCleanDirectory(IEngineParameters param, String directory)
        {
            bool result = true;
            try
            {
                String[] subdirs = System.IO.Directory.GetDirectories(directory);
                foreach (string subdir in subdirs)
                    TryCleanDirectory(param, subdir);

                // If a process has generated sub directories we need to make sure 
                // all files are cleaned up via TryCleanFile.
                String[] files = SIO.Directory.GetFiles(directory, "*.*");
                foreach (string file in files)
                    TryCleanFile(param, file);

                if (SIO.Directory.Exists(directory))
                    SIO.Directory.Delete(directory);
            }
            catch (System.IO.IOException ex)
            {
                result = false;
                if (ex.IsFileLocked())
                {
                    param.Log.ErrorCtx(LOG_CTX, "Directory {0} is locked.  Do you have the game running?",
                        directory);
                }
                else
                {
                    param.Log.ExceptionCtx(LOG_CTX, ex, "Exception deleting directory {0}.  Do you have the game running?",
                        directory);
                }
            }
            return (result);
        }
        #endregion // Abstract/Virtual Controller Methods
    }

} // RSG.Pipeline.Services namespace
