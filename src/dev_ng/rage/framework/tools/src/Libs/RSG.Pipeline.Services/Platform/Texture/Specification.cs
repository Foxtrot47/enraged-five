﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Platform;

namespace RSG.Pipeline.Services.Platform.Texture
{

    /// <summary>
    /// Texture pipeline specification data (TCL, TCS, TCP).
    /// </summary>
    /// <see cref="SpecificationFactory" />
    /// If adding new property members read from templates please update the
    /// Merge method.
    /// 
    public class Specification : ISpecification
    {
        #region Constants
        private static readonly String EXT_TCL = "tcl";
        private static readonly String EXT_TCS = "tcs";
        private static readonly String EXT_TCP = "tcp";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Immediate specification parent filename.
        /// </summary>
        public String ImmediateParent
        {
            get;
            private set;
        }

        /// <summary>
        /// Specification filenames in order of load.
        /// </summary>
        public IEnumerable<String> Filenames
        {
            get { return m_sFilenames; } 
        }
        private Stack<String> m_sFilenames;

        /// <summary>
        /// Source texture absolute paths (BMP, TGA, etc).
        /// </summary>
        public IEnumerable<String> SourceTextures
        {
            get { return m_lSourceTextures; }
        }
        private ICollection<String> m_lSourceTextures;

        /// <summary>
        /// Export texture absolute path (uncompressed DDS).
        /// </summary>
        public IEnumerable<IResourceTexture> ResourceTextures
        {
            get { return m_lResourceTextures; }
        }
        private ICollection<IResourceTexture> m_lResourceTextures;

        /// <summary>
        /// Parent specification template.
        /// </summary>
        public String Parent
        {
            get;
            private set;
        }

        /// <summary>
        /// ImageSplitHD property; splits the top-MIP chain.
        /// </summary>
        public bool ImageSplitHD
        {
            get;
            private set;
        }

        /// <summary>
        /// SkipProcessing property; skips texture pipeline processing.
        /// </summary>
        public bool SkipProcessing
        {
            get;
            private set;
        }

        /// <summary>
        /// ImageFiles property; additional texture inputs.
        /// </summary>
        public IEnumerable<String> ImageFiles
        {
            get { return (m_imageFiles); }
        }
        private ICollection<String> m_imageFiles;

        /// <summary>
        /// Log object.
        /// </summary>
        public static IUniversalLog Log
        {
            get;
            private set;
        }
        
        /// <summary>
        /// Array of filename extensions for top-level templates.
        /// </summary>
        public static String[] TemplateExtensions
        {
            get { return (m_lTemplateExtensions.ToArray()); }
        }
        private static List<String> m_lTemplateExtensions;

        /// <summary>
        /// Array of filename extensions for texture settings.
        /// </summary>
        public static String[] SpecificationExtensions
        {
            get { return (m_lSpecificationExtensions.ToArray()); }
        }
        private static List<String> m_lSpecificationExtensions;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor; used for merge.
        /// </summary>
        internal Specification()
        {
            this.m_sFilenames = new Stack<String>();
            this.m_imageFiles = new List<String>();
            this.m_lSourceTextures = new List<String>();
            this.m_lResourceTextures = new List<IResourceTexture>();
        }

        /// <summary>
        /// Constructor; from a TCL filename.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="filename"></param>
        public Specification(ITarget target, String filename)
           : this()
        {
            Load(target, filename);
        }

        /// <summary>
        /// Constructor; from a stream.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="stream"></param>
        public Specification(ITarget target, Stream stream)
            : this()
        {
            Load(target, stream);
        }

        /// <summary>
        /// Static constructor.
        /// </summary>
        static Specification()
        {
            LogFactory.Initialize();
            Log = LogFactory.CreateUniversalLog("Texture");
            m_lTemplateExtensions = new List<String>(new String[] { String.Format(".{0}", EXT_TCP) });
            m_lSpecificationExtensions = new List<String>(new String[] { String.Format(".{0}", EXT_TCL), String.Format(".{0}", EXT_TCS) });
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Load from a disk file.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="filename"></param>
        public void Load(ITarget target, String filename)
        {
            // We also check and load the target templates; this is an 
            // implicit parent so its loaded first.
            String targetExtension = PlatformUtils.PlatformRagebuilderTextureTemplateExtension(target.Platform);
            String targetSpecification = Path.ChangeExtension(filename, targetExtension);
            this.m_sFilenames.Push(targetSpecification);
            if (File.Exists(targetSpecification))
            {
#if USE_SPECIFICATION_FACTORY
                // Load using the factory; the template will then
                // become cached.
                ISpecification templateSpec = SpecificationFactory.Load(
                    target, targetSpecification);
                this.Merge(templateSpec);
#else
                this.Load(target, targetSpecification);
#endif // !USE_SPECIFICATION_FACTORY
            }

            using (FileStream fs = new FileStream(filename, FileMode.Open,
                FileAccess.Read))
            {
                this.m_sFilenames.Push(filename);
                Load(target, fs);
            }
        }

        /// <summary>
        /// Load from a data stream.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="stream"></param>
        public void Load(ITarget target, Stream stream)
        {
            XDocument xmlDoc = XDocument.Load(stream,
                LoadOptions.SetLineInfo | LoadOptions.SetBaseUri);

            // Check immediate parent.
            XElement xmlParentElem = xmlDoc.Root.XPathSelectElement(
                "//parent");
            if (null != xmlParentElem)
            {
                String filename = target.Environment.Subst(xmlParentElem.Value.Trim());
                if (String.IsNullOrWhiteSpace(this.ImmediateParent))
                    this.ImmediateParent = filename;

                // Load parent specification (if requested).
                if (!String.IsNullOrWhiteSpace(filename))
                {
                    filename = Path.GetFullPath(filename);
                    String tcsPath = Path.ChangeExtension(filename, EXT_TCS);
                    String tcpPath = Path.ChangeExtension(filename, EXT_TCP);
                    if (File.Exists(tcsPath))
                    {
                        try
                        {
#if USE_SPECIFICATION_FACTORY
                            // Load using the factory; the template will then
                            // become cached.
                            ISpecification templateSpec = SpecificationFactory.Load(
                                target, tcsPath);
                            this.Merge(templateSpec);
#else
                            this.Load(target, tcsPath);
#endif // !USE_SPECIFICATION_FACTORY
                        }
                        catch (System.Exception)
                        {
                            Specification.Log.Error("Error loading texture specification {0}; invalid XML?",
                                tcsPath);
                        }
                    }
                    else if (File.Exists(tcpPath))
                    {
                        try
                        {
#if USE_SPECIFICATION_FACTORY
                            // Load using the factory; the template will then
                            // become cached.
                            ISpecification templateSpec = SpecificationFactory.Load(
                                target, tcpPath);
                            this.Merge(templateSpec);
#else
                            this.Load(target, tcpPath);
#endif // !USE_SPECIFICATION_FACTORY
                        }
                        catch (System.Exception)
                        {
                            Specification.Log.Error("Error loading texture template {0}; invalid XML?",
                                tcpPath);
                        }
                    }
                    else
                    {
                        // We need to ensure the TCP template or TCS file that
                        // don't exist get pushed onto the filename stack as 
                        // they are dependencies.  This should be a valid 
                        // assumption unless we have TCL -> TCS -> TCS -> TCP
                        // or any other deeper chains.
                        if (0 == this.Filenames.Count())
                            this.m_sFilenames.Push(tcsPath);
                        else
                            this.m_sFilenames.Push(tcpPath);
                    }
                }
            }

            IEnumerable<XElement> xmlResourceTexturesElems = xmlDoc.Root.XPathSelectElements(
                "//resourceTextures/Item");
            if (null != xmlResourceTexturesElems)
            {
                foreach (XElement xmlResourceTextureElem in xmlResourceTexturesElems)
                {
                    IResourceTexture resourceTexture = new ResourceTexture(xmlResourceTextureElem, target);
                    this.m_lResourceTextures.Add(resourceTexture);
                }
            }

            XElement xmlSkipProcessingElem = xmlDoc.Root.XPathSelectElement(
                "//skipProcessing");
            if (null != xmlSkipProcessingElem)
            {
                String skipProcessingValue = xmlSkipProcessingElem.Attributes().
                    Where(attr => 0 == String.Compare("value", attr.Name.LocalName)).
                    Select(attr => attr.Value).FirstOrDefault();
                this.SkipProcessing = (skipProcessingValue != null) ? bool.Parse(skipProcessingValue) : false;
            }

            XElement xmlImageSplitHD = xmlDoc.Root.XPathSelectElement(
                "//imageSplitHD");
            if (null != xmlImageSplitHD)
            {
                String imageSplitHDValue = xmlImageSplitHD.Attributes().
                    Where(attr => 0 == String.Compare("value", attr.Name.LocalName)).
                    Select(attr => attr.Value).FirstOrDefault();
                this.ImageSplitHD = (imageSplitHDValue != null) ? bool.Parse(imageSplitHDValue) : false;
            }

            IEnumerable<XElement> xmlImageFilesElem = xmlDoc.Root.XPathSelectElements(
                "//imageFiles/Item");
            foreach (XElement xmlImageFileElem in xmlImageFilesElem)
            {
                String textureFilename = target.Environment.Subst(
                    xmlImageFileElem.Value.Trim());
                textureFilename = Path.GetFullPath(textureFilename);
                this.m_imageFiles.Add(textureFilename);
            }
        }

        /// <summary>
        /// Merge parent specification details into this specification.
        /// </summary>
        /// <param name="parentSpec"></param>
        /// Note: this method requires updating when new properties are added.
        /// 
        public void Merge(ISpecification parentSpec)
        {
            foreach (String parentFilename in parentSpec.Filenames)
                this.m_sFilenames.Push(parentFilename);
            this.m_lSourceTextures.AddRange(parentSpec.SourceTextures);
            this.m_lResourceTextures.AddRange(parentSpec.ResourceTextures);
            this.m_imageFiles.AddRange(parentSpec.ImageFiles);

            this.SkipProcessing = parentSpec.SkipProcessing;
            this.ImageSplitHD = parentSpec.ImageSplitHD;
        }
        #endregion // Controller Methods

        #region Static Controller Methods
        /// <summary>
        /// Return whether specified filename is a Texture Pipeline template.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static bool IsTemplate(String filename)
        {
            String ext = Path.GetExtension(filename);
            return (m_lTemplateExtensions.Contains(ext));
        }

        /// <summary>
        /// Return whether specified filename is a Texture Pipeline specification.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static bool IsSpecification(String filename)
        {
            String ext = Path.GetExtension(filename);
            return (m_lSpecificationExtensions.Contains(ext));
        }
        #endregion // Static Controller Methods
    }

} // RSG.Pipeline.Services.Platform.Texture namespace
