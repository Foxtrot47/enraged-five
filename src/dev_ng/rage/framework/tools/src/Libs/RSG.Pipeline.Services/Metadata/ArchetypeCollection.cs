﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;

namespace RSG.Pipeline.Services.Metadata
{

    /// <summary>
    /// Class for reading map ITYP files for RPF sorting and dependency 
    /// information.  Needs to be quick and dirty for RPF sorting.
    /// </summary>
    internal class ArchetypeCollection : 
        List<Archetype>,
        ICollection<Archetype>
    {
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="metadata_directory"></param>
        public ArchetypeCollection(String metadata_directory)
        {
            IEnumerable<String> ityp_filenames = Directory.GetFiles(metadata_directory, "*.ityp");
            foreach (String ityp_filename in ityp_filenames)
            {
                XDocument xmlDoc = XDocument.Load(ityp_filename,
                    LoadOptions.SetLineInfo | LoadOptions.SetBaseUri);
                foreach (XElement xmlElem in xmlDoc.Root.XPathSelectElements("/CMapTypes/archetypes/Item"))
                {
                    this.Add(new Archetype(xmlElem));
                }
            }
        }
        #endregion // Constructor(s)
    }

    /// <summary>
    /// Archetype abstraction.
    /// </summary>
    internal class Archetype
    {
        public String Name { get; set; }
        public String DrawableDictionary { get; set; }
        public String TextureDictionary { get; set; }
        public String PhysicsDictionary { get; set; }

        #region Constructor(s)
        /// <summary>
        /// Constructor; from an XElement.
        /// </summary>
        /// <param name="xmlElem"></param>
        public Archetype(XElement xmlElem)
        {
            this.Name = xmlElem.Element("name").Value.Trim();
            
            if (null != xmlElem.Element("drawableDictionary"))
                this.DrawableDictionary = xmlElem.Element("drawableDictionary").Value.Trim();
            else
                this.DrawableDictionary = String.Empty;

            if (null != xmlElem.Element("textureDictionary")) 
                this.TextureDictionary = xmlElem.Element("textureDictionary").Value.Trim();
            else
                this.TextureDictionary = String.Empty;

            XElement xmlPhysicsElem = xmlElem.Element("physicsDictionary");
            if (null != xmlPhysicsElem)
                this.PhysicsDictionary = xmlPhysicsElem.Value.Trim();
            else
                this.PhysicsDictionary = String.Empty;
        }
        #endregion // Constructor(s)
    }

} // RSG.Pipeline.Services.Metadata namespace
