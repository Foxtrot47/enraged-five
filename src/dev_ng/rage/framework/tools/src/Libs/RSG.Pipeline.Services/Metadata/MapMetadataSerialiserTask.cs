﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;

namespace RSG.Pipeline.Services.Metadata
{

    /// <summary>
    /// Map Metadata Serialiser task; serialise map container SceneXml to
    /// ITYP/IMAP combination.
    /// </summary>
    public class MapMetadataSerialiserTask :
        MapMetadataSerialiserTaskBase
    {
        #region Properties
        /// <summary>
        /// Task inputs.
        /// </summary>
        public IEnumerable<MapMetadataSerialiserInput> Inputs
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="inputs"></param>
        /// <param name="dependencies"></param>
        /// <param name="output"></param>
        /// <param name="assetCombineFilename"></param>
        public MapMetadataSerialiserTask(String name,
            IEnumerable<MapMetadataSerialiserInput> inputs,
            IEnumerable<MapMetadataSerialiserSceneXmlDependency> dependencies,
            IEnumerable<MapMetadataSerialiserManifestDependency> additionalManifestDependencies,
            IEnumerable<MapMetadataSerialiserCoreGameMetadataZipDependency> metadataDependencies,
            IEnumerable<MapMetadataSerialiserOutput> output,
            String assetCombineFilename,
            String mapExportAdditionsFilename)
            : base(name, dependencies, additionalManifestDependencies, metadataDependencies, output, assetCombineFilename, mapExportAdditionsFilename)
        {
            this.Inputs = inputs;
        }

        /// <summary>
        /// Constructor; from serialised XML configuration file.
        /// </summary>
        /// <param name="xmlTaskElem"></param>
        public MapMetadataSerialiserTask(XElement xmlTaskElem)
            : base(xmlTaskElem)
        {
            Debug.Assert(String.Equals(xmlTaskElem.Name.LocalName, "Task"),
                "Invalid 'Task' element.");

            this.Inputs = xmlTaskElem.XPathSelectElements("Input").
                Select(xmlInputElem => new MapMetadataSerialiserInput(xmlInputElem));
        }
        #endregion // Constructor(s)

        #region MapMetadataSerialiserTaskBase Methods
        /// <summary>
        /// Serialise task to an XElement.
        /// </summary>
        /// <returns></returns>
        public override XElement Serialise()
        {
            XElement xmlTaskElem = base.Serialise("Task", 
                this.Inputs.Select(input => input.Serialise()));
            return (xmlTaskElem);
        }
        #endregion // MapMetadataSerialiserTaskBase Methods
    }

} // RSG.Pipeline.Services.Metadata namespace
