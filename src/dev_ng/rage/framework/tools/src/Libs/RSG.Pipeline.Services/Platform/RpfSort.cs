﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.IO;
using System.Text.RegularExpressions;
using RSG.Base.Collections;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Pipeline.Core;
using RSG.Pipeline.Services.Metadata;
using RSG.Platform;

namespace RSG.Pipeline.Services.Platform
{

    /// <summary>
    /// RPF file construction sorting methods.
    /// </summary>
    public static class RpfSort
    {
        #region Constants
        /// <summary>
        /// Log context.
        /// </summary>
        private static String LOG_CTX = "RPF Sort";

        /// <summary>
        /// 
        /// </summary>
        private static Regex REGEX_MANIFEST = new Regex(@"_manifest\..mf", RegexOptions.Compiled);
        #endregion // Constants

        /// <summary>
        /// Sort an input list of files for RPF construction to optimise file
        /// order limiting seeking for the game runtime.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="files"></param>
        /// <param name="sorted_files"></param>
        /// <param name="metadata_directory"></param>
        /// <seealso cref="https://devstar.rockstargames.com/wiki/index.php/Dev:RPF_Sorting"/>
        /// 
        public static void SortFileList(ILog log,
            IEnumerable<Pair<String, String>> files, 
            out IEnumerable<Pair<String, String>> sorted_files,
            String metadata_directory)
        {
            log.ProfileCtx(LOG_CTX, "RPF sorting algorithm.");
            if (!String.IsNullOrEmpty(metadata_directory))
            {
                log.Message("ITYP metadata found; RPF will be sorted according to asset dependencies.");
                SortFileListWithITYP(log, metadata_directory, files, out sorted_files);
            }
            else
            {
                log.Message("ITYP metadata not found; RPF will be sorted according to asset basename conventions.");
                SortFileListDefault(log, files, out sorted_files);
            }

            // Additional sorting mechanisms may go here.

            Debug.Assert(files.Count() == sorted_files.Count(),
                "Input and output file lists have different sizes.",
                "Input list has {0} entries; output list has {1} entries.",
                files.Count(), sorted_files.Count());
            if (files.Count() != sorted_files.Count())
            {
                log.Error("Input list has {0} entries; output list has {1} entries.",
                files.Count(), sorted_files.Count());
            }
            log.ProfileEnd();
        }

        /// <summary>
        /// Sort file list without an ITYP; based on common basename of assets.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="files"></param>
        /// <param name="sorted_files"></param>
        private static void SortFileListDefault(ILog log,
            IEnumerable<Pair<String, String>> files, 
            out IEnumerable<Pair<String, String>> sorted_files)
        {
            List<Pair<String, String>> unsorted = files.ToList();
            unsorted.Sort(new RpfSortComparer());

            sorted_files = unsorted;
        }

        /// <summary>
        /// Sort file list with an ITYP; based on dependencies presented.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="metadata_directory"></param>
        /// <param name="files"></param>
        /// <param name="sorted_files"></param>
        private static void SortFileListWithITYP(ILog log,
            String metadata_directory, 
            IEnumerable<Pair<String, String>> files, 
            out IEnumerable<Pair<String, String>> sorted_files)
        {
            Debug.Assert(Directory.Exists(metadata_directory),
                String.Format("Metadata directory for sorting input not found: {0}.  Using fallback.", metadata_directory));
            if (!Directory.Exists(metadata_directory))
            {
                log.WarningCtx(LOG_CTX, "Metadata directory for sorting input not found: {0}.  Using fallback.",
                    metadata_directory);
                SortFileListDefault(log, files, out sorted_files);
            }
            else
            {
                ArchetypeCollection archCollection = new ArchetypeCollection(metadata_directory);

                IOrderedEnumerable<Pair<String, String>> manifest_files =
                    files.Where(f => REGEX_MANIFEST.IsMatch(f.Second)).OrderBy(f => f.Second);
                Regex regexITYP = FileType.ITYP.GetRegexValue();
                IOrderedEnumerable<Pair<String, String>> ityp_files =
                    files.Where(f => regexITYP.IsMatch(Path.GetExtension(f.Second))).OrderBy(f => f.Second);
                Regex regexIMAP = FileType.IMAP.GetRegexValue();
                IOrderedEnumerable<Pair<String, String>> imap_files =
                    files.Where(f => regexIMAP.IsMatch(Path.GetExtension(f.Second))).OrderBy(f => f.Second);
                Regex regexPhys = FileType.BoundsDictionary.GetRegexValue();
                IOrderedEnumerable<Pair<String, String>> physics_files =
                    files.Where(f => regexPhys.IsMatch(Path.GetExtension(f.Second))).OrderBy(f => f.Second);
                Regex regexBounds = FileType.BoundsFile.GetRegexValue();
                IOrderedEnumerable<Pair<String, String>> bounds_files =
                    files.Where(f => regexBounds.IsMatch(Path.GetExtension(f.Second))).OrderBy(f => f.Second);
                Regex regexClip = FileType.ClipDictionary.GetRegexValue();
                IOrderedEnumerable<Pair<String, String>> clip_files =
                    files.Where(f => regexClip.IsMatch(Path.GetExtension(f.Second))).OrderBy(f => f.Second);

                // Loop through archetypes.
                Regex regexDraw = FileType.Drawable.GetRegexValue();
                Regex regexFrag = FileType.Fragment.GetRegexValue();
                Regex regexDrawDict = FileType.DrawableDictionary.GetRegexValue();
                Regex regexTXD = FileType.TextureDictionary.GetRegexValue();
                List<Pair<String, String>> assets = new List<Pair<String, String>>();
                foreach (Archetype archetype in archCollection)
                {
                    Pair<String, String> drawable = files.Where(f =>
                        regexDraw.IsMatch(Path.GetExtension(f.Second)) &&
                        0 == String.Compare(Filename.GetBasename(f.Second), archetype.Name, true)).
                        FirstOrDefault();
                    Pair<String, String> fragment = files.Where(f =>
                        regexFrag.IsMatch(Path.GetExtension(f.Second)) &&
                        0 == String.Compare(Filename.GetBasename(f.Second), archetype.Name, true)).
                        FirstOrDefault();
                    Pair<String, String> drawableDict = null;
                    if (!String.IsNullOrEmpty(archetype.DrawableDictionary))
                        drawableDict = files.Where(f =>
                         regexDrawDict.IsMatch(Path.GetExtension(f.Second)) &&
                         0 == String.Compare(Filename.GetBasename(f.Second), archetype.DrawableDictionary, true)).
                         FirstOrDefault();
                    Pair<String, String> txd = null;
                    if (!String.IsNullOrEmpty(archetype.TextureDictionary))
                        txd = files.Where(f =>
                         regexTXD.IsMatch(Path.GetExtension(f.Second)) &&
                         0 == String.Compare(Filename.GetBasename(f.Second), archetype.TextureDictionary, true)).
                         FirstOrDefault();

                    if (txd is Pair<String, String>)
                        assets.Add(txd);
                    if (drawable is Pair<String, String>)
                        assets.Add(drawable);
                    if (fragment is Pair<String, String>)
                        assets.Add(fragment);
                    if (drawableDict is Pair<String, String>)
                        assets.Add(drawableDict);
                }
                
                IEnumerable<Pair<String, String>> remaining_files =
                    files.Where(f => !manifest_files.Contains(f));
                remaining_files = remaining_files.Where(f => !ityp_files.Contains(f));
                remaining_files = remaining_files.Where(f => !imap_files.Contains(f));
                remaining_files = remaining_files.Where(f => !physics_files.Contains(f));
                remaining_files = remaining_files.Where(f => !bounds_files.Contains(f));
                remaining_files = remaining_files.Where(f => !clip_files.Contains(f));
                remaining_files = remaining_files.Where(f => !assets.Contains(f));

                List<Pair<String, String>> sorted = new List<Pair<String, String>>();
                sorted.AddRange(manifest_files);
                sorted.AddRange(ityp_files);
                sorted.AddRange(imap_files);
                sorted.AddRange(physics_files);
                sorted.AddRange(bounds_files);
                sorted.AddRange(clip_files);
                sorted.AddRange(assets.Distinct());

                // Finally add our leftovers.
                sorted.AddRange(remaining_files);

                sorted_files = sorted;
            }
        }
    }

} // RSG.Pipeline.Services.Platform namespace
