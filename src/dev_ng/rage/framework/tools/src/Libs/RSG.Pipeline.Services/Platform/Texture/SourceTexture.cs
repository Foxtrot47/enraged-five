﻿using System;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Configuration;

namespace RSG.Pipeline.Services.Platform.Texture
{

    /// <summary>
    /// Source texture (BMP, TGA, etc).
    /// </summary>
    public class SourceTexture : ISourceTexture
    {
        #region Properties
        /// <summary>
        /// Absolute path to source texture file.
        /// </summary>
        public String AbsolutePath { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="xmlSourceTextureElem"></param>
        public SourceTexture(XElement xmlSourceTextureElem, ITarget target)
        {
            XElement xmlPathNameElem = xmlSourceTextureElem.XPathSelectElement("pathname");
            this.AbsolutePath = Path.GetFullPath(target.Environment.Subst(xmlPathNameElem.Value.Trim()));
        }
        #endregion // Constructor(s)
    }

} // RSG.Pipeline.Services.Platform.Texture namespace
