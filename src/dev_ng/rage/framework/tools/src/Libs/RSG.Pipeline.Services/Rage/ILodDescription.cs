﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Configuration;
using RSG.Rage.Entity;

namespace RSG.Pipeline.Services.Rage
{
    /// <summary>
    /// 
    /// </summary>
    public interface ILodDescription
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        String Platform { get; set; }

        /// <summary>
        /// 
        /// </summary>
        Mode ModeN { get; set; }

        /// <summary>
        /// Split type e.g. High for HD split.
        /// </summary>
        Split Split { get; set; }

        /// <summary>
        /// 
        /// </summary>
        IDictionary<LodLevel, String> LodLevels { get; set; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Returns a path built from platform, mode and split.
        /// </summary>
        /// <param name="rootPath"></param>
        /// <returns></returns>
        String GetDataLocation(String rootPath);

        /// <summary>
        /// Returns a path built from platform and mode.
        /// </summary>
        /// <param name="rootPath"></param>
        /// <returns></returns>
        String GetShortDataLocation(String rootPath);
        #endregion // Methods
    }
}
