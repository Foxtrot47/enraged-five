﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.Pipeline.Services.Platform.Manifest
{
    public class InteriorBounds
    {
        public static InteriorBounds FromManifest(XElement xmlElem)
        {
            String name = xmlElem.Element("Name").Value;
            String[] boundsNames = xmlElem.Element("Bounds")
                                          .Elements("Item")
                                          .Select(boundsItemElem => boundsItemElem.Value)
                                          .ToArray();

            return new InteriorBounds(name, boundsNames);
        }

        public static InteriorBounds FromBoundsAdditions(XElement element)
        {
            String name = element.Attribute("name").Value.ToString();
            List<String> boundsNames = new List<String>();
            foreach (XElement boundsElement in element.Elements("Bounds"))
            {
                boundsNames.Add(boundsElement.Attribute("name").Value.ToString());
            }

            return new InteriorBounds(name, boundsNames);
        }

        private InteriorBounds(String name, IEnumerable<String> boundsNames)
        {
            name_ = name;
            boundsNames_ = boundsNames;
        }

        /// <summary>
        /// Serialise to AsCInteriorBoundsFile XElement for RPF Manifest.
        /// </summary>
        /// <returns></returns>
        public XElement AsCInteriorBoundsFile(String name)
        {
            XElement mapDataGroupElement = new XElement(
                name,
                new XElement("Name", name_),
                new XElement("Bounds",
                    boundsNames_.Select(boundName => new XElement("Item", boundName))));

            return mapDataGroupElement;
        }

        private String name_;
        private IEnumerable<String> boundsNames_;
    }
}
