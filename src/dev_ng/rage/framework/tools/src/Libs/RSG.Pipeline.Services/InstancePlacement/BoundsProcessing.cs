﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using RSG.Bounds;
using RSG.Base.Logging.Universal;
using RSG.Base.Math;
using RSG.Platform;

namespace RSG.Pipeline.Services.InstancePlacement
{
    /// <summary>
    /// Static helper class for processing bounds used by instance placement.
    /// </summary>
    public static class BoundsProcessing
    {
        #region Constants
        /// <summary>
        /// Log context static.
        /// </summary>
        private static readonly String LOG_CTX = "Instance Placement Bounds Processing";
        #endregion Constants

        #region Public Static Methods
        /// <summary>
        /// Processes the bounds from a specified collision zip file.
        /// Will extract the collision, go through all the bnd files to create a collection of
        /// OptimisedBounds and create a bounding box based off the bound primitives.
        /// </summary>
        /// <param name="collisionZip"></param>
        /// <param name="optimisedBounds"></param>
        /// <param name="containerExtents"></param>
        /// <returns></returns>
        public static bool ProcessBounds(String collisionZip, ICollection<OptimisedBound> optimisedBounds, BoundingBox3f containerExtents, IUniversalLog log)
        {
            log.ProfileCtx(LOG_CTX, String.Format("Processing bounds from {0}...", collisionZip));

            //Temporary:  Unpack the collision zip file to process all bounds files.
            //The unpacking and packaging of these .zip files needs to occur at the pipeline level, possibly?
            //We have to consider that this may be wanted to run outside of the pipeline?
            string collisionDirectory = Path.Combine(Path.GetDirectoryName(collisionZip), Path.GetFileNameWithoutExtension(collisionZip));

            if (Directory.Exists(collisionDirectory))
            {
                try
                {
                    foreach (String file in Directory.EnumerateFiles(collisionDirectory))
                    {
                        File.Delete(file);
                    }
                }
                catch
                {
                    log.ErrorCtx(LOG_CTX, "Unable to delete cache directory '{0}'.", collisionDirectory);
                    return false;
                }
            }
            else
            {
                Directory.CreateDirectory(collisionDirectory);
            }

            IEnumerable<String> extractedFiles;
            bool extractResult = Zip.ExtractAll(collisionZip, collisionDirectory, true, null, out extractedFiles);

            if (!extractResult)
            {
                log.ErrorCtx(LOG_CTX, "Extracting '{0}' to '{1}' failed.",
                    collisionZip, collisionDirectory);
                return false;
            }

            //ICollection<BVH> bounds = new List<BVH>();
            string extension = RSG.Platform.FileType.ExportBoundFile.GetExportExtension();
            String[] collisionBounds = Directory.GetFiles(collisionDirectory, "*." + extension);

            // This is a zip file contain bound dictionaries 
            if (collisionBounds.Count() == 0)
            {
                string ibnExtension = RSG.Platform.FileType.BoundsFile.GetExportExtension();
                String[] collisionBoundDictionaries = Directory.GetFiles(collisionDirectory, "*." + ibnExtension + ".zip");

                foreach (String boundDict in collisionBoundDictionaries)
                {
                    // 
                    string collisionSubDirectory = Path.Combine(Path.GetDirectoryName(boundDict), Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(boundDict)));
                    if (!Directory.Exists(collisionSubDirectory))
                        Directory.CreateDirectory(collisionSubDirectory);

                    IEnumerable<String> extractedSubFiles;
                    extractResult = Zip.ExtractAll(boundDict, collisionSubDirectory, true, null, out extractedSubFiles);

                    if (!extractResult)
                    {
                        log.ErrorCtx(LOG_CTX, "Extracting '{0}' to '{1}' failed.",
                            collisionZip, collisionDirectory);
                        return false;
                    }

                    collisionBounds = Directory.GetFiles(collisionSubDirectory, "*." + extension);
                    BoundsProcessing.ProcessBounds(collisionBounds, optimisedBounds, containerExtents, log);
                }
            }
            else
            {
                BoundsProcessing.ProcessBounds(collisionBounds, optimisedBounds, containerExtents, log);
            }

            log.ProfileEnd();

            return true;
        }

#warning LPXO: Move to RSG.Base.Math with hardcoded threholds as default method params.
        /// <summary>
        /// A simple round function based on hardcoded thresholds.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static float RoundNearest(float input)
        {
            const float kMinThreshold = 0.001f;
            const float kMaxThreshold = 0.999f;

            float dec = Math.Abs(input - (int)input);
            if (dec < kMinThreshold || dec > kMaxThreshold)
            {
                return (float)Math.Round(input);
            }

            return (float)input;
        }
        #endregion // Public Static Methods

        #region Private Static Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="collisionBounds"></param>
        /// <param name="optimisedBounds"></param>
        /// <param name="containerExtents"></param>
        private static void ProcessBounds(String[] collisionBounds, ICollection<OptimisedBound> optimisedBounds, BoundingBox3f containerExtents, IUniversalLog log)
        {
            foreach (String collisionBound in collisionBounds)
            {
                BNDFile boundFile = BNDFile.Load(collisionBound);
                BoundsProcessing.ProcessBound(boundFile.BoundObject, boundFile.FileName, optimisedBounds, containerExtents, log, BNDFile.CollisionType.Default);

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="boundObj"></param>
        /// <param name="fileName"></param>
        /// <param name="optimisedBounds"></param>
        /// <param name="containerExtents"></param>
        private static void ProcessBound(BoundObject boundObj, String fileName, ICollection<OptimisedBound> optimisedBounds, BoundingBox3f containerExtents, IUniversalLog log, BNDFile.CollisionType collisionType)
        {
            if (boundObj is Composite)
            {
                foreach (CompositeChild cc in (boundObj as Composite).Children)
                {
                    BoundsProcessing.ProcessBound(cc.BoundObject, fileName, optimisedBounds, containerExtents, log, cc.CollisionType);
                }
            }
            else if (boundObj is BVH)
            {
                BVH bvh = boundObj as BVH;
                BVH sortedBvh = null;

                containerExtents.Expand(bvh.Bounds);

                // Ignore procedural collision meshes.
                if (collisionType == BNDFile.CollisionType.MaterialOnly)
                    return;

                if (BoundsProcessing.Sort(bvh, ref sortedBvh))
                {
                    OptimisedBound newOptimisedBound = new OptimisedBound(fileName, sortedBvh);
                    optimisedBounds.Add(newOptimisedBound);
                }
                else
                {
                    log.WarningCtx(LOG_CTX, "No bound polys found in {0} or a part of a composite within that file.  Ignoring.", fileName);
                }
            }
            else
            {
                log.WarningCtx(LOG_CTX, "Unhandled bound type type found in {0}", fileName);
            }

        }

        /// <summary>
        /// Re-orders the list of primitives based on x- then y-coordinates.
        /// </summary>
        private static bool Sort(BVH bvh, ref BVH sortedBvh)
        {
            //TODO:  A lot of assumptions here...
            if (bvh == null)
                return false;

            List<BVHPrimitive> primitiveList = bvh.Primitives.ToList();
            IEnumerable<BVHPrimitive> triangles = primitiveList.Where(primitive => (primitive is BVHTriangle));

            if (triangles.Count() == 0)
                return false;

            var sorted = triangles.OrderBy(primitive =>
            {
                BVHTriangle tri = primitive as BVHTriangle;
                BoundingBox3f box = tri.GetBoundingBox(bvh.Verts);
                return box.Min.X;
            }).ThenBy(primitive =>
            {
                BVHTriangle tri = primitive as BVHTriangle;
                BoundingBox3f box = tri.GetBoundingBox(bvh.Verts);
                return box.Min.Y;
            });

            sortedBvh = new BVH(bvh.Verts, bvh.NumPerVertAttributes, bvh.CentreOfGravity, bvh.Margin, bvh.Materials,
                bvh.MaterialColours, sorted.ToArray(), bvh.ReadEdgeNormals,
                bvh.SecondSurfaceDisplacements, bvh.SecondSurfaceMaxHeight, bvh.ShrunkVerts);

            return true;
        }
        #endregion // Private Static Methods
    }
}
