﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.Pipeline.Services.AssetPack
{

    /// <summary>
    /// Asset Pack ZipArchive class; used when interacting with the AssetPackSchedulerBuilder.
    /// </summary>
    /// <see cref="AssetPackScheduleBuilder" />
    /// 
    public class AssetPackZipArchive
    {
        public AssetPackZipArchive(string pathname, DuplicateBehaviour dupeBehaviour = DuplicateBehaviour.Error)
        {
            Pathname = pathname;
            DupeBehaviour = dupeBehaviour;

            directories_ = new List<AssetPackDirectory>();
            files_ = new List<AssetPackFile>();
        }

        public void AddDirectory(string pathname, string destination = null, string wildcard = null, bool recursive = false)
        {
            AssetPackDirectory newDirectory = new AssetPackDirectory(pathname, destination, wildcard, recursive);
            directories_.Add(newDirectory);
        }

        public void AddFile(string pathname, string destination = null)
        {
            AssetPackFile newFile = new AssetPackFile(pathname, destination);
            files_.Add(newFile);
        }

        public void RemoveFile(string pathname)
        {
            AssetPackFile fileToRemove = files_.FirstOrDefault(file => string.Compare(file.Pathname, pathname, true) == 0);
            if (fileToRemove != null)
                files_.Remove(fileToRemove);
        }

        internal XElement ToXElement()
        {
            XElement zipArchiveElement = new XElement("ZipArchive", new XAttribute("path", this.Pathname), new XAttribute("dupeBehaviour", AssetPackScheduleFile.ResolveDupeBehaviourEnum(this.DupeBehaviour)));
            if(directories_.Count > 0)
                zipArchiveElement.Add(directories_.Select(directory => directory.ToXElement()));
            if(files_.Count > 0)
                zipArchiveElement.Add(files_.Select(file => file.ToXElement()));

            return zipArchiveElement;
        }

        public string Pathname { get; private set; }
        public DuplicateBehaviour DupeBehaviour { get; private set; }

        private List<AssetPackDirectory> directories_;
        private List<AssetPackFile> files_;
    }

} // RSG.Pipeline.Services.AssetPack
