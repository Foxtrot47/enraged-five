﻿using System;
using System.IO;
using System.Threading;

namespace RSG.Pipeline.Services.Animation
{

    /// <summary>
    /// Static methods for animation executable and handling directories.
    /// </summary>
    /// DHM: its crazy that we have to do this but we've seen issues with creating directories
    /// when using remote packets in XGE.
    /// 
    public static class DirectoryServices
    {
        #region Constants
        private static int MS_SLEEP = 10;
        private static int ATTEMPTS = 10;
        #endregion // Constants

        /// <summary>
        /// Really try to create a directory.
        /// </summary>
        /// <param name="directory"></param>
        /// <returns></returns>
        public static bool CreateDirectory(String directory)
        {
            int attemptCounter = 0;

            while (!Directory.Exists(directory) && attemptCounter < ATTEMPTS)
            {
                Directory.CreateDirectory(directory);

                ++attemptCounter;
                Thread.Sleep(TimeSpan.FromMilliseconds(MS_SLEEP));
            }

            return (Directory.Exists(directory));
        }
    }

} // RSG.Pipeline.Services.Animation namespace
