﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.Pipeline.Services.BoundsProcessor
{
    /// <summary>
    /// Static configuration data that is optionally read from the command line (with sensible defaults)
    /// </summary>
    public class BoundsProcessorOptions
    {
        public BoundsProcessorOptions()
        {
            MaxBVHDataSize = defaultMaxBVHDataSize_;
            MaxStandardMapBVHDataSize = defaultMaxStandardMapBVHDataSize_;
            MaxHighDetailMapBVHDataSize = defaultMaxHighDetailMapBVHDataSize_;
            MaxStandardMapCompositeDataSize = defaultMaxStandardMapCompositeDataSize_;
            MaxHighDetailMapCompositeDataSize = defaultMaxHighDetailMapCompositeDataSize_;
            MaxMaterialColourPaletteSize = defaultMaxMaterialColourPaletteSize_;
            VertexWeldTolerance = defaultVertexWeldTolerance_;
            ColinearTriangleTolerance = defaultColinearTriangleTolerance_;
            MaxStandardBoundsXY = maxStandardBoundsXY_;
            MaxHighDetailBoundsXY = maxHighDetailBoundsXY_;
            MinimumPrimitiveCount = minPrimitiveCount_;
            Parallelise = parallelise_;
            Profile = profile_;
            ComplexMapCollisionSplitting = complexMapCollisionSplitting_;
            SplitAlternativesToConsider = splitAlternativesToConsider_;
            SplitDivergenceToConsider = splitDivergenceToConsider_;
            MaxNonBVHPropVolume = maxNonBVHPropVolume_;
            MaxNonBVHPropPrimitiveCount = maxNonBVHPropPrimitiveCount_;
            SmallestPermittedBoxDimension = smallestPermittedBoxDimension_;
            ArchiveOutput = archiveOutput_;
            MaxBVHBoundsXY = maxBVHBoundsXY_;
        }

        public XElement ToXElement()
        {
            return new XElement("Options",
                new XElement("MaxStandardCompositeSizeXY", MaxBVHBoundsXY),
                new XElement("MaxHighDetailCompositeSizeXY", MaxBVHBoundsXY),
                new XElement("MaxBVHDataSize", MaxBVHDataSize),
                new XElement("MaxStandardMapBVHDataSize", MaxStandardMapBVHDataSize),
                new XElement("MaxHighDetailMapBVHDataSize", MaxHighDetailMapBVHDataSize),
                new XElement("MaxStandardMapCompositeDataSize", MaxStandardMapCompositeDataSize),
                new XElement("MaxHighDetailMapCompositeDataSize", MaxHighDetailMapCompositeDataSize),
                new XElement("MaxMaterialColourPaletteSize", MaxMaterialColourPaletteSize),
                new XElement("MinPrimitivesPerComposite", MinimumPrimitiveCount),
                new XElement("Parallelise", Parallelise),
                new XElement("Profile", Profile),
                new XElement("ComplexMapCollisionSplitting", ComplexMapCollisionSplitting),
                new XElement("SplitAlternativesToConsider", SplitAlternativesToConsider),
                new XElement("SplitDivergenceToConsider", SplitDivergenceToConsider),
                new XElement("MaxNonBVHPropVolume", MaxNonBVHPropVolume),
                new XElement("MaxNonBVHPropPrimitiveCount", MaxNonBVHPropPrimitiveCount),
                new XElement("ArchiveOutput", ArchiveOutput));
        }

        public int MaxBVHDataSize { get; set; }
        public int MaxStandardMapBVHDataSize { get; set; }
        public int MaxHighDetailMapBVHDataSize { get; set; }
        public int MaxStandardMapCompositeDataSize { get; set; }
        public int MaxHighDetailMapCompositeDataSize { get; set; }
        public int MaxMaterialColourPaletteSize { get; set; }
        public float VertexWeldTolerance { get; set; }
        public float ColinearTriangleTolerance { get; set; }
        public float MaxStandardBoundsXY { get; set; }
        public float MaxHighDetailBoundsXY { get; set; }
        public int MinimumPrimitiveCount { get; set; }
        public bool Parallelise { get; set; }
        public bool Profile { get; set; }
        public bool ComplexMapCollisionSplitting { get; set; }
        public int SplitAlternativesToConsider { get; set; }
        public float SplitDivergenceToConsider { get; set; }
        public float MaxNonBVHPropVolume { get; set; }
        public int MaxNonBVHPropPrimitiveCount { get; set; }
        public float SmallestPermittedBoxDimension { get; set; }
        public bool ArchiveOutput { get; set; }
        public float MaxBVHBoundsXY { get; set; }

        private static readonly int defaultMaxBVHDataSize_ = 128 * 1024;
        private static readonly int defaultMaxStandardMapBVHDataSize_ = 128 * 1024;
        private static readonly int defaultMaxHighDetailMapBVHDataSize_ = 128 * 1024;
        private static readonly int defaultMaxStandardMapCompositeDataSize_ = 512 * 1024;
        private static readonly int defaultMaxHighDetailMapCompositeDataSize_ = 512 * 1024;
        private static readonly int defaultMaxMaterialColourPaletteSize_ = 28;
        private static readonly float defaultVertexWeldTolerance_ = 0.001f;
        private static readonly float defaultColinearTriangleTolerance_ = 0.000001f;
        private static readonly float maxStandardBoundsXY_ = 150.0f;
        private static readonly float maxHighDetailBoundsXY_ = 150.0f;
        private static readonly int minPrimitiveCount_ = 1500;
        private static readonly bool parallelise_ = true;
        private static readonly bool profile_ = false;
        private static readonly bool complexMapCollisionSplitting_ = true;
        private static readonly int splitAlternativesToConsider_ = 50;
        private static readonly float splitDivergenceToConsider_ = 0.015f;
        private static readonly float maxNonBVHPropVolume_ = 1.0f;
        private static readonly int maxNonBVHPropPrimitiveCount_ = 64;
        private static readonly float smallestPermittedBoxDimension_ = 0.0199f;// ~2cm (with room for some rounding error)
        private static readonly bool archiveOutput_ = true;
        private static readonly float maxBVHBoundsXY_ = 500.0f;
    }
}
