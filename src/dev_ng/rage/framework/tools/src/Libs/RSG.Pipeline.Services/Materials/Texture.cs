﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using RSG.Base.Configuration;
using RSG.SceneXml.Material;

namespace RSG.Pipeline.Services.Materials
{

    /// <summary>
    /// Wraps the SceneXml material source textures with the export logic thats
    /// implemented in ExportTexture and rexMax; i.e. handling the diff+alpha
    /// etc. based on the shader preset.
    /// </summary>
    public class Texture
    {
        #region Properties
        /// <summary>
        /// Source texture absolute filenames.
        /// </summary>
        public IEnumerable<String> SourceTextures
        {
            get;
            private set;
        }

        /// <summary>
        /// Source texture filename(s) for rexMax or TextureExport.
        /// </summary>
        /// I.e. [diffuse]+[alpha]
        public String ExportSourceData
        {
            get;
            private set;
        }

        /// <summary>
        /// Export texture absolute filename.
        /// </summary>
        public String ExportTexture
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Static constructor.
        /// </summary>
        public Texture(IEnumerable<String> sources, String export, String exportSourceData)
        {
            this.SourceTextures = sources;
            this.ExportTexture = export;
            this.ExportSourceData = exportSourceData;
        }
        #endregion // Constructor(s)

        #region Static Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="presets"></param>
        /// <param name="material"></param>
        /// <param name="outputDirectory"></param>
        /// <param name="exportFormatExt"></param>
        /// <returns></returns>
        public static IEnumerable<Texture> Create(IDictionary<String, ShaderPreset> presets, 
            MaterialDef material, String outputDirectory, String exportFormatExt)
        {
            List<Texture> textures = new List<Texture>();
            Debug.Assert(null != material, "Invalid material!");
            if (null == material)
                return (textures);

            if (material.HasSubMaterials)
            {
                foreach (MaterialDef submat in material.SubMaterials)
                    textures.AddRange(Create(presets, submat, outputDirectory, exportFormatExt));
            }
            else
            {
                // This will ignore "Standard" materials (for now).
                if (String.IsNullOrWhiteSpace(material.Preset))
                    return (textures);
                Debug.Assert(presets.ContainsKey(material.Preset),
                    String.Format("Preset '{0}' not found in preloaded list.", material.Preset));
                if (0 == material.TextureCount)
                    return (textures);

                ShaderPreset preset = presets[material.Preset];
                if (preset.IsDiffuseAlphaTexturesMerged())
                {
                    // Here we need to pluck the Diffuse and Alpha; and create Texture objects
                    // for everything else as normal.
                    IDictionary<TextureTypes, TextureDef> sourceTextures = new Dictionary<TextureTypes, TextureDef>();
                    Array.ForEach(material.Textures, t => {
                        if (!sourceTextures.ContainsKey(t.Type)) sourceTextures.Add(t.Type, t);
                    });

                    if (sourceTextures.ContainsKey(TextureTypes.DiffuseMap) &&
                        sourceTextures.ContainsKey(TextureTypes.DiffuseAlpha))
                    {
                        TextureDef diffuseTexture = sourceTextures[TextureTypes.DiffuseMap];
                        TextureDef alphaTexture = sourceTextures[TextureTypes.DiffuseAlpha];
                        String exportTextureBasename = String.Format("{0}{1}",
                            Path.GetFileNameWithoutExtension(diffuseTexture.FilePath),
                            Path.GetFileNameWithoutExtension(alphaTexture.FilePath));
                        String exportTextureFilename = Path.Combine(outputDirectory,
                            exportTextureBasename + exportFormatExt);

                        IEnumerable<String> sourceTextureFilenames = new String[] 
                            { diffuseTexture.FilePath, alphaTexture.FilePath };
                        String exportSourceData = String.Format("{0}+{1}",
                            diffuseTexture.FilePath, alphaTexture.FilePath);
                        Texture texture = new Texture(sourceTextureFilenames, exportTextureFilename, exportSourceData);
                        textures.Add(texture);
                    }
                    else if (sourceTextures.ContainsKey(TextureTypes.DiffuseMap))
                    {
                        TextureDef texture = sourceTextures[TextureTypes.DiffuseMap];
                        Texture diffuseTexture = Create(texture, outputDirectory, exportFormatExt);
                        textures.Add(diffuseTexture);
                    }
                    else if (sourceTextures.ContainsKey(TextureTypes.DiffuseAlpha))
                    {
                        TextureDef texture = sourceTextures[TextureTypes.DiffuseAlpha];
                        Texture alphaTexture = Create(texture, outputDirectory, exportFormatExt);
                        textures.Add(alphaTexture);
                    }

                    // Scoop up non-Diffuse and non-Alpha textures.
                    foreach (TextureDef texture in material.Textures)
                    {
                        if (sourceTextures.ContainsKey(TextureTypes.DiffuseMap) ||
                            sourceTextures.ContainsKey(TextureTypes.DiffuseAlpha))
                            continue;

                        Texture exportTexture = Create(texture, outputDirectory, exportFormatExt);
                        textures.Add(exportTexture);
                    }
                }
                else
                {
                    // This is the simple case of creating export textures for each
                    // source textures.
                    foreach (TextureDef texture in material.Textures)
                    {
                        Texture exportTexture = Create(texture, outputDirectory, exportFormatExt);
                        textures.Add(exportTexture);
                    }
                }
            }
            return (textures);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sourceTexture"></param>
        /// <param name="outputDirectory"></param>
        /// <param name="exportFormatExt"></param>
        /// <returns></returns>
        public static Texture Create(TextureDef texture, String outputDirectory, 
            String exportFormatExt)
        {
            String basename = Path.GetFileNameWithoutExtension(texture.FilePath);
            IEnumerable<String> sourceTextures = new String[] { texture.FilePath };
            String exportTextureFilename = Path.Combine(outputDirectory, basename + exportFormatExt);

            return (new Texture(sourceTextures, exportTextureFilename, texture.FilePath));
        }
        #endregion // Static Controller Methods

        #region Object Overridden Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public bool Equals(Object o)
        {
            if (!(o is Texture))
                return (false);

            return (((Texture)o).ExportTexture.Equals(this.ExportTexture, StringComparison.OrdinalIgnoreCase));
        }
        #endregion // Object Overridden Methods
    }

    /// <summary>
    /// 
    /// </summary>
    public class SameExportTexture : EqualityComparer<Texture>
    {
        public override bool Equals(Texture t1, Texture t2)
        {
            return (0 == String.Compare(t1.ExportTexture, t2.ExportTexture, true));
        }

        public override int GetHashCode(Texture t)
        {
            return (t.GetHashCode());
        }
    }

} // RSG.Pipeline.Services.Materials namespace
