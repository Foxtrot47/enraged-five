﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using RSG.Base.Collections;
using RSG.Base.Extensions;
using IB = RSG.Interop.Incredibuild;
using XGE = RSG.Interop.Incredibuild.XGE;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Core;
using RSG.Model.Statistics.Platform;

namespace RSG.Pipeline.Services.Platform
{

    /// <summary>
    /// Ragebuilder interface methods.
    /// </summary>
    /// DHM: aim of this class is to make everything internal so we don't
    /// access 'Ragebuilder' directly.  E.g. see Rpf utility class.
    public static class Ragebuilder
    {
        #region Constants
        private static readonly String LOG_CTX = "Ragebuilder";
        private static readonly String CONTEXT = @"Converting (?<src>[:\.\\/\w]*) to (?<dst>[:\.\\/\w]*) \[(?<platform>[\w]+)\]";
        private static readonly Regex REGEX_CONTEXT = new Regex(CONTEXT,
            RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase);
        private static readonly Regex REGEX_FATAL_ERROR = new Regex("Fatal Error: (.*)",
            RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase);
        private static readonly Regex REGEX_ERROR = new Regex("Error: (.*)",
            RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase);
        private static readonly Regex REGEX_WARN = new Regex("Warning: (.*)",
            RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase);

        // Ragebuilder Statistic Parsing Regular Expressions
        private static readonly Regex REGEX_STAT = new Regex("STAT: (?<stat>.*)",
            RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase);
        private static readonly Regex REGEX_STAT_PHYSICAL_BUCKET = new Regex(
            @"(?<resource>[:\.\\/\w\+_]*) physical: Bucket (?<index>[\d]+) size (?<size>[\d]+)\/(?<capacity>[\d]+) .*",
            RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase);
        private static readonly Regex REGEX_STAT_VIRTUAL_BUCKET = new Regex(
            @"(?<resource>[:\.\\/\w\+_]*) virtual: Bucket (?<index>[\d]+) size (?<size>[\d]+)\/(?<capacity>[\d]+) .*",
            RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase);

        /// <summary>
        /// Default packet size in kilobytes (for when no option is available).
        /// </summary>
        public static readonly int DEFAULT_PACKET_SIZE = 8192;

        /// <summary>
        /// Default maximum number of resources to convert in a single packet.
        /// </summary>
        public static readonly int DEFAULT_PACKET_MAX_FILES = 10;
        #endregion // Constants

        #region Enums
        /// <summary>
        /// Conversion types that Ragebuilder supports.
        /// </summary>
        private enum ConversionType
        {
            DirectoryToDirectory,
            FileToFile
        }
        #endregion Enums

        #region Static Member Data
        /// <summary>
        /// Dictionary storing packet counts for conversion tasks.
        /// </summary>
        static IDictionary<ITarget, int> m_TargetConvertTaskCount = new Dictionary<ITarget, int>();
        #endregion // Static Member Data

        #region Static Controller Methods
        /// <summary>
        /// Create RagePackets for each process; collapsing into packets limited
        /// by our parameters.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processor"></param>
        /// <param name="processes"></param>
        /// <param name="target"></param>
        /// <param name="packets"></param>
        public static void CreateXGEPacketsForRagebuilderProcesses(IEngineParameters param,
            IProcessor processor, IEnumerable<IProcess> processes, ITarget target,
            ICollection<RagePacket> packets)
        {
            ICollection<RagePacket> tempPackets = new List<RagePacket>();
            int packetFileLimit = processor.Parameters.ContainsKey(Constants.RageProcess_XGEFileLimit) ?
                (int)processor.Parameters[Constants.RageProcess_XGEFileLimit] : DEFAULT_PACKET_MAX_FILES;
            long packetSizeLimit = processor.Parameters.ContainsKey(Constants.RageProcess_XGEPacketSize) ?
                (int)processor.Parameters[Constants.RageProcess_XGEPacketSize] : DEFAULT_PACKET_SIZE * 1024;
            long packetSize = 0;
            int fileCount = 0;

            RagePacket rp = new RagePacket(target);
            foreach (IProcess process in processes)
            {
                IContentNode input = process.Inputs.First();
                IContentNode output = process.Outputs.First();

                // Input validation.     
                if ((input is Content.File) && (output is Content.Asset))
                {
                    packetSize += RagePacket.GetDataSize(process);
                    fileCount += RagePacket.GetInputCount(process);

                    if (((packetSize > packetSizeLimit) || fileCount >= packetFileLimit) && rp.Processes.Count > 0)
                    {
                        rp = new RagePacket(target);
                        packetSize = 0;
                        fileCount = 0;
                    }
                    rp.Processes.Add(process);

                    // Only add packet if it contains data; and hasn't already 
                    // been added.
                    if (!packets.Contains(rp))
                        packets.Add(rp);
                }
                else if ((input is Content.Directory) && (output is Content.Directory))
                {
                    RagePacket directoryRagePacket = new RagePacket(target);
                    directoryRagePacket.Processes.Add(process);
                    packets.Add(directoryRagePacket);
                }
                else
                {
                    param.Log.WarningCtx(LOG_CTX, "Unsupported conversion type: {0} => {1}.",
                        input.GetType().ToString(), output.GetType().ToString());
                }
            }
        }

        /// <summary>
        /// Create an XGE task object for a series of Rage conversion processes.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="target"></param>
        /// <param name="processes"></param>
        /// <param name="tool"></param>
        /// <returns></returns>
        public static XGE.ITask GetXgeConvertTask(IUniversalLog log, ITarget target, 
            IEnumerable<IProcess> processes, XGE.ITool tool)
        {
            if (!m_TargetConvertTaskCount.ContainsKey(target))
                m_TargetConvertTaskCount.Add(target, 0);

            ConversionType conversionType = ConversionType.FileToFile;
            if (processes.Count() == 1 &&
               processes.First().Inputs.Count() == 1 && processes.First().Inputs.First() is Content.Directory &&
               processes.First().Outputs.Count() == 1 && processes.First().Outputs.First() is Content.Directory)
            {
                // Conversions of one directory to another are special as they require a call to convert2directory rather than convert2
                conversionType = ConversionType.DirectoryToDirectory;
            }

            String taskName;
            if (conversionType == ConversionType.FileToFile)
            {
                long dataSize = RagePacket.GetDataSize(processes); 
                int fileCount = RagePacket.GetInputCount(processes);
                taskName = String.Format("Rage Packet {0} [{1} {2} files {3}KB]",
                    m_TargetConvertTaskCount[target]++, target.Platform, fileCount, dataSize / 1024);
            }
            else
            {
                taskName = String.Format("Rage Packet {0} [{1} (directory conversion)]", m_TargetConvertTaskCount[target]++, target.Platform);
            }

            XGE.ITaskJob task = new XGE.TaskJob(taskName);
            try
            {
                String logfileDirectory = Path.Combine(XGEFactory.GetTempDirectory(target.Branch), "RagebuilderLogs");
                String logfile = Path.Combine(logfileDirectory, String.Format("Convert_{0}_Ragebuilder.log", taskName.Replace(" ", "_")));
                String xgeConvertScript = target.Environment.Subst(target.GetRagebuilderConvertXGEScript());
                String requireStr = String.Format("require(\"{0}\")", xgeConvertScript.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar));
                String xgeArguments = target.GetRagebuilderConvertXGEArguments();
                String platform = RSG.Platform.PlatformUtils.PlatformToRagebuilderPlatform(target.Platform);
                if (!Directory.Exists(logfileDirectory))
                    Directory.CreateDirectory(logfileDirectory);

                // Create Ragebuilder script source file.
                String packetName = Path.ChangeExtension(taskName.Replace(' ', '_'), "rbs");
                String packetFilename = Path.Combine(XGEFactory.GetTempDirectory(target.Branch), "RagebuilderPackets", packetName);
                String packetDirectory = Path.GetDirectoryName(packetFilename);
                if (!Directory.Exists(packetDirectory))
                    Directory.CreateDirectory(packetDirectory);

                using (TextWriter writer = new StreamWriter(packetFilename))
                {
                    writer.WriteLine(requireStr);
                    writer.WriteLine("print( \"TASK: {0}\" )", taskName.Replace(' ', '_'));
                    if (conversionType == ConversionType.FileToFile)
                    {
                        foreach (IProcess process in processes)
                        {
                            Content.IFilesystemNode input = ((Content.IFilesystemNode)process.Inputs.First());
                            Content.IFilesystemNode output = ((Content.IFilesystemNode)process.Outputs.First());
                            String inputPathStr = input.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                            String outputPathStr = output.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                            String configPathStr = target.Branch.Project.Config.ToolsConfig.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                            AddProcessInputsAndOutputsToTask(process, ref task);

                            writer.WriteLine("convert2(\"{0}\", \"{1}\", \"{2}\", build, materials, procedurals, shader, shaderdb, options, \"{3}\", core_assets, assets, metadata_definitions)",
                                inputPathStr, outputPathStr, platform, configPathStr);

                            // Define the XGE task for the IProcess.
                            process.Parameters.Add(Constants.ProcessXGE_TaskName, taskName.Replace(' ', '_'));
                            process.Parameters.Add(Constants.ProcessXGE_Task, task);
                        }
                    }
                    else
                    {
                        IProcess process = processes.First();
                        Content.Directory inputDirectory = ((Content.Directory)process.Inputs.First());
                        Content.Directory outputDirectory = ((Content.Directory)process.Outputs.First());
                        String inputDirectoryStr = inputDirectory.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                        String inputSearchPattern = inputDirectory.Wildcard;
                        String outputDirectoryStr = outputDirectory.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                        String configPathStr = target.Branch.Project.Config.ToolsConfig.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                        String resourcePrefix = String.Empty;
                        if (process.Parameters.ContainsKey(Constants.RageProcess_ResourcePrefix))
                            resourcePrefix = (String)process.Parameters[Constants.RageProcess_ResourcePrefix];

                        AddProcessInputsAndOutputsToTask(process, ref task);

                        writer.WriteLine("convert2directory(\"{0}\", \"{1}\", \"{2}\", \"{3}\", build, materials, procedurals, shader, shaderdb, options, \"{4}\", core_assets, assets, metadata_definitions, \"{5}\")",
                            inputDirectoryStr, inputSearchPattern, outputDirectoryStr, platform, configPathStr, resourcePrefix);

                        // Define the XGE task for the IProcess.
                        process.Parameters.Add(Constants.ProcessXGE_TaskName, taskName.Replace(' ', '_'));
                        process.Parameters.Add(Constants.ProcessXGE_Task, task);
                    }
                }
                task.Caption = taskName;
                task.SourceFile = packetFilename;
                task.Parameters = String.Format("{0} {1} -logfile {2}",
                    packetFilename, xgeArguments, logfile);
                task.Tool = tool;
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception creating XGE task: {0}.", taskName);
            }

            return (task);
        }

        /// <summary>
        /// Parse Ragebuilder output errors, warnings and context into a log.
        /// </summary>
        /// <param name="branch">Branch.</param>
        /// <param name="log">Target log.</param>
        /// <param name="logfile">Ragebuilder log filename.</param>
        /// <param name="captureStatsOutput"></param>
        /// <param name="statsSize"></param>
        /// <param name="hasErrors"></param>
        public static void ParseRagebuilderOutput(IBranch branch, IUniversalLog log,
            String logfile, bool captureStatsOutput, long rollingStatsSize, out bool hasErrors)
        {
            log.ProfileCtx(LOG_CTX, "Parsing: {0}.", logfile);
            hasErrors = false;
            try
            {
                if (File.Exists(logfile))
                {
                    String resourceStatsLogFilename = Path.Combine(branch.Project.Config.ToolsLogs,
                        ResourceStatsLogFile.RESOURCE_STATS_LOGFILE_FILENAME);
                    ResourceStatsLogFile resourceStatsLog = new ResourceStatsLogFile(
                        resourceStatsLogFilename, rollingStatsSize);
                    using (StreamReader fs = new StreamReader(logfile))
                    {
                        String contextSource = String.Empty;
                        String contextDest = String.Empty;
                        String contextPlatform = String.Empty;
                        String line = String.Empty;
                        bool outputStatus = true;
                        while (null != (line = fs.ReadLine()))
                        {
                            if (String.IsNullOrWhiteSpace(line))
                                continue;

                            ICollection<ResourceBucketStat> stats = new List<ResourceBucketStat>();
                            outputStatus &= ParseRagebuilderOutputLine(log,
                                stats, line, captureStatsOutput, ref contextSource, 
                                ref contextDest, ref contextPlatform);
                            resourceStatsLog.AddResourceBucketStats(stats);
                        }
                        hasErrors = !outputStatus;
                    }
                }
                else
                {
                    log.WarningCtx(LOG_CTX, "Ragebuilder log: {0} does not exist.", logfile);
                }
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception parsing Ragebuilder log: {0}.", logfile);
            }
            finally
            {
                log.ProfileEnd();
            }
        }

        /// <summary>
        /// Parse Ragebuilder output errors, warnings and context into a log.
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="log"></param>
        /// <param name="logData"></param>
        /// <param name="captureStatsOutput"></param>
        /// <param name="rollingStatsSize"></param>
        /// <param name="hasErrors"></param>
        public static void ParseRagebuilderOutput(IBranch branch, IUniversalLog log,
            IEnumerable<String> logData, bool captureStatsOutput, long rollingStatsSize, out bool hasErrors)
        {
            hasErrors = false;
            try
            {
                String resourceStatsLogFilename = Path.Combine(branch.Project.Config.ToolsLogs,
                    ResourceStatsLogFile.RESOURCE_STATS_LOGFILE_FILENAME);
                ResourceStatsLogFile resourceStatsLog = new ResourceStatsLogFile(
                    resourceStatsLogFilename, rollingStatsSize);

                String contextSource = String.Empty;
                String contextDest = String.Empty;
                String contextPlatform = String.Empty;
                bool outputStatus = true;
                foreach (String line in logData)
                {
                    if (String.IsNullOrWhiteSpace(line))
                        continue;

                    ICollection<ResourceBucketStat> stats = new List<ResourceBucketStat>();
                    outputStatus &= ParseRagebuilderOutputLine(log, stats, line, 
                        captureStatsOutput, ref contextSource, ref contextDest,
                        ref contextPlatform);
                    resourceStatsLog.AddResourceBucketStats(stats);
                }
                hasErrors = !outputStatus;
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception parsing Ragebuilder output.");
            }
        }
        #endregion // Static Controller Methods

        #region Private Static Methods
        /// <summary>
        /// Add the process input and output filenames to the XGE task.
        /// </summary>
        /// <param name="process"></param>
        /// <param name="task"></param>
        private static void AddProcessInputsAndOutputsToTask(IProcess process, ref XGE.ITaskJob task)
        {
            foreach (IContentNode node in process.Inputs)
            {
                if ((node is Content.IFilesystemNode) && !(node is Content.Directory))
                {
                    Content.IFilesystemNode fsNode = (node as Content.IFilesystemNode);
                    task.InputFiles.Add(fsNode.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar));
                }
            }

            foreach (IContentNode node in process.Outputs)
            {
                if ( (node is Content.IFilesystemNode) && !(node is Content.Directory) )
                {
                    Content.IFilesystemNode fsNode = (node as Content.IFilesystemNode);
                    task.OutputFiles.Add(fsNode.AbsolutePath.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar));
                }
            }
        }

        /// <summary>
        /// Parse a single line of Ragebuilder output.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="resourceStats"></param>
        /// <param name="line"></param>
        /// <param name="captureStatsOutput"></param>
        /// <param name="contextSource"></param>
        /// <param name="contextDestination"></param>
        /// <param name="contextPlatform"></param>
        /// <returns>true for errors; false otherwise.</returns>
        private static bool ParseRagebuilderOutputLine(IUniversalLog log,
            ICollection<ResourceBucketStat> resourceStats,
            String line, bool captureStatsOutput, ref String contextSource,
            ref String contextDestination, ref String contextPlatform)
        {
            Match match = REGEX_CONTEXT.Match(line);
            if (match.Success)
            {
                contextSource = match.Groups["src"].Value;
                contextDestination = match.Groups["dst"].Value;
                contextPlatform = match.Groups["platform"].Value;
                return (false);
            }

            String lowerLine = line.ToLower();
            if (lowerLine.StartsWith("fatal error"))
            {
                match = REGEX_FATAL_ERROR.Match(line);
                if (match.Success)
                {
                    String message = Log.EscapeMessage(match.Value);
                    if (!String.IsNullOrEmpty(contextSource) && !String.IsNullOrEmpty(contextDestination))
                        log.ErrorCtx(LOG_CTX, "Fatal error converting asset: {0} to {1} for {2}.",
                            contextSource, contextDestination, contextPlatform);
                    log.ErrorCtx(LOG_CTX, message);
                    return (true);
                }
            }

            if (lowerLine.StartsWith("error"))
            {
                match = REGEX_ERROR.Match(line);
                if (match.Success)
                {
                    String message = Log.EscapeMessage(match.Value);
                    if (!String.IsNullOrEmpty(contextSource) && !String.IsNullOrEmpty(contextDestination))
                        log.ErrorCtx(LOG_CTX, "Error converting asset: {0} to {1} for {2}.",
                            contextSource, contextDestination, contextPlatform);
                    log.ErrorCtx(LOG_CTX, message);
                    return (true);
                }
            }

            if (lowerLine.StartsWith("warning"))
            {
                match = REGEX_WARN.Match(line);
                if (match.Success)
                {
                    String message = Log.EscapeMessage(match.Value);
                    if (!String.IsNullOrEmpty(contextSource) && !String.IsNullOrEmpty(contextDestination))
                        log.WarningCtx(LOG_CTX, "Warning converting asset: {0} to {1} for {2}.",
                            contextSource, contextDestination, contextPlatform);
                    log.WarningCtx(LOG_CTX, message);
                    return (false);
                }
            }

            // Optionally handle stats output from Ragebuilder.
            if (captureStatsOutput)
            {
                match = REGEX_STAT.Match(line);
                if (match.Success)
                {
                    RSG.Platform.Platform platform =
                        RSG.Platform.PlatformUtils.RagebuilderPlatformToPlatform(contextPlatform);
                    ParseStatsOutput(resourceStats, match, contextSource, 
                        contextDestination, platform);
                    return (false);
                }
            }
            return (false);
        }

        /// <summary>
        /// Parse Ragebuilder statistics output.
        /// </summary>
        /// <param name="resourceStats">Collection for resource bucket stats.</param>
        /// <param name="match"></param>
        /// <param name="source"></param>
        /// <param name="dest"></param>
        /// <param name="platform"></param>
        private static void ParseStatsOutput(ICollection<ResourceBucketStat> resourceStats,
            Match match, String source, String dest, RSG.Platform.Platform platform)
        {
            Debug.Assert(match.Success, "Passed in unsuccessful stat match.");
            if (!match.Success)
                return;

            String statMatch = match.Groups["stat"].Value;
            Match statTypeMatch = REGEX_STAT_PHYSICAL_BUCKET.Match(statMatch);
            if (statTypeMatch.Success)
            {
                String resource = statTypeMatch.Groups["resource"].Value;
                String extension = Path.GetExtension(resource);
                resource = Path.GetFileNameWithoutExtension(resource).ToLower();
                RSG.Platform.FileType filetype = 
                    RSG.Platform.FileTypeUtils.ConvertExtensionToFileType(extension);

                // Create new bucket stat.
                UInt32 id = 0;
                UInt32 size = 0;
                UInt32 capacity = 0;
                UInt32.TryParse(statTypeMatch.Groups["index"].Value, out id);
                UInt32.TryParse(statTypeMatch.Groups["size"].Value, out size);
                UInt32.TryParse(statTypeMatch.Groups["capacity"].Value, out capacity);

                ResourceBucketStat bucketStat = new ResourceBucketStat(platform,
                    filetype, resource, source, dest, id, ResourceBucketType.Physical, 
                    size, capacity);
                resourceStats.Add(bucketStat);
            }
            statTypeMatch = REGEX_STAT_VIRTUAL_BUCKET.Match(statMatch);
            if (statTypeMatch.Success)
            {
                String resource = statTypeMatch.Groups["resource"].Value;
                String extension = Path.GetExtension(resource);
                resource = Path.GetFileNameWithoutExtension(resource).ToLower();
                RSG.Platform.FileType filetype =
                    RSG.Platform.FileTypeUtils.ConvertExtensionToFileType(extension);

                // Create new bucket stat.
                UInt32 id = 0;
                UInt32 size = 0;
                UInt32 capacity = 0;
                UInt32.TryParse(statTypeMatch.Groups["index"].Value, out id);
                UInt32.TryParse(statTypeMatch.Groups["size"].Value, out size);
                UInt32.TryParse(statTypeMatch.Groups["capacity"].Value, out capacity);

                ResourceBucketStat bucketStat = new ResourceBucketStat(platform,
                    filetype, resource, source, dest, id, ResourceBucketType.Virtual, 
                    size, capacity);
                resourceStats.Add(bucketStat);
            }
        }
        #endregion // Private Static Methods
    }

} // RSG.Pipeline.Services.Platform namespace
