﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using RSG.Base.Tasks;
using RSG.Base.Windows.Dialogs;

namespace RSG.Pipeline.Services
{

    /// <summary>
    /// 
    /// </summary>
    public static class ProgressBox
    {
        /// <summary>
        /// Shows the task execution dialog and executes the task on a separate 
        /// thread.
        /// </summary>
        /// <param name="task">Task to execute.</param>
        /// <param name="context">Task context</param>
        /// <param name="cts"></param>
        /// <param name="title"></param>
        /// <returns>Whether the task successfully completed.</returns>
        public static bool Show(ITask task, ITaskContext context, 
            CancellationTokenSource cts, String title)
        {
            TaskExecutionDialog dialog = new TaskExecutionDialog();
            TaskExecutionViewModel vm = new TaskExecutionViewModel(title, task, cts, context);
            vm.AutoCloseOnCompletion = true;
            dialog.DataContext = vm;

            Nullable<bool> result = dialog.ShowDialog();
            return (result.HasValue ? result.Value : true);
        }

        /// <summary>
        /// Show the task execution dialog and executes the action on a separate
        /// thread.
        /// </summary>
        /// <param name="title"></param>
        /// <param name="action"></param>
        public static bool Show(String title, Action<ITaskContext, IProgress<TaskProgress>> action)
        {
            CancellationTokenSource cts = new CancellationTokenSource();
            ITask task = new ActionTask(title, action);
            ITaskContext ctx = new TaskContext();

            TaskExecutionViewModel vm = new TaskExecutionViewModel(title, task,
                cts, ctx);
            TaskExecutionDialog dialog = new TaskExecutionDialog(vm);
            vm.AutoCloseOnCompletion = true;

            Nullable<bool> result = dialog.ShowDialog();
            return (result.HasValue ? result.Value : true);
        }
    }

} // RSG.Pipeline.Services namespace
