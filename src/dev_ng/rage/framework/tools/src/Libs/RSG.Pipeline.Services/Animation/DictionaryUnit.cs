﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace RSG.Pipeline.Services.Animation
{
    /// <summary>
    /// A conatiner for an animation dictionary and any additional animations/clips that we want to include 
    /// from other locations.
    /// </summary>
    public sealed class DictionaryUnit :
        IDictionaryUnit
    {
        #region Fields
        private String _Dictionary;

        /// <summary>
        /// Root name for dicitonary item
        /// </summary>
        private readonly String m_Rootname;
        #endregion

        #region Properties
        /// <summary>
        /// Root namespace (corresponds to RPF output)
        /// </summary>
        public String Rootname
        {
            get { return m_Rootname; }
        }

        /// <summary>
        /// Dictionary unit name.  This is used to generate outputname
        /// </summary>
        public String DictionaryName
        {
            get
            {
                return _Dictionary;
            }
            private set
            {
                _Dictionary = value.ToLower();   
            }
        }

        /// <summary>
        /// Path to the main dictionary for the dictionary unit (if there is one).
        /// </summary>
        public String MainDictionaryPath
        {
            get;
            private set;
        }

        /// <summary>
        /// Additional files to be included with the data from the main dictionary.
        /// </summary>
        public IEnumerable<DictionaryItem> Inclusions
        {
            get;
            set;
        }

        /// <summary>
        /// Files to be exlcuded from the main dictionary.
        /// </summary>
        public IEnumerable<String> Exclusions
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public DictionaryUnit(String rootname, String mainDictionarypath )
        {
            Inclusions = new List<DictionaryItem>();
            Exclusions = new List<String>();

            m_Rootname = rootname;
            MainDictionaryPath = mainDictionarypath;
            DictionaryName = Path.GetFileName(MainDictionaryPath);
        }
        #endregion // Constructor(s)

        #region Methods
        /// <summary>
        /// Add additional file and alias.
        /// </summary>
        public void AddInclusion( DictionaryItem dictionaryItem )
        {
            (Inclusions as List<DictionaryItem>).Add(dictionaryItem);
        }

        /// <summary>
        /// Add an exclusion by alias.
        /// </summary>
        public void AddExclusion( String alias )
        {
            (Exclusions as List<String>).Add( alias );
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inUnit"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            DictionaryUnit inUnit = obj as DictionaryUnit;
            if (inUnit == null)
                return false;

            if (base.Equals(obj))
                return true;

            if (this.DictionaryName == inUnit.DictionaryName)
            {
                return true;
            }
            return false;
        }
        #endregion // Methods
    }

    /// <summary>
    /// Container class for a dictionary inclusion 
    /// </summary>
    public class DictionaryItem
    {
        #region Fields
        /// <summary>
        /// Path to source anim/clip pairing
        /// </summary>
        private readonly String m_Path;

        /// <summary>
        /// Anim/clip basename
        /// </summary>
        private readonly String m_Basename;

        /// <summary>
        /// Anim/clip override name
        /// </summary>
        private readonly String m_OverrideName;
        #endregion // Fields

        #region Properties
        /// <summary>
        /// Path to source anim/clip pairing
        /// </summary>
        public String Path
        {
            get { return m_Path; }
        }

        /// <summary>
        /// Anim/clip basename
        /// </summary>
        public String Basename
        {
            get { return m_Basename; }
        }

        /// <summary>
        ///  Anim/clip override name
        /// </summary>
        public String OverrideName
        {
            get { return m_OverrideName; }
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public DictionaryItem( String path, String basename, String overridename )
        {
            m_Path = path;
            m_Basename = basename;
            m_OverrideName = overridename;
        }
        #endregion // Constructor(s)

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="inUnit"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            DictionaryItem inItem = obj as DictionaryItem;
            if(inItem == null)
                return false;

            if (base.Equals(obj))
                return true;

            if (this.Path == inItem.Path &&
                this.Basename == inItem.Basename &&
                this.OverrideName == inItem.OverrideName)
            {
                return true;
            }
            return false;
        }
        #endregion

    }
}
