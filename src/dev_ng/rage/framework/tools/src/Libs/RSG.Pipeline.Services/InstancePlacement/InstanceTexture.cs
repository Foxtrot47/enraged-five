﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

using RSG.Base.Logging;
using RSG.Base.Logging.Universal;

using RSG.Base.Math;

namespace RSG.Pipeline.Services.InstancePlacement
{
    /// <summary>
    /// Class that stores a colour and a texture index.
    /// When we get the list of pixels at a given location we need to know
    /// what image that pixel came from so we can sample the data texture as well.
    /// </summary>
    public class ColorReference
    {
        public Color Color;
        public int TextureIndex;

        public ColorReference(Color col, int textureIndex)
        {
            Color = col;
            TextureIndex = textureIndex;
        }
    }

    public enum InstanceTextureChannel
    {
        CHANNEL_SCALE,      // R Channel
        CHANNEL_DENSITY,    // G Channel
        CHANNEL_UNUSED_1,   // B Channel
        CHANNEL_UNUSED_2    // A Channel
    }

    /// <summary>
    /// Class that defines a texture "layer".
    /// Each texture layer has a vegetation texture and can also
    /// have a "data" texture associated with it that specifies scale, density per pixel.
    /// </summary>
    public class InstanceTextureEntry
    {
        #region Variables
        public String TexturePath;
        public Bitmap Texture;

        public String DataTexturePath;
        public Bitmap DataTexture;

        public String TintTexturePath;
        public Bitmap TintTexture;
        #endregion

        #region Constructors
        public InstanceTextureEntry(String texturePath)
        {
            // Hook up our base vegetation texture.
            FileStream fileStream = new FileStream(texturePath, FileMode.Open, FileAccess.Read, FileShare.Read, 256);
            Bitmap texture = new Bitmap(fileStream);
            fileStream.Close();

            Texture = texture;
            TexturePath = texturePath;

            // No associated data texture for this vegetation.
            DataTexturePath = null;
            DataTexture = null;

            TintTexturePath = null;
            TintTexture = null;
        }

        public InstanceTextureEntry(String texturePath, String dataTexturePath = null, String tintTexturePath = null)
        {
            // Hook up our base vegetation texture.
            FileStream vegFileStream = new FileStream(texturePath, FileMode.Open, FileAccess.Read, FileShare.Read);
            Bitmap texture = new Bitmap(vegFileStream);
            vegFileStream.Close();

            Texture = texture;
            TexturePath = texturePath;

            if (!String.IsNullOrEmpty(dataTexturePath))
            {
                // Add the data texture.
                FileStream dataFileStream = new FileStream(dataTexturePath, FileMode.Open, FileAccess.Read, FileShare.Read);
                Bitmap dataTexture = new Bitmap(dataFileStream);
                dataFileStream.Close();

                DataTexture = dataTexture;
                DataTexturePath = dataTexturePath;
            }

            if (!String.IsNullOrEmpty(tintTexturePath))
            {
                // Add the tint texture.
                FileStream tintFileStream = new FileStream(tintTexturePath, FileMode.Open, FileAccess.Read, FileShare.Read);
                Bitmap tintTexture = new Bitmap(tintFileStream);
                tintFileStream.Close();

                TintTexture = tintTexture;
                TintTexturePath = tintTexturePath;
            }
        }
        #endregion
    }

    /// <summary>
    /// Structure defining an actual texture along with its location relative
    /// to the other textures.
    /// </summary>
    public class InstanceTexture
    {
        #region Variables
        public int Width;
        public int Height;
        public List<InstanceTextureEntry> TextureEntries;
        public Bitmap GlobalTintTexture;
        public Bitmap GlobalDataTexture;
        public TextureBounds Bounds;
        public BoundingBox3f ContainerBounds;
        #endregion Variables

        #region Constants
        private static readonly String LOG_CTX = "Instance Texture";

        public static readonly String DATA_TEXTURE_PREFIX = "_data";
        public static readonly String TINT_TEXTURE_PREFIX = "_tint";
        #endregion Constants

        #region Constructors

        public InstanceTexture(TextureBounds bounds, BoundingBox3f containerBounds)
        {
            Bounds = bounds;
            ContainerBounds = containerBounds;
            TextureEntries = new List<InstanceTextureEntry>();
        }
        #endregion Constructors

        /// <summary>
        /// Loads all the textures that were passed in. Will go through
        /// and sort the associated data textures and then create the 
        /// InstanceTextureEntry textures that store the bitmap data.
        /// </summary>
        /// <param name="texturePaths"></param>
        /// <returns></returns>
        public bool LoadTextures(IUniversalLog log, String[] texturePaths, String globalTintTexturePath, String globalDataTexturePath)
        {
            List<String> vegTextures = new List<String>();
            List<String> dataTextures = new List<String>();
            List<String> tintTextures = new List<String>();

            // Create a list of data textures and vegetation textures.
            foreach (String texturePath in texturePaths)
            {
                String filename = Path.GetFileNameWithoutExtension(texturePath);

                if (filename.Contains(DATA_TEXTURE_PREFIX))
                    dataTextures.Add(texturePath);
                else if (filename.Contains(TINT_TEXTURE_PREFIX))
                    tintTextures.Add(texturePath);
                else
                    vegTextures.Add(texturePath);
            }

            var randomisedVegTextures = vegTextures.OrderBy(a => Guid.NewGuid());

            // Match the data/tint texture with its associated vegetation layer texture.
            foreach (String vegTexture in randomisedVegTextures)
            {
                String directory = Path.GetDirectoryName(vegTexture);
                String filename = Path.GetFileNameWithoutExtension(vegTexture);
                String extension = Path.GetExtension(vegTexture);

                String dataFilename = Path.Combine(directory, (filename + DATA_TEXTURE_PREFIX + extension));
                String tintFilename = Path.Combine(directory, (filename + TINT_TEXTURE_PREFIX + extension));

                InstanceTextureEntry textureEntry = null;

                bool dataTextureExists = false;
                bool tintTextureExists = false;

                if (dataTextures.Contains(dataFilename))
                    dataTextureExists = true;
                if (tintTextures.Contains(tintFilename))
                    tintTextureExists = true;

                if (dataTextureExists && tintTextureExists)
                    textureEntry = new InstanceTextureEntry(vegTexture, dataFilename, tintFilename);
                else if (dataTextureExists)
                    textureEntry = new InstanceTextureEntry(vegTexture, dataFilename);
                else if (tintTextureExists)
                    textureEntry = new InstanceTextureEntry(vegTexture, null, tintFilename);
                else
                    textureEntry = new InstanceTextureEntry(vegTexture);

                TextureEntries.Add(textureEntry);
            }

            // Validate all the texture entries in each layer and make sure they all match in size.
            foreach (InstanceTextureEntry texEntry in TextureEntries)
            {
                int vegTexHeight = texEntry.Texture.Height;
                int vegTexWidth = texEntry.Texture.Width;

                if (texEntry.DataTexture != null && (texEntry.DataTexture.Height != vegTexHeight || texEntry.DataTexture.Width != vegTexWidth))
                {
                    log.ErrorCtx(LOG_CTX, "Data texture does not match the size of base vegetation texture");
                    log.ErrorCtx(LOG_CTX, "Vegetation Texture Size - W:{0}, H:{1} --> Data Texture Size - W:{2}, H:{3}", vegTexWidth, vegTexHeight, texEntry.DataTexture.Width, texEntry.DataTexture.Height);
                    return false;
                }

                if (texEntry.TintTexture != null && (texEntry.TintTexture.Height != vegTexHeight || texEntry.TintTexture.Width != vegTexWidth))
                {
                    log.ErrorCtx(LOG_CTX, "Tint texture does not match the size of base vegetation texture.");
                    log.ErrorCtx(LOG_CTX, "Vegetation Texture Size - W:{0}, H:{1} --> Tint Texture Size - W:{2}, H:{3}", vegTexWidth, vegTexHeight, texEntry.TintTexture.Width, texEntry.TintTexture.Height);
                    return false;
                }
            }

            if (!String.IsNullOrEmpty(globalTintTexturePath))
            {
                FileStream globalTintStream = new FileStream(globalTintTexturePath, FileMode.Open, FileAccess.Read, FileShare.Read);
                Bitmap tintTexture = new Bitmap(globalTintStream);
                globalTintStream.Close();

                GlobalTintTexture = tintTexture;
            }
            else
            {
                GlobalTintTexture = null;
            }

            if (!String.IsNullOrEmpty(globalDataTexturePath))
            {
                FileStream globalDataStream = new FileStream(globalDataTexturePath, FileMode.Open, FileAccess.Read);
                Bitmap dataTexture = new Bitmap(globalDataStream);
                globalDataStream.Close();

                GlobalDataTexture = dataTexture;
            }
            else
            {
                GlobalDataTexture = null;
            }

            // Make sure we have at least 1 texture and use it to store the width/height.
            InstanceTextureEntry entry = TextureEntries.FirstOrDefault();
            if (entry == null)
            {
                log.ErrorCtx(LOG_CTX, "No textures have been loaded. Aborting.");
                return false;
            }

            Width = entry.Texture.Width;
            Height = entry.Texture.Height;

#warning TALL: Hook up the texture size validation.
            /*
            // Make sure that if we have a global tint texture that it matches the size of the other textures.
            if (GlobalTintTexture != null)
            {
                if (GlobalTintTexture.Width != Width || GlobalTintTexture.Height != Height)
                {
                    log.ErrorCtx(LOG_CTX, "Global tint texture does not match the size of base vegetation textures.");
                    log.ErrorCtx(LOG_CTX, "Vegetation Texture Size - W:{0}, H:{1} --> Tint Texture Size - W:{2}, H:{3}", Width, Height, GlobalTintTexture.Width, GlobalTintTexture.Height);
                    return false;
                }
            }

            // Make sure that if we have a global data texture that it matches the size of the other textures.
            if (GlobalDataTexture != null)
            {
                if (GlobalDataTexture.Width != Width || GlobalDataTexture.Height != Height)
                {
                    log.ErrorCtx(LOG_CTX, "Global data texture does not match the size of base vegetation textures.");
                    log.ErrorCtx(LOG_CTX, "Vegetation Texture Size - W:{0}, H:{1} --> Data Texture Size - W:{2}, H:{3}", Width, Height, GlobalDataTexture.Width, GlobalDataTexture.Height);
                    return false;
                }
            }
            */

            return true;
        }

        /// <summary>
        /// Goes through all the "vegetation" textures and gets a list of ColorReferences
        /// to know the pixel colours as well as their texture index.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns>List of vegetation pixels at this x,y position</returns>
        public ColorReference[] GetVegetationPixel(int x, int y)
        {
            if (x < 0 || x >= Width || y < 0 || y >= Height)
                return null;

            List<ColorReference> colors = new List<ColorReference>();

            for (int i = 0; i < TextureEntries.Count; i++)
            {
                lock (TextureEntries[i].Texture)
                {
                    colors.Add(new ColorReference(TextureEntries[i].Texture.GetPixel(x, y), i));
                }
            }

            if (colors.Count > 0)
                return colors.ToArray();
            else
                return null;
        }

        /// <summary>
        /// Goes through all the "data" textures and gets a list of ColorReferences
        /// to know the pixel colours as well as their texture index.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns>List of data pixels at this x,y position</returns>
        public ColorReference[] GetPixelData(int x, int y, bool useGlobalData)
        {
            if (x < 0 || x >= Width || y < 0 || y >= Height)
                return null;

            List<ColorReference> colors = new List<ColorReference>();

            if (useGlobalData && GlobalDataTexture != null)
            {
                lock (GlobalDataTexture)
                {
                    // Use -1 so we know it's a global texture index.
                    colors.Add(new ColorReference(GlobalDataTexture.GetPixel(x, y), -1));
                }
            }
            else
            {
                for (int i = 0; i < TextureEntries.Count; i++)
                {
                    if (TextureEntries[i].DataTexture == null)
                        continue;

                    lock (TextureEntries[i].DataTexture)
                    {
                        colors.Add(new ColorReference(TextureEntries[i].DataTexture.GetPixel(x, y), i));
                    }
                }
            }

            if (colors.Count > 0)
                return colors.ToArray();
            else
                return null;
        }

        /// <summary>
        /// Goes through all the tint textures and gets a list of ColorReferences
        /// to know the pixel colours as well as their texture index.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns>List of tint pixels at this x,y position</returns>
        public ColorReference[] GetPixelTint(int x, int y, bool useGlobalTint)
        {
            if (x < 0 || x >= Width || y < 0 || y >= Height)
                return null;

            List<ColorReference> colors = new List<ColorReference>();

            if (useGlobalTint && GlobalTintTexture != null)
            {
                lock (GlobalTintTexture)
                {
                    // Use -1 so we know it's a global texture index.
                    colors.Add(new ColorReference(GlobalTintTexture.GetPixel(x, y), -1));
                }
            }
            else
            {
                for (int i = 0; i < TextureEntries.Count; i++)
                {
                    if (TextureEntries[i].TintTexture == null)
                        continue;

                    lock (TextureEntries[i].TintTexture)
                    {
                        colors.Add(new ColorReference(TextureEntries[i].TintTexture.GetPixel(x, y), i));
                    }
                }
            }

            if (colors.Count > 0)
                return colors.ToArray();
            else
                return null;
        }

        /// <summary>
        /// Based on a texture index, find the pixel at the given x,y coordinate
        /// from the data texture.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="textureIndex"></param>
        /// <returns>Data pixel colour</returns>
        public Color GetPixelData(int x, int y, int textureIndex)
        {
            Color value = Color.Blue;

            if (x < 0 || x >= Width || y < 0 || y >= Height || textureIndex >= TextureEntries.Count)
                return value;

            if (textureIndex == -1 && GlobalDataTexture != null)
            {
                lock (GlobalDataTexture)
                {
                    value = GlobalDataTexture.GetPixel(x, y);
                }
            }
            else if (textureIndex != -1)
            {
                if (TextureEntries[textureIndex].DataTexture == null)
                    return value;

                lock (TextureEntries[textureIndex].DataTexture)
                {
                    value = TextureEntries[textureIndex].DataTexture.GetPixel(x, y);
                }
            }

            return value;
        }

        /// <summary>
        /// Based on a texture index, find the pixel at the given x,y coordinate
        /// from the tint texture.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="textureIndex"></param>
        /// <returns>Data pixel colour</returns>
        public Color GetPixelTint(int x, int y, int textureIndex)
        {
            Color value = Color.White;

            if (x < 0 || x >= Width || y < 0 || y >= Height || textureIndex >= TextureEntries.Count)
                return value;

            if (textureIndex == -1 && GlobalTintTexture != null)
            {
                lock (GlobalTintTexture)
                {
                    value = GlobalTintTexture.GetPixel(x, y);
                }
            }
            else if (textureIndex != -1)
            {
                if (TextureEntries[textureIndex].TintTexture == null)
                    return value;

                lock (TextureEntries[textureIndex].TintTexture)
                {
                    value = TextureEntries[textureIndex].TintTexture.GetPixel(x, y);
                }
            }

            return value;
        }
    }
}

