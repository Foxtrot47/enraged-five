﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.Pipeline.Services.AssetPack
{

    /// <summary>
    /// 
    /// </summary>
    internal class AssetPackFile
    {
        internal AssetPackFile(string pathname, string destination = null)
        {
            Pathname = pathname;
            Destination = destination;
        }

        internal XElement ToXElement()
        {
            XElement fileElement = new XElement("File", new XAttribute("path", Pathname));
            if(Destination != null)
                fileElement.Add(new XAttribute("destination", Destination));

            return fileElement;
        }

        internal string Pathname { get; private set; }
        internal string Destination { get; private set; }
    }

} // RSG.Pipeline.Services.AssetPack namespace
