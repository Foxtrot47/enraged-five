﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.Pipeline.Services.BoundsProcessor
{

    /// <summary>
    /// Bounds Processor input abstraction.
    /// </summary>
    public class BoundsProcessorInput
    {
        #region Properties
        /// <summary>
        /// SceneXml absolute filename.
        /// </summary>
        public String SceneXmlPathname { get; private set; }
        
        /// <summary>
        /// SceneXml scene type (from Content-Tree).
        /// </summary>
        public String SceneType { get; private set; }

        /// <summary>
        /// Absolute filename to extracted mapname_collision.zip.
        /// </summary>
        public String BoundsPathname { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public String InputDir { get; private set; }

        /// <summary>
        /// String list of drawables to pack bounds for.
        /// </summary>
        /// Note: this is kind of obsolete since the new map pipeline will certainly
        /// not use this instead just adding additional files using the Asset Processor.
        public String DrawableList { get; private set; }
        
        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<String> AdditionalImaps { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="sceneXmlPathname"></param>
        /// <param name="sceneType"></param>
        /// <param name="boundsPathname"></param>
        /// <param name="inputDir"></param>
        /// <param name="drawableList"></param>
        /// <param name="additionalImaps"></param>
        public BoundsProcessorInput(String sceneXmlPathname, String sceneType, String boundsPathname,
            String inputDir, String drawableList, IEnumerable<String> additionalImaps)
        {
            this.SceneXmlPathname = sceneXmlPathname;
            this.SceneType = sceneType;
            this.BoundsPathname = boundsPathname;
            this.DrawableList = drawableList;
            this.InputDir = inputDir;
            this.AdditionalImaps = additionalImaps;
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Serialise to XElement.
        /// </summary>
        /// <returns></returns>
        public XElement ToXElement()
        {
            XElement element = new XElement("Input",
                new XAttribute("scenexml", SceneXmlPathname),
                new XAttribute("scenetype", SceneType));
            if (!String.IsNullOrEmpty(BoundsPathname))
                element.Add(new XAttribute("bounds", BoundsPathname));
            if (!String.IsNullOrEmpty(InputDir))
                element.Add(new XAttribute("inputDir", InputDir));
            if (!String.IsNullOrEmpty(DrawableList))
                element.Add(new XAttribute("drawableList", DrawableList));
            if (AdditionalImaps != null)
            {
                foreach (string additionalImap in AdditionalImaps)
                {
                    element.Add(new XElement("AdditionalImap", new XAttribute("file", additionalImap)));
                }
            }
            return element;
        }
        #endregion // Controller Methods
    }

} // RSG.Pipeline.Services.BoundsProcessor namespace
