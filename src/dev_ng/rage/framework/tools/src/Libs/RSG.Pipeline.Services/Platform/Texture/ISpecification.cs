﻿using System;
using System.Collections.Generic;

namespace RSG.Pipeline.Services.Platform.Texture
{
        
    /// <summary>
    /// Read-only interface to the texture pipeline specification
    /// metadata files.  Used in the pipeline for quick access to
    /// texture pipeline property for dependency tracking.
    /// </summary>
    /// Note: deliberately read-only and quick (not using RSG.Metadata)
    /// as we will process lots of these on any given build.
    /// 
    public interface ISpecification
    {
        #region Properties
        /// <summary>
        /// Immediate specification parent filename.
        /// </summary>
        String ImmediateParent { get; }

        /// <summary>
        /// Specification filenames in order of load.
        /// </summary>
        IEnumerable<String> Filenames { get; }
        
        /// <summary>
        /// Source texture absolute paths (BMP, TGA, etc).
        /// </summary>
        IEnumerable<String> SourceTextures { get; }

        /// <summary>
        /// Export texture absolute paths (uncompressed DDS).
        /// </summary>
        IEnumerable<IResourceTexture> ResourceTextures { get; }
        
        /// <summary>
        /// Parent specification template.
        /// </summary>
        String Parent { get; }

        /// <summary>
        /// ImageSplitHD property; splits the top-MIP chain.
        /// </summary>
        bool ImageSplitHD { get; }

        /// <summary>
        /// SkipProcessing property; skips texture pipeline processing.
        /// </summary>
        bool SkipProcessing { get; }

        /// <summary>
        /// ImageFiles property; additional texture inputs.
        /// </summary>
        IEnumerable<String> ImageFiles { get; }
        #endregion // Properties
    }

} // RSG.Pipeline.Services.Platform.Texture namespace
