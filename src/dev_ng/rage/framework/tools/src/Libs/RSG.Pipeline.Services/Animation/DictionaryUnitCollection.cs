﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.Services.Animation
{
    /// <summary>
    /// Wrapper for a collection DictionaryUnit objects
    /// </summary>
    public sealed class DictionaryUnitCollection
        : IEnumerable<IDictionaryUnit>
    {
        #region Fields
        /// <summary>
        /// The collection of DictionaryUnit objects
        /// </summary>
        private List<IDictionaryUnit> _Items = new List<IDictionaryUnit>();
        #endregion // Fields

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="unit"></param>
        /// <returns></returns>
        public void Add(IDictionaryUnit unit)
        {
            _Items.Add(unit);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public IDictionaryUnit FindDictionaryNamed(String name)
        {
            foreach (IDictionaryUnit unit in this._Items)
            {
                if (unit.DictionaryName.Equals(name))
                {
                    return unit;
                }
            }
            return null;
        }

        /// <summary>
        /// Determine whether the Item list already contains a DictionaryUnit.
        /// </summary>
        /// <param name="inClipGroup"></param>
        public bool Contains(ref IDictionaryUnit inUnit)
        {
            foreach (IDictionaryUnit unit in this._Items)
            {
                if (unit.Equals(inUnit))
                {
                    return true;
                }
            }
            return false;
        }
        #endregion // Methods

        #region IEnumerable<IDictionaryUnit> Members
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerator<IDictionaryUnit> GetEnumerator()
        {
            return _Items.GetEnumerator();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        #endregion // IEnumerable<DictionaryUnit> Members
    }


}
