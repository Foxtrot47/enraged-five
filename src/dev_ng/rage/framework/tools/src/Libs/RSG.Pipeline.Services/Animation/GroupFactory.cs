﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using RSG.ManagedRage.ClipAnimation;
using RSG.Base.Configuration;
using System.Xml;
using RSG.Base.Logging.Universal;

namespace RSG.Pipeline.Services.Animation
{
    /// <summary>
    /// Responsible for handling clips and their animations.  Ported from old pipeline.
    /// </summary>
    public static class GroupFactory
    {
        #region Constants
        const string PROPERTY_SUFFIX = "_DO_NOT_RESOURCE";
        const string PROPERTY_COMPRESSION_TEMPLATE = "Compressionfile" + PROPERTY_SUFFIX;
        const string PROPERTY_LINEAR_COMPRESSION = "LinearCompression" + PROPERTY_SUFFIX;
        const string PROPERTY_SKELETON_FILE = "Skelfile" + PROPERTY_SUFFIX;
        const string PROPERT_ADDITIVE = "Additive" + PROPERTY_SUFFIX;
        private static readonly String LOG_CTX = "Group Factory Processor";

        public static string EXT_CLIP = ".clip";
        const string EXT_CLIPXML = ".clipxml";
        const string EXT_ANIM = ".anim";
        #endregion // Constants

        #region Classes

        public class Entry
        {
            public Entry(Ionic.Zip.ZipEntry zip, String dir)
            {
                zipEntry = zip;
                dirEntry = dir;
            }

            public Ionic.Zip.ZipEntry zipEntry = new Ionic.Zip.ZipEntry();
            public String dirEntry = String.Empty;
        }

        #endregion 

        #region Methods

        public static bool IsDirectory(string input)
        {
            return (File.GetAttributes(input) & FileAttributes.Directory) == FileAttributes.Directory;
        }

        static int RunProcess(string strExe, string strArgs, IUniversalLog log)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.Arguments = strArgs;
            startInfo.FileName = strExe;
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.CreateNoWindow = true;
            startInfo.RedirectStandardOutput = true;
            startInfo.UseShellExecute = false;

            log.MessageCtx(LOG_CTX, strExe + " " + strArgs);

            Process proc = Process.Start(startInfo);
            String output = proc.StandardOutput.ReadToEnd();
            proc.WaitForExit();
            log.MessageCtx(LOG_CTX, output);

            return proc.ExitCode;
        }

        /// <summary>
        /// Prepare lists of anims and clips
        /// </summary>
        public static void Prepare(string zipFilename, ref List<Entry> clipFiles, ref List<Entry> animFiles, ref List<Entry> otherFiles)
        {
            bool isDir = IsDirectory(zipFilename);

            if (isDir)
            {
                string[] files = Directory.GetFiles(zipFilename);

                foreach (var entry in files)
                {
                    if (entry.EndsWith(EXT_CLIP))
                    {
                        clipFiles.Add(new Entry(null, entry));
                    }
                    else if (entry.EndsWith(EXT_ANIM))
                    {
                        animFiles.Add(new Entry(null, entry));
                    }
                    else
                    {
                        otherFiles.Add(new Entry(null, entry));
                    }
                }
            }
            else
            {
                using (var clipsArchive = Ionic.Zip.ZipFile.Read(zipFilename))
                {
                    foreach (var entry in clipsArchive.Entries)
                    {
                        if (entry.FileName.EndsWith(EXT_CLIP))
                        {
                            clipFiles.Add(new Entry(entry, null));
                        }
                        else if (entry.FileName.EndsWith(EXT_ANIM))
                        {
                            animFiles.Add(new Entry(entry, null));
                        }
                        else
                        {
                            otherFiles.Add(new Entry(entry, null));
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Serialise all clip lists.
        /// </summary>
        public static void Serialise(GroupCollection clipGroups, String outputDir, IUniversalLog log)
        {
            log.MessageCtx(LOG_CTX, "Serialising cliplists...");

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            using (XmlWriter writer = XmlWriter.Create(Path.Combine(outputDir, "cliplists.xml"), settings))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Lists");

                foreach (Group clipGroup in clipGroups)
                {
                    writer.WriteStartElement("List");

                    writer.WriteElementString("Compression", Path.GetFileNameWithoutExtension(clipGroup.CompressionTemplate));
                    writer.WriteElementString("Additive", clipGroup.Additive.ToString());
                    writer.WriteElementString("Skeleton", clipGroup.SkeletonFile);

                    writer.WriteStartElement("Clips");

                    foreach (string clip in clipGroup.ClipFiles)
                    {
                        writer.WriteElementString("Clip", clip);
                    }

                    writer.WriteEndElement();

                    writer.WriteEndElement();
                }

                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
        }

        /// <summary>
        /// Inject default skeleton file if one does not exist
        /// </summary>
        private static void InjectDefaultSkeleton(MClip.ClipType type, List<String> clipFiles, String defaultSkel, IUniversalLog log)
        {
            if (defaultSkel == String.Empty) return; // Optional param

            MClip.Init();
            foreach (String clipfile in clipFiles)
            {
                using (MClip rageClip = new MClip(type))
                {
                    bool clipLoaded = Convert.ToBoolean(rageClip.Load(clipfile));

                    if (clipLoaded)
                    {
                        bool hasSkel = rageClip.GetProperties().Any(item => item.GetName().ToLower() == PROPERTY_SKELETON_FILE.ToLower());

                        if (!hasSkel)
                        {
                            log.WarningCtx(LOG_CTX, "Injecting default skeleton '{0}' into clip file '{1}'", Path.GetFileName(defaultSkel), Path.GetFileName(clipfile));
#warning MW: Coalersing requires a skeleton file, James said altering coalerse itself was forbidden so we dump this bad boi right here
                            rageClip.GetProperties().Add(new MProperty(PROPERTY_SKELETON_FILE, new List<MPropertyAttribute>() { new MPropertyAttributeString(PROPERTY_SKELETON_FILE, Path.GetFileName(defaultSkel)) }));
                        
                            rageClip.Save();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Group clips that share properties.
        /// </summary>
        public static bool ExtractAndCreateClipGroups(IUniversalLog log, String zipFilename, String outputDir, String modDirectory, String templateDirectory, String skelDirectory, String clipFixupExe, String defaultSkel, String dlcPrefix="", bool useClipXml=false)
        {
            MClip.ClipType type = useClipXml ? MClip.ClipType.XML : MClip.ClipType.Normal;
            if (type == MClip.ClipType.XML) EXT_CLIP = EXT_CLIPXML;

            bool retval = true;

            log.MessageCtx(LOG_CTX, "Unpacking {0} to {1}", zipFilename, outputDir);
            List<Entry> clipFiles = new List<Entry>();
            List<Entry> animFiles = new List<Entry>();
            List<Entry> otherFiles = new List<Entry>();
            GroupCollection clipGroups = new GroupCollection();

            log.MessageCtx(LOG_CTX, "Creating clip groups...");
            if(!Directory.Exists(outputDir))
                Directory.CreateDirectory(outputDir);

            bool isDir = false;
            try
            {
                isDir = IsDirectory(zipFilename);
            }
            catch (FileNotFoundException /*e*/)
            {
                log.ErrorCtx(LOG_CTX, "File/Directory '{0}' does not exist.", zipFilename);
                Directory.Delete(outputDir, true);
                return false;
            }

            // Load our inclusions and exclusions
            List<DictionaryItem> inclusions = new List<DictionaryItem>();
            List<String> exclusions = new List<String>();

            String inclusionFile = String.Empty;
            String exclusionFile = String.Empty;

            if(modDirectory != null)
            {
                inclusionFile = Path.Combine(modDirectory, "inclusions.modlist");
                exclusionFile = Path.Combine(modDirectory, "exclusions.modlist");
            }

            bool hasInclusions = false;

            if (isDir)
            {
                if (!Directory.Exists(zipFilename))
                {
                    log.ErrorCtx(LOG_CTX, "Directory '{0}' does not exist.", zipFilename);
                    Directory.Delete(outputDir, true);
                    return false;
                }
            }
            else
            {
                if (File.Exists(inclusionFile))
                    hasInclusions = true;
                if (!File.Exists(zipFilename) && !hasInclusions)
                {
                    log.ErrorCtx(LOG_CTX, "Input zip file does not exist: {0} AND there is no inclusion data: {1}.", zipFilename, inclusionFile);
                    Directory.Delete(outputDir, true);
                    return false;
                }
            }
            
            if(File.Exists(zipFilename) || Directory.Exists(zipFilename))
            {
                GroupFactory.Prepare(zipFilename, ref clipFiles, ref animFiles, ref otherFiles);
            }

            if (clipFiles.Count != animFiles.Count || (clipFiles.Count == 0 && !hasInclusions))
            {
                log.ErrorCtx(LOG_CTX, "Mismatching clip/anim count or no clip/anims found in: {0}.  Is it a valid dictionary?", zipFilename);
                Directory.Delete(outputDir, true);
                return false;
            }

            try
            {
                // Read inclusions from inclusion file
                if(File.Exists(inclusionFile))
                {
                    using (StreamReader sr = new StreamReader(inclusionFile))
                    {
                        String line;
                        while ((line = sr.ReadLine()) != null)
                        {
                            String[] tokens = line.Split(',');

                            DictionaryItem dictUnit = new DictionaryItem(tokens[1].Trim(), tokens[0].Trim(), tokens[2].Trim());
                            inclusions.Add(dictUnit);
                        }
                        sr.Close();
                    }
                }

                // Read exclusions from inclusion file
                if (File.Exists(exclusionFile))
                {
                    using (StreamReader sr = new StreamReader(exclusionFile))
                    {
                        String line;
                        while ((line = sr.ReadLine()) != null) 
                        {
                            exclusions.Add(line.Trim());
                        }
                        sr.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                log.ToolExceptionCtx(LOG_CTX, ex, "Exception reading inclusion/exclusion files.");
            }

            foreach (Entry animfile in animFiles)
            {
                String filenameOnly = (isDir) ? Path.GetFileName(animfile.dirEntry) : animfile.zipEntry.FileName;
                String destFile = Path.Combine(outputDir, filenameOnly);
                if (!exclusions.Contains(Path.GetFileNameWithoutExtension(filenameOnly)))
                {
                    if (isDir)
                        File.Copy(animfile.dirEntry, destFile, true);
                    else
                        animfile.zipEntry.Extract(outputDir, Ionic.Zip.ExtractExistingFileAction.OverwriteSilently);
                }

                FileAttributes attributes = File.GetAttributes(destFile);
                if ((attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                    File.SetAttributes(destFile, attributes ^ FileAttributes.ReadOnly);
            }

            foreach (Entry clipfile in clipFiles)
            {
                String filenameOnly = (isDir) ? Path.GetFileName(clipfile.dirEntry) : clipfile.zipEntry.FileName;
                String destFile = Path.Combine(outputDir, filenameOnly);
                if (!exclusions.Contains(Path.GetFileNameWithoutExtension(filenameOnly)))
                {
                    if (isDir)
                        File.Copy(clipfile.dirEntry, destFile, true);
                    else
                        clipfile.zipEntry.Extract(outputDir, Ionic.Zip.ExtractExistingFileAction.OverwriteSilently);
                }

                FileAttributes attributes = File.GetAttributes(destFile);
                if ((attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                    File.SetAttributes(destFile, attributes ^ FileAttributes.ReadOnly);
            }

            foreach (Entry file in otherFiles)
            {
                String filenameOnly = (isDir) ? Path.GetFileName(file.dirEntry) : file.zipEntry.FileName;
                String destFile = Path.Combine(outputDir, filenameOnly);

                if (isDir)
                    File.Copy(file.dirEntry, destFile, true);
                else
                    file.zipEntry.Extract(outputDir, Ionic.Zip.ExtractExistingFileAction.OverwriteSilently);
            }

            bool nameOverriden = false;

            // If map prefix fixup is needed, rename the anims and clips 
            if (!String.IsNullOrEmpty(dlcPrefix))
            {
                string[] files = Directory.GetFiles(outputDir);

                // Copy the files to the new names
                foreach (string file in files)
                {
                    if (String.Compare(Path.GetExtension(file), ".clip", true) == 0 ||
                        String.Compare(Path.GetExtension(file), ".anim", true) == 0)
                    {
                        File.Move(file, Path.Combine(Path.GetDirectoryName(file), dlcPrefix + Path.GetFileName(file)));
                    }
                }

                nameOverriden = true;
            }

            List<String> masterClipFileList = new List<String>(Directory.GetFiles(outputDir, "*" + EXT_CLIP));

            InjectDefaultSkeleton(type, masterClipFileList, defaultSkel, log);

            foreach (DictionaryItem inclusion in inclusions)
            {
                // Try and use the basename as a wildcard.  if we get results then assume a wildcard has been used.
                List<String> wildcardFiles = new List<String>(Directory.GetFiles(inclusion.Path, inclusion.Basename));
                if(wildcardFiles.Count > 0)
                {
                    foreach(String file in wildcardFiles)
                    {
                        String destFile = Path.Combine(outputDir, Path.GetFileName(file));
                         File.Copy(file, destFile, true);
                        FileAttributes attributes = File.GetAttributes(destFile);
                        if ((attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                            File.SetAttributes(destFile, attributes ^ FileAttributes.ReadOnly);

                        if(Path.GetExtension(destFile) == EXT_CLIP)
                            masterClipFileList.Add(destFile);
                    }
                }
                // Else take the base name as an explicit reference to a clip/anim combo
                else
                {
                    String inclusionClipFilename = Path.Combine(inclusion.Path, inclusion.Basename + EXT_CLIP);
                    String inclusionAnimFilename = Path.Combine(inclusion.Path, inclusion.Basename + EXT_ANIM);
                    if (File.Exists(inclusionClipFilename) && File.Exists(inclusionAnimFilename))
                    {
                        String destClipFile = Path.Combine(outputDir, inclusion.Basename + EXT_CLIP);
                        String destAnimFile = Path.Combine(outputDir, inclusion.Basename + EXT_ANIM);

                        File.Copy(inclusionClipFilename, destClipFile, true);
                        FileAttributes attributes = File.GetAttributes(destClipFile);
                        if ((attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                            File.SetAttributes(destClipFile, attributes ^ FileAttributes.ReadOnly);

                        File.Copy(inclusionAnimFilename, destAnimFile, true);
                        attributes = File.GetAttributes(destAnimFile);
                        if ((attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                            File.SetAttributes(destAnimFile, attributes ^ FileAttributes.ReadOnly);

                        // Check for overriden basename
                        if (inclusion.OverrideName != "")
                        {
                            String newDestClipFile = Path.Combine(outputDir, inclusion.OverrideName + EXT_CLIP);
                            String newDestAnimFile = Path.Combine(outputDir, inclusion.OverrideName + EXT_ANIM);

                            File.Copy(destClipFile, newDestClipFile, true);
                            File.Copy(destAnimFile, newDestAnimFile, true);

                            File.Delete(destClipFile);
                            File.Delete(destAnimFile);

                            nameOverriden = true;
                            masterClipFileList.Add(newDestClipFile);
                        }
                        else
                        {
                            masterClipFileList.Add(destClipFile);
                        }
                    }
                    else
                    {
                        log.ErrorCtx(LOG_CTX, "Inclusion file {0} is missing from disk: {1}.", inclusion.Basename, inclusionClipFilename);
                        retval = false;
                    }
                }
            }

            // Invoke clip rename if any files have been renamed
            if (nameOverriden)
            {
                String fixup_args = String.Format("-dir {0} -nopopups", outputDir);
                RunProcess(clipFixupExe, fixup_args, log);
            }

            // Extract clip property information required to do the actual clip grouping
            MClip.Init();
            foreach (String clipFilename in masterClipFileList)
            {
                using (MClip rageClip = new MClip(type))
                {
                    MemoryStream stream = new MemoryStream();
                    bool clipLoaded = (bool)rageClip.Load(clipFilename);

                    if (clipLoaded)
                    {
                        MProperty propCompression = rageClip.FindProperty(PROPERTY_COMPRESSION_TEMPLATE);
                        MProperty propSkeleton = rageClip.FindProperty(PROPERTY_SKELETON_FILE);
                        MProperty propAdditive = rageClip.FindProperty(PROPERT_ADDITIVE);

                        Group clipGroup = new Group();
                        if (propCompression != null)
                        {
                            clipGroup.CompressionTemplate = (propCompression.GetPropertyAttributes()[0] as MPropertyAttributeString).GetString();
                            if (propSkeleton == null)
                                clipGroup.SkeletonFile = String.Empty;
                            else
                                clipGroup.SkeletonFile = (propSkeleton.GetPropertyAttributes()[0] as MPropertyAttributeString).GetString();
                            clipGroup.Additive = false;
                            if (propAdditive != null)
                            {
                                clipGroup.Additive = (int)(propAdditive.GetPropertyAttributes()[0] as MPropertyAttributeInt).GetInt() == 1 ? true : false;
                            }

                            // Bundle the compression settings and skeleton into a clip propery.  Required for coalesceing.
                            String compressionFilename = Path.Combine(templateDirectory, clipGroup.CompressionTemplate);
                            String skeletonFilename = Path.Combine(skelDirectory, clipGroup.SkeletonFile);

                            String compressionTemplateAsString = String.Empty;
                            compressionFilename = compressionFilename.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                            if (File.Exists(compressionFilename))
                            {
                                using(TextReader tr = File.OpenText(compressionFilename))
                                {
                                    String line = String.Empty;
                                    while ((line = tr.ReadLine()) != null)
                                    {
                                        compressionTemplateAsString = String.Concat(compressionTemplateAsString, line);
                                    }
                                }

                                List<MPropertyAttribute> propertyAttributes = new List<MPropertyAttribute>();
                                MPropertyAttributeString rules = new MPropertyAttributeString("Rules", compressionTemplateAsString);
                                propertyAttributes.Add(rules);

                                skeletonFilename = skeletonFilename.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                                if (File.Exists(skeletonFilename))
                                {
                                    MPropertyAttributeString skeleton = new MPropertyAttributeString("Skeleton", skeletonFilename);
                                    propertyAttributes.Add(skeleton);
                                }

                                MProperty compression = new MProperty("Compression_DO_NOT_RESOURCE", propertyAttributes);
                                rageClip.GetProperties().Add(compression);
                            }
                            else
                            {
                                log.ErrorCtx(LOG_CTX, "Couldn't find compression file {0} for clip {1}.", compressionFilename, clipFilename);
                                retval = false;
                            }
                            clipGroup.ClipFiles.Add(Path.ChangeExtension(clipFilename, ".clip"));
                            clipGroups.AddOrMerge(ref clipGroup);
                        }
                        else
                        {
                            log.ErrorCtx(LOG_CTX, "Skipping processing of {0} due to missing compression properties...", clipFilename);
                            retval = false;
                        }
                    }
                    else
                    {
                        log.ErrorCtx(LOG_CTX, "Failed to load clip {0}", clipFilename);
                        retval = false;
                    }
                    rageClip.Save();
                }
            }

            GroupFactory.Serialise(clipGroups, outputDir, log);
            return retval;
        }
        #endregion // Methods

    }
}
