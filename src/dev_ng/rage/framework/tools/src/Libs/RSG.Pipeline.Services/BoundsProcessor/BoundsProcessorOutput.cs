﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.Pipeline.Services.BoundsProcessor
{
    public class BoundsProcessorOutput
    {
        public BoundsProcessorOutput(String name, String directory, String manifestAdditionsPathname)
        {
            Name = name;
            Directory = directory;
            ManifestAdditionsPathname = manifestAdditionsPathname;
        }

        public XElement ToXElement()
        {
            return new XElement("Output",
                new XAttribute("name", Name),
                new XAttribute("directory", Directory),
                new XAttribute("manifest_additions", ManifestAdditionsPathname));
        }

        public String Name { get; private set; }
        public String Directory { get; private set; }
        public String ManifestAdditionsPathname { get; private set; }
    }
}
