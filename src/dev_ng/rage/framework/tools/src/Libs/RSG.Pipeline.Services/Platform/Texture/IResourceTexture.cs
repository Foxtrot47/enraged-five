﻿using System;
using System.Collections.Generic;
using System.Linq;
using RSG.Base.Attributes;

namespace RSG.Pipeline.Services.Platform.Texture
{

    /// <summary>
    /// Resource texture usage.
    /// </summary>
    /// Note: matches ::rage::CTextureConversionResourceTexture::eUsage.
    /// 
    public enum ResourceTextureUsage
    {
        [XmlConstant("usage_Default")]
        Default,

        [XmlConstant("usage_Diffuse")]
        Diffuse,

        [XmlConstant("usage_MipBlend")]
        MipBlend,

        [XmlConstant("usage_Source")]
        Source
    }

    /// <summary>
    /// Resource (exported DDS) texture.
    /// </summary>
    /// Note: matches ::rage::CTextureConversionResourceTexture.
    /// 
    public interface IResourceTexture
    {
        #region Properties
        /// <summary>
        /// Absolute path to resource/export texture.
        /// </summary>
        String AbsolutePath { get; }

        /// <summary>
        /// Resource texture usage.
        /// </summary>
        ResourceTextureUsage Usage { get; }

        /// <summary>
        /// Resource texture source textures.
        /// </summary>
        IEnumerable<ISourceTexture> SourceTextures { get; }
        #endregion // Properties
    }

} // RSG.Pipeline.Processor.Services.Texture namespace
