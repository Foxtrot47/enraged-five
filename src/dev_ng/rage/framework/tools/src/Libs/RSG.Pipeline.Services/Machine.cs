﻿using System;
using System.Management;

namespace RSG.Pipeline.Services
{

    /// <summary>
    /// Machine system information static class.
    /// </summary>
    /// Some of the values are fetched using the Windows Management Interface
    /// (WMI) which is known to be unreliable on XP/Vista/Server 2003.  For
    /// maintainability I've deliberately not used techniques such as those 
    /// described at http://blogs.adamsoftware.net/Engine/DeterminingthenumberofphysicalCPUsonWindows.aspx
    /// to determine core counts etc as we all use Windows 7.
    /// 
    public static class Machine
    {
        #region Static Properties
        /// <summary>
        /// Return true if this process is running in a 64-bit environment; 
        /// false otherwise.
        /// </summary>
        public static bool Is64BitProcess
        {
            get { return Environment.Is64BitProcess; }
        }

        /// <summary>
        /// Return true if this is a 64-bit Windows.
        /// </summary>
        public static bool Is64BitOperatingSystem
        {
            get { return Environment.Is64BitOperatingSystem; }
        }

        /// <summary>
        /// Return true if this is a 32-bit process running on a 64-bit OS.
        /// </summary>
        public static bool IsWow64Process
        {
            get { return Machine.Is64BitOperatingSystem && !Machine.Is64BitProcess; }
        }

        /// <summary>
        /// Return the number of physical processors in the system.
        /// </summary>
        public static int PhysicalProcessorCount
        {
            get;
            private set;
        }

        /// <summary>
        /// Return the number of processing cores in the system.
        /// </summary>
        public static int CoreCount
        {
            get;
            private set;
        }

        /// <summary>
        /// Return the logical processor count in the system.
        /// </summary>
        public static int LogicalProcessorCount
        {
            get { return Environment.ProcessorCount; }
        }
        #endregion // Static Properties

        #region Constructor(s)
        /// <summary>
        /// Static constructor.
        /// </summary>
        static Machine()
        {
            CoreCount = 0;
            PhysicalProcessorCount = 0;

            using (var processors = new ManagementObjectSearcher("Select NumberOfCores from Win32_Processor").Get())
            {
                foreach (ManagementBaseObject obj in processors)
                {
                    CoreCount += Convert.ToInt32(obj["NumberOfCores"]);
                }
            }

            using (var systemObjects = new ManagementObjectSearcher("Select NumberOfProcessors from Win32_ComputerSystem").Get())
            {
                foreach (ManagementBaseObject obj in systemObjects)
                {
                    PhysicalProcessorCount += Convert.ToInt32(obj["NumberOfProcessors"]);
                }
            }
        }
        #endregion // Constructor(s)
    }

} // RSG.Pipeline.Services namespace
