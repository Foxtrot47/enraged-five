﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using XGE = RSG.Interop.Incredibuild.XGE;

namespace RSG.Pipeline.Services
{

    /// <summary>
    /// XGE tool and task factory; for sharing tool and task creation methods
    /// between processors.
    /// </summary>
    public static class XGEFactory
    {
        #region Enumerations
        /// <summary>
        /// Common tool type enumeration.
        /// </summary>
        public enum CommonToolType
        {
            AssetExtract,
            AssetPacker,
            LodGenerator,
            AssetProcessor,
        }
        /// <summary>
        /// Animation tool type enumeration.
        /// </summary>
        public enum AnimationToolType
        {
            ClipGroup,
            Coalesce,
            Compress,
            Extract,
            IngamePostprocess,
            DictionaryMetadata,
            CutsceneMergeLights,
            CutsceneMergeFacial,
            CutsceneSection,
            CutsceneFinaliseCutfile,
            CutscenePostprocess,
            CutsceneConcatenation,
            CutsceneMergeSubtitles,
            CutsceneConcatenationLighting,
            CutsceneInjectDof,
            CutsceneInjectPrefix,
            CutsceneMergeExternal,
            ClipDictionaryProcess,
            CutsceneSliceDice,
            CutsceneMerge,
        }

        /// <summary>
        /// Map tool type enumeration.
        /// </summary>
        public enum MapToolType
        {
            AssetCombineProcessor,
            BoundsProcessor,
            MetadataSerialiser,
            MetadataSerialiser2,
            InstancePlacementProcessor,
            CollisionProcessor,
        }

        /// <summary>
        /// RPF tool type enumeration.
        /// </summary>
        public enum RpfToolType
        {
            RpfCreate,
            RpfManifest
        }

        /// <summary>
        /// Texture tool type enumeration.
        /// </summary>
        public enum TextureToolType
        {
            TextureExport,
        }

        /// <summary>
        /// Material tool type enumeration.
        /// </summary>
        public enum MaterialToolType
        {
            MaterialPresetConverter,
        }

        /// <summary>
        /// Script tool types.
        /// </summary>
        public enum ScriptToolType
        {
            ScriptCompiler,
            ScriptResourceCompiler,
            ScriptResourceCompilerX64
        }
        #endregion // Enumerations

        #region Constants
        #region Map Executables
        /// <summary>
        /// Map metadata serialiser executable path.
        /// </summary>
        private static readonly String MAP_METADATA_SERIALISER = "$(toolsbin)/MapExport/MapExportIDEIPL.exe";

        /// <summary>
        /// Map metadata serialiser (MkII) executable path.
        /// </summary>
        private static readonly String MAP_METADATA_SERIALISER2 = "$(toolsroot)/ironlib/bin/maps/RSG.Pipeline.MapExportMetadata.exe";

        /// <summary>
        /// Map asset combine processor executable path.
        /// </summary>
        private static readonly String MAP_ASSETCOMBINE_PROCESSOR = "$(toolsroot)/ironlib/bin/maps/RSG.Pipeline.MapAssetCombine.exe";

        /// <summary>
        /// Map collision processor executable path.
        /// </summary>
        private static readonly String MAP_BOUNDS_PROCESSOR = "$(toolsroot)/ironlib/bin/maps/RSG.Pipeline.BoundsProcessor.exe";

        /// <summary>
        /// Map instance placement processor executable path.
        /// </summary>
        private static readonly String INSTANCE_PLACEMENT_PROCESSOR = "$(toolsroot)/ironlib/bin/maps/RSG.Pipeline.InstancePlacementProcessor.exe";

        /// <summary>
        /// Material preset processor executable path.
        /// </summary>
        private static readonly String MATERIAL_PRESET_PROCESSOR = "$(toolsroot)/ironlib/bin/RSG.Pipeline.MaterialPresetConverter.exe";

        /// <summary>
        /// Map Collision Processor (MKII) executable path.
        /// </summary>
        private static readonly String MAP_COLLISION_PROCESSOR = "$(toolsroot)/ironlib/bin/maps/RSG.Pipeline.CollisionProcessor.exe";
        #endregion // Map Executables

        #region Animation Executables
        /// <summary>
        /// Animation preparation processor executable path.
        /// </summary>
        private static readonly String TOOL_CLIP_GROUP = "$(toolsroot)/ironlib/bin/anim/RSG.Pipeline.Animation.ClipGroup.exe";
        /// <summary>
        /// Animation coalesce processor executable path
        /// </summary>
        private static readonly String TOOL_COALESCE = "$(toolsroot)/ironlib/bin/anim/RSG.Pipeline.Animation.Coalesce.exe";
        /// <summary>
        /// Animation compress processor executable path
        /// </summary>
        private static readonly String TOOL_COMPRESS = "$(toolsroot)/ironlib/bin/anim/RSG.Pipeline.Animation.Compress.exe";
        /// <summary>
        /// Ingame animation dictionary build processor executable path
        /// </summary>
        private static readonly String TOOL_INGAME_DICTIONARY_METADATA = "$(toolsroot)/ironlib/bin/anim/RSG.Pipeline.Animation.DictionaryMetadata.exe";
        /// <summary>
        /// Ingame animation postprocess processor executable path
        /// </summary>
        private static readonly String TOOL_INGAME_POSTPROCESS = "$(toolsroot)/ironlib/bin/anim/RSG.Pipeline.Animation.Postprocess.exe";
        /// <summary>
        /// Cutscene animation merge lights processor executable path
        /// </summary>
        private static readonly String TOOL_CUTSCENE_MERGE_LIGHTS = "$(toolsroot)/ironlib/bin/anim/RSG.Pipeline.Animation.Cutscene.MergeLights.exe";
        /// <summary>
        /// Cutscene animation merge facial processor executable path
        /// </summary>
        private static readonly String TOOL_CUTSCENE_MERGE_FACIAL = "$(toolsroot)/ironlib/bin/anim/RSG.Pipeline.Animation.Cutscene.MergeFacial.exe";
        /// <summary>
        /// Cutscene animation section processor executable path
        /// </summary>
        private static readonly String TOOL_CUTSCENE_SECTION = "$(toolsroot)/ironlib/bin/anim/RSG.Pipeline.Animation.Cutscene.Section.exe";
        /// <summary>
        /// Cutscene animation cutfile finalisation processor executable path
        /// </summary>
        private static readonly String TOOL_CUTSCENE_FINALISE_CUTFILE = "$(toolsroot)/ironlib/bin/anim/RSG.Pipeline.Animation.Cutscene.FinaliseCutfile.exe";
        /// <summary>
        /// Cutscene animation postprocess processor executable path
        /// </summary>
        private static readonly String TOOL_CUTSCENE_POSTPROCESS = "$(toolsroot)/ironlib/bin/anim/RSG.Pipeline.Animation.Cutscene.Postprocess.exe";
        /// <summary>
        /// Cutscene animation concat processor executable path
        /// </summary>
        private static readonly String TOOL_CUTSCENE_CONCATENATION = "$(toolsroot)/ironlib/bin/anim/RSG.Pipeline.Animation.Cutscene.CutsceneConcatenation.exe";
        /// <summary>
        /// Cutscene light concat processor executable path
        /// </summary>
        private static readonly String TOOL_CUTSCENE_CONCATENATION_LIGHTING = "$(toolsroot)/ironlib/bin/anim/RSG.Pipeline.Animation.Cutscene.CutsceneConcatenationLighting.exe";
        /// <summary>
        /// Cutscene light concat processor executable path
        /// </summary>
        private static readonly String TOOL_CUTSCENE_INJECT_DOF = "$(toolsroot)/ironlib/bin/anim/RSG.Pipeline.Animation.Cutscene.InjectDof.exe";
        /// <summary>
        /// Cutscene light concat processor executable path
        /// </summary>
        private static readonly String TOOL_CUTSCENE_INJECT_PREFIX = "$(toolsroot)/ironlib/bin/anim/RSG.Pipeline.Animation.Cutscene.InjectPrefix.exe";
        /// <summary>
        /// Cutscene animation concat processor executable path
        /// </summary>
        private static readonly String TOOL_CUTSCENE_MERGE_SUBTITLE = "$(toolsroot)/ironlib/bin/anim/RSG.Pipeline.Animation.Cutscene.MergeSubtitle.exe";
        /// <summary>
        /// Cutscene animation concat processor executable path
        /// </summary>
        private static readonly String TOOL_CUTSCENE_MERGE_EXTERNAL = "$(toolsroot)/ironlib/bin/anim/RSG.Pipeline.Animation.Cutscene.MergeExternal.exe";
        /// <summary>
        /// Cutscene animation concat processor executable path
        /// </summary>
        private static readonly String TOOL_CLIPDICTIONARY = "$(toolsroot)/ironlib/bin/anim/RSG.Pipeline.Animation.ClipDictionary.exe";
        /// <summary>
        /// Cutscene animation concat processor executable path
        /// </summary>
        private static readonly String TOOL_CUTSCENE_SLICE_DICE = "$(toolsroot)/ironlib/bin/anim/RSG.Pipeline.Animation.Cutscene.SliceDice.exe";
        /// <summary>
        /// Cutscene animation merge processor executable path
        /// </summary>
        private static readonly String TOOL_CUTSCENE_MERGE = "$(toolsroot)/ironlib/bin/anim/RSG.Pipeline.Animation.Cutscene.CutsceneMerge.exe";
        #endregion // Animation Executables

        #region Common Executables
        /// <summary>
        /// Zip extraction exe
        /// </summary>
        private static readonly String TOOL_ASSET_EXTRACTION = "$(toolsroot)/ironlib/lib/RSG.Pipeline.ZipExtract.exe";

        /// <summary>
        /// Zip creation exe
        /// </summary>
        private static readonly String TOOL_ASSET_PACKER = "$(toolsroot)/ironlib/lib/RSG.Pipeline.ZipCreate.exe";

        /// <summary>
        /// RAGE LOD Generation exe
        /// </summary>
        private static readonly String TOOL_LOD_GENERATION = "$(toolsroot)/ironlib/lib/RSG.Pipeline.Rage.FragmentGenerator.exe";

        /// <summary>
        /// AssetProcessor exe
        /// </summary>
        private const string TOOL_ASSET_PROCESSOR = "$(toolsroot)/ironlib/bin/RSG.Pipeline.AssetProcessor.exe";
        #endregion // Commone Executables

        #region RPF Executables
        /// <summary>
        /// RPF Creator.
        /// </summary>
        private static readonly String TOOL_RPF_CREATE = "$(toolsroot)/ironlib/lib/RSG.Pipeline.RpfCreate.exe";
        
        /// <summary>
        /// RPF Manifest Creator.
        /// </summary>
        private static readonly String TOOL_RPF_MANIFEST = "$(toolsroot)/ironlib/lib/RSG.Pipeline.RpfManifest.exe";
        #endregion // RPF Executables

        #region Texture Executables
        /// <summary>
        /// Texture export executable.
        /// </summary>
        private static readonly String TOOL_TEXTURE_EXPORT = "$(toolsroot)/ironlib/bin/TextureExport.exe";
        #endregion // Texture Executables

        #region Script Executables
        /// <summary>
        /// Script compiler executable.
        /// </summary>
        private static readonly String TOOL_SCRIPT_COMPILER = "$(script)/sc.exe.exe";

        /// <summary>
        /// Script resource compiler executable.
        /// </summary>
        private static readonly String TOOL_SCRIPT_RESOURCE_COMPILER = "$(script)/scriptrc.exe";

        /// <summary>
        /// Script resource compiler executable (x64).
        /// </summary>
        private static readonly String TOOL_SCRIPT_RESOURCE_COMPILER_x64 = "$(script)/scriptrc_x64.exe";
        #endregion // Script Executables
        #endregion // Constants

        #region Static Member Data
        /// <summary>
        /// Static dictionary for storing common executables
        /// </summary>
        private static IDictionary<CommonToolType, String> m_CommonTools =
            new Dictionary<CommonToolType, String>()
        {
            {CommonToolType.AssetExtract, TOOL_ASSET_EXTRACTION},
            {CommonToolType.AssetPacker, TOOL_ASSET_PACKER},
            {CommonToolType.LodGenerator, TOOL_LOD_GENERATION},
            {CommonToolType.AssetProcessor, TOOL_ASSET_PROCESSOR},
        };

        /// <summary>
        /// Static dictionary for storing all animation executables
        /// </summary>
        private static IDictionary<AnimationToolType, String> m_AnimationTools = 
            new Dictionary<AnimationToolType,String>()
        {
            {AnimationToolType.ClipGroup, TOOL_CLIP_GROUP},
            {AnimationToolType.Coalesce, TOOL_COALESCE},
            {AnimationToolType.Compress, TOOL_COMPRESS},
            {AnimationToolType.DictionaryMetadata, TOOL_INGAME_DICTIONARY_METADATA},
            {AnimationToolType.IngamePostprocess, TOOL_INGAME_POSTPROCESS},
            {AnimationToolType.CutsceneMergeLights, TOOL_CUTSCENE_MERGE_LIGHTS},
            {AnimationToolType.CutsceneMergeFacial, TOOL_CUTSCENE_MERGE_FACIAL},
            {AnimationToolType.CutsceneSection, TOOL_CUTSCENE_SECTION},
            {AnimationToolType.CutsceneFinaliseCutfile, TOOL_CUTSCENE_FINALISE_CUTFILE},
            {AnimationToolType.CutscenePostprocess, TOOL_CUTSCENE_POSTPROCESS},
            {AnimationToolType.CutsceneConcatenation, TOOL_CUTSCENE_CONCATENATION},
            {AnimationToolType.CutsceneMergeSubtitles, TOOL_CUTSCENE_MERGE_SUBTITLE},
            {AnimationToolType.CutsceneConcatenationLighting, TOOL_CUTSCENE_CONCATENATION_LIGHTING},
            {AnimationToolType.CutsceneInjectDof, TOOL_CUTSCENE_INJECT_DOF},
            {AnimationToolType.CutsceneInjectPrefix, TOOL_CUTSCENE_INJECT_PREFIX},
            {AnimationToolType.CutsceneMergeExternal, TOOL_CUTSCENE_MERGE_EXTERNAL},
            {AnimationToolType.ClipDictionaryProcess, TOOL_CLIPDICTIONARY},
            {AnimationToolType.CutsceneSliceDice, TOOL_CUTSCENE_SLICE_DICE},
            {AnimationToolType.CutsceneMerge, TOOL_CUTSCENE_MERGE},
        };

        /// <summary>
        /// Static dictionary for storing all map executables.
        /// </summary>
        private static IDictionary<MapToolType, String> m_MapTools =
            new Dictionary<MapToolType, String>()
        {
            { MapToolType.AssetCombineProcessor, MAP_ASSETCOMBINE_PROCESSOR },
            { MapToolType.BoundsProcessor, MAP_BOUNDS_PROCESSOR },
            { MapToolType.MetadataSerialiser, MAP_METADATA_SERIALISER },
            { MapToolType.MetadataSerialiser2, MAP_METADATA_SERIALISER2 },
            { MapToolType.InstancePlacementProcessor, INSTANCE_PLACEMENT_PROCESSOR},
            { MapToolType.CollisionProcessor, MAP_COLLISION_PROCESSOR },
        };

        /// <summary>
        /// Static dictionary for storing all RPF executables.
        /// </summary>
        private static IDictionary<RpfToolType, String> m_RpfTools =
            new Dictionary<RpfToolType, String>()
        {
            { RpfToolType.RpfCreate, TOOL_RPF_CREATE },
            { RpfToolType.RpfManifest, TOOL_RPF_MANIFEST }
        };

        /// <summary>
        /// Static dictionary for storing all texture export executables.
        /// </summary>
        private static IDictionary<TextureToolType, String> m_TextureTools =
            new Dictionary<TextureToolType, String>()
        {
            { TextureToolType.TextureExport, TOOL_TEXTURE_EXPORT }
        };

        /// <summary>
        /// Static dictionary for storing all material export executables.
        /// </summary>
        private static IDictionary<MaterialToolType, String> m_MaterialTools =
            new Dictionary<MaterialToolType, String>()
        {
            {MaterialToolType.MaterialPresetConverter, MATERIAL_PRESET_PROCESSOR},
        };

        /// <summary>
        /// Static dictionary for storing all script executables.
        /// </summary>
        private static IDictionary<ScriptToolType, String> m_ScriptTools =
            new Dictionary<ScriptToolType, String>()
        {
            {ScriptToolType.ScriptCompiler, TOOL_SCRIPT_COMPILER},
            {ScriptToolType.ScriptResourceCompiler, TOOL_SCRIPT_RESOURCE_COMPILER},
            {ScriptToolType.ScriptResourceCompilerX64, TOOL_SCRIPT_RESOURCE_COMPILER_x64}
        };
        #endregion // Static Member Data

        #region Controller Methods
        /// <summary>
        /// Service-wrapper to Start an XGE project.
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="project"></param>
        /// <param name="title"></param>
        /// <param name="logfile"></param>
        /// <param name="rebuild"></param>
        /// <param name="showcmd"></param>
        /// <param name="show_monitor"></param>
        /// <returns></returns>
        public static bool Start(RSG.Base.Configuration.IBranch branch,
            XGE.IProject project, String title, String logfile, bool rebuild = false,
            bool showcmd = true, bool show_monitor = true)
        {
            String filename = Path.Combine(GetTempDirectory(branch),
                String.Format("{0}.xml", project.Name.Replace(' ', '_')));
            project.Write(filename);

            return (XGE.XGE.Start(title, filename, logfile, rebuild, showcmd, show_monitor));
        }

        /// <summary>
        /// Return temporary directory for all XGE builds.
        /// </summary>
        /// <param name="branch"></param>
        /// <returns></returns>
        public static String GetTempDirectory(IBranch branch)
        {
            IProject project = branch.Project;

            String tempDir = Path.Combine(project.Cache, branch.Name, "xge");
            return (tempDir);
        }

        /// <summary>
        /// Clear temporary directory of all files.
        /// </summary>
        /// <param name="branch"></param>
        public static void ClearTempDirectory(ILog log, IBranch branch)
        {
            String tempDir = GetTempDirectory(branch);
            if (Directory.Exists(tempDir))
            {
                try
                {
                    IEnumerable<String> files = Directory.EnumerateFiles(tempDir,
                        "*.*", SearchOption.AllDirectories);
                    foreach (String filename in files)
                        File.Delete(filename);
                    Directory.Delete(tempDir, true);
                    log.Message("XGE temporary cache directory cleared.");
                }
                catch (Exception ex)
                {
                    log.ToolException(ex, "Error clearing XGE temporary cache directory.");
                }
            }
        }

        /// <summary>
        /// Create an XGE tool object for one of the map processors.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="config"></param>
        /// <param name="toolType"></param>
        /// <param name="toolName"></param>
        /// <param name="allow_remote"></param>
        /// <returns></returns>       
        public static XGE.ITool GetMapProcessorTool(IUniversalLog log,
            IConfig config, MapToolType toolType, String toolName, bool allow_remote = true)
        {
            toolName = allow_remote ? toolName : String.Concat(toolName, " [local]");
            String exePath = config.Environment.Subst(m_MapTools[toolType]);

            XGE.Tool tool = new XGE.Tool(toolName, exePath, String.Empty);
            tool.AllowRemote = allow_remote;
            return (tool);
        }

        /// <summary>
        /// Create an XGE tool object for one of the animation processors.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="config"></param>
        /// <param name="toolType"></param>
        /// <param name="toolName"></param>
        /// <param name="allow_remote"></param>
        /// <returns></returns>
        public static XGE.ITool GetAnimationProcessorTool(IUniversalLog log,
            IConfig config, AnimationToolType toolType, String toolName, bool allow_remote = true)
        {
            toolName = allow_remote ? toolName : String.Concat(toolName, " [local]");
            String exePath = config.Environment.Subst(m_AnimationTools[toolType]);

            XGE.Tool tool = new XGE.Tool(toolName, exePath, String.Empty);
            tool.AllowRemote = allow_remote;
            return (tool);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="log"></param>
        /// <param name="config"></param>
        /// <param name="toolType"></param>
        /// <param name="toolName"></param>
        /// <param name="allow_remote"></param>
        /// <returns></returns>
        public static XGE.ITool GetProcessorTool(IUniversalLog log,
            IConfig config, CommonToolType toolType, String toolName, bool allow_remote = true)
        {
            toolName = allow_remote ? toolName : String.Concat(toolName, " [local]");
            String exePath = config.Environment.Subst(m_CommonTools[toolType]);

            XGE.Tool tool = new XGE.Tool(toolName, exePath, String.Empty);
            tool.AllowRemote = allow_remote;
            return (tool);
        }

        /// <summary>
        /// Create an XGE tool object for zip creation.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="config"></param>
        /// <param name="toolName"></param>
        /// <param name="allow_remote"></param>
        /// <returns></returns>
        public static XGE.ITool GetZipTool(IUniversalLog log, IConfig config,
            String toolName, bool allow_remote = true)
        {
            return (GetProcessorTool(log, config, CommonToolType.AssetPacker, toolName, allow_remote));
        }

        /// <summary>
        /// Creates an XGE tool object for AssetProcessor
        /// </summary>
        /// <param name="log"></param>
        /// <param name="config"></param>
        /// <param name="toolName"></param>
        /// <param name="allow_remote"></param>
        /// <returns></returns>
        public static XGE.ITool GetAssetProcessorTool(IUniversalLog log,
            IConfig config,
            string toolName,
            bool allow_remote = true)
        {
            return GetProcessorTool(log, config, CommonToolType.AssetProcessor, toolName, allow_remote);
        }

        /// <summary>
        /// Create an XGE tool object for RPF manipulation.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="config"></param>
        /// <param name="toolName"></param>
        /// <param name="allow_remote"></param>
        /// <returns></returns>
        public static XGE.ITool GetRpfTool(IUniversalLog log, IConfig config,
            String toolName, RpfToolType toolType, bool allow_remote = true)
        {
            toolName = allow_remote ? toolName : String.Concat(toolName, " [local]");
            String exePath = config.Environment.Subst(m_RpfTools[toolType]);

            XGE.Tool tool = new XGE.Tool(toolName, exePath, String.Empty);
            tool.AllowRemote = allow_remote;
            return (tool);
        }

        /// <summary>
        /// Create an XGE tool object for a particular target.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="allow_remote"></param>
        /// <returns></returns>
        public static XGE.ITool GetRagebuilderTool(ITarget target, 
            bool allow_remote = true)
        {
            Debug.Assert(target.Branch.PlatformConversionTools.ContainsKey(target.Platform),
                String.Format("Branch {0} has no conversion tool for platform {1}.", target.Branch.Name, target.Platform));
            if (!target.Branch.PlatformConversionTools.ContainsKey(target.Platform))
                return (null);

            String toolName = String.Empty;
            if (allow_remote)
                toolName = String.Format("Ragebuilder Convert [{0}]", target.Platform);
            else
                toolName = String.Format("Ragebuilder Convert [{0}, local]", target.Platform);
            String conversionToolPath = target.GetRagebuilderExecutable();

            XGE.Tool tool = new XGE.Tool(toolName);
            tool.Path = conversionToolPath;
            tool.AllowRemote = allow_remote;
            return (tool);
        }

        /// <summary>
        /// Create an XGE tool object for a texture tool.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="config"></param>
        /// <param name="toolName"></param>
        /// <param name="toolType"></param>
        /// <param name="allow_remote"></param>
        /// <returns></returns>
        public static XGE.ITool GetTextureTool(IUniversalLog log, IConfig config,
            String toolName, TextureToolType toolType, bool allow_remote = true)
        {
            toolName = allow_remote ? toolName : String.Concat(toolName, " [local]");
            String exePath = config.Environment.Subst(m_TextureTools[toolType]);

            XGE.Tool tool = new XGE.Tool(toolName, exePath, String.Empty);
            tool.AllowRemote = allow_remote;
            return (tool);
        }

        /// <summary>
        /// Create an XGE tool object for a material tool.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="config"></param>
        /// <param name="toolName"></param>
        /// <param name="toolType"></param>
        /// <param name="allow_remote"></param>
        /// <returns></returns>
        public static XGE.ITool GetMaterialTool(IUniversalLog log, IConfig config,
            String toolName, MaterialToolType toolType, bool allow_remote = true)
        {
            toolName = allow_remote ? toolName : String.Concat(toolName, " [local]");
            String exePath = config.Environment.Subst(m_MaterialTools[toolType]);

            XGE.Tool tool = new XGE.Tool(toolName, exePath, String.Empty);
            tool.AllowRemote = allow_remote;
            return (tool);
        }

        /// <summary>
        /// Create a XGE tool object for a script tool.
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="toolName"></param>
        /// <param name="toolType"></param>
        /// <param name="allow_remote"></param>
        /// <returns></returns>
        public static XGE.ITool GetScriptTool(IBranch branch, String toolName, 
            ScriptToolType toolType, bool allow_remote = true)
        {
            toolName = allow_remote ? toolName : String.Concat(toolName, " [local]");
            String exePath = branch.Environment.Subst(m_ScriptTools[toolType]);

            XGE.Tool tool = new XGE.Tool(toolName, exePath, String.Empty);
            tool.AllowRemote = allow_remote;
            return (tool);
        }
        #endregion // Controller Methods
    }

} // RSG.Pipeline.Services namespace
