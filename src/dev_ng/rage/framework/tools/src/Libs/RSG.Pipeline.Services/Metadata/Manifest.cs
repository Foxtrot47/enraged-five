﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;
using SIO = System.IO;

namespace RSG.Pipeline.Services.Metadata
{
    /// <summary>
    /// Manifest helper functions
    /// </summary>
    public static class Manifest
    {
        #region Public Methods
        /// <summary>
        /// Create manifest file.
        /// </summary>
        /// <param name="scenePathnames"></param>
        /// <returns></returns>
        public static XDocument CreateManifestFile(IEnumerable<String> scenePathnames)
        {
            XDocument document = new XDocument(new XDeclaration("1.0", "utf-8", null));
            StringBuilder description = new StringBuilder();
            description.Append("IMAP file generated from SceneXml files:\n");
            foreach (String scenePathname in scenePathnames)
                description.AppendFormat("\t\t{0}{1}", scenePathname, Environment.NewLine);
            XComment xmlDescription = new XComment(description.ToString());
            document.Add(xmlDescription);

            XElement rootElem = new XElement("ManifestData");
            document.Add(rootElem);

            return (document);
        }

        /// <summary>
        /// Save manifest data.
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="manifestAdditionsFilename"></param>
        public static void SaveManifestFile(XDocument xmlDoc, String manifestAdditionsFilename)
        {
            if (SIO.File.Exists(manifestAdditionsFilename))
                SIO.File.Delete(manifestAdditionsFilename);

            String manifestAdditionsDirectory = SIO.Path.GetDirectoryName(manifestAdditionsFilename);
            if (!SIO.Directory.Exists(manifestAdditionsDirectory))
            {
                SIO.Directory.CreateDirectory(manifestAdditionsDirectory);
            }
            xmlDoc.Save(manifestAdditionsFilename);
        }
        #endregion // Public Methods
    }
}
