﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Pipeline.Core;
using RSG.Pipeline.Content;

namespace RSG.Pipeline.Services.Platform
{

    /// <summary>
    /// Abstraction of a packet of work for the Rage processor to pass to XGE.
    /// </summary>
    public class RagePacket
    {
        #region Properties
        /// <summary>
        /// Packet target.
        /// </summary>
        public ITarget Target
        {
            get;
            private set;
        }

        /// <summary>
        /// Collection of processes for this packet.
        /// </summary>
        public ICollection<IProcess> Processes
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor; specifying packet target.
        /// </summary>
        public RagePacket(ITarget target)
        {
            this.Target = target;
            this.Processes = new List<IProcess>();
        }

        /// <summary>
        /// Constructor; specfying packet target and processes.
        /// </summary>
        /// <param name="processes"></param>
        public RagePacket(ITarget target, IEnumerable<IProcess> processes)
            : this(target)
        {
            this.Processes.AddRange(processes);
        }
        #endregion // Constructor(s)

        #region Static Utility Methods
        /// <summary>
        /// Return process input data size (in bytes) for a series of processes.
        /// </summary>
        /// <param name="processes"></param>
        /// <returns></returns>
        public static long GetDataSize(IEnumerable<IProcess> processes)
        {
#if ONCE_WE_HAVE_A_CLEVERER_METHOD
            // This takes into account all filesystem input sizes; as the DDS
            // files etc are inputs we end up with lots more packets than we
            // would normally so its disable for now.
            IEnumerable<IFilesystemNode> nodes = GetValidFilesystemInputs(processes);
            return (nodes.Sum(n => n.GetSize()));
#else
            IEnumerable<long> sizes = processes.Select(p => 
                p.Inputs.OfType<IFilesystemNode>().First().GetSize());
            return (sizes.Sum());
#endif
        }

        /// <summary>
        /// Return process input data size (in bytes) for a single process.
        /// </summary>
        /// <param name="process"></param>
        /// <returns></returns>
        public static long GetDataSize(IProcess process)
        {
#if ONCE_WE_HAVE_A_CLEVERER_METHOD
            // This takes into account all filesystem input sizes; as the DDS
            // files etc are inputs we end up with lots more packets than we
            // would normally so its disable for now.
            IEnumerable<IFilesystemNode> nodes = GetValidFilesystemInputs(process);
            return (nodes.Sum(n => n.GetSize()));
#else
            return (process.Inputs.OfType<IFilesystemNode>().First().GetSize());
#endif
        }

        /// <summary>
        /// Return number of inputs for a Rage process; ignoring inputs that
        /// aren't going to be converted.
        /// </summary>
        /// <param name="processes"></param>
        /// <returns></returns>
        public static int GetInputCount(IEnumerable<IProcess> processes)
        {
#if ONCE_WE_HAVE_A_CLEVERER_METHOD
            // This takes into account all filesystem input count; as the DDS
            // files etc are inputs we end up with lots more packets than we
            // would normally so its disable for now.
            IEnumerable<IFilesystemNode> nodes = GetValidFilesystemInputs(processes);
            return (nodes.Count());
#else
            return (processes.Count());
#endif
        }

        /// <summary>
        /// Return number of inputs for a Rage process; ignoring inputs that
        /// aren't going to be converted.
        /// </summary>
        /// <param name="process"></param>
        /// <returns></returns>
        public static int GetInputCount(IProcess process)
        {
#if ONCE_WE_HAVE_A_CLEVERER_METHOD
            // This takes into account all filesystem input count; as the DDS
            // files etc are inputs we end up with lots more packets than we
            // would normally so its disable for now.
            IEnumerable<IFilesystemNode> nodes = GetValidFilesystemInputs(process);
            return (nodes.Count());
#else
            return (1);
#endif
        }
        #endregion // Static Utility Methods

        #region Private Static Methods
        /// <summary>
        /// Return IContentNodes for a Rage process that are valid for packet
        /// calculations.
        /// </summary>
        /// <param name="processes"></param>
        /// <returns></returns>
        private static IEnumerable<IFilesystemNode> GetValidFilesystemInputs(IEnumerable<IProcess> processes)
        {
            IEnumerable<IFilesystemNode> nodes = processes.SelectMany(p => GetValidFilesystemInputs(p));
            return (nodes);
        }

        /// <summary>
        /// Return IContentNodes for a Rage process that are valid for packet
        /// calculations.
        /// </summary>
        /// <param name="process"></param>
        /// <returns></returns>
        private static IEnumerable<IFilesystemNode> GetValidFilesystemInputs(IProcess process)
        {
            IEnumerable<IFilesystemNode> nodes = process.Inputs.OfType<IFilesystemNode>();
            return (nodes);
        }
        #endregion // Private Static Methods
    }

} // RSG.Pipeline.Services.Platform namespace
