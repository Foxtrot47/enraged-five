﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using RSG.Metadata.Model;
using RSG.Metadata.Parser;
using RSG.Metadata.Data;
using RSG.Base.Configuration;
using RSG.Base.Logging.Universal;

namespace RSG.Pipeline.Services.Animation
{
    /// <summary>
    /// Singleton class for handling the dictionary modification metadata
    /// </summary>
    public sealed class DictionaryModManager
    {
        #region Constants
        /// <summary>
        /// The singleton instance.
        /// </summary>
        private static readonly DictionaryModManager _Instance = new DictionaryModManager();

        /// <summary>
        /// Dictionary namespace.
        /// </summary>
        private static readonly String TUNABLE_ROOT_NAME = "namespace";

        /// <summary>
        /// Dictionaries within a namespace.
        /// </summary>
        private static readonly String TUNABLE_DICTIONARIES = "dictionaries";

        /// <summary>
        /// Dictionary name tunable key.
        /// </summary>
        private static readonly String TUNABLE_DICT_NAME = "dictionaryname";

        /// <summary>
        /// Dictionary inclusions tunable key.
        /// </summary>
        private static readonly String TUNABLE_INCLUSIONS = "inclusions";

        /// <summary>
        /// Dictionary exclusions tunable key.
        /// </summary>
        private static readonly String TUNABLE_EXCLUSIONS = "exclusions";

        /// <summary>
        /// Dictionary filename tunable key.
        /// </summary>
        private static readonly String TUNABLE_PATH = "path";

        /// <summary>
        /// Dictionary listings tunable key.
        /// </summary>
        private static readonly String TUNABLE_LISTINGS = "listings";

        /// /// <summary>
        /// Dictionary name basename tunable key.
        /// </summary>
        private static readonly String TUNABLE_BASENAME = "basename";

        /// <summary>
        /// Dictionary name override tunable key.
        /// </summary>
        private static readonly String TUNABLE_OVERRIDENAME = "overridename";

        /// <summary>
        /// Processor log context.
        /// </summary>
        private static readonly String LOG_CTX = "Animation:DictionaryModificationManager";
        #endregion // Constants

        #region Fields
        private Dictionary<String, DictionaryUnitCollection> m_DictionaryUnits;
        private IEnumerable<String> m_ExcludedDictionaries;
        private static bool m_Initialised = false;
        #endregion // Fields

        #region Properties
        /// <summary>
        /// The singleton instance accessor.
        /// </summary>
        public static DictionaryModManager Instance
        {
            get { return _Instance; }
        }

        public Dictionary<String, DictionaryUnitCollection> DictionaryUnits
        {
            get { return m_DictionaryUnits; }
        }
        
        /*
        /// <summary>
        /// List of dictionaries that have been setup for modifications
        /// </summary>
        public DictionaryUnitCollection DictionaryUnits
        {
            get { return _DictionaryUnits; }
            set { _DictionaryUnits = value; }
        }
        DictionaryUnitCollection _DictionaryUnits;
        */
        /// <summary>
        /// List of dictionaries that we want to remove from the build process
        /// </summary>
        public IEnumerable<String> ExcludedDictionaries
        {
            get { return m_ExcludedDictionaries; }
           // set { m_ExcludedDictionaries = value; }
        }
        #endregion // Properties

        #region Public Methods
        /// <summary>
        /// Entry point for loading up the metadata and populating our data structures
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="dictionaryModFile"></param>
        /// <param name="log"></param>
        public bool InitWith(IBranch branch, String dictionaryModFile, IUniversalLog log)
        {
            if (m_Initialised)
                return true;

            bool retval = false;
            try
            {
                log.ProfileCtx(LOG_CTX, "Loading animation dictionary modification metadata.");
                StructureDictionary sd = new StructureDictionary();
                String animationPSC = Path.Combine(branch.Metadata, "definitions", "tools", "animation");
                sd.Load(branch, animationPSC);                

                Structure s = sd["CDictionaryDefs"];
                MetaFile mf = new MetaFile(s);
                //MetaFile mf = new MetaFile();
                mf.Deserialise(dictionaryModFile);
                log.ProfileEnd();

                ITunable excludedTunableArray = RSG.Metadata.Util.Tunable.FindFirstStuctureNamed( "excludedDictionaries", mf.Members );
                ITunable editedTunableArray = RSG.Metadata.Util.Tunable.FindFirstStuctureNamed( "editedDictionaries", mf.Members );

                // Collect dictionary exclusions
                List<String> excludedDictionaries = new List<String>();
                foreach ( ITunable tunable in (excludedTunableArray as ArrayTunable).Items )
                {
                    excludedDictionaries.Add((tunable as StringTunable).Value.ToLower(System.Globalization.CultureInfo.CurrentCulture));
                }

                // Collect edited dictionaries
                Dictionary<String, DictionaryUnitCollection> dictionaryUnitsByNamespace = new Dictionary<String, DictionaryUnitCollection>();
                foreach ( ITunable tunable in (editedTunableArray as ArrayTunable).Items )
                {
                    StructureTunable namespaceEntry = tunable as RSG.Metadata.Data.StructureTunable;
                    String rootName = (namespaceEntry[TUNABLE_ROOT_NAME] as StringTunable).Value;

                    ArrayTunable dictionaries = (namespaceEntry[TUNABLE_DICTIONARIES] as ArrayTunable);
                    DictionaryUnitCollection dictionaryUnits = new DictionaryUnitCollection();
                    foreach (ITunable tunableDict in (dictionaries as ArrayTunable).Items)
                    {
                        StructureTunable dictEntry = tunableDict as RSG.Metadata.Data.StructureTunable;

                        String dictionaryName = (dictEntry[TUNABLE_DICT_NAME] as StringTunable).Value;
                        IDictionaryUnit dictionaryUnit = new DictionaryUnit(rootName, dictionaryName);

                        ArrayTunable inclusionsTunable = (dictEntry[TUNABLE_INCLUSIONS] as ArrayTunable);
                        foreach (ITunable inclusionTunable in (inclusionsTunable as ArrayTunable).Items)
                        {
                            StringTunable entryPath = (inclusionTunable as StructureTunable)[TUNABLE_PATH] as StringTunable;

                            ArrayTunable listingsTunable = ((inclusionTunable as StructureTunable)[TUNABLE_LISTINGS] as ArrayTunable);
                            foreach (ITunable listingTunable in (listingsTunable as ArrayTunable).Items)
                            {
                                StringTunable entryBasename = (listingTunable as StructureTunable)[TUNABLE_BASENAME] as StringTunable;
                                StringTunable overrideName = (listingTunable as StructureTunable)[TUNABLE_OVERRIDENAME] as StringTunable;
                                if (entryBasename.Value.ToLower().Equals(overrideName.Value.ToLower()) || overrideName.Value == null)
                                    overrideName.Value = String.Empty;

                                DictionaryItem dictionaryItem = new DictionaryItem(entryPath.Value, entryBasename.Value, overrideName.Value);
                                dictionaryUnit.AddInclusion(dictionaryItem);
                            }
                        }
                        ArrayTunable exclusionsTunable = (dictEntry[TUNABLE_EXCLUSIONS] as ArrayTunable);
                        foreach (ITunable exclusionTunable in (exclusionsTunable as ArrayTunable).Items)
                        {
                            dictionaryUnit.AddExclusion((exclusionTunable as StringTunable).Value.ToLower(System.Globalization.CultureInfo.CurrentCulture));
                        }
                        dictionaryUnits.Add(dictionaryUnit);
                    }
                    dictionaryUnitsByNamespace.Add(rootName.ToLower(), dictionaryUnits);
                }
               
                m_ExcludedDictionaries = excludedDictionaries;
                m_DictionaryUnits = dictionaryUnitsByNamespace;
            }
            catch ( Exception ex )
            {
                log.ExceptionCtx(LOG_CTX, ex, "Exception loading dictionary modification metadata  from {0}.", dictionaryModFile);
            }
            m_Initialised = true;
            return retval;
        }
        #endregion // Public Methods

        #region Private Methods

        #endregion // Private Methods

        #region Constructor
        /// <summary>
        /// Private constructor
        /// </summary>
        private DictionaryModManager()
        {
            m_DictionaryUnits = new Dictionary<String,DictionaryUnitCollection>();
            m_ExcludedDictionaries = new List<String>();
        }
        #endregion // Constructor
    }
}
