﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using RSG.Base.Extensions;
using RSG.Model.Statistics.Platform;

namespace RSG.Pipeline.Services.Platform
{

    /// <summary>
    /// Resource statistics rolling log for streaming resource statistics out of
    /// the Rage processor.
    /// </summary>
    public class ResourceStatsLogFile
    {
        #region Constants
        /// <summary>
        /// Default filename extension String for resource statistics log file.
        /// </summary>
        private static readonly String EXTENSION = "xml";

        /// <summary>
        /// Wildcard used to discover the resource stats filenames.
        /// </summary>
        public static readonly String RESOURCE_STATS_LOGFILE_WILDCARD = "resource_statistics*.xml";

        /// <summary>
        /// Filename of resource stats
        /// </summary>
        public static readonly String RESOURCE_STATS_LOGFILE_FILENAME = "resource_statistics.xml";
        
        /// <summary>
        /// File size byte limit; before roll.
        /// </summary>
        public static readonly int FILE_SIZE_LIMIT_DEFAULT = 10 * 1024 * 1024; // 10MB
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Log filename on disk.
        /// </summary>
        public String Filename
        {
            get;
            private set;
        }

        /// <summary>
        /// Number of bytes for each file before it rolls to the next (in bytes).
        /// </summary>
        public long RollingSizeLimit
        {
            get;
            private set;
        }

        /// <summary>
        /// Number of ResourceBucketStat objects to buffer prior to flush.
        /// </summary>
        public int BufferSize
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Internal message buffer.
        /// </summary>
        private ICollection<ResourceBucketStat> m_BufferedStats;

        /// <summary>
        /// Rolling counter; to easily get new filename.
        /// </summary>
        private static uint ms_RollingCount = 0;
        #endregion // Member Data

        #region Constructor(s) / Destructor
        /// <summary>
        /// Constructor; opens log for append.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="rollingByteSize"></param>
        /// <param name="bufferSize"></param>
        public ResourceStatsLogFile(String filename, long rollingByteSize = 10 * 1024 * 1024, int bufferSize = 100)
        {
            this.Filename = filename;
            this.RollingSizeLimit = rollingByteSize;
            this.BufferSize = bufferSize;
            this.m_BufferedStats = new List<ResourceBucketStat>();

            String directory = Path.GetDirectoryName(this.Filename);
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);
        }

        /// <summary>
        /// Destructor.
        /// </summary>
        ~ResourceStatsLogFile()
        {
            this.Flush();
        }
        #endregion // Constructor(s) / Destructor

        #region Controller Methods
        /// <summary>
        /// Add ResourceBucketStat objects.
        /// </summary>
        /// <param name="stats"></param>
        public void AddResourceBucketStats(IEnumerable<ResourceBucketStat> stats)
        {
            lock (this.m_BufferedStats)
            {
                this.m_BufferedStats.AddRange(stats);
                if (this.m_BufferedStats.Count > this.BufferSize)
                    this.Flush();
            }
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Flush; optionally creating an empty file.
        /// </summary>
        private void Flush()
        {
            XDocument xmlDoc = null;
            if (File.Exists(this.Filename))
            {
                try
                {
                    xmlDoc = XDocument.Load(this.Filename);
                }
                catch (Exception ex)
                {
                    try
                    {
                        // Try moving the invalid/corrupt file.  Ensure
                        // we catch... if that fails it will get overwritten
                        // in the next step.  Such as life.
                        String invalidFilename = Path.ChangeExtension(
                            this.Filename, "_CORRUPT.xml");
                        File.Move(this.Filename, invalidFilename);
                        xmlDoc = null;
                    }
                    catch (Exception ex2) { }
                }
            }
            
            if (null == xmlDoc)
            {
                xmlDoc = new XDocument(
                    new XDeclaration("1.0", "utf-8", "yes"),
                    new XElement("ResourceStatBuckets")
                );
            }

            lock (this.m_BufferedStats)
            {
                String directory = Path.GetDirectoryName(this.Filename);
                if (!Directory.Exists(directory))
                    Directory.CreateDirectory(directory);

                try
                {
                    foreach (ResourceBucketStat stat in this.m_BufferedStats)
                    {
                        xmlDoc.Root.Add(stat.Serialise());
                    }
                    using (FileStream fs = new FileStream(this.Filename, FileMode.Create, FileAccess.Write))
                        xmlDoc.Save(fs);
                    this.m_BufferedStats.Clear();

                    FileInfo fi = new FileInfo(this.Filename);
                    if (fi.Length >= this.RollingSizeLimit)
                    {
                        String filename = Path.GetFileName(this.Filename);
                        filename = Path.ChangeExtension(filename, String.Format("{0}.{1}",
                            ms_RollingCount++, EXTENSION));
                        String archiveFilename = Path.Combine(directory, filename);
                        fi.MoveTo(archiveFilename);
                    }
                }
                catch (Exception e) { }
            }
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Services.Platform namespace
