﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;

using RSG.Base.Math;
using RSG.Metadata.Util;

namespace RSG.Pipeline.Services.InstancePlacement
{
    public class InstancePlacementStat
    {
        #region Properties
        /// <summary>
        /// Total number of batches.
        /// </summary>
        public int TotalBatchCount
        {
            get;
            protected set;
        }

        /// <summary>
        /// Total number of instances.
        /// </summary>
        public int TotalInstanceCount
        {
            get;
            protected set;
        }

        /// <summary>
        /// Minimum number of instances.
        /// </summary>
        public int MinInstances
        {
            get;
            protected set;
        }

        /// <summary>
        /// Maximum number of instances.
        /// </summary>
        public int MaxInstances
        {
            get;
            protected set;
        }

        /// <summary>
        /// Average number of instance.
        /// </summary>
        public int AverageInstances
        {
            get;
            protected set;
        }

        /// <summary>
        /// Total occupancy.
        /// </summary>
        public float TotalOccupancy
        {
            get;
            protected set;
        }

        /// <summary>
        /// Minimum occupancy.
        /// </summary>
        public float MinOccupancy
        {
            get;
            protected set;
        }

        /// <summary>
        /// Maximum occupancy.
        /// </summary>
        public float MaxOccupancy
        {
            get;
            protected set;
        }

        /// <summary>
        /// Average occupancy.
        /// </summary>
        public float AverageOccupancy
        {
            get;
            protected set;
        }

        /// <summary>
        /// Total batch bounding box volume (m3).
        /// </summary>
        public float TotalVolume
        {
            get;
            protected set;
        }

        /// <summary>
        /// Minimum batch bounding box volume (m3).
        /// </summary>
        public float MinVolume
        {
            get;
            protected set;
        }

        /// <summary>
        /// Maximum batch bounding box volume (m3).
        /// </summary>
        public float MaxVolume
        {
            get;
            protected set;
        }

        /// <summary>
        /// Average batch bounding box volume (m3).
        /// </summary>
        public float AverageVolume
        {
            get;
            protected set;
        }

        /// <summary>
        /// Total batch bound size for this archetype.
        /// </summary>
        public Vector3f TotalBoundSize
        {
            get;
            protected set;
        }

        /// <summary>
        /// Min batch bound size for this archetype.
        /// </summary>
        public Vector3f MinBoundSize
        {
            get;
            protected set;
        }

        /// <summary>
        /// Max batch bound size for this archetype.
        /// </summary>
        public Vector3f MaxBoundSize
        {
            get;
            protected set;
        }

        /// <summary>
        /// Total batch bound size for this archetype.
        /// </summary>
        public Vector3f AverageBoundSize
        {
            get;
            protected set;
        }

        /// <summary>
        /// Bounding box for this batch.
        /// </summary>
        public BoundingBox3f BoundingBox
        {
            get;
            protected set;
        }
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public InstancePlacementStat()
        {
            TotalBatchCount = 0;

            TotalInstanceCount = 0;
            MinInstances = int.MaxValue;
            MaxInstances = 0;
            AverageInstances = 0;

            TotalOccupancy = 0;
            MinOccupancy = float.MaxValue;
            MaxOccupancy = 0;
            AverageOccupancy = 0;

            TotalVolume = 0.0f;
            MinVolume = float.MaxValue;
            MaxVolume = 0.0f;
            AverageVolume = 0.0f;

            Vector3f maxVector = new Vector3f(float.MaxValue, float.MaxValue, float.MaxValue);
            Vector3f zeroVector = new Vector3f(0, 0, 0); 

            TotalBoundSize = zeroVector;
            MinBoundSize = maxVector;
            MaxBoundSize = zeroVector;
            AverageBoundSize = zeroVector;

            BoundingBox = new BoundingBox3f();
        }
        #endregion

        #region Methods
        /// <summary>
        /// Add a list of batches to our total batch.
        /// </summary>
        /// <param name="batchList"></param>
        public void AddInstanceBatches(IEnumerable<InstancePlacementArchetypeStat> batchList)
        {
            foreach (InstancePlacementArchetypeStat archetypeStat in batchList)
                AddInstanceBatch(archetypeStat.TotalInstanceCount, archetypeStat.TotalOccupancy, archetypeStat.TotalVolume, archetypeStat.BoundingBox, archetypeStat.TotalBatchCount);
        }

        /// <summary>
        /// Add a new batch of instances and calculate our min/max/average instances.
        /// </summary>
        /// <param name="instanceCount"></param>
        public void AddInstanceBatch(int instanceCount, float occupancy, float volume, BoundingBox3f bounds, int batchCount = -1)
        {
            BoundingBox = bounds;

            if (batchCount == -1)
                TotalBatchCount++;
            else
                TotalBatchCount += batchCount;

            if (instanceCount < MinInstances)
                MinInstances = instanceCount;
            if (instanceCount > MaxInstances)
                MaxInstances = instanceCount;

            TotalInstanceCount += instanceCount;
            AverageInstances = TotalInstanceCount / TotalBatchCount;

            if (occupancy < MinOccupancy)
                MinOccupancy = occupancy;
            if (occupancy > MaxOccupancy)
                MaxOccupancy = occupancy;

            TotalOccupancy += occupancy;
            AverageOccupancy = TotalOccupancy / TotalBatchCount;

            Vector3f boundsExtents = bounds.Max - bounds.Min;

            float batchVolume = boundsExtents.X * boundsExtents.Y * boundsExtents.Z;

            if (batchVolume < MinVolume)
            {
                MinVolume = batchVolume;
                MinBoundSize = boundsExtents;
            }

            if (batchVolume > MaxVolume)
            {
                MaxVolume = batchVolume;
                MaxBoundSize = boundsExtents;
            }

            TotalBoundSize += boundsExtents;
            AverageBoundSize = TotalBoundSize / TotalBatchCount;

            TotalVolume += batchVolume;
            AverageVolume = TotalVolume / TotalBatchCount;
        }
        #endregion
    }

    /// <summary>
    /// Class to handle all the batch lists per archetype.
    /// </summary>
    public class InstancePlacementContainerStat : InstancePlacementStat
    {
        #region Properties
        /// <summary>
        /// The name of the container we're storing the stat for.
        /// </summary>
        public String ContainerName
        {
            get;
            private set;
        }

        /// <summary>
        /// The name of the region we're storing the stat for.
        /// </summary>
        public String RegionName
        {
            get;
            private set;
        }

        /// <summary>
        /// Store by archetype all the batches we have.
        /// </summary>
        public Dictionary<String, List<InstancePlacementArchetypeStat>> ContainerStats
        {
            get;
            private set;
        }
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public InstancePlacementContainerStat(string containerName, string regionName)
            : base()
        {
            ContainerName = containerName;
            RegionName = regionName;
            ContainerStats = new Dictionary<string, List<InstancePlacementArchetypeStat>>();
        }
        #endregion

        #region Methods
        /// <summary>
        /// Add a new batch stat based on the archetype name.
        /// </summary>
        /// <param name="archetype"></param>
        /// <param name="batchStat"></param>
        public void AddBatchStats(string archetype, InstancePlacementArchetypeStat batchStat)
        {
            if (!ContainerStats.ContainsKey(archetype))
                ContainerStats.Add(archetype, new List<InstancePlacementArchetypeStat>());

            ContainerStats[archetype].Add(batchStat);
            AddInstanceBatch(batchStat.TotalInstanceCount, batchStat.TotalOccupancy, batchStat.TotalVolume, batchStat.BoundingBox, -1);
        }
        #endregion
    }

    /// <summary>
    /// Class to handle the stat per archetype.
    /// Calculates min/max/average stats.
    /// </summary>
    public class InstancePlacementArchetypeStat : InstancePlacementStat
    {
        #region Properties
        /// <summary>
        /// Name of the archetype.
        /// </summary>
        public String ArchetypeName
        {
            get;
            private set;
        }

        /// <summary>
        /// Colour for this batch.
        /// Used to render out vector maps for the batches.
        /// </summary>
        public Color ListColour
        {
            get;
            set;
        }
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="archetypeName"></param>
        public InstancePlacementArchetypeStat(string archetypeName)
            : base()
        {
            Vector3f zeroVector = new Vector3f(0, 0, 0);

            TotalBoundSize = zeroVector;
            MinBoundSize = zeroVector;
            MaxBoundSize = zeroVector;
            AverageBoundSize = zeroVector;

            ArchetypeName = archetypeName;
        }

        /// <summary>
        /// Build a new archetype stat with a collection of archetypes.
        /// </summary>
        /// <param name="archetypeName"></param>
        /// <param name="batchList"></param>
        public InstancePlacementArchetypeStat(string archetypeName, IEnumerable<InstancePlacementArchetypeStat> batchList)
        {
            ArchetypeName = archetypeName;
            AddInstanceBatches(batchList);
        }
        #endregion
    }

    /// <summary>
    /// Helper class to load/save the container stats file.
    /// </summary>
    public class InstancePlacementStatsUtil
    {
        #region Static Methods
        /// <summary>
        /// Load a container specific stats file.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static InstancePlacementContainerStat LoadContainerStatsFile(string filename)
        {
            XDocument xmlDoc = XDocument.Load(filename);

            string container = xmlDoc.Root.Attribute("container").Value;
            string region = xmlDoc.Root.Attribute("region").Value;

            IEnumerable<XElement> archetypeEntries =
                    xmlDoc.Root.XPathSelectElements("/InstancePlacementStats/Archetype");

            InstancePlacementContainerStat containerStat = new InstancePlacementContainerStat(container, region);

            foreach (XElement archetypeEntry in archetypeEntries)
            {
                string archetypeName = archetypeEntry.Attribute("name").Value;
                IEnumerable<XElement> batchElements = archetypeEntry.Elements("Batch");

                foreach (XElement batchElement in batchElements)
                {
                    InstancePlacementArchetypeStat archetypeStat = new InstancePlacementArchetypeStat(archetypeName);

                    int instanceCount = int.Parse(batchElement.Attribute("InstanceCount").Value);
                    float occupancy = float.Parse(batchElement.Attribute("Occupancy").Value);
                    float volume = float.Parse(batchElement.Attribute("Volume").Value);

                    XElement batchBoundsElement = batchElement.Element("BatchBounds");

                    XElement boundsMinElement = batchBoundsElement.Element("Min");
                    XElement boundsMaxElement = batchBoundsElement.Element("Max");

                    float boundsMinX = float.Parse(boundsMinElement.Attribute("X").Value);
                    float boundsMinY = float.Parse(boundsMinElement.Attribute("Y").Value);
                    float boundsMinZ = float.Parse(boundsMinElement.Attribute("Z").Value);

                    float boundsMaxX = float.Parse(boundsMaxElement.Attribute("X").Value);
                    float boundsMaxY = float.Parse(boundsMaxElement.Attribute("Y").Value);
                    float boundsMaxZ = float.Parse(boundsMaxElement.Attribute("Z").Value);

                    BoundingBox3f batchBounds = new BoundingBox3f(new Vector3f(boundsMinX, boundsMinY, boundsMinZ), new Vector3f(boundsMaxX, boundsMaxY, boundsMaxZ));

                    archetypeStat.AddInstanceBatch(instanceCount, occupancy, volume, batchBounds);
                    containerStat.AddBatchStats(archetypeName, archetypeStat);
                }
            }

            return containerStat;
        }

        /// <summary>
        /// Save a container specific stats file.
        /// </summary>
        /// <param name="statsFilename"></param>
        /// <param name="vectorBitmapFilename"></param>
        /// <param name="region"></param>
        /// <param name="worldExtents"></param>
        /// <param name="containerStat"></param>
        public static void SaveContainerStatsFile(string statsFilename, string vectorBitmapFilename, BoundingBox3f worldExtents, InstancePlacementContainerStat containerStat)
        {
            if (!String.IsNullOrEmpty(statsFilename))
            {
                XDocument doc = new XDocument();
                XElement baseElement = new XElement("InstancePlacementStats",
                                                    new XAttribute("container", containerStat.ContainerName),
                                                    new XAttribute("region", containerStat.RegionName));
                doc.Add(baseElement);

                baseElement.Add(new XElement("TotalBatches", containerStat.TotalBatchCount),
                                new XElement("TotalInstances", containerStat.TotalInstanceCount),

                                // Batch Stats
                                new XElement("MinBatchSize", containerStat.MinInstances),
                                new XElement("MaxBatchSize", containerStat.MaxInstances),
                                new XElement("AverageBatchSize", containerStat.AverageInstances),

                                // Occupancy Stats
                                new XElement("MinOccupancy", containerStat.MinOccupancy),
                                new XElement("MaxOccupancy", containerStat.MaxOccupancy),
                                new XElement("AverageOccupancy", containerStat.AverageOccupancy),

                                // Volume Stats
                                new XElement("MinVolume", containerStat.MinVolume),
                                new XElement("MaxVolume", containerStat.MaxVolume),
                                new XElement("AverageVolume", containerStat.AverageVolume));

                foreach (KeyValuePair<String, List<InstancePlacementArchetypeStat>> kvp in containerStat.ContainerStats)
                {
                    XElement archetypeElement = new XElement("Archetype", new XAttribute("name", kvp.Key));

                    foreach (InstancePlacementArchetypeStat entry in kvp.Value)
                    {
                        Vector3f boundsExtents = entry.BoundingBox.Max - entry.BoundingBox.Min;

                        float volume = boundsExtents.X * boundsExtents.Y * boundsExtents.Z;
                        float occupancy = entry.TotalInstanceCount / volume;

                        XElement entryElement = new XElement("Batch",
                                                            new XAttribute("InstanceCount", entry.TotalInstanceCount),
                                                            new XAttribute("Occupancy", occupancy),
                                                            new XAttribute("Volume", volume),
                                                            new XElement("BatchBounds",
                                                                new XElement("Min",
                                                                    new XAttribute("X", entry.BoundingBox.Min.X),
                                                                    new XAttribute("Y", entry.BoundingBox.Min.Y),
                                                                    new XAttribute("Z", entry.BoundingBox.Min.Z)),
                                                                new XElement("Max",
                                                                    new XAttribute("X", entry.BoundingBox.Max.X),
                                                                    new XAttribute("Y", entry.BoundingBox.Max.Y),
                                                                    new XAttribute("Z", entry.BoundingBox.Max.Z))));
                        archetypeElement.Add(entryElement);
                    }

                    baseElement.Add(archetypeElement);
                }

                doc.Save(statsFilename);
            }

            if (!String.IsNullOrEmpty(vectorBitmapFilename))
            {
                // Write out the vector image to show the batch bounds.
                int width = (int)(worldExtents.Max.X - worldExtents.Min.X);
                int height = (int)(worldExtents.Max.Y - worldExtents.Min.Y);

                Bitmap bitmap = new Bitmap(width, height);
                Graphics g = Graphics.FromImage(bitmap);

                foreach(KeyValuePair<String, List<InstancePlacementArchetypeStat>> stat in containerStat.ContainerStats)
                {
                    foreach (InstancePlacementArchetypeStat archetypeStat in stat.Value)
                    {
                        float bbWidth = archetypeStat.BoundingBox.Max.X - archetypeStat.BoundingBox.Min.X;
                        float bbHeight = archetypeStat.BoundingBox.Max.Y - archetypeStat.BoundingBox.Min.Y;

                        float x = archetypeStat.BoundingBox.Min.X - worldExtents.Min.X;
                        float y = archetypeStat.BoundingBox.Min.Y - worldExtents.Min.Y;

                        Brush outlineBrush = new SolidBrush(archetypeStat.ListColour);
                        Pen pen = new Pen(outlineBrush, 1);

                        g.DrawRectangle(pen, x, y, bbWidth, bbHeight);
                    }
                }

                bitmap.Save(vectorBitmapFilename, ImageFormat.Bmp);
            }
        }
    }
    #endregion
}
