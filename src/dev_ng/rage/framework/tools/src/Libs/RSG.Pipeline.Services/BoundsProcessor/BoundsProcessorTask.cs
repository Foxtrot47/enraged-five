﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.XPath;
using System.Xml.Linq;

namespace RSG.Pipeline.Services.BoundsProcessor
{
    public class BoundsProcessorTask
    {
        public BoundsProcessorTask(XElement taskElement)
        {
            inputs_ = new List<BoundsProcessorInput>();
            foreach (XElement inputElement in taskElement.Elements("Input"))
            {
                String sceneXmlPathname = inputElement.Attribute("scenexml").Value;
                String sceneType = inputElement.Attribute("scenetype").Value;
                XAttribute boundsAttribute = inputElement.Attribute("bounds");
                XAttribute inputDirAttribute = inputElement.Attribute("inputDir");
                XAttribute drawableListAttribute = inputElement.Attribute("drawableList");
                IEnumerable<String> additionalInputs = inputElement.XPathSelectElements("AdditionalImap").
                    Select(xmlAdditionalImap => xmlAdditionalImap.Attribute("file").Value);

                string boundsAttributeValue = String.Empty;
                if (boundsAttribute != null)
                    boundsAttributeValue = boundsAttribute.Value;

                string drawableListAttributeValue = String.Empty;
                if (drawableListAttribute != null)
                    drawableListAttributeValue = drawableListAttribute.Value;

                string inputDirAttributeValue = String.Empty;
                if (inputDirAttribute != null)
                    inputDirAttributeValue = inputDirAttribute.Value;

                inputs_.Add(new BoundsProcessorInput(sceneXmlPathname, sceneType, boundsAttributeValue, inputDirAttributeValue, drawableListAttributeValue, additionalInputs));
            }

            XElement outputElement = taskElement.Element("Output");
            String name = outputElement.Attribute("name").Value;
            String directory = outputElement.Attribute("directory").Value;

            // task.Output.ManifestAdditionsPathname can be null if we are on AP2.  This should be deprecated
            String manifestAdditionsPathname = "";
            if (outputElement.Attribute("manifest_additions") != null)
                manifestAdditionsPathname = outputElement.Attribute("manifest_additions").Value;

            Output = new BoundsProcessorOutput(name, directory, manifestAdditionsPathname);
        }

        public BoundsProcessorTask(IEnumerable<BoundsProcessorInput> inputs, BoundsProcessorOutput output)
        {
            inputs_ = new List<BoundsProcessorInput>(inputs);
            Output = output;
        }

        public XElement ToXElement()
        {
            return new XElement("Task",
                inputs_.Select(input => input.ToXElement()),
                Output.ToXElement());
        }

        public IEnumerable<BoundsProcessorInput> Inputs { get { return inputs_; } }
        public BoundsProcessorOutput Output { get; private set; }

        private List<BoundsProcessorInput> inputs_;
    }
}
