﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.IO;

using RSG.Rage.Entity;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;

namespace RSG.Pipeline.Services.Rage
{
    #region Enumerations
    /// <summary>
    /// 
    /// </summary>
    public enum Mode
    {
        Default,
        Multiplayer
    }

    /// <summary>
    /// 
    /// </summary>
    public enum Split
    {
        Default,
        High
    }
    
    #endregion // Enumerations

    /// <summary>
    /// 
    /// </summary>
    public sealed class LodDescription :
        ILodDescription
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public String Platform { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Mode ModeN { get; set; }

        /// <summary>
        /// Split type e.g.e High for HD split.
        /// </summary>
        public Split Split { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IDictionary<LodLevel, String> LodLevels { get; set; }
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="platform"></param>
        /// <param name="mode"></param>
        /// <param name="split"></param>
        /// <param name="lodlevels"></param>
        public LodDescription(String platform, Mode mode, Split split, IDictionary<LodLevel, String> lodlevels)
        {
            this.Platform = platform;
            this.ModeN = mode;
            this.Split = split;
            this.LodLevels = lodlevels;
        }
        #endregion // Constructors

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public String GetDataSubDirectoryWithSplit()
        {
            return Path.Combine(this.Platform.ToString(), this.ModeN.ToString(), this.Split.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public String GetDataSubDirectory()
        {
            return Path.Combine(this.Platform.ToString(), this.ModeN.ToString());
        }

        /// <summary>
        /// Returns a path built from platform, mode and split.
        /// </summary>
        /// <param name="rootPath"></param>
        /// <returns></returns>
        public String GetDataLocation(String rootPath)
        {
            return Path.Combine(rootPath, this.GetDataSubDirectoryWithSplit());
        }

        /// <summary>
        /// Returns a path built from platform and mode.
        /// </summary>
        /// <param name="rootPath"></param>
        /// <returns></returns>
        public String GetShortDataLocation(String rootPath)
        {
            return Path.Combine(rootPath, this.GetDataSubDirectory());
        }
        #endregion // Controller Methods

        #region Public Static Methods
        /// <summary>
        /// Creates a list of LOD descriptions from XML.
        /// </summary>
        /// <param name="lodDescriptionFile"></param>
        /// <returns></returns>
        public static IEnumerable<ILodDescription> LoadLodDescriptionsFromFile(String lodDescriptionFile, IConfig config, IUniversalLog log)
        {

            lodDescriptionFile = config.Environment.Subst(lodDescriptionFile);
            if (!File.Exists(lodDescriptionFile))
            {
                log.ErrorCtx("Fragment Generator", "LOD Description does not exist ({0}).  Cannot fabricate LOD hierarchy.", lodDescriptionFile);
            }

            XDocument xdoc = XDocument.Load(lodDescriptionFile);
            List<LodDescription> lodDescs = new List<LodDescription>();
            foreach (XElement elem in xdoc.Descendants("platform"))
            {

                String plat = elem.Attribute("value").Value;
                foreach (XElement childelem in elem.Descendants("mode"))
                {
                    String modeName = childelem.Attribute("value").Value;
                    Mode mode = LodDescription.ModeStringToMode(modeName);
                    foreach (XElement gchildelem in childelem.Descendants("split"))
                    {
                        String splitName = gchildelem.Attribute("value").Value;
                        Split split = LodDescription.SplitStringToSplit(splitName);
                        Dictionary<LodLevel, String> lodLevels = new Dictionary<LodLevel, String>();
                        foreach (XElement lodelem in gchildelem.Descendants("lod"))
                        {
                            String level = lodelem.Attribute("level").Value;
                            String source = lodelem.Attribute("source").Value;
                            LodLevel ll = LodGroup.LodNameToLevel(level);
                            lodLevels.Add(ll, source);
                        }
                        LodDescription ld = new LodDescription(plat, mode, split, lodLevels);
                        lodDescs.Add(ld);
                    }
                }
            }
            return (lodDescs);
        }

        /// <summary>
        /// Convert mode from string to enumerated value.
        /// </summary>
        /// <param name="lodlevel"></param>
        /// <returns></returns>
        public static Mode ModeStringToMode(String mode)
        {
            Mode m = Mode.Default;
            switch (mode)
            {
                case "mp":
                case "multiplayer":
                    m = Mode.Multiplayer;
                    break;
                default:
                    m = Mode.Default;
                    break;

            }
            return m;
        }

        /// <summary>
        ///  Convert split type from string to enumerated value.
        /// </summary>
        /// <param name="lodlevel"></param>
        /// <returns></returns>
        public static Split SplitStringToSplit(String mode)
        {
            Split s = Split.Default;
            switch (mode)
            {
                case "hi":
                case "high":
                    s = Split.High;
                    break;
                default:
                    s = Split.Default;
                    break;

            }
            return s;
        }
        #endregion // Public Static Methods
    }
}
