﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Logging.Universal;

namespace RSG.Pipeline.Services.Platform.Manifest
{
    
    /// <summary>
    /// Class representing a IMAP Group and IMAP Group Bounds pair.
    /// </summary>
    public class IMAPGroup
    {
        #region Constants
        private static readonly String ELEM_NAME = "Name";
        private static readonly String ATTR_VALUE = "value";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// IMAP Group Name.
        /// </summary>
        public String GroupName
        {
            get;
            private set;
        }

        /// <summary>
        /// IMAP Group Bounds asset names.
        /// </summary>
        public ICollection<String> Bounds
        {
            get;
            private set;
        }

        /// <summary>
        /// Activation types (artist chosen).
        /// </summary>
        /// Note: these are parCodeGen enumeration constant strings.  They are
        /// kept in this format for ease of translation (i.e. none required).
        /// 
        public ICollection<String> Flags
        {
            get;
            private set;
        }

        /// <summary>
        /// Weather activation types.
        /// </summary>
        public ICollection<String> WeatherTypes
        {
            get;
            private set;
        }

        /// <summary>
        /// Hours IMAP Group is active.
        /// </summary>
        public UInt32 ActiveHours
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="xmlGroup"></param>
        internal IMAPGroup(String groupName)
        {
            GroupName = groupName;
            Bounds = new List<String>();
            Flags = new List<String>();
            WeatherTypes = new List<String>();
            ActiveHours = 0;
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Serialise to CMapDataGroup XElement for RPF Manifest.
        /// </summary>
        /// <returns></returns>
        public XElement AsCMapDataGroup(String name)
        {
            XElement xmlMapDataGroup = new XElement(name);
            XElement xmlGroupName = new XElement(ELEM_NAME);
            xmlGroupName.SetValue(this.GroupName);
            xmlMapDataGroup.Add(xmlGroupName);

            // Bound asset names.
            if (this.Bounds.Any())
            {
                XElement xmlBounds = new XElement("Bounds");
                foreach (String bound in this.Bounds)
                {
                    XElement xmlBound = new XElement("Item");
                    xmlBound.SetValue(bound);
                    xmlBounds.Add(xmlBound);
                }
                xmlMapDataGroup.Add(xmlBounds);
            }

            // Flags.
            if (this.Flags.Any())
            {
                XElement xmlFlags = new XElement("Flags");
                xmlFlags.SetValue(String.Join("|", this.Flags));
                xmlMapDataGroup.Add(xmlFlags);
            }

            // Weather Types.
            if (this.WeatherTypes.Any())
            {
                XElement xmlWeatherTypes = new XElement("WeatherTypes");
                foreach (String weather in this.WeatherTypes)
                {
                    XElement xmlWeather = new XElement("Item");
                    xmlWeather.SetValue(weather);
                    xmlWeatherTypes.Add(xmlWeather);
                }
                xmlMapDataGroup.Add(xmlWeatherTypes);
            }

            // Hours On/Off.
            if (this.ActiveHours != 0)
            {
                XElement xmlHours = new XElement("HoursOnOff");
                xmlHours.SetAttributeValue(ATTR_VALUE, this.ActiveHours.ToString());
                xmlMapDataGroup.Add(xmlHours);
            }

            return (xmlMapDataGroup);
        }
        #endregion // Controller Methods

        #region Static Controller Methods
        public static IMAPGroup FromManifest(XElement element)
        {
            String name = element.Element("Name").Value;
            IMAPGroup imapGroup = new IMAPGroup(name);

            XElement boundsElement = element.Element("Bounds");
            if (boundsElement != null)
            {
                foreach (XElement boundsItemElement in boundsElement.Elements("Item"))
                {
                    imapGroup.Bounds.Add(boundsItemElement.Value);
                }
            }

            return imapGroup;
        }
        #endregion Static Controller Methods
    }

} // RSG.Pipeline.Services.Platform.Manifest namespace
