﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.Pipeline.Services.BoundsProcessor
{
    public class BoundsProcessorConfig
    {
        public static BoundsProcessorConfig Load(string pathname)
        {
            return new BoundsProcessorConfig(pathname);
        }

        public BoundsProcessorConfig(BoundsProcessorOptions options, IEnumerable<BoundsProcessorTask> tasks)
        {
            Options = options;
            tasks_ = new List<BoundsProcessorTask>(tasks);
        }

        public void Save(String pathname)
        {
            XDocument document = new XDocument(
                new XElement("BoundsProcessor",
                    Options.ToXElement(),
                    new XElement("Tasks",
                        tasks_.Select(task => task.ToXElement()))));

            document.Save(pathname);
        }

        private BoundsProcessorConfig(String pathname)
        {
            XDocument configDocument = XDocument.Load(pathname);

            // Load the options
            Options = new BoundsProcessorOptions();
            XElement optionsElement = configDocument.Root.Element("Options");

            if (optionsElement.Element("MaxStandardCompositeSizeXY") != null)
                Options.MaxStandardBoundsXY = float.Parse(optionsElement.Element("MaxStandardCompositeSizeXY").Value);

            if (optionsElement.Element("MaxHighDetailCompositeSizeXY") != null)
                Options.MaxHighDetailBoundsXY = float.Parse(optionsElement.Element("MaxHighDetailCompositeSizeXY").Value);

            if (optionsElement.Element("MaxBVHDataSize") != null)
                Options.MaxBVHDataSize = int.Parse(optionsElement.Element("MaxBVHDataSize").Value);

            if (optionsElement.Element("MaxStandardMapBVHDataSize") != null)
                Options.MaxStandardMapBVHDataSize = int.Parse(optionsElement.Element("MaxStandardMapBVHDataSize").Value);

            if (optionsElement.Element("MaxHighDetailMapBVHDataSize") != null)
                Options.MaxHighDetailMapBVHDataSize = int.Parse(optionsElement.Element("MaxHighDetailMapBVHDataSize").Value);

            if (optionsElement.Element("MaxStandardMapCompositeDataSize") != null)
                Options.MaxStandardMapCompositeDataSize = int.Parse(optionsElement.Element("MaxStandardMapCompositeDataSize").Value);

            if (optionsElement.Element("MaxHighDetailMapCompositeDataSize") != null)
                Options.MaxHighDetailMapCompositeDataSize = int.Parse(optionsElement.Element("MaxHighDetailMapCompositeDataSize").Value);

            if (optionsElement.Element("MaxMaterialColourPaletteSize") != null)
                Options.MaxMaterialColourPaletteSize = int.Parse(optionsElement.Element("MaxMaterialColourPaletteSize").Value);

            if (optionsElement.Element("MinPrimitivesPerComposite") != null)
                Options.MinimumPrimitiveCount = int.Parse(optionsElement.Element("MinPrimitivesPerComposite").Value);

            if (optionsElement.Element("Parallelise") != null)
                Options.Parallelise = bool.Parse(optionsElement.Element("Parallelise").Value);

            if (optionsElement.Element("Profile") != null)
                Options.Profile = bool.Parse(optionsElement.Element("Profile").Value);

            if (optionsElement.Element("ComplexMapCollisionSplitting") != null)
                Options.ComplexMapCollisionSplitting = bool.Parse(optionsElement.Element("ComplexMapCollisionSplitting").Value);

            if (optionsElement.Element("SplitAlternativesToConsider") != null)
                Options.SplitAlternativesToConsider = int.Parse(optionsElement.Element("SplitAlternativesToConsider").Value);

            if (optionsElement.Element("SplitDivergenceToConsider") != null)
                Options.SplitDivergenceToConsider = float.Parse(optionsElement.Element("SplitDivergenceToConsider").Value);

            if (optionsElement.Element("MaxNonBVHPropVolume") != null)
                Options.MaxNonBVHPropVolume = float.Parse(optionsElement.Element("MaxNonBVHPropVolume").Value);

            if (optionsElement.Element("MaxNonBVHPropPrimitiveCount") != null)
                Options.MaxNonBVHPropPrimitiveCount = int.Parse(optionsElement.Element("MaxNonBVHPropPrimitiveCount").Value);

            if (optionsElement.Element("ArchiveOutput") != null)
                Options.ArchiveOutput = bool.Parse(optionsElement.Element("ArchiveOutput").Value);

            if (optionsElement.Element("MaxBVHBoundsXY") != null)
                Options.MaxBVHBoundsXY = float.Parse(optionsElement.Element("MaxBVHBoundsXY").Value);

            // Load the tasks
            tasks_ = new List<BoundsProcessorTask>();
            XElement tasksElement = configDocument.Root.Element("Tasks");
            if (tasksElement != null)
            {
                foreach (XElement taskElement in tasksElement.Elements("Task"))
                    tasks_.Add(new BoundsProcessorTask(taskElement));
            }
            else// Support for old style task initialization
            {
                tasks_.Add(new BoundsProcessorTask(configDocument.Root));
            }
        }

        public BoundsProcessorOptions Options { get; private set; }
        public IEnumerable<BoundsProcessorTask> Tasks { get { return tasks_; } }

        private List<BoundsProcessorTask> tasks_;
    }
}
