﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Configuration;
using RSG.Base.Extensions;

namespace RSG.Pipeline.Services.Platform.Texture
{

    /// <summary>
    /// Resource (exported DDS) texture.
    /// </summary>
    public class ResourceTexture : IResourceTexture
    {
        #region Properties
        /// <summary>
        /// Absolute path to resource/export texture.
        /// </summary>
        public String AbsolutePath { get; private set; }

        /// <summary>
        /// Resource texture usage.
        /// </summary>
        public ResourceTextureUsage Usage { get; private set; }

        /// <summary>
        /// Resource texture source textures.
        /// </summary>
        public IEnumerable<ISourceTexture> SourceTextures { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="xmlResourceTextureElem"></param>
        public ResourceTexture(XElement xmlResourceTextureElem, ITarget target)
        {
            XElement xmlPathNameElem = xmlResourceTextureElem.XPathSelectElement("pathname");
            this.AbsolutePath = Path.GetFullPath(target.Environment.Subst(xmlPathNameElem.Value.Trim()));

#warning DHM FIX ME: set usage enum here

            IEnumerable<XElement> xmlSourceTextureElems = xmlResourceTextureElem.XPathSelectElements(
                "sourceTextures/Item");
            this.SourceTextures = xmlSourceTextureElems.Select(elem =>
                new SourceTexture(elem, target));
        }
        #endregion // Constructor(s)
    }

} // RSG.Pipeline.Services.Platform.Texture namespace
