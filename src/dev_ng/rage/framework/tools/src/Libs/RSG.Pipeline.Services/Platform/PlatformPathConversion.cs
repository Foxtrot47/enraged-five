﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Platform;
using RSG.Pipeline.Core;

namespace RSG.Pipeline.Services.Platform
{

    /// <summary>
    /// Utility methods for path and extension string conversion for targets.
    /// </summary>
    public static class PlatformPathConversion
    {
        /// <summary>
        /// Convert an extension string to a target extension string.
        /// </summary>
        /// <param name="target">Target to convert extension for.</param>
        /// <param name="extension"></param>
        /// <returns></returns>
        public static String ConvertExtensionToPlatform(ITarget target, String extension)
        {
            Char targetCharacter = target.Platform.PlatformRagebuilderCharacter();
            FileType ft = FileTypeUtils.ConvertExtensionToFileType(extension);
            switch (ft)
            {
                // Currently no special cases because of the awesome attributes.
                case FileType.Unknown:
                    {
                        Char exportChar = RSG.Platform.Platform.Independent.PlatformRagebuilderCharacter();
#warning DHM FIX ME: logging
                        //Rage.Log.Debug("Unrecognised file type: '{0}'.  Passing through using default target replacement.", extension);
                        return (extension.Replace(exportChar, targetCharacter));
                    }
                default:
                    {
                        String exportExt, platformExt;
                        ft.GetExtensions(out exportExt, out platformExt);

                        Debug.Assert(!String.IsNullOrEmpty(exportExt),
                            "Export extension is empty!");
                        Debug.Assert(!String.IsNullOrEmpty(platformExt),
                            "Platform extension is empty!");
                        platformExt = platformExt.Replace("?", target.Platform.PlatformRagebuilderCharacter().ToString());
                        return (extension.Replace(exportExt, platformExt));
                    }
            }
        }

        /// <summary>
        /// Takes in an IBranch and finds the ITarget for the platform if it exists in the 
        /// branch's targets list, null otherwise.
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="platform"></param>
        /// <returns></returns>
        public static ITarget GetTargetFromRagebuilderPlatform(IBranch branch, String platform)
        {
            if (branch == null)
                return null;

            RSG.Platform.Platform ePlatform = PlatformUtils.RagebuilderPlatformToPlatform(platform);

            if (branch.Targets.ContainsKey(ePlatform))
            {
                return branch.Targets[ePlatform];
            }

            return null;
        }

        /// <summary>
        /// Convert a filename string to a target filename string.  No remapping
        /// of directories are done.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="filename"></param>
        /// <param name="include_target_dir"></param>
        /// <returns></returns>
        public static String ConvertFilenameToPlatform(ITarget target, String filename, bool include_target_dir)
        {
            String[] extensions = Filename.GetExtensions(filename);

            // We need to carefully handle filenames with multiple extensions;
            // they normally just resolve to a single extension.
            if (0 == extensions.Length)
            {
                throw new ArgumentException(String.Format("Filename: '{0}' has no extension!", filename), "filename");
            }
            else if (1 == extensions.Length)
            {
                String directory = Path.GetDirectoryName(filename);
                String basename = Filename.GetBasename(filename);
                String platformExt = ConvertExtensionToPlatform(target, extensions[0]);
                String platformFilename = String.Empty;
                if (include_target_dir)
                {
                    String platform = target.Platform.ToString();
                    platformFilename = Path.Combine(directory, platform, String.Format("{0}{1}", basename, platformExt));
                }
                else
                {
                    platformFilename = Path.Combine(directory, String.Format("{0}{1}", basename, platformExt));
                }
                return (platformFilename);
            }
            else
            {
                FileType[] filetypes = Filename.GetFileTypes(filename);
                Debug.Assert(extensions.Length == filetypes.Length);

                String directory = Path.GetDirectoryName(filename);
                String basename = Filename.GetBasename(filename);
                String platformFilename = String.Empty;

                // If we have more than one extension we're really assuming that
                // we have a core type and zip container (or PSO and meta).
                switch (filetypes[0])
                {
                    case FileType.ZipArchive:
                        {
                            String platformExt = ConvertExtensionToPlatform(target, extensions[1]); 
                            if (include_target_dir)
                            {
                                String platform = target.Platform.ToString();
                                platformFilename = Path.Combine(directory, platform, String.Format("{0}{1}", basename, platformExt));
                            }
                            else
                            {
                                platformFilename = Path.Combine(directory, String.Format("{0}{1}", basename, platformExt));
                            }
                        }
                        break;
                    case FileType.Metadata:
                        {
                            Debug.Assert(FileType.PSO == filetypes[1],
                                String.Format("Only supported container for Metadata is PSO; got: {0}.", filetypes[1]));
                            if (FileType.PSO == filetypes[1])
                            {
                                if (include_target_dir)
                                {
                                    String platform = target.Platform.ToString();
                                    platformFilename = Path.Combine(directory, platform, String.Format("{0}.pso", basename));
                                }
                                else
                                {
                                    platformFilename = Path.Combine(directory, String.Format("{0}.pso", basename));
                                }
                            }
                            else if (FileType.PSO != filetypes[1])
                            {
                                throw new ArgumentException(String.Format("Only supported container for Metadata is PSO; got: {0}.", filetypes[1]));
                            }
                        }
                        break;
                    default:
                        Debug.Assert(false, String.Format("Unsupported container extension filename: '{0}'.", filename));
                        throw new ArgumentException(String.Format("Unsupported container extension filename: '{0}'.", filename));
                }

                return (platformFilename);
            }
        }

        /// <summary>
        /// Convert a filename string to a target filename string; remapping it
        /// into the branch target's build path.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="filename"></param>
        /// <param name="preview"></param>
        /// <returns></returns>
        public static String ConvertAndRemapFilenameToPlatform(ITarget target, String filename)
        {
            String platformFilename = ConvertFilenameToPlatform(target, filename, false);
            platformFilename = RemapFilenameToPlatform(target, platformFilename, false);

            return (platformFilename);
        }

        /// <summary>
        /// Convert a filename string to a target filename string; remapping it
        /// into the branch target's preview path.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static String ConvertAndRemapFilenameToPreview(ITarget target, Content.File inputFile)
        {
            String filename = inputFile.AbsolutePath;
            String platformFilename = ConvertFilenameToPlatform(target, filename, false);

            if (inputFile.Parameters.ContainsKey(Constants.Preserve_Folder_Structure))
                platformFilename = RemapFilenameToPlatform(target, platformFilename, true, (String)inputFile.Parameters[Constants.Preserve_Folder_Structure]);
            else
                platformFilename = RemapFilenameToPlatform(target, platformFilename, true);

            return (platformFilename);
        }

        /// <summary>
        /// Convert a filename string to a target filename string; remapping it
        /// into the branch target's resource cache.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="filename"></param>
        /// <param name="cacheDir"></param>
        /// <param name="includeTargetDir"></param>
        /// <returns></returns>
        public static String ConvertAndRemapFilenameToDirectory(ITarget target, String filename, 
            String cacheDir, bool includeTargetDir = true)
        {
            String platformFilename = ConvertFilenameToPlatform(target, filename, false);
            platformFilename = RemapFilenameToResourceCache(target, platformFilename, cacheDir);

            // Separate into per-target sub-directories.
            String directory = Path.GetDirectoryName(platformFilename);
            String justFilename = Path.GetFileName(platformFilename);
            if (includeTargetDir)
                platformFilename = Path.Combine(directory, target.Platform.ToString(), justFilename);
            else
                platformFilename = Path.Combine(directory, justFilename);

            return (platformFilename);
        }

        /// <summary>
        /// Convert a directory string to a target directory string; remapping it
        /// into the branch target's resource cache.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="filename"></param>
        /// <param name="cacheDir"></param>
        /// <param name="includeTargetDir"></param>
        /// <returns></returns>
        public static String ConvertAndRemapDirectoryToDirectory(ITarget target, String filename,
            String cacheDir, bool includeTargetDir = true)
        {
            String platformFilename = RemapFilenameToResourceCache(target, filename, cacheDir);

            // Separate into per-target sub-directories.
            String directory = Path.GetDirectoryName(platformFilename);
            String justFilename = Path.GetFileName(platformFilename);
            if (includeTargetDir)
                platformFilename = Path.Combine(directory, target.Platform.ToString(), justFilename);
            else
                platformFilename = Path.Combine(directory, justFilename);

            return (platformFilename);
        }

        /// <summary>
        /// Prepend a resource prefix string onto a filename.  Used for Map DLC resources.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="resourcePrefix"></param>
        /// <returns></returns>
        public static String PrependResourcePrefixToFilename(String filename, String resourcePrefix)
        {
            if (String.IsNullOrEmpty(resourcePrefix))
                return (filename); // Nothing to do.
            
            String directory = Path.GetDirectoryName(filename);
            String filenameWithExtension = Path.GetFileName(filename);
            if (filenameWithExtension.StartsWith(resourcePrefix, StringComparison.OrdinalIgnoreCase))
                return (filename); // Nothing to do; prevents dupe prefixes.
            
            filenameWithExtension = String.Format("{0}{1}", resourcePrefix, filenameWithExtension);

            return (Path.Combine(directory, filenameWithExtension));
        }

        #region Private Methods
        /// <summary>
        /// Remap filename (from assets or processed) into a target directory
        /// (this isn't really useful externally).
        /// </summary>
        /// <param name="target"></param>
        /// <param name="filename"></param>
        /// <param name="preview"></param>
        /// <returns></returns>
        private static String RemapFilenameToPlatform(ITarget target, String filename, bool preview = false, String prependPath = null)
        {
            String normalisedFilename = Path.GetFullPath(target.Environment.Subst(filename));
            if (!preview)
            {
                if (normalisedFilename.StartsWith(target.Branch.Export, StringComparison.OrdinalIgnoreCase))
                    normalisedFilename = normalisedFilename.Replace(target.Branch.Export, target.ResourcePath, StringComparison.OrdinalIgnoreCase);
                else if (normalisedFilename.StartsWith(target.Branch.Processed, StringComparison.OrdinalIgnoreCase))
                    normalisedFilename = normalisedFilename.Replace(target.Branch.Processed, target.ResourcePath, StringComparison.OrdinalIgnoreCase);
            }
            else
            {
                String assetName = Path.GetFileName(normalisedFilename);

                if (!String.IsNullOrEmpty(prependPath))
                    assetName = Path.Combine(prependPath, assetName);

                normalisedFilename = (Path.Combine(target.Branch.Project.Config.CoreProject.Branches[target.Branch.Name].Preview, assetName));
            }
            return (normalisedFilename);
        }
        
        /// <summary>
        /// Remap filename (from assets or processed) into a target directory
        /// (this isn't really useful externally).
        /// </summary>
        /// <param name="target"></param>
        /// <param name="filename"></param>
        /// <param name="cacheDir"></param>
        /// <returns></returns>
        private static String RemapFilenameToResourceCache(ITarget target, String filename, String cacheDir)
        {
            String normalisedFilename = Path.GetFullPath(target.Environment.Subst(filename));
            if (normalisedFilename.StartsWith(target.Branch.Export, StringComparison.OrdinalIgnoreCase))
                normalisedFilename = normalisedFilename.Replace(target.Branch.Export, cacheDir, StringComparison.OrdinalIgnoreCase);
            else if (normalisedFilename.StartsWith(target.Branch.Processed, StringComparison.OrdinalIgnoreCase))
                normalisedFilename = normalisedFilename.Replace(target.Branch.Processed, cacheDir, StringComparison.OrdinalIgnoreCase);

            return (normalisedFilename);
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Services.Platform namespace
