﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Logging.Universal;

namespace RSG.Pipeline.Services.Metadata
{
    
    /// <summary>
    /// Metadata Serialiser configuration data; loaded from XML.
    /// </summary>
    public class MapMetadataSerialiserConfig
    {
        #region Properties
        /// <summary>
        /// Options object.
        /// </summary>
        public MapMetadataSerialiserOptions Options
        {
            get;
            private set;
        }

        /// <summary>
        /// Metadata Serialiser tasks.
        /// </summary>
        public IEnumerable<MapMetadataSerialiserTaskBase> Tasks
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public MapMetadataSerialiserConfig(MapMetadataSerialiserOptions options,
                                           IEnumerable<MapMetadataSerialiserTaskBase> tasks)
        {
            this.Options = options;
            this.Tasks = tasks.ToArray();
        }

        /// <summary>
        /// Constructor; loading configuration data from XML document.
        /// </summary>
        /// <param name="filename"></param>
        public MapMetadataSerialiserConfig(String filename)
        {
            XDocument xmlDoc = XDocument.Load(filename);
            MapMetadataSerialiserOptions options = new MapMetadataSerialiserOptions(xmlDoc.Root.Element("Options"));
            IEnumerable<MapMetadataSerialiserTaskBase> tasks =
                xmlDoc.XPathSelectElements("/MapMetadataSerialiser/Tasks/Task").
                Select(xmlTaskElem => new MapMetadataSerialiserTask(xmlTaskElem));
            IEnumerable<MapMetadataSerialiserTaskBase> mergeTasks =
                xmlDoc.XPathSelectElements("/MapMetadataSerialiser/Tasks/MergeTask").
                Select(xmlTaskElem => new MapMetadataSerialiserMergeTask(xmlTaskElem));

            this.Options = options;
            this.Tasks = tasks.Concat(mergeTasks).ToArray();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Serialise the configuration data to an XML file.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
        public bool Serialise(IUniversalLog log, String filename)
        {
            bool result = true;
            try
            {
                XDocument xmlDoc = new XDocument(
                    new XDeclaration("1.0", "UTF-8", "yes"),
                    new XElement("MapMetadataSerialiser",
                        this.Options.Serialise(),
                        new XElement("Tasks",
                            this.Tasks.Select(t => t.Serialise())
                        ) // Tasks
                    ) // MapMetadataSerialiser
                ); // xmlDoc                
                xmlDoc.Save(filename);
            }
            catch (Exception ex)
            {
                log.Exception(ex, "Error serialising Map Metadata Configuration data.");
                result = false;
            }
            return (result);
        }
        #endregion // Controller Methods
    }

} // // RSG.Pipeline.Services.Metadata namespace
