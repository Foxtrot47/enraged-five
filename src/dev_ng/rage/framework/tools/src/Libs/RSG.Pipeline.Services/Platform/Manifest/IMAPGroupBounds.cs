﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.Pipeline.Services.Platform.Manifest
{
    public class IMAPGroupBounds
    {
        public static IMAPGroupBounds FromBoundsAdditions(XElement element)
        {
            String name = element.Attribute("name").Value.ToString();
            List<String> boundsNames = new List<String>();
            foreach (XElement boundsElement in element.Elements("Bounds"))
            {
                boundsNames.Add(boundsElement.Attribute("name").Value.ToString());
            }

            return new IMAPGroupBounds(name, boundsNames);
        }

        private IMAPGroupBounds(String name, IEnumerable<String> boundsNames)
        {
            Name = name;
            BoundsNames = boundsNames;
        }

        internal String Name { get;  private set;}
        internal IEnumerable<String> BoundsNames { get; private set; }
    }
}
