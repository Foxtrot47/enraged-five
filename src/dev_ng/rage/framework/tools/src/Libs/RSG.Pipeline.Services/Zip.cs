﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using SIOC = System.IO.Compression;
using Ionic.Zip;
using RSG.Base.Collections;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Base.Logging;

namespace RSG.Pipeline.Services
{

    /// <summary>
    /// Zipfile construction and extraction utility functions.
    /// </summary>
    public static class Zip
    {
        #region Constants
        /// <summary>
        /// Log context string.
        /// </summary>
        private static readonly String LOG_CTX = "Zip";
        #endregion // Constants

        #region Zip File Construction Methods
        /// <summary>
        /// Create a zip file (strips all path information).
        /// </summary>
        /// <param name="branch">Branch.</param>
        /// <param name="filename">Destination filename.</param>
        /// <param name="files">Enumerable of filenames to pack.</param>
        /// <param name="callback">Progress callback function.</param>
        /// <returns>true iff successful; false otherwise.</returns>
        public static bool Create(IBranch branch, String filename, IEnumerable<String> files, EventHandler<SaveProgressEventArgs> callback = null)
        {
            return (Zip.Create(branch, filename, files, false, callback));
        }

        /// <summary>
        /// Create a zip file (optionally keeping full path information).
        /// </summary>
        /// <param name="branch">Branch.</param>
        /// <param name="filename"></param>
        /// <param name="files"></param>
        /// <param name="useFullPaths"></param>
        /// <param name="callback"></param>
        /// <returns></returns>
        public static bool Create(IBranch branch, String filename, IEnumerable<String> files, bool useFullPaths, EventHandler<SaveProgressEventArgs> callback = null)
        {
            List<Pair<String, String>> file_list = new List<Pair<String, String>>();
            foreach (String fileEntry in files)
            {
                if (useFullPaths)
                    file_list.Add(new Pair<String, String>(fileEntry, fileEntry));
                else
                    file_list.Add(new Pair<String, String>(fileEntry, Path.GetFileName(fileEntry)));
            }
            return (Zip.Create(branch, filename, file_list.ToArray(), useFullPaths, callback));       
        }

        /// <summary>
        /// Create a zip file; defining the destination files (folder support).
        /// </summary>
        /// <param name="branch">Branch.</param>
        /// <param name="filename"></param>
        /// <param name="files"></param>
        /// <param name="callback"></param>
        /// <returns></returns>
        public static bool Create(IBranch branch, String filename, IEnumerable<Pair<String, String>> files, EventHandler<SaveProgressEventArgs> callback = null)
        {
            return (Zip.Create(branch, filename, files, false, callback));
        }

        /// <summary>
        /// Create a zip file; defining the destination files (folder support).
        /// </summary>
        /// <param name="branch">Branch.</param>
        /// <param name="filename"></param>
        /// <param name="files"></param>
        /// <param name="useFullPath"></param>
        /// <param name="callback"></param>
        /// <returns></returns>
        public static bool Create(IBranch branch, String filename, IEnumerable<Pair<String, String>> files, bool useFullPath, EventHandler<SaveProgressEventArgs> callback = null)
        {
            bool result = true;
            using (ZipFile zf = new ZipFile(filename))
            {
                try
                {
                    zf.SaveProgress += callback;
                    foreach (Pair<String, String> pair in files)
                    {
                        String source = pair.First;
                        String destination = pair.Second;

                        if (!File.Exists(source))
                        {
                            Log.StaticLog.ErrorCtx(LOG_CTX, "Input file: {0} does not exist.", source);
                            continue; // Skip file.
                        }

                        ZipEntry ze = null;
#warning DHM FIX ME: not sure this is good idea to strip out; should it be handled in the Asset Pack Processor?
                        if (useFullPath || Path.HasExtension(destination))
                            ze = zf.AddFile(source, Path.GetDirectoryName(destination));
                        else
                            ze = zf.AddFile(source, destination);
                    }

                    // Set comment based on tools version, user and host.
                    IConfig config = branch.Project.Config;
                    zf.Comment = String.Format("Version: {0} created by {1}@{2}", config.Version.LocalVersion.LabelName,
                        Environment.UserName, Environment.MachineName);

                    // Set other properties and save.
                    zf.UseZip64WhenSaving = Zip64Option.AsNecessary;
                    zf.CompressionLevel = Ionic.Zlib.CompressionLevel.Default;
                    zf.Save();
                }
                catch (Exception ex)
                {
                    Log.StaticLog.ExceptionCtx(LOG_CTX, ex, "Exception during zip file construction.");
                    result = false;
                }
                finally
                {
                    zf.SaveProgress -= callback;
                }
            }

            return (result);
        }

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="log"></param>
        ///// <param name="loggingContext"></param>
        ///// <param name="sourceFiles"></param>
        ///// <param name="outputFile"></param>
        ///// <returns></returns>
        //public static int Merge(IUniversalLog log, string loggingContext, IEnumerable<string> sourceFiles, string outputFile)
        //{
        //    // Actual merge. The stuff before was only to validate options and init.
        //    IEnumerable<Tuple<string, SIOC.ZipArchive>> inputZips = sourceFiles.Select(filename => new Tuple<string, SIOC.ZipArchive>(filename, SIOC.ZipFile.Open(filename, SIOC.ZipArchiveMode.Read))).ToList();

        //    // We compress an in memory stream, so that we perform everything in mem, and avoid reading and writing from/to disk
        //    Stream outputStream = new MemoryStream();
        //    // Magic of ZipArchive we can new it in Update mode and it works anyway. Yay
        //    SIOC.ZipArchive outputZip = new SIOC.ZipArchive(outputStream, SIOC.ZipArchiveMode.Update, true);

        //    // Loop through out drawable inputs; 
        //    foreach (Tuple<string, SIOC.ZipArchive> input in inputZips)
        //    {
        //        SIOC.ZipArchive inputArchive = input.Item2;

        //        // Loop through all entries packing them into output.
        //        foreach (SIOC.ZipArchiveEntry inputEntry in inputArchive.Entries)
        //        {
        //            string outputEntryName = inputEntry.FullName;
        //            SIOC.ZipArchiveEntry outputEntry = outputZip.CreateEntry(outputEntryName);
        //            using (Stream outputEntryStream = outputEntry.Open())
        //            {
        //                using (Stream inputStream = inputEntry.Open())
        //                {
        //                    inputStream.CopyTo(outputEntryStream);
        //                }
        //            }
        //        }
        //    }

        //    // Cleanup inputs.
        //    foreach (Tuple<string, SIOC.ZipArchive> inputAsset in inputZips)
        //    {
        //        inputAsset.Item2.Dispose();
        //    }

        //    // Dispose in Memory output archive to allow its stream to be written out later on
        //    outputZip.Dispose();

        //    string outputPath = Path.GetDirectoryName(outputFile);
        //    if (outputPath == null)
        //    {
        //        log.ToolErrorCtx(loggingContext, "Output path for specified output \"{0}\" is invalid.", outputFile);
        //        outputStream.Close();
        //        return Constants.Exit_Failure;
        //    }

        //    // check for output path existence and create the directory if not
        //    if (!Directory.Exists(outputPath))
        //        Directory.CreateDirectory(outputPath);

        //    // Since output ZipArchive has been disposed, we can write the memorystream to a file
        //    using (FileStream fileStream = new FileStream(outputFile, FileMode.Create))
        //    {
        //        outputStream.Seek(0, SeekOrigin.Begin);
        //        outputStream.CopyTo(fileStream);
        //    }

        //    outputStream.Close();

        //    return Constants.Exit_Success;
        //}
        #endregion // Zip File Construction Methods

        #region Zip File Extraction Methods
        /// <summary>
        /// Delegate used for zip file extraction callback.
        /// </summary>
        /// <param name="filename">Source Zip absolute filename.</param>
        /// <param name="source">Source file being extracted.</param>
        /// <param name="destination">Destination file being extracted.</param>
        public delegate void ZipFileExtractionCallback(String filename, ZipEntry source, String destination);

        /// <summary>
        /// Delegate used for zip file extraction (with overwrite support) callback.
        /// </summary>
        /// <param name="filename">Source Zip absolute filename.</param>
        /// <param name="source">Source file being extracted.</param>
        /// <param name="destination">Destination file being extracted.</param>
        /// <returns>true iff overwrite file; false otherwise.</returns>
        public delegate bool ZipFileOverwriteCallback(String filename, ZipEntry source, String destination);

        /// <summary>
        /// Extract all files from a zip file; calle decides upfront about
        /// overwrite process.
        /// </summary>
        /// <param name="filename">Zipfile filename.</param>
        /// <param name="destination">Destination directory (created if doesn't exist).</param>
        /// <param name="overwrite">true iff all files overwrite; false no local files overwritten.</param>
        /// <returns>true iff successful; false otherwise.</returns>
        public static bool ExtractAll(String filename, String destination, bool overwrite)
        {
            IEnumerable<String> filelist = null;
            return (ExtractAll(filename, destination, overwrite, null, out filelist));
        }

        /// <summary>
        /// Extract all files from a zip file. Allows updating the output to match parent timestamp.
        /// </summary>
        /// <param name="filename">Zipfile filename.</param>
        /// <param name="destination">Destination directory (created if doesn't exist).</param>
        /// <param name="overwrite">true iff all files overwrite; false no local files overwritten.</param>
        /// <param name="updateToParentTimestamp">Whether or not the extracted files should have their timestamp match the timestamp of the source file.</param>
        /// <returns>true iff successful; false otherwise.</returns>
        public static bool ExtractAll(String filename, String destination, bool overwrite, bool updateToParentTimestamp)
        {
            if (updateToParentTimestamp)
            {
                return ExtractAll(filename, destination, overwrite, Zip.UpdateToParentTimestamp);
            }
            else
            {
                return ExtractAll(filename, destination, overwrite, null);
            }
        }

        /// <summary>
        /// Extract all files from a zip file; callee decides upfront about 
        /// overwrite process.
        /// </summary>
        /// <param name="filename">Zipfile filename.</param>
        /// <param name="destination">Destination directory (created if doesn't exist).</param>
        /// <param name="overwrite">true iff all files overwrite; false no local files overwritten.</param>
        /// <param name="extractCb">Extraction callback; invoked for each file extracted.</param>
        /// <returns>true iff successful; false otherwise.</returns>
        public static bool ExtractAll(String filename, String destination, bool overwrite, ZipFileExtractionCallback extractCb)
        {
            IEnumerable<String> filelist = null;
            return (ExtractAll(filename, destination, overwrite, extractCb, out filelist));
        }

        /// <summary>
        /// Extract all files from a zip file; callee decides upfront about
        /// overwrite process.  Filelist is returned.
        /// </summary>
        /// <param name="filename">Zipfile filename.</param>
        /// <param name="destination">Destination directory (created if doesn't exist).</param>
        /// <param name="overwrite">true iff all files overwrite; false no local files overwritten.</param>
        /// <param name="extractCb">Extraction callback; invoked for each file extracted.</param>
        /// <param name="filelist">LIst of files in zip.</param>
        /// <returns>true iff successful; false otherwise.</returns>
        public static bool ExtractAll(String filename, String destination, bool overwrite, ZipFileExtractionCallback extractCb, out IEnumerable<String> filelist)
        {
            Debug.Assert(ZipFile.CheckZip(filename),
                String.Format("Zip file consistency error: '{0}'.", filename));
            if (!ZipFile.CheckZip(filename))
            {
                filelist = new String[] { };
                return (false);
            }

            ICollection<String> files = new List<String>();
            using (ZipFile zf = new ZipFile(filename))
            {
                zf.ExtractProgress += delegate(Object sender, ExtractProgressEventArgs e)
                {
                    if (e.EventType.HasFlag(ZipProgressEventType.Extracting_ExtractEntryWouldOverwrite))
                        e.CurrentEntry.ExtractExistingFile = overwrite ? ExtractExistingFileAction.OverwriteSilently : ExtractExistingFileAction.DoNotOverwrite;
                };
                foreach (ZipEntry ze in zf.Entries)
                {
                    String source = ze.FileName;
                    String dest = Path.Combine(destination, ze.FileName);
                    ze.Extract(destination, ExtractExistingFileAction.InvokeExtractProgressEvent);
                    files.Add(dest);

                    if (null != extractCb)
                        extractCb(filename, ze, dest);
                }
                filelist = files;
            }
            return (true);
        }

        /// <summary>
        /// Extract all files from a zip file with callee control over the 
        /// overwrite process.
        /// </summary>
        /// <param name="filename">Zipfile filename.</param>
        /// <param name="destination">Destination directory (created if doesn't exist).</param>
        /// <param name="overwriteCb">Overwrite callback; invoked for each file extracted, return true for overwrite.</param>
        /// <param name="extractCb">Extraction callback; invoked for each file extracted.</param>
        /// <returns>true iff successful; false otherwise.</returns>
        public static bool ExtractAll(String filename, String destination, ZipFileOverwriteCallback overwriteCb, ZipFileExtractionCallback extractCb)
        {
            IEnumerable<String> filelist = null;
            return (ExtractAll(filename, destination, overwriteCb, extractCb, out filelist));
        }

        /// <summary>
        /// Extract all files from a zip file with callee control over the
        /// overwrite process.  Filelist is returned.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="destination"></param>
        /// <param name="overwriteCb"></param>
        /// <param name="extractCb"></param>
        /// <param name="filelist"></param>
        /// <returns></returns>
        public static bool ExtractAll(String filename, String destination, ZipFileOverwriteCallback overwriteCb, ZipFileExtractionCallback extractCb, out IEnumerable<String> filelist)
        {
            Debug.Assert(ZipFile.CheckZip(filename),
                String.Format("Zip file consistency error: '{0}'.", filename));
            if (!ZipFile.CheckZip(filename))
            {
                filelist = new String[] { };
                return (false);
            }
                
            Debug.Assert(null != overwriteCb,
                "Overwrite callback is null.  Cannot continue.");
            if (null == overwriteCb)
                throw new ArgumentException("Overwrite callback is null.  Cannot continue.", "overwriteCb");

            ICollection<String> files = new List<String>();
            using (ZipFile zf = new ZipFile(filename))
            {
                zf.ExtractProgress += delegate(Object sender, ExtractProgressEventArgs e)
                {
                    if (e.EventType.HasFlag(ZipProgressEventType.Extracting_ExtractEntryWouldOverwrite))
                    {
                        String dest = Path.Combine(destination, e.CurrentEntry.FileName);
                        if (overwriteCb(filename, e.CurrentEntry, dest))
                            e.CurrentEntry.ExtractExistingFile = ExtractExistingFileAction.OverwriteSilently;
                        else
                            e.CurrentEntry.ExtractExistingFile = ExtractExistingFileAction.DoNotOverwrite;
                    }
                };
                foreach (ZipEntry ze in zf.Entries)
                {
                    String source = ze.FileName;
                    String dest = Path.Combine(destination, ze.FileName);
                    ze.Extract(destination, ExtractExistingFileAction.InvokeExtractProgressEvent);
                    if (File.Exists(dest))
                    {
                        files.Add(dest);

                        if (null != extractCb)
                            extractCb(filename, ze, dest);
                    }
                }
                filelist = files;
            }
            return (true);
        }

        /// <summary>
        /// Extract files from zip file that are newer than in the destination
        /// directory.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="destination"></param>
        /// <returns></returns>
        public static bool ExtractNewer(String filename, String destination)
        {
            IEnumerable<String> filelist = null;
            return (ExtractNewer(filename, destination, null, out filelist));
        }

        /// <summary>
        /// Extract files from zip file that are newer than in the destination
        /// directory.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="destination"></param>
        /// <param name="extractCb"></param>
        /// <returns></returns>
        public static bool ExtractNewer(String filename, String destination, ZipFileExtractionCallback extractCb)
        {
            IEnumerable<String> filelist = null;
            return (ExtractNewer(filename, destination, extractCb, out filelist));
        }

        /// <summary>
        /// Extract files from zip file that are newer than in the destination
        /// directory.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="destination"></param>
        /// <returns></returns>
        public static bool ExtractNewer(String filename, String destination, out IEnumerable<String> filelist)
        {
            return (ExtractNewer(filename, destination, Zip.UpdateToParentTimestamp, out filelist));
        }

        /// <summary>
        /// Extract files from zip file that are newer than in the destination
        /// directory.  Returning file list.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="destination"></param>
        /// <param name="extractCb"></param>
        /// <param name="filelist"></param>
        /// <returns></returns>
        public static bool ExtractNewer(String filename, String destination, ZipFileExtractionCallback extractCb, out IEnumerable<String> filelist)
        {
            Debug.Assert(ZipFile.CheckZip(filename),
                String.Format("Zip file consistency error: '{0}'.", filename));
            if (!ZipFile.CheckZip(filename))
            {
                filelist = new String[] { };
                return (false);
            }

            // Setup overwrite callback to handle destination files that 
            // already exist.
            ZipFileOverwriteCallback overwriteCb = delegate(String zipFilename, ZipEntry source, String dest)
                {
                    bool overwrite = false;
                    if (File.Exists(dest))
                    {
                        DateTime srcTime = source.LastModified.ToUniversalTime();
                        DateTime destTime = File.GetLastWriteTimeUtc(dest);

                        if (srcTime.IsLaterThan(destTime))
                            overwrite = true;
                    }
                    else
                    {
                        // Destination file doesn't exist.
                        overwrite = true;
                    }
                    return (overwrite);
                };

            return (ExtractAll(filename, destination, overwriteCb, extractCb, out filelist));
        }

        /// <summary>
        /// Extract files from zip file that are newer than in the destination
        /// directory. Optionally updates the extracted file timestamp to match
        /// the timestamp of the parent zip.
        /// </summary>
        /// <param name="filename">Input file extract newest files from.</param>
        /// <param name="destination">The destination output directory.</param>
        /// <param name="updateToParentTimestamp">Whether or not the extracted files should have their timestamp match the timestamp of the source file.</param>
        /// <returns>Whether or not the extraction succeeded.</returns>
        public static bool ExtractNewer(String filename, String destination, bool updateToParentTimestamp)
        {
            IEnumerable<String> filelist = null;
            if (updateToParentTimestamp)
            {
                return ExtractNewer(filename, destination, Zip.UpdateToParentTimestamp, out filelist);
            }
            else
            {
                return ExtractNewer(filename, destination, null, out filelist);
            }
        }

        /// <summary>
        /// Extracts the file within the zipFilename, outputting a MemoryStream.
        /// </summary>
        /// <param name="zipFilename"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static MemoryStream ExtractFile(String zipFilename, String filename)
        {
            using (ZipFile zf = ZipFile.Read(zipFilename))
            {
                ICollection<ZipEntry> entries = zf.Entries;
                foreach (ZipEntry entry in entries)
                {
                    if (String.Compare(filename, entry.FileName, true) == 0)
                    {
                        MemoryStream stream = new MemoryStream();
                        entry.Extract(stream);
                        return stream;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Extracts a file from a zip within a memory stream.
        /// Used for extracting zip files within zip files.
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="filename"></param>
        public static MemoryStream ExtractFile(MemoryStream stream, String filename)
        {
            using (ZipFile zip = ZipFile.Read(stream.GetBuffer()))
            {
                ICollection<ZipEntry> entries = zip.Entries;
                foreach (ZipEntry entry in entries)
                {
                    if (String.Compare(filename, entry.FileName, true) == 0)
                    {
                        MemoryStream zipStream = new MemoryStream();
                        entry.Extract(zipStream);
                        return zipStream;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Read list of files in the zip archive.
        /// </summary>
        /// <param name="filename">Zipfile filename</param>
        /// <param name="filelist">Output list of files.</param>
        /// <returns></returns>
        public static bool GetFileList(String filename, out IEnumerable<String> filelist)
        {
            Debug.Assert(ZipFile.CheckZip(filename),
                String.Format("Zip file consistency error: '{0}'.", filename));
            if (!ZipFile.CheckZip(filename))
            {
                filelist = new String[] { };
                return (false);
            }

            using (ZipFile zf = new ZipFile(filename))
            {
                String[] files = new String[zf.EntryFileNames.Count];
                zf.EntryFileNames.CopyTo(files, 0);
                filelist = files;
            }
            return (true);
        }
        #endregion // Zip File Extraction Methods

        #region Zipfile editing methods
        /// <summary>
        /// Create a zip file; defining the destination files (folder support).
        /// </summary>
        /// <param name="branch">Branch.</param>
        /// <param name="filename"></param>
        /// <param name="files"></param>
        /// <param name="useFullPath"></param>
        /// <param name="callback"></param>
        /// <returns></returns>
        public static bool Patch(IBranch branch, String filename, IEnumerable<Pair<String, String>> files, bool useFullPath, EventHandler<SaveProgressEventArgs> callback = null)
        {
            if (!File.Exists(filename))
            {
                throw (new FileNotFoundException("Zip file can't be patched", filename));
            }
            bool result = true;
            using (ZipFile zf = new ZipFile(filename))
            {
                try
                {
                    zf.SaveProgress += callback;
                    String filesCommentString = String.Empty;
                    foreach (Pair<String, String> pair in files)
                    {
                        String source = pair.First;
                        String destination = pair.Second;

                        if (!File.Exists(source))
                        {
                            Log.StaticLog.ErrorCtx(LOG_CTX, "Input file: {0} does not exist.", source);
                            continue; // Skip file.
                        }

                        ZipEntry ze = null;
#warning DHM FIX ME: not sure this is good idea to strip out; should it be handled in the Asset Pack Processor?
#warning Flo Fix it: ze is not used anyway. Is this dead code ?
                        if (useFullPath || Path.HasExtension(destination))
                            ze = zf.UpdateFile(source, Path.GetDirectoryName(destination));
                        else
                            ze = zf.UpdateFile(source, destination);
                        filesCommentString += "\n\t- " + destination + "/" + Path.GetFileName(source);
                    }

                    // Set comment based on tools version, user and host.
                    IConfig config = branch.Project.Config;
                    zf.Comment += String.Format("\nPatched on {0} with version: {1} by {2}@{3} with files:{4}", DateTime.Now.ToString(), config.Version.BaseVersion.LabelName,
                        Environment.UserName, Environment.MachineName, filesCommentString);

                    // Set other properties and save.
                    zf.UseZip64WhenSaving = Zip64Option.AsNecessary;
                    zf.CompressionLevel = Ionic.Zlib.CompressionLevel.Default;
                    zf.Save();
                }
                catch (Exception ex)
                {
                    Log.StaticLog.ExceptionCtx(LOG_CTX, ex, "Exception during zip file construction.");
                    result = false;
                }
                finally
                {
                    zf.SaveProgress -= callback;
                }
            }

            return (result);
        }

        #region Private Methods
        /// <summary>
        /// Callback handler to ensure files extracted get Now as modified time.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="source"></param>
        /// <param name="destination"></param>
        private static void UpdateWriteTime(String filename, ZipEntry source, String destination)
        {
            File.SetLastWriteTime(destination, DateTime.Now);
        }

        /// <summary>
        /// Updates the extracted file path to have the same write time as the parent zip file.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="source"></param>
        /// <param name="destination"></param>
        private static void UpdateToParentTimestamp(String filename, ZipEntry source, String destination)
        {
            DateTime parentZipModifiedTime = File.GetLastWriteTime(filename);
            if (System.IO.File.Exists(destination))
            {
                System.IO.FileAttributes attributes = System.IO.File.GetAttributes(destination);
                if (attributes.HasFlag(System.IO.FileAttributes.ReadOnly))
                {
                    System.IO.File.SetAttributes(destination, attributes ^ System.IO.FileAttributes.ReadOnly);
                }
            }
            File.SetLastWriteTime(destination, parentZipModifiedTime);
        }
        #endregion // Private Methods
    }

    #endregion //Zipfile editing methods

} // RSG.Pipeline.Services namespace
