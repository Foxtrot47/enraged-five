﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using RSG.Base.Configuration;

namespace RSG.Pipeline.Services.CollisionProcessor
{

    /// <summary>
    /// Collision Processor Task object.
    /// </summary>
    public class CollisionProcessorTask
    {
        #region Constants
        private static readonly String CONFIG_OP = "Operation";
        private static readonly String CONFIG_OUTPUT = "Output";
        private static readonly String CONFIG_FILENAME = "ConfigFilename";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Operation field.
        /// </summary>
        public Operation Operation
        {
            get;
            private set;
        }

        /// <summary>
        /// Operation configuration file (optional).
        /// </summary>
        public String ConfigFilename
        {
            get;
            private set;
        }

        /// <summary>
        /// Output directory absolute filename.
        /// </summary>
        public String OutputAsset
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="op"></param>
        /// <param name="configFilename"></param>
        /// <param name="outputAsset"></param>
        public CollisionProcessorTask(IBranch branch, Operation op, 
            String configFilename, String outputAsset)
        {
            this.Operation = op;
            this.ConfigFilename = branch.Environment.Subst(configFilename);
            this.OutputAsset = outputAsset;
        }

        /// <summary>
        /// Constructor; from XElement.
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="xmlTaskElem"></param>
        public CollisionProcessorTask(IBranch branch, XElement xmlTaskElem)
        {
            Operation op;
            if (Enum.TryParse(xmlTaskElem.Element(CONFIG_OP).Value, out op))
                this.Operation = op;
            if (null != xmlTaskElem.Element(CONFIG_FILENAME))
                this.ConfigFilename = branch.Environment.Subst(xmlTaskElem.Element(CONFIG_FILENAME).Value);
            this.OutputAsset = xmlTaskElem.Element(CONFIG_OUTPUT).Value;
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Serialise the task object to an XElement.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public XElement ToXElement(String name)
        {
            return (new XElement(name,
                new XElement(CONFIG_OP, this.Operation),
                new XElement(CONFIG_FILENAME, this.ConfigFilename),
                new XElement(CONFIG_OUTPUT, this.OutputAsset)));
        }
        #endregion // Controller Methods
    }

} // RSG.Pipeline.Services.CollisionProcessor namespace
