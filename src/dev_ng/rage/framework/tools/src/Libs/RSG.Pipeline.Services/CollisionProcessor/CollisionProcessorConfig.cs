﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using RSG.Base.Configuration;

namespace RSG.Pipeline.Services.CollisionProcessor
{

    /// <summary>
    /// Collision Processor configuration data.
    /// </summary>
    public class CollisionProcessorConfig
    {
        #region Constants
        private static readonly String CONFIG_ROOT = "CollisionProcessorConfig";
        private static readonly String CONFIG_TASKS = "Tasks";
        private static readonly String CONFIG_TASK = "Task";
        private static readonly String CONFIG_INPUTS = "Inputs";
        private static readonly String CONFIG_INPUT = "Input";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Tasks with operations to run for this Asset Processor instance.
        /// </summary>
        public IEnumerable<CollisionProcessorTask> Tasks
        {
            get;
            private set;
        }

        /// <summary>
        /// Operation input asset absolute filenames.
        /// </summary>
        public IEnumerable<String> InputAssets
        {
            get;
            private set;
        }

        /// <summary>
        /// Output asset absolute filename.
        /// </summary>
        public String OutputAsset
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor; from filename.  Use 'Load' static method instead.
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="filename"></param>
        private CollisionProcessorConfig(IBranch branch, String filename)
        {
            XDocument xmlDoc = XDocument.Load(filename);
            this.InputAssets = xmlDoc.Root.Element(CONFIG_INPUTS).
                Elements(CONFIG_INPUT).Select(xmlInputElem => xmlInputElem.Value);
            this.Tasks = xmlDoc.Root.Element(CONFIG_TASKS).
                Elements(CONFIG_TASK).Select(xmlTaskElem => new CollisionProcessorTask(branch, xmlTaskElem));
        }

        /// <summary>
        /// Constructor; from enumerable of tasks.
        /// </summary>
        /// <param name="inputAssets"></param>
        /// <param name="tasks"></param>
        public CollisionProcessorConfig(IEnumerable<String> inputAssets, 
            IEnumerable<CollisionProcessorTask> tasks)
        {
            this.InputAssets = inputAssets;
            this.Tasks = tasks;
        }
        #endregion // Constructor(s)

        #region Static Controller Methods
        /// <summary>
        /// Construction; from absolute filename.
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static CollisionProcessorConfig Load(IBranch branch, String filename)
        {
            return (new CollisionProcessorConfig(branch, filename));
        }
        #endregion // Static Controller Methods

        #region Controller Methods
        /// <summary>
        /// Serialise the Asset Processor configuration to XML file.
        /// </summary>
        /// <param name="filename"></param>
        public void Save(String filename)
        {
            XDocument xmlDoc = new XDocument(
                new XDeclaration("1.0", "utf-8", "yes"),
                new XElement(CONFIG_ROOT,
                    new XElement(CONFIG_INPUTS,
                        this.InputAssets.Select(input => new XElement(CONFIG_INPUT, input))),
                    new XElement(CONFIG_TASKS,
                        Tasks.Select(task => task.ToXElement(CONFIG_TASK)))));
            xmlDoc.Save(filename);
        }
        #endregion // Controller Methods
    }

} // RSG.Pipeline.Services.CollisionProcessor namespace
