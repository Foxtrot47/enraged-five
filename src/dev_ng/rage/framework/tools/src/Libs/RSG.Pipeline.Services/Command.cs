﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using RSG.Base.Collections;

namespace RSG.Pipeline.Services
{

    /// <summary>
    /// 
    /// </summary>
    public class Command
    {
        #region Properties
        /// <summary>
        /// Process executable.
        /// </summary>
        public String Executable
        {
            get;
            private set;
        }

        /// <summary>
        /// Process argument string.
        /// </summary>
        public String Arguments
        {
            get;
            private set;
        }

        /// <summary>
        /// Working directory of the process.
        /// </summary>
        public String WorkingDirectory
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        private RSG.Base.Logging.ILog Log
        {
            get;
            set;
        }
        #endregion // Properties

        /// <summary>
        /// 
        /// </summary>
        /// <param name="executable"></param>
        /// <param name="arguments"></param>
        public Command(String executable, String arguments)
            : this(executable, arguments, Directory.GetCurrentDirectory())
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="executable"></param>
        /// <param name="arguments"></param>
        /// <param name="workingDir"></param>
        public Command(String executable, String arguments, String workingDir)
        {
            this.Executable = executable;
            this.Arguments = arguments;
            this.WorkingDirectory = workingDir;

            Debug.Assert(File.Exists(this.Executable),
                String.Format("Command executable does not exist: \"{0}\".", this.Executable));
        }

        #region Controller Methods
        /// <summary>
        /// Execute the command.
        /// </summary>
        /// <returns></returns>
        public int Run(RSG.Base.Logging.ILog log)
        {
            this.Log = log;

            ProcessStartInfo ps = new ProcessStartInfo();
            ps.FileName = this.Executable;
            ps.Arguments = this.Arguments;
            ps.CreateNoWindow = true;
            ps.UseShellExecute = false;
            ps.RedirectStandardOutput = true;
            ps.RedirectStandardError = true;
            
            Process process = new Process();
            process.StartInfo = ps;
            process.Start();

            process.ErrorDataReceived += ReadStandardError;
            process.BeginErrorReadLine();
            process.StandardOutput.ReadToEnd();

            // Wait
            process.WaitForExit();

            Debug.Assert(process.HasExited);
            return (process.ExitCode);
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReadStandardError(Object sender, DataReceivedEventArgs e)
        {
            if (null != this.Log)
                return;

            this.Log.Error(e.Data);
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Services namespace
