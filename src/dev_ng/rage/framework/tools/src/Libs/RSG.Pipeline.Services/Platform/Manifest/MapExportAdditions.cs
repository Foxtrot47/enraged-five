﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;

namespace RSG.Pipeline.Services.Platform.Manifest
{

    /// <summary>
    /// Map Export Additions XML file reader.
    /// </summary>
    /// The 'MapExportAdditions' XML file is output by the map metadata serialiser
    /// and is a major input to the RPF manifest constructor.
    /// 
    public class MapExportAdditions
    {
        #region Properties
        /// <summary>
        /// Filename.
        /// </summary>
        public String Filename
        {
            get;
            private set;
        }

        /// <summary>
        /// Collection of ITYP streaming dependencies.
        /// </summary>
        public IEnumerable<ITYPDependency2> ITYPDependencies2
        {
            get { return itypDependencies2_; }
        }

        /// <summary>
        /// Collection of ITYP streaming dependencies.
        /// </summary>
        public IEnumerable<IMAPDependency2> IMAPDependencies2
        {
            get { return imapDependencies2_; }
        }

        /// <summary>
        /// (OBSOLETE)
        /// </summary>
        public IEnumerable<IMAPDependency> IMAPDependencies
        {
            get { return imapDependencies_; }
        }

        public IEnumerable<IMAPGroupMapData> IMAPGroupData
        {
            get { return imapGroupData_; }
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor; from filename.
        /// </summary>
        /// <param name="filename"></param>
        public MapExportAdditions(String filename)
        {
            itypDependencies2_ = new List<ITYPDependency2>();
            imapDependencies2_ = new List<IMAPDependency2>();
            imapDependencies_ = new List<IMAPDependency>();
            imapGroupData_ = new List<IMAPGroupMapData>();

            Load(filename);
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// Load XML data.
        /// </summary>
        /// <param name="filename"></param>
        private void Load(String filename)
        {
            this.Filename = filename;
            XDocument xmlDoc = XDocument.Load(filename);

            IEnumerable<XElement> xmlIMAPDepends = xmlDoc.XPathSelectElements(
                "/ManifestData/IMAPDependency");
            foreach (XElement xmlIMAPDependency in xmlIMAPDepends)
            {
                imapDependencies_.Add(IMAPDependency.FromMapExportAdditions(xmlIMAPDependency));
            }

            // ITYP->ITYP Dependencies
            IEnumerable<XElement> xmlITYPDependencies = xmlDoc.XPathSelectElements(
                "/ManifestData/ITYPDependency2");
            foreach (XElement xmlITYPDependency in xmlITYPDependencies)
            {
                itypDependencies2_.Add(ITYPDependency2.FromMapExportAdditions(xmlITYPDependency));
            }

            // ITYP->IMAP Dependencies
            IEnumerable<XElement> xmlIMAPDependencies = xmlDoc.XPathSelectElements(
                "/ManifestData/IMAPDependency2");
            foreach (XElement xmlIMAPDependency in xmlIMAPDependencies)
            {
                imapDependencies2_.Add(IMAPDependency2.FromMapExportAdditions(xmlIMAPDependency));
            }

            foreach (XElement imapGroupElement in xmlDoc.Root.Elements("IMAPGroup"))
            {
                imapGroupData_.Add(IMAPGroupMapData.FromMapExportAdditions(imapGroupElement));
            }
        }
        #endregion // Private Methods

        #region Private Data
        List<ITYPDependency2> itypDependencies2_;
        List<IMAPDependency2> imapDependencies2_;
        List<IMAPDependency> imapDependencies_;
        List<IMAPGroupMapData> imapGroupData_;
        #endregion
    }

} // RSG.Pipeline.Services.Platform.Manifest namespace
