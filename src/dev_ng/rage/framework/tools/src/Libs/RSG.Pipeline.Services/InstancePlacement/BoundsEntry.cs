﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

using RSG.Base.Math;

namespace RSG.Pipeline.Services.InstancePlacement
{
    /// <summary>
    /// Class to hold a bounds entry that is used when building up the bounds manifest file.
    /// This is basically a listing of a handful of containers with the bounds extents and 
    /// a collision path for each container.
    /// </summary>
    public class BoundsEntry
    {
        #region Constants
        private static readonly String MIN_X = "minX";
        private static readonly String MIN_Y = "minY";
        private static readonly String MIN_Z = "minZ";
        private static readonly String MAX_X = "maxX";
        private static readonly String MAX_Y = "maxY";
        private static readonly String MAX_Z = "maxZ";
        private static readonly String COLLISION_PATH = "collisionPath";
        private static readonly String BOUNDS_ENTRY = "BoundsEntry";
        #endregion

        #region Properties
        /// <summary>
        /// Collision path for the entry.
        /// </summary>
        public String CollisionPath
        {
            get;
            private set;
        }

        /// <summary>
        /// Bounding box extents from a specific collision zip file.
        /// </summary>
        public BoundingBox3f BoundingBox
        {
            get;
            private set;
        }
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Default Constructor
        /// </summary>
        public BoundsEntry()
        {
            CollisionPath = "";
            BoundingBox = new BoundingBox3f();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="collisionPath"></param>
        /// <param name="boundingBox"></param>
        public BoundsEntry(String collisionPath, BoundingBox3f boundingBox)
        {
            CollisionPath = collisionPath;
            BoundingBox = boundingBox;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Takes our current values and creates an XElement to serialise.
        /// </summary>
        /// <param name="elementName"></param>
        /// <returns></returns>
        public XElement ToXElement(String elementName)
        {
            XElement xmlOutputBounds = new XElement(elementName,
                new XElement(MIN_X, new XAttribute("value", BoundingBox.Min.X)),
                new XElement(MIN_Y, new XAttribute("value", BoundingBox.Min.Y)),
                new XElement(MIN_Z, new XAttribute("value", BoundingBox.Min.Z)),
                new XElement(MAX_X, new XAttribute("value", BoundingBox.Max.X)),
                new XElement(MAX_Y, new XAttribute("value", BoundingBox.Max.Y)),
                new XElement(MAX_Z, new XAttribute("value", BoundingBox.Max.Z)),
                new XElement(COLLISION_PATH, new XAttribute("value", CollisionPath)));

            return xmlOutputBounds;
        }

        /// <summary>
        /// Load data from an XElement into our class variables.
        /// </summary>
        /// <param name="element"></param>
        public void FromXElement(XElement element)
        {
            BoundingBox.Min.X = float.Parse(element.Element(MIN_X).Attribute("value").Value);
            BoundingBox.Min.Y = float.Parse(element.Element(MIN_Y).Attribute("value").Value);
            BoundingBox.Min.Z = float.Parse(element.Element(MIN_Z).Attribute("value").Value);
            BoundingBox.Max.X = float.Parse(element.Element(MAX_X).Attribute("value").Value);
            BoundingBox.Max.Y = float.Parse(element.Element(MAX_Y).Attribute("value").Value);
            BoundingBox.Max.Z = float.Parse(element.Element(MAX_Z).Attribute("value").Value);
            CollisionPath = element.Element(COLLISION_PATH).Attribute("value").Value;
        }
        #endregion

        #region Static Methods
        /// <summary>
        /// Loads in a bounds manifest file and returns a list of all the BoundEntry objects.
        /// </summary>
        /// <param name="boundsManifestXML"></param>
        /// <returns></returns>
        public static IEnumerable<BoundsEntry> LoadBoundsManifest(String boundsManifestXML)
        {
            List<BoundsEntry> boundsEntries = new List<BoundsEntry>();

            XDocument doc = XDocument.Load(boundsManifestXML);

            foreach (XElement element in doc.Root.Elements(BOUNDS_ENTRY))
            {
                BoundsEntry entry = new BoundsEntry();
                entry.FromXElement(element);
                boundsEntries.Add(entry);
            }

            return boundsEntries;
        }
        #endregion
    }
}
