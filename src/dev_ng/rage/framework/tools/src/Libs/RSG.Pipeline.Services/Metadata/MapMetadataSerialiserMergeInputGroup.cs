﻿//---------------------------------------------------------------------------------------------
// <copyright file="MapMetadataSerialiserMergeInputGroup.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Services.Metadata
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Xml.Linq;

    /// <summary>
    /// Map Metadata Serialiser input object (for merge tasks).
    /// </summary>
    public class MapMetadataSerialiserMergeInputGroup
    {
        #region Properties
        /// <summary>
        /// Group prefix.
        /// </summary>
        public String Prefix
        {
            get;
            private set;
        }

        /// <summary>
        /// SceneXml absolute filename.
        /// </summary>
        public IEnumerable<String> SceneFilenames
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="sceneFilename"></param>
        public MapMetadataSerialiserMergeInputGroup(String prefix, IEnumerable<String> sceneFilenames)
        {
            this.Prefix = prefix;
            this.SceneFilenames = sceneFilenames;
        }

        /// <summary>
        /// Constructor; from XML element.
        /// </summary>
        /// <param name="xmlInputElem"></param>
        public MapMetadataSerialiserMergeInputGroup(XElement inputGroupElement)
        {
            this.Prefix = inputGroupElement.Element("Prefix").Value;
            this.SceneFilenames = inputGroupElement.Elements("Input").Select(inputElement => inputElement.Value).ToArray();
        }
        #endregion // Constructor(s)

        #region Internal Methods
        /// <summary>
        /// Serialise input to an XElement.
        /// </summary>
        /// <returns></returns>
        internal XElement Serialise()
        {
            XElement xmlInputGroupElem = new XElement("InputGroups",
                new XElement("Prefix", Prefix),
                SceneFilenames.Select(sceneFilename => new XElement("Input", sceneFilename)));
            return (xmlInputGroupElem);
        }
        #endregion // Internal Methods
    }

} // RSG.Pipeline.Services.Metadata namespace
