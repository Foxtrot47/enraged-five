﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProfileSession.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace RSG.Services.Statistics.Contracts.DataContracts
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [KnownType(typeof(ExportProfileSession))]
    public class ProfileSession
    {
        [DataMember]
        public Guid ApplicationSessionGuid { get; set; }

        [DataMember]
        public DateTime EndTime { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public Guid? ParentSessionGuid { get; set; }

        [DataMember]
        public IList<ProfileSample> Samples { get; set; }

        [DataMember]
        public Guid SessionGuid { get; set; }

        [DataMember]
        public DateTime StartTime { get; set; }
    }
}
