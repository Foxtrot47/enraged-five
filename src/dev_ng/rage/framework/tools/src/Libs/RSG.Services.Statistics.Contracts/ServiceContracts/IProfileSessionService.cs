﻿//---------------------------------------------------------------------------------------------
// <copyright file="IProfileSessionService.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System.ServiceModel;
using RSG.Services.Statistics.Contracts.DataContracts;

namespace RSG.Services.Statistics.Contracts.ServiceContracts
{
    /// <summary>
    /// Service contract for profiling information.
    /// </summary>
    [ServiceContract]
    public interface IProfileSessionService
    {
        /// <summary>
        /// Tracks a new profile session.
        /// </summary>
        /// <param name="profileSessionDto"></param>
        [OperationContract(IsOneWay = true)]
        void AddSession(ProfileSession profileSessionDto);
    }
}
