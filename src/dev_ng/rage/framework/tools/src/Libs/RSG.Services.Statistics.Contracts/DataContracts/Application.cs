﻿//---------------------------------------------------------------------------------------------
// <copyright file="Application.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System.Runtime.Serialization;

namespace RSG.Services.Statistics.Contracts.DataContracts
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class Application
    {
        [DataMember]
        public string ApplicationName { get; set; }

        [DataMember]
        public string Version { get; set; }

        [DataMember]
        public string Project { get; set; }

        [DataMember]
        public string ToolsRoot { get; set; }

        [DataMember]
        public string LaunchLocation { get; set; }
    }
}
