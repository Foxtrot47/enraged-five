﻿//---------------------------------------------------------------------------------------------
// <copyright file="ApplicationSession.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Runtime.Serialization;

namespace RSG.Services.Statistics.Contracts.DataContracts
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class ApplicationSession
    {
        [DataMember]
        public Guid SessionGuid { get; set; }

        [DataMember]
        public DateTime OpenTimestamp { get; set; }

        [DataMember]
        public Guid? ParentSessionGuid { get; set; }

        [DataMember]
        public Application Application { get; set; }

        [DataMember]
        public User User { get; set; }

        [DataMember]
        public Machine Machine { get; set; }
    }
}
