﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProfileSample.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace RSG.Services.Statistics.Contracts.DataContracts
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class ProfileSample
    {
        [DataMember]
        public IList<ProfileSample> ChildSamples { get; set; }

        [DataMember]
        public DateTime EndTime { get; set; }

        [DataMember]
        public string Metric { get; set; }

        [DataMember]
        public DateTime StartTime { get; set; }
    }
}
