﻿//---------------------------------------------------------------------------------------------
// <copyright file="Machine.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System.Runtime.Serialization;

namespace RSG.Services.Statistics.Contracts.DataContracts
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class Machine
    {
        [DataMember]
        public string MachineName { get; set; }

        [DataMember]
        public string ProcessorName { get; set; }

        [DataMember]
        public uint LogicalProcessorCount { get; set; }

        [DataMember]
        public uint CoreCount { get; set; }

        [DataMember]
        public ulong MemoryCapacity { get; set; }

        [DataMember]
        public string GraphicsCardName { get; set; }
    }
}
