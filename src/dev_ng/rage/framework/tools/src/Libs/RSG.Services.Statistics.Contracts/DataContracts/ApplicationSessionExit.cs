﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Services.Statistics.Contracts.DataContracts
{
    [DataContract]
    public class ApplicationSessionExit
    {
        [DataMember]
        public DateTime CloseTimestamp { get; set; }

        [DataMember]
        public int ExitCode { get; set; }

        [DataMember]
        public Guid SessionGuid { get; set; }
    }
}
