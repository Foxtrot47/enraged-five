﻿//---------------------------------------------------------------------------------------------
// <copyright file="IApplicationSessionService.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System.ServiceModel;
using RSG.Services.Statistics.Contracts.DataContracts;

namespace RSG.Services.Statistics.Contracts.ServiceContracts
{
    /// <summary>
    /// Service contract for application session information.
    /// </summary>
    [ServiceContract]
    public interface IApplicationSessionService
    {
        /// <summary>
        /// Tracks a new application session.
        /// </summary>
        /// <param name="applicationSessionDto"></param>
        [OperationContract(IsOneWay = true)]
        void AddSession(ApplicationSession applicationSessionDto);
        
        /// <summary>
        /// Updates an application session with exit information.
        /// </summary>
        /// <param name="applicationSessionExitDto"></param>
        [OperationContract(IsOneWay = true)]
        void SetSessionExitInfo(ApplicationSessionExit applicationSessionExitDto);
    }
}
