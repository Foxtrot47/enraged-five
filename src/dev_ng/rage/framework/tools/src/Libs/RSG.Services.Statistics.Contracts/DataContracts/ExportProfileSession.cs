﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Services.Statistics.Contracts.DataContracts
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class ExportProfileSession : ProfileSession
    {
        [DataMember]
        public IEnumerable<string> ExportedFiles { get; set; }

        [DataMember]
        public string ExportType { get; set; }

        [DataMember]
        public bool IncredibuildEnabled { get; set; }

        [DataMember]
        public bool IncredibuildStandalone { get; set; }

        [DataMember]
        public int IncredibuildForcedCPUCount { get; set; }

        [DataMember]
        public IEnumerable<RSG.Platform.Platform> Platforms { get; set; }

        [DataMember]
        public long PrivateBytesEnd { get; set; }

        [DataMember]
        public long PrivateBytesStart { get; set; }

        [DataMember]
        public bool Success { get; set; }

        [DataMember]
        public string ToolsVersion { get; set; }
    }
}
