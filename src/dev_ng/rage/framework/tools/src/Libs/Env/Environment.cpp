//
// filename:	Environment.cpp
// author:		David Muir
// date:		18 April 2008
// description:	Generic environment class implementation.
//

// --- Include Files ------------------------------------------------------------

// Local Headers Files
#include "Environment.h"

// Stdlib Headers Files
#include <algorithm>

// Boost Headers Files
#pragma warning( push )
#pragma warning( disable : 4512 )
#include "boost/algorithm/string/replace.hpp"
#pragma warning( pop )

// --- Constants ----------------------------------------------------------------


// --- Code ---------------------------------------------------------------------

cEnvironment::cEnvironment( )
	: m_pSymbolTable( NULL )
{
	m_vEnvStack.clear();

	// Create new stack level.
	push( );
}

cEnvironment::~cEnvironment( )
{
	// Clear the Symbol Table stack
	for ( tEnvStack::iterator itEnv = m_vEnvStack.begin();
		itEnv != m_vEnvStack.end();
		++itEnv )
	{
		if ( NULL != (*itEnv) )
		{
			(*itEnv)->clear();
			delete (*itEnv);
			(*itEnv) = NULL;
		}
	}
	m_vEnvStack.clear();
}

bool
cEnvironment::lookup( const std::string& varname, std::string& varvalue )
{
	for ( tEnvStack::reverse_iterator itEnv = m_vEnvStack.rbegin();
		itEnv != m_vEnvStack.rend();
		++itEnv )
	{
		tSymbolTable::iterator itSym = (*itEnv)->find( varname );
		if ( (*itEnv)->end() != itSym )
		{
			varvalue = (*itSym).second;
			return ( true );
		}
	}

	return ( false );
}

void
cEnvironment::subst( std::string& output, const std::string& input)
{
	output = input;
	for ( tEnvStack::reverse_iterator itEnv = m_vEnvStack.rbegin();
		itEnv != m_vEnvStack.rend();
		++itEnv )
	{
		for ( tSymbolTable::iterator itSym = (*itEnv)->begin();
			itSym != (*itEnv)->end();
			++itSym )
		{
			std::string realvarname = "$(" + (*itSym).first + ")";
			std::string varvalue = (*itSym).second;
			output = boost::replace_all_copy( output, realvarname, varvalue );
		}
	}
}

void 
cEnvironment::subst( std::string& io )
{
	for ( tEnvStack::reverse_iterator itEnv = m_vEnvStack.rbegin();
		itEnv != m_vEnvStack.rend();
		++itEnv )
	{
		for ( tSymbolTable::iterator itSym = (*itEnv)->begin();
			itSym != (*itEnv)->end();
			++itSym )
		{
			std::string realvarname = "$(" + (*itSym).first + ")";
			std::string varvalue = (*itSym).second;
			boost::replace_all( io, realvarname, varvalue );
		}
	}
}

void 
cEnvironment::add( const std::string& varname, const std::string& varvalue )
{
	(*m_pSymbolTable)[varname] = varvalue;
}

void 
cEnvironment::clear( )
{
	m_pSymbolTable->clear( );
}

size_t
cEnvironment::size( )
{
	return ( m_pSymbolTable->size() );
}

void 
cEnvironment::push( )
{
	tSymbolTable* pSymTbl = new tSymbolTable();

#if _DEBUG
	size_t size = m_vEnvStack.size();
#endif
	m_vEnvStack.push_back( pSymTbl );
	assert( m_vEnvStack.size() == ( size+1 ) );

	m_pSymbolTable = pSymTbl;
}

void 
cEnvironment::pop( )
{
	// Don't pop last environment stack symbol table.
	if ( 1 == m_vEnvStack.size() )
		return;

	size_t nIndex = m_vEnvStack.size() - 1;
	tSymbolTable* pSymTbl = m_vEnvStack.at( nIndex );
	if ( NULL != pSymTbl )
	{
		delete pSymTbl;
		m_vEnvStack[nIndex] = NULL;
	}

	m_vEnvStack.pop_back( );
	m_pSymbolTable = m_vEnvStack.at( m_vEnvStack.size() - 1 );
}

// End of file
