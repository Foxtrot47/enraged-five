//
// filename:	Environment.h
// author:		David Muir
// date:		18 April 2008
// description:	Generic environment class interface
//

#ifndef INC_ENVIRONMENT_H_
#define INC_ENVIRONMENT_H_

// --- Include Files ------------------------------------------------------------

// C++ Stdlib Header Files
#include <map>
#include <string>
#include <vector>

// Boost Header Files
#include "boost/shared_ptr.hpp"
#include "boost/weak_ptr.hpp"

// --- Classes ------------------------------------------------------------------

/**
 * Generic environment class.
 */
class cEnvironment
{
public:
	cEnvironment( );
	~cEnvironment( );

	bool lookup( const std::string& varname, std::string& varvalue );
	void subst( std::string& output, const std::string& input );
	void subst( std::string& io );

	void add( const std::string& varname, const std::string& varvalue );
	void clear( );
	size_t size( );

	void push( );
	void pop( );

protected:
	typedef std::map<const std::string, std::string> tSymbolTable;
	typedef std::vector<tSymbolTable*> tEnvStack;

	tSymbolTable* m_pSymbolTable;
	tEnvStack m_vEnvStack;
};

// --- Globals ------------------------------------------------------------------

//! Shared-ownership pointer to a cEnvironment object
typedef boost::shared_ptr<cEnvironment> cEnvironmentSPtr;
//! Non-owning pointer to a cEnvironment object (owned by shared-pointer)
typedef boost::weak_ptr<cEnvironment> cEnvironmentWPtr;

#endif // !INC_ENVIRONMENT_H_

// End of file
