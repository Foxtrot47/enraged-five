﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.Widgets.Input
{
    /// <summary>
    /// Keep in sync with rage/base/src/input/pad.h
    /// </summary>
    public enum AnalogPadButton
    {
        /// <summary>
        /// 
        /// </summary>
        None,

        /// <summary>
        /// 
        /// </summary>
        LeftStick,

        /// <summary>
        /// 
        /// </summary>
        RightStick
    }
}
