﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.Widgets.Input
{
    /// <summary>
    /// Keep in sync with rage/base/src/input/pad.h
    /// </summary>
    public enum DigitalPadButton
    {
        /// <summary>
        /// 
        /// </summary>
        None = 0x0,

        /// <summary>
        /// 
        /// </summary>
        L2 = 0x0001,

        /// <summary>
        /// 
        /// </summary>
        R2 = 0x0002,

        /// <summary>
        /// 
        /// </summary>
        L1 = 0x0004,

        /// <summary>
        /// 
        /// </summary>
        R1 = 0x0008,

        /// <summary>
        /// 
        /// </summary>
        RUp = 0x0010,
        
        /// <summary>
        /// 
        /// </summary>
        RRight = 0x0020,

        /// <summary>
        /// 
        /// </summary>
        RDown = 0x0040,

        /// <summary>
        /// 
        /// </summary>
        RLeft = 0x0080,
        
        /// <summary>
        /// 
        /// </summary>
        Select = 0x0100,
        
        /// <summary>
        /// 
        /// </summary>
        L3 = 0x0200,

        /// <summary>
        /// 
        /// </summary>
        R3 = 0x0400,

        /// <summary>
        /// 
        /// </summary>
        Start = 0x0800,

        /// <summary>
        /// 
        /// </summary>
        LUp = 0x1000,

        /// <summary>
        /// 
        /// </summary>
        LRight = 0x2000,
        
        /// <summary>
        /// 
        /// </summary>
        LDown = 0x4000,

        /// <summary>
        /// 
        /// </summary>
        LLeft = 0x8000
    }
}
