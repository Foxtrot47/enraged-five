﻿using RSG.Rag.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.Widgets.Input
{
    /// <summary>
    /// 
    /// </summary>
    [WidgetGuidAttribute('i', 'n', 'p', 't')]
    public class WidgetPad : Widget
    {
        #region Member Data

        //private readonly DigitalPadButton[] _digitalButtonPressed;

        //private readonly AnalogPadButton[] _analogButtonPressed;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public WidgetPad(uint id, IWidgetResolver resolver, IPacketDispatcher dispatcher)
            : base(id, resolver, dispatcher)
        {
        }
        #endregion // Constructor(s)

        #region Properties
        #endregion // Properties

        #region Controller Methods
        /*
        public void DigitalButtonPressed(DigitalPadButton button)
        {
            m_digitalPadButtonsChecked[button] = true;

            BankRemotePacket p = new BankRemotePacket(this.Pipe);
            p.Begin(BankRemotePacket.EnumPacketType.USER_0, GetWidgetTypeGUID());
            p.Write_bkWidget(this.Pipe.LocalToRemote(this));
            p.Write_s32((int)button);
            p.Send();
        }

        public void DigitalButtonReleased(DigitalPadButton button, bool stillChecked)
        {
            m_digitalPadButtonsChecked[button] = stillChecked;

            BankRemotePacket p = new BankRemotePacket(this.Pipe);
            p.Begin(BankRemotePacket.EnumPacketType.USER_1, GetWidgetTypeGUID());
            p.Write_bkWidget(this.Pipe.LocalToRemote(this));
            p.Write_s32((int)button);
            p.Send();
        }

        public void AnalogButtonPresssed(AnalogPadButton button, byte x, byte y)
        {
            m_analogPadButtonsChecked[button] = true;
            m_analogStickPositions[(int)button - 1] = new Point(x, y);

            BankRemotePacket p = new BankRemotePacket(this.Pipe);
            p.Begin(BankRemotePacket.EnumPacketType.USER_2, GetWidgetTypeGUID());
            p.Write_bkWidget(this.Pipe.LocalToRemote(this));
            p.Write_s32((int)button);
            p.Write_u8(x);
            p.Write_u8(y);
            p.Send();
        }

        public void AnalogButtonMoved(AnalogPadButton button, byte x, byte y)
        {
            m_analogPadButtonsChecked[button] = true;
            m_analogStickPositions[(int)button - 1] = new Point(x, y);

            BankRemotePacket p = new BankRemotePacket(this.Pipe);
            p.Begin(BankRemotePacket.EnumPacketType.USER_3, GetWidgetTypeGUID());
            p.Write_bkWidget(this.Pipe.LocalToRemote(this));
            p.Write_s32((int)button);
            p.Write_u8(x);
            p.Write_u8(y);
            p.Send();
        }

        public void AnalogButtonReleased(AnalogPadButton button, byte x, byte y, bool isChecked)
        {
            m_analogPadButtonsChecked[button] = isChecked;
            m_analogStickPositions[(int)button - 1] = new Point(x, y);

            BankRemotePacket p = new BankRemotePacket(this.Pipe);
            p.Begin(BankRemotePacket.EnumPacketType.USER_4, GetWidgetTypeGUID());
            p.Write_bkWidget(this.Pipe.LocalToRemote(this));
            p.Write_s32((int)button);
            p.Write_u8(x);
            p.Write_u8(y);
            p.Send();
        }
        */
        #endregion // Controller Methods

        #region Message Processing
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void ProcessMessageCore(RemotePacketEventArgs e)
        {
            switch ((BankCommand)e.Packet.Command)
            {
                case BankCommand.Changed:
                    ProcessChangedMessage(e);
                    break;

                // Messages from RAG to the game.
                case BankCommand.User0:
                case BankCommand.User1:
                case BankCommand.User2:
                case BankCommand.User3:
                case BankCommand.User4:
                    e.Packet.ReadToEnd();
                    break;

                default:
                    base.ProcessMessageCore(e);
                    break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        public override bool ProcessCreateMessage(RemotePacketEventArgs e)
        {
            if (!base.ProcessCreateMessage(e))
            {
                return false;
            }

            // TODO
            e.Packet.ReadToEnd();
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void ProcessChangedMessage(RemotePacketEventArgs e)
        {
            // TODO
        }
        #endregion // Message Processing
    } // WidgetPad
}
