﻿//---------------------------------------------------------------------------------------------
// <copyright file="M201410241100_InitialDatabase.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.ToolsService.AssetPipeline.Data.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using RSG.Services.Persistence.Migrations;
    using FluentMigrator;

    /// <summary>
    /// Migration for the initial database creation.
    /// </summary>
    [DatetimeMigration("david.muir", 2014, 09, 28, 22, 43, "Initial database creation")]
    public class M201410241100_InitialDatabase : Migration
    {
        /// <summary>
        /// Migration upgrade method.
        /// </summary>
        public override void Up()
        {
            Create.Table("AssetBuildItem")
                .WithColumn("Id")
                    .AsInt64()
                    .NotNullable()
                    .PrimaryKey()
                    .Identity()
                .WithColumn("ProjectName")
                    .AsString()
                    .NotNullable()
                .WithColumn("BranchName")
                    .AsString()
                    .NotNullable()
                .WithColumn("BuildType")
                    .AsInt32()
                    .NotNullable()
                .WithColumn("StartTime")
                    .AsDateTime()
                    .NotNullable()
                .WithColumn("Duration")
                    .AsUInt64()
                    .NotNullable()
                .WithColumn("State")
                    .AsInt32()
                    .NotNullable()
                .WithColumn("Host")
                    .AsString()
                    .NotNullable()
                .WithColumn("Username")
                    .AsString()
                    .NotNullable()
                .WithColumn("LogFilename")
                    .AsString()
                    .NotNullable();

            Create.Table("SourceFile")
                .WithColumn("Id")
                    .AsInt64()
                    .NotNullable()
                    .PrimaryKey()
                    .Identity()
                .WithColumn("Filename")
                    .AsString()
                    .NotNullable();

            Create.Table("OutputFile")
                .WithColumn("Id")
                    .AsInt64()
                    .NotNullable()
                    .PrimaryKey()
                    .Identity()
                .WithColumn("Filename")
                    .AsString()
                    .NotNullable();

#if false
            Create.Table("ItemSourceFiles")
                .WithColumn("ItemId")
                    .AsInt64()
                    .NotNullable()
                .WithColumn("SourceFileId")
                    .AsInt64()
                    .NotNullable();
            Create.PrimaryKey()
                .OnTable("ItemSourceFiles")
                .Columns("ItemId", "SourceFileId");

            Create.ForeignKey("FK_ItemSourceFiles_ItemId_AssetBuildItem_Id")
                .FromTable("ItemSourceFiles")
                    .ForeignColumn("ItemId")
                .ToTable("AssetBuildItem")
                    .PrimaryColumn("Id");

            Create.ForeignKey("FK_ItemSourceFiles_SourceFileId_SourceFile_Id")
                .FromTable("ItemSourceFiles")
                    .ForeignColumn("SourceFileId")
                .ToTable("SourceFile")
                    .PrimaryColumn("Id");
#endif

            Execute.Sql("CREATE TABLE ItemSourceFiles" +
                        "( " +
                        "ItemId INTEGER NOT NULL," +
                        "SourceFileId INTEGER NOT NULL," +
                        "PRIMARY KEY (ItemId, SourceFileId)," +
                        "FOREIGN KEY (ItemId) REFERENCES AssetBuildItem(Id)," +
                        "FOREIGN KEY (SourceFileId) REFERENCES SourceFile(Id)" +
                        ")");

#if false
            Create.Table("ItemOutputFiles")
                .WithColumn("ItemId")
                    .AsInt64()
                    .NotNullable()
                .WithColumn("OutputFileId")
                    .AsInt64()
                    .NotNullable();
            Create.PrimaryKey()
                .OnTable("ItemOutputFiles")
                .Columns("ItemId", "OutputFileId");

            // Create foreign keys.

            Create.ForeignKey("FK_ItemOutputFiles_ItemId_AssetBuildItem_Id")
                .FromTable("ItemOutputFiles")
                    .ForeignColumn("ItemId")
                .ToTable("AssetBuildItem")
                    .PrimaryColumn("Id");

            Create.ForeignKey("FK_ItemOutputFiles_OutputFileId_OutputFile_Id")
                .FromTable("ItemOutputFiles")
                    .ForeignColumn("OutputFileId")
                .ToTable("OutputFile")
                    .PrimaryColumn("Id");
#endif

            Execute.Sql("CREATE TABLE ItemOutputFiles" +
                        "( " +
                        "ItemId INTEGER NOT NULL," +
                        "OutputFileId INTEGER NOT NULL," +
                        "PRIMARY KEY (ItemId, OutputFileId)," +
                        "FOREIGN KEY (ItemId) REFERENCES AssetBuildItem(Id)," +
                        "FOREIGN KEY (OutputFileId) REFERENCES OutputFile(Id)" +
                        ")");
        }

        /// <summary>
        /// Migration rollback method.
        /// </summary>
        public override void Down()
        {
            Delete.Table("ItemOutputFiles");
            Delete.Table("ItemSourceFiles");
            Delete.Table("OutputFile");
            Delete.Table("SourceFile");
            Delete.Table("AssetBuildItems");
        }
    }

} // RSG.ToolsService.AssetPipeline.Data.Migrations namespace
