﻿//---------------------------------------------------------------------------------------------
// <copyright file="EngineProgressServiceHostFactory.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.ToolsService.AssetPipeline
{
    using System.Reflection;
    using RSG.Configuration;
    using RSG.Services.Configuration;
    using RSG.Services.Configuration.AssetBuildPipeline;
    using RSG.Services.Persistence;
    using RSG.ToolsService.AssetPipeline.Data.Entities;
    using RSG.Services.Configuration.ServiceModel;

    /// <summary>
    /// 
    /// </summary>
    public class EngineProgressServiceHostFactory :
        ConfigServiceHostFactory<IServer>
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="EngineProgressServiceHostFactory"/>
        /// class using the specified server configuration object.
        /// </summary>
        public EngineProgressServiceHostFactory(IServer server)
            : base(server)
        {
        }
        #endregion // Constructor(s)
    }

} // RSG.ToolsService.AssetPipeline namespace