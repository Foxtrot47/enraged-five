﻿//---------------------------------------------------------------------------------------------
// <copyright file="AssetBuildHistoryService.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.ToolsService.AssetPipeline.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using RSG.Pipeline.Services.Engine;
    using RSG.Services.Configuration;
    using RSG.Services.Persistence;

    /// <summary>
    /// Asset Build History service implementation.
    /// </summary>
    internal sealed class AssetBuildHistoryService : 
        TransactionalService,
        IAssetBuildHistoryService
    {
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="server"></param>
        public AssetBuildHistoryService(IServer server)
            : base(server)
        {
        }
        #endregion // Constructor(s)

        #region IAssetBuildHistoryService Interface Methods
        /// <summary>
        /// Return information about client Build History.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<AssetBuildItem> GetBuildHistory()
        {
            throw (new NotImplementedException());
        }

        /// <summary>
        /// Clear the client Build History.
        /// </summary>
        public void ClearBuildHistory()
        {
            throw (new NotImplementedException());
        }
        #endregion // IAssetBuildHistoryService Interface Methods
    }

} // RSG.ToolsService.AssetPipeline.Services namespace
