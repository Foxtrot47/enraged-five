﻿//---------------------------------------------------------------------------------------------
// <copyright file="AssetBuildItem.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.ToolsService.AssetPipeline.Data.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using RSG.Services.Persistence.Data;
    using RSG.Pipeline.Services.Engine;

    /// <summary>
    /// Information regarding an Asset Build Item.
    /// </summary>
    public class AssetBuildItem : DomainEntityBase
    {
        public virtual String ProjectName { get; set; }

        public virtual String BranchName { get; set; }

        public virtual AssetBuildType BuildType { get; set; }

        public virtual DateTime StartTime { get; set; }

        public virtual TimeSpan Duration { get; set; }

        public virtual AssetBuildState State { get; set; }

        public virtual String Host { get; set; }

        public virtual String Username { get; set; }

        public virtual ICollection<String> SourceFilenames { get; set; }

        public virtual ICollection<String> OutputFilenames { get; set; }
        
        public virtual String LogFilename { get; set; }
    }

} // RSG.ToolsService.AssetPipeline.Data.Entities namespace
