﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using RSG.Services.Persistence.Data;
using RSG.ToolsService.AssetPipeline.Data.Entities;

namespace RSG.ToolsService.AssetPipeline.Data.Mappings
{

    public class OutputFileMap : ClassMapping<OutputFile>
    {
        public OutputFileMap()
        {
            Id(x => x.Id, m => m.Generator(Generators.Identity));
            Property(x => x.Filename, y => y.NotNullable(true));
        }
    }

}
