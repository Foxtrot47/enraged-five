﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Services.Persistence.Data;

namespace RSG.ToolsService.AssetPipeline.Data.Entities
{

    /// <summary>
    /// 
    /// </summary>
    public class OutputFile : DomainEntityBase
    {
        public virtual String Filename { get; set; }
    }
}
