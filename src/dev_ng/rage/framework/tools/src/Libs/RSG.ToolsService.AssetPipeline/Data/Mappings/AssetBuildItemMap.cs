﻿//---------------------------------------------------------------------------------------------
// <copyright file="AssetBuildItemMap.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.ToolsService.AssetPipeline.Data.Mappings
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using RSG.Services.Persistence.Data;
    using RSG.ToolsService.AssetPipeline.Data.Entities;

    /// <summary>
    /// NHibernate mapping for the <see cref="RSG.Pipeline.Services.Engine.AssetBuildItem"/> 
    /// class.
    /// </summary>
    public class AssetBuildItemMap : ClassMapping<AssetBuildItem>
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="AssetBuildItemMap"/> class.
        /// </summary>
        public AssetBuildItemMap()
        {
            Id(x => x.Id, m => m.Generator(Generators.Identity));
            Property(x => x.ProjectName, y => y.NotNullable(true));
            Property(x => x.BranchName, y => y.NotNullable(true));
            Property(x => x.BuildType, y => y.NotNullable(true));
            Property(x => x.StartTime, y => y.NotNullable(true));
            Property(x => x.Duration, y => y.NotNullable(true));
            Property(x => x.State, y => y.NotNullable(true));
            Property(x => x.Host, y => y.NotNullable(true));
            Property(x => x.Username, y => y.NotNullable(true));
            Set(x => x.SourceFilenames, m =>
            {
                m.Table("ItemSourceFiles");
                m.Cascade(Cascade.All);
                m.Key(k =>
                    {
                        k.Column("ItemId");
                        k.ForeignKey("FK_ItemSourceFiles_ItemId_AssetBuildItem_Id");
                    });
            },
            r => r.ManyToMany(p =>
                {
                    p.Column("SourceFileId");
                    p.ForeignKey("FK_ItemSourceFiles_SourceFileId_SourceFile_Id");
                }));
            Set(x => x.OutputFilenames, m =>
            {
                m.Table("ItemOutputFiles");
                m.Cascade(Cascade.All);
                m.Key(k =>
                {
                    k.Column("ItemId");
                    k.ForeignKey("FK_ItemOutputFiles_ItemId_AssetBuildItem_Id");
                });
            },
            r => r.ManyToMany(p =>
            {
                p.Column("OutputFileId");
                p.ForeignKey("FK_ItemOutputFiles_OutputFileId_OutputFile_Id");
            }));
            Property(x => x.LogFilename, y => y.NotNullable(true));
        }
    }

} // RSG.ToolsService.AssetPipeline.Data.Mappings namespace
