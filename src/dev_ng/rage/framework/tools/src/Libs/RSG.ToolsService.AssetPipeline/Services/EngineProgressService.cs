﻿//---------------------------------------------------------------------------------------------
// <copyright file="EngineProgressService.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.ToolsService.AssetPipeline.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using RSG.Pipeline.Services.Engine;
    using RSG.Services.Configuration;
    using RSG.Services.Persistence;

    /// <summary>
    /// 
    /// </summary>
    internal sealed class EngineProgressService : TransactionalService,
        IEngineProgressMonitoringService
    {
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="server"></param>
        public EngineProgressService(IServer server)
            : base(server)
        {
        }
        #endregion // Constructor(s)

        #region IEngineProgressMonitoringService Interface Methods
        /// <summary>
        /// Register for callback invocations.
        /// </summary>
        /// <param name="listenerConnection"></param>
        public Guid RegisterListener(String listenerConnection)
        {
            throw (new NotImplementedException());
        }

        /// <summary>
        /// Unregister for callback invocations.
        /// </summary>
        /// <param name="id"></param>
        public void UnregisterListener(Guid id)
        {
            throw (new NotImplementedException());
        }

        /// <summary>
        /// Determine if a the caller is registered for monitoring.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool IsRegistered(Guid id)
        {
            throw (new NotImplementedException());
        }
        #endregion // IEngineProgressMonitoringService Interface Methods
    }

} // RSG.ToolsService.AssetPipeline.Services namespace
