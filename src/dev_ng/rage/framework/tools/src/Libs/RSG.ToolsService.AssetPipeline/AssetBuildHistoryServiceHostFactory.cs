﻿//---------------------------------------------------------------------------------------------
// <copyright file="AssetBuildHistoryServiceHostFactory.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.ToolsService.AssetPipeline
{
    using System;
    using System.Reflection;
    using RSG.Configuration;
    using RSG.Services.Configuration;
    using RSG.Services.Configuration.AssetBuildPipeline;
    using RSG.Services.Persistence;
    using RSG.ToolsService.AssetPipeline.Data.Entities;

    /// <summary>
    /// 
    /// </summary>
    public class AssetBuildHistoryServiceHostFactory :
        SQLiteServiceHostFactory<IDatabaseServer>
    {
        #region Properties
        /// <summary>
        /// Gets a reference to the assembly that contains the nhibernate mappings.
        /// </summary>
        protected override Assembly MappingsAssembly
        {
            get { return typeof(AssetBuildItem).Assembly; }
        }

        /// <summary>
        /// Gets a reference to the assembly that contains the fluent migrations.
        /// </summary>
        protected override Assembly MigrationsAssembly
        {
            get { return typeof(AssetBuildItem).Assembly; }
        }

        /// <summary>
        /// Gets the namespace that the migration classes are in.
        /// </summary>
        protected override String MigrationsNamespace
        {
            get { return "RSG.ToolsService.AssetPipeline.Data.Migrations"; }
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="AssetBuildHistoryServiceHostFactory"/>
        /// class using the specified server configuration object.
        /// </summary>
        public AssetBuildHistoryServiceHostFactory(IDatabaseServer server)
            : base(server, false)
        {
        }
        #endregion // Constructor(s)
    }

} // RSG.ToolsService.AssetPipeline namespace