﻿//---------------------------------------------------------------------------------------------
// <copyright file="AssetPipelineService.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.ToolsService.AssetPipeline
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.Composition;
    using System.ServiceModel;
    using RSG.Base.Logging;
    using RSG.Base.Logging.Universal;
    using RSG.Configuration;
    using RSG.Services.Configuration;
    using RSG.Services.Configuration.AssetBuildPipeline;
    using RSG.ToolsService.Contracts;

    /// <summary>
    /// Asset Build Pipeline Tools Service.
    /// </summary>
    [Export(typeof(IToolsService))]
    internal class AssetPipelineService : IToolsService
    {
        #region Constants
        private const String SERVICE_NAME = "Rockstar Games Asset Pipeline Services";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Service name.
        /// </summary>
        public String ServiceName
        {
            get { return (SERVICE_NAME); }
        }
        #endregion // Properties
        
        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        private ICollection<ServiceHost> _serviceHosts;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public AssetPipelineService()
        {
            this._serviceHosts = new List<ServiceHost>();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Asset Pipeline Service: Startup handler.
        /// </summary>
        /// <param name="args"></param>
        public void OnStart(IUniversalLog log, IConfig config)
        {
            log.Debug("AssetPipelineService::OnStart()");

            AssetBuildHistoryServiceConfig conf = new AssetBuildHistoryServiceConfig(config);
            AssetBuildHistoryServiceHostFactory hostFactory = new AssetBuildHistoryServiceHostFactory(conf.DefaultServer);
            try
            {
                ServiceHost host = hostFactory.CreateServiceHost(typeof(Services.AssetBuildHistoryService));
                _serviceHosts.Add(host);

                host.Open();

                foreach (Uri baseAddress in host.BaseAddresses)
                    log.Message("Engine Progress Service hosted at {0}.", baseAddress.AbsoluteUri);
                log.Message("WCF Host Startup.");
            }
            catch (AddressAlreadyInUseException ex)
            {
                log.ToolExceptionCtx(this.ServiceName, ex, "Exception starting service.");
            }
        }

        /// <summary>
        /// ASset Pipeline Service: Shutdown handler.
        /// </summary>
        public void OnStop(IUniversalLog log)
        {
            log.Debug("AssetPipelineService::OnStop()");
            using (new ProfileContext(log, "Shutting down WCF Services"))
            {
                foreach (ServiceHost host in _serviceHosts)
                {
                    host.Close();
                }
                _serviceHosts.Clear();
            }
        }
        #endregion // Controller Methods
    }

} // RSG.ToolsService.AssetPipeline namespace
