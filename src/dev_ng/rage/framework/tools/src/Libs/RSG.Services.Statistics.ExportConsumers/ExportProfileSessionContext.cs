﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.Logging;
using RSG.Configuration;
using RSG.Interop.Incredibuild;
using RSG.Services.Configuration;
using RSG.Services.Statistics.Consumers;
using RSG.Services.Statistics.Contracts.DataContracts;

namespace RSG.Services.Statistics.ExportConsumers
{
    /// <summary>
    /// Custom profile session context for export based profiling sessions.
    /// </summary>
    public class ExportProfileSessionContext : ProfileSessionContext<ExportProfileSession>
    {
        #region Fields
        /// <summary>
        /// Reference to the current process.
        /// </summary>
        private readonly Process _currentProcess;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ExportProfileSessionContext"/>
        /// class using the specified parameters.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="statisticsServer"></param>
        /// <param name="name"></param>
        /// <param name="applicationSessionGuid"></param>
        /// <param name="parentProfileSessionGuid"></param>
        /// <param name="exportedFiles"></param>
        /// <param name="platforms"></param>
        /// <param name="toolsVersion"></param>
        public ExportProfileSessionContext(
            ILog log,
            IServer statisticsServer,
            string name,
            Guid applicationSessionGuid,
            string[] exportedFiles,
            RSG.Platform.Platform[] platforms,
            IVersion toolsVersion)
            : this(log, statisticsServer, name, applicationSessionGuid, null, exportedFiles, platforms, toolsVersion)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ExportProfileSessionContext"/>
        /// class using the specified parameters.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="statisticsServer"></param>
        /// <param name="name"></param>
        /// <param name="applicationSessionGuid"></param>
        /// <param name="parentProfileSessionGuid"></param>
        /// <param name="exportedFiles"></param>
        /// <param name="platforms"></param>
        /// <param name="toolsVersion"></param>
        public ExportProfileSessionContext(
            ILog log,
            IServer statisticsServer,
            string name,
            Guid applicationSessionGuid,
            Guid? parentProfileSessionGuid,
            string[] exportedFiles,
            RSG.Platform.Platform[] platforms,
            IVersion toolsVersion)
            : base(log, statisticsServer, name, applicationSessionGuid, parentProfileSessionGuid)
        {
            ProfileSession.ExportedFiles = exportedFiles;
            ProfileSession.Platforms = platforms;
            ProfileSession.IncredibuildEnabled = Incredibuild.IsAgentEnabled;
            ProfileSession.IncredibuildStandalone = Incredibuild.IsAgentStandalone;
            ProfileSession.IncredibuildForcedCPUCount = Incredibuild.AgentForceCPUCount;
            ProfileSession.ToolsVersion = String.Format("{0}.{1}", toolsVersion.Major, toolsVersion.Minor);

            _currentProcess = Process.GetCurrentProcess();
            ProfileSession.PrivateBytesStart = _currentProcess.PrivateMemorySize64;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Sets the flag indicating whether the export successfully completed.
        /// </summary>
        public bool ExportSuccessful
        {
            set { ProfileSession.Success = value; }
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Protected implementation of Dispose pattern.
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (Disposed)
            {
                return;
            }

            _currentProcess.Refresh();
            ProfileSession.PrivateBytesEnd = _currentProcess.PrivateMemorySize64;

            base.Dispose(disposing);
        }
        #endregion
    }
}
