﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.Model.Common.Animation;
using RSG.Model.Common.Mission;
using RSG.Model.Common.Weapon;
using RSG.Model.Statistics.Platform;
using RSG.Platform;
using RSG.ROS;
using RSG.Statistics.Common;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Dto.Enums;
using RSG.Statistics.Common.ServiceContract;
using RSG.Services.Common.Clients;
using RSG.Base.Configuration.Services;

namespace RSG.Statistics.Client.ServiceClients
{
    /// <summary>
    /// 
    /// </summary>
    public class EnumClient : ConfigAwareClient<IEnumService>, IEnumService
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public EnumClient()
            : base(new StatisticsConfig().DefaultServer)
        {
        }
        
        /// <summary>
        /// 
        /// </summary>
        public EnumClient(IServiceHostConfig config)
            : base(config)
        {
        }
        #endregion // Constructor(s)

        #region IEnumService Implementation
        /// <summary>
        /// Returns a list of all build configs.
        /// </summary>
        public IList<EnumDto<BuildConfig>> GetBuildConfigs()
        {
            return ExecuteCommand(() => base.Channel.GetBuildConfigs());
        }

        /// <summary>
        /// Returns a list of all clip dictionary categories.
        /// </summary>
        public IList<EnumDto<ClipDictionaryCategory>> GetClipDictionaryCategories()
        {
            return ExecuteCommand(() => base.Channel.GetClipDictionaryCategories());
        }

        /// <summary>
        /// Returns a list of all date time group modes.
        /// </summary>
        public IList<EnumDto<DateTimeGroupMode>> GetDateTimeGroupModes()
        {
            return ExecuteCommand(() => base.Channel.GetDateTimeGroupModes());
        }

        /// <summary>
        /// Returns a list of all deathmatch sub types.
        /// </summary>
        public IList<EnumDto<DeathmatchSubType>> GetDeathmatchSubTypes()
        {
            return ExecuteCommand(() => base.Channel.GetDeathmatchSubTypes());
        }

        /// <summary>
        /// Returns a list of all file types.
        /// </summary>
        public IList<EnumDto<FileType>> GetFileTypes()
        {
            return ExecuteCommand(() => base.Channel.GetFileTypes());
        }

        /// <summary>
        /// Returns a list of all freemode mission categories.
        /// </summary>
        public IList<EnumDto<FreemodeMissionCategory>> GetFreemodeMissionCategories()
        {
            return ExecuteCommand(() => base.Channel.GetFreemodeMissionCategories());
        }

        /// <summary>
        /// Returns a list of all build configs.
        /// </summary>
        public IList<EnumDto<GameType>> GetGameTypes()
        {
            return ExecuteCommand(() => base.Channel.GetGameTypes());
        }

        /// <summary>
        /// Returns a list of all job results.
        /// </summary>
        public IList<EnumDto<JobResult>> GetJobResults()
        {
            return ExecuteCommand(() => base.Channel.GetJobResults());
        }

        /// <summary>
        /// Returns a list of all match types.
        /// </summary>
        public IList<EnumDto<MatchType>> GetMatchTypes()
        {
            return ExecuteCommand(() => base.Channel.GetMatchTypes());
        }

        /// <summary>
        /// Returns a list of all mission attempt results.
        /// </summary>
        public IList<EnumDto<MissionAttemptResult>> GetMissionAttemptResults()
        {
            return ExecuteCommand(() => base.Channel.GetMissionAttemptResults());
        }

        /// <summary>
        /// Returns a list of all mission categories.
        /// </summary>
        public IList<EnumDto<MissionCategory>> GetMissionCategories()
        {
            return ExecuteCommand(() => base.Channel.GetMissionCategories());
        }

        /// <summary>
        /// Returns a list of all mission sub types.
        /// </summary>
        public IList<EnumDto<MissionSubType>> GetMissionSubTypes()
        {
            return ExecuteCommand(() => base.Channel.GetMissionSubTypes());
        }

        /// <summary>
        /// Returns a list of all build configs.
        /// </summary>
        public IList<EnumDto<RSG.Platform.Platform>> GetPlatforms()
        {
            return ExecuteCommand(() => base.Channel.GetPlatforms());
        }

        /// <summary>
        /// Returns a list of all race sub types.
        /// </summary>
        public IList<EnumDto<RaceSubType>> GetRaceSubTypes()
        {
            return ExecuteCommand(() => base.Channel.GetRaceSubTypes());
        }

        /// <summary>
        /// Returns a list of all resource bucket types.
        /// </summary>
        public IList<EnumDto<ResourceBucketType>> GetResourceBucketTypes()
        {
            return ExecuteCommand(() => base.Channel.GetResourceBucketTypes());
        }

        /// <summary>
        /// Returns a list of all ROS platforms.
        /// </summary>
        public IList<EnumDto<ROSPlatform>> GetROSPlatforms()
        {
            return ExecuteCommand(() => base.Channel.GetROSPlatforms());
        }

        /// <summary>
        /// Returns a list of all shop types.
        /// </summary>
        public IList<EnumDto<ShopType>> GetShopTypes()
        {
            return ExecuteCommand(() => base.Channel.GetShopTypes());
        }

        /// <summary>
        /// Returns a list of all spend categories.
        /// </summary>
        public IList<EnumDto<SpendCategory>> GetSpendCategories()
        {
            return ExecuteCommand(() => base.Channel.GetSpendCategories());
        }

        /// <summary>
        /// Returns a list of all times of the day.
        /// </summary>
        public IList<EnumDto<TimeOfDay>> GetTimeOfDays()
        {
            return ExecuteCommand(() => base.Channel.GetTimeOfDays());
        }

        /// <summary>
        /// Returns a list of all vehicle categories.
        /// </summary>
        public IList<EnumDto<VehicleCategory>> GetVehicleCategories()
        {
            return ExecuteCommand(() => base.Channel.GetVehicleCategories());
        }

        /// <summary>
        /// Returns a list of all weapon categories.
        /// </summary>
        public IList<EnumDto<WeaponCategory>> GetWeaponCategories()
        {
            return ExecuteCommand(() => base.Channel.GetWeaponCategories());
        }

        /// <summary>
        /// Returns a list of all weather types.
        /// </summary>
        public IList<EnumDto<WeatherType>> GetWeatherTypes()
        {
            return ExecuteCommand(() => base.Channel.GetWeatherTypes());
        }
        #endregion // IEnumService Implementation
    } // EnumClient
}
