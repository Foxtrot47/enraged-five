﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common.ServiceContract;
using RSG.Statistics.Common.Model.Playthrough;
using RSG.Statistics.Common.Config;
using RSG.Base.Configuration.Services;
using RSG.Services.Common.Clients;

namespace RSG.Statistics.Client.ServiceClients
{
    /// <summary>
    /// 
    /// </summary>
    public class PlaythroughClient : ConfigAwareClient<IPlaythroughService>, IPlaythroughService
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public PlaythroughClient(IServiceHostConfig config)
            : base(config)
        {
        }
        #endregion // Constructor(s)

        #region IPlaythroughService Implementation
        /// <summary>
        /// Retrieves list of all users that have performed a playthrough session.
        /// </summary>
        /// <returns></returns>
        public List<PlaythroughUser> GetUsers()
        {
            return ExecuteCommand(() => base.Channel.GetUsers());
        }

        /// <summary>
        /// Retrieves list of all builds that have been used for playthroughs.
        /// </summary>
        /// <returns></returns>
        public List<String> GetBuilds()
        {
            return ExecuteCommand(() => base.Channel.GetBuilds());
        }

        /// <summary>
        /// Retrieves list of all platforms that playthroughs have occurred on.
        /// </summary>
        /// <returns></returns>
        public List<RSG.Platform.Platform> GetPlatforms()
        {
            return ExecuteCommand(() => base.Channel.GetPlatforms());
        }

        /// <summary>
        /// Returns the list of playthrough session the database contains.
        /// </summary>
        /// <returns></returns>
        public List<SessionSubmission> GetSessions()
        {
            return ExecuteCommand(() => base.Channel.GetSessions());
        }

        /// <summary>
        /// Returns the list of mission attempts associated with a particular session.
        /// </summary>
        /// <returns></returns>
        public List<MissionAttemptSubmission> GetMissionAttempts(String sessionId)
        {
            return ExecuteCommand(() => base.Channel.GetMissionAttempts(sessionId));
        }

        /// <summary>
        /// Returns the list of checkpoint attempts associated with a particular mission attempt.
        /// </summary>
        /// <returns></returns>
        public List<CheckpointAttemptSubmission> GetCheckpointAttempts(String sessionId, String missionAttemptId)
        {
            return ExecuteCommand(() => base.Channel.GetCheckpointAttempts(sessionId, missionAttemptId));
        }

        /// <summary>
        /// Creates a new playthrough session.
        /// </summary>
        /// <returns></returns>
        public SessionSubmission CreateSession(SessionSubmission session)
        {
            return ExecuteCommand(() => base.Channel.CreateSession(session));
        }

        /// <summary>
        /// Update an existing playthrough session.
        /// </summary>
        /// <returns></returns>
        public void UpdateSession(String id, SessionSubmission session)
        {
            ExecuteCommand(() => base.Channel.UpdateSession(id, session));
        }

        /// <summary>
        /// Add mission attempt information to a playthrough session.
        /// </summary>
        /// <returns></returns>
        public MissionAttemptSubmission AddMissionAttempt(String id, MissionAttemptSubmission attempt)
        {
            return ExecuteCommand(() => base.Channel.AddMissionAttempt(id, attempt));
        }

        /// <summary>
        /// Update an existing mission attempt.
        /// </summary>
        public void UpdateMissionAttempt(String sessionId, String attemptId, MissionAttemptSubmission attempt)
        {
            ExecuteCommand(() => base.Channel.UpdateMissionAttempt(sessionId, attemptId, attempt));
        }

        /// <summary>
        /// Add mission checkpoint attempt information to a playthrough session/mission attempt.
        /// </summary>
        public CheckpointAttemptSubmission AddCheckpointAttempt(String sessionId, String attemptId, CheckpointAttemptSubmission attempt)
        {
            return ExecuteCommand(() => base.Channel.AddCheckpointAttempt(sessionId, attemptId, attempt));
        }
        
        /// <summary>
        /// Update mission checkpoint attempt information.
        /// </summary>
        /// <returns></returns>
        public void UpdateCheckpointAttempt(String sessionId, String missionAttemptId, String checkpointAttemptId, CheckpointAttemptSubmission attempt)
        {
            ExecuteCommand(() => base.Channel.UpdateCheckpointAttempt(sessionId, missionAttemptId, checkpointAttemptId, attempt));
        }

        /// <summary>
        /// Adds a generic comment to a particular playthrough session.
        /// </summary>
        public void AddGenericComment(String sessionId, CommentSubmission comment)
        {
            ExecuteCommand(() => base.Channel.AddGenericComment(sessionId, comment));
        }
        #endregion // IPlaythroughService Implementation
    } // PlaythroughClient
}
