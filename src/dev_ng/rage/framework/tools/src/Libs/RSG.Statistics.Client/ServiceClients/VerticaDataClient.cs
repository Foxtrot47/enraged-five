﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.ROS;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Model.VerticaData;
using RSG.Statistics.Common.ServiceContract;
using RSG.Services.Common.Clients;
using RSG.Base.Configuration.Services;

namespace RSG.Statistics.Client.ServiceClients
{
    /// <summary>
    /// 
    /// </summary>
    public class VerticaDataClient : ConfigAwareClient<IVerticaDataService>, IVerticaDataService
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public VerticaDataClient(IServiceHostConfig config)
            : base(config)
        {
        }
        #endregion // Constructor(s)

        #region Clans
        /// <summary>
        /// Searches for a clan based on a partial name.
        /// </summary>
        public List<Clan> SearchForClan(String searchName)
        {
            return ExecuteCommand(() => base.Channel.SearchForClan(searchName));
        }

        /// <summary>
        /// Returns the list of gamers that are members of a particular clan.
        /// </summary>
        public List<Gamer> GetClanMembers(String id)
        {
            return ExecuteCommand(() => base.Channel.GetClanMembers(id));
        }
        #endregion // Clans

        #region Gamers
        /// <summary>
        /// Updates the list of websites.
        /// </summary>
        public List<Gamer> SearchForGamer(String searchName, String platform)
        {
            return ExecuteCommand(() => base.Channel.SearchForGamer(searchName, platform));
        }

        /// <summary>
        /// Validates a list of gamers returning only the valid ones.
        /// </summary>
        public List<Gamer> ValidateGamerList(List<Gamer> gamers)
        {
            return ExecuteCommand(() => base.Channel.ValidateGamerList(gamers));
        }

        /// <summary>
        /// Retrieves the list of all gamer groups.
        /// </summary>
        public List<GamerGroup> GetGamerGroups()
        {
            return ExecuteCommand(() => base.Channel.GetGamerGroups());
        }

        /// <summary>
        /// Creates a new gamer group.
        /// </summary>
        public List<String> CreateGamerGroup(GamerGroup group)
        {
            return ExecuteCommand(() => base.Channel.CreateGamerGroup(group));
        }

        /// <summary>
        /// Updates an existing gamer group.
        /// </summary>
        public List<String> UpdateGamerGroup(GamerGroup group, String name)
        {
            return ExecuteCommand(() => base.Channel.UpdateGamerGroup(group, name));
        }

        /// <summary>
        /// Deletes an existing gamer group.
        /// </summary>
        public void DeleteGamerGroup(String name)
        {
            ExecuteCommand(() => base.Channel.DeleteGamerGroup(name));
        }
        #endregion // Gamers

        #region Missions
        /// <summary>
        /// Searches for a UGC mission user with the particular name.
        /// </summary>
        public List<UGCMission> SearchForMission(String searchName, String matchType, String missionCategory, String published)
        {
            return ExecuteCommand(() => base.Channel.SearchForMission(searchName, matchType, missionCategory, published));
        }
        #endregion // Missions

        #region Users
        /// <summary>
        /// Updates the list of websites.
        /// </summary>
        public List<String> SearchForUser(String searchName)
        {
            return ExecuteCommand(() => base.Channel.SearchForUser(searchName));
        }

        /// <summary>
        /// Validates a list of users returning only the valid ones.
        /// </summary>
        public List<String> ValidateUserList(List<String> users)
        {
            return ExecuteCommand(() => base.Channel.ValidateUserList(users));
        }

        /// <summary>
        /// Retrieves the list of all user groups.
        /// </summary>
        public List<UserGroup> GetUserGroups()
        {
            return ExecuteCommand(() => base.Channel.GetUserGroups());
        }

        /// <summary>
        /// Creates a new user group.
        /// </summary>
        public List<String> CreateUserGroup(UserGroup group)
        {
            return ExecuteCommand(() => base.Channel.CreateUserGroup(group));
        }

        /// <summary>
        /// Updates an existing user group.
        /// </summary>
        public List<String> UpdateUserGroup(UserGroup group, String name)
        {
            return ExecuteCommand(() => base.Channel.UpdateUserGroup(group, name));
        }

        /// <summary>
        /// Deletes an existing user group.
        /// </summary>
        public void DeleteUserGroup(String name)
        {
            ExecuteCommand(() => base.Channel.DeleteUserGroup(name));
        }
        #endregion // Users
    } // VerticaDataClient
}
