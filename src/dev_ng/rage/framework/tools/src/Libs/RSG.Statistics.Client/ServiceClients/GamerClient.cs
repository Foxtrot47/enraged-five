﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common.ServiceContract;
using System.ServiceModel;
using RSG.Statistics.Common.Dto.Telemetry;
using RSG.Statistics.Common.Dto.Telemetry.Metadata;
using RSG.Statistics.Common.Config;

namespace RSG.Statistics.Client.ServiceClients
{
    /// <summary>
    /// 
    /// </summary>
    public class GamerClient : ClientBaseEx<IGamerService>, IGamerService
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public GamerClient(IServer server)
            : base(server)
        {
        }
        #endregion // Constructor(s)

        #region IGamerService Implementation
        /// <summary>
        /// 
        /// </summary>
        /// <param name="platform"></param>
        /// <returns></returns>
        public GamerDtos GetAllForPlatform(string platform)
        {
            return ExecuteCommand(() => base.Channel.GetAllForPlatform(platform));
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="platform"></param>
        /// <param name="gamerTag"></param>
        /// <returns></returns>
        public GamerDto GetByGamerTag(string platform, string gamerTag)
        {
            return ExecuteCommand(() => base.Channel.GetByGamerTag(platform, gamerTag));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="platform"></param>
        /// <param name="gamerTag"></param>
        /// <returns></returns>
        public GamerDto PutGamer(string platform, string gamerTag)
        {
            return ExecuteCommand(() => base.Channel.PutGamer(platform, gamerTag));
        }
        #endregion // IGamerService Implementation
    } // GamerClient
}
