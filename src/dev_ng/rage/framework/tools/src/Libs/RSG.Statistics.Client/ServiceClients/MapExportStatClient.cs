﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common.ServiceContract;
using RSG.Statistics.Common.Dto.ExportStats;
using System.ServiceModel;
using System.ServiceModel.Channels;
using RSG.Statistics.Common.Config;
using RSG.Services.Common.Clients;
using RSG.Base.Configuration.Services;

namespace RSG.Statistics.Client.ServiceClients
{
    /// <summary>
    /// 
    /// </summary>
    public class MapExportStatClient : ConfigAwareClient<IMapExportStatService>, IMapExportStatService
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public MapExportStatClient(IServiceHostConfig config)
            : base(config)
        {
        }
    
        /// <summary>
        /// 
        /// </summary>
        /// <param name="displayMessage"></param>
        public MapExportStatClient(Binding binding, EndpointAddress remoteAddress)
            : base(binding, remoteAddress)
        {
        }
        #endregion // Constructor(s)

        #region IMapExportStatService Implementation
        /// <summary>
        /// Creates a new map network export stat from the specified database object.
        /// </summary>
        /// <param name="dto">
        /// The database object that should be used to 
        /// </param>
        public void CreateMapNetworkExportStat(MapNetworkExportStatDto dto)
        {
            ExecuteCommand(() => base.Channel.CreateMapNetworkExportStat(dto));
        }

        /// <summary>
        /// Creates a new map export stat.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public MapExportStatDto CreateMapExportStat(MapExportStatDto dto)
        {
            return ExecuteCommand(() => base.Channel.CreateMapExportStat(dto));
        }

        /// <summary>
        /// Creates a new map export stat.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public MapExportStatDto UpdateMapExportStat(string statId, MapExportStatDto dto)
        {
            return ExecuteCommand(() => base.Channel.UpdateMapExportStat(statId, dto));
        }

        /// <summary>
        /// Creates a new map check stat.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public MapCheckStatDto CreateMapCheckStat(String exportStatId, MapCheckStatDto dto)
        {
            return ExecuteCommand(() => base.Channel.CreateMapCheckStat(exportStatId, dto));
        }

        /// <summary>
        /// Creates a new map export stat.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public MapCheckStatDto UpdateMapCheckStat(String exportStatId, String mapCheckStatId, MapCheckStatDto dto)
        {
            return ExecuteCommand(() => base.Channel.UpdateMapCheckStat(exportStatId, mapCheckStatId, dto));
        }

        /// <summary>
        /// Creates a new map check stat.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public SectionExportStatDto CreateSectionExportStat(String exportStatId, SectionExportStatDto dto)
        {
            return ExecuteCommand(() => base.Channel.CreateSectionExportStat(exportStatId, dto));
        }

        /// <summary>
        /// Creates a new section export stat.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public SectionExportStatDto UpdateSectionExportStat(String exportStatId, String sectionExportStatId, SectionExportStatDto dto)
        {
            return ExecuteCommand(() => base.Channel.UpdateSectionExportStat(exportStatId, sectionExportStatId, dto));
        }

        /// <summary>
        /// Creates a new section export sub stat.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public SectionExportSubStatDto CreateSectionExportSubStat(String exportStatId, String sectionExportStatId, SectionExportSubStatDto dto)
        {
            return ExecuteCommand(() => base.Channel.CreateSectionExportSubStat(exportStatId, sectionExportStatId, dto));
        }

        /// <summary>
        /// Creates a new section export stat.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public SectionExportSubStatDto UpdateSectionExportSubStat(String exportStatId, String sectionExportStatId, String subStatId, SectionExportSubStatDto dto)
        {
            return ExecuteCommand(() => base.Channel.UpdateSectionExportSubStat(exportStatId, sectionExportStatId, subStatId, dto));
        }

        /// <summary>
        /// Creates a new image build stat.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public ImageBuildStatDto CreateImageBuildStat(String exportStatId, ImageBuildStatDto dto)
        {
            return ExecuteCommand(() => base.Channel.CreateImageBuildStat(exportStatId, dto));
        }

        /// <summary>
        /// Updates an image build stat.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public ImageBuildStatDto UpdateImageBuildStat(String exportStatId, String imageBuildStatId, ImageBuildStatDto dto)
        {
            return ExecuteCommand(() => base.Channel.UpdateImageBuildStat(exportStatId, imageBuildStatId, dto));
        }

        /// <summary>
        /// Retrieves a list of users that have ever exported data.
        /// </summary>
        /// <returns></returns>
        public ExportUserDtos GetAllUsers()
        {
            return ExecuteCommand(() => base.Channel.GetAllUsers());
        }

        /// <summary>
        /// Scrapes the database generating a csv containing comparative export times for all sections/users.
        /// </summary>
        /// <returns></returns>
        public ComparativeExportStatDtos GetComparitiveExportStats()
        {
            return ExecuteCommand(() => base.Channel.GetComparitiveExportStats());
        }

        /// <summary>
        /// Scrapes the database generating a csv containing comparative weekly export times for all sections.
        /// </summary>
        /// <returns></returns>
        public WeeklyExportStatDtos GetWeeklyExportStats()
        {
            return ExecuteCommand(() => base.Channel.GetWeeklyExportStats());
        }
        #endregion // IMapExportStatService Implementation
    } // MapExportStatClient
}
