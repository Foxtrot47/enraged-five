﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using RSG.Statistics.Common.ServiceContract;
using RSG.Statistics.Common.Dto.GameAssets;
using RSG.Statistics.Common.Dto.GameAssets.Bundles;
using RSG.Statistics.Common.Dto.GameAssetStats.Bundles;
using RSG.Statistics.Common.Dto.GameAssetStats;
using RSG.Model.Common.Animation;
using RSG.Statistics.Common.Config;
using RSG.Services.Common.Clients;
using RSG.Base.Configuration.Services;

namespace RSG.Statistics.Client.ServiceClients
{
    /// <summary>
    /// 
    /// </summary>
    public class BuildClient : ConfigAwareClient<IBuildService>, IBuildService
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public BuildClient()
            : base(new StatisticsConfig().DefaultServer)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public BuildClient(IServiceHostConfig config)
            : base(config)
        {
        }
        #endregion // Constructor(s)

        #region Animations
        /// <summary>
        /// Creates per build stats for clip dictionaries.
        /// </summary>
        public void CreateClipDictionaryStats(String buildIdentifier, IClipDictionaryCollection clipDictionaries)
        {
            ExecuteCommand(() => base.Channel.CreateClipDictionaryStats(buildIdentifier, clipDictionaries));
        }
        #endregion // Animations

        #region Builds
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public BuildDtos GetAll()
        {
            return ExecuteCommand(() => base.Channel.GetAll());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="identifier"></param>
        /// <returns></returns>
        public BuildDto GetByIdentifier(string identifier)
        {
            return ExecuteCommand(() => base.Channel.GetByIdentifier(identifier));
        }

        /// <summary>
        /// Updates or creates a list of game assets.
        /// </summary>
        /// <param name="assetId"></param>
        /// <param name="asset"></param>
        /// <returns></returns>
        public void PutAssets(BuildDtos dtos)
        {
            ExecuteCommand(() => base.Channel.PutAssets(dtos));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="identifier"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        public BuildDto PutAsset(string identifier, BuildDto dto)
        {
            return ExecuteCommand(() => base.Channel.PutAsset(identifier, dto));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="identifier"></param>
        public void DeleteAsset(string identifier)
        {
            ExecuteCommand(() => base.Channel.DeleteAsset(identifier));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public BuildDto GetLatest()
        {
            return ExecuteCommand(() => base.Channel.GetLatest());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        public BuildDtos GetLatestBuilds(uint count)
        {
            return ExecuteCommand(() => base.Channel.GetLatestBuilds(count));
        }

        /// <summary>
        /// 
        /// </summary>
        public void UpdateBuildFeatures()
        {
            ExecuteCommand(() => base.Channel.UpdateBuildFeatures());
        }
        #endregion // Builds

        #region Cutscenes
        /// <summary>
        /// Creates per build stats for cutscenes.
        /// </summary>
        public void CreateCutsceneStats(String buildIdentifier, ICutsceneCollection cutscenes)
        {
            ExecuteCommand(() => base.Channel.CreateCutsceneStats(buildIdentifier, cutscenes));
        }
        
        /// <summary>
        /// Gets the list of cutscenes that are associated with the specified build.
        /// </summary>
        public ICutsceneCollection GetCutsceneStats(String buildIdentifier)
        {
            return ExecuteCommand(() => base.Channel.GetCutsceneStats(buildIdentifier));
        }
        #endregion // Cutscenes

        #region Levels
        /// <summary>
        /// 
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <returns></returns>
        public LevelDtos GetAllLevelsForBuild(string buildIdentifier)
        {
            return ExecuteCommand(() => base.Channel.GetAllLevelsForBuild(buildIdentifier));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <returns></returns>
        public LevelDto GetLevelForBuild(string buildIdentifier, string levelHash)
        {
            return ExecuteCommand(() => base.Channel.GetLevelForBuild(buildIdentifier, levelHash));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="levelStatBundle"></param>
        /// <returns></returns>
        public void CreateLevelStat(string buildIdentifier, string levelHash, LevelStatBundleDto levelStatBundle)
        {
            ExecuteCommand(() => base.Channel.CreateLevelStat(buildIdentifier, levelHash, levelStatBundle));
        }

        /// <summary>
        /// Retrieves a specific sub set of stats for a particular set of levels.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="fetchDto"></param>
        /// <returns></returns>
        public List<LevelStatDto> GetMultipleLevelStats(String buildIdentifier, StreamableStatFetchDto fetchDto)
        {
            return ExecuteCommand(() => base.Channel.GetMultipleLevelStats(buildIdentifier, fetchDto));
        }
        #endregion // Levels

        #region Map Hierarchy
        /// <summary>
        /// 
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <returns></returns>
        public MapHierarchyBundleDto GetMapHierarchy(string buildIdentifier, string levelHash)
        {
            return ExecuteCommand(() => base.Channel.GetMapHierarchy(buildIdentifier, levelHash));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="hierarchyBundle"></param>
        public void CreateMapHierarchy(string buildIdentifier, string levelHash, MapHierarchyBundleDto hierarchyBundle)
        {
            ExecuteCommand(() => base.Channel.CreateMapHierarchy(buildIdentifier, levelHash, hierarchyBundle));
        }

        /// <summary>
        /// Creates a new map area stat for the specified build and map area.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="mapAreaHash"></param>
        /// <param name="mapAreaStatBundle"></param>
        public void UpdateMapAreaStat(string buildIdentifier, string levelHash, string mapAreaHash, MapAreaStatBundleDto mapAreaStatBundle)
        {
            ExecuteCommand(() => base.Channel.UpdateMapAreaStat(buildIdentifier, levelHash, mapAreaHash, mapAreaStatBundle));
        }

        /// <summary>
        /// Creates a new map section stat for the specified build and map section.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="mapAreaHash"></param>
        /// <param name="mapAreaStatBundle"></param>
        public void UpdateMapSectionStat(string buildIdentifier, string levelHash, string mapSectionHash, MapSectionStatBundleDto mapSectionStatBundle)
        {
            ExecuteCommand(() => base.Channel.UpdateMapSectionStat(buildIdentifier, levelHash, mapSectionHash, mapSectionStatBundle));
        }

        /// <summary>
        /// Creates a new map section stat for the specified build, level and map section.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="mapAreaHash"></param>
        /// <param name="mapAreaStatBundle"></param>
        public void CreateArchetypeStats(String buildIdentifier, String levelHash, String mapSectionHash, ArchetypeStatBundles archetypeStatBundles)
        {
            ExecuteCommand(() => base.Channel.CreateArchetypeStats(buildIdentifier, levelHash, mapSectionHash, archetypeStatBundles));
        }

        /// <summary>
        /// Creates entity stats for a particular map section.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="mapAreaHash"></param>
        /// <param name="mapAreaStatBundle"></param>
        public void CreateEntityStats(String buildIdentifier, String levelHash, String mapSectionHash, EntityStatBundles entityStatBundles)
        {
            ExecuteCommand(() => base.Channel.CreateEntityStats(buildIdentifier, levelHash, mapSectionHash, entityStatBundles));
        }

        /// <summary>
        /// Creates a new car gen stat for the specified build, level and map section.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="mapSectionHash"></param>
        /// <param name="dtos"></param>
        public void CreateCarGenStats(String buildIdentifier, String levelHash, String mapSectionHash, CarGenStatDtos dtos)
        {
            ExecuteCommand(() => base.Channel.CreateCarGenStats(buildIdentifier, levelHash, mapSectionHash, dtos));
        }
        
        /// <summary>
        /// Creates a new spawn point stat for the specified build and level.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="dtos"></param>
        public void CreateSpawnPointStats(String buildIdentifier, String levelHash, SpawnPointStatDtos dtos)
        {
            ExecuteCommand(() => base.Channel.CreateSpawnPointStats(buildIdentifier, levelHash, dtos));
        }

        /// <summary>
        /// Retrieves a specific sub set of stats for a particular set map section.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="fetchDto"></param>
        /// <returns></returns>
        public List<MapSectionStatDto> GetMultipleMapSectionStats(String buildIdentifier, String levelHash, StreamableStatFetchDto fetchDto)
        {
            return ExecuteCommand(() => base.Channel.GetMultipleMapSectionStats(buildIdentifier, levelHash, fetchDto));
        }
        
        /// <summary>
        /// Retrieves a specific sub set of stats for a particular set of archetypes.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="fetchDto"></param>
        /// <returns></returns>
        public List<ArchetypeStatDto> GetMultipleArchetypeStats(String buildIdentifier, String levelHash, StreamableStatFetchDto fetchDto)
        {
            return ExecuteCommand(() => base.Channel.GetMultipleArchetypeStats(buildIdentifier, levelHash, fetchDto));
        }

        /// <summary>
        /// Retrieves a specific sub set of stats for a particular set map section.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="fetchDto"></param>
        /// <returns></returns>
        public List<EntityStatDto> GetMultipleEntityStats(String buildIdentifier, String levelHash, StreamableStatFetchDto fetchDto)
        {
            return ExecuteCommand(() => base.Channel.GetMultipleEntityStats(buildIdentifier, levelHash, fetchDto));
        }

        /// <summary>
        /// Retrieves a specific sub set of stats for a particular set of room.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="fetchDto"></param>
        /// <returns></returns>
        public List<RoomStatDto> GetMultipleRoomStats(String buildIdentifier, String levelHash, StreamableStatFetchDto fetchDto)
        {
            return ExecuteCommand(() => base.Channel.GetMultipleRoomStats(buildIdentifier, levelHash, fetchDto));
        }
        #endregion // Map Hierarchy

        #region Characters
        /// <summary>
        /// Gets a list of characters that are associated with the specified build.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <returns></returns>
        public CharacterDtos GetAllCharactersForBuild(string buildIdentifier)
        {
            return ExecuteCommand(() => base.Channel.GetAllCharactersForBuild(buildIdentifier));
        }

        /// <summary>
        /// Gets an individual character for a particular build based on the character's hash.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="characterHash">Hash of the character's name.</param>
        /// <returns></returns>
        public CharacterDto GetCharacterForBuild(string buildIdentifier, string characterHash)
        {
            return ExecuteCommand(() => base.Channel.GetCharacterForBuild(buildIdentifier, characterHash));
        }

        /// <summary>
        /// Gets an individual character statistic for a particular build and character.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="characterHash">Hash of the character's name.</param>
        /// <returns></returns>
        public CharacterStatBundleDto GetCharacterStat(string buildIdentifier, string characterHash)
        {
            CharacterStatBundleDto bundle = ExecuteCommand(() => base.Channel.GetCharacterStat(buildIdentifier, characterHash));
            bundle.ResolveReferences();
            return bundle;
        }

        /// <summary>
        /// Creates a new character stat for the specified build and character.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="characterHash">Hash of the character's name.</param>
        /// <param name="characterStatBundle">Data transfer object containing all the information relating to a character stat.</param>
        /// <returns></returns>
        public void CreateCharacterStat(string buildIdentifier, string characterHash, CharacterStatBundleDto characterStatBundle)
        {
            ExecuteCommand(() => base.Channel.CreateCharacterStat(buildIdentifier, characterHash, characterStatBundle));
        }
        #endregion // Characters

        #region Vehicles
        /// <summary>
        /// 
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <returns></returns>
        public VehicleDtos GetAllVehiclesForBuild(string buildIdentifier)
        {
            return ExecuteCommand(() => base.Channel.GetAllVehiclesForBuild(buildIdentifier));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="vehicleHash"></param>
        /// <returns></returns>
        public VehicleDto GetVehicleForBuild(string buildIdentifier, string vehicleHash)
        {
            return ExecuteCommand(() => base.Channel.GetVehicleForBuild(buildIdentifier, vehicleHash));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="vehicleHash"></param>
        /// <returns></returns>
        public VehicleStatBundleDto GetVehicleStat(string buildIdentifier, string vehicleHash)
        {
            VehicleStatBundleDto bundle = ExecuteCommand(() => base.Channel.GetVehicleStat(buildIdentifier, vehicleHash));
            bundle.ResolveReferences();
            return bundle;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="vehicleHash"></param>
        /// <returns></returns>
        public void CreateVehicleStat(string buildIdentifier, string vehicleHash, VehicleStatBundleDto vehicleStatBundle)
        {
            ExecuteCommand(() => base.Channel.CreateVehicleStat(buildIdentifier, vehicleHash, vehicleStatBundle));
        }
        #endregion // Vehicles

        #region Weapons
        /// <summary>
        /// Gets a list of weapons that are associated with the specified build.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <returns></returns>
        public WeaponDtos GetAllWeaponsForBuild(string buildIdentifier)
        {
            return ExecuteCommand(() => base.Channel.GetAllWeaponsForBuild(buildIdentifier));
        }

        /// <summary>
        /// Gets an individual weapon for a particular build based on the weapon's hash.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="weaponHash">Hash of the weapon's name.</param>
        /// <returns></returns>
        public WeaponDto GetWeaponForBuild(string buildIdentifier, string weaponHash)
        {
            return ExecuteCommand(() => base.Channel.GetWeaponForBuild(buildIdentifier, weaponHash));
        }

        /// <summary>
        /// Gets an individual weapon statistic for a particular build and weapon.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="weaponHash">Hash of the weapon's name.</param>
        /// <returns></returns>
        public WeaponStatBundleDto GetWeaponStat(string buildIdentifier, string weaponHash)
        {
            WeaponStatBundleDto bundle = ExecuteCommand(() => base.Channel.GetWeaponStat(buildIdentifier, weaponHash));
            bundle.ResolveReferences();
            return bundle;
        }

        /// <summary>
        /// Creates a new weapon stat for the specified build and weapon.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="weaponHash">Hash of the weapon's name.</param>
        /// <param name="weaponStatBundle">Data transfer object containing all the information relating to a weapon stat.</param>
        /// <returns></returns>
        public void CreateWeaponStat(string buildIdentifier, string weaponHash, WeaponStatBundleDto weaponStatBundle)
        {
            ExecuteCommand(() => base.Channel.CreateWeaponStat(buildIdentifier, weaponHash, weaponStatBundle));
        }
        #endregion // Weapons

        #region Data Deletion
        /// <summary>
        /// 
        /// </summary>
        /// <param name="buildIdentifier"></param>
        public void DeleteAssetStats(string buildIdentifier)
        {
            ExecuteCommand(() => base.Channel.DeleteAssetStats(buildIdentifier));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buildIdentifier"></param>
        public void DeleteTelemetryStats(string buildIdentifier)
        {
            ExecuteCommand(() => base.Channel.DeleteTelemetryStats(buildIdentifier));
        }
        #endregion // Data Deletion
    } // BuildClient
}
