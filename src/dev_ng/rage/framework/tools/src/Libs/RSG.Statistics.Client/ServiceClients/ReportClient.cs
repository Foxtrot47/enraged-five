﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using RSG.Base.Configuration.Services;
using RSG.Services.Common.Clients;
using RSG.Statistics.Common.Async;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Dto.Reports;
using RSG.Statistics.Common.ServiceContract;

namespace RSG.Statistics.Client.ServiceClients
{
    /// <summary>
    /// 
    /// </summary>
    public class ReportClient : ConfigAwareClient<IReportService>, IReportService
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public ReportClient(IServiceHostConfig config)
            : base(config)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="displayMessage"></param>
        public ReportClient(Binding binding, EndpointAddress remoteAddress)
            : base(binding, remoteAddress)
        {
        }
        #endregion // Constructor(s)

        #region IReportService Implementation
        /// <summary>
        /// Returns a list of all the available reports.
        /// </summary>
        /// <returns></returns>
        public List<String> GetAvailableReportList()
        {
            return ExecuteCommand(() => base.Channel.GetAvailableReportList());
        }

        /// <summary>
        /// Returns the list of parameters required for a particular report.
        /// </summary>
        /// <returns></returns>
        public List<ReportParameter> GetReportParameters(String identifier)
        {
            return ExecuteCommand(() => base.Channel.GetReportParameters(identifier));
        }

        /// <summary>
        /// Executes a particular report with the passed in parameters.
        /// </summary>
        /// <returns></returns>
        public object RunReport(String identifier, bool forceRegen, List<ReportParameter> parameters)
        {
            return ExecuteCommand(() => base.Channel.RunReport(identifier, forceRegen, parameters));
        }

        /// <summary>
        /// Executes a particular report with the passed in parameters in a background thread.
        /// </summary>
        /// <returns></returns>
        public IAsyncCommandResult RunReportAsync(String identifier, bool forceRegen, List<ReportParameter> parameters)
        {
            return ExecuteCommand(() => base.Channel.RunReportAsync(identifier, forceRegen, parameters));
        }

        /// <summary>
        /// Returns the list of stylesheets that are available for the particular report.
        /// </summary>
        /// <returns></returns>
        public List<String> GetReportStylesheetNames(String identifier)
        {
            return ExecuteCommand(() => base.Channel.GetReportStylesheetNames(identifier));
        }

        /// <summary>
        /// Returns a specific stylesheet for the requested report.
        /// </summary>
        /// <returns></returns>
        public Stream GetReportStylesheet(String identifier, String stylesheet)
        {
            return ExecuteCommand(() => base.Channel.GetReportStylesheet(identifier, stylesheet));
        }

        /// <summary>
        /// Returns a list of all presets that the database contains.
        /// </summary>
        /// <returns></returns>
        public List<ReportPreset> GetPresets()
        {
            return ExecuteCommand(() => base.Channel.GetPresets());
        }

        /// <summary>
        /// Creates a new report preset.
        /// </summary>
        public ReportPreset CreatePreset(ReportPreset preset)
        {
            return ExecuteCommand(() => base.Channel.CreatePreset(preset));
        }

        /// <summary>
        /// Deletes an existing report preset.
        /// </summary>
        /// <param name="id"></param>
        public void DeletePreset(String id)
        {
            ExecuteCommand(() => base.Channel.DeletePreset(id));
        }
        #endregion // IReportService Implementation

        #region IQueryAsyncService Implementation
        /// <summary>
        /// 
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public IAsyncCommandResult Query(string guid)
        {
            return ExecuteCommand(() => base.Channel.Query(guid));
        }
        #endregion // IQueryAsyncService Implementation

        #region Hashing
        /// <summary>
        /// Computes the uint32 hash for the provided input strings.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public IDictionary<String, uint> ComputeHashes(IList<String> inputs)
        {
            return ExecuteCommand(() => base.Channel.ComputeHashes(inputs));
        }
        #endregion // Hashing
    } // ReportClient
}
