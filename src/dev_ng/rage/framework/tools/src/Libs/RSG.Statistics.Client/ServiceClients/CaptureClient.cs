﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common.ServiceContract;
using RSG.Statistics.Common.Dto;
using RSG.Model.Statistics.Captures;
using RSG.Model.Statistics.Captures.Historical;
using System.ServiceModel.Channels;
using System.ServiceModel;
using RSG.Statistics.Common.Config;
using RSG.Services.Common.Clients;
using RSG.Base.Configuration.Services;

namespace RSG.Statistics.Client.ServiceClients
{
    /// <summary>
    /// 
    /// </summary>
    public class CaptureClient : ConfigAwareClient<ICaptureService>, ICaptureService
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public CaptureClient(IServiceHostConfig config)
            : base(config)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="displayMessage"></param>
        public CaptureClient(Binding binding, EndpointAddress remoteAddress)
            : base(binding, remoteAddress)
        {
        }
        #endregion // Constructor(s)

        #region ICaptureService Methods

        /// <summary>
        /// Retrieve changlelists in the capturesessions.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<uint> RetrieveChangelists()
        {
            return ExecuteCommand(() => base.Channel.RetrieveChangelists());
        }
        
        /// <summary>
        /// Retrieve buildsids of capturesessions with perfstats.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> RetrievePerfstatBuilds()
        {
            return ExecuteCommand(() => base.Channel.RetrievePerfstatBuilds());
        }

        /// <summary>
        /// Creates a new zone with the specified name.
        /// </summary>
        /// <returns></returns>
        public NameHashDto CreateZone(string zoneName)
        {
            return ExecuteCommand(() => base.Channel.CreateZone(zoneName));
        }

        /// <summary>
        /// Creates a new cpu set with the specified name.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public NameHashDto CreateCpuSet(string setName)
        {
            return ExecuteCommand(() => base.Channel.CreateCpuSet(setName));
        }

        /// <summary>
        /// Creates a new cpu metric with the specified name.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public NameHashDto CreateCpuMetric(string metricName)
        {
            return ExecuteCommand(() => base.Channel.CreateCpuMetric(metricName));
        }

        /// <summary>
        /// Creates a new thread with the specified name.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public NameHashDto CreateThreadName(string name)
        {
            return ExecuteCommand(() => base.Channel.CreateThreadName(name));
        }

        /// <summary>
        /// Creates a new memory bucket with the specified name.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public NameHashDto CreateMemoryBucket(string bucketName)
        {
            return ExecuteCommand(() => base.Channel.CreateMemoryBucket(bucketName));
        }

        /// <summary>
        /// Creates a new streaming memory module with the specified name.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public NameHashDto CreateStreamingMemoryModule(string moduleName)
        {
            return ExecuteCommand(() => base.Channel.CreateStreamingMemoryModule(moduleName));
        }

        /// <summary>
        /// Creates a new streaming memory category with the specified name.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public NameHashDto CreateStreamingMemoryCategory(string categoryName)
        {
            return ExecuteCommand(() => base.Channel.CreateStreamingMemoryCategory(categoryName));
        }

        /// <summary>
        /// Creates a new set of stats of the per stats variety.
        /// </summary>
        /// <param name="session"></param>
        public void CreatePerfStats(TestSession session)
        {
            ExecuteCommand(() => base.Channel.CreatePerfStats(session));
        }

        /// <summary>
        /// Creates a new set of stats associated with a particular changelist.
        /// </summary>
        /// <param name="session"></param>
        public void CreateChangelistStats(String changelistNumber, TestSession session)
        {
            ExecuteCommand(() => base.Channel.CreateChangelistStats(changelistNumber, session));
        }

        /// <summary>
        /// Retrieves the changelist number of the latest set of changelist capture stat.
        /// </summary>
        public uint RetrieveLatestChangelistNumber()
        {
            return ExecuteCommand(() => base.Channel.RetrieveLatestChangelistNumber());
        }

        /// <summary>
        /// Retrieves the zones that were a part of a particular per changelist capture.
        /// </summary>
        /// <param name="changelistNumber"></param>
        public NameHashDtos RetrieveChangelistZones(String changelistNumber)
        {
            return ExecuteCommand(() => base.Channel.RetrieveChangelistZones(changelistNumber));
        }

        /// <summary>
        /// Retrieves all historical data associated with a particular zone.
        /// </summary>
        public HistoricalZoneResults RetrieveHistoricalResultsForZone(String changelistNumber, String zoneIdentifier, uint maxResults)
        {
            return ExecuteCommand(() => base.Channel.RetrieveHistoricalResultsForZone(changelistNumber, zoneIdentifier, maxResults));
        }

        /// <summary>
        /// Retrieves the zones that were a part of a particular per build capture.
        /// </summary>
        /// <param name="buildNumber"></param>
        /// <returns></returns>
        public NameHashDtos RetrievePerfStatZones(String buildNumber)
        {
            return ExecuteCommand(() => base.Channel.RetrievePerfStatZones(buildNumber));
        }

        /// <summary>
        /// Retrieves all historical perfstat data associated with a particular zone.
        /// a build range can be specified.
        /// </summary>
        public HistoricalZoneResults RetrievePerfStatHistoricalResultsForZone(String buildNumber, String zoneIdentifier, uint maxResults)
        {
            return ExecuteCommand(() => base.Channel.RetrievePerfStatHistoricalResultsForZone(buildNumber, zoneIdentifier, maxResults));
        }

        /// <summary>
        /// Retrieve the name of the metrics availableto show for a particular automated test.
        /// </summary>
        public List<String> RetrieveAutomatedTestPerformanceMetrics(String automatedTestNumber, String buildIdentifier, String platformIdentifier, uint levelIdentifier)
        {
            return ExecuteCommand(() => base.Channel.RetrieveAutomatedTestPerformanceMetrics(automatedTestNumber, buildIdentifier, platformIdentifier, levelIdentifier));
        }

        /// <summary>
        /// Retrieves summary information for all automated capture stats associated with a particular build/level/platform.
        /// </summary>
        public TestSessionSummary RetreieveAutomatedTestPerformanceSummary(String automatedTestNumber, String buildIdentifier, String platformIdentifier, uint levelIdentifier)
        {
            return ExecuteCommand(() => base.Channel.RetreieveAutomatedTestPerformanceSummary(automatedTestNumber, buildIdentifier, platformIdentifier, levelIdentifier));
        }
        #endregion // ICaptureService Methods
    } // CaptureClient
}
