﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using RSG.Base.Configuration.Services;
using RSG.Services.Common.Clients;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.ServiceContract;
using SceneXmlDto = RSG.Statistics.Common.Dto.SceneXml;

namespace RSG.Statistics.Client.ServiceClients
{
    /// <summary>
    /// Client for manipulating scene xml data stored on the server.
    /// </summary>
    public class SceneXmlManipulationClient : ConfigAwareClient<ISceneXmlManipulationService>, ISceneXmlManipulationService
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public SceneXmlManipulationClient(IServiceHostConfig config)
            : base(config)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="binding"></param>
        /// <param name="remoteAddress"></param>
        public SceneXmlManipulationClient(Binding binding, EndpointAddress remoteAddress)
            : base(binding, remoteAddress)
        {
        }
        #endregion // Constructor(s)

        #region ISceneXmlManipulationService Implementation
        /// <summary>
        /// Populates the database with a particular scene xml file.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="scene"></param>
        public void PopulateSceneXmlFile(String filename, SceneXmlDto.Scene scene)
        {
            ExecuteCommand(() => base.Channel.PopulateSceneXmlFile(filename, scene));
        }

        /// <summary>
        /// Deletes all data relating to a particular scene xml file.
        /// </summary>
        /// <param name="filename"></param>
        public void DeleteSceneXmlFile(String filename)
        {
            ExecuteCommand(() => base.Channel.DeleteSceneXmlFile(filename));
        }
        #endregion // ISceneXmlManipulationService Implementation
    } // SceneXmlManipulationClient
}
