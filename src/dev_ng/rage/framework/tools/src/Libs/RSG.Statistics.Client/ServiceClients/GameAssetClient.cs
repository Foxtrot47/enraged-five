﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common.Mission;
using RSG.Model.Common.Animation;
using RSG.Statistics.Common.ServiceContract;
using RSG.Statistics.Common.Model.GameAssets;
using RSG.Statistics.Common.Config;
using RSG.Services.Common.Clients;
using RSG.Base.Configuration.Services;

namespace RSG.Statistics.Client.ServiceClients
{
    /// <summary>
    /// 
    /// </summary>
    public class GameAssetClient : ConfigAwareClient<IGameAssetService>, IGameAssetService
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public GameAssetClient()
            : base(new StatisticsConfig().DefaultServer)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public GameAssetClient(IServiceHostConfig config)
            : base(config)
        {
        }
        #endregion // Constructor(s)

        #region Animations
        /// <summary>
        /// Updates or creates a list of ClipDictionaries ( animations ).
        /// </summary>
        public void UpdateClipDictionaries(IClipDictionaryCollection clipDictionaries)
        {
            ExecuteCommand(() => base.Channel.UpdateClipDictionaries(clipDictionaries));
        }
        #endregion Animations

        #region Cash Packs
        /// <summary>
        /// Updates the list of cash-packs that are currently active in game.
        /// </summary>
        public void UpdateCashPacks(IList<CashPack> cashPacks)
        {
            ExecuteCommand(() => base.Channel.UpdateCashPacks(cashPacks));
        }
        #endregion // Cash Packs

        #region Cutscenes
        /// <summary>
        /// Updates or creates a list of cutscenes.
        /// </summary>
        public void UpdateCutscenes(ICutsceneCollection cutscenes)
        {
            ExecuteCommand(() => base.Channel.UpdateCutscenes(cutscenes));
        }
        #endregion // Cutscenes

        #region Draw Lists
        /// <summary>
        /// Updates the list of draw lists.
        /// </summary>
        public void UpdateDrawLists(IList<GameAsset> drawLists)
        {
            ExecuteCommand(() => base.Channel.UpdateDrawLists(drawLists));
        }

        /// <summary>
        /// Returns a list of all draw lists.
        /// </summary>
        /// <returns></returns>
        public IList<GameAsset> GetDrawLists()
        {
            return ExecuteCommand(() => base.Channel.GetDrawLists());
        }
        #endregion // Draw Lists

        #region Memory Pools, Stores & Heaps
        /// <summary>
        /// Updates the memory hash / name DB table for memorypools
        /// </summary>
        /// <param name="hashDict"></param>
        public void UpdateMemoryPools(IDictionary<uint, String> hashDict)
        {
            ExecuteCommand(() => base.Channel.UpdateMemoryPools(hashDict));
        }

        /// <summary>
        /// Updates the memory hash / name DB table for memorystores
        /// </summary>
        /// <param name="hashDict"></param>
        public void UpdateMemoryStores(IDictionary<uint, String> hashDict)
        {
            ExecuteCommand(() => base.Channel.UpdateMemoryStores(hashDict));
        }

        /// <summary>
        /// Updates or creates a dictionary of memory heap hashes to memory heap names.
        /// </summary>
        public void UpdateMemoryHeaps(IDictionary<uint, String> hashDict)
        {
            ExecuteCommand(() => base.Channel.UpdateMemoryHeaps(hashDict));
        }
        #endregion // Memory Pools, Stores & Heaps

        #region Mini-Game Variants
        /// <summary>
        /// Updates the list of mini-game variants.
        /// </summary>
        public void UpdateMiniGameVariants(IList<MiniGameVariant> miniGames)
        {
            ExecuteCommand(() => base.Channel.UpdateMiniGameVariants(miniGames));
        }
        #endregion // Mini-Game Variants

        #region Missions
        /// <summary>
        /// Returns a list of all missions.
        /// </summary>
        /// <returns></returns>
        public IMissionCollection GetMissions()
        {
            return ExecuteCommand(() => base.Channel.GetMissions());
        }

        /// <summary>
        /// Updates or creates a list of missions.
        /// </summary>
        public void UpdateMissions(IMissionCollection missions)
        {
            ExecuteCommand(() => base.Channel.UpdateMissions(missions));
        }

        /// <summary>
        /// Deletes a single mission.
        /// </summary>
        public void DeleteMission(String identifier)
        {
            ExecuteCommand(() => base.Channel.DeleteMission(identifier));
        }
        #endregion // Missions

        #region Movies
        /// <summary>
        /// Updates the list of movies.
        /// </summary>
        public void UpdateMovies(IList<FriendlyGameAsset> movies)
        {
            ExecuteCommand(() => base.Channel.UpdateMovies(movies));
        }
        #endregion // Movies

        #region Properties
        /// <summary>
        /// Updates the list of properties.
        /// </summary>
        public void UpdateProperties(IDictionary<uint, String> properties)
        {
            ExecuteCommand(() => base.Channel.UpdateProperties(properties));
        }
        #endregion // Properties

        #region Ranks
        /// <summary>
        /// Updates the list of ranks.
        /// </summary>
        public void UpdateRanks(IList<Rank> ranks)
        {
            ExecuteCommand(() => base.Channel.UpdateRanks(ranks));
        }
        #endregion // Ranks

        #region Radio Stations
        /// <summary>
        /// Updates the list of TV shows.
        /// </summary>
        public void UpdateRadioStations(IList<FriendlyGameAsset> radioStations)
        {
            ExecuteCommand(() => base.Channel.UpdateRadioStations(radioStations));
        }
        #endregion // Radio Stations

        #region Shopping
        /// <summary>
        /// Updates all shop names.
        /// </summary>
        public void UpdateShops(IList<Shop> shops)
        {
            ExecuteCommand(() => base.Channel.UpdateShops(shops));
        }

        /// <summary>
        /// Updates all shop items.
        /// </summary>
        public void UpdateShopItems(IList<ShopItem> shopItems)
        {
            ExecuteCommand(() => base.Channel.UpdateShopItems(shopItems));
        }
        
        /// <summary>
        /// Returns a list of all shop items.
        /// </summary>
        /// <returns></returns>
        public IList<ShopItem> GetShopItems()
        {
            return ExecuteCommand(() => base.Channel.GetShopItems());
        }
        #endregion // Shopping

        #region TV Shows
        /// <summary>
        /// Updates the list of TV shows.
        /// </summary>
        public void UpdateTVShows(IList<FriendlyGameAsset> tvShows)
        {
            ExecuteCommand(() => base.Channel.UpdateTVShows(tvShows));
        }
        #endregion // TV Shows

        #region Unlocks
        /// <summary>
        /// Updates the list of unlocks.
        /// </summary>
        public void UpdateUnlocks(IList<Unlock> unlocks)
        {
            ExecuteCommand(() => base.Channel.UpdateUnlocks(unlocks));
        }
        #endregion // Unlocks

        #region Vehicles
        /// <summary>
        /// Returns a list of all vehicles.
        /// </summary>
        public List<Vehicle> GetVehicles()
        {
            return ExecuteCommand(() => base.Channel.GetVehicles());
        }
        #endregion // Vehicles

        #region Weapons
        /// <summary>
        /// Returns a list of all weapons.
        /// </summary>
        public List<Weapon> GetWeapons()
        {
            return ExecuteCommand(() => base.Channel.GetWeapons());
        }
        #endregion // Weapons

        #region Websites
        /// <summary>
        /// Updates the list of websites.
        /// </summary>
        public void UpdateWebsites(IDictionary<uint, String> websites)
        {
            ExecuteCommand(() => base.Channel.UpdateWebsites(websites));
        }
        #endregion // Websites

        #region XP
        /// <summary>
        /// Updates the list of XP categories.
        /// </summary>
        public void UpdateXPCategories(IList<FriendlyGameAsset> categories)
        {
            ExecuteCommand(() => base.Channel.UpdateXPCategories(categories));
        }

        /// <summary>
        /// Updates the list of XP types.
        /// </summary>
        public void UpdateXPTypes(IList<FriendlyGameAsset> types)
        {
            ExecuteCommand(() => base.Channel.UpdateXPTypes(types));
        }
        #endregion // XP
    } // GameAssetClient
}
