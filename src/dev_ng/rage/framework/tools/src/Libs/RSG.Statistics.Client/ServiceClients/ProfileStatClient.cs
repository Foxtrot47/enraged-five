﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using RSG.Base.Configuration.Services;
using RSG.Services.Common.Clients;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Dto.ProfileStats;
using RSG.Statistics.Common.ServiceContract;

namespace RSG.Statistics.Client.ServiceClients
{
    /// <summary>
    /// Profile stat client
    /// </summary>
    public class ProfileStatClient : ConfigAwareClient<IProfileStatService>, IProfileStatService
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public ProfileStatClient(IServiceHostConfig config)
            : base(config)
        {
        }
        #endregion // Constructor(s)

        #region IProfileStatService Implementation
        /// <summary>
        /// Updates the profile stat definitions that the server is aware of.
        /// </summary>
        /// <returns></returns>
        public void UpdateDefinitions(IList<ProfileStatDefinitionDto> definitions)
        {
            ExecuteCommand(() => base.Channel.UpdateDefinitions(definitions));
        }

        /// <summary>
        /// Retrieves the list of active definitions.
        /// </summary>
        public IList<ProfileStatDefinitionDto> GetActiveDefinitions()
        {
            return ExecuteCommand(() => base.Channel.GetActiveDefinitions());
        }
        #endregion // IProfileStatService Implementation
    } // ProfileStatClient
}
