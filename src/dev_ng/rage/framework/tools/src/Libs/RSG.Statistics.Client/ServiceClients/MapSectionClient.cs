﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common.ServiceContract;
using RSG.Statistics.Common.Dto.GameAssets;
using RSG.Statistics.Common.Dto.GameAssetStats;
using RSG.Statistics.Common.Config;
using RSG.Services.Common.Clients;
using RSG.Base.Configuration.Services;

namespace RSG.Statistics.Client.ServiceClients
{
    /// <summary>
    /// 
    /// </summary>
    public class MapSectionClient : ConfigAwareClient<IMapSectionService>, IMapSectionService
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public MapSectionClient(IServiceHostConfig config)
            : base(config)
        {
        }
        #endregion // Constructor(s)

        #region IMapSectionService Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public MapSectionDtos GetAll()
        {
            return ExecuteCommand(() => base.Channel.GetAll());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="identifier"></param>
        /// <returns></returns>
        public MapSectionDto GetByIdentifier(string identifier)
        {
            return ExecuteCommand(() => base.Channel.GetByIdentifier(identifier));
        }

        /// <summary>
        /// Updates or creates a list of game assets.
        /// </summary>
        /// <param name="assetId"></param>
        /// <param name="asset"></param>
        /// <returns></returns>
        public void PutAssets(MapSectionDtos dtos)
        {
            ExecuteCommand(() => base.Channel.PutAssets(dtos));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="identifier"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        public MapSectionDto PutAsset(string identifier, MapSectionDto dto)
        {
            return ExecuteCommand(() => base.Channel.PutAsset(identifier, dto));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="identifier"></param>
        public void DeleteAsset(string identifier)
        {
            ExecuteCommand(() => base.Channel.DeleteAsset(identifier));
        }
        #endregion // IMapSectionService Methods
    } // MapSectionClient
}
