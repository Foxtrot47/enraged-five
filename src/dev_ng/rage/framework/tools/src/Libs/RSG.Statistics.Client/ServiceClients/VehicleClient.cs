﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using RSG.Statistics.Common.ServiceContract;
using RSG.Statistics.Common.Dto.GameAssets;
using RSG.Statistics.Common.Dto.ProfileStats;
using RSG.Statistics.Common.Dto;
using RSG.Statistics.Common.Dto.GameAssetStats;
using RSG.Statistics.Common.Config;
using RSG.Services.Common.Clients;
using RSG.Base.Configuration.Services;

namespace RSG.Statistics.Client.ServiceClients
{
    /// <summary>
    /// 
    /// </summary>
    public class VehicleClient : ConfigAwareClient<IVehicleService>, IVehicleService
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public VehicleClient(IServiceHostConfig config)
            : base(config)
        {
        }
        #endregion // Constructor(s)
    
        #region IVehicleService Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public VehicleDtos GetAll()
        {
            return ExecuteCommand(() => base.Channel.GetAll());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="identifier"></param>
        /// <returns></returns>
        public VehicleDto GetByIdentifier(string identifier)
        {
            return ExecuteCommand(() => base.Channel.GetByIdentifier(identifier));
        }

        /// <summary>
        /// Updates or creates a list of game assets.
        /// </summary>
        /// <param name="assetId"></param>
        /// <param name="asset"></param>
        /// <returns></returns>
        public void PutAssets(VehicleDtos dtos)
        {
            ExecuteCommand(() => base.Channel.PutAssets(dtos));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="identifier"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        public VehicleDto PutAsset(string identifier, VehicleDto dto)
        {
            return ExecuteCommand(() => base.Channel.PutAsset(identifier, dto));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="identifier"></param>
        public void DeleteAsset(string identifier)
        {
            ExecuteCommand(() => base.Channel.DeleteAsset(identifier));
        }
        #endregion // IVehicleService Methods
    } // VehicleClient
}
