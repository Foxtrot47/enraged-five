﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common.ServiceContract;
using RSG.Statistics.Common.Dto.GameAssets;
using RSG.Statistics.Common.Config;
using RSG.Services.Common.Clients;
using RSG.Base.Configuration.Services;

namespace RSG.Statistics.Client.ServiceClients
{
    /// <summary>
    /// Client for interacting with the archetype service.
    /// </summary>
    public class ArchetypeClient : ConfigAwareClient<IArchetypeService>, IArchetypeService
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public ArchetypeClient(IServiceHostConfig config)
            : base(config)
        {
        }
        #endregion // Constructor(s)

        #region IArchetypeService Methods
        /// <summary>
        /// Returns a list of all archetypes.
        /// </summary>
        /// <returns></returns>
        public ArchetypeDtos GetAll()
        {
            return ExecuteCommand(() => base.Channel.GetAll());
        }

        /// <summary>
        /// Returns a list of all drawables.
        /// </summary>
        /// <returns></returns>
        public ArchetypeDtos GetAllDrawables()
        {
            return ExecuteCommand(() => base.Channel.GetAll());
        }

        /// <summary>
        /// Updates or creates a drawable archetype based on its identifier.
        /// </summary>
        /// <param name="assetId"></param>
        /// <param name="asset"></param>
        /// <returns></returns>
        public DrawableArchetypeDto PutDrawable(string identifier, DrawableArchetypeDto dto)
        {
            return ExecuteCommand(() => base.Channel.PutDrawable(identifier, dto));
        }
        
        /// <summary>
        /// Updates or creates a list of drawable archetypes.
        /// </summary>
        /// <param name="dtos"></param>
        public void PutDrawables(ArchetypeDtos dtos)
        {
            ExecuteCommand(() => base.Channel.PutDrawables(dtos));
        }

        /// <summary>
        /// Returns a list of all fragments.
        /// </summary>
        /// <returns></returns>
        public ArchetypeDtos GetAllFragments()
        {
            return ExecuteCommand(() => base.Channel.GetAllFragments());
        }

        /// <summary>
        /// Updates or creates a list of fragment archetypes.
        /// </summary>
        /// <param name="dtos"></param>
        public void PutFragments(ArchetypeDtos dtos)
        {
            ExecuteCommand(() => base.Channel.PutFragments(dtos));
        }

        /// <summary>
        /// Updates or creates a fragment archetype based on its identifier
        /// </summary>
        /// <param name="assetId"></param>
        /// <param name="asset"></param>
        /// <returns></returns>
        public FragmentArchetypeDto PutFragment(string identifier, FragmentArchetypeDto dto)
        {
            return ExecuteCommand(() => base.Channel.PutFragment(identifier, dto));
        }

        /// <summary>
        /// Returns a list of all stated anims.
        /// </summary>
        /// <returns></returns>
        public ArchetypeDtos GetAllStatedAnims()
        {
            return ExecuteCommand(() => base.Channel.GetAllStatedAnims());
        }

        /// <summary>
        /// Updates or creates a list of stated anim archetypes.
        /// </summary>
        /// <param name="dtos"></param>
        public void PutStatedAnims(ArchetypeDtos dtos)
        {
            ExecuteCommand(() => base.Channel.PutStatedAnims(dtos));
        }

        /// <summary>
        /// Updates or creates a stated anim archetype based on its identifier
        /// </summary>
        /// <param name="assetId"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        public StatedAnimArchetypeDto PutStatedAnim(string identifier, StatedAnimArchetypeDto dto)
        {
            return ExecuteCommand(() => base.Channel.PutStatedAnim(identifier, dto));
        }

        /// <summary>
        /// Returns a list of all interiors.
        /// </summary>
        /// <returns></returns>
        public ArchetypeDtos GetAllInteriors()
        {
            return ExecuteCommand(() => base.Channel.GetAllInteriors());
        }

        /// <summary>
        /// Updates or creates a list of interior archetypes.
        /// </summary>
        /// <param name="dtos"></param>
        public void PutInteriors(ArchetypeDtos dtos)
        {
            ExecuteCommand(() => base.Channel.PutInteriors(dtos));
        }

        /// <summary>
        /// Updates or creates an interior archetype based on its identifier.
        /// </summary>
        /// <param name="assetId"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        public InteriorArchetypeDto PutInterior(string identifier, InteriorArchetypeDto dto)
        {
            return ExecuteCommand(() => base.Channel.PutInterior(identifier, dto));
        }
        #endregion // IArchetypeService Methods
    } // ArchetypeClient
}
