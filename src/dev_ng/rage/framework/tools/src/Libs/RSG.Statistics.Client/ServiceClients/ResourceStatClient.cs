﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common.ServiceContract;
using RSG.Statistics.Common.Dto;
using RSG.Model.Statistics.Captures;
using RSG.Model.Statistics.Captures.Historical;
using System.ServiceModel.Channels;
using System.ServiceModel;
using RSG.Model.Statistics.Platform;
using RSG.Statistics.Common.Config;
using RSG.Services.Common.Clients;
using RSG.Base.Configuration.Services;

namespace RSG.Statistics.Client.ServiceClients
{
    /// <summary>
    /// Client for resource stats
    /// </summary>
    public class ResourceStatClient : ConfigAwareClient<IResourceStatService>, IResourceStatService
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public ResourceStatClient(IServiceHostConfig config)
            : base(config)
        {
        }
        #endregion // Constructor(s)

        #region IResourceStatService Methods

        /// <summary>
        /// Creates new resource stats
        /// </summary>
        /// <param name="session"></param>
        public void CreateResourceStats(List<ResourceStat> stats)
        {
            ExecuteCommand(() => base.Channel.CreateResourceStats(stats));
        }

        #endregion // IResourceStat Methods
    } // ResourceStatClient
}
