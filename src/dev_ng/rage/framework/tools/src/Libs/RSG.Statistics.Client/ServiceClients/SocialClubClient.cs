﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Configuration.Services;
using RSG.Services.Common.Clients;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Model.SocialClub;
using RSG.Statistics.Common.ServiceContract;

namespace RSG.Statistics.Client.ServiceClients
{
    /// <summary>
    /// Client for the social club services.
    /// </summary>
    public class SocialClubClient : ConfigAwareClient<ISocialClubService>, ISocialClubService
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public SocialClubClient(IServiceHostConfig config)
            : base(config)
        {
        }
        #endregion // Constructor(s)

        #region Countries
        /// <summary>
        /// Retrieves the list of countries.
        /// </summary>
        /// <returns></returns>
        public List<Country> GetCountries()
        {
            return ExecuteCommand(() => base.Channel.GetCountries());
        }

        /// <summary>
        /// Creates a new country.
        /// </summary>
        /// <returns></returns>
        public Country CreateCountry(Country country)
        {
            return ExecuteCommand(() => base.Channel.CreateCountry(country));
        }
        #endregion // Countries
    } // SocialClubClient
}
