﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common.ServiceContract;
using System.ServiceModel;
using RSG.Statistics.Common.Dto.GameAssets;
using RSG.Statistics.Common.Config;
using RSG.Base.Configuration.Services;
using RSG.Services.Common.Clients;

namespace RSG.Statistics.Client.ServiceClients
{
    /// <summary>
    /// 
    /// </summary>
    public class WeaponClient : ConfigAwareClient<IWeaponService>, IWeaponService
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public WeaponClient(IServiceHostConfig config)
            : base(config)
        {
        }
        #endregion // Constructor(s)
    
        #region IWeaponService Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public WeaponDtos GetAll()
        {
            return ExecuteCommand(() => base.Channel.GetAll());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="identifier"></param>
        /// <returns></returns>
        public WeaponDto GetByIdentifier(string identifier)
        {
            return ExecuteCommand(() => base.Channel.GetByIdentifier(identifier));
        }

        /// <summary>
        /// Updates or creates a list of game assets.
        /// </summary>
        /// <param name="assetId"></param>
        /// <param name="asset"></param>
        /// <returns></returns>
        public void PutAssets(WeaponDtos dtos)
        {
            ExecuteCommand(() => base.Channel.PutAssets(dtos));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="identifier"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        public WeaponDto PutAsset(string identifier, WeaponDto dto)
        {
            return ExecuteCommand(() => base.Channel.PutAsset(identifier, dto));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="identifier"></param>
        public void DeleteAsset(string identifier)
        {
            ExecuteCommand(() => base.Channel.DeleteAsset(identifier));
        }
        #endregion // IWeaponService Methods
    } // WeaponClient
}
