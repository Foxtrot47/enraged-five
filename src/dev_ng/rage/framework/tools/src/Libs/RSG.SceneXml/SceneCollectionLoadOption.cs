﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.SceneXml
{

    /// <summary>
    /// SceneCollection auto-Scene load options.
    /// </summary>
    [Flags]
    public enum SceneCollectionLoadOption : uint
    {
        None = 0,
        
        Props = 1,
        Interiors = 2,

        All = 0xFFFFFFFF,
    }

} // RSG.SceneXml namespace
