﻿//
// File: Attributes.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of Attribute name and defaults classes.
//

using System;

namespace RSG.SceneXml.Material
{
    /// <summary>
    /// Texture Types (DiffuseMap, BumpMap, etc)
    /// </summary>
    public enum TextureTypes
    {
        None = 0
        , DiffuseMap
        , BumpMap
        , SpecularMap
        , GlossMap
        , SpecularLevelMap
        , AmbientMap
        , SelfIlluminationMap
        , OpacityMap
        , FilterColourMap
        , ReflectionMap
        , RefractionMap
        , DisplacementMap
        , DiffuseAlpha
        , BumpAlpha
        , SpecularAlpha
    }

    /// <summary>
    /// Material types (Multi, Standard, Rage)
    /// </summary>
    public enum MaterialTypes
    {
        None = 0
        , MultiMaterial
        , Standard
        , Rage
    }

} // RSG.SceneXml.Material namespace

// Attributes.cs