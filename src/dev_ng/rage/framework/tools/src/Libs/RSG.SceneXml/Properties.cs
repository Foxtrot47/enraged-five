//
// File: Properties.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of Property names and default classes.
//

using System;
using RSG.Base.Math;
using System.Collections.Generic;

namespace RSG.SceneXml
{

    /// <summary>
    /// 
    /// </summary>
    public static class PropNames
    {
        #region Gta Light / Gta LightPhoto
//         public static readonly String TWODFX_LIGHT_COLOUR = "Colour".ToLower();
//         public static readonly String TWODFX_LIGHT_INTENSITY = "Intensity".ToLower();
//         public static readonly String TWODFX_LIGHT_ATTENUATION_END = "Attenuation End".ToLower();
//         public static readonly String TWODFX_LIGHT_HOTSPOT = "Hotspot".ToLower();
// 		public static readonly String TWODFX_LIGHT_FALLSIZE = "Fallsize".ToLower();
// 		public static readonly String TWODFX_LIGHT_SHADOW_MAP = "Shadowmap".ToLower();
//         public static readonly String TWODFX_LIGHT_EXP_FALLOFF = "Exponential Falloff".ToLower();
//         public static readonly String TWODFX_LIGHT_USE_CULLPLANE = "Use Cullplane".ToLower();
//         public static readonly String TWODFX_LIGHT_USE_VOL_OUTER_COLOUR = "Use Volume Outer Colour".ToLower();
//         public static readonly String TWODFX_LIGHT_CULLPLANE = "CullPlane".ToLower();
//         public static readonly String TWODFX_LIGHT_VOL_OUTER_COLOUR = "Volume Outer Colour".ToLower();
//         public static readonly String TWODFX_LIGHT_VOL_OUTER_COLOUR_INTEN = "Volume Outer Colour Intensity".ToLower();
        #endregion // Gta Light / Gta LightPhoto
    }

    /// <summary>
    /// 
    /// </summary>
    public static class PropDefaults
    {

        #region RS IMAP Group Dummy
        public static readonly String IMAP_GROUP_DUMMY_NAME = null;
        #endregion

        #region Gta Light / Gta LightPhoto
//         public static readonly int TWODFX_LIGHT_TYPE = 0;
//         public static readonly Vector4f TWODFX_LIGHT_COLOUR = new Vector4f();
//         public static readonly float TWODFX_LIGHT_INTENSITY = 1.0f;
//         public static readonly float TWODFX_LIGHT_ATTENUATION_END = 1.0f;
//         public static readonly bool TWODFX_LIGHT_IS_SPOT = false; 
//         public static readonly float TWODFX_LIGHT_HOTSPOT = 0.0f;
//         public static readonly float TWODFX_LIGHT_FALLSIZE = 0.0f;
//         public static readonly String TWODFX_LIGHT_SHADOW_MAP = null;
//         public static readonly float TWODFX_LIGHT_EXP_FALLOFF = 8.0f;
//         public static readonly bool TWODFX_LIGHT_USE_CULLPLANE = false;
//         public static readonly bool TWODFX_LIGHT_USE_VOL_OUTER_COLOUR = false;
//         public static readonly Vector3f TWODFX_LIGHT_VOL_OUTER_COLOUR = new Vector3f();
//         public static readonly Vector4f TWODFX_LIGHT_CULLPLANE = new Vector4f(0, 0, 1, 25);
//         public static readonly float TWODFX_LIGHT_VOL_OUTER_COLOUR_INTEN = 1.0f;
        #endregion // Gta Light / Gta LightPhoto

    }

} // RSG.SceneXml namespace

// Properties.cs