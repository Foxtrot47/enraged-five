﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.SceneXml
{

    /// <summary>
    /// Exception class used for RSG.SceneXml parse errors.
    /// </summary>
    public class SceneParseException : Exception
    {
        #region Constructors
        public SceneParseException()
            : base()
        {
        }

        public SceneParseException(String sMessage)
            : base(sMessage)
        {
        }

        public SceneParseException(String sMessage, Exception InnerException)
            : base(sMessage, InnerException)
        {
        }

        public SceneParseException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
            : base(info, context)
        {
        }
        #endregion // Constructor(s)
    }

} // RSG.SceneXml namespace
