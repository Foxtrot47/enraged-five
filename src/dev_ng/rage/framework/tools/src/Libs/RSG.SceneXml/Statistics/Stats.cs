﻿using System;
using System.IO;
using System.Xml;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Xml.XPath;
using System.Collections.Generic;
using RSG.Base.ConfigParser;
using RSG.Base.Logging;

namespace RSG.SceneXml.Statistics
{
    /// <summary>
    /// 
    /// </summary>
    public class Stats : BaseStats
    {
        #region Enums

        enum LimitState
        {
            Fine,
            Over,
            Maxed
        };

        #endregion // Emuns

        #region Properties

        #region Static Constants

        private static XmlDocument s_document;
#warning DHM FIX ME: constants used that are defined in EXPORT XML.  *sigh*
        private static int m_sMagicMultiplier = 6;
        private static int m_sByteLimit = (15 * 1024 * 1024) * m_sMagicMultiplier; //15 * m_sMagicMultiplier
        private static int m_sByteMaxLimit = (25 * 1024 * 1024) * m_sMagicMultiplier; //25 * m_sMagicMultiplier
        private static int m_sTxdByteLimit = (int)(5 * 1024 * 1024) * m_sMagicMultiplier; //5 * m_sMagicMultiplier
        private static int m_sDrawableByteLimit = (int)(5 * 1024 * 1024) * m_sMagicMultiplier; //5 * m_sMagicMultiplier
        private static int m_sTxdByteMaxLimit = (10 * 1024 * 1024) * m_sMagicMultiplier; //10 * m_sMagicMultiplier
        private static int m_sDrawableByteMaxLimit = (10 * 1024 * 1024) * m_sMagicMultiplier; //10 * m_sMagicMultiplier
        private static int m_sDrawableCountLimit = 25; //25
        private static int m_sTxdCountLimit = 10; //10
        private static float m_sBoundDrawableDistanceRatio = 10.0f; //The draw distance is expected to be no more than X times the bound size.
        private static ConfigExportView m_sExportConfiguration = null;
        #endregion // Static Constants

        /// <summary>
        /// True iff this object was able to successfully parse the scene xml for statistics.
        /// </summary>
        public Boolean ValidStatistics
        {
            get { return m_validStatistics; }
            set { m_validStatistics = value; }
        }
        public Boolean m_validStatistics;

        /// <summary>
        /// A dictionary that contains all of the texture dictionary statistics
        /// for a scene
        /// </summary>
        public Dictionary<String, TxdStats> TxdStats
        {
            get { return m_txdStats; }
            set { m_txdStats = value; }
        }
        private Dictionary<String, TxdStats> m_txdStats;

        /// <summary>
        /// A dictionary that contains all of the drawable statistics
        /// for a scene
        /// </summary>
        public Dictionary<Guid, DrawableStats> GeometryStats
        {
            get { return m_geometryStats; }
            set { m_geometryStats = value; }
        }
        private Dictionary<Guid, DrawableStats> m_geometryStats;

        /// <summary>
        /// A dictionary that contains all of the collision statistics
        /// for a scene
        /// </summary>
        public Dictionary<Guid, DrawableStats> CollisionStats
        {
            get { return m_collisionStats; }
            set { m_collisionStats = value; }
        }
        private Dictionary<Guid, DrawableStats> m_collisionStats;

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// Default constructor - sets the name and generates the statistics for
        /// the given scene xml file
        /// </summary>
        /// <param name="scene"></param>
        public Stats(Scene scene)
            :base(Path.GetFileNameWithoutExtension(scene.Filename), scene)
        {
            this.TxdStats = new Dictionary<String, TxdStats>();
            this.GeometryStats = new Dictionary<Guid, DrawableStats>();
            this.CollisionStats = new Dictionary<Guid, DrawableStats>();
            this.ValidStatistics = false;

            ParseConfigurationLimits();
        }

        #endregion // Constructor(s)

        #region Private Function(s)

        /// <summary>
        /// Parses from an XML file.
        /// </summary>
        private void ParseConfigurationLimits()
        {
            if (m_sExportConfiguration == null)
                m_sExportConfiguration = new ConfigExportView();

            float boundsDrawableDistanceRatio = m_sExportConfiguration.GetBoundDrawableDistanceRatio();
            if (boundsDrawableDistanceRatio > 0.0f) m_sBoundDrawableDistanceRatio = boundsDrawableDistanceRatio;

            int drawableCountLimit = m_sExportConfiguration.GetDrawableCountLimit();
            if (drawableCountLimit > 0) m_sDrawableCountLimit = drawableCountLimit;

            int txdCountLimit = m_sExportConfiguration.GetTxdCountLimit();
            if (txdCountLimit > 0) m_sTxdCountLimit = txdCountLimit;
        }
         
        private DrawableStats ParseGeometryStat(XmlTextReader reader)
        {
            DrawableStats statistic = new DrawableStats("", this.Scene);
            try
            {
                string name = reader.GetAttribute("name");
                statistic.Name = name != null ? name : string.Empty;
                string guid = reader.GetAttribute("guid");
                statistic.Guid = guid != null ? new Guid(guid) : Guid.Empty;
                string size = reader.GetAttribute("size");
                statistic.Size = size != null ? Convert.ToUInt32(size) : 0;
                string polycount = reader.GetAttribute("polycount");
                statistic.PolyCount = polycount != null ? Convert.ToUInt32(polycount) : 0;
                string polydensity = reader.GetAttribute("polydensity");
                if (polydensity != null)
                {
                    float result = 0.0f;
                    float.TryParse(polydensity, out result);
                    statistic.PolyDensity = result;
                }
                else
                {
                    statistic.PolyDensity = 0.0f;
                }
            }
            catch (System.OverflowException ex)
            {
                Log.Log__ErrorCtx(reader.BaseURI, "Overflow error in scenexml stats parsing: {0}", ex.Message);
            }
            finally
            {
                reader.Skip();
            }
            return statistic;
        }

        private TxdStats ParseTxdStat(XmlTextReader reader)
        {
            TxdStats statistic = new TxdStats("", this.Scene);
            try
            {
                string name = reader.GetAttribute("name");
                statistic.Name = name != null ? name.ToLower() : string.Empty;
                string guid = reader.GetAttribute("guid");
                statistic.Guid = guid != null ? new Guid(guid) : Guid.Empty;
                string size = reader.GetAttribute("size");
                statistic.Size = size != null ? Convert.ToUInt32(size) : 0;
            }
            catch (System.OverflowException ex)
            {
                Log.Log__ErrorCtx(reader.BaseURI, "Overflow error in scenexml stats parsing: {0}", ex.Message);
            }
            finally
            {
                reader.Skip();
            }
            return statistic;
        }

        private void ParseStatistics(XmlTextReader reader)
        {
            if (reader.IsEmptyElement)
            {
                reader.ReadStartElement();
            }
            else
            {
                reader.ReadStartElement();
                while (reader.MoveToContent() == XmlNodeType.Element)
                {
                    if (reader.Name == "geos" || reader.Name == "geometries")
                    {
                        if (reader.IsEmptyElement)
                        {
                            reader.ReadStartElement();
                        }
                        else
                        {
                            reader.ReadStartElement();
                            while (reader.MoveToContent() == XmlNodeType.Element)
                            {
                                if (reader.Name == "geo" || reader.Name == "geometry")
                                {
                                    DrawableStats stat = ParseGeometryStat(reader);
                                    AddStatIfDrawable(stat, this.Scene, this.GeometryStats);
                                }
                                else
                                {
                                    reader.Skip();
                                }
                            }
                            if (reader.NodeType == XmlNodeType.Text)
                                reader.ReadString();
                            if (reader.NodeType == XmlNodeType.EndElement)
                                reader.ReadEndElement();
                        }
                    }
                    else if (reader.Name == "collision_geometry")
                    {
                        if (reader.IsEmptyElement)
                        {
                            reader.ReadStartElement();
                        }
                        else
                        {
                            reader.ReadStartElement();
                            while (reader.MoveToContent() == XmlNodeType.Element)
                            {
                                if (reader.Name == "geometry")
                                {
                                    DrawableStats stat = ParseGeometryStat(reader);
                                    AddStatIfDrawable(stat, this.Scene, this.GeometryStats);
                                }
                                else
                                {
                                    reader.Skip();
                                }
                            }
                            if (reader.NodeType == XmlNodeType.Text)
                                reader.ReadString();
                            if (reader.NodeType == XmlNodeType.EndElement)
                                reader.ReadEndElement();
                        }
                    }
                    else if (reader.Name == "txds")
                    {
                        if (reader.IsEmptyElement)
                        {
                            reader.ReadStartElement();
                        }
                        else
                        {
                            reader.ReadStartElement();
                            while (reader.MoveToContent() == XmlNodeType.Element)
                            {
                                if (reader.Name== "txd")
                                {
                                    TxdStats stat = ParseTxdStat(reader);
                                    if (!this.TxdStats.ContainsKey(stat.Name))
                                    {
                                        this.TxdStats.Add(stat.Name, stat);
                                    }
                                }
                                else
                                {
                                    reader.Skip();
                                }
                            }
                            if (reader.NodeType == XmlNodeType.Text)
                                reader.ReadString();
                            if (reader.NodeType == XmlNodeType.EndElement)
                                reader.ReadEndElement();
                        }
                    }
                    else
                    {
                        reader.Skip();
                    }
                }
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }

        private void ParseStats(XPathNavigator navigator)
        {
            try
            {
                this.ValidStatistics = true;
                XPathNodeIterator geoemtryDefIt = null;
                geoemtryDefIt = navigator.Select(XPathExpression.Compile("/scene/stats/geos/geo"));
                if (geoemtryDefIt.Count == 0)
                {
                    geoemtryDefIt = navigator.Select(XPathExpression.Compile("/scene/stats/geometries/geometry"));
                }
                if (geoemtryDefIt.Count == 0)
                {
                    this.ValidStatistics = false;
                }

                while (geoemtryDefIt.MoveNext())
                {
                    XPathNavigator geometryNavigator = geoemtryDefIt.Current;
                    XPathNodeIterator attributeIt = geometryNavigator.Select(XPathExpression.Compile("@*"));

                    DrawableStats stats = new DrawableStats("", this.Scene);
                    while (attributeIt.MoveNext())
                    {
                        XPathNavigator attributeNavigator = attributeIt.Current;
                        if (typeof(String) != attributeNavigator.ValueType)
                            continue;

                        String value = (attributeNavigator.TypedValue as String);

                        try
                        {
                            if ("name" == attributeNavigator.Name)
                            {
                                stats.Name = value;
                            }
                            else if ("guid" == attributeNavigator.Name)
                            {
                                stats.Guid = new Guid(value);
                            }
                            else if ("size" == attributeNavigator.Name)
                            {
                                stats.Size = Convert.ToUInt32(value);
                            }
                            else if ("polycount" == attributeNavigator.Name)
                            {
                                stats.PolyCount = Convert.ToUInt32(value);
                            }
                            else if ("polydensity" == attributeNavigator.Name)
                            {
                                float result = 0.0f;
                                float.TryParse(value, out result);
                                stats.PolyDensity = result;
                            }
                        }
                        catch(System.OverflowException ex)
                        {
                            Log.Log__ErrorCtx(navigator.BaseURI, "Overflow error in scenexml stats parsing: {0}", ex.Message);
                        }
                    }

                    // Remove collision or container LOD ref objects so that we only have drawable objects in this stats dictionary
                    AddStatIfDrawable(stats, this.Scene, this.GeometryStats);
                }
                
                XPathNodeIterator collisionDefIt = navigator.Select(XPathExpression.Compile("/scene/stats/collision_geometry/geometry"));
                if (collisionDefIt.Count == 0)
                {
                    this.ValidStatistics = false;
                }
                while (collisionDefIt.MoveNext())
                {
                    XPathNavigator collisionNavigator = collisionDefIt.Current;
                    XPathNodeIterator attributeIt = collisionNavigator.Select(XPathExpression.Compile("@*"));

                    DrawableStats stats = new DrawableStats("", this.Scene);
                    while (attributeIt.MoveNext())
                    {
                        XPathNavigator attributeNavigator = attributeIt.Current;
                        if (typeof(String) != attributeNavigator.ValueType)
                            continue;

                        String value = (attributeNavigator.TypedValue as String);

                        try
                        {
                            if ("name" == attributeNavigator.Name)
                            {
                                stats.Name = value;
                            }
                            else if ("guid" == attributeNavigator.Name)
                            {
                                stats.Guid = new Guid(value);
                            }
                            else if ("size" == attributeNavigator.Name)
                            {
                                stats.Size = Convert.ToUInt32(value);
                            }
                            else if ("polycount" == attributeNavigator.Name)
                            {
                                stats.PolyCount = Convert.ToUInt32(value);
                            }
                            else if ("polydensity" == attributeNavigator.Name)
                            {
                                float result = 0.0f;
                                float.TryParse(value, out result);
                                stats.PolyDensity = result;
                            }
                        }
                        catch (System.OverflowException ex)
                        {
                            Log.Log__ErrorCtx(navigator.BaseURI, "Overflow error in scenexml stats parsing: {0}", ex.Message);
                        }
                    }

                    // Remove collision objects so that we only have drawable objects in this stats dictionary
                    TargetObjectDef sceneObject = this.Scene.FindObject(stats.Guid);
                    if (sceneObject != null)
                    {
                        if (!this.CollisionStats.ContainsKey(stats.Guid))
                        {
                            this.CollisionStats.Add(stats.Guid, stats);
                        }
                    }
                }

                XPathNodeIterator txdIt = navigator.Select(XPathExpression.Compile("/scene/stats/txds/txd"));
                if (txdIt.Count == 0)
                {
                    this.ValidStatistics = false;
                }
                while (txdIt.MoveNext())
                {
                    XPathNavigator objectsNavigator = txdIt.Current;
                    XPathNodeIterator attributeIt = objectsNavigator.Select(XPathExpression.Compile("@*"));

                    TxdStats stats = new TxdStats("", this.Scene);
                    while (attributeIt.MoveNext())
                    {
                        XPathNavigator attributeNavigator = attributeIt.Current;
                        if (typeof(String) != attributeNavigator.ValueType)
                            continue;

                        String value = (attributeNavigator.TypedValue as String);

                        if ("name" == attributeNavigator.Name)
                        {
                            stats.Name = value;
                        }
                        else if ("size" == attributeNavigator.Name)
                        {
                            stats.Size = Convert.ToUInt32(value);
                        }
                        else if ("guid" == attributeNavigator.Name)
                        {
                            stats.Guid = new Guid(value);
                        }

                    }
                    if (!this.TxdStats.ContainsKey(stats.Name))
                    {
                        this.TxdStats.Add(stats.Name, stats);
                    }
                }

                if (this.TxdStats.Count > 0 || this.GeometryStats.Count > 0 || this.CollisionStats.Count > 0)
                {
                    this.ValidStatistics = true;
                }
            }
            catch (Exception ex)
            {
                Log.Log__Exception(ex, "Unhandled exception parsing XML stats.");
            }
        }

        private void ParseStatsForExport(XPathNavigator navigator)
        {
            try
            {
                XPathNodeIterator geoemtryDefIt = null;
                geoemtryDefIt = navigator.Select(XPathExpression.Compile("/scene/stats/geos/geo"));
                if (geoemtryDefIt.Count == 0)
                {
                    geoemtryDefIt = navigator.Select(XPathExpression.Compile("/scene/stats/geometries/geometry"));
                }

                while (geoemtryDefIt.MoveNext())
                {
                    XPathNavigator geometryNavigator = geoemtryDefIt.Current;
                    XPathNodeIterator attributeIt = geometryNavigator.Select(XPathExpression.Compile("@*"));

                    DrawableStats stats = new DrawableStats("", this.Scene);
                    while (attributeIt.MoveNext())
                    {
                        XPathNavigator attributeNavigator = attributeIt.Current;
                        if (typeof(String) != attributeNavigator.ValueType)
                            continue;

                        String value = (attributeNavigator.TypedValue as String);

                        try
                        {
                            if ("name" == attributeNavigator.Name)
                            {
                                stats.Name = value;
                            }
                            else if ("guid" == attributeNavigator.Name)
                            {
                                stats.Guid = new Guid(value);
                            }
                            else if ("size" == attributeNavigator.Name)
                            {
                                stats.Size = Convert.ToUInt32(value);
                            }
                            else if ("polycount" == attributeNavigator.Name)
                            {
                                stats.PolyCount = Convert.ToUInt32(value);
                            }
                            else if ("polydensity" == attributeNavigator.Name)
                            {
                                float result = 0.0f;
                                float.TryParse(value, out result);
                                stats.PolyDensity = result;
                            }
                        }
                        catch (System.OverflowException ex)
                        {
                            Log.Log__ErrorCtx(navigator.BaseURI, "Overflow error in scenexml stats parsing: {0}", ex.Message);
                        }
                    }

                    // Remove collision or container LOD ref objects so that we only have drawable objects in this stats dictionary
                    AddStatIfDrawable(stats, this.Scene, this.GeometryStats);
                }

                XPathNodeIterator collisionDefIt = navigator.Select(XPathExpression.Compile("/scene/stats/collision_geometry/geometry"));

                while (collisionDefIt.MoveNext())
                {
                    XPathNavigator collisionNavigator = collisionDefIt.Current;
                    XPathNodeIterator attributeIt = collisionNavigator.Select(XPathExpression.Compile("@*"));

                    DrawableStats stats = new DrawableStats("", this.Scene);
                    while (attributeIt.MoveNext())
                    {
                        XPathNavigator attributeNavigator = attributeIt.Current;
                        if (typeof(String) != attributeNavigator.ValueType)
                            continue;

                        String value = (attributeNavigator.TypedValue as String);

                        if ("name" == attributeNavigator.Name)
                        {
                            stats.Name = value;
                        }
                        else if ("guid" == attributeNavigator.Name)
                        {
                            stats.Guid = new Guid(value);
                        }
                        else if ("size" == attributeNavigator.Name)
                        {
                            stats.Size = Convert.ToUInt32(value);
                        }
                        else if ("polycount" == attributeNavigator.Name)
                        {
                            stats.PolyCount = Convert.ToUInt32(value);
                        }
                        else if ("polydensity" == attributeNavigator.Name)
                        {
                            float result = 0.0f;
                            float.TryParse(value, out result);
                            stats.PolyDensity = result;
                        }
                    }

                    TargetObjectDef sceneObject = this.Scene.FindObject(stats.Guid);
                    if (sceneObject != null)
                    {
                        if (!this.CollisionStats.ContainsKey(stats.Guid))
                        {
                            this.CollisionStats.Add(stats.Guid, stats);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Log__Exception(ex, "Unhandled exception parsing stats.");
            }
        }

        /// <summary>
        /// Loads the scene statistics from the given scene xml file location
        /// </summary>
        /// <param name="scene"></param>
        private void LoadForExport(String filename)
        {
            XmlDocument document = new XmlDocument();
            try
            {
                document.Load(filename);
            }
            catch (XmlException ex)
            {
                Log.Log__Exception(ex, "XML parse exception");
            }

            this.ParseStatsForExport(document.CreateNavigator());
        }

        /// <summary>
        /// Loads the scene statistics from the given stream reader,
        /// used when the model map project is loading the scene data.
        /// </summary>
        /// <param name="stream"></param>
        private void Load(XmlDocument document)
        {
            this.ParseStats(document.CreateNavigator());
        }

        /// <summary>
        /// Loads the scene statistics from the given stream reader,
        /// used when the model map project is loading the scene data.
        /// </summary>
        /// <param name="stream"></param>
        private void Load(XmlTextReader reader)
        {
            this.ParseStatistics(reader);
        }

        /// <summary>
        /// Generates the statistics using the file stream and
        /// file sizes.
        /// </summary>
        /// <param name="scene"></param>
        private void Generate(String filename, String mapStream)
        {
            //stats generation part
            String rootpath = mapStream;

            foreach (TargetObjectDef obj in this.Scene.Walk(Scene.WalkMode.DepthFirst))
            {
                if (obj.IsObject() && !obj.IsMiloTri() && !obj.IsContainerLODRefObject())
                {
                    ObjectTypes type = ObjectTypes.Unknown;
                    if (obj.IsInternalRef() || obj.IsRefObject() || obj.IsXRef() || obj.IsRefInternalObject())
                    {
                        type = ObjectTypes.Reference;
                    }
                    else
                    {
                        type = ObjectTypes.Object;
                    }

                    if (!obj.DontExport())
                    {
                        if (this.GeometryStats.ContainsKey(obj.Guid) == true)
                        {
                            this.GeometryStats[obj.Guid].Type = type;
                        }

                        String idrpath = String.Format("{0}\\{1}.idr.zip", rootpath, obj.Name);
                        if (obj.IsFragment())
                        {
                            idrpath = String.Format("{0}\\{1}.ift.zip", rootpath, obj.Name);
                        }

                        System.IO.FileInfo fi = new System.IO.FileInfo(idrpath);
                        if (fi.Exists)
                        {
                            if (this.GeometryStats.ContainsKey(obj.Guid) == false)
                            {
                                DrawableStats stats = new DrawableStats(obj.Name, this.Scene);
                                stats.Type = type;
                                this.GeometryStats.Add(obj.Guid, stats);
                            }

                            this.GeometryStats[obj.Guid].Size = (uint)fi.Length;
                        }
                        else if (obj.HasLODChildren())
                        {
                            String directory = rootpath + "\\" + obj.Name;
                            if (Directory.Exists(directory))
                            {
                                String[] files = Directory.GetFiles(directory, "*.*", SearchOption.TopDirectoryOnly);

                                uint size = 0;
                                foreach (String file in files)
                                {
                                    fi = new System.IO.FileInfo(file);
                                    if (fi.Exists)
                                    {
                                        size += (uint)fi.Length;
                                    }
                                }

                                if (this.GeometryStats.ContainsKey(obj.Guid) == false)
                                {
                                    DrawableStats stats = new DrawableStats(obj.Name, this.Scene);
                                    this.GeometryStats.Add(obj.Guid, stats);
                                }

                                this.GeometryStats[obj.Guid].Size = size;
                            }
                        }

                        if (obj.Attributes.ContainsKey(AttrNames.OBJ_TXD))
                        {
                            String txdName = obj.GetAttribute(AttrNames.OBJ_TXD, AttrDefaults.OBJ_TXD);
                            txdName = txdName.ToLower();
                            String txdpath = String.Format("{0}\\{1}.itd.zip", rootpath, txdName);
                            if (this.TxdStats.ContainsKey(txdName) == true)
                            {
                                this.TxdStats[txdName].Usage++;

                                if (0 == this.TxdStats[txdName].Size)
                                {
                                    fi = new System.IO.FileInfo(txdpath);
                                    if (fi.Exists)
                                        this.TxdStats[txdName].Size = (uint)fi.Length;
                                }
                            }
                            else
                            {
                                fi = new System.IO.FileInfo(txdpath);
                                int length = 0;

                                if (fi.Exists)
                                {
                                    length = (int)fi.Length;
                                }

                                TxdStats txdstats = new TxdStats(txdName, this.Scene);
                                txdstats.Usage = 1;
                                txdstats.Size = (uint)length;
                                this.TxdStats.Add(txdName, txdstats);
                            }
                        }
                    }
                    else
                    {
                        if (this.GeometryStats.ContainsKey(obj.Guid) == false)
                        {
                            DrawableStats stats = new DrawableStats(obj.Name, this.Scene);
                            this.GeometryStats.Add(obj.Guid, stats);
                        }

                        this.GeometryStats[obj.Guid].Size = 0;
                    }
                }
                else if (obj.IsCollision())
                {
                    if (this.CollisionStats.ContainsKey(obj.Guid) == false)
                    {
                        DrawableStats stats = new DrawableStats(obj.Name, this.Scene);
                        this.CollisionStats.Add(obj.Guid, stats);
                    }
                }
                else if (obj.IsMiloTri())
                {
                    if (this.GeometryStats.ContainsKey(obj.Guid) == false)
                    {
                        DrawableStats stats = new DrawableStats(obj.Name, this.Scene);
                        stats.Type = ObjectTypes.InteriorReference;
                        this.GeometryStats.Add(obj.Guid, stats);
                    }
                    else
                    {
                        this.GeometryStats[obj.Guid].Type = ObjectTypes.InteriorReference;
                    }
                }
            }
        }

        /// <summary>
        /// Sets a attribute called the given name on the given node with
        /// the given value
        /// </summary>
        /// <param name="node"></param>
        /// <param name="name"></param>
        /// <param name="value"></param>
        private static void SetAttribute(ref XmlNode node, String name, Object value)
        {
            XmlNode attr = s_document.CreateAttribute(name);
            attr.Value = String.Format("{0}", value);
            node.Attributes.SetNamedItem(attr);
        }

        /// <summary>
        /// Adds the stat if it's not collision or a container LOD ref object
        /// </summary>
        /// <param name="stat"></param>
        /// <param name="scene"></param>
        /// <param name="geometryStats"></param>
        private static void AddStatIfDrawable(DrawableStats stat, Scene scene, Dictionary<Guid, DrawableStats> geometryStats)
        {
            TargetObjectDef sceneObject = scene.FindObject(stat.Guid);
            if (sceneObject != null && !sceneObject.IsCollision() && !sceneObject.IsContainerLODRefObject())
            {
                if (!geometryStats.ContainsKey(stat.Guid))
                {
                    geometryStats.Add(stat.Guid, stat);
                }
            }
        }

        /// <summary>
        /// Makes a value that is in bytes look pretty by determining
        /// what type it should be outputted to based on the size
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        private String PrettyByteText(int bytes)
        {
            if (bytes < 1024)
            {
                return String.Format("{0} byte(s)", bytes);
            }
            else if (bytes < 1024 * 1024)
            {
                return String.Format("{0} Kbyte(s)", bytes >> 10);
            }
            else
            {
                int mbytes = bytes >> 20;
                int mbyte = 1024 * 1024;
                int leftover = bytes - (mbytes * mbyte);

                if (leftover > 0)
                {
                    return String.Format("{0} Mbyte(s)", ((double)bytes / (double)mbyte).ToString("F1"));
                }

                return String.Format("{0} Mbyte(s)", bytes >> 20);
            }
        }

        #endregion // Private Function(s)

        #region Public Function(s)

        /// <summary>
        /// Used to create all of the statistics for a scene
        /// either by parsing the scene xml or generating 
        /// them from the stream
        /// </summary>
        public void CreateStatistics(String filename, String mapStream)
        {
            this.LoadForExport(filename);
            this.Generate(filename, mapStream);
        }

        /// <summary>
        /// Used to create all of the statistics for a scene
        /// either by parsing the scene xml or generating 
        /// them from the stream
        /// </summary>
        public void LoadStatistics(XmlDocument document)
        {
            this.Load(document);
        }

        /// <summary>
        /// Used to create all of the statistics for a scene
        /// either by parsing the scene xml or generating 
        /// them from the stream
        /// </summary>
        public void LoadStatistics(XmlTextReader reader)
        {
            this.Load(reader);
        }

        /// <summary>
        /// Saves the scene xml file back out after the statistics have 
        /// been generated for it
        /// </summary>
        public void Save(String filename)
        {
            //open the scene xml
            s_document = new XmlDocument();
            s_document.Load(filename);

            XmlNode root = s_document.DocumentElement;

            //add a node if it doesn't exist
            XmlNode scene = root.SelectSingleNode("/scene");

            if (scene == null)
                return;

            XmlNode node_stats = scene.SelectSingleNode("stats");

            if (node_stats == null)
            {
                node_stats = s_document.CreateElement("stats");
                scene.AppendChild(node_stats);
            }

            //geometry information

            XmlNode drawables = node_stats.SelectSingleNode("geometries");

            if (drawables == null)
            {
                drawables = s_document.CreateElement("geometries");
                node_stats.AppendChild(drawables);
            }

            drawables.RemoveAll();

            foreach (KeyValuePair<Guid, DrawableStats> stats in this.GeometryStats)
            {
                XmlNode drawable = s_document.CreateElement("geometry");

                SetAttribute(ref drawable, "guid", stats.Key.ToString().ToUpper());
                SetAttribute(ref drawable, "name", stats.Value.Name);
                SetAttribute(ref drawable, "polycount", stats.Value.PolyCount);
                SetAttribute(ref drawable, "polydensity", stats.Value.PolyDensity);
                SetAttribute(ref drawable, "size", stats.Value.Size);

                drawables.AppendChild(drawable);
            }
            
            // collision information

            XmlNode collisionGeoemtries = node_stats.SelectSingleNode("collision_geometry");

            if (collisionGeoemtries == null)
            {
                collisionGeoemtries = s_document.CreateElement("collision_geometry");
                node_stats.AppendChild(collisionGeoemtries);
            }

            collisionGeoemtries.RemoveAll();

            foreach (KeyValuePair<Guid, DrawableStats> stats in this.CollisionStats)
            {
                XmlNode drawable = s_document.CreateElement("geometry");

                SetAttribute(ref drawable, "guid", stats.Key.ToString().ToUpper());
                SetAttribute(ref drawable, "name", stats.Value.Name);
                SetAttribute(ref drawable, "polycount", stats.Value.PolyCount);
                SetAttribute(ref drawable, "polydensity", stats.Value.PolyDensity);

                collisionGeoemtries.AppendChild(drawable);
            }

            //txd information

            XmlNode txds = node_stats.SelectSingleNode("txds");

            if (txds == null)
            {
                txds = s_document.CreateElement("txds");
                node_stats.AppendChild(txds);
            }

            txds.RemoveAll();

            foreach (TxdStats stats in this.TxdStats.Values)
            {
                XmlNode txd = s_document.CreateElement("txd");

                String guid = Guid.NewGuid().ToString().ToUpper();
                SetAttribute(ref txd, "guid", guid);
                SetAttribute(ref txd, "name", stats.Name);
                SetAttribute(ref txd, "size", stats.Size);

                txds.AppendChild(txd);
            }

            //write back out again
            s_document.Save(filename);
        }

        /// <summary>
        /// Exports the html report that lists drawable and texture limits
        /// and whether the map file exceeds them or not.
        /// </summary>
        public void ExportReport(String rootpath)
        {
            String allReportDir =  Directory.GetParent(rootpath).FullName;
            String reportFilename = Path.Combine(allReportDir, this.Name) + ".html";

            long totalBytesTxd = this.TxdStats.Values.Sum(txdStat => txdStat.Size);
            long totalBytesDrawables = this.GeometryStats.Values.Sum(geoStat => geoStat.Size);
            long totalBytesCollision = 0;
            long totalBytes = totalBytesTxd + totalBytesDrawables;

            if (Directory.Exists(rootpath))
            {
                String[] ibn_files = Directory.GetFiles(rootpath, "*.ibn.zip", SearchOption.TopDirectoryOnly);
                foreach (String ibn_file in ibn_files)
                {
                    if (!File.Exists(ibn_file))
                        continue;
                    FileInfo fi = new FileInfo(ibn_file);
                    totalBytesCollision += fi.Length;
                }
            }
            else
            {
                Log.Log__Warning("Map root export path {0} does not exist.  Statistics report will be incorrect.",
                    rootpath);
            }

            int drawableCount = this.GeometryStats.Values.Count(geoStat => geoStat.Type == ObjectTypes.Object);
            long drawablePolyCount = this.GeometryStats.Values.Sum(geoStat => geoStat.PolyCount);
            int referenceCount = this.GeometryStats.Values.Count(geoStat => geoStat.Type == ObjectTypes.Reference);
            int interiorReferenceCount = this.GeometryStats.Values.Count(geoStat => geoStat.Type == ObjectTypes.InteriorReference);
            int collisionCount = this.CollisionStats.Values.Count;
            long collisionPolyCount = this.CollisionStats.Values.Sum(collStat => collStat.PolyCount);
            Dictionary<TargetObjectDef, float> boundDrawableDistanceDict = new Dictionary<TargetObjectDef, float>();

            // Drawable Limits
            LimitState drawlimit = LimitState.Fine;
            foreach (KeyValuePair<Guid, DrawableStats> keyvalue in this.GeometryStats)
            {
                if ((keyvalue.Value.Size > m_sDrawableByteLimit) && (drawlimit == LimitState.Fine))
                {
                    drawlimit = LimitState.Over;
                }
                else if (keyvalue.Value.Size > m_sDrawableByteMaxLimit)
                {
                    drawlimit = LimitState.Maxed;
                }

                //Compute the drawable-bound ratio.
                TargetObjectDef sceneObject = this.Scene.FindObject(keyvalue.Key);
                float lodDistance = sceneObject.GetAttribute(AttrNames.OBJ_LOD_DISTANCE, AttrDefaults.OBJ_LOD_DISTANCE);
                Base.Math.Vector3f distanceVector = sceneObject.LocalBoundingBox.Max - sceneObject.LocalBoundingBox.Centre();
                float distance = (float) distanceVector.Magnitude();
                float ratio = lodDistance / distance;
                if ( ratio > m_sBoundDrawableDistanceRatio )
                {
                    if (boundDrawableDistanceDict.ContainsKey(sceneObject) == false)
                    {
                        float estimatedLODDistance = (m_sBoundDrawableDistanceRatio * distance);
                        boundDrawableDistanceDict.Add(sceneObject, estimatedLODDistance);
                    }
                }
            }

            // TXD Limits
            LimitState txdlimit = LimitState.Fine;
            foreach (KeyValuePair<String, TxdStats> keyvalue in this.TxdStats)
            {
                if ((keyvalue.Value.Size > m_sTxdByteLimit) && (drawlimit == LimitState.Fine))
                {
                    drawlimit = LimitState.Over;
                }
                else if (keyvalue.Value.Size > m_sTxdByteMaxLimit)
                {
                    drawlimit = LimitState.Maxed;
                }
            }
            
            // Use HtmlTextWriter to render out our HTML document.
            using (StreamWriter reportFile = new StreamWriter(reportFilename))
            {
                String toolsroot = Environment.GetEnvironmentVariable("RS_TOOLSROOT");
                HtmlTextWriter writer = new HtmlTextWriter(reportFile);

                writer.RenderBeginTag(HtmlTextWriterTag.Html);
                writer.RenderBeginTag(HtmlTextWriterTag.Head);
                writer.RenderBeginTag(HtmlTextWriterTag.Title);
                writer.Write(String.Format("Rockstar Map Export Report: {0}", 
                    Path.GetFileNameWithoutExtension(this.Scene.Filename)));
                writer.RenderEndTag();
                writer.AddAttribute(HtmlTextWriterAttribute.Type, "text/css");
                writer.RenderBeginTag(HtmlTextWriterTag.Style);
                writer.AddStyleAttribute(HtmlTextWriterStyle.BackgroundColor, "lightgray");
                writer.AddStyleAttribute(HtmlTextWriterStyle.BackgroundImage, 
                    String.Format("url('file://{0}/dcc/current/max2012/UI/RsImages/Rstar_logo_bw_keyline.png') no-repeat fixed;",toolsroot));
                writer.AddStyleAttribute(HtmlTextWriterStyle.FontFamily, "'Helvetica', sans-serif");
                writer.RenderEndTag();
                writer.RenderEndTag();
                writer.RenderBeginTag(HtmlTextWriterTag.Body);
                
                // Summary
                String drawableSummary = String.Format("{0} drawable(s), {1} polys using {2}", drawableCount, drawablePolyCount, PrettyByteText((int)totalBytesDrawables));
                String txdSummary = String.Format("{0} txd(s) using {1}", this.TxdStats.Count, PrettyByteText((int)totalBytesTxd));
                String collisionSummary = String.Format("{0} collision meshes/primitives, {1} polys using {2}", collisionCount, collisionPolyCount, PrettyByteText((int)totalBytesCollision));
                String referenceSummary = String.Format("{0} reference(s)", referenceCount);
                String interiorReferenceSummary = String.Format("{0} interior reference(s)", interiorReferenceCount);
                String totalSummary = String.Format("txds and drawables using {0}", PrettyByteText((int)totalBytes));

                writer.RenderBeginTag(HtmlTextWriterTag.H1);
                writer.Write(String.Format("Export summary for {0}", this.Name));
                writer.RenderEndTag(); // H1
                writer.RenderBeginTag(HtmlTextWriterTag.Ul);
                AddSummaryListEntry(writer, drawableSummary);
                AddSummaryListEntry(writer, txdSummary);
                AddSummaryListEntry(writer, collisionSummary);
                AddSummaryListEntry(writer, referenceSummary);
                AddSummaryListEntry(writer, interiorReferenceSummary);
                AddSummaryListEntry(writer, totalSummary);
                writer.RenderEndTag(); // Ul

                // Advice
                writer.RenderBeginTag(HtmlTextWriterTag.H1);
                writer.Write("Advice");
                writer.RenderEndTag(); // H1
                writer.RenderBeginTag(HtmlTextWriterTag.P);
                writer.Write("Any help that can be given to improve the container will be listed here.");
                writer.RenderEndTag(); // P
                writer.RenderBeginTag(HtmlTextWriterTag.Ul);
                if (totalBytes > m_sByteLimit)
                    AddSummaryListEntry(writer, String.Format("Your block is using {0} for your models and txds (discounting xrefs). An average to aim for would be closer to {1}", PrettyByteText((int)totalBytes), PrettyByteText((int)m_sByteLimit)));
                if (totalBytes > m_sByteMaxLimit)
                    AddSummaryListEntry(writer, String.Format("You REALLY need to fix the memory usage before your next checkin or you'll be killing the streaming in the game", PrettyByteText((int)totalBytes), PrettyByteText((int)m_sByteLimit)));
                if (drawableCount > m_sDrawableCountLimit)
                    AddSummaryListEntry(writer, String.Format("You have {0} drawable objects when it should be below {1}", drawableCount, m_sDrawableCountLimit));
                if (this.TxdStats.Count > m_sTxdCountLimit)
                    AddSummaryListEntry(writer, String.Format("You have {0} txds when it should be below {1}", this.TxdStats.Count, m_sTxdCountLimit));

                if (drawlimit != LimitState.Fine)
                {
                    String advice = String.Format("You have the following exported drawables that are over {0} this will REALLY hurt the memory and streaming, please fix them:", PrettyByteText(m_sDrawableByteLimit));
                    AddSummaryListEntry(writer, advice);

                    if (drawlimit == LimitState.Maxed)
                    {
                        advice = String.Format("(NO REALLY SOME ARE > {0} FIX THEM NOW BEFORE CHECKIN)", PrettyByteText(m_sDrawableByteMaxLimit));
                        AddSummaryListEntry(writer, advice);
                    }
                }

                foreach (KeyValuePair<Guid, DrawableStats> keyvalue in this.GeometryStats)
                {
                    if (keyvalue.Value.Size > m_sDrawableByteLimit)
                    {
                        String advice = String.Format("{0} is {1}", keyvalue.Value.Name, PrettyByteText((int)keyvalue.Value.Size));
                        AddSummaryListEntry(writer, advice);
                    }
                }

                if (txdlimit != LimitState.Fine)
                {
                    String advice = String.Format("You have the following exported texture dictionaries that are over {0} this will REALLY hurt the memory and streaming, please fix them:", PrettyByteText(m_sTxdByteLimit));
                    AddSummaryListEntry(writer, advice);

                    if (txdlimit == LimitState.Maxed)
                    {
                        advice = String.Format("(NO REALLY SOME ARE > {0} FIX THEM NOW BEFORE CHECKIN)", PrettyByteText(m_sTxdByteMaxLimit));
                        AddSummaryListEntry(writer, advice);
                    }
                }

                foreach (KeyValuePair<String, TxdStats> keyvalue in this.TxdStats)
                {
                    if (keyvalue.Value.Size > m_sTxdByteMaxLimit)
                    {
                        String advice = String.Format("{0} is {1}", keyvalue.Value.Name, PrettyByteText((int)keyvalue.Value.Size));
                        AddSummaryListEntry(writer, advice);
                    }
                }

                if (boundDrawableDistanceDict.Count > 0)
                {
                    String advice = String.Format("You have {0} objects whose LOD distances are too large for the size of the object.  You may want to lower these draw distances.", boundDrawableDistanceDict.Count);
                    AddSummaryListEntry(writer, advice);

                    foreach (KeyValuePair<TargetObjectDef, float> objectInfo in boundDrawableDistanceDict)
                    {
                        float rounded = (float) Math.Round( objectInfo.Value - 0.5f );
                        advice = String.Format("{0}'s LOD distance should be reduced to {1}.", objectInfo.Key.Name, rounded);
                        AddSummaryListEntry(writer, advice);
                    }
                }

                writer.RenderEndTag(); // Body
                writer.RenderEndTag(); // Html
            }
        }
        #endregion // Public Function(s)

        #region Private Methods
        #region HTML Report Helper Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="entry"></param>
        private void AddSummaryListEntry(HtmlTextWriter writer, String entry)
        {
            String toolsroot = Environment.GetEnvironmentVariable("RS_TOOLSROOT");
            writer.AddAttribute(HtmlTextWriterAttribute.Style,
                String.Format("list-style-image: url('file://{0}/dcc/current/max2012/UI/RsImages/StarBullet.png')", toolsroot));
            writer.RenderBeginTag(HtmlTextWriterTag.Li);
            writer.Write(entry);
            writer.RenderEndTag();
        }
        #endregion // HTML Report Helper Methods
        #endregion // Private Methods

    }
}
