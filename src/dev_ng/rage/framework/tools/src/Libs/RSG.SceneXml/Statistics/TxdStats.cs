﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.SceneXml;

namespace RSG.SceneXml.Statistics
{
    /// <summary>
    /// Texture Dictionary Statistics
    /// </summary>
    public class TxdStats : BaseStats
    {
        #region Properties

        /// <summary>
        /// The size of the texture dictionary in bytes
        /// </summary>
        public uint Size
        {
            get { return m_size; }
            set { m_size = value; }
        }
        private uint m_size;

        /// <summary>
        /// 
        /// </summary>
        public Guid Guid
        {
            get { return m_guid; }
            set { m_guid = value; }
        }
        private Guid m_guid;

        /// <summary>
        /// The number of objects that use this texture
        /// dictionary
        /// </summary>
        public int Usage
        {
            get { return m_usage; }
            set { m_usage = value; }
        }
        private int m_usage;

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// Default constructor, sets the name
        /// of the texture dictionary
        /// </summary>
        /// <param name="name"></param>
        public TxdStats(String name, Scene scene)
            : base(name, scene)
        {
            this.Guid = Guid.Empty;
        }

        #endregion // Constructor(s)
    }
}
