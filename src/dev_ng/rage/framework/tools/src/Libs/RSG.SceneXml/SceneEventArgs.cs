﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.SceneXml
{

    /// <summary>
    /// Event argument class; adding a Scene object.
    /// </summary>
    public class SceneEventArgs : EventArgs
    {
        #region Properties
        /// <summary>
        /// Associated Scene object.
        /// </summary>
        public Scene Scene
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="scene"></param>
        public SceneEventArgs(Scene scene)
        {
            this.Scene = scene;
        }
        #endregion // Constructor(s)
    }

} // RSG.SceneXml namespace
