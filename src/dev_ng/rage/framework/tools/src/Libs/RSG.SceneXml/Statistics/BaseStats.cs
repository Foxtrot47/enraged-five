﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.SceneXml;

namespace RSG.SceneXml.Statistics
{
    /// <summary>
    /// Base class for all of the separate statistics.
    /// </summary>
    public class BaseStats
    {
        #region Properties

        /// <summary>
        /// The name of the object that the stat is for.
        /// </summary>
        public String Name
        {
            get { return m_name; }
            set { m_name = value; }
        }
        private String m_name;

        /// <summary>
        /// The scene object that this stats object belongs to.
        /// </summary>
        public Scene Scene
        {
            get { return m_scene; }
            set { m_scene = value; }
        }
        private Scene m_scene;

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// Default constructor, sets the name property
        /// </summary>
        public BaseStats(String name, Scene scene)
        {
            this.Name = name.ToLower();
            this.Scene = scene;
        }

        #endregion Constructor(s)
    }
}
