﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.SceneXml
{

    /// <summary>
    /// Flags used to determine what gets loaded during a scene creation.
    /// </summary>
    [Flags]
    public enum LoadOptions
    {
        /// <summary>
        /// Nothing is loaded
        /// </summary>
        None = 0x00,

        /// <summary>
        /// Makes sure the objects are loaded.
        /// </summary>
        Objects = 0x02,

        /// <summary>
        /// The materials are loaded
        /// </summary>
        Materials = 0x04,

        /// <summary>
        /// The statistics are loaded
        /// </summary>
        Statistics = 0x08,

        /// <summary>
        /// Everything is loaded (this is the default).
        /// </summary>
        All = 0xFFFFFF,
    }

} // RSG.SceneXml namespace
