//
// File: LODHierarchyDef.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of LODHierarchyDef class
//

using RSG.Base.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Xml;
using System.Xml.XPath;

namespace RSG.SceneXml
{
    
    /// <summary>
    /// LOD Hierarchy definition for ObjectDef objects.
    /// </summary>
    public class LODHierarchyDef
    {
        #region Constants
        #region XPath Compiled Expressions
        private static readonly XPathExpression sC_s_xpath_XPathParent =
            XPathExpression.Compile("parent/@guid");
        private static readonly XPathExpression sC_s_xpath_XPathChild =
            XPathExpression.Compile("children/child/@guid");
        private static readonly XPathExpression sC_s_xpath_XPathSecondaryParent =
            XPathExpression.Compile("secondaryParent/@guid");
        private static readonly XPathExpression sC_s_xpath_XPathSecondaryChild =
            XPathExpression.Compile("secondaryChildren/child/@guid");
        #endregion // XPath Compiled Expressions
        #endregion // Constants

        #region Properties and Associated Member Data
        /// <summary>
        /// LOD Parent reference.
        /// </summary>
        public string Name
        {
            get { return m_Name; }
        }
        private string m_Name;

        /// <summary>
        /// LOD Parent reference.
        /// </summary>
        public TargetObjectDef Parent
        {
            get { return m_Parent; }
        }
        private TargetObjectDef m_Parent;

        /// <summary>
        /// LOD Children references.
        /// </summary>
        public TargetObjectDef[] Children
        {
            get { return m_aChildren; }
        }
        private TargetObjectDef[] m_aChildren;

        /// <summary>
        /// LOD sibling references (including self)
        /// </summary>
        public TargetObjectDef[] Siblings
        {
            get
            {
                if (Parent != null && Parent.LOD != null && Parent.LOD.Children != null)
                    return Parent.LOD.Children;

                return new TargetObjectDef[0];
            }
        }
        #endregion // Properties and Associated Member Data

        #region Member Data
        /// <summary>
        /// LOD Parent GUID.
        /// </summary>
        private Guid m_guidParent;

        /// <summary>
        /// LOD Children GUIDs.
        /// </summary>
        private Guid[] m_guidChildren;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="children"></param>
        public LODHierarchyDef(Guid parent, Guid[] children)
        {
            this.m_guidParent = parent;
            this.m_guidChildren = children;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="navigator"></param>
        public LODHierarchyDef(XPathNavigator navigator)
        {
            String channelName = navigator.GetAttribute("name","");
            if (null != channelName)
                m_Name = channelName;
            ParseParent(navigator);
            ParseChildren(navigator);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        public LODHierarchyDef(XmlTextReader reader)
        {
            this.m_guidChildren = new Guid[0];

            String channelName = reader.GetAttribute("name");
            if (null != channelName)
                m_Name = channelName;

            if (reader.IsEmptyElement)
                reader.ReadStartElement();
            else
            {
                reader.ReadStartElement();
                while (reader.MoveToContent() == XmlNodeType.Element)
                {
                    if (String.Equals(reader.Name, "parent"))
                    {
                        ParseParent(reader);
                    }
                    else if (String.Equals(reader.Name, "children"))
                    {
                        ParseChildren(reader);
                    }
                    else
                    {
                        reader.Skip();
                    }
                }
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Resolve all ObjectDef links from Scene.
        /// </summary>
        /// <param name="scene"></param>
        public void ResolveLinks(Scene scene)
        {
            if (this.m_guidParent != Guid.Empty)
                this.m_Parent = scene.FindObject(this.m_guidParent);

            this.m_aChildren = new TargetObjectDef[this.m_guidChildren.Length];
            int n = 0;
            foreach (Guid child in this.m_guidChildren)
            {
                if (child == Guid.Empty)
                    continue;
                this.m_aChildren[n] = scene.FindObject(child);
                ++n;
            }
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        private void ParseSkip(XmlTextReader reader)
        {
            if (reader.IsEmptyElement)
                reader.ReadStartElement();
            else
            {
                reader.ReadStartElement();
                while (reader.MoveToContent() == XmlNodeType.Element)
                {
                    ParseSkip(reader);
                }
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        private Guid ParseAttribute(XmlTextReader reader)
        {
            String value = reader.GetAttribute("guid");
            if (!String.IsNullOrWhiteSpace(value) && !String.IsNullOrEmpty(value))
            {
                return Guid.Parse(value);
            }
            return default(Guid);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        private void ParseParent(XmlTextReader reader)
        {
            Guid guid = ParseAttribute(reader);
            this.m_guidParent = guid;

            ParseSkip(reader);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="secondary"></param>
        private void ParseChildren(XmlTextReader reader)
        {
            if (reader.IsEmptyElement)
                reader.ReadStartElement();
            else
            {
                reader.ReadStartElement();
                List<Guid> children = new List<Guid>();
                while (reader.MoveToContent() == XmlNodeType.Element)
                {
                    if (String.Equals(reader.Name, "child"))
                    {
                        children.Add(ParseAttribute(reader));
                    }
                    ParseSkip(reader);
                }
                this.m_guidChildren = children.ToArray();

                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }

        /// <summary>
        /// Parse parent node.
        /// </summary>
        /// <param name="navigator"></param>
        private void ParseParent(XPathNavigator navigator)
        {
            XPathNodeIterator parentGuidIt = navigator.Select(sC_s_xpath_XPathParent);
            while (parentGuidIt.MoveNext())
            {
                if (typeof(String) != parentGuidIt.Current.ValueType)
                    continue;
                String value = (parentGuidIt.Current.TypedValue as String);

                this.m_guidParent = new Guid(value);
            }
        }

        /// <summary>
        /// Parse all children.
        /// </summary>
        /// <param name="navigator"></param>
        private void ParseChildren(XPathNavigator navigator)
        {
            XPathNodeIterator childGuidIt = navigator.Select(sC_s_xpath_XPathChild);
            this.m_guidChildren = new Guid[childGuidIt.Count];
            while (childGuidIt.MoveNext())
            {
                if (typeof(String) != childGuidIt.Current.ValueType)
                    continue;
                String value = (childGuidIt.Current.TypedValue as String);

                this.m_guidChildren[childGuidIt.CurrentPosition-1] = new Guid(value);
            }
        }
        #endregion // Private Methods
    }

} // RSG.SceneXml namespace

// LODHierarchyDef.cs
