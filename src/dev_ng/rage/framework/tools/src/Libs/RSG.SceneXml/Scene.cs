using System;
using System.Collections.Generic;
using System.Diagnostics;
using SIO = System.IO;
using System.Linq;
using System.Xml;
using System.Xml.XPath;
using RSG.Base.Attributes;
using RSG.Base.ConfigParser;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.SceneXml.Material;
using RSG.SceneXml.Statistics;
using RSG.Pipeline.Core;
using RSG.Pipeline.Content;
using RSG.Pipeline.Content.Algorithm;

namespace RSG.SceneXml
{

    /// <summary>
    /// Scene object containing object tree.
    /// </summary>
    public class Scene
    {
        #region Constants
        public static readonly String LOG_CTX = "Scene";
        public readonly float sC_s_f_Version = 1.30f;
        #region XPath Compiled Expressions
        private static readonly XPathExpression sC_s_xpath_XPathSceneAttrs =
            XPathExpression.Compile("/scene/@*");
        private static readonly XPathExpression sC_s_xpath_XPathObjectAttrs =
            XPathExpression.Compile("//object/@*");
        private static readonly XPathExpression sC_s_xpath_XPathRootObjectDefs = 
            XPathExpression.Compile("/scene/objects/object");
        private static readonly XPathExpression sC_s_xpath_XPathRootMaterialsDefs =
            XPathExpression.Compile("/scene/materials/material");
        private static readonly XPathExpression sC_s_xpath_XPathExcludedObjects =
            XPathExpression.Compile("/scene/excluded_objects/excluded_object/@guid");
        #endregion // XPath Compiled Expressions
        #region Scene Root Attribute Names
        private static readonly String sC_s_s_SceneVersion = "version";
        private static readonly String sC_s_s_SceneFilename = "filename";
        private static readonly String sC_s_s_SceneTimestamp = "timestamp";
        private static readonly String sC_s_s_SceneUser = "user";
        #endregion // Scene Root Attribute Names
        #endregion // Constants

        #region Enumerations
        /// <summary>
        /// WalkMode describes how the scene graph is traversed using the Walk 
        /// method.
        /// </summary>
        public enum WalkMode
        {
            BreadthFirst,   //!< Visit all siblings before visiting children.
            DepthFirst      //!< Visit all children before siblings.
        };
        #endregion // Enumerations

        #region Properties and Associated Member Data
        /// <summary>
        /// RSG.SceneXml XML loaded filename (absolute path).
        /// </summary>
        public String Filename
        {
            get;
            private set;
        }

        /// <summary>
        /// RSG.SceneXml file version.
        /// </summary>
        public float Version
        {
            get;
            private set;
        }

        /// <summary>
        /// Exported filename (DCC package source file, absolute path).
        /// </summary>
        public String ExportFilename
        {
            get;
            private set;
        }

        /// <summary>
        /// Date and time of export from DCC package.
        /// </summary>
        public DateTime ExportTimestamp
        {
            get;
            private set;
        }

        /// <summary>
        /// Username who exported the RSG.SceneXml file from the DCC package.
        /// </summary>
        public String ExportUser
        {
            get;
            private set;
        }

        /// <summary>
        /// Array of root scene objects.
        /// </summary>
        public TargetObjectDef[] Objects
        {
            get { return objects_; }
            private set
            {
                objects_ = value;

                // build a name->object map for quicker lookups
                nameObjectMap_.Clear();
                foreach (TargetObjectDef objectDef in objects_)
                {
                    // Where objects have the same name, only the first is included here
                    String objectNameLower = objectDef.Name.ToLower();
                    if (!nameObjectMap_.ContainsKey(objectNameLower))
                        nameObjectMap_.Add(objectNameLower, objectDef);
                }
            }
        }
        private TargetObjectDef[] objects_;
        private Dictionary<String, TargetObjectDef> nameObjectMap_ = new Dictionary<String, TargetObjectDef>();// to optimise lookups

        /// <summary>
        /// The options used to load the scene xml file.
        /// </summary>
        public LoadOptions LoadOptions
        {
            get;
            private set;
        }

        /// <summary>
        /// Array of root scene materials.
        /// </summary>
        public Material.MaterialDef[] Materials
        {
            get;
            private set;
        }
        
        /// <summary>
        /// The statistics for this scene xml
        /// </summary>
        public Statistics.Stats Statistics
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public Guid[] ExcludedObjects
        {
            get;
            private set;
        }

        /// <summary>
        /// Scene map container type.
        /// </summary>
        [Obsolete("Use SceneType instead.")]
        public ContentNodeMap.MapSubType MapType
        {
            get;
            private set;
        }

        /// <summary>
        /// Scene type (more than just for environment data).
        /// </summary>
        public SceneType SceneType 
        { 
            get; 
            private set; 
        }

        #region Scene Unique Identifier Properties
        /// <summary>
        /// Unique Scene 16-bit identifier.
        /// </summary>
        public UInt16 Hash16
        {
            get;
            private set;
        }

        /// <summary>
        /// Unique Scene 32-bit identifier.
        /// </summary>
        public UInt32 Hash32
        {
            get;
            private set;
        }
        #endregion // Scene Unique Identifier Properties

        /// <summary>
        /// Log object.
        /// </summary>
        internal static IUniversalLog Log
        {
            get;
            private set;
        }

        /// <summary>
        /// DHM: Dunno what this is needed for; redundant.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [Obsolete]
        public delegate bool DeadEndFunc(TargetObjectDef obj);
        #endregion // Properties and Associated Member Data

        #region Private Data
        /// <summary>
        /// ObjectDef table, keyed by GUID to ObjectDef object.
        /// </summary>
        /// This cache dictionary is used for quick look ups to resolve 
        /// references between ObjectDef objects.  It is populated when
        /// ObjectDef's are added into our Scene (recursively).
        public Dictionary<Guid, TargetObjectDef> TableLookup
        {
            get { return m_TableLookup; }
        }
        private Dictionary<Guid, TargetObjectDef> m_TableLookup;

        public Dictionary<Guid, TargetObjectDef> AttrTableLookup
        {
            get { return m_AttrTableLookup; }
        }
        private Dictionary<Guid, TargetObjectDef> m_AttrTableLookup;

        /// <summary>
        /// MaterialDef table, keyed by GUID to MaterialDef object.
        /// </summary>
        /// This cache dictionary is used for quick look ups to resolve 
        /// references between MaterialDef objects.  It is populated when
        /// MaterialDef's are added into our Scene (recursively).
        public Dictionary<Guid, Material.MaterialDef> MaterialLookup
        {
            get { return m_MaterialTableLookup; }
        }
        private Dictionary<Guid, Material.MaterialDef> m_MaterialTableLookup;

        /// <summary>
        /// DrawableStat table, keyed by object GUID to DrawableStat object.
        /// </summary>
        public Dictionary<Guid, Statistics.DrawableStats> GeometryStatsLookup
        {
            get;
            private set;
        }
        
        /// <summary>
        /// TxdStats table, keyed by txd name to TxdStats object.
        /// </summary>
        public Dictionary<String, Statistics.TxdStats> TxdStatsLookup
        {
            get;
            private set;
        }

        /// <summary>
        /// Get all interior groups.  This is an optimisation to help with TargetObjectDef.IsPartOfAnEntitySet()
        /// </summary>
        public TargetObjectDef[] InteriorGroups
        {
            get
            {
                if (m_interiorGroups == null)
                    m_interiorGroups = this.Walk(Scene.WalkMode.BreadthFirst).Where(obj => obj.IsInteriorGroup()).ToArray();

                return m_interiorGroups;
            }
        }
        private TargetObjectDef[] m_interiorGroups;
  
        #endregion // Private Data

        #region Constructor(s)
        /// <summary>
        /// Constructor; general use Scene load.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="loadOptions"></param>
        /// <param name="useTextReaderMethod"></param>
        [Obsolete("Use AP3 IContentTree version instead. ** This doesn't have SceneType set!! **")]
        public Scene(String filename, LoadOptions loadOptions, bool useTextReaderMethod)
            : this(null, null, filename, loadOptions, useTextReaderMethod)
        {
        }

        /// <summary>
        /// Constructor; for Asset Pipeline 3 content-tree.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="mapNode"></param>
        /// <param name="useTextReaderMethod"></param>
        /// <param name="loadOptions"></param>
        public Scene(IContentTree tree, ContentTreeHelper helper, String filename, LoadOptions loadOptions = SceneXml.LoadOptions.All, bool useTextReaderMethod = false)
        {
            if (String.IsNullOrEmpty(filename))
                throw new ArgumentException("filename");

            IContentNode sceneNode = null;
            this.SceneType = SceneType.Unknown;
            if ((null != tree) && (null != helper))
            {
                String sceneName = System.IO.Path.GetFileNameWithoutExtension(filename);
                IEnumerable<IContentNode> mapSourceNodes = helper.GetAllMapExportNodes();
                IContentNode mapSourceNode = mapSourceNodes.FirstOrDefault(n => String.Equals(n.Name, sceneName, StringComparison.OrdinalIgnoreCase));

                if (null != mapSourceNode)
                {
                    IEnumerable<IProcess> processes = tree.FindProcessesWithInput(mapSourceNode);
                    IProcess exportProcess = processes.FirstOrDefault(p =>
                        String.Equals(p.ProcessorClassName, "RSG.Pipeline.Processor.Common.Dcc3dsmaxExportProcessor", StringComparison.OrdinalIgnoreCase));
                    if (null != exportProcess)
                    {
                        sceneNode = exportProcess.Inputs.First();
                        this.SceneType = GetSceneType(exportProcess.Inputs.First());
                    }
                }
            }
            
            InitAndLoadFrom(filename, loadOptions, useTextReaderMethod);
            SetUniqueIdentifiers(sceneNode);
        }

        /// <summary>
        /// Constructor; for Asset Pipeline 3 without content-tree.  
        /// This should only be used when we really need to avoid a content tree load.
        /// It's important to note that this construction path doesn't set the hashes.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="mapNode"></param>
        /// <param name="useTextReaderMethod"></param>
        /// <param name="loadOptions"></param>
        public Scene(SceneType sceneType, String filename, LoadOptions loadOptions = SceneXml.LoadOptions.All, bool useTextReaderMethod = false)
        {
            if (String.IsNullOrEmpty(filename))
                throw new ArgumentException("filename");

            this.SceneType = sceneType;

            InitAndLoadFrom(filename, loadOptions, useTextReaderMethod);
        }

        /// <summary>
        /// Constructor, from RSG.SceneXml XML file.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="mapNode"></param>
        /// <param name="useTextReaderMethod"></param>
        /// <param name="loadOptions"></param>
        [Obsolete("Use AP3 IContentTree version instead.")]
        public Scene(String filename, ContentNodeMap mapNode = null, LoadOptions loadOptions = SceneXml.LoadOptions.All, bool useTextReaderMethod = false)
        {
            if (filename == null)
                throw new ArgumentNullException("filename");

            if (mapNode != null)
                this.SceneType = SceneTypeUtils.GetSceneType(mapNode);
            else
                this.SceneType = SceneType.Unknown;

            InitAndLoadFrom(filename, loadOptions, useTextReaderMethod);
            SetUniqueIdentifiers(mapNode);
        }

        /// <summary>
        /// Static constructor.
        /// </summary>
        static Scene()
        {
            Scene.Log = LogFactory.CreateUniversalLog("RSG.SceneXml.Scene");
        }
        #endregion // Constructor(s)

        #region Static Controller Methods
        /// <summary>
        /// Return the SceneType for a given IContentNode; default SceneType.Unknown.
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public static SceneType GetSceneType(IContentNode node)
        {
            Debug.Assert(node is RSG.Pipeline.Content.IFilesystemNode,
                "Expecting IFilesystemNode.");
            if (!(node is RSG.Pipeline.Content.IFilesystemNode))
                throw (new ArgumentException("Expecting IFilesystemNode", "node"));

            Debug.Assert(node.Parameters.ContainsKey(Constants.ParamMap_3dsmaxType),
                String.Format("Expecting {0} parameter.", Constants.ParamMap_3dsmaxType));
            if (!node.Parameters.ContainsKey(Constants.ParamMap_3dsmaxType))
            {
                Scene.Log.ErrorCtx(LOG_CTX, "Content-node for '{0}' does not define '{1}' parameter key.  SceneType will be Unknown.",
                    ((RSG.Pipeline.Content.IFilesystemNode)node).AbsolutePath, Constants.ParamMap_3dsmaxType);
                return (SceneType.Unknown);
            }

            SceneType type = SceneType.Unknown;
            if (node is RSG.Pipeline.Content.IFilesystemNode)
            {
                String maxType = String.Empty;
                maxType = (String)node.Parameters[Constants.ParamMap_3dsmaxType];

                type = GetSceneTypeForString(maxType);
            }
            return (type);
        }

        /// <summary>
        /// Return the SceneType for a given filename; default SceneType.Unknown.
        /// </summary>
        /// <param name="tree"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static SceneType GetSceneType(IContentTree tree, String filename)
        {
            IContentNode node = tree.CreateFile(filename);
            return (GetSceneType(node));
        }

        /// <summary>
        /// Static construction method that looks up the correct map node.  This should be used sparingly as it has to initialise the content tree
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="loadOptions"></param>
        /// <param name="useTextReaderMethod"></param>
        /// <returns></returns>
        [Obsolete("Use AP3 methods and SceneCollection instead.  This is very expensive.")]
        public static Scene Load(String filename, LoadOptions loadOptions, bool useTextReaderMethod)
        {
            using (ConfigGameView configGameView = new ConfigGameView())
            {
                string contentName = SIO.Path.GetFileNameWithoutExtension(filename);
                ContentNodeMap mapNode = (ContentNodeMap)configGameView.Content.Root.FindFirst(contentName, "map");
                return new Scene(filename, mapNode, loadOptions, useTextReaderMethod);
            }
        }
        #endregion // Static Controller Methods

        #region Controller Methods
        /// <summary>
        /// Find an object definition based on object unique GUID.
        /// </summary>
        /// <param name="guid">Object GUID to find</param>
        /// <returns>ObjectDef if found, otherwise null</returns>
        public TargetObjectDef FindObject(Guid guid, bool useAttrGuid = false)
        {
            Debug.Assert(m_TableLookup.Count > 0, "Lookup table is empty, attempting to use prior to population?");
            if (useAttrGuid && AttrTableLookup.ContainsKey(guid))
                return (AttrTableLookup[guid]);
            if (m_TableLookup.ContainsKey(guid))
                return (m_TableLookup[guid]);
            return (null);
        }

        /// <summary>
        /// Find an object definition based on the cached names of objects at the root of the scene (fine for prop and interior scenes)
        /// </summary>
        /// <param name="objectName">Object Name to find</param>
        /// <returns>ObjectDef if found, otherwise null</returns>
        public TargetObjectDef FindObject(String objectName)
        {
            TargetObjectDef objectDef;
            // TargetObjectDef is a class, hence its default() will be null
            nameObjectMap_.TryGetValue(objectName.ToLower(), out objectDef);

            return objectDef;
        }

        /// <summary>
        /// Find an object definition based on a full search for objects with that name
        /// </summary>
        /// <param name="objectName">Object Name to find</param>
        /// <returns>ObjectDef if found, otherwise null</returns>
        public TargetObjectDef FindObjectSlow(String objectName)
        {
            foreach (TargetObjectDef objectDef in this.Walk(WalkMode.DepthFirst))
            {
                if (String.Equals(objectDef.Name, objectName, StringComparison.OrdinalIgnoreCase))
                    return objectDef;
            }

            return null;
        }

        /// <summary>
        /// Finds all objects with the material GUID assigned.
        /// </summary>
        /// <param name="materialGuid"></param>
        /// <returns></returns>
        public TargetObjectDef[] FindObjectsWithMaterial(Guid materialGuid)
        {
            IEnumerable<TargetObjectDef> foundObjects = this.Objects.Where(objectDef => objectDef.Material == materialGuid);
            return foundObjects.ToArray();
        }

        /// <summary>
        /// Walk the scene's complete object tree.
        /// </summary>
        /// <param name="mode">Tree walk mode (depth or breadth first)</param>
        /// <returns>Enumerable yields ObjectDef object</returns>
        /// 
        /// Example Usage:
        ///   foreach (ObjectDef obj in scene.Walk(Scene.WalkMode.DepthFirst))
        ///   {
        ///     // Do work with obj
        ///   }
        ///   foreach (ObjectDef obj in scene.Walk(Scene.WalkMode.BreadthFirst))
        ///   {
        ///     // Do work with obj
        ///   }
        ///   
        public IEnumerable<TargetObjectDef> Walk(WalkMode mode, DeadEndFunc IsDeadEnd = null)
        {
            switch (mode)
            {
                case WalkMode.DepthFirst:
                    foreach (TargetObjectDef rootobj in this.Objects)
                    {
                        if (null != IsDeadEnd && IsDeadEnd(rootobj))
                            continue;
                        foreach (TargetObjectDef obj in WalkInternal(mode, rootobj))
                            yield return obj;
                    }
                    break;
                case WalkMode.BreadthFirst:
                    // Yield siblings
                    foreach (TargetObjectDef rootobj in this.Objects)
                        yield return rootobj;
                    // Yield children
                    foreach (TargetObjectDef rootobj in this.Objects)
                        foreach (TargetObjectDef obj in WalkInternal(mode, rootobj))
                            yield return obj;
                    break;
                default:
                    Debug.Assert(false, String.Format("Unsupported WalkMode {0}.", mode));
                    break;
            }
        }

        /// <summary>
        /// Walks through the entire scene tree setting the ITarget on each object.
        /// Used when per-platform attribute calls are done, the appropriate platform attributes are queried
        /// </summary>
        /// <returns>void</returns>
        public void SetSceneTarget(ITarget target)
        {
            foreach (TargetObjectDef obj in this.Walk(Scene.WalkMode.DepthFirst))
            {
                obj.Target = target;
            }
        }

        /// <summary>
        /// Generate some post export stats into the scene xml
        /// </summary>
        /// <returns>void</returns>
        public void GenerateStats(String mapStream)
        {
            this.Statistics.CreateStatistics(this.Filename, mapStream);
            this.Statistics.Save(this.Filename);
            this.Statistics.ExportReport(mapStream);
        }

        /// <summary>
        /// Gets a list of source textures from the scene, with the option of only
        /// getting the textures from objects marked for export.
        /// </summary>
        /// <param name="sourceTextures"></param>
        /// <param name="filterOutDontExportObjects"></param>
        public void GetSceneTextureFilenames(ref List<String> sourceTextures, bool filterOutDontExportObjects)
        {
            foreach (TargetObjectDef obj in this.Walk(WalkMode.BreadthFirst))
            {
                if (!obj.IsObject())
                    continue;
                if (obj.IsRefObject() || obj.IsXRef() || obj.IsInternalRef())
                    continue;
                if (filterOutDontExportObjects && obj.DontExport())
                    continue;
                
                Guid materialGuid = obj.Material;
                if (!this.MaterialLookup.ContainsKey(materialGuid))
                    continue;

                MaterialDef matDef = this.MaterialLookup[materialGuid];
                // If a material entry, check textures
                if (matDef.Textures != null)
                {
                    IEnumerable<String> matSourceTextureList = matDef.Textures.Select(m => m.FilePath);
                    sourceTextures.AddRange(matSourceTextureList);
                }

                // If a multimaterial then check all submaterials for textures
                if (matDef.SubMaterials != null)
                {
                    foreach (MaterialDef submat in matDef.SubMaterials)
                    {
                        if (submat.Textures != null)
                        {
                            IEnumerable<String> subMatSourceTextureList = submat.Textures.Select(m => m.FilePath);
                            sourceTextures.AddRange(subMatSourceTextureList);
                        }
                    }
                }
            }
            sourceTextures = sourceTextures.Distinct().ToList();

        }
        #endregion // Controller Methods

        #region Object Overrides
        /// <summary>
        /// Hash code for object.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return (base.GetHashCode());
        }

        /// <summary>
        /// Equality; based on SceneXml filename path.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(Object obj)
        {
            Scene comparedTo = obj as Scene;
            return (comparedTo != null) && ( Filename == comparedTo.Filename);
        }
        #endregion // Object Overrides

        #region Private Methods
        /// <summary>
        /// Initialise object and load scene (shared between constructors).
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="loadOptions"></param>
        /// <param name="useTextReaderMethod"></param>
        private void InitAndLoadFrom(String filename, LoadOptions loadOptions, bool useTextReaderMethod)
        {
            if (useTextReaderMethod)
            {
                this.LoadOptions = loadOptions;
                this.m_TableLookup = new Dictionary<Guid, TargetObjectDef>();
                this.m_AttrTableLookup = new Dictionary<Guid, TargetObjectDef>();
                this.m_MaterialTableLookup = new Dictionary<Guid, Material.MaterialDef>();
                this.Filename = SIO.Path.GetFullPath(filename);
                this.Statistics = new Statistics.Stats(this);
                this.Objects = new TargetObjectDef[0];
                this.Materials = new MaterialDef[0];
                this.ExcludedObjects = new Guid[0];
                this.GeometryStatsLookup = new Dictionary<Guid, Statistics.DrawableStats>();
                this.TxdStatsLookup = new Dictionary<String, SceneXml.Statistics.TxdStats>();

                if (!SIO.File.Exists(filename))
                    return;

                XmlTextReader reader = null;
                try
                {
                    using (reader = new XmlTextReader(filename))
                    {
                        reader.MoveToContent();
                        this.Parse(reader);
                    }
                }
                catch (Exception ex)
                {
                    Scene.Log.ToolExceptionCtx(LOG_CTX, ex, "SceneXML parse Exception.");
                    if (reader != null)
                        reader.Close();
                }
            }
            else
            {
                this.LoadOptions = loadOptions;
                this.m_TableLookup = new Dictionary<Guid, TargetObjectDef>();
                this.m_AttrTableLookup = new Dictionary<Guid, TargetObjectDef>();
                this.m_MaterialTableLookup = new Dictionary<Guid, Material.MaterialDef>();
                this.Filename = SIO.Path.GetFullPath(filename);
                this.Statistics = new Statistics.Stats(this);

                try
                {
                    XmlDocument document = new XmlDocument();
                    document.Load(filename);

                    ParseExcludedObjects(document);
                    ParseObjects(document);
                    ParseMaterials(document);
                    ParseStatistics(document);
                }
                catch (Exception ex)
                {
                    Scene.Log.ToolExceptionCtx(LOG_CTX, ex, "SceneXML parse Exception.");
                }
            }
        }

        #region XML Text Reader Parsing Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        private void ParseSkip(XmlTextReader reader)
        {
            if (reader.IsEmptyElement)
                reader.ReadStartElement();
            else
            {
                reader.ReadStartElement();
                while (reader.MoveToContent() == XmlNodeType.Element)
                {
                    ParseSkip(reader);
                }
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        private void Parse(XmlTextReader reader)
        {
            if (reader.IsEmptyElement)
                this.ParseAttributes(reader);
            else
            {
                this.ParseAttributes(reader);
                while (reader.MoveToContent() == XmlNodeType.Element)
                {
                    if (reader.Name == "objects")
                    {
                        if (this.LoadOptions.HasFlag(SceneXml.LoadOptions.Objects))
                            this.ParseObjects(reader);
                        else
                            this.ParseSkip(reader);
                    }
                    else if (reader.Name == "materials")
                    {
                        if (this.LoadOptions.HasFlag(SceneXml.LoadOptions.Materials))
                            this.ParseMaterials(reader);
                        else
                            this.ParseSkip(reader);
                    }
                    else if (reader.Name == "stats")
                    {
                        if (this.LoadOptions.HasFlag(SceneXml.LoadOptions.Statistics))
                            this.ParseStatistics(reader);
                        else
                            this.ParseSkip(reader);
                    }
                    else
                    {
                        this.ParseSkip(reader);
                    }
                }
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        private void ParseAttributes(XmlTextReader reader)
        {
            String user = reader.GetAttribute(sC_s_s_SceneUser);
            this.ExportUser = user ?? String.Empty;

            String timeStamp = reader.GetAttribute(sC_s_s_SceneTimestamp);
            this.ExportTimestamp = timeStamp != null ? DateTime.Parse(timeStamp) : this.ExportTimestamp;

            String filename = reader.GetAttribute(sC_s_s_SceneFilename);
            if (!String.IsNullOrEmpty(filename) || !String.IsNullOrWhiteSpace(filename))
                this.ExportFilename = SIO.Path.GetFullPath(filename);
            else
                this.ExportFilename = String.Empty;

            String version = reader.GetAttribute(sC_s_s_SceneVersion);
            this.Version = version != null ? float.Parse(version) : this.Version;

            // Version check.
            if (this.Version != sC_s_f_Version)
            {
                String message = String.Format("SceneXml XML file version mismatch.  Found {0} expecting {1} on file {2}.",
                    this.Version, this.sC_s_f_Version, this.Filename);
                Scene.Log.WarningCtx(LOG_CTX, message);
            }

            reader.ReadStartElement();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        private void ParseObjects(XmlTextReader reader)
        {
            if (reader.IsEmptyElement)
                reader.ReadStartElement();
            else
            {
                reader.ReadStartElement();
                List<TargetObjectDef> objectList = new List<TargetObjectDef>();
                while (reader.MoveToContent() == XmlNodeType.Element)
                {
                    if (reader.Name == "object")
                    {
                        TargetObjectDef rootobject = new TargetObjectDef(reader, this);
                        objectList.Add(rootobject);
                        RecurseProcessObject(rootobject);
                    }
                    else
                    {
                        this.ParseSkip(reader);
                    }
                }
                this.Objects = objectList.ToArray();
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }

            // Post-process the Scene graph, resolving object references.
            foreach (TargetObjectDef obj in Walk(WalkMode.DepthFirst))
            {
                obj.ResolveLinks(this);
            }
            // Post-process the Scene graph, determining LOD level.
            // Has to be a separate pass.
            foreach (TargetObjectDef obj in Walk(WalkMode.DepthFirst))
            {
                obj.DetermineLodLevel();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        private void ParseMaterials(XmlTextReader reader)
        {
            if (reader.IsEmptyElement)
                reader.ReadStartElement();
            else
            {
                reader.ReadStartElement();
                List<MaterialDef> materialList = new List<MaterialDef>();
                while (reader.MoveToContent() == XmlNodeType.Element)
                {
                    if (reader.Name == "material")
                    {
                        MaterialDef rootmaterial = new MaterialDef(reader, this);
                        materialList.Add(rootmaterial);
                        ProcessMaterial(rootmaterial);
                    }
                    else
                    {
                        this.ParseSkip(reader);
                    }
                }
                this.Materials = materialList.ToArray();
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        private void ParseStatistics(XmlTextReader reader)
        {
            this.Statistics.LoadStatistics(reader);
            this.GeometryStatsLookup = new Dictionary<Guid, DrawableStats>();
            this.TxdStatsLookup = new Dictionary<string, TxdStats>();
            foreach (var geometryStat in this.Statistics.GeometryStats)
            {
                this.GeometryStatsLookup.Add(geometryStat.Key, geometryStat.Value);
            }
            foreach (var geometryStat in this.Statistics.CollisionStats)
            {
                this.GeometryStatsLookup.Add(geometryStat.Key, geometryStat.Value);
            }
            foreach (var txdStat in this.Statistics.TxdStats)
            {
                this.TxdStatsLookup.Add(txdStat.Key, txdStat.Value);
            }
        }
        #endregion // XML Text Reader Parsing Methods

        #region XML Document Parsing Methods
        /// <summary>
        /// Parses the excluded objects from the given scene xml document
        /// </summary>
        /// <param name="document"></param>
        private void ParseExcludedObjects(XmlDocument document)
        {
            XPathNavigator navigator = document.CreateNavigator();

            XPathNodeIterator excludedObjectIt = navigator.Select(sC_s_xpath_XPathExcludedObjects);
            List<Guid> excludedObjects = new List<Guid>();
            while (excludedObjectIt.MoveNext())
            {
                XPathNavigator objectsNavigator = excludedObjectIt.Current;

                Guid guid = new Guid(objectsNavigator.Value);
                excludedObjects.Add(guid);
            }
            this.ExcludedObjects = excludedObjects.ToArray();
        }

        /// <summary>
        /// Parses the objects from the given scene xml document
        /// </summary>
        /// <param name="document"></param>
        private void ParseObjects(XmlDocument document)
        {
            XPathNavigator navigator = document.CreateNavigator();

            // Parse our attributes, there are particular attributes we look
            // for here, ignoring the rest.
            XPathNodeIterator rootAttrIt = navigator.Select(sC_s_xpath_XPathSceneAttrs);
            while (rootAttrIt.MoveNext())
            {
                if (typeof(String) != rootAttrIt.Current.ValueType)
                    continue;
                String value = rootAttrIt.Current.TypedValue as String;

                if (sC_s_s_SceneVersion == rootAttrIt.Current.Name)
                    this.Version = float.Parse(value);
                else if (sC_s_s_SceneFilename == rootAttrIt.Current.Name)
                {
                    if (String.Empty != value)
                        this.ExportFilename = SIO.Path.GetFullPath(value);
                }
                else if (sC_s_s_SceneTimestamp == rootAttrIt.Current.Name) 
                    this.ExportTimestamp = DateTime.Parse(value);
                else if (sC_s_s_SceneUser == rootAttrIt.Current.Name)
                    this.ExportUser = value;
            }

            // Version check.
            if (this.Version != sC_s_f_Version)
            {
                String message = String.Format("SceneXml XML file version mismatch.  Found {0} expecting {1} on file {2}.",
                    this.Version, this.sC_s_f_Version, this.Filename);
                Scene.Log.WarningCtx(LOG_CTX, message);
            }

            // Parse our root Object Definitions, these take care of our
            // child objects.
            XPathNodeIterator objectDefIt = navigator.Select(sC_s_xpath_XPathRootObjectDefs);
            List<TargetObjectDef> objectDefs = new List<TargetObjectDef>();
            while (objectDefIt.MoveNext())
            {
                XPathNavigator objectsNavigator = objectDefIt.Current;
                TargetObjectDef rootobject = new TargetObjectDef(objectsNavigator, this);

                if (!ExcludedObjects.Contains(rootobject.AttributeGuid))
                {
                    objectDefs.Add(rootobject);
                    RecurseProcessObject(rootobject);
                }
            }
            this.Objects = objectDefs.ToArray();

            // Post-process the Scene graph, resolving object references.
            foreach (TargetObjectDef obj in Walk(WalkMode.DepthFirst))
            {
                obj.ResolveLinks(this);
            }
            // Post-process the Scene graph, determining LOD level.
            // Has to be a separate pass.
            foreach (TargetObjectDef obj in Walk(WalkMode.DepthFirst))
            {
                obj.DetermineLodLevel();
            }
        }

        /// <summary>
        /// Parses the materials from the given scene xml document
        /// </summary>
        /// <param name="document"></param>
        private void ParseMaterials(XmlDocument document)
        {
            XPathNavigator navigator = document.CreateNavigator();

            // Parse our root Material Definitions, these take care of our
            // sub materials
            XPathNodeIterator materialDefit = navigator.Select(sC_s_xpath_XPathRootMaterialsDefs);
            this.Materials = new Material.MaterialDef[materialDefit.Count];
            while (materialDefit.MoveNext())
            {
                XPathNavigator materialsNavigator = materialDefit.Current;
                Material.MaterialDef rootobject = new Material.MaterialDef(materialsNavigator, this);
                this.Materials[materialDefit.CurrentPosition - 1] = rootobject;
                ProcessMaterial(rootobject);
            }
        }

        /// <summary>
        /// Parses the stats from the given scene xml document
        /// </summary>
        /// <param name="document"></param>
        private void ParseStatistics(XmlDocument document)
        {
            if (this.Statistics != null)
            {
                this.Statistics.LoadStatistics(document);
            }
            this.GeometryStatsLookup = new Dictionary<Guid, Statistics.DrawableStats>();
            this.TxdStatsLookup = new Dictionary<String, SceneXml.Statistics.TxdStats>();

            // Create a statistic lookup table
            XPathNavigator navigator = document.CreateNavigator();
            XPathNodeIterator geoemtryDefIt = null;
            geoemtryDefIt = navigator.Select(XPathExpression.Compile("/scene/stats/geos/geo"));
            if (geoemtryDefIt.Count == 0)
            {
                geoemtryDefIt = navigator.Select(XPathExpression.Compile("/scene/stats/geometries/geometry"));
            }
            while (geoemtryDefIt.MoveNext())
            {
                XPathNavigator geometryNavigator = geoemtryDefIt.Current;
                XPathNodeIterator attributeIt = geometryNavigator.Select(XPathExpression.Compile("@*"));

                Statistics.DrawableStats stats = new Statistics.DrawableStats("", this);
                while (attributeIt.MoveNext())
                {
                    XPathNavigator attributeNavigator = attributeIt.Current;
                    if (typeof(String) != attributeNavigator.ValueType)
                        continue;

                    String value = (attributeNavigator.TypedValue as String);

                    try
                    {
                        if ("name" == attributeNavigator.Name)
                        {
                            stats.Name = value;
                        }
                        else if ("guid" == attributeNavigator.Name)
                        {
                            stats.Guid = new Guid(value);
                        }
                        else if ("size" == attributeNavigator.Name)
                        {
                            stats.Size = Convert.ToUInt32(value);
                        }
                        else if ("polycount" == attributeNavigator.Name)
                        {
                            stats.PolyCount = Convert.ToUInt32(value);
                        }
                        else if ("polydensity" == attributeNavigator.Name)
                        {
                            float result = 0.0f;
                            float.TryParse(value, out result);
                            stats.PolyDensity = result;
                        }
                    }
                    catch (System.OverflowException ex)
                    {
                        Scene.Log.ToolExceptionCtx(LOG_CTX, ex,
                            "Overflow error in scenexml stats {0} parsing: {1}", navigator.BaseURI, ex.Message);
                    }
                }
                TargetObjectDef sceneObject = this.FindObject(stats.Guid);
                if (sceneObject != null && !this.GeometryStatsLookup.ContainsKey(stats.Guid))
                {
                    this.GeometryStatsLookup.Add(stats.Guid, stats);
                }
            }
            XPathNodeIterator collisionDefIt = navigator.Select(XPathExpression.Compile("/scene/stats/collision_geometry/geometry"));
            while (collisionDefIt.MoveNext())
            {
                XPathNavigator collisionNavigator = collisionDefIt.Current;
                XPathNodeIterator attributeIt = collisionNavigator.Select(XPathExpression.Compile("@*"));

                Statistics.DrawableStats stats = new Statistics.DrawableStats("", this);
                while (attributeIt.MoveNext())
                {
                    XPathNavigator attributeNavigator = attributeIt.Current;
                    if (typeof(String) != attributeNavigator.ValueType)
                        continue;

                    String value = (attributeNavigator.TypedValue as String);
                    try
                    {
                        if ("name" == attributeNavigator.Name)
                        {
                            stats.Name = value;
                        }
                        else if ("guid" == attributeNavigator.Name)
                        {
                            stats.Guid = new Guid(value);
                        }
                        else if ("polycount" == attributeNavigator.Name)
                        {
                            stats.PolyCount = Convert.ToUInt32(value);
                        }
                        else if ("polydensity" == attributeNavigator.Name)
                        {
                            float result = 0.0f;
                            float.TryParse(value, out result);
                            stats.PolyDensity = result;
                        }
                    }
                    catch (System.OverflowException ex)
                    {
                        Scene.Log.ToolExceptionCtx(LOG_CTX, ex,
                            "Overflow error in scenexml {0} stats parsing: {1}", navigator.BaseURI, ex.Message);
                    }
                }
                TargetObjectDef sceneObject = this.FindObject(stats.Guid);
                if (sceneObject != null && !this.GeometryStatsLookup.ContainsKey(stats.Guid))
                {
                    this.GeometryStatsLookup.Add(stats.Guid, stats);
                }
            }
            XPathNodeIterator txdIt = navigator.Select(XPathExpression.Compile("/scene/stats/txds/txd"));
            while (txdIt.MoveNext())
            {
                XPathNavigator objectsNavigator = txdIt.Current;
                XPathNodeIterator attributeIt = objectsNavigator.Select(XPathExpression.Compile("@*"));

                Statistics.TxdStats stats = new Statistics.TxdStats("", this);
                while (attributeIt.MoveNext())
                {
                    XPathNavigator attributeNavigator = attributeIt.Current;
                    if (typeof(String) != attributeNavigator.ValueType)
                        continue;

                    String value = (attributeNavigator.TypedValue as String);

                    try
                    {
                        if ("name" == attributeNavigator.Name)
                        {
                            stats.Name = value;
                        }
                        else if ("size" == attributeNavigator.Name)
                        {
                            stats.Size = Convert.ToUInt32(value);
                        }
                        else if ("guid" == attributeNavigator.Name)
                        {
                            stats.Guid = new Guid(value);
                        }
                    }
                    catch (System.OverflowException ex)
                    {
                        Scene.Log.ToolExceptionCtx(LOG_CTX, ex,
                            "Overflow error in scenexml {0} stats parsing: {1}", navigator.BaseURI, ex.Message);
                    }
                }
                if (!this.TxdStatsLookup.ContainsKey(stats.Name))
                {
                    this.TxdStatsLookup.Add(stats.Name, stats);
                }
            }
        }
        #endregion // XML Document Parsing Methods

        /// <summary>
        /// Walk the scene's object tree from a specific node.
        /// </summary>
        /// <param name="mode">Tree walk mode (depth or breadth first)</param>
        /// <returns>Enumerable yields ObjectDef object</returns>
        private IEnumerable<TargetObjectDef> WalkInternal(WalkMode mode, TargetObjectDef obj)
        {
            // Yield current ObjectDef.
            if (WalkMode.DepthFirst == mode)
                yield return obj;

            if (obj.Children.Length > 0)
            {
                switch (mode)
                {
                    case WalkMode.DepthFirst:
                        foreach (TargetObjectDef child in obj.Children)
                        {
                            foreach (TargetObjectDef o in WalkInternal(mode, child))
                                yield return o;
                        }
                        break;
                    case WalkMode.BreadthFirst:
                        // Yield siblings
                        foreach (TargetObjectDef child in obj.Children)
                            yield return child;
                        // Yield children
                        foreach (TargetObjectDef child in obj.Children)
                            foreach (TargetObjectDef o in WalkInternal(mode, child))
                                yield return o;
                        break;
                    default:
                        Debug.Assert(false, String.Format("Unsupported WalkMode {0}.", mode));
                        break;
                }
            }
        }

        /// <summary>
        /// Recursively process ObjectDef populating our lookup table.
        /// </summary>
        /// <param name="root"></param>
        void RecurseProcessObject(TargetObjectDef root)
        {
            Debug.Assert(!m_TableLookup.ContainsKey(root.Guid),
                String.Format("Lookup table already contains this object ({0}).  Internal error.", root.ToString()));
            
            // Add root
            if (m_TableLookup.ContainsKey(root.Guid))
            {
                Log.ToolErrorCtx(root.Name, "SceneXML contains duplicate GUID {0} for {1} and {2}. https://devstar.rockstargames.com/wiki/index.php/Map_Export_Errors#Duplicate_RsGuid_in_SceneXML_loading", root.Guid, root.Name, m_TableLookup[root.Guid].Name);
            }
            else
            {
                m_TableLookup.Add(root.Guid, root);
            }

            if (root.AttributeGuid != Guid.Empty && root.IsObject() && !root.IsRefObject())
            {
                if (m_AttrTableLookup.ContainsKey(root.AttributeGuid))
                {
                    String filename = SIO.Path.GetFileNameWithoutExtension(m_AttrTableLookup[root.AttributeGuid].MyScene.ExportFilename);
                    Scene.Log.WarningCtx(root.Name, "Object's attribute guid {0} already existent in scene {1} on object {2}. Don't use in slave object tech, or reset guid.", root.AttributeGuid.ToString(), filename, m_AttrTableLookup[root.AttributeGuid].Name);
                }
                else
                    m_AttrTableLookup.Add(root.AttributeGuid, root);
            }
            
            // Recurse
            foreach (TargetObjectDef childObj in root.Children)
            {
                RecurseProcessObject(childObj);
            }
        }

        /// <summary>
        /// Recursively process MaterialDef populating our lookup table.
        /// </summary>
        /// <param name="root"></param>
        void ProcessMaterial(Material.MaterialDef root)
        {
            Debug.Assert(!m_MaterialTableLookup.ContainsKey(root.Guid),
                String.Format("Lookup table already contains this object ({0}).  Internal error.", root.ToString()));

            // Add root
            m_MaterialTableLookup.Add(root.Guid, root);
        }

        /// <summary>
        /// Set the 16-bit and 32-bit unique identifiers; these are used
        /// by the IMAP serialiser as seeds for unique per-entity IDs.
        /// </summary>
        /// <param name="mapNode"></param>
        /// 
        /// ** DO NOT CHANGE THESE CALCULATIONS **
        /// 
        void SetUniqueIdentifiers(IContentNode mapNode)
        {
            if (mapNode != null &&
                Objects.Length > 0 &&
                Objects[0].IsContainer() &&
                Guid.Empty != Objects[0].AttributeGuid)
            {
                uint seed = 0;

                // FD : need to cast to int before casting to uint.
                // It's a matter of 'cast into' vs 'convert into' here (see : http://stackoverflow.com/a/13290776 )
                if (mapNode.Parameters.ContainsKey(Constants.ParamMap_Seed))
                    seed = (uint)(int)mapNode.Parameters[Constants.ParamMap_Seed];

                // Map container; set hashes.
#if USE_NEW_HASHING_ALGORITHM
                String id = Objects[0].AttributeGuid.ToString();
                uint h32 = RSG.Base.Security.Cryptography.OneAtATime.ComputePartialHash(id, seed);
                this.Hash32 = RSG.Base.Security.Cryptography.OneAtATime.ComputeFinalHash(h32);                
                this.Hash16 = (UInt16)(this.Hash32 & 0x0000FFFF);
#else
                // Map container; set hashes.
                String id = Objects[0].AttributeGuid.ToString();
                this.Hash32 = RSG.ManagedRage.StringHashUtil.atStringHash(id, seed);
                this.Hash16 = (UInt16)(this.Hash32 & 0x0000FFFF);
#endif // !USE_NEW_HASHING_ALOGORITHM
            }
            else
            {
                // Non-maps but lets set our hashes anyway.
#if USE_NEW_HASHING_ALGORITHM
                String id = this.Filename;
                this.Hash32 = RSG.Base.Security.Cryptography.OneAtATime.ComputeHash(id);
                this.Hash16 = (UInt16)(this.Hash32 & 0x0000FFFF);
#else
                String id = this.Filename;
                this.Hash32 = RSG.ManagedRage.StringHashUtil.atStringHash(id, 0);
                this.Hash16 = (UInt16)(this.Hash32 & 0x0000FFFF);
#endif // !USE_NEW_HASHING_ALOGORITHM
            }
        }

        /// <summary>
        /// Set the 16-bit and 32-bit unique identifiers; these are used
        /// by the IMAP serialiser as seeds for unique per-entity IDs.
        /// </summary>
        /// DO NOT CHANGE THESE CALCULATIONS.
        /// 
        [Obsolete("Use AP3 IContentTree instead; private method.")]
        void SetUniqueIdentifiers(ContentNodeMap mapNode)
        {
            if (mapNode != null &&
                Objects.Length > 0 &&
                Objects[0].IsContainer() &&
                Guid.Empty != Objects[0].AttributeGuid)
            {
                // Map container; set hashes.
                String id = Objects[0].AttributeGuid.ToString();
                this.Hash32 = RSG.ManagedRage.StringHashUtil.atStringHash(id, mapNode.Seed);
                this.Hash16 = (UInt16)(this.Hash32 & 0x0000FFFF);
            }
            else
            {
                // Non-maps but lets set our hashes anyway.
                String id = this.Filename;
                this.Hash32 = RSG.ManagedRage.StringHashUtil.atStringHash(id, 0);
                this.Hash16 = (UInt16)(this.Hash32 & 0x0000FFFF);
            }
        }

        /// <summary>
        /// Return SceneType for a string.
        /// </summary>
        /// <param name="maxType"></param>
        /// <returns></returns>
        private static SceneType GetSceneTypeForString(String maxType)
        {
            SceneType[] sceneTypes = (SceneType[])Enum.GetValues(typeof(SceneType));

            Type type = typeof(SceneType);
            foreach (SceneType sceneType in sceneTypes)
            {
                System.Reflection.FieldInfo fi = type.GetField(sceneType.ToString());
                XmlConstantAttribute[] attributes =
                    fi.GetCustomAttributes(typeof(XmlConstantAttribute), false) as XmlConstantAttribute[];

                if (attributes != null &&
                    attributes.Length > 0 &&
                    attributes[0].Constant == maxType)
                    return (sceneType);
            }
            return (SceneType.Unknown);
        }
        #endregion // Private Methods
    }
        
} // RSG.SceneXml namespace

// RSG.SceneXml.cs
