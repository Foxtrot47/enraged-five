//
// File: Attributes.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of Attribute name and defaults classes.
//

using System;
using System.Collections.Generic;

namespace RSG.SceneXml
{
    /// <summary>
    /// DCC / 3dsmax object class constants.
    /// </summary>
    public static class ClassConsts
    {
        #region DCC / 3dsmax Object Classes
        public static readonly String MAX_CLASS_CONTAINER = "Container".ToLower();
        public static readonly String MAX_CLASS_DUMMY = "Dummy".ToLower();
        public static readonly String MAX_CLASS_BONE = "Bone".ToLower();
        public static readonly String MAX_CLASS_HELPER = "Helper".ToLower();
        public static readonly String MAX_CLASS_EDITABLE_MESH = "editable_mesh".ToLower();
        public static readonly String MAX_CLASS_EDITABLE_POLY = "editable_poly".ToLower();
        public static readonly String MAX_CLASS_EDITABLE_SPLINE = "Editable_Spline".ToLower();
        public static readonly String MAX_CLASS_XREF = "xref_object".ToLower();
        public static readonly String MAX_CLASS_RSREF = "rsrefobject".ToLower();
        public static readonly String MAX_CLASS_RSINTERNALREF = "rsinternalref".ToLower();
        public static readonly String MAX_CLASS_RSCONTAINERLODREF = "rscontainerlodref".ToLower();
        public static readonly String MAX_CLASS_INTERNALREF = "InternalRef".ToLower();
        public static readonly String MAX_SUPERCLASS_HELPER = "Helper".ToLower();
        public static readonly String MAX_CLASS_RSLEDGE = "rs_ledge".ToLower();
        public static readonly String MAX_CLASS_ANIM_PROXY = "StatedAnimProxy".ToLower();
        public static readonly String MAX_CLASS_SLOWZONE = "gta_slowzone".ToLower();
        public static readonly String MAX_CLASS_TEXT = "Text".ToLower();
        public static readonly String MAX_CLASS_LINE = "Line".ToLower();
        public static readonly String MAX_GROUP_HEAD= "Max_Group_Head".ToLower();
        public static readonly String MAX_RAGELIGHT = "Rage_Light".ToLower();
        public static readonly String MAX_LIGHT_INSTANCE = "Rage_Light_Instance".ToLower();
        public static readonly String MAX_SPAWN_POINT_INSTANCE = "RsSpawnPointInstance".ToLower();
        public static readonly String MAX_VISIBILITY_BOUND = "RsVisibilityBound".ToLower();
        public static readonly String MAX_OCCLUSION_BOX = "RsOcclusionBox".ToLower();
        public static readonly String MAX_IMAP_GROUP_DUMMY = "RS_IMAP_Group".ToLower();
        public static readonly String MAX_OBJECT_GROUP = "RS_ObjectGroup".ToLower();
        public static readonly String MAX_INTERIOR_GROUP = "RS_InteriorGroup".ToLower();
        public static readonly String MAX_IMAP_GROUP_DEFINITION = "RS_IMAP_Group_Helper".ToLower();
        public static readonly String MAX_STREAMING_EXTENTS_OVERRIDE_BOX = "RsStreamingExtentsOverrideBox".ToLower();
        public static readonly String MAX_COLLISION_CAPSULE = "Col_Capsule".ToLower();
        public static readonly String MAX_COLLISION_PLANE = "Col_Plane".ToLower();
        public static readonly String MAX_CLASS_POINT_HELPER = "Point_Helper".ToLower();
        public static readonly String MAX_CLASS_CABLE_PROXY = "Cable_Proxy".ToLower();
        #endregion // DCC / 3dsmax Object Classes
    }

    /// <summary>
    /// Attribute Class Constants
    /// </summary>
    public static class AttrClass
    {
        #region Attribute Classes
        public static readonly String NONE = "".ToLower();
        public static readonly String BLOCK = "Gta Block".ToLower();
        public static readonly String CARGEN = "Gta CarGen".ToLower();
        public static readonly String COLLISION = "Gta Collision".ToLower();
        public static readonly String GARAGEAREA = "Gta GarageArea".ToLower();
        public static readonly String GROUP = "Gta Group".ToLower();
        public static readonly String LODMODIFIER = "Gta LodModifier".ToLower();
        public static readonly String MILO = "Gta MILO".ToLower();
        public static readonly String MILOTRI = "Gta MILOTri".ToLower();
        public static readonly String MLOPORTAL = "Gta MloPortal".ToLower();
        public static readonly String MLOROOM = "Gta MloRoom".ToLower();
        public static readonly String OBJECT = "Gta Object".ToLower();
        public static readonly String PICKUP = "Gta Pickup".ToLower();
        public static readonly String TIMECYCLE_BOX = "Gta TimeCycle".ToLower();
        public static readonly String TIMECYCLE_SPHERE = "Gta TC Sphere".ToLower();
        public static readonly String ZONE = "Gta Zone".ToLower();
        public static readonly String NETRESTART = "NetRestart".ToLower();
        public static readonly String VEHICLE_NODE = "VehicleNode".ToLower();
        public static readonly String VEHICLE_LINK = "VehicleLink".ToLower();
        public static readonly String PATROL_NODE = "PatrolNode".ToLower();
        public static readonly String PATROL_LINK = "PatrolLink".ToLower();
        public static readonly String SLOWZONE = "Gta SlowZone".ToLower();
        public static readonly String CONTAINER = "RS Container".ToLower();
        public static readonly String PROPGROUP = "RS PropGroup".ToLower();

        public static readonly String TWODFX_LADDER = "GtaLadderFX".ToLower();
        public static readonly String TWODFX_RSLADDER = "RSLadder".ToLower();
        public static readonly String TWODFX_AUDIO_EMITTER = "Gta AudioEmit".ToLower();
        public static readonly String TWODFX_BUOYANCY = "Gta Buoyancy".ToLower();
        public static readonly String TWODFX_DECAL = "RsDecal".ToLower();
        public static readonly String TWODFX_EXPLOSION_EFFECT = "Gta Explosion".ToLower();
        public static readonly String TWODFX_LIGHT = "Gta Light".ToLower();
        public static readonly String TWODFX_LIGHT_PHOTO = "Gta LightPhoto".ToLower();
        public static readonly String TWODFX_LIGHT_SHAFT = "Gta LightShaft".ToLower();
        public static readonly String TWODFX_PROC_OBJECT = "Gta ProcObj".ToLower();
        public static readonly String TWODFX_SCRIPT = "Gta Script".ToLower();
        public static readonly String TWODFX_PARTICLE_EFFECT = "RAGE Particle".ToLower();
        public static readonly String TWODFX_PED_QUEUE = "Gta Queue".ToLower();
        public static readonly String TWODFX_SCROLLBARS = "Gta Scrollbars".ToLower();
        public static readonly String TWODFX_SPAWN_POINT = "Gta SpawnPoint".ToLower();
        public static readonly String TWODFX_SWAYABLE_EFFECT = "Gta Swayable".ToLower();
        public static readonly String TWODFX_WALK_DONT_WALK = "Gta WalkDontWalk".ToLower();
        public static readonly String TWODFX_WIND_DISTURBANCE = "RsWindDisturbance".ToLower();
        public static readonly String TWODFX_WINDOW_LEDGE = "Gta WindowLegde".ToLower();        
        public static readonly String TWODFX_LEDGE = "RSLedge".ToLower();
        public static readonly String STATED_ANIM_PROXY = "RsStatedAnim".ToLower();
        public static readonly String TWODFX_DOOR = "RS Door".ToLower();
        public static readonly String TWODFX_EXPRESSION = "RS Expression".ToLower();
        #endregion // Attribute Classes
    }

    /// <summary>
    /// Attribute Name Constants.
    /// </summary>
    public static class AttrNames
    {
        #region Attribute Names
        #region Gta Object
        //----------------------------------------------------------------
		// Gta Object
		//----------------------------------------------------------------			
		public static readonly String OBJ_LOD_DISTANCE			= "LOD distance".ToLower();
        public static readonly String OBJ_CHILD_LOD_DISTANCE    = "Child LOD distance".ToLower();
        public static readonly String OBJ_NO_TXD                = "No TXD".ToLower();
		public static readonly String OBJ_TXD					= "TXD".ToLower();
		public static readonly String OBJ_REAL_TXD				= "REALTXD".ToLower();
		public static readonly String OBJ_COLLISION_GROUP		= "Collision Group".ToLower();
		public static readonly String OBJ_IPL_GROUP			= "IPL Group".ToLower();
		public static readonly String OBJ_AREA_CODE			= "Area Code".ToLower();
        public static readonly String OBJ_DRAW_LAST			= "Draw last".ToLower();
		public static readonly String OBJ_EXPORT_VERTEX_COLOURS= "Export Vertex Colours".ToLower();
		public static readonly String OBJ_LEVEL_DESIGN_OBJECT	= "Level Design Object".ToLower();
		public static readonly String OBJ_BREAKABLE			= "Breakable".ToLower();		
		public static readonly String OBJ_DONT_COLLIDE_WITH_FLYER = "Dont Collide With Flyer".ToLower();
		public static readonly String OBJ_DONT_ADD_TO_IPL		= "Dont Add To IPL".ToLower();
		public static readonly String OBJ_DONT_EXPORT			= "Dont Export".ToLower();
		public static readonly String OBJ_IS_DYNAMIC			= "Is Dynamic".ToLower();
		public static readonly String OBJ_IS_FIXED				= "Is Fixed".ToLower();
		public static readonly String OBJ_BACKFACE_CULL		= "Backface Cull".ToLower();
		public static readonly String OBJ_LOW_PRIORITY			= "Low Priority".ToLower();
		public static readonly String OBJ_USE_FULL_MATRIX		= "Use Full Matrix".ToLower();
		public static readonly String OBJ_NON_STREAMED_INST	= "Non Streamed Instance".ToLower();
        public static readonly String OBJ_USE_ALTERNATE_FADE_DISTANCE = "Use Alternate Fade Distance".ToLower();
		public static readonly String OBJ_IS_UNDERWATER		= "Is Underwater".ToLower();
		public static readonly String OBJ_IS_TUNNEL			= "Is Tunnel".ToLower();
		public static readonly String OBJ_IS_TUNNEL_TRANSITION	= "Is Tunnel Transition".ToLower();
		public static readonly String OBJ_DOES_NOT_PROVIDE_AI_COVER = "Does Not Provide AI Cover".ToLower();
		public static readonly String OBJ_BREAK_USE_PIVOT		= "Break Use Pivot".ToLower();
		public static readonly String OBJ_MILO_PREVIEW			= "Milo Preview".ToLower();
        public static readonly String OBJ_GROUP_NAME =  "groupName".ToLower();
        public static readonly String OBJ_ANIM_STATE = "animState".ToLower();
        public static readonly String OBJ_IS_CLOTH = "Is Cloth".ToLower();
        public static readonly String OBJ_IS_FRAGMENT = "Is Fragment".ToLower();
		public static readonly String OBJ_IS_FRAGMENT_PROXY	    = "Is FragProxy".ToLower();
        public static readonly String OBJ_IS_ANIMPROXY          = "Is AnimProxy".ToLower();
        public static readonly String OBJ_INSTANCE              = "Instanced".ToLower();
		public static readonly String OBJ_DONT_CAST_SHADOWS	    = "Dont cast shadows".ToLower();
		public static readonly String OBJ_CAST_TEXTURE_SHADOWS	= "Cast texture shadows".ToLower();
		public static readonly String OBJ_BLOCK_ID				= "BlockID".ToLower();
		public static readonly String OBJ_ATTRIBUTE			= "Attribute".ToLower();
		public static readonly String OBJ_IS_LADDER			= "Is Ladder".ToLower();
		public static readonly String OBJ_HAS_DOOR_PHYSICS		= "Has Door Physics".ToLower();
		public static readonly String OBJ_PACK_TEXTURES		= "Pack Textures".ToLower();
		public static readonly String OBJ_DOES_NOT_TOUCH_WATER	= "Does Not Touch Water".ToLower();
		public static readonly String OBJ_IS_FIXED_FOR_NAVIGATION = "Is Fixed For Navigation".ToLower();
		public static readonly String OBJ_ANIM					= "Anim".ToLower();
		public static readonly String OBJ_HAS_ANIM				= "Has Anim".ToLower();
		public static readonly String OBJ_HAS_UV_ANIM			= "Has UvAnim".ToLower();
		public static readonly String OBJ_DONT_AVOID_BY_PEDS	= "Don't Avoid By Peds".ToLower();
		public static readonly String OBJ_NEW_FRAG				= "New Frag".ToLower();
		public static readonly String OBJ_HOLD_FOR_LOD			= "Hold for LOD".ToLower();
		public static readonly String OBJ_INSTANCE_LOD_DISTANCE= "Instance LOD distance".ToLower();
		public static readonly String OBJ_HAND_EDITED_COLLISION= "Hand edited collision".ToLower();
		public static readonly String OBJ_ENABLE_AMBIENT_MULTIPLIER = "Enable Ambient Multiplier".ToLower();
		public static readonly String OBJ_ALWAYS_LOADED		= "Always Loaded".ToLower();
		public static readonly String OBJ_EDITED_BY_LEEDS		= "Edited by Leeds".ToLower();
		public static readonly String OBJ_SHADOW_ONLY			= "Shadow Only".ToLower();
		public static readonly String OBJ_DISABLE_PED_SPAWNING	= "Disable Ped Spawning".ToLower();
		public static readonly String OBJ_STATIC_SHADOWS		= "Static Shadows".ToLower();
		public static readonly String OBJ_DYNAMIC_SHADOWS		= "Dynamic Shadows".ToLower();
		public static readonly String OBJ_ON_ALL_DAY			= "On all day".ToLower();
		public static readonly String OBJ_SUPERLOD_WITH_ALPHA	= "Superlod with Alpha".ToLower();
		public static readonly String OBJ_TOUGH_FOR_BULLETS	= "Tough for Bullets".ToLower();
		public static readonly String OBJ_INSTANCE_COLLISION	= "Instance Collision".ToLower();
		public static readonly String OBJ_AUTHORED_ORIENTS		= "Authored Orients".ToLower();
		public static readonly String OBJ_DONT_ADD_TO_IDE		= "Dont Add To IDE".ToLower();
		public static readonly String OBJ_REVERSE_COVER_FOR_PLAYER = "Reverse Cover For Player".ToLower();
		public static readonly String OBJ_DAY_NIGHT_AMBIENT	= "Day/Night Ambient".ToLower();
		public static readonly String OBJ_PRIORITY				= "Priority".ToLower();
		public static readonly String OBJ_EXPORT_GEOMETRY		= "Export Geometry".ToLower();
		public static readonly String OBJ_ALLOW_ANY_LOD_DISTANCE	= "Allow Any Lod Distance".ToLower();
		public static readonly String OBJ_MODEL_GROUP			= "Model Group".ToLower();
        public static readonly String OBJ_AMBIENT_OCCLUSION_MULTIPLIER = "AO Multiplier".ToLower();
        public static readonly String OBJ_ARTIFICIAL_AMBIENT_OCCLUSION_MULTIPLIER = "Artificial AO Multiplier".ToLower();
        public static readonly String OBJ_IS_DEBUG              = "Is Debug".ToLower();
        public static readonly String OBJ_LIGHT_GROUP           = "Light Group".ToLower();
        public static readonly String OBJ_ADOPT_ME              = "Adopt Me".ToLower();
        public static readonly String OBJ_TINT = "Tint".ToLower();
        public static readonly String OBJ_IS_OCCLUSION = "Is Occlusion".ToLower();
        public static readonly String OBJ_IS_WATER_ONLY_OCCLUSION = "Is Water Only Occlusion".ToLower();
        public static readonly String OBJ_DONT_RENDER_IN_SHADOWS = "Dont Render In Shadows".ToLower();
        public static readonly String OBJ_ONLY_RENDER_IN_SHADOWS = "Only Render In Shadows".ToLower();
        public static readonly String OBJ_DONT_RENDER_IN_REFLECTIONS = "Dont Render In Reflections".ToLower();
        public static readonly String OBJ_ONLY_RENDER_IN_REFLECTIONS = "Only Render In Reflections".ToLower();
        public static readonly String OBJ_DONT_RENDER_IN_WATER_REFLECTIONS = "Dont Render In Water Reflections".ToLower();
        public static readonly String OBJ_ONLY_RENDER_IN_WATER_REFLECTIONS = "Only Render In Water Reflections".ToLower();
        public static readonly String OBJ_DONT_RENDER_IN_MIRROR = "Dont Render In Mirror".ToLower();
        public static readonly String OBJ_ONLY_RENDER_IN_MIRROR = "Only Render In Mirror".ToLower();
        public static readonly String OBJ_DONT_RENDER_IN_MIRROR_REFLECTIONS = "Dont Render In Mirror Reflections".ToLower();
        public static readonly String OBJ_ONLY_RENDER_IN_MIRROR_REFLECTIONS = "Only Render In Mirror Reflections".ToLower();
        public static readonly String OBJ_CLIMBABLE_BY_AI = "Climbable by AI".ToLower();
        public static readonly String OBJ_DONT_WRITE_ZBUFFER = "Dont Write to Z Buffer".ToLower();
        public static readonly String OBJ_ALLOW_NON_UNIFORM_SCALED_INSTANCES = "Allow Non-Uniform Scaled Instances".ToLower();
        public static readonly String OBJ_FORCE_BAKE_COLLISION = "Force Bake Collision".ToLower();
        public static readonly String OBJ_NEVER_BAKE_COLLISION = "Never Bake Collision".ToLower();
        public static readonly String OBJ_IS_TREE = "Is Tree".ToLower();
        public static readonly String OBJ_HD_TEXTURES_DISTANCE = "HD Textures Distance".ToLower();
        public static readonly String OBJ_SUPPRESS_HD_TEXTURES = "Suppress HD Textures".ToLower();
        public static readonly String OBJ_GROUPID = "Group ID".ToLower();
        public static readonly String OBJ_OVERRIDE_PHYSICS_BOUNDS = "Override Physics Bounds".ToLower();
        public static readonly String OBJ_AUTOSTART_ANIM = "Autostart Anim".ToLower();
        public static readonly String OBJ_DEFAULT_ENTITY_SETS = "Default Entity Sets".ToLower();
        public static readonly String OBJ_FORCE_ZERO_PRIORITY = "Force 0 priority".ToLower();
        public static readonly String OBJ_CRITICAL = "Critical".ToLower();
        public static readonly String OBJ_NEW_DLC = "New DLC".ToLower();
        public static readonly string OBJ_TXD_DLC = "txd dlc";
        public static readonly String OBJ_HAS_PRE_REFLECTED_WATER_PROXY = "Has Pre-Reflected Water Proxy".ToLower();
        public static readonly String OBJ_HAS_DRAWABLE_PROXY_FOR_WATER_REFLECTIONS = "Has Drawable Proxy For Water Reflections".ToLower();
        public static readonly String OBJ_PERMANENT_ARCHTYPE = "Permanent Archetype".ToLower();
        public static readonly String OBJ_HAS_ALPHA_SHADOW = "Has Alpha Shadow".ToLower();
        public static readonly String OBJ_IGNORE_IN_BOUND_CALC = "Ignore In Bound Calculation".ToLower();

        // Milo instance flags
        public static readonly String OBJ_MILO_INSTANCE_TURN_ON_GPS = "Turn On GPS".ToLower();
        public static readonly String OBJ_MILO_INSTANCE_CAP_ALPHA = "Cap Alpha of Interior Contents".ToLower();
        public static readonly String OBJ_MILO_INSTANCE_SHORT_FADE_DISTANCE = "Short Fade Distance".ToLower();

        // Dead DLC flags.
        [Obsolete("No longer available.", true)]
        public static readonly String OBJ_REPLACED_DLC = "Replaced DLC".ToLower();
        [Obsolete("No longer available.", true)]
        public static readonly String OBJ_TXDDLC_DLC = "TXD DLC".ToLower();

		// Likely obsolete -- DHM 18 November 2008 (required for IDE)
		[Obsolete]
        public static readonly String OBJ_LIGHT_REFLECTION		= "Light reflection".ToLower();
        [Obsolete]
        public static readonly String OBJ_DO_NOT_FADE = "Do not fade".ToLower();
        [Obsolete]
        public static readonly String OBJ_NO_ZBUFFER_WRITE = "No Zbuffer write".ToLower();

        #region Gta Time
        //----------------------------------------------------------------
		// Gta Object : Time
		//----------------------------------------------------------------			
		public static readonly String OBJ_IS_TIME_OBJECT = "Is time object".ToLower();
		public static readonly String TOBJ_ALLOW_VANISH_WHILST_VIEWED = "Allow Vanish Whilst Viewed".ToLower();
		public static readonly String TOBJ_TIME_1		= "1".ToLower();
		public static readonly String TOBJ_TIME_2		= "2".ToLower();
		public static readonly String TOBJ_TIME_3		= "3".ToLower();
		public static readonly String TOBJ_TIME_4		= "4".ToLower();
		public static readonly String TOBJ_TIME_5		= "5".ToLower();
		public static readonly String TOBJ_TIME_6		= "6".ToLower();
		public static readonly String TOBJ_TIME_7		= "7".ToLower();
		public static readonly String TOBJ_TIME_8		= "8".ToLower();
		public static readonly String TOBJ_TIME_9		= "9".ToLower();
		public static readonly String TOBJ_TIME_10		= "10".ToLower();
		public static readonly String TOBJ_TIME_11		= "11".ToLower();
		public static readonly String TOBJ_TIME_12		= "12".ToLower();
		public static readonly String TOBJ_TIME_13		= "13".ToLower();
		public static readonly String TOBJ_TIME_14		= "14".ToLower();
		public static readonly String TOBJ_TIME_15		= "15".ToLower();
		public static readonly String TOBJ_TIME_16		= "16".ToLower();
		public static readonly String TOBJ_TIME_17		= "17".ToLower();
		public static readonly String TOBJ_TIME_18		= "18".ToLower();
		public static readonly String TOBJ_TIME_19		= "19".ToLower();
		public static readonly String TOBJ_TIME_20		= "20".ToLower();
		public static readonly String TOBJ_TIME_21		= "21".ToLower();
		public static readonly String TOBJ_TIME_22		= "22".ToLower();
		public static readonly String TOBJ_TIME_23		= "23".ToLower();
		public static readonly String TOBJ_TIME_24		= "24".ToLower();
        #endregion // Gta Time
        #endregion // Gta Object

        #region Gta Collision
        //--------------------------------------------------------------------------
		// Gta Collision
		//--------------------------------------------------------------------------		
        public static readonly String COLL_AUDIO_MATERIAL = "Audio Material".ToLower();
        public static readonly String COLL_COMPOSTIE_ORDER = "Composite Order".ToLower();
        public static readonly String COLL_ROOM_ID = "Room ID".ToLower();
        #endregion // Gta Collision

        #region Gta Block
        //--------------------------------------------------------------------------
		// Gta Block
		//--------------------------------------------------------------------------
		public static readonly String BLOCK_BLOCK_ID		= "BlockID".ToLower();
        public static readonly String BLOCK_OWNER = "Owner".ToLower();
		public static readonly String BLOCK_ARTIST		= "Artist".ToLower();
		public static readonly String BLOCK_DATE_EDITED	= "Date Edited".ToLower();
		public static readonly String BLOCK_FIRST_PASS	= "1st Pass".ToLower();
		public static readonly String BLOCK_SECOND_PASS_STARTED	= "2nd Pass".ToLower();
		public static readonly String BLOCK_SECOND_PASS_OUTSOURCED		= "2nd Pass Outsourced".ToLower();
		public static readonly String BLOCK_SECOND_PASS_COMPLETE	= "2nd Pass Complete".ToLower();
		public static readonly String BLOCK_THIRD_PASS_COMPLETE = "3rd Pass Complete".ToLower();
		public static readonly String BLOCK_COMPLETE		= "Complete".ToLower();
		public static readonly String BLOCK_MEMMODEL		= "MemModel".ToLower();
		public static readonly String BLOCK_MEMCOLLISION	= "MemCollision".ToLower();
		public static readonly String BLOCK_MEMTEXTURE	= "MemTexture".ToLower();
		public static readonly String BLOCK_NUMMODELS	= "NumModels".ToLower();
		public static readonly String BLOCK_NUMXREFS		= "NumXRefs".ToLower();
		public static readonly String BLOCK_FREEZE		= "Freeze".ToLower();
        #endregion // Gta Block

        #region Gta Milo
        //--------------------------------------------------------------------------
		// Gta Milo
		//--------------------------------------------------------------------------		
		public static readonly String MILO_IS_MLO_LOD = "Is MLO LOD".ToLower();
		public static readonly String MILO_DONT_EXPORT_MILO	= "Don\"t Export Milo".ToLower();
		public static readonly String MILO_PROPOGATE_INSTANCING = "Propogate Instancing".ToLower();
		public static readonly String MILO_OFFICE_POPULATION = "Office Population".ToLower();
		public static readonly String MILO_ALLOW_SPRINTING = "Allow Sprinting".ToLower();
		public static readonly String MILO_CUTSCENE_ONLY = "Cutscene Only".ToLower();
		public static readonly String MILO_LOD_WHEN_LOCKED = "LOD When Locked".ToLower();
		public static readonly String MILO_NO_WATER_REFLECTION = "No Water Reflection".ToLower();
		public static readonly String MILO_DONT_DRAW_IF_LOCKED = "Dont Draw If Locked".ToLower();
		public static readonly String MILO_DETAIL = "Detail".ToLower();
		public static readonly String MILO_PRIORITISE_DECALS = "Prioritise Decals/Alphas".ToLower();
        public static readonly String MILO_GROUPID = "Milo Group".ToLower();

        // Milo instance flags (for in-situ interiors)
        public static readonly String MILO_INSTANCE_TURN_ON_GPS = "Turn On GPS".ToLower();
        public static readonly String MILO_INSTANCE_CAP_ALPHA = "Cap Alpha of Interior Contents".ToLower();
        public static readonly String MILO_INSTANCE_SHORT_FADE_DISTANCE = "Short Fade Distance".ToLower();
        #endregion Gta Milo

        #region Gta MiloTri
        //--------------------------------------------------------------------------
		// Gta MiloTri
		//--------------------------------------------------------------------------		
		public static readonly String MILOTRI_DONT_EXPORT_MILO_TRI = "Don't Export Milo Tri".ToLower();
        public static readonly String MILOTRI_GROUPID = "Milo Group";
        public static readonly String MILOTRI_FLOORID = "Milo Floor";
        #endregion // Gta MiloTri

        #region Gta MloRoom
        //--------------------------------------------------------------------------
		// Gta MloRoom
		//--------------------------------------------------------------------------		
        public static readonly String MLOROOM_TIMECYCLE			= "Timecycle".ToLower();
        public static readonly String MLOROOM_SECONDARY_TIMECYCLE = "Secondary Timecycle".ToLower();
		public static readonly String MLOROOM_BLEND				= "Blend".ToLower();
		public static readonly String MLOROOM_FREEZE_PEDS		= "Freeze Peds".ToLower();
		public static readonly String MLOROOM_FREEZE_CARS		= "Freeze Cars".ToLower();
		public static readonly String MLOROOM_REDUCE_PEDS		= "Reduce Peds".ToLower();
		public static readonly String MLOROOM_REDUCE_CARS		= "Reduce Cars".ToLower();
		public static readonly String MLOROOM_NO_DIRECTIONAL_LIGHT = "No Directional Light".ToLower();
		public static readonly String MLOROOM_NO_EXTERIOR_LIGHT	= "No Exterior Light".ToLower();
		public static readonly String MLOROOM_FORCE_FREEZE		= "Force Freeze".ToLower();
        public static readonly String MLOROOM_ROOM_FLOOR = "Room Floor".ToLower();
        public static readonly String MLOROOM_DEPTH = "Depth".ToLower();
        public static readonly String MLOROOM_FORCE_DIRECTIONAL_LIGHT = "Force Directional Light".ToLower();
        public static readonly String MLOROOM_DONT_RENDER_EXTERIOR = "Don't Render Exterior".ToLower();
        public static readonly String MLOROOM_MIRROR_POTENTIALLY_VISIBLE = "Mirror Potentially Visible".ToLower();
        #endregion // Gta MloRoom

        #region Gta MloPortal
        //--------------------------------------------------------------------------
		// Gta MloPortal
		//--------------------------------------------------------------------------
		public static readonly String MLOPORTAL_FADE_DISTANCE = "Fade Distance".ToLower();
		public static readonly String MLOPORTAL_RED = "Red".ToLower();
		public static readonly String MLOPORTAL_GREEN = "Green".ToLower();
		public static readonly String MLOPORTAL_BLUE = "Blue".ToLower();		
		public static readonly String MLOPORTAL_ONE_WAY = "One Way".ToLower();
		public static readonly String MLOPORTAL_MLO_LINK = "MLO Link".ToLower();		
        public static readonly String MLOPORTAL_IGNORE_BY_MODIFIER = "Ignored by Modifier".ToLower();		
		public static readonly String MLOPORTAL_EXPENSIVE_SHADERS = "Expensive Shaders".ToLower();
		public static readonly String MLOPORTAL_DRAW_LOW_LOD_ONLY = "Draw Low LOD Only".ToLower();
		public static readonly String MLOPORTAL_ALLOW_CLOSING = "Allow Closing".ToLower();
        public static readonly String MLOPORTAL_MIRROR_USES_DIRECTIONAL_LIGHT = "Mirror Uses Directional Light".ToLower();
        public static readonly String MLOPORTAL_MIRROR_PORTAL_TRAVERSAL = "Mirror Portal Traversal".ToLower();        
        public static readonly String MLOPORTAL_OPACITY = "Opacity".ToLower();
        public static readonly String MLOPORTAL_MIRROR_CAN_SEE_EXTERIOR_VIEW = "Mirror Can See Exterior View".ToLower();
        public static readonly String MLOPORTAL_USE_LIGHT_BLEED = "Use Light Bleed".ToLower();
       
        // AJM: 2 extra Mirror types have been added, but to restrict and simplify the setup in MAX it's now using a 
        // drop-down selection to select a type, which the serialiser then sets the correct flags for.  Leaving in Mirror 
        // and Mirror Floor for now although maxscript is going to get added to set the new drop-down box index based on any objects 
        // that already have either Mirror or Mirror Floor set
        public static readonly String MLOPORTAL_MIRROR = "Mirror".ToLower();
        public static readonly String MLOPORTAL_MIRROR_FLOOR = "Mirror Floor".ToLower();
        public static readonly String MLOPORTAL_MIRROR_TYPE = "Mirror Type".ToLower();
        public static readonly String MLOPORTAL_MIRROR_PRIORITY = "Mirror Priority".ToLower();
        #endregion // Gta MloPortal

        #region Gta CarGen
        //--------------------------------------------------------------------------
		// Cargen
		//--------------------------------------------------------------------------
        public static readonly String CARGEN_MODEL = "Model".ToLower();
        public static readonly String CARGEN_COLOUR1 = "Colour1".ToLower();
        public static readonly String CARGEN_COLOUR2 = "Colour2".ToLower();
        public static readonly String CARGEN_COLOUR3 = "Colour3".ToLower();
        public static readonly String CARGEN_COLOUR4 = "Colour4".ToLower();
        public static readonly String CARGEN_HIGH_PRIORITY = "High Priority".ToLower();
        public static readonly String CARGEN_IGNORE_POPULATION_LIMIT = "Ignore Population Limit".ToLower();
        public static readonly String CARGEN_ALARM_CHANCE = "Alarm Chance".ToLower();
        public static readonly String CARGEN_LOCKED_CHANCE = "Locked Chance".ToLower();
        public static readonly String CARGEN_MIN_DELAY = "Min Delay".ToLower();
        public static readonly String CARGEN_MAX_DELAY = "Max Delay".ToLower();
        public static readonly String CARGEN_POLICE = "Police".ToLower();
        public static readonly String CARGEN_FIRETRUCK = "Firetruck".ToLower();
        public static readonly String CARGEN_AMBULANCE = "Ambulance".ToLower();
        public static readonly String CARGEN_ALIGN_LEFT = "Align Left".ToLower();
        public static readonly String CARGEN_ALIGN_RIGHT = "Align Right".ToLower();
        public static readonly String CARGEN_SINGLE_PLAYER = "Single Player".ToLower();
        public static readonly String CARGEN_NETWORK = "Network".ToLower();
        public static readonly String CARGEN_CREATION_RULE = "Creation Rule".ToLower();
        public static readonly String CARGEN_SCENARIO = "Scenario".ToLower();
        public static readonly String CARGEN_ACTIVE_DAY = "Active During Day".ToLower();
        public static readonly String CARGEN_ACTIVE_NIGHT = "Active During Night".ToLower();
        public static readonly String CARGEN_MODEL_SET = "Model Set".ToLower();
        public static readonly String CARGEN_LOW_PRIORITY = "Low Priority".ToLower();
        #endregion // Gta CarGen

        #region VehicleNode / Vehicle Link
        //--------------------------------------------------------------------------
		// Vehicle Node
		//--------------------------------------------------------------------------
		public static readonly String VEHNOD_DISABLED	= "Disabled".ToLower();
		public static readonly String VEHNOD_WATER		= "Water".ToLower();
        public static readonly String VEHNOD_ROAD_BLOCK = "RoadBlock".ToLower();
		public static readonly String VEHNOD_LOW_BRIDGE	= "LowBridge".ToLower();
		public static readonly String VEHNOD_SPEED		= "Speed".ToLower();
		public static readonly String VEHNOD_SPECIAL		= "Special".ToLower();
		public static readonly String VEHNOD_DENSITY		= "Density".ToLower();
		public static readonly String VEHNOD_STREETNAME	= "Streetname".ToLower();
		public static readonly String VEHNOD_HIGHWAY		= "Highway".ToLower();
		public static readonly String VEHNOD_NO_GPS		= "NoGps".ToLower();
		public static readonly String VEHNOD_TUNNEL		= "Tunnel".ToLower();
		public static readonly String VEHNOD_OPENSPACE	= "OpenSpace".ToLower();
        public static readonly String VEHNOD_CANNOTGOLEFT	= "Cannot Go Left".ToLower();
        public static readonly String VEHNOD_LEFTTURNSONLY = "Left Turns Only".ToLower();
        public static readonly String VEHNOD_OFF_ROAD = "Off Road".ToLower();
        public static readonly String VEHNOD_CANNOT_GO_RIGHT = "Cannot Go Right".ToLower();
        public static readonly String VEHNOD_NO_BIG_VEHICLES = "No Big Vehicles".ToLower();
        public static readonly String VEHNOD_INDICATE_KEEP_LEFT = "Indicate Keep Left".ToLower();
        public static readonly String VEHNOD_INDICATE_KEEP_RIGHT = "Indicate Keep Right".ToLower();
        public static readonly String VEHNOD_SLIP_LANE = "Slip Lane".ToLower();

		
		//--------------------------------------------------------------------------
		// Vehicle Link
		//--------------------------------------------------------------------------
		public static readonly String VEHLNK_LANES_IN	= "Lanes In".ToLower();
		public static readonly String VEHLNK_LANES_OUT	= "Lanes Out".ToLower();
		public static readonly String VEHLNK_WIDTH		= "Width".ToLower();
		public static readonly String VEHLNK_NARROW_ROAD	= "Narrowroad".ToLower();
		public static readonly String VEHLNK_GPS_BOTH_WAYS = "GpsBothWays".ToLower();
        public static readonly String VEHLNK_BLOCK_IF_NO_LANES   = "Block If No Lanes".ToLower();
        public static readonly String VEHLNK_SHORTCUT = "Shortcut".ToLower();
        public static readonly String VEHLNK_NONAVIGATION = "Dont Use For Navigation".ToLower();
        #endregion // VehicleNode / VehicleLink

        #region PatrolNode / PatrolLink
        //--------------------------------------------------------------------------
		// Patrol Node
		//--------------------------------------------------------------------------
		public static readonly String PATNOD_DURATION	= "Patrol Duration".ToLower();
		public static readonly String PATNOD_HEADING_X	= "Heading X".ToLower();
		public static readonly String PATNOD_HEADING_Y	= "Heading Y".ToLower();
		public static readonly String PATNOD_HEADING_Z	= "Heading Z".ToLower();
		public static readonly String PATNOD_ASSOCIATED_BASE = "Associated Base".ToLower();
		public static readonly String PATNOD_ROUTE_NAME	= "Route Name".ToLower();
		public static readonly String PATNOD_PATROL_TYPE	= "Patrol Type".ToLower();
		
		//--------------------------------------------------------------------------
		// Patrol Link
		//--------------------------------------------------------------------------
        // Currently empty.
        #endregion // PatrolNode / PatrolLink

        #region Gta Particle
        //--------------------------------------------------------------------------
		// Gta Particle
		//--------------------------------------------------------------------------		
		public static readonly String TWODFX_PARTICLE_NAME		= "Name".ToLower();
		public static readonly String TWODFX_PARTICLE_TRIGGER	= "Trigger".ToLower();
        public static readonly String TWODFX_PARTICLE_IS_TRIGGERED = "Is Triggered".ToLower();
        public static readonly String TWODFX_PARTICLE_ATTACH = "Attach".ToLower();
		public static readonly String TWODFX_PARTICLE_ATTACH_TO_ALL = "Attach to All".ToLower();
		public static readonly String TWODFX_PARTICLE_SCALE	= "Scale".ToLower();
		public static readonly String TWODFX_PARTICLE_PROBABILITY = "Probability".ToLower();
		public static readonly String TWODFX_PARTICLE_COLOUR_R	= "R".ToLower();
		public static readonly String TWODFX_PARTICLE_COLOUR_G	= "G".ToLower();
		public static readonly String TWODFX_PARTICLE_COLOUR_B	= "B".ToLower();
        public static readonly String TWODFX_PARTICLE_COLOUR_A = "A".ToLower();
        public static readonly String TWODFX_PARTICLE_SIZE_X = "Emitter Size X".ToLower();
        public static readonly String TWODFX_PARTICLE_SIZE_Y = "Emitter Size Y".ToLower();
        public static readonly String TWODFX_PARTICLE_SIZE_Z = "Emitter Size Z".ToLower();
        public static readonly String TWODFX_PARTICLE_DONTEXPORT = "Dont Export".ToLower();
		// Flags
		public static readonly String TWODFX_PARTICLE_IGNORE_DAMAGE_MODEL = "Ignore Damage Model".ToLower();
		public static readonly String TWODFX_PARTICLE_PLAY_ON_PARENT = "Play On Parent".ToLower();
        public static readonly String TWODFX_PARTICLE_HAS_TINT = "Has Tint".ToLower();
        public static readonly String TWODFX_PARTICLE_ONLY_ON_DAMAGE_MODEL = "Only on damage model".ToLower();
        public static readonly String TWODFX_PARTICLE_ALLOW_RUBBER_BULLET_SHOT_FX = "Allow Rubber Bullet Shot FX".ToLower();
        public static readonly String TWODFX_PARTICLE_ALLOW_ELECTRIC_BULLET_SHOT_FX = "Allow Electric Bullet Shot FX".ToLower();
        #endregion // Gta Particle

        #region Rs Decal
        //--------------------------------------------------------------------------
        // Rs Decal
        //--------------------------------------------------------------------------	
        public static readonly String TWODFX_DECAL_NAME = "Decal Name".ToLower();
        public static readonly String TWODFX_DECAL_TRIGGER = "Trigger".ToLower();
        public static readonly String TWODFX_DECAL_ATTACH = "Attach".ToLower();
        public static readonly String TWODFX_DECAL_ATTACH_TO_ALL = "Attach To All".ToLower();
        public static readonly String TWODFX_DECAL_SCALE = "Scale".ToLower();
        public static readonly String TWODFX_DECAL_PROBABILITY = "Probability".ToLower();
        // Flags
        public static readonly String TWODFX_DECAL_IGNORE_DAMAGE_MODEL = "Ignore Damage Model".ToLower();
        public static readonly String TWODFX_DECAL_PLAY_ON_PARENT = "Play On Parent".ToLower();
        public static readonly String TWODFX_DECAL_ONLY_ON_DAMAGE_MODEL = "Only On Damage Model".ToLower();
        public static readonly String TWODFX_DECAL_ALLOW_RUBBER_BULLET_SHOT_FX = "Allow Rubber Bullet Shot FX".ToLower();
        public static readonly String TWODFX_DECAL_ALLOW_ELECTRIC_BULLET_SHOT_FX = "Allow Electric Bullet Shot FX".ToLower();
        #endregion // Rs Decal

        #region Gta Explosion
        //--------------------------------------------------------------------------
        // Gta Explosion
        //--------------------------------------------------------------------------
        public static readonly String TWODFX_EXPLOSION_ATTACH = "Attach".ToLower();
        public static readonly String TWODFX_EXPLOSION_ATTACH_TO_ALL = "Attach to All".ToLower();
        public static readonly String TWODFX_EXPLOSION_TRIGGER = "Trigger".ToLower();
        public static readonly String TWODFX_EXPLOSION_NAME = "Explosion Name".ToLower();

        // Flags
        public static readonly String TWODFX_EXPLOSION_IGNORE_DAMAGE_MODEL = "Ignore Damage Model".ToLower();
        public static readonly String TWODFX_EXPLOSION_PLAY_ON_PARENT = "Play On Parent".ToLower();
        public static readonly String TWODFX_EXPLOSION_ONLY_ON_DAMAGE_MODEL = "Only on damage model".ToLower();
        public static readonly String TWODFX_EXPLOSION_ALLOW_RUBBER_BULLET_SHOT_FX = "Allow Rubber Bullet Shot FX".ToLower();
        public static readonly String TWODFX_EXPLOSION_ALLOW_ELECTRIC_BULLET_SHOT_FX = "Allow Electric Bullet Shot FX".ToLower();
        #endregion // Gta Explosion

        #region Gta Light / Gta LightPhoto
        //--------------------------------------------------------------------------
		// Gta Light and Gta LightPhoto
		//--------------------------------------------------------------------------		
		public static readonly String TWODFX_LIGHT_CORONA_SIZE		= "Corona size".ToLower();
		public static readonly String TWODFX_LIGHT_FLASHINESS		= "Flashiness".ToLower();
		public static readonly String TWODFX_LIGHT_ATTACH			= "Attach".ToLower();
		public static readonly String TWODFX_LIGHT_VOLUME_SIZE		= "Volume Size".ToLower();
		public static readonly String TWODFX_LIGHT_VOLUME_INTENSITY	= "Volume Intensity".ToLower();
		public static readonly String TWODFX_LIGHT_CORONA_INTENSITY = "Corona Intensity".ToLower();
        public static readonly String TWODFX_LIGHT_CORONA_ZBIAS     = "Corona Z-Bias".ToLower();
        public static readonly String TWODFX_LIGHT_SHADOW_ALPHA     = "Shadow Alpha".ToLower();
        public static readonly String TWODFX_LIGHT_SHADOW_NAME      = "Shadow name".ToLower();
        public static readonly String TWODFX_LIGHT_LIGHT_FADE_DISTANCE = "Light Fade Distance".ToLower();
        public static readonly String TWODFX_LIGHT_VOLUME_FADE_DISTANCE = "Volume Fade Distance".ToLower();
        public static readonly String TWODFX_LIGHT_SHADOW_FADE_DISTANCE = "Shadow Fade Distance".ToLower();
        public static readonly String TWODFX_LIGHT_SPECULAR_FADE_DISTANCE = "Specular Fade Distance".ToLower();
        public static readonly String TWODFX_LIGHT_SHADOW_NEAR_CLIP = "Shadow Near Clip".ToLower();
        public static readonly String TWODFX_LIGHT_SHADOW_BLUR =    "Shadow Blur".ToLower();
		
		// Flags
// 		public static readonly String TWODFX_LIGHT_FADE_BETWEEN_DAY_AND_NIGHT = "Fade Between Day and Night".ToLower();
// 		public static readonly String TWODFX_LIGHT_DAY				= "Day".ToLower();
// 		public static readonly String TWODFX_LIGHT_NIGHT			    = "Night".ToLower();
		public static readonly String TWODFX_LIGHT_IS_ELECTRIC		= "Is Electric".ToLower();
//		public static readonly String TWODFX_LIGHT_ONLY_FROM_BELOW	= "Only From Below".ToLower();
		public static readonly String TWODFX_LIGHT_STROBE			= "Strobe".ToLower();
		public static readonly String TWODFX_LIGHT_PLANE			    = "Plane".ToLower();
		public static readonly String TWODFX_LIGHT_CAST_STATIC_SHADOWS = "Cast Static Object Shadow".ToLower();
		public static readonly String TWODFX_LIGHT_CAST_DYNAMIC_SHADOWS = "Cast Dynamic Object Shadow".ToLower();
		public static readonly String TWODFX_LIGHT_CALC_FROM_SUNLIGHT = "Calc From Sunlight".ToLower();
//        public static readonly String TWODFX_LIGHT_CALC_FROM_EXTERIOR_AMBIENT = "Calc From Exterior Ambient".ToLower();
		public static readonly String TWODFX_LIGHT_ENABLE_BUZZING	= "Enable Buzzing".ToLower();
		public static readonly String TWODFX_LIGHT_FORCE_BUZZING	    = "Force Buzzing".ToLower();
		public static readonly String TWODFX_LIGHT_VOLUME_DRAWING	= "Volume Drawing".ToLower();
		public static readonly String TWODFX_LIGHT_NO_SPECULAR		= "No Specular".ToLower();
//         public static readonly String TWODFX_LIGHT_BOUNCE_LIGHT     = "Bounce Light".ToLower();
        public static readonly String TWODFX_LIGHT_DONT_EXPORT      = "Dont Export".ToLower();
        public static readonly String TWODFX_LIGHT_INT_AND_EXT      = "INT and EXT".ToLower();
        public static readonly String TWODFX_LIGHT_CORONA_ONLY      = "Corona Only".ToLower();
        public static readonly String TWODFX_LIGHT_RENDER_IN_REFLECTION = "Render In Reflection".ToLower();
        public static readonly String TWODFX_LIGHT_DISABLE_IN_CUTSCENE = "Disable In Cutscene".ToLower();
        public static readonly String TWODFX_LIGHT_CAST_HIGHER_RES_SHADOW = "Cast Higher Res Shadow".ToLower();
        public static readonly String TWODFX_LIGHT_CAST_ONLY_LOWRES_SHADOWS = "Cast Only Lower Res Shadows".ToLower();
        public static readonly String TWODFX_LIGHT_ADD_TO_LOD_LIGHTS = "Add To Lod Lights".ToLower();
        public static readonly String TWODFX_LIGHT_DONT_LIGHT_ALPHA = "Dont Light Alpha".ToLower();
        public static readonly String TWODFX_LIGHT_SHADOWS_IF_POSSIBLE = "Shadows If Possible".ToLower();
        public static readonly String TWODFX_LIGHT_ADD_TO_VIP_LOD_LIGHTS = "Add To VIP LOD Lights".ToLower();
        public static readonly String TWODFX_LIGHT_FORCE_MEDIUM_LOD_LIGHT = "Force Medium Lod Light".ToLower();
        public static readonly String TWODFX_LIGHT_CORONA_ONLY_LOD_LIGHT = "Corona Only Lod Light".ToLower();
        #endregion // Gta Light / Gta LightPhoto

        #region Gta LightShaft
        //--------------------------------------------------------------------------
        // Gta LightShaft
        //--------------------------------------------------------------------------		
//         public static readonly String TWODFX_LIGHTSHAFT_DENSITY = "Density".ToLower();
        public static readonly String TWODFX_LIGHTSHAFT_LENGTH = "Length".ToLower();
        public static readonly String TWODFX_LIGHTSHAFT_INTENSITY = "Intensity".ToLower();
        public static readonly String TWODFX_LIGHTSHAFT_DIRECTION_X = "Direction X".ToLower();
        public static readonly String TWODFX_LIGHTSHAFT_DIRECTION_Y = "Direction Y".ToLower();
        public static readonly String TWODFX_LIGHTSHAFT_DIRECTION_Z = "Direction Z".ToLower();
        public static readonly String TWODFX_LIGHTSHAFT_COLOUR_R = "R".ToLower();
        public static readonly String TWODFX_LIGHTSHAFT_COLOUR_G = "G".ToLower();
        public static readonly String TWODFX_LIGHTSHAFT_COLOUR_B = "B".ToLower();
        public static readonly String TWODFX_LIGHTSHAFT_USE_SUN_LIGHT_DIRECTION = "Use Sun Light Direction".ToLower();
        public static readonly String TWODFX_LIGHTSHAFT_USE_SUN_LIGHT_COLOUR = "Use Sun Light Colour".ToLower();
        public static readonly String TWODFX_LIGHTSHAFT_DENSITY_TYPE = "Density Type".ToLower();
        public static readonly String TWODFX_LIGHTSHAFT_VOLUME_TYPE = "Volume Type".ToLower();
        public static readonly String TWODFX_LIGHTSHAFT_SOFTNESS = "Softness".ToLower();
        public static readonly String TWODFX_LIGHTSHAFT_DIRECTION_AMOUNT = "Direction Amount".ToLower();
        public static readonly String TWODFX_LIGHTSHAFT_SCALE_BY_SUN_COLOUR = "Scale By Sun Colour".ToLower();
        public static readonly String TWODFX_LIGHTSHAFT_SCALE_BY_SUN_INTENSITY = "Scale By Sun Intensity".ToLower();
        public static readonly String TWODFX_LIGHTSHAFT_DRAW_IN_FRONT_AND_BEHIND = "Draw In Front And Behind".ToLower();
        public static readonly String TWODFX_LIGHTSHAFT_FADE_IN_TIME_START = "Fade In Time Start".ToLower();
        public static readonly String TWODFX_LIGHTSHAFT_FADE_IN_TIME_END = "Fade In Time End".ToLower();
        public static readonly String TWODFX_LIGHTSHAFT_FADE_OUT_TIME_START = "Fade Out Time Start".ToLower();
        public static readonly String TWODFX_LIGHTSHAFT_FADE_OUT_TIME_END = "Fade Out Time End".ToLower();
        public static readonly String TWODFX_LIGHTSHAFT_FADE_DISTANCE_START = "Fade Distance Start".ToLower();
        public static readonly String TWODFX_LIGHTSHAFT_FADE_DISTANCE_END = "Fade Distance End".ToLower();
        public static readonly String TWODFX_LIGHTSHAFT_FLASHINESS = "Flashiness".ToLower();
        public static readonly String TWODFX_LIGHTSHAFT_SQUARED_OFF_ENDCAP = "Squared Off Endcap".ToLower();

        //         public static readonly String TWODFX_LIGHTSHAFT_SHADOWED = "Shadowed".ToLower();
//         public static readonly String TWODFX_LIGHTSHAFT_PER_PIXEL_SHADOWED = "Per Pixel Shadowed".ToLower();
        #endregion // Gta LightShaft

        #region Gta Swayable
        //--------------------------------------------------------------------------
		// Gta Swayable
		//--------------------------------------------------------------------------		
		public static readonly String TWODFX_SWAYABLE_BONEID		    = "Attach".ToLower();
		public static readonly String TWODFX_SWAYABLE_LOW_WIND_SPEED = "Low Wind Sway Speed".ToLower();
		public static readonly String TWODFX_SWAYABLE_LOW_WIND_AMPLITUDE = "Low Wind Sway Amplitude".ToLower();
		public static readonly String TWODFX_SWAYABLE_HIGH_WIND_SPEED = "High Wind Sway Speed".ToLower();
		public static readonly String TWODFX_SWAYABLE_HIGH_WIND_AMPLITUDE = "High Wind Sway Amplitude".ToLower();
        #endregion // Gta Swayable

        #region Gta Ladder FX
        //--------------------------------------------------------------------------
		// Gta Ladders
		//--------------------------------------------------------------------------	
		public static readonly String TWODFX_LADDER_CAN_GET_OFF_AT_TOP = "Can Get Off At Top".ToLower();
        #endregion // Gta Ladder FX

        #region RS Ladder
        public static readonly String TWODFX_RSLADDER_CAN_GET_OFF_AT_TOP = "Can Get Off At Top".ToLower();
        public static readonly String TWODFX_RSLADDER_CAN_GET_OFF_AT_BOTTOM = "Can Get Off At Bottom".ToLower();
        #endregion // RS Ladder

        #region Gta TimeCycle
        //--------------------------------------------------------------------------
		// Gta TimeCycle
		//--------------------------------------------------------------------------	
        public static readonly String TWODFX_TCYC_BOX_PERCENTAGE    = "Percentage".ToLower();
        public static readonly String TWODFX_TCYC_BOX_START_HOUR = "StartHour".ToLower();
        public static readonly String TWODFX_TCYC_BOX_END_HOUR = "EndHour".ToLower();
        public static readonly String TWODFX_TCYC_BOX_IDSTRING = "IDString".ToLower();
        public static readonly String TWODFX_TCYC_BOX_RANGE = "Range".ToLower();

        public static readonly String TWODFX_TCYC_SPHERE_NAME = "Name".ToLower();
        public static readonly String TWODFX_TCYC_SPHERE_RANGE = "Range".ToLower();
        public static readonly String TWODFX_TCYC_SPHERE_PERCENTAGE = "Percentage".ToLower();
        public static readonly String TWODFX_TCYC_SPHERE_START_HOUR = "StartHour".ToLower();
        public static readonly String TWODFX_TCYC_SPHERE_END_HOUR = "EndHour".ToLower();
        #endregion // Gta TimeCycle

        #region Gta SpawnPoint
        //--------------------------------------------------------------------------
        // Gta SpawnPoint
        //--------------------------------------------------------------------------	 
        public static readonly String TWODFX_SPAWNPOINT_PED_TYPE = "ped type".ToLower();
        public static readonly String TWODFX_SPAWNPOINT_START_TIME = "time start override".ToLower();
        public static readonly String TWODFX_SPAWNPOINT_END_TIME = "time end override".ToLower();
        public static readonly String TWODFX_SPAWNPOINT_MODEL_SET = "Model Set".ToLower();
        public static readonly String TWODFX_SPAWNPOINT_AVAILABILITY = "Availability".ToLower();
        public static readonly String TWODFX_SPAWNPOINT_RADIUS = "Radius".ToLower();
        public static readonly String TWODFX_SPAWNPOINT_TIME_TILL_PED_LEAVES = "Time Till Ped Leaves".ToLower();

        #endregion // Gta SpawnPoint

        #region RsSpawnPointFlags
        //--------------------------------------------------------------------------
        // RsSpawnPointFlags
        //--------------------------------------------------------------------------
        public static readonly String SPAWNPOINTFLAGS_STATIONARY_REACTIONS = "Stationary Reactions".ToLower();
        public static readonly String SPAWNPOINTFLAGS_NO_SPAWN = "No Spawn".ToLower();
        #endregion // RsSpawnPointFlags

        #region Gta AudioEmit
        //--------------------------------------------------------------------------
        // Gta AudioEmit
        //--------------------------------------------------------------------------	
        public static readonly String TWODFX_AUDIO_EMITTER_EFFECT = "Effect".ToLower();
        #endregion // Gta AudioEmit

        #region Gta ProcObj
        //--------------------------------------------------------------------------
        // Gta ProcObj
        //--------------------------------------------------------------------------	
        public static readonly String TWODFX_PROC_OBJECT = "Object".ToLower();
        public static readonly String TWODFX_PROC_SPACING = "Spacing".ToLower();
        public static readonly String TWODFX_PROC_MIN_SCALE = "MinScale".ToLower();
        public static readonly String TWODFX_PROC_MAX_SCALE = "MaxScale".ToLower();
        public static readonly String TWODFX_PROC_MIN_SCALE_Z = "MinScaleZ".ToLower();
        public static readonly String TWODFX_PROC_MAX_SCALE_Z = "MaxScaleZ".ToLower();
        public static readonly String TWODFX_PROC_MIN_Z_OFFSET = "ZOffsetMin".ToLower();
        public static readonly String TWODFX_PROC_MAX_Z_OFFSET = "ZOffsetMax".ToLower();
        public static readonly String TWODFX_PROC_IS_ALIGNED = "IsAligned".ToLower();
        #endregion // Gta ProcObj

        #region Gta LodModifier
        //--------------------------------------------------------------------------
        // Gta LodModifier
        //--------------------------------------------------------------------------	
        public static readonly String LODMODIFIER_CULL_WATER = "Cull Water".ToLower();
        #endregion // Gta LodModifier

        #region RS Container
        // None required yet!
        #endregion // RS Container

        #region Gta Script
        //--------------------------------------------------------------------------
        // Gta Script
        //--------------------------------------------------------------------------
        public static readonly String SCRIPT_NAME = "Name".ToLower();
        #endregion // Gta Script

        #region RS PropGroup
        public static readonly String PROPGROUP_DONT_EXPORT = "Dont Export".ToLower();
        #endregion // RS PropGroup

        #region Rs Wind Disturbance
        public static readonly String TWODFX_WIND_ATTACH = "Attach".ToLower();
        public static readonly String TWODFX_WIND_TYPE = "Disturbance Type".ToLower();
        public static readonly String TWODFX_WIND_STRENGTH = "Strength".ToLower();
        public static readonly String TWODFX_WIND_STRENGTH_MULTIPLIES_GLOBAL_WIND = "Strength Multiplies Global Wind".ToLower();
        public static readonly String TWODFX_WIND_ATTACH_TO_ALL = "Attach To All".ToLower();
        #endregion // Rs Wind Disturbance

        #region RS Door
        //--------------------------------------------------------------------------
        // RS Door
        //--------------------------------------------------------------------------	
        public static readonly String TWODFX_DOOR_ENABLE_LIMIT_ANGLE = "Enable Limit Angle".ToLower();
        public static readonly String TWODFX_DOOR_STARTS_LOCKED = "Starts Locked".ToLower();
        public static readonly String TWODFX_DOOR_CAN_BREAK = "Can Break".ToLower();
        public static readonly String TWODFX_DOOR_LIMIT_ANGLE = "Limit Angle".ToLower();
        public static readonly String TWODFX_DOOR_DOOR_TARGET_RATIO = "Door Target Ratio".ToLower();
        #endregion // RS Door

        #region RS Expression
        //--------------------------------------------------------------------------
        // RS Expression
        //--------------------------------------------------------------------------	
        public static readonly String TWODFX_EXPRESSION_DICTIONARY_NAME = "Dictionary Name".ToLower();
        public static readonly String TWODFX_EXPRESSION_NAME = "Expression Name".ToLower();
        public static readonly String TWODFX_EXPRESSION_CREATURE_METADATA_NAME = "Creature Metadata Name".ToLower();
        public static readonly String TWODFX_EXPRESSION_INIT_ON_COLLISION = "Initialise On Collision".ToLower();
        #endregion // RS Expression

        #region RsStatedAnim
        public static readonly String STATEDANIM_ANIM_START = "Animation Start".ToLower();
        public static readonly String STATEDANIM_ANIM_END = "Animation End".ToLower();
        public static readonly String STATEDANIM_LOD_DISTANCE = "LOD distance".ToLower();
        #endregion

        #region RsActive
        public static readonly String RSACTIVE_ACTIVE = "Active".ToLower();
        #endregion

        #region Uncategorised
        //--------------------------------------------------------------------------
        // UNCATEGORISED
        //--------------------------------------------------------------------------	
        public static readonly String ADDITIVE = "Additive".ToLower();
        public static readonly String IS_SUBWAY = "Is Subway".ToLower();
        public static readonly String IGNORE_LIGHTING = "Ignore Lighting".ToLower();
        public static readonly String TIME_ON = "Time on".ToLower();
        public static readonly String TIME_OFF = "Time off".ToLower();
        public static readonly String NO_SHADOWS = "No Shadows".ToLower();
        public static readonly String SMASHABLE = "Smashable".ToLower();
        public static readonly String DISTANCE_ON = "Distance on".ToLower();
        public static readonly String DISTANCE_OFF = "Distance off".ToLower();
        public static readonly String TYPE = "Type".ToLower();
        public static readonly String START_TIME = "Start Time".ToLower();
        public static readonly String END_TIME = "End Time".ToLower();
        public static readonly String SPEED = "Speed".ToLower();
        public static readonly String SIZE = "Size".ToLower();
        public static readonly String INTEREST = "Interest".ToLower();
        public static readonly String LOOK_AT = "Look At".ToLower();
        public static readonly String DIRECTION = "Direction".ToLower();
        public static readonly String IS_GARAGE_DOOR = "Is Garage Door".ToLower();
        public static readonly String GROUP = "Group".ToLower();
        public static readonly String SEED = "Seed".ToLower();
        public static readonly String STATUS = "Status".ToLower();
        public static readonly String LOD_PARENT = "LOD Parent".ToLower();
        public static readonly String IS_TREE = "Is Tree".ToLower();
        public static readonly String EXPLODES_WHEN_SHOT = "Explodes When Shot".ToLower();
        public static readonly String ENCLOSED = "Enclosed".ToLower();
        public static readonly String STAIRS = "Stairs".ToLower();
        public static readonly String FIRST_PERSON = "1st Person".ToLower();
        public static readonly String STOP_RAINING = "Stop Raining".ToLower();
        public static readonly String NO_POLICE = "No Police".ToLower();
        public static readonly String IGNORE = "Ignore".ToLower();
        public static readonly String LOAD_COLLISION = "Load Collision".ToLower();
        public static readonly String CAN_SEE_SUBWAY = "Can see subway".ToLower();
        public static readonly String POLICE_WALK = "Police walk".ToLower();
        public static readonly String IN_ROOM_AUDIO = "In room audio".ToLower();
        public static readonly String FEWER_PEDS = "Fewer Peds".ToLower();
        public static readonly String WANTED_DROP_AMOUNT = "Wanted Drop Amount".ToLower();
        public static readonly String PICKUP_TYPE = "Pickup Type".ToLower();
        public static readonly String ROTATE = "Rotate".ToLower();
        public static readonly String RUBBISH = "Rubbish".ToLower();
        public static readonly String TITLE = "Title".ToLower();
        public static readonly String INVERT_ROTATION = "InvertRotation".ToLower();
        public static readonly String LEAVE_CAMERA = "LeaveCamera".ToLower();
        public static readonly String EXTRA_COLOUR = "Extra Colour".ToLower();
        public static readonly String IS_DOOR = "Is Door".ToLower();
        public static readonly String IS_CRANE = "Is Crane".ToLower();
        public static readonly String SHOP_ENTRANCE = "Shop Entrance".ToLower();
        public static readonly String FADE = "Fade".ToLower();
        public static readonly String LINKED = "Linked".ToLower();
        public static readonly String SHOP_EXIT = "Shop Exit".ToLower();
        public static readonly String LINE1 = "Linel".ToLower();
        public static readonly String LINE2 = "Line2".ToLower();
        public static readonly String LINE3 = "Line3".ToLower();
        public static readonly String LINE4 = "Line4".ToLower();
        public static readonly String WARP_CARS = "Warp Cars".ToLower();
        public static readonly String WARP_BIKES = "Warp Bikes".ToLower();
        public static readonly String NUM_LINES = "Num Lines".ToLower();
        public static readonly String NUM_LETTERS = "Num Letters".ToLower();
        public static readonly String DAY_BRIGHTNESS = "Day Brightness".ToLower();
        public static readonly String NIGHT_BRIGHTNESS = "Night Brightness".ToLower();
        public static readonly String DRAW_IN_WET_ONLY = "Draw In Wet Only".ToLower();

        public static readonly String INDEX = "Index".ToLower();
        public static readonly String USAGE = "Usage".ToLower();
        public static readonly String SCORE = "Score".ToLower();
        public static readonly String RULE = "Rule".ToLower();
        public static readonly String GOING_UP = "Going Up".ToLower();
        public static readonly String IS_TAG = "Is Tag".ToLower();
        public static readonly String OPEN_TIME = "Open Time".ToLower();
        public static readonly String CLOSE_TIME = "Close Time".ToLower();
        public static readonly String WARP_GANG = "Warp Gang".ToLower();
        public static readonly String DO_NOT_WARP = "Do not warp".ToLower();
        public static readonly String SCRIPT = "Script".ToLower();
        public static readonly String VISIBLE = "Visible".ToLower();
        public static readonly String SCREENS_1 = "Screens 1".ToLower();
        public static readonly String SCREENS_2 = "Screens 2".ToLower();
        public static readonly String SCREENS_3 = "Screens 3".ToLower();
        public static readonly String SCREENS_4 = "Screens 4".ToLower();
        public static readonly String SCREENS_5 = "Screens 5".ToLower();
        public static readonly String SCREENS_6 = "Screens 6".ToLower();
        public static readonly String NUM_RANDOM_PEDS = "Num Random Peds".ToLower();
        public static readonly String SOUND = "Sound".ToLower();
        public static readonly String ACTIVE = "Active".ToLower();
        public static readonly String RANDOM_OPEN_TIME = "Random Open Time".ToLower();
        public static readonly String PALETTE_ID = "Palette ID".ToLower();
        public static readonly String BURGLARY_HOUSE = "Burglary House".ToLower();
        public static readonly String TUNNEL_TRANSITION = "Tunnel Transition".ToLower();
        public static readonly String TUNNEL = "Tunnel".ToLower();
        public static readonly String MILITARY_AREA = "Military Area".ToLower();
        public static readonly String HAS_TUNNEL_COLOURS = "Has Tunnel Colours".ToLower();
        public static readonly String EXTRA_AIR_RESISTANCE = "Extra Air Resistance".ToLower();
        public static readonly String FEWER_CARS = "Fewer Cars".ToLower();
        public static readonly String IS_TRAFFIC_LIGHT = "Is Traffic Light".ToLower();
        public static readonly String IGNORE_POPULATION_LIMIT = "Ignore Population Limit".ToLower();
        public static readonly String ACTIVE_DURING_DAY = "Active During Day".ToLower();
        public static readonly String ACTIVE_DURING_NIGHT = "Active During Night".ToLower();
        public static readonly String WORK_TYPE = "Work Type".ToLower();
        public static readonly String HOME = "Home".ToLower();
        public static readonly String LEISURE_TYPE = "Leisure Type".ToLower();
        public static readonly String FOOD_TYPE = "Food Type".ToLower();
        #endregion // Uncategorised
        #endregion // Attribute Names
    }

    /// <summary>
    /// Attribute Default Value Constants.
    /// </summary>
    public static class AttrDefaults
    {
        #region Attribute Default Values
        #region Gta Object
        //----------------------------------------------------------------
        // Gta Object
        //----------------------------------------------------------------			
        public static readonly float OBJ_LOD_DISTANCE = 100.0f;
        public static readonly float OBJ_CHILD_LOD_DISTANCE = 0.0f;
        public static readonly String OBJ_TXD = "CHANGEME";
        public static readonly bool OBJ_NO_TXD = false;
        public static readonly String OBJ_REAL_TXD = "null";
        public static readonly bool OBJ_NEW_TXD = false;
        public static readonly String OBJ_COLLISION_GROUP = "undefined";
        public static readonly String OBJ_IPL_GROUP = "undefined";
        public static readonly int OBJ_AREA_CODE = 0;
        public static readonly bool OBJ_DRAW_LAST = false;
        public static readonly bool OBJ_EXPORT_VERTEX_COLOURS = true;
        public static readonly bool OBJ_LEVEL_DESIGN_OBJECT = false;
        public static readonly bool OBJ_BREAKABLE = false;
        public static readonly bool OBJ_DONT_COLLIDE_WITH_FLYER = false;
        public static readonly bool OBJ_DONT_ADD_TO_IPL = false;
        public static readonly bool OBJ_DONT_EXPORT = false;
        public static readonly bool OBJ_IS_DYNAMIC = false;
        public static readonly bool OBJ_IS_FIXED = false;
        public static readonly bool OBJ_BACKFACE_CULL = true;
        public static readonly bool OBJ_LOW_PRIORITY = false;
        public static readonly bool OBJ_USE_FULL_MATRIX = false;
        public static readonly bool OBJ_NON_STREAMED_INST = false;
        public static readonly bool OBJ_USE_ALTERNATE_FADE_DISTANCE = false;
        public static readonly bool OBJ_IS_UNDERWATER = false;
        public static readonly bool OBJ_IS_TUNNEL = false;
        public static readonly bool OBJ_IS_TUNNEL_TRANSITION = false;
        public static readonly bool OBJ_DOES_NOT_PROVIDE_AI_COVER = false;
        public static readonly bool OBJ_BREAK_USE_PIVOT = false;
        public static readonly bool OBJ_MILO_PREVIEW = true;
        public static readonly String OBJ_GROUP_NAME = "DEFAULT";
        public static readonly String OBJ_ANIM_STATE = "";
        public static readonly bool OBJ_IS_CLOTH = false;
        public static readonly bool OBJ_IS_FRAGMENT = false;
        public static readonly bool OBJ_IS_ANIMPROXY = false;
        public static readonly bool OBJ_IS_FRAGMENT_PROXY = false;
        public static readonly bool OBJ_INSTANCE = false;
        public static readonly bool OBJ_DONT_CAST_SHADOWS = false;
        public static readonly bool OBJ_CAST_TEXTURE_SHADOWS = false;
        public static readonly String OBJ_BLOCK_ID = "unknown";
        public static readonly int OBJ_ATTRIBUTE = 0;
        public static readonly bool OBJ_IS_LADDER = false;
        public static readonly bool OBJ_HAS_DOOR_PHYSICS = false;
        public static readonly bool OBJ_PACK_TEXTURES = false;
        public static readonly bool OBJ_DOES_NOT_TOUCH_WATER = false;
        public static readonly bool OBJ_IS_FIXED_FOR_NAVIGATION = false;
        public static readonly String OBJ_ANIM = "";
        public static readonly bool OBJ_HAS_ANIM = false;
        public static readonly bool OBJ_HAS_UV_ANIM = false;
        public static readonly bool OBJ_DONT_AVOID_BY_PEDS = false;
        public static readonly bool OBJ_NEW_FRAG = false;
        public static readonly bool OBJ_HOLD_FOR_LOD = false;
        public static readonly bool OBJ_INSTANCE_LOD_DISTANCE = false;
        public static readonly bool OBJ_HAND_EDITED_COLLISION = false;
        public static readonly bool OBJ_ENABLE_AMBIENT_MULTIPLIER = true;
        public static readonly bool OBJ_ALWAYS_LOADED = false;
        public static readonly bool OBJ_EDITED_BY_LEEDS = false;
        public static readonly bool OBJ_SHADOW_ONLY = false;
        public static readonly bool OBJ_DISABLE_PED_SPAWNING = false;
        public static readonly bool OBJ_STATIC_SHADOWS = true;
        public static readonly bool OBJ_DYNAMIC_SHADOWS = true;
        public static readonly bool OBJ_ON_ALL_DAY = false;
        public static readonly bool OBJ_SUPERLOD_WITH_ALPHA = false;
        public static readonly bool OBJ_TOUGH_FOR_BULLETS = false;
        public static readonly bool OBJ_INSTANCE_COLLISION = false;
        public static readonly bool OBJ_AUTHORED_ORIENTS = false;
        public static readonly bool OBJ_DONT_ADD_TO_IDE = false;
        public static readonly bool OBJ_REVERSE_COVER_FOR_PLAYER = false;
        public static readonly bool OBJ_DAY_NIGHT_AMBIENT = false;
        public static readonly int OBJ_PRIORITY = 0;
        public static readonly bool OBJ_EXPORT_GEOMETRY = true;
        public static readonly bool OBJ_ALLOW_ANY_LOD_DISTANCE = false;
        public static readonly String OBJ_MODEL_GROUP = "null";
        public static readonly bool OBJ_LIGHT_REFLECTION = false;
        public static readonly bool OBJ_DO_NOT_FADE = false;
        public static readonly bool OBJ_NO_ZBUFFER_WRITE = false;
        public static readonly bool OBJ_IS_DEBUG = false;
        public static readonly int OBJ_AMBIENT_OCCLUSION_MULTIPLIER = 255;
        public static readonly int OBJ_ARTIFICIAL_AMBIENT_OCCLUSION_MULTIPLIER = 255;
        public static readonly int OBJ_LIGHT_GROUP = 0;
        public static readonly bool OBJ_ADOPT_ME = false;
        public static readonly int OBJ_TINT = 0;
        public static readonly bool OBJ_IS_OCCLUSION = false;
        public static readonly bool OBJ_IS_WATER_ONLY_OCCLUSION = false;
        public static readonly bool OBJ_DONT_RENDER_IN_SHADOWS = false;
        public static readonly bool OBJ_ONLY_RENDER_IN_SHADOWS = false;
        public static readonly bool OBJ_DONT_RENDER_IN_REFLECTIONS = false;
        public static readonly bool OBJ_ONLY_RENDER_IN_REFLECTIONS = false;
        public static readonly bool OBJ_DONT_RENDER_IN_WATER_REFLECTIONS = false;
        public static readonly bool OBJ_ONLY_RENDER_IN_WATER_REFLECTIONS = false;
        public static readonly bool OBJ_DONT_RENDER_IN_MIRROR = false;
        public static readonly bool OBJ_ONLY_RENDER_IN_MIRROR = false;
        public static readonly bool OBJ_DONT_RENDER_IN_MIRROR_REFLECTIONS = false;
        public static readonly bool OBJ_ONLY_RENDER_IN_MIRROR_REFLECTIONS = false;
        public static readonly bool OBJ_CLIMBABLE_BY_AI = false;
        public static readonly bool OBJ_DONT_WRITE_ZBUFFER = false;
        public static readonly bool OBJ_ALLOW_NON_UNIFORM_SCALED_INSTANCES = false;
        public static readonly bool OBJ_FORCE_BAKE_COLLISION = false;
        public static readonly bool OBJ_NEVER_BAKE_COLLISION = false;
        public static readonly bool OBJ_IS_TREE = false;
        public static readonly float OBJ_HD_TEXTURES_DISTANCE = 5.0f;
        public static readonly bool OBJ_SUPPRESS_HD_TEXTURES = false;
        public static readonly int OBJ_GROUPID = 0;
        public static readonly bool OBJ_OVERRIDE_PHYSICS_BOUNDS = false;
        public static readonly bool OBJ_AUTOSTART_ANIM = false;
        public static readonly string OBJ_DEFAULT_ENTITY_SETS = "";
        public static readonly bool OBJ_FORCE_ZERO_PRIORITY = false;
        public static readonly bool OBJ_CRITICAL = false;
        public static readonly bool OBJ_NEW_DLC = false;
        public static readonly bool OBJ_REPLACED_DLC = false;
        public static readonly bool OBJ_TXDDLC_DLC = false;
        public static readonly bool OBJ_HAS_PRE_REFLECTED_WATER_PROXY = false;
        public static readonly bool OBJ_HAS_DRAWABLE_PROXY_FOR_WATER_REFLECTIONS = false;
        public static readonly bool OBJ_PERMANENT_ARCHTYPE = false;
        public static readonly bool OBJ_HAS_ALPHA_SHADOW = false;
        public static readonly bool OBJ_IGNORE_IN_BOUND_CALC = false;

        // Milo instance flags
        public static readonly bool OBJ_MILO_INSTANCE_TURN_ON_GPS = false;
        public static readonly bool OBJ_MILO_INSTANCE_CAP_ALPHA = false;
        public static readonly bool OBJ_MILO_INSTANCE_SHORT_FADE_DISTANCE = false;

        //----------------------------------------------------------------
        // Gta Object : Time
        //----------------------------------------------------------------			
        public static readonly bool OBJ_IS_TIME_OBJECT = false;
        public static readonly bool TOBJ_ALLOW_VANISH_WHILST_VIEWED = false;
        public static readonly bool TOBJ_TIME_1 = true;
        public static readonly bool TOBJ_TIME_2 = true;
        public static readonly bool TOBJ_TIME_3 = true;
        public static readonly bool TOBJ_TIME_4 = true;
        public static readonly bool TOBJ_TIME_5 = true;
        public static readonly bool TOBJ_TIME_6 = true;
        public static readonly bool TOBJ_TIME_7 = false;
        public static readonly bool TOBJ_TIME_8 = false;
        public static readonly bool TOBJ_TIME_9 = false;
        public static readonly bool TOBJ_TIME_10 = false;
        public static readonly bool TOBJ_TIME_11 = false;
        public static readonly bool TOBJ_TIME_12 = false;
        public static readonly bool TOBJ_TIME_13 = false;
        public static readonly bool TOBJ_TIME_14 = false;
        public static readonly bool TOBJ_TIME_15 = false;
        public static readonly bool TOBJ_TIME_16 = false;
        public static readonly bool TOBJ_TIME_17 = false;
        public static readonly bool TOBJ_TIME_18 = false;
        public static readonly bool TOBJ_TIME_19 = false;
        public static readonly bool TOBJ_TIME_20 = false;
        public static readonly bool TOBJ_TIME_21 = true;
        public static readonly bool TOBJ_TIME_22 = true;
        public static readonly bool TOBJ_TIME_23 = true;
        public static readonly bool TOBJ_TIME_24 = true;
        #endregion // Gta Object

        #region Gta Block
        //--------------------------------------------------------------------------
        // Gta Block
        //--------------------------------------------------------------------------		
        public static readonly String BLOCK_BLOCK_ID = "changeme";
        public static readonly String BLOCK_OWNER = "R* North";
        public static readonly String BLOCK_ARTIST = "unknown";
        public static readonly String BLOCK_DATE_EDITED = "unknown";
        public static readonly bool BLOCK_FIRST_PASS = false;
        public static readonly bool BLOCK_SECOND_PASS_STARTED = false;
        public static readonly bool BLOCK_SECOND_PASS_OUTSOURCED = false;
        public static readonly bool BLOCK_SECOND_PASS_COMPLETE = false;
        public static readonly bool BLOCK_THIRD_PASS_COMPLETE = false;
        public static readonly bool BLOCK_COMPLETE = false;
        public static readonly int BLOCK_MEMMODEL = 0;
        public static readonly int BLOCK_MEMCOLLISION = 0;
        public static readonly int BLOCK_MEMTEXTURE = 0;
        public static readonly int BLOCK_NUMMODELS = 0;
        public static readonly int BLOCK_NUMXREFS = 0;
        public static readonly bool BLOCK_FREEZE = false;
        #endregion // Gta Block

        #region Gta CarGen
        //--------------------------------------------------------------------------
        // Cargen
        //--------------------------------------------------------------------------
        public static readonly String CARGEN_MODEL = "";
        public static readonly int CARGEN_COLOUR1 = -1;
        public static readonly int CARGEN_COLOUR2 = -1;
        public static readonly int CARGEN_COLOUR3 = -1;
        public static readonly int CARGEN_COLOUR4 = -1;
        public static readonly bool CARGEN_HIGH_PRIORITY = false;
        public static readonly bool CARGEN_IGNORE_POPULATION_LIMIT = false;
        public static readonly int CARGEN_ALARM_CHANCE = 0;
        public static readonly int CARGEN_LOCKED_CHANCE = 0;
        public static readonly int CARGEN_MIN_DELAY = -1;
        public static readonly int CARGEN_MAX_DELAY = -1;
        public static readonly bool CARGEN_POLICE = false;
        public static readonly bool CARGEN_FIRETRUCK = false;
        public static readonly bool CARGEN_AMBULANCE = false;
        public static readonly bool CARGEN_ALIGN_LEFT = false;
        public static readonly bool CARGEN_ALIGN_RIGHT = false;
        public static readonly bool CARGEN_SINGLE_PLAYER = true;
        public static readonly bool CARGEN_NETWORK = true;
        public static readonly int CARGEN_CREATION_RULE = 0;
        public static readonly int CARGEN_SCENARIO = 0;
        public static readonly bool CARGEN_ACTIVE_DAY = true;
        public static readonly bool CARGEN_ACTIVE_NIGHT = true;
        public static readonly String CARGEN_MODEL_SET = "";
        public static readonly bool CARGEN_LOW_PRIORITY = true;
        #endregion // Gta CarGen

        #region Gta Collision
        //--------------------------------------------------------------------------
        // Gta Collision
        //--------------------------------------------------------------------------		
        public static readonly String COLL_AUDIO_MATERIAL = "";
        public static readonly int COLL_COMPOSTIE_ORDER = -1;
        public static readonly int COLL_ROOM_ID = 0;
        #endregion // Gta Collision

        #region Gta Explosion
        //--------------------------------------------------------------------------
        // Gta Explosion
        //--------------------------------------------------------------------------
        public static readonly int TWODFX_EXPLOSION_ATTACH = 0;
        public static readonly bool TWODFX_EXPLOSION_ATTACH_TO_ALL = false;
        public static readonly int TWODFX_EXPLOSION_TRIGGER = 0;
        public static readonly String TWODFX_EXPLOSION_NAME = String.Empty;

        // Flags
        public static readonly bool TWODFX_EXPLOSION_IGNORE_DAMAGE_MODEL = false;
        public static readonly bool TWODFX_EXPLOSION_PLAY_ON_PARENT = false;
        public static readonly bool TWODFX_EXPLOSION_ONLY_ON_DAMAGE_MODEL = false;
        public static readonly bool TWODFX_EXPLOSION_ALLOW_RUBBER_BULLET_SHOT_FX = false;
        public static readonly bool TWODFX_EXPLOSION_ALLOW_ELECTRIC_BULLET_SHOT_FX = false;
        #endregion // Gta Explosion

        #region Gta Milo
        //--------------------------------------------------------------------------
        // Gta Milo
        //--------------------------------------------------------------------------		
        public static readonly bool MILO_DONT_EXPORT_MILO = false;
        public static readonly bool MILO_PROPOGATE_INSTANCING = false;
        public static readonly bool MILO_OFFICE_POPULATION = false;
        public static readonly bool MILO_ALLOW_SPRINTING = false;
        public static readonly bool MILO_CUTSCENE_ONLY = false;
        public static readonly bool MILO_LOD_WHEN_LOCKED = false;
        public static readonly bool MILO_NO_WATER_REFLECTION = false;
        public static readonly bool MILO_DONT_DRAW_IF_LOCKED = false;
        public static readonly float MILO_DETAIL = 30.0f; // Attribute default is -1.0f but that would screw the streaming extents.
        public static readonly bool MILO_PRIORITISE_DECALS = false;
        public static readonly int MILO_GROUPID = 0;

        // Milo instance flags (for in-situ interiors)
        public static readonly bool MILO_INSTANCE_TURN_ON_GPS = false;
        public static readonly bool MILO_INSTANCE_CAP_ALPHA = false;
        public static readonly bool MILO_INSTANCE_SHORT_FADE_DISTANCE = false;
        #endregion // Gta Milo

        #region Gta MiloTri
        //--------------------------------------------------------------------------
        // Gta MiloTri
        //--------------------------------------------------------------------------		
        public static readonly bool MILOTRI_DONT_EXPORT_MILO_TRI = false;
        public static readonly int MILOTRI_GROUPID = 0;
        public static readonly int MILOTRI_FLOORID = 0;
        #endregion // Gta MiloTri

        #region Gta MloRoom
        //--------------------------------------------------------------------------
        // Gta MloRoom
        //--------------------------------------------------------------------------		
        public static readonly String MLOROOM_TIMECYCLE = "";
        public static readonly String MLOROOM_SECONDARY_TIMECYCLE = "";
        public static readonly float MLOROOM_BLEND = 1.0f;
        public static readonly bool MLOROOM_FREEZE_PEDS = false;
        public static readonly bool MLOROOM_FREEZE_CARS = false;
        public static readonly bool MLOROOM_REDUCE_PEDS = true;
        public static readonly bool MLOROOM_REDUCE_CARS = true;
        public static readonly bool MLOROOM_NO_DIRECTIONAL_LIGHT = false;
        public static readonly bool MLOROOM_NO_EXTERIOR_LIGHT = false;
        public static readonly bool MLOROOM_FORCE_FREEZE = false;
        public static readonly int MLOROOM_ROOM_FLOOR = 0;
        public static readonly int MLOROOM_DEPTH = 0;
        public static readonly bool MLOROOM_FORCE_DIRECTIONAL_LIGHT = false;
        public static readonly bool MLOROOM_DONT_RENDER_EXTERIOR = false;
        public static readonly bool MLOROOM_MIRROR_POTENTIALLY_VISIBLE = false;
        #endregion // Gta MloRoom

        #region Gta MloPortal
        //--------------------------------------------------------------------------
        // Gta MloPortal
        //--------------------------------------------------------------------------
        public static readonly float MLOPORTAL_FADE_DISTANCE = 1.0f;
        public static readonly int MLOPORTAL_RED = 255;
        public static readonly int MLOPORTAL_GREEN = 255;
        public static readonly int MLOPORTAL_BLUE = 255;
        public static readonly bool MLOPORTAL_ONE_WAY = false;
        public static readonly bool MLOPORTAL_MLO_LINK = false;
        public static readonly bool MLOPORTAL_MIRROR = false;
        public static readonly bool MLOPORTAL_IGNORE_BY_MODIFIER = false;
        public static readonly bool MLOPORTAL_EXPENSIVE_SHADERS = false;
        public static readonly bool MLOPORTAL_DRAW_LOW_LOD_ONLY = false;
        public static readonly bool MLOPORTAL_ALLOW_CLOSING = true;
        public static readonly bool MLOPORTAL_MIRROR_USES_DIRECTIONAL_LIGHT = false;
        public static readonly bool MLOPORTAL_MIRROR_PORTAL_TRAVERSAL = false;
        public static readonly bool MLOPORTAL_MIRROR_FLOOR = false;
        public static readonly float MLOPORTAL_OPACITY = 0.0f;
        public static readonly bool MLOPORTAL_MIRROR_CAN_SEE_EXTERIOR_VIEW = false;
        public static readonly int MLOPORTAL_MIRROR_TYPE = 0;      // None
        public static readonly int MLOPORTAL_MIRROR_PRIORITY = 0;
        public static readonly bool MLOPORTAL_USE_LIGHT_BLEED = false;
        #endregion // Gta MloPortal

        #region Vehicle Node / Link
        //--------------------------------------------------------------------------
        // Vehicle Node
        //--------------------------------------------------------------------------
        public static readonly bool VEHNOD_DISABLED = false;
        public static readonly bool VEHNOD_WATER = false;
        public static readonly bool VEHNOD_ROAD_BLOCK = false;
        public static readonly bool VEHNOD_LOW_BRIDGE = false;
        public static readonly int VEHNOD_SPEED = 1;
        public static readonly int VEHNOD_SPECIAL = 0;
        public static readonly int VEHNOD_DENSITY = 8;
        public static readonly String VEHNOD_STREETNAME = "";
        public static readonly bool VEHNOD_HIGHWAY = false;
        public static readonly bool VEHNOD_NO_GPS = false;
        public static readonly bool VEHNOD_TUNNEL = false;
        public static readonly bool VEHNOD_OPENSPACE = false;
        public static readonly bool VEHNOD_CANNOTGOLEFT = false;
        public static readonly bool VEHNOD_LEFTTURNSONLY = false;
        public static readonly bool VEHNOD_OFF_ROAD = false;
        public static readonly bool VEHNOD_CANNOT_GO_RIGHT = false;
        public static readonly bool VEHNOD_NO_BIG_VEHICLES = false;
        public static readonly bool VEHNOD_INDICATE_KEEP_LEFT = false;
        public static readonly bool VEHNOD_INDICATE_KEEP_RIGHT = false;
        public static readonly bool VEHNOD_SLIP_LANE = false;

        //--------------------------------------------------------------------------
        // Vehicle Link
        //--------------------------------------------------------------------------
        public static readonly int VEHLNK_LANES_IN = 1;
        public static readonly int VEHLNK_LANES_OUT = 1;
        public static readonly int VEHLNK_WIDTH = 0;
        public static readonly bool VEHLNK_NARROW_ROAD = false;
        public static readonly bool VEHLNK_GPS_BOTH_WAYS = false;
        public static readonly bool VEHLNK_BLOCK_IF_NO_LANES = false;
        public static readonly bool VEHLNK_SHORTCUT = false;
        public static readonly bool VEHLNK_NONAVIGATION = false;
        #endregion // Vehicle Node / Link

        #region Patrol Node / Link
        //--------------------------------------------------------------------------
        // Patrol Node
        //--------------------------------------------------------------------------
        public static readonly int PATNOD_DURATION = -1;
        public static readonly float PATNOD_HEADING_X = 0.0f;
        public static readonly float PATNOD_HEADING_Y = 0.0f;
        public static readonly float PATNOD_HEADING_Z = 0.0f;
        public static readonly String PATNOD_ASSOCIATED_BASE = "";
        public static readonly String PATNOD_ROUTE_NAME = "";
        public static readonly String PATNOD_PATROL_TYPE = "";

        //--------------------------------------------------------------------------
        // Patrol Link
        //--------------------------------------------------------------------------
        // Currently empty.
        #endregion // Patrol Node / Link

        #region Gta Particle
        //--------------------------------------------------------------------------
        // Gta Particle
        //--------------------------------------------------------------------------		
        public static readonly String TWODFX_PARTICLE_NAME = "AMB_COLD_AIR_FLOOR";
        public static readonly int TWODFX_PARTICLE_TRIGGER = 0;
        public static readonly bool TWODFX_PARTICLE_IS_TRIGGERED = false;
        public static readonly int TWODFX_PARTICLE_ATTACH = 0;
        public static readonly bool TWODFX_PARTICLE_ATTACH_TO_ALL = false;
        public static readonly float TWODFX_PARTICLE_SCALE = 1.0f;
        public static readonly int TWODFX_PARTICLE_PROBABILITY = 100;
        public static readonly int TWODFX_PARTICLE_COLOUR_R = 255;
        public static readonly int TWODFX_PARTICLE_COLOUR_G = 255;
        public static readonly int TWODFX_PARTICLE_COLOUR_B = 255;
        public static readonly int TWODFX_PARTICLE_COLOUR_A = 255;
        public static readonly float TWODFX_PARTICLE_SIZE_X = 1.0f;
        public static readonly float TWODFX_PARTICLE_SIZE_Y = 1.0f;
        public static readonly float TWODFX_PARTICLE_SIZE_Z = 1.0f;
        public static readonly bool TWODFX_PARTICLE_DONTEXPORT = false;

        // Flags
        public static readonly bool TWODFX_PARTICLE_IGNORE_DAMAGE_MODEL = false;
        public static readonly bool TWODFX_PARTICLE_PLAY_ON_PARENT = false;
        public static readonly bool TWODFX_PARTICLE_HAS_TINT = false;
        public static readonly bool TWODFX_PARTICLE_ONLY_ON_DAMAGE_MODEL = false;
        public static readonly bool TWODFX_PARTICLE_ALLOW_RUBBER_BULLET_SHOT_FX = false;
        public static readonly bool TWODFX_PARTICLE_ALLOW_ELECTRIC_BULLET_SHOT_FX = false;

        #endregion // Gta Particle

        #region Rs Decal
        //--------------------------------------------------------------------------
        // Rs Decal
        //--------------------------------------------------------------------------	
        public static readonly String TWODFX_DECAL_NAME = "";
        public static readonly int TWODFX_DECAL_TRIGGER = 0;
        public static readonly int TWODFX_DECAL_ATTACH = 0;
        public static readonly bool TWODFX_DECAL_ATTACH_TO_ALL = false;
        public static readonly float TWODFX_DECAL_SCALE = 1.0f;
        public static readonly int TWODFX_DECAL_PROBABILITY = 100;
        // Flags
        public static readonly bool TWODFX_DECAL_IGNORE_DAMAGE_MODEL = false;
        public static readonly bool TWODFX_DECAL_PLAY_ON_PARENT = false;
        public static readonly bool TWODFX_DECAL_ONLY_ON_DAMAGE_MODEL = false;
        public static readonly bool TWODFX_DECAL_ALLOW_RUBBER_BULLET_SHOT_FX = false;
        public static readonly bool TWODFX_DECAL_ALLOW_ELECTRIC_BULLET_SHOT_FX = false;
        #endregion // Rs Decal

        #region Gta Light / LightPhoto
        //--------------------------------------------------------------------------
        // Gta Light and Gta LightPhoto
        //--------------------------------------------------------------------------
        public static readonly float TWODFX_LIGHT_CORONA_SIZE = 0.0f;
        public static readonly int TWODFX_LIGHT_FLASHINESS = 0;
        public static readonly String TWODFX_LIGHT_SHADOW_NAME = "null";
        public static readonly String TWODFX_LIGHTPHOTO_SHADOW_NAME = "shad_exp";

        public static readonly int TWODFX_LIGHT_ATTACH = 0;
        public static readonly float TWODFX_LIGHT_VOLUME_SIZE = 1.0f;
        public static readonly float TWODFX_LIGHT_VOLUME_INTENSITY = 1.0f;
        public static readonly float TWODFX_LIGHT_CORONA_INTENSITY = 1.0f;
        public static readonly float TWODFX_LIGHT_CORONA_ZBIAS = 0.1f;
        public static readonly int TWODFX_LIGHT_SHADOW_ALPHA = 0;
        public static readonly int TWODFX_LIGHT_LIGHT_FADE_DISTANCE = 0;
        public static readonly int TWODFX_LIGHT_VOLUME_FADE_DISTANCE = 0;
        public static readonly int TWODFX_LIGHT_SHADOW_FADE_DISTANCE = 0;
        public static readonly int TWODFX_LIGHT_SPECULAR_FADE_DISTANCE = 0;
        public static readonly float TWODFX_LIGHT_SHADOW_NEAR_CLIP = 0.01f;
        public static readonly float TWODFX_LIGHT_SHADOW_BLUR = 0.0f;

        // Flags
        public static readonly bool TWODFX_LIGHT_IS_ELECTRIC = false;
        public static readonly bool TWODFX_LIGHT_STROBE = false;
        public static readonly bool TWODFX_LIGHT_PLANE = false;
        public static readonly bool TWODFX_LIGHT_CAST_STATIC_SHADOWS = false;
        public static readonly bool TWODFX_LIGHT_CAST_DYNAMIC_SHADOWS = false;
        public static readonly bool TWODFX_LIGHT_CALC_FROM_SUNLIGHT = false;
        public static readonly bool TWODFX_LIGHT_ENABLE_BUZZING = false;
        public static readonly bool TWODFX_LIGHT_FORCE_BUZZING = false;
        public static readonly bool TWODFX_LIGHT_VOLUME_DRAWING = false;
        public static readonly bool TWODFX_LIGHT_NO_SPECULAR = false;
        public static readonly bool TWODFX_LIGHT_INVERT_CLAMP = false;
        public static readonly bool TWODFX_LIGHT_INT_AND_EXT = false;
        public static readonly bool TWODFX_LIGHT_CORONA_ONLY = false;
        public static readonly bool TWODFX_LIGHT_USE_CULLPLANE = false;
        public static readonly bool TWODFX_LIGHT_USE_VOL_OUTER_COLOUR = false;
        public static readonly bool TWODFX_LIGHT_DONT_EXPORT = false;
        public static readonly int TWODFX_LIGHT_RENDER_IN_REFLECTION = 0;
        public static readonly bool TWODFX_LIGHT_DISABLE_IN_CUTSCENE = false;
        public static readonly bool TWODFX_LIGHT_CAST_HIGHER_RES_SHADOW = false;
        public static readonly bool TWODFX_LIGHT_CAST_ONLY_LOWRES_SHADOWS = false;
        public static readonly bool TWODFX_LIGHT_ADD_TO_LOD_LIGHTS = false;
        public static readonly bool TWODFX_LIGHT_DONT_LIGHT_ALPHA = false;
        public static readonly bool TWODFX_LIGHT_SHADOWS_IF_POSSIBLE = false;
        public static readonly bool TWODFX_LIGHT_ADD_TO_VIP_LOD_LIGHTS = false;
        public static readonly bool TWODFX_LIGHT_FORCE_MEDIUM_LOD_LIGHT = false;
        public static readonly bool TWODFX_LIGHT_CORONA_ONLY_LOD_LIGHT = false;
        #endregion // Gta Light / LightPhoto

        #region Gta LightShaft
        //--------------------------------------------------------------------------
        // Gta LightShaft
        //--------------------------------------------------------------------------		
        //        public static readonly float TWODFX_LIGHTSHAFT_DENSITY = 1.0f;
        public static readonly float TWODFX_LIGHTSHAFT_LENGTH = 2.0f;
        public static readonly float TWODFX_LIGHTSHAFT_INTENSITY = 1.0f;
        public static readonly float TWODFX_LIGHTSHAFT_DIRECTION_X = 0.0f;
        public static readonly float TWODFX_LIGHTSHAFT_DIRECTION_Y = 0.0f;
        public static readonly float TWODFX_LIGHTSHAFT_DIRECTION_Z = 1.0f;
        public static readonly int TWODFX_LIGHTSHAFT_COLOUR_R = 255;
        public static readonly int TWODFX_LIGHTSHAFT_COLOUR_G = 255;
        public static readonly int TWODFX_LIGHTSHAFT_COLOUR_B = 255;
        public static readonly float TWODFX_LIGHTSHAFT_SOFTNESS = 0.0f;
        public static readonly int TWODFX_LIGHTSHAFT_DENSITY_TYPE = 0;
        public static readonly int TWODFX_LIGHTSHAFT_VOLUME_TYPE = 0;
        public static readonly float TWODFX_LIGHTSHAFT_DIRECTION_AMOUNT = 0.0f;
        public static readonly float TWODFX_LIGHTSHAFT_FADE_IN_TIME_START = 0.0f;
        public static readonly float TWODFX_LIGHTSHAFT_FADE_IN_TIME_END = 0.0f;
        public static readonly float TWODFX_LIGHTSHAFT_FADE_OUT_TIME_START = 0.0f;
        public static readonly float TWODFX_LIGHTSHAFT_FADE_OUT_TIME_END = 0.0f;
        public static readonly float TWODFX_LIGHTSHAFT_FADE_DISTANCE_START = 0.0f;
        public static readonly float TWODFX_LIGHTSHAFT_FADE_DISTANCE_END = 0.0f;
        public static readonly int TWODFX_LIGHTSHAFT_FLASHINESS = 0;

        // flags
        public static readonly bool TWODFX_LIGHTSHAFT_USE_SUN_LIGHT_DIRECTION = true;
        public static readonly bool TWODFX_LIGHTSHAFT_USE_SUN_LIGHT_COLOUR = true;
        public static readonly bool TWODFX_LIGHTSHAFT_SCALE_BY_SUN_COLOUR = false;
        public static readonly bool TWODFX_LIGHTSHAFT_SCALE_BY_SUN_INTENSITY = true;
        public static readonly bool TWODFX_LIGHTSHAFT_DRAW_IN_FRONT_AND_BEHIND = false;
        public static readonly bool TWODFX_LIGHTSHAFT_SQUARED_OFF_ENDCAP = false;

        //         public static readonly bool TWODFX_LIGHTSHAFT_SHADOWED = false;
        //         public static readonly bool TWODFX_LIGHTSHAFT_PER_PIXEL_SHADOWED = false;

        #endregion // Gta LightShaft

        #region Gta Swayable
        //--------------------------------------------------------------------------
        // Gta Swayable
        //--------------------------------------------------------------------------		
        public static readonly int TWODFX_SWAYABLE_BONEID = 0;
        public static readonly float TWODFX_SWAYABLE_LOW_WIND_SPEED = 4.0f;
        public static readonly float TWODFX_SWAYABLE_LOW_WIND_AMPLITUDE = 0.0f;
        public static readonly float TWODFX_SWAYABLE_HIGH_WIND_SPEED = 3.0f;
        public static readonly float TWODFX_SWAYABLE_HIGH_WIND_AMPLITUDE = 0.32f;
        #endregion // Gta Swayable

        #region Gta Ladders
        //--------------------------------------------------------------------------
        // Gta Ladders
        //--------------------------------------------------------------------------	
        public static readonly bool TWODFX_LADDER_CAN_GET_OFF_AT_TOP = false;
        #endregion // Gta Ladders

        #region RS Ladders
        //--------------------------------------------------------------------------
        // RS Ladders
        //--------------------------------------------------------------------------	
        public static readonly bool TWODFX_RSLADDER_CAN_GET_OFF_AT_TOP = true;
        public static readonly bool TWODFX_RSLADDER_CAN_GET_OFF_AT_BOTTOM = true;
        #endregion // RS Ladders

        #region Gta TimeCycle
        //--------------------------------------------------------------------------
        // Gta TimeCycle
        //--------------------------------------------------------------------------	
        public static readonly float TWODFX_TCYC_BOX_PERCENTAGE = 0.0f;
        public static readonly int TWODFX_TCYC_BOX_START_HOUR = 0;
        public static readonly int TWODFX_TCYC_BOX_END_HOUR = 23;
        public static readonly String TWODFX_TCYC_BOX_IDSTRING = String.Empty;
        public static readonly float TWODFX_TCYC_BOX_RANGE = 100.0f;

        public static readonly String TWODFX_TCYC_SPHERE_NAME = String.Empty;
        public static readonly float TWODFX_TCYC_SPHERE_RANGE = 0.25f;
        public static readonly float TWODFX_TCYC_SPHERE_PERCENTAGE = 100.0f;
        public static readonly int TWODFX_TCYC_SPHERE_START_HOUR = 0;
        public static readonly int TWODFX_TCYC_SPHERE_END_HOUR = 23;
        #endregion // Gta TimeCycle

        #region Gta SpawnPoint
        //--------------------------------------------------------------------------
        // Gta SpawnPoint
        //--------------------------------------------------------------------------	 
        public static readonly String TWODFX_SPAWNPOINT_PED_TYPE = "any";
        public static readonly int TWODFX_SPAWNPOINT_START_TIME = 0;
        public static readonly int TWODFX_SPAWNPOINT_END_TIME = 0;
        public static readonly String TWODFX_SPAWNPOINT_MODEL_SET = "any";
        public static readonly int TWODFX_SPAWNPOINT_AVAILABILITY = 0;
        public static readonly float TWODFX_SPAWNPOINT_RADIUS = 0.0f;
        public static readonly float TWODFX_SPAWNPOINT_TIME_TILL_PED_LEAVES = 0.0f;
        #endregion // Gta SpawnPoint

        #region RsSpawnPointFlags
        //--------------------------------------------------------------------------
        // RsSpawnPointFlags
        //--------------------------------------------------------------------------
        public static readonly bool SPAWNPOINTFLAGS_STATIONARY_REACTIONS = false;
        public static readonly bool SPAWNPOINTFLAGS_NO_SPAWN = false;
        #endregion // RsSpawnPointFlags

        #region Gta AudioEmit
        //--------------------------------------------------------------------------
        // Gta AudioEmit
        //--------------------------------------------------------------------------	
        public static readonly String TWODFX_AUDIO_EMITTER_EFFECT = "";
        #endregion // Gta AudioEmit

        #region Gta ProcObj
        //--------------------------------------------------------------------------
        // Gta ProcObj
        //--------------------------------------------------------------------------	
        public static readonly String TWODFX_PROC_OBJECT = "";
        public static readonly float TWODFX_PROC_SPACING = 2.0f;
        public static readonly float TWODFX_PROC_MIN_SCALE = 1.0f;
        public static readonly float TWODFX_PROC_MAX_SCALE = 1.0f;
        public static readonly float TWODFX_PROC_MIN_SCALE_Z = 1.0f;
        public static readonly float TWODFX_PROC_MAX_SCALE_Z = 1.0f;
        public static readonly float TWODFX_PROC_MIN_Z_OFFSET = 0.0f;
        public static readonly float TWODFX_PROC_MAX_Z_OFFSET = 0.0f;
        public static readonly bool TWODFX_PROC_IS_ALIGNED = false;
        #endregion // Gta ProcObj

        #region Gta LodModifier
        //--------------------------------------------------------------------------
        // Gta LodModifier
        //--------------------------------------------------------------------------	
        public static readonly bool LODMODIFIER_CULL_WATER = false;
        #endregion // Gta LodModifier

        #region RS Container
        // None required yet!
        #endregion // RS Container

        #region Gta Script
        //--------------------------------------------------------------------------
        // Gta Script
        //--------------------------------------------------------------------------
        public static readonly String SCRIPT_NAME = "none".ToLower();
        #endregion // Gta Script

        #region Rs Wind Disturbance
        //--------------------------------------------------------------------------
        // Rs Wind Disturbance
        //--------------------------------------------------------------------------	
        public static readonly int TWODFX_WIND_TYPE = 0;
        public static readonly int TWODFX_WIND_ATTACH = 0;
        public static readonly float TWODFX_WIND_STRENGTH = 1.0f;
        public static readonly bool TWODFX_WIND_ATTACH_TO_ALL = false;
        // Flags
        public static readonly bool TWODFX_WIND_STRENGTH_MULTIPLIES_GLOBAL_WIND = false;
        #endregion // Rs Wind Disturbance

        #region RS Door
        //--------------------------------------------------------------------------
        // RS Door
        //--------------------------------------------------------------------------	
        public static readonly bool TWODFX_DOOR_ENABLE_LIMIT_ANGLE = true;
        public static readonly bool TWODFX_DOOR_STARTS_LOCKED = false;
        public static readonly bool TWODFX_DOOR_CAN_BREAK = false;
        public static readonly float TWODFX_DOOR_LIMIT_ANGLE = 90.0f;
        public static readonly float TWODFX_DOOR_DOOR_TARGET_RATIO = 0.0f;
        #endregion // RS Door

        #region RS Expression
        //--------------------------------------------------------------------------
        // RS Expression
        //--------------------------------------------------------------------------	
        public static readonly String TWODFX_EXPRESSION_DICTIONARY_NAME = "";
        public static readonly String TWODFX_EXPRESSION_NAME = "";
        public static readonly String TWODFX_EXPRESSION_CREATURE_METADATA_NAME = "";
        public static readonly bool TWODFX_EXPRESSION_INIT_ON_COLLISION = false;
        #endregion // RS Expression

        #region RsStatedAnim
        public static readonly int STATEDANIM_ANIM_START = 0;
        public static readonly int STATEDANIM_ANIM_END = 0;
        public static readonly float STATEDANIM_LOD_DISTANCE = 100.0f;
        #endregion

        #region RsActive
        public static readonly bool RSACTIVE_ACTIVE = true;
        #endregion

        #region RS PropGroup
        public static readonly bool PROPGROUP_DONT_EXPORT = true;
        #endregion // RS PropGroup
        #endregion // Attribute Default Values

        /// <summary>
        /// Structure is now way-out-of-date.  This is duplicating data,
        /// please do not use.  It will be nuked.
        /// </summary>
#warning DHM FIX ME: remove this duplicate data structure.
        [Obsolete]
        public static Dictionary<String, dynamic> AttributeValueDefaultMap = new Dictionary<String, dynamic>
        {
            // Gta Object
 		    {AttrNames.OBJ_LOD_DISTANCE,                        AttrDefaults.OBJ_LOD_DISTANCE},

            // Gta Light
		    {AttrNames.TWODFX_LIGHT_CORONA_SIZE,                AttrDefaults.TWODFX_LIGHT_CORONA_SIZE},
		    {AttrNames.TWODFX_LIGHT_FLASHINESS,                   AttrDefaults.TWODFX_LIGHT_FLASHINESS},
		    {AttrNames.TWODFX_LIGHT_ATTACH,                     AttrDefaults.TWODFX_LIGHT_ATTACH},
		    {AttrNames.TWODFX_LIGHT_VOLUME_SIZE,                   AttrDefaults.TWODFX_LIGHT_VOLUME_SIZE},
		    {AttrNames.TWODFX_LIGHT_VOLUME_INTENSITY,                   AttrDefaults.TWODFX_LIGHT_VOLUME_INTENSITY},
		    {AttrNames.TWODFX_LIGHT_CORONA_INTENSITY,                   AttrDefaults.TWODFX_LIGHT_CORONA_INTENSITY},
		    {AttrNames.TWODFX_LIGHT_SHADOW_ALPHA,                   AttrDefaults.TWODFX_LIGHT_SHADOW_ALPHA},
		
		    // Flags
		    {AttrNames.TWODFX_LIGHT_IS_ELECTRIC,                   AttrDefaults.TWODFX_LIGHT_IS_ELECTRIC},
		    {AttrNames.TWODFX_LIGHT_STROBE,                     AttrDefaults.TWODFX_LIGHT_STROBE},
		    {AttrNames.TWODFX_LIGHT_PLANE,                      AttrDefaults.TWODFX_LIGHT_PLANE},
		    {AttrNames.TWODFX_LIGHT_CAST_STATIC_SHADOWS,                   AttrDefaults.TWODFX_LIGHT_CAST_STATIC_SHADOWS},
		    {AttrNames.TWODFX_LIGHT_CAST_DYNAMIC_SHADOWS,                   AttrDefaults.TWODFX_LIGHT_CAST_DYNAMIC_SHADOWS},
		    {AttrNames.TWODFX_LIGHT_CALC_FROM_SUNLIGHT,                   AttrDefaults.TWODFX_LIGHT_CALC_FROM_SUNLIGHT},
		    {AttrNames.TWODFX_LIGHT_ENABLE_BUZZING,                   AttrDefaults.TWODFX_LIGHT_ENABLE_BUZZING},
		    {AttrNames.TWODFX_LIGHT_FORCE_BUZZING,                   AttrDefaults.TWODFX_LIGHT_FORCE_BUZZING},
		    {AttrNames.TWODFX_LIGHT_VOLUME_DRAWING,                   AttrDefaults.TWODFX_LIGHT_VOLUME_DRAWING},
		    {AttrNames.TWODFX_LIGHT_NO_SPECULAR,                   AttrDefaults.TWODFX_LIGHT_NO_SPECULAR},
            {AttrNames.TWODFX_LIGHT_DONT_EXPORT,                      AttrDefaults.TWODFX_LIGHT_DONT_EXPORT},
    //		{OBJ_IS_TIME_OBJECT = "Is time object".ToLower();
		    {AttrNames.TOBJ_ALLOW_VANISH_WHILST_VIEWED,                   AttrDefaults.TOBJ_ALLOW_VANISH_WHILST_VIEWED},
		    {AttrNames.TOBJ_TIME_1,                             AttrDefaults.TOBJ_TIME_1},
		    {AttrNames.TOBJ_TIME_2,                             AttrDefaults.TOBJ_TIME_2},
		    {AttrNames.TOBJ_TIME_3,                             AttrDefaults.TOBJ_TIME_3},
		    {AttrNames.TOBJ_TIME_4,                             AttrDefaults.TOBJ_TIME_4},
		    {AttrNames.TOBJ_TIME_5,                   AttrDefaults.TOBJ_TIME_5},
		    {AttrNames.TOBJ_TIME_6,                   AttrDefaults.TOBJ_TIME_6},
		    {AttrNames.TOBJ_TIME_7,                   AttrDefaults.TOBJ_TIME_7},
		    {AttrNames.TOBJ_TIME_8,                   AttrDefaults.TOBJ_TIME_8},
		    {AttrNames.TOBJ_TIME_9,                   AttrDefaults.TOBJ_TIME_9},
		    {AttrNames.TOBJ_TIME_10,                   AttrDefaults.TOBJ_TIME_10},
		    {AttrNames.TOBJ_TIME_11,                   AttrDefaults.TOBJ_TIME_11},
		    {AttrNames.TOBJ_TIME_12,                   AttrDefaults.TOBJ_TIME_12},
		    {AttrNames.TOBJ_TIME_13,                   AttrDefaults.TOBJ_TIME_13},
		    {AttrNames.TOBJ_TIME_14,                   AttrDefaults.TOBJ_TIME_14},
		    {AttrNames.TOBJ_TIME_15,                   AttrDefaults.TOBJ_TIME_15},
		    {AttrNames.TOBJ_TIME_16,                   AttrDefaults.TOBJ_TIME_16},
		    {AttrNames.TOBJ_TIME_17,                   AttrDefaults.TOBJ_TIME_17},
		    {AttrNames.TOBJ_TIME_18,                   AttrDefaults.TOBJ_TIME_18},
		    {AttrNames.TOBJ_TIME_19,                   AttrDefaults.TOBJ_TIME_19},
		    {AttrNames.TOBJ_TIME_20,                   AttrDefaults.TOBJ_TIME_20},
		    {AttrNames.TOBJ_TIME_21,                   AttrDefaults.TOBJ_TIME_21},
		    {AttrNames.TOBJ_TIME_22,                   AttrDefaults.TOBJ_TIME_22},
		    {AttrNames.TOBJ_TIME_23,                   AttrDefaults.TOBJ_TIME_23},
		    {AttrNames.TOBJ_TIME_24,                   AttrDefaults.TOBJ_TIME_24}
        };
    }

    /// <summary>
    /// Gta Object "Attribute" attribute integer values.
    /// </summary>
    public enum ObjAttributeValues
    {
        None = 0,
        UNUSED_1,
        Is_Ladder,
        Is_Traffic_Light,
        Bendy_Plant,
        Is_Garage_Door,
        Water_Level,
        Is_Normal_Door,
        Is_Sliding_Door,
        Is_Barrier,
        Is_Sliding_Door_Vertical,
        Noisy_Bush,
        Is_Railway_Barrier_Door,
        UNUSED_13,
        UNUSED_14,
        UNUSED_15,
        UNUSED_16,
        UNUSED_17,
        UNUSED_18,
        UNUSED_19,
        UNUSED_20,
        UNUSED_21,
        UNUSED_22,
        UNUSED_23,
        UNUSED_24,
        UNUSED_25,
        UNUSED_26,
        UNUSED_27,
        Rubbish,
        Rubbish_Only_On_Bin_Day,
        Clock,
        Tree,
        Street_Light
    };

    /// <summary>
    /// RsWindDisturbance "Wind Type" attribute integer values.
    /// </summary>
    public enum RsWindDisturbance
    {
        Hemisphere
    };

    #region Ladder Material Types
    /// <summary>
    /// 3ds max 1 index based enum
    /// </summary>
    public enum LadderMaterialTypes
    {
        METAL_SOLID_LADDER = 1,
        METAL_LIGHT_LADDER = 2,
        WOODEN_LADDER = 3,
        LAST = 4
    }
    #endregion  
} // RSG.SceneXml namespace

// Attributes.cs
