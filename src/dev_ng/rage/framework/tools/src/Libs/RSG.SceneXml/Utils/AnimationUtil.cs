﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Logging;

namespace RSG.SceneXml.Utils
{
    /// <summary>
    /// Utility class for animation related algorithms
    /// </summary>
    public class AnimationUtil
    {
        public static void CalculateRelativeAnimRange(
            TargetObjectDef def, string propertyName,
            float globalAnimStart, float globalAnimEnd, 
            out float relativeAnimStart, out float relativeAnimEnd)
        {
            relativeAnimStart = 0.0f;
            relativeAnimEnd = 1.0f;
            float animationLength = globalAnimEnd - globalAnimStart;
            List<RSG.SceneXml.sKeyFrame> ActiveKeys;
            if (def.KeyedProperties.TryGetValue(propertyName, out ActiveKeys))
            {
                if (ActiveKeys.Count > 0)
                {
                    if (animationLength > 0)
                    {
                        int firstStartTime = 999999;
                        int firstStopTime = 999999;
                        int lastStopTime = 0;
                        foreach (sKeyFrame keyframe in ActiveKeys)
                        {
                            if (keyframe.m_nState == 1 && keyframe.m_nTime < firstStartTime)
                                firstStartTime = keyframe.m_nTime;
                            if (keyframe.m_nState == 0 && keyframe.m_nTime > lastStopTime)
                                lastStopTime = keyframe.m_nTime;
                            if (keyframe.m_nState == 0 && keyframe.m_nTime < firstStopTime)
                                firstStopTime = keyframe.m_nTime;
                        }
                        relativeAnimStart = ((float)firstStartTime - globalAnimStart) / ((float)animationLength);
                        relativeAnimEnd = ((float)lastStopTime - globalAnimStart) / ((float)animationLength);
                        float relativeFIRSTStopTime = ((float)firstStopTime - globalAnimStart) / ((float)animationLength);
                        if ((relativeAnimEnd > 1.0f) ||
                            (relativeAnimEnd < relativeAnimStart))
                        {
                            relativeAnimEnd = 1.0f;
                        }
                        if ((relativeAnimStart < 0.0f) ||
                            (relativeAnimStart < relativeFIRSTStopTime))
                        {
                            relativeAnimStart = 0.0f;
                        }
                    }
                    else
                    {
                        Log.Log__DebugCtx(def.Name, "Object {0} has zero animation length!", def.Name);
                    }
                }
            }
            else
            {
                Log.Log__MessageCtx(def.Name, "Object {0} doesn't have \"active\" keyframes.", def.Name);
            }
        }
    }
}
