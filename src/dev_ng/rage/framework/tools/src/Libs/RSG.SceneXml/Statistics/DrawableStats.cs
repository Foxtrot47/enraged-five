﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.SceneXml;

namespace RSG.SceneXml.Statistics
{
    public enum ObjectTypes
    {
        Unknown,
        Object,
        Reference,
        InteriorReference,
        Collision,
    }

    public class DrawableStats : BaseStats
    {
        #region Properties

        public ObjectTypes Type
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public Guid Guid
        {
            get { return m_guid; }
            set { m_guid = value; }
        }
        private Guid m_guid;

        /// <summary>
        /// 
        /// </summary>
        public uint Size
        {
            get { return m_size; }
            set { m_size = value; }
        }
        public uint m_size;

        /// <summary>
        /// 
        /// </summary>
        public int ShaderCount
        {
            get { return m_shaderCount; }
            set { m_shaderCount = value; }
        }
        public int m_shaderCount;

        /// <summary>
        /// 
        /// </summary>
        public uint PolyCount
        {
            get { return m_polyCount; }
            set { m_polyCount = value; }
        }
        private uint m_polyCount;

        /// <summary>
        /// 
        /// </summary>
        public float PolyDensity
        {
            get { return m_polyDensity; }
            set { m_polyDensity = value; }
        }
        private float m_polyDensity;

        #endregion // Properties

        #region Constructor(s)

        public DrawableStats(String name, Scene scene)
            : base(name, scene)
        {
            this.Guid = Guid.Empty;
            this.Type = ObjectTypes.Unknown;
        }

        #endregion // Constructor(s)

    }
}
