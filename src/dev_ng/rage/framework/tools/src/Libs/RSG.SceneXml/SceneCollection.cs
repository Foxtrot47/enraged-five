﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using RSG.Base.Collections;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Content;
using RSG.Pipeline.Content.Algorithm;
using RSG.Pipeline.Core;
using SIO = System.IO;

namespace RSG.SceneXml
{

    /// <summary>
    /// SceneCollection; abstraction of loading SceneXml data providing the
    /// user with Scene objects containing fully resolved ObjectDefs.
    /// </summary>
    /// All tools should use this class when manipulating Scene objects so
    /// they don't individually have to worry about unresolved reference
    /// objects.
    ///
    /// There are a couple of events that external applications can use to 
    /// trigger specific functionality after Scenes are loaded or cleared. 
    /// 
    public sealed class SceneCollection :
        ISceneCollection,
        IEnumerable<Scene>
    {
        #region Constants
        /// <summary>
        /// Log context string.
        /// </summary>
        private static readonly String LOG_CTX = "Scene Collection";
        #endregion // Constants

        #region Events
        /// <summary>
        /// Event raised when a new Scene object is loaded.
        /// </summary>
        /// Note: not raised for generic Scene objects that are preloaded.
        /// 
        public event EventHandler<SceneEventArgs> PostSceneLoad;
        #endregion // Events
        
        #region Member Data
        /// <summary>
        /// Branch.
        /// </summary>
        private IBranch m_Branch;

        /// <summary>
        /// Content-tree.
        /// </summary>
        private IContentTree m_ContentTree;

        /// <summary>
        /// Dictionary storage for Scene objects.
        /// </summary>
        private Dictionary<String, Scene> m_Scenes;

        /// <summary>
        /// Scenes that failed their loading; Keeping references of those to avoid loading them several times.
        /// </summary>
        private readonly HashSet<string> m_nullScenes; 

        /// <summary>
        /// Option declaring which generic data to load
        /// </summary>
        private SceneCollectionLoadOption m_SceneLoadOption;

        /// <summary>
        /// Content-tree helper object.
        /// </summary>
        private ContentTreeHelper m_treeHelper;

        /// <summary>
        /// SceneCollection log.
        /// </summary>
        private static IUniversalLog Log;

        /// <summary>
        /// Queue used to store failed resolved object and try to get them during the last attempt
        /// </summary>
        private readonly Queue<TargetObjectDef> m_failedRefs = new Queue<TargetObjectDef>();

        private bool m_loaded = false;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="branch">Branch</param>
        /// <param name="tree">Content-tree</param>
        /// <param name="sceneLoadOption"></param>
        /// This constructor should be used in preference to the parameter-less
        /// version as it saves the ConfigGameView construction overhead.
        /// 
        public SceneCollection(IBranch branch, IContentTree tree, SceneCollectionLoadOption sceneLoadOption = SceneCollectionLoadOption.All)
        {
            this.m_Branch = branch;
            this.m_ContentTree = tree;
            this.m_Scenes = new Dictionary<String, Scene>();
            this.m_nullScenes = new HashSet<string>();
            this.m_SceneLoadOption = sceneLoadOption;
            this.m_treeHelper = new ContentTreeHelper(tree);
        }

        /// <summary>
        /// Static constructor.
        /// </summary>
        static SceneCollection()
        {
            SceneCollection.Log = LogFactory.CreateUniversalLog("RSG.SceneXml.SceneCollection");
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Load a new Scene from XML.
        /// </summary>
        /// <param name="filename">Absolute filename to SceneXml.</param>
        /// <returns>Scene object or null</returns>
        /// This is a separate method to the "GetScene" method to aid readability
        /// of user code; so that a complete SceneXml filename can be used
        public Scene LoadScene(String filename)
        {
            LoadGenericFiles();

            return LoadScene(filename, false);
        }

        /// <summary>
        /// Overload for loading a new Scene from XML to allow the user to specify
        /// whether to use a XmlTextReader instead of a XmlDocument when parsing a scene
        /// </summary>
        /// <param name="filename">Absolute filename to SceneXml.</param>
        /// <param name="useTextReader">Flag indicating whether we should use a XmlTextReader</param>
        /// <returns>Scene object or null</returns>
        public Scene LoadScene(String filename, bool useTextReader)
        {
            String name = SIO.Path.GetFileNameWithoutExtension(filename).ToLower();

            Scene scene;
            if(m_Scenes.TryGetValue(name, out scene))
                return scene;

            scene = LoadScene(this.m_ContentTree, filename, true, useTextReader);

            if (m_failedRefs.Count > 0)
            {
                // we had at least one unresolved reference during loading, let's get all the props and interiors loaded up
                // and see if we can find our references in those.
                LoadGenericFiles();

                // LoadGenericFiles loads map, that try to resolve refs, so that bit of code can call itself later on.
                // we dont want to dequeue an already emptied queue, right ?
                TargetObjectDef objectToResolve = m_failedRefs.Count > 0 ? m_failedRefs.Dequeue() : null;

                while (objectToResolve != null)
                {
                    ResolveReference(false, objectToResolve);

                    objectToResolve = m_failedRefs.Count > 0 ? m_failedRefs.Dequeue() : null;
                }
            }

            return scene;
        }

        /// <summary>
        /// Unloads a Scene based on its xml filename
        /// </summary>
        /// <param name="filename">Absolute filename to SceneXml.</param>
        public void UnloadScene(String filename)
        {
            String name = SIO.Path.GetFileNameWithoutExtension(filename).ToLower();
            if (this.m_Scenes.ContainsKey(name))
                this.m_Scenes.Remove(name);
        }

        /// <summary>
        /// Set the platform on each scene in the collection.
        /// Need to keep this out of the "Load" function because we only want to load
        /// once but be able to switch it up on the fly.
        /// </summary>
        /// <param name="target">ITarget platform to set on the scene collection</param>
        public void SetTarget(ITarget target)
        {
            foreach (KeyValuePair<String, Scene> kvp in this.m_Scenes)
            {
                kvp.Value.SetSceneTarget(target);
            }
        }

        /// <summary>
        /// Return a Scene object for the specified map name.
        /// </summary>
        /// <param name="name">Map name (e.g. "dt1_01", "v_construction")</param>
        /// <returns>Scene object or null.</returns>
        /// The Scene objects are cached; so they are only loaded once.  When
        /// loaded any external references are automatically resolved.
        /// 
        /// This resolution is the reason you *need* to use SceneCollection!
        /// 
        public Scene GetScene(String name)
        {
            Scene sceneLookup = null;
            string sceneName = name.ToLower();

            if (!this.m_Scenes.ContainsKey(sceneName))
            {
                IEnumerable<IContentNode> nodes = m_treeHelper.GetAllMapExportNodes();
                IContentNode firstNodeForDCC = nodes.FirstOrDefault(node => node.Name.Equals(sceneName, StringComparison.OrdinalIgnoreCase));

                if (null != firstNodeForDCC)
                {
                    IEnumerable<IProcess> processes = this.m_ContentTree.FindProcessesWithInput(firstNodeForDCC);
                    IProcess exportProcess = processes.FirstOrDefault(p => p.ProcessorClassName.Equals("RSG.Pipeline.Processor.Common.Dcc3dsmaxExportProcessor"));
                    if (null != exportProcess)
                    {
                        IEnumerable<IContentNode> outputs = exportProcess.Outputs;
                        File outputFile = outputs.OfType<File>().FirstOrDefault(file => file.Extension == ".zip");

                        if (null != outputFile)
                        {
#warning DHM FIX ME: when we get all XML in the Content-Tree we can fix this!
                            String sceneXmlFilename = SIO.Path.ChangeExtension(outputFile.AbsolutePath, ".xml");
                            Scene scene = null;
                            if (SIO.File.Exists(sceneXmlFilename))
                            {
                                scene = LoadScene(sceneXmlFilename, true);
                            }
                            else if (this.m_Branch.Project.IsDLC)
                            {
                                // Otherwise and we're DLC then try to load the core-game.
                                IBranch coreBranch = m_Branch.Project.Config.CoreProject.Branches[m_Branch.Name];
                                sceneXmlFilename = sceneXmlFilename.ToLower().Replace(m_Branch.Assets.ToLower(), coreBranch.Assets.ToLower());
                                scene = LoadScene(sceneXmlFilename, true);
                            } 

                            if (null != scene)
                            {
                                this.m_Scenes[sceneName] = scene;
                                sceneLookup = scene;
                            }
                            else
                            {
                                // skip logging if the scene as already been tried to get loaded and failed
                                if (!m_nullScenes.Contains(sceneName))
                                    SceneCollection.Log.WarningCtx(LOG_CTX,
                                        "Failed to load SceneXml for map '{0}'; error loading file.",
                                        name);
                            }
                        }
                        else
                        {
                            SceneCollection.Log.WarningCtx(LOG_CTX,
                                "Failed to load SceneXml file for map '{0}'; the map build process was misconfigured.",
                                name);
                        }
                    }
                    else
                    {
                        SceneCollection.Log.WarningCtx(LOG_CTX,
                            "Failed to load SceneXml file for '{0}'; no map export process defined.",
                            name);
                    }
                }
                else
                {
                    SceneCollection.Log.WarningCtx(LOG_CTX,
                        "Failed to load SceneXml file for map '{0}'; the content-node was not found.",
                        name);
                }
            }
            else
            {
                sceneLookup = this.m_Scenes[sceneName];
            }
            return (sceneLookup);
        }

        /// <summary>
        /// Find an object.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="sceneName"></param>
        /// <returns>Pair of Scene name and found ObjectDef (or null)</returns>
        public Pair<String, TargetObjectDef> Find(String name, String sceneName = null)
        {
            if (!String.IsNullOrEmpty(sceneName))
            {
                Scene scene = GetScene(sceneName);
                if (null == scene)
                    return (new Pair<String, TargetObjectDef>(String.Empty, null));
                TargetObjectDef obj = scene.FindObject(name);
                if (null != obj)
                    return (new Pair<String, TargetObjectDef>(sceneName, obj));
            }
            else
            {
                foreach (KeyValuePair<String, Scene> kvp in this.m_Scenes)
                {
                    String searchSceneName = kvp.Key;
                    Scene scene = kvp.Value;
                    if (null == scene)
                        continue; // Don't try to search an invalid Scene.

                    TargetObjectDef obj = scene.FindObject(name);
                    if (null != obj)
                        return (new Pair<String, TargetObjectDef>(searchSceneName, obj));
                }
            }
            return (new Pair<String, TargetObjectDef>(String.Empty, null));
        }

        /// <summary>
        /// Clear the internal Scene collection; then preload the generic
        /// files.
        /// </summary>
        public void Clear()
        {
            this.m_Scenes.Clear();
        }

        /// <summary>
        /// Gather all loaded scenes of a particular type.
        /// </summary>
        /// <param name="sceneType"></param>
        /// <returns></returns>
        public IEnumerable<Scene> GetScenesOfMapType(SceneType sceneType)
        {
            return this.m_Scenes.Values.Where(scene => ((scene != null) && (scene.SceneType == sceneType)));
        }

        #endregion // Controller Methods

        #region IEnumerable<Scene> Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerator<Scene> GetEnumerator()
        {
            foreach (Scene scene in this.m_Scenes.Values)
                yield return scene;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return (GetEnumerator());
        }
        #endregion // IEnumerable<Scene> Methods

        #region Private Methods
        /// <summary>
        /// Preload Scene objects for generic (prop and interior) files.
        /// </summary>
        private void LoadGenericFiles()
        {
            if (m_loaded)
                return;

            if (this.m_SceneLoadOption == SceneCollectionLoadOption.None)
                return;

            // set it BEFORE actually loading to avoid recursive calls to that one.
            m_loaded = true;

            if (this.m_SceneLoadOption.HasFlag(SceneCollectionLoadOption.Props))
                LoadMapSceneFiles(SceneType.EnvironmentProps);

            if (this.m_SceneLoadOption.HasFlag(SceneCollectionLoadOption.Interiors))
                LoadMapSceneFiles(SceneType.EnvironmentInterior);


        }

        /// <summary>
        /// Preload Scene objects for generic (prop and interior) files.
        /// </summary>
        /// <param name="type"></param>
        /// We preload these so we can resolve any references.
        /// 
        private void LoadMapSceneFiles(SceneType type)
        {
            Log.Warning("Potential Unresolved Reference: loading Props and Interiors.");

            IEnumerable<IProcess> mapExportProcesses = this.m_treeHelper.GetAllMapExportProcesses();
            IEnumerable<IContentNode> mapSourceFiles = mapExportProcesses.SelectMany(p => p.Inputs);
            foreach (IContentNode mapSourceFile in mapSourceFiles)
            {
                // Key is the 3dsmax map node, Value is the exported nodes.
                if (type != Scene.GetSceneType(mapSourceFile))
                    continue;

                IContentNode mapZipNode = this.m_treeHelper.GetExportZipNodeFromMaxFileNode(mapSourceFile);
                IContentNode mapSceneXmlNode = this.m_treeHelper.GetSceneXmlNodeFromExportZipNode(mapZipNode);
                IFilesystemNode node = (IFilesystemNode)mapSceneXmlNode;

                // skip scenes that were already unsuccessfully loaded to filter out error messages
                if (m_nullScenes.Contains(node.Name))
                    continue;

#warning DHM FIX ME: SceneXml files will likely need to be in the content-tree for ITYP/IMAP merging loveliness.
                String filename = SIO.Path.ChangeExtension(node.AbsolutePath, ".xml");
                Scene scene = LoadScene(m_ContentTree, filename, false, true, false);

                //trying to load core equivalent if an interior/prop is not found locally to resolve unresolved shit
                if (scene == null && m_Branch.Project.IsDLC)
                {
                    node = (IFilesystemNode)m_treeHelper.GetCoreBranchEquivalent(m_Branch, node);
                    filename = SIO.Path.ChangeExtension(node.AbsolutePath, ".xml");
                    scene = LoadScene(m_ContentTree, filename, false, true, false);
                }

                if (scene == null)
                {
                    if (m_Branch.Project.IsDLC)
                    {
                        Log.Warning("DLC: SceneXml not found in DLC nor Core. Potential broken data ahead.");
                    }

                    // keeping track of failed loading (aka "null scenes"). The LoadScene() method takes care of writing the Warning
                    m_nullScenes.Add(node.Name);
                }
                else
                {
                    this.m_Scenes[node.Name] = scene;
                }
            }
        }

        public void LoadMapSceneFilesForBranch(SceneType type, IBranch branch)
        {
            IEnumerable<IProcess> mapExportProcesses = this.m_treeHelper.GetAllMapExportProcesses();
            IEnumerable<IContentNode> mapSourceFiles = mapExportProcesses.SelectMany(p => p.Inputs).Where(node =>
            {
                var file = node as IFilesystemNode;
                if (file == null) return false;
                return file.AbsolutePath.StartsWith(branch.Art, StringComparison.OrdinalIgnoreCase) && type == Scene.GetSceneType(file);
            });

            foreach (IContentNode mapSourceFile in mapSourceFiles)
            {
                IContentNode mapZipNode = this.m_treeHelper.GetExportZipNodeFromMaxFileNode(mapSourceFile);
                IContentNode mapSceneXmlNode = this.m_treeHelper.GetSceneXmlNodeFromExportZipNode(mapZipNode);
                IFilesystemNode node = (IFilesystemNode)mapSceneXmlNode;

                // skip scenes that were already unsuccessfully loaded to filter out error messages
                if (m_nullScenes.Contains(node.Name))
                    continue;

#warning DHM FIX ME: SceneXml files will likely need to be in the content-tree for ITYP/IMAP merging loveliness.
                String filename = SIO.Path.ChangeExtension(node.AbsolutePath, ".xml");
                Scene scene = LoadScene(m_ContentTree, filename, false, true, false);

                if (scene == null)
                {
                    // keeping track of failed loading (aka "null scenes"). The LoadScene() method takes care of writing the Warning
                    m_nullScenes.Add(node.Name);
                }
                else
                {
                    this.m_Scenes[node.Name] = scene;
                }
            }
        }

        /// <summary>
        /// Load a new Scene from XML.
        /// </summary>
        /// <param name="tree"></param>
        /// <param name="filename"></param>
        /// <param name="raiseSceneLoadEvent"></param>
        /// <param name="useTextReaderMethod"></param>
        /// <param name="firstPass"></param>
        /// <returns></returns>
        private Scene LoadScene(IContentTree tree, String filename, bool raiseSceneLoadEvent, bool useTextReaderMethod, bool firstPass = true)
        {
            String name = SIO.Path.GetFileNameWithoutExtension(filename).ToLower();
            //Debug.Assert(!this.m_Scenes.ContainsKey(name), String.Format("Attempting to load Scene '{0}' ('{1}') but it is already loaded.  Internal error.",name, filename));

            if (this.m_Scenes.ContainsKey(name))
                return (this.m_Scenes[name]);

            Scene scene = null;
            if (SIO.File.Exists(filename))
            {
                SceneCollection.Log.MessageCtx(LOG_CTX, "Loading SceneXml file: '{0}'.",
                    SIO.Path.GetFileNameWithoutExtension(filename));
                scene = new Scene(tree, m_treeHelper, filename, LoadOptions.All, useTextReaderMethod);
                this.m_Scenes[name] = scene;

                ResolveRefObjects(scene, firstPass);

                if (raiseSceneLoadEvent && null != this.PostSceneLoad)
                    PostSceneLoad(this, new SceneEventArgs(scene));
            }
            else
            {
                if (m_nullScenes.Contains(name))
                    return null;

                SceneCollection.Log.WarningCtx(LOG_CTX,
                    "Failed to load SceneXml file for map '{0}'; the file '{1}' does not exist.",
                    name, filename);
            }

            return (scene);
        }

        /// <summary>
        /// Resolve all ObjectDef.RefObject references.
        /// </summary>
        /// <param name="scene"></param>
        /// <param name="firstPass">Tells if we are doing a first pass on resolving objectDef, or if we're in a "fallback to load everything" scenario</param>
        /// This is where the magic happens; the resolution of all external
        /// ObjectDefs.
        /// 
        /// We used to only consider the ObjectDef's RefFilename; this couldn't
        /// cope with props or interiors moving source file.
        /// 
        private void ResolveRefObjects(Scene scene, bool firstPass)
        {
            foreach (TargetObjectDef obj in scene.Walk(Scene.WalkMode.DepthFirst))
            {
                if (!obj.IsXRef() && !obj.IsRefObject())
                    continue; // Only process external references.
                if (String.IsNullOrWhiteSpace(obj.RefName))
                {
                    SceneCollection.Log.WarningCtx(obj.Name,
                        "[art] External reference object '{0}' ('{1}') does not have a source object name.  This cannot be resolved.",
                        obj.Name, SIO.Path.GetFileNameWithoutExtension(scene.Filename));
                    continue;
                }

                ResolveReference(firstPass, obj);
            }
        }

        private void ResolveReference(bool firstPass, TargetObjectDef obj)
        {
            // For speed we assume the RefFilename is correct; and fallback
            // to finding the source ObjectDef from anywhere else otherwise.
            String refSourceFilename = (obj.RefFile != null) ? SIO.Path.GetFullPath(obj.RefFile) : String.Empty;
            String refSourceName = SIO.Path.GetFileNameWithoutExtension(refSourceFilename).ToLower();
            String refName = obj.RefName.ToLower();

            bool refFileChanged = String.IsNullOrEmpty(refSourceFilename);
            Pair<String, TargetObjectDef> refObject = this.Find(refName, refSourceName);

            if (String.IsNullOrEmpty(refObject.First) || null == refObject.Second)
            {
                refObject = this.Find(refName);
                refFileChanged = true;
            }
            else if (!String.IsNullOrEmpty(refObject.First) && refObject.First == refSourceName)
            {
                // Ensure we pick up the refFile change when the object is
                // found and the ref scene differs.
                refFileChanged = true;
            }

            if (String.Empty == refObject.First || null == refObject.Second)
            {
                if (firstPass)
                {
                    // Log the failed refs resolve and store them for later attempt
                    m_failedRefs.Enqueue(obj);
                }
                else
                {
                    SceneCollection.Log.WarningCtx(obj.Name,
                        "[art] External reference object '{0}' ('{1}') does not resolve (referenced from '{2}').",
                        obj.Name, obj.RefName, SIO.Path.GetFileNameWithoutExtension(obj.RefFile));
                }
            }
            else
            {
                // Determine ref file if we have flagged it changed.
                if (refFileChanged)
                {
                    obj.RefFile = refObject.Second.MyScene.ExportFilename;
                }

                // Resolve ObjectDef reference.
                obj.RefObject = refObject.Second;

                // Resolve any children; e.g. entity per-instance lights.
                foreach (TargetObjectDef child in obj.Children)
                {
                    if (child.IsLightInstance() || child.IsSpawnPointInstance())
                    {
                        string refHandle = child.GetParameter<String>("refHandle", String.Empty);
                        if (String.Empty == refHandle)
                        {
                            SceneCollection.Log.ErrorCtx(child.Name,
                                "Instance Object {0} has an invalid reference handle string.", child.Name);
                            continue;
                        }
                        Guid id = new Guid(refHandle);
                        child.RefObject = GetChildWithMatchingGuid(obj.RefObject, id);
                    }
                }
            }
        }

        /// <summary>
        /// Return child ObjectDef that has a matching GUID for instance overrides.
        /// </summary>
        /// <param name="root"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        private TargetObjectDef GetChildWithMatchingGuid(TargetObjectDef root, Guid id)
        {
            if (id.Equals(root.AttributeGuid))
                return root;

            if(null!=root.SkeletonRoot)
                root = root.SkeletonRoot;

            foreach (TargetObjectDef refChild in root.Children)
            {
                TargetObjectDef r = GetChildWithMatchingGuid(refChild, id);
                if (null != r)
                    return r;
            }
            return (null);
        }
        #endregion // Private Methods
    }

} // RSG.SceneXml namespace
