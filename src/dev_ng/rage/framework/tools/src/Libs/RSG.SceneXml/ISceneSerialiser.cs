//
// File: ISceneSerialiser.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of ISceneSerialiser interface
//

using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace RSG.SceneXml
{

    /// <summary>
    /// Single RSG.SceneXml.Scene serialiser interface.
    /// </summary>
    public interface ISceneSerialiser
    {
        #region Properties
        /// <summary>
        /// Accessor to the Scene to be serialised.
        /// </summary>
        Scene Scene { get; }
        #endregion // Properties

        #region Controller Methods
        /// <summary>
        /// Write the serialised scene to a disk location (default options).
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        bool Write(String filename);

        /// <summary>
        /// Write the serialised scene to a disk location (with options).
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        bool Write(String filename, Dictionary<String, Object> options);
        #endregion // Controller Methods
    }

    /// <summary>
    /// Multiple RSG.SceneXml.Scen serialiser interface.
    /// </summary>
    public interface IMultipleSceneSerialiser
    {
        #region Properties
        /// <summary>
        /// Accessor to the Scenes to be serialised.
        /// </summary>
        Scene[] Scenes { get; }
        #endregion // Properties

        #region Controller Methods
        /// <summary>
        /// Write the serialised scene to a disk location (default options).
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        bool Write(String filename);

        /// <summary>
        /// Write the serialised scene to a disk location (with options).
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        bool Write(String filename, Dictionary<String, Object> options);

        /// <summary>
        /// Write manifest data to XmlDocument.
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <returns></returns>
        bool WriteManifestData(XDocument xmlDoc);
        #endregion // Controller Methods
    }

} // RSG.SceneXml namespace

// ISceneSerialiser.cs
