﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.XPath;

using System.Reflection;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;

using RSG.Base.Math;
using RSG.Base.Configuration;
using RSG.ObjectLinks;

namespace RSG.SceneXml
{
    using AttributeContainer = Dictionary<String, Object>;
    using KeyContainer = Dictionary<String, List<sKeyFrame>>;

    /// <summary>
    /// ObjectDef with associated Target platform.
    /// </summary>
    public class TargetObjectDef
    {
        #region Variables
        private ITarget     target_;
        private ObjectDef   objectDef_;
        #endregion

        #region Proxy Object Constants
        private const String PROXY_FRAG = "_frag_";
        private const String PROXY_MILO = "_milo_";
        private const String PROXY_ANIM = "_anim";
        #endregion // Proxy Object Constants

        #region Properties
        public String Name
        {
            get { return objectDef_.Name; }
        }

        public String RefName
        {
            get { return objectDef_.RefName; }
        }

        public String Class
        {
            get { return objectDef_.Class; }
        }

        public String SuperClass
        {
            get { return objectDef_.Superclass; }
        }

        public Guid Guid
        {
            get { return objectDef_.Guid; }
        }

        public Guid AttributeGuid
        {
            get { return objectDef_.AttributeGuid; }
        }

        public Guid Material
        {
            get { return objectDef_.Material; }
        }

        public AttributeContainer Attributes
        {
            get
            {
                AttributeContainer attributes;

                if (target_ == null)
                    attributes = objectDef_.Attributes;
                else
                    attributes = objectDef_.CombinePlatformAttributes(target_.Platform);

                return attributes;
            }
        }

        public KeyContainer KeyedProperties
        {
            get { return objectDef_.KeyedProperties; }
        }

        public ObjectDef ObjectDef
        {
            get { return objectDef_; }
        }

        public ITarget Target
        {
            get { return target_; }
            set { target_ = value; }
        }

        public ObjectDef.LodLevel LODLevel
        {
            get { return objectDef_.LODLevel; }
        }

        public TargetObjectDef[] Children
        {
            get
            {
                List<TargetObjectDef> children = new List<TargetObjectDef>();
                foreach (ObjectDef objDef in objectDef_.Children)
                    children.Add(new TargetObjectDef(target_, objDef));

                return children.ToArray();
            }
        }

        public TargetObjectDef Parent
        {
            get
            {
                if (objectDef_.Parent != null)
                    return new TargetObjectDef(target_, objectDef_.Parent);

                return null;
            }
        }

        public TargetObjectDef RefObject
        {
            get
            {
                if (objectDef_.RefObject != null)
                    return new TargetObjectDef(target_, objectDef_.RefObject);

                return null;
            }
            set
            {
                if (value != null)
                    objectDef_.RefObject = value.ObjectDef;
            }
        }

        public TargetObjectDef[] SubObjects
        {
            get
            {
                List<TargetObjectDef> subObjs = new List<TargetObjectDef>();
                foreach (ObjectDef objDef in objectDef_.SubObjects)
                    subObjs.Add(new TargetObjectDef(target_, objDef));
                return subObjs.ToArray();
            }
        }

        public AttributeContainer CustomAttributes
        {
            get { return objectDef_.CustomAttributes; }
        }

        public AttributeContainer UserProperties
        {
            get { return objectDef_.UserProperties; }
        }

        public AttributeContainer[] ParamBlocks
        {
            get { return objectDef_.ParamBlocks; }
        }

        public RSG.Base.Math.Matrix34f NodeTransform
        {
            get { return objectDef_.NodeTransform; }
            set { objectDef_.NodeTransform = value; }
        }

        public RSG.Base.Math.Quaternionf NodeRotation
        {
            get { return objectDef_.NodeRotation; }
        }

        public RSG.Base.Math.Vector3f NodeScale
        {
            get { return objectDef_.NodeScale; }
        }

        public RSG.Base.Math.Matrix34f ObjectTransform
        {
            get { return objectDef_.ObjectTransform; }
        }

        public RSG.Base.Math.Quaternionf ObjectRotation
        {
            get { return objectDef_.ObjectRotation; }
        }

        public RSG.Base.Math.Vector3f ObjectScale
        {
            get { return objectDef_.ObjectScale; }
        }

        public RSG.Base.Math.BoundingBox3f LocalBoundingBox
        {
            get { return objectDef_.LocalBoundingBox; }
            set { objectDef_.LocalBoundingBox = value; }
        }

        public RSG.Base.Math.BoundingBox3f WorldBoundingBox
        {
            get { return objectDef_.WorldBoundingBox; }
            set { objectDef_.WorldBoundingBox = value; }
        }

        public Scene MyScene
        {
            get { return objectDef_.MyScene; }
        }

        public String RefFile
        {
            get { return objectDef_.RefFile; }
            set { objectDef_.RefFile = value; }
        }

        public TargetObjectDef InternalRef
        {
            get
            {
                if (objectDef_.InternalRef != null)
                    return new TargetObjectDef(target_, objectDef_.InternalRef);

                return null;
            }
        }

        public Guid InternalRefGuid
        {
            get { return objectDef_.InternalRefGuid; }
        }

        public LODHierarchyDef LOD
        {
            get { return objectDef_.LOD; }
        }

        public LODHierarchyDef DrawableLOD
        {
            get { return objectDef_.DrawableLOD; }
        }

        public Guid SkeletonRootGuid
        {
            get { return objectDef_.SkeletonRootGuid; }
        }

        public TargetObjectDef SkeletonRoot
        {
            get
            {
                if (objectDef_.SkeletonRoot != null)
                    return new TargetObjectDef(target_, objectDef_.SkeletonRoot);

                return null;
            }
        }

        public TargetObjectDef SkinObj
        {
            get
            {
                if (objectDef_.SkinObj != null)
                    return new TargetObjectDef(target_, objectDef_.SkinObj);

                return null;
            }
            set { objectDef_.SkinObj = value.ObjectDef; }
        }

        public int AnimLength
        {
            get { return objectDef_.AnimLength; }
        }

        public List<sRefdTexture> RefdTextures
        {
            get { return objectDef_.RefdTextures; }
        }

        public Guid[] GenericReferenceGuids
        {
            get { return objectDef_.GenericReferenceGuids; }
        }

        public String AttributeClass
        {
            get { return objectDef_.AttributeClass; }
        }

        public int PolyCount
        {
            get { return objectDef_.PolyCount; }
        }

        public Dictionary<LinkChannel, LODHierarchyDef> SceneLinks
        {
            get { return objectDef_.SceneLinks; }
        }
        #endregion

        #region Static Properties
        private static MethodInfo[] s2DFXMethods;
        static TargetObjectDef()
        {
            List<MethodInfo> fxMethodInfo = new List<MethodInfo>();
            MethodInfo[] methods = typeof(TargetObjectDef).GetMethods();
            foreach (MethodInfo methodInfo in methods)
            {
                // Skip public methods.
                if (!methodInfo.IsPublic)
                    continue;

                Match m = Regex.Match(methodInfo.Name, "Is2dfx[A-Za-z]+");
                if (!m.Success)
                    continue;

                // Invoke our method.
                fxMethodInfo.Add(methodInfo);
            }
            s2DFXMethods = fxMethodInfo.ToArray();
        }

        #endregion
        #region Constructors
        public TargetObjectDef(ITarget target, ObjectDef objDef)
        {
            target_ = target;
            objectDef_ = objDef;
        }

        public TargetObjectDef(XmlTextReader reader, Scene scene)
        {
            objectDef_ = new ObjectDef(reader, scene);
            target_ = null;
        }

        public TargetObjectDef(XPathNavigator navigator, Scene scene)
        {
            objectDef_ = new ObjectDef(navigator, scene);
            target_ = null;
        }

        public TargetObjectDef(String name, String classname, String attr_class, Scene scene)
        {
            objectDef_ = new ObjectDef(name, classname, attr_class, scene);
            target_ = null;
        }
        #endregion

        #region Object Overrides
        public override bool Equals(Object obj)
        {
            if (obj is TargetObjectDef)
            {
                TargetObjectDef def = (obj as TargetObjectDef);
                return (this.Guid.Equals(def.Guid));
            }

            return (false);
        }

        public override int GetHashCode()
        {
            return this.Guid.GetHashCode();
        }

        public override string ToString()
        {
            return (String.Format("TargetObjectDef: [{0}]{1}", this.Name, this.Guid));
        }
        #endregion

        #region Attribute/Parameter/Property Accessing
        public T GetAttribute<T>(String name, T defValue)
        {
            // If our target is null that means we don't care about platform attributes so go directly to the base attributes.
            if (target_ == null)
                return objectDef_.GetAttribute(name, defValue);

            if (objectDef_.HasPlatformAttribute(target_.Platform, name))
            {
                return objectDef_.GetPlatformAttribute(target_.Platform, name, defValue);
            }
            else
            {
                return objectDef_.GetAttribute(name, defValue);
            }
        }

        /// <summary>
        /// Read an attribute value as string for maxscrip compatibility
        /// 
        /// -- Not sure if we want to use platform attributes here.
        /// </summary>
        /// <param name="name">String attribute name</param>
        /// <returns>Attribute value as string</returns>
        public object GetAttribute(String name, object defValue)
        {
            if (target_ == null)
                return objectDef_.GetAttribute(name, defValue);

            if (objectDef_.HasPlatformAttribute(target_.Platform, name))
            {
                return objectDef_.GetPlatformAttribute(target_.Platform, name, defValue) as object;
            }
            else
            {
                return objectDef_.GetAttribute(name, defValue) as object;
            }
        }

        public void SetAttribute(string name, object value)
        {
            objectDef_.SetAttribute(name, value);
        }

        public T GetParameter<T>(String name, T defValue)
        {
            return objectDef_.GetParameter(name, defValue);
        }

        public object GetProperty(string propertyName)
        {
            return objectDef_.GetProperty(propertyName);
        }

        public T GetProperty<T>(String name, T defValue)
        {
            return objectDef_.GetProperty(name, defValue);
        }

        public bool IsActiveForPlatform(Platform.Platform platform)
        {
            return objectDef_.IsActiveForPlatform(platform);
        }

        public bool HasPlatformSpecificAttributes(Platform.Platform platform)
        {
            return objectDef_.HasPlatformSpecificAttributes(platform);
        }

        /// <summary>
        /// Conglomerate all the attributes into a list that can be easily iterate on
        /// in environments such as MaxScript.
        /// </summary>
        /// <returns>Returns all attributes in an array format.</returns>
        [Obsolete]
        public AttributeListItem[] ConvertAttributesToArray()
        {
            return objectDef_.ConvertAttributesToArray();
        }

        /// <summary>
        /// Finds a particular property based on name.
        /// Non-templated version since MaxScript doesn't understand something as futuristic
        /// and advanced as that.
        /// </summary>
        /// <param name="propertyName">Property Name to find</param>
        /// <returns>Property's value if found.</returns>
        public object GetUserProperty(String propertyName)
        {
            return this.objectDef_.GetUserProperty(propertyName);
        }

        /// <summary>
        /// Read a property value, if not present return default value.
        /// </summary>
        /// <param name="name">String attribute name</param>
        /// <param name="defValue">Default value</param>
        /// <returns>Attribute value or default value</returns>
        public T GetUserProperty<T>(String name, T defValue)
        {
            return this.objectDef_.GetUserProperty(name, defValue);
        }
        #endregion

        #region Map Specific Attributes
        /// <summary>
        /// Determine whether this object should be exported or not.
        /// </summary>
        /// <returns>true if should not be exported, false otherwise</returns>
        public bool DontExport()
        {
            return (this.GetAttribute(AttrNames.OBJ_DONT_EXPORT, AttrDefaults.OBJ_DONT_EXPORT));
        }

        /// <summary>
        /// Determine whether this object should be exported to IDE file or not.
        /// </summary>
        /// <returns>true iff should not be added to IDE file, false otherwise</returns>
        public bool DontExportIDE()
        {
            return (this.GetAttribute(AttrNames.OBJ_DONT_ADD_TO_IDE, AttrDefaults.OBJ_DONT_ADD_TO_IDE));
        }

        /// <summary>
        /// Determine whether this object should be exported to IPL file or not.
        /// </summary>
        /// <returns>true iff should not be added to IPL file, false otherwise</returns>
        public bool DontExportIPL()
        {
            return (this.GetAttribute(AttrNames.OBJ_DONT_ADD_TO_IPL, AttrDefaults.OBJ_DONT_ADD_TO_IPL) || this.GetAttribute(AttrNames.MILOTRI_DONT_EXPORT_MILO_TRI, AttrDefaults.MILOTRI_DONT_EXPORT_MILO_TRI));
        }

        /// <summary>
        /// Determine whether this light / lightphoto effect should be exported or not.
        /// </summary>
        /// <returns>true if should not be exported, false otherwise</returns>
        public bool DontExportLightEffect()
        {
            return (this.GetAttribute(AttrNames.TWODFX_LIGHT_DONT_EXPORT, AttrDefaults.TWODFX_LIGHT_DONT_EXPORT));
        }

        /// <summary>
        /// Determine whether this object is a fragment.
        /// </summary>
        public bool IsFragment()
        {
            return (this.GetAttribute(AttrNames.OBJ_IS_FRAGMENT, AttrDefaults.OBJ_IS_FRAGMENT));
        }

        /// <summary>
        /// Determine whether this object is a fragment proxy.
        /// </summary>
        public bool IsFragmentProxy()
        {
            return (this.GetAttribute(AttrNames.OBJ_IS_FRAGMENT_PROXY, AttrDefaults.OBJ_IS_FRAGMENT_PROXY));
        }

        #region LOD Attributes
        public bool HasDrawableLODChildren()
        {
            return objectDef_.HasDrawableLODChildren();
        }

        public bool HasDrawableLODParent()
        {
            return objectDef_.HasDrawableLODParent();
        }

        public bool HasLODChildren()
        {
            return objectDef_.HasLODChildren();
        }

        public bool HasLODParent()
        {
            return objectDef_.HasLODParent();
        }

        public bool HasRenderSimParent()
        {
            return objectDef_.HasRenderSimParent();
        }

        public bool HasRenderSimChildren()
        {
            return objectDef_.HasRenderSimChildren();
        }

        public bool HasShadowMeshLink()
        {
            if (SceneLinks.ContainsKey(LinkChannel.LinkChannelShadowMesh))
            {
                if (SceneLinks[LinkChannel.LinkChannelShadowMesh].Children.Length > 0)
                    return true;
            }
            return false;
        }
        #endregion

        #endregion

        #region General Attributes

        #region Collision
        /// <summary>
        /// Does this object pass the current rules on being baked - this is queried during IMAP export and the bounds processor
        /// </summary>
        public bool WillHaveCollisionBaked()
        {
            if (RefObject != null && RefObject.IsDynObject())
                return false;// we never bake dynamic props

            bool definitionRequiresBaking = RefObject != null && RefObject.HasForceBakeCollisionFlag();
            bool instanceIsScaled = NodeScale.Dist(new Vector3f(1.0f, 1.0f, 1.0f)) > 0.00001f;
            int priority = GetPriority();

            bool result = (definitionRequiresBaking && !HasNeverBakeCollisionFlag() && (priority == 0)) ||
                          (instanceIsScaled && !HasNeverBakeCollisionFlag()) ||
                          HasForceBakeCollisionFlag();

            return result;
        }

        /// <summary>
        /// Determine whether this object has child collision data.
        /// </summary>
        /// <returns></returns>
        public bool HasCollision()
        {
            return objectDef_.HasCollision();
        }

        /// <summary>
        /// Determine whether this object is a Gta Collision.
        /// </summary>
        public bool IsCollision()
        {
            return (IsAttributeClass(AttrClass.COLLISION));
        }

        /// <summary>
        /// Determine whether this object has an audio material.
        /// </summary>
        public bool HasAudioMaterial()
        {
            String material = this.GetAttribute(AttrNames.COLL_AUDIO_MATERIAL, AttrDefaults.COLL_AUDIO_MATERIAL);
            return (IsCollision() && (material.Length > 0));
        }

        /// <summary>
        /// Determine whether this object is flagged to have its collision baked into the static bounds.
        /// </summary>
        public bool HasForceBakeCollisionFlag()
        {
            return (this.GetAttribute(AttrNames.OBJ_FORCE_BAKE_COLLISION, AttrDefaults.OBJ_FORCE_BAKE_COLLISION));
        }

        /// <summary>
        /// Determine whether this object is flagged to never have its collision baked into the static bounds.
        /// </summary>
        public bool HasNeverBakeCollisionFlag()
        {
            return (this.GetAttribute(AttrNames.OBJ_NEVER_BAKE_COLLISION, AttrDefaults.OBJ_NEVER_BAKE_COLLISION));
        }

        #endregion

        #region Generic Object Type Query Methods
        /// <summary>
        /// Determine whether this object is of a specific Attribute Class.
        /// </summary>
        /// <param name="classname"></param>
        /// <returns></returns>
        public bool IsAttributeClass(String classname)
        {
            return String.Equals(objectDef_.AttributeClass, classname, StringComparison.CurrentCultureIgnoreCase);
        }

        /// <summary>
        /// Determine whether this object is of a specific DCC / 3dsmax Superclass.
        /// </summary>
        /// <param name="classname"></param>
        /// <returns></returns>
        public bool IsMaxSuperclass(String classname)
        {
            return String.Equals(objectDef_.Superclass, classname);
        }

        /// <summary>
        /// Determine whether this object is of a specific DCC / 3dsmax Class.
        /// </summary>
        /// <param name="classname"></param>
        /// <returns></returns>
        public bool IsMaxClass(String classname)
        {
            return String.Equals(objectDef_.Class, classname);
        }

        /// <summary>
        /// Determine whether this object is of a specific DCC / 3dsmax Class which we are ignoring.
        /// </summary>
        /// <param name="classname"></param>
        /// <returns></returns>
        public bool IsIgnoredMaxLightClass()
        {
            return objectDef_.IsIgnoredMaxLightClass();
        }

        /// <summary>
        /// Determine whether this object has light children.
        /// </summary>
        /// <returns></returns>
        public bool HasLights()
        {
            return objectDef_.HasLights();
        }

        /// <summary>
        /// Determine whether this object has 2dfx children.
        /// </summary>
        /// <returns></returns>
        public bool Has2dfx()
        {
            return objectDef_.Has2dfx();
        }

        /// <summary>
        /// Determine whether this object has RsVisbilityBound override.
        /// </summary>
        /// <returns></returns>
        /// Note: we only go one level deep here.
        /// 
        public bool HasVisibilityBoundOverride()
        {
            return objectDef_.HasVisibilityBoundOverride();
        }
        #endregion // Generic Object Type Query Methods

        #region DCC Class Query Methods
        /// <summary>
        /// Determine whether this object is an XRef.
        /// </summary>
        public bool IsXRef()
        {
            return (IsMaxClass(ClassConsts.MAX_CLASS_XREF));
        }

        /// <summary>
        /// Determine whether this object is an InternalRef.
        /// </summary>
        public bool IsInternalRef()
        {
            return (IsMaxClass(ClassConsts.MAX_CLASS_INTERNALREF));
        }

        /// <summary>
        /// Determine whether this object is a Helper subclass.
        /// </summary>
        /// <returns></returns>
        public bool IsHelper()
        {
            return (IsMaxSuperclass(ClassConsts.MAX_CLASS_HELPER));
        }

        /// <summary>
        /// Determine whether this object is a Dummy helper.
        /// </summary>
        /// <returns></returns>
        public bool IsDummy()
        {
            return (IsMaxClass(ClassConsts.MAX_CLASS_DUMMY));
        }

        /// <summary>
        /// Determine whether this object is a Dummy helper.
        /// </summary>
        /// <returns></returns>
        public bool IsBone()
        {
            return (IsMaxClass(ClassConsts.MAX_CLASS_BONE));
        }

        /// <summary>
        /// Determine whether this object is a RS PropGroup Dummy helper.
        /// </summary>
        /// <returns></returns>
        public bool IsPropGroup()
        {
            return (IsDummy() && IsAttributeClass(AttrClass.PROPGROUP));
        }

        /// <summary>
        /// Determine whether this object is a Container helper.
        /// </summary>
        /// <returns></returns>
        public bool IsContainer()
        {
            return (IsMaxClass(ClassConsts.MAX_CLASS_CONTAINER));
        }

        /// <summary>
        /// Determine whether this object is an Editable Spline geometry object.
        /// </summary>
        /// <returns></returns>
        public bool IsEditableSpline()
        {
            return (IsMaxClass(ClassConsts.MAX_CLASS_EDITABLE_SPLINE));
        }

        /// <summary>
        /// Determine whether this object is a 3dsmax Text object.
        /// </summary>
        /// <returns></returns>
        public bool IsText()
        {
            return (IsMaxClass(ClassConsts.MAX_CLASS_TEXT));
        }

        /// <summary>
        /// Determine whether this object is a Line geometry object.
        /// </summary>
        /// <returns></returns>
        public bool IsLine()
        {
            return (IsMaxClass(ClassConsts.MAX_CLASS_LINE));
        }
        /// <summary>
        /// Determine whether this object is an instanced rage light.
        /// </summary>
        /// <returns></returns>
        public bool IsLightInstance()
        {
            return (IsMaxClass(ClassConsts.MAX_LIGHT_INSTANCE));
        }
        /// <summary>
        /// Determine whether this object is an instanced rage light.
        /// </summary>
        /// <returns></returns>
        public bool IsRageLight()
        {
            return (IsMaxClass(ClassConsts.MAX_RAGELIGHT));
        }
        /// <summary>
        /// Determine whether this object is an instanced spawn point.
        /// </summary>
        /// <returns></returns>
        public bool IsSpawnPointInstance()
        {
            return (IsMaxClass(ClassConsts.MAX_SPAWN_POINT_INSTANCE));
        }
        #endregion

        #region Specific Object Attribute Class Query Methods
        /// <summary>
        /// Determine whether this object is active for the platform specified by the ITarget.
        /// </summary>
        public bool IsActive()
        {
            return GetAttribute(AttrNames.RSACTIVE_ACTIVE, AttrDefaults.RSACTIVE_ACTIVE);
        }

        /// <summary>
        /// Determine whether this object is a Gta Block.
        /// </summary>
        public bool IsBlock()
        {
            return (IsAttributeClass(AttrClass.BLOCK));
        }

        /// <summary>
        /// Determine whether this object is a Gta CarGen.
        /// </summary>
        public bool IsCarGen()
        {
            return (IsAttributeClass(AttrClass.CARGEN));
        }

        /// <summary>
        /// Determine whether this object has the Gta Object attribute class and can thus be a valid entity.
        /// </summary>
        public bool IsEntity()
        {
            return (IsAttributeClass(AttrClass.OBJECT));
        }

        /// <summary>
        /// Dertermine whether this object is a Gta GarageArea.
        /// </summary>
        public bool IsGarageArea()
        {
            return (IsAttributeClass(AttrClass.GARAGEAREA));
        }

        /// <summary>
        /// Determine whether this object is a Gta Group.
        /// </summary>
        /// <returns></returns>
        public bool IsGroup()
        {
            return (IsAttributeClass(AttrClass.GROUP));
        }

        /// <summary>
        /// Determine whether this object is a MAX Group.
        /// </summary>
        /// <returns></returns>
        public bool IsMAXGroup()
        {
            return objectDef_.IsMAXGroup();
        }

        /// <summary>
        /// Determine whether this object is a Gta LODMofifier.
        /// </summary>
        public bool IsLODModifier()
        {
            return (IsAttributeClass(AttrClass.LODMODIFIER));
        }

        /// <summary>
        /// Determine whether this object is a Gta Milo.
        /// </summary>
        public bool IsMilo()
        {
            return (IsAttributeClass(AttrClass.MILO));
        }

        public bool IsObjectGroup()
        {
            return IsMaxClass(ClassConsts.MAX_OBJECT_GROUP);
        }

        public bool IsInteriorGroup()
        {
            return IsMaxClass(ClassConsts.MAX_INTERIOR_GROUP);
        }

        /// <summary>
        /// Determine whether this object is a Gta MiloTri.
        /// </summary>
        public bool IsMiloTri()
        {
            String objectName = String.Empty;
            if (null != this.ParamBlocks)
                objectName = this.GetParameter("ObjectName", String.Empty);
            bool isMiloRsRef = objectName.Contains("_milo_");

            return (isMiloRsRef || IsAttributeClass(AttrClass.MILOTRI));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool IsMiloAssoc()
        {
            return (HasParent() && this.Parent.IsMiloTri());
        }

        /// <summary>
        /// Determine whether this object is a Gta MloPortal.
        /// </summary>
        public bool IsMloPortal()
        {
            return (IsAttributeClass(AttrClass.MLOPORTAL));
        }

        /// <summary>
        /// Determine whether this object is a Gta MloRoom.
        /// </summary>
        public bool IsMloRoom()
        {
            return (IsAttributeClass(AttrClass.MLOROOM));
        }

        /// <summary>
        /// Determine whether this object is a Gta Object.
        /// </summary>
        public bool IsObject()
        {
            // IsMiloTro check in for MILO RsRefObjects.
            return (IsAttributeClass(AttrClass.OBJECT) && !IsMiloTri() && !IsOcclusion() && !IsBone());
        }

        /// <summary>
        /// Determine whether this object is an RsRefObject
        /// </summary>
        /// <returns></returns>
        public bool IsRefObject()
        {
            return (IsMaxClass(ClassConsts.MAX_CLASS_RSREF));
        }

        /// <summary>
        /// Determine whether this object is an RsRefObject and has been resolved.
        /// </summary>
        /// <returns></returns>
        public bool IsResolvedRefObject()
        {
            return (this.IsRefObject() && (null != this.RefObject));
        }

        /// <summary>
        /// Dertermine whether this object is an RsInternalRef object.
        /// </summary>
        /// <returns></returns>
        public bool IsRefInternalObject()
        {
            return (IsMaxClass(ClassConsts.MAX_CLASS_RSINTERNALREF));
        }

        /// <summary>
        /// Determine whether this object is an RsContainerLODRefObject
        /// </summary>
        /// <returns></returns>
        public bool IsContainerLODRefObject()
        {
            return (IsMaxClass(ClassConsts.MAX_CLASS_RSCONTAINERLODREF));
        }


        /// <summary>
        /// Determine whether this object is a Gta Pickup.
        /// </summary>
        public bool IsPickup()
        {
            return (IsAttributeClass(AttrClass.PICKUP));
        }

        /// <summary>
        /// Determine whether this object is a Gta TimeCycle.
        /// </summary>
        public bool IsTimeCycleBox()
        {
            return (IsAttributeClass(AttrClass.TIMECYCLE_BOX));
        }

        /// <summary>
        /// Determine whether this object is a Gta TC Sphere.
        /// </summary>
        public bool IsTimeCycleSphere()
        {
            return (IsAttributeClass(AttrClass.TIMECYCLE_SPHERE));
        }

        /// <summary>
        /// Determine whether this object is a Gta Zone.
        /// </summary>
        public bool IsZone()
        {
            return (IsAttributeClass(AttrClass.ZONE));
        }

        /// <summary>
        /// Determine whether this object is a 2dfx.
        /// </summary>
        /// This method uses some .Net reflection magic to invoke all public
        /// methods of this class which begin with "Is2dfx" (excluding
        /// ourselves).
        public bool Is2dfx()
        {
            foreach (MethodInfo methodInfo in s2DFXMethods)
            {
                // Invoke our method.
                Object result = (methodInfo.Invoke(this, null));
                if ((typeof(Boolean) == result.GetType()) && (true == (Boolean)result))
                    return (true);
            }

            return (false);
        }

        /// <summary>
        /// Determine whether this object is a Lod Modifier.
        /// </summary>
        /// <returns></returns>
        public bool IsLodModifier()
        {
            return (IsAttributeClass(AttrClass.LODMODIFIER));
        }

        /// <summary>
        /// Determine whether this object is a Slow Zone.
        /// </summary>
        /// <returns></returns>
        public bool IsSlowZone()
        {
            return (IsMaxClass(ClassConsts.MAX_CLASS_SLOWZONE) || IsAttributeClass(AttrClass.SLOWZONE));
        }

        /// <summary>
        /// Determine whether this object is a Vehicle Node.
        /// </summary>
        /// <returns></returns>
        /// Note: not sure why this also returns true for NetRestart objects.
        public bool IsVehicleNode()
        {
            return (IsAttributeClass(AttrClass.VEHICLE_NODE) || IsAttributeClass(AttrClass.NETRESTART));
        }

        /// <summary>
        /// Determine whether this object is a Vehicle Link.
        /// </summary>
        /// <returns></returns>
        public bool IsVehicleLink()
        {
            return (IsAttributeClass(AttrClass.VEHICLE_LINK));
        }

        /// <summary>
        /// Determine whether this object is a Patrol Node.
        /// </summary>
        /// <returns></returns>
        public bool IsPatrolNode()
        {
            return (IsAttributeClass(AttrClass.PATROL_NODE));
        }

        /// <summary>
        /// Determine whether this object is a Patrol Link.
        /// </summary>
        /// <returns></returns>
        public bool IsPatrolLink()
        {
            return (IsAttributeClass(AttrClass.PATROL_LINK));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool IsVisibilityBound()
        {
            return (IsMaxClass(ClassConsts.MAX_VISIBILITY_BOUND));
        }

        /// <summary>
        /// Determine whether this object is an occlusion object.
        /// </summary>
        /// <returns></returns>
        public bool IsOcclusion()
        {
            return (IsOcclusionBox() || IsOcclusionMesh());
        }

        /// <summary>
        /// Determine whether this object is an occlusion mesh.
        /// </summary>
        /// <returns></returns>
        public bool IsOcclusionMesh()
        {
            return (IsAttributeClass(AttrClass.OBJECT) && GetAttribute(AttrNames.OBJ_IS_OCCLUSION, AttrDefaults.OBJ_IS_OCCLUSION));
        }

        /// <summary>
        /// Determine whether this object is an occlusion box.
        /// </summary>
        /// <returns></returns>
        public bool IsOcclusionBox()
        {
            return (IsMaxClass(ClassConsts.MAX_OCCLUSION_BOX));
        }

        #region 2dfx Object Attribute Class Query Methods
        /// <summary>
        /// Determine whether this object is a 2dfx ladder.
        /// </summary>
        /// <returns></returns>
        public bool Is2dfxLadderFx()
        {
            return (IsAttributeClass(AttrClass.TWODFX_LADDER));
        }

        /// <summary>
        /// Determine whether this object is a 2dfx ladder.
        /// </summary>
        /// <returns></returns>
        public bool Is2dfxRSLadder()
        {
            return (IsAttributeClass(AttrClass.TWODFX_RSLADDER));
        }

        /// <summary>
        /// Determine whether this object is a 2dfx audio emitter.
        /// </summary>
        public bool Is2dfxAudioEmitter()
        {
            return (IsAttributeClass(AttrClass.TWODFX_AUDIO_EMITTER));
        }

        /// <summary>
        /// Determine whether this object is a 2dfx buoyancy.
        /// </summary>
        public bool Is2dfxBuoyancy()
        {
            return (IsAttributeClass(AttrClass.TWODFX_BUOYANCY));
        }

        /// <summary>
        /// Determine whether this object is a 2dfx decal.
        /// </summary>
        public bool Is2dfxDecal()
        {
            return (IsAttributeClass(AttrClass.TWODFX_DECAL));
        }

        /// <summary>
        /// Determine whether this object is a 2dfx explosion effect.
        /// </summary>
        public bool Is2dfxExplosionEffect()
        {
            return (IsAttributeClass(AttrClass.TWODFX_EXPLOSION_EFFECT));
        }

        /// <summary>
        /// Determine whether this object is a 2dfx light effect.
        /// </summary>
        /// <returns></returns>
        public bool Is2dfxLightEffect()
        {
            return ((IsAttributeClass(AttrClass.TWODFX_LIGHT) ||
                IsAttributeClass(AttrClass.TWODFX_LIGHT_PHOTO))
                && (!IsIgnoredMaxLightClass()));
        }

        /// <summary>
        /// Determine whether this object is a 2dfx light shaft.
        /// </summary>
        public bool Is2dfxLightShaft()
        {
            return (IsAttributeClass(AttrClass.TWODFX_LIGHT_SHAFT));
        }

        /// <summary>
        /// Determine whether this object is a 2dfx proc object.
        /// </summary>
        public bool Is2dfxProcObject()
        {
            return (IsAttributeClass(AttrClass.TWODFX_PROC_OBJECT));
        }

        /// <summary>
        /// Determine whether this object is a 2dfx script.
        /// </summary>
        public bool Is2dfxScript()
        {
            return (IsAttributeClass(AttrClass.TWODFX_SCRIPT));
        }

        /// <summary>
        /// Determine whether this object is a 2dfx particle effect.
        /// </summary>
        public bool Is2dfxParticleEffect()
        {
            return (IsAttributeClass(AttrClass.TWODFX_PARTICLE_EFFECT));
        }

        /// <summary>
        /// Determine whether this object is a 2dfx pedqueue.
        /// </summary>
        public bool Is2dfxPedQueue()
        {
            return (IsAttributeClass(AttrClass.TWODFX_PED_QUEUE));
        }

        /// <summary>
        /// Determine whether this object is a 2dfx scrollbar.
        /// </summary>
        public bool Is2dfxScrollbar()
        {
            return (IsAttributeClass(AttrClass.TWODFX_SCROLLBARS));
        }

        /// <summary>
        /// Determine whether this object is a 2dfx spawn point.
        /// </summary>
        public bool Is2dfxSpawnPoint()
        {
            return (IsAttributeClass(AttrClass.TWODFX_SPAWN_POINT));
        }

        /// <summary>
        /// Determine whether this object is a 2dfx swayable effect.
        /// </summary>
        public bool Is2dfxSwayableEffect()
        {
            return (IsAttributeClass(AttrClass.TWODFX_SWAYABLE_EFFECT));
        }

        /// <summary>
        /// Determine whether this object is a 2dfx walk dont walk.
        /// </summary>
        public bool Is2dfxWalkDontWalk()
        {
            return (IsAttributeClass(AttrClass.TWODFX_WALK_DONT_WALK));
        }

        /// <summary>
        /// Determine whether this object is a 2dfx WindowLedge.
        /// </summary>
        public bool Is2dfxWindowLedge()
        {
            return (IsAttributeClass(AttrClass.TWODFX_WINDOW_LEDGE));
        }

        /// <summary>
        /// Determine whether this object is a 2dfx Wind Disturbance.
        /// </summary>
        /// <returns></returns>
        public bool Is2dfxRsWindDisturbance()
        {
            return (IsAttributeClass(AttrClass.TWODFX_WIND_DISTURBANCE));
        }

        /// <summary>
        /// Determine whether this object is a 2dfx Ledge.
        /// </summary>
        public bool Is2dfxLedge()
        {
            return (IsAttributeClass(AttrClass.TWODFX_LEDGE) || IsMaxClass(ClassConsts.MAX_CLASS_RSLEDGE));
        }

        /// <summary>
        /// Determine whether this object is a 2dfx door.
        /// </summary>
        public bool Is2dfxDoor()
        {
            return (IsAttributeClass(AttrClass.TWODFX_DOOR));
        }

        /// <summary>
        /// Determine whether this object is a 2dfx expression.
        /// </summary>
        public bool Is2dfxExpression()
        {
            return (IsAttributeClass(AttrClass.TWODFX_EXPRESSION));
        }
        #endregion // 2dfx Object Attribute Class Query Methods
        #endregion // Specific Object Attribute Class Query Methods

        #region Specific Object Attribute Value Query Methods
        /// <summary>
        /// Determine whether this object is a time object.
        /// </summary>
        public bool IsTimeObject()
        {
            return (this.GetAttribute(AttrNames.OBJ_IS_TIME_OBJECT, AttrDefaults.OBJ_IS_TIME_OBJECT));
        }

        /// <summary>
        /// Determine whether this object is an anim object.
        /// </summary>
        public bool IsAnimObject()
        {
            return (this.GetAttribute(AttrNames.OBJ_HAS_ANIM, AttrDefaults.OBJ_HAS_ANIM) ||
                this.GetAttribute(AttrNames.OBJ_HAS_UV_ANIM, AttrDefaults.OBJ_HAS_UV_ANIM));
        }

        /// <summary>
        /// Determine whether this object is a dynamic object.
        /// </summary>
        public bool IsDynObject()
        {
            return (this.GetAttribute(AttrNames.OBJ_IS_DYNAMIC, AttrDefaults.OBJ_IS_DYNAMIC));
        }

        /// <summary>
        /// Determine whether this object is cloth.
        /// </summary>
        public bool IsCloth()
        {
            return (this.GetAttribute(AttrNames.OBJ_IS_CLOTH, AttrDefaults.OBJ_IS_CLOTH));
        }

        /// <summary>
        /// Determine whether this object is a StatedAnim object.
        /// </summary>
        /// <returns></returns>
        public bool IsStatedAnim()
        {
            return (AttrDefaults.OBJ_GROUP_NAME != this.GetAttribute(AttrNames.OBJ_GROUP_NAME, AttrDefaults.OBJ_GROUP_NAME));
        }

        /// <summary>
        /// Determine whether this object is an statedAnimProxy.
        /// </summary>
        public bool IsAnimProxy()
        {
            String objectName = this.Name;
            bool isStatedAnimRef = objectName.EndsWith(PROXY_ANIM);

            return (isStatedAnimRef && (IsAttributeClass(AttrClass.STATED_ANIM_PROXY) || IsMaxClass(ClassConsts.MAX_CLASS_ANIM_PROXY)));
        }

        /// <summary>
        /// Determine whether this object is an IMAP group dummy
        /// </summary>
        public bool IsIMAPGroupDummy()
        {
            return IsMaxClass(ClassConsts.MAX_IMAP_GROUP_DUMMY);
        }

        /// <summary>
        /// Determine whether this object is an IMAP group dummy
        /// </summary>
        public bool IsIMAPGroupDefinition()
        {
            return IsMaxClass(ClassConsts.MAX_IMAP_GROUP_DEFINITION);
        }

        /// <summary>
        /// Is this object part of an interior entity set?
        /// This requires scanning the scene so is non-trivial to call
        /// </summary>
        /// <returns></returns>
        public bool IsPartOfAnEntitySet()
        {
            if (!isPartOfAnEntitySetEvaluated_)
            {
                isPartOfAnEntitySet_ = IsPartOfAnEntitySetInternal();
                isPartOfAnEntitySetEvaluated_ = true;
            }

            return isPartOfAnEntitySet_;
        }

        /// <summary>
        /// Determine whether this object is a streaming extents override box
        /// </summary>
        public bool IsStreamingExtentsOverrideBox()
        {
            return IsMaxClass(ClassConsts.MAX_STREAMING_EXTENTS_OVERRIDE_BOX);
        }

        /// <summary>
        /// Determine whether this object is a Point Helper.
        /// </summary>
        /// <returns></returns>
        public bool IsPointHelper()
        {
            return IsMaxClass(ClassConsts.MAX_CLASS_POINT_HELPER);
        }

        /// <summary>
        /// Determine whether this object is a Cable Proxy.
        /// </summary>
        /// <returns></returns>
        public bool IsCableProxy()
        {
            return IsMaxClass(ClassConsts.MAX_CLASS_CABLE_PROXY);
        }
        #endregion

        #region DLC Query Methods
        /// <summary>
        /// Determine whether this object is flagged as New DLC.
        /// </summary>
        /// <returns></returns>
        public bool IsNewDLCObject()
        {
            return this.GetAttribute(AttrNames.OBJ_NEW_DLC, AttrDefaults.OBJ_NEW_DLC);
        }

        public bool HasNewTxd()
        {
            return this.GetAttribute(AttrNames.OBJ_TXD_DLC, AttrDefaults.OBJ_NEW_TXD);
        }
        #endregion // DLC Query Methods

        #endregion

        #region Methods
        public bool HasParent()
        {
            return objectDef_.HasParent();
        }

        public void ResolveLinks(Scene scene)
        {
            objectDef_.ResolveLinks(scene);
        }

        public void DetermineLodLevel()
        {
            objectDef_.DetermineLodLevel();
        }

        public float GetLODDistance()
        {
            if (!IsObject() && !IsXRef() && !IsRefObject() && !IsInternalRef() && !IsRefInternalObject() && !IsAnimProxy())
                return (-1.0f);

            if (IsAnimProxy())
                return (this.GetAttribute(AttrNames.STATEDANIM_LOD_DISTANCE, AttrDefaults.STATEDANIM_LOD_DISTANCE));

            // If this object is within a Drawable LOD hierarchy: we return the
            // lowest detail meshes LOD distance (as thats what should be exported).
            if (this.HasDrawableLODParent())
                return (this.DrawableLOD.Parent.GetLODDistance());

            // Default to our "LOD distance" attribute.
            return (this.GetAttribute(AttrNames.OBJ_LOD_DISTANCE, AttrDefaults.OBJ_LOD_DISTANCE));
        }

        public bool HasPriority()
        {
            return Attributes.ContainsKey(AttrNames.OBJ_PRIORITY);
        }

        public int GetPriority()
        {
            return GetAttribute(AttrNames.OBJ_PRIORITY, AttrDefaults.OBJ_PRIORITY);
        }

        public ObjectDef.InstanceType GetInstanceType()
        {
            return objectDef_.GetInstanceType();
        }

        public String GetStatedAnimName()
        {
            return this.GetAttribute(AttrNames.OBJ_GROUP_NAME, AttrDefaults.OBJ_GROUP_NAME);
        }

        public String[] GetPtfxEffectTags()
        {
            List<String> effectTags = new List<String>();
            if (Is2dfxParticleEffect())
            {
                String effectTag = GetAttribute(AttrNames.TWODFX_PARTICLE_NAME,
                    AttrDefaults.TWODFX_PARTICLE_NAME);
                if (!effectTags.Contains(effectTag))
                    effectTags.Add(effectTag);
            }
            foreach (TargetObjectDef obj in Children)
            {
                String[] childTags = obj.GetPtfxEffectTags();
                effectTags.AddRange(childTags);
            }

            return (effectTags.ToArray());
        }

        public String GetObjectName()
        {
            String name = ("Scene_Root");
            if (IsXRef() || IsInternalRef() || IsRefObject() || IsRefInternalObject())
            {
                if (string.IsNullOrEmpty(RefName))
                {
                    // TODO Flo: Log__ErrorCtx was disabled, so creating the message was useless. Any follow up on that ?
                    //String message = String.Format("Invalid RsRef object!  Name '{0}' has no objectName property.", this.Name);
                    //Log.Log__ErrorCtx(this.Name, message);
                }
                else
                {
                    if (RefName.EndsWith(PROXY_MILO))
                        name = RefName.Replace(PROXY_MILO, String.Empty);
                    else if (RefName.EndsWith(PROXY_FRAG))
                        name = RefName.Replace(PROXY_FRAG, String.Empty);
                    else if (RefName.EndsWith(PROXY_ANIM))
                        name = RefName.Replace(PROXY_ANIM, String.Empty);
                    else
                        name = RefName;
                }
            }
            else if (IsAnimProxy() && Name.EndsWith(PROXY_ANIM))
                name = (Name.Replace(PROXY_ANIM, String.Empty));
            else if (IsMiloTri() && Name.EndsWith(PROXY_MILO))
                name = (Name.Replace(PROXY_MILO, String.Empty));
            else if (IsFragment() && Name.EndsWith(PROXY_FRAG))
                name = (Name.Replace(PROXY_FRAG, String.Empty));
            else if (Is2dfx())
            {
                // AJM: Added this clause for 2dfxScript for GTA5 #9455 to support child Gta Scripts linked together 
                // traversing up the hierarchy until it finds a non-2dfcScript object which it's attached to.
                if (this.HasParent() && Is2dfxScript())
                {
                    TargetObjectDef parent = this.Parent;
                    while ((null != parent) && (parent.HasParent()) && parent.Parent.Is2dfxScript())
                    {
                        parent = parent.Parent;
                    }
                    name = parent.GetObjectName();
                }
                // Find 2dfx parent object; walk parent hierarchy until we hit a frag or anim.
                else if (this.HasParent() && this.Parent.IsObject() && !this.Parent.IsHelper())
                {
                    TargetObjectDef parent = this.Parent;
                    while ((null != parent) && (parent.HasParent()) &&
                        (parent.Parent.IsObject()) && (!parent.Parent.IsHelper()))
                    {
                        parent = parent.Parent;
                    }
                    name = parent.GetObjectName();
                }
                else if (this.HasParent())
                {
                    TargetObjectDef parent = this.Parent;
                    while ((null != parent) && (parent.HasParent()))
                    {
                        parent = parent.Parent;
                    }
                    if (null != parent.SkinObj)
                        name = parent.SkinObj.GetObjectName();
                }
            }
            else
            {
                name = this.Name;
            }
            return (name.Replace(" ", "_"));
        }

        public BoundingBox3f GetLocalBoundingBox()
        {
            return objectDef_.GetLocalBoundingBox();
        }

        public TargetObjectDef[] GetChildrenRecursive()
        {
            ICollection<TargetObjectDef> childrenCollection = new List<TargetObjectDef>();
            GetChildrenRecursive(childrenCollection);

            return childrenCollection.ToArray();
        }
        #endregion

        #region Private Methods
        private void GetChildrenRecursive(ICollection<TargetObjectDef> childrenCollection)
        {
            // Breadth first
            foreach (TargetObjectDef childObjectDef in Children)
                childrenCollection.Add(childObjectDef);

            foreach (TargetObjectDef childObjectDef in Children)
                childObjectDef.GetChildrenRecursive(childrenCollection);
        }

        // Evaluating whether an object is part of an entity def is very expensive so we cache the result
        private bool IsPartOfAnEntitySetInternal()
        {
            // Find all of the Interior Groups in the scene.  Is 'this' part of that set?
            TargetObjectDef[] interiorGroups = this.MyScene.InteriorGroups;
            foreach (TargetObjectDef interiorGroup in interiorGroups)
            {
                Object[] groupObjGuids = interiorGroup.GetParameter(ParamNames.INTERIOR_GROUP_OBJECTS, ParamDefaults.INTERIOR_GROUP_OBJECTS);
                foreach (Guid groupObjGuid in groupObjGuids)
                {
                    TargetObjectDef groupObj = this.MyScene.FindObject(groupObjGuid);
                    if (this.Equals(groupObj))
                        return true;
                }
            }

            return false;
        }
        #endregion Private Methods

        #region Static Functions
        public static bool IsLODParentOf(TargetObjectDef child, TargetObjectDef parent)
        {
            if (!child.HasLODParent())
                return (false);

            // Walk up the LOD Parent tree; look for our other entity.
            TargetObjectDef parentTest = child.LOD.Parent;
            while (null != parentTest)
            {
                if (parentTest == parent)
                    return (true);

                if (parentTest.HasLODParent())
                    parentTest = parentTest.LOD.Parent;
                else
                    parentTest = null;
            }

            return (false);
        }

        public static bool IsLODChildOf(TargetObjectDef parent, TargetObjectDef child)
        {
            return (IsLODParentOf(child, parent));
        }
        #endregion

        #region Private Data
        // Evaluating whether an object is part of an entity def is very expensive so we cache the result
        private bool isPartOfAnEntitySetEvaluated_ = false;
        private bool isPartOfAnEntitySet_ = false;
        #endregion Private Data
    }

} // RSG.SceneXml namespace
