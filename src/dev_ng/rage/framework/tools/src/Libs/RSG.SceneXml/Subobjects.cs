//
// File: Subobjects.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of Subobjects class
//

using System;

namespace RSG.SceneXml
{

    /// <summary>
    /// 
    /// </summary>
    public static class Subobjects
    {
        #region GtaLadderFX
        //--------------------------------------------------------------------------
        // GtaLadderFX
        //--------------------------------------------------------------------------	
        public static readonly int TWODFX_LADDER_BOTTOM = 0;
        public static readonly int TWODFX_LADDER_TOP = 1;
        public static readonly int TWODFX_LADDER_NORMAL = 2;
        #endregion // GtaLadderFX
    }

} // RSG.SceneXml

// Subobjects.cs
