﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Attributes;

namespace RSG.SceneXml
{
    
    /// <summary>
    /// Enum representing map type.
    /// </summary>
    public enum SceneType
    {
        // Default value; used when no content-tree present.
        Unknown,

        [XmlConstant("props")]
        EnvironmentProps,

        [XmlConstant("interior")]
        EnvironmentInterior,

        [XmlConstant("map_container")]
        EnvironmentContainer,

        [XmlConstant("lod_container")]
        EnvironmentContainerLod,

        [XmlConstant("occl_container")]
        EnvironmentContainerOcclusion,

        [XmlConstant("map_combine")]
        EnvironmentCombine,

        [XmlConstant("vehicle")]
        Vehicle,

        [XmlConstant("character")]
        Character,

        [XmlConstant("weapon")]
        Weapon
    }

    /// <summary>
    /// SceneType enumeration utility and extension methods.
    /// </summary>
    public static class SceneTypeUtils
    {
        /// <summary>
        /// Convert SceneType to ContentNodeMap.MapSubType.
        /// </summary>
        /// <param name="sceneType"></param>
        /// <returns></returns>
        [Obsolete("Use AP3 ContentTree; Scene.SceneType instead.")]
        public static RSG.Base.ConfigParser.ContentNodeMap.MapSubType GetOldMapType(SceneType sceneType)
        {
            switch (sceneType)
            {
                case SceneType.EnvironmentCombine:
                    return (RSG.Base.ConfigParser.ContentNodeMap.MapSubType.AssetCombine);
                case SceneType.EnvironmentContainer:
                    return (RSG.Base.ConfigParser.ContentNodeMap.MapSubType.Container);
                case SceneType.EnvironmentContainerLod:
                    return (RSG.Base.ConfigParser.ContentNodeMap.MapSubType.ContainerLod);
                case SceneType.EnvironmentContainerOcclusion:
                    return (RSG.Base.ConfigParser.ContentNodeMap.MapSubType.ContainerOccl);
                case SceneType.EnvironmentInterior:
                    return (RSG.Base.ConfigParser.ContentNodeMap.MapSubType.Interior);
                case SceneType.EnvironmentProps:
                    return (RSG.Base.ConfigParser.ContentNodeMap.MapSubType.Props);
                case SceneType.Unknown:
                default:
                    return (RSG.Base.ConfigParser.ContentNodeMap.MapSubType.Unknown);
            }
        }

        /// <summary>
        /// Convert ContentNodeMap.MapSubType to SceneType.
        /// </summary>
        /// <param name="mapNode"></param>
        /// <returns></returns>
        [Obsolete("Use AP3 ContentTree; Scene.SceneType instead.")]
        public static SceneType GetSceneType(RSG.Base.ConfigParser.ContentNodeMap mapNode)
        {
            switch (mapNode.MapType)
            {
                case RSG.Base.ConfigParser.ContentNodeMap.MapSubType.AssetCombine:
                    return (SceneType.EnvironmentCombine);
                case RSG.Base.ConfigParser.ContentNodeMap.MapSubType.Container:
                    return (SceneType.EnvironmentContainer);
                case RSG.Base.ConfigParser.ContentNodeMap.MapSubType.ContainerLod:
                    return (SceneType.EnvironmentContainerLod);
                case RSG.Base.ConfigParser.ContentNodeMap.MapSubType.ContainerOccl:
                    return (SceneType.EnvironmentContainerOcclusion);
                case RSG.Base.ConfigParser.ContentNodeMap.MapSubType.ContainerProps:
                    return (SceneType.EnvironmentContainer);
                case RSG.Base.ConfigParser.ContentNodeMap.MapSubType.Interior:
                    return (SceneType.EnvironmentInterior);
                case RSG.Base.ConfigParser.ContentNodeMap.MapSubType.Props:
                    return (SceneType.EnvironmentProps);
                case RSG.Base.ConfigParser.ContentNodeMap.MapSubType.Unknown:
                default:
                    return (SceneType.Unknown);
            }
        }

        public static SceneType FromContentTreeSceneType(String sceneTypeText)
        {
            if (String.Compare(sceneTypeText, "props", true) == 0)
                return SceneType.EnvironmentProps;
            else if (String.Compare(sceneTypeText, "interior", true) == 0)
                return SceneType.EnvironmentInterior;
            else if (String.Compare(sceneTypeText, "map_container", true) == 0)
                return SceneType.EnvironmentContainer;
            else if (String.Compare(sceneTypeText, "lod_container", true) == 0)
                return SceneType.EnvironmentContainerLod;
            else if (String.Compare(sceneTypeText, "occl_container", true) == 0)
                return SceneType.EnvironmentContainerOcclusion;

            return SceneType.Unknown;
        }
    }

} // RSG.SceneXml namespace

