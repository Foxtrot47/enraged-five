//
// File: Parameters.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of Parameter names and default classes.
//

using System;
using System.Collections.Generic;
using RSG.Base.Math;

namespace RSG.SceneXml
{

    /// <summary>
    /// Parameter Name Constants (within Parameter Blocks)
    /// </summary>
    public static class ParamNames
    {
        #region RsRefObject
        //--------------------------------------------------------------------------
        // RsRefObject
        //--------------------------------------------------------------------------	
        public static readonly String RSREFOBJECT_FILENAME = "filename".ToLower();
        public static readonly String RSREFOBJECT_OBJECTNAME = "objectName".ToLower();
        #endregion // RsRefObject

        #region Gta CarGen
        //--------------------------------------------------------------------------
        // Gta CarGen
        //--------------------------------------------------------------------------	
        public static readonly String CARGEN_WIDTH = "width".ToLower();
        public static readonly String CARGEN_HEIGHT = "height".ToLower();
        public static readonly String CARGEN_DEPTH = "depth".ToLower();
        #endregion // Gta CarGen

        #region Gta LodModifier
        //--------------------------------------------------------------------------
        // Gta LodModifier
        //--------------------------------------------------------------------------
        public static readonly String LODMODIFIER_WIDTH = "0".ToLower();
        public static readonly String LODMODIFIER_LENGTH = "1".ToLower();
        public static readonly String LODMODIFIER_HEIGHT = "2".ToLower();
        public static readonly String LODMODIFIER_LODNODELIST = "LodNodeList".ToLower();
        #endregion // Gta LodModifier

        #region RSLedge
        //--------------------------------------------------------------------------
        // RSLedge
        //--------------------------------------------------------------------------	
        public static readonly String LEDGE_LENGTH = "0".ToLower();
        public static readonly String LEDGE_WIDTH = "1".ToLower();
        public static readonly String LEDGE_GRID = "2".ToLower();
        #endregion // RSLedge

        #region Gta MloPortal
        //--------------------------------------------------------------------------
        // Gta MloPortal
        //--------------------------------------------------------------------------
        public static readonly String MLOPORTAL_ATTACHED_OBJECTS = "PortalObjects".ToLower();	
        public static readonly String MLOPORTAL_FIRST_ROOM = "LinkFirst".ToLower();
        public static readonly String MLOPORTAL_SECOND_ROOM = "LinkSecond".ToLower();
        public static readonly String MLOPORTAL_AUDIO_OCCL_HASH = "AudioOcclusionHash".ToLower();
        public static readonly String MLOPORTAL_LENGTH = "0".ToLower();
        public static readonly String MLOPORTAL_WIDTH = "1".ToLower();
        public static readonly String MLOPORTAL_GRID = "2".ToLower();
        #endregion // Gta MloPortal

        #region Gta Scrollbars
        //--------------------------------------------------------------------------
        // Gta Scrollbars
        //--------------------------------------------------------------------------	
        public static readonly String TWODFX_SCROLLBARS_TYPE = "Type".ToLower();
        public static readonly String TWODFX_SCROLLBARS_HEIGHT = "Height".ToLower();
        public static readonly String TWODFX_SCROLLBARS_NUMPOINTS = "NumPoints".ToLower();
        #endregion // Gta Scrollbars

        #region Gta SpawnPoint
        //--------------------------------------------------------------------------
        // Gta SpawnPoint
        //--------------------------------------------------------------------------	
        public static readonly String TWODFX_SPAWNPOINT_SPAWNTYPE = "spawnType".ToLower();
        public static readonly String TWODFX_SPAWNPOINT_GROUPTYPE = "groupType".ToLower();
        #endregion // Gta SpawnPoint

        #region Gta LightShaft
        //--------------------------------------------------------------------------
        // Gta LightShaft
        //--------------------------------------------------------------------------	
        public static readonly String TWODFX_LIGHTSHAFT_LENGTH = "0".ToLower();
        public static readonly String TWODFX_LIGHTSHAFT_WIDTH = "1".ToLower();
        #endregion // Gta LightShaft

        #region RsOcclusionBox
        //--------------------------------------------------------------------------
        // RsOcclusionBox
        //--------------------------------------------------------------------------	
        public static readonly String OCCLUSIONBOX_LENGTH = "Length".ToLower();
        public static readonly String OCCLUSIONBOX_HEIGHT = "Height".ToLower();
        public static readonly String OCCLUSIONBOX_WIDTH = "Width".ToLower();
        #endregion // RsOcclusionBox

        #region RSLadder
        //--------------------------------------------------------------------------
        // RSLadder
        //--------------------------------------------------------------------------	
        public static readonly String TWODFX_RSLADDER_HEIGHT = "ladderHeight".ToLower();
        public static readonly String TWODFX_RSLADDER_TEMPLATE = "template".ToLower();
        public static readonly String TWODFX_RSLADDER_BOTTOM_NODE_Z_OFFSET = "bottomNodePosZ".ToLower();
        public static readonly String TWODFX_RSLADDER_MATERIAL_TYPE = "materialType".ToLower();
        #endregion // RSLadder

        #region RsWindDisturbance
        //--------------------------------------------------------------------------
        // RsWindDisturbance
        //--------------------------------------------------------------------------	
        public static readonly String TWODFX_WIND_SIZE_RADIUS = "windSize".ToLower();
        #endregion // RsWindDisturbance

        #region Lights
        public static readonly String[] TWODFX_LIGHT_TYPE_NAMES =               { "undefined", "omni_light", "free_spot", "free_directional_light", "targeted_spot", "targeted_directional_light" };
        public static readonly String[] TWODFX_LIGHT_ENABLED =                  { "lightEnabled".ToLower() };
        public static readonly String[] TWODFX_LIGHT_TYPE =                     { "lightType".ToLower() };
        public static readonly String[] TWODFX_LIGHT_COLOUR =                   { "lightColour".ToLower(),      "Color".ToLower() };
        public static readonly String[] TWODFX_LIGHT_INTENSITY =                { "lightIntensity".ToLower(),   "Multiplier".ToLower() };
        public static readonly String[] TWODFX_LIGHT_ATTENUATION_END =          { "lightAttenEnd".ToLower(),    "Attenuation Far End".ToLower() };
        public static readonly String[] TWODFX_LIGHT_HOTSPOT =                  { "lightHotspot".ToLower(),     "Hotspot".ToLower() };
        public static readonly String[] TWODFX_LIGHT_CAPSULE_WIDTH =            { "lightWidthAttenEnd".ToLower() };
        public static readonly String[] TWODFX_LIGHT_FALLSIZE =                 { "lightFalloff".ToLower(), "Falloff".ToLower() };
        public static readonly String[] TWODFX_LIGHT_SHADOW_MAP =               { "lightShadowMap".ToLower() };
        public static readonly String[] TWODFX_LIGHT_EXP_FALLOFF =              { "lightExpFalloff".ToLower() };
        public static readonly String[] TWODFX_LIGHT_USE_CULLPLANE =            { "lightUseCullPlane".ToLower() };
        public static readonly String[] TWODFX_LIGHT_USE_VOL_OUTER_COLOUR =     { "lightUseOuterColour".ToLower() };
        public static readonly String[] TWODFX_LIGHT_CULLPLANE =                { "lightCullPlane".ToLower() };
        public static readonly String[] TWODFX_LIGHT_VOL_OUTER_COLOUR =         { "lightOuterColour".ToLower() };
        public static readonly String[] TWODFX_LIGHT_VOL_OUTER_COLOUR_INTEN =   { "lightOuterColourInten".ToLower() };
        public static readonly String[] TWODFX_LIGHT_VOL_OUTER_COLOUR_EXP   =   { "lightOuterColourExp".ToLower() };

        /// <summary>
        /// TWODFX_SCENEXML_LIGHTTYPES
        /// The indeces used in SceneXML for the exported lights.
        /// </summary>
        public enum TWODFX_SCENEXML_LIGHTTYPES
        {
            TWODFX_SCENEXML_LIGHTTYPE_OMNI              = 1,
            TWODFX_SCENEXML_LIGHTTYPE_FREESPOT          = 2,
            TWODFX_SCENEXML_LIGHTTYPE_FREEDIRECTIONAL   = 3,
            TWODFX_SCENEXML_LIGHTTYPE_TARGETSPOT        = 4,
            TWODFX_SCENEXML_LIGHTTYPE_TARGETDIRECTIONAL = 5,
            TWODFX_SCENEXML_LIGHTTYPE_CAPSULE           = 6
        }
        #endregion //RageLIght

        #region IMAP Group Helper
        public static readonly String IMAP_GROUP_TYPES = "imapGroupTypes".ToLower();
        public static readonly String IMAP_GROUP_WEATHER_TYPES = "imapGroupWeatherTypes".ToLower();
        public static readonly String IMAP_GROUP_TIME = "imapGroupTime".ToLower();
        #endregion

        #region ObjectGroup
        public static readonly String OBJECT_GROUP_NAME = "objectGroupName".ToLower();
        #endregion

        #region interiorGroup
        public static readonly String INTERIOR_GROUP_OBJECTS = "interiorGroupObjects".ToLower();
        #endregion
    }

    /// <summary>
    /// Parameter Default Value Constants
    /// </summary>
    public static class ParamDefaults
    {
        #region RsRefObject
        //--------------------------------------------------------------------------
        // RsRefObject
        //--------------------------------------------------------------------------	
        public static readonly String RSREFOBJECT_FILENAME = String.Empty;
        public static readonly String RSREFOBJECT_OBJECTNAME = String.Empty;
        #endregion // RsRefObject

        #region Gta CarGen
        //--------------------------------------------------------------------------
        // Gta CarGen
        //--------------------------------------------------------------------------	
        public static readonly float CARGEN_WIDTH = 1.0f;
        public static readonly float CARGEN_HEIGHT = 1.0f;
        public static readonly float CARGEN_DEPTH = 1.0f;
        #endregion // Gta CarGen

        #region Gta LodModifier
        //--------------------------------------------------------------------------
        // Gta LodModifier
        //--------------------------------------------------------------------------
        public static readonly float LODMODIFIER_WIDTH = 0.0f;
        public static readonly float LODMODIFIER_LENGTH = 0.0f;
        public static readonly float LODMODIFIER_HEIGHT = 0.0f;
        #endregion // Gta LodModifier

        #region RSLedge
        //--------------------------------------------------------------------------
        // RSLedge
        //--------------------------------------------------------------------------	
        public static readonly float LEDGE_LENGTH = 0.0f;
        public static readonly float LEDGE_WIDTH = 0.0f;
        public static readonly float LEDGE_GRID = 0.0f;
        #endregion // RSLedge

        #region Gta MloPortal
        //--------------------------------------------------------------------------
        // Gta MloPortal
        //--------------------------------------------------------------------------
        public static readonly Object[] MLOPORTAL_ATTACHED_OBJECTS = { System.Guid.Empty, System.Guid.Empty, System.Guid.Empty, System.Guid.Empty };
        public static readonly Guid MLOPORTAL_FIRST_ROOM = Guid.Empty;
        public static readonly Guid MLOPORTAL_SECOND_ROOM = Guid.Empty;
        public static readonly string MLOPORTAL_AUDIO_OCCL_HASH = "";
        public static readonly float MLOPORTAL_LENGTH = 0.0f;
        public static readonly float MLOPORTAL_WIDTH = 0.0f;
        public static readonly float MLOPORTAL_GRID = 0.0f;
        #endregion // Gta MloPortal

        #region Gta Scrollbars
        //--------------------------------------------------------------------------
        // Gta Scrollbars
        //--------------------------------------------------------------------------	
        public static readonly int TWODFX_SCROLLBARS_TYPE = 0;
        public static readonly float TWODFX_SCROLLBARS_HEIGHT = 1.0f;
        public static readonly int TWODFX_SCROLLBARS_NUMPOINTS = 1;
        #endregion // Gta Scrollbars

        #region Gta SpawnPoint
        //--------------------------------------------------------------------------
        // Gta SpawnPoint
        //--------------------------------------------------------------------------	
        public static readonly String TWODFX_SPAWNPOINT_SPAWNTYPE = "PROP_HUMAN_SEAT_BENCH";
        public static readonly String TWODFX_SPAWNPOINT_GROUPTYPE = String.Empty;
        #endregion // Gta SpawnPoint

        #region Gta LightShaft
        //--------------------------------------------------------------------------
        // Gta LightShaft
        //--------------------------------------------------------------------------	
        public static readonly float TWODFX_LIGHTSHAFT_LENGTH = 0.0f;
        public static readonly float TWODFX_LIGHTSHAFT_WIDTH = 0.0f;
        #endregion // Gta LightShaft

        #region RsOcclusionBox
        //--------------------------------------------------------------------------
        // RsOcclusionBox
        //--------------------------------------------------------------------------	
        public static readonly float OCCLUSIONBOX_LENGTH = 10.0f;
        public static readonly float OCCLUSIONBOX_HEIGHT = 10.0f;
        public static readonly float OCCLUSIONBOX_WIDTH = 10.0f;
        #endregion // RsOcclusionBox

        #region RSLadder
        //--------------------------------------------------------------------------
        // RSLadder
        //--------------------------------------------------------------------------	
        public static readonly float TWODFX_RSLADDER_HEIGHT = 3.0f;
        public static readonly String TWODFX_RSLADDER_TEMPLATE = "DEFAULT";
        public static readonly float TWODFX_RSLADDER_BOTTOM_NODE_Z_OFFSET = 0.0f;
        public static readonly int TWODFX_RSLADDER_MATERIAL_TYPE = 1;           // Set to 1 as the default max value is 1
        #endregion // RSLadder

        #region RsWindDisturbance
        //--------------------------------------------------------------------------
        // RsWindDisturbance
        //--------------------------------------------------------------------------	
        public static readonly float TWODFX_WIND_SIZE_RADIUS = 1.0f;
        #endregion // RsWindDisturbance

        #region Gta Light / Gta LightPhoto
        public static readonly int TWODFX_LIGHT_TYPE = 1;
        public static readonly Vector3f TWODFX_LIGHT_COLOUR = new Vector3f(1.0f, 1.0f, 1.0f);
        public static readonly bool TWODFX_LIGHT_ENABLED = true;
        public static readonly float TWODFX_LIGHT_INTENSITY = 1.0f;
        public static readonly float TWODFX_LIGHT_ATTENUATION_END = 50.0f;
        public static readonly float TWODFX_LIGHT_CAPSULE_WIDTH = 1.0f;
        public static readonly bool TWODFX_LIGHT_IS_SPOT = false;
        public static readonly float TWODFX_LIGHT_HOTSPOT = 20.0f;
        public static readonly float TWODFX_LIGHT_FALLSIZE = 50.0f;
        public static readonly String TWODFX_LIGHT_SHADOW_MAP = null;
        public static readonly float TWODFX_LIGHT_EXP_FALLOFF = 8.0f;
        public static readonly bool TWODFX_LIGHT_USE_CULLPLANE = false;
        public static readonly bool TWODFX_LIGHT_USE_VOL_OUTER_COLOUR = false;
        public static readonly Vector3f TWODFX_LIGHT_VOL_OUTER_COLOUR = new Vector3f(1.0f, 1.0f, 1.0f);
        public static readonly Vector4f TWODFX_LIGHT_CULLPLANE = new Vector4f(0, 0, 1, 25);
        public static readonly float TWODFX_LIGHT_VOL_OUTER_COLOUR_INTEN = 1.0f;
        public static readonly float TWODFX_LIGHT_VOL_OUTER_COLOUR_EXP = 1.0f;
        #endregion // Gta Light / Gta LightPhoto

        #region IMAP Group Helper
        public static readonly String IMAP_GROUP_NAME = "";
        public static readonly Object[] IMAP_GROUP_TYPES = new Object[0];
        public static readonly Object[] IMAP_GROUP_WEATHER_TYPES = new Object[0];
        public static readonly int IMAP_GROUP_TIME = 0;
        #endregion

        #region ObjectGroup
        public static readonly String OBJECT_GROUP_NAME = "";
        #endregion

        #region interiorGroup
        public static readonly Object[] INTERIOR_GROUP_OBJECTS = new Object[0];
        #endregion
    }

    #region Helper functions
    public static class ParamHelpers
    {
        public static T GetLightParam<T>(TargetObjectDef lightObj, string[] paramName, T paramDefault)
        {
            int index = 1;

            if (lightObj.IsRageLight() || lightObj.IsLightInstance())
            {
                index = 0;
            }
            else
            {
                if (paramName == ParamNames.TWODFX_LIGHT_TYPE)
                {
                    List<string> nameList = new List<string>(ParamNames.TWODFX_LIGHT_TYPE_NAMES);
                    object classNameIndex = nameList.IndexOf(lightObj.Class);
                    return (T)classNameIndex;
                }
                if (paramName == ParamNames.TWODFX_LIGHT_SHADOW_MAP)
                {
                    try
                    {
                        sRefdTexture tex = lightObj.RefdTextures.Find(aTex => aTex.m_refNum == 2);
                        return (T)(object)tex.m_filename;
                    }
                    catch (ArgumentNullException /*ex*/)
                    {
                        return (T)(object)"";
                    }
                }
            }
            
            if (index >= paramName.Length)
                index = 0;

            return lightObj.GetParameter(paramName[index], paramDefault);
        }

        /// <summary>
        /// Does a lookup in the array of overridden property names and gives back value from according object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="instObj">instanced light object</param>
        /// <param name="propKey">name of property to look up</param>
        /// <param name="defaultVal">default value to give back if property not on any object</param>
        /// <param name="instProps">array of overridden properties</param>
        /// <returns></returns>
        public static T GetLightInstanceParameter<T>(TargetObjectDef instObj, string[] propKey, T defaultVal, List<string> instProps)
        {
            // is this an instance overridden prop?
            if (instProps.Contains(propKey[0]) || (propKey.Length>1 && instProps.Contains(propKey[1])))
                return (T)GetLightParam(instObj, propKey, defaultVal);
            //otherwise get the prop of the referenced object
            return GetLightParam(instObj.RefObject, propKey, defaultVal);
        }

        /// <summary>
        /// Set flag if property is true.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="prop"></param>
        /// <param name="defVal"></param>
        /// <param name="flag"></param>
        /// <param name="flags"></param>
        public static void SetLightParamFlag(SceneXml.TargetObjectDef obj, String[] prop, bool defVal, UInt32 flag, ref UInt32 flags)
        {
            bool attrVal = GetLightParam(obj, prop, defVal);
            if (attrVal)
                flags |= flag;
            else
                flags &= ~flag;
        }
    }

    #endregion

} // RSG.SceneXml namespace

// Parameters.cs
