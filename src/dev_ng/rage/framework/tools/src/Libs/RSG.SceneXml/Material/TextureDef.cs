﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using RSG.Base.Logging;

namespace RSG.SceneXml.Material
{
    /// <summary>
    /// Represents a texture that has been defined in a SceneXml file.
    /// </summary>
    public class TextureDef
    {
        #region Constants
        #region MultiMaterial Attribute Names
        private static readonly String sC_s_s_TextureFilePath = "filename";
        private static readonly String sC_s_s_TextureWidth = "width";
        private static readonly String sC_s_s_TextureHeight = "height";
        private static readonly String sC_s_s_TextureType = "type";
        #endregion // MultiMaterial Attribute Names
        #region XPath Compiled Expressions
        private static readonly XPathExpression sC_s_xpath_XPathTextureDefAttrs = XPathExpression.Compile("@*");
        #endregion // XPath Compiled Expressions
        #endregion // Constants

        #region Properties and Associated Member Data

        /// <summary>
        /// Texture file path.
        /// </summary>
        public String FilePath
        {
            get { return m_sFilePath; }
        }
        private String m_sFilePath;

        /// <summary>
        /// Parent material in Scene.
        /// </summary>
        public MaterialDef Parent
        {
            get { return m_Parent; }
            internal set { m_Parent = value; }
        }
        private MaterialDef m_Parent;

        /// <summary>
        /// Texture type
        /// </summary>
        public TextureTypes Type
        {
            get { return m_Type; }
        }
        private TextureTypes m_Type;
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor, from XPathNavigator.
        /// </summary>
        public TextureDef(XPathNavigator navigator, MaterialDef parent)
        {
            try
            {
                this.m_Parent = parent;
                Parse( navigator );
            }
            catch (Exception ex)
            {
                String message = String.Format("Invalid Material Definition: {0} {1}.", this.m_sFilePath, navigator.InnerXml);
                SceneParseException sceneEx = new SceneParseException(message, ex);
                Log.Log__Exception(sceneEx, "Error parsing SceneXml.");
                throw sceneEx;
            }
        }

        /// <summary>
        /// Constructor, from XPathNavigator.
        /// </summary>
        public TextureDef(XmlTextReader reader, MaterialDef parent)
        {
            this.m_Parent = parent;
            Parse(reader);
        }
        #endregion // Constructor(s)

        #region Private Functions

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        private void ParseSkip(XmlTextReader reader)
        {
            if (reader.IsEmptyElement)
                reader.ReadStartElement();
            else
            {
                reader.ReadStartElement();
                while (reader.MoveToContent() == XmlNodeType.Element)
                {
                    ParseSkip(reader);
                }
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="navigator"></param>
        private void Parse(XmlTextReader reader)
        {
            if (reader.IsEmptyElement)
                ParseAttributes(reader);
            else
            {
                ParseAttributes(reader);
                ParseMembers(reader);
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="reader"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        private String GetAttribute(String name, XmlTextReader reader, String defaultValue)
        {
            String attribute = reader.GetAttribute(name);
            if (String.IsNullOrWhiteSpace(attribute) || String.IsNullOrEmpty(attribute))
            {
                attribute = defaultValue;
            }
            return attribute;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="reader"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        private int GetAttributeAsInt(String name, XmlTextReader reader, int defaultValue)
        {
            int integer = defaultValue;
            String attribute = reader.GetAttribute(name);
            if (!String.IsNullOrWhiteSpace(attribute) && !String.IsNullOrEmpty(attribute))
            {
                integer = int.Parse(attribute);
            }
            return integer;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        private void ParseAttributes(XmlTextReader reader)
        {
            this.m_sFilePath = this.GetAttribute(sC_s_s_TextureFilePath, reader, String.Empty);
            String type = this.GetAttribute(sC_s_s_TextureType, reader, String.Empty);
            if (!String.IsNullOrEmpty(type) && !String.IsNullOrWhiteSpace(type))
            {
                switch (type)
                {
                    case "Diffuse Texture":
                        this.m_Type = TextureTypes.DiffuseMap;
                        break;
                    case "Bump Texture":
                        this.m_Type = TextureTypes.BumpMap;
                        break;
                    case "Specular Texture":
                        this.m_Type = TextureTypes.SpecularMap;
                        break;
                    case "Diffuse Texture Alpha":
                        this.m_Type = TextureTypes.DiffuseAlpha;
                        break;
                    case "Bump Texture Alpha":
                        this.m_Type = TextureTypes.BumpAlpha;
                        break;
                    case "Specular Texture Alpha":
                        this.m_Type = TextureTypes.SpecularAlpha;
                        break;
                    case "Ambient Texture":
                        this.m_Type = TextureTypes.AmbientMap;
                        break;
                    case "Specular Level":
                        this.m_Type = TextureTypes.SpecularLevelMap;
                        break;
                    case "Glossiness":
                        this.m_Type = TextureTypes.GlossMap;
                        break;
                    case "Self-illumination":
                        this.m_Type = TextureTypes.SelfIlluminationMap;
                        break;
                    case "Opacity Texture":
                        this.m_Type = TextureTypes.OpacityMap;
                        break;
                    case "Filter color Texture":
                        this.m_Type = TextureTypes.FilterColourMap;
                        break;
                    case "Reflection Texture":
                        this.m_Type = TextureTypes.ReflectionMap;
                        break;
                    case "Refraction Texture":
                        this.m_Type = TextureTypes.RefractionMap;
                        break;
                    case "Displacement Texture":
                        this.m_Type = TextureTypes.DisplacementMap;
                        break;
                    default:
                        if (type.Contains("Diffuse"))
                            this.m_Type = TextureTypes.DiffuseMap;
                        else if (type.Contains("Bump"))
                            this.m_Type = TextureTypes.BumpMap;
                        else if (type.Contains("Specular"))
                            this.m_Type = TextureTypes.SpecularMap;
                        else
                            this.m_Type = TextureTypes.None;
                        break;
                }
            }
            reader.ReadStartElement();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="navigator"></param>
        private void ParseMembers(XmlTextReader reader)
        {
            while (reader.MoveToContent() == XmlNodeType.Element)
            {
                this.ParseSkip(reader);
            }
            if (reader.NodeType == XmlNodeType.Text)
                reader.ReadString();
            if (reader.NodeType == XmlNodeType.EndElement)
                reader.ReadEndElement();
        }

        private void Parse(XPathNavigator navigator)
        {
            XPathNodeIterator textureAttrIt = navigator.Select(sC_s_xpath_XPathTextureDefAttrs);
            while (textureAttrIt.MoveNext())
            {
                if (typeof(String) != textureAttrIt.Current.ValueType)
                    continue;
                String value = (textureAttrIt.Current.TypedValue as String);

                if (sC_s_s_TextureFilePath == textureAttrIt.Current.Name)
                {
                    this.m_sFilePath = value;
                }
                if (sC_s_s_TextureType == textureAttrIt.Current.Name)
                    switch (value)
                    {
                        case "Diffuse Texture":
                            this.m_Type = TextureTypes.DiffuseMap;
                            break;
                        case "Bump Texture":
                            this.m_Type = TextureTypes.BumpMap;
                            break;
                        case "Specular Texture":
                            this.m_Type = TextureTypes.SpecularMap;
                            break;
                        case "Diffuse Texture Alpha":
                            this.m_Type = TextureTypes.DiffuseAlpha;
                            break;
                        case "Bump Texture Alpha":
                            this.m_Type = TextureTypes.BumpAlpha;
                            break;
                        case "Specular Texture Alpha":
                            this.m_Type = TextureTypes.SpecularAlpha;
                            break;
                        case "Ambient Texture":
                            this.m_Type = TextureTypes.AmbientMap;
                            break;
                        case "Specular Level":
                            this.m_Type = TextureTypes.SpecularLevelMap;
                            break;
                        case "Glossiness":
                            this.m_Type = TextureTypes.GlossMap;
                            break;
                        case "Self-illumination":
                            this.m_Type = TextureTypes.SelfIlluminationMap;
                            break;
                        case "Opacity Texture":
                            this.m_Type = TextureTypes.OpacityMap;
                            break;
                        case "Filter color Texture":
                            this.m_Type = TextureTypes.FilterColourMap;
                            break;
                        case "Reflection Texture":
                            this.m_Type = TextureTypes.ReflectionMap;
                            break;
                        case "Refraction Texture":
                            this.m_Type = TextureTypes.RefractionMap;
                            break;
                        case "Displacement Texture":
                            this.m_Type = TextureTypes.DisplacementMap;
                            break;
                        default:
                            this.m_Type = TextureTypes.None;
                            break;
                    }
            }
        }
        #endregion // Private Functions
    }
}
