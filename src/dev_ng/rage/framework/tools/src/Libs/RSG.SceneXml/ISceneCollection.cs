﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.SceneXml
{

    /// <summary>
    /// SceneCollection interface.
    /// </summary>
    public interface ISceneCollection
    {
        #region Methods
        /// <summary>
        /// Load a new Scene from XML.
        /// </summary>
        /// <param name="filename">Absolute filename to SceneXml.</param>
        /// <returns>Scene object or null</returns>
        /// This is a separate method to the "GetScene" method to aid readability
        /// of user code; so that a complete SceneXml filename can be used
        Scene LoadScene(String filename);

        /// <summary>
        /// Overload for loading a new Scene from XML to allow the user to specify
        /// whether to use a XmlTextReader instead of a XmlDocument when parsing a scene
        /// </summary>
        /// <param name="filename">Absolute filename to SceneXml.</param>
        /// <param name="useTextReader">Flag indicating whether we should use a XmlTextReader</param>
        /// <returns>Scene object or null</returns>
        Scene LoadScene(String filename, bool useTextReader);

        /// <summary>
        /// Unloads a Scene based on its XML filename
        /// </summary>
        /// <param name="filename">Absolute filename to SceneXml.</param>
        void UnloadScene(String filename);

        /// <summary>
        /// Return a Scene object for the specified map name.
        /// </summary>
        /// <param name="name">Map name (e.g. "dt1_01", "v_construction")</param>
        /// <returns>Scene object or null.</returns>
        /// The Scene objects are cached; so they are only loaded once.  When
        /// loaded any external references are automatically resolved.
        /// 
        /// This resolution is the reason you *need* to use SceneCollection!
        /// 
        Scene GetScene(String name);

        /// <summary>
        /// Find an object.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="sceneName"></param>
        /// <returns>Pair of Scene name and found ObjectDef (or null)</returns>
        RSG.Base.Collections.Pair<String, TargetObjectDef> Find(String name, String sceneName = null);
        #endregion // Methods
    }

} // RSG.SceneXml namespace
