//
// File: ObjectDef.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of ObjectDef class
//

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.XPath;
using RSG.Base.Logging;
using RSG.Base.Math;
using RSG.Base.Extensions;
using RSG.ObjectLinks;

namespace RSG.SceneXml
{
    // AttributeContainer type alias.
    using AttributeContainer = Dictionary<String, Object>;
    using KeyContainer = Dictionary<String, List<sKeyFrame>>;
    #region Types
    /// <summary>
    /// Struct for saving a keyframe'd property
    /// </summary>
    public struct sKeyFrame
    {
        public int m_nTime;
        public int m_nState;
    }

    /// <summary>
    /// A texture that is referenced by a node, but not necessarily in the paramblocks.
    /// </summary>
    public struct sRefdTexture
    {
        public string m_name;
        public int m_refNum;
        public string m_filename;
    }

    /// <summary>
    /// Type to describe a node in the RsScenelinkHierarchy
    /// </summary>
    public struct sSceneLinkChannel
    {
        public String Name;
        public ObjectDef Parent;
        public List<ObjectDef> Children;
    }

    #endregion // Types

    /// <summary>
    /// Struct used to convert dictionaries into arrays/lists.
    /// </summary>
    [Obsolete]
    public class AttributeListItem
    {
        public string AttributeName
        {
            get { return m_AttributeName; }
            set { m_AttributeName = value; }
        }

        public object AttributeObject
        {
            get { return m_AttributeObject; }
            set { m_AttributeObject = value; }
        }

        private string m_AttributeName;
        private object m_AttributeObject;
    };

    /// <summary>
    /// Scene object class.
    /// </summary>
    /// The ObjectDef class describes a RSG.SceneXml scene object.  These are
    /// created when the RSG.SceneXml file is parsed.
    public class ObjectDef
    {
        #region Constants
        #region XPath Compiled Expressions
        private static readonly XPathExpression sC_s_xpath_XPathObjectDefAttrs =
            XPathExpression.Compile("@*");
        private static readonly XPathExpression sC_s_xpath_XPathChildObjectDefs =
            XPathExpression.Compile("children/object");
        private static readonly XPathExpression sC_s_xpath_XPathLODHierarchy =
            XPathExpression.Compile("lod_hierarchy");
        private static readonly XPathExpression sC_s_xpath_XPathDrawableLODHierarchy =
            XPathExpression.Compile("lod_drawable_hierarchy");
        private static readonly XPathExpression sC_s_xpath_XPathLinkChannel =
            XPathExpression.Compile("scene_link_hierarchy/channel");
        private static readonly XPathExpression sC_s_xpath_XPathLinkChild = 
            XPathExpression.Compile("children/child");
        private static readonly XPathExpression sC_s_xpath_XPathLinkParent = 
            XPathExpression.Compile("parent");
        private static readonly XPathExpression sC_s_xpath_XPathLocalBoundingBoxMin =
            XPathExpression.Compile("boundingbox3/local/min");
        private static readonly XPathExpression sC_s_xpath_XPathLocalBoundingBoxMax =
            XPathExpression.Compile("boundingbox3/local/max");
        private static readonly XPathExpression sC_s_xpath_XPathWorldBoundingBoxMin =
            XPathExpression.Compile("boundingbox3/world/min");
        private static readonly XPathExpression sC_s_xpath_XPathWorldBoundingBoxMax =
            XPathExpression.Compile("boundingbox3/world/max");
        private static readonly XPathExpression sC_s_xpath_XPathObjectTransformPos =
            XPathExpression.Compile("transform/object/position");
        private static readonly XPathExpression sC_s_xpath_XPathObjectTransformRot =
            XPathExpression.Compile("transform/object/rotation");
        private static readonly XPathExpression sC_s_xpath_XPathObjectTransformScale =
            XPathExpression.Compile("transform/object/scale");
        private static readonly XPathExpression sC_s_xpath_XPathNodeTransformPos =
            XPathExpression.Compile("transform/node/position");
        private static readonly XPathExpression sC_s_xpath_XPathNodeTransformRot =
            XPathExpression.Compile("transform/node/rotation");
        private static readonly XPathExpression sC_s_xpath_XPathNodeTransformScale =
            XPathExpression.Compile("transform/node/scale");
        private static readonly XPathExpression sC_s_xpath_XPathSubObject =
            XPathExpression.Compile("subobjects/object");
        private static readonly XPathExpression sC_s_xpath_XPathAttrClass =
            XPathExpression.Compile("attributes/@class");
        private static readonly XPathExpression sC_s_xpath_XPathAttrGuid =
            XPathExpression.Compile("attributes/@guid");
        private static readonly XPathExpression sC_s_xpath_XPathAttr =
            XPathExpression.Compile("attributes/attribute");
        private static readonly XPathExpression sC_s_xpath_XPathPlatformAttrPlatform =
            XPathExpression.Compile("platformAttributes/platform");
        private static readonly XPathExpression sC_s_xpath_XPathPlatformAttr =
            XPathExpression.Compile("@name");
        private static readonly XPathExpression sC_s_xpath_XPathPlatformAttribute =
            XPathExpression.Compile("attribute");
        private static readonly XPathExpression sC_s_xpath_XPathUserProperty =
            XPathExpression.Compile("user_properties/property");
        private static readonly XPathExpression sC_s_xpath_XPathProperty =
            XPathExpression.Compile("properties/property");
        private static readonly XPathExpression sC_s_xpath_XPathCustomAttr =
            XPathExpression.Compile("custom_attrs/custom");
        private static readonly XPathExpression sC_s_xpath_XPathParamBlock =
            XPathExpression.Compile("paramblocks/paramblock");
        private static readonly XPathExpression sC_s_xpath_XPathRefdTextures =
            XPathExpression.Compile("refdTextures/texture");
        private static readonly XPathExpression sC_s_xpath_XPathParamBlockParam =
            XPathExpression.Compile("parameter");
        private static readonly XPathExpression sC_s_xpath_XPathObjectRef =
            XPathExpression.Compile("references/ref/@guid");
        private static readonly XPathExpression sC_s_xpath_XPathPolycount =
            XPathExpression.Compile("polycount/@value");

        private static readonly XPathExpression sC_s_xpath_XPathAnimSkeletonRootObj =
            XPathExpression.Compile("anim_attrs/skeleton_root_obj/@guid");
        private static readonly XPathExpression sC_s_xpath_XPathAnimLength =
            XPathExpression.Compile("anim_attrs/animation_length/@value");
        private static readonly XPathExpression sC_s_xpath_XPathAnimKeys =
            XPathExpression.Compile("anim_attrs/keys");
        private static readonly XPathExpression sC_s_xpath_XPathAnimKeyAttrs =
            XPathExpression.Compile("@*");
        #endregion // XPath Compiled Expressions
        #region Object Attribute Names
        private static readonly String sC_s_s_ObjectName = "name";
        private static readonly String sC_s_s_ObjectGuid = "guid";
        private static readonly String sC_s_s_ObjectMaterial = "material";
        private static readonly String sC_s_s_ObjectClass = "class";
        private static readonly String sC_s_s_ObjectSuperclass = "superclass";
        private static readonly String sC_s_s_ObjectRefName = "refname";
        private static readonly String sC_s_s_ObjectRefName2 = "objectname";
        private static readonly String sC_s_s_ObjectRefFile = "reffile";
        private static readonly String sC_s_s_ObjectRefFile2 = "filename";
        private static readonly String sC_s_s_ObjectRef = "ref";

        #endregion // Object Attribute Names
        #region Attribute/Property/Parameter Attribute Names
        private static readonly String sC_s_s_AttrName = "name";
        private static readonly String sC_s_s_AttrType = "type";
        private static readonly String sC_s_s_AttrRefnum = "refNum";
        private static readonly String sC_s_s_AttrFilename = "filename";
        private static readonly String sC_s_s_AttrValue = "value";
        private static readonly String sC_s_s_AttrFrame = "frame";
        private static readonly String sC_s_s_AttrTypeArray = "array";
        private static readonly String sC_s_s_AttrTypeInt = "int";
        private static readonly String sC_s_s_AttrTypeFloat = "float";
        private static readonly String sC_s_s_AttrTypeBool = "bool";
        private static readonly String sC_s_s_AttrTypeRGB = "rgb";
        private static readonly String sC_s_s_AttrTypeRGBA = "rgba";
        private static readonly String sC_s_s_AttrTypeVectorXYZ = "xyz";
        private static readonly String sC_s_s_AttrTypeVectorXYZW = "xyzw";
        private static readonly String sC_s_s_AttrTypeObject = "object";

        private static readonly String sC_s_s_TransformElement = "transform";
        private static readonly String sC_s_s_BoundingBoxElement = "boundingbox3";
        private static readonly String sC_s_s_PolyCountElement = "polycount";
        private static readonly String sC_s_s_SubObjectsElement = "subobjects";
        private static readonly String sC_s_s_ChildrenElement = "children";
        private static readonly String sC_s_s_LodHierarchyElement = "lod_hierarchy";
        private static readonly String sC_s_s_LodDrawableHierarchyElement = "lod_drawable_hierarchy";
        private static readonly String sC_s_s_SceneLinkHierarchyElement = "scene_link_hierarchy";
        private static readonly String sC_s_s_AttributesElement = "attributes";
        private static readonly String sC_s_s_PlatformAttributesElement = "platformAttributes";
        private static readonly String sC_s_s_PropertiesElement = "properties";
        private static readonly String sC_s_s_CustomAttributesElement = "custom_attrs";
        private static readonly String sC_s_s_UserPropertiesElement = "user_properties";
        private static readonly String sC_s_s_AnimAttributesElement = "anim_attrs";
        private static readonly String sC_s_s_ParamBlocksElement = "paramblocks";
        private static readonly String sC_s_s_ReferencedTexturesElement = "refdTextures";

        private static readonly String sC_s_s_NodeElement = "node";		
        #endregion // Attribute/Property/Parameter Attribute Names

        #region Hash Lookup Table
        private static Dictionary<String, int> HashLookupTable = new Dictionary<String, int>()
        {
            { sC_s_s_ObjectName, sC_s_s_ObjectName.GetHashCode() },
            { sC_s_s_ObjectGuid, sC_s_s_ObjectGuid.GetHashCode() },
            { sC_s_s_ObjectMaterial, sC_s_s_ObjectMaterial.GetHashCode() },
            { sC_s_s_ObjectClass, sC_s_s_ObjectClass.GetHashCode() },
            { sC_s_s_ObjectSuperclass, sC_s_s_ObjectSuperclass.GetHashCode() },
            { sC_s_s_ObjectRefName, sC_s_s_ObjectRefName.GetHashCode() },
            { sC_s_s_ObjectRefName2, sC_s_s_ObjectRefName2.GetHashCode() },
            { sC_s_s_ObjectRefFile, sC_s_s_ObjectRefFile.GetHashCode() },
            { sC_s_s_ObjectRefFile2, sC_s_s_ObjectRefFile2.GetHashCode() },
            { sC_s_s_ObjectRef, sC_s_s_ObjectRef.GetHashCode() },
            //{ sC_s_s_AttrName, sC_s_s_AttrName.GetHashCode() }, //Duplicate of sC_s_s_ObjectName
            { sC_s_s_AttrType, sC_s_s_AttrType.GetHashCode() },
            { sC_s_s_AttrRefnum, sC_s_s_AttrRefnum.GetHashCode() },
            //{ sC_s_s_AttrFilename, sC_s_s_AttrFilename.GetHashCode() },  //sC_s_s_ObjectRefName2
            { sC_s_s_AttrValue, sC_s_s_AttrValue.GetHashCode() },
            { sC_s_s_AttrFrame, sC_s_s_AttrFrame.GetHashCode() },
            { sC_s_s_AttrTypeArray, sC_s_s_AttrTypeArray.GetHashCode() },
            { sC_s_s_AttrTypeInt, sC_s_s_AttrTypeInt.GetHashCode() },
            { sC_s_s_AttrTypeFloat, sC_s_s_AttrTypeFloat.GetHashCode() },
            { sC_s_s_AttrTypeBool, sC_s_s_AttrTypeBool.GetHashCode() },
            { sC_s_s_AttrTypeRGB, sC_s_s_AttrTypeRGB.GetHashCode() },
            { sC_s_s_AttrTypeRGBA, sC_s_s_AttrTypeRGBA.GetHashCode() },
            { sC_s_s_AttrTypeVectorXYZ, sC_s_s_AttrTypeVectorXYZ.GetHashCode() },
            { sC_s_s_AttrTypeVectorXYZW, sC_s_s_AttrTypeVectorXYZW.GetHashCode() },
            { sC_s_s_AttrTypeObject, sC_s_s_AttrTypeObject.GetHashCode() },
            { sC_s_s_TransformElement, sC_s_s_TransformElement.GetHashCode() },
            { sC_s_s_BoundingBoxElement, sC_s_s_BoundingBoxElement.GetHashCode() },
            { sC_s_s_PolyCountElement, sC_s_s_PolyCountElement.GetHashCode() },
            { sC_s_s_SubObjectsElement, sC_s_s_SubObjectsElement.GetHashCode() },
            { sC_s_s_ChildrenElement, sC_s_s_ChildrenElement.GetHashCode() },
            { sC_s_s_LodHierarchyElement, sC_s_s_LodHierarchyElement.GetHashCode() },
            { sC_s_s_LodDrawableHierarchyElement, sC_s_s_LodDrawableHierarchyElement.GetHashCode() },
            { sC_s_s_SceneLinkHierarchyElement, sC_s_s_SceneLinkHierarchyElement.GetHashCode() },
            { sC_s_s_AttributesElement, sC_s_s_AttributesElement.GetHashCode() },
            { sC_s_s_PlatformAttributesElement, sC_s_s_PlatformAttributesElement.GetHashCode() },
            { sC_s_s_PropertiesElement, sC_s_s_PropertiesElement.GetHashCode() },
            { sC_s_s_CustomAttributesElement, sC_s_s_CustomAttributesElement.GetHashCode() },
            { sC_s_s_UserPropertiesElement, sC_s_s_UserPropertiesElement.GetHashCode() },
            { sC_s_s_AnimAttributesElement, sC_s_s_AnimAttributesElement.GetHashCode() },
            { sC_s_s_ParamBlocksElement, sC_s_s_ParamBlocksElement.GetHashCode() },
            { sC_s_s_ReferencedTexturesElement, sC_s_s_ReferencedTexturesElement.GetHashCode() },
            { sC_s_s_NodeElement, sC_s_s_NodeElement.GetHashCode() }
        };

        static Vector3f s_zeroVector = new Vector3f();
        static Vector3f s_minVector = new Vector3f(float.MinValue, float.MinValue, float.MinValue);
        static Vector3f s_maxVector = new Vector3f(float.MaxValue, float.MaxValue, float.MaxValue);
        static Quaternionf s_defaultQuaternion = new Quaternionf();

        delegate void ParseAttributeCallback(XPathNavigator navigator, string name, string valueStr, ref AttributeContainer container);
        static Dictionary<int, ParseAttributeCallback> ParseAttributeCallbacks = new Dictionary<int, ParseAttributeCallback>()
        {
            {HashLookupTable[sC_s_s_AttrTypeBool], ParseAttributeBool},
            {HashLookupTable[sC_s_s_AttrTypeInt], ParseAttributeInt},
            {HashLookupTable[sC_s_s_AttrTypeFloat], ParseAttributeFloat},
            {HashLookupTable[sC_s_s_AttrTypeObject], ParseAttributeGuid},
            {HashLookupTable[sC_s_s_AttrTypeRGB], ParseAttributeRGB},
            {HashLookupTable[sC_s_s_AttrTypeRGBA], ParseAttributeRGBA},
            {HashLookupTable[sC_s_s_AttrTypeVectorXYZ], ParseAttributeVector3},
            {HashLookupTable[sC_s_s_AttrTypeVectorXYZW], ParseAttributeVector4}
        };
        #endregion 

        #region Static Collections
        private static readonly String[] IGNORED_LIGHT_CLASSES = { "mr_Sky_Portal".ToLower(), 
                                                                     "Target_Light".ToLower(), 
                                                                     "Free_Light".ToLower() };
        private static readonly List<String> IGNORED_LIGHT_CLASSES_LIST = new List<String>(IGNORED_LIGHT_CLASSES);
        #endregion

        #region Proxy Object Constants
        private const String PROXY_FRAG = "_frag_";
        private const String PROXY_MILO = "_milo_";
        private const String PROXY_ANIM = "_anim";
        #endregion // Proxy Object Constants
        #endregion // Constants

        #region Enumerations
        /// <summary>
        /// Instance type (SLOD, LOD, HD).
        /// </summary>
        [Obsolete]
        public enum InstanceType
        {
            /// <summary>
            /// Instance type is invalid (not been set).
            /// </summary>
            None = -1,
            /// <summary>
            /// Instance is a Super LOD object.
            /// </summary>
            SLOD = 0,
            /// <summary>
            /// Instance is a LOD object.
            /// </summary>
            LOD = 1,
            /// <summary>
            /// Instance is a HD (high-detail) object.
            /// </summary>
            HD = 2
        }

        /// <summary>
        /// LOD Level.
        /// </summary>
        public enum LodLevel
        {
            UNKNOWN = -1,
            HD = 0,
            LOD = 1,
            SLOD1 = 2,
            SLOD2 = 3,
            SLOD3 = 4,
            ORPHANHD = 5,
            SLOD4 = 6,
        }
        #endregion // Enumerations

        #region Properties and Associated Member Data
        /// <summary>
        /// Object name.
        /// </summary>
        public String Name
        {
            get { return m_sName; }
        }
        private String m_sName;

        /// <summary>
        /// Object's LOD level.
        /// </summary>
        /// This replaces the obsolete InstanceType.
        /// 
        public LodLevel LODLevel
        {
            get;
            protected set;
        }

        /// <summary>
        /// Object GUID (unique identifier).
        /// </summary>
        public Guid Guid
        {
            get { return m_Guid; }
        }
        private Guid m_Guid;

        /// <summary>
        /// Assigned material GUID (unique identifier).
        /// </summary>
        public Guid Material
        {
            get { return m_MaterialGuid; }
        }
        private Guid m_MaterialGuid;

        /// <summary>
        /// Node world-space transformation matrix.
        /// </summary>
        public Matrix34f NodeTransform
        {
            get { return m_mNodeTransform; }
            set { m_mNodeTransform = value; }
        }
        private Matrix34f m_mNodeTransform;

        /// <summary>
        /// Node world-space rotation.
        /// </summary>
        public Quaternionf NodeRotation
        {
            get { return m_nodeRotation; }
        }
        private Quaternionf m_nodeRotation;

        /// <summary>
        /// Node world-space scale factors.
        /// </summary>
        public Vector3f NodeScale
        {
            get;
            set;
        }

        /// <summary>
        /// Object's world-space transformation matrix.
        /// </summary>
        public Matrix34f ObjectTransform
        {
            get { return m_mObjectTransform; }
        }
        private Matrix34f m_mObjectTransform;

        /// <summary>
        /// Object's world-space rotation.
        /// </summary>
        public Quaternionf ObjectRotation
        {
            get { return m_objectRotation; }
        }
        private Quaternionf m_objectRotation;

        /// <summary>
        /// Object's world-space scale factor.
        /// </summary>
        public Vector3f ObjectScale
        {
            get;
            set;
        }

        /// <summary>
        /// Object local bounding box.
        /// </summary>
        public BoundingBox3f LocalBoundingBox
        {
            get { return m_LocalBoundingBox; }
            set { m_LocalBoundingBox = value; }
        }
        private BoundingBox3f m_LocalBoundingBox;

        /// <summary>
        /// Object world bounding box.
        /// </summary>
        public BoundingBox3f WorldBoundingBox
        {
            get { return m_WorldBoundingBox; }
            set { m_WorldBoundingBox = value; }
        }
        private BoundingBox3f m_WorldBoundingBox;

        /// <summary>
        /// Parent object in Scene.
        /// </summary>
        public ObjectDef Parent
        {
            get { return m_Parent; }
            internal set { m_Parent = value; }
        }
        private ObjectDef m_Parent;

        /// <summary>
        /// Object DCC class.
        /// </summary>
        public String Class
        {
            get { return m_sClass; }
        }
        private String m_sClass;

        /// <summary>
        /// Object DCC superclass.
        /// </summary>
        public String Superclass
        {
            get { return m_sSuperclass; }
        }
        private String m_sSuperclass;

        /// <summary>
        /// Associated LOD hierarchy (parent and child meshes).
        /// </summary>
        public LODHierarchyDef LOD
        {
            get { return m_SceneLinks.ContainsKey(LinkChannel.LinkChannelSceneLod) ? m_SceneLinks[LinkChannel.LinkChannelSceneLod] : null; }
        }

        /// <summary>
        /// Associated Drawable LOD hierarchy (parent and child meshes).
        /// </summary>
        public LODHierarchyDef DrawableLOD
        {
            get { return m_SceneLinks.ContainsKey(LinkChannel.LinkChannelDrawableLod) ? m_SceneLinks[LinkChannel.LinkChannelDrawableLod] : null; }
        }

        /// <summary>
        /// SceneLink hierarchy containing all LOD types, shadow proxies, etc
        /// </summary>
        public Dictionary<LinkChannel, LODHierarchyDef> SceneLinks
        {
            get { return m_SceneLinks; }
        }
        private Dictionary<LinkChannel, LODHierarchyDef> m_SceneLinks;

        /// <summary>
        /// Object DCC reference name (e.g. XRef, InternalRef).
        /// </summary>
        public String RefName
        {
            get { return m_sRefName; }
        }
        private String m_sRefName;

        /// <summary>
        /// Object DCC reference.
        /// Needs extra resolve step after loading prop scenes in instance serialisation.
        /// </summary>
        public ObjectDef RefObject
        {
            get { return m_sRefObject; }
            set { m_sRefObject = value; }
        }
        private ObjectDef m_sRefObject;

        /// <summary>
        /// Object DCC reference filename (absolute filename, e.g. XRef).
        /// </summary>
        public String RefFile
        {
            get { return m_sRefFile; }
            internal set { m_sRefFile = value; } // Used by SceneCollection.
        }
        private String m_sRefFile;

        /// <summary>
        /// 
        /// </summary>
        public ObjectDef InternalRef
        {
            get;
            private set;
        }
        
        /// <summary>
        /// 
        /// </summary>
        public Guid InternalRefGuid
        {
            get { return m_InternalRefGuid; }
        }
        private Guid m_InternalRefGuid;

        /// <summary>
        /// Generic object reference GUIDs.
        /// </summary>
        public Guid[] GenericReferenceGuids
        {
            get { return m_GenericReferenceGuids; }
        }
        private Guid[] m_GenericReferenceGuids;

        /// <summary>
        /// Generic object references (resolved).
        /// </summary>
        public ObjectDef[] GenericReferences
        {
            get { return m_GenericReferences; }
        }
        private ObjectDef[] m_GenericReferences;

        /// <summary>
        /// Array of child objects.
        /// </summary>
        public ObjectDef[] Children
        {
            get { return (m_aChildren); }
            internal set { m_aChildren = value; }
        }
        private ObjectDef[] m_aChildren;

        /// <summary>
        /// Array of sub-objects.
        /// </summary>
        public ObjectDef[] SubObjects
        {
            get { return m_aSubObjects; }
        }
        private ObjectDef[] m_aSubObjects;

        /// <summary>
        /// Attribute container class (e.g. "Gta Object", "Gta Collision").
        /// </summary>
        public String AttributeClass
        {
            get { return m_sAttributeClass; }
        }
        private String m_sAttributeClass;

        /// <summary>
        /// Attribute container global identifier.
        /// </summary>
        public Guid AttributeGuid
        {
            get { return m_sAttributeGuid; }
        }
        private Guid m_sAttributeGuid;

        /// <summary>
        /// Attributes dictionary.
        /// </summary>
        public AttributeContainer Attributes
        {
            get { return m_dAttributes; }
        }
        private AttributeContainer m_dAttributes;

        /// <summary>
        /// Platform Attributes dictionary.
        /// </summary>
        public Dictionary<RSG.Platform.Platform, AttributeContainer> PlatformAttributes
        {
            get { return m_dPlatformAttributes; }
        }
        private Dictionary<RSG.Platform.Platform, AttributeContainer> m_dPlatformAttributes;

        /// <summary>
        /// User Properties dictionary.
        /// </summary>
        public AttributeContainer UserProperties
        {
            get { return m_dUserProperties; }
        }
        private AttributeContainer m_dUserProperties;

        /// <summary>
        /// Properties dictionary.
        /// </summary>
        public AttributeContainer Properties
        {
            get { return m_dProperties; }
        }
        private AttributeContainer m_dProperties;

        /// <summary>
        /// Properties dictionary.
        /// </summary>
        public AttributeContainer[] ParamBlocks
        {
            get { return m_aParamBlocks; }
        }
        private AttributeContainer[] m_aParamBlocks;

        /// <summary>
        /// dictionary of textures referenced by object.
        /// </summary>
        public List<sRefdTexture> RefdTextures
        {
            get { return m_aRefdTextures; }
        }
        private List<sRefdTexture> m_aRefdTextures;

        /// <summary>
        /// Custom attributes dictionary.
        /// </summary>
        public AttributeContainer CustomAttributes
        {
            get { return m_dCustomAttributes; }
        }
        private AttributeContainer m_dCustomAttributes;

        /// <summary>
        /// Number of polys in object (trimesh and collision support).
        /// </summary>
        public int PolyCount
        {
            get { return m_nPolyCount; }
        }
        private int m_nPolyCount;

        /// <summary>
        /// Associated skeleton object's GUID with this skin.
        /// </summary>
        public Guid SkeletonRootGuid
        {
            get;
            private set;
        }

        /// <summary>
        /// Associated skeleton object.
        /// </summary>
        public ObjectDef SkeletonRoot
        {
            get { return m_SkeletonRoot; }
            private set
            {
                m_SkeletonRoot = value;
                if(null!=value)
                    value.SkinObj = this;
            }
        }
        private ObjectDef m_SkeletonRoot;

        /// <summary>
        /// Associated skin object of this skeleton.
        /// </summary>
        public ObjectDef SkinObj
        {
            get { return m_SkinObj; }
            set { m_SkinObj = value; }
        }
        private ObjectDef m_SkinObj;

        /// <summary>
        /// Length of animation of all children (only for "hasAnim"'d objects)
        /// </summary>
        public int AnimLength
        {
            get { return m_nAnimLength; }
        }
        private int m_nAnimLength;

        /// <summary>
        /// Array of activity keyframes
        /// </summary>
        public KeyContainer KeyedProperties
        {
            get { return m_aKeyedProperties; }
        }
        private KeyContainer m_aKeyedProperties;

        /// <summary>
        /// My scene object
        /// </summary>
        public Scene MyScene
        {
            get { return m_oScene; }
        }
        private Scene m_oScene;
        #endregion // Properties and Associated Member Data
        
        #region Constructor(s)
        /// <summary>
        /// Constructor, using name.
        /// </summary>
        /// <param name="name"></param>
        /// This constructor is rarely used; added for Milo Limbo room 
        /// construction.
        public ObjectDef(String name, String classname, String attr_class, Scene scene)
        {
            this.m_sName = name;
            this.m_oScene = scene;
            this.m_sClass = classname;
            this.m_sSuperclass = String.Empty;
            this.m_sAttributeClass = attr_class;
            this.m_dAttributes = new AttributeContainer();
            this.m_dPlatformAttributes = new Dictionary<RSG.Platform.Platform, AttributeContainer>();
            this.m_dUserProperties = new AttributeContainer();
            this.m_dProperties = new AttributeContainer();
            this.m_aParamBlocks = null;
            this.m_dCustomAttributes = new AttributeContainer();
            this.m_SceneLinks = new Dictionary<LinkChannel, LODHierarchyDef>();
        }

        /// <summary>
        /// Constructor, from XPathNavigator.
        /// </summary>
        public ObjectDef(XPathNavigator navigator, Scene scene)
        {
            try
            {
                this.m_oScene = scene;
                this.m_sSuperclass = String.Empty;
                this.m_sAttributeClass = "_INVALID";
                this.m_dAttributes = new AttributeContainer();
                this.m_dPlatformAttributes = new Dictionary<RSG.Platform.Platform, AttributeContainer>();
                this.m_dUserProperties = new AttributeContainer();
                this.m_dProperties = new AttributeContainer();
                this.m_aParamBlocks = null;
                this.m_dCustomAttributes = new AttributeContainer();
                this.m_aKeyedProperties = new KeyContainer();
                this.m_SceneLinks = new Dictionary<LinkChannel, LODHierarchyDef>();

                Parse(navigator);
            }
            catch (Exception ex)
            {
                String message = String.Format("Invalid Object Definition: {0} {1} {2}.", 
                    this.Name, ex.Message, navigator.InnerXml);
                SceneParseException sceneEx = new SceneParseException(message, ex);
                Log.Log__Exception(sceneEx, "Error parsing SceneXml.");
                throw sceneEx;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="scene"></param>
        public ObjectDef(XmlTextReader reader, Scene scene)
        {
            this.m_oScene = scene;
            this.m_sSuperclass = String.Empty;
            this.m_sAttributeClass = "_INVALID";
            this.m_dAttributes = new AttributeContainer();
            this.m_dPlatformAttributes = new Dictionary<RSG.Platform.Platform, AttributeContainer>();
            this.m_dUserProperties = new AttributeContainer();
            this.m_dProperties = new AttributeContainer();
            this.m_aParamBlocks = null;
            this.m_dCustomAttributes = new AttributeContainer();
            this.m_aChildren = new ObjectDef[0];
            this.m_aKeyedProperties = new KeyContainer();
            this.m_GenericReferences = new ObjectDef[0];
            this.m_GenericReferenceGuids = new System.Guid[0];
            this.m_aParamBlocks = new AttributeContainer[0];
            this.m_aRefdTextures = new List<sRefdTexture>();
            this.m_aSubObjects = new ObjectDef[0];
            this.m_SceneLinks = new Dictionary<LinkChannel, LODHierarchyDef>();

            Parse(reader);
        }

        static ObjectDef()
        {
            //Verify that the hash table has unique values.
            Dictionary<int, String> values = new Dictionary<int, String>();
            foreach (KeyValuePair<String, int> hashPair in HashLookupTable)
            {
                if (values.ContainsKey(hashPair.Value))
                {
                    String message = String.Format("Hash table collision on value: {0}.  Strings \"{1}\" and \"{2}\" have the same hash value.",
                        hashPair.Value, hashPair.Key, values[hashPair.Value]);

                    SceneParseException sceneEx = new SceneParseException(message, null);
                    throw sceneEx;
                }
                else
                {
                    values.Add(hashPair.Value, hashPair.Key);
                }
            }
        }
        #endregion // Constructor(s)

        #region Object Overrides
        /// <summary>
        /// Determine whether two ObjectDef objects are equal.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(Object obj)
        {
            if (obj is ObjectDef)
            {
                ObjectDef def = (obj as ObjectDef);
                return (this.Guid.Equals(def.Guid));
            }

            return (false);
        }

        /// <summary>
        /// Return
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return (String.Format("ObjectDef: [{0}]{1}", this.Name, this.Guid));
        }
        #endregion // Object Overrides

        #region Controller Methods
        /// <summary>
        /// Determine instance type from LOD hierarchy.
        /// </summary>
        /// <returns></returns>
        [Obsolete]
        public InstanceType GetInstanceType()
        {
            if (this.HasLODChildren())
            {
                // We need to determine whether LOD or SLOD.
                // SLOD objects children (LOD) have children (HD).
                foreach (TargetObjectDef child in this.LOD.Children)
                {
                    if (null == child)
                    {
                        String message = String.Format("LOD Child of {0} is null.  Objects don't share same container?", this.Name);
                        Log.Log__ErrorCtx(this.Name, message);
                        Debug.Assert(null != child, message);
                    }
                    else
                    {
                        if (child.HasLODChildren())
                            return (InstanceType.SLOD);
                    }
                }

                return (InstanceType.LOD);
            }
            else if (this.HasLODParent())
            {
                // This is here to check that we don't flag up SLOD children
                // as HD if they don't have any children.
                InstanceType parentType = this.LOD.Parent.GetInstanceType();
                switch (parentType)
                {
                    case InstanceType.SLOD:
                        return (InstanceType.LOD);
                    case InstanceType.LOD:
                        return (InstanceType.HD);
                    default:
                        String message = String.Format("Strange LOD hierarchy for {0}.  Parent has invalid InstanceType.", this);
                        Log.Log__Error(message);
                        Debug.Assert(false, message);
                        break;
                }
            }

            return (InstanceType.HD);
        }

        /// <summary>
        /// Return scene root parent, null if no parent.
        /// </summary>
        /// <returns></returns>
        public ObjectDef GetRootParent()
        {
            if (!this.HasParent())
                return (null);

            ObjectDef objParent = this.Parent;
            while (objParent.HasParent())
                objParent = objParent.Parent;
            return (objParent);
        }


        /// <summary>
        /// Return root parent for export, null if no parent.
        /// </summary>
        /// <returns></returns>
        public ObjectDef GetExportRoot()
        {
            if (null != this.SkinObj)
                return this.SkinObj;
            
            if (!this.HasParent())
                return (null);
            
            ObjectDef objParent = this.Parent;
            ObjectDef prevParent = this;

            while ( null != objParent )
            {
                if (null != objParent.SkinObj)
                {
                    return objParent.SkinObj;
                }
                else if (   !objParent.IsMilo() &&
                            !objParent.IsMiloTri() &&
                            !objParent.IsContainer() &&
                            !objParent.IsIMAPGroupDefinition() &&
                            !objParent.IsMloPortal() &&
                            !objParent.IsMloRoom() &&
                            !objParent.IsPropGroup())
                {
                    prevParent = objParent;
                    objParent = objParent.Parent;
                }
                else
                    break;
            }
            return (prevParent);
        }

        /// <summary>
        /// Determine whether this object has a valid parent reference.
        /// </summary>
        public bool HasParent()
        {
            return (null != this.Parent);
        }

        /// <summary>
        /// Determine whether this object has a valid LOD parent reference.
        /// </summary>
        /// <returns></returns>
        public bool HasLODParent()
        {
            return ((null != this.LOD) && (null != this.LOD.Parent));
        }

        /// <summary>
        /// Determine whether this object has valid LOD child references.
        /// </summary>
        /// <returns></returns>
        public bool HasLODChildren()
        {
            return ((null != this.LOD) && (this.LOD.Children.Length > 0));
        }

        /// <summary>
        /// Determine whether this object has a valid Drawable LOD parent reference.
        /// </summary>
        /// <returns></returns>
        public bool HasDrawableLODParent()
        {
            return ((null != this.DrawableLOD) && (null != this.DrawableLOD.Parent));
        }

        /// <summary>
        /// Determine whether this object has valid Drawable LOD child references.
        /// </summary>
        /// <returns></returns>
        public bool HasDrawableLODChildren()
        {
            return ((null != this.DrawableLOD) && (this.DrawableLOD.Children.Length > 0));
        }

        /// <summary>
        /// Determine whether this object has a valid Drawable LOD parent reference.
        /// </summary>
        /// <returns></returns>
        public bool HasRenderSimParent()
        {
            return ((this.SceneLinks.ContainsKey(LinkChannel.LinkChannelRenderSim)) && (null != this.SceneLinks[LinkChannel.LinkChannelRenderSim].Parent));
        }

        /// <summary>
        /// Determine whether this object has valid Drawable LOD child references.
        /// </summary>
        /// <returns></returns>
        public bool HasRenderSimChildren()
        {
            return ((this.SceneLinks.ContainsKey(LinkChannel.LinkChannelRenderSim)) && (this.SceneLinks[LinkChannel.LinkChannelRenderSim].Children.Length > 0));
        }

        /// <summary>
        /// Determine whether this object has child collision data.
        /// </summary>
        /// <returns></returns>
        public bool HasCollision()
        {
            foreach (ObjectDef child in this.Children)
            {
                if (child.IsCollision())
                    return (true);

                if (child.HasCollision())
                    return (true);
            }

            return (false);
        }

        /// <summary>
        /// Determine whether this object has light children.
        /// </summary>
        /// <returns></returns>
        public bool HasLights()
        {
            foreach (ObjectDef child in this.Children)
            {
                if (child.IsLightInstance() || child.Is2dfxLightEffect())
                    return (true);
                
                if (child.HasLights())
                    return (true);
            }
            if (null != this.SkeletonRoot)
                return this.SkeletonRoot.HasLights();
            return (false);
        }

        /// <summary>
        /// Determine whether this object has 2dfx children.
        /// </summary>
        /// <returns></returns>
        public bool Has2dfx()
        {
            foreach (ObjectDef child in this.Children)
            {
                if (child.Is2dfx())
                    return (true);

                if (child.Has2dfx())
                    return (true);
            }
            if (null != this.SkeletonRoot)
                return this.SkeletonRoot.Has2dfx();
            return (false);
        }

        /// <summary>
        /// Determine whether this object has RsVisbilityBound override.
        /// </summary>
        /// <returns></returns>
        /// Note: we only go one level deep here.
        /// 
        public bool HasVisibilityBoundOverride()
        {
            foreach (ObjectDef child in this.Children)
            {
                if (child.IsVisibilityBound())
                    return (true);
            }
            return (false);
        }

        /// <summary>
        /// Determine whether this object has platform specific attributes.
        /// </summary>
        /// <returns>true if a single platform attribute exists, false otherwise</returns>
        public bool HasPlatformAttributes()
        {
            return m_dPlatformAttributes.Count > 0;
        }

        /// <summary>
        /// Determine whether this object has platform attributes for a specific platform.
        /// </summary>
        /// <returns>true if a single platform attribute exists, false otherwise</returns>
        public bool HasPlatformSpecificAttributes(RSG.Platform.Platform platform)
        {
            if (!HasPlatformAttributes())
                return false;

            if (m_dPlatformAttributes.ContainsKey(platform))
                return true;

            return false;
        }

        /// <summary>
        /// Determine whether this object is active for the given platform.
        /// </summary>
        /// <returns>true if object is active, false otherwise</returns>
        public bool IsActiveForPlatform(RSG.Platform.Platform platform)
        {
            // If we don't have any platform attributes then assume it's active.
            if (!HasPlatformAttributes())
                return true;

            return (this.GetPlatformAttribute(platform, AttrNames.RSACTIVE_ACTIVE, AttrDefaults.RSACTIVE_ACTIVE));
        }

        /// <summary>
        /// Takes the platform attributes and merges them together with the standard attributes
        /// to make 1 set of attributes for that platform.
        /// </summary>
        /// <returns>New attribute container with the combined attributes</returns>
        public AttributeContainer CombinePlatformAttributes(RSG.Platform.Platform platform)
        {
            if (!HasPlatformAttributes())
                return m_dAttributes;

            if (!m_dPlatformAttributes.ContainsKey(platform))
                return m_dAttributes;

            AttributeContainer platformContainer = m_dPlatformAttributes[platform];
            AttributeContainer combinedContainer = new AttributeContainer();

            foreach (string key in m_dAttributes.Keys)
            {
                if (platformContainer.ContainsKey(key))
                    combinedContainer[key] = platformContainer[key];
                else
                    combinedContainer[key] = m_dAttributes[key];
            }

            return combinedContainer;
        }

        /// <summary>
        /// Return the map name for an ObjectDef object.
        /// </summary>
        /// <returns></returns>
        /// This takes into account the object's class etc (e.g. XRef, 
        /// InternalRef, 2dfx etc).
        public String GetObjectName()
        {
            String name = ("Scene_Root");
            if (IsXRef() || IsInternalRef() || IsRefObject() || IsRefInternalObject())
            {
                if (String.IsNullOrEmpty(RefName))
                {
                    String message = String.Format("Invalid RsRef object!  Name '{0}' has no objectName property.", this.Name);
                    Log.Log__ErrorCtx(this.Name, message);
                }
                else
                {
                    if (RefName.EndsWith(PROXY_MILO))
                        name = RefName.Replace(PROXY_MILO, String.Empty);
                    else if (RefName.EndsWith(PROXY_FRAG))
                        name = RefName.Replace(PROXY_FRAG, String.Empty);
                    else if (RefName.EndsWith(PROXY_ANIM))
                        name = RefName.Replace(PROXY_ANIM, String.Empty);
                    else
                        name = RefName;
                }
            }
            else if (IsAnimProxy() && Name.EndsWith(PROXY_ANIM))
                name = (Name.Replace(PROXY_ANIM, String.Empty));
            else if (IsMiloTri() && Name.EndsWith(PROXY_MILO))
                name = (Name.Replace(PROXY_MILO, String.Empty));
            else if (IsFragment() && Name.EndsWith(PROXY_FRAG))
                name = (Name.Replace(PROXY_FRAG, String.Empty));
            else if (Is2dfx())
            {
                // AJM: Added this clause for 2dfxScript for GTA5 #9455 to support child Gta Scripts linked together 
                // traversing up the hierarchy until it finds a non-2dfcScript object which it's attached to.
                if (this.HasParent() && Is2dfxScript())
                {
                    ObjectDef parent = this.Parent;
                    while ((null != parent) && (parent.HasParent()) && parent.Parent.Is2dfxScript())
                    {
                        parent = parent.Parent;
                    }
                    name = parent.GetObjectName();
                }
                // Find 2dfx parent object; walk parent hierarchy until we hit a frag or anim.
                else if (this.HasParent() && this.Parent.IsObject() && !this.Parent.IsHelper())
                {
                    ObjectDef parent = this.Parent;
                    while ((null != parent) && (parent.HasParent()) &&
                        (parent.Parent.IsObject()) && (!parent.Parent.IsHelper()))
                    {
                        parent = parent.Parent;
                    }
                    name = parent.GetObjectName();
                }
                else if (this.HasParent())
                {
                    ObjectDef parent = this.Parent;
                    while ((null != parent) && (parent.HasParent()))
                    {
                        parent = parent.Parent;
                    }
                    if (null != parent.SkinObj)
                        name = parent.SkinObj.GetObjectName();
                }
            }
            else
            {
                name = this.Name;
            }
            return (name.Replace(" ", "_"));
        }


        /// <summary>
        /// Conglomerate all the attributes into a list that can be easily iterate on
        /// in environments such as MaxScript.
        /// </summary>
        /// <returns>Returns all attributes in an array format.</returns>
        [Obsolete]
        public AttributeListItem[] ConvertAttributesToArray()
        {
            int arrayIndex = 0;
            AttributeListItem[] attributeList = new AttributeListItem[Attributes.Keys.Count];
            foreach (string key in Attributes.Keys)
            {
                AttributeListItem newObject = new AttributeListItem();
                newObject.AttributeName = key;
                newObject.AttributeObject = Attributes[key];
                attributeList[arrayIndex++] = newObject;
            }

            return attributeList;
        }

        /// <summary>
        /// Return the LOD distance for this object (or default).
        /// This method is used throughout the toolchain but the metadata serialisers should never use this.  
        /// Instead they should go via RSG.SceneXml.MapExport.Util.ObjectLODDistanceIntegrator to ensure that LOD overrides are taken into account (mess).
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public float GetLODDistance()
        {
            if (!IsObject() && !IsXRef() && !IsRefObject() && !IsInternalRef() && !IsRefInternalObject() && !IsAnimProxy())
                return (-1.0f);

            if (IsAnimProxy())
                return (this.GetAttribute(AttrNames.STATEDANIM_LOD_DISTANCE, AttrDefaults.STATEDANIM_LOD_DISTANCE));

            // If this object is within a Drawable LOD hierarchy: we return the
            // lowest detail meshes LOD distance (as thats what should be exported).
            if (this.HasDrawableLODParent())
                return (this.DrawableLOD.Parent.GetLODDistance());

            // Default to our "LOD distance" attribute.
            return (this.GetAttribute(AttrNames.OBJ_LOD_DISTANCE, AttrDefaults.OBJ_LOD_DISTANCE));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objects"></param>
        /// <returns></returns>
        public String[] GetPtfxEffectTags()
        {
            List<String> effectTags = new List<String>();
            if (Is2dfxParticleEffect())
            {
                String effectTag = GetAttribute(AttrNames.TWODFX_PARTICLE_NAME,
                    AttrDefaults.TWODFX_PARTICLE_NAME);
                if (!effectTags.Contains(effectTag))
                    effectTags.Add(effectTag);
            }
            foreach (ObjectDef obj in Children)
            {
                String[] childTags = obj.GetPtfxEffectTags();
                effectTags.AddRange(childTags);
            }

            return (effectTags.ToArray());
        }

        /// <summary>
        /// Return the statedAnim for this object (or default).
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public string GetStatedAnimName()
        {
            return this.GetAttribute(AttrNames.OBJ_GROUP_NAME, AttrDefaults.OBJ_GROUP_NAME);
        }

        /// <summary>
        /// Read an attribute value, if not present return default value.
        /// </summary>
        /// <param name="name">String attribute name</param>
        /// <param name="defValue">Default value</param>
        /// <returns>Attribute value or default value</returns>
        public T GetAttribute<T>(String name, T defValue)
        {
            string nameToLower = name.ToLower();

            object returnValue;
            if (Attributes.TryGetValue(nameToLower, out returnValue))
                return (T)returnValue;

            return defValue;
        }

        /// <summary>
        /// Read an attribute value as string for maxscrip compatibility
        /// </summary>
        /// <param name="name">String attribute name</param>
        /// <param name="defValue">Default value</param>
        /// <returns>Attribute value as string</returns>
        public object GetAttribute(String name, object defValue)
        {
            string nameToLower = name.ToLower();

            object returnValue;
            if (Attributes.TryGetValue(nameToLower, out returnValue))
                return returnValue;

            return defValue;
        }

        public object GetAttribute(String name)
        {
            return GetAttribute(name, null);
        }

        public T GetPlatformAttribute<T>(Platform.Platform platform, String name, T defValue)
        {
            if (!PlatformAttributes.ContainsKey(platform))
                return defValue;

            AttributeContainer container = PlatformAttributes[platform];
            string nameToLower = name.ToLower();

            object returnValue;
            if (container.TryGetValue(nameToLower, out returnValue))
                return (T)returnValue;

            return defValue;
        }

        public object GetPlatformAttribute(RSG.Platform.Platform platform, String name, object defValue)
        {
            if (!this.PlatformAttributes.ContainsKey(platform))
                return defValue;

            AttributeContainer container = PlatformAttributes[platform];
            string nameToLower = name.ToLower();

            object returnValue;
            if (container.TryGetValue(nameToLower, out returnValue))
                return returnValue;

            return defValue;
        }

        public bool HasPlatformAttribute(RSG.Platform.Platform platform, String name)
        {
            if (!this.PlatformAttributes.ContainsKey(platform))
                return false;

            AttributeContainer container = this.PlatformAttributes[platform];

            return container.ContainsKey(name.ToLower());
        }

        /// <summary>
        /// Override an attribute value (for use with Scene Overrides)
        /// </summary>
        public void SetAttribute(String name, object value)
        {
            string nameToLower = name.ToLower();

            if (this.Attributes.ContainsKey(nameToLower))
            {
                this.Attributes[nameToLower] = value;
            }
            else
            {
                this.Attributes.Add(nameToLower, value);
            }
        }

        /// <summary>
        /// Read a parameter value, if not present return default value.
        /// </summary>
        /// <param name="name">String attribute name</param>
        /// <param name="defValue">Default value</param>
        /// <returns>Attribute value or default value</returns>
        public T GetParameter<T>(String name, T defValue)
        {
            string nameToLower = name.ToLower();

            foreach (AttributeContainer container in ParamBlocks)
            {
                object val;
                if (container.TryGetValue(nameToLower, out val))
                {
                    if (typeof(T) == typeof(bool) && val is int)
                        val = 1 != (int)val;

                    return ((T)val);
                }
            }
            return (defValue);
        }

        /// <summary>
        /// Finds a particular property based on name.
        /// Non-templated version since MaxScript doesn't understand something as futuristic
        /// and advanced as that.
        /// </summary>
        /// <param name="propertyName">Property Name to find</param>
        /// <returns>Property's value if found.</returns>
        public object GetUserProperty(String propertyName)
        {
            if (!this.UserProperties.ContainsKey(propertyName.ToLower()))
                return null;

            return ((object)this.UserProperties[propertyName.ToLower()]);
        }

        /// <summary>
        /// Read a property value, if not present return default value.
        /// </summary>
        /// <param name="name">String attribute name</param>
        /// <param name="defValue">Default value</param>
        /// <returns>Attribute value or default value</returns>
        public T GetUserProperty<T>(String name, T defValue)
        {
            string nameToLower = name.ToLower();

            if (!this.UserProperties.ContainsKey(nameToLower))
                return (defValue);

            return ((T)this.UserProperties[nameToLower]);
        }

        /// <summary>
        /// Finds a particular property based on name.
        /// Non-templated version since MaxScript doesn't understand something as futuristic
        /// and advanced as that.
        /// </summary>
        /// <param name="propertyName">Property Name to find</param>
        /// <returns>Property's value if found.</returns>
        public object GetProperty(string propertyName)
        {
            string lowerProperty = propertyName.ToLower();

            object propertyValue;
            Properties.TryGetValue(lowerProperty, out propertyValue);
            return propertyValue; // object is ref type, TryGetValue returns default(T) if not found, so it'll return null if not found.
        }

        /// <summary>
        /// Read a property value, if not present return default value.
        /// </summary>
        /// <param name="name">String attribute name</param>
        /// <param name="defValue">Default value</param>
        /// <returns>Attribute value or default value</returns>
        public T GetProperty<T>(String name, T defValue)
        {
            string nameToLower = name.ToLower();

            if (!this.Properties.ContainsKey(nameToLower))
                return (defValue);

            object val = this.Properties[nameToLower];
            if(typeof(T)==typeof(bool) && val.GetType()==typeof(int))
                val = 1 != (int)val;

            return ((T)val);
        }

        private void GetLocalBoundingBoxRec(ObjectDef obj, ObjectDef parent, ref BoundingBox3f bbox)
        {
            if (!obj.GetAttribute(AttrNames.OBJ_IGNORE_IN_BOUND_CALC, AttrDefaults.OBJ_IGNORE_IN_BOUND_CALC))
            {
                BoundingBox3f lbbox = new BoundingBox3f();
                if (null != obj.LocalBoundingBox)
                    lbbox = new BoundingBox3f(obj.LocalBoundingBox);

                if (null != parent)
                {
                    Vector3f offset = obj.NodeTransform.Translation - parent.NodeTransform.Translation;
                    lbbox.Min += offset;
                    lbbox.Max += offset;
                }
                bbox.Expand(lbbox);
            }

            List<ObjectDef> children = new List<ObjectDef>();
            children.AddRange(obj.Children);
            if (obj.SceneLinks.ContainsKey(LinkChannel.LinkChannelShadowMesh) && null != obj.SceneLinks[LinkChannel.LinkChannelShadowMesh].Parent)
            {
                children.Add(obj.SceneLinks[LinkChannel.LinkChannelShadowMesh].Parent.ObjectDef);
            }

            foreach (ObjectDef o in children)
            {
                if (!o.IsObject())
                    continue;

                if (null != parent)
                    GetLocalBoundingBoxRec(o, parent, ref bbox);
                else
                    GetLocalBoundingBoxRec(o, obj, ref bbox);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public BoundingBox3f GetLocalBoundingBox()
        {
            BoundingBox3f bbox = new BoundingBox3f();
            if (this.HasVisibilityBoundOverride())
            {
                foreach (ObjectDef child in this.Children)
                {
                    if (!child.IsVisibilityBound())
                        continue;
                    bbox = child.LocalBoundingBox;
                }
            }
            else
            {
                GetLocalBoundingBoxRec(this, null, ref bbox);
            }

            // JWR - We should really throw here if bbox.IsEmpty is true
            // Going forward we should have GetLocalBoundingBox() that throws and TryGetLocalBoundingBox() which returns null in that situation
            // However, this is seen as too risky a change to make at this stage of proceedings.

            return (bbox);
        }

        #region Generic Object Type Query Methods
        /// <summary>
        /// Determine whether this object is of a specific Attribute Class.
        /// </summary>
        /// <param name="classname"></param>
        /// <returns></returns>
        public bool IsAttributeClass(String classname)
        {
            return String.Equals(this.AttributeClass, classname, StringComparison.CurrentCultureIgnoreCase);
        }

        /// <summary>
        /// Determine whether this object is of a specific DCC / 3dsmax Superclass.
        /// </summary>
        /// <param name="classname"></param>
        /// <returns></returns>
        public bool IsMaxSuperclass(String classname)
        {
            return String.Equals(this.Superclass, classname);
        }

        /// <summary>
        /// Determine whether this object is of a specific DCC / 3dsmax Class.
        /// </summary>
        /// <param name="classname"></param>
        /// <returns></returns>
        public bool IsMaxClass(String classname)
        {
            return String.Equals(this.Class, classname);
        }

        /// <summary>
        /// Determine whether this object is of a specific DCC / 3dsmax Class which we are ignoring.
        /// </summary>
        /// <param name="classname"></param>
        /// <returns></returns>
        public bool IsIgnoredMaxLightClass()
        {          
            return IGNORED_LIGHT_CLASSES_LIST.Contains(this.Class);
        }
        #endregion // Generic Object Type Query Methods

        #region Specific DCC Class Query Methods
        /// <summary>
        /// Determine whether this object is an XRef.
        /// </summary>
        public bool IsXRef()
        {
            return (IsMaxClass(ClassConsts.MAX_CLASS_XREF));
        }

        /// <summary>
        /// Determine whether this object is an InternalRef.
        /// </summary>
        public bool IsInternalRef()
        {
            return (IsMaxClass(ClassConsts.MAX_CLASS_INTERNALREF));
        }

        /// <summary>
        /// Determine whether this object is a Helper subclass.
        /// </summary>
        /// <returns></returns>
        public bool IsHelper()
        {
            return (IsMaxSuperclass(ClassConsts.MAX_CLASS_HELPER));
        }

        /// <summary>
        /// Determine whether this object is a Dummy helper.
        /// </summary>
        /// <returns></returns>
        public bool IsDummy()
        {
            return (IsMaxClass(ClassConsts.MAX_CLASS_DUMMY));
        }

        /// <summary>
        /// Determine whether this object is a Dummy helper.
        /// </summary>
        /// <returns></returns>
        public bool IsBone()
        {
            return (IsMaxClass(ClassConsts.MAX_CLASS_BONE));
        }

        /// <summary>
        /// Determine whether this object is a RS PropGroup Dummy helper.
        /// </summary>
        /// <returns></returns>
        public bool IsPropGroup()
        {
            return (IsDummy() && IsAttributeClass(AttrClass.PROPGROUP));
        }

        /// <summary>
        /// Determine whether this object is a Container helper.
        /// </summary>
        /// <returns></returns>
        public bool IsContainer()
        {
            return (IsMaxClass(ClassConsts.MAX_CLASS_CONTAINER));
        }

        /// <summary>
        /// Determine whether this object is an Editable Spline geometry object.
        /// </summary>
        /// <returns></returns>
        public bool IsEditableSpline()
        {
            return (IsMaxClass(ClassConsts.MAX_CLASS_EDITABLE_SPLINE));
        }

        /// <summary>
        /// Determine whether this object is a 3dsmax Text object.
        /// </summary>
        /// <returns></returns>
        public bool IsText()
        {
            return (IsMaxClass(ClassConsts.MAX_CLASS_TEXT));
        }

        /// <summary>
        /// Determine whether this object is a Line geometry object.
        /// </summary>
        /// <returns></returns>
        public bool IsLine()
        {
            return (IsMaxClass(ClassConsts.MAX_CLASS_LINE));
        }
        /// <summary>
        /// Determine whether this object is an instanced rage light.
        /// </summary>
        /// <returns></returns>
        public bool IsLightInstance()
        {
            return (IsMaxClass(ClassConsts.MAX_LIGHT_INSTANCE));
        }
        /// <summary>
        /// Determine whether this object is an instanced rage light.
        /// </summary>
        /// <returns></returns>
        public bool IsRageLight()
        {
            return (IsMaxClass(ClassConsts.MAX_RAGELIGHT));
        }
        /// <summary>
        /// Determine whether this object is an instanced spawn point.
        /// </summary>
        /// <returns></returns>
        public bool IsSpawnPointInstance()
        {
            return (IsMaxClass(ClassConsts.MAX_SPAWN_POINT_INSTANCE));
        }
        #endregion // Specific DCC Class Query Methods

        #region Specific Object Attribute Class Query Methods
        /// <summary>
        /// Determine whether this object is a Gta Block.
        /// </summary>
        public bool IsBlock()
        {
            return (IsAttributeClass(AttrClass.BLOCK));
        }

        /// <summary>
        /// Determine whether this object is a Gta CarGen.
        /// </summary>
        public bool IsCarGen()
        {
            return (IsAttributeClass(AttrClass.CARGEN));
        }

        /// <summary>
        /// Determine whether this object is a Gta Collision.
        /// </summary>
        public bool IsCollision()
        {
            return (IsAttributeClass(AttrClass.COLLISION));
        }

        /// <summary>
        /// Determine whether this object has the Gta Object attribute class and can thus be a valid entity.
        /// </summary>
        public bool IsEntity()
        {
            return (IsAttributeClass(AttrClass.OBJECT));
        }

        /// <summary>
        /// Dertermine whether this object is a Gta GarageArea.
        /// </summary>
        public bool IsGarageArea()
        {
            return (IsAttributeClass(AttrClass.GARAGEAREA));
        }

        /// <summary>
        /// Determine whether this object is a Gta Group.
        /// </summary>
        /// <returns></returns>
        public bool IsGroup()
        {
            return (IsAttributeClass(AttrClass.GROUP));
        }

        /// <summary>
        /// Determine whether this object is a MAX Group.
        /// </summary>
        /// <returns></returns>
        public bool IsMAXGroup()
        {
            return m_sClass == ClassConsts.MAX_GROUP_HEAD;
        }

        /// <summary>
        /// Determine whether this object is a Gta LODMofifier.
        /// </summary>
        public bool IsLODModifier()
        {
            return (IsAttributeClass(AttrClass.LODMODIFIER));
        }

        /// <summary>
        /// Determine whether this object is a Gta Milo.
        /// </summary>
        public bool IsMilo()
        {
            return (IsAttributeClass(AttrClass.MILO));
        }

        public bool IsObjectGroup()
        {
            return IsMaxClass(ClassConsts.MAX_OBJECT_GROUP);
        }

        public bool IsInteriorGroup()
        {
            return IsMaxClass(ClassConsts.MAX_INTERIOR_GROUP);
        }

        /// <summary>
        /// Determine whether this object is a Gta MiloTri.
        /// </summary>
        public bool IsMiloTri()
        {
            String objectName = String.Empty;
            if (null != this.ParamBlocks)
                objectName = this.GetParameter("ObjectName", String.Empty);
            bool isMiloRsRef = objectName.Contains("_milo_");

            return (isMiloRsRef || IsAttributeClass(AttrClass.MILOTRI));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool IsMiloAssoc()
        {
            return (HasParent() && this.Parent.IsMiloTri());
        }

        /// <summary>
        /// Determine whether this object is a Gta MloPortal.
        /// </summary>
        public bool IsMloPortal()
        {
            return (IsAttributeClass(AttrClass.MLOPORTAL));
        }

        /// <summary>
        /// Determine whether this object is a Gta MloRoom.
        /// </summary>
        public bool IsMloRoom()
        {
            return (IsAttributeClass(AttrClass.MLOROOM));
        }

        /// <summary>
        /// Determine whether this object is a Gta Object.
        /// </summary>
        public bool IsObject()
        {
            // IsMiloTro check in for MILO RsRefObjects.
            return (IsAttributeClass(AttrClass.OBJECT) && !IsMiloTri() && !IsOcclusion() && !IsBone());
        }

        /// <summary>
        /// Determine whether this object is an RsRefObject
        /// </summary>
        /// <returns></returns>
        public bool IsRefObject()
        {
            return (IsMaxClass(ClassConsts.MAX_CLASS_RSREF));
        }

        /// <summary>
        /// Determine whether this object is an RsRefObject and has been resolved.
        /// </summary>
        /// <returns></returns>
        public bool IsResolvedRefObject()
        {
            return (this.IsRefObject() && (null != this.RefObject));
        }

        /// <summary>
        /// Dertermine whether this object is an RsInternalRef object.
        /// </summary>
        /// <returns></returns>
        public bool IsRefInternalObject()
        {
            return (IsMaxClass(ClassConsts.MAX_CLASS_RSINTERNALREF));
        }

        /// <summary>
        /// Determine whether this object is an RsContainerLODRefObject
        /// </summary>
        /// <returns></returns>
        public bool IsContainerLODRefObject()
        {
            return (IsMaxClass(ClassConsts.MAX_CLASS_RSCONTAINERLODREF));
        }


        /// <summary>
        /// Determine whether this object is a Gta Pickup.
        /// </summary>
        public bool IsPickup()
        {
            return (IsAttributeClass(AttrClass.PICKUP));
        }

        /// <summary>
        /// Determine whether this object is a Gta TimeCycle.
        /// </summary>
        public bool IsTimeCycleBox()
        {
            return (IsAttributeClass(AttrClass.TIMECYCLE_BOX));
        }

        /// <summary>
        /// Determine whether this object is a Gta TC Sphere.
        /// </summary>
        public bool IsTimeCycleSphere()
        {
            return (IsAttributeClass(AttrClass.TIMECYCLE_SPHERE));
        }

        /// <summary>
        /// Determine whether this object is a Gta Zone.
        /// </summary>
        public bool IsZone()
        {
            return (IsAttributeClass(AttrClass.ZONE));
        }

        /// <summary>
        /// Determine whether this object is a 2dfx.
        /// </summary>
        /// This method uses some .Net reflection magic to invoke all public
        /// methods of this class which begin with "Is2dfx" (excluding
        /// ourselves).
        public bool Is2dfx()
        {
            MethodInfo[] methods = this.GetType().GetMethods();
            foreach (MethodInfo methodInfo in methods)
            {
                // Skip public methods.
                if (!methodInfo.IsPublic)
                    continue;

                Match m = Regex.Match(methodInfo.Name, "Is2dfx[A-Za-z]+");
                if (!m.Success)
                    continue;

                // Invoke our method.
                Object result = (methodInfo.Invoke(this, null));
                if ((typeof(Boolean) == result.GetType()) && (true == (Boolean)result))
                    return (true);
            }

            return (false);
        }

        /// <summary>
        /// Determine whether this object is a Lod Modifier.
        /// </summary>
        /// <returns></returns>
        public bool IsLodModifier()
        {
            return (IsAttributeClass(AttrClass.LODMODIFIER));
        }

        /// <summary>
        /// Determine whether this object is a Slow Zone.
        /// </summary>
        /// <returns></returns>
        public bool IsSlowZone()
        {
            return (IsMaxClass(ClassConsts.MAX_CLASS_SLOWZONE) || IsAttributeClass(AttrClass.SLOWZONE));
        }

        /// <summary>
        /// Determine whether this object is a Vehicle Node.
        /// </summary>
        /// <returns></returns>
        /// Note: not sure why this also returns true for NetRestart objects.
        public bool IsVehicleNode()
        {
            return (IsAttributeClass(AttrClass.VEHICLE_NODE) ||
                IsAttributeClass(AttrClass.NETRESTART));
        }

        /// <summary>
        /// Determine whether this object is a Vehicle Link.
        /// </summary>
        /// <returns></returns>
        public bool IsVehicleLink()
        {
            return (IsAttributeClass(AttrClass.VEHICLE_LINK));
        }

        /// <summary>
        /// Determine whether this object is a Patrol Node.
        /// </summary>
        /// <returns></returns>
        public bool IsPatrolNode()
        {
            return (IsAttributeClass(AttrClass.PATROL_NODE));
        }

        /// <summary>
        /// Determine whether this object is a Patrol Link.
        /// </summary>
        /// <returns></returns>
        public bool IsPatrolLink()
        {
            return (IsAttributeClass(AttrClass.PATROL_LINK));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool IsVisibilityBound()
        {
            return (IsMaxClass(ClassConsts.MAX_VISIBILITY_BOUND));
        }

        /// <summary>
        /// Determine whether this object is an occlusion object.
        /// </summary>
        /// <returns></returns>
        public bool IsOcclusion()
        {
            return (IsOcclusionBox() || IsOcclusionMesh());
        }

        /// <summary>
        /// Determine whether this object is an occlusion mesh.
        /// </summary>
        /// <returns></returns>
        public bool IsOcclusionMesh()
        {
            return (IsAttributeClass(AttrClass.OBJECT) && GetAttribute(AttrNames.OBJ_IS_OCCLUSION, AttrDefaults.OBJ_IS_OCCLUSION));
        }

        /// <summary>
        /// Determine whether this object is an occlusion box.
        /// </summary>
        /// <returns></returns>
        public bool IsOcclusionBox()
        {
            return (IsMaxClass(ClassConsts.MAX_OCCLUSION_BOX));
        }

        #region 2dfx Object Attribute Class Query Methods
        /// <summary>
        /// Determine whether this object is a 2dfx ladder.
        /// </summary>
        /// <returns></returns>
        public bool Is2dfxLadderFx()
        {
            return (IsAttributeClass(AttrClass.TWODFX_LADDER));
        }

        /// <summary>
        /// Determine whether this object is a 2dfx ladder.
        /// </summary>
        /// <returns></returns>
        public bool Is2dfxRSLadder()
        {
            return (IsAttributeClass(AttrClass.TWODFX_RSLADDER));
        }

        /// <summary>
        /// Determine whether this object is a 2dfx audio emitter.
        /// </summary>
        public bool Is2dfxAudioEmitter()
        {
            return (IsAttributeClass(AttrClass.TWODFX_AUDIO_EMITTER));
        }

        /// <summary>
        /// Determine whether this object is a 2dfx buoyancy.
        /// </summary>
        public bool Is2dfxBuoyancy()
        {
            return (IsAttributeClass(AttrClass.TWODFX_BUOYANCY));
        }

        /// <summary>
        /// Determine whether this object is a 2dfx decal.
        /// </summary>
        public bool Is2dfxDecal()
        {
            return (IsAttributeClass(AttrClass.TWODFX_DECAL));
        }

        /// <summary>
        /// Determine whether this object is a 2dfx explosion effect.
        /// </summary>
        public bool Is2dfxExplosionEffect()
        {
            return (IsAttributeClass(AttrClass.TWODFX_EXPLOSION_EFFECT));
        }

        /// <summary>
        /// Determine whether this object is a 2dfx light effect.
        /// </summary>
        /// <returns></returns>
        public bool Is2dfxLightEffect()
        {
            return ((IsAttributeClass(AttrClass.TWODFX_LIGHT) ||
                IsAttributeClass(AttrClass.TWODFX_LIGHT_PHOTO))
                && (!IsIgnoredMaxLightClass()));
        }

        /// <summary>
        /// Determine whether this object is a 2dfx light shaft.
        /// </summary>
        public bool Is2dfxLightShaft()
        {
            return (IsAttributeClass(AttrClass.TWODFX_LIGHT_SHAFT));
        }

        /// <summary>
        /// Determine whether this object is a 2dfx proc object.
        /// </summary>
        public bool Is2dfxProcObject()
        {
            return (IsAttributeClass(AttrClass.TWODFX_PROC_OBJECT));
        }

        /// <summary>
        /// Determine whether this object is a 2dfx script.
        /// </summary>
        public bool Is2dfxScript()
        {
            return (IsAttributeClass(AttrClass.TWODFX_SCRIPT));
        }

        /// <summary>
        /// Determine whether this object is a 2dfx particle effect.
        /// </summary>
        public bool Is2dfxParticleEffect()
        {
            return (IsAttributeClass(AttrClass.TWODFX_PARTICLE_EFFECT));
        }

        /// <summary>
        /// Determine whether this object is a 2dfx pedqueue.
        /// </summary>
        public bool Is2dfxPedQueue()
        {
            return (IsAttributeClass(AttrClass.TWODFX_PED_QUEUE));
        }

        /// <summary>
        /// Determine whether this object is a 2dfx scrollbar.
        /// </summary>
        public bool Is2dfxScrollbar()
        {
            return (IsAttributeClass(AttrClass.TWODFX_SCROLLBARS));
        }

        /// <summary>
        /// Determine whether this object is a 2dfx spawn point.
        /// </summary>
        public bool Is2dfxSpawnPoint()
        {
            return (IsAttributeClass(AttrClass.TWODFX_SPAWN_POINT));
        }

        /// <summary>
        /// Determine whether this object is a 2dfx swayable effect.
        /// </summary>
        public bool Is2dfxSwayableEffect()
        {
            return (IsAttributeClass(AttrClass.TWODFX_SWAYABLE_EFFECT));
        }

        /// <summary>
        /// Determine whether this object is a 2dfx walk dont walk.
        /// </summary>
        public bool Is2dfxWalkDontWalk()
        {
            return (IsAttributeClass(AttrClass.TWODFX_WALK_DONT_WALK));
        }

        /// <summary>
        /// Determine whether this object is a 2dfx WindowLedge.
        /// </summary>
        public bool Is2dfxWindowLedge()
        {
            return (IsAttributeClass(AttrClass.TWODFX_WINDOW_LEDGE));
        }

        /// <summary>
        /// Determine whether this object is a 2dfx Wind Disturbance.
        /// </summary>
        /// <returns></returns>
        public bool Is2dfxRsWindDisturbance()
        {
            return (IsAttributeClass(AttrClass.TWODFX_WIND_DISTURBANCE));
        }

        /// <summary>
        /// Determine whether this object is a 2dfx Ledge.
        /// </summary>
        public bool Is2dfxLedge()
        {
            return (IsAttributeClass(AttrClass.TWODFX_LEDGE) || IsMaxClass(ClassConsts.MAX_CLASS_RSLEDGE));
        }

        /// <summary>
        /// Determine whether this object is a 2dfx door.
        /// </summary>
        public bool Is2dfxDoor()
        {
            return (IsAttributeClass(AttrClass.TWODFX_DOOR));
        }

        /// <summary>
        /// Determine whether this object is a 2dfx expression.
        /// </summary>
        public bool Is2dfxExpression()
        {
            return (IsAttributeClass(AttrClass.TWODFX_EXPRESSION));
        }
        #endregion // 2dfx Object Attribute Class Query Methods
        #endregion // Specific Object Attribute Class Query Methods

        #region Specific Object Attribute Value Query Methods
        /// <summary>
        /// Determine whether this object is a time object.
        /// </summary>
        public bool IsTimeObject()
        {
            return (this.GetAttribute(AttrNames.OBJ_IS_TIME_OBJECT, AttrDefaults.OBJ_IS_TIME_OBJECT));
        }

        /// <summary>
        /// Determine whether this object is an anim object.
        /// </summary>
        public bool IsAnimObject()
        {
            return (this.GetAttribute(AttrNames.OBJ_HAS_ANIM, AttrDefaults.OBJ_HAS_ANIM) || 
                this.GetAttribute(AttrNames.OBJ_HAS_UV_ANIM, AttrDefaults.OBJ_HAS_UV_ANIM));
        }

        /// <summary>
        /// Determine whether this object is a dynamic object.
        /// </summary>
        public bool IsDynObject()
        {
            return (this.GetAttribute(AttrNames.OBJ_IS_DYNAMIC, AttrDefaults.OBJ_IS_DYNAMIC));
        }

        /// <summary>
        /// Determine whether this object is a fragment.
        /// </summary>
        public bool IsFragment()
        {
            return (this.GetAttribute(AttrNames.OBJ_IS_FRAGMENT, AttrDefaults.OBJ_IS_FRAGMENT));
        }

        /// <summary>
        /// Determine whether this object is a fragment proxy.
        /// </summary>
        public bool IsFragmentProxy()
        {
            return (this.GetAttribute(AttrNames.OBJ_IS_FRAGMENT_PROXY, AttrDefaults.OBJ_IS_FRAGMENT_PROXY));
        }

        /// <summary>
        /// Determine whether this object is cloth.
        /// </summary>
        public bool IsCloth()
        {
            return (this.GetAttribute(AttrNames.OBJ_IS_CLOTH, AttrDefaults.OBJ_IS_CLOTH));
        }

        /// <summary>
        /// Determine whether this object is a StatedAnim object.
        /// </summary>
        /// <returns></returns>
        public bool IsStatedAnim()
        {
            return (AttrDefaults.OBJ_GROUP_NAME != this.GetAttribute(AttrNames.OBJ_GROUP_NAME, AttrDefaults.OBJ_GROUP_NAME));
        }

        /// <summary>
        /// Determine whether this object is an statedAnimProxy.
        /// </summary>
        public bool IsAnimProxy()
        {
            String objectName = this.Name;
            bool isStatedAnimRef = objectName.EndsWith(PROXY_ANIM);

            return (isStatedAnimRef && (IsAttributeClass(AttrClass.STATED_ANIM_PROXY) || IsMaxClass(ClassConsts.MAX_CLASS_ANIM_PROXY)));
        }

        /// <summary>
        /// Determine whether this object is an IMAP group dummy
        /// </summary>
        public bool IsIMAPGroupDummy()
        {
            return IsMaxClass(ClassConsts.MAX_IMAP_GROUP_DUMMY);
        }
    
        /// <summary>
        /// Determine whether this object is an IMAP group dummy
        /// </summary>
        public bool IsIMAPGroupDefinition()
        {
            return IsMaxClass(ClassConsts.MAX_IMAP_GROUP_DEFINITION);
        }
        /// <summary>
        /// Determine whether this object has an audio material.
        /// </summary>
        public bool HasAudioMaterial()
        {
            String material = this.GetAttribute(AttrNames.COLL_AUDIO_MATERIAL, AttrDefaults.COLL_AUDIO_MATERIAL);
            return (IsCollision() && (material.Length > 0));
        }

        /// <summary>
        /// Determine whether this object is flagged to have its collision baked into the static bounds.
        /// </summary>
        public bool HasForceBakeCollisionFlag()
        {
            return (this.GetAttribute(AttrNames.OBJ_FORCE_BAKE_COLLISION, AttrDefaults.OBJ_FORCE_BAKE_COLLISION));
        }

        /// <summary>
        /// Determine whether this object is flagged to never have its collision baked into the static bounds.
        /// </summary>
        public bool HasNeverBakeCollisionFlag()
        {
            return (this.GetAttribute(AttrNames.OBJ_NEVER_BAKE_COLLISION, AttrDefaults.OBJ_NEVER_BAKE_COLLISION));
        }

        /// <summary>
        /// Does this object pass the current rules on being baked - this is queried during IMAP export and the bounds processor
        /// </summary>
        public bool WillHaveCollisionBaked()
        {
            if (RefObject != null && RefObject.IsDynObject())
                return false;// we never bake dynamic props

            bool definitionRequiresBaking = RefObject != null && RefObject.HasForceBakeCollisionFlag();
            bool instanceIsScaled = NodeScale.Dist(new Vector3f(1.0f, 1.0f, 1.0f)) > 0.00001f;
            int priority = GetAttribute(AttrNames.OBJ_PRIORITY, AttrDefaults.OBJ_PRIORITY);

            bool result = (definitionRequiresBaking && !HasNeverBakeCollisionFlag() && (priority == 0)) ||
                          (instanceIsScaled && !HasNeverBakeCollisionFlag()) ||
                          HasForceBakeCollisionFlag();

            return result;
        }

        /// <summary>
        /// Is this object part of an interior entity set?
        /// This requires scanning the scene so is non-trivial to call
        /// </summary>
        /// <returns></returns>
        public bool IsPartOfAnEntitySet()
        {
            // Find all of the Interior Groups in the scene.  Is 'this' part of that set?
            TargetObjectDef[] interiorGroups = this.MyScene.Walk(Scene.WalkMode.BreadthFirst).Where(obj => obj.IsInteriorGroup()).ToArray();
            foreach (TargetObjectDef interiorGroup in interiorGroups)
            {
                Object[] groupObjGuids = interiorGroup.GetParameter(ParamNames.INTERIOR_GROUP_OBJECTS, ParamDefaults.INTERIOR_GROUP_OBJECTS);
                foreach (Guid groupObjGuid in groupObjGuids)
                {
                    ObjectDef groupObj = this.MyScene.FindObject(groupObjGuid).ObjectDef;
                    if (this == groupObj)
                        return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Determine whether this object is a streaming extents override box
        /// </summary>
        public bool IsStreamingExtentsOverrideBox()
        {
            return IsMaxClass(ClassConsts.MAX_STREAMING_EXTENTS_OVERRIDE_BOX);
        }
        #endregion // Specific Object Attribute Value Query Methods
        #endregion // Controller Methods

        #region Static Controller Methods
        /// <summary>
        /// Return whether specified child object has parent as one of its LOD
        /// parents (somewhere up the LOD tree).
        /// </summary>
        /// <returns></returns>
        public static bool IsLODParentOf(ObjectDef child, ObjectDef parent)
        {
            if (!child.HasLODParent())
                return (false);

            // Walk up the LOD Parent tree; look for our other entity.
            ObjectDef parentTest = child.LOD.Parent.ObjectDef;
            while (null != parentTest)
            {
                if (parentTest == parent)
                    return (true);

                if (parentTest.HasLODParent())
                    parentTest = parentTest.LOD.Parent.ObjectDef;
                else
                    parentTest = null;
            }

            return (false);
        }

        /// <summary>
        /// Return whether specified entity is a child of this one (somewhere
        /// down the LOD tree).
        /// </summary>
        /// <returns></returns>
        public static bool IsLODChildOf(ObjectDef parent, ObjectDef child)
        {
            return (IsLODParentOf(child, parent));
        }
        #endregion // Static Controller Methods

        #region Internal Methods
        /// <summary>
        /// Resolve all ObjectDef links between ObjectDef nodes.
        /// </summary>
        /// <param name="scene"></param>
        internal void ResolveLinks(Scene scene)
        {
            // Resolve internalref objects.
            if (IsInternalRef() && this.InternalRefGuid != Guid.Empty)
            {
                TargetObjectDef targetObjDef = scene.FindObject(this.InternalRefGuid);

                if (targetObjDef == null)
                    this.InternalRef = null;
                else
                    this.InternalRef = targetObjDef.ObjectDef;
            }
            else if (IsRefInternalObject())
            {
                String name = GetParameter(ParamNames.RSREFOBJECT_OBJECTNAME, ParamDefaults.RSREFOBJECT_OBJECTNAME);
                TargetObjectDef targetObjDef = scene.FindObjectSlow(name);

                if (targetObjDef == null)
                    this.InternalRef = null;
                else
                    this.InternalRef = targetObjDef.ObjectDef;
            }

            // Resolve generic references to objects.
            List<ObjectDef> objs = new List<ObjectDef>();
            foreach (Guid guid in this.GenericReferenceGuids)
                objs.Add(scene.FindObject(guid).ObjectDef);
            this.m_GenericReferences = objs.ToArray();
            Debug.Assert(this.GenericReferenceGuids.Length == this.GenericReferences.Length,
                String.Format("Not all generic references were resolved for {0}.", this.Name));

            // Resolve SceneLink References
            foreach (LODHierarchyDef channel in SceneLinks.Values)
            {
                channel.ResolveLinks(scene);
            }
                

            // Resolve Skin/Skeleton References
            if (Guid.Empty != this.SkeletonRootGuid)
            {
                this.SkeletonRoot = scene.FindObject(this.SkeletonRootGuid).ObjectDef;
            }
        }

        /// <summary>
        /// Determine this Object's LOD Level.
        /// </summary>
        internal void DetermineLodLevel()
        {
            bool isRef = (this.IsRefObject() || this.IsXRef() || this.IsInternalRef() || this.IsRefInternalObject());
            if (!this.IsObject() && !isRef && !this.IsMilo())
            {
                this.LODLevel = LodLevel.UNKNOWN;
                return;
            }

            int lodLevelsBelow = CountLODLevelsBelowMe();

            if (SceneType.EnvironmentContainer == this.MyScene.SceneType)
            {
                switch (lodLevelsBelow)
                {
                    case 0:
                        if (this.HasLODParent())
                            this.LODLevel = LodLevel.HD;
                        else
                            this.LODLevel = LodLevel.ORPHANHD;
                        break;
                    case 1:
                        this.LODLevel = LodLevel.LOD;
                        break;
                    case 2:
                        this.LODLevel = LodLevel.SLOD1;
                        break;
                    default:
                        this.LODLevel = LodLevel.UNKNOWN;
                        break;
                }
            }
            else if (SceneType.EnvironmentContainerLod == this.MyScene.SceneType)
            {
                switch (lodLevelsBelow)
                {
                    case 0:
                        this.LODLevel = LodLevel.SLOD2;
                        break;
                    case 1:
                        this.LODLevel = LodLevel.SLOD3;
                        break;
                    case 2:
                        this.LODLevel = LodLevel.SLOD4;
                        break;
                    default:
                        this.LODLevel = LodLevel.UNKNOWN;
                        break;
                }
            }
            else if ((SceneType.EnvironmentProps == this.MyScene.SceneType) ||
                     (SceneType.EnvironmentInterior == this.MyScene.SceneType))
            {
                this.LODLevel = LodLevel.ORPHANHD;
            }
            else
            {
                this.LODLevel = LodLevel.UNKNOWN;
            }
        }
        #endregion // Internal Methods

        #region Private Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        private void ParseSkip(XmlTextReader reader)
        {
            if (reader.IsEmptyElement)
                reader.ReadStartElement();
            else
            {
                reader.ReadStartElement();
                while (reader.MoveToContent() == XmlNodeType.Element)
                {
                    ParseSkip(reader);
                }
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }

        /// <summary>
        /// Parse, completing the object's member data.
        /// </summary>
        /// <param name="reader"></param>
        private void Parse(XmlTextReader reader)
        {
            if (reader.IsEmptyElement)
                ParseObjectAttributes(reader);
            else
            {
                ParseObjectAttributes(reader);
                ParseMembers(reader);
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="reader"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        private String GetAttribute(String name, XmlTextReader reader, String defaultValue)
        {
            String attribute = reader.GetAttribute(name);
            if (String.IsNullOrWhiteSpace(attribute))
            {
                attribute = defaultValue;
            }
            return attribute;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="reader"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        private Guid GetAttributeAsGuid(String name, XmlTextReader reader, Guid defaultValue)
        {
            Guid guid = defaultValue;
            String attribute = reader.GetAttribute(name);
            if (!String.IsNullOrWhiteSpace(attribute))
            {
                guid = new Guid(attribute);
            }
            return guid;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="reader"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        private int GetAttributeAsInt(String name, XmlTextReader reader, int defaultValue)
        {
            int integer = defaultValue;
            String attribute = reader.GetAttribute(name);
            if (!String.IsNullOrWhiteSpace(attribute))
            {
                integer = int.Parse(attribute);
            }
            return integer;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        private void ParseObjectAttributes(XmlTextReader reader)
        {
            this.m_sName = reader.GetAttribute(sC_s_s_ObjectName);
            this.m_Guid = this.GetAttributeAsGuid(sC_s_s_ObjectGuid, reader, default(Guid));
            this.m_MaterialGuid = this.GetAttributeAsGuid(sC_s_s_ObjectMaterial, reader, default(Guid));
            this.m_sClass = this.GetAttribute(sC_s_s_ObjectClass, reader, String.Empty).ToLower();
            this.m_sSuperclass = this.GetAttribute(sC_s_s_ObjectSuperclass, reader, String.Empty).ToLower();
            this.m_sRefName = reader.GetAttribute(sC_s_s_ObjectRefName);
            this.m_sRefFile = reader.GetAttribute(sC_s_s_ObjectRefFile);
            this.m_sRefFile = this.m_sRefFile != null ? Path.GetFullPath(this.m_sRefFile) : this.m_sRefFile;
            this.m_InternalRefGuid = this.GetAttributeAsGuid(sC_s_s_ObjectRef, reader, default(Guid));

            reader.ReadStartElement();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        private void ParseMembers(XmlTextReader reader)
        {
            while (reader.MoveToContent() == XmlNodeType.Element)
            {
                int nameHash = reader.Name.GetHashCode();
                if (nameHash == HashLookupTable[sC_s_s_TransformElement])
                {
                    this.ParseTransform(reader);
                }
                else if (nameHash == HashLookupTable[sC_s_s_BoundingBoxElement])
                {
                    this.ParseBoundingBox(reader);
                }
                else if (nameHash == HashLookupTable[sC_s_s_PolyCountElement])
                {
                    this.ParsePolyCount(reader);
                }
                else if (nameHash == HashLookupTable[sC_s_s_SubObjectsElement])
                {
                    this.ParseSubObjects(reader);
                }
                else if (nameHash == HashLookupTable[sC_s_s_ChildrenElement])
                {
                    this.ParseChildren(reader);
                }
                else if (nameHash == HashLookupTable[sC_s_s_SceneLinkHierarchyElement])
                {
                    this.ParseSceneLinks(reader);
                }
                else if (nameHash == HashLookupTable[sC_s_s_AttributesElement])
                {
                    m_sAttributeGuid = this.GetAttributeAsGuid("guid", reader, Guid.Empty);
                    m_sAttributeClass = this.GetAttribute("class", reader, "").ToLower();
                    this.ParseAttributes(reader, ref m_dAttributes, "attribute");
                }
                else if (nameHash == HashLookupTable[sC_s_s_PlatformAttributesElement])
                {
                    this.ParsePlatformAttributes(reader);
                }
                else if (nameHash == HashLookupTable[sC_s_s_PropertiesElement])
                {
                    this.ParseAttributes(reader, ref m_dProperties, "property");
                }
                else if (nameHash == HashLookupTable[sC_s_s_CustomAttributesElement])
                {
                    this.ParseAttributes(reader, ref m_dCustomAttributes, "custom");
                }
                else if (nameHash == HashLookupTable[sC_s_s_UserPropertiesElement])
                {
                    this.ParseAttributes(reader, ref m_dUserProperties, "property");
                }
                else if (nameHash == HashLookupTable[sC_s_s_AnimAttributesElement])
                {
                    this.ParseAnimationAttributes(reader);
                }
                else if (nameHash == HashLookupTable[sC_s_s_ParamBlocksElement])
                {
                    this.ParseParamBlocks(reader);
                }
                else if (nameHash == HashLookupTable[sC_s_s_ReferencedTexturesElement])
                {
                    this.ParseRefdTextures(reader);
                }
                else
                {
                    this.ParseSkip(reader);
                }
            }
            if (reader.NodeType == XmlNodeType.Text)
                reader.ReadString();
            if (reader.NodeType == XmlNodeType.EndElement)
                reader.ReadEndElement();
        }

        private Vector3f cachedObjectPosition = new Vector3f();
        private Quaternionf cachedObjectRotation = new Quaternionf();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="objectTransform"></param>
        private void ParseTransform(XmlTextReader reader, Boolean objectTransform)
        {
            if (reader.IsEmptyElement)
                reader.ReadStartElement();
            else
            {
                Vector3f position = null;
                Quaternionf rotation = null;
                Vector3f scale = null;

                reader.ReadStartElement();
                while (reader.MoveToContent() == XmlNodeType.Element)
                {
                    float x = 0.0f, y = 0.0f, z = 0.0f, w = 0.0f;

                    if (String.Equals(reader.Name, "position"))
                    {
                        ParseTransformElement(reader, "x", out x);
                        ParseTransformElement(reader, "y", out y);
                        ParseTransformElement(reader, "z", out z);

                        position = new Vector3f(x, y, z);
                        reader.ReadStartElement();
                    }
                    else if (String.Equals(reader.Name, "rotation"))
                    {
                        ParseTransformElement(reader, "x", out x);
                        ParseTransformElement(reader, "y", out y);
                        ParseTransformElement(reader, "z", out z);
                        ParseTransformElement(reader, "w", out w);

                        rotation = new Quaternionf(x, y, z, w);
                        rotation.Normalise();
                        reader.ReadStartElement();
                    }
                    else if (String.Equals(reader.Name, "scale"))
                    {
                        ParseTransformElement(reader, "x", out x);
                        ParseTransformElement(reader, "y", out y);
                        ParseTransformElement(reader, "z", out z);

                        scale = new Vector3f(x, y, z);
                        reader.ReadStartElement();
                    }
                    else
                    {
                        this.ParseSkip(reader);
                    }
                }
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();

                if (objectTransform)
                {
                    if (position == null)
                        position = new Vector3f();
                    if (rotation == null)
                        rotation = new Quaternionf();
                    if (scale == null)
                        scale = new Vector3f(1.0f, 1.0f, 1.0f);

                    this.m_mObjectTransform = new Matrix34f(rotation);
                    this.m_mObjectTransform.Translation = position;
                    this.m_objectRotation = rotation;
                    this.ObjectScale = scale;

                    cachedObjectPosition = position;
                    cachedObjectRotation = rotation;
                }
                else
                {
                    if (position == null)
                        position = cachedObjectPosition;
                    if (rotation == null)
                        rotation = cachedObjectRotation;
                    if (scale == null)
                        scale = new Vector3f(1.0f, 1.0f, 1.0f);

                    this.m_mNodeTransform = new Matrix34f(rotation);
                    this.m_mNodeTransform.Translation = position;
                    this.m_nodeRotation = rotation;
                    this.NodeScale = scale;
                }
            }
        }

        /// <summary>
        /// Parse our transform XML data.
        /// </summary>
        private void ParseTransform(XmlTextReader reader)
        {
            if (reader.IsEmptyElement)
                reader.ReadStartElement();
            else
            {
                reader.ReadStartElement();
                while (reader.MoveToContent() == XmlNodeType.Element)
                {
                    if (String.Equals(reader.Name, "object"))
                    {
                        this.ParseTransform(reader, true);
                    }
                    else if (String.Equals(reader.Name, "node"))
                    {
                        this.ParseTransform(reader, false);
                    }
                    else
                    {
                        this.ParseSkip(reader);
                    }
                }
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="attrName"></param>
        /// <param name="val"></param>
        private void ParseTransformElement(XmlTextReader reader, String attrName, out float val)
        {
            if (!float.TryParse(reader.GetAttribute(attrName), out val))
            {
                Scene.Log.ErrorCtx(this.Name, reader, "Failed to parse object's transform '{0}' parameter '{1}'.  Invalid float.",
                    reader.Name, attrName);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="localBoundingBox"></param>
        private void ParseBoundingBox(XmlTextReader reader, Boolean localBoundingBox)
        {
            if (reader.IsEmptyElement)
                reader.ReadStartElement();
            else
            {
                Vector3f min = null;
                Vector3f max = null;

                reader.ReadStartElement();
                while (reader.MoveToContent() == XmlNodeType.Element)
                {
                    float x = 0.0f, y = 0.0f, z = 0.0f;

                    if (String.Equals(reader.Name, "min"))
                    {
                        float.TryParse(reader.GetAttribute("x"), out x);
                        float.TryParse(reader.GetAttribute("y"), out y);
                        float.TryParse(reader.GetAttribute("z"), out z);

                        min = new Vector3f(x, y, z);
                        reader.ReadStartElement();
                    }
                    else if (String.Equals(reader.Name, "max"))
                    {
                        float.TryParse(reader.GetAttribute("x"), out x);
                        float.TryParse(reader.GetAttribute("y"), out y);
                        float.TryParse(reader.GetAttribute("z"), out z);

                        max = new Vector3f(x, y, z);
                        reader.ReadStartElement();
                    }
                    else
                    {
                        reader.Skip();
                    }
                }
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();

                BoundingBox3f bbox = new BoundingBox3f();
                if (min != null)
                    bbox.Expand(min);
                if (max != null)
                    bbox.Expand(max);

                if (localBoundingBox)
                {
                    if (!bbox.IsEmpty)
                        this.LocalBoundingBox = bbox;
                }
                else
                {
                    if (!bbox.IsEmpty)
                        this.WorldBoundingBox = bbox;
                }
            }
        }

        /// <summary>
        /// Parse our boundingbox XML data.
        /// </summary>
        private void ParseBoundingBox(XmlTextReader reader)
        {
            if (reader.IsEmptyElement)
                reader.ReadStartElement();
            else
            {
                reader.ReadStartElement();
                while (reader.MoveToContent() == XmlNodeType.Element)
                {
                    if (String.Equals(reader.Name, "local"))
                    {
                        this.ParseBoundingBox(reader, true);
                    }
                    else if (String.Equals(reader.Name, "world"))
                    {
                        this.ParseBoundingBox(reader, false);
                    }
                    else
                    {
                        reader.Skip();
                    }
                }
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }

        /// <summary>
        /// Parse our sub-objects.
        /// </summary>
        /// <param name="navigator">XPath document navigator</param>
        private void ParseSubObjects(XmlTextReader reader)
        {
            List<ObjectDef> objectList = new List<ObjectDef>();
            if (reader.IsEmptyElement)
                reader.ReadStartElement();
            else
            {
                reader.ReadStartElement();
                while (reader.MoveToContent() == XmlNodeType.Element)
                {
                    if (String.Equals(reader.Name, "object"))
                    {
                        ObjectDef child = new ObjectDef(reader, m_oScene);
                        objectList.Add(child);
                    }
                    else
                    {
                        reader.Skip();
                    }
                }
                this.m_aSubObjects = objectList.ToArray();
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }

        /// <summary>
        /// Parse our object's children.
        /// </summary>
        private void ParseChildren(XmlTextReader reader)
        {
            // Parse our root Object Definitions, these take care of our
            // child objects.
            List<ObjectDef> objectList = new List<ObjectDef>();
            if (reader.IsEmptyElement)
                reader.ReadStartElement();
            else
            {
                reader.ReadStartElement();
                while (reader.MoveToContent() == XmlNodeType.Element)
                {
                    if (String.Equals(reader.Name, "object"))
                    {
                        ObjectDef child = new ObjectDef(reader, m_oScene);
                        child.Parent = this;
                        objectList.Add(child);
                    }
                    else
                    {
                        this.ParseSkip(reader);
                    }
                }
                this.m_aChildren = objectList.ToArray();
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        private void ParsePolyCount(XmlTextReader reader)
        {
            String value = reader.GetAttribute("value");
            if (!String.IsNullOrEmpty(value) && !String.IsNullOrWhiteSpace(value))
            {
                this.m_nPolyCount = int.Parse(value);
            }
            reader.Skip();
        }

        void ParseSceneLinks(XmlTextReader reader)
        {
            if (reader.IsEmptyElement)
                reader.ReadStartElement();
            else
            {
                reader.ReadStartElement();
                while (reader.MoveToContent() == XmlNodeType.Element)
                {
                    if (String.Equals(reader.Name, "channel"))
                    {
                        LODHierarchyDef hierDef = new LODHierarchyDef(reader);
                        Enum channel = EnumExtensions.GetValByDisplayName<LinkChannel>(hierDef.Name);
                        if(channel != null)
                            this.m_SceneLinks.Add((LinkChannel)channel, hierDef);
                    }
                    reader.Skip();
                }
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }

        // temporary dict used as buffer to read paramblocks, to avoid creating new temprary collection
        // before returning the final values
        private Dictionary<string, object> m_arrayContainer = new Dictionary<string, object>();

        /// <summary>
        /// Parse a single attribute, placing it into the appropriate container.
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="container"></param>
        private void ParseAttribute(XmlTextReader reader, ref AttributeContainer container)
        {
            string name = reader.GetAttribute(sC_s_s_AttrName);
            string type = reader.GetAttribute(sC_s_s_AttrType);
            string valuestr = reader.GetAttribute(sC_s_s_AttrValue);

            name = (name == null) ? String.Empty : name.Trim().ToLower();
            type = (type == null) ? String.Empty : type.Trim().ToLower();
            valuestr = (valuestr == null) ? String.Empty : valuestr.Trim().ToLower();

            int typeHash = type.GetHashCode();

            // Initialise an attribute.
            if (HashLookupTable[sC_s_s_AttrTypeArray] == typeHash)
            {
                if (reader.IsEmptyElement)
                {
                    reader.ReadStartElement();
                    container[name] = new Object[0]; // Empty array.
                }
                else
                {
                    reader.ReadStartElement();

                    m_arrayContainer.Clear();
                    while (reader.MoveToContent() == XmlNodeType.Element)
                    {
                        if (String.Equals("parameter", reader.Name))
                        {
                            ParseAttribute(reader, ref m_arrayContainer);
                        }
                        else
                        {
                            reader.Skip();
                        }
                    }
                    container[name] = m_arrayContainer.Values.ToArray();

                    if (reader.NodeType == XmlNodeType.Text)
                        reader.ReadString();
                    if (reader.NodeType == XmlNodeType.EndElement)
                        reader.ReadEndElement();
                }
            }
            else if (HashLookupTable[sC_s_s_AttrTypeBool] == typeHash)
            {
                container[name] = bool.Parse(valuestr);
                reader.Skip();
            }
            else if (HashLookupTable[sC_s_s_AttrTypeInt] == typeHash)
            {
                container[name] = int.Parse(valuestr);
                reader.Skip();
            }
            else if (HashLookupTable[sC_s_s_AttrTypeFloat] == typeHash)
            {
                container[name] = float.Parse(valuestr);
                reader.Skip();
            }
            else if (HashLookupTable[sC_s_s_AttrTypeObject] == typeHash)
            {
                container[name] = new Guid(valuestr);
                reader.Skip();
            }
            else if (HashLookupTable[sC_s_s_AttrTypeRGB] == typeHash)
            {
                if (reader.IsEmptyElement)
                    reader.ReadStartElement();
                else
                {
                    reader.ReadStartElement();
                    while (reader.MoveToContent() == XmlNodeType.Element)
                    {
                        string rString = reader.GetAttribute("r");
                        float r = !String.IsNullOrEmpty(rString) ? float.Parse(rString) : 0.0f;
                        string gString = reader.GetAttribute("g");
                        float g = !String.IsNullOrEmpty(gString) ? float.Parse(gString) : 0.0f;
                        string bString = reader.GetAttribute("b");
                        float b = !String.IsNullOrEmpty(bString) ? float.Parse(bString) : 0.0f;
                        container[name] = new Colour3f(r, g, b);
                        reader.Skip();
                    }
                    if (reader.NodeType == XmlNodeType.Text)
                        reader.ReadString();
                    if (reader.NodeType == XmlNodeType.EndElement)
                        reader.ReadEndElement();
                }
            }
            else if (HashLookupTable[sC_s_s_AttrTypeRGBA] == typeHash)
            {
                if (reader.IsEmptyElement)
                    reader.ReadStartElement();
                else
                {
                    reader.ReadStartElement();
                    while (reader.MoveToContent() == XmlNodeType.Element)
                    {
                        string rString = reader.GetAttribute("r");
                        float r = !String.IsNullOrEmpty(rString) ? float.Parse(rString) : 0.0f;
                        string gString = reader.GetAttribute("g");
                        float g = !String.IsNullOrEmpty(gString) ? float.Parse(gString) : 0.0f;
                        string bString = reader.GetAttribute("b");
                        float b = !String.IsNullOrEmpty(bString) ? float.Parse(bString) : 0.0f;
                        string aString = reader.GetAttribute("a");
                        float a = !String.IsNullOrEmpty(aString) ? float.Parse(aString) : 0.0f;
                        container[name] = new Vector4f(r, g, b, a);
                        reader.Skip();
                    }
                    if (reader.NodeType == XmlNodeType.Text)
                        reader.ReadString();
                    if (reader.NodeType == XmlNodeType.EndElement)
                        reader.ReadEndElement();
                }
            }
            else if (HashLookupTable[sC_s_s_AttrTypeVectorXYZ] == typeHash)
            {
                if (reader.IsEmptyElement)
                    reader.ReadStartElement();
                else
                {
                    reader.ReadStartElement();
                    while (reader.MoveToContent() == XmlNodeType.Element)
                    {
                        string rString = reader.GetAttribute("x");
                        float x = !String.IsNullOrEmpty(rString) ? float.Parse(rString) : 0.0f;
                        string gString = reader.GetAttribute("y");
                        float y = !String.IsNullOrEmpty(gString) ? float.Parse(gString) : 0.0f;
                        string bString = reader.GetAttribute("z");
                        float z = !String.IsNullOrEmpty(bString) ? float.Parse(bString) : 0.0f;
                        if (String.Equals(reader.Name, "colour"))
                            container[name] = new Colour3f(x, y, z);
                        else
                            container[name] = new Vector3f(x, y, z);

                        reader.Skip();
                    }
                    if (reader.NodeType == XmlNodeType.Text)
                        reader.ReadString();
                    if (reader.NodeType == XmlNodeType.EndElement)
                        reader.ReadEndElement();
                }
            }
            else if (HashLookupTable[sC_s_s_AttrTypeVectorXYZW] == typeHash)
            {
                if (reader.IsEmptyElement)
                    reader.ReadStartElement();
                else
                {
                    reader.ReadStartElement();
                    while (reader.MoveToContent() == XmlNodeType.Element)
                    {
                        string rString = reader.GetAttribute("x");
                        float x = !String.IsNullOrEmpty(rString) ? float.Parse(rString) : 0.0f;
                        string gString = reader.GetAttribute("y");
                        float y = !String.IsNullOrEmpty(gString) ? float.Parse(gString) : 0.0f;
                        string bString = reader.GetAttribute("z");
                        float z = !String.IsNullOrEmpty(bString) ? float.Parse(bString) : 0.0f;
                        string aString = reader.GetAttribute("w");
                        float w = !String.IsNullOrEmpty(aString) ? float.Parse(aString) : 0.0f;
                        container[name] = new Vector4f(x, y, z, w);
                        reader.Skip();
                    }
                    if (reader.NodeType == XmlNodeType.Text)
                        reader.ReadString();
                    if (reader.NodeType == XmlNodeType.EndElement)
                        reader.ReadEndElement();
                }
            }
            else
            {
                reader.Skip();
                if (String.Equals(sC_s_s_ObjectRefName, name, StringComparison.CurrentCultureIgnoreCase))
                    m_sRefName = valuestr;
                else if (String.Equals(sC_s_s_ObjectRefName2, name, StringComparison.CurrentCultureIgnoreCase))
                    m_sRefName = valuestr;
                else if (String.Equals(sC_s_s_ObjectRefFile2, name, StringComparison.CurrentCultureIgnoreCase))
                    m_sRefFile = valuestr;
                container[name] = valuestr;
            }
        }

        /// <summary>
        /// Parse our attribute, property and parameter block XML data.
        /// </summary>
        private void ParseAttributes(XmlTextReader reader, ref AttributeContainer container, String atrributeNodeName)
        {
            if (reader.IsEmptyElement)
                reader.ReadStartElement();
            else
            {
                reader.ReadStartElement();
                while (reader.MoveToContent() == XmlNodeType.Element)
                {
                    if (String.Equals(reader.Name, atrributeNodeName))
                    {
                        ParseAttribute(reader, ref container);
                    }
                    else
                    {
                        reader.Skip();
                    }
                }
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }

        /// <summary>
        /// Parse our platform attributes.
        /// </summary>
        private void ParsePlatformAttributes(XmlTextReader reader)
        {
            if (reader.IsEmptyElement)
            {
                reader.ReadStartElement();
            }
            else
            {
                reader.ReadStartElement();
                while (reader.MoveToContent() == XmlNodeType.Element)
                {
                    if (String.Equals(reader.Name, "platform"))
                    {
                        AttributeContainer container = new AttributeContainer();
                        String platform = reader.GetAttribute("name");

                        reader.ReadStartElement();
                        while (reader.MoveToContent() == XmlNodeType.Element)
                        {
                            if (String.Equals(reader.Name, "attribute"))
                            {
                                ParseAttribute(reader, ref container);
                            }
                            else
                            {
                                reader.Skip();
                            }
                        }
                        
                        if (reader.NodeType == XmlNodeType.Text)
                            reader.ReadString();

                        if (reader.NodeType == XmlNodeType.EndElement)
                        {
                            reader.ReadEndElement();

                            RSG.Platform.Platform ePlatform = RSG.Platform.PlatformUtils.PlatformFromString(platform);
                            m_dPlatformAttributes[ePlatform] = container;
                        }
                    }
                    else
                    {
                        reader.Skip();
                    }
                }
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }

        /// <summary>
        /// Parse properties with Keyframes in animation Properties
        /// </summary>
        private void ParseKeyedProperties(XmlTextReader reader)
        {
            if (reader.IsEmptyElement)
                reader.ReadStartElement();
            else
            {
                // new keyed Property
                List<sKeyFrame> keys = new List<sKeyFrame>();
                m_aKeyedProperties.Add(reader.Name, keys);

                reader.ReadStartElement();
                while (reader.MoveToContent() == XmlNodeType.Element)
                {
                    if (String.Equals(reader.Name, "key"))
                    {
                        int state = GetAttributeAsInt(sC_s_s_AttrValue, reader, 0);
                        int tickTime = GetAttributeAsInt(sC_s_s_AttrFrame, reader, 0);

                        sKeyFrame item = new sKeyFrame();
                        item.m_nTime = tickTime;
                        item.m_nState = state;
                        keys.Add(item);
                        ParseSkip(reader);
                    }
                    else
                    {
                        ParseSkip(reader);
                    }
                }
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }

        /// <summary>
        /// Parse animation attributes
        /// </summary>
        private void ParseAnimationAttributes(XmlTextReader reader)
        {
            if (reader.IsEmptyElement)
                reader.ReadStartElement();
            else
            {
                reader.ReadStartElement();
                while (reader.MoveToContent() == XmlNodeType.Element)
                {
                    if (String.Equals(reader.Name, "skeleton_root_obj"))
                    {
                        this.SkeletonRootGuid = GetAttributeAsGuid("guid", reader, default(Guid));
                        reader.Skip();
                    }
                    else if (String.Equals(reader.Name, "animation_length"))
                    {
                        this.m_nAnimLength = GetAttributeAsInt("value", reader, default(int));
                        ParseSkip(reader);
                    }
                    else if (String.Equals(reader.Name, "keys"))
                    {
                        if (reader.IsEmptyElement)
                            reader.ReadStartElement();
                        else
                        {
                            reader.ReadStartElement();
                            while (reader.MoveToContent() == XmlNodeType.Element)
                            {
                                ParseKeyedProperties(reader);
                            }
                            if (reader.NodeType == XmlNodeType.Text)
                                reader.ReadString();
                            if (reader.NodeType == XmlNodeType.EndElement)
                                reader.ReadEndElement();
                        }
                    }
                    else
                    {
                        ParseSkip(reader);
                    }
                }
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }

        // temporary list used as buffer to read paramblocks, to avoid creating new temprary collection
        // before returning the final values
        private readonly List<AttributeContainer> m_paramBlocks = new List<AttributeContainer>( 20 );

        /// <summary>
        /// Parse a ParamBlock definition.
        /// </summary>
        /// <param name="reader"></param>
        private void ParseParamBlocks(XmlTextReader reader)
        {
            if (reader.IsEmptyElement)
                reader.ReadStartElement();
            else
            {
                m_paramBlocks.Clear();
                reader.ReadStartElement();
                while (reader.MoveToContent() == XmlNodeType.Element)
                {
                    if (String.Equals(reader.Name, "paramblock"))
                    {
                        m_paramBlocks.Add(ParseParamBlock(reader));
                    }
                    else
                    {
                        reader.Skip();
                    }
                }
                this.m_aParamBlocks = m_paramBlocks.ToArray();
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }

        /// <summary>
        /// Parse a single Parameter Block XML definition.
        /// </summary>
        /// <param name="reader"></param>
        private AttributeContainer ParseParamBlock(XmlTextReader reader)
        {
            AttributeContainer container = new AttributeContainer();
            if (reader.IsEmptyElement)
                reader.ReadStartElement();
            else
            {
                reader.ReadStartElement();
                while (reader.MoveToContent() == XmlNodeType.Element)
                {
                    if (String.Equals(reader.Name, "parameter"))
                    {
                        ParseAttribute(reader, ref container);
                    }
                    else
                    {
                        reader.Skip();
                    }
                }
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();

            }
            return container;
        }

        /// <summary>
        /// Parse a ParamBlock definition.
        /// </summary>
        /// <param name="reader"></param>
        private void ParseRefdTextures(XmlTextReader reader)
        {
            if (reader.IsEmptyElement)
                reader.ReadStartElement();
            else
            {
                reader.ReadStartElement();
                this.m_aRefdTextures = new List<sRefdTexture>();
                while (reader.MoveToContent() == XmlNodeType.Element)
                {
                    if (String.Equals(reader.Name, "texture"))
                    {
                       this.ParseRefdTexture(reader);
                    }
                    reader.Skip();
                }
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }

        /// <summary>
        /// Parse a single Parameter Block XML definition.
        /// </summary>
        /// <param name="reader"></param>
        private void ParseRefdTexture(XmlTextReader reader)
        {
            string name = reader.GetAttribute(sC_s_s_AttrName);
            string filename = reader.GetAttribute(sC_s_s_AttrFilename);
            string refnum = reader.GetAttribute(sC_s_s_AttrRefnum);

            sRefdTexture tex = new sRefdTexture
            {
                m_name = (name == null) ? String.Empty : name.Trim().ToLower(),
                m_filename = (filename == null) ? String.Empty : filename.Trim(),
                m_refNum = (string.IsNullOrWhiteSpace(refnum)) ? default(int) : int.Parse(refnum)
            };

            m_aRefdTextures.Add(tex);
        }

        /// <summary>
        /// Parse, completing the object's member data.
        /// </summary>
        /// <param name="navigator"></param>
        private void Parse(XPathNavigator navigator)
        {
            // Parse our attributes, there are particular attributes we look
            // for here, ignoring the rest.
            XPathNodeIterator objectAttrIt = navigator.Select(sC_s_xpath_XPathObjectDefAttrs);
            while (objectAttrIt.MoveNext())
            {
                if (typeof(String) != objectAttrIt.Current.ValueType)
                    continue;
                String value = (objectAttrIt.Current.TypedValue as String);
                int nameHash = objectAttrIt.Current.Name.GetHashCode();

                if (HashLookupTable[sC_s_s_ObjectName] == nameHash)
                    this.m_sName = value;
                else if (HashLookupTable[sC_s_s_ObjectGuid] == nameHash)
                    this.m_Guid = new Guid(value);
                else if (HashLookupTable[sC_s_s_ObjectMaterial] == nameHash)
                    this.m_MaterialGuid = new Guid(value);
                else if (HashLookupTable[sC_s_s_ObjectClass]== nameHash)
                    this.m_sClass = value.ToLower();
                else if (HashLookupTable[sC_s_s_ObjectSuperclass] == nameHash)
                    this.m_sSuperclass = value.ToLower();
                else if (HashLookupTable[sC_s_s_ObjectRefName] == nameHash)
                    this.m_sRefName = value;
                else if (HashLookupTable[sC_s_s_ObjectRefFile]== nameHash)
                    this.m_sRefFile = Path.GetFullPath(value);
                else if (HashLookupTable[sC_s_s_ObjectRef] == nameHash)
                    this.m_InternalRefGuid = new Guid(value);
            }

            // Parse Generic Object References
            List<Guid> refGuids = new List<Guid>();
            XPathNodeIterator refIt = navigator.Select(sC_s_xpath_XPathObjectRef);
            while (refIt.MoveNext())
            {
                if (typeof(String) != objectAttrIt.Current.ValueType)
                    continue;
                String value = (refIt.Current.TypedValue as String);

                refGuids.Add(new Guid(value));
            }
            this.m_GenericReferenceGuids = refGuids.ToArray();

            ParseTransform(navigator);
            ParseBoundingBox(navigator);
            ParseAttributes(navigator);
            ParsePlatformAttributes(navigator);
            ParseParamBlocks(navigator);
            ParseRefdTextures(navigator);
            ParseSubObjects(navigator);
            ParseChildren(navigator);
            ParseSceneLinks(navigator);
            ParsePolyCount(navigator);
            ParseAnimationAttributes(navigator);
        }

        /// <summary>
        /// Parse our transform XML data.
        /// </summary>
        /// <param name="navigator"></param>
        void ParseTransform(XPathNavigator navigator)
        {
            // ObjectTransform
            Vector3f objectPosition = null;
            Quaternionf objectRotation = s_defaultQuaternion; 
            Vector3f objectScale = s_zeroVector;
            XPathNodeIterator posIt = navigator.Select(sC_s_xpath_XPathObjectTransformPos);
            if (posIt.Count == 0)
            {
                objectPosition = new Vector3f();
            }
            else
            {
                while (posIt.MoveNext())
                {
                    objectPosition = new Vector3f(posIt.Current);
                }
            }
            
            XPathNodeIterator rotIt = navigator.Select(sC_s_xpath_XPathObjectTransformRot);
            while (rotIt.MoveNext())
            {
                objectRotation = new Quaternionf(rotIt.Current);
                objectRotation.Normalise();
            }
            this.m_mObjectTransform = new Matrix34f(objectRotation);
            this.m_mObjectTransform.Translation = objectPosition;

            // ObjectScale
            this.ObjectScale = new Vector3f(1.0f, 1.0f, 1.0f);
            XPathNodeIterator objScaleIt = navigator.Select(sC_s_xpath_XPathObjectTransformScale);
            while (objScaleIt.MoveNext())
            {
                this.ObjectScale = new Vector3f(objScaleIt.Current);
            }

            // NodeTransform
            Vector3f nodePosition = new Vector3f(objectPosition);
            Quaternionf nodeRotation = new Quaternionf(objectRotation);
            XPathNodeIterator nodePosIt = navigator.Select(sC_s_xpath_XPathNodeTransformPos);
            while (nodePosIt.MoveNext())
            {
                nodePosition = new Vector3f(nodePosIt.Current);
            }

            XPathNodeIterator nodeRotIt = navigator.Select(sC_s_xpath_XPathNodeTransformRot);
            while (nodeRotIt.MoveNext())
            {
                nodeRotation = new Quaternionf(nodeRotIt.Current);
                nodeRotation.Normalise();
            }
            this.m_mNodeTransform = new Matrix34f(nodeRotation);
            this.m_mNodeTransform.Translation = nodePosition;

            // NodeScale
            this.NodeScale = new Vector3f(1.0f, 1.0f, 1.0f);
            XPathNodeIterator nodeScaleIt = navigator.Select(sC_s_xpath_XPathNodeTransformScale);
            while (nodeScaleIt.MoveNext())
            {
                this.NodeScale = new Vector3f(nodeScaleIt.Current);
            }
        }

        /// <summary>
        /// Parse properties with Keyframes in animation Properties
        /// </summary>
        private void ParseKeyedProperties(XPathNodeIterator attrElem)
        {
            attrElem.Current.MoveToFirstChild();

            XPathNavigator attrNavigator = attrElem.Current;
            List<sKeyFrame> keys = new List<sKeyFrame>();
            m_aKeyedProperties.Add(attrNavigator.Name, keys);
            attrElem.Current.MoveToFirstChild();

            while (attrElem.Current.MoveToNext())
            {
                XPathNodeIterator attrIt = attrNavigator.Select(sC_s_xpath_XPathAnimKeyAttrs);
                int tickTime = 0, state = 0;
                while (attrIt.MoveNext())
                {
                    String value = attrIt.Current.TypedValue as String;
                    if (value == null)
                        continue;

                    int hashName = attrIt.Current.Name.GetHashCode();
                    if (HashLookupTable[sC_s_s_AttrValue] == hashName)
                        state = int.Parse(value);
                    else if (HashLookupTable[sC_s_s_AttrFrame] == hashName)
                        tickTime = int.Parse(value);
                }
                sKeyFrame item = new sKeyFrame();
                item.m_nTime = tickTime;
                item.m_nState = state;
                keys.Add(item);
            }
        }

        /// <summary>
        /// Parse animation attributes
        /// </summary>
        /// <param name="navigator"></param>
        private void ParseAnimationAttributes(XPathNavigator navigator)
        {
            // root object of attached skeleton
            XPathNodeIterator skelObj = navigator.Select(sC_s_xpath_XPathAnimSkeletonRootObj);
            while (skelObj.MoveNext())
            {
                String value = skelObj.Current.TypedValue as String;
                if (value != null)
                    SkeletonRootGuid = new Guid(value);
            }

            // Animation length
            XPathNodeIterator lengthObj = navigator.Select(sC_s_xpath_XPathAnimLength);
            while (lengthObj.MoveNext())
            {
                String value = lengthObj.Current.TypedValue as String;
                if (value != null)
                    this.m_nAnimLength = int.Parse(value);
            }

            // keys of the "active" property of the max object
            XPathNodeIterator attrElem = navigator.Select(sC_s_xpath_XPathAnimKeys);
            attrElem.Current.MoveToFirstChild();
            while (attrElem.MoveNext())
            {
                ParseKeyedProperties(attrElem);
            }
        }

        /// <summary>
        /// Parse our boundingbox XML data.
        /// </summary>
        /// <param name="navigator"></param>
        private void ParseBoundingBox(XPathNavigator navigator)
        {
            // Local-space bounding box
            RSG.Base.Math.BoundingBox3f localbbox = new RSG.Base.Math.BoundingBox3f();            
            XPathNodeIterator itLocalBBMin = navigator.Select(sC_s_xpath_XPathLocalBoundingBoxMin);
            while (itLocalBBMin.MoveNext())
                localbbox.Expand(new RSG.Base.Math.Vector3f(itLocalBBMin.Current));
            XPathNodeIterator itLocalBBMax = navigator.Select(sC_s_xpath_XPathLocalBoundingBoxMax);
            while (itLocalBBMax.MoveNext())
                localbbox.Expand(new RSG.Base.Math.Vector3f(itLocalBBMax.Current));
            
            if (!localbbox.IsEmpty)
                this.LocalBoundingBox = localbbox;

            // World-space bounding box
            RSG.Base.Math.BoundingBox3f worldbbox = new RSG.Base.Math.BoundingBox3f();
            XPathNodeIterator itWorldBBMin = navigator.Select(sC_s_xpath_XPathWorldBoundingBoxMin);
            while (itWorldBBMin.MoveNext())
                worldbbox.Expand(new RSG.Base.Math.Vector3f(itWorldBBMin.Current));
            XPathNodeIterator itWorldBBMax = navigator.Select(sC_s_xpath_XPathWorldBoundingBoxMax);
            while (itWorldBBMax.MoveNext())
                worldbbox.Expand(new RSG.Base.Math.Vector3f(itWorldBBMax.Current));
                        
            if (!worldbbox.IsEmpty)
                this.WorldBoundingBox = worldbbox;
        }

        /// <summary>
        /// Parse our attribute, property and parameter block XML data.
        /// </summary>
        /// <param name="navigator">XPath document navigator</param>
        private void ParseAttributes(XPathNavigator navigator)
        {
            XPathNodeIterator classIt = navigator.Select(sC_s_xpath_XPathAttrClass);
            while (classIt.MoveNext())
            {
                String value = classIt.Current.TypedValue as String;
                if (value != null)
                    m_sAttributeClass = value.ToLower();
            }

            XPathNodeIterator guidIt = navigator.Select(sC_s_xpath_XPathAttrGuid);
            while (guidIt.MoveNext())
            {
                String value = guidIt.Current.TypedValue as String;
                if (value != null)
                    m_sAttributeGuid = new Guid(value);
            }

            XPathNodeIterator attrIt = navigator.Select(sC_s_xpath_XPathAttr);
            while (attrIt.MoveNext())
            {
                XPathNavigator attrNavigator = attrIt.Current;
                ParseAttribute(attrNavigator, ref this.m_dAttributes);
            }

            XPathNodeIterator propIt = navigator.Select(sC_s_xpath_XPathProperty);
            while (propIt.MoveNext())
            {
                XPathNavigator propNavigator = propIt.Current;
                ParseAttribute(propNavigator, ref this.m_dProperties);
            }

            XPathNodeIterator custIt = navigator.Select(sC_s_xpath_XPathCustomAttr);
            while (custIt.MoveNext())
            {
                XPathNavigator custAttrNavigator = custIt.Current;
                ParseAttribute(custAttrNavigator, ref this.m_dCustomAttributes);
            }

            XPathNodeIterator userIt = navigator.Select(sC_s_xpath_XPathUserProperty);
            while (userIt.MoveNext())
            {
                XPathNavigator userAttrNavigator = userIt.Current;
                ParseAttribute(userAttrNavigator, ref this.m_dUserProperties);
            }
        }

        /// <summary>
        /// Parse our platform attributes.
        /// </summary>
        /// <param name="navigator">XPath document navigator</param>
        private void ParsePlatformAttributes(XPathNavigator navigator)
        {
            XPathNodeIterator platformAttrIter = navigator.Select(sC_s_xpath_XPathPlatformAttrPlatform);

            while (platformAttrIter.MoveNext())
            {
                XPathNavigator platformNavigator = platformAttrIter.Current;
                XPathNodeIterator platformNameIt = platformNavigator.Select(sC_s_xpath_XPathPlatformAttr);
                String platform = "";

                // Find the platform name
                while (platformNameIt.MoveNext())
                {
                    string platformName = platformNameIt.Current.TypedValue as String;

                    if (platformName != null)
                        platform = platformName;
                }

                // Parse the platform specific attributes.
                if (!String.IsNullOrEmpty(platform))
                {
                    XPathNodeIterator platformAttr = platformNavigator.Select(sC_s_xpath_XPathPlatformAttribute);
                    while (platformAttr.MoveNext())
                    {
                        XPathNavigator platformAttrNavigator = platformAttr.Current;
                        AttributeContainer attrContainer = new AttributeContainer();
                        ParseAttribute(platformAttrNavigator, ref attrContainer);

                        RSG.Platform.Platform platformEnum = RSG.Platform.PlatformUtils.PlatformFromString(platform);

                        this.m_dPlatformAttributes[platformEnum] = attrContainer;
                    }
                }
            }
        }

        /// <summary>
        /// Parse a ParamBlock definition.
        /// </summary>
        /// <param name="navigator"></param>
        private void ParseParamBlocks(XPathNavigator navigator)
        {
            XPathNodeIterator paramblockIt = navigator.Select(sC_s_xpath_XPathParamBlock);
            this.m_aParamBlocks = new AttributeContainer[paramblockIt.Count];
            while (paramblockIt.MoveNext())
            {
                AttributeContainer paramblock = ParseParamBlock(paramblockIt.Current);
                this.m_aParamBlocks[paramblockIt.CurrentPosition - 1] = paramblock;
            }
        }
        
        /// <summary>
        /// Parse the definition of the referenced textures.
        /// </summary>
        /// <param name="navigator"></param>
        private void ParseRefdTextures(XPathNavigator navigator)
        {
            XPathNodeIterator reftexIt = navigator.Select(sC_s_xpath_XPathRefdTextures);
            this.m_aRefdTextures = new List<sRefdTexture>(reftexIt.Count);
            while (reftexIt.MoveNext())
            {
                ParseRefdTexture(reftexIt.Current);
            }
        }

        /// <summary>
        /// Parse a single attribute, placing it into the appropriate container.
        /// </summary>
        /// <param name="navigator"></param>
        private void ParseRefdTexture(XPathNavigator navigator)
        {
            // Parse our attribute's XML attributes, there are particular 
            // attributes we look for here, ignoring the rest.
            XPathNodeIterator attrIt = navigator.Select(sC_s_xpath_XPathObjectDefAttrs);
            sRefdTexture tex = new sRefdTexture();
            while (attrIt.MoveNext())
            {
                String value = attrIt.Current.TypedValue as String;
                if (value == null)
                    continue;
                int nameHash = attrIt.Current.Name.GetHashCode();

                if (HashLookupTable[sC_s_s_AttrName] == nameHash)
                    tex.m_name = value.ToLower();
                else if (HashLookupTable[sC_s_s_AttrRefnum] == nameHash)
                    tex.m_refNum = Int32.Parse(value);
                else if (HashLookupTable[sC_s_s_AttrFilename] == nameHash)
                    tex.m_filename = value;
            }
            this.m_aRefdTextures.Add(tex);
        }

        /// <summary>
        /// Parses a single attribute bool.
        /// </summary>
        /// <param name="navigator"></param>
        /// <param name="name"></param>
        /// <param name="valuestr"></param>
        /// <param name="container"></param>
        static void ParseAttributeBool(XPathNavigator navigator, string name, string valuestr, ref AttributeContainer container)
        {
            container[name] = bool.Parse(valuestr);
        }

        /// <summary>
        /// Parses an integer attribute.
        /// </summary>
        /// <param name="navigator"></param>
        /// <param name="name"></param>
        /// <param name="valuestr"></param>
        /// <param name="container"></param>
        static void ParseAttributeInt(XPathNavigator navigator, string name, string valuestr, ref AttributeContainer container)
        {
            container[name] = int.Parse(valuestr);
        }

        /// <summary>
        /// Parses a float attribute.
        /// </summary>
        /// <param name="navigator"></param>
        /// <param name="name"></param>
        /// <param name="valuestr"></param>
        /// <param name="container"></param>
        static void ParseAttributeFloat(XPathNavigator navigator, string name, string valuestr, ref AttributeContainer container)
        {
            container[name] = float.Parse(valuestr);
        }

        /// <summary>
        /// Parses a GUID.
        /// </summary>
        /// <param name="navigator"></param>
        /// <param name="name"></param>
        /// <param name="valuestr"></param>
        /// <param name="container"></param>
        static void ParseAttributeGuid(XPathNavigator navigator, string name, string valuestr, ref AttributeContainer container)
        {
            container[name] = new Guid(valuestr);
        }

        /// <summary>
        /// Parses an RGB color.
        /// </summary>
        /// <param name="navigator"></param>
        /// <param name="name"></param>
        /// <param name="valuestr"></param>
        /// <param name="container"></param>
        static void ParseAttributeRGB(XPathNavigator navigator, string name, string valuestr, ref AttributeContainer container)
        {
            XmlNode node = ((IHasXmlNode)navigator).GetNode().FirstChild;
            container[name] = new Colour3f(node);
        }

        /// <summary>
        /// Parses a RGBA color.
        /// </summary>
        /// <param name="navigator"></param>
        /// <param name="name"></param>
        /// <param name="valuestr"></param>
        /// <param name="container"></param>
        static void ParseAttributeRGBA(XPathNavigator navigator, string name, string valuestr, ref AttributeContainer container)
        {
            XmlNode node = ((IHasXmlNode)navigator).GetNode().FirstChild;
            container[name] = new Vector4f(node);
        }

        /// <summary>
        /// Parses a Vector3 type.
        /// </summary>
        /// <param name="navigator"></param>
        /// <param name="name"></param>
        /// <param name="valuestr"></param>
        /// <param name="container"></param>
        static void ParseAttributeVector3(XPathNavigator navigator, string name, string valuestr, ref AttributeContainer container)
        {
            XmlNode node = ((IHasXmlNode)navigator).GetNode().FirstChild;
            if (node.Name == "colour")
                container[name] = new Colour3f(node);
            else
                container[name] = new Vector3f(node);
        }

        /// <summary>
        /// Parses a Vector4 type.
        /// </summary>
        /// <param name="navigator"></param>
        /// <param name="name"></param>
        /// <param name="valuestr"></param>
        /// <param name="container"></param>
        static void ParseAttributeVector4(XPathNavigator navigator, string name, string valuestr, ref AttributeContainer container)
        {
            XmlNode node = ((IHasXmlNode)navigator).GetNode().FirstChild;
            container[name] = new Vector4f(node);
        }

        // temporary Dictionary as buffer to read arrayContainer, to avoid creating new temporary collection
        // before returning the final values
        AttributeContainer m_arrayContainerNavigator = new AttributeContainer();

        /// <summary>
        /// Parse a single attribute, placing it into the appropriate container.
        /// </summary>
        /// <param name="navigator"></param>
        /// <param name="container"></param>
        private void ParseAttribute(XPathNavigator navigator, ref AttributeContainer container)
        {
            // Parse our attribute's XML attributes, there are particular 
            // attributes we look for here, ignoring the rest.
            XPathNodeIterator attributesIterator = navigator.Select(sC_s_xpath_XPathObjectDefAttrs);
            String name = "", type = "", valuestr = "";
            while (attributesIterator.MoveNext())
            {
                string value = attributesIterator.Current.TypedValue as string;
                if (value == null)
                    continue;

                int nameHash = attributesIterator.Current.Name.GetHashCode();

                if (HashLookupTable[sC_s_s_AttrName] == nameHash)
                    name = value.ToLower();
                else if (HashLookupTable[sC_s_s_AttrType] == nameHash)
                    type = value.ToLower();
                else if (HashLookupTable[sC_s_s_AttrValue] == nameHash)
                    valuestr = value;
            }

            name = name.Trim();
            type = type.Trim();
            int typeHash = type.GetHashCode();
            valuestr = valuestr.Trim();

            if (ParseAttributeCallbacks.ContainsKey(typeHash))
            {
                ParseAttributeCallbacks[typeHash](navigator, name, valuestr, ref container);
            }
            else if (HashLookupTable[sC_s_s_AttrTypeArray] == typeHash)
            {
                XPathNodeIterator attrElem = navigator.Select(sC_s_xpath_XPathParamBlockParam);
                m_arrayContainerNavigator.Clear();
                while (attrElem.MoveNext())
                {
                    ParseAttribute(attrElem.Current, ref m_arrayContainerNavigator);
                }

                container[name] = m_arrayContainerNavigator.Values.ToArray();
            }
            else
            {
                int nameHash = name.GetHashCode();
                if (HashLookupTable[sC_s_s_ObjectRefName] == nameHash)
                    m_sRefName = valuestr;
                else if (HashLookupTable[sC_s_s_ObjectRefName2] == nameHash)
                    m_sRefName = valuestr;
                else if (HashLookupTable[sC_s_s_ObjectRefFile2] == nameHash)
                    m_sRefFile = valuestr;
                container[name] = valuestr;
            }
        }

        /// <summary>
        /// Parse a single Parameter Block XML definition.
        /// </summary>
        /// <param name="navigator"></param>
        private AttributeContainer ParseParamBlock(XPathNavigator navigator)
        {
            AttributeContainer container = new AttributeContainer();
            XPathNodeIterator paramIt = navigator.Select(sC_s_xpath_XPathParamBlockParam);
            while (paramIt.MoveNext())
            {
                ParseAttribute(paramIt.Current, ref container);
            }
            return (container);
        }

        /// <summary>
        /// Parse our sub-objects.
        /// </summary>
        /// <param name="navigator">XPath document navigator</param>
        private void ParseSubObjects(XPathNavigator navigator)
        {
            XPathNodeIterator subobjIt = navigator.Select(sC_s_xpath_XPathSubObject);
            this.m_aSubObjects = new ObjectDef[subobjIt.Count];
            while (subobjIt.MoveNext())
            {
                XPathNavigator subobjNavigator = subobjIt.Current;
                this.m_aSubObjects[subobjIt.CurrentPosition - 1] = new ObjectDef(subobjNavigator, m_oScene);
            }
        }

        /// <summary>
        /// Parse our object's children.
        /// </summary>
        /// <param name="navigator">XPath document navigator</param>
        private void ParseChildren(XPathNavigator navigator)
        {
            // Parse our root Object Definitions, these take care of our
            // child objects.
            XPathNodeIterator objectDefIt = navigator.Select(sC_s_xpath_XPathChildObjectDefs);
            this.m_aChildren = new ObjectDef[objectDefIt.Count];
            while (objectDefIt.MoveNext())
            {
                XPathNavigator objectsNavigator = objectDefIt.Current;
                ObjectDef child = new ObjectDef(objectsNavigator, m_oScene);
                this.m_aChildren[objectDefIt.CurrentPosition - 1] = child;
                child.Parent = this;
            }
        }

        private void ParseSceneLinks(XPathNavigator navigator)
        {
            XPathNodeIterator linkChannelIt = navigator.Select(sC_s_xpath_XPathLinkChannel);
            while (linkChannelIt.MoveNext())
            {
                LODHierarchyDef hierDef = new LODHierarchyDef(linkChannelIt.Current);
                Enum value = EnumExtensions.GetValByDisplayName<LinkChannel>(hierDef.Name);
                if (value!=null)
                    this.m_SceneLinks.Add((LinkChannel)value, hierDef);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="navigator"></param>
        private void ParsePolyCount(XPathNavigator navigator)
        {
            // Polycount statistic.
            XPathNodeIterator polyIt = navigator.Select(sC_s_xpath_XPathPolycount);
            while (polyIt.MoveNext())
            {
                String value = polyIt.Current.TypedValue as String;
                if ( value != null )
                    m_nPolyCount = int.Parse(value);
            }
        }

        /// <summary>
        /// Returns the number of active LOD levels below this object
        /// </summary>
        /// <returns></returns>
        private int CountLODLevelsBelowMe()
        {
            int result = 0;

            List<ObjectDef> lodChildren = GetAllLODChildren(new ObjectDef[] { this });

            while (lodChildren.Count > 0)
            {
                ++result;
                lodChildren = GetAllLODChildren(lodChildren);
            }

            return result;
        }

        private List<ObjectDef> GetAllLODChildren(IEnumerable<ObjectDef> lodObjects)
        {
            List<ObjectDef> lodChildren = new List<ObjectDef>();
            foreach (ObjectDef lodObject in lodObjects)
            {
                if (lodObject.HasLODChildren())
                {
                    foreach (TargetObjectDef child in lodObject.LOD.Children)
                        lodChildren.Add(child.ObjectDef);
                }
            }

            return lodChildren;
        }
        #endregion // Private Methods
    }

} // RSG.SceneXml namespace

// ObjectDef.cs
