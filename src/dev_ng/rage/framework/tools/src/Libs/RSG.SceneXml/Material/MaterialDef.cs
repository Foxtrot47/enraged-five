﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.XPath;

using RSG.Base.Logging;

namespace RSG.SceneXml.Material
{
    public class MaterialDef
    {
        #region Constants
        #region MultiMaterial Attribute Names
        private static readonly String sC_s_s_MaterialName = "name";
        private static readonly String sC_s_s_MaterialType = "type";
        private static readonly String sC_s_s_MaterialGuid = "guid";
        private static readonly String sC_s_s_MaterialPreset = "preset";
        #endregion // MultiMaterial Attribute Names
        #region XPath Compiled Expressions
        private static readonly XPathExpression sC_s_xpath_XPathMaterialDefAttrs = XPathExpression.Compile("@*");
        private static readonly XPathExpression sC_s_xpath_XPathSubMaterialDefs = XPathExpression.Compile("submaterials/material");
        private static readonly XPathExpression sC_s_xpath_XPathTexturesDef = XPathExpression.Compile("textures/texture");
        #endregion // XPath Compiled Expressions
        #endregion // Constants

        #region Properties and Associated Member Data

        /// <summary>
        /// Material name.
        /// </summary>
        public String Name
        {
            get { return m_sName; }
        }
        private String m_sName;

        /// <summary>
        /// Material GUID (unique identifier).
        /// </summary>
        public Guid Guid
        {
            get { return m_Guid; }
            set { m_Guid = value; }
        }
        private Guid m_Guid;

        /// <summary>
        /// My scene object
        /// </summary>
        public Scene MyScene
        {
            get { return m_oScene; }
        }
        private Scene m_oScene;

        /// <summary>
        /// The materials preset if one is present
        /// </summary>
        public String Preset
        {
            get { return m_sPreset; }
        }
        public bool HasPresetString
        {
            get { return ( m_sPreset != null && m_sPreset != String.Empty ); }
        }
        private String m_sPreset;

        public MaterialTypes MaterialType
        {
            get { return m_Type; }
        }
        private MaterialTypes m_Type;

        /// <summary>
        /// Parent material if applicible
        /// </summary>
        public MaterialDef Parent
        {
            get { return m_Parent; }
        }
        public bool HasParent
        {
            get { return m_Parent != null; }
        }
        private MaterialDef m_Parent;

        /// <summary>
        /// An array of any submaterials for this material
        /// </summary>
        public MaterialDef[] SubMaterials
        {
            get { return m_aSubMaterials; }
        }
        public int  SubMaterialsCount
        {
            get { return m_aSubMaterials == null ? 0 : m_aSubMaterials.Length; }
        }
        public bool HasSubMaterials
        {
            get { return m_aSubMaterials != null; }
        }
        private MaterialDef[] m_aSubMaterials;

        /// <summary>
        /// An array of any textures for this material
        /// </summary>
        public TextureDef[] Textures
        {
            get { return m_aTextures; }
        }
        public int TextureCount
        {
            get { return m_aTextures == null ? 0 : m_aTextures.Length; }
        }
        public bool HasTextures
        {
            get { return m_aTextures != null; }
        }
        private TextureDef[] m_aTextures;

        #endregion // Properties and Associated Member Data

        #region Public Methods
        /// <summary>
        /// Get every texture pathname used by any part of this MaterialDef instance
        /// </summary>
        /// <returns></returns>
        public IEnumerable<String> GetTextureBasenamesRecursive()
        {
            List<String> texturePathnames = new List<String>();

            GetTextureBasenamesRecursive(texturePathnames);

            return texturePathnames;
        }

        /// <summary>
        /// Return all texture filenames used by any part of this MaterialDef instance.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<String> GetTextureFilenamesRecursive()
        {
            List<String> textureFilenames = new List<String>();
            GetTextureFilenamesRecursive(textureFilenames);

            return (textureFilenames);
        }
        #endregion // Public Methods

        #region Constructor(s)
        /// <summary>
        /// Constructor, from XPathNavigator to create a parent material
        /// </summary>
        public MaterialDef(XPathNavigator navigator, Scene scene)
        {
            try
            {
                this.m_oScene = scene;
                this.m_Parent = null;
                Parse(navigator);
            }
            catch (Exception ex)
            {
                String message = String.Format("Invalid Material Definition: {0} {1}.", this.Name, navigator.InnerXml);
                SceneParseException sceneEx = new SceneParseException(message, ex);
                Log.Log__Exception(sceneEx, "Error parsing SceneXml.");
                throw sceneEx;
            }
        }
        /// <summary>
        /// Constructor, from XPathNavigator to create a child material
        /// </summary>
        public MaterialDef(XPathNavigator navigator, MaterialDef parent)
        {
            try
            {
                this.m_oScene = parent.MyScene;
                this.m_Parent = parent;
                Parse(navigator);
            }
            catch (Exception ex)
            {
                String message = String.Format("Invalid Material Definition: {0} {1}.", this.Name, navigator.InnerXml);
                SceneParseException sceneEx = new SceneParseException(message, ex);
                Log.Log__Exception(sceneEx, "Error parsing SceneXml.");
                throw sceneEx;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="scene"></param>
        public MaterialDef(XmlTextReader reader, Scene scene)
        {
            this.m_oScene = scene;
            this.m_Parent = null;
            Parse(reader);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="scene"></param>
        public MaterialDef(XmlTextReader reader, MaterialDef parent)
        {
            this.m_oScene = parent.MyScene;
            this.m_Parent = parent;
            Parse(reader);
        }

        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        private void ParseSkip(XmlTextReader reader)
        {
            if (reader.IsEmptyElement)
                reader.ReadStartElement();
            else
            {
                reader.ReadStartElement();
                while (reader.MoveToContent() == XmlNodeType.Element)
                {
                    ParseSkip(reader);
                }
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="navigator"></param>
        private void Parse(XmlTextReader reader)
        {
            if (reader.IsEmptyElement)
                ParseAttributes(reader);
            else
            {
                ParseAttributes(reader);
                ParseMembers(reader);
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="reader"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        private String GetAttribute(String name, XmlTextReader reader, String defaultValue)
        {
            String attribute = reader.GetAttribute(name);
            if (String.IsNullOrWhiteSpace(attribute) || String.IsNullOrEmpty(attribute))
            {
                attribute = defaultValue;
            }
            return attribute;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="reader"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        private Guid GetAttributeAsGuid(String name, XmlTextReader reader, Guid defaultValue)
        {
            Guid guid = defaultValue;
            String attribute = reader.GetAttribute(name);
            if (!String.IsNullOrWhiteSpace(attribute) && !String.IsNullOrEmpty(attribute))
            {
                guid = new Guid(attribute);
            }
            return guid;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        private void ParseAttributes(XmlTextReader reader)
        {
            this.m_sName = reader.GetAttribute(sC_s_s_MaterialName);
            this.m_Guid = this.GetAttributeAsGuid(sC_s_s_MaterialGuid, reader, default(Guid));
            this.m_sPreset = reader.GetAttribute(sC_s_s_MaterialPreset);

            String type = this.GetAttribute(sC_s_s_MaterialType, reader, null);
            if (!String.IsNullOrEmpty(type) && !String.IsNullOrWhiteSpace(type))
            {
                switch (type)
                {
                    case "Rage Shader":
                        this.m_Type = MaterialTypes.Rage;
                        break;
                    case "Standard":
                        this.m_Type = MaterialTypes.Standard;
                        break;
                    case "Multi/Sub-Object":
                        this.m_Type = MaterialTypes.MultiMaterial;
                        break;
                    default:
                        this.m_Type = MaterialTypes.None;
                        break;
                }
            }

            reader.ReadStartElement();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="navigator"></param>
        private void ParseMembers(XmlTextReader reader)
        {
            while (reader.MoveToContent() == XmlNodeType.Element)
            {
                if (String.Equals(reader.Name, "submaterials"))
                {
                    this.ParseSubMaterials(reader);
                }
                else if (String.Equals(reader.Name, "textures"))
                {
                    this.ParseTextures(reader);
                }
                else
                {
                    this.ParseSkip(reader);
                }
            }
            if (reader.NodeType == XmlNodeType.Text)
                reader.ReadString();
            if (reader.NodeType == XmlNodeType.EndElement)
                reader.ReadEndElement();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="navigator"></param>
        private void ParseSubMaterials(XmlTextReader reader)
        {
            if (reader.IsEmptyElement)
                reader.ReadStartElement();
            else
            {
                reader.ReadStartElement();
                var subMaterials = new List<MaterialDef>();
                while (reader.MoveToContent() == XmlNodeType.Element)
                {
                    if (String.Equals(reader.Name, "material"))
                    {
                        MaterialDef rootMaterial = new MaterialDef(reader, this);
                        subMaterials.Add(rootMaterial);
                    }
                    else
                    {
                        this.ParseSkip(reader);
                    }
                }
                if (subMaterials.Count > 0)
                {
                    this.m_aSubMaterials = subMaterials.ToArray();
                }
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="navigator"></param>
        private void ParseTextures(XmlTextReader reader)
        {
            if (reader.IsEmptyElement)
                reader.ReadStartElement();
            else
            {
                reader.ReadStartElement();
                var textures = new List<TextureDef>();
                while (reader.MoveToContent() == XmlNodeType.Element)
                {
                    if (String.Equals(reader.Name, "texture"))
                    {
                        var texture = new TextureDef(reader, this);
                        textures.Add(texture);
                    }
                    else
                    {
                        this.ParseSkip(reader);
                    }
                }
                if (textures.Count > 0)
                {
                    this.m_aTextures = textures.ToArray();
                }
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }

        private void Parse(XPathNavigator navigator)
        {
            // Parse our attributes, there are particular attributes we look
            // for here, ignoring the rest.
            XPathNodeIterator materialAttrIt = navigator.Select(sC_s_xpath_XPathMaterialDefAttrs);
            while (materialAttrIt.MoveNext())
            {
                if (typeof(String) != materialAttrIt.Current.ValueType)
                    continue;
                String value = (materialAttrIt.Current.TypedValue as String);

                if ( sC_s_s_MaterialName == materialAttrIt.Current.Name )
                    this.m_sName = value;
                if ( sC_s_s_MaterialGuid == materialAttrIt.Current.Name )
                    this.m_Guid = new Guid(value);
                if (sC_s_s_MaterialType == materialAttrIt.Current.Name)
                    switch (value)
                    {
                        case "Rage Shader":
                            this.m_Type = MaterialTypes.Rage;
                            break;
                        case "Standard":
                            this.m_Type = MaterialTypes.Standard;
                            break;
                        case "Multi/Sub-Object":
                            this.m_Type = MaterialTypes.MultiMaterial;
                            break;
                        default:
                            this.m_Type = MaterialTypes.None;
                            break;
                    }
                if (sC_s_s_MaterialPreset == materialAttrIt.Current.Name)
                    this.m_sPreset = value;
            }

            ParseTextures(navigator);
            ParseSubMaterials(navigator);
        }

        private void ParseSubMaterials(XPathNavigator navigator)
        {
            XPathNodeIterator materialDefIt = navigator.Select(sC_s_xpath_XPathSubMaterialDefs);
            if (materialDefIt.Count > 0)
            {
                this.m_aSubMaterials = new MaterialDef[materialDefIt.Count];
                while (materialDefIt.MoveNext())
                {
                    XPathNavigator materialNavigator = materialDefIt.Current;
                    Material.MaterialDef rootobject = new Material.MaterialDef(materialNavigator, this);
                    m_aSubMaterials[materialDefIt.CurrentPosition - 1] = rootobject;
                }
            }
        }

        private void ParseTextures(XPathNavigator navigator)
        {
            XPathNodeIterator textureDefIt = navigator.Select(sC_s_xpath_XPathTexturesDef);
            if (textureDefIt.Count > 0)
            {
                this.m_aTextures = new TextureDef[textureDefIt.Count];
                while (textureDefIt.MoveNext())
                {
                    XPathNavigator textureNavigator = textureDefIt.Current;
                    TextureDef rootobject = new Material.TextureDef(textureNavigator, this);
                    m_aTextures[textureDefIt.CurrentPosition - 1] = rootobject;
                }
            }
        }

        private void GetTextureBasenamesRecursive(ICollection<String> textureBasenames)
        {
            if (this.MaterialType == MaterialTypes.MultiMaterial)
            {
                if (this.SubMaterials != null)// stupid model - should use null object pattern here.  MS have provided empty arrays for this purpose.
                {
                    foreach (MaterialDef subMaterial in this.SubMaterials)
                        subMaterial.GetTextureBasenamesRecursive(textureBasenames);
                }
            }
            else
            {
                if (this.HasTextures)// stupid model - should use null object pattern here.  MS have provided empty arrays for this purpose.
                {
                    foreach (TextureDef texture in this.Textures)
                        textureBasenames.Add(System.IO.Path.GetFileNameWithoutExtension(texture.FilePath));
                }
            }
        }

        private void GetTextureFilenamesRecursive(ICollection<String> textureFilenames)
        {
            if (this.MaterialType == MaterialTypes.MultiMaterial)
            {
                if (this.SubMaterials != null)// stupid model - should use null object pattern here.  MS have provided empty arrays for this purpose.
                {
                    foreach (MaterialDef subMaterial in this.SubMaterials)
                        subMaterial.GetTextureFilenamesRecursive(textureFilenames);
                }
            }
            else
            {
                if (this.HasTextures)// stupid model - should use null object pattern here.  MS have provided empty arrays for this purpose.
                {
                    foreach (TextureDef texture in this.Textures)
                        textureFilenames.Add(texture.FilePath);
                }
            }
        }
        #endregion // Private Methods
    }
}
