﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using RSG.Base.Configuration;

using RSG.Pipeline.PBR;
using RSG.Pipeline.PBR.Documents;
using RSG.Interop.Adobe.Photoshop;

namespace RSG.Pipeline.PBR.Outputs
{
    public class StandardMaterialMapB : BaseMapOutput
    {
        #region Member Data

        private IConfig _config = ConfigFactory.CreateConfig();
        private StandardLayerGroup _layerGroup;

        #endregion // Member Data

        #region Properties

        public override String Name
        {
            get { return TextureNamingConvention.PBRMaterialMapB.Name; }
        }

        public override String LayerGroupPath
        {
            get { return _layerGroup.Material; }
        }

        public override NamingConventionDescription NamingConvention
        {
            get { return TextureNamingConvention.PBRMaterialMapB; }
        }

        #endregion //Properties

        #region Constructor(s)

        public StandardMaterialMapB()
        {
            _layerGroup = new StandardLayerGroup();
        }

        #endregion // Constructor(s)

        #region Methods

        public override String Export(Document doc, ExportOptions options)
        {
            if (RSG.Pipeline.PBR.Photoshop.Application.IsConnected())
            {
                IMasterMaterialDocument matDoc = MasterMaterialDocumentFactory.FromPhotoshopDocument(doc);

                // Hide all layers sitting outside of a layer group (at root level).
                foreach (ArtLayer layer in doc.ArtLayers)
                {
                    layer.Visible = false;
                }

                // Export maps to be packed.
                String heightMap = RSG.Pipeline.PBR.Export.ExportHeight(matDoc);
                String translucencyMap = RSG.Pipeline.PBR.Export.ExportTranslucency(matDoc);
                String detailMapIndex = RSG.Pipeline.PBR.Export.ExportDetailMapID(matDoc);
                String detailMapIntensity = RSG.Pipeline.PBR.Export.ExportDetailMapIntensity(matDoc);

                String basename = TextureNamingConvention.PBRMaterialMapB.Transform(options.Name);
                return RSG.Pipeline.PBR.Photoshop.PackMap(doc, options, basename, detailMapIntensity, detailMapIndex, heightMap, translucencyMap );
            }

            return null;
        }

        #endregion // Methods
    }
}
