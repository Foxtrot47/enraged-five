﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.ComponentModel;

namespace RSG.Pipeline.PBR
{
    /// <summary>
    /// Master material document type enumeration.
    /// </summary>
    public enum MasterMaterialDocumentType
    {
        /// <summary>
        /// Photoshop document doesn't have a master material document type; default.
        /// </summary>
        None = 0,

        /// <summary>
        /// Standard master material document.
        /// </summary>
        [Description("Standard")]
        Standard = 1,

        /// <summary>
        /// Vegetation master material document.
        /// </summary>
        [Description("Vegetation")]
        Vegetation = 2,

        /// <summary>
        /// Character Cloth master material document.
        /// </summary>
        [Description("Character Cloth")]
        CharacterCloth = 3,

        /// <summary>
        /// Character Skin master material document.
        /// </summary>
        [Description("Character Skin")]
        CharacterSkin = 4,

        [Description("GTA V Liberty City")]
        GTAVLibertyCity = 5
    }

    public static class MasterMaterialDocumentTypeExtensions
    {
        public static string GetDescription(this MasterMaterialDocumentType value)
        {
            Type type = value.GetType();
            string name = Enum.GetName(type, value);
            if (name != null)
            {
                FieldInfo field = type.GetField(name);
                if (field != null)
                {
                    DescriptionAttribute attr =
                           Attribute.GetCustomAttribute(field,
                             typeof(DescriptionAttribute)) as DescriptionAttribute;
                    if (attr != null)
                    {
                        return attr.Description;
                    }
                }
            }
            return null;
        }
    }
}
