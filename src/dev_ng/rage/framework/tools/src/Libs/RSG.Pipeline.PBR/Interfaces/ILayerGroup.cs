﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.PBR
{
    public interface ILayerGroup
    {
        #region Properties

        String Albedo { get; }
        String Alpha { get; }
        String AmbientOcclusion { get; }
        String Emissive { get; }
        String DetailMaps { get; }
        String DetailMapsID { get; }
        String DetailMapsIntensity { get; }
        String Height { get; }
        String Material { get; }
        String Miscellaneous { get; }
        String Modifiers { get; }
        String Normal { get; }
        String RoughnessModifier { get; }
        String RougherModifier { get; }
        String SmootherModifier { get; }
        String Translucency { get; }
        String Temp { get; }

        // Non-PBR (For GTA V Liberty City)
        String Diffuse { get; }
        String Specular { get; }

        #endregion // Properties

        #region Methods

        void Create(IMasterMaterialDocument doc);

        #endregion // Methods
    }
}
