﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

using RSG.Base.Configuration;

using RSG.Pipeline.PBR;
using RSG.Pipeline.PBR.Documents;
using RSG.Interop.Adobe.Photoshop;

namespace RSG.Pipeline.PBR.Outputs
{
    /// <summary>
    /// Material Map A map output.
    /// </summary>
    public class StandardMaterialMapA : BaseMapOutput
    {
        #region Member Data

        private IConfig _config = ConfigFactory.CreateConfig();
        private StandardLayerGroup _layerGroup;

        #endregion // Member Data

        #region Properties

        public override String Name
        {
            get { return TextureNamingConvention.PBRMaterialMapA.Name; }
        }

        public override String LayerGroupPath
        {
            get { return this._layerGroup.Material; }
        }

        public override NamingConventionDescription NamingConvention
        {
            get { return TextureNamingConvention.PBRMaterialMapA; }
        }

        #endregion //Properties

        #region Constructor(s)

        public StandardMaterialMapA()
        {
            this._layerGroup = new StandardLayerGroup();
        }

        #endregion // Constructor(s)

        #region Methods

        public override String Export(Document doc, ExportOptions options)
        {
            if (RSG.Pipeline.PBR.Photoshop.Application.IsConnected())
            {
                IMasterMaterialDocument matDoc = MasterMaterialDocumentFactory.FromPhotoshopDocument(doc);
                
                // Hide all layers sitting outside of a layer group (at root level).
                foreach (ArtLayer layer in doc.ArtLayers)
                {
                    layer.Visible = false;
                }

                // Export maps to be packed.
                String metalMap = RSG.Pipeline.PBR.Export.ExportMetalness(matDoc);
                String sheenMap = RSG.Pipeline.PBR.Export.ExportSheen(matDoc);
                String aoMap = RSG.Pipeline.PBR.Export.ExportAmbientOcclusion(matDoc);
                String roughnessMap = RSG.Pipeline.PBR.Export.ExportRoughness(matDoc);

                // Pack the map.
                String basename = TextureNamingConvention.PBRMaterialMapA.Transform(options.Name);
                return RSG.Pipeline.PBR.Photoshop.PackMap(doc, options, basename, metalMap, roughnessMap, sheenMap, aoMap);
            }

            return null;
        }

        #endregion // Methods
    }
}
