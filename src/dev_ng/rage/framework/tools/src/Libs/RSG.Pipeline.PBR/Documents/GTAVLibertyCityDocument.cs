﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Interop.Adobe.Photoshop;

using RSG.Pipeline.PBR;
using RSG.Pipeline.PBR.Outputs;


namespace RSG.Pipeline.PBR.Documents
{
    /// <summary>
    /// Defines paths to layer groups in the master material document.
    /// </summary>
    public sealed class GTAVLiberyCityLayerGroup : BaseLayerGroup
    {
        #region Properties
        #endregion // Properties

        #region Constructor(s)

        public GTAVLiberyCityLayerGroup()
            : base()
        {
        }

        #endregion // Constructor(s)

        #region Methods

        public override void Create(IMasterMaterialDocument doc)
        {
            // Albedo
            RSG.Pipeline.PBR.Photoshop.Helpers.FindLayerGroupByPath(doc.Document, this.Diffuse);

            // Misc
            RSG.Pipeline.PBR.Photoshop.Helpers.FindLayerGroupByPath(doc.Document, this.Miscellaneous);
            RSG.Pipeline.PBR.Photoshop.Helpers.FindLayerGroupByPath(doc.Document, this.Normal);
            RSG.Pipeline.PBR.Photoshop.Helpers.FindLayerGroupByPath(doc.Document, this.Specular);
            RSG.Pipeline.PBR.Photoshop.Helpers.FindLayerGroupByPath(doc.Document, this.Alpha);
        }

        #endregion // Methods
    }

    /// <summary>
    /// Standard Master Material Document.
    /// </summary>
    class GTAVLibertyCityMasterMaterialDocument : BaseMasterMaterialDocument
    {
        #region Properties

        public override String AssignedDetailMapGroup
        {
            get { return "Environment"; }
        }

        #endregion // Properties

        #region Constructor(s)

        public GTAVLibertyCityMasterMaterialDocument(Document doc, bool createLayerGroups = false)
            : base(doc, createLayerGroups)
        {
        }

        public GTAVLibertyCityMasterMaterialDocument(int width, int height, int resolution = 72, NewDocumentMode mode = NewDocumentMode.NewRGB, bool createLayerGroups = false)
            : base(MasterMaterialDocumentType.GTAVLibertyCity, width, height, resolution, mode, createLayerGroups)
        {
        }

        #endregion // Constructor(s)

        #region Overridden Methods

        public override void Setup(bool createLayerGroups)
        {
            GTAVLiberyCityLayerGroup layerGroup = new GTAVLiberyCityLayerGroup();
            this.LayerGroup = layerGroup;

            if (createLayerGroups == true)
            {
                layerGroup.Create(this);
            }
        }

        public override string[] Export(ExportOptions options)
        {
            // Setup outputs based on the options.
            if (options.ExportAlbedo)
            {
                this.AddOutput(new StandardAlbedo());
            }

            if (options.ExportAlpha)
            {
                this.AddOutput(new StandardAlpha());
            }

            if (options.ExportNormal)
            {
                this.AddOutput(new StandardNormal());
            }

            if (options.ExportMaterialMapA)
            {
                this.AddOutput(new StandardMaterialMapA());
            }

            if (options.ExportMaterialMapB)
            {
                this.AddOutput(new StandardMaterialMapB());
            }

            if (options.ExportEmissive)
            {
                this.AddOutput(new StandardEmissive());
            }

            return base.Export(options);
        }

        #endregion // Overridden Methods
    }
}
