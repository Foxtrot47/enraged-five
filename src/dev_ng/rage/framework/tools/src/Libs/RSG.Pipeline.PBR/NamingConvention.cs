﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Threading.Tasks;

namespace RSG.Pipeline.PBR
{
    public sealed class NamingConventionDescription
    {
        #region Member Data

        private string _id;
        private string _name;
        private string _prefix;
        private string _suffix;

        #endregion // Member Data

        #region Constructor(s)

        public NamingConventionDescription(string id, string name, string prefix, string suffix)
        {
            _id = id;
            _name = name;
            _prefix = prefix;
            _suffix = suffix;
        }

        #endregion // Constructor(s)

        #region Properties

        public string Id
        {
            get { return _id; }
        }

        public string Name
        {
            get { return _name; }
        }

        public string Prefix
        {
            get { return _prefix; }
        }

        public string Suffix
        {
            get { return _suffix; }
        }

        #endregion // Properties

        #region Methods

        /// <summary>
        /// Transforms the supplied filename into one with the naming convention applied and returns it.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns>String of the transformed filename.</returns>
        public String Transform(String filename)
        {
            String dirname = System.IO.Path.GetDirectoryName(filename);
            String basename = System.IO.Path.GetFileNameWithoutExtension(filename);

            if (dirname != "")
            {
                return String.Format("{0}\\{1}{2}{3}", dirname, this._prefix, basename, this._suffix);
            }

            return String.Format("{0}{1}{2}", this._prefix, basename, this._suffix);
        }

        #endregion // Methods
    }

    public static class TextureNamingConvention
    {
        #region Constants

        private const String ID_ALBEDO               = "albedo";
        private const String ID_ALPHA                = "alpha";
        private const String ID_ALBEDO_ALPHA         = "albedoAlpha";
        private const String ID_NORMAL               = "normal";
        private const String ID_CHANNEL              = "channel";
        private const String ID_PBR_MAP_A            = "pbrMapA";
        private const String ID_PBR_MAP_B            = "pbrMapB";
        private const String ID_HEIGHT               = "height";
        private const String ID_ROUGHNESS            = "roughness";
        private const String ID_AMBIENT_OCCLUSION    = "ao";
        private const String ID_METALNESS            = "metalness";
        private const String ID_SHEEN                = "sheen";
        private const String ID_TRANSLUCENCY         = "translucency";
        private const String ID_EMISSIVE             = "emissive";
        
        #endregion // Constants

        #region Member Data

        private static NamingConventionDescription _albedo;
        private static NamingConventionDescription _alpha;
        private static NamingConventionDescription _albedoAlpha;
        private static NamingConventionDescription _normal;
        private static NamingConventionDescription _channel;
        private static NamingConventionDescription _pbrMaterialMapA;
        private static NamingConventionDescription _pbrMaterialMapB;
        private static NamingConventionDescription _height;
        private static NamingConventionDescription _roughness;
        private static NamingConventionDescription _ambientOcclusion;
        private static NamingConventionDescription _translucency;
        private static NamingConventionDescription _sheen;
        private static NamingConventionDescription _metalness;
        private static NamingConventionDescription _emissive;

        #endregion // Member Data

        #region Methods

        public static void LoadFromConfig(string configFilename)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(configFilename);

            XmlNodeList nodes = doc.GetElementsByTagName("TextureNamingConvention");

            foreach (XmlNode node in nodes)
            {
                string id = node.Attributes[0].Value;
                string name = node.Attributes[1].Value;
                string prefix = node.Attributes[2].Value;
                string suffix = node.Attributes[3].Value;

                NamingConventionDescription desc = new NamingConventionDescription(id, name, prefix, suffix);

                switch (id)
                {
                    case ID_ALBEDO: _albedo = desc; break;
                    case ID_ALPHA: _alpha = desc; break;
                    case ID_ALBEDO_ALPHA: _albedoAlpha = desc; break;
                    case ID_NORMAL: _normal = desc; break;
                    case ID_CHANNEL: _channel = desc; break;
                    case ID_PBR_MAP_A: _pbrMaterialMapA = desc; break;
                    case ID_PBR_MAP_B: _pbrMaterialMapB = desc; break;
                    case ID_HEIGHT: _height = desc; break;
                    case ID_ROUGHNESS: _roughness = desc; break;
                    case ID_AMBIENT_OCCLUSION: _ambientOcclusion = desc; break;
                    case ID_METALNESS: _metalness = desc; break;
                    case ID_SHEEN: _sheen = desc; break;
                    case ID_TRANSLUCENCY: _translucency = desc; break;
                    case ID_EMISSIVE: _emissive = desc; break;
                    default: break;
                }
            }
        }

        #endregion // Methods

        #region Properties

        public static NamingConventionDescription Albedo
        {
            get { return _albedo; }
        }

        public static NamingConventionDescription Alpha
        {
            get { return _alpha; }
        }

        public static NamingConventionDescription AlbedoAlpha
        {
            get { return _albedoAlpha; }
        }

        public static NamingConventionDescription Normal
        {
            get { return _normal; }
        }

        public static NamingConventionDescription Emissive
        {
            get { return _emissive; }
        }

        public static NamingConventionDescription Channel
        {
            get { return _channel; }
        }

        public static NamingConventionDescription PBRMaterialMapA
        {
            get { return _pbrMaterialMapA; }
        }

        public static NamingConventionDescription PBRMaterialMapB
        {
            get { return _pbrMaterialMapB; }
        }

        public static NamingConventionDescription Height
        {
            get { return _height; }
        }

        public static NamingConventionDescription Roughness
        {
            get { return _roughness; }
        }

        public static NamingConventionDescription AmbientOcclusion
        {
            get { return _ambientOcclusion; }
        }

        public static NamingConventionDescription Metalness
        {
            get { return _metalness; }
        }

        public static NamingConventionDescription Sheen
        {
            get { return _sheen; }
        }

        public static NamingConventionDescription Translucency
        {
            get { return _translucency; }
        }

        #endregion // Properties
    }
}
