﻿using System;
using System.IO;
using System.Diagnostics;
using System.Xml;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;

using RSG.Interop.Adobe.Photoshop;
using RSG.Base.Configuration;
using RSG.SourceControl.Perforce;

namespace RSG.Pipeline.PBR
{
    /// <summary>
    /// Represents a group of PBR surfaces.
    /// </summary>
    public class SurfaceGroup
    {
        #region Constants

        private string m_name;
        private Dictionary<string, Surface> m_surfaces = new Dictionary<string, Surface>();

        #endregion // Constants

        #region Constructor

        public SurfaceGroup(string name)
        {
            this.m_name = name;
        }

        #endregion // Constructor

        #region Properties

        public string Name
        {
            get { return this.m_name; }
        }

        public Dictionary<string, Surface> Surfaces
        {
            get { return this.m_surfaces; }
        }

        #endregion // Properties

        #region Methods

        public void AddSurface(Surface surface)
        {
            this.m_surfaces.Add(surface.Name, surface);
        }

        public Surface GetSurface(string surfaceName)
        {
            if (this.m_surfaces.ContainsKey(surfaceName))
            {
                return this.m_surfaces[surfaceName];
            }

            return null;
        }

        #endregion // Methods
    }

    /// <summary>
    /// Represents a PBR surface.
    /// </summary>
    public class Surface
    {
        #region Member Data

        private String m_name;
        private String m_fullName;
        private String m_group;
        private String m_modifierFullName;
        private Color m_maskColor;
        private Color m_albedoColor;
        private Color m_roughness;
        private Color m_metalness;
        private Color m_sheen;
        private double m_roughnessModLimit;
        private bool m_isMetal;
        private bool m_isSheen;
        private List<String> m_imageFilenames;
        private int m_currentImageIdx;

        private ArtLayer m_layer;
        private LayerSet m_roughnessModifierLayer;
        private LayerSet m_rootLayer;

        #endregion // Member Data

        #region Constructor(s)

        public Surface(String name, String group, Color maskColor, Color albedoColor, float roughness = 0.0f, float metalness = 0.0f, float sheen = 0.0f, float roughnessModLimit = 0.2f, List<String> imageFilenames = null, bool isMetal = false, bool isSheen = false)
        {
            this.m_name = name;
            this.m_group = group;
            this.m_fullName = group + "." + name;
            this.m_modifierFullName = String.Format("{0}{1}", SurfaceManager.ModifierPrefix, this.m_fullName);
            this.m_maskColor = maskColor;
            this.m_roughness = Color.FromArgb((int)(roughness * 255), (int)(roughness * 255), (int)(roughness * 255));
            this.m_metalness = Color.FromArgb((int)(metalness * 255), (int)(metalness * 255), (int)(metalness * 255));
            this.m_sheen = Color.FromArgb((int)(sheen * 255), (int)(sheen * 255), (int)(sheen * 255));
            this.m_albedoColor = albedoColor;
            this.m_roughnessModLimit = roughnessModLimit;
            this.m_isMetal = isMetal;
            this.m_isSheen = isSheen;
            this.m_imageFilenames = imageFilenames;
            this.m_currentImageIdx = 0;
        }

        #endregion // Constructor(s)

        #region Properties

        public ArtLayer Layer
        {
            get { return this.m_layer; }
            set { this.m_layer = value; }
        }

        public LayerSet RoughnessModifierLayer
        {
            get { return this.m_roughnessModifierLayer; }
            set { this.m_roughnessModifierLayer = value; }
        }

        public LayerSet RootLayer
        {
            get { return this.m_rootLayer; }
            set { this.m_rootLayer = value; }
        }

        public String Name
        {
            get { return this.m_name; }
        }

        public String FullName
        {
            get { return this.m_fullName; }
        }

        public String ModifierFullName
        {
            get { return this.m_modifierFullName; }
        }

        public String ImageFilename
        {
            get { return this.m_imageFilenames[this.m_currentImageIdx]; }
        }

        public List<String> ImageFilenames
        {
            get { return this.m_imageFilenames; }
        }

        public int CurrentImageIndex
        {
            get { return this.m_currentImageIdx; }
            set
            {
                this.m_currentImageIdx = value;

                if (this.m_currentImageIdx < 0)
                {
                    this.m_currentImageIdx = this.m_imageFilenames.Count - 1;
                }
                else if (this.m_currentImageIdx > (this.m_imageFilenames.Count - 1))
                {
                    this.m_currentImageIdx = 0;
                }
            }
        }

        public String Group
        {
            get { return this.m_group; }
        }

        public Color MaskColor
        {
            get { return this.m_maskColor; }
        }

        public Color AlbedoColor
        {
            get { return this.m_albedoColor; }
        }

        public Color Roughness
        {
            get { return this.m_roughness; }
        }

        public double RoughnessModifierLimit
        {
            get { return this.m_roughnessModLimit; }
        }

        public Color Metalness
        {
            get { return this.m_metalness; }
        }

        public Color Sheen
        {
            get { return this.m_sheen; }
        }

        public bool IsMetal
        {
            get { return this.m_isMetal; }
        }

        public bool IsSheen
        {
            get { return this.m_isSheen; }
        }

        #endregion // Properties
    }

    /// <summary>
    /// Manager class for Surface objects.
    /// </summary>
    public static class SurfaceManager
    {
        #region Member Data

        private static IConfig _config = ConfigFactory.CreateConfig();
        private static Dictionary<String, SurfaceGroup> _surfaceGroups = new Dictionary<String, SurfaceGroup>();

        private static String _materialsRoot = _config.ToolsRoot + "\\techart\\etc\\config\\material";
        private static String _imagesRoot = _materialsRoot + "\\images";
        private static String _defaultImage = _imagesRoot + "\\default.png";

        private static String _modfierPrefix = "[Modifier] ";

        // Key: Mask color string, represented as "r,g,b".
        // Value: Surface object.
        private static Dictionary<String, Surface> _allSurfaces = new Dictionary<String, Surface>();

        #endregion // Member Data

        #region Properties

        public static String ModifierPrefix
        {
            get { return _modfierPrefix; }
        }

        public static String MaterialsRoot
        {
            get { return _materialsRoot; }
        }

        public static String ImagesRoot
        {
            get { return _imagesRoot; }
        }

        public static Dictionary<String, SurfaceGroup> Groups
        {
            get { return _surfaceGroups; }
        }

        #endregion // Properties

        #region Methods

        public static void Load()
        {
            string materialSurfacesXml = _materialsRoot + "\\materialSurfaces.xml";

            _surfaceGroups.Clear();

            // Sync to latest revision of the material surfaces xml.
            if (RSG.Pipeline.PBR.Photoshop.UseSourceControl)
            {
                try
                {
                    RSG.SourceControl.Perforce.P4 p4 = new RSG.SourceControl.Perforce.P4();
                    p4.Connect();
                    p4.Run("sync", materialSurfacesXml);
                }
                catch { }
            }

            XmlDocument doc = new XmlDocument();
            doc.Load(materialSurfacesXml);

            XmlNodeList groupNodes = doc.GetElementsByTagName("Group");

            foreach (XmlNode groupNode in groupNodes)
            {
                string groupName = groupNode.Attributes[0].Value;
                SurfaceGroup surfaceGroup = new SurfaceGroup(groupName);

                foreach (XmlNode surfaceNode in groupNode.ChildNodes)
                {
                    String surfaceName = surfaceNode.Attributes["name"].Value;
                    String xmlRGB = surfaceNode.Attributes["maskRGB"].Value;
                    float roughness = float.Parse(surfaceNode.Attributes["roughness"].Value);
                    float metalness = float.Parse(surfaceNode.Attributes["metalness"].Value);
                    float sheen = float.Parse(surfaceNode.Attributes["sheen"].Value);
                    float roughnessModLimit = float.Parse(surfaceNode.Attributes["roughnessModLimit"].Value);

                    String[] rawRGB = xmlRGB.Split(',');
                    List<int> rgb = new List<int>();

                    foreach (String item in rawRGB)
                    {
                        rgb.Add(int.Parse(item));
                    }

                    Color maskRGB = Color.FromArgb(rgb[0], rgb[1], rgb[2]);
                    Color albedoRGB = Color.FromArgb(0, 0, 0, 0);

                    // Get the Albedo RGB color.
                    XmlNode albedoRgbNode = surfaceNode.Attributes.GetNamedItem("albedoRGB");

                    if (albedoRgbNode != null)
                    {
                        String xmlAlbedoRGB = albedoRgbNode.Value;

                        String[] rawAlbedoRGB = xmlAlbedoRGB.Split(',');
                        List<int> albedoRgb = new List<int>();

                        foreach (String item in rawAlbedoRGB)
                        {
                            albedoRgb.Add(int.Parse(item));
                        }

                        albedoRGB = Color.FromArgb(albedoRgb[0], albedoRgb[1], albedoRgb[2]);
                    }

                    // Get isMetal and isSheen flags.
                    XmlNode isMetalNode = surfaceNode.Attributes.GetNamedItem("isMetal");
                    XmlNode isSheenNode = surfaceNode.Attributes.GetNamedItem("isSheen");

                    bool isMetal = false;
                    bool isSheen = false;

                    if (isMetalNode != null)
                    {
                        isMetal = Convert.ToBoolean(isMetalNode.Value);
                    }

                    if (isSheenNode != null)
                    {
                        isSheen = Convert.ToBoolean(isSheenNode.Value);
                    }

                    // Get the image filename.
                    List<String> imageFilenames = new List<String>();

                    String imageFilename = _imagesRoot + "\\" + groupName + "." + surfaceName + ".png";

                    DirectoryInfo dirInfo = new DirectoryInfo(_imagesRoot);
                    String pattern = String.Format("{0}.{1}*.png", groupName, surfaceName);
                    FileInfo[] files = dirInfo.GetFiles(pattern);

                    if (files.Length == 0)
                    {
                        imageFilenames.Add(_defaultImage);
                    }
                    else
                    {
                        foreach (FileInfo file in files)
                        {
                            imageFilenames.Add(file.FullName);
                        }
                    }

                    Surface surface = new Surface(surfaceName, groupName, maskRGB, albedoRGB, roughness, metalness, sheen, roughnessModLimit, imageFilenames, isMetal, isSheen);

                    if (!_allSurfaces.ContainsKey(xmlRGB))
                    {
                        _allSurfaces.Add(xmlRGB, surface);
                        surfaceGroup.AddSurface(surface);
                    }
                    else
                    {
                        MessageBox.Show("The surface named \"" + surface.FullName + "\" is using a duplicated Mask RGB color!  Cannot recognize this surface until a unique Mask RGB color is defined.  This can be resolved by editing the following file:\n\n" + materialSurfacesXml, "Rockstar");
                    }
                }

                _surfaceGroups.Add(groupName, surfaceGroup);
            }
        }

        public static SurfaceGroup GetGroup(String groupName)
        {
            if (_surfaceGroups.ContainsKey(groupName))
            {
                return _surfaceGroups[groupName];
            }

            return null;
        }

        public static Surface GetSurface(String fullName)
        {
            if (fullName.Contains('.'))
            {
                String[] rawNames = fullName.Split('.');

                if (rawNames[0].StartsWith(_modfierPrefix))
                {
                    rawNames[0] = rawNames[0].Substring(_modfierPrefix.Length, rawNames[0].Length - _modfierPrefix.Length);
                }

                return GetSurface(rawNames[0], rawNames[1]);
            }

            return null;
        }

        public static Surface GetSurface(String groupName, String surfaceName)
        {
            if (_surfaceGroups.ContainsKey(groupName))
            {
                SurfaceGroup group = _surfaceGroups[groupName];

                if (group.Surfaces.ContainsKey(surfaceName))
                {
                    return group.Surfaces[surfaceName];
                }
            }

            return null;
        }

        public static Surface GetSurfaceByMaskColor(String maskRgb)
        {
            if (_allSurfaces.ContainsKey(maskRgb))
            {
                return _allSurfaces[maskRgb];
            }

            return null;
        }

        public static List<Surface> GetAllSurfaces()
        {
            List<Surface> surfaces = new List<Surface>();

            foreach (SurfaceGroup group in _surfaceGroups.Values)
            {
                foreach (Surface surface in group.Surfaces.Values)
                {
                    surfaces.Add(surface);
                }
            }

            return surfaces;
        }

        #endregion // Methods
    }
}
