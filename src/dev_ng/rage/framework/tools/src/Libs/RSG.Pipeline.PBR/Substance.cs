﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Interop.Adobe.Photoshop;

namespace RSG.Pipeline.PBR
{
    public static class Substance
    {
        public static void CreateAlbedoMap(IMasterMaterialDocument doc, float lightEqualizer, float highlightsCancel, float shadowsCancel, float diff2Albedo)
        {
            if (RSG.Pipeline.PBR.Photoshop.Application.IsConnected())
            {
                if (doc != null)
                {
                    string sbsDiffuseToAlbedoLayerName = "Substance Diffuse to Albedo";
                    string projCacheDir = Export.TempExportDirectory;

                    RSG.Pipeline.PBR.Photoshop.Helpers.SetLayerVisibilityByPath(doc.Document, doc.LayerGroup.Albedo, sbsDiffuseToAlbedoLayerName, false);
                    RSG.Pipeline.PBR.Photoshop.Helpers.SetLayerGroupVisibilityByPath(doc.Document, doc.LayerGroup.Albedo, true);

                    String outputFilename = TextureNamingConvention.Albedo.Transform(doc.Document.Name);
                    ExportOptions options = new ExportOptions();
                    options.Path = projCacheDir;

                    string albedoMap = Export.ExportAlbedo(doc, options);

                    if (albedoMap != "")
                    {
                        string sbsar = RSG.Interop.Substance.Path.Archives + "\\Diffuse_to_Albedo.sbsar";

                        RSG.SourceControl.Perforce.P4 p4 = new RSG.SourceControl.Perforce.P4();
                        p4.Connect();
                        p4.Run("sync", sbsar);

                        RSG.Interop.Substance.SbsWrapper sbs = new RSG.Interop.Substance.SbsWrapper(sbsar);
                        sbs.SetOutputName("diffuse_to_albedo");
                        sbs.SetInputImage("inputDiffuse", albedoMap);
                        sbs.SetInputParameter("Light_Equalizer", lightEqualizer.ToString());
                        sbs.SetInputParameter("Highlights_Cancel", highlightsCancel.ToString());
                        sbs.SetInputParameter("Shadows_Cancel", shadowsCancel.ToString());
                        sbs.SetInputParameter("Diffuse_2_Albedo", diff2Albedo.ToString());
                        sbs.SetOutputSize((int)doc.Document.Width, (int)doc.Document.Height);
                        List<string> result = sbs.Render(projCacheDir);

                        if (result.Count == 1)
                        {
                            string aoMap = result[0];
                            Document aoDoc = RSG.Pipeline.PBR.Photoshop.Application.Open(aoMap);
                            aoDoc.Selection.SelectAll();
                            aoDoc.Selection.Copy();
                            aoDoc.Close();

                            RSG.Pipeline.PBR.Photoshop.Application.ActiveDocument = doc.Document;

                            ArtLayer diffToAlbedoLayer = RSG.Pipeline.PBR.Photoshop.Helpers.FindLayerByPath(doc.Document, doc.LayerGroup.Albedo + "/" + sbsDiffuseToAlbedoLayerName);
                            LayerSet albedoGroup = RSG.Pipeline.PBR.Photoshop.Helpers.FindLayerGroupByPath(doc.Document, doc.LayerGroup.Albedo);

                            if (diffToAlbedoLayer == null)
                            {
                                if (albedoGroup == null)
                                {
                                    albedoGroup = doc.Document.LayerSets.Add();
                                    albedoGroup.Name = TextureNamingConvention.Albedo.Name;
                                }

                                diffToAlbedoLayer = albedoGroup.ArtLayers.Add();
                                diffToAlbedoLayer.Name = sbsDiffuseToAlbedoLayerName;
                            }

                            doc.DisplayAlbedo();

                            doc.Document.ActiveLayer = diffToAlbedoLayer;
                            diffToAlbedoLayer.Visible = true;

                            diffToAlbedoLayer.Clear();
                            ArtLayer pastedLayer = doc.Document.Paste();
                            pastedLayer.Name = sbsDiffuseToAlbedoLayerName;
                        }
                    }
                }
            }
        }

        public static void CreateAmbientOcclusionMap(IMasterMaterialDocument doc, float spreading)
        {
            if (RSG.Pipeline.PBR.Photoshop.Application.IsConnected())
            {
                if (doc != null)
                {
                    string projCacheDir = Export.TempExportDirectory;

                    String outputFilename = TextureNamingConvention.Height.Transform(doc.Document.Name);
                    ExportOptions options = new ExportOptions();
                    options.Path = projCacheDir;

                    string heightMap = Export.ExportHeight(doc);

                    if (heightMap != "")
                    {
                        string sbsar = RSG.Interop.Substance.Path.Archives + "\\Height_to_Ambient_Occlusion.sbsar";
                        RSG.Interop.Substance.SbsWrapper sbs = new RSG.Interop.Substance.SbsWrapper(sbsar);
                        sbs.SetOutputName("height_to_ambient_occlusion");
                        sbs.SetInputImage("inputHeight", heightMap);
                        sbs.SetInputParameter("spreading", spreading.ToString());
                        sbs.SetOutputSize((int)doc.Document.Width, (int)doc.Document.Height);
                        List<string> result = sbs.Render(projCacheDir);

                        if (result.Count == 1)
                        {
                            string aoMap = result[0];
                            Document aoDoc = RSG.Pipeline.PBR.Photoshop.Application.Open(aoMap);
                            aoDoc.Selection.SelectAll();
                            aoDoc.Selection.Copy();
                            aoDoc.Close();

                            RSG.Pipeline.PBR.Photoshop.Application.ActiveDocument = doc.Document;

                            ArtLayer aoLayer = RSG.Pipeline.PBR.Photoshop.Helpers.FindLayerByPath(doc.Document, doc.LayerGroup.AmbientOcclusion + "/Substance Ambient Occlusion");

                            LayerSet aoGroup = RSG.Pipeline.PBR.Photoshop.Helpers.FindLayerGroupByPath(doc.Document, doc.LayerGroup.AmbientOcclusion);
                            LayerSet miscGroup = RSG.Pipeline.PBR.Photoshop.Helpers.FindLayerGroupByPath(doc.Document, doc.LayerGroup.Miscellaneous);

                            if (aoLayer == null)
                            {
                                if (aoGroup == null)
                                {
                                    if (miscGroup == null)
                                    {
                                        miscGroup = doc.Document.LayerSets.Add();
                                        miscGroup.Name = "Misc";
                                    }

                                    miscGroup.Visible = true;

                                    aoGroup = miscGroup.LayerSets.Add();
                                    aoGroup.Name = TextureNamingConvention.AmbientOcclusion.Name;
                                }
                                aoLayer = aoGroup.ArtLayers.Add();
                                aoLayer.Name = "Substance Ambient Occlusion";
                            }

                            miscGroup.Visible = true;

                            foreach (LayerSet layerSet in miscGroup.LayerSets)
                            {
                                if (layerSet.Name == TextureNamingConvention.AmbientOcclusion.Name)
                                {
                                    layerSet.Visible = true;
                                }
                                else
                                {
                                    layerSet.Visible = false;
                                }
                            }

                            doc.Document.ActiveLayer = aoLayer;
                            aoLayer.Clear();
                            ArtLayer pastedLayer = doc.Document.Paste();
                            pastedLayer.Name = "Substance Ambient Occlusion";
                        }
                    }
                }
            }
        }

        public static void CreateHeightMap(IMasterMaterialDocument doc)
        {
            if (RSG.Pipeline.PBR.Photoshop.Application.IsConnected())
            {
                if (doc != null)
                {
                    string projCacheDir = Export.TempExportDirectory;

                    // Export Albedo
                    String outputFilename = TextureNamingConvention.Normal.Transform(doc.Document.Name);
                    ExportOptions options = new ExportOptions();
                    options.Path = projCacheDir;

                    string normalMap = Export.ExportNormal(doc, options);

                    if (normalMap != "")
                    {
                        string sbsar = RSG.Interop.Substance.Path.Archives + "\\Normal_to_Height.sbsar";
                        RSG.Interop.Substance.SbsWrapper sbs = new RSG.Interop.Substance.SbsWrapper(sbsar);
                        sbs.SetOutputName("normal_to_height");
                        sbs.SetInputImage("inputNormal", normalMap);
                        sbs.SetOutputSize((int)doc.Document.Width, (int)doc.Document.Height);
                        List<string> result = sbs.Render(projCacheDir);

                        if (result.Count == 1)
                        {
                            string heightMap = result[0];
                            Document heightDoc = RSG.Pipeline.PBR.Photoshop.Application.Open(heightMap);
                            heightDoc.Selection.SelectAll();
                            heightDoc.Selection.Copy();
                            heightDoc.Close();

                            RSG.Pipeline.PBR.Photoshop.Application.ActiveDocument = doc.Document;

                            ArtLayer heightLayer = RSG.Pipeline.PBR.Photoshop.Helpers.FindLayerByPath(doc.Document, doc.LayerGroup.Height + "/Substance Height");
                            LayerSet heightGroup = RSG.Pipeline.PBR.Photoshop.Helpers.FindLayerGroupByPath(doc.Document, doc.LayerGroup.Height);
                            LayerSet miscGroup = RSG.Pipeline.PBR.Photoshop.Helpers.FindLayerGroupByPath(doc.Document, doc.LayerGroup.Miscellaneous);

                            miscGroup.Visible = true;

                            foreach (LayerSet layerSet in miscGroup.LayerSets)
                            {
                                if (layerSet.Name == TextureNamingConvention.Height.Name)
                                {
                                    layerSet.Visible = true;
                                }
                                else
                                {
                                    layerSet.Visible = false;
                                }
                            }

                            doc.Document.ActiveLayer = heightLayer;
                            heightLayer.Clear();
                            ArtLayer pastedLayer = doc.Document.Paste();
                            pastedLayer.Name = "Substance Height";
                        }
                    }
                }
            }
        }

        public static void CreateNormalMap(IMasterMaterialDocument doc, float intensity)
        {
            if (RSG.Pipeline.PBR.Photoshop.Application.IsConnected())
            {
                if (doc != null)
                {
                    string projCacheDir = Export.TempExportDirectory;

                    // Export Albedo
                    ExportOptions options = new ExportOptions();
                    options.Path = projCacheDir;

                    String outputFilename = TextureNamingConvention.Albedo.Transform(doc.Document.Name);

                    string albedoMap = Export.ExportLayerGroup(doc.Document, options, doc.LayerGroup.Albedo, outputFilename);

                    if (albedoMap != "")
                    {
                        string sbsar = RSG.Interop.Substance.Path.Archives + "\\Albedo_to_Normal.sbsar";
                        RSG.Interop.Substance.SbsWrapper sbs = new RSG.Interop.Substance.SbsWrapper(sbsar);
                        sbs.SetOutputName("albedo_to_normal");
                        sbs.SetInputImage("inputAlbedo", albedoMap);
                        sbs.SetInputParameter("intensity", intensity.ToString());
                        sbs.SetOutputSize((int)doc.Document.Width, (int)doc.Document.Height);
                        List<string> result = sbs.Render(projCacheDir);

                        if (result.Count == 1)
                        {
                            string normalMap = result[0];
                            Document normalDoc = RSG.Pipeline.PBR.Photoshop.Application.Open(normalMap);
                            normalDoc.Selection.SelectAll();
                            normalDoc.Selection.Copy();
                            normalDoc.Close();

                            RSG.Pipeline.PBR.Photoshop.Application.ActiveDocument = doc.Document;

                            ArtLayer normalLayer = RSG.Pipeline.PBR.Photoshop.Helpers.FindLayerByPath(doc.Document, doc.LayerGroup.Normal + "/Substance Normal");
                            LayerSet normalGroup = RSG.Pipeline.PBR.Photoshop.Helpers.FindLayerGroupByPath(doc.Document, doc.LayerGroup.Normal);
                            LayerSet miscGroup = RSG.Pipeline.PBR.Photoshop.Helpers.FindLayerGroupByPath(doc.Document, doc.LayerGroup.Miscellaneous);

                            miscGroup.Visible = true;

                            foreach (LayerSet layerSet in miscGroup.LayerSets)
                            {
                                if (layerSet.Name == TextureNamingConvention.Normal.Name)
                                {
                                    layerSet.Visible = true;
                                }
                                else
                                {
                                    layerSet.Visible = false;
                                }
                            }

                            doc.Document.ActiveLayer = normalLayer;
                            normalLayer.Clear();
                            ArtLayer pastedLayer = doc.Document.Paste();
                            pastedLayer.Name = "Substance Normal";
                        }
                    }
                }
            }
        }
    }
}
