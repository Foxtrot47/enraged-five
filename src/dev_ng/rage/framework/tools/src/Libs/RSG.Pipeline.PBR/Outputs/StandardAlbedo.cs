﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

using RSG.Pipeline.PBR;
using RSG.Pipeline.PBR.Documents;
using RSG.Interop.Adobe.Photoshop;
using RSG.Base.Configuration;

namespace RSG.Pipeline.PBR.Outputs
{
    public class StandardAlbedo : BaseMapOutput
    {
        #region Member Data

        private StandardLayerGroup _layerGroup;
        private IConfig m_config = ConfigFactory.CreateConfig();

        #endregion // Member Data

        #region Properties

        public override String Name
        {
            get { return TextureNamingConvention.Albedo.Name; }
        }

        public override String LayerGroupPath
        {
            get { return _layerGroup.Albedo; }
        }

        public override NamingConventionDescription NamingConvention
        {
            get { return TextureNamingConvention.Albedo; }
        }

        #endregion //Properties

        #region Constructor(s)

        public StandardAlbedo()
        {
            _layerGroup = new StandardLayerGroup();
        }

        #endregion // Constructor(s)

        #region Methods

        public override String Export(Document doc, ExportOptions options)
        {
            IMasterMaterialDocument matDoc = MasterMaterialDocumentFactory.FromPhotoshopDocument(doc);
            String result = null;

            if (options.CheckAlbedoBrightness)
            {
                bool doExport = true;

                // Temporarily export albedo so that we can check its brightness before allowing it to be exported.
                ExportOptions tempOptions = new ExportOptions();
                tempOptions.Path = RSG.Pipeline.PBR.Export.TempExportDirectory;

                String tempAlbedo = RSG.Pipeline.PBR.Export.ExportLayerGroup(doc, tempOptions, this.LayerGroupPath, "tempAlbedo");

                float low = 52.0f / 255.0f;
                float high = 242.0f / 255.0f;
                float brightness = MapFactory.GetAverageBrightness(tempAlbedo);

                if (brightness > high)
                {
                    if (MessageBox.Show(String.Format("The average brightness of the Albedo is too high!  Currently it is at {0} when the top-end shouldn't be above {1}.\n\nWould you like to still export the Albedo?", (int)(brightness * 255.0), (int)(high * 255.0)), "Photoshop PBR", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                    {
                        doExport = false;
                    }
                }
                else if (brightness < low)
                {
                    if (MessageBox.Show(String.Format("The average brightness of the Albedo is too low!  Currently it is at {0} when the low-end shouldn't be below {1}.\n\nWould you like to still export the Albedo?", (int)(brightness * 255.0), (int)(low * 255.0)), "Photoshop PBR", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                    {
                        doExport = false;
                    }
                }

                if (doExport)
                {
                    result = RSG.Pipeline.PBR.Export.ExportAlbedo(matDoc, options);
                }
                else
                {
                    MessageBox.Show("Aborted export of the Albedo map!", "Photoshop PBR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            else
            {
                result = RSG.Pipeline.PBR.Export.ExportAlbedo(matDoc, options);
            }

            return result;
        }

        public override String ExportLiveUpdateMap(Document doc, ExportOptions options)
        {
            // Save DDS to the project's cache directory.
            Directory.CreateDirectory(RSG.Pipeline.PBR.Export.TempExportDirectory);

            // Save texture as TGA.
            TargaSaveOptions tgaOptions = new TargaSaveOptions();
            tgaOptions.Resolution = TargaBitsPerPixel.Targa32Bits;
            tgaOptions.AlphaChannels = true;

            String tgaFilename = String.Format("{0}\\{1}.tga", RSG.Pipeline.PBR.Export.TempExportDirectory, this.Name);

            // Convert to 8bpp so that we can save as a TGA.
            doc.BitsPerChannel = BitsPerChannelType.Document8Bits;

            List<DDSInputOption> inputOptions = new List<DDSInputOption>();
            List<DDSCompression> compression = new List<DDSCompression>();

            // Add custom alpha channel.
            if (options.ExportAlpha && this.AlphaMapFilename != null)
            {
                bool createAlphaChannel = true;

                // Create alpha channel if one doesn't exist.
                foreach (Channel channel in doc.Channels)
                {
                    if (channel.Name.ToLower().Contains("alpha"))
                    {
                        createAlphaChannel = false;
                    }
                }

                Document alphaDoc = RSG.Pipeline.PBR.Photoshop.Application.Open(this.AlphaMapFilename);
                alphaDoc.Selection.SelectAll();
                alphaDoc.Selection.Copy();
                alphaDoc.Close();

                if (createAlphaChannel)
                {
                    RSG.Pipeline.PBR.Photoshop.Application.ActiveDocument = doc;

                    Channel alphaChannel = doc.Channels.Add();
                    alphaChannel.Name = "Alpha";
                }

                RSG.Pipeline.PBR.Photoshop.Helpers.SetActiveChannels(doc, false, false, false, true);
                doc.Paste(false);
            }

            // Save.
            doc.SaveAs(tgaFilename, tgaOptions);

            inputOptions.Add(DDSInputOption.Color);
            compression.Add(DDSCompression.DXT5);

            String result = DDS.Compress(tgaFilename, inputOptions, compression);

            File.Delete(tgaFilename);

            return result;
        }

        #endregion // Methods
    }
}
