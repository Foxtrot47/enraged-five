﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Interop.Adobe.Photoshop;

namespace RSG.Pipeline.PBR
{
    public interface IMapOutput
    {
        #region Properties

        String Name { get; }
        String LayerGroupPath { get; }
        NamingConventionDescription NamingConvention { get; }
        String AlphaMapFilename { get; set; }

        #endregion // Properties

        #region Methods

        String Export(Document doc, ExportOptions options);
        String ExportLiveUpdateMap(Document doc, ExportOptions options);

        #endregion // Methods
    }
}
