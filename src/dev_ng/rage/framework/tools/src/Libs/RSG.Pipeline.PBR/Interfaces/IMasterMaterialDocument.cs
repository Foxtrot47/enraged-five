﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Interop.Adobe.Photoshop;
using RSG.Pipeline.PBR;

namespace RSG.Pipeline.PBR
{
    public interface IMasterMaterialDocument
    {
        #region Properties

        double Version { get; set; }
        int ExportWidth { get; set; }
        int ExportHeight { get; set; }
        MasterMaterialDocumentType Type { get; }
        Document Document { get; }
        ILayerGroup LayerGroup { get; set; }
        String AssignedDetailMapGroup { get; }

        #endregion // Properties

        #region Methods

        void Serialize();
        void Deserialize();
        void ConvertToType(MasterMaterialDocumentType toType);
        string[] Export(ExportOptions options);

        List<Surface> GetSurfaces();
        LayerSet GetMaterialLayerSet();
        LayerSet GetSurfaceLayerSet(Surface surface);
        LayerSet GetSurfaceModifierLayerSet(Surface surface);

        LayerSet SelectSurface(String groupName, String surfaceName);
        LayerSet SelectSurface(Surface surface);
        LayerSet SelectSurfaceModifier(Surface surface);
        void SelectDetailMap(DetailMap detailMap);

        void DisplayAllMaterials();
        void DisplayAlbedo();
        void DisplayAlpha();
        void DisplayAmbientOcclusion();
        void DisplayEmissive();
        void DisplayHeight();
        void DisplayTranslucency();
        void DisplayNormal();
        void DisplayRoughness();
        void DisplayMetalness();
        void DisplayModifier();
        void DisplayDetailMapIntensity();
        void DisplayDetailMapIDs();
        void DisplaySheen();

        void SetLayerFillColorsToRoughness();
        void SetLayerFillColorsToMetalness();
        void SetLayerFillColorsToSheen();

        void SetRoughnessOpacity(double opacity);
        void SetModifierOpacity(double opacity);

        #endregion // Methods
    }
}
