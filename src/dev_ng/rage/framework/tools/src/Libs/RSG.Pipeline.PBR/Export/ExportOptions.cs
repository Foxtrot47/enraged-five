﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Interop.Adobe.Photoshop;

namespace RSG.Pipeline.PBR
{
    public class ExportOptions
    {
        #region Constructor(s)

        public ExportOptions()
        {
        }

        public ExportOptions(Document doc)
        {
            this.FullName = doc.FullName;
            this.Name = doc.Name;
        }

        #endregion // Constructor(s)

        #region Properties

        /// <summary>
        /// The export path for the document.
        /// </summary>
        public String Path = null;

        /// <summary>
        /// The name of the document we are exporting.
        /// </summary>
        public String Name = null;

        /// <summary>
        /// The full name of the document we are exporting.
        /// </summary>
        public String FullName = null;

        /// <summary>
        /// Use the document's path as the export path.
        /// </summary>
        public bool UseDocumentAsExportPath = true;

        /// <summary>
        /// Export the Albedo map.
        /// </summary>
        public bool ExportAlbedo = true;

        /// <summary>
        /// Export the Alpha map.
        /// </summary>
        public bool ExportAlpha = true;

        /// <summary>
        /// Whether or not to pack the Alpha into the Albedo.
        /// </summary>
        public bool PackAlbedoAlpha = false;

        /// <summary>
        /// Export the Emissive map.
        /// </summary>
        public bool ExportEmissive = true;

        /// <summary>
        /// Export the Normal map.
        /// </summary>
        public bool ExportNormal = true;

        /// <summary>
        /// Export Material Map A.
        /// </summary>
        public bool ExportMaterialMapA = true;

        /// <summary>
        /// Export Material Map B.
        /// </summary>
        public bool ExportMaterialMapB = true;

        /// <summary>
        /// Show pop-up messages during export.
        /// </summary>
        public bool Quiet = false;

        /// <summary>
        /// Save a backup of the document before exporting.
        /// </summary>
        public bool SaveBackup = true;

        /// <summary>
        /// Close document after the export has completed.
        /// </summary>
        public bool CloseDocumentAfterExport = false;

        /// <summary>
        /// Set the image width before exporting.
        /// </summary>
        public int Width = -1;

        /// <summary>
        /// Set the image height before exporting.
        /// </summary>
        public int Height = -1;

        /// <summary>
        /// Whether or not to create an alpha channel in the map.
        /// </summary>
        public bool CreateAlphaChannel = true;

        /// <summary>
        /// Converts outputs to DDS and tells RAG to update the textures.
        /// </summary>
        public bool LiveUpdate = true;

        /// <summary>
        /// Test if the Albedo is too bright/dark for the surface.
        /// </summary>
        public bool CheckAlbedoBrightness = true;

        #endregion // Properties
    }
}
