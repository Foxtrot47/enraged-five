﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using RSG.Pipeline.PBR;
using RSG.Pipeline.PBR.Documents;
using RSG.Interop.Adobe.Photoshop;

namespace RSG.Pipeline.PBR.Outputs
{
    public class StandardNormal : BaseMapOutput
    {
        #region Member Data

        private StandardLayerGroup _layerGroup;

        #endregion // Member Data

        #region Properties

        public override String Name
        {
            get { return TextureNamingConvention.Normal.Name; }
        }

        public override String LayerGroupPath
        {
            get { return _layerGroup.Normal; }
        }

        public override NamingConventionDescription NamingConvention
        {
            get { return TextureNamingConvention.Normal; }
        }

        #endregion //Properties

        #region Constructor(s)

        public StandardNormal()
        {
            _layerGroup = new StandardLayerGroup();
        }

        #endregion // Constructor(s)

        #region Methods

        public override String Export(Document doc, ExportOptions options)
        {
            IMasterMaterialDocument matDoc = MasterMaterialDocumentFactory.FromPhotoshopDocument(doc);
            return RSG.Pipeline.PBR.Export.ExportNormal(matDoc, options);
        }

        public override String ExportLiveUpdateMap(Document doc, ExportOptions options)
        {
            // Save DDS to the project's cache directory.
            Directory.CreateDirectory(RSG.Pipeline.PBR.Export.TempExportDirectory);

            // Save texture as TGA.
            TargaSaveOptions tgaOptions = new TargaSaveOptions();
            tgaOptions.Resolution = TargaBitsPerPixel.Targa32Bits;
            tgaOptions.AlphaChannels = true;

            String tgaFilename = String.Format("{0}\\{1}.tga", RSG.Pipeline.PBR.Export.TempExportDirectory, this.Name);

            // Convert to 8bpp so that we can save as a TGA.
            doc.BitsPerChannel = BitsPerChannelType.Document8Bits;

            // Save.
            doc.SaveAs(tgaFilename, tgaOptions);

            List<DDSInputOption> inputOptions = new List<DDSInputOption>();
            List<DDSCompression> compression = new List<DDSCompression>();

            inputOptions.Add(DDSInputOption.Normal);
            compression.Add(DDSCompression.ATI2_3Dc);

            String result = DDS.Compress(tgaFilename, inputOptions, compression);

            File.Delete(tgaFilename);

            return result;
        }

        #endregion // Methods
    }
}
