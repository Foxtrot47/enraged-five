﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Drawing;
using System.Windows.Media.Imaging;

using RSG.Base.Configuration;
using RSG.SourceControl.Perforce;

namespace RSG.Pipeline.PBR
{
    public class DetailMapGroup
    {
        private String m_name;
        private List<DetailMap> m_detailMaps;

        public DetailMapGroup(String name)
        {
            this.m_name = name;
            this.m_detailMaps = new List<DetailMap>();
        }

        public String Name
        {
            get { return this.m_name; }
        }

        public IEnumerable<DetailMap> DetailMaps
        {
            get { return this.m_detailMaps.ToArray(); }
        }

        public void Add(DetailMap detailMap)
        {
            this.m_detailMaps.Add(detailMap);
        }
    }

    public class DetailMap
    {
        #region Member Data

        private String m_name;
        private String m_layerName;
        private int m_value;
        private int m_id;
        private BitmapImage m_image;

        #endregion // Member Data

        #region Constructor(s)

        public DetailMap(String name, int value, int id)
        {
            this.m_name = name;
            this.m_value = value;
            this.m_id = id;
            this.m_layerName = String.Format("{0} (ID:{1})", name, id);
        }

        #endregion // Constructor(s)

        #region Properties

        /// <summary>
        /// The name of the detail map.
        /// </summary>
        public String Name
        {
            get { return this.m_name; }
        }

        /// <summary>
        /// The Photoshop layer name for this detail map.
        /// </summary>
        public String LayerName
        {
            get { return this.m_layerName; }
        }

        /// <summary>
        /// The HSB brightness value of the detail map.
        /// </summary>
        public int Value
        {
            get { return this.m_value; }
        }

        /// <summary>
        /// The run-time ID for the detail map.
        /// </summary>
        public int ID
        {
            get { return this.m_id; }
        }

        public BitmapImage Thumbnail
        {
            get { return this.m_image; }
        }

        #endregion

        #region Methods

        public void SetThumbnail(Bitmap image)
        {
            MemoryStream ms = new MemoryStream();
            image.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
            
            BitmapImage bmpImage = new BitmapImage();
            bmpImage.BeginInit();
            ms.Seek(0, SeekOrigin.Begin);
            bmpImage.StreamSource = ms;
            bmpImage.EndInit();

            this.m_image = bmpImage;
        }

        #endregion
    }

    public static class DetailMapManager
    {
        #region Member Data

        private static Dictionary<String, DetailMapGroup> m_detailMapGroups = new Dictionary<String, DetailMapGroup>();
        private static IConfig m_config = ConfigFactory.CreateConfig();
        private static string m_materialsRoot = m_config.ToolsRoot + "\\techart\\etc\\config\\material";

        public static readonly int DETAIL_MAP_WIDTH = 128;
        public static readonly int DETAIL_MAP_HEIGHT = 64;

        #endregion // Member Data

        #region Properties

        public static Dictionary<String, DetailMapGroup> Groups
        {
            get { return m_detailMapGroups; }
        }

        #endregion

        #region Methods

        public static DetailMapGroup GetGroup(String name)
        {
            if (m_detailMapGroups.ContainsKey(name))
            {
                return m_detailMapGroups[name];
            }

            return null;
        }

        public static void Load()
        {
            string detailMapsXml = m_materialsRoot + "\\detailMaps.xml";

            m_detailMapGroups.Clear();

            // Sync to latest revision of the detail maps xml.
            if (RSG.Pipeline.PBR.Photoshop.UseSourceControl)
            {
                try
                {
                    RSG.SourceControl.Perforce.P4 p4 = new RSG.SourceControl.Perforce.P4();
                    p4.Connect();
                    p4.Run("sync", detailMapsXml);
                }
                catch { }
            }

            XmlDocument doc = new XmlDocument();
            doc.Load(detailMapsXml);

            XmlNodeList detailMapGroupNodes = doc.GetElementsByTagName("DetailMapGroup");

            foreach (XmlNode detailMapGroupNode in detailMapGroupNodes)
            {
                String groupName = detailMapGroupNode.Attributes["Name"].Value;
                String thumbnailName = detailMapGroupNode.Attributes["Thumbnail"].Value;
                DetailMapGroup group = new DetailMapGroup(groupName);

                Bitmap original = new Bitmap(String.Format("{0}\\techart\\standalone\\PhotoshopPBR\\detailMaps\\{1}", m_config.ToolsRoot, thumbnailName));

                XmlNodeList detailMapNodes = detailMapGroupNode.SelectNodes("DetailMap");

                foreach (XmlNode detailMapNode in detailMapNodes)
                {
                    String name = detailMapNode.Attributes["Name"].Value;
                    int value = Convert.ToInt32(detailMapNode.Attributes["Value"].Value);
                    int id = Convert.ToInt32(detailMapNode.Attributes["ID"].Value);

                    DetailMap detailMap = new DetailMap(name, value, id);

                    // Grag the specific detail map out of the atlas.
                    Rectangle rect = new Rectangle(id * DETAIL_MAP_WIDTH, 0, DETAIL_MAP_WIDTH, DETAIL_MAP_HEIGHT);
                    Bitmap thumbnail = (Bitmap)original.Clone(rect, original.PixelFormat);
                    detailMap.SetThumbnail(thumbnail);

                    group.Add(detailMap);
                }

                m_detailMapGroups.Add(groupName, group);
            }
        }

        #endregion // Methods
    }
}
