﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.ComponentModel;
using System.Reflection;

using RSG.Base.Configuration;

namespace RSG.Pipeline.PBR
{
    public enum DDSInputOption
    {
        /// <summary>
        /// The input image is a color map (default).
        /// </summary>
        [Description("-color")]
        Color,

        /// <summary>
        /// The input image is a normal map.
        /// </summary>
        [Description("-normal")]
        Normal,

        /// <summary>
        /// Convert input to normal map.
        /// </summary>
        [Description("-tonormal")]
        ToNormal,

        /// <summary>
        /// Clamp wrapping mode (default).
        /// </summary>
        [Description("-clamp")]
        Clamp,

        /// <summary>
        /// Repeate wrapping mode.
        /// </summary>
        [Description("-repeat")]
        Repeat,

        /// <summary>
        /// Disable mipmap generation.
        /// </summary>
        [Description("-nomips")]
        NoMips
    }

    public enum DDSCompression
    {
        /// <summary>
        /// Fast compression.
        /// </summary>
        [Description("-fast")]
        Fast,

        /// <summary>
        /// Do not use cuda compressor.
        /// </summary>
        [Description("-nocuda")]
        NoCuda,

        /// <summary>
        /// RGBA format.
        /// </summary>
        [Description("-rgb")]
        RGB,

        /// <summary>
        /// BC1 format (DXT1).
        /// </summary>
        [Description("-bc1")]
        DXT1,

        /// <summary>
        /// BC1 normal map format (DXT1nm).
        /// </summary>
        [Description("-bc1n")]
        DXT1_NM,

        /// <summary>
        /// BC1 format with binary alpha (DXT1a).
        /// </summary>
        [Description("-bc1a")]
        DXT1_A,

        /// <summary>
        /// BC2 format (DXT3).
        /// </summary>
        [Description("-bc2")]
        DXT3,

        /// <summary>
        /// BC3 format (DXT5).
        /// </summary>
        [Description("-bc3")]
        DXT5,

        /// <summary>
        /// BC3 normal map format (DXT5nm).
        /// </summary>
        [Description("-bc3n")]
        DXT5_NM,

        /// <summary>
        /// BC4 format (ATI1).
        /// </summary>
        [Description("-bc4")]
        ATI1,

        /// <summary>
        /// BC5 format (3Dc/ATI2).
        /// </summary>
        [Description("-bc5")]
        ATI2_3Dc
    }

    public static class DDS
    {
        #region Member Data

        private static IConfig m_config = ConfigFactory.CreateConfig();
        private static String m_nvcompressExe = String.Format("{0}\\techart\\standalone\\PhotoshopPBR\\tools\\nvcompress.exe", m_config.ToolsRoot);

        #endregion // Member Data

        #region Methods

        /// <summary>
        /// Compresses the given Targa (.tga) and produces a DDS file.
        /// </summary>
        /// <param name="tgaFilename">The Targa (.tga) file to compress.</param>
        /// <param name="input">List of input options to use.</param>
        /// <param name="compression">List of compression types to use.</param>
        /// <returns>The resulting DDS filename.</returns>
        public static String Compress(String tgaFilename, IEnumerable<DDSInputOption> inputOptions, IEnumerable<DDSCompression> compressionOptions)
        {
            String ddsFilename = String.Format("{0}\\{1}.dds", Path.GetDirectoryName(tgaFilename), Path.GetFileNameWithoutExtension(tgaFilename));

            // Build up a string of the input options.
            StringBuilder options = new StringBuilder();

            foreach (DDSInputOption inputOption in inputOptions)
            {
                options.AppendFormat("{0} ", inputOption.GetDescription());
            }

            foreach (DDSCompression compressionOption in compressionOptions)
            {
                options.AppendFormat("{0} ", compressionOption.GetDescription());
            }

            String convertArgs = String.Format("{0}\"{1}\" \"{2}\"", options.ToString(), tgaFilename, ddsFilename);
            String batchFile = String.Format("{0}\\convertToDDS.bat", m_config.ToolsTemp);

            System.IO.File.WriteAllText(batchFile, String.Format("{0} {1}", m_nvcompressExe, convertArgs));

            Process process = new Process();
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = batchFile;
            process.StartInfo = startInfo;
            process.Start();
            process.WaitForExit();

            // Get rid of the temp files.
            File.Delete(batchFile);

            return ddsFilename;
        }

        #endregion // Methods
    }

    public static class DDSExtensions
    {
        public static string GetDescription(this DDSInputOption value)
        {
            Type type = value.GetType();
            string name = Enum.GetName(type, value);
            if (name != null)
            {
                FieldInfo field = type.GetField(name);
                if (field != null)
                {
                    DescriptionAttribute attr =
                           Attribute.GetCustomAttribute(field,
                             typeof(DescriptionAttribute)) as DescriptionAttribute;
                    if (attr != null)
                    {
                        return attr.Description;
                    }
                }
            }
            return null;
        }

        public static string GetDescription(this DDSCompression value)
        {
            Type type = value.GetType();
            string name = Enum.GetName(type, value);
            if (name != null)
            {
                FieldInfo field = type.GetField(name);
                if (field != null)
                {
                    DescriptionAttribute attr =
                           Attribute.GetCustomAttribute(field,
                             typeof(DescriptionAttribute)) as DescriptionAttribute;
                    if (attr != null)
                    {
                        return attr.Description;
                    }
                }
            }
            return null;
        }
    }
}
