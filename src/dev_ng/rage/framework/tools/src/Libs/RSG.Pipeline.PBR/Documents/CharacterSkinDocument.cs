﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Interop.Adobe.Photoshop;

using RSG.Pipeline.PBR;
using RSG.Pipeline.PBR.Outputs;


namespace RSG.Pipeline.PBR.Documents
{
    /// <summary>
    /// Defines paths to layer groups in the master material document.
    /// </summary>
    public sealed class CharacterSkinLayerGroup : BaseLayerGroup
    {
        #region Properties
        #endregion // Properties

        #region Constructor(s)

        public CharacterSkinLayerGroup()
            : base()
        {
        }

        #endregion // Constructor(s)

        #region Methods
        #endregion // Methods
    }

    /// <summary>
    /// Character Skin Master Material Document.
    /// </summary>
    class CharacterSkinMasterMaterialDocument : BaseMasterMaterialDocument
    {
        #region Properties

        public override String AssignedDetailMapGroup
        {
            get { return "Character"; }
        }

        #endregion // Properties

        #region Constructor(s)

        public CharacterSkinMasterMaterialDocument(Document doc, bool createLayerGroups = false)
            : base(doc, createLayerGroups)
        {
        }

        public CharacterSkinMasterMaterialDocument(int width, int height, int resolution = 72, NewDocumentMode mode = NewDocumentMode.NewRGB, bool createLayerGroups = false)
            : base(MasterMaterialDocumentType.CharacterSkin, width, height, resolution, mode, createLayerGroups)
        {
        }

        #endregion // Constructor(s)

        #region Overridden Methods

        public override void Setup(bool createLayerGroups)
        {
            CharacterSkinLayerGroup layerGroup = new CharacterSkinLayerGroup();
            this.LayerGroup = layerGroup;

            if (createLayerGroups == true)
            {
                layerGroup.Create(this);
            }
        }

        public override string[] Export(ExportOptions options)
        {
            // Setup outputs based on the options.
            if (options.ExportAlpha)
            {
                this.AddOutput(new StandardAlpha());
            }

            if (options.ExportAlbedo)
            {
                this.AddOutput(new StandardAlbedo());
            }

            if (options.ExportNormal)
            {
                this.AddOutput(new StandardNormal());
            }

            if (options.ExportMaterialMapA)
            {
                this.AddOutput(new StandardMaterialMapA());
            }

            if (options.ExportMaterialMapB)
            {
                this.AddOutput(new StandardMaterialMapB());
            }

            if (options.ExportEmissive)
            {
                this.AddOutput(new StandardEmissive());
            }

            return base.Export(options);
        }

        #endregion // Overridden Methods
    }
}
