﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Pipeline.PBR;
using RSG.Pipeline.PBR.Documents;
using RSG.Interop.Adobe.Photoshop;

namespace RSG.Pipeline.PBR.Outputs
{
    /// <summary>
    /// Emissive map output.
    /// </summary>
    public class StandardEmissive : BaseMapOutput
    {
        #region Member Data

        private StandardLayerGroup _layerGroup;

        #endregion // Member Data

        #region Properties

        /// <summary>
        /// Name of the output.
        /// </summary>
        public override String Name
        {
            get { return TextureNamingConvention.Emissive.Name; }
        }

        /// <summary>
        /// Photoshop layer group path for this output.
        /// </summary>
        public override String LayerGroupPath
        {
            get { return _layerGroup.Emissive; }
        }

        public override NamingConventionDescription NamingConvention
        {
            get { return TextureNamingConvention.Emissive; }
        }

        #endregion //Properties

        #region Constructor(s)

        public StandardEmissive()
        {
            _layerGroup = new StandardLayerGroup();
        }

        #endregion // Constructor(s)

        #region Methods

        public override String Export(Document doc, ExportOptions options)
        {
            IMasterMaterialDocument matDoc = MasterMaterialDocumentFactory.FromPhotoshopDocument(doc);
            return RSG.Pipeline.PBR.Export.ExportEmissive(matDoc, options);
        }

        public override String ExportLiveUpdateMap(Document doc, ExportOptions options)
        {
            // No live update map for emissive.
            return null;
        }

        #endregion // Methods
    }
}
