﻿using System;
using System.IO;
using System.Diagnostics;
using System.Xml;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Media.Imaging;
using System.Windows.Forms;
using System.Threading;
using System.Reflection;

using RSG.Base.Configuration;
using RSG.SourceControl.Perforce;

namespace RSG.Pipeline.PBR
{
    /// <summary>
    /// Generate maps off-line.
    /// </summary>
    public static class MapFactory
    {
        #region Member Data
        #endregion // Constants

        #region Methods

        public static float GetAverageBrightness(string imageFilename)
        {
            Bitmap bmp = new Bitmap(imageFilename);

            long r = 0;
            long g = 0;
            long b = 0;

            long total = 0;

            for (int x = 0; x < bmp.Width; x++)
            {
                for (int y = 0; y < bmp.Height; y++)
                {
                    Color clr = bmp.GetPixel(x, y);

                    r += clr.R;
                    g += clr.G;
                    b += clr.B;

                    total++;
                }
            }

            // Calculate average
            r /= total;
            g /= total;
            b /= total;

            bmp.Dispose();

            if (r < 0)
            {
                r = 0;
            }

            if (r > 255)
            {
                r = 255;
            }

            if (g < 0)
            {
                g = 0;
            }

            if (g > 255)
            {
                g = 255;
            }

            if (b < 0)
            {
                b = 0;
            }

            if (b > 255)
            {
                b = 255;
            }

            Color avgColor = Color.FromArgb((int)r, (int)g, (int)b);
            return avgColor.GetBrightness();
        }

        #endregion // Methods
    }
}
