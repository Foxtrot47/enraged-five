﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;

using RSG.Pipeline.PBR;
using RSG.Interop.Adobe.Photoshop;

namespace RSG.Pipeline.PBR.Documents
{
    public static class MasterMaterialDocumentFactory
    {
        #region Constants

        private static readonly String XML_ROOT = "Rockstar";
        private static readonly String XML_DOC_TYPE = "DocumentType";
        private static readonly String XML_VERSION = "Version";

        #endregion // Constants

        #region Serialization Methods

        private static MasterMaterialDocumentType Serialize(Document doc)
        {
            String xml = String.Format("<{0}>\n", XML_ROOT);

            xml += String.Format("\t<{0}>{1}</{0}>\n", XML_VERSION, 2);
            xml += String.Format("\t<{0}>{1}</{0}>\n", XML_DOC_TYPE, MasterMaterialDocumentType.Standard);

            // End root tag.
            xml += String.Format("</{0}>\n", XML_ROOT);

            doc.Info.Instructions = xml;

            return MasterMaterialDocumentType.Standard;
        }

        private static MasterMaterialDocumentType Deserialize(Document doc)
        {
            String xml = doc.Info.Instructions;

            if (xml != "")
            {
                XDocument xmlDoc = XDocument.Parse(xml);
                return (MasterMaterialDocumentType)Enum.Parse(typeof(MasterMaterialDocumentType), xmlDoc.Root.Element(XML_DOC_TYPE).Value);
            }
            else
            {
                return Serialize(doc); 
            }
        }

        #endregion // Serialization Methods

        #region Private Methods

        private static IMasterMaterialDocument GetFromType(Document doc, MasterMaterialDocumentType type)
        {
            IMasterMaterialDocument masterMatDoc;

            switch (type)
            {
                case MasterMaterialDocumentType.None:
                    masterMatDoc = new StandardMasterMaterialDocument(doc);
                    break;

                case MasterMaterialDocumentType.Standard:
                    masterMatDoc = new StandardMasterMaterialDocument(doc);
                    break;

                case MasterMaterialDocumentType.Vegetation:
                    masterMatDoc = new VegetationMasterMaterialDocument(doc);
                    break;

                case MasterMaterialDocumentType.CharacterCloth:
                    masterMatDoc = new CharacterClothMasterMaterialDocument(doc);
                    break;

                case MasterMaterialDocumentType.CharacterSkin:
                    masterMatDoc = new CharacterSkinMasterMaterialDocument(doc);
                    break;

                case MasterMaterialDocumentType.GTAVLibertyCity:
                    masterMatDoc = new GTAVLibertyCityMasterMaterialDocument(doc);
                    break;

                default:
                    masterMatDoc = null;
                    break;
            }

            return masterMatDoc;
        }

        #endregion // Private Methods

        #region Methods

        /// <summary>
        /// Create a new Master Material document.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        public static IMasterMaterialDocument Create(MasterMaterialDocumentType type, int width, int height)
        {
            IMasterMaterialDocument masterMatDoc;

            switch (type)
            {
                case MasterMaterialDocumentType.None:
                    masterMatDoc = new StandardMasterMaterialDocument(width, height, 72, NewDocumentMode.NewRGB, true);
                    break;

                case MasterMaterialDocumentType.Standard:
                    masterMatDoc = new StandardMasterMaterialDocument(width, height, 72, NewDocumentMode.NewRGB, true);
                    break;

                case MasterMaterialDocumentType.Vegetation:
                    masterMatDoc = new VegetationMasterMaterialDocument(width, height, 72, NewDocumentMode.NewRGB, true);
                    break;

                case MasterMaterialDocumentType.CharacterCloth:
                    masterMatDoc = new CharacterClothMasterMaterialDocument(width, height, 72, NewDocumentMode.NewRGB, true);
                    break;

                case MasterMaterialDocumentType.CharacterSkin:
                    masterMatDoc = new CharacterSkinMasterMaterialDocument(width, height, 72, NewDocumentMode.NewRGB, true);
                    break;

                case MasterMaterialDocumentType.GTAVLibertyCity:
                    masterMatDoc = new GTAVLibertyCityMasterMaterialDocument(width, height, 72, NewDocumentMode.NewRGB, true);
                    break;

                default:
                    masterMatDoc = null;
                    break;
            }

            return masterMatDoc;
        }

        /// <summary>
        /// Get the appropriate Master Material document from the supplied Photoshop document.
        /// </summary>
        /// <param name="doc"></param>
        /// <returns></returns>
        public static IMasterMaterialDocument FromPhotoshopDocument(Document doc)
        {
            MasterMaterialDocumentType masterMatDocType = Deserialize(doc);
            return GetFromType(doc, masterMatDocType);
        }

        /// <summary>
        /// Converts the supplied Photoshop document to a different type of Master Material document.
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="toType"></param>
        /// <returns></returns>
        public static IMasterMaterialDocument ConvertToType(Document doc, MasterMaterialDocumentType toType)
        {
            // Convert open document to the new type.
            IMasterMaterialDocument currentMasterMatDoc = FromPhotoshopDocument(doc);
            currentMasterMatDoc.ConvertToType(toType);

            // Return appropriate document type.
            return GetFromType(doc, toType);
        }

        /// <summary>
        /// Open a file and return it as a Master Material document.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static IMasterMaterialDocument Open(string filename)
        {
            Document doc = RSG.Pipeline.PBR.Photoshop.Application.Open(filename);
            MasterMaterialDocumentType masterMatDocType = Deserialize(doc);

            return GetFromType(doc, masterMatDocType);
        }

        #endregion // Methods
    }
}
