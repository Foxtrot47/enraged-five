﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Windows.Forms;
using System.IO;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Reflection;

using RSG.Interop.Adobe.Photoshop;
using RSG.Pipeline.PBR;
using RSG.RAG.RemoteConnection;
using RSG.Base.Configuration;

namespace RSG.Pipeline.PBR
{
    public abstract partial class BaseMasterMaterialDocument : IMasterMaterialDocument
    {
        #region Constants

        private readonly String XML_ROOT = "Rockstar";
        private readonly String XML_DOC_TYPE = "DocumentType";
        private readonly String XML_EXPORT_WIDTH = "ExportWidth";
        private readonly String XML_EXPORT_HEIGHT = "ExportHeight";
        private readonly String XML_VERSION = "Version";
        private readonly String XML_LAST_LAYER_UI_COLOR = "LastLayerUIColor";

        #endregion // Constants

        #region Member Data

        private IConfig _config = ConfigFactory.CreateConfig();
        private Document _doc;
        private Dictionary<String, IMapOutput> _outputs;
        private ILayerGroup _layerGroup;
        private MasterMaterialDocumentType _type;
        private double _version = 2.0;

        private int _exportWidth = -1;
        private int _exportHeight = -1;

        private LayerUIColor[] _layerUIColors = new LayerUIColor[] { LayerUIColor.Red, LayerUIColor.Green, LayerUIColor.Orange, LayerUIColor.Blue, LayerUIColor.Violet, LayerUIColor.Yellow, LayerUIColor.Gray};
        private int _currentLayerUIColor = 0;

        // Used to store/restore visibility of layer sets.
        private Dictionary<LayerSet, bool> _layerSets = new Dictionary<LayerSet, bool>();

        private String _name;

        #endregion // Member Data

        #region Properties

        /// <summary>
        /// Name that represents the type of document.
        /// </summary>
        public virtual String Name
        {
            get { return this._name; }
        }

        /// <summary>
        /// The assigned Detail Map group for this document type.
        /// </summary>
        public abstract String AssignedDetailMapGroup
        {
            get;
        }

        /// <summary>
        /// The type of master material document.
        /// </summary>
        public MasterMaterialDocumentType Type
        {
            get { return this._type; }
        }

        /// <summary>
        /// Handle to the associated Photoshop document.
        /// </summary>
        public Document Document
        {
            get { return this._doc; }
        }

        /// <summary>
        /// Handle to the layer groups in the document.
        /// </summary>
        public ILayerGroup LayerGroup
        {
            get { return this._layerGroup; }
            set { this._layerGroup = value; }
        }

        /// <summary>
        /// Export width of the document.
        /// </summary>
        public int ExportWidth
        {
            get { return this._exportWidth; }
            set { this._exportWidth = value; this.Serialize(); }
        }

        /// <summary>
        /// Export height of the document.
        /// </summary>
        public int ExportHeight
        {
            get { return this._exportHeight; }
            set { this._exportHeight = value; this.Serialize(); }
        }

        /// <summary>
        /// Master material document version.
        /// </summary>
        public double Version
        {
            get { return this._version; }
            set { this._version = value; this.Serialize(); }
        }

        #endregion // Properties

        #region Constructor(s)

        public BaseMasterMaterialDocument(MasterMaterialDocumentType type, double width, double height, double resolution = 72, NewDocumentMode mode = NewDocumentMode.NewRGB, bool createLayerGroups = false)
        {
            if (RSG.Pipeline.PBR.Photoshop.Application.IsConnected())
            {
                // Store current ruler unit preference.
                Units rulerUnits = RSG.Pipeline.PBR.Photoshop.Application.Preferences.RulerUnits;

                // Force ruler units to be pixels.  This appears to fix issues when creating new documents whose ruler units are set to inches, it would
                // create massive sized files.
                RSG.Pipeline.PBR.Photoshop.Application.Preferences.RulerUnits = Units.Pixels;

                // Set untitled document name.
                String docName = String.Format("Untitled {0} Material", type.ToString());

                this._doc = RSG.Pipeline.PBR.Photoshop.Application.Documents.Add(width, height, resolution, docName, mode, DocumentFill.Transparent, 1.0, BitsPerChannelType.Document16Bits);

                // Restore ruler unit preference.
                RSG.Pipeline.PBR.Photoshop.Application.Preferences.RulerUnits = rulerUnits;

                // Initialize list to hold map outputs for this document.
                this._outputs = new Dictionary<String, IMapOutput>();

                // Set the document type.
                this._type = type;

                // Set export size.
                this._exportWidth = (int)width;
                this._exportHeight = (int)height;

                // Set the name of the document type to the enum.
                this._name = type.ToString();

                // Write metadata to the document.
                this.Serialize();

                // Call setup function.
                this.Setup(createLayerGroups);

                // Update to the correct version.
                this.VersionUpdate();
            }
        }

        public BaseMasterMaterialDocument(Document doc, bool createLayerGroups = false)
        {
            this._doc = doc;

            // Initialize list to hold map outputs for this document.
            this._outputs = new Dictionary<String, IMapOutput>();

            // Read metadata from the document.
            this.Deserialize();

            // Call setup function.
            this.Setup(createLayerGroups);

            // Update to the correct version.
            this.VersionUpdate();
        }

        #endregion

        #region Virtual Methods

        /// <summary>
        /// Sets the layer ui color for the supplied layer.  The color is picked automatically.
        /// </summary>
        /// <param name="layer"></param>
        public void SetLayerUIColor(ILayer layer)
        {
            this.Document.ActiveLayer = layer;
            LayerUIColor currentColor = _layerUIColors[_currentLayerUIColor];
            RSG.Pipeline.PBR.Photoshop.Helpers.SetLayerUIColor(currentColor);

            _currentLayerUIColor += 1;

            if (_currentLayerUIColor > _layerUIColors.Length - 1)
            {
                _currentLayerUIColor = 0;
            }
        }

        /// <summary>
        /// Setup process for the Photoshop document.
        /// </summary>
        public abstract void Setup(bool createLayerGroups);

        public virtual bool UpdateSurfaceName(String oldName, String newName)
        {
            // Get old group/surface name.
            if (oldName.Contains('.') && newName.Contains('.'))
            {
                String[] splitOld = oldName.Split('.');
                String oldGroupName = splitOld[0];
                String oldSurfaceName = splitOld[1];

                String[] splitNew = newName.Split('.');
                String newGroupName = splitNew[0];
                String newSurfaceName = splitNew[1];

                // Construct paths.
                String oldLayerGroupPath = String.Format("{0}/{1}", this.LayerGroup.Material, oldGroupName);
                String oldLayerPath = String.Format("{0}/{1}", oldLayerGroupPath, oldSurfaceName);

                String newLayerGroupPath = String.Format("{0}/{1}", this.LayerGroup.Material, newGroupName);
                String newLayerPath = String.Format("{0}/{1}", newLayerGroupPath, newSurfaceName);

                // Update layer if found.
                ArtLayer oldLayer = RSG.Pipeline.PBR.Photoshop.Helpers.FindLayerByPath(this._doc, oldLayerPath, false);

                if (oldLayer != null)
                {
                    // Rename.
                    oldLayer.Name = newSurfaceName;

                    // If under a new group, move layer to that group.
                    if (oldGroupName != newGroupName)
                    {
                        // Record the current active layer.
                        var oldSelectedLayer = this._doc.ActiveLayer;

                        LayerSet materialLayerSet = RSG.Pipeline.PBR.Photoshop.Helpers.FindLayerGroupByPath(this._doc, this._layerGroup.Material);
                        LayerSet newLayerSet = RSG.Pipeline.PBR.Photoshop.Helpers.FindLayerGroupByPath(this._doc, newLayerGroupPath);

                        // Create if layer group doesn't exist.
                        if (newLayerSet == null)
                        {
                            if (materialLayerSet == null)
                            {
                                materialLayerSet = this._doc.LayerSets.Add();
                                materialLayerSet.Name = this._layerGroup.Material;
                            }

                            this._doc.ActiveLayer = materialLayerSet;

                            newLayerSet = materialLayerSet.LayerSets.Add();
                            newLayerSet.Name = newGroupName;
                        }

                        this._doc.ActiveLayer = newLayerSet;
                        oldLayer.Move(newLayerSet, ElementPlacement.PlaceInside);

                        this._doc.ActiveLayer = oldSelectedLayer;
                    }

                    String msg = String.Format("The material surface \"{0}\\{1}\" was migrated to \"{2}\\{3}\".", oldGroupName, oldSurfaceName, newGroupName, newSurfaceName);
                    MessageBox.Show(msg, "Photoshop PBR", MessageBoxButtons.OK);

                    return true;
                }
            }

            return false;
        }

        private void HandleVersion1Update()
        {
            // Migrate to new format.
            if (this._version == 1.0)
            {
                MessageBox.Show("This document needs to be updated!  There is a new format for how materials and modifiers are organized, which will be re-organized for you now.  You will be messaged once the process has finished.\n\nPlease press OK to start the process.", "Photoshop PBR - Update", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                // Create new temp layer set.
                LayerSet tempLayerSet = Photoshop.Helpers.FindLayerGroupByPath(this.Document, this.LayerGroup.Temp, true);

                LayerSet materialLayerSet = Photoshop.Helpers.FindLayerGroupByPath(this.Document, this.LayerGroup.Material);

                if (materialLayerSet != null)
                {
                    // Move DO NOT EXPORT layers.
                    foreach (ArtLayer layer in materialLayerSet.ArtLayers)
                    {
                        if (layer.Name.StartsWith("DO NOT"))
                        {
                            layer.Move(tempLayerSet, ElementPlacement.PlaceInside);
                        }
                    }

                    // Migrate materials to new format.
                    foreach (LayerSet surfaceGroup in materialLayerSet.LayerSets.Reverse<LayerSet>())
                    {
                        bool migrated = false;

                        foreach (ArtLayer surfaceLayer in surfaceGroup.ArtLayers.Reverse<ArtLayer>())
                        {
                            Surface surface = SurfaceManager.GetSurface(surfaceGroup.Name, surfaceLayer.Name);

                            if (surface != null)
                            {
                                this.Document.ActiveLayer = materialLayerSet;

                                // Create layer set.
                                LayerSet surfaceLayerSet = materialLayerSet.LayerSets.Add();
                                surfaceLayerSet.Name = surface.FullName;
                                this.SetLayerUIColor(surfaceLayerSet);

                                // Create solid color layer.
                                SolidColor clr = RSG.Pipeline.PBR.Photoshop.Helpers.GetFillLayerColor(surfaceLayer);
                                this.Document.ActiveLayer = surfaceLayerSet;
                                ArtLayer newSurfaceLayer = RSG.Pipeline.PBR.Photoshop.Helpers.CreateFillLayer(this.Document, clr.RGB.Red, clr.RGB.Green, clr.RGB.Blue, false);
                                newSurfaceLayer.Name = surface.FullName;
                                RSG.Pipeline.PBR.Photoshop.Helpers.SelectLayerMask();
                                RSG.Pipeline.PBR.Photoshop.Helpers.DeleteLayerMask();

                                // Copy layer mask.
                                this.Document.ActiveLayer = surfaceLayer;
                                Photoshop.Helpers.SelectLayerMask();
                                Photoshop.Helpers.CreateSelectionFromLayerMask();
                                this.Document.ActiveLayer = surfaceLayerSet;
                                Photoshop.Helpers.CreateLayerMask(LayerMaskType.RevealSelection);
                                
                                // Create modifier layer set.
                                this.Document.ActiveLayer = surfaceLayerSet;
                                LayerSet modifierLayerSet = surfaceLayerSet.LayerSets.Add();
                                modifierLayerSet.BlendMode = BlendMode.NormalBlend;
                                modifierLayerSet.Name = "Modifier";

                                migrated = true;
                            }
                        }

                        // Delete layer group.
                        if (migrated)
                        {
                            surfaceGroup.Delete();
                        }
                    }
                }

                // Rename obsolete modifiers layerset.
                LayerSet modifiersLayerSet = Photoshop.Helpers.FindLayerGroupByPath(this.Document, this.LayerGroup.Modifiers);

                if (modifiersLayerSet != null)
                {
                    modifiersLayerSet.Name += " [OBSOLETE - PLEASE MIGRATE TO EACH MATERIAL]";
                }

                this._version = 2;
                this.Serialize();

                String msg = "Update complete!  Here are notes about what just happened:\n\n";
                msg += "- There is a new layer group named \"Temp [DOES NOT EXPORT]\".  Feel free to put whatever you'd like there, it won't participate in the export process.\n\n";
                msg += "- The top-level \"Modifiers\" layer group is now obsolete!  Each material surface now has its own \"Modifiers\" layer group, so you will need to manually migrate your modifiers appropriately.\n\n";
                msg += "- Feel free to delete anything marked as obsolete.  Also feel free to delete the \"DO NOT PAINT - FOR EXPORT ONLY\" layers.  Those aren't necessary any longer.";

                MessageBox.Show(msg, "Photoshop PBR - Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        public virtual void VersionUpdate()
        {
            this.HandleVersion1Update();

            String versionXml = String.Format("{0}\\techart\\etc\\config\\material\\masterMaterialDocumentVersions.xml", _config.ToolsRoot);

            if (File.Exists(versionXml))
            {
                Dictionary<int, XmlNode> versionsToUpdate = new Dictionary<int, XmlNode>();

                // Load versions.
                XmlDocument doc = new XmlDocument();
                doc.Load(versionXml);

                XmlNodeList versionNodes = doc.GetElementsByTagName("MaterialMaterialDocumentVersion");

                // Determine which versions to update first.  Then sort them to ensure we are updating in the correct order.
                foreach (XmlNode versionNode in versionNodes)
                {
                    XmlNode docVersion = versionNode.Attributes.GetNamedItem("Version");

                    if (docVersion != null)
                    {
                        int version = Convert.ToInt32(docVersion.Value);

                        // Only update if there is a newer version than the current version of the document.
                        if (version > this._version)
                        {
                            versionsToUpdate.Add(version, versionNode);
                        }
                    }
                }

                // Order the versions.  Want to ensure we update in the correct order.
                List<int> orderedVersions = versionsToUpdate.Keys.ToList();
                orderedVersions.Sort();

                foreach (int version in orderedVersions)
                {
                    XmlNode versionNode = versionsToUpdate[version];

                    if (versionNode != null)
                    {
                        // Deal with surface name changes.
                        XmlNodeList surfaceNameChangeNodes = versionNode.SelectNodes("SurfaceNameChanges//SurfaceNameChange");

                        foreach (XmlNode surfaceNameChangeNode in surfaceNameChangeNodes)
                        {
                            String oldName = surfaceNameChangeNode.Attributes.GetNamedItem("Old").Value;
                            String newName = surfaceNameChangeNode.Attributes.GetNamedItem("New").Value;

                            if (this.UpdateSurfaceName(oldName, newName))
                            {
                                // Update the document version.
                                this.Version = version;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Export the Photoshop document.
        /// </summary>
        public virtual String[] Export(ExportOptions options)
        {
            this.Serialize();

            List<string> exportedFiles = new List<string>();

            if (RSG.Pipeline.PBR.Photoshop.CheckoutFile(options.FullName))
            {
                // Check for resolution.
                if (this._doc.Resolution != 72)
                {
                    if (!options.Quiet)
                    {
                        MessageBox.Show("The resolution of \"" + options.Name + "\" is not 72!  It is currently set to " + this._doc.Resolution.ToString() + ".  Skipping export of this document until the resolution is fixed.", "Photoshop PBR");
                    }
                    return null;
                }

                // Check to make sure document is a Photoshop PSD file.
                if (!options.FullName.ToLower().EndsWith(".psd"))
                {
                    if (!options.Quiet)
                    {
                        MessageBox.Show("Can only export from a .PSD file!  Skipping export of this document.", "Photoshop PBR");
                    }
                    return null;
                }

                // Start export.

                //_ulog.MessageCtx("Export", "Exporting document " + doc.FullName);

                // Ensure document is at 16-bits per channel.
                this.ConvertTo16bpp();

                // Save backup.
                if (options.SaveBackup)
                {
                    RSG.Pipeline.PBR.Photoshop.SaveBackup(this._doc);
                }

                // Ensure export directory exists.
                string exportDir = options.Path;

                if (options.UseDocumentAsExportPath == true)
                {
                    exportDir = System.IO.Path.GetDirectoryName(options.FullName);
                    options.Path = exportDir;
                }

                Directory.CreateDirectory(exportDir);

                bool doResize = this.ExportWidth != this._doc.Width || this.ExportHeight != this._doc.Height;

                String alphaMapFilename = null;

                // Iterate over each map output and export.
                foreach (IMapOutput output in this._outputs.Values)
                {
                    String mapResult = output.Export(this._doc, options);

                    if (mapResult != null)
                    {
                        exportedFiles.Add(mapResult);

                        // Record the exported alpha map, to be packed later into the albedo map for live update.
                        if (output.Name == TextureNamingConvention.Alpha.Name)
                        {
                            alphaMapFilename = mapResult;
                        }

                        if (doResize || options.LiveUpdate)
                        {
                            Document resultDoc = RSG.Pipeline.PBR.Photoshop.Application.Open(mapResult);

                            // Resize if necessary.
                            if (doResize)
                            {
                                resultDoc.ResizeImage(this.ExportWidth, this.ExportHeight, 72.0, ResampleMethod.Bicubic, 1.0);
                                resultDoc.Save();
                            }

                            // Export live update texture for the game.
                            if (options.LiveUpdate)
                            {
                                if (output.Name == TextureNamingConvention.Albedo.Name)
                                {
                                    if (alphaMapFilename != null)
                                    {
                                        output.AlphaMapFilename = alphaMapFilename;
                                    }
                                }

                                output.ExportLiveUpdateMap(resultDoc, options);
                            }

                            resultDoc.Close();
                            RSG.Pipeline.PBR.Photoshop.Application.ActiveDocument = this._doc;
                        }
                    }
                }
            }

            return exportedFiles.ToArray();
        }

        /// <summary>
        /// Convert this master material document to another type.
        /// </summary>
        /// <param name="toType">Convert to type.</param>
        public virtual void ConvertToType(MasterMaterialDocumentType toType)
        {
            this._type = toType;
            this.Setup(true);
            this.Serialize();
        }

        /// <summary>
        /// Get a collection of Surface objects for each surface in the document.
        /// </summary>
        /// <returns></returns>
        public virtual List<Surface> GetSurfaces()
        {
            Dictionary<String, Surface> surfaces = new Dictionary<String, Surface>();
            Helpers helpers = new Helpers(RSG.Pipeline.PBR.Photoshop.Application);

            if (this._layerGroup != null)
            {
                LayerSet materialLayerSet = this.GetMaterialLayerSet();

                if (materialLayerSet != null)
                {
                    foreach (LayerSet surfaceLayer in materialLayerSet.LayerSets)
                    {
                        Surface surface = SurfaceManager.GetSurface(surfaceLayer.Name);

                        if (surface != null)
                        {
                            surface.RootLayer = surfaceLayer;
                            surface.Layer = surfaceLayer.ArtLayers[0];

                            // If no Roughness layerset exists, create it.
                            if (surfaceLayer.LayerSets.Count == 0)
                            {
                                LayerSet roughnessLayerSet = surfaceLayer.LayerSets.Add();
                                roughnessLayerSet.Name = "Modifier";
                            }

                            surface.RoughnessModifierLayer = surfaceLayer.LayerSets[0];
                            surfaces[surface.FullName] = surface;
                        }
                    }
                }
            }

            return surfaces.Values.ToList();
        }

        /// <summary>
        /// Selects the ArtLayer corresponding to the supplied surface.  If the ArtLayer doesn't exist,
        /// one will be created.
        /// </summary>
        /// <param name="groupName"></param>
        /// <param name="surfaceName"></param>
        /// <returns></returns>
        public virtual LayerSet SelectSurface(String groupName, String surfaceName)
        {
            Surface surface = SurfaceManager.GetSurface(groupName, surfaceName);

            if (surface != null)
            {
                return SelectSurface(surface);
            }

            return null;
        }

        /// <summary>
        /// Selects the ArtLayer corresponding to the supplied surface.  If the ArtLayer doesn't exist,
        /// one will be created.
        /// </summary>
        /// <param name="surface"></param>
        /// <returns></returns>
        public virtual LayerSet SelectSurface(Surface surface)
        {
            Helpers helpers = new Helpers(RSG.Pipeline.PBR.Photoshop.Application);

            LayerSet materialLayerGroup = this.GetMaterialLayerSet();
            //ArtLayer surfaceLayer = this.GetSurfaceArtLayer(surface);
            LayerSet surfaceLayerSet = this.GetSurfaceLayerSet(surface);

            if (surfaceLayerSet != null)
            {
                this.Document.ActiveLayer = surfaceLayerSet;
                helpers.SelectLayerMask();
            }
            else
            {
                // If path object active
                PathItem pathItem = helpers.GetActivePath(this.Document);

                if (pathItem != null)
                {
                    pathItem.MakeSelection(0);
                }

                // The type of layer mask to create.
                LayerMaskType maskType = LayerMaskType.HideAll;

                // If there is a current selection, reveal the selection.
                try
                {
                    var hasSelection = this.Document.Selection.Bounds;
                    maskType = LayerMaskType.RevealSelection;
                }
                catch { }

                this.Document.ActiveLayer = materialLayerGroup;

                // Create layer set for the surface.
                surfaceLayerSet = materialLayerGroup.LayerSets.Add();
                surfaceLayerSet.Name = surface.FullName;
                this.SetLayerUIColor(surfaceLayerSet);

                helpers.CreateLayerMask(maskType);

                ArtLayer surfaceLayer = helpers.CreateFillLayer(this.Document, surface.MaskColor.R, surface.MaskColor.G, surface.MaskColor.B, false);
                surfaceLayer.Name = surface.FullName;
                RSG.Pipeline.PBR.Photoshop.Helpers.SelectLayerMask();
                RSG.Pipeline.PBR.Photoshop.Helpers.DeleteLayerMask();

                // Create layer set for the modifier.
                this.SelectSurfaceModifier(surface);

                // Create metal albedo layer.
                if (surface.IsMetal)
                {
                    // Only create this layer if there is an albedo color for it that isn't pure black.
                    if (surface.AlbedoColor.R != 0 && surface.AlbedoColor.G != 0 && surface.AlbedoColor.B != 0)
                    {
                        helpers.CreateSelectionFromLayerMask();

                        LayerSet albedoLayerSet = helpers.FindLayerGroupByPath(this.Document, this.LayerGroup.Albedo);

                        if (albedoLayerSet != null)
                        {
                            this.Document.ActiveLayer = albedoLayerSet;

                            ArtLayer metalAlbedoLayer = helpers.CreateFillLayer(this.Document, surface.AlbedoColor.R, surface.AlbedoColor.G, surface.AlbedoColor.B, false);
                            metalAlbedoLayer.Name = String.Format("{0} Metal Albedo", surface.FullName);
                        }
                    }
                }
            }

            // Select the surface layer mask.
            this.Document.ActiveLayer = surfaceLayerSet;
            helpers.SelectLayerMask();

            // Set foreground color to white.
            SolidColor white = new SolidColor();
            white.RGB.Red = 255;
            white.RGB.Green = 255;
            white.RGB.Blue = 255;

            RSG.Pipeline.PBR.Photoshop.Application.ForegroundColor = white;

            // We created a new surface, so serialize it to the document.
            this.Serialize();

            return surfaceLayerSet;
        }

        /// <summary>
        /// Get the Material LayerSet object.
        /// </summary>
        /// <returns></returns>
        public virtual LayerSet GetMaterialLayerSet()
        {
            Helpers helpers = new Helpers(RSG.Pipeline.PBR.Photoshop.Application);
            return helpers.FindLayerGroupByPath(this.Document, this.LayerGroup.Material);
        }

        /// <summary>
        /// Get the LayerSet for a surface.
        /// </summary>
        /// <param name="surface"></param>
        /// <returns></returns>
        public virtual LayerSet GetSurfaceLayerSet(Surface surface)
        {
            Helpers helpers = new Helpers(RSG.Pipeline.PBR.Photoshop.Application);
            return helpers.FindLayerGroupByPath(this.Document, String.Format("{0}/{1}", this.LayerGroup.Material, surface.FullName), false);
        }

        /// <summary>
        /// Get the LayerSet for a surface modifier.
        /// </summary>
        /// <param name="surface"></param>
        /// <returns></returns>
        public virtual LayerSet GetSurfaceModifierLayerSet(Surface surface)
        {
            Helpers helpers = new Helpers(RSG.Pipeline.PBR.Photoshop.Application);
            return helpers.FindLayerGroupByPath(this.Document, String.Format("{0}/{1}/Modifier", this.LayerGroup.Material, surface.FullName), false);
        }

        /// <summary>
        /// Select the modifier LayerSet for a surface. If one doesn't exist, it will be created.
        /// </summary>
        /// <param name="surface"></param>
        /// <returns></returns>
        public virtual LayerSet SelectSurfaceModifier(Surface surface)
        {
            LayerSet surfaceModifier = this.GetSurfaceModifierLayerSet(surface);

            if (surfaceModifier == null)
            {
                LayerSet surfaceLayerSet = this.GetSurfaceLayerSet(surface);
                surfaceModifier = surfaceLayerSet.LayerSets.Add();
                surfaceModifier.Name = "Modifier";
            }

            surfaceModifier.Visible = true;
            this.Document.ActiveLayer = surfaceModifier;

            return surfaceModifier;
        }

        /// <summary>
        /// Serialize metadata about this document to the Photoshop document.  Data is stored as an XML string in the Instructions slot
        /// of the Photoshop document.
        /// </summary>
        public virtual void Serialize()
        {
            String xml = String.Format("<{0}>\n", this.XML_ROOT);

            xml += String.Format("\t<{0}>{1}</{0}>\n", this.XML_DOC_TYPE, this._type);
            xml += String.Format("\t<{0}>{1}</{0}>\n", this.XML_VERSION, this._version);
            xml += String.Format("\t<{0}>{1}</{0}>\n", this.XML_EXPORT_WIDTH, this._exportWidth.ToString());
            xml += String.Format("\t<{0}>{1}</{0}>\n", this.XML_EXPORT_HEIGHT, this._exportHeight.ToString());
            xml += String.Format("\t<{0}>{1}</{0}>\n", this.XML_LAST_LAYER_UI_COLOR, this._currentLayerUIColor.ToString());

            // Write out surfaces.
            xml += "\t<Surfaces>\n";

            List<Surface> surfaces = this.GetSurfaces();

            foreach (Surface surface in surfaces)
            {
                xml += String.Format("\t\t<Surface>{0}</Surface>\n", surface.FullName);
            }

            xml += "\t</Surfaces>\n";

            // End root tag.
            xml += String.Format("</{0}>\n", this.XML_ROOT);

            this._doc.Metadata = xml;
        }

        /// <summary>
        /// Deserialize metadata in the Photoshop document to this document.  Data is stored as an XML string in the Instructions slot
        /// of the Photoshop document.
        /// </summary>
        public virtual void Deserialize()
        {
            bool serialize = false;

            String xml = this._doc.Metadata;

            XDocument xmlDoc = XDocument.Parse(xml);

            this._type = (MasterMaterialDocumentType)Enum.Parse(typeof(MasterMaterialDocumentType), xmlDoc.Root.Element(XML_DOC_TYPE).Value);

            // Current layer ui color.
            XElement currentUIColor = xmlDoc.Root.Element(XML_LAST_LAYER_UI_COLOR);

            if (currentUIColor != null)
            {
                this._currentLayerUIColor = (int)currentUIColor;
            }

            // Get export width and height.
            XElement exportWidth = xmlDoc.Root.Element(XML_EXPORT_WIDTH);
            XElement exportHeight = xmlDoc.Root.Element(XML_EXPORT_HEIGHT);

            if (exportWidth != null)
            {
                if (!this.IsPowerOfTwo((ulong)exportWidth))
                {
                    this._exportWidth = (int)this._doc.Width;
                    serialize = true;
                }
                else
                {
                    this._exportWidth = Convert.ToInt32(xmlDoc.Root.Element(XML_EXPORT_WIDTH).Value);
                }
            }
            else
            {
                this._exportWidth = (int)this._doc.Width;
                serialize = true;
            }

            if (exportHeight != null)
            {
                if (!this.IsPowerOfTwo((ulong)exportHeight))
                {
                    this._exportHeight = (int)this._doc.Height;
                    serialize = true;
                }
                else
                {
                    this._exportHeight = Convert.ToInt32(xmlDoc.Root.Element(XML_EXPORT_HEIGHT).Value);
                }
            }
            else
            {
                this._exportHeight = (int)this._doc.Height;
                serialize = true;
            }

            // Document version
            XElement version = xmlDoc.Root.Element(XML_VERSION);

            if (version != null)
            {
                this._version = Convert.ToDouble(xmlDoc.Root.Element(XML_VERSION).Value);
            }
            else
            {
                this._version = 1;
            }

            // Set the name of the document type to the enum.
            this._name = this._type.ToString();

            // Nothing has been serialized to the document yet, so do it now.
            if (serialize)
            {
                this.Serialize();
            }
        }

        #endregion // Virtual Methods

        #region Methods

        /// <summary>
        /// Test if a number is power of two.
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        private bool IsPowerOfTwo(ulong x)
        {
            return (x != 0) && ((x & (x - 1)) == 0);
        }

        /// <summary>
        /// Converts the document to 16-bits per pixel.
        /// </summary>
        public void ConvertTo16bpp()
        {
            if (this.Document.BitsPerChannel != BitsPerChannelType.Document16Bits)
            {
                this.Document.BitsPerChannel = BitsPerChannelType.Document16Bits;
            }
        }

        /// <summary>
        /// Add a new IMapOutput to the document.
        /// </summary>
        public void AddOutput(IMapOutput output)
        {
            if (!this._outputs.ContainsKey(output.Name))
            {
                this._outputs.Add(output.Name, output);
            }
        }

        /// <summary>
        /// Remove the IMapOutput from the document.
        /// </summary>
        public void RemoveOutput(String outputName)
        {
            if (this._outputs.ContainsKey(outputName))
            {
                this._outputs.Remove(outputName);
            }
        }

        public bool IsRoughnessModifierLayer(ArtLayer layer)
        {
            return layer.Name.StartsWith(SurfaceManager.ModifierPrefix);
        }

        /// <summary>
        /// Display all material ArtLayer's.
        /// </summary>
        public void DisplayAllMaterials()
        {
            if (RSG.Pipeline.PBR.Photoshop.Application.IsConnected())
            {
                Helpers helpers = new Helpers(RSG.Pipeline.PBR.Photoshop.Application);
                helpers.SetLayerGroupVisibilityByPath(this.Document, this.LayerGroup.Material, true);

                List<Surface> surfaces = this.GetSurfaces();

                foreach (Surface surface in surfaces)
                {
                    surface.Layer.Visible = true;
                    surface.RoughnessModifierLayer.Visible = true;
                }

                // Hide background layer.
                foreach (ArtLayer layer in this.Document.ArtLayers)
                {
                    if (layer.IsBackgroundLayer == true)
                    {
                        layer.Visible = false;
                    }
                }
            }
        }

        /// <summary>
        /// Display only the Albedo layer set.
        /// </summary>
        public void DisplayAlbedo()
        {
            Helpers helpers = new Helpers(RSG.Pipeline.PBR.Photoshop.Application);
            helpers.SetLayerGroupVisibilityByPath(this.Document, this.LayerGroup.Albedo, true);
        }

        /// <summary>
        /// Display only the Alpha layer set.
        /// </summary>
        public void DisplayAlpha()
        {
            Helpers helpers = new Helpers(RSG.Pipeline.PBR.Photoshop.Application);
            helpers.SetLayerGroupVisibilityByPath(this.Document, this.LayerGroup.Alpha, true);
        }

        /// <summary>
        /// Display only the Ambient Occlusion layer set.
        /// </summary>
        public void DisplayAmbientOcclusion()
        {
            Helpers helpers = new Helpers(RSG.Pipeline.PBR.Photoshop.Application);
            helpers.SetLayerGroupVisibilityByPath(this.Document, this.LayerGroup.AmbientOcclusion, true);
        }

        /// <summary>
        /// Display only the Height layer set.
        /// </summary>
        public void DisplayHeight()
        {
            Helpers helpers = new Helpers(RSG.Pipeline.PBR.Photoshop.Application);
            helpers.SetLayerGroupVisibilityByPath(this.Document, this.LayerGroup.Height, true);
        }

        /// <summary>
        /// Display only the Translucency layer set.
        /// </summary>
        public void DisplayTranslucency()
        {
            Helpers helpers = new Helpers(RSG.Pipeline.PBR.Photoshop.Application);
            helpers.SetLayerGroupVisibilityByPath(this.Document, this.LayerGroup.Translucency, true);
        }

        /// <summary>
        /// Display only the Normal layer set.
        /// </summary>
        public void DisplayNormal()
        {
            Helpers helpers = new Helpers(RSG.Pipeline.PBR.Photoshop.Application);
            helpers.SetLayerGroupVisibilityByPath(this.Document, this.LayerGroup.Normal, true);
        }

        /// <summary>
        /// Display only the Emissive layer set.
        /// </summary>
        public void DisplayEmissive()
        {
            Helpers helpers = new Helpers(RSG.Pipeline.PBR.Photoshop.Application);
            helpers.SetLayerGroupVisibilityByPath(this.Document, this.LayerGroup.Emissive, true);
        }

        /// <summary>
        /// Display only roughness ArtLayer's.
        /// </summary>
        public void DisplayRoughness()
        {
            if (RSG.Pipeline.PBR.Photoshop.Application.IsConnected())
            {
                Helpers helpers = new Helpers(RSG.Pipeline.PBR.Photoshop.Application);
                helpers.SetLayerGroupVisibilityByPath(this.Document, this.LayerGroup.Material, true);

                List<Surface> surfaces = this.GetSurfaces();

                foreach (Surface surface in surfaces)
                {
                    surface.Layer.Visible = true;
                    surface.RoughnessModifierLayer.Visible = false;
                }
            }
        }

        /// <summary>
        /// Display only modifier ArtLayer's.
        /// </summary>
        public void DisplayModifier()
        {
            if (RSG.Pipeline.PBR.Photoshop.Application.IsConnected())
            {
                Helpers helpers = new Helpers(RSG.Pipeline.PBR.Photoshop.Application);
                helpers.SetLayerGroupVisibilityByPath(this.Document, this.LayerGroup.Material, true);

                List<Surface> surfaces = this.GetSurfaces();

                foreach (Surface surface in surfaces)
                {
                    try
                    {
                        surface.Layer.Visible = true;
                        surface.RoughnessModifierLayer.Visible = true;
                        surface.RoughnessModifierLayer.Opacity = 100.0;
                        surface.RoughnessModifierLayer.BlendMode = BlendMode.NormalBlend;
                    }
                    catch { }
                }
            }
        }

        /// <summary>
        /// Display only metalness ArtLayer's.
        /// </summary>
        public void DisplayMetalness()
        {
            if (RSG.Pipeline.PBR.Photoshop.Application.IsConnected())
            {
                Helpers helpers = new Helpers(RSG.Pipeline.PBR.Photoshop.Application);
                helpers.SetLayerGroupVisibilityByPath(this.Document, this.LayerGroup.Material, true);

                List<Surface> surfaces = this.GetSurfaces();

                foreach (Surface surface in surfaces)
                {
                    if (surface.IsMetal == true)
                    {
                        surface.Layer.Visible = true;
                    }
                    else
                    {
                        surface.Layer.Visible = false;
                    }

                    surface.RoughnessModifierLayer.Visible = false;
                }
            }
        }

        /// <summary>
        /// Display only detail map intensity ArtLayer's.
        /// </summary>
        public void DisplayDetailMapIntensity()
        {
            if (RSG.Pipeline.PBR.Photoshop.Application.IsConnected())
            {
                Helpers helpers = new Helpers(RSG.Pipeline.PBR.Photoshop.Application);
                helpers.SetLayerGroupVisibilityByPath(this.Document, this.LayerGroup.DetailMapsIntensity, true);
            }
        }

        /// <summary>
        /// Display only detail map index ArtLayer's.
        /// </summary>
        public void DisplayDetailMapIDs()
        {
            if (RSG.Pipeline.PBR.Photoshop.Application.IsConnected())
            {
                Helpers helpers = new Helpers(RSG.Pipeline.PBR.Photoshop.Application);
                helpers.SetLayerGroupVisibilityByPath(this.Document, this.LayerGroup.DetailMapsID, true);
            }
        }

        /// <summary>
        /// Display only sheen ArtLayer's.
        /// </summary>
        public void DisplaySheen()
        {
            if (RSG.Pipeline.PBR.Photoshop.Application.IsConnected())
            {
                Helpers helpers = new Helpers(RSG.Pipeline.PBR.Photoshop.Application);
                helpers.SetLayerGroupVisibilityByPath(this.Document, this.LayerGroup.Material, true);

                List<Surface> surfaces = this.GetSurfaces();

                foreach (Surface surface in surfaces)
                {
                    if (surface.IsSheen == true)
                    {
                        surface.Layer.Visible = true;
                    }
                    else
                    {
                        surface.Layer.Visible = false;
                    }

                    surface.RoughnessModifierLayer.Visible = false;
                }
            }
        }

        /// <summary>
        /// Set the solid color fill layer for all surfaces to their roughness color.
        /// </summary>
        public void SetLayerFillColorsToRoughness()
        {
            List<Surface> surfaces = this.GetSurfaces();

            foreach (Surface surface in surfaces)
            {
                try
                {
                    surface.Layer.Opacity = 100;
                }
                catch { }

                RSG.Pipeline.PBR.Photoshop.Helpers.SetFillLayerColor(surface.Layer, surface.Roughness);
            }
        }

        /// <summary>
        /// Set the solid color fill layer for all surfaces to their metalness color.
        /// </summary>
        public void SetLayerFillColorsToMetalness()
        {
            List<Surface> surfaces = this.GetSurfaces();

            foreach (Surface surface in surfaces)
            {
                try
                {
                    surface.Layer.Opacity = 100;
                }
                catch { }

                RSG.Pipeline.PBR.Photoshop.Helpers.SetFillLayerColor(surface.Layer, surface.Metalness);
            }
        }

        /// <summary>
        /// Set the solid color fill layer for all surfaces to their sheen color.
        /// </summary>
        public void SetLayerFillColorsToSheen()
        {
            List<Surface> surfaces = this.GetSurfaces();

            foreach (Surface surface in surfaces)
            {
                try
                {
                    surface.Layer.Opacity = 100;
                }
                catch { }

                RSG.Pipeline.PBR.Photoshop.Helpers.SetFillLayerColor(surface.Layer, surface.Sheen);
            }
        }

        /// <summary>
        /// Set the opacity of the roughness.
        /// </summary>
        /// <param name="opacity"></param>
        public void SetRoughnessOpacity(double opacity)
        {
            List<Surface> surfaces = this.GetSurfaces();

            foreach (Surface surface in surfaces)
            {
                try
                {
                    surface.RootLayer.Visible = true;
                    surface.RootLayer.Opacity = opacity;
                }
                catch { }
            }
        }

        /// <summary>
        /// Set the opacity of the roughness.
        /// </summary>
        /// <param name="opacity"></param>
        public void SetModifierOpacity(double opacity)
        {
            List<Surface> surfaces = this.GetSurfaces();

            foreach (Surface surface in surfaces)
            {
                try
                {
                    surface.RoughnessModifierLayer.Visible = true;
                    surface.RoughnessModifierLayer.Opacity = opacity;
                }
                catch { }
            }
        }

        /// <summary>
        /// Set the solid color fill layer for all surfaces to the channel mask color.
        /// </summary>
        public void SetLayerFillColorsChannelMask()
        {
            List<Surface> surfaces = this.GetSurfaces();

            foreach (Surface surface in surfaces)
            {
                try
                {
                    surface.Layer.Opacity = 100;
                }
                catch { }

                RSG.Pipeline.PBR.Photoshop.Helpers.SetFillLayerColor(surface.Layer, surface.MaskColor);
            }
        }

        /// <summary>
        /// Select a detail map.
        /// </summary>
        /// <param name="detailMap"></param>
        public void SelectDetailMap(DetailMap detailMap)
        {
            if (RSG.Pipeline.PBR.Photoshop.Application.IsConnected())
            {
                ArtLayer detailMapLayer = RSG.Pipeline.PBR.Photoshop.Helpers.FindLayerByPath(this.Document, this.LayerGroup.DetailMapsID + "/" + detailMap.LayerName, false);

                LayerSet detailMapsLayerSet = RSG.Pipeline.PBR.Photoshop.Helpers.FindLayerGroupByPath(this.Document, this.LayerGroup.DetailMaps);
                LayerSet detailMapsIDLayerSet = RSG.Pipeline.PBR.Photoshop.Helpers.FindLayerGroupByPath(this.Document, this.LayerGroup.DetailMapsID);
                LayerSet detailMapsIntensityLayerSet = RSG.Pipeline.PBR.Photoshop.Helpers.FindLayerGroupByPath(this.Document, this.LayerGroup.DetailMapsIntensity);

                if (detailMapsLayerSet == null)
                {
                    detailMapsLayerSet = this.Document.LayerSets.Add();
                    detailMapsLayerSet.Name = this.LayerGroup.DetailMaps;
                }

                if (detailMapsIDLayerSet == null)
                {
                    detailMapsIDLayerSet = detailMapsLayerSet.LayerSets.Add();
                    detailMapsIDLayerSet.Name = "ID";
                }

                if (detailMapsIntensityLayerSet == null)
                {
                    detailMapsIntensityLayerSet = detailMapsLayerSet.LayerSets.Add();
                    detailMapsIntensityLayerSet.Name = "Intensity";
                }

                if (detailMapLayer != null)
                {
                    this.Document.ActiveLayer = detailMapLayer;
                    RSG.Pipeline.PBR.Photoshop.Helpers.SelectLayerMask();
                }
                else
                {
                    this.Document.ActiveLayer = detailMapsIDLayerSet;

                    SolidColor layerClr = new SolidColor();
                    HSBColor hsbClr = new HSBColor();
                    hsbClr.Brightness = detailMap.Value;
                    layerClr.HSB = hsbClr;

                    detailMapLayer = RSG.Pipeline.PBR.Photoshop.Helpers.CreateFillLayer(this.Document, layerClr.RGB.Red, layerClr.RGB.Green, layerClr.RGB.Blue);
                    detailMapLayer.Name = detailMap.LayerName;
                }
            }
        }

        #endregion // Methods
    }
}
