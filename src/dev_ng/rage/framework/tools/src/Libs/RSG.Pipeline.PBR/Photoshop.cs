﻿using System;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Forms;
using System.Threading;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;

using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.Configuration;
using RSG.SourceControl.Perforce;

using RSG.Interop.Adobe.Photoshop;
using RSG.Interop.Substance;

using RSG.Pipeline.PBR;
using RSG.Pipeline.PBR.Documents;

namespace RSG.Pipeline.PBR
{
    /// <summary>
    /// Main class for the Photoshop PBR pipeline.
    /// </summary>
    public static class Photoshop
    {
        #region Member Data

        private static RSG.Interop.Adobe.Photoshop.Application _app = new RSG.Interop.Adobe.Photoshop.Application();
        private static RSG.Interop.Adobe.Photoshop.Helpers _helpers = new RSG.Interop.Adobe.Photoshop.Helpers(_app);

        private static IConfig _config = ConfigFactory.CreateConfig();
        private static readonly String _ulogViewer = _config.ToolsBin + "\\UniversalLogViewer\\UniversalLogViewer.exe";

        private static bool _initialized = false;
        private static bool _useSourceControl = true;

        //private static Dictionary<ArtLayer, bool> _layers = new Dictionary<ArtLayer, bool>();
        //private static Dictionary<LayerSet, bool> _layerSets = new Dictionary<LayerSet, bool>();

        private static RSG.SourceControl.Perforce.P4 _p4 = new RSG.SourceControl.Perforce.P4();

        private static RSG.Pipeline.PBR.Backup _backup = new RSG.Pipeline.PBR.Backup();
        private static RSG.Base.Logging.Universal.IUniversalLog _ulog;

        #endregion // Member Data

        #region Properties

        public static RSG.Interop.Adobe.Photoshop.Application Application
        {
            get { return _app; }
        }

        public static RSG.Interop.Adobe.Photoshop.Helpers Helpers
        {
            get { return _helpers; }
        }

        public static bool Initialized
        {
            get { return _initialized; }
        }

        public static bool UseSourceControl
        {
            get { return _useSourceControl; }
            set { _useSourceControl = value; }
        }

        public static RSG.Pipeline.PBR.Backup Backup
        {
            get { return _backup; }
        }

        #endregion // Properties

        #region Generic Methods

        public static void Initialize()
        {
            if (_app.IsConnected())
            {
                RSG.Base.Logging.LogFactory.Initialize();
                RSG.Base.Logging.LogFactory.CreateApplicationConsoleLogTarget();
                _ulog = RSG.Base.Logging.LogFactory.ApplicationLog;
                RSG.Base.Logging.LogFactory.ApplicationShutdown();
                _ulog.Name = "Photoshop PBR";

                // Ensure the cache directory exists.
                Directory.CreateDirectory(Export.TempExportDirectory);

                string namingConventionsXml = _config.ToolsRoot + "\\techart\\etc\\config\\material\\namingConventions.xml";
                string colorSettingsFile = _config.ToolsRoot + "\\techart\\etc\\config\\material\\RockstarGamesPBR.csf";

                // Get latest naming conventions xml.
                if (_useSourceControl)
                {
                    if (_p4.IsValidConnection(true, true))
                    {
                        _p4.Connect();
                    }
                    else
                    {
                        MessageBox.Show("Could not connect to Perforce!  Please make sure you are logged in.\n\nQuitting.", "Photoshop PBR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Environment.Exit(0);
                    }

                    try
                    {
                        _p4.Run("sync", namingConventionsXml);
                        _p4.Run("sync", colorSettingsFile);
                        _p4.Run("sync", SurfaceManager.ImagesRoot + "\\...");
                    }
                    catch { }
                }

                // Load color settings.
                if (File.Exists(colorSettingsFile))
                {
                    _helpers.LoadColorSettings(colorSettingsFile);
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show("Could not load color settings!  Please ensure you have the following file:\n\n" + colorSettingsFile, "Photoshop PBR");
                }

                // Load detail map data.
                DetailMapManager.Load();

                // Init texture naming conventions.
                TextureNamingConvention.LoadFromConfig(namingConventionsXml);

                _backup.BackupName = "PhotoshopPBR";

                //_ulog.Message("Finished initializing PBR pipeline.");
                _ulog.MessageCtx("Initialize", "Finished initializing Photoshop PBR Pipeline.");
                _initialized = true;
            }
        }

        public static void Quit(bool save = false)
        {
            RSG.Interop.Adobe.Photoshop.SaveOptions saveOptions = RSG.Interop.Adobe.Photoshop.SaveOptions.DoNotSaveChanges;

            if (save)
            {
                saveOptions = RSG.Interop.Adobe.Photoshop.SaveOptions.SaveChanges;
            }

            foreach (Document doc in _app.Documents)
            {
                doc.Close(saveOptions);
            }

            _app.Quit();
            _initialized = false;
        }

        public static String PackMap(Document doc, ExportOptions options, string outputFilename, string redChannelFilename, string greenChannelFilename, string blueChannelFilename, string alphaChannelFilename)
        {
            // Pack map via Photoshop (slow...)
            Document matDoc = _app.Documents.Add(doc.Width, doc.Height, 72.0, "__PACKING_MAP__", NewDocumentMode.NewRGB, DocumentFill.White);

            // Red channel
            if (redChannelFilename != null)
            {
                Document redChannelDoc = _app.Open(redChannelFilename);
                _helpers.SetActiveChannels(redChannelDoc, true, false, false, false);
                redChannelDoc.Selection.SelectAll();
                redChannelDoc.Selection.Copy();
                redChannelDoc.Close();

                _app.ActiveDocument = matDoc;
                _helpers.SetActiveChannels(matDoc, true, false, false, false);
                matDoc.Paste(false);
            }

            // Green channel
            if (greenChannelFilename != null)
            {
                Document greenChannelDoc = _app.Open(greenChannelFilename);
                _helpers.SetActiveChannels(greenChannelDoc, false, true, false, false);
                greenChannelDoc.Selection.SelectAll();
                greenChannelDoc.Selection.Copy();
                greenChannelDoc.Close();

                _app.ActiveDocument = matDoc;
                _helpers.SetActiveChannels(matDoc, false, true, false, false);
                matDoc.Paste(false);
            }

            // Blue channel
            if (blueChannelFilename != null)
            {
                Document blueChannelDoc = _app.Open(blueChannelFilename);
                _helpers.SetActiveChannels(blueChannelDoc, false, false, true, false);
                blueChannelDoc.Selection.SelectAll();
                blueChannelDoc.Selection.Copy();
                blueChannelDoc.Close();

                _app.ActiveDocument = matDoc;
                _helpers.SetActiveChannels(matDoc, false, false, true, false);
                matDoc.Paste(false);
            }

            // Pack translucency
            bool saveAlpha = false;

            if (alphaChannelFilename != null)
            {
                _app.ActiveDocument = matDoc;

                Channel alphaChannel = matDoc.Channels.Add();
                alphaChannel.Name = "Alpha";

                Document alphaChannelDoc = _app.Open(alphaChannelFilename);
                _helpers.SetActiveChannels(alphaChannelDoc, false, false, false, true);
                alphaChannelDoc.Selection.SelectAll();
                alphaChannelDoc.Selection.Copy();
                alphaChannelDoc.Close();

                _app.ActiveDocument = matDoc;
                _helpers.SetActiveChannels(matDoc, false, false, false, true);
                matDoc.Paste(false);

                matDoc.Selection.Deselect();

                saveAlpha = true;
            }

            // Save
            TiffSaveOptions tiffOptions = new TiffSaveOptions();
            tiffOptions.Layers = false;
            tiffOptions.AlphaChannels = saveAlpha;

            string filename = options.Path + "\\" + outputFilename + ".tif";

            if (RSG.Pipeline.PBR.Photoshop.CheckoutFile(filename))
            {
                matDoc.SaveAs(filename, tiffOptions);
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("Could not save following file because it is not writable!\n\n" + filename, "Photoshop PBR");
            }

            matDoc.Close();
            _app.ActiveDocument = doc;

            return filename;
        }

        public static bool MakeFileWritable(string filename, bool quiet = false)
        {
            if (File.Exists(filename))
            {
                FileInfo fileInfo = new FileInfo(filename);

                if (fileInfo.IsReadOnly)
                {
                    if (quiet == false)
                    {
                        DialogResult result = System.Windows.Forms.MessageBox.Show("The current file is read-only!  Make writable?\n\n" + filename, "Photoshop PBR", MessageBoxButtons.YesNo);

                        if (result == DialogResult.Yes)
                        {
                            File.SetAttributes(filename, FileAttributes.Normal);
                        }
                    }
                    else
                    {
                        File.SetAttributes(filename, FileAttributes.Normal);
                    }
                }

                // Check to make sure file was set to writable.
                FileInfo newFileInfo = new FileInfo(filename);
                return !newFileInfo.IsReadOnly;
            }
            else
            {
                return true;
            }
        }

        public static bool CheckoutFile(string filename, bool quiet = false)
        {
            if (_useSourceControl)
            {
                if (_p4.Exists(filename))
                {
                    P4API.P4RecordSet recordSet = _p4.Run("edit", filename);
                }
                else
                {
                    string[] args = new string[3];

                    args[0] = "-t";
                    args[1] = "binary+m";
                    args[2] = filename;

                    _p4.Run("add", args);
                }
            }
            else
            {
                // Source control isn't enabled, so ask if the user wants to make the file writable.
                return MakeFileWritable(filename, quiet);
            }

            return true;
        }

        public static bool ConvertTifToPsd(Document doc, bool quiet = false)
        {
            if (doc.Name.EndsWith(".tif"))
            {
                PhotoshopSaveOptions options = new PhotoshopSaveOptions();

                string tifFilename = doc.FullName;

                string path = System.IO.Path.GetDirectoryName(tifFilename);
                string basename = System.IO.Path.GetFileNameWithoutExtension(tifFilename);

                string psdFilename = String.Format("{0}\\{1}.psd", path, basename);

                doc.SaveAs(psdFilename, options);

                // Add file to Perforce.
                if (UseSourceControl)
                {
                    try
                    {
                        RSG.SourceControl.Perforce.P4 p4 = new RSG.SourceControl.Perforce.P4();
                        p4.Connect();

                        // Add PSD to Perforce.
                        string[] args = new string[3];

                        args[0] = "-t";
                        args[1] = "binary+m";
                        args[2] = psdFilename;

                        p4.Run("add", args);

                        // Mark TIF for delete.
                        p4.Run("delete", tifFilename);
                        return true;
                    }
                    catch
                    {
                        if (!quiet)
                        {
                            System.Windows.Forms.MessageBox.Show("An unknown error occurred while attempting to add the PSD file, or delete the TIF file in Perforce!  The document has already been converted, so please add/delete the files manually.", "Photoshop PBR");
                        }

                        return false;
                    }
                }

                return true;
            }
            return false;
        }

        public static void SaveBackup(Document doc)
        {
            if (_app.IsConnected())
            {
                _backup.CreateBackup(doc);
            }
        }

        public static bool HasTranslucency(Document doc)
        {
            if (_app.IsConnected())
            {
                IMasterMaterialDocument matDoc = MasterMaterialDocumentFactory.FromPhotoshopDocument(doc);

                LayerSet translucencyLayerSet = _helpers.FindLayerGroupByPath(doc, matDoc.LayerGroup.Translucency);

                if (translucencyLayerSet.ArtLayers.Count > 0)
                {
                    foreach (ArtLayer layer in translucencyLayerSet.ArtLayers)
                    {
                        if (layer.Visible)
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        public static bool IsLayerGroupEmpty(Document doc, string layerGroupPath, LayerSet currentLayerSet = null)
        {
            LayerSet layerSet;

            if (currentLayerSet == null)
            {
                layerSet = _helpers.FindLayerGroupByPath(doc, layerGroupPath);
            }
            else
            {
                layerSet = currentLayerSet;
            }

            if (layerSet != null)
            {
                foreach (LayerSet childLayerSet in layerSet.LayerSets)
                {
                    bool isEmpty = IsLayerGroupEmpty(doc, layerGroupPath, childLayerSet);

                    if (!isEmpty)
                    {
                        return false;
                    }
                }

                return layerSet.ArtLayers.Count == 0;
            }

            return true;
        }

        

        #endregion // Generic

        #region Substance Methods

        

        

        

        

        #endregion // Substance Functions

        #region Layer Fill Color Methods

        

        public static Dictionary<ArtLayer, Color> StoreCurrentLayerFillColors(Document doc)
        {
            IMasterMaterialDocument matDoc = MasterMaterialDocumentFactory.FromPhotoshopDocument(doc);
            Dictionary<ArtLayer, Color> currentColors = new Dictionary<ArtLayer, Color>();
            List<Surface> surfaces = matDoc.GetSurfaces();

            foreach (Surface surface in surfaces)
            {
                SolidColor currentFillColor = _helpers.GetFillLayerColor(surface.Layer);
                Color clr = Color.FromArgb((int)currentFillColor.RGB.Red, (int)currentFillColor.RGB.Green, (int)currentFillColor.RGB.Blue);

                // Construct a path to the layer.
                string layerPath = matDoc.LayerGroup.Material + "/" + surface.Layer.Name;

                currentColors.Add(surface.Layer, clr);
            }

            return currentColors;
        }

        public static void RestoreLayerFillColors(Dictionary<ArtLayer, Color> colors)
        {
            // Restore layer colors.
            foreach (KeyValuePair<ArtLayer, Color> entry in colors)
            {
                _helpers.SetFillLayerColor(entry.Key, entry.Value);
            }
        }

        #endregion // Layer Fill Colors
    }
}

