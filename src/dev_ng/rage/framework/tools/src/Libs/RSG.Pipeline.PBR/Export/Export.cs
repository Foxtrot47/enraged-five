﻿using System;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Media.Imaging;
using System.Windows.Forms;
using System.Threading;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;

using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.Configuration;
using RSG.SourceControl.Perforce;

using RSG.Pipeline.PBR;
using RSG.Pipeline.PBR.Documents;

using RSG.Interop.Adobe.Photoshop;
using RSG.Interop.Substance;
using RSG.RAG.RemoteConnection;

namespace RSG.Pipeline.PBR
{
    /// <summary>
    /// Primary class for exporting.
    /// </summary>
    public static class Export
    {
        #region Member Data

        private static IConfig _config = ConfigFactory.CreateConfig();
        private static Units _currentRulerUnits;
        private static String _exportDocName = "___EXPORTING___";

        #endregion // Member Data

        #region Properties

        public static readonly String TempExportDirectory           = String.Format("{0}\\pbr", _config.Project.Cache);
        public static readonly String TempAlbedoMetalnessMap        = String.Format("{0}\\tempAlbedoMetalnessMap.tif", TempExportDirectory);
        public static readonly String TempMetalnessMap              = String.Format("{0}\\tempMetalnessMap.tif", TempExportDirectory);
        public static readonly String TempRoughnessChannelMap       = String.Format("{0}\\tempRoughnessChannelMap.tif", TempExportDirectory);
        public static readonly String TempRoughnessMap              = String.Format("{0}\\tempRoughnessMap.tif", TempExportDirectory);
        public static readonly String TempRoughnessModifierMap      = String.Format("{0}\\tempRoughnessModifierMap.tif", TempExportDirectory);
        public static readonly String TempSheenChannelMap           = String.Format("{0}\\tempSheenChannelMap.tif", TempExportDirectory);
        public static readonly String TempSheenMap                  = String.Format("{0}\\tempSheenMap.tif", TempExportDirectory);
        public static readonly String TempUnmodifiedMetalnessMap    = String.Format("{0}\\tempUnmodifiedMetalnessMap.tif", TempExportDirectory);
        public static readonly String TempUnmodifiedRoughnessMap    = String.Format("{0}\\tempUnmodifiedRoughnessMap.tif", TempExportDirectory);
        public static readonly String TempDetailMap                 = String.Format("{0}\\tempDetailMap.tif", TempExportDirectory);
        public static readonly String TempDetailIntensityMap        = String.Format("{0}\\tempDetailIntensityMap.tif", TempExportDirectory);
        public static readonly String TempAmbientOcclusionMap       = String.Format("{0}\\tempAmbientOcclusionMap.tif", TempExportDirectory);
        public static readonly String TempTranslucencyMap           = String.Format("{0}\\tempTranslucencyMap.tif", TempExportDirectory);
        public static readonly String TempHeightMap                 = String.Format("{0}\\tempHeightMap.tif", TempExportDirectory);

        #endregion

        #region Export Methods

        public static bool IsValidToExport(Document doc, bool quiet = false)
        {
            if (doc != null)
            {
                try
                {
                    string docFullName = doc.FullName.ToLower();
                    return docFullName.EndsWith(".psd");
                }
                catch
                {
                    if (!quiet)
                    {
                        System.Windows.Forms.MessageBox.Show("Cannot export a new document that hasn't been saved to disk!  Aborting export.", "Photoshop PBR");
                    }
                }
            }
            return false;
        }

        public static bool IsValidToExport(string filename, bool quiet = false)
        {
            return filename.ToLower().EndsWith(".psd");
        }

        public static bool IsValidToExport(IMasterMaterialDocument doc, bool quiet = false)
        {
            return doc.Document.FullName.ToLower().EndsWith(".psd");
        }

        private static void PreExport(ExportOptions options)
        {
            if (!Photoshop.Initialized)
            {
                Photoshop.Initialize();
            }

            // Close any export documents that were left open.
            foreach (Document doc in Photoshop.Application.Documents)
            {
                if (doc.Name == _exportDocName)
                {
                    doc.Close();
                }
            }

            // Store current ruler unit preference.
            _currentRulerUnits = Photoshop.Application.Preferences.RulerUnits;

            // Force ruler units to be pixels.  This appears to fix issues when creating new documents whose ruler units are set to inches, it would
            // create massive sized files.
            Photoshop.Application.Preferences.RulerUnits = Units.Pixels;

            Photoshop.Helpers.BlockRefresh();
        }

        private static void PostExport(ExportOptions options)
        {
            // Restore ruler unit preference.
            Photoshop.Application.Preferences.RulerUnits = _currentRulerUnits;

            // Tell Rag to auto-reload all of the textures.
            if (options.LiveUpdate)
            {
                String widgetPath = "_LiveEdit_/Shader Edit/reload all textures";
                RemoteConnection rag = new RemoteConnection();

                if (rag.IsConnected())
                {
                    rag.PressButton(widgetPath);
                }
            }

            if (!options.Quiet)
            {
                System.Windows.Forms.MessageBox.Show("Export complete.", "Rockstar");
            }
        }

        public static string[] DoExport(IEnumerable<IMasterMaterialDocument> docs, ExportOptions options)
        {
            PreExport(options);

            List<string> exportedFiles = new List<string>();

            foreach (IMasterMaterialDocument doc in docs)
            {
                if (IsValidToExport(doc))
                {
                    options.Name = doc.Document.Name;
                    options.FullName = doc.Document.FullName;

                    // Duplicate the document to export.
                    IMasterMaterialDocument exportDoc = MasterMaterialDocumentFactory.FromPhotoshopDocument(doc.Document.Duplicate(_exportDocName));

                    // Set as active document.
                    Photoshop.Application.ActiveDocument = exportDoc.Document;

                    // Adjust what to export if we have empty groups.
                    if (options.ExportAlbedo)
                    {
                        options.ExportAlbedo = !Photoshop.IsLayerGroupEmpty(exportDoc.Document, exportDoc.LayerGroup.Albedo);
                    }

                    if (options.ExportAlpha)
                    {
                        options.ExportAlpha = !Photoshop.IsLayerGroupEmpty(exportDoc.Document, exportDoc.LayerGroup.Alpha);
                    }

                    if (options.ExportEmissive)
                    {
                        options.ExportEmissive = !Photoshop.IsLayerGroupEmpty(exportDoc.Document, exportDoc.LayerGroup.Emissive);
                    }

                    if (options.ExportMaterialMapA)
                    {
                        options.ExportMaterialMapA = !Photoshop.IsLayerGroupEmpty(exportDoc.Document, exportDoc.LayerGroup.Material);
                    }

                    if (options.ExportMaterialMapB)
                    {
                        options.ExportMaterialMapB = !Photoshop.IsLayerGroupEmpty(exportDoc.Document, exportDoc.LayerGroup.Translucency) || !Photoshop.IsLayerGroupEmpty(exportDoc.Document, exportDoc.LayerGroup.Height) || !Photoshop.IsLayerGroupEmpty(exportDoc.Document, exportDoc.LayerGroup.DetailMapsID);
                    }                    

                    if (options.ExportNormal)
                    {
                        options.ExportNormal = !Photoshop.IsLayerGroupEmpty(exportDoc.Document, exportDoc.LayerGroup.Normal);
                    }

                    // Export.
                    string[] files = exportDoc.Export(options);

                    if (files != null)
                    {
                        foreach (string file in files)
                        {
                            exportedFiles.Add(file);
                        }
                    }

                    exportDoc.Document.Close();

                    doc.Serialize();
                    Photoshop.Application.ActiveDocument = doc.Document;
                }
            }

            PostExport(options);

            return exportedFiles.ToArray();
        }

        #endregion // Export Methods

        public static string ExportLayerGroup(Document doc, ExportOptions options, string layerGroupPath, string outputFilename)
        {
            string filename = "";

            if (Photoshop.Application.IsConnected())
            {
                if (doc != null)
                {
                    // Ensure the document has been previously saved to a file on disk.
                    bool saved = false;

                    // Photoshop will throw an exception if you try to access the document's
                    // .FullName property if it hasn't been saved to disk yet.
                    try
                    {
                        string fullname = options.FullName;
                        saved = true;
                    }
                    catch
                    {
                        saved = false;
                    }

                    if (saved)
                    {
                        // Hide background layers.
                        foreach (ArtLayer layer in doc.ArtLayers)
                        {
                            layer.Visible = false;
                        }

                        Photoshop.Helpers.SetLayerGroupVisibilityByPath(doc, layerGroupPath, true);

                        string basename = System.IO.Path.GetFileNameWithoutExtension(options.FullName);
                        filename += options.Path + "\\" + outputFilename + ".tif";

                        // No Perforce is necessary if saving to the cache directory.
                        if (options.Path.Contains(TempExportDirectory))
                        {
                            TiffSaveOptions tiffOptions = new TiffSaveOptions();
                            tiffOptions.AlphaChannels = true;
                            tiffOptions.Layers = false;

                            doc.SaveAs(filename, tiffOptions);
                        }
                        else
                        {
                            if (Photoshop.CheckoutFile(filename))
                            {
                                TiffSaveOptions tiffOptions = new TiffSaveOptions();
                                tiffOptions.AlphaChannels = true;
                                tiffOptions.Layers = false;

                                doc.SaveAs(filename, tiffOptions);
                            }
                            else
                            {
                                System.Windows.Forms.MessageBox.Show("Could not export the following file because it could not be checked out!\n\n" + filename, "Photoshop PBR");
                            }
                        }
                    }
                    else
                    {
                        System.Windows.Forms.MessageBox.Show("The Photoshop file has not yet been saved!");
                    }
                }
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("The tool is not connected to Photoshop!  Cannot export.", "Photoshop PBR");
            }

            return filename;
        }

        public static String ExportDetailMapIntensity(IMasterMaterialDocument doc)
        {
            if (Photoshop.Application.IsConnected())
            {
                doc.DisplayDetailMapIntensity();

                LayerSet layerSet = Photoshop.Helpers.FindLayerGroupByPath(doc.Document, doc.LayerGroup.DetailMapsIntensity);

                if (layerSet != null)
                {
                    // Create a blank black layer if empty.
                    if (layerSet.ArtLayers.Count == 0)
                    {
                        doc.Document.ActiveLayer = layerSet;
                        ArtLayer bgLayer = Photoshop.Helpers.CreateFillLayer(doc.Document, 0, 0, 0, false);
                        bgLayer.Name = "Detail Map Intensity BG";
                    }
                }

                TiffSaveOptions options = new TiffSaveOptions();
                options.AlphaChannels = true;
                options.Layers = false;

                doc.Document.SaveAs(TempDetailIntensityMap, options);

                return TempDetailIntensityMap;
            }

            return null;
        }

        public static String ExportDetailMapID(IMasterMaterialDocument doc)
        {
            if (Photoshop.Application.IsConnected())
            {
                doc.DisplayDetailMapIDs();

                LayerSet layerSet = Photoshop.Helpers.FindLayerGroupByPath(doc.Document, doc.LayerGroup.DetailMapsID);

                if (layerSet != null)
                {
                    // Create a blank black layer if empty.
                    if (layerSet.ArtLayers.Count == 0)
                    {
                        doc.Document.ActiveLayer = layerSet;
                        ArtLayer bgLayer = Photoshop.Helpers.CreateFillLayer(doc.Document, 0, 0, 0, false);
                        bgLayer.Name = "Detail Map ID BG";
                    }
                }

                TiffSaveOptions options = new TiffSaveOptions();
                options.AlphaChannels = true;
                options.Layers = false;

                doc.Document.SaveAs(TempDetailMap, options);

                return TempDetailMap;
            }

            return null;
        }

        public static String ExportAmbientOcclusion(IMasterMaterialDocument doc)
        {
            if (Photoshop.Application.IsConnected())
            {
                LayerSet aoLayerSet = Photoshop.Helpers.FindLayerGroupByPath(doc.Document, doc.LayerGroup.AmbientOcclusion);

                if (aoLayerSet != null)
                {
                    // Create a blank white layer group if empty.
                    if (aoLayerSet.ArtLayers.Count == 0)
                    {
                        doc.Document.ActiveLayer = aoLayerSet;
                        ArtLayer aoLayer = Photoshop.Helpers.CreateFillLayer(doc.Document, 255, 255, 255, false);
                        aoLayer.Name = "Ambient Occlusion BG";
                    }
                }

                ExportOptions cacheOptions = new ExportOptions(doc.Document);
                cacheOptions.Path = TempExportDirectory;

                return RSG.Pipeline.PBR.Export.ExportLayerGroup(doc.Document, cacheOptions, doc.LayerGroup.AmbientOcclusion, "tempAmbientOcclusionMap");
            }

            return null;
        }

        public static String ExportRoughness(IMasterMaterialDocument doc)
        {
            if (Photoshop.Application.IsConnected())
            {
                LayerSet materialLayerSet = Photoshop.Helpers.FindLayerGroupByPath(doc.Document, doc.LayerGroup.Material);
                materialLayerSet.Visible = true;

                doc.SetLayerFillColorsToRoughness();
                
                // Go through each surface layer set.  Collapse the modifier and set to linear light.
                List<Surface> surfaces = doc.GetSurfaces();

                foreach (Surface surface in surfaces)
                {
                    // If the modifier layer set is empty, create a 50% gray fill layer.
                    if (surface.RoughnessModifierLayer.ArtLayers.Count == 0)
                    {
                        doc.Document.ActiveLayer = surface.RoughnessModifierLayer;
                        ArtLayer modBgLayer = Photoshop.Helpers.CreateFillLayer(doc.Document, 128, 128, 128, false);
                        modBgLayer.Move(surface.RoughnessModifierLayer, ElementPlacement.PlaceInside);
                    }

                    ArtLayer modifier = surface.RoughnessModifierLayer.Merge();
                    modifier.Visible = true;
                    modifier.Opacity = 100.0;
                    modifier.FillOpacity = surface.RoughnessModifierLimit * 100.0;
                    modifier.BlendMode = BlendMode.LinearLight;

                    surface.Layer.Visible = true;
                }

                TiffSaveOptions options = new TiffSaveOptions();
                options.AlphaChannels = true;
                options.Layers = false;

                doc.Document.SaveAs(TempRoughnessMap, options);

                return TempRoughnessMap;
            }

            return null;
        }

        public static String ExportSheen(IMasterMaterialDocument doc)
        {
            if (Photoshop.Application.IsConnected())
            {
                doc.SetLayerFillColorsToSheen();
                doc.DisplaySheen();

                // Add black BG layer.  In case there are no sheen materials, we want it to be black.
                LayerSet materialLayerSet = RSG.Pipeline.PBR.Photoshop.Helpers.FindLayerGroupByPath(doc.Document, doc.LayerGroup.Material);
                doc.Document.ActiveLayer = materialLayerSet;

                ArtLayer sheenBg = RSG.Pipeline.PBR.Photoshop.Helpers.CreateFillLayer(doc.Document, 0, 0, 0, false);
                sheenBg.Move(materialLayerSet, ElementPlacement.PlaceAtEnd);

                TiffSaveOptions options = new TiffSaveOptions();
                options.AlphaChannels = true;
                options.Layers = false;

                doc.Document.SaveAs(TempSheenMap, options);

                return TempSheenMap;
            }

            return null;
        }

        public static String ExportAlbedo(IMasterMaterialDocument doc, ExportOptions options)
        {
            if (Photoshop.Application.IsConnected())
            {
                if (options.ExportAlpha && options.PackAlbedoAlpha)
                {
                    // Export Albedo.
                    String outputFilename = TextureNamingConvention.AlbedoAlpha.Transform(options.Name);
                    String albedoMapFilename = Export.ExportLayerGroup(doc.Document, options, doc.LayerGroup.Albedo, outputFilename);

                    // Export Alpha.
                    ExportOptions alphaOptions = new ExportOptions();
                    alphaOptions.Name = "tempAlpha";
                    alphaOptions.FullName = String.Format("{0}\\tempAlpha.tif", Export.TempExportDirectory);
                    alphaOptions.Path = Export.TempExportDirectory;

                    String alphaMapFilename = Export.ExportAlpha(doc, alphaOptions);

                    Document alphaDoc = RSG.Pipeline.PBR.Photoshop.Application.Open(alphaMapFilename);
                    alphaDoc.Selection.SelectAll();
                    alphaDoc.Selection.Copy();
                    alphaDoc.Close();

                    // Open exported Albedo so that we can pack the Alpha into it.
                    Document albedoDoc = RSG.Pipeline.PBR.Photoshop.Application.Open(albedoMapFilename);
                    Channel alphaChannel = albedoDoc.Channels.Add();
                    alphaChannel.Name = "Alpha";

                    // Paste Alpha map.
                    RSG.Pipeline.PBR.Photoshop.Helpers.SetActiveChannels(albedoDoc, false, false, false, true);
                    albedoDoc.Paste(false);

                    // Save new Albedo map.
                    albedoDoc.Save();
                    albedoDoc.Close();

                    // Restore passed in document as the active one.
                    RSG.Pipeline.PBR.Photoshop.Application.ActiveDocument = doc.Document;

                    return albedoMapFilename;
                }
                else
                {
                    String outputFilename = TextureNamingConvention.Albedo.Transform(options.Name);
                    return Export.ExportLayerGroup(doc.Document, options, doc.LayerGroup.Albedo, outputFilename);
                }
            }

            return null;
        }

        public static String ExportNormal(IMasterMaterialDocument doc, ExportOptions options)
        {
            if (Photoshop.Application.IsConnected())
            {
                RSG.Pipeline.PBR.Photoshop.Helpers.SetLayerGroupVisibilityByPath(doc.Document, doc.LayerGroup.Normal, true);
                LayerSet normalLayerSet = RSG.Pipeline.PBR.Photoshop.Helpers.FindLayerGroupByPath(doc.Document, doc.LayerGroup.Normal);

                if (normalLayerSet != null)
                {
                    bool createBg = true;

                    // If all of the layers are hidden, need to make a temp bg layer.
                    foreach (ArtLayer layer in normalLayerSet.ArtLayers)
                    {
                        if (layer.Visible)
                        {
                            createBg = false;
                            break;
                        }
                    }

                    if (createBg)
                    {
                        doc.Document.ActiveLayer = normalLayerSet;
                        Photoshop.Helpers.CreateFillLayer(doc.Document, 128, 128, 255, false);
                    }
                }

                String outputFilename = TextureNamingConvention.Normal.Transform(options.Name);
                return RSG.Pipeline.PBR.Export.ExportLayerGroup(doc.Document, options, doc.LayerGroup.Normal, outputFilename);
            }

            return null;
        }

        public static String ExportEmissive(IMasterMaterialDocument doc, ExportOptions options)
        {
            if (Photoshop.Application.IsConnected())
            {
                String outputFilename = TextureNamingConvention.Emissive.Transform(options.Name);
                return RSG.Pipeline.PBR.Export.ExportLayerGroup(doc.Document, options, doc.LayerGroup.Emissive, outputFilename);
            }

            return null;
        }

        public static String ExportAlpha(IMasterMaterialDocument doc, ExportOptions options)
        {
            if (Photoshop.Application.IsConnected())
            {
                String outputFilename = TextureNamingConvention.Alpha.Transform(options.Name);
                return RSG.Pipeline.PBR.Export.ExportLayerGroup(doc.Document, options, doc.LayerGroup.Alpha, outputFilename);
            }

            return null;
        }

        public static String ExportTranslucency(IMasterMaterialDocument doc)
        {
            if (Photoshop.Application.IsConnected())
            {
                Helpers helpers = new Helpers(Photoshop.Application);

                // If layer group is empty, create a base background layer.
                LayerSet translucencyLayerSet = helpers.FindLayerGroupByPath(doc.Document, doc.LayerGroup.Translucency);

                if (translucencyLayerSet.ArtLayers.Count == 0)
                {
                    doc.Document.ActiveLayer = translucencyLayerSet;
                    ArtLayer translucencyBG = helpers.CreateFillLayer(doc.Document, 255, 255, 255, false);
                    translucencyBG.Name = "Translucency BG";
                }
                
                ExportOptions cacheOptions = new ExportOptions(doc.Document);
                cacheOptions.Path = RSG.Pipeline.PBR.Export.TempExportDirectory;

                return RSG.Pipeline.PBR.Export.ExportLayerGroup(doc.Document, cacheOptions, doc.LayerGroup.Translucency, "tempTranslucencyMap");
            }

            return null;
        }

        public static String ExportHeight(IMasterMaterialDocument doc)
        {
            if (Photoshop.Application.IsConnected())
            {
                Helpers helpers = new Helpers(Photoshop.Application);

                // If layer group is empty, create a base background layer.
                LayerSet heightLayerSet = helpers.FindLayerGroupByPath(doc.Document, doc.LayerGroup.Height);

                if (heightLayerSet.ArtLayers.Count == 0)
                {
                    doc.Document.ActiveLayer = heightLayerSet;
                    ArtLayer heightBG = helpers.CreateFillLayer(doc.Document, 128, 128, 128, false);
                    heightBG.Name = "Height BG";
                }

                ExportOptions cacheOptions = new ExportOptions(doc.Document);
                cacheOptions.Path = RSG.Pipeline.PBR.Export.TempExportDirectory;

                return RSG.Pipeline.PBR.Export.ExportLayerGroup(doc.Document, cacheOptions, doc.LayerGroup.Height, "tempHeightMap");
            }

            return null;
        }

        public static String ExportMetalness(IMasterMaterialDocument doc)
        {
            if (Photoshop.Application.IsConnected())
            {
                // Create black BG layer if it doesn't exist.
                ArtLayer materialBG = Photoshop.Helpers.FindLayerByPath(doc.Document, String.Format("{0}/DO NOT PAINT - FOR EXPORT ONLY", doc.LayerGroup.Material), false);
                LayerSet materialLayerSet = doc.GetMaterialLayerSet();

                if (materialBG == null)
                {
                    doc.Document.ActiveLayer = materialLayerSet;

                    materialBG = Photoshop.Helpers.CreateFillLayer(doc.Document, 0, 0, 0, false);
                    materialBG.Name = "DO NOT PAINT - FOR EXPORT ONLY";
                }

                // Ensure BG is visible and always at the end.
                if (materialBG != null)
                {
                    materialBG.Visible = true;
                    materialBG.AllLocked = true;
                    materialBG.Move(materialLayerSet, ElementPlacement.PlaceAtEnd);
                }

                doc.SetLayerFillColorsToMetalness();
                doc.DisplayMetalness();

                TiffSaveOptions options = new TiffSaveOptions();
                options.AlphaChannels = true;
                options.Layers = false;

                doc.Document.SaveAs(TempUnmodifiedMetalnessMap, options);

                if (materialBG != null)
                {
                    materialBG.Visible = false;
                }

                return TempUnmodifiedMetalnessMap;
            }

            return null;
        }
    }
}
