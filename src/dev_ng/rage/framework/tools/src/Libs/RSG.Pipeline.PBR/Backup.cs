﻿using System;
using System.IO;
using System.Diagnostics;
using System.Xml;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Media.Imaging;
using System.Windows.Forms;
using System.Threading;

using RSG.Base.Configuration;
using RSG.Interop.Adobe.Photoshop;

namespace RSG.Pipeline.PBR
{
    class DateCompareFileInfo : IComparer<FileInfo>
    {
        /// <summary>
        /// Compare the last dates of the File infos
        /// </summary>
        /// <param name="fi1">First FileInfo to check</param>
        /// <param name="fi2">Second FileInfo to check</param>
        /// <returns></returns>
        public int Compare(FileInfo fi1, FileInfo fi2)
        {
            int result;
            if (fi1.LastWriteTime == fi2.LastWriteTime)
            {
                result = 0;
            }
            else if (fi1.LastWriteTime < fi2.LastWriteTime)
            {
                result = 1;
            }
            else
            {
                result = -1;
            }

            return result;
        }
    }

    public class Backup
    {
        /*
         * Simple system to backup a Photoshop document.  Defaults to the project's cache directory.
         */

        #region Member Data

        private static IConfig _config = ConfigFactory.CreateConfig();

        private bool _enabled = true;
        private string _backupDirectory = Export.TempExportDirectory;
        private string _backupBaseFilename = "PhotoshopBackup";
        private int _numBackups = 10;
        private int _currentBackup = 1;
        private int _maxNumBackups = 100;
        private int _minNumBackups = 0;
        private int _interval = 5;

        #endregion // Member Data

        #region Properties

        public bool Enabled
        {
            get { return _enabled; }
            set { _enabled = value; }
        }

        public int NumBackups
        {
            get { return _numBackups; }
            set { _numBackups = value; DetermineCurrentBackup(); }
        }

        public int CurrentBackup
        {
            get { return _currentBackup; }
        }

        public string BackupName
        {
            get { return _backupBaseFilename; }
            set { _backupBaseFilename = value; DetermineCurrentBackup(); }
        }

        public string BackupDirectory
        {
            get { return _backupDirectory; }
            set { _backupDirectory = value; DetermineCurrentBackup(); }
        }

        public int MaxNumBackups
        {
            get { return _maxNumBackups; }
        }

        public int MinNumBackups
        {
            get { return _minNumBackups; }
        }

        public int Interval
        {
            get { return _interval; }
            set { _interval = value; }
        }

        public int IntervalInMinutes
        {
            get { return _interval * 60000; }
        }

        #endregion // Properties

        #region Constructor(s)

        public Backup()
        {
            DetermineCurrentBackup();
        }

        #endregion // Constructor(s)

        #region Private Methods

        private void IncrementBackup()
        {
            _currentBackup += 1;

            if (_currentBackup > _numBackups)
            {
                _currentBackup = 1;
            }
        }

        private void DetermineCurrentBackup()
        {
            string latestBackup = FindLatestBackup();

            if (latestBackup != null)
            {
                string basename = Path.GetFileNameWithoutExtension(latestBackup);

                // Turn last two digits into the current index.
                int index = Convert.ToInt32(basename.Substring(basename.Length - 2));

                if (index > _maxNumBackups)
                {
                    _currentBackup = _maxNumBackups;
                }
                else if (index < _minNumBackups)
                {
                    _currentBackup = _minNumBackups;
                }
                else
                {
                    _currentBackup = index;
                }
            }
        }

        private String FindLatestBackup()
        {
            String latestBackupFilename = null;
            DirectoryInfo dirInfo = new DirectoryInfo(_backupDirectory);
            String pattern = String.Format("{0}_AutoBack*.psd", _backupBaseFilename);
            FileInfo[] files = dirInfo.GetFiles(pattern);

            if (files.Length > 0)
            {
                DateCompareFileInfo dateCompare = new DateCompareFileInfo();
                Array.Sort(files, dateCompare);
                latestBackupFilename = files[0].FullName;
            }

            return latestBackupFilename;
        }

        #endregion // Private Methods

        #region Methods

        public string GetCurrentBackupFilename()
        {
            string filename;

            if (_currentBackup < 10)
            {
                filename = _backupDirectory + "\\" + _backupBaseFilename + "_AutoBack0" + _currentBackup.ToString() + ".psd";
            }
            else
            {
                filename = _backupDirectory + "\\" + _backupBaseFilename + "_AutoBack" + _currentBackup.ToString() + ".psd";
            }

            return filename;
        }

        public void CreateBackup(dynamic doc)
        {
            if (RSG.Pipeline.PBR.Photoshop.Application.IsConnected() && this._enabled)
            {
                IncrementBackup();

                PhotoshopSaveOptions options = new PhotoshopSaveOptions();
                doc.SaveAs(GetCurrentBackupFilename(), options, true);
            }
        }

        public void OpenLastBackup(dynamic app)
        {
            if (RSG.Pipeline.PBR.Photoshop.Application.IsConnected())
            {
                String filename = FindLatestBackup();

                if (File.Exists(filename))
                {
                    app.Open(filename);
                }
                else
                {
                    MessageBox.Show("Could not find a backup file to open!", "Photoshop PBR");
                }
            }
        }

        #endregion // Methods
    }
}
