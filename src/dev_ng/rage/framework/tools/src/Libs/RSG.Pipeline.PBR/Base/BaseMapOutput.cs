﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;

using RSG.Interop.Adobe.Photoshop;
using RSG.Base.Configuration;

namespace RSG.Pipeline.PBR
{
    public abstract class BaseMapOutput : IMapOutput
    {
        #region Member Data

        private IConfig m_config = ConfigFactory.CreateConfig();
        private String m_alphaMapFilename = null;

        #endregion // Member Data

        #region Properties

        /// <summary>
        /// Name of the output.
        /// </summary>
        public abstract String Name
        {
            get;
        }

        /// <summary>
        /// The path to the layer group for the map.
        /// </summary>
        public abstract String LayerGroupPath
        {
            get;
        }

        /// <summary>
        /// Naming convention for the map.
        /// </summary>
        public abstract NamingConventionDescription NamingConvention
        {
            get;
        }

        /// <summary>
        /// Really only used for recording the alpha map for packing into the albedo for live update.
        /// </summary>
        public String AlphaMapFilename
        {
            get { return this.m_alphaMapFilename; }
            set { this.m_alphaMapFilename = value; }
        }

        #endregion // Properties

        #region Constructor(s)

        public BaseMapOutput()
        {
        }

        #endregion // Constructor(s)

        #region Methods

        /// <summary>
        /// Exports the map output.
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public abstract String Export(Document doc, ExportOptions options);

        /// <summary>
        /// Exports the map output for live update.
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public virtual String ExportLiveUpdateMap(Document doc, ExportOptions options)
        {
            // Save DDS to the project's cache directory.
            Directory.CreateDirectory(RSG.Pipeline.PBR.Export.TempExportDirectory);

            // Save texture as TGA.
            TargaSaveOptions tgaOptions = new TargaSaveOptions();
            tgaOptions.Resolution = TargaBitsPerPixel.Targa32Bits;
            tgaOptions.AlphaChannels = true;

            String tgaFilename = String.Format("{0}\\{1}.tga", RSG.Pipeline.PBR.Export.TempExportDirectory, this.Name);

            // Convert to 8bpp so that we can save as a TGA.
            doc.BitsPerChannel = BitsPerChannelType.Document8Bits;

            // Save.
            doc.SaveAs(tgaFilename, tgaOptions);

            List<DDSInputOption> inputOptions = new List<DDSInputOption>();
            List<DDSCompression> compression = new List<DDSCompression>();

            inputOptions.Add(DDSInputOption.Color);
            compression.Add(DDSCompression.DXT5);

            String result = DDS.Compress(tgaFilename, inputOptions, compression);

            File.Delete(tgaFilename);

            return result;
        }

        #endregion // Methods
    }
}
