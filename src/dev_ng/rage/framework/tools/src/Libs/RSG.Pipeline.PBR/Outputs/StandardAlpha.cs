﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Pipeline.PBR;
using RSG.Pipeline.PBR.Documents;
using RSG.Interop.Adobe.Photoshop;

namespace RSG.Pipeline.PBR.Outputs
{
    /// <summary>
    /// Alpha map output.
    /// </summary>
    public class StandardAlpha : BaseMapOutput
    {
        #region Member Data

        private StandardLayerGroup _layerGroup;

        #endregion // Member Data

        #region Properties

        public override String Name
        {
            get { return TextureNamingConvention.Alpha.Name; }
        }

        public override String LayerGroupPath
        {
            get { return _layerGroup.Alpha; }
        }

        public override NamingConventionDescription NamingConvention
        {
            get { return TextureNamingConvention.Alpha; }
        }

        #endregion //Properties

        #region Constructor(s)

        public StandardAlpha()
        {
            _layerGroup = new StandardLayerGroup();
        }

        #endregion // Constructor(s)

        #region Methods

        public override String Export(Document doc, ExportOptions options)
        {
            if (!options.PackAlbedoAlpha)
            {
                IMasterMaterialDocument matDoc = MasterMaterialDocumentFactory.FromPhotoshopDocument(doc);
                return RSG.Pipeline.PBR.Export.ExportAlpha(matDoc, options);
            }

            return null;
        }

        public override String ExportLiveUpdateMap(Document doc, ExportOptions options)
        {
            // No live update map for alpha.
            return null;
        }

        #endregion // Methods
    }
}
