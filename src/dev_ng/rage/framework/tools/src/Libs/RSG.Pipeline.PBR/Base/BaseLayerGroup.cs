﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.PBR
{
    public abstract class BaseLayerGroup : ILayerGroup
    {
        #region Properties

        public virtual String Albedo { get { return TextureNamingConvention.Albedo.Name; } }

        public virtual String Miscellaneous { get { return "Misc"; } }
        public virtual String Normal { get { return this.Miscellaneous + "/" + TextureNamingConvention.Normal.Name; } }
        public virtual String Translucency { get { return this.Miscellaneous + "/" + TextureNamingConvention.Translucency.Name; } }
        public virtual String Height { get { return this.Miscellaneous + "/" + TextureNamingConvention.Height.Name; } }
        public virtual String AmbientOcclusion { get { return this.Miscellaneous + "/" + TextureNamingConvention.AmbientOcclusion.Name; } }
        public virtual String Alpha { get { return this.Miscellaneous + "/" + TextureNamingConvention.Alpha.Name; } }
        public virtual String Emissive { get { return this.Miscellaneous + "/" + TextureNamingConvention.Emissive.Name; } }

        public virtual String Material { get { return "Material"; } }

        // LEGACY: Modifiers are no-longer supported like this.
        public virtual String Modifiers { get { return "Modifiers"; } }
        public virtual String RoughnessModifier { get { return this.Modifiers + "/" + TextureNamingConvention.Roughness.Name; } }
        public virtual String RougherModifier { get { return this.RoughnessModifier + "/Rougher"; } }
        public virtual String SmootherModifier { get { return this.RoughnessModifier + "/Smoother"; } }

        // Detail Maps
        public virtual String DetailMaps { get { return "Detail Maps"; } }
        public virtual String DetailMapsID { get { return this.DetailMaps + "/ID"; } }
        public virtual String DetailMapsIntensity { get { return this.DetailMaps + "/Intensity"; } }

        // Temp
        public virtual String Temp { get { return "Temp [DOES NOT EXPORT]"; } }

        // Non-PBR (for GTA V Liberty City)
        public virtual String Diffuse { get { return "Diffuse"; } }
        public virtual String Specular { get { return this.Miscellaneous + "/Specular"; } }

        #endregion // Properties

        #region Constructor(s)

        public BaseLayerGroup()
        {
        }

        #endregion // Constructor(s)

        #region Methods

        /// <summary>
        /// Create layer groups.
        /// </summary>
        public virtual void Create(IMasterMaterialDocument doc)
        {
            // Albedo
            RSG.Pipeline.PBR.Photoshop.Helpers.FindLayerGroupByPath(doc.Document, this.Albedo, true);

            // Misc
            RSG.Pipeline.PBR.Photoshop.Helpers.FindLayerGroupByPath(doc.Document, this.Miscellaneous, true);
            RSG.Pipeline.PBR.Photoshop.Helpers.FindLayerGroupByPath(doc.Document, this.Normal, true);
            RSG.Pipeline.PBR.Photoshop.Helpers.FindLayerGroupByPath(doc.Document, this.Translucency, true);
            RSG.Pipeline.PBR.Photoshop.Helpers.FindLayerGroupByPath(doc.Document, this.Height, true);
            RSG.Pipeline.PBR.Photoshop.Helpers.FindLayerGroupByPath(doc.Document, this.AmbientOcclusion, true);
            RSG.Pipeline.PBR.Photoshop.Helpers.FindLayerGroupByPath(doc.Document, this.Alpha, true);
            RSG.Pipeline.PBR.Photoshop.Helpers.FindLayerGroupByPath(doc.Document, this.Emissive, true);

            // Material
            RSG.Pipeline.PBR.Photoshop.Helpers.FindLayerGroupByPath(doc.Document, this.Material, true);

            // Detail Maps
            RSG.Pipeline.PBR.Photoshop.Helpers.FindLayerGroupByPath(doc.Document, this.DetailMaps, true);
            RSG.Pipeline.PBR.Photoshop.Helpers.FindLayerGroupByPath(doc.Document, this.DetailMapsID, true);
            RSG.Pipeline.PBR.Photoshop.Helpers.FindLayerGroupByPath(doc.Document, this.DetailMapsIntensity, true);

            // Temp
            RSG.Pipeline.PBR.Photoshop.Helpers.FindLayerGroupByPath(doc.Document, this.Temp, true);
        }

        #endregion // Methods
    }
}
