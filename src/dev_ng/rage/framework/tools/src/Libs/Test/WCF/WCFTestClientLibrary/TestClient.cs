﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace WCFTestClient
{
    public class TestClient
    {

        #region Fields
        /// <summary>
        /// 
        /// </summary>
        private readonly BasicReference.BasicServiceClient m_BasicClient;
        /// <summary>
        /// 
        /// </summary>
        private readonly UploadReference.FileUploadServiceClient m_UploadClient;
        #endregion

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public TestClient()
        {
            m_BasicClient = new BasicReference.BasicServiceClient();
            m_UploadClient = new UploadReference.FileUploadServiceClient();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        public void Run()
        {
            String message = m_BasicClient.GetData(35);
            Console.WriteLine(message);
           
            String file = "c:/systrayrfs.log";
            try
            {
                FileStream fs = new FileStream(file, FileMode.Open);
                m_UploadClient.Upload("testupload.txt", fs);
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine("Error: {0}", ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void Stop()
        {
            m_BasicClient.Close();
            m_UploadClient.Close();
        }
        #endregion // Controller Methods
    }
}
