﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.IO;
using System.Configuration;

namespace WCFTestServiceLibrary
{
    /// <summary>
    /// Simple Upload service. 
    /// (http://bartwullems.blogspot.co.uk/2011/01/streaming-files-over-wcf.html)
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall,
    ConcurrencyMode = ConcurrencyMode.Single)]
    public class FileUploadService : IFileUploadService
    {
        #region IFileUploadService Members
        /// <summary>
        /// Upload a file given an UploadRequest object
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public UploadResponse Upload(UploadRequest request)
        {
            try
            {
                // Prepararation for upload directory and destination file path
                String uploadDirectory = ConfigurationManager.AppSettings["uploadDirectory"];
                if (!Directory.Exists(uploadDirectory))
                {
                    Directory.CreateDirectory(uploadDirectory);
                }

                String path = Path.Combine(uploadDirectory, request.FileName);
                if (File.Exists(path))
                {
                    File.Delete(path);
                }

                // Read the incoming stream and save it to file
                const Int32 bufferSize = 2048;
                Byte[] buffer = new Byte[bufferSize];
                using (FileStream outputStream = new FileStream(path, FileMode.Create, FileAccess.Write))
                {
                    Int32 bytesRead = request.DataStream.Read(buffer, 0, bufferSize);
                    while (bytesRead > 0)
                    {
                        outputStream.Write(buffer, 0, bytesRead);
                        bytesRead = request.DataStream.Read(buffer, 0, bufferSize);
                    }
                    outputStream.Close();
                }
                return new UploadResponse
                {
                    UploadSucceeded = false
                };
            }
            catch (Exception ex)
            {
                return new UploadResponse
                    {
                        UploadSucceeded = false
                    };
            }
        }
        #endregion // IFileUploadService Members
    }
}
