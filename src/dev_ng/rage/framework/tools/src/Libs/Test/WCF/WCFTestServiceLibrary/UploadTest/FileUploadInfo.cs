﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.IO;

namespace WCFTestServiceLibrary
{
    /// <summary>
    /// Simple MessageContract wrapper for an upload file containing the filename in the
    /// message header and the streamed data in the message body
    /// </summary>
    [MessageContract]
    public class UploadRequest
    {
        [MessageHeader(MustUnderstand = true)]
        public string FileName { get; set; }
    
        [MessageBodyMember(Order = 1)]
        public Stream DataStream { get; set; }
    }

    /// <summary>
    /// Simple MessageContract weapper for upload status
    /// </summary>
    [MessageContract]
    public class UploadResponse
    {
        [MessageBodyMember(Order = 1)]
        public bool UploadSucceeded { get; set; }
    }
}
