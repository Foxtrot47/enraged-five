﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WCFTestServiceLibrary
{
    /// <summary>
    /// Simple Upload service. 
    /// (http://bartwullems.blogspot.co.uk/2011/01/streaming-files-over-wcf.html)
    /// </summary>
    [ServiceContract]
    public interface IFileUploadService
    {
        [OperationContract]
        UploadResponse Upload(UploadRequest uploadRequest);
    }
}
