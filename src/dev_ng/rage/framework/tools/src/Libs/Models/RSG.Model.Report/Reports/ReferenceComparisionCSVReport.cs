﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Map;
using System.IO;
using RSG.Model.Common;

namespace RSG.Model.Report.Reports
{
    /// <summary>
    /// 
    /// </summary>
    public class ReferenceComparisionCSVReport : CSVReport, IDynamicSectionReport
    {
        #region Constants
        private const String NAME = "Reference Comparison CSV Report";
        private const String DESCRIPTION = "Exports a reference comparison into a .csv file";
        #endregion // Constants

        #region Construtcor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public ReferenceComparisionCSVReport()
            : base(NAME, DESCRIPTION)
        {
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Generate the report dynamically.
        /// </summary>
        /// <param name="selectedSections">Sections to be used by the report</param>
        public void Generate(List<MapSection> sections)
        {
            using (StreamWriter sw = new StreamWriter(Filename))
            {
                string header = "Reference Name, ";
                foreach (MapSection section in sections)
                    header += string.Format("{0}, ", section.Name);
                sw.WriteLine(header.TrimEnd(new char[] { ',', ' ' }));

                var references = new Dictionary<string, List<int>>();
                int index = 0;
                foreach (MapSection section in sections)
                {
                    foreach (IAsset asset in section.AssetChildren)
                    {
                        if (asset is MapInstance)
                        {
                            if ((asset as MapInstance).IsReference && (asset as MapInstance).ReferencedDefinition != null)
                            {
                                MapDefinition definition = (asset as MapInstance).ReferencedDefinition;
                                if (!references.ContainsKey(definition.Name.ToLower()))
                                {
                                    references.Add(definition.Name.ToLower(), new List<int>(sections.Count));
                                    for (int i = 0; i < sections.Count; i++)
                                        references[definition.Name.ToLower()].Add(0);
                                }
                                references[definition.Name.ToLower()][index]++;
                            }
                        }
                    }
                    index++;
                }

                foreach (KeyValuePair<string, List<int>> reference in references)
                {
                    string referenceLine = string.Format("{0}, ", reference.Key);
                    foreach (int count in reference.Value)
                        referenceLine += string.Format("{0}, ", count.ToString());

                    sw.WriteLine(referenceLine.TrimEnd(new char[] { ',', ' ' }));
                }
            }
        }
        #endregion // Controller Methods
    } // ReferenceComparisionReport
} // RSG.Model.Report.Reports
