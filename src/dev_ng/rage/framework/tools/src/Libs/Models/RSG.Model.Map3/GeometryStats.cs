﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Model.Common;

namespace RSG.Model.Map3
{
    internal class GeometryStats : IGeometryStats
    {
        internal GeometryStats(string archetypeName, float heuristic)
        {
            archetypeName_ = archetypeName;
            heuristic_ = heuristic;
        }

        public string ArchetypeName
        {
            get { return archetypeName_; }
        }

        public float Heuristic
        {
            get { return heuristic_; }
        }

        private string archetypeName_;
        private float heuristic_;
    }
}
