﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;

namespace RSG.Model.Map
{
    /// <summary>
    /// Map component interface.
    /// </summary>
    public interface IMapComponent : IAsset
    {
        IMapContainer Container { get; }
    } // IMapComponent
    
} // RSG.Model.Map namespace
