﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using RSG.Model.Common.GameText;

namespace RSG.Model.Asset.GameText
{
    /// <summary>
    /// 
    /// </summary>
    public class GameTextSection : IGameTextSection
    {
        /// <summary>
        /// 
        /// </summary>
        private class GameTextKey : IEquatable<GameTextKey>
        {
            public String Label { get; private set; }
            public RSG.Platform.Platform? Platform { get; private set; }

            public GameTextKey(String label, RSG.Platform.Platform? platform)
            {
                Label = label;
                Platform = platform;
            }

            public bool Equals(GameTextKey other)
            {
                return Object.Equals(Label, other.Label) && Object.Equals(Platform, other.Platform);
            }

            public bool Equals(object o)
            {
                return this.Equals(o as GameTextKey);
            }

            public override int GetHashCode()
            {
                int hash = Label.GetHashCode();
                if (Platform != null)
                {
                    hash ^= Platform.GetHashCode();
                }
                return hash;
            }

            public override String ToString()
            {
                return String.Format("{0} - {1}", Label, (Platform.HasValue ? Platform.ToString() : "else"));
            }
        }

        /// <summary>
        /// Name for this section.
        /// </summary>
        private String _name;

        /// <summary>
        /// 
        /// </summary>
        private IDictionary<GameTextKey, IGameTextEntry> _entries = new Dictionary<GameTextKey, IGameTextEntry>();

        /// <summary>
        /// Name of the section.
        /// </summary>
        public String Name
        {
            get { return _name; }
        }

        /// <summary>
        /// 
        /// </summary>
        public GameTextSection(String name)
        {
            _name = name;
        }

        /// <summary>
        /// Retrieves the text associated with a particular text label for the specified platform.
        /// </summary>
        /// <param name="label"></param>
        /// <param name="platform"></param>
        /// <returns></returns>
        public String GetText(String label, RSG.Platform.Platform platform)
        {
            // Does a platform specific string exist for this label?
            label = label.ToLower();

            IGameTextEntry entry;
            if (_entries.TryGetValue(new GameTextKey(label, platform), out entry))
            {
                return entry.Text;
            }

            // Check for a fallback value.
            if (_entries.TryGetValue(new GameTextKey(label, null), out entry))
            {
                return entry.Text;
            }

            // We weren't able to determine the text for the particular label/platform combination.  Bail.
            throw new KeyNotFoundException(String.Format("The '{0}' section doesn't contain the '{1}' label.", Name, label));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="label"></param>
        /// <param name="platform"></param>
        /// <param name="text"></param>
        public void AddText(String label, RSG.Platform.Platform? platform, String text)
        {
            // Make sure that we don't already have a piece of text associated with this label/platform combination.
            label = label.ToLower();
            GameTextKey key = new GameTextKey(label, platform);

            IGameTextEntry existingEntry;
            if (_entries.TryGetValue(key, out existingEntry))
            {
                Debug.Assert(false, "A game text entry with the specified label/platform combination already exists.");
                return;
            }

            // Add the new entry.
            _entries.Add(key, new GameTextEntry(label, platform, text));
        }
    } // GameTextSection
}
