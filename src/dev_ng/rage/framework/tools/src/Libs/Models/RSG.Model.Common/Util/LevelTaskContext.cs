﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using RSG.Base.Tasks;
using RSG.Model.Common.Map;

namespace RSG.Model.Common.Util
{
    /// <summary>
    /// 
    /// </summary>
    public class LevelTaskContext : TaskContext
    {
        /// <summary>
        /// 
        /// </summary>
        public ILevel Level
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public IMapHierarchy MapHierarchy
        {
            get
            {
                return Level.MapHierarchy;
            }
        }
        
        /// <summary>
        /// Default constructor.
        /// </summary>
        public LevelTaskContext(ILevel level)
            : this(CancellationToken.None, level)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="token"></param>
        public LevelTaskContext(CancellationToken token, ILevel level)
            : base(token)
        {
            Level = level;
        }
    } // OverlayLevelContext
}
