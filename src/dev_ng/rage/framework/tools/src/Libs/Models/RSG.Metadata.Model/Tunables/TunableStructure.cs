﻿//---------------------------------------------------------------------------------------------
// <copyright file="TunableStructure.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Tunables
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Xml;
    using System.Xml.Linq;
    using RSG.Base.Extensions;
    using RSG.Base.Logging;
    using RSG.Editor;
    using RSG.Editor.Model;
    using RSG.Metadata.Model.Definitions;
    using RSG.Metadata.Model.Definitions.Members;
    using RSG.Metadata.Model.Resources;

    /// <summary>
    /// Represents a tunable structure containing all tunables created from a single
    /// structure definition.
    /// </summary>
    public class TunableStructure : ModelBase, ITunableParent
    {
        #region Fields
        /// <summary>
        /// The private field for the <see cref="BasePath"/> property.
        /// </summary>
        private string _basePath;

        /// <summary>
        /// The private field used for the <see cref="InheritanceLevel"/> property.
        /// </summary>
        private TunableStructure _inheritanceParent;

        /// <summary>
        /// The private field used for the <see cref="InheritanceHierarchyDepth"/> property.
        /// </summary>
        private TunableStructure _inheritanceChild;

        /// <summary>
        /// The private field for the <see cref="Tunables"/> property.
        /// </summary>
        private ModelCollection<ITunable> _tunables;

        /// <summary>
        /// The private field for the <see cref="Definition"/> property.
        /// </summary>
        private IStructure _definition;

        /// <summary>
        /// The private field used for the <see cref="Tunables"/> property.
        /// </summary>
        private ReadOnlyModelCollection<ITunable> _readOnlyTunables;

        /// <summary>
        /// The private field used for the <see cref="UndoEngine"/> property.
        /// </summary>
        private UndoEngine _undoEngine;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="TunableStructure"/> class.
        /// </summary>
        /// <param name="definition">
        /// The definition that this structure tunable will represent.
        /// </param>
        /// <param name="undoEngine">
        /// The undo engine that will be associated with this model.
        /// </param>
        public TunableStructure(IStructure definition, UndoEngine undoEngine)
        {
            if (definition == null)
            {
                throw new SmartArgumentNullException(() => definition);
            }

            ValidationResult validationResult = definition.Validate(true);
            IList<IValidationError> errors = validationResult.GetErrorsIncludingChildren();
            if (errors.Count > 0)
            {
                string msg = StringTable.InvalidPscStructureData;
                string file = String.Empty;
                string line = String.Empty;
                string position = String.Empty;
                if (errors[0].HasLocationInformation)
                {
                    file = errors[0].Location.FullPath;
                    line = errors[0].Location.Line.ToStringInvariant();
                    position = errors[0].Location.Column.ToStringInvariant();
                }

                throw new MetadataException(
                    msg, definition.DataType, errors[0].Message, file, line, position);
            }

            this._undoEngine = undoEngine;
            this._definition = definition;
            this._tunables = new ModelCollection<ITunable>(this);
            using (new SuspendUndoEngine(undoEngine, SuspendUndoEngineMode.Both))
            {
                this.CreateTunables();
            }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="TunableStructure"/> class using the
        /// specified System.Xml.XmlReader as a initialising data provider.
        /// </summary>
        /// <param name="definition">
        /// The definition that this structure tunable will represent.
        /// </param>
        /// <param name="undoEngine">
        /// The undo engine that will be associated with this model.
        /// </param>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <param name="log">
        /// A object that is used to log any warnings or errors produced during the parsing of
        /// the specified reader.
        /// </param>
        public TunableStructure(
            IStructure definition, UndoEngine undoEngine, XmlReader reader, ILog log)
        {
            if (reader == null)
            {
                throw new SmartArgumentNullException(() => reader);
            }

            if (definition == null)
            {
                throw new SmartArgumentNullException(() => definition);
            }

            if (log == null)
            {
                throw new SmartArgumentNullException(() => log);
            }

            ValidationResult validationResult = definition.Validate(true);
            IList<IValidationError> errors = validationResult.GetErrorsIncludingChildren();
            if (errors.Count > 0)
            {
                string msg = StringTable.InvalidPscStructureData;
                string file = String.Empty;
                string line = String.Empty;
                string position = String.Empty;
                if (errors[0].HasLocationInformation)
                {
                    file = errors[0].Location.FullPath;
                    line = errors[0].Location.Line.ToStringInvariant();
                    position = errors[0].Location.Column.ToStringInvariant();
                }

                throw new MetadataException(
                    msg, definition.DataType, errors[0].Message, file, line, position);
            }

            reader.ValidateStartPosition(definition.MetadataName);

            this._undoEngine = undoEngine;
            this._basePath = reader.BaseURI;
            this._definition = definition;
            this._tunables = new ModelCollection<ITunable>(this);
            using (new SuspendUndoEngine(undoEngine, SuspendUndoEngineMode.Both))
            {
                this.CreateTunables(reader, log);
            }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="TunableStructure"/> class using the
        /// specified System.Xml.XmlReader as a initialising data provider.
        /// </summary>
        /// <param name="reader">
        /// The XmlReader object used to parse the tunable values.
        /// </param>
        /// <param name="undoEngine">
        /// The undo engine that will be associated with this model.
        /// </param>
        /// <param name="scope">
        /// A definition dictionary used to resolve structure references.
        /// </param>
        /// <param name="log">
        /// A object that is used to log any warnings or errors produced during the parsing of
        /// the specified reader.
        /// </param>
        public TunableStructure(
            XmlReader reader, UndoEngine undoEngine, IDefinitionDictionary scope, ILog log)
        {
            if (reader == null)
            {
                throw new SmartArgumentNullException(() => reader);
            }

            if (scope == null)
            {
                throw new SmartArgumentNullException(() => scope);
            }

            if (log == null)
            {
                throw new SmartArgumentNullException(() => log);
            }

            string type = reader.Name.Replace("__", "::");
            IStructure definition = scope.GetStructure(type);
            if (definition == null)
            {
                string msg = StringTable.UnresolvedRootMetadataType;
                throw new MetadataException(msg, type);
            }

            ValidationResult validationResult = definition.Validate(true);
            IList<IValidationError> errors = validationResult.GetErrorsIncludingChildren();
            if (errors.Count > 0)
            {
                string msg = StringTable.InvalidPscStructureData;
                string file = String.Empty;
                string line = String.Empty;
                string position = String.Empty;
                if (errors[0].HasLocationInformation)
                {
                    file = errors[0].Location.FullPath;
                    line = errors[0].Location.Line.ToStringInvariant();
                    position = errors[0].Location.Column.ToStringInvariant();
                }

                throw new MetadataException(
                    msg, definition.DataType, errors[0].Message, file, line, position);
            }

            this._undoEngine = undoEngine;
            this._basePath = reader.BaseURI;
            this._definition = definition;
            this._tunables = new ModelCollection<ITunable>(this);
            using (new SuspendUndoEngine(undoEngine, SuspendUndoEngineMode.Both))
            {
                this.CreateTunables(reader, log);
            }
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the base uri for the file this instance was loaded from.
        /// </summary>
        public string BasePath
        {
            get { return this._basePath; }
        }

        /// <summary>
        /// Gets the structure definition used to create this tunable structure.
        /// </summary>
        public IStructure Definition
        {
            get { return this._definition; }
        }

        /// <summary>
        /// Gets the depth of the inheritance hierarchy this tunable structure is currently
        /// inside.
        /// </summary>
        public int InheritanceHierarchyDepth
        {
            get
            {
                int parents = this.InheritanceLevel;
                int children = 0;
                TunableStructure child = this._inheritanceChild;
                while (child != null)
                {
                    children++;
                    child = child._inheritanceChild;
                }

                return parents + children + 1;
            }
        }

        /// <summary>
        /// Gets the inheritance level this tunable current resides in. This level represents
        /// the number of inheritance levels above this tunable that currently exist.
        /// </summary>
        public int InheritanceLevel
        {
            get
            {
                if (this._inheritanceParent == null)
                {
                    return 0;
                }

                return this._inheritanceParent.InheritanceLevel + 1;
            }
        }

        /// <summary>
        /// Gets a collection of the tunables that are defined for this structure.
        /// </summary>
        public ReadOnlyModelCollection<ITunable> Tunables
        {
            get
            {
                if (this._readOnlyTunables == null)
                {
                    this._readOnlyTunables = this._tunables.ToReadOnly();
                }

                return this._readOnlyTunables;
            }
        }

        /// <summary>
        /// Gets the tunable structure this instance belongs to.
        /// </summary>
        TunableStructure ITunableParent.TunableStructure
        {
            get { return this; }
        }

        /// <summary>
        /// Gets the instance of the <see cref="UndoEngine"/>class that this object is using as
        /// its undo engine.
        /// </summary>
        public override UndoEngine UndoEngine
        {
            get { return this._undoEngine; }
        }

        /// <summary>
        /// Gets the tunable at the specified index.
        /// </summary>
        /// <param name="index">
        /// The index of the tunable to return.
        /// </param>
        /// <returns>
        /// The tunable at the specified index.
        /// </returns>
        public ITunable this[int index]
        {
            get { return this._tunables[index]; }
        }

        /// <summary>
        /// Gets the tunable with the specified metadata name.
        /// </summary>
        /// <param name="metadataName">
        /// The metadata name of the tunable to return.
        /// </param>
        /// <returns>
        /// The tunable with the specified metadata name.
        /// </returns>
        public ITunable this[string metadataName]
        {
            get
            {
                StringComparison comparisonType = StringComparison.OrdinalIgnoreCase;
                foreach (ITunable tunable in this._tunables)
                {
                    string name = tunable.Member.MetadataName;
                    if (String.Equals(name, metadataName, comparisonType))
                    {
                        return tunable;
                    }
                }

                return null;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a XML document for a structure tunable with the specified data type and
        /// content.
        /// </summary>
        /// <param name="dataType">
        /// The data type of the structure the file is being created for. This is set as the
        /// XML element node name to give the created element.
        /// </param>
        /// <param name="content">
        /// The contents of the element created.
        /// </param>
        /// <returns>
        /// A XML document for a structure tunable with the specified data type and content.
        /// </returns>
        public static XDocument CreateXDocument(string dataType, params object[] content)
        {
            string name = dataType.Replace("::", "__");
            return new XDocument(new XElement(name, content));
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="ITunable"/> by
        /// looking at their values.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance by their values;
        /// otherwise, false.
        /// </returns>
        public bool EqualByValue(TunableStructure other)
        {
            if (other == null)
            {
                return false;
            }

            if (this.Tunables.Count != other.Tunables.Count)
            {
                return false;
            }

            for (int i = 0; i < this.Tunables.Count; i++)
            {
                if (!this.Tunables[i].EqualByValue(other.Tunables[i]))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Serialises this instance into the specified xml writer.
        /// </summary>
        /// <param name="writer">
        /// The xml writer to write this instance into.
        /// </param>
        /// <param name="serialiseDefaultTunables">
        /// A value indicating whether the tunables that have a default value should be written
        /// to the specified writer or not.
        /// </param>
        public void Serialise(XmlWriter writer, bool serialiseDefaultTunables)
        {
            foreach (ITunable tunable in this._tunables)
            {
                if (!serialiseDefaultTunables && tunable.HasDefaultValue)
                {
                    continue;
                }

                writer.WriteStartElement(tunable.Member.MetadataName);
                tunable.Serialise(writer, serialiseDefaultTunables);
                writer.WriteEndElement();
            }
        }

        /// <summary>
        /// Updates this tunables HasDefaultValue property.
        /// </summary>
        public void UpdateHasDefaultValue()
        {
        }

        /// <summary>
        /// Sets the source of this tunables value when the value is being inherited.
        /// </summary>
        /// <param name="parent">
        /// The tunable whose value will be used when this tunable is being inherited.
        /// </param>
        internal void SetInheritanceParent(TunableStructure parent)
        {
            if (Object.ReferenceEquals(parent, this._inheritanceParent))
            {
                return;
            }

            if (Object.ReferenceEquals(parent, this))
            {
                throw new NotSupportedException("Cannot set a tunable to parent itself.");
            }

            TunableStructure oldValue = this._inheritanceParent;
            this._inheritanceParent = parent;
            if (parent != null)
            {
                foreach (ITunable parentTunable in parent.Tunables)
                {
                    IMember member = parentTunable.Member;
                    foreach (ITunable tunable in this.Tunables)
                    {
                        if (Object.ReferenceEquals(member, tunable.Member))
                        {
                            ((TunableBase)tunable).SetInheritanceParent(parentTunable);
                            break;
                        }
                    }
                }
            }

            if (oldValue != null)
            {
                if (Object.ReferenceEquals(oldValue._inheritanceChild, this))
                {
                    oldValue._inheritanceChild = null;
                }

                PropertyChangedEventManager.RemoveHandler(
                    oldValue, this.OnInheritLevelChanged, "InheritanceLevel");
            }

            if (parent != null)
            {
                parent._inheritanceChild = this;
                PropertyChangedEventManager.AddHandler(
                    parent, this.OnInheritLevelChanged, "InheritanceLevel");
            }

            this.NotifyPropertyChanged("InheritanceLevel");
        }

        /// <summary>
        /// Creates the tunables that represent the associated definition members.
        /// </summary>
        private void CreateTunables()
        {
            MemberInfo members = this.Definition.GetMemberInformation();
            ITunable[] tunables = new ITunable[members.Count];
            TunableFactory factory = new TunableFactory(this);
            foreach (MemberOrderInfo memberInfo in members.Values)
            {
                ITunable tunable = factory.Create(memberInfo.Member);
                if (tunable == null)
                {
                    throw new MetadataException(
                        "Unable to create a tunable for the member {0}.",
                        memberInfo.Member.Name);
                }

                tunables[memberInfo.Position] = tunable;
            }

            this._tunables.AddRange(tunables);
        }

        /// <summary>
        /// Creates the tunables that represent the associated definition members using the
        /// specified System.Xml.XmlReader as a initialising data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// the tunables from.
        /// </param>
        /// <param name="log">
        /// A object that is used to log any warnings or errors produced during the parsing of
        /// the specified reader.
        /// </param>
        private void CreateTunables(XmlReader reader, ILog log)
        {
            IXmlLineInfo lineInfo = reader as IXmlLineInfo;
            string line = lineInfo.LineNumber.ToStringInvariant();
            string pos = lineInfo.LinePosition.ToStringInvariant();

            MemberInfo members = this.Definition.GetMemberInformation();
            ITunable[] tunables = new ITunable[members.Count];
            TunableFactory factory = new TunableFactory(this);
            reader.ReadStartElement();
            while (reader.MoveToElementOrComment())
            {
                if (reader.NodeType == XmlNodeType.Comment)
                {
                    reader.Skip();
                    continue;
                }

                MemberOrderInfo orderInfo;
                string memberLine = lineInfo.LineNumber.ToStringInvariant();
                string memberPos = lineInfo.LinePosition.ToStringInvariant();

                if (!members.TryGetValue(reader.Name, out orderInfo))
                {
                    string msg = StringTable.StructAdditionalMemberFoundError;
                    log.Warning(msg, reader.Name, memberLine, memberPos);
                    reader.Skip();
                    continue;
                }

                ITunable tunable = factory.Create(reader, orderInfo.Member, log);
                if (tunable == null)
                {
                    this.OnTunableCreationFailed(orderInfo, memberLine, memberPos);
                }

                tunables[orderInfo.Position] = tunable;
            }

            foreach (MemberOrderInfo memberInfo in members.Values)
            {
                int index = memberInfo.Position;
                if (tunables[index] == null)
                {
                    ITunable tunable = factory.Create(memberInfo.Member);
                    if (tunable == null)
                    {
                        this.OnTunableCreationFailed(memberInfo, line, pos);
                    }

                    tunables[index] = tunable;
                }
            }

            this._tunables.AddRange(tunables);
        }

        /// <summary>
        /// Called if the inherit level changes on the parent.
        /// </summary>
        /// <param name="sender">
        /// The inheritance parent whose values changed.
        /// </param>
        /// <param name="e">
        /// The event data.
        /// </param>
        private void OnInheritLevelChanged(object sender, PropertyChangedEventArgs e)
        {
            this.NotifyPropertyChanged("InheritanceLevel");
            this.NotifyPropertyChanged("ValueSourceInheritanceLevel");
        }

        /// <summary>
        /// Throws a exception due to a tunable being unable to be created.
        /// </summary>
        /// <param name="info">
        /// The member info for the tunable that failed.
        /// </param>
        /// <param name="line">
        /// The associated line number inside the data source if applicable.
        /// </param>
        /// <param name="pos">
        /// The associated line position number inside the data source if applicable.
        /// </param>
        private void OnTunableCreationFailed(MemberOrderInfo info, string line, string pos)
        {
            string type = info.Member.TypeName;
            UnknownMember unknownMember = info.Member as UnknownMember;
            if (unknownMember != null)
            {
                type = unknownMember.XmlNodeName;
            }

            string msg = StringTable.StructUnknownMemberError;
            throw new MetadataException(
                msg,
                info.Member.Name,
                type,
                this.Definition.ShortDataType,
                line,
                pos);
        }
        #endregion Methods
    } // RSG.Metadata.Model.Tunables.TunableStructure {Class}
} // RSG.Metadata.Model.Tunables {Namespace}
