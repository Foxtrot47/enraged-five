﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.Base.ConfigParser;

namespace RSG.Model.Map
{
    /// <summary>
    /// Represents a single map area, definied using the
    /// config parser
    /// </summary>
    public class MapArea : AssetContainerBase, IMapContainer
    {
        #region Members

        private ILevel m_level;
        private ContentNodeGroup m_areaNode;
        private IMapContainer m_container;

        #endregion // Members

        #region Properties

        /// <summary>
        /// Reference to the owner Level object.
        /// </summary>
        public ILevel Level
        {
            get { return m_level; }
            set
            {
                if (m_level == value)
                    return;

                SetPropertyValue(value, () => Level,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_level = (ILevel)newValue;
                        }
                ));
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        public ContentNodeGroup AreaNode
        {
            get { return m_areaNode; }
            set
            {
                if (m_areaNode == value)
                    return;

                ContentNodeGroup oldValue = this.m_areaNode;

                SetPropertyValue(value, () => AreaNode,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_areaNode = (ContentNodeGroup)newValue;
                        }
                ));

                OnGroupNodeChanged(oldValue, value);
            }
        }

        /// <summary>
        /// The immediate parent map container, or
        /// null if non exist
        /// </summary>
        public IMapContainer Container
        {
            get { return m_container; }
            set
            {
                if (m_container == value)
                    return;

                SetPropertyValue(value, () => Container,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_container = (IMapContainer)newValue;
                        }
                ));
            }
        }

        #endregion // Properties

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        public MapArea(ILevel level, ContentNodeGroup group, IMapContainer container)
        {
            this.Level = level;
            this.AreaNode = group;
            this.Container = container;
            this.Name = group.RawRelativePath;
        }

        #endregion // Constructors

        #region Property Changed Callbacks

        /// <summary>
        /// Called when the main group node gets changed
        /// </summary>
        private void OnGroupNodeChanged(ContentNodeGroup oldValue, ContentNodeGroup newValue)
        {
            this.Name = newValue.RawRelativePath;
        }

        #endregion // Property Changed Callbacks

        #region IEnumerable<IMapComponent>

        /// <summary>
        /// 
        /// </summary>
        IEnumerator<IMapComponent> IEnumerable<IMapComponent>.GetEnumerator()
        {
            foreach (IMapComponent component in this.AssetChildren.Where(c => c is IMapComponent == true))
            {
                yield return component;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerator)this);
        }

        #endregion // IEnumerable<IMapContainer>
    } // MapArea
} // RSG.Model.Map
