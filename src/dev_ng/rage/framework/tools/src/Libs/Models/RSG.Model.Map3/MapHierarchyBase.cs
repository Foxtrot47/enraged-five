﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common.Map;
using RSG.Model.Common;
using RSG.Base.Extensions;
using System.ComponentModel;
using RSG.Base.Collections;
using RSG.Base.Tasks;

namespace RSG.Model.Map3
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class MapHierarchyBase : AssetBase, IMapHierarchy
    {
        #region Properties
        /// <summary>
        /// Reference to the parent level
        /// </summary>
        [Browsable(false)]
        public ILevel OwningLevel
        {
            get;
            protected set;
        }

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public ObservableCollection<IMapNode> ChildNodes
        {
            get;
            protected set;
        }

        /// <summary>
        /// Returns all areas in this map hierarchy
        /// </summary>
        [Browsable(false)]
        public IEnumerable<IMapArea> AllAreas
        {
            get
            {
                return GetAreaEnumerator(ChildNodes);
            }
        }

        /// <summary>
        /// Returns all sections in this map hierarchy
        /// </summary>
        [Browsable(false)]
        public IEnumerable<IMapSection> AllSections
        {
            get
            {
                return GetSectionEnumerator(ChildNodes);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public IDictionary<String, IMapArchetype> ArchetypeLookup
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public MapStatStreamingCoordinator StreamingCoordinator
        {
            get;
            private set;
        }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public MapHierarchyBase(ILevel level, MapStatStreamingCoordinator streamingCoordinator)
            : base("Map Areas")
        {
            OwningLevel = level;
            ChildNodes = new ObservableCollection<IMapNode>();
            ArchetypeLookup = new Dictionary<String, IMapArchetype>();
            StreamingCoordinator = streamingCoordinator;
        }
        #endregion // Constructor(s)

        #region IMapHierarchy Implementation
        /// <summary>
        /// 
        /// </summary>
        /// <param name="archetype"></param>
        public void AddArchetypeToLookup(IMapArchetype archetype)
        {
            lock (ArchetypeLookup)
            {
                if (!ArchetypeLookup.ContainsKey(archetype.Name.ToLower()))
                {
                    ArchetypeLookup.Add(archetype.Name.ToLower(), archetype);
                }
            }
        }

        /// <summary>
        /// Load specific statistics for a single section.
        /// </summary>
        /// <param name="section"></param>
        /// <param name="statsToLoad"></param>
        /// <param name="async"></param>
        public void RequestStatisticsForSection(IMapSection section, IEnumerable<StreamableStat> statsToLoad, bool async)
        {
            StreamingCoordinator.LoadStats(new IMapSection[] { section }, statsToLoad, async);
        }

        /// <summary>
        /// Load specific statistics for a single section.
        /// </summary>
        /// <param name="section"></param>
        /// <param name="statsToLoad"></param>
        /// <param name="async"></param>
        public void RequestStatisticsForSections(IEnumerable<IMapSection> sections, IEnumerable<StreamableStat> statsToLoad, bool async)
        {
            StreamingCoordinator.LoadStats(sections, statsToLoad, async);
        }

        /// <summary>
        /// Load specific statistics for a list of archetypes.
        /// </summary>
        /// <param name="section"></param>
        /// <param name="statsToLoad"></param>
        /// <param name="async"></param>
        public void RequestStatisticsForMapArchetypes(IEnumerable<IMapArchetype> archetypes, IEnumerable<StreamableStat> statsToLoad, bool async)
        {
            StreamingCoordinator.LoadStats(archetypes, statsToLoad, async);
        }

        /// <summary>
        /// Load specific statistics for a drawable archetype.
        /// </summary>
        /// <param name="fragment"></param>
        /// <param name="statsToLoad"></param>
        /// <param name="async"></param>
        public void RequestStatisticsForDrawable(IDrawableArchetype drawable, IEnumerable<StreamableStat> statsToLoad, bool async)
        {
            StreamingCoordinator.LoadStats(new IDrawableArchetype[] { drawable }, statsToLoad, async);
        }

        /// <summary>
        /// Load specific statistics for a fragment archetype.
        /// </summary>
        /// <param name="fragment"></param>
        /// <param name="statsToLoad"></param>
        /// <param name="async"></param>
        public void RequestStatisticsForFragment(IFragmentArchetype fragment, IEnumerable<StreamableStat> statsToLoad, bool async)
        {
            StreamingCoordinator.LoadStats(new IFragmentArchetype[] { fragment }, statsToLoad, async);
        }

        /// <summary>
        /// Load specific statistics for an interior archetype.
        /// </summary>
        /// <param name="fragment"></param>
        /// <param name="statsToLoad"></param>
        /// <param name="async"></param>
        public void RequestStatisticsForInterior(IInteriorArchetype interior, IEnumerable<StreamableStat> statsToLoad, bool async)
        {
            StreamingCoordinator.LoadStats(new IInteriorArchetype[] { interior }, statsToLoad, async);
        }

        /// <summary>
        /// Load specific statistics for a stated anim archetype.
        /// </summary>
        /// <param name="fragment"></param>
        /// <param name="statsToLoad"></param>
        /// <param name="async"></param>
        public void RequestStatisticsForStatedAnim(IStatedAnimArchetype statedAnim, IEnumerable<StreamableStat> statsToLoad, bool async)
        {
            StreamingCoordinator.LoadStats(new IStatedAnimArchetype[] { statedAnim }, statsToLoad, async);
        }

        /// <summary>
        /// Load specific statistics for a room.
        /// </summary>
        /// <param name="fragment"></param>
        /// <param name="statsToLoad"></param>
        /// <param name="async"></param>
        public void RequestStatisticsForRoom(IRoom room, IEnumerable<StreamableStat> statsToLoad, bool async)
        {
            StreamingCoordinator.LoadStats(new IRoom[] { room }, statsToLoad, async);
        }

        /// <summary>
        /// Load specific statistics for an entity.
        /// </summary>
        /// <param name="fragment"></param>
        /// <param name="statsToLoad"></param>
        /// <param name="async"></param>
        public void RequestStatisticsForEntity(IEntity entity, IEnumerable<StreamableStat> statsToLoad, bool async)
        {
            StreamingCoordinator.LoadStats(new IEntity[] { entity }, statsToLoad, async);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="statsToLoad"></param>
        /// <param name="async"></param>
        public void RequestStatisticsForEntities(IEnumerable<IEntity> entities, IEnumerable<StreamableStat> statsToLoad, bool async)
        {
            StreamingCoordinator.LoadStats(entities, statsToLoad, async);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sections"></param>
        /// <param name="statsToLoad"></param>
        /// <returns></returns>
        public ITask CreateStatLoadTask(IEnumerable<IMapSection> sections, IEnumerable<StreamableStat> statsToLoad)
        {
            return StreamingCoordinator.CreateLoadTask(sections, statsToLoad);
        }
        #endregion // IMapHierarchy Implementation

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nodes"></param>
        /// <returns></returns>
        private IEnumerable<IMapArea> GetAreaEnumerator(IEnumerable<IMapNode> nodes)
        {
            foreach (IMapNode node in nodes)
            {
                if (node is IMapArea)
                {
                    IMapArea area = node as IMapArea;
                    yield return area;

                    foreach (IMapArea childArea in GetAreaEnumerator(area.ChildNodes))
                    {
                        yield return childArea;
                    }
                }
            }
        }

        /// <summary>
        /// Recursively traverses the tree hierarchy to retrieve all the sections
        /// </summary>
        /// <returns></returns>
        private IEnumerable<IMapSection> GetSectionEnumerator(IEnumerable<IMapNode> nodes)
        {
            foreach (IMapNode node in nodes)
            {
                if (node is IMapSection)
                {
                    yield return (IMapSection)node;
                }
                else if (node is IMapArea)
                {
                    IMapArea area = node as IMapArea;
                    foreach (IMapSection childSection in GetSectionEnumerator(area.ChildNodes))
                    {
                        yield return childSection;
                    }
                }
            }
        }
        #endregion // Private Methods
    } // MapHierarchy
}
