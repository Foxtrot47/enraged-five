﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Math;

namespace RSG.Model.Map.Statistics
{
    /// <summary>
    /// A class that contains all the statistics for a specfic
    /// definition in memory.
    /// </summary>
    public class DefinitionStatistics
    {
        #region Properties

        /// <summary>
        /// The size in bytes of the geometry for
        /// this definition
        /// </summary>
        public int GeometrySize
        {
            get;
            private set;
        }

        /// <summary>
        /// The size in bytes of the txd for
        /// this definition
        /// </summary>
        public int TXDSize
        {
            get;
            private set;
        }
        
        /// <summary>
        /// The number of polygons that this
        /// definition has
        /// </summary>
        public int PolygonCount
        {
            get;
            private set;
        }

        /// <summary>
        /// The density of polygons the
        /// definition has based on the size
        /// of the definitions bounding box
        /// </summary>
        public float PolygonDensity
        {
            get;
            private set;
        }

        /// <summary>
        /// The number of polygons that this
        /// definition has for any collision
        /// attached to it
        /// </summary>
        public int CollisionPolygonCount
        {
            get;
            private set;
        }

        /// <summary>
        /// The density of collision polygons the
        /// definition has based on the size
        /// of the definitions bounding box
        /// </summary>
        public float CollisionPolygonDensity
        {
            get;
            private set;
        }

        /// <summary>
        /// The density of collision polygons
        /// compared to the geometry polygons.
        /// If this is > 1.0 this is bad
        /// </summary>
        public float CollisionPolygonRatio
        {
            get;
            private set;
        }

        #endregion // Properties

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        public DefinitionStatistics()
        {
            this.GeometrySize = 0;
            this.TXDSize = 0;
            this.PolygonCount = 0;
            this.PolygonDensity = 0.0f;
            this.CollisionPolygonCount = 0;
            this.CollisionPolygonDensity = 0.0f;
            this.CollisionPolygonRatio = 0.0f;
        }

        #endregion // Constructors

        #region Public Methods

        /// <summary>
        /// Sets the geometry statistics for the definition
        /// </summary>
        public void AddGeometryStatistics(int geometrySize, int polycount, float inverseArea)
        {
            this.GeometrySize += geometrySize;
            this.PolygonCount += polycount;
            this.PolygonDensity = this.PolygonCount * inverseArea;
        }

        /// <summary>
        /// Sets the txd statistics for the definition
        /// </summary>
        public void AddTXDStatistics(int txdSize)
        {
            this.TXDSize += txdSize;
        }

        /// <summary>
        /// Sets the collision statistics for the definition, uses the
        /// currently polycount to get the collision ratio
        /// </summary>
        public void AddCollisionStatistics(int polycount, float inverseArea)
        {
            this.CollisionPolygonCount += polycount;
            this.CollisionPolygonDensity = this.CollisionPolygonCount * inverseArea;

            float polygonCount = (float)this.PolygonCount;
            if (polygonCount != 0.0f)
            {
                float ratio = (float)this.CollisionPolygonCount / polygonCount;
                this.CollisionPolygonRatio = ratio;
            }
            else
            {
                this.CollisionPolygonRatio = 0.0f;
            }
        }

        #endregion // Public Methods
    } // DefinitionStatistics

    /// <summary>
    /// Extenstion functions for a map section that concerntrate on statistics
    /// </summary>
    public static class SectionStatistics
    {
        public static int GetTotalCost(this MapSection section, InstanceType type, Boolean justUnique = true)
        {
            List<IMapDefinition> definitions = new List<IMapDefinition>();
            GetAppropriateDefinitions(ref definitions, section, type, justUnique);

            int geometryCost = 0;
            int txdCost = 0;
            List<String> uniqueTxds = new List<String>();
            foreach (IMapDefinition definition in definitions)
            {
                geometryCost += definition.Statistics.GeometrySize;
                if (definition.Attributes.ContainsKey(RSG.SceneXml.AttrNames.OBJ_TXD))
                {
                    String txdName = (String)definition.Attributes[RSG.SceneXml.AttrNames.OBJ_TXD];
                    if (!uniqueTxds.Contains(txdName))
                    {
                        txdCost += definition.Statistics.TXDSize;
                        uniqueTxds.Add(txdName);
                    }
                }
            }
            return geometryCost + txdCost;
        }

        public static int GetGeometryCost(this MapSection section, InstanceType type, Boolean justUnique = true)
        {
            List<IMapDefinition> definitions = new List<IMapDefinition>();
            GetAppropriateDefinitions(ref definitions, section, type, justUnique);

            int geometryCost = 0;
            foreach (IMapDefinition definition in definitions)
            {
                geometryCost += definition.Statistics.GeometrySize;
            }
            return geometryCost;
        }

        public static int GetTXDCost(this MapSection section, InstanceType type)
        {
            List<IMapDefinition> definitions = new List<IMapDefinition>();
            GetAppropriateDefinitions(ref definitions, section, type, true);

            int txdCost = 0;
            List<String> uniqueTxds = new List<String>();
            foreach (IMapDefinition definition in definitions)
            {
                if (definition.Attributes.ContainsKey(RSG.SceneXml.AttrNames.OBJ_TXD))
                {
                    String txdName = (String)definition.Attributes[RSG.SceneXml.AttrNames.OBJ_TXD];
                    if (!uniqueTxds.Contains(txdName))
                    {
                        txdCost += definition.Statistics.TXDSize;
                        uniqueTxds.Add(txdName);
                    }
                }
            }
            return txdCost;
        }

        public static int GetGeometryCount(this MapSection section, InstanceType type, Boolean justUnique = true)
        {
            List<IMapDefinition> definitions = new List<IMapDefinition>();
            GetAppropriateDefinitions(ref definitions, section, type, justUnique);

            return definitions.Count;
        }

        public static int GetTXDCount(this MapSection section, InstanceType type)
        {
            List<IMapDefinition> definitions = new List<IMapDefinition>();
            GetAppropriateDefinitions(ref definitions, section, type, false);

            List<String> uniqueTxds = new List<String>();
            foreach (IMapDefinition definition in definitions)
            {
                if (definition.Attributes.ContainsKey(RSG.SceneXml.AttrNames.OBJ_TXD))
                {
                    String txdName = (String)definition.Attributes[RSG.SceneXml.AttrNames.OBJ_TXD];
                    if (!uniqueTxds.Contains(txdName))
                    {
                        uniqueTxds.Add(txdName);
                    }                   
                }
            }

            return uniqueTxds.Count;
        }

        public static int GetPolycount(this MapSection section, InstanceType type, Boolean justUnique = true)
        {
            List<IMapDefinition> definitions = new List<IMapDefinition>();
            GetAppropriateDefinitions(ref definitions, section, type, justUnique);

            int count = 0;
            foreach (IMapDefinition definition in definitions)
            {
                count += definition.Statistics.PolygonCount;
            }
            return count;
        }

        public static int GetCollisionPolycount(this MapSection section, InstanceType type, Boolean justUnique = true)
        {
            List<IMapDefinition> definitions = new List<IMapDefinition>();
            GetAppropriateDefinitions(ref definitions, section, type, justUnique);

            int count = 0;
            foreach (IMapDefinition definition in definitions)
            {
                count += definition.Statistics.CollisionPolygonCount;
            }
            return count;
        }

        public static float GetPolycountDensity(this MapSection section, InstanceType type, Boolean justUnique = true)
        {
            List<IMapDefinition> definitions = new List<IMapDefinition>();
            float inverseArea = GetAppropriateDefinitions(ref definitions, section, type, justUnique);

            int count = 0;
            float maxDensity = float.MinValue;
            foreach (IMapDefinition definition in definitions)
            {
                count += definition.Statistics.PolygonCount;
                maxDensity = definition.Statistics.PolygonDensity > maxDensity ? definition.Statistics.PolygonDensity : maxDensity;
            }
            if (section.SectionType == SectionTypes.MAP || section.SectionType == SectionTypes.PROPGROUP)
            {
                return count * inverseArea;
            }
            else
            {
                return maxDensity == float.MinValue ? 0.0f : maxDensity;
            }
        }

        public static float GetCollisionPolycountDensity(this MapSection section, InstanceType type, Boolean justUnique = true)
        {
            List<IMapDefinition> definitions = new List<IMapDefinition>();
            float inverseArea = GetAppropriateDefinitions(ref definitions, section, type, justUnique);

            int count = 0;
            float maxDensity = float.MinValue;
            foreach (IMapDefinition definition in definitions)
            {
                count += definition.Statistics.CollisionPolygonCount;
                maxDensity = definition.Statistics.CollisionPolygonCount > maxDensity ? definition.Statistics.CollisionPolygonCount : maxDensity;
            }
            if (section.SectionType == SectionTypes.MAP || section.SectionType == SectionTypes.PROPGROUP)
            {
                return count * inverseArea;
            }
            else
            {
                return maxDensity == float.MinValue ? 0.0f : maxDensity;
            }
        }

        public static float GetCollisionRatio(this MapSection section, InstanceType type, Boolean justUnique = true)
        {
            List<IMapDefinition> definitions = new List<IMapDefinition>();
            GetAppropriateDefinitions(ref definitions, section, type, justUnique);

            int polygonCount = 0;
            int collisionPolycount = 0;
            float maxRatio = float.MinValue;
            foreach (IMapDefinition definition in definitions)
            {
                collisionPolycount += definition.Statistics.CollisionPolygonCount;
                polygonCount += definition.Statistics.PolygonCount;
                if (definition.Statistics.PolygonCount != 0)
                {
                    float ratio = (float)definition.Statistics.CollisionPolygonCount / polygonCount;
                    maxRatio = ratio > maxRatio ? ratio : maxRatio;
                }
            }
            if (section.SectionType == SectionTypes.MAP || section.SectionType == SectionTypes.PROPGROUP)
            {
                if (polygonCount != 0.0f)
                {
                    return (float)collisionPolycount / polygonCount;
                }
                else
                {
                    return 0.0f;
                }
            }
            else
            {
                return maxRatio;
            }
        }

        public static float GetLodDistance(this MapSection section, InstanceType type)
        {
            return GetAppropriateLodDistance(section, type);
        }

        /// <summary>
        /// Gets the definitions, that contain the statistics, for a specific section. This is
        /// based on the section type and state.
        /// </summary>
        private static float GetAppropriateDefinitions(ref List<IMapDefinition> appropriate, MapSection section, InstanceType type, Boolean justUnique)
        {
            BoundingBox3f bbox = null;
            if (section.SectionType == SectionTypes.MAP || section.SectionType == SectionTypes.PROPGROUP)
            {
                if (section.Instances != null)
                {
                    foreach (MapInstance instance in section.Instances)
                    {
                        if (instance.ResolvedDefinition != null)
                        {
                            if (type == InstanceType.REF)
                            {
                                if (section.Definitions == null || !section.Definitions.Contains(instance.ResolvedDefinition))
                                {
                                    if (justUnique == true)
                                    {
                                        if (!appropriate.Contains(instance.ResolvedDefinition))
                                        {
                                            appropriate.Add(instance.ResolvedDefinition);
                                            if (bbox == null)
                                                bbox = new BoundingBox3f(instance.WorldBoundingBox);
                                            else
                                                bbox.Expand(instance.WorldBoundingBox);
                                        }
                                    }
                                    else
                                    {
                                        appropriate.Add(instance.ResolvedDefinition);
                                        if (bbox == null)
                                            bbox = new BoundingBox3f(instance.WorldBoundingBox);
                                        else
                                            bbox.Expand(instance.WorldBoundingBox);
                                    }                                    
                                }
                            }
                            else if (section.Definitions != null && section.Definitions.Contains(instance.ResolvedDefinition))
                            {
                                if (instance.InstanceType == type)
                                {
                                    if (justUnique == true)
                                    {
                                        if (!appropriate.Contains(instance.ResolvedDefinition))
                                        {
                                            appropriate.Add(instance.ResolvedDefinition);
                                            if (bbox == null)
                                                bbox = new BoundingBox3f(instance.WorldBoundingBox);
                                            else
                                                bbox.Expand(instance.WorldBoundingBox);
                                        }
                                    }
                                    else
                                    {
                                        appropriate.Add(instance.ResolvedDefinition);
                                        if (bbox == null)
                                            bbox = new BoundingBox3f(instance.WorldBoundingBox);
                                        else
                                            bbox.Expand(instance.WorldBoundingBox);
                                    }    
                                }
                            }
                        }
                    }
                }
            }
            else if (type == InstanceType.HD)
            {
                if (section.Definitions != null)
                {
                    foreach (IMapDefinition definition in section.Definitions)
                    {
                        if (!appropriate.Contains(definition))
                        {
                            if (justUnique == true)
                            {
                                if (!appropriate.Contains(definition))
                                {
                                    appropriate.Add(definition);
                                    if (bbox == null)
                                        bbox = new BoundingBox3f(definition.LocalBoundingBox);
                                    else
                                        bbox.Expand(definition.LocalBoundingBox);
                                }
                            }
                            else
                            {
                                appropriate.Add(definition);
                                if (bbox == null)
                                    bbox = new BoundingBox3f(definition.LocalBoundingBox);
                                else
                                    bbox.Expand(definition.LocalBoundingBox);
                            }    
                        }
                    }
                }
            }
            if (section.PropGroup != null)
            {
                BoundingBox3f propGroupBox = GetAppropriateDefinitionsForPropGroup(ref appropriate, section.PropGroup, type, justUnique);
                if (propGroupBox != null)
                {
                    if (bbox == null)
                        bbox = new BoundingBox3f(propGroupBox);
                    else
                        bbox.Expand(propGroupBox);
                }
            }

            return GetInverseAreaFromBoundingBox(bbox);
        }

        private static BoundingBox3f GetAppropriateDefinitionsForPropGroup(ref List<IMapDefinition> appropriate, MapSection section, InstanceType type, Boolean justUnique)
        {
            BoundingBox3f bbox = null;
            if (section.SectionType == SectionTypes.MAP || section.SectionType == SectionTypes.PROPGROUP)
            {
                if (section.Instances != null)
                {
                    foreach (MapInstance instance in section.Instances)
                    {
                        if (instance.ResolvedDefinition != null)
                        {
                            if (type == InstanceType.REF)
                            {
                                if (section.Definitions == null || !section.Definitions.Contains(instance.ResolvedDefinition))
                                {
                                    if (justUnique == true)
                                    {
                                        if (!appropriate.Contains(instance.ResolvedDefinition))
                                        {
                                            appropriate.Add(instance.ResolvedDefinition);
                                            if (bbox == null && instance.WorldBoundingBox != null)
                                                bbox = new BoundingBox3f(instance.WorldBoundingBox);
                                            else if (instance.WorldBoundingBox != null)
                                                bbox.Expand(instance.WorldBoundingBox);
                                        }
                                    }
                                    else
                                    {
                                        appropriate.Add(instance.ResolvedDefinition);
                                        if (bbox == null && instance.WorldBoundingBox != null)
                                            bbox = new BoundingBox3f(instance.WorldBoundingBox);
                                        else if (instance.WorldBoundingBox != null)
                                            bbox.Expand(instance.WorldBoundingBox);
                                    }
                                }
                            }
                            else if (section.Definitions != null && section.Definitions.Contains(instance.ResolvedDefinition))
                            {
                                if (instance.InstanceType == type)
                                {
                                    if (justUnique == true)
                                    {
                                        if (!appropriate.Contains(instance.ResolvedDefinition))
                                        {
                                            appropriate.Add(instance.ResolvedDefinition);
                                            if (bbox == null && instance.WorldBoundingBox != null)
                                                bbox = new BoundingBox3f(instance.WorldBoundingBox);
                                            else if (instance.WorldBoundingBox != null)
                                                bbox.Expand(instance.WorldBoundingBox);
                                        }
                                    }
                                    else
                                    {
                                        appropriate.Add(instance.ResolvedDefinition);
                                        if (bbox == null && instance.WorldBoundingBox != null)
                                            bbox = new BoundingBox3f(instance.WorldBoundingBox);
                                        else if (instance.WorldBoundingBox != null)
                                            bbox.Expand(instance.WorldBoundingBox);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else if (type == InstanceType.HD)
            {
                if (section.Definitions != null)
                {
                    foreach (IMapDefinition definition in section.Definitions)
                    {
                        if (!appropriate.Contains(definition))
                        {
                            if (justUnique == true)
                            {
                                if (!appropriate.Contains(definition))
                                {
                                    appropriate.Add(definition);
                                    if (bbox == null)
                                        bbox = new BoundingBox3f(definition.LocalBoundingBox);
                                    else
                                        bbox.Expand(definition.LocalBoundingBox);
                                }
                            }
                            else
                            {
                                appropriate.Add(definition);
                                if (bbox == null)
                                    bbox = new BoundingBox3f(definition.LocalBoundingBox);
                                else
                                    bbox.Expand(definition.LocalBoundingBox);
                            }
                        }
                    }
                }
            }
            return bbox;
        }

        /// <summary>
        /// Uses the given bounding box to create and
        /// returns the inverse area for it. (Default return is 0.0f)
        /// </summary>
        private static float GetInverseAreaFromBoundingBox(RSG.Base.Math.BoundingBox3f surroundingBox)
        {
            if (surroundingBox != null)
            {
                float sectionWidth = surroundingBox.Max.X - surroundingBox.Min.X;
                float sectionLength = surroundingBox.Max.Y - surroundingBox.Min.Y;
                float sectionHeight = surroundingBox.Max.Z - surroundingBox.Min.Z;
                if (sectionWidth > 0.1f && sectionLength > 0.1f && sectionHeight > 0.1f)
                {
                    return 1.0f / (sectionWidth * sectionLength * sectionHeight);
                }
            }

            return 0.0f;
        }

        private static float GetAppropriateLodDistance(MapSection section, InstanceType type)
        {
            float maxDistance = float.MinValue;
            if (section.SectionType == SectionTypes.MAP || section.SectionType == SectionTypes.PROPGROUP)
            {
                if (section.Instances != null)
                {
                    foreach (MapInstance instance in section.Instances)
                    {
                        if (type == InstanceType.REF)
                        {
                            if (section.Definitions == null || !section.Definitions.Contains(instance.ResolvedDefinition))
                            {
                                maxDistance = instance.LodDistance > maxDistance ? instance.LodDistance : maxDistance;
                            }
                        }
                        else if (section.Definitions != null && section.Definitions.Contains(instance.ResolvedDefinition))
                        {
                            if (instance.InstanceType == type)
                            {
                                maxDistance = instance.LodDistance > maxDistance ? instance.LodDistance : maxDistance;
                            }
                        }
                    }
                }
            }
            else if (type == InstanceType.HD)
            {
                if (section.Definitions != null)
                {
                    foreach (IMapDefinition definition in section.Definitions)
                    {
                        if (definition.Attributes.ContainsKey(RSG.SceneXml.AttrNames.OBJ_LOD_DISTANCE))
                        {
                            float distance = (float)definition.Attributes[RSG.SceneXml.AttrNames.OBJ_LOD_DISTANCE];
                            maxDistance = distance > maxDistance ? distance : maxDistance;
                        }
                        else
                        {
                            float distance = 100.0f;
                            maxDistance = distance > maxDistance ? distance : maxDistance;
                        }
                    }
                }
            }
            if (section.PropGroup != null)
            {
                float distance = GetAppropriateLodDistanceForPropGroup(section.PropGroup, type);
                maxDistance = distance > maxDistance ? distance : maxDistance;
            }

            return maxDistance == float.MinValue ? 0.0f : maxDistance;
        }

        private static float GetAppropriateLodDistanceForPropGroup(MapSection section, InstanceType type)
        {
            float maxDistance = float.MinValue;
            if (section.SectionType == SectionTypes.MAP || section.SectionType == SectionTypes.PROPGROUP)
            {
                if (section.Instances != null)
                {
                    foreach (MapInstance instance in section.Instances)
                    {
                        if (type == InstanceType.REF)
                        {
                            if (section.Definitions == null || !section.Definitions.Contains(instance.ResolvedDefinition))
                            {
                                maxDistance = instance.LodDistance > maxDistance ? instance.LodDistance : maxDistance;
                            }
                        }
                        else if (section.Definitions != null && section.Definitions.Contains(instance.ResolvedDefinition))
                        {
                            if (instance.InstanceType == type)
                            {
                                maxDistance = instance.LodDistance > maxDistance ? instance.LodDistance : maxDistance;
                            }
                        }
                    }
                }
            }
            else if (type == InstanceType.HD)
            {
                if (section.Definitions != null)
                {
                    foreach (IMapDefinition definition in section.Definitions)
                    {
                        if (definition.Attributes.ContainsKey(RSG.SceneXml.AttrNames.OBJ_LOD_DISTANCE))
                        {
                            float distance = (float)definition.Attributes[RSG.SceneXml.AttrNames.OBJ_LOD_DISTANCE];
                            maxDistance = distance > maxDistance ? distance : maxDistance;
                        }
                        else
                        {
                            float distance = 100.0f;
                            maxDistance = distance > maxDistance ? distance : maxDistance;
                        }
                    }
                }
            }
            return maxDistance == float.MinValue ? 0.0f : maxDistance;
        }
    }

    /// <summary>
    /// A base class for all the statistics so that a map section can use them all
    /// </summary>
    public abstract class Base
    {
        #region Total Cost Statistics

        public abstract int GetHighTotalCost();
        public abstract int GetLodTotalCost();
        public abstract int GetSLodTotalCost();

        #endregion // Total Cost Statistics

        #region Geometry Cost Statistics

        public abstract int GetHighGeometryCost();
        public abstract int GetLodGeometryCost();
        public abstract int GetSLodGeometryCost();

        #endregion // Geometry Cost Statistics

        #region TXD Cost Statistics

        public abstract int GetHighTXDCost();
        public abstract int GetLodTXDCost();
        public abstract int GetSLodTXDCost();

        #endregion // TXD Cost Statistics

        #region Geometry Count Statistics

        public abstract int GetHighGeometryCount();
        public abstract int GetLodGeometryCount();
        public abstract int GetSLodGeometryCount();

        #endregion // Geometry Count Statistics

        #region TXD Count Statistics

        public abstract int GetHighTXDCount();
        public abstract int GetLodTXDCount();
        public abstract int GetSLodTXDCount();

        #endregion // TXD Count Statistics

        #region Polygon Count Statistics

        public abstract int GetHighPolygonCount();
        public abstract int GetLodPolygonCount();
        public abstract int GetSLodPolygonCount();

        #endregion // Polygon Count Statistics

        #region Polygon Density Statistics

        public abstract float GetHighPolygonDensity();
        public abstract float GetLodPolygonDensity();
        public abstract float GetSLodPolygonDensity();

        #endregion // Polygon Density Statistics

        #region Collision Statistics

        public abstract int GetCollisionPolygonCount();
        public abstract float GetCollisionPolygonDensity();
        public abstract float GetCollisionPolygonRatio();

        #endregion // Collision Statistics

        #region LOD Distance Statistics

        public abstract int GetHighLodDistance();
        public abstract int GetLodLodDistance();
        public abstract int GetSLodLodDistance();

        #endregion // LOD Distance Statistics

        #region Reference Total Cost Statistics

        public abstract int GetAllRefTotalCost();
        public abstract int GetUniqueRefTotalCost();
        
        #endregion // Reference Cost Statistics

        #region Reference Geometry Cost Statistics

        public abstract int GetAllRefGeometryCost();
        public abstract int GetUniqueRefGeometryCost();

        #endregion // Reference Geometry Cost Statistics

        #region Reference TXD Statistics

        public abstract int GetAllRefTXDCost();
        public abstract int GetAllRefTXDCount();

        #endregion // Reference TXD Statistics

        #region Reference Count Statistics

        public abstract int GetAllRefCount();
        public abstract int GetUniqueRefCount();

        #endregion // Reference Count Statistics

        #region Reference Polygon Count Statistics

        public abstract int GetReferencePolygonCountAll();
        public abstract int GetReferencePolygonCountUnique();

        #endregion // Reference Polygon Count Statistics

        #region Reference Polygon Density Statistics

        public abstract float GetReferencePolygonDensityAll();
        public abstract float GetReferencePolygonDensityUnique();

        #endregion // Reference Polygon Density Statistics

        #region Reference Collision Count Statistics

        public abstract int GetReferenceCollisionPolygonCountAll();
        public abstract int GetReferenceCollisionPolygonCountUnique();

        #endregion // Reference Collision Count Statistics

        #region Reference Collision Density Statistics

        public abstract float GetReferenceCollisionPolygonDensityAll();
        public abstract float GetReferenceCollisionPolygonDensityUnique();

        #endregion // Reference Collision Density Statistics

        #region Reference Collision Ratio Statistics

        public abstract float GetReferenceCollisionPolygonRatioAll();
        public abstract float GetReferenceCollisionPolygonRatioUnique();

        #endregion // Reference Collision Ratio Statistics

        #region Reference LOD Distance Statistics

        public abstract float GetReferenceLodDistance();
        
        #endregion Reference // LOD Distance Statistics

    }
}
