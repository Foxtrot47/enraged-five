﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using RSG.Base.Collections;

namespace RSG.Model.Map.ViewModel
{
    /// <summary>
    /// 
    /// </summary>
    public class MapMiloViewModel :
        RSG.Base.Editor.HierarchicalViewModelBase, IMapViewModelComponent
    {
        #region Properties

        /// <summary>
        /// The name of the map section
        /// </summary>
        public String Name
        {
            get { return Model.Name.ToLower(); }
        }

        /// <summary>
        /// The model reference that this view model represents.
        /// </summary>
        public MapMilo Model
        {
            get { return m_model; }
            set { m_model = value; }
        }
        private MapMilo m_model;

        /// <summary>
        /// The level that this section belongs to.
        /// </summary>
        public LevelViewModel Level
        {
            get;
            private set;
        }
        
        /// <summary>
        /// The user data that can be attached to this view model
        /// </summary>
        public UserData ViewModelUserData
        {
            get { return m_viewModelUserData; }
            set { m_viewModelUserData = value; }
        }
        private UserData m_viewModelUserData = new UserData();

        /// <summary>
        /// A list of textures that this definition has attached to
        /// it. This list only has unique textures in it, no duplicates
        /// </summary>
        public ObservableCollection<IViewModel> Instances
        {
            get { return m_instances; }
            set
            {
                SetPropertyValue(value, () => this.Instances,
                    new PropertySetDelegate(delegate(Object newValue) { m_instances = (ObservableCollection<IViewModel>)newValue; }));
            }
        }
        private ObservableCollection<IViewModel> m_instances;

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        public MapMiloViewModel()
        {
        }

        /// <summary>
        /// Default Constructor
        /// </summary>
        public MapMiloViewModel(IViewModel parent, MapMilo model)
        {
            this.Parent = parent;
            this.Model = model;
            this.Instances = new ObservableCollection<IViewModel>();
            this.Instances.Add(new MapInstanceViewModel());

            if (parent is IMapViewModelContainer)
            {
                IMapViewModelContainer containerParent = parent as IMapViewModelContainer;
                LevelViewModel level = containerParent as LevelViewModel;
                while (containerParent != null && !(level is LevelViewModel))
                {
                    containerParent = containerParent.Parent as IMapViewModelContainer;
                    level = containerParent as LevelViewModel;
                }

                this.Level = level;
            }

            foreach (MapInstance instance in model.Instances)
            {
                this.Instances.Add(new MapInstanceViewModel(this, instance));
            }
            foreach (MapRoom room in model.Rooms)
            {
                this.Instances.Add(new MapRoomViewModel(this, room));
            }
        }

        #endregion // Constructors

        #region Override Functions

        protected override void OnFirstExpanded()
        {
            ObservableCollection<IViewModel> instances = new ObservableCollection<IViewModel>();
            if (this.Model.Instances != null)
            {
                foreach (MapInstance instance in this.Model.Instances)
                {
                    instances.Add(new MapInstanceViewModel(this, instance));
                }
            }
            if (this.Model.Rooms != null)
            {
                foreach (MapRoom room in this.Model.Rooms)
                {
                    instances.Add(new MapRoomViewModel(this, room));
                }
            }
            this.Instances = instances;
        }

        #endregion // Override Functions
    }

    public class MapRoomViewModel :
        RSG.Base.Editor.HierarchicalViewModelBase, IMapViewModelComponent
    {
        #region Properties

        /// <summary>
        /// The name of the map section
        /// </summary>
        public String Name
        {
            get { return Model.Name.ToLower(); }
        }

        /// <summary>
        /// The model reference that this view model represents.
        /// </summary>
        public MapRoom Model
        {
            get { return m_model; }
            set { m_model = value; }
        }
        private MapRoom m_model;

        /// <summary>
        /// The level that this section belongs to.
        /// </summary>
        public LevelViewModel Level
        {
            get;
            private set;
        }
        
        /// <summary>
        /// The user data that can be attached to this view model
        /// </summary>
        public UserData ViewModelUserData
        {
            get { return m_viewModelUserData; }
            set { m_viewModelUserData = value; }
        }
        private UserData m_viewModelUserData = new UserData();

        /// <summary>
        /// A list of textures that this definition has attached to
        /// it. This list only has unique textures in it, no duplicates
        /// </summary>
        public ObservableCollection<MapInstanceViewModel> Instances
        {
            get { return m_instances; }
            set
            {
                SetPropertyValue(value, () => this.Instances,
                    new PropertySetDelegate(delegate(Object newValue) { m_instances = (ObservableCollection<MapInstanceViewModel>)newValue; }));
            }
        }
        private ObservableCollection<MapInstanceViewModel> m_instances;

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Default Constructor
        /// </summary>
        public MapRoomViewModel(IViewModel parent, MapRoom model)
        {
            this.Parent = parent;
            this.Model = model;
            this.Instances = new ObservableCollection<MapInstanceViewModel>();
            this.Instances.Add(new MapInstanceViewModel());
        }

        #endregion // Constructors

        #region Override Functions

        protected override void OnFirstExpanded()
        {
            ObservableCollection<MapInstanceViewModel> instances = new ObservableCollection<MapInstanceViewModel>();
            if (this.Model.Instances != null)
            {
                foreach (MapInstance instance in this.Model.Instances)
                {
                    instances.Add(new MapInstanceViewModel(this, instance));
                }
            }
            this.Instances = instances;
        }

        #endregion // Override Functions
    }
}
