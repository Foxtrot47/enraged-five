﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Collections;

namespace RSG.Model.Common.Mission
{
    /// <summary>
    /// Interface for a collection of missions.
    /// </summary>
    public interface IMissionCollection :
        IAsset,
        IEnumerable<IMission>,
        ICollection<IMission>
    {
        /// <summary>
        /// Missions that this collection contains.
        /// </summary>
        ObservableCollection<IMission> Missions { get; }
    } // IMissionCollection
}
