﻿using System;
using System.Collections.Generic;
using System.IO;
using RSG.Base.ConfigParser;
using RSG.Base.Configuration;
using RSG.SourceControl.Perforce;

namespace RSG.Model.Perforce.Util
{

    /// <summary>
    /// 
    /// </summary>
    public static class Creation
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="gv"></param>
        /// <param name="p4"></param>
        /// <returns></returns>
        public static Workspace GetWorkspace(IConfig config, out P4 p4)
        {
            String oldRoot = Directory.GetCurrentDirectory();

            Directory.SetCurrentDirectory(config.CoreProject.Root);
            p4 = new P4();
            p4.Connect();

            return (new Workspace(p4));
        }
    }

} // RSG.Model.Perforce.Util namespace
