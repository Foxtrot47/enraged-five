﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor;

namespace RSG.Model.Map.Statistics
{
    public enum MapSectionStatisticType
    {
        Size,
        Double,
        Distance,
        Percentage,
        Integer,
    }
    
    /// <summary>
    /// 
    /// </summary>
    public class MapSectionStatistic : ModelBase
    {
        #region Members
        protected String m_name;
        protected String m_description;
        protected MapSectionStatisticType m_statisticType;
        protected int? m_totalStatistic = null;
        protected int? m_highDynamicStatistic = null;
        protected int? m_highNonDynamicStatistic = null;
        protected int? m_lodDynamicStatistic = null;
        protected int? m_lodNonDynamicStatistic = null;
        protected int? m_slodDynamicStatistic = null;
        protected int? m_slodNonDynamicStatistic = null;
        #endregion // Members

        #region Properties
        /// <summary>
        /// The name of the statistic
        /// </summary>
        public String Name
        {
            get { return m_name; }
            set
            {
                if (m_name == value)
                    return;

                SetPropertyValue(value, () => Name,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_name = (String)newValue;
                        }
                ));
            }
        }
        
        /// <summary>
        /// The description for the statistic
        /// </summary>
        public String Description
        {
            get { return m_description; }
            set
            {
                if (m_description == value)
                    return;

                SetPropertyValue(value, () => Description,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_description = (String)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// The type of statistic
        /// </summary>
        public MapSectionStatisticType StatisticType
        {
            get { return m_statisticType; }
            set
            {
                if (Object.Equals(m_statisticType, value))
                    return;

                SetPropertyValue(value, () => StatisticType,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_statisticType = (MapSectionStatisticType)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual int? TotalStatistic
        {
            get { return m_totalStatistic; }
            set
            {
                if (Object.Equals(m_totalStatistic, value))
                    return;

                SetPropertyValue(value, () => TotalStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_totalStatistic = (int?)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual int? HighDynamicStatistic
        {
            get { return m_highDynamicStatistic; }
            set
            {
                if (Object.Equals(m_highDynamicStatistic, value))
                    return;

                SetPropertyValue(value, m_highDynamicStatistic, () => HighDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_highDynamicStatistic = (int?)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual int? HighNonDynamicStatistic
        {
            get { return m_highNonDynamicStatistic; }
            set
            {
                if (Object.Equals(m_highNonDynamicStatistic, value))
                    return;

                SetPropertyValue(value, m_highNonDynamicStatistic, () => HighNonDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_highNonDynamicStatistic = (int?)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual int? LodDynamicStatistic
        {
            get { return m_lodDynamicStatistic; }
            set
            {
                if (Object.Equals(m_lodDynamicStatistic, value))
                    return;

                SetPropertyValue(value, m_lodDynamicStatistic, () => LodDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_lodDynamicStatistic = (int?)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual int? LodNonDynamicStatistic
        {
            get { return m_lodNonDynamicStatistic; }
            set
            {
                if (Object.Equals(m_lodNonDynamicStatistic, value))
                    return;

                SetPropertyValue(value, m_lodNonDynamicStatistic, () => LodNonDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_lodNonDynamicStatistic = (int?)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual int? SlodDynamicStatistic
        {
            get { return m_slodDynamicStatistic; }
            set
            {
                if (Object.Equals(m_slodDynamicStatistic, value))
                    return;

                SetPropertyValue(value, m_slodDynamicStatistic, () => SlodDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_slodDynamicStatistic = (int?)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual int? SlodNonDynamicStatistic
        {
            get { return m_slodNonDynamicStatistic; }
            set
            {
                if (Object.Equals(m_slodNonDynamicStatistic, value))
                    return;

                SetPropertyValue(value, m_slodNonDynamicStatistic, () => SlodNonDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_slodNonDynamicStatistic = (int?)newValue;
                        }
                ));
            }
        }
        #endregion // Properties
        
        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public MapSectionStatistic(String name, String description, MapSectionStatisticType type)
        {
            this.Name = name;
            this.Description = description;
            this.StatisticType = type;
        }
        #endregion // Constructors
        
        #region Protected Functions
        /// <summary>
        /// Turns a integer size in bytes to a string representation
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        public static String SizeToString(ulong size)
        {
            String prettySize = String.Empty;
            ulong bytes = size;
            if (bytes < 1024 * 1024)
            {
                ulong kbytes = bytes >> 10;
                ulong kbyte = 1024;
                ulong leftover = bytes - (kbytes * kbyte);

                if (leftover > 0)
                {
                    prettySize = String.Format("{0} KB", ((double)bytes / (double)kbyte).ToString("F1"));
                }
                else
                {
                    prettySize = String.Format("{0} KB", bytes >> 10);
                }
            }
            else
            {
                ulong mbytes = bytes >> 20;
                ulong mbyte = 1024 * 1024;
                ulong leftover = bytes - (mbytes * mbyte);

                if (leftover > 0)
                {
                    prettySize = String.Format("{0} MB", ((double)bytes / (double)mbyte).ToString("F1"));
                }
                else
                {
                    prettySize = String.Format("{0} MB", bytes >> 20);
                }
            }
            return prettySize;
        }
        #endregion // Protected Functions
    } // MapSectionStatistic

    ///// <summary>
    ///// 
    ///// </summary>
    //public class SizeStatisticBase : MapSectionStatistic
    //{
    //    #region Constructors

    //    /// <summary>
    //    /// Default constructor
    //    /// </summary>
    //    public SizeStatisticBase(String name, String description)
    //        : base(name, description, MapSectionStatisticType.Size)
    //    {
    //    }

    //    #endregion // Constructors
    //} // SizeStatisticBase

    /// <summary>
    /// 
    /// </summary>
    public class CountStatisticBase : MapSectionStatistic
    {
        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        public CountStatisticBase(String name, String description)
            : base(name, description, MapSectionStatisticType.Integer)
        {
        }

        #endregion // Constructors
    } // CountStatisticBase
    
    /// <summary>
    /// 
    /// </summary>
    public class DensityStatisticBase : MapSectionStatistic
    {
        #region Members
        protected new double? m_totalStatistic = null;
        protected new double? m_highDynamicStatistic = null;
        protected new double? m_highNonDynamicStatistic = null;
        protected new double? m_lodDynamicStatistic = null;
        protected new double? m_lodNonDynamicStatistic = null;
        protected new double? m_slodDynamicStatistic = null;
        protected new double? m_slodNonDynamicStatistic = null;
        #endregion // Members

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public new double? TotalStatistic
        {
            get { return m_totalStatistic; }
            set
            {
                if (Object.Equals(m_totalStatistic, value))
                    return;

                SetPropertyValue(value, () => TotalStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_totalStatistic = (double?)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public new double? HighDynamicStatistic
        {
            get { return m_highDynamicStatistic; }
            set
            {
                if (Object.Equals(m_highDynamicStatistic, value))
                    return;

                SetPropertyValue(value, m_highDynamicStatistic, () => HighDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_highDynamicStatistic = (double?)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public new double? HighNonDynamicStatistic
        {
            get { return m_highNonDynamicStatistic; }
            set
            {
                if (Object.Equals(m_highNonDynamicStatistic, value))
                    return;

                SetPropertyValue(value, m_highNonDynamicStatistic, () => HighNonDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_highNonDynamicStatistic = (double?)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public new double? LodDynamicStatistic
        {
            get { return m_lodDynamicStatistic; }
            set
            {
                if (Object.Equals(m_lodDynamicStatistic, value))
                    return;

                SetPropertyValue(value, m_lodDynamicStatistic, () => LodDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_lodDynamicStatistic = (double?)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public new double? LodNonDynamicStatistic
        {
            get { return m_lodNonDynamicStatistic; }
            set
            {
                if (Object.Equals(m_lodNonDynamicStatistic, value))
                    return;

                SetPropertyValue(value, m_lodNonDynamicStatistic, () => LodNonDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_lodNonDynamicStatistic = (double?)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public new double? SlodDynamicStatistic
        {
            get { return m_slodDynamicStatistic; }
            set
            {
                if (Object.Equals(m_slodDynamicStatistic, value))
                    return;

                SetPropertyValue(value, m_slodDynamicStatistic, () => SlodDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_slodDynamicStatistic = (double?)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public new double? SlodNonDynamicStatistic
        {
            get { return m_slodNonDynamicStatistic; }
            set
            {
                if (Object.Equals(m_slodNonDynamicStatistic, value))
                    return;

                SetPropertyValue(value, m_slodNonDynamicStatistic, () => SlodNonDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_slodNonDynamicStatistic = (double?)newValue;
                        }
                ));
            }
        }
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public DensityStatisticBase(String name, String description)
            : base(name, description, MapSectionStatisticType.Double)
        {
        }
        #endregion // Constructors
    } // DensityStatisticBase

    /// <summary>
    /// 
    /// </summary>
    public class RatioStatisticBase : MapSectionStatistic
    {
        #region Members
        protected new double? m_totalStatistic = null;
        protected new double? m_highDynamicStatistic = null;
        protected new double? m_highNonDynamicStatistic = null;
        protected new double? m_lodDynamicStatistic = null;
        protected new double? m_lodNonDynamicStatistic = null;
        protected new double? m_slodDynamicStatistic = null;
        protected new double? m_slodNonDynamicStatistic = null;
        #endregion // Members

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public new double? TotalStatistic
        {
            get { return m_totalStatistic; }
            set
            {
                if (Object.Equals(m_totalStatistic, value))
                    return;

                SetPropertyValue(value, () => TotalStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_totalStatistic = (double?)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public new double? HighDynamicStatistic
        {
            get { return m_highDynamicStatistic; }
            set
            {
                if (Object.Equals(m_highDynamicStatistic, value))
                    return;

                SetPropertyValue(value, m_highDynamicStatistic, () => HighDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_highDynamicStatistic = (double?)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public new double? HighNonDynamicStatistic
        {
            get { return m_highNonDynamicStatistic; }
            set
            {
                if (Object.Equals(m_highNonDynamicStatistic, value))
                    return;

                SetPropertyValue(value, m_highNonDynamicStatistic, () => HighNonDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_highNonDynamicStatistic = (double?)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public new double? LodDynamicStatistic
        {
            get { return m_lodDynamicStatistic; }
            set
            {
                if (Object.Equals(m_lodDynamicStatistic, value))
                    return;

                SetPropertyValue(value, m_lodDynamicStatistic, () => LodDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_lodDynamicStatistic = (double?)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public new double? LodNonDynamicStatistic
        {
            get { return m_lodNonDynamicStatistic; }
            set
            {
                if (Object.Equals(m_lodNonDynamicStatistic, value))
                    return;

                SetPropertyValue(value, m_lodNonDynamicStatistic, () => LodNonDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_lodNonDynamicStatistic = (double?)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public new double? SlodDynamicStatistic
        {
            get { return m_slodDynamicStatistic; }
            set
            {
                if (Object.Equals(m_slodDynamicStatistic, value))
                    return;

                SetPropertyValue(value, m_slodDynamicStatistic, () => SlodDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_slodDynamicStatistic = (double?)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public new double? SlodNonDynamicStatistic
        {
            get { return m_slodNonDynamicStatistic; }
            set
            {
                if (Object.Equals(m_slodNonDynamicStatistic, value))
                    return;

                SetPropertyValue(value, m_slodNonDynamicStatistic, () => SlodNonDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_slodNonDynamicStatistic = (double?)newValue;
                        }
                ));
            }
        }
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public RatioStatisticBase(String name, String description)
            : base(name, description, MapSectionStatisticType.Percentage)
        {
        }
        #endregion // Constructors
    } // RatioStatisticBase

    /// <summary>
    /// 
    /// </summary>
    public class DistanceStatisticBase : MapSectionStatistic
    {
        #region Members
        protected new float? m_totalStatistic = null;
        protected new float? m_highDynamicStatistic = null;
        protected new float? m_highNonDynamicStatistic = null;
        protected new float? m_lodDynamicStatistic = null;
        protected new float? m_lodNonDynamicStatistic = null;
        protected new float? m_slodDynamicStatistic = null;
        protected new float? m_slodNonDynamicStatistic = null;
        #endregion // Members

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public new float? TotalStatistic
        {
            get { return m_totalStatistic; }
            set
            {
                if (Object.Equals(m_totalStatistic, value))
                    return;

                SetPropertyValue(value, () => TotalStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_totalStatistic = (float?)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public new float? HighDynamicStatistic
        {
            get { return m_highDynamicStatistic; }
            set
            {
                if (Object.Equals(m_highDynamicStatistic, value))
                    return;

                SetPropertyValue(value, m_highDynamicStatistic, () => HighDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_highDynamicStatistic = (float?)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public new float? HighNonDynamicStatistic
        {
            get { return m_highNonDynamicStatistic; }
            set
            {
                if (Object.Equals(m_highNonDynamicStatistic, value))
                    return;

                SetPropertyValue(value, m_highNonDynamicStatistic, () => HighNonDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_highNonDynamicStatistic = (float?)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public new float? LodDynamicStatistic
        {
            get { return m_lodDynamicStatistic; }
            set
            {
                if (Object.Equals(m_lodDynamicStatistic, value))
                    return;

                SetPropertyValue(value, m_lodDynamicStatistic, () => LodDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_lodDynamicStatistic = (float?)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public new float? LodNonDynamicStatistic
        {
            get { return m_lodNonDynamicStatistic; }
            set
            {
                if (Object.Equals(m_lodNonDynamicStatistic, value))
                    return;

                SetPropertyValue(value, m_lodNonDynamicStatistic, () => LodNonDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_lodNonDynamicStatistic = (float?)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public new float? SlodDynamicStatistic
        {
            get { return m_slodDynamicStatistic; }
            set
            {
                if (Object.Equals(m_slodDynamicStatistic, value))
                    return;

                SetPropertyValue(value, m_slodDynamicStatistic, () => SlodDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_slodDynamicStatistic = (float?)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public new float? SlodNonDynamicStatistic
        {
            get { return m_slodNonDynamicStatistic; }
            set
            {
                if (Object.Equals(m_slodNonDynamicStatistic, value))
                    return;

                SetPropertyValue(value, m_slodNonDynamicStatistic, () => SlodNonDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_slodNonDynamicStatistic = (float?)newValue;
                        }
                ));
            }
        }
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public DistanceStatisticBase(String name, String description)
            : base(name, description, MapSectionStatisticType.Distance)
        {
        }
        #endregion // Constructors
    } // DistanceStatisticBase

    ///// <summary>
    ///// 
    ///// </summary>
    //public class TotalCostStatistic : SizeStatisticBase
    //{
    //    #region Constants

    //    private static readonly String StatisticDescription = "The cost of the everything in the container using the independent size.";

    //    #endregion // Constants

    //    #region Constructors
        
    //    /// <summary>
    //    /// Default constructor
    //    /// </summary>
    //    public TotalCostStatistic(RawSectionStatistics rawStats)
    //        : base(StatisticNames.TOTAL_COST, StatisticDescription)
    //    {
    //        var group = RawSectionStatistics.StatisticGroups.Instances;
    //        var instanceType = RawSectionStatistics.InstanceType.Total;

    //        this.m_highStatistic = rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].DrawableCost + rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].TXDCost;
    //        this.m_lodStatistic = rawStats[group][RawSectionStatistics.LodLevel.LOD][instanceType].DrawableCost + rawStats[group][RawSectionStatistics.LodLevel.LOD][instanceType].TXDCost;
    //        this.m_slodStatistic = rawStats[group][RawSectionStatistics.LodLevel.SLOD1][instanceType].DrawableCost + rawStats[group][RawSectionStatistics.LodLevel.SLOD1][instanceType].TXDCost;

    //        group = RawSectionStatistics.StatisticGroups.Interiors;
    //        this.m_interiorStatistic = rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].DrawableCost + rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].TXDCost;

    //        group = RawSectionStatistics.StatisticGroups.NonDrawableInstances;
    //        instanceType = RawSectionStatistics.InstanceType.Dynamic;
    //        this.m_dynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.Total][instanceType].DrawableCost + rawStats[group][RawSectionStatistics.LodLevel.Total][instanceType].TXDCost;

    //        instanceType = RawSectionStatistics.InstanceType.NonDynamic;
    //        this.m_nonDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.Total][instanceType].DrawableCost + rawStats[group][RawSectionStatistics.LodLevel.Total][instanceType].TXDCost;

    //        instanceType = RawSectionStatistics.InstanceType.Total;
    //        this.m_allPropsStatistic = rawStats[group][RawSectionStatistics.LodLevel.Total][instanceType].DrawableCost + rawStats[group][RawSectionStatistics.LodLevel.Total][instanceType].TXDCost;
    //        this.m_uniqueStatistic = this.m_allPropsStatistic;

    //        this.m_totalStatistic = this.m_highStatistic + this.m_lodStatistic + 
    //            this.m_slodStatistic + this.m_interiorStatistic + this.m_allPropsStatistic;
    //    }

    //    #endregion // Constructors
    //} // TotalCostStatistic

    ///// <summary>
    ///// 
    ///// </summary>
    //public class GeometryCostStatistic : SizeStatisticBase
    //{
    //    #region Constants

    //    private static readonly String StatisticDescription = "The cost of the geometry in the container using the independent size.";

    //    #endregion // Constants

    //    #region Constructors
        
    //    /// <summary>
    //    /// Default constructor
    //    /// </summary>
    //    public GeometryCostStatistic(RawSectionStatistics rawStats)
    //        : base(StatisticNames.GEOMETRY_COST, StatisticDescription)
    //    {
    //        var group = RawSectionStatistics.StatisticGroups.Instances;
    //        var instanceType = RawSectionStatistics.InstanceType.Total;

    //        this.m_highStatistic = rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].DrawableCost;
    //        this.m_lodStatistic = rawStats[group][RawSectionStatistics.LodLevel.LOD][instanceType].DrawableCost;
    //        this.m_slodStatistic = rawStats[group][RawSectionStatistics.LodLevel.SLOD1][instanceType].DrawableCost;

    //        group = RawSectionStatistics.StatisticGroups.Interiors;
    //        this.m_interiorStatistic = rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].DrawableCost;

    //        group = RawSectionStatistics.StatisticGroups.NonDrawableInstances;
    //        instanceType = RawSectionStatistics.InstanceType.Dynamic;
    //        this.m_dynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.Total][instanceType].DrawableCost;

    //        instanceType = RawSectionStatistics.InstanceType.NonDynamic;
    //        this.m_nonDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.Total][instanceType].DrawableCost;

    //        instanceType = RawSectionStatistics.InstanceType.Total;
    //        this.m_allPropsStatistic = rawStats[group][RawSectionStatistics.LodLevel.Total][instanceType].DrawableCost;
    //        this.m_uniqueStatistic = this.m_allPropsStatistic;

    //        this.m_totalStatistic = this.m_highStatistic + this.m_lodStatistic +
    //            this.m_slodStatistic + this.m_interiorStatistic + this.m_allPropsStatistic;
    //    }

    //    #endregion // Constructors
    //} // GeometryCostStatistic

    ///// <summary>
    ///// 
    ///// </summary>
    //public class TextureDictionaryCostStatistic : SizeStatisticBase
    //{
    //    #region Constants

    //    private static readonly String StatisticDescription = "The cost of the texture dictionaries in the container using the independent size.";

    //    #endregion // Constants

    //    #region Constructors

    //    /// <summary>
    //    /// Default constructor
    //    /// </summary>
    //    public TextureDictionaryCostStatistic(RawSectionStatistics rawStats)
    //        : base(StatisticNames.TXD_COST, StatisticDescription)
    //    {
    //        var group = RawSectionStatistics.StatisticGroups.Instances;
    //        var instanceType = RawSectionStatistics.InstanceType.Total;

    //        this.m_highStatistic = rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].TXDCost;
    //        this.m_lodStatistic = rawStats[group][RawSectionStatistics.LodLevel.LOD][instanceType].TXDCost;
    //        this.m_slodStatistic = rawStats[group][RawSectionStatistics.LodLevel.SLOD1][instanceType].TXDCost;

    //        group = RawSectionStatistics.StatisticGroups.Interiors;
    //        this.m_interiorStatistic = rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].TXDCost;

    //        group = RawSectionStatistics.StatisticGroups.NonDrawableInstances;
    //        instanceType = RawSectionStatistics.InstanceType.Dynamic;
    //        this.m_dynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.Total][instanceType].TXDCost;

    //        instanceType = RawSectionStatistics.InstanceType.NonDynamic;
    //        this.m_nonDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.Total][instanceType].TXDCost;

    //        instanceType = RawSectionStatistics.InstanceType.Total;
    //        this.m_allPropsStatistic = rawStats[group][RawSectionStatistics.LodLevel.Total][instanceType].TXDCost;
    //        this.m_uniqueStatistic = this.m_allPropsStatistic;

    //        this.m_totalStatistic = this.m_highStatistic + this.m_lodStatistic +
    //            this.m_slodStatistic + this.m_interiorStatistic + this.m_allPropsStatistic;
    //    }

    //    #endregion // Constructors
    //} // TextureDictionaryCostStatistic


    /// <summary>
    /// 
    /// </summary>
    public class DrawableCountStatistic : CountStatisticBase
    {
        #region Constants
        private static readonly String StatisticDescription = "The number of drawables in the map container.";
        #endregion // Constants

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public DrawableCountStatistic(RawSectionStatistics rawStats)
            : base("Drawable Count", StatisticDescription)
        {
            var group = RawSectionStatistics.StatisticGroups.Drawables;
            var instanceType = RawSectionStatistics.InstanceType.Dynamic;

            foreach (var item in rawStats)
                System.Diagnostics.Debug.Print(string.Format("{0}", item));


            this.m_highDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.Total][instanceType].Count;
            this.m_lodDynamicStatistic = null;
            this.m_slodDynamicStatistic = null;

            instanceType = RawSectionStatistics.InstanceType.NonDynamic;

            this.m_highNonDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.Total][instanceType].Count;
            this.m_lodNonDynamicStatistic = null;
            this.m_slodNonDynamicStatistic = null;

            instanceType = RawSectionStatistics.InstanceType.Total;

            this.m_totalStatistic = rawStats[group][RawSectionStatistics.LodLevel.Total][instanceType].Count;
        }
        #endregion // Constructors
    } // DrawableInstanceCountStatistic

    /// <summary>
    /// 
    /// </summary>
    public class DrawableInstanceCountStatistic : CountStatisticBase
    {
        #region Constants
        private static readonly String StatisticDescription = "The number of drawable instances in the map container.";
        #endregion // Constants

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public DrawableInstanceCountStatistic(RawSectionStatistics rawStats)
            : base("Drawable Instance Count", StatisticDescription)
        {
            var group = RawSectionStatistics.StatisticGroups.DrawableInstances;
            var instanceType = RawSectionStatistics.InstanceType.Dynamic;

            this.m_highDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].Count;
            this.m_lodDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.LOD][instanceType].Count;
            this.m_slodDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.SLOD1][instanceType].Count;

            instanceType = RawSectionStatistics.InstanceType.NonDynamic;

            this.m_highNonDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].Count;
            this.m_lodNonDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.LOD][instanceType].Count;
            this.m_slodNonDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.SLOD1][instanceType].Count;

            instanceType = RawSectionStatistics.InstanceType.Total;

            this.m_totalStatistic = rawStats[group][RawSectionStatistics.LodLevel.Total][instanceType].Count;
        }
        #endregion // Constructors
    } // DrawableInstanceCountStatistic

    /// <summary>
    /// 
    /// </summary>
    public class NonDrawableInstanceCountStatistic : CountStatisticBase
    {
        #region Constants
        private static readonly String StatisticDescription = "The number of non drawable instances in the map container.";
        #endregion // Constants

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public NonDrawableInstanceCountStatistic(RawSectionStatistics rawStats)
            : base("Non Drawable Instance Count", StatisticDescription)
        {
            var group = RawSectionStatistics.StatisticGroups.NonDrawableInstances;
            var instanceType = RawSectionStatistics.InstanceType.Dynamic;

            this.m_highDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].Count;
            this.m_lodDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.LOD][instanceType].Count;
            this.m_slodDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.SLOD1][instanceType].Count;

            instanceType = RawSectionStatistics.InstanceType.NonDynamic;

            this.m_highNonDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].Count;
            this.m_lodNonDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.LOD][instanceType].Count;
            this.m_slodNonDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.SLOD1][instanceType].Count;

            instanceType = RawSectionStatistics.InstanceType.Total;

            this.m_totalStatistic = rawStats[group][RawSectionStatistics.LodLevel.Total][instanceType].Count;
        }
        #endregion // Constructors
    } // NonDrawableInstanceCountStatistic

    /// <summary>
    /// 
    /// </summary>
    public class InteriorInstanceCountStatistic : CountStatisticBase
    {
        #region Constants
        private static readonly String StatisticDescription = "The number of interior instances in the map container.";
        #endregion // Constants

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public InteriorInstanceCountStatistic(RawSectionStatistics rawStats)
            : base("Interior Instance Count", StatisticDescription)
        {
            var group = RawSectionStatistics.StatisticGroups.Interiors;
            var instanceType = RawSectionStatistics.InstanceType.Dynamic;

            this.m_highDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].Count;
            this.m_lodDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.LOD][instanceType].Count;
            this.m_slodDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.SLOD1][instanceType].Count;

            instanceType = RawSectionStatistics.InstanceType.NonDynamic;

            this.m_highNonDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].Count;
            this.m_lodNonDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.LOD][instanceType].Count;
            this.m_slodNonDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.SLOD1][instanceType].Count;

            instanceType = RawSectionStatistics.InstanceType.Total;

            this.m_totalStatistic = rawStats[group][RawSectionStatistics.LodLevel.Total][instanceType].Count;
        }
        #endregion // Constructors
    } // InteriorInstanceCountStatistic

    /// <summary>
    /// 
    /// </summary>
    public class TxdCountStatistic : CountStatisticBase
    {
        #region Constants
        private static readonly String StatisticDescription = "The number of unique texture dictionaries that need loading for the map container.";
        #endregion // Constants

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public TxdCountStatistic(RawSectionStatistics rawStats)
            : base("TXD Count", StatisticDescription)
        {
            var group = RawSectionStatistics.StatisticGroups.Instances;
            var instanceType = RawSectionStatistics.InstanceType.Dynamic;

            this.m_highDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].TXDCount;
            this.m_lodDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.LOD][instanceType].TXDCount;
            this.m_slodDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.SLOD1][instanceType].TXDCount;

            instanceType = RawSectionStatistics.InstanceType.NonDynamic;

            this.m_highNonDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].TXDCount;
            this.m_lodNonDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.LOD][instanceType].TXDCount;
            this.m_slodNonDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.SLOD1][instanceType].TXDCount;

            instanceType = RawSectionStatistics.InstanceType.Total;

            this.m_totalStatistic = rawStats[group][RawSectionStatistics.LodLevel.Total][instanceType].TXDCount;
        }
        #endregion // Constructors
    } // TxdCountStatistic

    /// <summary>
    /// 
    /// </summary>
    public class ShaderCountStatistic : CountStatisticBase
    {
        #region Constants
        private static readonly String StatisticDescription = "The number of unique shaders that need loading for the map container.";
        #endregion // Constants

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public ShaderCountStatistic(RawSectionStatistics rawStats)
            : base("Shader Count", StatisticDescription)
        {
            var group = RawSectionStatistics.StatisticGroups.Instances;
            var instanceType = RawSectionStatistics.InstanceType.Dynamic;

            this.m_highDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].ShaderCount;
            this.m_lodDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.LOD][instanceType].ShaderCount;
            this.m_slodDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.SLOD1][instanceType].ShaderCount;

            instanceType = RawSectionStatistics.InstanceType.NonDynamic;

            this.m_highNonDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].ShaderCount;
            this.m_lodNonDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.LOD][instanceType].ShaderCount;
            this.m_slodNonDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.SLOD1][instanceType].ShaderCount;

            instanceType = RawSectionStatistics.InstanceType.Total;

            this.m_totalStatistic = rawStats[group][RawSectionStatistics.LodLevel.Total][instanceType].ShaderCount;
        }
        #endregion // Constructors
    } // ShaderCountStatistic

    /// <summary>
    /// 
    /// </summary>
    public class PolygonCountStatistic : CountStatisticBase
    {
        #region Constants
        private static readonly String StatisticDescription = "The number of polygons in the map container.";
        #endregion // Constants

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public PolygonCountStatistic(RawSectionStatistics rawStats)
            : base("Polygon Count", StatisticDescription)
        {
            var group = RawSectionStatistics.StatisticGroups.Instances;
            var instanceType = RawSectionStatistics.InstanceType.Dynamic;

            this.m_highDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].PolygonCount;
            this.m_lodDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.LOD][instanceType].PolygonCount;
            this.m_slodDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.SLOD1][instanceType].PolygonCount;

            instanceType = RawSectionStatistics.InstanceType.NonDynamic;

            this.m_highNonDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].PolygonCount;
            this.m_lodNonDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.LOD][instanceType].PolygonCount;
            this.m_slodNonDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.SLOD1][instanceType].PolygonCount;

            instanceType = RawSectionStatistics.InstanceType.Total;

            this.m_totalStatistic = rawStats[group][RawSectionStatistics.LodLevel.Total][instanceType].PolygonCount;
        }
        #endregion // Constructors
    } // PolygonCountStatistic

    /// <summary>
    /// 
    /// </summary>
    public class CollisionPolygonCountStatistic : CountStatisticBase
    {
        #region Constants
        private static readonly String StatisticDescription = "The number of collision polygons in the map container.";
        #endregion // Constants

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public CollisionPolygonCountStatistic(RawSectionStatistics rawStats)
            : base("Collision Polygon Count", StatisticDescription)
        {
            var group = RawSectionStatistics.StatisticGroups.Instances;
            var instanceType = RawSectionStatistics.InstanceType.Dynamic;

            this.m_highDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].CollisionCount;

            instanceType = RawSectionStatistics.InstanceType.NonDynamic;

            this.m_highNonDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].CollisionCount;

            instanceType = RawSectionStatistics.InstanceType.Total;

            this.m_totalStatistic = rawStats[group][RawSectionStatistics.LodLevel.Total][instanceType].CollisionCount;
        }
        #endregion // Constructors
    } // CollisionPolygonCountStatistic

    /// <summary>
    /// 
    /// </summary>
    public class MoverPolygonCountStatistic : CountStatisticBase
    {
        #region Constants
        private static readonly String StatisticDescription = "The number of mover collision polygons in the map container.";
        #endregion // Constants

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public MoverPolygonCountStatistic(RawSectionStatistics rawStats)
            : base("Mover Polygon Count", StatisticDescription)
        {
            var group = RawSectionStatistics.StatisticGroups.Instances;
            var instanceType = RawSectionStatistics.InstanceType.Dynamic;

            this.m_highDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].MoverCollisionCount;

            instanceType = RawSectionStatistics.InstanceType.NonDynamic;

            this.m_highNonDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].MoverCollisionCount;

            instanceType = RawSectionStatistics.InstanceType.Total;

            this.m_totalStatistic = rawStats[group][RawSectionStatistics.LodLevel.Total][instanceType].MoverCollisionCount;
        }
        #endregion // Constructors
    } // MoverPolygonCountStatistic

    /// <summary>
    /// 
    /// </summary>
    public class ShooterPolygonCountStatistic : CountStatisticBase
    {
        #region Constants
        private static readonly String StatisticDescription = "The number of shooter collision polygons in the map container.";
        #endregion // Constants

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public ShooterPolygonCountStatistic(RawSectionStatistics rawStats)
            : base("Shooter Polygon Count", StatisticDescription)
        {
            var group = RawSectionStatistics.StatisticGroups.Instances;
            var instanceType = RawSectionStatistics.InstanceType.Dynamic;

            this.m_highDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].WeaponCollisionCount;

            instanceType = RawSectionStatistics.InstanceType.NonDynamic;

            this.m_highNonDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].WeaponCollisionCount;

            instanceType = RawSectionStatistics.InstanceType.Total;

            this.m_totalStatistic = rawStats[group][RawSectionStatistics.LodLevel.Total][instanceType].WeaponCollisionCount;
        }
        #endregion // Constructors
    } // ShooterPolygonCountStatistic

    /// <summary>
    /// 
    /// </summary>
    public class CollisionPolygonRatioStatistic : RatioStatisticBase
    {
        #region Constants
        private static readonly String StatisticDescription = "The ratio between the number of collision polygons and the number of polygons.";
        #endregion // Constants

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public CollisionPolygonRatioStatistic(RawSectionStatistics rawStats)
            : base("Collision Polygon Ratio", StatisticDescription)
        {
            var group = RawSectionStatistics.StatisticGroups.Instances;
            var instanceType = RawSectionStatistics.InstanceType.Dynamic;

            int polygonCount = rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].PolygonCount;
            if (polygonCount != 0)
                this.m_highDynamicStatistic = (double)rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].CollisionCount / (double)polygonCount;
            else
                this.m_highDynamicStatistic = 0.0;

            instanceType = RawSectionStatistics.InstanceType.NonDynamic;

            polygonCount = rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].PolygonCount;
            if (polygonCount != 0)
                this.m_highNonDynamicStatistic = (double)rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].CollisionCount / (double)polygonCount;
            else
                this.m_highNonDynamicStatistic = 0.0;

            instanceType = RawSectionStatistics.InstanceType.Total;

            polygonCount = rawStats[group][RawSectionStatistics.LodLevel.Total][instanceType].PolygonCount;
            if (polygonCount != 0)
                this.m_totalStatistic = (double)rawStats[group][RawSectionStatistics.LodLevel.Total][instanceType].CollisionCount / (double)polygonCount;
            else
                this.m_totalStatistic = 0.0;
        }
        #endregion // Constructors
    } // CollisionPolygonRatioStatistic

    /// <summary>
    /// 
    /// </summary>
    public class MoverShooterPolygonRatioStatistic : RatioStatisticBase
    {
        #region Constants
        private static readonly String StatisticDescription = "The ratio between the number of mover collision polygons and the number of shooter collision polygons.";
        #endregion // Constants

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public MoverShooterPolygonRatioStatistic(RawSectionStatistics rawStats)
            : base("Mover/Shooter Polygon Ratio", StatisticDescription)
        {
            var group = RawSectionStatistics.StatisticGroups.Instances;
            var instanceType = RawSectionStatistics.InstanceType.Dynamic;

            int polygonCount = rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].WeaponCollisionCount;
            if (polygonCount != 0)
                this.m_highDynamicStatistic = (double)rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].MoverCollisionCount / (double)polygonCount;
            else
                this.m_highDynamicStatistic = 0.0;

            instanceType = RawSectionStatistics.InstanceType.NonDynamic;

            polygonCount = rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].WeaponCollisionCount;
            if (polygonCount != 0)
                this.m_highNonDynamicStatistic = (double)rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].MoverCollisionCount / (double)polygonCount;
            else
                this.m_highNonDynamicStatistic = 0.0;

            instanceType = RawSectionStatistics.InstanceType.Total;

            polygonCount = rawStats[group][RawSectionStatistics.LodLevel.Total][instanceType].WeaponCollisionCount;
            if (polygonCount != 0)
                this.m_totalStatistic = (double)rawStats[group][RawSectionStatistics.LodLevel.Total][instanceType].MoverCollisionCount / (double)polygonCount;
            else
                this.m_totalStatistic = 0.0;
        }
        #endregion // Constructors
    } // MoverShooterPolygonRatioStatistic

    /// <summary>
    /// 
    /// </summary>
    public class DrawableDensityStatistic : DensityStatisticBase
    {
        #region Constants
        private static readonly String StatisticDescription = "The number of drawables per meter squared.";
        #endregion // Constants

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public DrawableDensityStatistic(RawSectionStatistics rawStats, float area)
            : base("Drawable Density", StatisticDescription)
        {
            if (area == 0.0f)
                return;

            float inverseArea = 1.0f / area;
            var group = RawSectionStatistics.StatisticGroups.Drawables;
            var instanceType = RawSectionStatistics.InstanceType.Dynamic;

            this.m_highDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].Count * inverseArea;
            this.m_lodDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.LOD][instanceType].Count * inverseArea;
            this.m_slodDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.SLOD1][instanceType].Count * inverseArea;

            instanceType = RawSectionStatistics.InstanceType.NonDynamic;

            this.m_highNonDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].Count * inverseArea;
            this.m_lodNonDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.LOD][instanceType].Count * inverseArea;
            this.m_slodNonDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.SLOD1][instanceType].Count * inverseArea;

            instanceType = RawSectionStatistics.InstanceType.Total;

            this.m_totalStatistic = rawStats[group][RawSectionStatistics.LodLevel.Total][instanceType].Count * inverseArea;
        }
        #endregion // Constructors
    } // DrawableDensityStatistic

    /// <summary>
    /// 
    /// </summary>
    public class DrawableInstanceDensityStatistic : DensityStatisticBase
    {
        #region Constants
        private static readonly String StatisticDescription = "The number of drawable instances per meter squared.";
        #endregion // Constants

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public DrawableInstanceDensityStatistic(RawSectionStatistics rawStats, float area)
            : base("Drawable Instance Density", StatisticDescription)
        {
            if (area == 0.0f)
                return;

            float inverseArea = 1.0f / area;
            var group = RawSectionStatistics.StatisticGroups.DrawableInstances;
            var instanceType = RawSectionStatistics.InstanceType.Dynamic;

            this.m_highDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].Count * inverseArea;
            this.m_lodDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.LOD][instanceType].Count * inverseArea;
            this.m_slodDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.SLOD1][instanceType].Count * inverseArea;

            instanceType = RawSectionStatistics.InstanceType.NonDynamic;

            this.m_highNonDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].Count * inverseArea;
            this.m_lodNonDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.LOD][instanceType].Count * inverseArea;
            this.m_slodNonDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.SLOD1][instanceType].Count * inverseArea;

            instanceType = RawSectionStatistics.InstanceType.Total;

            this.m_totalStatistic = rawStats[group][RawSectionStatistics.LodLevel.Total][instanceType].Count * inverseArea;
        }
        #endregion // Constructors
    } // DrawableInstanceDensityStatistic

    /// <summary>
    /// 
    /// </summary>
    public class NonDrawableDensityStatistic : DensityStatisticBase
    {
        #region Constants
        private static readonly String StatisticDescription = "The number of non-drawable instances per meter squared.";
        #endregion // Constants

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public NonDrawableDensityStatistic(RawSectionStatistics rawStats, float area)
            : base("Non-Drawable Instance Density", StatisticDescription)
        {
            if (area == 0.0f)
                return;

            float inverseArea = 1.0f / area;
            var group = RawSectionStatistics.StatisticGroups.NonDrawableInstances;
            var instanceType = RawSectionStatistics.InstanceType.Dynamic;

            this.m_highDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].Count * inverseArea;
            this.m_lodDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.LOD][instanceType].Count * inverseArea;
            this.m_slodDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.SLOD1][instanceType].Count * inverseArea;

            instanceType = RawSectionStatistics.InstanceType.NonDynamic;

            this.m_highNonDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].Count * inverseArea;
            this.m_lodNonDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.LOD][instanceType].Count * inverseArea;
            this.m_slodNonDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.SLOD1][instanceType].Count * inverseArea;

            instanceType = RawSectionStatistics.InstanceType.Total;

            this.m_totalStatistic = rawStats[group][RawSectionStatistics.LodLevel.Total][instanceType].Count * inverseArea;
        }
        #endregion // Constructors
    } // NonDrawableDensityStatistic

    /// <summary>
    /// 
    /// </summary>
    public class TxdDensityStatistic : DensityStatisticBase
    {
        #region Constants
        private static readonly String StatisticDescription = "The number of unique texture dictionaries per meter squared.";
        #endregion // Constants

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public TxdDensityStatistic(RawSectionStatistics rawStats, float area)
            : base("TXD Density", StatisticDescription)
        {
            if (area == 0.0f)
                return;

            float inverseArea = 1.0f / area;
            var group = RawSectionStatistics.StatisticGroups.Instances;
            var instanceType = RawSectionStatistics.InstanceType.Dynamic;

            this.m_highDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].TXDCount * inverseArea;
            this.m_lodDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.LOD][instanceType].TXDCount * inverseArea;
            this.m_slodDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.SLOD1][instanceType].TXDCount * inverseArea;

            instanceType = RawSectionStatistics.InstanceType.NonDynamic;

            this.m_highNonDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].TXDCount * inverseArea;
            this.m_lodNonDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.LOD][instanceType].TXDCount * inverseArea;
            this.m_slodNonDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.SLOD1][instanceType].TXDCount * inverseArea;

            instanceType = RawSectionStatistics.InstanceType.Total;

            this.m_totalStatistic = rawStats[group][RawSectionStatistics.LodLevel.Total][instanceType].TXDCount * inverseArea;
        }
        #endregion // Constructors
    } // TxdDensityStatistic

    /// <summary>
    /// 
    /// </summary>
    public class ShaderDensityStatistic : DensityStatisticBase
    {
        #region Constants
        private static readonly String StatisticDescription = "The number of unique shaders per meter squared.";
        #endregion // Constants

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public ShaderDensityStatistic(RawSectionStatistics rawStats, float area)
            : base("Shader Density", StatisticDescription)
        {
            if (area == 0.0f)
                return;

            float inverseArea = 1.0f / area;
            var group = RawSectionStatistics.StatisticGroups.Instances;
            var instanceType = RawSectionStatistics.InstanceType.Dynamic;

            this.m_highDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].ShaderCount * inverseArea;
            this.m_lodDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.LOD][instanceType].ShaderCount * inverseArea;
            this.m_slodDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.SLOD1][instanceType].ShaderCount * inverseArea;

            instanceType = RawSectionStatistics.InstanceType.NonDynamic;

            this.m_highNonDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].ShaderCount * inverseArea;
            this.m_lodNonDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.LOD][instanceType].ShaderCount * inverseArea;
            this.m_slodNonDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.SLOD1][instanceType].ShaderCount * inverseArea;

            instanceType = RawSectionStatistics.InstanceType.Total;

            this.m_totalStatistic = rawStats[group][RawSectionStatistics.LodLevel.Total][instanceType].ShaderCount * inverseArea;
        }
        #endregion // Constructors
    } // ShaderDensityStatistic

    /// <summary>
    /// 
    /// </summary>
    public class PolygonDensityStatistic : DensityStatisticBase
    {
        #region Constants
        private static readonly String StatisticDescription = "The number of polygons per meter squared.";
        #endregion // Constants

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public PolygonDensityStatistic(RawSectionStatistics rawStats, float area)
            : base("Polygon Density", StatisticDescription)
        {
            if (area == 0.0f)
                return;

            float inverseArea = 1.0f / area;
            var group = RawSectionStatistics.StatisticGroups.Instances;
            var instanceType = RawSectionStatistics.InstanceType.Dynamic;

            this.m_highDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].PolygonCount * inverseArea;
            this.m_lodDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.LOD][instanceType].PolygonCount * inverseArea;
            this.m_slodDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.SLOD1][instanceType].PolygonCount * inverseArea;

            instanceType = RawSectionStatistics.InstanceType.NonDynamic;

            this.m_highNonDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].PolygonCount * inverseArea;
            this.m_lodNonDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.LOD][instanceType].PolygonCount * inverseArea;
            this.m_slodNonDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.SLOD1][instanceType].PolygonCount * inverseArea;

            instanceType = RawSectionStatistics.InstanceType.Total;

            this.m_totalStatistic = rawStats[group][RawSectionStatistics.LodLevel.Total][instanceType].PolygonCount * inverseArea;
        }
        #endregion // Constructors
    } // PolygonDensityStatistic

    /// <summary>
    /// 
    /// </summary>
    public class CollisionPolygonDensityStatistic : DensityStatisticBase
    {
        #region Constants
        private static readonly String StatisticDescription = "The number of collision polygons per meter squared.";
        #endregion // Constants

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public CollisionPolygonDensityStatistic(RawSectionStatistics rawStats, float area)
            : base("Collision Polygon Density", StatisticDescription)
        {
            if (area == 0.0f)
                return;

            float inverseArea = 1.0f / area;
            var group = RawSectionStatistics.StatisticGroups.Instances;
            var instanceType = RawSectionStatistics.InstanceType.Dynamic;

            this.m_highDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].CollisionCount * inverseArea;

            instanceType = RawSectionStatistics.InstanceType.NonDynamic;

            this.m_highNonDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].CollisionCount * inverseArea;

            instanceType = RawSectionStatistics.InstanceType.Total;

            this.m_totalStatistic = rawStats[group][RawSectionStatistics.LodLevel.Total][instanceType].CollisionCount * inverseArea;
        }
        #endregion // Constructors
    } // CollisionPolygonDensityStatistic

    /// <summary>
    /// 
    /// </summary>
    public class LodDistanceStatistic : DensityStatisticBase
    {
        #region Constants
        private static readonly String StatisticDescription = "The lod distance.";
        #endregion // Constants

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public LodDistanceStatistic(RawSectionStatistics rawStats)
            : base("Maximum Lod Distance", StatisticDescription)
        {
            var group = RawSectionStatistics.StatisticGroups.Instances;
            var instanceType = RawSectionStatistics.InstanceType.Dynamic;

            this.m_highDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].LodDistance;
            this.m_lodDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.LOD][instanceType].LodDistance;
            this.m_slodDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.SLOD1][instanceType].LodDistance;

            instanceType = RawSectionStatistics.InstanceType.NonDynamic;

            this.m_highNonDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.HD][instanceType].LodDistance;
            this.m_lodNonDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.LOD][instanceType].LodDistance;
            this.m_slodNonDynamicStatistic = rawStats[group][RawSectionStatistics.LodLevel.SLOD1][instanceType].LodDistance;

            instanceType = RawSectionStatistics.InstanceType.Total;

            this.m_totalStatistic = rawStats[group][RawSectionStatistics.LodLevel.Total][instanceType].LodDistance;
        }
        #endregion // Constructors
    } // LodDistanceStatistic

} // RSG.Model.Map.Statistics
