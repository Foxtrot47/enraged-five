﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using RSG.Base.Threading;
using RSG.Base.Tasks;
using RSG.Model.Common;
using RSG.Model.Report;
using RSG.SourceControl.Perforce;
using P4API;

namespace RSG.Model.Report2.Reports.Map
{

    /// <summary>
    /// Report for displaying information about duplicate art source textures.
    /// </summary>
    /// Textures are considered duplicates if they have the same basename but
    /// different source folders.
    /// 
    public class DuplicateSourceTextureReport : 
        CSVReport, 
        IDynamicReport,
        IReportFileProvider
    {
        #region Constants 
        private static readonly String NAME = "Duplicate Source Texture Report";
        private static readonly String DESC = "Report for displaying information about duplicate art source textures.";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (m_task == null)
                {
                    m_task = new ActionTask("Generating report", (context, progress) => GenerateReport((DynamicReportContext)context, progress));
                }
                return m_task;
            }
        }
        private ITask m_task;
        #endregion // Properties
        
        /// <summary>
        /// Structure for storing map source texture usage information.
        /// </summary>
        class MapSourceTextureUsage
        {
            public String Basename;
            public List<String> SourceFiles;
            public int UsageCount;

            #region Constructor(s)
            public MapSourceTextureUsage(String basename)
            {
                this.Basename = basename;
                this.SourceFiles = new List<String>();
                this.UsageCount = 0;
            }
            #endregion // Constructor(s)
        }

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="desc"></param>
        public DuplicateSourceTextureReport()
            : base(NAME, DESC)
        {
        }
        #endregion // Constructor(s)
        
        #region Task Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void GenerateReport(DynamicReportContext context, IProgress<TaskProgress> progress)
        {
            String mapSourceTextureDirectory = Path.Combine(context.Branch.Art, "textures");
            if (Directory.Exists(mapSourceTextureDirectory))
            {
                IDictionary<String, MapSourceTextureUsage> mapSourceUsage = 
                    new Dictionary<String, MapSourceTextureUsage>();

                using (P4 p4 = context.Branch.Project.SCMConnect())
                {
                    FileMapping[] fms = FileMapping.Create(p4, mapSourceTextureDirectory + "\\...");
                    IEnumerable<String> depotPaths = fms.Where(fm => fm.Mapped).Select(fm => fm.DepotFilename);
                    P4RecordSet records = p4.Run("files", depotPaths.ToArray());
                    float increment = 1.0f / records.Records.Length;
                    foreach (P4Record record in records.Records)
                    {
                        String mapSourceTexture = record["depotFile"];
                        String message = String.Format("Analysing source texture: {0}", mapSourceTexture);
                        progress.Report(new TaskProgress(increment, true, message));

                        String sourceFilename = mapSourceTexture.ToLower();
                        if (sourceFilename.EndsWith(".psd", StringComparison.OrdinalIgnoreCase))
                            continue;

                        String basename = Path.GetFileNameWithoutExtension(mapSourceTexture).ToLower();
                        if (String.IsNullOrWhiteSpace(basename))
                            continue;
                        
                        if (!mapSourceUsage.ContainsKey(basename))
                        {
                            MapSourceTextureUsage newUsage = new MapSourceTextureUsage(basename);
                            mapSourceUsage.Add(basename, newUsage);
                        }

                        MapSourceTextureUsage usage = mapSourceUsage[basename];
                        if (!usage.SourceFiles.Contains(sourceFilename))
                            usage.SourceFiles.Add(sourceFilename);
                        ++usage.UsageCount;
                    }

                    // Write out CSV data.
                    using (StreamWriter writer = new StreamWriter(Filename))
                    {
                        WriteCsvObjectHeader(writer);
                        foreach (MapSourceTextureUsage usage in mapSourceUsage.Values)
                        {
                            if (usage.UsageCount > 1)
                                WriteCsvObjectItem(writer, usage);
                        }
                    }
                }
            }
            else
            {
#warning DHM FIX ME: need to error?  Expected context to have a log object.
                // Error?
            }
        }
        #endregion // Task Methods


        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sw"></param>
        /// <param name="milo"></param>
        private void WriteCsvObjectHeader(StreamWriter sw)
        {
            String header = "TextureBasename,UsageCount,NumberOfFiles,Files";
            sw.WriteLine(header);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sw"></param>
        /// <param name="usage"></param>
        private void WriteCsvObjectItem(StreamWriter sw, MapSourceTextureUsage usage)
        {
            String line = String.Format("{0},{1},{2},{3}",
                usage.Basename, usage.UsageCount, usage.SourceFiles.Count, String.Join("|", usage.SourceFiles.ToArray()));
            sw.WriteLine(line);
        }
        #endregion // Private Methods
    }

} // RSG.Model.Report2.Reports.Map namespace
