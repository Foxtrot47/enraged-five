﻿//---------------------------------------------------------------------------------------------
// <copyright file="LineRecordedData.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.Model
{
    using System;

    /// <summary>
    /// Contains recording data for a number of dialogue lines. Where the lines can be
    /// identified by the audio filepath.
    /// </summary>
    public class LineRecordedData
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Channel"/> property.
        /// </summary>
        private int _channel;

        /// <summary>
        /// The private field used for the <see cref="Event"/> property.
        /// </summary>
        private int _event;

        /// <summary>
        /// The private field used for the <see cref="ClipName"/> property.
        /// </summary>
        private string _clipName;

        /// <summary>
        /// The private field used for the <see cref="StartTime"/> property.
        /// </summary>
        private string _startTime;

        /// <summary>
        /// The private field used for the <see cref="EndTime"/> property.
        /// </summary>
        private string _endTime;

        /// <summary>
        /// The private field used for the <see cref="Duration"/> property.
        /// </summary>
        private string _duration;

        /// <summary>
        /// The private field used for the <see cref="State"/> property.
        /// </summary>
        private string _state;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="LineRecordedData"/> class.
        /// </summary>
        public LineRecordedData()
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="LineRecordedData"/> class.
        /// </summary>
        /// <param name="dataLine">
        /// The string line that contains the data for the recording.
        /// </param>
        public LineRecordedData(string dataLine)
        {
            string[] parts = dataLine.Split(
                new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            if (parts.Length > 0)
            {
                //// Channel
                string part = parts[0].Trim();
                int.TryParse(part, out this._channel);
            }

            if (parts.Length > 1)
            {
                //// Event
                string part = parts[1].Trim();
                int.TryParse(part, out this._event);
            }

            if (parts.Length > 2)
            {
                //// Clip Name
                this._clipName = parts[2].Trim();
            }

            if (parts.Length > 3)
            {
                //// Start Time
                this._startTime = parts[3].Trim();
            }

            if (parts.Length > 4)
            {
                //// End Time
                this._endTime = parts[4].Trim();
            }

            if (parts.Length > 5)
            {
                //// Duration
                this._duration = parts[5].Trim();
            }

            if (parts.Length > 6)
            {
                //// State
                this._state = parts[6].Trim();
            }
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the channel this line has been recorded on.
        /// </summary>
        public int Channel
        {
            get { return this._channel; }
            set { this._channel = value; }
        }

        /// <summary>
        /// Gets or sets the event of the recording.
        /// </summary>
        public int Event
        {
            get { return this._event; }
            set { this._event = value; }
        }

        /// <summary>
        /// Gets or sets the clip name (the audio filepath) that the recording is of.
        /// </summary>
        public string ClipName
        {
            get { return this._clipName; }
            set { this._clipName = value; }
        }

        /// <summary>
        /// Gets or sets the start time for the line on the track.
        /// </summary>
        public string StartTime
        {
            get { return this._startTime; }
            set { this._startTime = value; }
        }

        /// <summary>
        /// Gets or sets the end time for the line on the track.
        /// </summary>
        public string EndTime
        {
            get { return this._endTime; }
            set { this._endTime = value; }
        }

        /// <summary>
        /// Gets or sets the duration of the line recording.
        /// </summary>
        public string Duration
        {
            get { return this._duration; }
            set { this._duration = value; }
        }

        /// <summary>
        /// Gets or sets the current state of the recording. (i.e. whether it is muted or not).
        /// </summary>
        public string State
        {
            get { return this._state; }
            set { this._state = value; }
        }
        #endregion Properties
    } // RSG.Text.Model.LineRecordedData {Class}
} // RSG.Text.Model {Namespace}
