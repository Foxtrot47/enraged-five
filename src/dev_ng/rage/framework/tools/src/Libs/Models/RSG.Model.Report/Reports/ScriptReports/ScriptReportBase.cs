﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.ConfigParser;
using System.Web.UI;
using System.Diagnostics;
using RSG.Base.Logging;
using System.IO;
using System.Text.RegularExpressions;

namespace RSG.Model.Report.Reports.ScriptReports
{
    /// <summary>
    /// Base class for all script based reports
    /// </summary>
    public abstract class ScriptReportBase : HTMLReport, IDynamicReport
    {
        #region Constants
        protected static readonly String[] SCRIPT_EXTENSIONS;
        private const String SINGLEPLAYER_DIRECTORY = "singleplayer";
        private const String MULTIPLAYER_DIRECTORY = "multiplayer";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Determines whether the map data should be loaded
        /// </summary>
        public bool LoadMapData
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Determines whether the car generators should be loaded
        /// 
        /// </summary>
        public bool LoadCarGenarators
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// The file that we wish to search for enum values in
        /// </summary>
        public abstract string EnumFile
        {
            get;
        }

        /// <summary>
        /// The regex to use when searching through the enum file
        /// </summary>
        public abstract Regex EnumValueRegex
        {
            get;
        }
        #endregion // Properties

        #region Internal Classes
        /// <summary>
        /// Helper class that contains information about about a single occurrence of an enum value
        /// </summary>
        protected class EnumValueInfo
        {
            public List<string> Files { get; protected set; }

            public EnumValueInfo()
            {
                Files = new List<string>();
            }
        } // EnumValueInfo
        #endregion // Internal Classes
        
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ScriptReportBase(string name, string desc)
            : base(name, desc)
        {
        }

        /// <summary>
        /// Static constructor
        /// </summary>
        static ScriptReportBase()
        {
            SCRIPT_EXTENSIONS = new String[] { ".sc", ".sch" };
        }
        #endregion // Constructor(s)
        
        #region Controller Methods
        /// <summary>
        /// Generate the report dynamically.
        /// </summary>
        public virtual void Generate(ConfigGameView gv)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            // Get a list of all the model enum names
            IEnumerable<string> modelEnumNames = ExtractModelEnumNames(gv.ScriptDir);

            // Search through all the script files gathering information about each of them
            SortedDictionary<string, EnumValueInfo> infoMap = GatherAllModelEnumInfo(gv.ScriptDir, modelEnumNames);

            // Generate the report
            Stream = WriteReport(infoMap);

            sw.Stop();
            Log.Log__Message("{0} report took {1}ms to generate.", this.Name, sw.ElapsedMilliseconds);
        }
        #endregion // Controller Methods

        #region Protected Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="gv"></param>
        /// <returns></returns>
        protected IEnumerable<string> ExtractModelEnumNames(string scriptDir)
        {
            // Generate the path to the model enums script file
            string filePath = GetFullScriptFilePath(scriptDir);

            if (File.Exists(filePath))
            {
                // Read the file line by line looking for enum names
                using (StreamReader fileStream = new StreamReader(filePath))
                {
                    string line;

                    while ((line = fileStream.ReadLine()) != null)
                    {
                        // Try to match the regex
                        Match match = EnumValueRegex.Match(line);

                        if (match.Success)
                        {
                            Group nameGroup = match.Groups["name"];
                            yield return nameGroup.Value;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scriptDir"></param>
        /// <returns></returns>
        protected string GetFullScriptFilePath(string scriptDir)
        {
            return Path.GetFullPath(EnumFile.Replace("script:", scriptDir));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scriptDir"></param>
        /// <returns></returns>
        protected SortedDictionary<string, EnumValueInfo> GatherAllModelEnumInfo(string scriptDir, IEnumerable<string> enumNames)
        {
            // Do a prepass to populate the info map dictionary and a regex dictionary
            SortedDictionary<string, EnumValueInfo> infoMap = new SortedDictionary<string, EnumValueInfo>();
            Dictionary<string, Regex> regexMap = new Dictionary<string, Regex>();
            foreach (string name in enumNames)
            {
                // Add an entry for this name in the map
                infoMap.Add(name, new EnumValueInfo());
                regexMap.Add(name, new Regex(name));
            }

            // Iterate over all the script files 
            string filePath = GetFullScriptFilePath(scriptDir);

            IEnumerable<string> scriptFiles = Directory.EnumerateFiles(scriptDir, "*.*", SearchOption.AllDirectories).Where(file => SCRIPT_EXTENSIONS.Contains(Path.GetExtension(file)) && file != filePath);
            foreach (string scriptFile in scriptFiles)
            {
                GatherModelEnumInfo(scriptFile, ref infoMap, regexMap);
            }

            return infoMap;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="infoMap"></param>
        private void GatherModelEnumInfo(string file, ref SortedDictionary<string, EnumValueInfo> infoMap, Dictionary<string, Regex> regexMap)
        {
            // Open up the file
            using (StreamReader fileStream = new StreamReader(file))
            {
                string content = fileStream.ReadToEnd();

                foreach (KeyValuePair<string, EnumValueInfo> pair in infoMap)
                {
                    Regex nameRegex = regexMap[pair.Key];

                    Match match = nameRegex.Match(content);
                    if (match.Success)
                    {
                        pair.Value.Files.Add(file);
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        protected Stream WriteReport(SortedDictionary<string, EnumValueInfo> info)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter reportFile = new StreamWriter(stream);

            HtmlTextWriter writer = new HtmlTextWriter(reportFile);
            writer.RenderBeginTag(HtmlTextWriterTag.Html);
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Head);
                writer.RenderEndTag();

                writer.RenderBeginTag(HtmlTextWriterTag.Body);
                {
                    WriteHeader(writer, "Script Enums Report");

                    WriteSubHeader(writer, "Summary");
                    OutputSummary(writer, info);

                    WriteSubHeader(writer, "Unused Model Enums");
                    OutputModelEnums(writer, info.Where(pair => pair.Value.Files.Count == 0), null);

                    WriteSubHeader(writer, "Singleplayer Model Enums");
                    OutputModelEnums(writer, info.Where(pair => pair.Value.Files.FirstOrDefault(file => file.Contains(SINGLEPLAYER_DIRECTORY)) != null), SINGLEPLAYER_DIRECTORY);

                    WriteSubHeader(writer, "Multiplayer Model Enums");
                    OutputModelEnums(writer, info.Where(pair => pair.Value.Files.FirstOrDefault(file => file.Contains(MULTIPLAYER_DIRECTORY)) != null), MULTIPLAYER_DIRECTORY);
                }
                writer.RenderEndTag();
            }
            writer.RenderEndTag();
            writer.Flush();

            stream.Position = 0;
            return stream;
        }

        /// <summary>
        /// Outputs a summary of the information we found
        /// </summary>
        /// <param name="writer"></param>
        protected void OutputSummary(HtmlTextWriter writer, SortedDictionary<string, EnumValueInfo> infoMap)
        {
            // Gather some stats about the extracted information
            int totalCount = infoMap.Count;
            int usedCount = 0;
            int singleplayerCount = 0;
            int multiplayerCount = 0;

            foreach (KeyValuePair<string, EnumValueInfo> pair in infoMap)
            {
                EnumValueInfo info = pair.Value;

                if (info.Files.Count > 0)
                {
                    ++usedCount;

                    string singleplayerInstance = info.Files.FirstOrDefault(file => file.Contains(SINGLEPLAYER_DIRECTORY));
                    if (singleplayerInstance != null)
                    {
                        ++singleplayerCount;
                    }

                    string multplayerInstance = info.Files.FirstOrDefault(file => file.Contains(MULTIPLAYER_DIRECTORY));
                    if (multplayerInstance != null)
                    {
                        ++multiplayerCount;
                    }
                }
            }

            // Output the summary
            WriteParagraph(writer, String.Format("A total of {0} enum names exists in the {1} file.", totalCount, EnumFile));

            writer.RenderBeginTag(HtmlTextWriterTag.Ul);
            writer.RenderBeginTag(HtmlTextWriterTag.Li);
            writer.Write(String.Format("{0} of them are being used.", usedCount));
            writer.RenderEndTag();
            writer.RenderBeginTag(HtmlTextWriterTag.Li);
            writer.Write(String.Format("{0} of them are being used in singleplayer files.", singleplayerCount));
            writer.RenderEndTag();
            writer.RenderBeginTag(HtmlTextWriterTag.Li);
            writer.Write(String.Format("{0} of them are being used in multiplayer files.", multiplayerCount));
            writer.RenderEndTag();
            writer.RenderBeginTag(HtmlTextWriterTag.Li);
            writer.Write(String.Format("{0} aren't being used at all.", (totalCount - usedCount)));
            writer.RenderEndTag();
            writer.RenderEndTag();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="items"></param>
        protected void OutputModelEnums(HtmlTextWriter writer, IEnumerable<KeyValuePair<string, EnumValueInfo>> items, string exampleMatch)
        {
            if (items.Count() == 0)
            {
                WriteParagraph(writer, "non found");
            }
            else
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Table);

                // Header Row
                writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.RenderBeginTag(HtmlTextWriterTag.B);
                writer.Write("Script Enum Name");
                writer.RenderEndTag();
                writer.RenderEndTag();

                if (exampleMatch != null)
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.Td);
                    writer.RenderBeginTag(HtmlTextWriterTag.B);
                    writer.Write("Example of Where Used");
                    writer.RenderEndTag();
                    writer.RenderEndTag();
                }
                writer.RenderEndTag();

                // Output a line for each of them
                foreach (KeyValuePair<string, EnumValueInfo> pair in items)
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                    writer.RenderBeginTag(HtmlTextWriterTag.Td);
                    writer.Write(pair.Key);
                    writer.RenderEndTag();

                    if (exampleMatch != null)
                    {
                        writer.RenderBeginTag(HtmlTextWriterTag.Td);
                        writer.Write(pair.Value.Files.FirstOrDefault(file => file.Contains(exampleMatch)));
                        writer.RenderEndTag();
                    }

                    writer.RenderEndTag();
                }

                writer.RenderEndTag();
                writer.WriteBreak();
            }
        }
        #endregion // Protected Methods
    } // ScriptReportBase
}
