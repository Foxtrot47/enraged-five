﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Model.Common;
using RSG.Model.Common.RadioStation;

namespace RSG.Model.Asset.RadioStation
{
    /// <summary>
    /// 
    /// </summary>
    public class RadioStationFactory
    {
        #region Constants
        /// <summary>
        /// Location of the xml file that contains the list of radio stations.
        /// </summary>
        private const String c_radioStationsMetaFile = @"$(audio)\assets\Objects\Core\Audio\GameObjects\Radio\Stations.xml";

        /// <summary>
        /// Location of the english radio station names.
        /// </summary>
        private const String c_radioStationNamesFile = @"$(text)\American\americanZit.txt";
        #endregion // Constants

        #region Member Data
        /// <summary>
        /// Local config object.
        /// </summary>
        private IConfig Config { get; set; }
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public RadioStationFactory()
            : this(ConfigFactory.CreateConfig())
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="config"></param>
        public RadioStationFactory(IConfig config)
        {
            Config = config;
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// Creates a new mission collection from the specified source.
        /// </summary>
        public IRadioStationCollection CreateCollection(DataSource source, string buildIdentifier = null)
        {
            IRadioStationCollection collection = null;

            if (source == DataSource.SceneXml)
            {
                Log.Log__Profile("RadioStationFactory:CreateCollection:SceneXml");
                collection = CreateCollectionFromExportData();
                Log.Log__ProfileEnd();
            }
            else if (source == DataSource.Database)
            {
                Log.Log__Profile("RadioStationFactory:CreateCollection:Database.");
                collection = CreateCollectionFromDatabase(buildIdentifier);
                Log.Log__ProfileEnd();
            }
            else
            {
                Debug.Assert(false, String.Format("Unknown data source '{0}' specified while creating a radio station collection.", source.ToString()));
                throw new ArgumentException(String.Format("Unknown data source '{0}' specified while creating a radio station collection.", source.ToString()));
            }

            return collection;
        }
        #endregion // Public Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private IRadioStationCollection CreateCollectionFromExportData()
        {
            String radioStationMetaFile = Config.CoreProject.DefaultBranch.Environment.Subst(c_radioStationsMetaFile);
            String radioStationNamesFile = Config.CoreProject.DefaultBranch.Environment.Subst(c_radioStationNamesFile);
            Debug.Assert(File.Exists(radioStationMetaFile), String.Format("{0} doesn't exist.", radioStationMetaFile));
            Debug.Assert(File.Exists(radioStationNamesFile), String.Format("{0} doesn't exist.", radioStationNamesFile));

            // Create the collection
            RadioStationCollection radioStations = new RadioStationCollection();
            if (File.Exists(radioStationMetaFile))
            {
                XDocument xmlDoc = XDocument.Load(radioStationMetaFile);
                Dictionary<String, String> friendlyNameLookup = ParseGameTextFile(radioStationNamesFile);

                foreach (XElement itemElem in xmlDoc.XPathSelectElements("//RadioStationSettings/Name"))
                {
                    try
                    {
                        String name = itemElem.Value;

                        // Check to see whether we can find a friendly name.
                        String friendlyName = name;
                        String key = name.ToLower();
                        if (friendlyNameLookup.ContainsKey(key))
                        {
                            friendlyName = friendlyNameLookup[key];
                        }

                        // Create a new radio station and add it to the collection.
                        radioStations.Add(new RSG.Model.Common.RadioStation.RadioStation(name, friendlyName));
                    }
                    catch (System.Exception ex)
                    {
                        Log.Log__Exception(ex, "Error occurred while parsing the vehicles.meta file.");
                    }
                }
            }

            return radioStations;
        }

        /// <summary>
        /// Parse a game text file to extract all the key value pairs.
        /// </summary>
        private Dictionary<String, String> ParseGameTextFile(String filename)
        {
            // Parse file.
            Dictionary<String, String> lookup = new Dictionary<String, String>();

            if (File.Exists(filename))
            {
                using (StreamReader reader = new StreamReader(filename))
                {
                    String key = null;

                    String line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        if (line.StartsWith("["))
                        {
                            key = line.Trim().Trim(new char[] { '[', ']' }).ToLower();
                        }
                        else if (key != null)
                        {
                            lookup[key] = line;
                            key = null;
                        }
                    }
                }
            }

            return lookup;
        }

        /// <summary>
        /// Creates a new radio station collection from the database.
        /// </summary>
        private IRadioStationCollection CreateCollectionFromDatabase(String buildIdentifier)
        {
            throw new NotImplementedException();
            //IRadioStationCollection radioStations = null;
            //return radioStations;
        }
        #endregion // Private Methods
    } // RadioStationFactory
}
