﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using RSG.Base.Attributes;
using RSG.Base.ConfigParser;
using RSG.ManagedRage;
using RSG.Model.Asset;
using RSG.Model.Common;
using RSG.Model.Common.Util;
using RSG.SceneXml;
using RSG.SceneXml.Material;
using RSG.Platform;
using System.Text.RegularExpressions;
using RSG.Base.Extensions;
using System.Xml.Linq;
using RSG.Model.Asset.Util;

namespace RSG.Model.Vehicle
{
    /// <summary>
    /// 
    /// </summary>
    internal struct VehicleMetadataInfo
    {
        public String GameName { get; set; }
        public String ModelName { get; set; }
        public String VehicleType { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class LocalVehicle : Vehicle
    {
        #region Enumerations
        /// <summary>
        /// Object def type's to use while parsing the scene xml file
        /// </summary>
        private enum ObjectDefType
        {
            [Regex(@"\Achassis\z")]
            Chassis,
            [Regex(@"\Adoor_(p|d)side_.+\z")]
            Door,
            [Regex(@"\Aseat_(p|d)side_.+\z")]
            Seat,
            [Regex(@"\Awheel_.+\z")]
            Wheel,
            [Regex(@"\A.+light_.+\z")]
            Light,
            [Regex(@"\Aextra_.+\z")]
            ExtraPoint,
            [Regex(@"\Ahandlebars\z")]
            Handlebars,
            [Regex(@"\Aattach_male\z")]
            TrailerPart,
            [Regex(@"\Arudder2?\z")]
            Rudder,
            [Regex(@"\Arotor_(rear|main)(_slow|_fast)\z")]
            Rotor,
            [Regex(@"\A(((static_|moving)prop)|(prop(_1_slow|_1_fast|_2_slow|_2_fast)))\z")]
            Propeller,
            [Regex(@"\Aelevator_(l|r)\z")]
            Elevator,
            Collision,
            Unknown,
        }
        #endregion // Enumerations
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public LocalVehicle(String modelName, String friendlyName, String gameName, VehicleCategory category, IVehicleCollection parentCollection)
            : base(modelName, friendlyName, gameName, category, parentCollection)
        {
        }
        #endregion // Constructor(s)

        #region StreamableAssetContainerBase Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="statsToLoad"></param>
        protected override void LoadStatsInternal(IEnumerable<StreamableStat> statsToLoad)
        {
            Debug.Assert(ParentCollection is LocalVehicleCollection, "Parent collection isn't an LocalVehicleCollection.");
            LocalVehicleCollection localCollection = (LocalVehicleCollection)ParentCollection;

            if (!AreStatsLoaded(statsToLoad.Where(item => item != StreamableVehicleStat.PlatformStats)))
            {
                LoadStatsFromExportData(localCollection.ExportDataPath);
            }

            if (statsToLoad.Contains(StreamableVehicleStat.PlatformStats) && !IsStatLoaded(StreamableVehicleStat.PlatformStats))
            {
                // Get the path to the independent rpf file
                List<ContentNode> contentNodes = localCollection.GameView.Content.Root.FindAll("vehiclesdirectory");

                string independentPath = null;
                if (contentNodes.Count > 0)
                {
                    independentPath = LocalLoadUtil.GeneratePathForNode(contentNodes[0]);
                }

                // Open up the pack files
                IDictionary<RSG.Platform.Platform, Packfile> packFiles = LocalLoadUtil.OpenPackFiles(independentPath, localCollection.GameView, localCollection.Config);
                LoadPlatformStatsFromExportData(packFiles);

                // Close all the pack files we opened
                LocalLoadUtil.ClosePackFiles(packFiles);
            }
        }
        #endregion // StreamableAssetContainerBase Overrides

        #region Static Methods
        /// <summary>
        /// 
        /// </summary>
        internal static VehicleMetadataInfo ExtractDataFromMetadataElement(XElement itemElem)
        {
            VehicleMetadataInfo info = new VehicleMetadataInfo();

            XElement nameElem = itemElem.Element("gameName");
            Debug.Assert(nameElem != null, "gameName element doesn't exist.");
            if (nameElem != null)
            {
                info.GameName = nameElem.Value;
            }

            XElement modelElem = itemElem.Element("modelName");
            Debug.Assert(modelElem != null, "modelName element doesn't exist.");
            if (modelElem != null)
            {
                info.ModelName = modelElem.Value;
            }

            XElement typeElem = itemElem.Element("type");
            Debug.Assert(typeElem != null, "type element doesn't exist.");
            if (typeElem != null)
            {
                info.VehicleType = typeElem.Value;
            }

            return info;
        }
        #endregion // Static Methods

        #region Internal Methods
        /// <summary>
        /// Loads data from the appropriate source
        /// </summary>
        /// <param name="source"></param>
        internal void LoadStatsFromExportData(string exportDataPath)
        {
            string sceneXmlFileName = Path.Combine(exportDataPath, Name + ".xml");
            Parse(sceneXmlFileName);
        }

        /// <summary>
        /// 
        /// </summary>
        internal void LoadPlatformStatsFromExportData(IDictionary<RSG.Platform.Platform, Packfile> packFiles)
        {
            // Next load in the platform stats
            PlatformStats = new Dictionary<RSG.Platform.Platform, IVehiclePlatformStat>();

            foreach (KeyValuePair<RSG.Platform.Platform, Packfile> pair in packFiles)
            {
                uint physicalSize = 0;
                uint virtualSize = 0;
                uint hiPhysicalSize = 0;
                uint hiVirtualSize = 0;

                // Process all of the extensions we were provided
                foreach (string extension in GetExtensionsForPlatform(pair.Key))
                {
                    PackEntry entry = pair.Value.FindEntry(Name + "." + extension);
                    if (entry != null)
                    {
                        physicalSize += (uint)entry.PhysicalSize;
                        virtualSize += (uint)entry.VirtualSize;
                    }

                    entry = pair.Value.FindEntry(Name + "_hi." + extension);
                    if (entry != null)
                    {
                        hiPhysicalSize += (uint)entry.PhysicalSize;
                        hiVirtualSize += (uint)entry.VirtualSize;
                    }
                }

                if (physicalSize != 0 && virtualSize != 0)
                {
                    IVehiclePlatformStat platformStat = new VehiclePlatformStat(pair.Key, physicalSize, virtualSize, hiPhysicalSize, hiVirtualSize);
                    PlatformStats.Add(pair.Key, platformStat);
                }
            }
        }
        #endregion // Internal Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="platform"></param>
        /// <returns></returns>
        private string[] GetExtensionsForPlatform(RSG.Platform.Platform platform)
        {
            List<string> extensions = new List<string>();

            foreach (string ext in new string[] { "ft", "td", "cd" })
            {
                extensions.Add(platform.PlatformRagebuilderCharacter() + ext);
            }

            return extensions.ToArray();
        }

        /// <summary>
        /// Parse Vehicle SceneXml.
        /// </summary>
        private bool Parse(String filename)
        {
            // Make sure the file we want to parse exists
            if (!File.Exists(filename))
            {
                return false;
            }

            ParseInternal(filename);
            return true;
        }

        /// <summary>
        /// Parse Vehicle SceneXml (internal)
        /// </summary>
        /// <param name="sceneXmlFilename"></param>
        private void ParseInternal(String sceneXmlFilename)
        {
            // Initialise the stats
            Shaders = new IShader[0];
            PolyCount = 0;
            CollisionCount = 0;
            BoneCount = 0;
            HasLod1 = false;
            HasLod2 = false;
            HasLod3 = false;
            WheelCount = 0;
            SeatCount = 0;
            DoorCount = 0;
            LightCount = 0;
            ExtraCount = 0;
            RudderCount = 0;
            RotorCount = 0;
            PropellerCount = 0;
            ElevatorCount = 0;
            HandlebarCount = 0;

            // And now read them in.
            Scene scene = new Scene(sceneXmlFilename);
            TargetObjectDef vehicleMesh = ParseObjects(scene);
            ParseMaterials(scene, vehicleMesh);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scene"></param>
        private void ParseMaterials(Scene scene, TargetObjectDef vehicleMesh)
        {
            List<IShader> shaders = new List<IShader>();
            if (vehicleMesh != null)
            {
                MaterialDef material = null;
                if (scene.MaterialLookup.TryGetValue(vehicleMesh.Material, out material))
                {
                    GetTexturesFromMaterial(material, ref shaders);
                }
            }

            shaders.Sort();
            this.Shaders = shaders.ToArray();

            // Add the textures as children
            //lock (this.AssetChildren)
            {
                this.AssetChildren.Clear();
                this.AssetChildren.AddRange(Textures);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="material"></param>
        /// <param name="textures"></param>
        /// <param name="shaders"></param>
        private void GetTexturesFromMaterial(MaterialDef material, ref List<IShader> shaders)
        {
            if (material.TextureCount > 0)
            {
                Shader shader = null;
                if (!string.IsNullOrEmpty(material.Preset))
                {
                    shader = new Shader(material.Preset);

                    foreach (TextureDef texture in material.Textures)
                    {
                        Texture newTexture = new Texture(texture.FilePath);
                        shader.Textures.Add(newTexture);
                    }

                    // Add the shader if it doesn't already exist
                    if (!shaders.Contains(shader))
                    {
                        shaders.Add(shader);
                    }
                }
            }

            if (material.HasSubMaterials)
            {
                foreach (MaterialDef submat in material.SubMaterials)
                {
                    GetTexturesFromMaterial(submat, ref shaders);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scene"></param>
        private TargetObjectDef ParseObjects(Scene scene)
        {
            // Go through top level geometry and find
            // lod objects if they exist and the Skeleton
            TargetObjectDef skeleton = null;
            TargetObjectDef vehicleMesh = null;
            TargetObjectDef lod1 = null;
            TargetObjectDef lod2 = null;
            TargetObjectDef lod3 = null;
            string name = string.Empty;
            foreach (var obj in scene.Objects)
            {
                if (obj.IsObject())
                {
                    if (skeleton == null && obj.SkeletonRoot != null)
                    {
                        skeleton = obj.SkeletonRoot;
                        name = skeleton.Name.Replace("_skel", "");
                        this.PolyCount = obj.PolyCount;
                        vehicleMesh = obj;
                    }
                }
            }
            if (skeleton != null)
            {
                foreach (var obj in scene.Objects)
                {
                    if (!obj.DontExport())
                    {
                        if (obj.Name.StartsWith(name + "_l1"))
                        {
                            if (obj.IsObject())
                            {
                                lod1 = obj;
                            }
                            else
                            {
                                foreach (var child in obj.Children)
                                {
                                    if (child.IsObject())
                                    {
                                        lod1 = child;
                                        break;
                                    }
                                }
                            }
                        }
                        else if (obj.Name.StartsWith(name + "_l2"))
                        {
                            if (obj.IsObject())
                            {
                                lod2 = obj;
                            }
                            else
                            {
                                foreach (var child in obj.Children)
                                {
                                    if (child.IsObject())
                                    {
                                        lod2 = child;
                                        break;
                                    }
                                }
                            }
                        }
                        else if (obj.Name.StartsWith(name + "_l3"))
                        {
                            if (obj.IsObject())
                            {
                                lod3 = obj;
                            }
                            else
                            {
                                foreach (var child in obj.Children)
                                {
                                    if (child.IsObject())
                                    {
                                        lod3 = child;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            this.HasLod1 = lod1 != null;
            this.HasLod2 = lod2 != null;
            this.HasLod3 = lod3 != null;
            ParseSkeleton(skeleton);
            return vehicleMesh;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="skeleton"></param>
        private void ParseSkeleton(TargetObjectDef root)
        {
            if (root == null)
                return;
            if (root.IsDummy())
                this.BoneCount++;

            foreach (var child in root.Children)
            {
                switch (GetDummyType(child))
                {
                    case ObjectDefType.Collision:
                        this.CollisionCount += child.PolyCount;
                        break;
                    case ObjectDefType.Door:
                        this.DoorCount++;
                        break;
                    case ObjectDefType.Light:
                        this.LightCount++;
                        break;
                    case ObjectDefType.ExtraPoint:
                        this.ExtraCount++;
                        break;
                    case ObjectDefType.Seat:
                        this.SeatCount++;
                        break;
                    case ObjectDefType.Wheel:
                        this.WheelCount++;
                        break;
                    case ObjectDefType.Rudder:
                        this.RudderCount++;
                        break;
                    case ObjectDefType.Rotor:
                        this.RotorCount++;
                        break;
                    case ObjectDefType.Propeller:
                        this.PropellerCount++;
                        break;
                    case ObjectDefType.Elevator:
                        this.ElevatorCount++;
                        break;
                    case ObjectDefType.Handlebars:
                        this.HandlebarCount++;
                        break;

                    case ObjectDefType.Chassis:
                    case ObjectDefType.Unknown:
                    default:
                        break;
                }
                ParseSkeleton(child);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private ObjectDefType GetDummyType(TargetObjectDef obj)
        {
            if (obj.IsDummy())
            {
                foreach (ObjectDefType type in Enum.GetValues(typeof(ObjectDefType)))
                {
                    Regex reg = type.GetRegexValue();
                    if (reg != null && reg.IsMatch(obj.Name))
                    {
                        return type;
                    }
                }
            }
            else if (obj.IsCollision())
            {
                return ObjectDefType.Collision;
            }
            return ObjectDefType.Unknown;
        }
        #endregion // Private Methods
    } // LocalVehicle
}
