﻿namespace RSG.Model.Character.Game
{
    /// <summary>
    /// 
    /// </summary>
    public class GameCharacter
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Name"/> property.
        /// </summary>
        private string _name;

        /// <summary>
        /// The private field used for the <see cref="PropsName"/> property.
        /// </summary>
        private string _propsName;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="GameCharacter"/> class.
        /// </summary>
        public GameCharacter()
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the name of this character.
        /// </summary>
        public string Name
        {
            get { return this._name; }
            set { this._name = value; }
        }

        /// <summary>
        /// Gets or sets the name used for the props for this character.
        /// </summary>
        public string PropsName
        {
            get { return this._propsName; }
            set { this._propsName = value; }
        }
        #endregion Properties
    }
}
