﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common.Map
{
    /// <summary>
    /// The different types of map sections that are available
    /// </summary>
    public enum SectionType
    {
        /// <summary>
        /// Type is unknown; happens when you can't tell the difference between a prop container and a interior container.
        /// </summary>
        Unknown,

        /// <summary>
        /// Section represents a map section that has both imap and ityp data in it.
        /// </summary>
        Container,

        /// <summary>
        /// Section represents a prop group - a max file that contains xrefs for a map container or a map container that contains rsrefs
        /// </summary>
        ContainerProp,

        /// <summary>
        /// Section represents multiple props that can be referenced inside a map section
        /// </summary>
        Prop,

        /// <summary>
        /// Section represents a single interior that can be referenced inside a map container
        /// </summary>
        Interior,

        /// <summary>
        /// Section represents a map section with section lods in it.
        /// </summary>
        ContainerLod,

        /// <summary>
        /// Section represents a map section with section occuludes in it.
        /// </summary>
        ContainerOcclusion
    } // SectionType
}
