﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.Base.Math;
using RSG.SceneXml;
using RSG.Model.Common.Map;
using System.Xml.Linq;

namespace RSG.Model.Map
{
    /// <summary>
    /// 
    /// </summary>
    public class SpawnPoint : AssetBase, ISpawnPoint
    {
        #region Constants
        private const string c_positionElement = "offsetPosition";
        private const string c_spawnTypeElement = "spawnType";

        private const string c_xAttribute = "x";
        private const string c_yAttribute = "y";
        private const string c_zAttribute = "z";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Position in world-space.
        /// </summary>
        public Vector3f Position
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public string SourceFile
        {
            get;
            private set;
        }

        /// <summary>
        /// Type of spawn point this is.
        /// </summary>
        public string SpawnType
        {
            get;
            private set;
        }
        #endregion // Properties
        
        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public SpawnPoint(ObjectDef sceneObject)
            : base(sceneObject.Name)
        {
            Position = sceneObject.NodeTransform.Translation;
            SpawnType = sceneObject.GetParameter(ParamNames.TWODFX_SPAWNPOINT_SPAWNTYPE, ParamDefaults.TWODFX_SPAWNPOINT_SPAWNTYPE);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlElem"></param>
        /// <param name="sourceFile"></param>
        public SpawnPoint(XElement xmlElem, string sourceFile)
        {
            SourceFile = sourceFile;

            XElement posElem = xmlElem.Element(c_positionElement);
            if (posElem != null)
            {
                XAttribute xAtt = posElem.Attribute(c_xAttribute);
                XAttribute yAtt = posElem.Attribute(c_yAttribute);
                XAttribute zAtt = posElem.Attribute(c_zAttribute);

                if (xAtt != null && yAtt != null && zAtt != null)
                {
                    Position = new Vector3f((float)Double.Parse(xAtt.Value), (float)Double.Parse(yAtt.Value), (float)Double.Parse(zAtt.Value));
                }
            }

            XElement typeElem = xmlElem.Element(c_spawnTypeElement);
            if (typeElem != null)
            {
                SpawnType = typeElem.Value;
            }
        }
        #endregion // Constructor
    } // SpawnPoint
}
