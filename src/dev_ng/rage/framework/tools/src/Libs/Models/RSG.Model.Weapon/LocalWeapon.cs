﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using RSG.Base.ConfigParser;
using RSG.ManagedRage;
using RSG.Model.Asset;
using RSG.Model.Common;
using RSG.Platform;
using RSG.SceneXml;
using RSG.SceneXml.Material;
using RSG.Model.Common.Util;
using System.Xml.Linq;
using RSG.Model.Asset.Util;
using RSG.Model.Common.Weapon;

namespace RSG.Model.Weapon
{
    /// <summary>
    /// 
    /// </summary>
    internal struct WeaponMetadataInfo
    {
        public String GameName { get; set; }
        public String ModelName { get; set; }
        public String StatName { get; set; }
        public String HumanNameHash { get; set; }
        public WeaponCategory Category { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class LocalWeapon : Weapon
    {
        #region Constants
        /// <summary>
        /// 
        /// </summary>
        private const String c_skeletonName = "Gun_Root";
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// Constructor from a SceneXml file.
        /// </summary>
        public LocalWeapon(String name, String friendlyName, String modelName, String statName, WeaponCategory category, IWeaponCollection parentCollection)
            : base(name, friendlyName, modelName, statName, category, parentCollection)
        {
        }
        #endregion // Constructor(s)

        #region StreamableAssetContainerBase Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="statsToLoad"></param>
        protected override void LoadStatsInternal(IEnumerable<StreamableStat> statsToLoad)
        {
            Debug.Assert(ParentCollection is LocalWeaponCollection, "Parent collection isn't an ExportDataWeaponCollection.");
            LocalWeaponCollection localCollection = (LocalWeaponCollection)ParentCollection;

            if (!AreStatsLoaded(statsToLoad.Where(item => item != StreamableWeaponStat.PlatformStats)))
            {
                LoadStatsFromExportData(localCollection.ExportDataPath);
            }

            if (statsToLoad.Contains(StreamableWeaponStat.PlatformStats) && !IsStatLoaded(StreamableWeaponStat.PlatformStats))
            {
                // Get the path to the independent rpf file
                List<ContentNode> contentNodes = localCollection.GameView.Content.Root.FindAll("weaponszip");

                string independentPath = null;
                if (contentNodes.Count > 0)
                {
                    independentPath = LocalLoadUtil.GeneratePathForNode(contentNodes[0]);
                }

                // Open up the pack files
                IDictionary<RSG.Platform.Platform, Packfile> packFiles = LocalLoadUtil.OpenPackFiles(independentPath, localCollection.GameView, localCollection.Config);
                LoadPlatformStatsFromExportData(packFiles);

                // Close all the pack files we opened
                LocalLoadUtil.ClosePackFiles(packFiles);
            }
        }
        #endregion // StreamableAssetContainerBase Overrides

        #region Static Methods
        /// <summary>
        /// 
        /// </summary>
        internal static WeaponMetadataInfo ExtractDataFromMetadataElement(XElement itemElem)
        {
            WeaponMetadataInfo info = new WeaponMetadataInfo();

            XElement nameElem = itemElem.Element("Name");
            Debug.Assert(nameElem != null, "Name element doesn't exist.");
            if (nameElem != null)
            {
                info.GameName = nameElem.Value;
            }

            XElement groupElem = itemElem.Element("Group");
            Debug.Assert(groupElem != null, "Group element doesn't exist.");
            if (groupElem != null && !groupElem.IsEmpty)
            {
                WeaponCategory category;
                WeaponCategoryUtils.TryParseFromRuntimeString(groupElem.Value, out category);
                info.Category = category;
            }

            XElement modelElem = itemElem.Element("Model");
            Debug.Assert(modelElem != null, "Model element doesn't exist.");
            if (modelElem != null && !modelElem.IsEmpty)
            {
                info.ModelName = modelElem.Value;
            }

            XElement statElem = itemElem.Element("StatName");
            Debug.Assert(statElem != null, "Stat name element doesn't exist.");
            if (statElem != null && !statElem.IsEmpty)
            {
                info.StatName = statElem.Value;
            }

            XElement nameLookupElem = itemElem.Element("HumanNameHash");
            Debug.Assert(nameLookupElem != null, "Human name element doesn't exist.");
            if (nameLookupElem != null && !nameLookupElem.IsEmpty)
            {
                info.HumanNameHash = nameLookupElem.Value;
            }

            return info;
        }
        #endregion // Static Methods

        #region Internal Methods
        /// <summary>
        /// Load basic stats from the export data scene xml file
        /// </summary>
        internal void LoadStatsFromExportData(String exportDataPath)
        {
            PolyCount = 0;
            CollisionCount = 0;
            BoneCount = 0;
            HasLod = false;
            LodPolyCount = 0;
            GripCount = 0;
            MagazineCount = 0;
            AttachmentCount = 0;

            bool fileParsed = false;
            if (ModelName != null)
            {
                String sceneXmlFilename = Path.Combine(exportDataPath, ModelName) + ".xml";

                if (File.Exists(sceneXmlFilename))
                {
                    Scene scene = new Scene(sceneXmlFilename);
                    ParseObjects(scene);
                    fileParsed = true;
                }
            }
            if (!fileParsed)
            {
                Shaders = new IShader[] { };
                PlatformStats = new Dictionary<RSG.Platform.Platform, IWeaponPlatformStat>();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="exportDataPath"></param>
        internal void LoadPlatformStatsFromExportData(IDictionary<RSG.Platform.Platform, Packfile> packFiles)
        {
            // Next load in the platform stats
            PlatformStats = new Dictionary<RSG.Platform.Platform, IWeaponPlatformStat>();

            if (packFiles == null)
            {
                return;
            }

            foreach (KeyValuePair<RSG.Platform.Platform, Packfile> pair in packFiles)
            {
                uint physicalSize = 0;
                uint virtualSize = 0;

                // Process all of the extensions we were provided
                foreach (string extension in GetExtensionsForPlatform(pair.Key))
                {
                    PackEntry entry = pair.Value.FindEntry(ModelName + "." + extension);
                    if (entry != null)
                    {
                        physicalSize += (uint)entry.PhysicalSize;
                        virtualSize += (uint)entry.VirtualSize;
                    }
                }

                if (physicalSize != 0 && virtualSize != 0)
                {
                    IWeaponPlatformStat platformStat = new WeaponPlatformStat(pair.Key, physicalSize, virtualSize);
                    PlatformStats.Add(pair.Key, platformStat);
                }
            }
        }
        #endregion // Internal Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="scene"></param>
        private void ParseObjects(Scene scene)
        {
            TargetObjectDef skeleton = null;
            TargetObjectDef mesh = null;
            foreach (TargetObjectDef obj in scene.Objects)
            {
                if (String.Equals(obj.Name, this.ModelName, StringComparison.OrdinalIgnoreCase))
                {
                    mesh = obj;
                }
            }
            foreach (TargetObjectDef obj in scene.Objects)
            {
                TargetObjectDef skel = FindSkeletion(obj);
                if (skel != null)
                {
                    skeleton = skel;
                    break;
                }
            }

            List<IShader> shaders = new List<IShader>();
            ParseMesh(scene, mesh, ref shaders);

            shaders.Sort();
            this.Shaders = shaders.ToArray();

            this.AssetChildren.Clear();
            this.AssetChildren.AddRange(Textures);

            ParseSkeleton(skeleton);

            // Check for the presence of a LOD/attachments
            foreach (var obj in scene.Objects)
            {
                if (!obj.DontExport())
                {
                    if (obj.Name.StartsWith(this.ModelName + "_LOD", StringComparison.OrdinalIgnoreCase))
                    {
                        if (obj.IsObject())
                        {
                            HasLod = true;
                            LodPolyCount += obj.PolyCount;
                        }
                    }
                    else if (obj.Name.StartsWith(this.ModelName + "_", StringComparison.OrdinalIgnoreCase))
                    {
                        if (obj.IsObject())
                        {
                            MagazineCount++;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="root"></param>
        /// <returns></returns>
        private TargetObjectDef FindSkeletion(TargetObjectDef root)
        {
            if (String.Equals(root.Name, c_skeletonName, StringComparison.OrdinalIgnoreCase))
            {
                return root;
            }
            else
            {
                foreach (TargetObjectDef child in root.Children)
                {
                    TargetObjectDef skel = FindSkeletion(child);
                    if (skel != null)
                    {
                        return skel;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="skeleton"></param>
        private void ParseMesh(Scene scene, TargetObjectDef root, ref List<IShader> shaders)
        {
            if (root == null)
                return;
            if (root.DontExport())
                return;

            if (root.IsObject())
            {
                this.PolyCount += root.PolyCount;
            }
            else if (root.IsCollision())
            {
                this.CollisionCount += root.PolyCount;
            }

            MaterialDef material = null;
            if (scene.MaterialLookup.TryGetValue(root.Material, out material))
            {
                GetTexturesFromMaterial(material, ref shaders);
            }
            foreach (var child in root.Children)
            {
                ParseMesh(scene, child, ref shaders);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="skeleton"></param>
        private void ParseSkeleton(TargetObjectDef root)
        {
            if (root == null)
                return;
            if (root.IsBone())
                this.BoneCount++;
            if (root.Name.StartsWith("WAP", StringComparison.OrdinalIgnoreCase))
                this.AttachmentCount++;
            else if (root.Name.StartsWith("Gun_Grip", StringComparison.OrdinalIgnoreCase))
                this.GripCount++;


            foreach (var child in root.Children)
            {
                ParseSkeleton(child);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="material"></param>
        /// <param name="textures"></param>
        /// <param name="shaders"></param>
        private void GetTexturesFromMaterial(MaterialDef material, ref List<IShader> shaders)
        {
            if (material.TextureCount > 0)
            {
                Shader shader = null;
                if (!string.IsNullOrEmpty(material.Preset))
                {
                    shader = new Shader(material.Preset);

                    foreach (TextureDef texture in material.Textures)
                    {
                        Texture newTexture = new Texture(texture.FilePath);
                        if (shader != null)
                        {
                            shader.Textures.Add(newTexture);
                        }
                    }

                    if (!shaders.Contains(shader))
                    {
                        shaders.Add(shader);
                    }
                }
            }

            if (material.HasSubMaterials)
            {
                foreach (MaterialDef submat in material.SubMaterials)
                {
                    GetTexturesFromMaterial(submat, ref shaders);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        private string[] GetExtensionsForPlatform(RSG.Platform.Platform platform)
        {
            List<string> extensions = new List<string>();

            foreach (string ext in new string[] { "dr", "td" })
            {
                extensions.Add(platform.PlatformRagebuilderCharacter() + ext);
            }

            return extensions.ToArray();
        }
        #endregion // Private Methods
    } // LocalWeapon
}
