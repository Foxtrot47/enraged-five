﻿using System.Collections.Generic;
using System.Linq;
using RSG.Base.Tasks;
using RSG.Base.ConfigParser;
using RSG.Base.Logging;
using System.IO;
using System.Xml.Linq;
using RSG.Interop.Incredibuild.XGE;
using System;
using System.Diagnostics;
using RSG.Model.Common;

namespace RSG.Model.Report.Reports.Map
{
    /// <summary>
    /// Collision hole analysis report.
    /// </summary>
    public class CollisionHoleAnalysisReport : Report, IReportFileProvider, IDynamicLevelReport
    {
        #region Private helper classes

        /// <summary>
        /// Task item.
        /// </summary>
        private class TaskItem
        {
            #region Public properties

            /// <summary>
            /// Name of the file to process.
            /// </summary>
            public string FileToProcess { get; set; }

            /// <summary>
            /// Tool parameters.
            /// </summary>
            public string Params { get; set; }

            #endregion

            #region Constructor

            /// <summary>
            /// Constructor.
            /// </summary>
            /// <param name="file">Name of the file to process.</param>
            /// <param name="parameters">Tool parameters.</param>
            public TaskItem(string file, string parameters)
            {
                FileToProcess = file;
                Params = parameters;
            }

            #endregion
        }

        /// <summary>
        /// Task grouping.
        /// </summary>
        private class TaskGroup
        {
            #region Public properties

            /// <summary>
            /// Name of the task group.
            /// </summary>
            public string Name { get; set; }

            /// <summary>
            /// Group items.
            /// </summary>
            public List<TaskItem> Items { get; set; }

            #endregion

            #region Constructor

            /// <summary>
            /// Constructor.
            /// </summary>
            /// <param name="name">Name of the task group.</param>
            public TaskGroup(string name)
            {
                Name = name;
                Items = new List<TaskItem>();
            }

            #endregion
        }

        #endregion

        #region Constants

        private static readonly float DEFAULT_STEP_VALUE = 0.1f; // 10cm probe value.
        private static readonly float FORTY_FIVE_DEGREES = (float)(Math.PI / 4.0);

        #endregion

        #region Private member fields

        private Base.Tasks.ITask m_task;
        private float m_stepValue;
        private string m_gameLevel;

        #endregion

        #region Public properties

        /// <summary>
        /// Step value (in metres) for the ray probe.
        /// </summary>
        public float StepValue
        {
            get { return m_stepValue; }
            set { m_stepValue = value; }
        }

        /// <summary>
        /// Filter used for the save dialog
        /// </summary>
        public string Filter { get { return "Portable Network Graphic (PNG)|*.png"; } }

        /// <summary>
        /// Extensions that this report support
        /// </summary>
        public string SupportedExtension { get { return ".png"; } }

        /// <summary>
        /// Filename specifying where a report should be output
        /// </summary>
        public string Filename { get; set; }

        /// <summary>
        /// Task.
        /// </summary>
        public Base.Tasks.ITask GenerationTask
        {
            get
            {
                if (m_task == null)
                {
                    m_task = new ActionTask("Collision Hole Analysis", (context, progress) => GenerateXGEConfig((DynamicLevelReportContext)context, progress));
                }

                return m_task;
            }
        }

        /// <summary>
        /// List of stats that are required to generate this report.
        /// </summary>
        public IEnumerable<StreamableStat> RequiredStats
        {
            get { return null; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        public CollisionHoleAnalysisReport()
            : base("Collision Hole Analysis", "Analysis of collision mesh coverage for each map section")
        {
            m_stepValue = DEFAULT_STEP_VALUE;
        }

        #endregion

        /// <summary>
        /// Generates the XGE configuration file and starts an IncrediBuild process.
        /// </summary>
        /// <param name="context">Context.</param>
        /// <param name="progress">Progress.</param>
        private void GenerateXGEConfig(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            bool result = false;
            string gameLevel = (context == null || context.Level == null) ? String.Empty : context.Level.Name;

            if (String.IsNullOrEmpty(gameLevel))
            {
                Log.Log__Error("Game level must be specified for collision coverage report.");
                return;
            }

            using (ConfigGameView gv = new ConfigGameView())
            {
                Log.Log__Message("Processing collision coverage...");

                string outDir = Path.Combine(Path.GetDirectoryName(Filename), "collisionreliefdata");
                string xgeBaseDirectory = gv.ToolsTempDir + String.Format(@"\xge\{0}\dev\collision_relief", gameLevel);
                Directory.CreateDirectory(xgeBaseDirectory);
                
                List<string> ragebuilderScriptPathnames = new List<string>();

                TaskGroup mapFiles = new TaskGroup("Processing Map Files");

                var zipNodes = gv.Content.Root.FindAll("processedmapzip");

                foreach (var zipNode in zipNodes.OfType<ContentNodeMapProcessedZip>())
                {
                    if (!zipNode.Filename.Contains(gameLevel))
                    {
                        continue;
                    }

                    string paramsFormat = "--outdir {0} --section {1} --step {2}";
                    string paramString = String.Format(paramsFormat, outDir, zipNode.Filename, m_stepValue);

                    mapFiles.Items.Add(new TaskItem(zipNode.Filename, paramString));
                }

                List<TaskGroup> groups = new List<TaskGroup>() { mapFiles }; 

                string colReliefProcessPathname = Path.Combine(gv.ToolsBinDir, @"CollisionRelief\ColReliefGen.exe");

                if (!File.Exists(colReliefProcessPathname))
                {
                    Log.Log__Warning("Collision coverage report requires ColReliefGen.exe to be available. Check tools\\bin\\CollisionRelief\\colReliefGen.exe.");
                    return;
                }

                RunProcess(colReliefProcessPathname, "--clean"); // delete existing data.

                XDocument xgeConfigDocument = new XDocument(
                    new XElement("BuildSet", new XAttribute("FormatVersion", 1),
                        new XElement("Environments",
                            new XElement("Environment", new XAttribute("Name", "Environment"),
                                new XElement("Tools",
                                    new XElement("Tool", new XAttribute("OutputPrefix", "Generating collision relief map..."),
                                                         new XAttribute("Name", "Collision_Relief_Builder"),
                                                         new XAttribute("OutputFileMasks", "*.csv"),
                                                         new XAttribute("AllowRemote", "false"),
                                                         new XAttribute("TimeLimit", "3600"),
                                                         new XAttribute("Path", colReliefProcessPathname),
                                                         new XAttribute("AllowRestartOnLocal", "false"),
                                                         new XAttribute("Params", ""),
                                                         new XAttribute("GroupPrefix", "Serialising remotely..."))),
                                new XElement("Variables",
                                    new XElement("Variable", new XAttribute("Name", "BinVar"),
                                                             new XAttribute("Value", gv.ToolsBinDir))))),
                        new XElement("Project", new XAttribute("Name", "Collision Relief Map Generation"),
                                                new XAttribute("Env", "Environment"),

                            groups.Select(group =>

                            new XElement("TaskGroup", new XAttribute("Name",group.Name),
                                                      new XAttribute("Env", "Environment"),
                                                      new XAttribute("Tool", "Collision_Relief_Builder"),
                                                      new XAttribute("WorkingDir", "$(BinVar)"),
                                group.Items.Select(task => new XElement("Task", new XAttribute("SourceFile", ""),
                                                                                                         new XAttribute("Params", task.Params),
                                                                                                         new XAttribute("Caption", Path.GetFileNameWithoutExtension(task.FileToProcess)))))))));

                string xgeConfigDocumentPathname = Path.Combine(xgeBaseDirectory, "collision_relief.xml");
                xgeConfigDocument.Save(xgeConfigDocumentPathname);
                string xgeLogPathname = Path.Combine(xgeBaseDirectory, "collision_relief.log");

                result = XGE.Start("Collision Relief Map Generation", xgeConfigDocumentPathname, xgeLogPathname);

                if (result)
                {
                    string reportDir = Path.Combine(gv.AssetsDir, @"reports\");
                    string image = Filename; 
                    float dotSize = m_stepValue * ((float)(Math.Sin(FORTY_FIVE_DEGREES) * 2.0));

                    RunProcess(colReliefProcessPathname, String.Format("--makeimage {0} --outdir {1} --step {2}", image, outDir, dotSize));
                }
            }
        }

        /// <summary>
        /// Run an external process.
        /// </summary>
        /// <param name="executable">Executable.</param>
        /// <param name="arguments">Command line arguments.</param>
        private void RunProcess(string executable, string arguments)
        {
            try
            {
                Process proc = new Process();
                proc.StartInfo.FileName = executable;
                proc.StartInfo.Arguments = arguments;
                proc.Start();
                proc.WaitForExit();
            }
            catch
            {

            }
        }
    }
}
