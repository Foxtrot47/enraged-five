﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using RSG.Base.ConfigParser;
using Ionic.Zip;
using RSG.Model.GlobalTXD;
using RSG.Base.Logging;

namespace RSG.Model.Map3.Util
{
    /// <summary>
    /// Common methods used to get at metadata related file names
    /// </summary>
    public static class Metadata
    {
        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="textureName"></param>
        /// <param name="sectionZipFilename"></param>
        /// <param name="drawableName"></param>
        /// <param name="txdName"></param>
        /// <param name="gv"></param>
        /// <returns></returns>
        public static string GetTcsFilename(string textureName, string sectionZipFilename, string drawableName, string txdName, ConfigGameView gv, bool checkExistence = true)
        {
            String tcsPath = String.Empty;

            if (File.Exists(sectionZipFilename))
            {
                String tclname = textureName + ".tcl";

                try
                {
                    using (ZipFile sectionZip = ZipFile.Read(sectionZipFilename))
                    {
                        MemoryStream tclStream = null;

                        // First look inside the drawable (i,e the .idr.zip file with this classes name inside the containers zip)
                        if (drawableName != null)
                        {
                            String drawableZipFilename = drawableName + ".idr.zip";
                            if (sectionZip.ContainsEntry(drawableZipFilename))
                            {
                                using (MemoryStream drawableStream = new MemoryStream())
                                {
                                    ZipEntry entry = sectionZip[drawableZipFilename];
                                    entry.Extract(drawableStream);
                                    drawableStream.Seek(0, SeekOrigin.Begin);
                                    ZipFile drawableZip = ZipFile.Read(drawableStream);
                                    if (drawableZip.ContainsEntry(tclname))
                                    {
                                        tclStream = new MemoryStream();
                                        drawableZip[tclname].Extract(tclStream);
                                    }
                                }
                            }
                        }

                        // Look inside the texture dictionary (i,e the .itd.zip file with this classes txd name inside the containers zip)
                        if (tclStream == null && txdName != null)
                        {
                            String txdZipFilename = txdName + ".itd.zip";
                            if (sectionZip.ContainsEntry(txdZipFilename))
                            {
                                using (MemoryStream txdStream = new MemoryStream())
                                {
                                    ZipEntry entry = sectionZip[txdZipFilename];
                                    entry.Extract(txdStream);
                                    txdStream.Seek(0, SeekOrigin.Begin);
                                    ZipFile txdZip = ZipFile.Read(txdStream);
                                    if (txdZip.ContainsEntry(tclname))
                                    {
                                        tclStream = new MemoryStream();
                                        txdZip[tclname].Extract(tclStream);
                                    }
                                }
                            }
                        }

                        // If we still haven't found it we need to try the parent txd 
                        if (tclStream == null)
                        {
                            GlobalRoot parentedRoot = new GlobalTXD.GlobalRoot(Path.Combine(gv.AssetsDir, "maps", "ParentTxds.xml"), Path.Combine(gv.AssetsDir, "maps", "Textures"));
                            GlobalTextureDictionary dictionary = null;
                            FindGlobalDictionaryWithTexture(parentedRoot, textureName, ref dictionary);
                            String gtxdZipPath = Path.Combine(gv.ExportDir, "data", "cdimages", "gtxd.zip");
                            if (dictionary != null && File.Exists(gtxdZipPath))
                            {
                                using (ZipFile gtxdZip = ZipFile.Read(gtxdZipPath))
                                {
                                    if (gtxdZip.ContainsEntry(dictionary.Name + ".itd.zip"))
                                    {
                                        using (MemoryStream gtxdStream = new MemoryStream())
                                        {
                                            ZipEntry gtxdEntry = gtxdZip[dictionary.Name + ".itd.zip"];
                                            gtxdEntry.Extract(gtxdStream);
                                            gtxdStream.Seek(0, SeekOrigin.Begin);
                                            using (ZipFile dictionaryZip = ZipFile.Read(gtxdStream))
                                            {
                                                if (dictionaryZip.ContainsEntry(tclname))
                                                {
                                                    tclStream = new MemoryStream();
                                                    dictionaryZip[tclname].Extract(tclStream);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (tclStream != null)
                        {
                            tcsPath = GetTcsFilenameFromTclStream(tclStream, gv, checkExistence);
                            tclStream.Dispose();
                            tclStream.Close();
                        }
                    }
                }
                catch
                {
                }
            }

            return tcsPath;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tclStream"></param>
        /// <param name="gv"></param>
        /// <returns></returns>
        public static string GetTcsFilenameFromTclStream(MemoryStream tclStream, ConfigGameView gv, bool checkExistence = true)
        {
            string tcsPath = null;

            byte[] buffer = tclStream.GetBuffer();
            String tclFile = Encoding.UTF8.GetString(buffer);
            int startIndex = tclFile.IndexOf("<parent>");
            int endIndex = tclFile.IndexOf("</parent>");
            if (startIndex > 0 && endIndex > startIndex + 8)
            {
                tcsPath = tclFile.Substring(startIndex + 8, endIndex - startIndex - 8);
                if (!String.IsNullOrEmpty(tcsPath))
                {
                    tcsPath = tcsPath.Replace("${RS_ASSETS}", gv.AssetsDir);
                    tcsPath = tcsPath.Replace("$(assets)", gv.AssetsDir);
                    tcsPath = tcsPath.Replace("}", "%");
                    tcsPath = tcsPath.Replace("${", "%");
                    tcsPath = System.Environment.ExpandEnvironmentVariables(tcsPath);
                    tcsPath += ".tcs";
                    if (!System.IO.File.Exists(tcsPath))
                    {
                        Log.Log__Message("Found tcs {0} but not located on local disk.", tcsPath);
                        tcsPath = String.Empty;
                    }
                }
            }

            buffer = null;
            return tcsPath;
        }
        #endregion Public Methods

        #region Private Methods
        /// <summary>
        /// Helper function
        /// </summary>
        /// <param name="root"></param>
        /// <param name="textureName"></param>
        /// <param name="dictionary"></param>
        private static void FindGlobalDictionaryWithTexture(IDictionaryContainer root, String textureName, ref GlobalTextureDictionary dictionary)
        {
            foreach (GlobalTextureDictionary childDictionary in root.GlobalTextureDictionaries.Values)
            {
                if (childDictionary.WorkInProgress)
                    continue;

                if (childDictionary.ContainsTexture(textureName))
                {
                    dictionary = childDictionary;
                    return;
                }

                FindGlobalDictionaryWithTexture(childDictionary, textureName, ref dictionary);
                if (dictionary != null)
                    break;
            }
        }
        #endregion // Private Methods
    } // Metadata
}
