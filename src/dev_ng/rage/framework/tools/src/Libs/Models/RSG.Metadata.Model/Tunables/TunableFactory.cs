﻿// --------------------------------------------------------------------------------------------
// <copyright file="TunableFactory.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Tunables
{
    using System.Collections.Generic;
    using System.Xml;
    using RSG.Base.Logging;
    using RSG.Metadata.Model.Definitions.Members;

    /// <summary>
    /// Provides methods to create parCodeGen tunables.
    /// </summary>
    public class TunableFactory
    {
        #region Fields
        /// <summary>
        /// The private reference to the tunable parent that this factory is going to be
        /// creating tunables for.
        /// </summary>
        private ITunableParent _tunableParent;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="TunableFactory"/> class.
        /// </summary>
        /// <param name="tunableParent">
        /// The tunable parent that this factory is going to be making tunables for.
        /// </param>
        public TunableFactory(ITunableParent tunableParent)
        {
            this._tunableParent = tunableParent;
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Creates a <see cref="ITunable"/> object using the specified member to determine the
        /// tunable created.
        /// </summary>
        /// <param name="member">
        /// The parCodeGen member that determines the type of tunable to create.
        /// </param>
        /// <returns>
        /// A parCodeGen tunable that's type is determined by the specified member.
        /// </returns>
        public ITunable Create(IMember member)
        {
            return member.CreateTunable(this._tunableParent);
        }

        /// <summary>
        /// Creates a <see cref="ITunable"/> object using the specified member to determine the
        /// tunable created and the specified components to initialise it.
        /// </summary>
        /// <param name="components">
        /// The string components that are used to initialise the new tunable.
        /// </param>
        /// <param name="member">
        /// The parCodeGen member that determines the type of tunable to create.
        /// </param>
        /// <returns>
        /// A parCodeGen tunable that's type is determined by the specified member.
        /// </returns>
        public ITunable Create(List<string> components, IScalarMember member)
        {
            return member.CreateTunable(components, this._tunableParent);
        }

        /// <summary>
        /// Creates a <see cref="ITunable"/> object using the specified member to determine the
        /// tunable created and the specified Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The Xml.XmlReader that provides the data used to initialise the new tunable.
        /// </param>
        /// <param name="member">
        /// The parCodeGen member that determines the type of tunable to create.
        /// </param>
        /// <param name="log">
        /// A object that is used to log any warnings or errors produced during the parsing of
        /// the specified reader.
        /// </param>
        /// <returns>
        /// A parCodeGen tunable that's type is determined by the specified member.
        /// </returns>
        public ITunable Create(XmlReader reader, IMember member, ILog log)
        {
            return member.CreateTunable(reader, this._tunableParent, log);
        }
        #endregion Methods
    } // RSG.Metadata.Model.Tunables.TunableFactory {Class}
} // RSG.Metadata.Model.Tunables {Namespace}
