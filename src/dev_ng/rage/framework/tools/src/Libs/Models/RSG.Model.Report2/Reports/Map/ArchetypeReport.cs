﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.ConfigParser;
using RSG.Model.Common;
using RSG.Model.Common.Map;
using RSG.Base.Extensions;
using RSG.Base.Tasks;
using System.Diagnostics;

namespace RSG.Model.Report.Reports.Map
{
    /// <summary>
    /// 
    /// </summary>
    public class ArchetypeReport : CSVReport, IDynamicLevelReport
    {
        #region Constants
        private const String c_name = "Archetype Report";
        private const String c_description = "Exports the selected levels archetypes into a .csv file";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (m_task == null)
                {
                    CompositeTask task = new CompositeTask("Generate Report");
                    task.AddSubTask(new ActionTask("Loading data", (context, progress) => EnsureDataLoaded((DynamicLevelReportContext)context, progress)), 50);
                    task.AddSubTask(new ActionTask("Generating report", (context, progress) => GenerateReport((DynamicLevelReportContext)context, progress)));
                    m_task = task;
                }
                return m_task;
            }
        }
        private ITask m_task;

        /// <summary>
        /// List of stats that are required to generate this report.
        /// </summary>
        public IEnumerable<StreamableStat> RequiredStats
        {
            get { return null; }
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public ArchetypeReport()
            : base(c_name, c_description)
        {
        }
        #endregion // Constructor(s)

        #region Task Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void EnsureDataLoaded(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            IMapHierarchy hierarchy = context.Level.MapHierarchy;
            Debug.Assert(hierarchy != null, "Unable to load date for report");
            if (hierarchy == null || hierarchy.AllSections == null)
            {
                return;
            }

            List<IMapSection> mapSections = hierarchy.AllSections.ToList();
            float increment = 1.0f / mapSections.Count;
            string fmt = "Loading {0}";
            foreach (IMapSection section in mapSections)
            {
                context.Token.ThrowIfCancellationRequested();
                string message = String.Format(fmt, section.Name);
                progress.Report(new TaskProgress(increment, true, message));

                section.LoadStats(new List<StreamableStat>() { StreamableMapSectionStat.Archetypes, StreamableMapSectionStat.Entities });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void GenerateReport(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            string directory = System.IO.Path.GetDirectoryName(Filename);
            string filename = System.IO.Path.GetFileNameWithoutExtension(Filename);
            string objectFilename = System.IO.Path.Combine(directory, filename + ".csv");
            string interiorFilename = System.IO.Path.Combine(directory, filename + "_interiors.csv");
            ExportObjectCSVReport(objectFilename, interiorFilename, context.Level, context.GameView);
        }
        #endregion // Task Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="objectFilename"></param>
        /// <param name="interiorFilename"></param>
        /// <param name="level"></param>
        /// <param name="gv"></param>
        private void ExportObjectCSVReport(String objectFilename, String interiorFilename, ILevel level, ConfigGameView gv)
        {
            // Attempt the get the map hierarchy from the level
            IMapHierarchy hierarchy = level.MapHierarchy;

            if (hierarchy != null)
            {
                // Get all the archetypes
                IEnumerable<IMapArchetype> allArchetypes = hierarchy.AllSections.SelectMany(section => section.Archetypes);

                // Determine which collision bit flag combinations are in use
                HashSet<CollisionType> collisionFlags = new HashSet<CollisionType>();
                foreach (IMapArchetype archetype in allArchetypes)
                {
                    collisionFlags.AddRange(archetype.CollisionTypePolygonCounts.Keys);
                }

                // Sort the collision type flags so they are always in the same order in the CSV
                List<CollisionType> sortedCollisionFlags = new List<CollisionType>();
                sortedCollisionFlags.AddRange(collisionFlags);
                sortedCollisionFlags.Sort();

                // Generate the report
                using (StreamWriter objectSw = new StreamWriter(objectFilename))
                {
                    using (StreamWriter interiorSw = new StreamWriter(interiorFilename))
                    {
                        WriteCsvObjectHeader(objectSw, sortedCollisionFlags, false);
                        WriteCsvObjectHeader(interiorSw, sortedCollisionFlags, true);

                        foreach (IMapArchetype archetype in allArchetypes)
                        {
                            WriteCsvArchetype(objectSw, interiorSw, archetype, sortedCollisionFlags, level.GeometryStats);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sw"></param>
        /// <param name="milo"></param>
        private void WriteCsvObjectHeader(StreamWriter sw, IEnumerable<CollisionType> collisionFlags, bool milo)
        {
            string header = "Name,Section,Parent Area,Grandparent Area,Tri Count";
            if (!milo)
            {
                header += ",Bounding Box Volume (m^3),Tri Density (tris/m^3)";
            }
            header += ",Instance Count,Dont Add To IPL,Permanent,Texture Count,Shader Count";
            if (!milo)
            {
                header += ",Fragment,Dynamic,Fixed,Lod Distance,TXD Name,Rendering Cost,Rendering Cost * Lod Distance";
            }
            foreach (CollisionType flag in collisionFlags)
            {
                header += String.Format(",{0} Collision", String.Join(" + ", flag.GetDisplayNames().ToArray()));
            }
            if (!milo)
            {
                header += String.Format(",Medium LOD %,Low LOD %,Very Low LOD %");
            }
            sw.WriteLine(header);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="objectSw"></param>
        /// <param name="interiorSw"></param>
        /// <param name="archetype"></param>
        /// <param name="collisionFlags"></param>
        /// <param name="geometryStatsCollection"></param>
        private void WriteCsvArchetype(StreamWriter objectSw, 
                                       StreamWriter interiorSw, 
                                       IMapArchetype archetype, 
                                       IEnumerable<CollisionType> collisionFlags, 
                                       IGeometryStatsCollection geometryStatsCollection)
        {
            IMapSection section = archetype.ParentSection;
            IMapArea parentArea = section.ParentArea;
            IMapArea grandParentArea = (parentArea != null ? parentArea.ParentArea : null);

            string scvRecord = String.Format("{0},{1},{2},{3}", archetype.Name, section.Name, parentArea != null ? parentArea.Name : "n/a", grandParentArea != null ? grandParentArea.Name : "n/a");

            if (!(archetype is IInteriorArchetype))
            {
                float triDensity = -1.0f;
                if (archetype.BoundingBox.Volume() != 0)
                {
                    triDensity = (float)archetype.PolygonCount / archetype.BoundingBox.Volume();
                }

                string archetypeNameLower = archetype.Name.ToLower();
                float heuristic = -1.0f;
                float heuristicByLODDistance = -1.0f;
                if (geometryStatsCollection != null && geometryStatsCollection.HasStatsForArchetype(archetypeNameLower))
                {
                    heuristic = geometryStatsCollection.GetStatsForArchetype(archetypeNameLower).Heuristic;
                    heuristicByLODDistance = heuristic * archetype.LodDistance;
                }

                scvRecord += String.Format(",{0},{1},{2}", archetype.PolygonCount, archetype.BoundingBox.Volume(), (triDensity == -1.0f ? "n/a" : triDensity.ToString()));
                scvRecord += String.Format(",{0},{1},{2},{3},{4}", archetype.Entities.Count, archetype.DontAddToIpl, archetype.IsPermanentArchetype, archetype.Textures.Count(), archetype.Shaders.Count());

                scvRecord += String.Format(",{0}", (archetype is IFragmentArchetype));
                if (archetype is ISimpleMapArchetype)
                {
                    scvRecord += String.Format(",{0},{1}", (archetype as ISimpleMapArchetype).IsDynamic, (archetype as ISimpleMapArchetype).IsFixed);
                }
                else
                {
                    scvRecord += ",False,False";
                }

                scvRecord += String.Format(",{0},{1}", archetype.LodDistance, String.Join(";", archetype.TxdExportSizes.Keys));
                scvRecord += String.Format(",{0},{1}", heuristic.ToString("F4"), heuristicByLODDistance.ToString("F2"));
            }
            else
            {
                scvRecord += String.Format(",{0},{1},{2},{3},{4},{5}", archetype.PolygonCount, archetype.Entities.Count, archetype.DontAddToIpl, archetype.IsPermanentArchetype, archetype.Textures.Count(), archetype.Shaders.Count());
            }
            foreach (CollisionType flag in collisionFlags)
            {
                uint collisionPolyCount = 0;
                if (archetype.CollisionTypePolygonCounts.ContainsKey(flag))
                {
                    collisionPolyCount = archetype.CollisionTypePolygonCounts[flag];
                }
                scvRecord += String.Format(",{0}", collisionPolyCount);
            }

            if (!(archetype is IInteriorArchetype))
            {
                if (archetype is ISimpleMapArchetype)
                {
                    ISimpleMapArchetype simpleArchetype = (ISimpleMapArchetype)archetype;

                    foreach (MapDrawableLODLevel lodLevel in (MapDrawableLODLevel[])Enum.GetValues(typeof(MapDrawableLODLevel)))
                    {
                        float lodPercentage = 0.0f;
                        if (simpleArchetype.DrawableLODs.ContainsKey(lodLevel))
                        {
                            lodPercentage = (((float)simpleArchetype.DrawableLODs[lodLevel].PolygonCount / (float)archetype.PolygonCount) * 100.0f);
                            scvRecord += String.Format(",{0:0.0}", lodPercentage);
                        }
                        else
                        {
                            scvRecord += ",n/a";
                        }
                    }
                }
                else
                {
                    scvRecord += String.Format(",n/a,n/a,n/a");
                }
            }

            // Output the record to the appropriate stream writer
            if (archetype is IInteriorArchetype)
            {
                interiorSw.WriteLine(scvRecord);
            }
            else
            {
                objectSw.WriteLine(scvRecord);
            }
        }
        #endregion // Private Methods
    } // ArchetypeReport
}
