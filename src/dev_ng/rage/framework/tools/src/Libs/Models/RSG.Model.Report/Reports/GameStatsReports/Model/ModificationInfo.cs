﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.Model.Report.Reports.GameStatsReports.Model
{
    /// <summary>
    /// Contains information about a single modification
    /// </summary>
    public class ModificationInfo : IComparable<ModificationInfo>
    {
        public string Username { get; private set; }
        public string Email { get; set; }
        public string Url { get; private set; }
        public string Comment { get; private set; }
        public uint Changelist { get; private set; }

        public ModificationInfo(XElement element)
        {
            // Extract the various info
            XElement usernameElement = element.Element("UserName");
            if (usernameElement != null)
            {
                Username = usernameElement.Value;
            }

            XElement urlElement = element.Element("Url");
            if (urlElement != null)
            {
                Url = urlElement.Value;
            }

            XElement commentElement = element.Element("Comment");
            if (commentElement != null)
            {
                Comment = commentElement.Value;
            }

            XElement changeElement = element.Element("ChangeNumber");
            if (changeElement != null)
            {
                Changelist = UInt32.Parse(changeElement.Value);
            }
        }

        #region IComparable<ModificationInfo> Interface
        /// <summary>
        /// Compare this ModificationInfo to another.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(ModificationInfo other)
        {
            if (other == null)
            {
                return 1;
            }

            return (Changelist.CompareTo(other.Changelist));
        }
        #endregion // IComparable<IVehicle> Interface
    } // ModificationInfo
}
