﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Configuration;

namespace RSG.Model.Common.GameText
{
    /// <summary>
    /// 
    /// </summary>
    public interface IGameTextCollection
    {
        /// <summary>
        /// Retrieves the text associated with a particular text label for the specified platform and optionally
        /// within a particular section.
        /// </summary>
        /// <param name="label"></param>
        /// <param name="language"></param>
        /// <param name="platform"></param>
        /// <param name="sectionName"></param>
        /// <returns></returns>
        String GetText(String label, Language language, RSG.Platform.Platform platform, String sectionName = null);

        /// <summary>
        /// Attempts to retrieve the text for a particular label/language/platform combination.
        /// </summary>
        /// <param name="label"></param>
        /// <param name="language"></param>
        /// <param name="platform"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        bool TryGetText(String label, Language language, RSG.Platform.Platform platform, out String text);

        /// <summary>
        /// Attempts to retrieve the text for a particular label/language/platform/section combination.
        /// </summary>
        /// <param name="label"></param>
        /// <param name="language"></param>
        /// <param name="platform"></param>
        /// <param name="sectionName"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        bool TryGetText(String label, Language language, RSG.Platform.Platform platform, String sectionName, out String text);

        /// <summary>
        /// Loads a particular language.
        /// </summary>
        /// <param name="language"></param>
        /// <returns></returns>
        bool LoadLanguage(Language language);
    } // IGameTextCollection
}
