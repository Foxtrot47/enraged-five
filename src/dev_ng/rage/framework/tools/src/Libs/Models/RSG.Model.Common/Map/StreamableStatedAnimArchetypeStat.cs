﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common.Map
{
    /// <summary>
    /// Enumeration of all streamable stats a stated anim object can have.
    /// </summary>
    [DependentStat(StreamableMapSectionStat.ArchetypesString)]
    public static class StreamableStatedAnimArchetypeStat
    {
        public const String ComponentArchetypesString = "046678CD-18C0-4715-AEC8-A6E36180E517";

        public static readonly StreamableStat ComponentArchetypes = new StreamableStat(ComponentArchetypesString, true, true);
    } // StreamableStatedAnimArchetypeStat
}
