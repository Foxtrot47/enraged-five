﻿namespace RSG.Model.Vehicle.Game
{
    using System.Collections.Generic;
    using System.Diagnostics;

    /// <summary>
    /// Represents a single Vehicle kit that can be referenced through a game vehicle.
    /// </summary>
    [DebuggerDisplay("Name = {Name}, Model Count = {_modelNames.Count}")]
    public class GameVehicleKit
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Name"/> property.
        /// </summary>
        private string _name;

        /// <summary>
        /// The private field used for the <see cref="ModelNames"/> property.
        /// </summary>
        private List<string> _modelNames;

        /// <summary>
        /// The private field used for the <see cref="ReferenceCount"/> property.
        /// </summary>
        private int _referenceCount;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="GameVehicleKit"/> class.
        /// </summary>
        public GameVehicleKit()
        {
            this._modelNames = new List<string>();
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the name used for this vehicle kit.
        /// </summary>
        public string Name
        {
            get { return this._name; }
            set { this._name = value; }
        }

        /// <summary>
        /// Gets a iterator around the models that this kit uses.
        /// </summary>
        public IEnumerable<string> ModelNames
        {
            get
            {
                foreach (string modelName in this._modelNames)
                {
                    yield return modelName;
                }
            }
        }

        /// <summary>
        /// Gets or sets the number of times this archetype has been referenced by a vehicle.
        /// </summary>
        public int ReferenceCount
        {
            get { return this._referenceCount; }
            set { this._referenceCount = value; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Adds the specified model name into this vehicle kits model list.
        /// </summary>
        /// <param name="effectName">
        /// The name of the model to add.
        /// </param>
        public void AddModel(string modelName)
        {
            if (!this._modelNames.Contains(modelName))
            {
                this._modelNames.Add(modelName);
            }
        }
        #endregion Methods
    } // RSG.Model.Vehicle.Game.GameVehicleKit {Class}
} // RSG.Model.Vehicle.Game {Namespace}
