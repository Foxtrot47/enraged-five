﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;

namespace RSG.Model.Report
{
    /// <summary>
    /// 
    /// </summary>
    public interface IReportMailProvider
    {
        /// <summary>
        /// Mail message containing the report data
        /// </summary>
        MailMessage MailMessage
        {
            get;
        }
    } // IReportMailProvider
}
