﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using RSG.Model.Common;
using RSG.Base.Tasks;
using RSG.Model.Common.Map;

namespace RSG.Model.Report.Reports.Map
{
    /// <summary>
    /// 
    /// </summary>
    public class TextureComparisonReport : CSVReport, IDynamicSectionReport, IReportFileProvider
    {
        #region Constants
        private const String c_name = "Texture Comparison Report";
        private const String c_description = "Exports a texture comparison into a .csv file";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (m_task == null)
                {
                    CompositeTask task = new CompositeTask("Generate Report");
                    task.AddSubTask(new ActionTask("Loading data", (context, progress) => EnsureDataLoaded((DynamicSectionReportContext)context, progress)));
                    task.AddSubTask(new ActionTask("Generating report", (context, progress) => GenerateReport((DynamicSectionReportContext)context, progress)));
                    m_task = task;
                }
                return m_task;
            }
        }
        private ITask m_task;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public TextureComparisonReport()
            : base(c_name, c_description)
        {
        }
        #endregion // Constructor(s)

        #region Task Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void EnsureDataLoaded(DynamicSectionReportContext context, IProgress<TaskProgress> progress)
        {
            float increment = 1.0f / context.Sections.Count();

            // Load all the map section data
            foreach (IMapSection section in context.Sections)
            {
                context.Token.ThrowIfCancellationRequested();

                // Update the progress information
                string message = String.Format("Loading {0}", section.Name);
                progress.Report(new TaskProgress(increment, true, message));

                section.RequestAllStatistics(false);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void GenerateReport(DynamicSectionReportContext context, IProgress<TaskProgress> progress)
        {
            using (StreamWriter sw = new StreamWriter(Filename))
            {
                string header = "Texture Name,";
                header += String.Join(",", context.Sections.Select(item => item.Name));
                sw.WriteLine(header);

                IDictionary<string, IList<int>> textures = new Dictionary<string, IList<int>>();
                int index = 0;
                foreach (IMapSection section in context.Sections)
                {
                    foreach (IEntity entity in section.ChildEntities)
                    {
                        IMapArchetype archetype = entity.ReferencedArchetype as IMapArchetype;
                        if (archetype != null)
                        {
                            foreach (ITexture texture in archetype.Textures)
                            {
                                if (!textures.ContainsKey(texture.Name.ToLower()))
                                {
                                    textures.Add(texture.Name.ToLower(), new List<int>(context.Sections.Count));
                                    for (int i = 0; i < context.Sections.Count; i++)
                                    {
                                        textures[texture.Name.ToLower()].Add(0);
                                    }
                                }
                                textures[texture.Name.ToLower()][index]++;
                            }
                        }
                    }
                    index++;
                }

                foreach (KeyValuePair<string, IList<int>> texture in textures)
                {
                    string textureLine = string.Format("{0},", texture.Key);
                    textureLine += String.Join(",", texture.Value);
                    sw.WriteLine(textureLine);
                }
            }
        }
        #endregion // Task Methods
    } // TextureComparisonReport
} // RSG.Model.Report.Reports.Map
