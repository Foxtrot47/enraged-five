﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.Model.Statistics.Captures
{
    /// <summary>
    /// Contains information about a single modification
    /// </summary>
    public class ModificationInfo : IComparable<ModificationInfo>
    {
        #region Properties
        public String Username { get; private set; }
        [Obsolete("Email address in the modifications.xml file is invalid, we need to generate it manually using the username/p4.")]
        public String Email { get; set; }
        public String Url { get; private set; }
        public String Comment { get; private set; }
        public uint Changelist { get; private set; }
        public DateTime Time { get; private set; }
        public String File { get; private set; }
        public String Folder { get; private set; }

        public String DepotPath
        {
            get
            {
                return String.Format("{0}/{1}", Folder, File);
            }
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Generates the info from a node in the modifications xml document.
        /// </summary>
        /// <param name="element"></param>
        public ModificationInfo(XElement element)
        {
            // Extract the various info
            XElement usernameElement = element.Element("UserName");
            if (usernameElement != null)
            {
                Username = usernameElement.Value;
            }

            XElement urlElement = element.Element("Url");
            if (urlElement != null)
            {
                Url = urlElement.Value;
            }

            XElement commentElement = element.Element("Comment");
            if (commentElement != null)
            {
                Comment = commentElement.Value.Replace("\\n", "\n");
            }

            XElement changeElement = element.Element("ChangeNumber");
            if (changeElement != null)
            {
                Changelist = UInt32.Parse(changeElement.Value);
            }

            XElement timeElement = element.Element("ModifiedTime");
            if (timeElement != null)
            {
                Time = DateTime.Parse(timeElement.Value);
            }
            
            XElement fileElement = element.Element("FileName");
            if (fileElement != null)
            {
                File = fileElement.Value;
            }

            XElement folderElement = element.Element("FolderName");
            if (folderElement != null)
            {
                Folder = folderElement.Value;
            }
        }
        #endregion // Constructor(s)

        #region IComparable<ModificationInfo> Interface
        /// <summary>
        /// Compare this ModificationInfo to another.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(ModificationInfo other)
        {
            if (other == null)
            {
                return 1;
            }

            return (Changelist.CompareTo(other.Changelist));
        }
        #endregion // IComparable<ModificationInfo> Interface
    } // ModificationInfo
}
