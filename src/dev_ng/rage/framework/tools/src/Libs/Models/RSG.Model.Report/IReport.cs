﻿using System;
using RSG.Model.Common;
using RSG.Base.Configuration.Reports;

namespace RSG.Model.Report
{
    /// <summary>
    /// Report Model Interface
    /// </summary>
    public interface IReport : IAsset
    {
        #region Properties
        /// <summary>
        /// A description of the report.
        /// </summary>
        string Description { get; }
        #endregion // Properties
    } // IReport
} // RSG.Model.Report
