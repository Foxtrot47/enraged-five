﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common.Map
{
    /// <summary>
    /// 
    /// </summary>
    public class StatKey : IEquatable<StatKey>
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public StatGroup Group { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public StatLodLevel LodLevel { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public StatEntityType EntityType { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IncludesPropGroup { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="group"></param>
        /// <param name="lodLevel"></param>
        /// <param name="entityType"></param>
        public StatKey(StatGroup group, StatLodLevel lodLevel, StatEntityType entityType, bool includesPropGroup = false)
        {
            Group = group;
            LodLevel = lodLevel;
            EntityType = entityType;
            IncludesPropGroup = includesPropGroup;
        }
        #endregion // Constructor(s)

        #region Object Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return String.Format("({0},{1},{2}{3})", Group, LodLevel, EntityType, (IncludesPropGroup ? ",IncludesPropGroup" : ""));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return base.Equals(obj);
            }

            return ((obj is StatKey) && Equals(obj as StatKey));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return (Group.GetHashCode() ^ LodLevel.GetHashCode() ^ EntityType.GetHashCode() ^ IncludesPropGroup.GetHashCode());
        }
        #endregion // Object Overrides

        #region IEquatable<StatKey> Interface Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(StatKey other)
        {
            if (other == null)
            {
                return false;
            }

            return (Group == other.Group &&
                LodLevel == other.LodLevel &&
                EntityType == other.EntityType &&
                IncludesPropGroup == other.IncludesPropGroup);
        }
        #endregion // IEquatable<StatKey> Interface Methods
    } // StatKey
}
