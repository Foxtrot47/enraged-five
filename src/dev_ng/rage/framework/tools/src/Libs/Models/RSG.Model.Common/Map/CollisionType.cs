﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Reflection;
using RSG.Base.Extensions;

namespace RSG.Model.Common.Map
{
    /// <summary>
    /// Various collision types that an object can have.
    /// The description property contains the name of the user property to search for when in an objectdef.
    /// </summary>
    [Flags]
    public enum CollisionType
    {
        [Description("weapons")]
        [DefaultValue(true)]
        Weapon = 0x1,

        [Description("mover")]
        [DefaultValue(true)]
        Mover = 0x2,

        [Description("river")]
        [DefaultValue(false)]
        River = 0x4,

        [Description("deep_material_surface")]
        [DefaultValue(false)]
        DeepMaterialSurface = 0x8,

        [Description("foliage")]
        [DefaultValue(false)]
        Foliage = 0x10,

        [Description("horse")]
        [DefaultValue(true)]
        Horse = 0x20,

        [Description("cover")]
        [DefaultValue(true)]
        Cover = 0x40,

        [Description("vehicle")]
        [DefaultValue(true)]
        Vehicle = 0x80,

        [Description("stair_slope")]
        [DefaultValue(false)]
        StairSlope = 0x100,

        [Description("material")]
        [DefaultValue(false)]
        MaterialOnly = 0x200
    } // CollisionType

    /// <summary>
    /// Extension method for getting the user property from an enum value
    /// </summary>
    public static class CollisionTypeEnumExtensions
    {
        /// <summary>
        /// All flag combinations for the CollisionType object
        /// </summary>
        public static IEnumerable<CollisionType> AllFlagCombinations
        {
            get
            {
                if (m_allFlagCombinations == null)
                {
                    m_allFlagCombinations = EnumExtensions.EnumerateAllFlags<CollisionType>();
                }
                return m_allFlagCombinations;
            }
        }
        private static IEnumerable<CollisionType> m_allFlagCombinations;

        /// <summary>
        /// Retrieves the first description string associated with an enum value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetUserPropertyName(this CollisionType value)
        {
            return value.GetDescriptionValue();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsSetByDefault(this CollisionType value)
        {
            return (bool)value.GetDefaultValue();
        }

        /// <summary>
        /// Returns the list of display names for the enum value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static IEnumerable<string> GetDisplayNames(this CollisionType value)
        {
            Type type = value.GetType();

            foreach (CollisionType flag in Enum.GetValues(typeof(CollisionType)))
            {
                if ((value & flag) != 0)
                {
                    yield return flag.ToString();
                }
            }
        }
    } // CollisionTypeEnumExtensions
}
