﻿// --------------------------------------------------------------------------------------------
// <copyright file="MapMember.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions.Members
{
    using System;
    using System.Xml;
    using RSG.Base.Extensions;
    using RSG.Base.Logging;
    using RSG.Editor;
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// Represents the &lt;map&gt; member node in the parCodeGen system that can be instanced
    /// in a metadata file.
    /// </summary>
    public class MapMember : MemberBase, IEquatable<MapMember>
    {
        #region Fields
        /// <summary>
        /// The string representation of the <see cref="KeyType.AtHashString"/> constant.
        /// </summary>
        private const string MapKeyAtHashString = "atHashString";

        /// <summary>
        /// The string representation of the <see cref="KeyType.AtHashValue"/> constant.
        /// </summary>
        private const string MapKeyAtHashValue = "atHashValue";

        /// <summary>
        /// The string representation of the <see cref="KeyType.Enum"/> constant.
        /// </summary>
        private const string MapKeyEnum = "enum";

        /// <summary>
        /// The string representation of the <see cref="KeyType.Signed16"/> constant.
        /// </summary>
        private const string MapKeySigned16 = "s16";

        /// <summary>
        /// The string representation of the <see cref="KeyType.Signed32"/> constant.
        /// </summary>
        private const string MapKeySigned32 = "s32";

        /// <summary>
        /// The string representation of the <see cref="KeyType.Signed8"/> constant.
        /// </summary>
        private const string MapKeySigned8 = "s8";

        /// <summary>
        /// The string representation of the <see cref="KeyType.Unsigned16"/> constant.
        /// </summary>
        private const string MapKeyUnsigned16 = "u16";

        /// <summary>
        /// The string representation of the <see cref="KeyType.Unsigned32"/> constant.
        /// </summary>
        private const string MapKeyUnsigned32 = "u32";

        /// <summary>
        /// The string representation of the <see cref="KeyType.Unsigned8"/> constant.
        /// </summary>
        private const string MapKeyUnsigned8 = "u8";

        /// <summary>
        /// The string representation of the <see cref="MapType.AtBinary"/> constant.
        /// </summary>
        private const string MapTypeAtBinaryMap = "atBinaryMap";

        /// <summary>
        /// The string representation of the <see cref="MapType.AtMap"/> constant.
        /// </summary>
        private const string MapTypeAtMap = "atMap";

        /// <summary>
        /// The name of the xml attribute containing the data for the
        /// <see cref="EnumKeySize"/> property.
        /// </summary>
        private const string XmlEnumKeySizeAttr = "enumKeySize";

        /// <summary>
        /// The name of the xml attribute containing the data for the
        /// <see cref="EnumKeyType"/> property.
        /// </summary>
        private const string XmlEnumKeyTypeAttr = "enumKeyType";

        /// <summary>
        /// The name of the xml attribute containing the data for the
        /// <see cref="AddGroupWidgets"/> property.
        /// </summary>
        private const string XmlGroupWidgetsAttr = "addGroupWidget ";

        /// <summary>
        /// The name of the xml attribute containing the data for the
        /// <see cref="Key"/> property.
        /// </summary>
        private const string XmlKeyAttr = "key";

        /// <summary>
        /// The name of the xml attribute containing the data for the
        /// <see cref="Type"/> property.
        /// </summary>
        private const string XmlTypeAttr = "type";

        /// <summary>
        /// The private field used for the <see cref="AddGroupWidgets"/> property.
        /// </summary>
        private string _addGroupWidgets;

        /// <summary>
        /// The private field used for the <see cref="ElementType"/> property.
        /// </summary>
        private IMember _elementType;

        /// <summary>
        /// The private field used for the <see cref="EnumKeySize"/> property.
        /// </summary>
        private string _enumKeySize;

        /// <summary>
        /// The private field used for the <see cref="EnumKey"/> property.
        /// </summary>
        private string _enumKeyType;

        /// <summary>
        /// The private field used for the <see cref="Key"/> property.
        /// </summary>
        private string _key;

        /// <summary>
        /// The private field used for the <see cref="Type"/> property.
        /// </summary>
        private string _type;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MapMember"/> class to be one of the
        /// members of the specified structure.
        /// </summary>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        public MapMember(IStructure structure)
            : base(structure)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="MapMember"/> class as a copy of the
        /// specified instance.
        /// </summary>
        /// <param name="other">
        /// The instance to copy.
        /// </param>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        public MapMember(MapMember other, IStructure structure)
            : base(other, structure)
        {
            this._addGroupWidgets = other._addGroupWidgets;
            this._type = other._type;
            this._key = other._key;
            this._elementType = other._elementType;
            this._enumKeySize = other._enumKeySize;
            this._enumKeyType = other._enumKeyType;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="MapMember"/> class.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        public MapMember(XmlReader reader, IStructure structure)
            : base(reader, structure)
        {
            this.Deserialise(reader);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether the child widgets should be displayed.
        /// </summary>
        public bool AddGroupWidgets
        {
            get { return this.Dictionary.To<bool>(this._addGroupWidgets, true); }
            set { this.SetProperty(ref this._addGroupWidgets, value.ToString()); }
        }

        /// <summary>
        /// Gets or sets the member that is used as the item to this array.
        /// </summary>
        public IMember ElementType
        {
            get { return this._elementType; }
            set { this.SetProperty(ref this._elementType, value); }
        }

        /// <summary>
        /// Gets the <see cref="IEnumeration"/> object being used as a key
        /// for this map is applicable; otherwise, null.
        /// </summary>
        public IEnumeration EnumKey
        {
            get
            {
                IDefinitionDictionary scope = null;
                if (this.Structure != null)
                {
                    scope = this.Structure.Dictionary;
                }

                return this.GetEnumerationType(scope);
            }
        }

        /// <summary>
        /// Gets or sets the size of the key if the key is creating using a enumeration type.
        /// </summary>
        public ushort EnumKeySize
        {
            get { return this.Dictionary.To<ushort>(this._enumKeySize, 32); }
            set { this.SetProperty(ref this._enumKeySize, value.ToString()); }
        }

        /// <summary>
        /// Gets or sets the enumeration data type that should be used as the key for this map.
        /// </summary>
        public string EnumKeyType
        {
            get { return this._enumKeyType; }
            set { this.SetProperty(ref this._enumKeyType, value, "EnumKeyType", "EnumKey"); }
        }

        /// <summary>
        /// Gets a value indicating whether this map is of a fixed length.
        /// </summary>
        public bool IsFixedLength
        {
            get
            {
                if (this.EnumKeyType != null)
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// Gets or sets the key type used for this map member.
        /// </summary>
        public KeyType Key
        {
            get { return this.GetKeyFromString(this._key); }
            set { this.SetProperty(ref this._key, this.GetStringFromKey(value)); }
        }

        /// <summary>
        /// Gets or sets the type of array this array member represents in the c++ code.
        /// </summary>
        public MapType Type
        {
            get { return this.GetTypeFromString(this._type); }
            set { this.SetProperty(ref this._type, this.GetStringFromType(value)); }
        }

        /// <summary>
        /// Gets the type name for this member.
        /// </summary>
        public override string TypeName
        {
            get { return "map"; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new <see cref="MapMember"/> that is a copy of this instance.
        /// </summary>
        /// <returns>
        /// A new <see cref="MapMember"/> that is a copy of this instance.
        /// </returns>
        public new MapMember Clone()
        {
            return new MapMember(this, this.Structure);
        }

        /// <summary>
        /// Creates a <see cref="ITunable"/> object that represents a instance of this member
        /// with it's default value set.
        /// </summary>
        /// <param name="parent">
        /// The tunable parent the new tunable will belong to.
        /// </param>
        /// <returns>
        /// A parCodeGen tunable that represents a instance of this member.
        /// </returns>
        public override ITunable CreateTunable(ITunableParent parent)
        {
            return new MapTunable(this, parent);
        }

        /// <summary>
        /// Creates a <see cref="ITunable"/> object that represents a instance of this member
        /// using the specified Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The Xml.XmlReader that provides the data used to initialise the new tunable.
        /// </param>
        /// <param name="parent">
        /// The tunable parent the new tunable will belong to.
        /// </param>
        /// <param name="log">
        /// A object that is used to log any warnings or errors produced during the parsing of
        /// the specified reader.
        /// </param>
        /// <returns>
        /// A parCodeGen tunable that represents a instance of this member.
        /// </returns>
        public override ITunable CreateTunable(
            XmlReader reader, ITunableParent parent, ILog log)
        {
            return new MapTunable(reader, this, parent, log);
        }

        /// <summary>
        /// Creates a deep copy of the current instance.
        /// </summary>
        /// <returns>
        /// A deep copy of the current System.Object.
        /// </returns>
        public override object DeepClone()
        {
            return this.Clone();
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="MapMember"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public bool Equals(MapMember other)
        {
            if (other == null)
            {
                return false;
            }

            return object.ReferenceEquals(other, this);
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="IMember"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public override bool Equals(IMember other)
        {
            return this.Equals(other as MapMember);
        }

        /// <summary>
        /// Writes the xml representation of this instance onto the specified
        /// System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that the xml representation of this instance should be
        /// written to.
        /// </param>
        public override void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                throw new SmartArgumentNullException(() => writer);
            }

            this.SerialiseBaseAttributes(writer);
            if (this._type != null)
            {
                writer.WriteAttributeString(XmlTypeAttr, this._type);
            }

            if (this._key != null)
            {
                writer.WriteAttributeString(XmlKeyAttr, this._key);
            }

            if (this._addGroupWidgets != null)
            {
                writer.WriteAttributeString(XmlGroupWidgetsAttr, this._addGroupWidgets);
            }

            if (this._enumKeyType != null)
            {
                writer.WriteAttributeString(XmlEnumKeyTypeAttr, this._enumKeyType);
            }

            if (this._enumKeySize != null)
            {
                writer.WriteAttributeString(XmlEnumKeySizeAttr, this._enumKeySize);
            }

            if (this.ElementType == null)
            {
                return;
            }

            writer.WriteStartAttribute(MemberFactory.GetNodeNameForMember(this.ElementType));
            this.ElementType.Serialise(writer);
            writer.WriteEndElement();
        }

        /// <summary>
        /// Creates the member type for this array using the specified System.Xml.XmlReader
        /// as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to create the member type.
        /// </param>
        private void CreateElementType(XmlReader reader)
        {
            reader.Read();
            while (reader.MoveToContent() != XmlNodeType.EndElement)
            {
                if (!reader.IsStartElement())
                {
                    reader.Read();
                    continue;
                }

                if (this.ElementType == null)
                {
                    this.ElementType = MemberFactory.Create(reader, this.Structure);
                    this.ElementType.IsSubMember = true;
                    continue;
                }

                reader.Skip();
            }

            reader.Read();
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            this._addGroupWidgets = reader.GetAttribute(XmlGroupWidgetsAttr);
            this._type = reader.GetAttribute(XmlTypeAttr);
            this._key = reader.GetAttribute(XmlKeyAttr);
            this._enumKeySize = reader.GetAttribute(XmlEnumKeySizeAttr);
            this._enumKeyType = reader.GetAttribute(XmlEnumKeyTypeAttr);
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToElementOrComment())
            {
                if (reader.NodeType == XmlNodeType.Comment)
                {
                    reader.Skip();
                    continue;
                }

                if (this.ElementType == null)
                {
                    this.ElementType = MemberFactory.Create(reader, this.Structure);
                    if (this.ElementType == null)
                    {
                        reader.Skip();
                    }
                    else
                    {
                        this.ElementType.IsSubMember = true;
                    }

                    continue;
                }
                else
                {
                    reader.Skip();
                }
            }

            if (reader.NodeType == XmlNodeType.EndElement)
            {
                reader.ReadEndElement();
            }
        }

        /// <summary>
        /// Looks through the specified scope and returns the enumeration that this map
        /// member is using for its enum key.
        /// </summary>
        /// <param name="scope">
        /// A definition dictionary containing all the other types in the search scope.
        /// </param>
        /// <returns>
        /// This maps enum key.
        /// </returns>
        private IEnumeration GetEnumerationType(IDefinitionDictionary scope)
        {
            if (this._enumKeyType == null || scope == null)
            {
                return null;
            }

            return scope.GetEnumeration(this._enumKeyType);
        }

        /// <summary>
        /// Gets the map key that is equivalent to the specified string.
        /// </summary>
        /// <param name="key">
        /// The string to determine the map key type to return.
        /// </param>
        /// <returns>
        /// The map key type that is equivalent to the specified string.
        /// </returns>
        private KeyType GetKeyFromString(string key)
        {
            if (key == null)
            {
                return KeyType.Unknown;
            }
            else if (String.Equals(key, MapKeySigned8))
            {
                return KeyType.Signed8;
            }
            else if (String.Equals(key, MapKeyUnsigned8))
            {
                return KeyType.Unsigned8;
            }
            else if (String.Equals(key, MapKeySigned16))
            {
                return KeyType.Signed16;
            }
            else if (String.Equals(key, MapKeyUnsigned16))
            {
                return KeyType.Unsigned16;
            }
            else if (String.Equals(key, MapKeySigned32))
            {
                return KeyType.Signed32;
            }
            else if (String.Equals(key, MapKeyUnsigned32))
            {
                return KeyType.Unsigned32;
            }
            else if (String.Equals(key, MapKeyEnum))
            {
                return KeyType.Enum;
            }
            else if (String.Equals(key, MapKeyAtHashValue))
            {
                return KeyType.AtHashValue;
            }
            else if (String.Equals(key, MapKeyAtHashString))
            {
                return KeyType.AtHashString;
            }
            else
            {
                return KeyType.Unrecognised;
            }
        }

        /// <summary>
        /// Gets the string value that is equivalent to the specified key.
        /// </summary>
        /// <param name="key">
        /// The map key type that the returned string will be equivalent to.
        /// </param>
        /// <returns>
        /// The string that is equivalent to the specified map key type.
        /// </returns>
        private string GetStringFromKey(KeyType key)
        {
            switch (key)
            {
                case KeyType.Signed8:
                    return MapKeySigned8;
                case KeyType.Unsigned8:
                    return MapKeyUnsigned8;
                case KeyType.Signed16:
                    return MapKeySigned16;
                case KeyType.Unsigned16:
                    return MapKeyUnsigned16;
                case KeyType.Signed32:
                    return MapKeySigned32;
                case KeyType.Unsigned32:
                    return MapKeyUnsigned32;
                case KeyType.Enum:
                    return MapKeyEnum;
                case KeyType.AtHashString:
                    return MapKeyAtHashString;
                case KeyType.AtHashValue:
                    return MapKeyAtHashValue;
                case KeyType.Unknown:
                case KeyType.Unrecognised:
                default:
                    return null;
            }
        }

        /// <summary>
        /// Gets the string value that is equivalent to the specified type.
        /// </summary>
        /// <param name="type">
        /// The type that the returned string will be equivalent to.
        /// </param>
        /// <returns>
        /// The string that is equivalent to the specified type.
        /// </returns>
        private string GetStringFromType(MapType type)
        {
            switch (type)
            {
                case MapType.AtMap:
                    return MapTypeAtMap;
                case MapType.AtBinary:
                    return MapTypeAtBinaryMap;
                case MapType.Unknown:
                case MapType.Unrecognised:
                default:
                    return null;
            }
        }

        /// <summary>
        /// Gets the map type that is equivalent to the specified string.
        /// </summary>
        /// <param name="type">
        /// The string to determine the type to return.
        /// </param>
        /// <returns>
        /// The type that is equivalent to the specified string.
        /// </returns>
        private MapType GetTypeFromString(string type)
        {
            if (type == null)
            {
                return MapType.Unknown;
            }
            else if (String.Equals(type, MapTypeAtMap))
            {
                return MapType.AtMap;
            }
            else if (String.Equals(type, MapTypeAtBinaryMap))
            {
                return MapType.AtBinary;
            }
            else
            {
                return MapType.Unrecognised;
            }
        }

        /// <summary>
        /// Determines whether the specified key is a valid map key.
        /// </summary>
        /// <param name="key">
        /// The key to test.
        /// </param>
        /// <returns>
        /// True if the specified string can be used as a map key; otherwise, false.
        /// </returns>
        private bool IsKeyValid(string key)
        {
            if (this._key != "atHashString")
            {
                return true;
            }
            else if (this._key != "atHashValue")
            {
                return true;
            }
            else if (this._key != "s8")
            {
                return true;
            }
            else if (this._key != "s16")
            {
                return true;
            }
            else if (this._key != "s32")
            {
                return true;
            }
            else if (this._key != "u8")
            {
                return true;
            }
            else if (this._key != "u16")
            {
                return true;
            }
            else if (this._key != "u32")
            {
                return true;
            }
            else if (this._key != "atHashString")
            {
                return true;
            }
            else if (this._key != "atHashValue")
            {
                return true;
            }

            return false;
        }
        #endregion Methods
    } // RSG.Metadata.Model.Definitions.Members.MapMember {Class}
} // RSG.Metadata.Model.Definitions.Members {Namespace}
