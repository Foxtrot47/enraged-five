﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using RSG.Base.ConfigParser;
using RSG.Base.Collections;
using RSG.SceneXml;
using RSG.Base.Logging;
using RSG.Model.Map.Statistics;
using RSG.Base.Math;
using RSG.Model.Map.Utilities;

namespace RSG.Model.Map
{
    using StatisticsContainer = Dictionary<String, Object>;

    public enum SectionTypes
    {
        UNKNOWN,
        MAP,            // Section represents a map section that has both ide and ipl data in it.
        PROPGROUP,      // Section represents a prop group - a max file that contains xrefs for a map container or a map container that contains rsrefs
        PROPS,          // Section represents multiple props that can be referenced inside a map contaier
        INTERIORS,      // Section represents a single interior that can be referenced inside a map contaier
    }

    /// <summary>
    /// 
    /// </summary>
    public class MapSection : IMapComponent, INotifyPropertyChanged, ISearchable
    {
        #region Events

        /// <summary>
        /// Property changed event fired when the name changes so that the 
        /// binding can be dynamic.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Create the OnPropertyChanged method to raise the event
        /// </summary>
        /// <param name="name"></param>
        protected void OnPropertyChanged(String name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        #endregion

        #region Constants
        private static readonly String EXT_IDE = ".ide";
        private static readonly String EXT_IPL = ".ipl";
        private static readonly String EXT_ZIP = ".zip";
        private static readonly String EXT_XML = ".xml";
        #endregion // Constants

        #region Published Events
        /// <summary>
        /// Event raised when attached user-data object is changed.
        /// </summary>
        public event EventHandler UserDataChanged;
        #endregion // Published Events

        #region Properties and Associated Member Data

        /// <summary>
        /// The type of this section (i.e what is in this section) (map objects, map references, single/multiple props or single/multiple interiors)
        /// </summary>
        public SectionTypes SectionType
        {
            get { return m_sectionType; }
            set { m_sectionType = value; }
        }
        private SectionTypes m_sectionType;

        /// <summary>
        /// Reference to the owner Level object.
        /// </summary>
        public Level Level
        {
            get;
            private set;
        }

        /// <summary>
        /// Map section name.
        /// </summary>
        public String Name { get { return this.MapNode.Name; } }

        /// <summary>
        /// Map section source art path
        /// </summary>
        public String SourceArtPath 
        { 
            get { return this.MapNode.Path; } 
        }

        /// <summary>
        /// 
        /// </summary>
        public String SourceAssetFilename
        {
            get 
            { 
                if (m_sourceAssetFilename == null)
                {
                    m_sourceAssetFilename = Path.Combine(this.MapNode.Path, this.ZipDataNode.Children[0].Name + ".max");
                }
                return m_sourceAssetFilename;
            }
            private set { m_sourceAssetFilename = value; }
        }
        private String m_sourceAssetFilename = null;

        /// <summary>
        /// Map section object name prefix.
        /// </summary>
        public String Prefix { get { return this.MapNode.Prefix; } }

        /// <summary>
        /// Map section exports definitions?
        /// </summary>
        public bool ExportDefinitions { get { return this.MapNode.ExportDefinitions; } }

        /// <summary>
        /// Map section exports instances?
        /// </summary>
        public bool ExportInstances { get { return this.MapNode.ExportInstances; } }

        /// <summary>
        /// Map section exports RPF data?
        /// </summary>
        public bool ExportData { get { return this.MapNode.ExportData; } }

        /// <summary>
        /// Map section export data source path.
        /// </summary>
        public String ExportPath
        {
            get
            {
                if (m_exportPath == null)
                {
                    m_exportPath = ZipDataNode.Path;
                }
                return m_exportPath;
            }
            private set { m_exportPath = value; }
        }
        private String m_exportPath = null;

        /// <summary>
        /// Array of absolute export filenames for this Map Section.
        /// </summary>
        public String[] ExportFiles
        {
            get
            {
                List<String> files = new List<String>();

                if (!String.IsNullOrEmpty(this.IdeFilename))
                {
                    files.Add(this.IdeFilename);
                }
                if (!String.IsNullOrEmpty(this.IplFilename))
                {
                    files.Add(this.IplFilename);
                }
                if (!String.IsNullOrEmpty(this.ZipFilename))
                {
                    files.Add(this.ZipFilename);
                }
                if (!String.IsNullOrEmpty(this.SceneXmlFilename))
                {
                    files.Add(this.SceneXmlFilename);
                }
                return (files.ToArray());
            }
        }

        /// <summary>
        /// The filepath to the scene xml for this map section
        /// </summary>
        public String SceneXmlFilename
        {
            get;
            private set;
        }

        /// <summary>
        /// The filename of the zip file node
        /// </summary>]
        public String ZipFilename
        {
            get;
            private set;
        }

        /// <summary>
        /// The main zip map node that is used to create this map section
        /// </summary>
        private ContentNodeMapZip ZipDataNode;

        /// <summary>
        /// 
        /// </summary>
        public String IdeFilename
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public String IplFilename
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public String MapNodePath
        {
            get;
            private set;
        }

        /// <summary>
        /// Reference to user-defined data (application specific map section data).
        /// </summary>
        public Object UserData
        {
            get { return m_UserData; }
            set { 
                m_UserData = value;
                if (null != UserDataChanged)
                    UserDataChanged(this, EventArgs.Empty);
            }
        }
        private Object m_UserData;

        /// <summary>
        /// The model user data for this map section
        /// </summary>
        public UserData ModelUserData
        {
            get { return m_modelUserData; }
            set { m_modelUserData = value; }
        }
        private UserData m_modelUserData;

        /// <summary>
        /// The immediate parent of this map section
        /// </summary>
        public IMapContainer Container
        {
            get { return m_container; }
            set { m_container = value; }
        }
        private IMapContainer m_container = null;

        /// <summary>
        /// The statistics that belong to this map section
        /// </summary>
        public Statistics.Base SectionStatistics
        {
            get { return m_sectionStatistics; }
            set { m_sectionStatistics = value; }
        }
        private Statistics.Base m_sectionStatistics;

        /// <summary>
        /// A dictionary of statistics on this map section,
        /// the statistics include collision statistics
        /// as well. This dictionary is indexed by statistic 
        /// names found in RSG.Model.Map.Statistics.StatNames
        /// </summary>
        public StatisticsContainer Statistics
        {
            get;
            private set;
        }

        /// <summary>
        /// A list of all the definitions that exist in
        /// this map section
        /// </summary>
        public List<IMapDefinition> Definitions
        {
            get;
            private set;
        }

        /// <summary>
        /// A list of all the instances that exist in
        /// this map section
        /// </summary>
        public List<MapInstance> Instances
        {
            get;
            private set;
        }

        /// <summary>
        /// The set of texture dictionaries that this
        /// section has in it (the texture dictionaries that
        /// will be created on export)
        /// </summary>
        public TextureDictionarySet TextureDictionaries
        {
            get;
            private set;
        }

        /// <summary>
        /// Represents whether this map section is valid.
        /// (i.e whether this map sections scene xml exists)
        /// </summary>
        public Boolean ValidMapData
        {
            get { return m_validMapData; }
            set { m_validMapData = value; OnPropertyChanged("ValidMapData"); }
        }
        private Boolean m_validMapData;

        /// <summary>
        /// A reference to the prop group (if one exists)
        /// for this map section
        /// </summary>
        public MapSection PropGroup
        {
            get;
            set;
        }

        /// <summary>
        /// The parent of this searchable object
        /// </summary>
        public ISearchable SearchableParent
        {
            get;
            private set;
        }

        #endregion // Properties and Associated Member ExportData

        #region Private Member Data

        /// <summary>
        /// Source art content node.  Not public because this may change in a 
        /// future version of the model.
        /// </summary>
        private RSG.Base.ConfigParser.ContentNodeMap MapNode;

        #endregion // Private Member Data

        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        /// <param name="container"></param>
        public MapSection(Level level, ContentNodeMap map, ContentNodeMapZip mapzip)
        {
            this.Level = level;
            this.MapNode = map;
            this.ZipDataNode = mapzip;
            this.ModelUserData = new UserData();
            this.ValidMapData = true;
            this.SearchableParent = level;

#if !DEBUG
                    //this.IndependentPath = this.ZipDataNode.Path;
#endif // !_DEBUG

            this.SourceAssetFilename = String.Empty;
            this.SourceAssetFilename = System.IO.Path.Combine(map.Path, ZipDataNode.Children[0].Name + ".max");

            foreach (ContentNode node in this.ZipDataNode.Children)
            {
                if (!(node is ContentNodeFile))
                    continue;

                ContentNodeFile fileNode = (node as ContentNodeFile);
                if (EXT_IDE == Path.GetExtension(fileNode.Filename))
                    this.IdeFilename = Path.Combine(ZipDataNode.Path, fileNode.Filename);
                else if (EXT_IPL == Path.GetExtension(fileNode.Filename))
                    this.IplFilename = Path.Combine(ZipDataNode.Path, fileNode.Filename);
                else if (EXT_ZIP == Path.GetExtension(fileNode.Filename))
                    this.ZipFilename = Path.Combine(ZipDataNode.Path, fileNode.Filename);
                else if (EXT_XML == Path.GetExtension(fileNode.Filename))
                    this.SceneXmlFilename = Path.Combine(ZipDataNode.Path, fileNode.Filename);

            }

            if (File.Exists(this.SceneXmlFilename))
            {
                this.CreateStatistics();
            }
            else
            {
                this.ValidMapData = false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="container"></param>
        public MapSection(Level level, IMapContainer container, ContentNodeMap map, ContentNodeMapZip mapzip)
        {
            this.Level = level;
            this.MapNode = map;
            this.ZipDataNode = mapzip;
            this.Container = container;
            this.ModelUserData = new UserData();
            this.ValidMapData = true;
            this.SearchableParent = container;
            
            this.SourceAssetFilename = String.Empty;
            this.SourceAssetFilename = System.IO.Path.Combine(map.Path, ZipDataNode.Children[0].Name + ".max");

            foreach (ContentNode node in this.ZipDataNode.Children)
            {
                if (!(node is ContentNodeFile))
                    continue;

                ContentNodeFile fileNode = (node as ContentNodeFile);
                if (EXT_IDE == Path.GetExtension(fileNode.Filename))
                    this.IdeFilename = Path.Combine(ZipDataNode.Path, fileNode.Filename);
                else if (EXT_IPL == Path.GetExtension(fileNode.Filename))
                    this.IplFilename = Path.Combine(ZipDataNode.Path, fileNode.Filename);
                else if (EXT_ZIP == Path.GetExtension(fileNode.Filename))
                    this.ZipFilename = Path.Combine(ZipDataNode.Path, fileNode.Filename);
                else if (EXT_XML == Path.GetExtension(fileNode.Filename))
                    this.SceneXmlFilename = Path.Combine(ZipDataNode.Path, fileNode.Filename);

            }

            if (File.Exists(this.SceneXmlFilename))
            {
                this.CreateStatistics();
            }
            else
            {
                this.ValidMapData = false;
            }
        }

        #endregion // Constructor(s)

        #region Object Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return (this.Name);
        }
        #endregion // Object Overrides

        #region Private Function(s)

        /// <summary>
        /// Gets the scene xml object from the scene manager and creates the statistics
        /// from the scene object.
        /// </summary>
        private void CreateStatistics()
        {
            Scene scene = SceneManager.GetScene(this.SceneXmlFilename);

            if (scene != null)
            {
                this.SectionType = DetermineSectionType(scene);
                //this.Statistics = new Map.Statistics.MapStatistics(scene, this);

                switch (this.SectionType)
                {
                    case SectionTypes.MAP:
                        break;
                    case SectionTypes.PROPGROUP:
                        break;
                    case SectionTypes.PROPS:
                        this.SectionStatistics = new PropFile(this);
                        break;
                    case SectionTypes.INTERIORS:
                        break;
                    case SectionTypes.UNKNOWN:
                    default:
                        break;
                }
            }

            scene = null;
        }

        /// <summary>
        /// Gets the scene xml object from the scene manager and creates the statistics
        /// from the scene object.
        /// </summary>
        private void CreateStatistics(LevelReferences references)
        {
            Scene scene = SceneManager.GetScene(this.SceneXmlFilename);

            if (scene != null)
            {
                this.SectionType = DetermineSectionType(scene);

                switch (this.SectionType)
                {
                    case SectionTypes.MAP:
                        this.SectionStatistics = new MapContainer(this, references);
                        break;
                    case SectionTypes.PROPGROUP:
                        this.SectionStatistics = new PropGroup(this, references);
                        break;
                    case SectionTypes.INTERIORS:
                        this.SectionStatistics = new InteriorFile(this, references);
                        break;
                    case SectionTypes.PROPS:
                        this.SectionStatistics = new PropFile(this);
                        break;
                    case SectionTypes.UNKNOWN:
                    default:
                        break;
                }
            }

            scene = null;
        }

        /// <summary>
        /// Gets the scene xml object from the scene manager and creates the statistics
        /// from the scene object.
        /// </summary>
        private void CreateStatistics(LevelReferences references, Scene scene)
        {
            if (scene != null)
            {
                this.SectionType = DetermineSectionType(scene);

                switch (this.SectionType)
                {
                    case SectionTypes.MAP:
                        this.SectionStatistics = new MapContainer(this, references);
                        break;
                    case SectionTypes.PROPGROUP:
                        this.SectionStatistics = new PropGroup(this, references);
                        break;
                    case SectionTypes.INTERIORS:
                        this.SectionStatistics = new InteriorFile(this, references);
                        break;
                    case SectionTypes.PROPS:
                        this.SectionStatistics = new PropFile(this);
                        break;
                    case SectionTypes.UNKNOWN:
                    default:
                        break;
                }
            }

            scene = null;
        }

        /// <summary>
        /// This function is used to create a scene data for this map section given that
        /// this section represents a generic prop file
        /// </summary>
        private Boolean CreateDataFromPropSceneXml(Scene scene, DefinitionList genericDefinitions)
        {
            try
            {
                this.Definitions = new List<IMapDefinition>();
                List<TargetObjectDef> completedObjects = new List<TargetObjectDef>();
                Dictionary<String, List<TargetObjectDef>> animGroups = new Dictionary<String, List<TargetObjectDef>>();
                foreach (TargetObjectDef sceneObject in scene.Objects)
                {
                    if (completedObjects.Contains(sceneObject))
                        continue;
                    if (sceneObject.IsDummy())
                        continue;
                    if (sceneObject.DontExport())
                        continue;

                    if (sceneObject.IsFragment())
                    {
                        MapDefinition definition = MapDefinition.CreateFragmentDefinition(sceneObject, scene);
                        definition.SearchableParent = this;
                        this.Definitions.Add(definition);
                        MapDefinition newDefinition = new MapDefinition(sceneObject, scene);
                        newDefinition.SearchableParent = this;
                        this.Definitions.Add(newDefinition);
                    }
                    else if (sceneObject.IsAnimProxy())
                    {
                        String groupName = sceneObject.Name;
                        groupName = groupName.Split(new String[] { "_anim" }, StringSplitOptions.None)[0];
                        if (!animGroups.ContainsKey(groupName))
                            animGroups.Add(groupName, new List<TargetObjectDef>());

                        animGroups[groupName].Add(sceneObject);
                        completedObjects.Add(sceneObject);
                    }
                    else if (sceneObject.IsStatedAnim())
                    {
                    //    String groupName = sceneObject.Attributes[AttrNames.OBJ_GROUP_NAME] as String;
                    //    if (!animGroups.ContainsKey(groupName))
                    //        animGroups.Add(groupName, new List<ObjectDef>());

                    //    animGroups[groupName].Add(sceneObject);
                    //    if (sceneObject.SkeletonRootObj != null)
                    //    {
                    //        ObjectDef skeleton = scene.FindObject(sceneObject.SkeletonRootObj);
                    //        if (skeleton != null)
                    //        {
                    //            animGroups[groupName].Add(skeleton);
                    //            completedObjects.Add(skeleton);
                    //        }
                    //    }
                    //    completedObjects.Add(sceneObject);
                    }
                    else if (sceneObject.IsObject())
                    {
                        // If it has a drawable lod we need to create a single definition
                        // for all objects in the hierarchy (max. 4 and completely linear)
                        List<TargetObjectDef> hierarchy = new List<TargetObjectDef>();
                        DefinitionLODHierarchy.GetFullDefinitionLodHierarchy(sceneObject, ref hierarchy);
                        foreach (TargetObjectDef hierarchyObject in hierarchy)
                        {
                            completedObjects.Add(hierarchyObject);
                        }
                        MapDefinition definition = new MapDefinition(hierarchy, scene);
                        definition.SearchableParent = this;
                        this.Definitions.Add(definition);
                    }
                }
                foreach (List<TargetObjectDef> group in animGroups.Values)
                {
                    TargetObjectDef start = null;
                    TargetObjectDef end = null;
                    TargetObjectDef proxy = null;
                    List<TargetObjectDef> anims = new List<TargetObjectDef>();
                    foreach (TargetObjectDef groupObject in group)
                    {
                        if (groupObject.Attributes.ContainsKey(AttrNames.OBJ_ANIM_STATE))
                        {
                            String state = groupObject.Attributes[AttrNames.OBJ_ANIM_STATE] as String;

                            if (String.Compare(state, "end", true) == 0)
                            {
                                end = groupObject;
                            }
                            else if (String.Compare(state, "start", true) == 0)
                            {
                                start = groupObject;
                            }
                            else
                            {
                                anims.Add(groupObject);
                            }
                        }
                        else if (groupObject.IsAnimProxy())
                        {
                            proxy = groupObject;
                        }
                        else
                        {
                            anims.Add(groupObject);
                        }
                    }
                    if (start != null && end != null && proxy != null)
                    {
                        MapDefinition definition = new MapDefinition(start, end, anims, proxy, scene);
                        definition.SearchableParent = this;
                        this.Definitions.Add(definition);
                    }
                }

                if (null != genericDefinitions)
                {
                    foreach (MapDefinition definition in this.Definitions)
                    {
                        genericDefinitions.AddDefinition(this.SourceAssetFilename.Normalize(), definition.Name, definition);
                    }
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// This function is used to create a scene data for this map section given that
        /// this section represents a generic interior file
        /// </summary>
        private Boolean CreateDataFromInteriorSceneXml(Scene scene, DefinitionList genericDefinitions)
        {
            try
            {
                this.Definitions = new List<IMapDefinition>();
                Dictionary<String, TargetObjectDef> milos = new Dictionary<String, TargetObjectDef>();
                // Be carefully there can be more than one interior per file
                // find all milo tris.
                foreach (TargetObjectDef sceneObject in scene.Objects)
                {
                    if (sceneObject.DontExport())
                        continue;
                    if (sceneObject.IsDummy())
                        continue;

                    if (sceneObject.IsMilo())
                    {
                        String miloName = sceneObject.Name + "_milo_";
                        milos.Add(miloName, sceneObject);
                    }
                }
                foreach (TargetObjectDef milo in milos.Values)
                {
                    MapMilo definition = new MapMilo(milo, scene, genericDefinitions);
                    this.Definitions.Add(definition);
                    if (null != genericDefinitions)
                        genericDefinitions.AddDefinition(this.SourceAssetFilename.Normalize(), definition.Name, definition);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// This function is used to create a scene data for this map section given that
        /// this section represents a non generic prop group container
        /// </summary>
        private Boolean CreateDataFromPropGroupSceneXml(Scene scene, DefinitionList genericDefinitions)
        {
            try
            {
                this.Instances = new List<MapInstance>();
                this.Definitions = new List<IMapDefinition>();
                Dictionary<TargetObjectDef, MapInstance> completedObjects = new Dictionary<TargetObjectDef, MapInstance>();
                List<TargetObjectDef> internalRefs = new List<TargetObjectDef>();
                foreach (TargetObjectDef sceneObject in scene.Objects)
                {
                    if (completedObjects.ContainsKey(sceneObject))
                        continue;
                    if (sceneObject.DontExport())
                        continue;
                    if (sceneObject.IsDummy())
                        continue;

                    if (sceneObject.IsXRef() || sceneObject.IsRefObject() || sceneObject.IsInternalRef())
                    {
                        if (!sceneObject.IsInternalRef())
                        {
                            MapInstance instance = new MapInstance(sceneObject, scene, genericDefinitions);
                            instance.SearchableParent = this;
                            this.Instances.Add(instance);
                            completedObjects.Add(sceneObject, instance);
                        }
                        else
                        {
                            internalRefs.Add(sceneObject);
                        }
                    }
                    else if (sceneObject.IsObject())
                    {
                        MapDefinition definition = new MapDefinition(sceneObject, scene);
                        definition.SearchableParent = this;
                        this.Definitions.Add(definition);
                        MapInstance instance = new MapInstance(sceneObject, scene, definition);
                        instance.SearchableParent = this;
                        this.Instances.Add(instance);

                        completedObjects.Add(sceneObject, instance);
                    }

                    if (sceneObject.IsContainer())
                    {
                        foreach (TargetObjectDef child in sceneObject.Children)
                        {
                            if (completedObjects.ContainsKey(child))
                                continue;
                            if (sceneObject.DontExport())
                                continue;
                            if (sceneObject.IsDummy())
                                continue;

                            if (child.IsXRef() || child.IsRefObject() || child.IsInternalRef())
                            {
                                if (!child.IsInternalRef())
                                {
                                    MapInstance instance = new MapInstance(child, scene, genericDefinitions);
                                    instance.SearchableParent = this;
                                    this.Instances.Add(instance);
                                    completedObjects.Add(child, instance);
                                }
                                else
                                {
                                    internalRefs.Add(child);
                                }
                            }
                            else if (child.IsObject())
                            {
                                MapDefinition definition = new MapDefinition(child, scene);
                                definition.SearchableParent = this;
                                this.Definitions.Add(definition);
                                MapInstance instance = new MapInstance(child, scene, definition);
                                instance.SearchableParent = this;
                                this.Instances.Add(instance);

                                completedObjects.Add(child, instance);
                            }
                        }
                    }
                }
                foreach (TargetObjectDef internalRef in internalRefs)
                {
                    foreach (IMapDefinition definition in this.Definitions)
                    {
                        if (definition.Name == internalRef.RefName)
                        {
                            MapInstance instance = new MapInstance(internalRef, scene, definition);
                            instance.SearchableParent = this;
                            this.Instances.Add(instance);
                            completedObjects.Add(internalRef, instance);
                            break;
                        }
                    }
                }
                foreach (var instance in completedObjects)
                {
                    instance.Value.ResolveLodHierarchy(instance.Key, scene, this.Instances);
                }
                foreach (MapDefinition definition in this.Definitions)
                {
                    genericDefinitions.AddDefinition(this.SourceAssetFilename.Normalize(), definition.Name, definition);
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// This function is used to create a scene data for this map section given that
        /// this section represents a non generic map container
        /// </summary>
        private Boolean CreateDataFromMapSceneXml(Scene scene, DefinitionList genericDefinitions)
        {
            try
            {
                this.Instances = new List<MapInstance>();
                this.Definitions = new List<IMapDefinition>();
                Dictionary<TargetObjectDef, MapInstance> completedObjects = new Dictionary<TargetObjectDef, MapInstance>();
                List<TargetObjectDef> internalRefs = new List<TargetObjectDef>();
                BoundingBox3f surroundingBox = new BoundingBox3f();
                foreach (TargetObjectDef sceneObject in scene.Objects)
                {
                    if (completedObjects.ContainsKey(sceneObject))
                        continue;
                    if (sceneObject.DontExport())
                        continue;
                    if (sceneObject.IsDummy())
                        continue;

                    if (sceneObject.IsXRef() || sceneObject.IsRefObject() || sceneObject.IsInternalRef())
                    {
                        if (!sceneObject.IsInternalRef())
                        {
                            MapInstance instance = new MapInstance(sceneObject, scene, genericDefinitions);
                            instance.SearchableParent = this;
                            this.Instances.Add(instance);
                            if (instance.ResolvedDefinition != null && instance.ResolvedDefinition.LocalBoundingBox != null)
                            {
                                surroundingBox.Expand(instance.ResolvedDefinition.LocalBoundingBox);
                            }
                            completedObjects.Add(sceneObject, instance);
                        }
                        else
                        {
                            internalRefs.Add(sceneObject);
                        }
                    }
                    else if (sceneObject.IsObject())
                    {
                        MapDefinition definition = new MapDefinition(sceneObject, scene);
                        definition.SearchableParent = this;
                        this.Definitions.Add(definition);
                        surroundingBox.Expand(definition.LocalBoundingBox);
                        MapInstance instance = new MapInstance(sceneObject, scene, definition);
                        instance.SearchableParent = this;
                        this.Instances.Add(instance);

                        completedObjects.Add(sceneObject, instance);    
                    }
                    if (sceneObject.IsContainer())
                    {
                        foreach (TargetObjectDef child in sceneObject.Children)
                        {
                            if (completedObjects.ContainsKey(child))
                                continue;
                            if (child.DontExport())
                                continue;
                            if (child.IsDummy())
                                continue;

                            if (child.IsXRef() || child.IsRefObject() || child.IsInternalRef())
                            {
                                if (!child.IsInternalRef())
                                {
                                    MapInstance instance = new MapInstance(child, scene, genericDefinitions);
                                    instance.SearchableParent = this;
                                    this.Instances.Add(instance);
                                    if (instance.ResolvedDefinition != null && instance.ResolvedDefinition.LocalBoundingBox != null)
                                    {
                                        surroundingBox.Expand(instance.ResolvedDefinition.LocalBoundingBox);
                                    }
                                    completedObjects.Add(child, instance);
                                }
                                else
                                {
                                    internalRefs.Add(child);
                                }
                            }
                            else if (child.IsObject())
                            {
                                MapDefinition definition = new MapDefinition(child, scene);
                                definition.SearchableParent = this;
                                this.Definitions.Add(definition);

                                if (!child.DontExportIPL())
                                {
                                    MapInstance instance = new MapInstance(child, scene, definition);
                                    instance.SearchableParent = this;
                                    surroundingBox.Expand(instance.WorldBoundingBox);
                                    this.Instances.Add(instance);
                                    completedObjects.Add(child, instance);
                                }
                            }
                        }
                    }
                }
                foreach (TargetObjectDef internalRef in internalRefs)
                {
                    foreach (IMapDefinition definition in this.Definitions)
                    {
                        if (definition.Name == internalRef.RefName)
                        {
                            MapInstance instance = new MapInstance(internalRef, scene, definition);
                            instance.SearchableParent = this;
                            surroundingBox.Expand(instance.WorldBoundingBox);
                            this.Instances.Add(instance);

                            completedObjects.Add(internalRef, instance);
                            break;
                        }
                    }
                }

                this.TextureDictionaries = new TextureDictionarySet(this.Definitions, this);
                foreach (var instance in completedObjects)
                {
                    instance.Value.ResolveLodHierarchy(instance.Key, scene, this.Instances);
                }
                foreach (MapDefinition definition in this.Definitions)
                {
                    genericDefinitions.AddDefinition(this.SourceAssetFilename.Normalize(), definition.Name, definition);
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Uses the given bounding box to create and
        /// returns the inverse area for it. (Default return is 0.0f)
        /// </summary>
        private float GetInverseAreaFromBoundingBox(RSG.Base.Math.BoundingBox3f surroundingBox)
        {
            float sectionWidth = surroundingBox.Max.X - surroundingBox.Min.X;
            float sectionLength = surroundingBox.Max.Y - surroundingBox.Min.Y;
            float sectionHeight = surroundingBox.Max.Z - surroundingBox.Min.Z;
            if (sectionWidth > 0.1f && sectionLength > 0.1f && sectionHeight > 0.1f)
            {
                return 1.0f / (sectionWidth * sectionLength * sectionHeight);
            }

            return 0.0f;
        }

        /// <summary>
        /// Fills the given dictionary with unique references and also calculates the 
        /// bounding box that contains all the instances and the inverse area for that box,
        /// this return true iff there is at least once instance found
        /// </summary>
        private Boolean GetUniqueInstances(ref Dictionary<String, MapInstance> uniqueInstances, ref float inverseArea)
        {
            RSG.Base.Math.BoundingBox3f surroundingBox = new RSG.Base.Math.BoundingBox3f();
            if (this.Instances != null && this.Instances.Count > 0)
            {
                foreach (MapInstance instance in this.Instances)
                {
                    if (instance != null && instance.ResolvedDefinition != null)
                    {
                        if (instance.WorldBoundingBox != null)
                            surroundingBox.Expand(instance.WorldBoundingBox);

                        if (!uniqueInstances.ContainsKey(instance.ResolvedDefinition.Name))
                        {
                            uniqueInstances.Add(instance.ResolvedDefinition.Name, instance);
                        }
                    }
                }
                inverseArea = GetInverseAreaFromBoundingBox(surroundingBox);
                return true;
            }

            return false;
        }

        #endregion // Private Function(s)

        #region Public Function(s)

        /// <summary>
        /// Determines the type of map section the statistics represents
        /// </summary>
        /// <param name="scene"></param>
        public SectionTypes DetermineSectionType(Scene scene)
        {
            this.SectionType = SectionTypes.UNKNOWN;
            if (this.ExportInstances == true)
            {
                if (this.ExportDefinitions == true)
                {
                    this.SectionType = SectionTypes.MAP;
                }
                else
                {
                    this.SectionType = SectionTypes.PROPGROUP;
                }
            }
            else
            {
                if (this.ExportDefinitions == true)
                {
                    this.SectionType = SectionTypes.PROPS;
                    if (scene.Objects != null)
                    {
                        foreach (TargetObjectDef sceneObject in scene.Objects)
                        {
                            if (sceneObject != null && sceneObject.IsMilo())
                            {
                                this.SectionType = SectionTypes.INTERIORS;
                                break;
                            }
                        }
                    }
                }
            }
            return this.SectionType;
        }

        public Boolean InitialisedInfo { get; set; }
        public Boolean InitialisedStatistics { get; set; }

        public Boolean InitialiseSection(Boolean createInfo, Boolean createStatistics)
        {
            if (createInfo == true)
            {
                if (this.InitialisedInfo == false)
                {
                    InitialisedInfo = true;
                    String path = this.ExportPath;
                    String filename = null;
                    foreach (ContentNode node in this.ZipDataNode.Children)
                    {
                        if (!(node is ContentNodeFile))
                            continue;

                        ContentNodeFile fileNode = (node as ContentNodeFile);
                        String extenstion = fileNode.Extension;
                        if (filename == null)
                        {
                            filename = Path.GetFileNameWithoutExtension(fileNode.Filename);
                        }
                        if ("ide" == extenstion)
                        {
                            this.IdeFilename = Path.Combine(path, filename + ".ide");
                        }
                        else if ("ipl" == extenstion)
                        {
                            this.IplFilename = Path.Combine(path, filename + ".ipl");
                        }
                        else if ("zip" == extenstion)
                        {
                            this.ZipFilename = Path.Combine(path, filename + ".zip");
                        }
                        else if ("xml" == extenstion)
                        {
                            this.SceneXmlFilename = Path.Combine(path, filename + ".xml");
                        }
                    }
                }

                if (!File.Exists(this.SceneXmlFilename))
                {
                    return false;
                }
            }
            if (createStatistics == true)
            {
                InitialisedStatistics = true;
                this.CreateStatistics();
            }

            return true;
        }

        /// <summary>
        /// Initialises the map section and uses the given references to resolve the references on the way through
        /// and add any more.
        /// </summary>
        public Boolean InitialiseSection(Boolean createInfo, Boolean createStatistics, LevelReferences references)
        {
            if (createInfo == true)
            {
                InitialisedInfo = true;
                String path = this.ExportPath;
                String filename = null;
                foreach (ContentNode node in this.ZipDataNode.Children)
                {
                    if (!(node is ContentNodeFile))
                        continue;

                    ContentNodeFile fileNode = (node as ContentNodeFile);
                    String extenstion = fileNode.Extension;
                    if (filename == null)
                    {
                        filename = Path.GetFileNameWithoutExtension(fileNode.Filename);
                    }
                    if ("ide" == extenstion)
                    {
                        this.IdeFilename = Path.Combine(path, filename + ".ide");
                    }
                    else if ("ipl" == extenstion)
                    {
                        this.IplFilename = Path.Combine(path, filename + ".ipl");
                    }
                    else if ("zip" == extenstion)
                    {
                        this.ZipFilename = Path.Combine(path, filename + ".zip");
                    }
                    else if ("xml" == extenstion)
                    {
                        this.SceneXmlFilename = Path.Combine(path, filename + ".xml");
                    }
                }

                if (!File.Exists(this.SceneXmlFilename))
                {
                    return false;
                }
            }
            if (createStatistics == true)
            {
                InitialisedStatistics = true;
                this.CreateStatistics(references);
            }

            return true;
        }

        /// <summary>
        /// Retrieves the export path and uses it to create
        /// the xml, zip, ipl, ide paths. This checks to see if
        /// the scene xml file exists on the clients workspace
        /// and if it doesn't returns false else returns true.
        /// </summary>
        public Boolean CreateExportPaths()
        {
            String path = this.ExportPath;
            String filename = null;
            foreach (ContentNode node in this.ZipDataNode.Children)
            {
                if (!(node is ContentNodeFile))
                    continue;

                ContentNodeFile fileNode = (node as ContentNodeFile);
                String extenstion = fileNode.Extension;
                if (filename == null)
                {
                    filename = Path.GetFileNameWithoutExtension(fileNode.Filename);
                }
                if ("ide" == extenstion)
                {
                    this.IdeFilename = Path.Combine(path, filename + ".ide");
                }
                else if ("ipl" == extenstion)
                {
                    this.IplFilename = Path.Combine(path, filename + ".ipl");
                }
                else if ("zip" == extenstion)
                {
                    this.ZipFilename = Path.Combine(path, filename + ".zip");
                }
                else if ("xml" == extenstion)
                {
                    this.SceneXmlFilename = Path.Combine(path, filename + ".xml");
                }
            }

            if (!File.Exists(this.SceneXmlFilename))
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Creates all the data (export paths, statistics, and children)
        /// using the data in the scene xml, this also makes sure that any definitions
        /// found are added to the given list and all definitions needed are got from
        /// the given list.
        /// </summary>
        public Boolean CreateDataFromSceneXml(DefinitionList genericDefintions)
        {
            if (String.IsNullOrEmpty(this.SceneXmlFilename))
            {
                if (!this.CreateExportPaths())
                {
                    return false;
                }
            }

            Scene scene = SceneManager.GetScene(this.SceneXmlFilename);
            if (scene != null)
            {
                this.DetermineSectionType(scene);

                switch (this.SectionType)
                {
                    case SectionTypes.MAP:
                        CreateDataFromMapSceneXml(scene, genericDefintions);
                        break;
                    case SectionTypes.PROPGROUP:
                        CreateDataFromPropGroupSceneXml(scene, genericDefintions);
                        break;
                    case SectionTypes.INTERIORS:
                        CreateDataFromInteriorSceneXml(scene, genericDefintions);
                        break;
                    case SectionTypes.PROPS:
                        CreateDataFromPropSceneXml(scene, genericDefintions);
                        break;
                    case SectionTypes.UNKNOWN:
                    default:
                        break;
                }
            }

            return true;
        }

        #endregion // Public Functions(s)

        #region Walk Properties

        /// <summary>
        /// Walks through the children of ISearchables, going
        /// depth first
        /// </summary>
        public IEnumerable<ISearchable> WalkSearchDepth
        {
            get
            {
                yield return this;
                if (this.Definitions != null)
                {
                    List<IMapDefinition> definitions = new List<IMapDefinition>(this.Definitions);
                    foreach (ISearchable searchable in definitions.Where(d => d is ISearchable == true))
                    {
                        foreach (ISearchable searchableChld in searchable.WalkSearchDepth)
                        {
                            yield return searchableChld;
                        }
                    }
                }
                if (this.Instances != null)
                {
                    List<MapInstance> instances = new List<MapInstance>(this.Instances);
                    foreach (ISearchable searchable in instances.Where(i => i is ISearchable == true))
                    {
                        foreach (ISearchable searchableChld in searchable.WalkSearchDepth)
                        {
                            yield return searchableChld;
                        }
                    }
                }
                if (this.TextureDictionaries != null)
                {
                    foreach (ISearchable searchableChld in this.TextureDictionaries.WalkSearchDepth)
                    {
                        yield return searchableChld;
                    }
                }
            }
        }

        #endregion // Walk Properties

        #region Obsolete
        
        //        /// <summary>
        //        /// Map section independent data source path.
        //        /// </summary> 
        //        [Obsolete]
        //        public String IndependentPath
        //        {
        //            get
        //            {
        //#if DEBUG
        //                return this.MapDataNode.Path;
        //#else
        //                return m_sIndpendentPath; 
        //#endif // DEBUG
        //            }
        //            protected set { m_sIndpendentPath = value; }
        //        }
        //        private String m_sIndpendentPath;

        //        /// <summary>
        //        /// Array of absolute independent filenames for this Map Section.
        //        /// </summary>
        //        [Obsolete]
        //        public String[] IndependentFiles
        //        {
        //            get
        //            {
        //                List<String> files = new List<String>();
        //                if (!String.IsNullOrEmpty(this.IdeFilename))
        //                    files.Add(this.IdeFilename);
        //                if (!String.IsNullOrEmpty(this.IplFilename))
        //                    files.Add(this.IplFilename);
        //                if (!String.IsNullOrEmpty(this.RpfFilename))
        //                    files.Add(this.RpfFilename);
        //                return (files.ToArray());
        //            }
        //        }

        //        /// <summary>
        //        /// 
        //        /// </summary>
        //        [Obsolete]
        //        public String RpfFilename
        //        {
        //            get;
        //            private set;
        //        }

        //        /// <summary>
        //        /// Indpendent data content node.  Not public because this may change 
        //        /// in a future version of the model.
        //        /// </summary>
        //        [Obsolete]
        //        private RSG.Base.ConfigParser.ContentNodeMapRpf MapDataNode;

        //        /// <summary>
        //        /// The statistics that belong to this map section
        //        /// </summary>
        //        [Obsolete]
        //        public Statistics.MapStatistics Statistics
        //        {
        //            get { return m_statistics; }
        //            set { m_statistics = value; }
        //        }
        //        private Statistics.MapStatistics m_statistics;

        //        /// <summary>
        //        /// 
        //        /// </summary>
        //        /// <param name="container"></param>
        //        public MapSection(Level level, IMapContainer container, ContentNodeMap map, ContentNodeMapRpf maprpf)
        //        {
        //            this.Level = level;
        //            this.MapNode = map;
        //            this.MapDataNode = maprpf;
        //            this.Container = container;
        //            this.ModelUserData = new UserData();

        //#if !DEBUG
        //            this.IndependentPath = this.MapDataNode.Path;
        //#endif // !_DEBUG
        //        }

        #endregion // Obsolete
    } // MapSection

    /// <summary>
    /// 
    /// </summary>
    public class MapSectionEventArgs : EventArgs
    {
        #region Properties and Associated Member ExportData
        /// <summary>
        /// 
        /// </summary>
        public MapSection AssociatedSection
        {
            get { return m_AssociatedSection; }
            private set { m_AssociatedSection = value; }
        }
        private MapSection m_AssociatedSection;
        #endregion // Properties and Associated Member ExportData

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="prop"></param>
        public MapSectionEventArgs(MapSection section)
        {
            this.AssociatedSection = section;
        }
        #endregion // Constructor(s)
    }

} // RSG.Model.Map namespace
