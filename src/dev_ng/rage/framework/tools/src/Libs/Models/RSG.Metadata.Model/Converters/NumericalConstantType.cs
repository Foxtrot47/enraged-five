﻿//---------------------------------------------------------------------------------------------
// <copyright file="NumericalConstantType.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Converters
{
    /// <summary>
    /// Defines all of the different types a numerical constant can be.
    /// </summary>
    public enum NumericalConstantType
    {
        /// <summary>
        /// The equivalent to any type that is not known to the parser.
        /// </summary>
        Unknown,

        /// <summary>
        /// The equivalent to System.Byte type.
        /// </summary>
        Byte,

        /// <summary>
        /// The equivalent to System.Double type.
        /// </summary>
        Double,

        /// <summary>
        /// The equivalent to System.Decimal type.
        /// </summary>
        Decimal,

        /// <summary>
        /// The equivalent to System.Single type.
        /// </summary>
        Float,

        /// <summary>
        /// The equivalent to System.Int64 type.
        /// </summary>
        Long,

        /// <summary>
        /// The equivalent to System.UInt64 type.
        /// </summary>
        UnsignedLong,

        /// <summary>
        /// The equivalent to System.Int32 type.
        /// </summary>
        Int,

        /// <summary>
        /// The equivalent to System.UInt32 type.
        /// </summary>
        UnsignedInt,

        /// <summary>
        /// The equivalent to System.Int16 type.
        /// </summary>
        Short,

        /// <summary>
        /// The equivalent to System.UInt16 type.
        /// </summary>
        UnsignedShort,

        /// <summary>
        /// The equivalent to System.SByte type.
        /// </summary>
        SignedByte
    } // RSG.Metadata.Model.Converters.NumericalConstantType
} // RSG.Metadata.Model.Converters {Namespace}
