﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Map;
using System.IO;
using RSG.Model.Common;

namespace RSG.Model.Report.Reports
{
    /// <summary>
    /// 
    /// </summary>
    public class TextureComparisonCSVReport : CSVReport, IDynamicSectionReport, IReportFileProvider
    {
        #region Constants
        private const String NAME = "Texture Comparison CSV Report";
        private const String DESCRIPTION = "Exports a texture comparison into a .csv file";
        #endregion // Constants

        #region Construtcor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public TextureComparisonCSVReport()
            : base(NAME, DESCRIPTION)
        {
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Generate the report dynamically.
        /// </summary>
        /// <param name="selectedSections">Sections to be used by the report</param>
        public void Generate(List<MapSection> sections)
        {
            using (StreamWriter sw = new StreamWriter(Filename))
            {
                string header = "Texture Name, ";
                foreach (MapSection section in sections)
                    header += string.Format("{0}, ", section.Name);
                sw.WriteLine(header.TrimEnd(new char[] { ',', ' ' }));

                var textures = new Dictionary<string, List<int>>();
                int index = 0;
                foreach (MapSection section in sections)
                {
                    foreach (IAsset asset in section.AssetChildren)
                    {
                        if (asset is MapInstance && (asset as MapInstance).ReferencedDefinition != null)
                        {
                            MapDefinition definition = (asset as MapInstance).ReferencedDefinition;
                            foreach (IAsset childAsset in definition.AssetChildren)
                            {
                                if (!(childAsset is ITexture))
                                    continue;

                                ITexture texture = childAsset as ITexture;
                                if (!textures.ContainsKey(texture.Name.ToLower()))
                                {
                                    textures.Add(texture.Name.ToLower(), new List<int>(sections.Count));
                                    for (int i = 0; i < sections.Count; i++)
                                        textures[texture.Name.ToLower()].Add(0);
                                }
                                textures[texture.Name.ToLower()][index]++;
                            }
                        }
                    }
                    index++;
                }
                foreach (KeyValuePair<string, List<int>> texture in textures)
                {
                    string textureLine = string.Format("{0}, ", texture.Key);
                    foreach (int count in texture.Value)
                        textureLine += string.Format("{0}, ", count.ToString());

                    sw.WriteLine(textureLine.TrimEnd(new char[] { ',', ' ' }));
                }
            }
        }
        #endregion // Controller Methods
    } // ReferenceComparisionReport
} // RSG.Model.Report.Reports
