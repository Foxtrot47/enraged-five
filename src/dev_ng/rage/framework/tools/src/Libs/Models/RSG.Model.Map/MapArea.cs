﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Diagnostics;
using RSG.Base.ConfigParser;
using RSG.Base.Collections;
using RSG.Model.Map.Utilities;

namespace RSG.Model.Map
{

    /// <summary>
    /// Game map area object model; containing a set of IMapComponents.
    /// </summary>
    public class MapArea : IMapContainer, ISearchable
    {        
        #region Published Events
        /// <summary>
        /// Event raised when attached user-data object is changed.
        /// </summary>
        public event EventHandler UserDataChanged;
        #endregion // Published Events

        #region Properties and Associated Member Data

        /// <summary>
        /// Reference to the owner Level object.
        /// </summary>
        public Level Level
        {
            get;
            private set;
        }

        /// <summary>
        /// Map area name.
        /// </summary>
        public String Name
        {
            get { return this.GroupNode.RawRelativePath; }
        }

        /// <summary>
        /// Dictionary of MapComponent objects for this level.
        /// </summary>
        public Dictionary<String, IMapComponent> MapComponents
        {
            get;
            protected set;
        }

        /// <summary>
        /// Indexer.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public IMapComponent this[String componentName]
        {
            get
            {
                Debug.Assert(this.MapComponents.ContainsKey(componentName),
                    String.Format("Map component with name {0} not found.", componentName));
                return (this.MapComponents[componentName.ToLower()]);
            }
        }

        /// <summary>
        /// Reference to user-defined data (application specific level data).
        /// </summary>
        public Object UserData
        {
            get { return m_UserData; }
            set
            {
                m_UserData = value;
                if (null != UserDataChanged)
                    UserDataChanged(this, EventArgs.Empty);
            }
        }
        private Object m_UserData;

        /// <summary>
        /// The model user data for this map section
        /// </summary>
        public UserData ModelUserData
        {
            get { return m_modelUserData; }
            set { m_modelUserData = value; }
        }
        private UserData m_modelUserData = new UserData();

        public IEnumerable<IMapComponent> ComponentChildren
        {
            get
            {
                foreach (IMapComponent component in this.MapComponents.Values)
                {
                    if ((component is IMapComponent == true) && (component is IMapContainer == false))
                    {
                        yield return component;
                    }
                }
            }
        }

        public IEnumerable<IMapContainer> ContainerChildren
        {
            get
            {
                foreach (IMapComponent container in this.MapComponents.Values)
                {
                    if ((container is IMapContainer == true))
                    {
                        yield return container as IMapContainer;
                    }
                }
            }
        }

        /// <summary>
        /// The parent of this searchable object
        /// </summary>
        public ISearchable SearchableParent
        {
            get;
            private set;
        }

        #endregion // Properties and Associated Member Data

        #region Private Member Data
        /// <summary>
        /// 
        /// </summary>
        private RSG.Base.ConfigParser.ContentNodeGroup GroupNode;
        #endregion // Private Member Data

        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="cm"></param>
        public MapArea(Level level, ContentNodeGroup group, IMapContainer container)
        {
            this.Level = level;
            this.GroupNode = group;
            this.MapComponents = new Dictionary<String, IMapComponent>();
            this.SearchableParent = container;
        }

        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Return true iff level contains a MapComponent with specified name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool ContainsMapComponent(String name)
        {
            return (this.MapComponents.ContainsKey(name.ToLower()));
        }
        #endregion // Controller Methods

        #region IEnumerable<IMapContainer>
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerator<IMapComponent> IEnumerable<IMapComponent>.GetEnumerator()
        {
            foreach (IMapComponent l in this.MapComponents.Values)
                yield return l;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerator)this);
        }
        #endregion // IEnumerable<IMapContainer>

        #region Walk Properties

        /// <summary>
        /// Walks through the children of ISearchables, going
        /// depth first
        /// </summary>
        public IEnumerable<ISearchable> WalkSearchDepth
        {
            get
            {
                yield return this;
                foreach (ISearchable searchable in this.MapComponents.Values.Where(c => c is ISearchable == true))
                {
                    foreach (ISearchable searchableChld in searchable.WalkSearchDepth)
                    {
                        yield return searchableChld;
                    }
                }
            }
        }

        #endregion // Walk Properties
    }

    /// <summary>
    /// 
    /// </summary>
    public class AreaEventArgs : EventArgs
    {
        #region Properties and Associated Member ExportData
        /// <summary>
        /// 
        /// </summary>
        public MapArea AssociatedArea
        {
            get;
            private set;
        }
        #endregion // Properties and Associated Member ExportData

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="prop"></param>
        public AreaEventArgs(MapArea area)
        {
            this.AssociatedArea = area;
        }
        #endregion // Constructor(s)
    }

} // RSG.Model.Map namespace
