﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.ConfigParser;

namespace RSG.Model.Report
{
    /// <summary>
    /// A report that generates it's content on the fly
    /// </summary>
    public interface IDynamicReport : IReport
    {
        /// <summary>
        /// Generate the report dynamically.
        /// </summary>
        void Generate(ConfigGameView gv);
    } // IDynamicReport
} // RSG.Model.Report namespace
