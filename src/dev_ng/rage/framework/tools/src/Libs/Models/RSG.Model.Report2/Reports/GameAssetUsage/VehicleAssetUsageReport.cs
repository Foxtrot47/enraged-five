﻿namespace RSG.Model.Report2.Reports.GameAssetUsage
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using RSG.Base.Tasks;
    using RSG.ManagedRage;
    using RSG.Metadata.Model;
    using RSG.Model.Common;
    using RSG.Model.Report;
    using RSG.Platform;
    using RSG.Model.Vehicle.Game;
    using RSG.Platform.Attributes;
    using RSG.Base.Configuration;
    using System.Web.UI;

    /// <summary>
    /// The report that provides details of how the vehicle assets are being used in the game
    /// including lists that detail assets that are not referenced or cannot be resolved.
    /// </summary>
    public class VehicleAssetUsageReport
        : RSG.Model.Report.Report, IReportFileProvider, IDynamicLevelReport
    {
        #region Fields
        /// <summary>
        /// The name of this report.
        /// </summary>
        private const string ReportName = "Vehicle Asset Usage";

        /// <summary>
        /// The description for this report.
        /// </summary>
        private const string ReportDescription = "Provides lists detailing the vehicle asset usage in the game.";

        /// <summary>
        /// The private field used for the <see cref="GenerationTask"/> property.
        /// </summary>
        private ITask _generationTask;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="VehicleAssetUsageReport"/> class.
        /// </summary>
        public VehicleAssetUsageReport()
        {
            this.Name = ReportName;
            this.Description = ReportDescription;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Filter used for the save dialog
        /// </summary>
        public string Filter
        {
            get
            {
                return "HTML (Html File)|*.html";
            }
        }

        /// <summary>
        /// Extensions that this report support
        /// </summary>
        public string SupportedExtension
        {
            get
            {
                return ".html";
            }
        }

        /// <summary>
        /// Filename specifying where a report should be output
        /// </summary>
        public string Filename
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the task to run for generating this report.
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (this._generationTask == null)
                {
                    this._generationTask = new ActionTask(
                        "Generating report", this.GenerateReport);
                }

                return this._generationTask;
            }
        }

        public IEnumerable<StreamableStat> RequiredStats
        {
            get { return Enumerable.Empty<StreamableStat>(); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Generates the report dynamically.
        /// </summary>
        /// <param name="context">
        /// The context for this asynchronous task which includes the cancellation token.
        /// </param>
        /// <param name="progress">
        /// The progress object to use to report the progress of the generation.
        /// </param>
        private void GenerateReport(ITaskContext context, IProgress<TaskProgress> progress)
        {
            DynamicLevelReportContext dynamicContext = context as DynamicLevelReportContext;
            if (dynamicContext == null)
            {
                return;
            }

            IBranch branch = dynamicContext.Branch;
            ILevel level = dynamicContext.Level;
            StructureDictionary structures = new StructureDictionary();
            structures.Load(branch, Path.Combine(branch.Metadata, "definitions"));
            GameVehicles vehicles = new GameVehicles(branch, level, structures);
            PlatformEntries platformEntires = new PlatformEntries();
            foreach (Platform platform in dynamicContext.Platforms)
            {
                platformEntires.Add(platform, this.GetEntries(platform, branch, level));
            }

            MemoryStream stream = new MemoryStream();
            StreamWriter streamWriter = new StreamWriter(stream);
            HtmlTextWriter htmlWriter = new HtmlTextWriter(streamWriter);

            htmlWriter.RenderBeginTag(HtmlTextWriterTag.Html);
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.Head);
            htmlWriter.RenderEndTag();
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.Body);

            htmlWriter.RenderBeginTag(HtmlTextWriterTag.H1);
            htmlWriter.Write("Vehicle Asset Usage");
            htmlWriter.RenderEndTag();

            htmlWriter.AddStyleAttribute(HtmlTextWriterStyle.MarginBottom, "0");
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.H2);
            htmlWriter.Write("Not Referenced Assets");
            htmlWriter.RenderEndTag();

            htmlWriter.AddStyleAttribute(HtmlTextWriterStyle.FontSize, "small");
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.I);
            htmlWriter.Write("These are the assets located inside the vehicle rpf files that have not been referenced inside the vehicle metadata.");
            htmlWriter.RenderEndTag();

            string notReferencedHeader = "{0} Not Referenced Assets: {1} found";
            foreach (KeyValuePair<Platform, VehicleRpfEntries> kvp in platformEntires)
            {
                VehicleRpfEntries entries = kvp.Value;
                List<string> notReferenced = this.NotReferenced(kvp.Key, vehicles, entries);

                string platformName = kvp.Key.PlatformToFriendlyName();
                htmlWriter.RenderBeginTag(HtmlTextWriterTag.H3);
                htmlWriter.Write(notReferencedHeader, platformName, notReferenced.Count);
                htmlWriter.RenderEndTag();

                foreach (string notReferencedAsset in notReferenced)
                {
                    htmlWriter.Write(notReferencedAsset);
                    htmlWriter.Write("<br />");
                }
            }

            htmlWriter.AddStyleAttribute(HtmlTextWriterStyle.MarginBottom, "0");
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.H2);
            htmlWriter.Write("Missing Assets");
            htmlWriter.RenderEndTag();

            htmlWriter.AddStyleAttribute(HtmlTextWriterStyle.FontSize, "small");
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.I);
            htmlWriter.Write("These are the definitions contained within the vehicle metadata files whose associated assets cannot be found in the build rpf files.");
            htmlWriter.RenderEndTag();

            string notFoundHeader = "{0} Missing Assets: {1} found";
            foreach (KeyValuePair<Platform, VehicleRpfEntries> kvp in platformEntires)
            {
                VehicleRpfEntries entries = kvp.Value;
                List<string> notFound = this.NotFound(kvp.Key, vehicles, entries);

                string platformName = kvp.Key.PlatformToFriendlyName();
                htmlWriter.RenderBeginTag(HtmlTextWriterTag.H3);
                htmlWriter.Write(notFoundHeader, platformName, notFound.Count);
                htmlWriter.RenderEndTag();

                foreach (string missingAsset in notFound)
                {
                    htmlWriter.Write(missingAsset);
                    htmlWriter.Write("<br />");
                }
            }

            htmlWriter.AddStyleAttribute(HtmlTextWriterStyle.MarginBottom, "0");
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.H2);
            htmlWriter.Write("Not Referenced Vehicle Variations");
            htmlWriter.RenderEndTag();

            htmlWriter.AddStyleAttribute(HtmlTextWriterStyle.FontSize, "small");
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.I);
            htmlWriter.Write("These are the CVehicleVariationData structures that have been defined within the carvariation metadata file but aren't referenced within the CVehicleInfo list inside the vehicles metadata file.");
            htmlWriter.RenderEndTag();

            List<string> unreferencedVariations = this.UnreferencedVariations(vehicles);
            htmlWriter.AddStyleAttribute(HtmlTextWriterStyle.MarginBottom, "0");
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.H3);
            htmlWriter.Write("{0} found", unreferencedVariations.Count);
            htmlWriter.RenderEndTag();
            foreach (string variation in unreferencedVariations)
            {
                htmlWriter.Write("{0}", variation);
                htmlWriter.Write("<br />");
            }

            htmlWriter.AddStyleAttribute(HtmlTextWriterStyle.MarginBottom, "0");
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.H2);
            htmlWriter.Write("Not Referenced Vehicle Modification Kits");
            htmlWriter.RenderEndTag();

            htmlWriter.AddStyleAttribute(HtmlTextWriterStyle.FontSize, "small");
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.I);
            htmlWriter.Write("These are the CVehicleKit structures that have been defined within the carcols metadata file but aren't referenced within any of the definied CVehicleVariationData items inside the carvariations metadata file.");
            htmlWriter.RenderEndTag();

            List<string> unreferencedModKits = this.UnreferencedModKits(vehicles);
            htmlWriter.AddStyleAttribute(HtmlTextWriterStyle.MarginBottom, "0");
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.H3);
            htmlWriter.Write("{0} found", unreferencedModKits.Count);
            htmlWriter.RenderEndTag();
            foreach (string kit in unreferencedModKits)
            {
                htmlWriter.Write("{0}", kit);
                htmlWriter.Write("<br />");
            }

            htmlWriter.RenderEndTag(); // Body
            htmlWriter.Flush();

            stream.Position = 0;
            string outputFile = Path.ChangeExtension(this.Filename, "html");
            using (Stream fileStream = new FileStream(outputFile, FileMode.Create, FileAccess.Write))
            {
                stream.CopyTo(fileStream);
            }
        }

        private VehicleRpfEntries GetEntries(Platform platform, IBranch branch, ILevel level)
        {
            ITarget target = branch.Targets[platform];
            string dir = target.ResourcePath;
            string levelDirectory = Path.Combine(dir, "levels", level.Name);
            string modsDirectory = Path.Combine(dir, "levels", level.Name, "vehiclemods");

            VehicleRpfEntries vehicleEntries = new VehicleRpfEntries();

            vehicleEntries.VehicleEntires = new RpfEntries();
            this.AddEntries(levelDirectory, "vehicles.rpf", vehicleEntries.VehicleEntires);

            vehicleEntries.ModEntires = new RpfEntries();
            this.AddEntriesWithPattern(modsDirectory, "*_mods.rpf", vehicleEntries.ModEntires);

            return vehicleEntries;
        }

        private void AddEntriesWithPattern(string directory, string pattern, RpfEntries container)
        {
            if (!Directory.Exists(directory))
            {
                return;
            }

            SearchOption option = SearchOption.TopDirectoryOnly;
            string[] files = Directory.GetFiles(directory, pattern, option);
            foreach (string path in files)
            {
                this.AddEntries(path, container);
            }
        }

        private void AddEntries(string directory, string filename, RpfEntries container)
        {
            container.Add(filename, this.GetEntries(directory, filename));
        }

        private void AddEntries(string path, Dictionary<string, List<string>> container)
        {
            string filename = Path.GetFileNameWithoutExtension(path);
            container.Add(filename, this.GetEntries(path));
        }

        private List<string> GetEntries(string directory, string filename)
        {
            string path = Path.Combine(directory, filename);
            return this.GetEntries(path);
        }

        private List<string> GetEntries(string path)
        {
            if (!File.Exists(path))
            {
                return new List<string>();
            }

            Packfile packFile = new Packfile();
            packFile.Load(path);
            List<string> entries = new List<string>();
            foreach (PackEntry entry in packFile.Entries)
            {
                string name = entry.Name;
                if (!String.IsNullOrEmpty(entry.Path))
                {
                    name = name.Insert(0, entry.Path + "/");
                }

                entries.Add(name);
            }

            return entries;
        }

        private List<string> UnreferencedVariations(GameVehicles vehicles)
        {
            List<string> unreferenced = new List<string>();
            foreach (GameVehicleVariation variation in vehicles.VehicleVariations)
            {
                if (variation.ReferenceCount != 0)
                {
                    continue;
                }

                unreferenced.Add(variation.ModelName);
            }

            return unreferenced;
        }

        private List<string> UnreferencedModKits(GameVehicles vehicles)
        {
            List<string> unreferenced = new List<string>();
            foreach (GameVehicleKit kit in vehicles.VehicleKits)
            {
                if (kit.ReferenceCount != 0)
                {
                    continue;
                }

                unreferenced.Add(kit.Name);
            }

            return unreferenced;
        }

        private List<string> NotReferenced(Platform platform, GameVehicles vehicles, VehicleRpfEntries entries)
        {
            string textureExtension = GetExtension(FileType.TextureDictionary, platform);
            string drawableDictionaryExtension = GetExtension(FileType.DrawableDictionary, platform);
            string drawableExtension = GetExtension(FileType.Drawable, platform);
            string fragmentExtension = GetExtension(FileType.Fragment, platform);
            string clipExtension = GetExtension(FileType.ClipDictionary, platform);

            List<string> notReferenced = new List<string>();
            HashSet<string> referenced = new HashSet<string>();
            foreach (string wheelModel in vehicles.WheelModels)
            {
                referenced.Add(wheelModel + drawableDictionaryExtension);
                referenced.Add(wheelModel + drawableExtension);
            }

            foreach (string textureDictionary in vehicles.TextureDictionaries)
            {
                referenced.Add(textureDictionary + textureExtension);
                referenced.Add(textureDictionary + "+hi" + textureExtension);
            }

            foreach (GameVehicle vehicle in vehicles.Vehicles)
            {
                if (!string.IsNullOrEmpty(vehicle.RoofClipDictionary))
                {
                    referenced.Add(vehicle.RoofClipDictionary + clipExtension);
                }

                referenced.Add(vehicle.ModelName + fragmentExtension);
                referenced.Add(vehicle.ModelName + "_hi" + fragmentExtension);
                if (!string.IsNullOrEmpty(vehicle.TextureDictionary))
                {
                    referenced.Add(vehicle.TextureDictionary + textureExtension);
                    referenced.Add(vehicle.TextureDictionary + "+hi" + textureExtension);
                }

                if (vehicle.Variation == null)
                {
                    continue;
                }

                foreach (KeyValuePair<string, GameVehicleKit> kit in vehicle.Variation.Kits)
                {
                    if (kit.Value == null)
                    {
                        continue;
                    }

                    foreach (string modelName in kit.Value.ModelNames)
                    {
                        referenced.Add(modelName + fragmentExtension);
                        referenced.Add(modelName + "_hi" + fragmentExtension);
                    }
                }
            }

            foreach (string entry in entries.ModEntires.AllEntries())
            {
                if (!referenced.Contains(entry))
                {
                    notReferenced.Add(entry);
                }
            }

            foreach (string entry in entries.VehicleEntires.AllEntries())
            {
                if (!referenced.Contains(entry))
                {
                    notReferenced.Add(entry);
                }
            }

            return notReferenced;
        }

        private List<string> NotFound(Platform platform, GameVehicles vehicles, VehicleRpfEntries entries)
        {
            string textureExtension = GetExtension(FileType.TextureDictionary, platform);
            string drawableDictionaryExtension = GetExtension(FileType.DrawableDictionary, platform);
            string drawableExtension = GetExtension(FileType.Drawable, platform);
            string fragmentExtension = GetExtension(FileType.Fragment, platform);
            string clipExtension = GetExtension(FileType.ClipDictionary, platform);

            HashSet<string> modEntries = new HashSet<string>(entries.ModEntires.AllEntries());
            HashSet<string> vehicleEntries = new HashSet<string>(entries.VehicleEntires.AllEntries());

            List<string> notFound = new List<string>();
            foreach (GameVehicleKit kit in vehicles.VehicleKits)
            {
                if (kit.ReferenceCount == 0)
                {
                    continue;
                }

                foreach (string modelName in kit.ModelNames)
                {
                    if (!modEntries.Contains(modelName + fragmentExtension))
                    {
                        notFound.Add(modelName + fragmentExtension);
                    }
                }
            }

            foreach (string wheelModel in vehicles.WheelModels)
            {
                if (!modEntries.Contains(wheelModel + drawableExtension))
                {
                    notFound.Add(wheelModel + drawableExtension);
                }
            }

            foreach (string textureDictionary in vehicles.TextureDictionaries)
            {
                if (!vehicleEntries.Contains(textureDictionary + textureExtension))
                {
                    notFound.Add(textureDictionary + textureExtension);
                }
            }

            foreach (GameVehicle vehicle in vehicles.Vehicles)
            {
                if (!vehicleEntries.Contains(vehicle.ModelName + fragmentExtension))
                {
                    notFound.Add(vehicle.ModelName + fragmentExtension);
                }

                if (!string.IsNullOrEmpty(vehicle.TextureDictionary) && vehicle.TextureDictionary != "null")
                {
                    if (!vehicleEntries.Contains(vehicle.TextureDictionary + textureExtension))
                    {
                        notFound.Add(vehicle.TextureDictionary + textureExtension);
                    }
                }

                if (!string.IsNullOrEmpty(vehicle.RoofClipDictionary))
                {
                    if (!vehicleEntries.Contains(vehicle.RoofClipDictionary + clipExtension))
                    {
                        notFound.Add(vehicle.RoofClipDictionary + clipExtension);
                    }
                }
            }

            return notFound;
        }

        private string GetExtension(FileType type, Platform platform)
        {
            return "." + type.CreateExtensionForPlatform(platform);
        }
        #endregion

        private class RpfEntries : Dictionary<string, List<string>>
        {
            public List<string> AllEntries()
            {
                List<string> entries = new List<string>();
                foreach (List<string> values in this.Values)
                {
                    entries.AddRange(values);
                }

                return entries;
            }
        }

        private class VehicleRpfEntries
        {
            public RpfEntries ModEntires
            {
                get;
                set;
            }

            public RpfEntries VehicleEntires
            {
                get;
                set;
            }
        }

        private class PlatformEntries : Dictionary<Platform, VehicleRpfEntries>
        {
        }
    } // RSG.Model.Report2.Reports.GameAssetUsage.WeaponAssetUsageReport {Class}
} // RSG.Model.Report2.Reports.GameAssetUsage {Namespace}
