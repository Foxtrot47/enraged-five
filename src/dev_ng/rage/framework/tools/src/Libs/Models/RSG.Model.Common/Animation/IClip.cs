﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common.Animation
{
    /// <summary>
    /// IClip interface 
    /// </summary>
    public interface IClip : IAsset, IComparable<IClip>, IEquatable<IClip>
    {
        /// <summary>
        /// Collection of Animations the clip references
        /// </summary>
        IEnumerable<IAnimation> Animations { get; }
    } // interface IClip
} // namespace RSG.Model.Common.Animation
