﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.ComponentModel;

namespace RSG.Model.Common
{
    /// <summary>
    /// Base class for a asset container
    /// </summary>
    public abstract class AssetContainerBase : AssetBase, IHasAssetChildren
    {
        #region Members

        private RSG.Base.Collections.ObservableCollection<IAsset> m_assetChildren;

        #endregion // Members

        #region Properties

        /// <summary>
        /// The collection of assets belonging to this container
        /// </summary>
        [Browsable(false)]
        public RSG.Base.Collections.ObservableCollection<IAsset> AssetChildren
        {
            get { return m_assetChildren; }
            set
            {
                if (m_assetChildren == value)
                    return;

                SetPropertyValue(value, () => AssetChildren,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_assetChildren = (RSG.Base.Collections.ObservableCollection<IAsset>)newValue;
                        }
                ));
            }
        }

        #endregion // Properties

        #region Constructors

        /// <summary>
        /// Default Constructor
        /// </summary>
        public AssetContainerBase()
            : base()
        {
            this.AssetChildren = new RSG.Base.Collections.ObservableCollection<IAsset>();
        }

        /// <summary>
        /// Default Constructor
        /// </summary>
        public AssetContainerBase(String name)
            : base(name)
        {
            this.AssetChildren = new RSG.Base.Collections.ObservableCollection<IAsset>();
        }

        #endregion // Constructors

        #region Public Methods
        /// <summary>
        /// Returns true if this container contains a asset in it
        /// with the given name
        /// </summary>
        public Boolean ContainsAsset(String name)
        {
            foreach (IAsset asset in this.AssetChildren)
            {
                if (String.Compare(asset.Name, name) == 0)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public override List<IAsset> Find(Regex expression)
        {
            var results = new List<IAsset>();

            if (!string.IsNullOrEmpty(this.Name))
            {
                if (expression.IsMatch(this.Name))
                    results.Add(this);
            }
            if (this.AssetChildren != null)
            {
                foreach (var child in this.AssetChildren)
                {
                    results.AddRange(child.Find(expression));
                }
            }

            return results;
        }
        #endregion // Public Methods
    } // AssetContainerBase
} // RSG.Model.Common
