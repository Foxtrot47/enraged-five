﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.SceneXml;
using RSG.SceneXml.Statistics;
using RSG.Base.Math;

namespace RSG.Model.Map.Statistics
{
    public class InteriorFile
        : Base
    {
        #region Properties

        /// <summary>
        /// The statistics from this interior
        /// </summary>
        public SingleInterior InteriorStatistic
        {
            get { return m_interiorStatistic; }
            set { m_interiorStatistic = value; }
        }
        private SingleInterior m_interiorStatistic;

        public BoundingBox3f SectionBoundingBox
        {
            get { return m_sectionBoundingBox; }
            set { m_sectionBoundingBox = value; }
        }
        private BoundingBox3f m_sectionBoundingBox;

        public float InverseArea
        {
            get { return m_inverseArea; }
            set { m_inverseArea = value; }
        }
        private float m_inverseArea;

        /// <summary>
        /// The maximum draw distance for all the references
        /// </summary>
        public float MaxReferenceDistance
        {
            get;
            set;
        }

        #endregion // Properties

        #region Constructor(s)
        
        /// <summary>
        /// Default constructor
        /// </summary>
        public InteriorFile(MapSection section, LevelReferences references)
        {
            this.SectionBoundingBox = new BoundingBox3f();
            this.InteriorStatistic = new SingleInterior();
            this.MaxReferenceDistance = 0.0f;
            Scene scene = SceneManager.GetScene(section.SceneXmlFilename);

            if (scene != null)
            {
                Stats sceneStatistics = scene.Statistics;
                if (sceneStatistics != null)
                {
                    if (sceneStatistics.GeometryStats.Count > 0)
                    {
                        foreach (TargetObjectDef obj in scene.Objects)
                        {
                            if (obj.IsMilo())
                            {
                                ParseMilo(obj, scene, references);

                                float sectionWidth = SectionBoundingBox.Max.X - SectionBoundingBox.Min.X;
                                float sectionLength = SectionBoundingBox.Max.Y - SectionBoundingBox.Min.Y;
                                float sectionHeight = SectionBoundingBox.Max.Z - SectionBoundingBox.Min.Z;
                                if (sectionWidth > 0.1f && sectionLength > 0.1f && sectionHeight > 0.1f)
                                {
                                    this.InverseArea = 1.0f / (sectionWidth * sectionLength * sectionHeight);
                                }
                            }
                            else if (obj.IsMiloTri())
                            {
                                this.InteriorStatistic.ReferenceName = obj.Name;
                            }
                        }
                    }
                }
            }

            this.InteriorStatistic.Polydensity = this.InteriorStatistic.Polycount * this.InverseArea;
            this.InteriorStatistic.CollisionPolydensity = this.InteriorStatistic.CollisionPolycount * this.InverseArea;

            String sourcePath = new Uri(section.SourceAssetFilename).AbsolutePath;
            if (this.InteriorStatistic != null)
            {
                references.AddReference(sourcePath, this.InteriorStatistic.ReferenceName, this.InteriorStatistic);
            }
            else
            {
                RSG.Base.Logging.Log.Log__Warning("Unable to create the map reference for the interior {0}", sourcePath);
            }            
        }

        #endregion // Constructor(s)

        #region Statistics Creation

        /// <summary>
        /// Gets all of the statistics for the given milo, making sure to walk down through all of the children
        /// </summary>
        private void ParseMilo(TargetObjectDef milo, Scene scene, LevelReferences references)
        {
            foreach (TargetObjectDef child in milo.Children)
            {
                if (child.IsMilo())
                {
                    ParseMilo(child, scene, references);
                }
                else if (child.IsObject())
                {
                    if (child.DontExport() == false && child.DontExportIDE() == false && child.DontExportIPL() == false)
                    {
                        if (child.WorldBoundingBox != null)
                        {
                            this.SectionBoundingBox.Expand(child.WorldBoundingBox);
                        }

                        if (child.IsXRef() || child.IsRefObject() || child.IsInternalRef())
                        {
                            GenerateReference(child, references);
                        }
                        else
                        {
                            // Get statistics for the object
                            DrawableStats statistics = null;
                            if (scene.Statistics.GeometryStats.TryGetValue(child.Guid, out statistics))
                            {
                                this.InteriorStatistic.Polycount += statistics.PolyCount;
                                this.InteriorStatistic.GeometrySize += statistics.Size;
                            }
                            foreach (TargetObjectDef objChild in child.Children)
                            {
                                if (objChild.IsCollision())
                                {
                                    if (scene.Statistics.CollisionStats.TryGetValue(objChild.Guid, out statistics))
                                    {
                                        this.InteriorStatistic.CollisionPolycount += statistics.PolyCount;
                                    }
                                }
                            }

                            GenerateTxd(child, scene.Statistics.TxdStats);
                        }
                    }
                }
                else if (child.IsMloRoom())
                {
                    ParseMilo(child, scene, references);
                }
            }
        }

        /// <summary>
        /// Creates a dictionary of references that belong to this map section so that
        /// the statistics can be generated later in a second pass
        /// </summary>
        private void GenerateReference(TargetObjectDef reference, LevelReferences references)
        {
            LevelReferenceStatistics resolvedReference = references.GetReferenceStatistics(reference.RefFile, reference.RefName);
            if (resolvedReference != null)
            {
                references.ReferenceFiles[reference.RefFile][reference.RefName].AppearenceCount++;
                references.ReferenceFiles[reference.RefFile][reference.RefName].AppearenceLocations.Add(reference.WorldBoundingBox.Centre());

                // You need to add the reference statistics into the interior statistic since 
                // a interior should be just a single reference
                this.InteriorStatistic.Polycount += resolvedReference.Polycount;
                this.InteriorStatistic.GeometrySize += resolvedReference.GeometrySize;
                this.InteriorStatistic.CollisionPolycount += resolvedReference.CollisionPolycount;

                foreach (KeyValuePair<String, uint> txd in resolvedReference.Txds)
                {
                    if (!this.InteriorStatistic.Txds.ContainsKey(txd.Key))
                    {
                        this.InteriorStatistic.Txds.Add(txd.Key, txd.Value);
                    }
                }
            }
        }

        /// <summary>
        /// Generates a txd statistic from a scene object and the txd stats
        /// </summary>
        private void GenerateTxd(TargetObjectDef sceneObject, IDictionary<String, TxdStats> statistics)
        {
            String txdName = sceneObject.GetAttribute(AttrNames.OBJ_TXD, AttrDefaults.OBJ_TXD);

            if (!this.InteriorStatistic.Txds.ContainsKey(txdName))
            {
                TxdStats statistic = null;
                if (statistics.TryGetValue(txdName, out statistic))
                {
                    this.InteriorStatistic.Txds.Add(txdName, statistic.Size);
                }
            }
        }

        #endregion // Statistics Creation

        #region Override Functions

        #region Total Cost Statistics

        public override int GetHighTotalCost()
        {
            return -1;
        }

        public override int GetLodTotalCost()
        {
            return -1;
        }

        public override int GetSLodTotalCost()
        {
            return -1;
        }

        #endregion // Total Cost Statistics

        #region Geometry Cost Statistics

        public override int GetHighGeometryCost()
        {
            return -1;
        }

        public override int GetLodGeometryCost()
        {
            return -1;
        }

        public override int GetSLodGeometryCost()
        {
            return -1;
        }

        #endregion // Geometry Cost Statistics

        #region TXD Cost Statistics

        public override int GetHighTXDCost()
        {
            return -1;
        }

        public override int GetLodTXDCost()
        {
            return -1;
        }

        public override int GetSLodTXDCost()
        {
            return -1;
        }

        #endregion // TXD Cost Statistics

        #region Geometry Count Statistics

        public override int GetHighGeometryCount()
        {
            return -1;
        }

        public override int GetLodGeometryCount()
        {
            return -1;
        }

        public override int GetSLodGeometryCount()
        {
            return -1;
        }

        #endregion // Geometry Count Statistics

        #region TXD Count Statistics

        public override int GetHighTXDCount()
        {
            return -1;
        }

        public override int GetLodTXDCount()
        {
            return -1;
        }

        public override int GetSLodTXDCount()
        {
            return -1;
        }

        #endregion // TXD Count Statistics

        #region Polygon Count Statistics

        public override int GetHighPolygonCount()
        {
            return -1;
        }

        public override int GetLodPolygonCount()
        {
            return -1;
        }

        public override int GetSLodPolygonCount()
        {
            return -1;
        }

        #endregion // Polygon Count Statistics

        #region Polygon Density Statistics

        public override float GetHighPolygonDensity()
        {
            return -1;
        }

        public override float GetLodPolygonDensity()
        {
            return -1;
        }

        public override float GetSLodPolygonDensity()
        {
            return -1;
        }

        #endregion // Polygon Density Statistics

        #region Collision Statistics

        public override int GetCollisionPolygonCount()
        {
            return -1;
        }

        public override float GetCollisionPolygonDensity()
        {
            return -1;
        }

        public override float GetCollisionPolygonRatio()
        {
            return -1;
        }

        #endregion // Collision Statistics

        #region LOD Distance Statistics

        public override int GetHighLodDistance()
        {
            return -1;
        }

        public override int GetLodLodDistance()
        {
            return -1;
        }

        public override int GetSLodLodDistance()
        {
            return -1;
        }

        #endregion // LOD Distance Statistics

        #region Reference Total Cost Statistics

        public override int GetAllRefTotalCost()
        {
            return -1;
        }

        public override int GetUniqueRefTotalCost()
        {
            return -1;
        }

        #endregion // Reference Cost Statistics

        #region Reference Geometry Cost Statistics

        public override int GetAllRefGeometryCost()
        {
            return -1;
        }

        public override int GetUniqueRefGeometryCost()
        {
            return -1;
        }

        #endregion // Reference Geometry Cost Statistics

        #region Reference TXD Statistics

        public override int GetAllRefTXDCost()
        {
            return -1;
        }

        public override int GetAllRefTXDCount()
        {
            return -1;
        }

        #endregion // Reference TXD Statistics

        #region Reference Count Statistics

        public override int GetAllRefCount()
        {
            return -1;
        }

        public override int GetUniqueRefCount()
        {
            return -1;
        }

        #endregion // Reference Count Statistics

        #region Reference Polygon Count Statistics

        public override int GetReferencePolygonCountAll()
        {
            return -1;
        }

        public override int GetReferencePolygonCountUnique()
        {
            return -1;
        }

        #endregion // Reference Polygon Count Statistics

        #region Reference Polygon Density Statistics

        public override float GetReferencePolygonDensityAll()
        {
            return -1;
        }

        public override float GetReferencePolygonDensityUnique()
        {
            return -1;
        }

        #endregion // Reference Polygon Density Statistics

        #region Reference Collision Count Statistics

        public override int GetReferenceCollisionPolygonCountAll()
        {
            return -1;
        }

        public override int GetReferenceCollisionPolygonCountUnique()
        {
            return -1;
        }

        #endregion // Reference Collision Count Statistics

        #region Reference Collision Density Statistics

        public override float GetReferenceCollisionPolygonDensityAll()
        {
            return -1;
        }

        public override float GetReferenceCollisionPolygonDensityUnique()
        {
            return -1;
        }

        #endregion // Reference Collision Density Statistics

        #region Reference Collision Ratio Statistics

        public override float GetReferenceCollisionPolygonRatioAll()
        {
            return -1;
        }

        public override float GetReferenceCollisionPolygonRatioUnique()
        {
            return -1;
        }

        #endregion // Reference Collision Ratio Statistics

        #region Reference LOD Distance Statistics

        public override float GetReferenceLodDistance()
        {
            return -1;
        }

        #endregion Reference // LOD Distance Statistics

        #endregion // Override Functions
    }
}
