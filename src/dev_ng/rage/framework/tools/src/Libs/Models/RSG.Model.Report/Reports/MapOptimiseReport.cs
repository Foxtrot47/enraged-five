﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using RSG.Base.ConfigParser;
using RSG.Model.Common;
using RSG.Model.Map;
using RSG.Model.Map.Statistics;

namespace RSG.Model.Report.Reports
{
    using SortStats = SortedDictionary<float, List<MapSection>>;

    /// <summary>
    /// 
    /// </summary>
    public class MapOptimiseReport : HTMLReport, IDynamicMapReport
    {
        #region Constants
        private const String NAME = "Map Optimise Report";
        private const String DESCRIPTION = "Generate and display a map optimisation report.";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Determines whether the map data should be loaded
        /// </summary>
        public bool LoadMapData
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Determines whether the car generators should be loaded
        /// 
        /// </summary>
        public bool LoadCarGenarators
        {
            get { return false; }
        }
        #endregion

        #region Construtcor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public MapOptimiseReport()
            : base(NAME, DESCRIPTION)
        {
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        /// <param name="gv"></param>
        public void Generate(ILevel level, ConfigGameView gv, IPlatformCollection platforms)
        {
            GenerateMapOptimiseReport(level, gv);
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        /// <param name="gv"></param>
        public void GenerateMapOptimiseReport(ILevel level, ConfigGameView gv)
        {
            RSG.Model.Map.Util.Creation.InitialiseLevelStatistics(level, gv);

            SortedDictionary<float, List<MapSection>> shaderDensities = new SortedDictionary<float, List<MapSection>>();
            SortedDictionary<float, List<MapSection>> polygonDensities = new SortedDictionary<float, List<MapSection>>();
            SortedDictionary<float, List<MapSection>> drawableInstanceDensities = new SortedDictionary<float, List<MapSection>>();
            SortedDictionary<float, List<MapSection>> nonDrawableDensities = new SortedDictionary<float, List<MapSection>>();
            SortedDictionary<float, List<MapSection>> uniqueNonDrawableDensities = new SortedDictionary<float, List<MapSection>>();
            GetSortedStatistics(level, ref shaderDensities, ref polygonDensities, ref drawableInstanceDensities, ref nonDrawableDensities, ref uniqueNonDrawableDensities);

            MemoryStream stream = new MemoryStream();
            StreamWriter reportFile = new StreamWriter(stream);

            HtmlTextWriter writer = new HtmlTextWriter(reportFile);
            writer.RenderBeginTag(HtmlTextWriterTag.Html);
                writer.RenderBeginTag(HtmlTextWriterTag.Head);
                writer.RenderEndTag();
                writer.RenderBeginTag(HtmlTextWriterTag.Body);

                    writer.RenderBeginTag(HtmlTextWriterTag.H1);
                    writer.Write("Map Optimisation Report");
                    writer.RenderEndTag();

                    RenderStatTable(shaderDensities, writer, "The 20 Worst Sections For Shader Density");
                    RenderStatTable(polygonDensities, writer, "The 20 Worst Sections For Polygon Density");
                    RenderStatTable(drawableInstanceDensities, writer, "The 20 Worst Sections For Object Density");
                    RenderStatTable(nonDrawableDensities, writer, "The 20 Worst Sections For Reference Density");
                    RenderStatTable(uniqueNonDrawableDensities, writer, "The 20 Worst Sections For Unique Reference Density");

                writer.RenderEndTag();
            writer.RenderEndTag();
            writer.Flush();

            stream.Position = 0;
            Stream = stream;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        /// <param name="shaderDensities"></param>
        /// <param name="polygonDensities"></param>
        /// <param name="drawableInstanceDensities"></param>
        /// <param name="nonDrawableDensities"></param>
        /// <param name="uniqueNonDrawableDensities"></param>
        private void GetSortedStatistics(ILevel level, ref SortStats shaderDensities, ref SortStats polygonDensities, ref SortStats drawableInstanceDensities,
            ref SortStats nonDrawableDensities, ref SortStats uniqueNonDrawableDensities)
        {
            foreach (MapSection section in RSG.Model.Map.Util.Enumerations.GetAllMapSections(level, null, true))
            {
                if (section.Area == 0.0f)
                    continue;

                var statGroup = RawSectionStatistics.StatisticGroups.Instances;
                var lodLevel = RawSectionStatistics.LodLevel.Total;
                var instanceType = RawSectionStatistics.InstanceType.Total;

                float shaderDensity = section.RawSectionStatistics[statGroup][lodLevel][instanceType].ShaderCount / section.Area;
                if (!shaderDensities.ContainsKey(shaderDensity))
                    shaderDensities.Add(shaderDensity, new List<MapSection>());
                shaderDensities[shaderDensity].Add(section);

                float polygonDensity = section.RawSectionStatistics[statGroup][lodLevel][instanceType].PolygonCount / section.Area;
                if (!polygonDensities.ContainsKey(polygonDensity))
                    polygonDensities.Add(polygonDensity, new List<MapSection>());
                polygonDensities[polygonDensity].Add(section);

                float drawableInstanceDensity = section.RawSectionStatistics[RawSectionStatistics.StatisticGroups.DrawableInstances][lodLevel][instanceType].Count / section.Area;
                if (!drawableInstanceDensities.ContainsKey(drawableInstanceDensity))
                    drawableInstanceDensities.Add(drawableInstanceDensity, new List<MapSection>());
                drawableInstanceDensities[drawableInstanceDensity].Add(section);        
                
                float nonDrawableInstanceDensity = section.RawSectionStatistics[RawSectionStatistics.StatisticGroups.NonDrawableInstances][lodLevel][instanceType].Count / section.Area;
                if (!nonDrawableDensities.ContainsKey(nonDrawableInstanceDensity))
                    nonDrawableDensities.Add(nonDrawableInstanceDensity, new List<MapSection>());
                nonDrawableDensities[nonDrawableInstanceDensity].Add(section);           
                
                float uniqueNonDrawableInstanceDensity = section.RawSectionStatistics[RawSectionStatistics.StatisticGroups.NonDrawableInstances][lodLevel][instanceType].UniqueCount / section.Area;
                if (!uniqueNonDrawableDensities.ContainsKey(uniqueNonDrawableInstanceDensity))
                    uniqueNonDrawableDensities.Add(uniqueNonDrawableInstanceDensity, new List<MapSection>());
                uniqueNonDrawableDensities[uniqueNonDrawableInstanceDensity].Add(section);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stats"></param>
        /// <param name="writer"></param>
        /// <param name="header"></param>
        private void RenderStatTable(SortStats stats, HtmlTextWriter writer, string header)
        {
            writer.RenderBeginTag(HtmlTextWriterTag.H1);
            writer.Write(header);
            writer.RenderEndTag();
            int index = 0;

            writer.AddAttribute(HtmlTextWriterAttribute.Border, "1");
            writer.AddAttribute(HtmlTextWriterAttribute.Width, "40%");
            writer.RenderBeginTag(HtmlTextWriterTag.Table);
            writer.RenderBeginTag(HtmlTextWriterTag.Tr);
            writer.RenderBeginTag(HtmlTextWriterTag.Th);
            writer.Write("Section");
            writer.RenderEndTag();
            writer.RenderBeginTag(HtmlTextWriterTag.Th);
            writer.Write("Density Value");
            writer.RenderEndTag();
            writer.RenderEndTag();

            foreach (KeyValuePair<float, List<MapSection>> kvp in stats.Reverse())
            {
                foreach (MapSection section in kvp.Value)
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.Tr);

                    writer.RenderBeginTag(HtmlTextWriterTag.Td);
                    writer.Write(String.Format("{0}    ", section.Name));
                    writer.RenderEndTag();

                    writer.RenderBeginTag(HtmlTextWriterTag.Td);
                    writer.Write(String.Format("{0}", kvp.Key.ToString()));
                    writer.RenderEndTag();

                    writer.RenderEndTag();
                    if (++index == 20)
                        break;
                }
                if (index == 20)
                    break;
            }

            writer.RenderEndTag();
        }
        #endregion // Private Methods
    } // MapOptimiseReport
} // RSG.Model.Report.Reports
