﻿//---------------------------------------------------------------------------------------------
// <copyright file="DialogueSpecialFlags.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.Model
{
    using System;

    /// <summary>
    /// Defines the different flags that can be set onto a dialogue special that used
    /// during the build process.
    /// </summary>
    [Flags]
    public enum DialogueSpecialFlags
    {
        /// <summary>
        /// No special flags have been associated.
        /// </summary>
        None = 0,

        /// <summary>
        /// Using this means that the line can be thought of as having the headset sub-mix
        /// value set to true.
        /// </summary>
        Headset = 1 << 0,

        /// <summary>
        /// Using this means that the line can be thought of as having the pad speaker value
        /// set to true.
        /// </summary>
        PadSpeaker = 1 << 1,
    } // RSG.Text.Model.DialogueSpecialFlags {Enum}
} // RSG.Text.Model {Namespace}
