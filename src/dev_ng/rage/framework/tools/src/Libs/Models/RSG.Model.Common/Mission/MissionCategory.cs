﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common.Mission
{
    /// <summary>
    /// Type of missions we have.
    /// </summary>
    public enum MissionCategory
    {
        /// <summary>
        /// Missions that don't fall into any specific category.
        /// </summary>
        Other,

        /// <summary>
        /// Mission that makes up the main story.
        /// </summary>
        MainStory,

        /// <summary>
        /// Random character based misison.
        /// </summary>
        RandomCharacter,

        /// <summary>
        /// Random event that occurred.
        /// </summary>
        RandomEvent,

        /// <summary>
        /// Odd job mission.
        /// </summary>
        Oddjob,

        /// <summary>
        /// Mini game.
        /// </summary>
        MiniGame,

        /// <summary>
        /// Property based mission.
        /// </summary>
        Property,

        /// <summary>
        /// Ambient missions.
        /// </summary>
        Ambient
    } // MissionCategory
}
