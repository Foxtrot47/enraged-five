﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProjectItem.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.Model
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Xml;
    using RSG.Editor;
    using RSG.Editor.Model;
    using RSG.Interop.Microsoft.Windows;

    /// <summary>
    /// Represents a single generic project item contained within a projects item group. The
    /// include value for this item has to be unique compared to the other project items within
    /// the same project item scope.
    /// </summary>
    public class ProjectItem : ModelBase
    {
        #region Fields
        /// <summary>
        /// A dictionary of metadata that has been attached to this project item.
        /// </summary>
        private HashSet<string> _builtinMetadata;

        /// <summary>
        /// The private field used for the <see cref="Include"/> property.
        /// </summary>
        private string _include;

        /// <summary>
        /// The private field used for the <see cref="ItemType"/> property.
        /// </summary>
        private string _itemType;

        /// <summary>
        /// A dictionary of metadata that has been attached to this project item.
        /// </summary>
        private Dictionary<string, string>[] _metadataSets;

        /// <summary>
        /// The private reference to the project that owns this item.
        /// </summary>
        private IProjectItemScope _scope;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ProjectItem"/> class.
        /// </summary>
        /// <param name="type">
        /// The type of this item.
        /// </param>
        /// <param name="include">
        /// The include value for the project item.
        /// </param>
        /// <param name="scope">
        /// The scope that this item belongs to.
        /// </param>
        public ProjectItem(string type, string include, IProjectItemScope scope)
        {
            if (scope == null)
            {
                throw new SmartArgumentNullException(() => scope);
            }

            this._scope = scope;
            this._itemType = type;
            this._include = include;

            this._metadataSets = new Dictionary<string, string>[MetadataLevelUtil.Count];
            for (int i = 0; i < MetadataLevelUtil.Count; i++)
            {
                this._metadataSets[i] = new Dictionary<string, string>();
            }

            this._builtinMetadata = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            this._builtinMetadata.UnionWith(this.GetBuiltInMetadataKeys());
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ProjectItem"/> class.
        /// </summary>
        /// <param name="type">
        /// The type of this item.
        /// </param>
        /// <param name="include">
        /// The include value for the project item.
        /// </param>
        protected ProjectItem(string type, string include)
        {
            this._metadataSets = new Dictionary<string, string>[MetadataLevelUtil.Count];
            for (int i = 0; i < MetadataLevelUtil.Count; i++)
            {
                this._metadataSets[i] = new Dictionary<string, string>();
            }

            this._builtinMetadata = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            this._builtinMetadata.UnionWith(this.GetBuiltInMetadataKeys());
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ProjectItem"/> class as a copy of the
        /// specified instance.
        /// </summary>
        /// <param name="other">
        /// The other instance to copy.
        /// </param>
        protected ProjectItem(ProjectItem other)
        {
            this._scope = other._scope;
            this._itemType = other._itemType;
            this._include = other._include;

            this._metadataSets = new Dictionary<string, string>[other._metadataSets.Length];
            for (int i = 0; i < other._metadataSets.Length; i++)
            {
                this._metadataSets[i] = new Dictionary<string, string>(other._metadataSets[i]);
            }

            this._builtinMetadata = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            this._builtinMetadata.UnionWith(this.GetBuiltInMetadataKeys());
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the include value for this project item.
        /// </summary>
        public string Include
        {
            get { return this._include; }
            set { this.SetProperty(ref this._include, value); }
        }

        /// <summary>
        /// Gets the item type name for this project item.
        /// </summary>
        public string ItemType
        {
            get { return this._itemType; }
        }

        /// <summary>
        /// Gets the scope that this project item is underneath.
        /// </summary>
        public IProjectItemScope Scope
        {
            get { return this._scope; }
        }

        /// <summary>
        /// Gets an array containing all of the metadata keys this project item currently
        /// contains.
        /// </summary>
        internal string[] MetadataKeys
        {
            get
            {
                HashSet<string> keys = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
                foreach (Dictionary<string, string> set in this._metadataSets)
                {
                    foreach (string key in set.Keys)
                    {
                        keys.Add(key);
                    }
                }

                return keys.ToArray();
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Changes the scope for this item. This makes sure that the include value gets
        /// updated to make it relative with its new scope.
        /// </summary>
        /// <param name="newScope">
        /// The new project scope for this item.
        /// </param>
        public void ChangeProjectScope(IProjectItemScope newScope)
        {
            bool updating = !Object.ReferenceEquals(newScope, this._scope);
            if (!updating)
            {
                Debug.Assert(updating, "Specified new scope is the same as the current one");
                return;
            }

            string fullPath = this.GetMetadata("FullPath");
            string directory = newScope.DirectoryPath;
            if (!String.IsNullOrWhiteSpace(directory))
            {
                this.Include = Shlwapi.MakeRelativeFromDirectoryToFile(directory, fullPath);
            }

            this._scope.RemoveProjectItem(this);
            this._scope = newScope;
            this._scope.AddProjectItem(this);
        }

        /// <summary>
        /// Creates a deep copy of the current instance.
        /// </summary>
        /// <returns>
        /// A deep copy of the current System.Object.
        /// </returns>
        public override object DeepClone()
        {
            return new ProjectItem(this);
        }

        /// <summary>
        /// Retrieves an array containing all of the available metadata keys for this item.
        /// </summary>
        /// <returns>
        /// An array containing all of the available metadata keys for this item.
        /// </returns>
        public string[] GetAvaliableMetadataKeys()
        {
            HashSet<string> keys = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            foreach (Dictionary<string, string> set in this._metadataSets)
            {
                foreach (string key in set.Keys)
                {
                    keys.Add(key);
                }
            }

            foreach (string builtin in this.GetBuiltInMetadataKeys())
            {
                keys.Add(builtin);
            }

            return keys.ToArray();
        }

        /// <summary>
        /// Retrieves the value of the metadata property with the specified name.
        /// </summary>
        /// <param name="key">
        /// The metadata key.
        /// </param>
        /// <param name="defaultValue">
        /// The value to return if the metadata property doesn't exist.
        /// </param>
        /// <returns>
        /// The value of the metadata property with the specified name if it exists; otherwise,
        /// the specified fall-back value.
        /// </returns>
        public string GetMetadata(string key, string defaultValue)
        {
            if (this._builtinMetadata.Contains(key))
            {
                return this.GetBuiltinMetadataValue(key);
            }

            string value = null;
            foreach (Dictionary<string, string> set in this._metadataSets)
            {
                string setValue;
                if (set.TryGetValue(key, out setValue))
                {
                    value = setValue;
                }
            }

            return value ?? defaultValue;
        }

        /// <summary>
        /// Retrieves the value of the metadata property with the specified name as a boolean
        /// value.
        /// </summary>
        /// <param name="key">
        /// The metadata key.
        /// </param>
        /// <param name="defaultValue">
        /// The value to return if the metadata property doesn't exist.
        /// </param>
        /// <returns>
        /// The value of the metadata property with the specified name if it exists; otherwise,
        /// the specified fall-back value.
        /// </returns>
        public bool GetMetadataAsBool(string key, bool defaultValue)
        {
            string value = this.GetMetadata(key, null);
            if (value == null)
            {
                return defaultValue;
            }

            bool result = false;
            if (!Boolean.TryParse(value, out result))
            {
                return defaultValue;
            }

            return result;
        }

        /// <summary>
        /// Retrieves the value of the metadata property with the specified name at the
        /// specified value.
        /// </summary>
        /// <param name="key">
        /// The metadata key.
        /// </param>
        /// <param name="defaultValue">
        /// The value to return if the metadata property doesn't exist.
        /// </param>
        /// <param name="level">
        /// The metadata level the value will be taken from.
        /// </param>
        /// <returns>
        /// The value of the metadata property with the specified name if it exists at the
        /// specified metadata level; otherwise, the specified fall-back value.
        /// </returns>
        public string GetMetadata(string key, string defaultValue, MetadataLevel level)
        {
            if (this._builtinMetadata.Contains(key))
            {
                return this.GetBuiltinMetadataValue(key);
            }

            string value = null;
            Dictionary<string, string> set = this._metadataSets[(int)level];
            string setValue;
            if (set.TryGetValue(key, out setValue))
            {
                value = setValue;
            }

            return value ?? defaultValue;
        }

        /// <summary>
        /// Retrieves the value of the metadata property with the specified name at the
        /// specified metadata level.
        /// </summary>
        /// <param name="key">
        /// The name of the metadata property to retrieve the value of.
        /// </param>
        /// <param name="level">
        /// The level the value will be taken from.
        /// </param>
        /// <returns>
        /// The value of the metadata property with the specified name if it exists at the
        /// specified metadata level; otherwise, null.
        /// </returns>
        public string GetMetadata(string key, MetadataLevel level)
        {
            return this.GetMetadata(key, null, level);
        }

        /// <summary>
        /// Retrieves the value of the metadata property with the specified name.
        /// </summary>
        /// <param name="key">
        /// The name of the metadata property to retrieve the value of.
        /// </param>
        /// <returns>
        /// The value of the metadata property with the specified name if it exists; otherwise,
        /// null.
        /// </returns>
        public string GetMetadata(string key)
        {
            return this.GetMetadata(key, null);
        }

        /// <summary>
        /// Determines whether this item has a metadata value with the given name.
        /// </summary>
        /// <param name="name">
        /// The name of the metadata to be searched for.
        /// </param>
        /// <returns>
        /// Returns true if metadata with the given name is defined on this item; otherwise,
        /// false.
        /// </returns>
        public bool HasMetadataKey(string name)
        {
            foreach (Dictionary<string, string> set in this._metadataSets)
            {
                foreach (string key in set.Keys)
                {
                    if (String.Equals(key, name))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Removes any metadata with the given name on this item from all metadata levels.
        /// </summary>
        /// <param name="name">
        /// The name of the metadata to remove.
        /// </param>
        public void RemoveMetadata(string name)
        {
            foreach (Dictionary<string, string> set in this._metadataSets)
            {
                while (set.ContainsKey(name))
                {
                    set.Remove(name);
                }
            }
        }

        /// <summary>
        /// Removes any metadata with the given name on this item from the specified metadata
        /// levels.
        /// </summary>
        /// <param name="name">
        /// The name of the metadata to remove.
        /// </param>
        /// <param name="level">
        /// The metadata level that the metadata will be removed from.
        /// </param>
        public void RemoveMetadata(string name, MetadataLevel level)
        {
            Dictionary<string, string> set = this._metadataSets[(int)level];
            while (set.ContainsKey(name))
            {
                set.Remove(name);
            }
        }

        /// <summary>
        /// Sets the metadata value for this item with the specified name to the specified
        /// value and on the specified metadata level.
        /// </summary>
        /// <param name="name">
        /// The name of the metadata value to set.
        /// </param>
        /// <param name="value">
        /// The value to set the metadata to.
        /// </param>
        /// <param name="level">
        /// The level at which the metadata will be set on.
        /// </param>
        public void SetMetadata(string name, string value, MetadataLevel level)
        {
            this._metadataSets[(int)level][name] = value;
        }

        /// <summary>
        /// Updates this items include path based on the new file path for this item.
        /// </summary>
        /// <param name="path">
        /// The new full path for this item.
        /// </param>
        /// <returns>
        /// True if the include path has been updated due to the path change; otherwise, false.
        /// </returns>
        public bool UpdateIncludeValueFromPathChange(string path)
        {
            string previous = this.Include;
            if (this._scope == null)
            {
                this.Include = Path.GetFullPath(path);
                return !String.Equals(previous, this.Include);
            }

            string directory = this._scope.DirectoryPath;
            if (!String.IsNullOrWhiteSpace(directory))
            {
                this.Include = Shlwapi.MakeRelativeFromDirectoryToFile(directory, path);
            }
            else
            {
                this.Include = Path.GetFileName(path);
            }

            return !String.Equals(previous, this.Include);
        }

        /// <summary>
        /// Updates this items include path based on the new file path for this item.
        /// </summary>
        /// <param name="oldPath">
        /// The new full file path of this item.
        /// </param>
        public void UpdateIncludeValueFromScopePathChange(string oldPath)
        {
            string path = null;
            if (oldPath == null)
            {
                path = this.Include;
            }
            else
            {
                path = Path.Combine(Path.GetDirectoryName(oldPath), this.Include);
                path = Path.GetFullPath(path);
            }

            if (this._scope == null)
            {
                this.Include = Path.GetFullPath(path);
                return;
            }

            string directory = this._scope.DirectoryPath;
            if (!String.IsNullOrWhiteSpace(directory))
            {
                this.Include = Shlwapi.MakeRelativeFromDirectoryToFile(directory, path);
            }
            else
            {
                this.Include = Path.GetFileName(path);
            }
        }

        /// <summary>
        /// Determines whether this item contains metadata value for the specified metadata
        /// level.
        /// </summary>
        /// <param name="level">
        /// The metadata level to test.
        /// </param>
        /// <returns>
        /// True if this item contains metadata values for the specified metadata level;
        /// otherwise, false.
        /// </returns>
        internal virtual bool ContainsData(MetadataLevel level)
        {
            return this._metadataSets[(int)level].Count > 0;
        }

        /// <summary>
        /// Deserialises metadata values from the specified reader and stores them for this
        /// project item for the specified metadata level.
        /// </summary>
        /// <param name="reader">
        /// The reader containing the data for the metadata values.
        /// </param>
        /// <param name="level">
        /// The level that the parsed metadata will be stored as.
        /// </param>
        internal void DeserialiseMetadata(XmlReader reader, MetadataLevel level)
        {
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            int index = (int)level;
            while (reader.MoveToContent() == XmlNodeType.Element)
            {
                this._metadataSets[index][reader.Name] = reader.ReadElementContentAsString();
            }

            if (reader.NodeType == XmlNodeType.EndElement)
            {
                reader.ReadEndElement();
            }
        }

        /// <summary>
        /// Serialises the data contained within this instance onto the specified xml writer.
        /// </summary>
        /// <param name="writer">
        /// The xml writer this instance will be serialised to.
        /// </param>
        /// <param name="level">
        /// The metadata level of the values that need serialising.
        /// </param>
        internal void Serialise(XmlWriter writer, MetadataLevel level)
        {
            writer.WriteAttributeString("Include", this.Include);
            foreach (KeyValuePair<string, string> metadata in this._metadataSets[(int)level])
            {
                writer.WriteElementString(metadata.Key, metadata.Value);
            }
        }

        /// <summary>
        /// Retrieves an array with the built in metadata keys. This can be overridden to add
        /// custom built in keys.
        /// </summary>
        /// <returns>
        /// An array containing the built in metadata keys.
        /// </returns>
        protected virtual string[] GetBuiltInMetadataKeys()
        {
            return new string[] { "Fullpath" };
        }

        /// <summary>
        /// Retrieves a metadata value for the specified built in metadata keys. The available
        /// keys are restricted to the ones returned by <see cref="GetBuiltInMetadataKeys"/>.
        /// </summary>
        /// <param name="name">
        /// The name of the metadata value to retrieve.
        /// </param>
        /// <returns>
        /// The metadata value for the built in metadata with the specified name.
        /// </returns>
        protected virtual string GetBuiltinMetadataValue(string name)
        {
            if (String.Equals("FullPath", name, StringComparison.OrdinalIgnoreCase))
            {
                if (this._scope == null)
                {
                    return Path.GetFullPath(this.Include);
                }

                string fullpath = Path.Combine(this._scope.DirectoryPath, this.Include);
                return Path.GetFullPath(fullpath);
            }

            Debug.Assert(false, "Unrecognised built-in metadata value");
            return null;
        }
        #endregion Methods
    } // RSG.Project.Model.ProjectItem {Class}
} // RSG.Project.Model {Namespace}
