﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common.Map;
using RSG.Model.Common;
using System.ComponentModel;
using RSG.Base.Math;
using RSG.SceneXml;
using System.Diagnostics;
using RSG.Statistics.Common.Dto.GameAssetStats;
using RSG.Base.Logging;

namespace RSG.Model.Map3
{
    /// <summary>
    /// 
    /// </summary>
    public class StatedAnimArchetype : MapArchetypeBase, IStatedAnimArchetype
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        [StreamableStatAttribute(StreamableStatedAnimArchetypeStat.ComponentArchetypesString)]
        public ICollection<IMapArchetype> ComponentArchetypes
        {
            get
            {
                CheckStatLoaded(StreamableStatedAnimArchetypeStat.ComponentArchetypes);
                return m_componentArchetypes;
            }
            private set
            {
                SetPropertyValue(ref m_componentArchetypes, value, "ComponentArchetypes");
                SetStatLodaded(StreamableStatedAnimArchetypeStat.ComponentArchetypes, true);
            }
        }
        private ICollection<IMapArchetype> m_componentArchetypes;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public StatedAnimArchetype(string name, IMapSection parent)
            : base(name, parent)
        {
        }
        #endregion // Constructor(s)
        
        #region IMapArchetype Implementation
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sceneObject"></param>
        /// <param name="scene"></param>
        public override void LoadStatsFromExportData(TargetObjectDef sceneObject, Scene scene)
        {
            Debug.Assert(false, "Programmer error. Use LoadStatsFromExportData() instead.");
            throw new NotSupportedException("Programmer error. Use LoadStatsFromExportData() instead.");
        }

        /// <summary>
        /// Load basic stats from the scene xml object def
        /// </summary>
        /// <param name="sceneObject"></param>
        /// <param name="scene"></param>
        public void LoadStatsFromExportData(List<TargetObjectDef> objectDefs, Scene scene)
        {
            BoundingBox = new BoundingBox3f();
            LodDistance = 0.0f;
            Shaders = new IShader[0];
            ExportGeometrySize = 0;
            PolygonCount = 0;
            CollisionPolygonCount = 0;
            CollisionTypePolygonCounts = new Dictionary<CollisionType, uint>();
            HasAttachedLight = false;
            HasExplosiveEffect = false;

            // Categorise the list of object defs we received
            List<TargetObjectDef> fragmentDefs = new List<TargetObjectDef>();
            List<TargetObjectDef> drawableDefs = new List<TargetObjectDef>();
            CategoriseObjects(objectDefs, ref fragmentDefs, ref drawableDefs);

            // Process any child archetypes
            IList<IMapArchetype> childArchetypes = new List<IMapArchetype>();
            foreach (TargetObjectDef fragmentDef in fragmentDefs)
            {
                IFragmentArchetype fragment = new FragmentArchetype(fragmentDef.Name, ParentSection);
                (fragment as FragmentArchetype).LoadStatsFromExportData(fragmentDef, scene);
                childArchetypes.Add(fragment);
            }

            foreach (TargetObjectDef drawableDef in drawableDefs)
            {
                IDrawableArchetype drawable = new DrawableArchetype(drawableDef.Name, ParentSection);
                (drawable as DrawableArchetype).LoadStatsFromExportData(drawableDef, scene);
                childArchetypes.Add(drawable);
            }

            ComponentArchetypes = childArchetypes;

            // Keep track of which txd's we've already added to the stats
            TxdExportSizes = new Dictionary<string, uint>();
            HashSet<IShader> shaders = new HashSet<IShader>();

            foreach (IMapArchetype childArchetype in childArchetypes)
            {
                ExportGeometrySize += childArchetype.ExportGeometrySize;
                
                foreach (IShader shader in childArchetype.Shaders)
                {
                    shaders.Add(shader);
                }

                if (childArchetype.PolygonCount > PolygonCount)
                {
                    PolygonCount = childArchetype.PolygonCount;
                    CollisionPolygonCount = childArchetype.CollisionPolygonCount;
                    BoundingBox = childArchetype.BoundingBox;
                }

                foreach (KeyValuePair<string, uint> txdPair in childArchetype.TxdExportSizes)
                {
                    if (!TxdExportSizes.ContainsKey(txdPair.Key))
                    {
                        TxdExportSizes.Add(txdPair);
                    }
                }
                

                if (childArchetype.LodDistance > LodDistance)
                {
                    LodDistance = childArchetype.LodDistance;
                }

                foreach (KeyValuePair<CollisionType, uint> pair in childArchetype.CollisionTypePolygonCounts)
                {
                    if (!CollisionTypePolygonCounts.ContainsKey(pair.Key))
                    {
                        CollisionTypePolygonCounts.Add(pair.Key, 0);
                    }
                    CollisionTypePolygonCounts[pair.Key] += pair.Value;
                }

                if (childArchetype.HasAttachedLight == true)
                {
                    HasAttachedLight = true;
                }

                if (childArchetype.HasExplosiveEffect == true)
                {
                    HasExplosiveEffect = true;
                }
            }

            Shaders = shaders.ToArray();
        }
        #endregion // IArchetype Implementation

        #region SimpleMapArchetypeBase Overrides
        /// <summary>
        /// Load specific statistics for this map section
        /// </summary>
        /// <param name="statsToLoad"></param>
        /// <param name="async"></param>
        public override void RequestStatistics(IEnumerable<StreamableStat> statsToLoad, bool async)
        {
            ParentSection.MapHierarchy.RequestStatisticsForStatedAnim(this, statsToLoad, async);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="stat"></param>
        protected override void ProcessComplexArchetypeStat(ArchetypeStatDto dto, StreamableStat stat)
        {
            StatedAnimArchetypeStatDto statedAnimDto = dto as StatedAnimArchetypeStatDto;
            Debug.Assert(statedAnimDto != null, "Incorrect dto type encountered.");

            if (stat == StreamableStatedAnimArchetypeStat.ComponentArchetypes)
            {
                ProcessComponentArchetypeStats(statedAnimDto.ComponentArchetypeStats);
            }
            else
            {
                base.ProcessComplexArchetypeStat(dto, stat);
            }
        }
        #endregion // SimpleMapArchetypeBase Overrides

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="objectDefs"></param>
        /// <param name="fragments"></param>
        /// <param name="drawables"></param>
        private void CategoriseObjects(List<TargetObjectDef> objectDefs, ref List<TargetObjectDef> fragments, ref List<TargetObjectDef> drawables)
        {
            foreach (TargetObjectDef sceneObject in objectDefs)
            {
                // Only track objects with an anim state
                if (sceneObject.Attributes.ContainsKey(AttrNames.OBJ_ANIM_STATE) && sceneObject.Attributes[AttrNames.OBJ_ANIM_STATE] is String)
                {
                    if (sceneObject.IsCloth())
                    {
                        if (sceneObject.DrawableLOD != null && (sceneObject.DrawableLOD.Children != null && sceneObject.DrawableLOD.Children.Count() != 0))
                        {
                            continue;
                        }

                        drawables.Add(sceneObject);
                    }
                    else if (sceneObject.IsFragment())
                    {
                        if (sceneObject.DrawableLOD != null && (sceneObject.DrawableLOD.Children != null && sceneObject.DrawableLOD.Children.Count() != 0))
                        {
                            continue;
                        }

                        fragments.Add(sceneObject);
                    }
                    else if (sceneObject.IsObject() && !sceneObject.IsXRef() && !sceneObject.IsRefObject() && !sceneObject.IsInternalRef() && !sceneObject.IsRefInternalObject()) // A definition and posible instance
                    {
                        if (sceneObject.DrawableLOD != null && (sceneObject.DrawableLOD.Children != null && sceneObject.DrawableLOD.Children.Count() != 0))
                        {
                            continue;
                        }

                        drawables.Add(sceneObject);
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="archetypeDtos"></param>
        private void ProcessComponentArchetypeStats(List<BasicArchetypeStatDto> archetypeDtos)
        {
            IList<IMapArchetype> childArchetypes = new List<IMapArchetype>();
            
            lock (ParentSection.MapHierarchy.ArchetypeLookup)
            {
                foreach (BasicArchetypeStatDto archetypDto in archetypeDtos)
                {
                    if (ParentSection.MapHierarchy.ArchetypeLookup.ContainsKey(archetypDto.Name.ToLower()))
                    {
                        IMapArchetype archetype = ParentSection.MapHierarchy.ArchetypeLookup[archetypDto.Name.ToLower()];
                        childArchetypes.Add(archetype);
                    }
                    else
                    {
                        Log.Log__Warning("Archetype '{0}' wasn't found in the archetype look up for stated anim '{1}'.", archetypDto.Name, Name);
                    }
                }
            }

            ComponentArchetypes = childArchetypes;
        }
        #endregion // Private Methods

        #region Object Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return (String.Format("Stated Anim Archetype: [{0}]{1}", this.Name, this.Hash));
        }
        #endregion // Object Overrides
    } // StatedAnimArchetype
}
