﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common.Map
{
    /// <summary>
    /// Enumeration of all statistics that a vehicle can have associated with it
    /// </summary>
    [DependentStat(StreamableMapSectionStat.EntitiesString)]
    public static class StreamableEntityStat
    {
        public const String PositionString = "E2B8ED54-911B-4B3B-87BF-32AD3F8F6986";
        public const String BoundingBoxString = "EA4CC668-84BE-44AC-A5B7-F6201A027A47";
        public const String PriorityLevelString = "C5932565-FFDE-4EDC-A937-DDB055C9EC66";
        public const String IsLowPriorityString = "425DB729-7EB1-4AAA-BB0C-63A848E66455";
        public const String IsReferenceString = "82AC2AFC-B606-49B6-8456-A569B2B47A97";
        public const String IsInteriorReferenceString = "8AB58E72-057E-4A98-AA8A-214B52323CC3";
        public const String LodLevelString = "4EA713A8-A990-4768-BA85-B6EC0CFAEA91";
        public const String LodDistanceOverrideString = "C0248540-B5B6-4494-98E3-C9F8C955315E";
        public const String LodParentString = "76D1874F-DE50-4779-AC27-58505C2F1782";
        public const String ForceBakeCollisionString = "08D5CF23-D8CD-4F5E-95EA-27BAD19A8CBA";
        public const String SpawnPointsString = "87495084-86CB-4772-BF2E-0AA50BA8F76F";
        public const String LodDistanceForChildrenString = "53CE9A5B-29BD-4CB1-A7E3-12E315C73A4D";
        public const String AttributeGuidString = "6D4B17FA-50D7-404E-8031-8E4D3EAC39A7";
        public const String HasScalingString = "C120EEDF-7949-4AA5-85AF-E624B643E371";
        public const String HasSLOD2LinkString = "5C981295-88DE-4D8A-958E-155F55F5459E";
        public const String RageLightInstanceCountString = "14FEF05A-E800-4DF2-B349-79B559FA3ABE";
        public const String RotationString = "F6F019EF-8BDA-4E61-9D95-86A98FA94D7D";
        public const String TransformString = "CA3273D5-F839-44FC-8E1F-8D3F072DE684";

        public static readonly StreamableStat Position = new StreamableStat(PositionString, true, true, false);
        public static readonly StreamableStat BoundingBox = new StreamableStat(BoundingBoxString, true, true, false);
        public static readonly StreamableStat PriorityLevel = new StreamableStat(PriorityLevelString, true);
        public static readonly StreamableStat IsLowPriority = new StreamableStat(IsLowPriorityString, true);
        public static readonly StreamableStat IsReference = new StreamableStat(IsReferenceString, true);
        public static readonly StreamableStat IsInteriorReference = new StreamableStat(IsInteriorReferenceString, true);
        public static readonly StreamableStat LodLevel = new StreamableStat(LodLevelString, true, true, false);
        public static readonly StreamableStat LodDistanceOverride = new StreamableStat(LodDistanceOverrideString, true);
        public static readonly StreamableStat LodParent = new StreamableStat(LodParentString, true, true);
        public static readonly StreamableStat ForceBakeCollision = new StreamableStat(ForceBakeCollisionString, true);
        public static readonly StreamableStat SpawnPoints = new StreamableStat(SpawnPointsString, false);
        public static readonly StreamableStat LodDistanceForChildren = new StreamableStat(LodDistanceForChildrenString, false);
        public static readonly StreamableStat AttributeGuid = new StreamableStat(AttributeGuidString, false);
        public static readonly StreamableStat HasScaling= new StreamableStat(HasScalingString, false);
        public static readonly StreamableStat HasSLOD2Link = new StreamableStat(HasSLOD2LinkString, false);
        public static readonly StreamableStat RageLightInstanceCount = new StreamableStat(RageLightInstanceCountString, false);
        public static readonly StreamableStat Rotation = new StreamableStat(RotationString, false);
        public static readonly StreamableStat Transform = new StreamableStat(TransformString, false);
    } // StreamableEntityStat
}
