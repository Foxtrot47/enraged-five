﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common.Map
{
    /// <summary>
    /// Drawable/fragment
    /// </summary>
    public interface ISimpleMapArchetype : IMapArchetype
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        new ITexture[] Textures { get; }

        /// <summary>
        /// 
        /// </summary>
        bool IsDynamic { get; }

        /// <summary>
        /// 
        /// </summary>
        bool IsFixed { get; }

        /// <summary>
        /// Obtained from the AttrNames.OBJ_COLLISION_GROUP attribute.
        /// </summary>
        string CollisionGroup { get; }

        /// <summary>
        /// Whether the collision group this archetype is in is the default group.
        /// </summary>
        bool HasDefaultCollisionGroup { get; }

        /// <summary>
        /// Obtained from the AttrNames.OBJ_FORCE_BAKE_COLLISION attribute.
        /// </summary>
        bool ForceBakeCollision { get; }

        /// <summary>
        /// Obtained from the AttrNames.OBJ_NEVER_BAKE_COLLISION attribute.
        /// </summary>
        bool NeverBakeCollision { get; }

        /// <summary>
        /// List of spawn points associated with this archetype.
        /// </summary>
        ICollection<ISpawnPoint> SpawnPoints { get; }
        
        ///<summary>
        /// Drawable LODs this archetype has
        /// </summary>
        IDictionary<MapDrawableLODLevel, IMapDrawableLOD> DrawableLODs { get; }
        #endregion // Properties
    } // ISimpleMapArchetype
}
