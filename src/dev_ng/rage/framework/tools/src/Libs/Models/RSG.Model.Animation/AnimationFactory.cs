﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common.Animation;
using RSG.Statistics.Client.ServiceClients;
using RSG.Statistics.Common.Dto.GameAssets;
using System.IO;
using Ionic.Zip;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using System.Diagnostics;
using RSG.Base.Logging;
using RSG.Model.Common;
using RSG.Platform;
using RSG.Pipeline.Core;
using System.Xml.Linq;
using System.Xml.XPath;

namespace RSG.Model.Animation
{
    /// <summary>
    /// 
    /// </summary>
    public class AnimationFactory : IAnimationFactory
    {
        #region Constants
        /// <summary>
        /// Directory to parse for getting the base list of cutscene names.
        /// </summary>
        private const String c_cutsceneDirectory = "$(export)/anim/cutscene";

        /// <summary>
        /// Location of the cutscene exclusion file.
        /// </summary>
        private const String c_cutsceneExclusionFile = "$(toolsconfig)/config/cutscene/dev/excludedscenes.txt";

        /// <summary>
        /// File which contains additional information for cutscenes (mission links/friendly names).
        /// </summary>
        private const String c_cutsceneExtraDataFile = "$(toolsconfig)/config/statistics/cutscenes.xml";

        /// <summary>
        /// Wildcard to use when searching for cutscene part zips.
        /// </summary>
        private const String c_cutscenePartZipWildcard = "*.icd.zip";

        /// <summary>
        /// Directory that contains the concat files for cutscenes.
        /// </summary>
        private const String c_cutsceneConcatDirectory = "$(assets)/cuts/!!Cutlists";

        /// <summary>
        /// Extension that the cutscene concat files use.
        /// </summary>
        private const String c_cutsceneConcatExtension = ".concatlist";

        /// <summary>
        /// Location of the animation files from which to source the local clip dictionary collection
        /// </summary>
        private const String c_animationDirectory = "$(export)/anim";

        /// <summary>
        /// Extension used when searching for animation zips
        /// </summary>
        private const String c_animationZipExtension = ".icd.zip";

        /// <summary>
        /// Extension to search for within an animation zip file for an Animation.
        /// </summary>
        private const String c_animationExtension = ".anim";

        /// <summary>
        /// Extension to search for within an animation zip file for a Clip.
        /// </summary>
        private const String c_clipExtension = ".clip";

        /// <summary>
        /// Extension to search for in the cutscene zip file
        /// </summary>
        private const String c_cutsceneExtension = ".cut";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Private log object.
        /// </summary>
        private ILog Log { get; set; }

        /// <summary>
        /// Branch configuration object.
        /// </summary>
        private IBranch Branch { get; set; }

        /// <summary>
        /// Content tree reference.
        /// </summary>
        private IContentTree ContentTree { get; set; }
        #endregion
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public AnimationFactory(IBranch branch, IContentTree contentTree)
        {
            Log = LogFactory.CreateUniversalLog("Animation Factory");
            Branch = branch;
            ContentTree = contentTree;
        }
        #endregion // Constructor(s)

        #region IAnimationFactory Implementation
        /// <summary>
        /// Creates a new cutscene collection from the specified source.
        /// </summary>
        /// <param name="source">Source to use for creating the collection.</param>
        /// <param name="buildIdentifier">Optional build identifier for when the source is Database.</param>
        public ICutsceneCollection CreateCutsceneCollection(DataSource source, String buildIdentifier = null)
        {
            ICutsceneCollection collection = null;

            if (source == DataSource.SceneXml)
            {
                Log.Profile("AnimationFactory:CreateCutsceneCollection:SceneXml");
                collection = CreateCutsceneCollectionFromExportData();
                Log.ProfileEnd();
            }
            else if (source == DataSource.Database)
            {
                Log.Profile("AnimationFactory:CreateCutsceneCollection:Database");
                collection = CreateCutsceneCollectionFromDatabase(buildIdentifier);
                Log.ProfileEnd();
            }
            else
            {
                Debug.Assert(false, String.Format("Unknown data source '{0}' specified while creating a cutscene collection.", source.ToString()));
                throw new ArgumentException(String.Format("Unknown data source '{0}' specified while creating a cutscene collection.", source.ToString()));
            }

            return collection;
        }
        
        /// <summary>
        /// Creates a collection of clip dictionaries from the specified source.
        /// </summary>
        /// <param name="source">Source to use for creating the collection.</param>
        /// <param name="buildIdentifier">Optional build identifier for when the source is Database.</param>
        public IClipDictionaryCollection CreateClipDictionaryCollection(DataSource source, String buildIdentifier = null)
        {
            IClipDictionaryCollection collection = null;

            if (source == DataSource.SceneXml)
            {
                Log.Profile("AnimationFactory:CreateClipDictionaryCollection:SceneXml");
                collection = CreateClipDictionaryCollectionFromExportData();
                Log.ProfileEnd();
            }
            else if (source == DataSource.Database)
            {
                Log.Profile("AnimationFactory:CreateClipDictionaryCollection:Database");
                collection = CreateClipDictionaryCollectionFromDatabase(buildIdentifier);
                Log.ProfileEnd();
            }
            else
            {
                Debug.Assert(false, String.Format("Unknown data source '{0}' specified while creating a clip dictionary collection.", source.ToString()));
                throw new ArgumentException(String.Format("Unknown data source '{0}' specified while creating a clip dictionary collection.", source.ToString()));
            }

            return collection;
        }

        /// <summary>
        /// Creates animation clip dictionary collections for RSEXPORT/anim/cutscene
        /// </summary>
        /// <returns></returns>
        public IEnumerable<IClipDictionaryCollection> CreateCutsceneClipDictionaryCollectionsFromExportData()
        {
            return CreateClipDictionaryCollectionsFromExportData("cutscene", ClipDictionaryCategory.Cutscene);
        }

        /// <summary>
        /// Creates animation clip dictionary collections for RSEXPORT/anim/ingame
        /// </summary>
        /// <returns></returns>
        public IEnumerable<IClipDictionaryCollection> CreateInGameClipDictionaryCollectionsFromExportData()
        {
            return CreateClipDictionaryCollectionsFromExportData("ingame", ClipDictionaryCategory.InGame);
        }
        #endregion // IAnimationFactory Implementation

        #region Private Methods
        /// <summary>
        /// Parses the list of cutscene parts in the cutscene directory creating a cutscene for all of those that
        /// don't exist in the exclusions file.
        /// </summary>
        private ICutsceneCollection CreateCutsceneCollectionFromExportData()
        {
            // Parse the exclusions file to get the list of cutscene parts to ignore.
            String exclusionsFile = Branch.Project.Environment.Subst(c_cutsceneExclusionFile);
            Debug.Assert(File.Exists(exclusionsFile), "Cutscenes exclusion file doesn't exist on disk.");
            if (!File.Exists(exclusionsFile))
            {
                throw new FileNotFoundException("Cutscenes exclusion file doesn't exist on disk.", exclusionsFile);
            }
            ISet<String> excludedScenes = new HashSet<String>();
            excludedScenes.AddRange(
                File.ReadAllLines(exclusionsFile)
                    .Where(item => !String.IsNullOrWhiteSpace(item))
                    .Select(item => item.ToLower()));

            // Next go over all the cutscene parts that exist in the cutscene directory.
            String cutsceneDirectory = Branch.Environment.Subst(c_cutsceneDirectory);
            Debug.Assert(Directory.Exists(cutsceneDirectory), "Cutscene directory doesn't exist on disk.");
            if (!Directory.Exists(cutsceneDirectory))
            {
                throw new DirectoryNotFoundException("Cutscene directory doesn't exist on disk.");
            }

            // Generate the lookup for cutscene name to mission/friendly names.
            String extraDataFile = Branch.Project.Environment.Subst(c_cutsceneExtraDataFile);
            Debug.Assert(File.Exists(extraDataFile), "Cutscenes extra data file doesn't exist on disk.");
            if (!File.Exists(extraDataFile))
            {
                throw new FileNotFoundException("Cutscenes extra data file doesn't exist on disk.", exclusionsFile);
            }
            IDictionary<String, Tuple<String, String, bool>> dataLookup = new Dictionary<String, Tuple<String, String, bool>>();

            XDocument doc = XDocument.Load(extraDataFile);
            foreach (XElement missionElem in doc.XPathSelectElements("/cutscenes/mission"))
            {
                String missionId = null;
                XAttribute idAtt = missionElem.Attribute("id");
                if (idAtt != null)
                {
                    missionId = idAtt.Value;
                }

                foreach (XElement cutsceneElem in missionElem.Elements("cutscene"))
                {
                    XAttribute nameAtt = cutsceneElem.Attribute("name");
                    XAttribute friendlyNameAtt = cutsceneElem.Attribute("friendly_name");
                    XAttribute hasBranchAtt = cutsceneElem.Attribute("has_branch");
                    if (nameAtt != null)
                    {
                        String cutsceneName = nameAtt.Value;
                        String friendlyName = null;
                        bool hasBranch = false;
                        if (friendlyNameAtt != null && !String.IsNullOrEmpty(friendlyNameAtt.Value))
                        {
                            friendlyName = friendlyNameAtt.Value;
                        }
                        if (hasBranchAtt != null)
                        {
                            hasBranch = Boolean.Parse(hasBranchAtt.Value);
                        }

                        dataLookup[cutsceneName.ToLower()] = Tuple.Create(friendlyName, missionId, hasBranch);
                    }
                }
            }

            // Create the collection itself.
            LocalCutsceneCollection cutscenes = new LocalCutsceneCollection();
            foreach (String directory in Directory.GetDirectories(cutsceneDirectory))
            {
                foreach (String cutscenePartFile in Directory.GetFiles(directory, c_cutscenePartZipWildcard))
                {
                    String cutscenePartName = Filename.GetBasename(cutscenePartFile).ToLower();

                    // Only add this cutscene to the list if it isn't in the excluded scenes list.
                    if (!excludedScenes.Contains(cutscenePartName))
                    {
                        String partFilename = Path.Combine(Branch.Assets, "cuts", cutscenePartName, "data.cutxml");
                        String missionId = null;
                        String friendlyName = null;
                        bool hasBranch = false;
                        if (dataLookup.ContainsKey(cutscenePartName))
                        {
                            friendlyName = dataLookup[cutscenePartName].Item1;
                            missionId = dataLookup[cutscenePartName].Item2;
                            hasBranch = dataLookup[cutscenePartName].Item3;
                        }

                        Cutscene cutscene = LocalCutscene.CreateFromCutscenePart(new CutscenePart(cutscenePartName, partFilename),
                            friendlyName, missionId, hasBranch, cutscenePartFile, Branch, ContentTree);
                        cutscenes.Add(cutscene);
                    }
                }
            }

            // We also need to add cutscenes for all those that are present in the concat list directory.
            String concatsDirectory = Branch.Environment.Subst(c_cutsceneConcatDirectory);
            Debug.Assert(Directory.Exists(concatsDirectory), "Cutscene concats directory doesn't exist on disk.");
            if (!Directory.Exists(concatsDirectory))
            {
                throw new DirectoryNotFoundException("Cutscene concats directory doesn't exist on disk.");
            }

            foreach (String concatFile in Directory.GetFiles(concatsDirectory, String.Format("*{0}", c_cutsceneConcatExtension)))
            {
                String cutsceneName = Path.GetFileNameWithoutExtension(concatFile).ToLower();
                String missionId = null;
                String friendlyName = null;
                bool hasBranches = false;
                if (dataLookup.ContainsKey(cutsceneName))
                {
                    friendlyName = dataLookup[cutsceneName].Item1;
                    missionId = dataLookup[cutsceneName].Item2;
                    hasBranches = dataLookup[cutsceneName].Item3;
                }

                try
                {
                    Cutscene cutscene = LocalCutscene.CreateFromConcatsFile(concatFile, friendlyName, missionId, hasBranches, Branch, ContentTree);
                    if (cutscene != null)
                    {
                        cutscenes.Add(cutscene);
                    }
                }
                catch (System.Exception ex)
                {
                    Log.Exception(ex, "Failed to create cutscene off of the {0} concats file.", concatFile);
                }
            }

            return cutscenes;
        }

        /// <summary>
        /// 
        /// </summary>
        private ICutsceneCollection CreateCutsceneCollectionFromDatabase(String buildIdentifier)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        private IClipDictionaryCollection CreateClipDictionaryCollectionFromExportData()
        {
            IClipDictionaryCollection collection = new ClipDictionaryCollection("Clip Dictionaries");

            // 3 Places in the anim folder to get clip dictionaries from: clipmax, cutscene and ingame.
            String animPath = Branch.Environment.Subst(c_animationDirectory);

            collection.ClipDictionaries.AddRange(GetClipDictionariesInDirectory(Path.Combine(animPath, "clipmax"), ClipDictionaryCategory.ClipMax));
            collection.ClipDictionaries.AddRange(GetClipDictionariesForCutscenes());
            collection.ClipDictionaries.AddRange(GetClipDictionariesInDirectory(Path.Combine(animPath, "ingame"), ClipDictionaryCategory.InGame));

            // Look in these two export folders as well: levels, models
            String exportPath = Branch.Export;

            collection.ClipDictionaries.AddRange(GetClipDictionariesInDirectory(Path.Combine(exportPath, "levels"), ClipDictionaryCategory.Other));
            collection.ClipDictionaries.AddRange(GetClipDictionariesInDirectory(Path.Combine(exportPath, "models"), ClipDictionaryCategory.Other));

            return collection;
        }

        /// <summary>
        /// 
        /// </summary>
        private IClipDictionaryCollection CreateClipDictionaryCollectionFromDatabase(String buildIdentifier)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        private IEnumerable<IClipDictionary> GetClipDictionariesInDirectory(String dir, ClipDictionaryCategory category)
        {
            IList<IClipDictionary> clipDictionaries = new List<IClipDictionary>();

            // Process the icd files first
            foreach (String icdFile in Directory.GetFiles(dir, String.Format("*{0}", c_animationZipExtension), SearchOption.AllDirectories))
            {
                IClipDictionary clipDict = CreateClipDictionaryFromFile(Filename.GetBasename(icdFile), File.OpenRead(icdFile), category);
                if (clipDict != null)
                {
                    clipDictionaries.Add(clipDict);
                }
            }

            // Process any zip files next.
            foreach (String zipFile in Directory.GetFiles(dir, "*.zip", SearchOption.AllDirectories))
            {
                String[] extensions = Filename.GetExtensions(zipFile);
                if (extensions.Length > 1)
                {
                    continue;
                }

                try
                {
                    using (ZipFile zipZip = ZipFile.Read(zipFile))
                    {
                        foreach (ZipEntry entry in zipZip.Entries)
                        {
                            extensions = Filename.GetExtensions(entry.FileName);
                            if (extensions.Length == 2 && extensions[0] == ".zip" && extensions[1] == ".icd")
                            {
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    entry.Extract(stream);
                                    stream.Position = 0;

                                    IClipDictionary clipDict = CreateClipDictionaryFromFile(Filename.GetBasename(entry.FileName), stream, category);
                                    if (clipDict != null)
                                    {
                                        clipDictionaries.Add(clipDict);
                                    }
                                }
                            }
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    Log.Exception(ex, "Failed to process the '{0}' zip file.", zipFile);
                }
            }

            return clipDictionaries;
        }

        /// <summary>
        /// 
        /// </summary>
        private IClipDictionary CreateClipDictionaryFromFile(String dictName, Stream fileStream, ClipDictionaryCategory category)
        {
            try
            {
                IClipDictionary clipDictionary = new ClipDictionary(dictName, category);

                using (ZipFile icdZip = ZipFile.Read(fileStream))
                {
                    foreach (ZipEntry entry in icdZip.Entries.Where(entry => String.Compare(Path.GetExtension(entry.FileName), c_clipExtension, true) == 0))
                    {
                        Clip clip = new Clip(Filename.GetBasename(entry.FileName));
                        // don't load from zip - too slow - just ASSUME for now that a side by side anim exists ( this is assumed in the Clip model object )
                        clipDictionary.Add(clip);
                    }
                }

                return clipDictionary;
            }
            catch (System.Exception ex)
            {
                Log.Exception(ex, "Failed to create clip dictionary '{0}' from stream.", dictName);
            }
            return null;
        }

        /// <summary>
        /// Creates a collection of clip dictionaries based off of the cutscene data.
        /// </summary>
        /// <returns></returns>
        private IEnumerable<IClipDictionary> GetClipDictionariesForCutscenes()
        {
            // Iterate over all the .icd.zip files in the processed cutscene directory.
            String cutsceneProcessedPath = Path.Combine(Branch.Processed, "anim", "cutscene");

            foreach (String icdFile in System.IO.Directory.GetFiles(cutsceneProcessedPath, "*.icd.zip", SearchOption.AllDirectories))
            {
                using (FileStream fileStream = File.OpenRead(icdFile))
                {
                    IClipDictionary clipDictionary = CreateClipDictionaryFromFile(Filename.GetBasename(icdFile), fileStream, ClipDictionaryCategory.Cutscene);
                    if (clipDictionary  != null)
                    {
                        yield return clipDictionary;
                    }
                }
            }
        }

        /// <summary>
        /// Scanning the export directory, create the model objects representing the clip dictionary collections contained within.
        /// - This assumes that the folders are flat and contain only a flat list of fodler herein.
        /// </summary>
        /// <param name="folder">the folder to scan under RSEXPORT/anim/ eg. "ingame"</param>
        /// <returns></returns>
        private IEnumerable<IClipDictionaryCollection> CreateClipDictionaryCollectionsFromExportData(String topFolder, ClipDictionaryCategory category)
        {
            List<IClipDictionaryCollection> clipDictionaryCollections = new List<IClipDictionaryCollection>();

            // Make the folder name to scan
            string dirPath = Branch.Project.Environment.Subst(c_animationDirectory);
            string scanPath = Path.Combine(dirPath, topFolder);

            if (Directory.Exists(scanPath))
            {
                string[] directories = Directory.GetDirectories(scanPath, "*", SearchOption.TopDirectoryOnly);
                foreach (string directory in directories)
                {
                    string dir = Path.Combine(dirPath, directory);
                    ClipDictionaryCollection clipDictionaryCollection = new ClipDictionaryCollection(dir);
                    string[] filenames = Directory.GetFiles(dir, String.Format("*{0}", c_animationZipExtension), SearchOption.TopDirectoryOnly);

                    foreach (string filename in filenames)
                    {
                        ClipDictionary clipDictionary = new ClipDictionary(Filename.GetBasename(filename), category);

                        using (ZipFile sectionZip = ZipFile.Read(filename))
                        {
                            foreach (ZipEntry entry in sectionZip.Entries.Where(entry => String.Compare(Path.GetExtension(entry.FileName), c_clipExtension) == 0))
                            {
                                Clip clip = new Clip(Filename.GetBasename(entry.FileName));
                                // don't load from zip - too slow - just ASSUME for now that a side by side anim exists ( this is assumed in the Clip model object )
                                clipDictionary.Add(clip);
                            }
                        }

                        clipDictionaryCollection.Add(clipDictionary);
                    }

                    clipDictionaryCollections.Add(clipDictionaryCollection);
                }
            }

            return clipDictionaryCollections;
        }
        #endregion // Private Methods
    } // AnimationFactory
}
