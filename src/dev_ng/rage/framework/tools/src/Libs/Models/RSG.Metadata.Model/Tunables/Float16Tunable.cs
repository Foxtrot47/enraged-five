﻿//---------------------------------------------------------------------------------------------
// <copyright file="Float16Tunable.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Tunables
{
    using System;
    using System.ComponentModel;
    using System.Xml;
    using RSG.Base.Extensions;
    using RSG.Base.Logging;
    using RSG.Editor;
    using RSG.Editor.Model;
    using RSG.Metadata.Model.Definitions.Members;
    using RSG.Metadata.Model.Resources;

    /// <summary>
    /// Represents a tunable used by the parCodeGen system that is instancing a
    /// <see cref="Float16Member"/> object.
    /// </summary>
    public class Float16Tunable : TunableBase
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Value"/> property.
        /// </summary>
        private float _value;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="Float16Tunable"/> class to be as
        /// instance of the specified member.
        /// </summary>
        /// <param name="member">
        /// The member that this tunable will be instancing.
        /// </param>
        /// <param name="parent">
        /// The tunable parent this tunable will belong to.
        /// </param>
        public Float16Tunable(Float16Member member, ITunableParent parent)
            : base(member, parent)
        {
            this._value = member.InitialValue;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Float16Tunable"/> class as a copy of
        /// the specified instance.
        /// </summary>
        /// <param name="other">
        /// The instance to copy.
        /// </param>
        /// <param name="parent">
        /// The tunable parent this tunable will belong to.
        /// </param>
        public Float16Tunable(Float16Tunable other, ITunableParent parent)
            : base(other, parent)
        {
            this._value = other._value;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Float16Tunable"/> class using the
        /// specified System.Xml.XmlReader as a initialising data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <param name="member">
        /// The member that this tunable will be instancing.
        /// </param>
        /// <param name="parent">
        /// The tunable parent this tunable will belong to.
        /// </param>
        /// <param name="log">
        /// A object that is used to log any warnings or errors produced during the parsing of
        /// the specified reader.
        /// </param>
        public Float16Tunable(
            XmlReader reader, Float16Member member, ITunableParent parent, ILog log)
            : base(reader, member, parent)
        {
            this.Deserialise(reader, log);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the float member that this tunable is instancing.
        /// </summary>
        public Float16Member Float16Member
        {
            get
            {
                Float16Member member = this.Member as Float16Member;
                if (member != null)
                {
                    return member;
                }

                return new Float16Member(this.TunableStructure.Definition);
            }
        }

        /// <summary>
        /// Gets or sets the value assigned to this float tunable.
        /// </summary>
        public float Value
        {
            get { return this._value; }
            set { this.SetProperty(ref this._value, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new <see cref="Float16Tunable"/> that is a copy of this instance.
        /// </summary>
        /// <returns>
        /// A new <see cref="Float16Tunable"/> that is a copy of this instance.
        /// </returns>
        public new Float16Tunable Clone()
        {
            return new Float16Tunable(this, this.TunableStructure);
        }

        /// <summary>
        /// Creates a deep copy of the current instance.
        /// </summary>
        /// <returns>
        /// A deep copy of the current System.Object.
        /// </returns>
        public override object DeepClone()
        {
            return this.Clone();
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="ITunable"/> by
        /// looking at their values.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance by their values;
        /// otherwise, false.
        /// </returns>
        public override bool EqualByValue(ITunable other)
        {
            return this.EqualByValue(other as Float16Tunable);
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="ITunable"/> by
        /// looking at their values.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance by their values;
        /// otherwise, false.
        /// </returns>
        public bool EqualByValue(Float16Tunable other)
        {
            if (other == null)
            {
                return false;
            }

            return this.Value == other.Value;
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified
        /// <see cref="Float16Tunable"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public bool Equals(Float16Tunable other)
        {
            if (other == null)
            {
                return false;
            }

            return Object.ReferenceEquals(other, this);
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="ITunable"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public override bool Equals(ITunable other)
        {
            return this.Equals(other as Float16Tunable);
        }

        /// <summary>
        /// Gets the string representation of this tunables value.
        /// </summary>
        /// <returns>
        /// A string that represents this tunables value.
        /// </returns>
        public string GetScalarRepresentation()
        {
            return this.GetScalarRepresentation(this.Value);
        }

        /// <summary>
        /// Resets this tunable to have its default value. (i.e no source data with its initial
        /// values).
        /// </summary>
        public override void ResetValueToDefaultCore()
        {
            Float16Tunable source = this.InheritanceParent as Float16Tunable;
            if (source != null)
            {
                this.Value = source.Value;
            }
            else
            {
                this.Value = this.Float16Member.InitialValue;
            }
        }

        /// <summary>
        /// Writes the xml representation of this instance onto the specified
        /// System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that the xml representation of this instance should be
        /// written to.
        /// </param>
        /// <param name="serialiseDefaultTunables">
        /// A value indicating whether the tunables that have a default value should be written
        /// to the specified writer or not.
        /// </param>
        public override void Serialise(XmlWriter writer, bool serialiseDefaultTunables)
        {
            if (writer == null)
            {
                throw new SmartArgumentNullException(() => writer);
            }

            writer.WriteAttributeString("value", this.GetScalarRepresentation());
        }

        /// <summary>
        /// Returns the string scalar representation of this tunable. This string is used
        /// within an array.
        /// </summary>
        /// <returns>
        /// The string scalar representation.
        /// </returns>
        public override string ToString()
        {
            return this.GetScalarRepresentation(this.Value);
        }

        /// <summary>
        /// Validates this entity and creates a validation result object containing all of the
        /// errors and warnings.
        /// </summary>
        /// <param name="recursive">
        /// A value indicating whether any child objects should also be validated.
        /// </param>
        /// <returns>
        /// A new validation object containing all of the errors and warnings.
        /// </returns>
        public override ValidationResult Validate(bool recursive)
        {
            ValidationResult result = new ValidationResult();
            if (this.Value > this.Float16Member.Maximum)
            {
                string value = this.Value.ToStringInvariant();
                string maximum = this.Float16Member.Maximum.ToStringInvariant();
                string msg = StringTable.GreaterThanMaximum.FormatInvariant(value, maximum);

                result.AddError("Value", msg);
            }

            if (this.Value < this.Float16Member.Minimum)
            {
                string value = this.Value.ToStringInvariant();
                string minimum = this.Float16Member.Minimum.ToStringInvariant();
                string msg = StringTable.LessThanMinimum.FormatInvariant(value, minimum);

                result.AddError("Value", msg);
            }

            return result;
        }

        /// <summary>
        /// Called whenever the tunable whose value will be used when this tunable is being
        /// inherited changes.
        /// </summary>
        /// <param name="oldValue">
        /// The old inheritance parent.
        /// </param>
        /// <param name="newValue">
        /// The new inheritance parent.
        /// </param>
        protected override void InheritanceParentChanged(ITunable oldValue, ITunable newValue)
        {
            string name = "Value";
            if (oldValue != null)
            {
                PropertyChangedEventManager.RemoveHandler(
                    oldValue, this.OnInheritedValueChanged, name);
            }

            Float16Tunable source = newValue as Float16Tunable;
            if (source == null)
            {
                throw new NotSupportedException(
                    "Only the smae type can be an inheritance parent.");
            }

            PropertyChangedEventManager.AddHandler(source, this.OnInheritedValueChanged, name);
            if (this.HasDefaultValue)
            {
                this._value = source.Value;
                this.NotifyPropertyChanged("Value");
            }
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        /// <param name="log">
        /// A object that is used to log any warnings or errors produced during the parsing of
        /// the specified reader.
        /// </param>
        private void Deserialise(XmlReader reader, ILog log)
        {
            if (reader == null)
            {
                return;
            }

            IXmlLineInfo lineInfo = reader as IXmlLineInfo;
            int line = lineInfo.LineNumber;
            int pos = lineInfo.LinePosition;

            try
            {
                if (!reader.IsEmptyElement)
                {
                    string msg = StringTable.TunableInnerXmlError;
                    log.Warning(msg, "float16", line, pos);
                }

                if (reader.AttributeCount > 1)
                {
                    log.Warning(StringTable.FloatAttributeCountError, line, pos);
                }

                string value = reader.GetAttribute("value");
                if (value == null)
                {
                    log.Warning(StringTable.FloatTunableValueMissingError, line, pos);
                }
                else
                {
                    float fallback = this.Float16Member.InitialValue;
                    var result = this.Dictionary.TryTo<float>(value, fallback);
                    this._value = result.Value;
                }
            }
            catch (Exception ex)
            {
                string msg = StringTable.FloatTunableDeserialiseError;
                throw new MetadataException(msg, line, pos, ex.Message);
            }

            reader.Skip();
        }

        /// <summary>
        /// Gets the string representation of this tunables value.
        /// </summary>
        /// <param name="value">
        /// The value to convert to the string.
        /// </param>
        /// <returns>
        /// A string that represents this tunables value.
        /// </returns>
        private string GetScalarRepresentation(float value)
        {
            if (this.Float16Member.HighPrecision)
            {
                return value.ToStringInvariant("F9");
            }
            else
            {
                return value.ToStringInvariant("F6");
            }
        }

        /// <summary>
        /// Called whenever the value of the inheritance parent changes so that the values can
        /// be kept in sync.
        /// </summary>
        /// <param name="sender">
        /// The inheritance parent.
        /// </param>
        /// <param name="e">
        /// The event data.
        /// </param>
        private void OnInheritedValueChanged(object sender, PropertyChangedEventArgs e)
        {
            if (!this.HasDefaultValue)
            {
                return;
            }

            Float16Tunable source = this.InheritanceParent as Float16Tunable;
            if (source != null)
            {
                this._value = source.Value;
                this.NotifyPropertyChanged("Value");
            }
        }
        #endregion Methods
    } // RSG.Metadata.Model.Tunables.Float16Tunable {Class}
} // RSG.Metadata.Model.Tunables {Namespace}
