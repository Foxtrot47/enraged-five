﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.GlobalTXD
{
    /// <summary>
    /// Represents a object that can be found in the global texture dictionary hierarchy
    /// </summary>
    interface IDictionaryChild
    {
        /// <summary>
        /// The parent container that conatins this object
        /// </summary>
        IDictionaryContainer Parent { get; }

        /// <summary>
        /// The root object for this object
        /// </summary>
        GlobalRoot Root { get; }

    }
}
