﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common.Animation
{
    /// <summary>
    /// 
    /// </summary>
    public interface ICutsceneCollection : IEnumerable<ICutscene>, ICollection<ICutscene>
    {
    } // ICutsceneCollection
}
