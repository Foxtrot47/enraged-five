﻿namespace RSG.Model.Weapon.Game
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using RSG.Base.Configuration;
    using RSG.Metadata.Data;
    using RSG.Metadata.Model;
    using RSG.Metadata.Parser;

    /// <summary>
    /// Represents all of the weapons that are being mounted by the game through the
    /// waepons.meta data file and its associated meta files.
    /// </summary>
    public class GameWeapons
    {
        #region Fields
        /// <summary>
        /// The name of the weapons metadata file that contains the data mounted by the game
        /// that defines all of the weapons used in the game.
        /// </summary>
        private const string weaponFilename = @"data\ai\weapons.meta";
        
        /// <summary>
        /// The name of the weapons metadata file that contains the data defining information
        /// about each weapon component used in the game.
        /// </summary>
        private const string componentFilename = @"data\ai\weaponcomponents.meta";

        /// <summary>
        /// The name of the weapons metadata file that contains the data defining the archetype
        /// specific information for each weapon.
        /// </summary>
        private const string archetypeFilename = @"data\weaponArchetypes.meta";

        /// <summary>
        /// The name of the weapons metadata file that contains the data defining the different
        /// clip sets that are valid for wach weapon.
        /// </summary>
        private const string animationFilename = @"data\ai\weaponanimations.meta";

        /// <summary>
        /// The private field used for the <see cref="Weapons"/> property.
        /// </summary>
        private List<GameWeapon> _weapons;

        /// <summary>
        /// The private field used for the <see cref="WeaponComponents"/> property.
        /// </summary>
        private List<GameWeaponComponent> _weaponComponents;

        /// <summary>
        /// The private field used for the <see cref="WeaponAmmos"/> property.
        /// </summary>
        private List<GameWeaponAmmo> _weaponAmmos;

        /// <summary>
        /// The private field used for the <see cref="WeaponArchetypes"/> property.
        /// </summary>
        private List<GameWeaponArchetype> _weaponArchetypes;

        /// <summary>
        /// The private field used for the <see cref="WeaponAnimations"/> property.
        /// </summary>
        private List<GameWeaponAnimationSet> _weaponAnimations;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="GameWeapons"/> class using the
        /// specified game branch to locate the data files used during loading.
        /// </summary>
        /// <param name="branch">
        /// The branch that the data should be loaded from.
        /// </param>
        /// <param name="structures">
        /// The structure dictionary containing the metadata definitions to use to parse the
        /// metadata files.
        /// </param>
        public GameWeapons(IBranch branch, StructureDictionary structures)
        {
            this._weapons = new List<GameWeapon>();
            string weaponsmeta = Path.Combine(branch.Common, weaponFilename);
            string componentmeta = Path.Combine(branch.Common, componentFilename);
            string archetypemeta = Path.Combine(branch.Common, archetypeFilename);
            string animationmeta = Path.Combine(branch.Common, animationFilename);

            if (File.Exists(archetypemeta))
            {
                this.ParseArchetypeMetadata(archetypemeta, structures);
            }

            if (File.Exists(componentmeta))
            {
                this.ParseComponentMetadata(componentmeta, structures);
            }

            if (File.Exists(animationmeta))
            {
                this.ParseAnimationMetadata(animationmeta, structures);
            }

            if (File.Exists(weaponsmeta))
            {
                this.ParseWeaponsMetadata(weaponsmeta, structures);
            }
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a iterator around all of the game weapons loaded through this instance.
        /// </summary>
        public IEnumerable<GameWeapon> Weapons
        {
            get
            {
                if (this._weapons == null)
                {
                    yield break;
                }

                foreach (GameWeapon weapon in this._weapons)
                {
                    yield return weapon;
                }
            }
        }

        /// <summary>
        /// Gets a iterator around all of the game weapons loaded through this instance.
        /// </summary>
        public IEnumerable<string> UsedTextureDictionaries
        {
            get
            {
                if (this._weaponArchetypes == null)
                {
                    yield break;
                }

                foreach (GameWeaponArchetype archetype in this._weaponArchetypes)
                {
                    if (archetype == null || archetype.ReferenceCount == 0)
                    {
                        continue;
                    }

                    yield return archetype.TextureDictionaryName;
                }
            }
        }

        /// <summary>
        /// Gets a iterator around all of the game weapons loaded through this instance.
        /// </summary>
        public IEnumerable<string> UsedModels
        {
            get
            {
                if (this._weaponArchetypes == null)
                {
                    yield break;
                }

                foreach (GameWeaponArchetype archetype in this._weaponArchetypes)
                {
                    if (archetype == null || archetype.ReferenceCount == 0)
                    {
                        continue;
                    }

                    yield return archetype.Model;
                }
            }
        }

        /// <summary>
        /// Gets a iterator around all of the models that are referenced but don't have
        /// associated archetypes defined for them.
        /// </summary>
        public IEnumerable<string> UnfoundArchetypeDefinitions
        {
            get
            {
                foreach (GameWeapon weapon in this._weapons)
                {
                    if (String.IsNullOrEmpty(weapon.Model))
                    {
                        continue;
                    }

                    if (weapon.Archetype == null)
                    {
                        yield return weapon.Model;
                    }
                }

                foreach (GameWeaponAmmo ammo in this._weaponAmmos)
                {
                    if (String.IsNullOrEmpty(ammo.Model))
                    {
                        continue;
                    }

                    if (ammo.Archetype == null)
                    {
                        yield return ammo.Model;
                    }
                }

                foreach (GameWeaponComponent component in this._weaponComponents)
                {
                    if (String.IsNullOrEmpty(component.Model))
                    {
                        continue;
                    }

                    if (component.Archetype == null)
                    {
                        yield return component.Model;
                    }
                }
            }
        }

        /// <summary>
        /// Gets a iterator around all of the models that are referenced but don't have
        /// associated archetypes defined for them.
        /// </summary>
        public IEnumerable<string> UnfoundComponentDefinitions
        {
            get
            {
                foreach (GameWeapon weapon in this._weapons)
                {
                    foreach (KeyValuePair<string, GameWeaponComponent> component in weapon.Components)
                    {
                        if (component.Value == null)
                        {
                            yield return component.Key;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets a iterator around all of the defined weapon archetypes that are not being
        /// referenced through the weapons.meta file.
        /// </summary>
        public IEnumerable<GameWeaponArchetype> UnreferencedArchetypeDefinitions
        {
            get
            {
                return from a in this._weaponArchetypes
                       where a.ReferenceCount == 0
                       select a;
            }
        }

        /// <summary>
        /// Gets a iterator around all of the defined weapon ammo types that are not being
        /// referenced through the weapons.meta file.
        /// </summary>
        public IEnumerable<GameWeaponAmmo> UnreferencedAmmoDefinitions
        {
            get
            {
                return from a in this._weaponAmmos
                       where a.ReferenceCount == 0
                       select a;
            }
        }

        /// <summary>
        /// Gets a iterator around all of the defined weapon components that are not being
        /// referenced through the weapons.meta file.
        /// </summary>
        public IEnumerable<GameWeaponComponent> UnreferencedComponentDefinitions
        {
            get
            {
                return from c in this._weaponComponents
                       where c.ReferenceCount == 0
                       select c;
            }
        }

        /// <summary>
        /// Gets a iterator around all of the defined weapon animation sets that are not being
        /// referenced through the weapons.meta file.
        /// </summary>
        public IEnumerable<GameWeaponAnimationSet> UnreferencedAnimationDefinitions
        {
            get
            {
                return from a in this._weaponAnimations
                       where a.ReferenceCount == 0
                       select a;
            }
        }

        /// <summary>
        /// Gets the definied game weapon with the specified name.
        /// </summary>
        /// <param name="name">
        /// The name of the game weapon to retrieve.
        /// </param>
        /// <returns>
        /// The game weapon with the specified name if found; otherwise, null.
        /// </returns>
        public GameWeapon this[string name]
        {
            get
            {
                foreach (GameWeapon weapon in this._weapons)
                {
                    if (String.CompareOrdinal(name, weapon.Name) == 0)
                    {
                        return weapon;
                    }
                }

                return null;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Parses the weapons metadata file.
        /// </summary>
        /// <param name="filename">
        /// The name of the file to parse.
        /// </param>
        /// <param name="structures">
        /// The structure dictionary containing the metadata definitions to use to parse the
        /// specified file.
        /// </param>
        private void ParseWeaponsMetadata(string filename, StructureDictionary structures)
        {
            MetaFile metadata = new MetaFile(filename, structures);

            Structure ammoStructure = structures["CAmmoInfo"];
            List<StructureTunable> weaponAmmos = new List<StructureTunable>();
            foreach (StructureTunable t in metadata.AllMembers.OfType<StructureTunable>())
            {
                StructMember m = t.Definition as StructMember;
                if (m == null || m.Definition == null)
                {
                    continue;
                }

                if (!m.Definition.IsA(ammoStructure))
                {
                    continue;
                }

                weaponAmmos.Add(t);
            }

            this._weaponAmmos = new List<GameWeaponAmmo>(weaponAmmos.Count);
            for (int i = 0; i < weaponAmmos.Count; i++)
            {
                GameWeaponAmmo ammo = new GameWeaponAmmo();
                foreach (StringTunable child in weaponAmmos[i].Values.OfType<StringTunable>())
                {
                    if (string.Compare(child.Name, "Name", true) == 0)
                    {
                        ammo.Name = child.Value;
                    }
                    else if (string.Compare(child.Name, "Model", true) == 0)
                    {
                        ammo.Model = child.Value;
                    }
                    else if (child.Name.Contains("Fx"))
                    {
                        ammo.AddEffect(child.Value);
                    }
                }

                if (ammo.Model != null)
                {
                    GameWeaponArchetype archetype = (from a in _weaponArchetypes
                                                     where string.Compare(a.Model, ammo.Model, true) == 0
                                                     select a).FirstOrDefault();

                    if (archetype != null)
                    {
                        ammo.Archetype = archetype;
                    }
                }

                this._weaponAmmos.Add(ammo);
            }

            List<StructureTunable> weaponInfos = new List<StructureTunable>();
            foreach (StructureTunable t in metadata.AllMembers.OfType<StructureTunable>())
            {
                StructMember m = t.Definition as StructMember;
                if (m == null || m.Definition == null)
                {
                    continue;
                }

                if (string.CompareOrdinal(m.Definition.DataType, "CWeaponInfo") != 0)
                {
                    continue;
                }

                weaponInfos.Add(t);
            }

            this._weapons = new List<GameWeapon>(weaponInfos.Count);
            for (int i = 0; i < weaponInfos.Count; i++)
            {
                this._weapons.Add(new GameWeapon());
                this.ParseWeaponInfo(i, weaponInfos[i]);
            }
        }

        /// <summary>
        /// Parses the weapons component metadata file.
        /// </summary>
        /// <param name="filename">
        /// The name of the file to parse.
        /// </param>
        /// <param name="structures">
        /// The structure dictionary containing the metadata definitions to use to parse the
        /// specified file.
        /// </param>
        private void ParseComponentMetadata(string filename, StructureDictionary structures)
        {
            MetaFile metadata = new MetaFile(filename, structures);
            List<StructureTunable> weaponComponents = new List<StructureTunable>();
            Structure componentStructure = structures["CWeaponComponentInfo"];
            if (componentStructure == null)
            {
                return;
            }

            foreach (StructureTunable t in metadata.AllMembers.OfType<StructureTunable>())
            {
                StructMember m = t.Definition as StructMember;
                if (m == null || m.Definition == null)
                {
                    continue;
                }

                if (!m.Definition.IsA(componentStructure))
                {
                    continue;
                }

                weaponComponents.Add(t);
            }

            this._weaponComponents = new List<GameWeaponComponent>(weaponComponents.Count);
            for (int i = 0; i < weaponComponents.Count; i++)
            {
                GameWeaponComponent component = new GameWeaponComponent();
                foreach (StringTunable child in weaponComponents[i].Values.OfType<StringTunable>())
                {
                    if (string.Compare(child.Name, "Name", true) == 0)
                    {
                        component.Name = child.Value;
                    }
                    else if (string.Compare(child.Name, "Model", true) == 0)
                    {
                        component.Model = child.Value;
                    }
                    else if (child.Name.Contains("Fx"))
                    {
                        component.AddEffect(child.Value);
                    }
                }

                if (component.Model != null)
                {
                    GameWeaponArchetype archetype = (from a in _weaponArchetypes
                                                     where string.Compare(a.Model, component.Model, true) == 0
                                                     select a).FirstOrDefault();

                    if (archetype != null)
                    {
                        component.Archetype = archetype;
                    }
                }

                this._weaponComponents.Add(component);
            }
        }

        /// <summary>
        /// Parses the weapons archetype file.
        /// </summary>
        /// <param name="filename">
        /// The name of the file to parse.
        /// </param>
        /// <param name="structures">
        /// The structure dictionary containing the metadata definitions to use to parse the
        /// specified file.
        /// </param>
        private void ParseArchetypeMetadata(string filename, StructureDictionary structures)
        {
            MetaFile metadata = new MetaFile(filename, structures);
            List<StructureTunable> modelInfos = new List<StructureTunable>();
            foreach (StructureTunable t in metadata.AllMembers.OfType<StructureTunable>())
            {
                StructMember m = t.Definition as StructMember;
                if (m == null || m.Definition == null)
                {
                    continue;
                }

                if (string.CompareOrdinal(m.Definition.DataType, "CWeaponModelInfo::InitData") != 0)
                {
                    continue;
                }

                modelInfos.Add(t);
            }

            this._weaponArchetypes = new List<GameWeaponArchetype>(modelInfos.Count);
            foreach (StructureTunable modelInfo in modelInfos)
            {
                string modelName = null;
                string txdName = null;
                string ptfxAssetName = null;
                foreach (StringTunable child in modelInfo.Values.OfType<StringTunable>())
                {
                    if (string.Compare(child.Name, "modelName", true) == 0)
                    {
                        modelName = child.Value;
                    }
                    else if (string.Compare(child.Name, "txdName", true) == 0)
                    {
                        txdName = child.Value;
                    }
                    else if (string.Compare(child.Name, "ptfxAssetName", true) == 0)
                    {
                        if (child.Value != "null")
                        {
                            ptfxAssetName = child.Value;
                        }
                    }
                }

                GameWeaponArchetype archetype = new GameWeaponArchetype();
                archetype.Model = modelName;
                archetype.TextureDictionaryName = txdName;
                archetype.EffectsDictionaryName = ptfxAssetName;
                this._weaponArchetypes.Add(archetype);
            }
        }

        /// <summary>
        /// Parses the weapons animation file.
        /// </summary>
        /// <param name="filename">
        /// The name of the file to parse.
        /// </param>
        /// <param name="structures">
        /// The structure dictionary containing the metadata definitions to use to parse the
        /// specified file.
        /// </param>
        private void ParseAnimationMetadata(string filename, StructureDictionary structures)
        {
            MetaFile metadata = new MetaFile(filename, structures);
            List<MapTunable> animationSets = new List<MapTunable>();
            foreach (MapTunable t in metadata.AllMembers.OfType<MapTunable>())
            {
                if (string.CompareOrdinal(t.Name, "WeaponAnimations") != 0)
                {
                    continue;
                }

                animationSets.Add(t);
            }

            this._weaponAnimations = new List<GameWeaponAnimationSet>(animationSets.Count);
            foreach (MapTunable modelInfo in animationSets)
            {
                foreach (MapItemTunable item in modelInfo.Items)
                {
                    GameWeaponAnimationSet set = new GameWeaponAnimationSet();
                    set.Name = item.Key;
                    StructureTunable value = item.Value as StructureTunable;
                    if (value != null)
                    {
                        foreach (StringTunable child in value.Values.OfType<StringTunable>())
                        {
                            if (string.IsNullOrEmpty(child.Value) || !child.Name.Contains("ClipSet"))
                            {
                                continue;
                            }

                            set.AddClipSet(child.Value);
                        }
                    }

                    this._weaponAnimations.Add(set);
                }
            }
        }
        

        /// <summary>
        /// Parses a single weapons info structure tunable.
        /// </summary>
        /// <param name="index">
        /// The index of the <see cref="GameWeapon"/> object that is being modified with the
        /// parsed data.
        /// </param>
        /// <param name="tunable">
        /// The structure tunable object to parse.
        /// </param>
        private void ParseWeaponInfo(int index, StructureTunable tunable)
        {
            GameWeapon weapon = this._weapons[index];
            foreach (StringTunable child in tunable.Values.OfType<StringTunable>())
            {
                if (string.Compare(child.Name, "Name", true) == 0)
                {
                    weapon.Name = child.Value;
                }
                else if (string.Compare(child.Name, "Model", true) == 0)
                {
                    weapon.Model = child.Value;
                }
            }

            foreach (StructureTunable child in tunable.Values.OfType<StructureTunable>())
            {
                if (string.Compare(child.Name, "Fx", true) == 0)
                {
                    foreach (StringTunable fxChild in tunable.OfType<StringTunable>())
                    {
                        if (!fxChild.Name.Contains("Fx"))
                        {
                            continue;
                        }

                        weapon.AddEffect(fxChild.Value);
                    }

                    break;
                }
            }

            List<StructureTunable> components = new List<StructureTunable>();
            foreach (ArrayTunable child in tunable.Values.OfType<ArrayTunable>())
            {
                if (string.Compare(child.Name, "AttachPoints", true) != 0)
                {
                    continue;
                }

                foreach (StructureTunable item in child.Items.OfType<StructureTunable>())
                {
                    foreach (ArrayTunable components1 in item.Values.OfType<ArrayTunable>())
                    {
                        if (string.Compare(components1.Name, "Components", true) != 0)
                        {
                            continue;
                        }

                        foreach (StructureTunable component in components1.Items.OfType<StructureTunable>())
                        {
                            StructMember m = component.Definition as StructMember;
                            if (string.CompareOrdinal(m.Definition.DataType, "CWeaponComponentPoint::sComponent") != 0)
                            {
                                continue;
                            }

                            components.Add(component);
                        }
                    }
                }

                break;
            }

            foreach (StructureTunable t in components)
            {
                foreach (StringTunable child in t.Values.OfType<StringTunable>())
                {
                    if (string.Compare(child.Name, "Name", true) != 0)
                    {
                        continue;
                    }

                    GameWeaponComponent component = (from c in this._weaponComponents
                                                     where c.Name == child.Value
                                                     select c).FirstOrDefault();

                    weapon.AddComponent(child.Value, component);

                    break;
                }
            }


            foreach (PointerTunable child in tunable.Values.OfType<PointerTunable>())
            {
                if (string.Compare(child.Name, "AmmoInfo", true) == 0)
                {
                    string ammoInfo = child.Value as string;
                    if (ammoInfo == "NULL")
                    {
                        break;
                    }

                    weapon.Ammo = (from a in _weaponAmmos
                                   where string.Compare(a.Name, ammoInfo, true) == 0
                                   select a).FirstOrDefault();

                    break;
                }
            }

            foreach (GameWeaponAnimationSet animationSet in this._weaponAnimations)
            {
                if (string.Compare(animationSet.Name, weapon.Name) == 0)
                {
                    weapon.AddAnimationSet(animationSet);
                }
            }

            if (weapon.Model != null)
            {
                weapon.Archetype = (from a in _weaponArchetypes
                                    where string.Compare(a.Model, weapon.Model, true) == 0
                                    select a).FirstOrDefault();
            }
        }
        #endregion Methods
    } // RSG.Model.Weapon.Game.GameWeapons {Class}
} // RSG.Model.Weapon.Game {Namespace}
