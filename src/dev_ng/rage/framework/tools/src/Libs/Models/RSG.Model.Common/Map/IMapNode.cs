﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common.Map
{
    /// <summary>
    /// A node of the map hierarchy
    /// </summary>
    public interface IMapNode : IAsset
    {
        /// <summary>
        /// Map hierarchy this node is a part of.
        /// </summary>
        IMapHierarchy MapHierarchy { get; }

        /// <summary>
        /// Level this node is a part of.
        /// </summary>
        ILevel Level { get; }

        /// <summary>
        /// Returns this map node's parent.  Only the root level nodes can have a null parent.
        /// </summary>
        IMapArea ParentArea { get; }
    } // IMapNode
}
