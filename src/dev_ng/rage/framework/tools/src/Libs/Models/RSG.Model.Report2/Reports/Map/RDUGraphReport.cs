﻿namespace RSG.Model.Report2.Reports.Map
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net.Mail;
    using RSG.Base.Configuration;
    using RSG.Base.Configuration.Bugstar;
    using RSG.Base.Tasks;
    using RSG.Interop.Bugstar;
    using RSG.Interop.Bugstar.Organisation;
    using RSG.Interop.Bugstar.Search;
    using RSG.Model.Report;
    using RSG.SourceControl.Perforce;

    /// <summary>
    /// 
    /// </summary>
    public class RDUGraphReport : CSVReport, IDynamicReport, IReportMailProvider
    {
        #region Fields
        /// <summary>
        /// The name of this report.
        /// </summary>
        private const string ReportName = "RDU Containers";

        /// <summary>
        /// The description for this report.
        /// </summary>
        private const string ReportDescription = "Bugstar Outsource Report";

        /// <summary>
        /// The private field used for the <see cref="DefaultEmailList"/> property.
        /// </summary>
        private string defaultEmailList;

        /// <summary>
        /// The private field used for the <see cref="EmailSender"/> property.
        /// </summary>
        private string emailSender;

        /// <summary>
        /// The private field used for the <see cref="GraphName"/> property.
        /// </summary>
        private string graphName;

        /// <summary>
        /// The private field used for the <see cref="GenerationTask"/> property.
        /// </summary>
        private ITask generationTask;

        /// <summary>
        /// The private field used for the <see cref="MailMessage"/> property.
        /// </summary>
        private MailMessage mailMessage;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="RDUGraphReport"/> class.
        /// </summary>
        public RDUGraphReport()
        {
            this.Name = ReportName;
            this.Description = ReportDescription;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the mail message to send when the report is invoked
        /// </summary>
        public MailMessage MailMessage
        {
            get { return this.mailMessage; }
            private set { this.mailMessage = value; }
        }

        /// <summary>
        /// Gets the task to run for generating this report.
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (this.generationTask == null)
                {
                    this.generationTask = new ActionTask(
                        "Generating report", this.GenerateReport);
                }

                return this.generationTask;
            }
        }

        /// <summary>
        /// Gets or sets the separated list of the default people to email.
        /// </summary>
        public string DefaultEmailList
        {
            get { return this.defaultEmailList; }
            set { this.defaultEmailList = value; }
        }

        /// <summary>
        /// Gets or sets the email address for the sender of the email.
        /// </summary>
        public string EmailSender
        {
            get { return this.emailSender; }
            set { this.emailSender = value; }
        }

        /// <summary>
        /// Gets or sets the graph name the bugstar data will be retrieved from.
        /// </summary>
        public string GraphName
        {
            get { return this.graphName; }
            set { this.graphName = value; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Generates the report dynamically.
        /// </summary>
        /// <param name="context">
        /// The context for this asynchronous task which includes the cancellation token.
        /// </param>
        /// <param name="progress">
        /// The progress object to use to report the progress of the generation.
        /// </param>
        private void GenerateReport(ITaskContext context, IProgress<TaskProgress> progress)
        {
            DynamicReportContext dynamicContext = context as DynamicReportContext;
            if (dynamicContext == null)
            {
                return;
            }

            String currDir = Directory.GetCurrentDirectory();
            RSG.SourceControl.Perforce.P4 p4 = new RSG.SourceControl.Perforce.P4();

            try
            {
                IBugstarConfig bugstarConfig = ConfigFactory.CreateBugstarConfig();
                BugstarConnection connection = new BugstarConnection(bugstarConfig.RESTService, bugstarConfig.AttachmentService);
                connection.Login(bugstarConfig.ReadOnlyUsername, bugstarConfig.ReadOnlyPassword, "");

                Project bugstarProject = Project.GetProjectById(connection, bugstarConfig.ProjectId);
                User bugstarUser = bugstarProject.Users.FirstOrDefault(u => u.UserName == bugstarConfig.ReadOnlyUsername);

                Graph outsourceGraph = null;
                foreach (Graph graph in bugstarUser.Graphs)
                {
                    if (graph.Name != this.GraphName)
                    {
                        continue;
                    }

                    outsourceGraph = graph;
                    break;
                }

                if (outsourceGraph == null)
                {
                    return;
                }

                p4.Connect();
                Stream exportStream = this.GetExportStream(p4, outsourceGraph);

                using (Stream fileStream = new FileStream(this.Filename, FileMode.Create))
                {
                    exportStream.Position = 0;
                    exportStream.CopyTo(fileStream);
                }

                this.GenerateEmail(exportStream);
            }
            catch (System.Exception ex)
            {
                RSG.Base.Logging.Log.Log__Exception(ex, "Unhandled exception");
            }
            finally
            {
                if (p4 != null)
                {
                    p4.Disconnect();
                }

                Directory.SetCurrentDirectory(currDir);
            }
        }

        /// <summary>
        /// Retrieves the stream that is the csv file so that it can be saved and attached to
        /// the email.
        /// </summary>
        /// <param name="outsourceGraph">
        /// The bugstar graph that contains the data used to create the report.
        /// </param>
        /// <returns>
        /// The stream that contains the data of the csv report.
        /// </returns>
        private Stream GetExportStream(P4 p4, Graph outsourceGraph)
        {
            IConfig config = ConfigFactory.CreateConfig();
            string artDirectory = config.Project.DefaultBranch.Art;
            string cityE = Path.Combine(artDirectory, "Models", "_CityE", "...", "*.maxc");
            string cityW = Path.Combine(artDirectory, "Models", "_CityW", "...", "*.maxc");
            string hills = Path.Combine(artDirectory, "Models", "_Hills", "...", "*.maxc");
            string prologue = Path.Combine(artDirectory, "Models", "_prologue", "...", "*.maxc");

            Dictionary<string, FileState> fileStates = new Dictionary<string, FileState>();
            foreach (FileState fileState in FileState.Create(p4, cityE))
            {
                if (fileState.HeadAction == FileAction.Delete ||
                    fileState.HeadAction == FileAction.MoveAndDelete)
                {
                    continue;
                }

                if (fileState.ClientFilename.Contains("WIP") || fileState.ClientFilename.Contains("Old_Max_Files"))
                {
                    continue;
                }

                string name = Path.GetFileNameWithoutExtension(fileState.ClientFilename);
                fileStates.Add(name.ToLower(), fileState);
            }

            foreach (FileState fileState in FileState.Create(p4, cityW))
            {
                if (fileState.HeadAction == FileAction.Delete ||
                    fileState.HeadAction == FileAction.MoveAndDelete)
                {
                    continue;
                }

                if (fileState.ClientFilename.Contains("WIP") || fileState.ClientFilename.Contains("Old_Max_Files"))
                {
                    continue;
                }

                string name = Path.GetFileNameWithoutExtension(fileState.ClientFilename);
                fileStates.Add(name.ToLower(), fileState);
            }

            foreach (FileState fileState in FileState.Create(p4, hills))
            {
                if (fileState.HeadAction == FileAction.Delete ||
                    fileState.HeadAction == FileAction.MoveAndDelete)
                {
                    continue;
                }

                if (fileState.ClientFilename.Contains("WIP") || fileState.ClientFilename.Contains("Old_Max_Files"))
                {
                    continue;
                }

                string name = Path.GetFileNameWithoutExtension(fileState.ClientFilename);
                fileStates.Add(name.ToLower(), fileState);
            }

            foreach (FileState fileState in FileState.Create(p4, prologue))
            {
                if (fileState.HeadAction == FileAction.Delete ||
                    fileState.HeadAction == FileAction.MoveAndDelete)
                {
                    continue;
                }

                if (fileState.ClientFilename.Contains("WIP") || fileState.ClientFilename.Contains("Old_Max_Files"))
                {
                    continue;
                }

                string name = Path.GetFileNameWithoutExtension(fileState.ClientFilename);
                fileStates.Add(name.ToLower(), fileState);
            }

            SortedDictionary<int, List<string>> sortedData = new SortedDictionary<int, List<string>>();
            foreach (KeyValuePair<string, int> data in outsourceGraph.DataPoints)
            {
                List<string> sections = null;
                if (!sortedData.TryGetValue(data.Value, out sections))
                {
                    sections = new List<string>();
                    sortedData.Add(data.Value, sections);
                }

                sections.Add(data.Key);
            }

            Stream stream = new MemoryStream();
            StreamWriter sw = new StreamWriter(stream);
            string header = "Container Name,Bug Count,Container Checked Out,Prop Group Checked Out";
            sw.WriteLine(header);


            foreach (KeyValuePair<int, List<string>> data in sortedData.Reverse())
            {
                int bugCount = data.Key;
                foreach (string section in data.Value)
                {
                    string containerName = section.ToLower();
                    string propContainerName = containerName + "_props";

                    string line = string.Format("{0},{1},", section, bugCount.ToString());
                    bool containerCheckedOut = false;
                    FileState containerState = null;
                    if (!fileStates.TryGetValue(containerName, out containerState))
                    {
                        containerCheckedOut = false;
                    }
                    else
                    {
                        containerCheckedOut =
                            containerState.OtherOpen ||
                            containerState.OpenAction == FileAction.Edit;
                    }

                    bool propContainerCheckedOut = false;
                    FileState propContainerState = null;
                    if (!fileStates.TryGetValue(propContainerName, out propContainerState))
                    {
                        propContainerCheckedOut = false;
                    }
                    else
                    {
                        propContainerCheckedOut =
                            propContainerState.OtherOpen ||
                            propContainerState.OpenAction == FileAction.Edit;
                    }

                    if (propContainerCheckedOut && containerCheckedOut)
                    {
                        continue;
                    }

                    line += string.Format("{0},{1}", containerCheckedOut.ToString(), propContainerCheckedOut.ToString());
                    sw.WriteLine(line);
                }
            }

            sw.Flush();
            return stream;
        }

        /// <summary>
        /// Creates the email message object that is used to email the report to the associated
        /// email lists.
        /// </summary>
        /// <param name="stream">
        /// The data that is attached to the email.
        /// </param>
        private void GenerateEmail(Stream stream)
        {
            DateTime date = DateTime.Now;
            this.mailMessage = new MailMessage();

            // Add basic information.
            mailMessage.From = new MailAddress(EmailSender);
            mailMessage.Subject = "Available containers with Bug Count";

            if (DefaultEmailList != null)
            {
                foreach (string email in DefaultEmailList.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries).Distinct())
                {
                    mailMessage.To.Add(email);
                }
            }

            // Add the csv's as attachments
            stream.Position = 0;
            mailMessage.Attachments.Add(new System.Net.Mail.Attachment(stream, String.Format("RDU_containers_{0}-{1:00}-{2:00}.csv", date.Year, date.Month, date.Day)));
            stream.Position = 0;
        }
        #endregion
    } // RSG.Model.Report2.Reports.Map.RDUGraphReport
} // RSG.Model.Report2.Reports.Map
