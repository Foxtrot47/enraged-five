﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common.Animation
{
    /// <summary>
    /// Interface for cutscene based platform statistics.
    /// </summary>
    public interface ICutscenePlatformStat
    {
        /// <summary>
        /// Total physical size.
        /// </summary>
        ulong PhysicalSize { get; }

        /// <summary>
        /// Total virtual size.
        /// </summary>
        ulong VirtualSize { get; }

        /// <summary>
        /// Name of the file where this platform data resides.
        /// </summary>
        String PlatformDataPath { get; }

        /// <summary>
        /// Sizes of the various sections for this cutscene.
        /// </summary>
        IList<ICutscenePlatformSectionStat> Sections { get; }
    } // ICutscenePlatformStat
}
