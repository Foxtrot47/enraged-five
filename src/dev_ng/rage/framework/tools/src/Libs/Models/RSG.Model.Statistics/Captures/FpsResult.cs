﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Runtime.Serialization;
using System.Diagnostics;

namespace RSG.Model.Statistics.Captures
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class FpsResult
    {
        #region Properties
        /// <summary>
        /// Minimum value recorded for this result.
        /// </summary>
        [DataMember]
        public float Min { get; set; }

        /// <summary>
        /// Maximum value recorded for this result.
        /// </summary>
        [DataMember]
        public float Max { get; set; }

        /// <summary>
        /// Average value recorded for this result.
        /// </summary>
        [DataMember]
        public float Average { get; set; }

        /// <summary>
        /// Standard deviation for this result.
        /// </summary>
        [DataMember]
        public float StandardDeviation { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public FpsResult()
        {
        }
        #endregion // Constructor(s)

        #region Static Controller Methods
        /// <summary>
        /// Creates an fps result from the data in a stats.xml fps element.
        /// </summary>
        internal static FpsResult CreateFromElement(XElement elem)
        {
            FpsResult result = new FpsResult();

            // Extract the various stats
            XElement minElement = elem.Element("min");
            Debug.Assert(minElement != null, "Missing min sub-element.");
            if (minElement == null)
            {
                throw new ArgumentNullException("Fps element is missing the min sub-element.");
            }
            result.Min = Single.Parse(minElement.Attribute("value").Value);

            XElement maxElement = elem.Element("max");
            Debug.Assert(maxElement != null, "Missing max sub-element.");
            if (maxElement == null)
            {
                throw new ArgumentNullException("Fps element is missing the max sub-element.");
            }
            result.Max = Single.Parse(maxElement.Attribute("value").Value);

            XElement avgElement = elem.Element("average");
            Debug.Assert(avgElement != null, "Missing average sub-element.");
            if (avgElement == null)
            {
                throw new ArgumentNullException("Fps element is missing the average sub-element.");
            }
            result.Average = Single.Parse(avgElement.Attribute("value").Value);

            XElement stdElement = elem.Element("std");
            Debug.Assert(stdElement != null, "Missing std sub-element.");
            if (stdElement == null)
            {
                throw new ArgumentNullException("Fps element is missing the std sub-element.");
            }
            result.StandardDeviation = Single.Parse(stdElement.Attribute("value").Value);

            return result;
        }
        #endregion // Static Controller Methods
    } // FpsResult
}
