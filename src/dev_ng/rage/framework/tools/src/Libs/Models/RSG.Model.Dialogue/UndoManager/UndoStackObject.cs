﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using RSG.Model.Dialogue.UndoInterface;

namespace RSG.Model.Dialogue.UndoManager
{

    /// <summary>
    /// The delegate definition that is used to set the property
    /// to a new value
    /// </summary>
    public delegate void UndoSetterDelegate(Object value);

    /// <summary>
    /// Base class for the unco stack objects
    /// </summary>
    public class UndoStackObject
    {
        #region Constructor(s)

        public UndoStackObject()
        {
        }

        #endregion // Constructor(s)

    } // UndoStackObject

    /// <summary>
    /// Contains the properties for a single undo 'move'
    /// </summary>
    public class SingleUndoStackObject : UndoStackObject
    {
        #region Properties

        /// <summary>
        /// The orginal object that sent the undo creation event.
        /// </summary>
        public Object Sender
        {
            get { return m_sender; }
            set { m_sender = value; }
        }
        private Object m_sender;

        /// <summary>
        /// The value of the changed object before the changed occurred
        /// </summary>
        public Object OldValue
        {
            get { return m_oldValue; }
            set { m_oldValue = value; }
        }
        private Object m_oldValue;

        /// <summary>
        /// The value of the changed object after the changed occurred
        /// </summary>
        public Object NewValue
        {
            get { return m_newValue; }
            set { m_newValue = value; }
        }
        private Object m_newValue;

        /// <summary>
        /// The expression that represents the method that orginally 
        /// called the property change
        /// </summary>
        public MemberExpression CallingMethod
        {
            get { return m_callingMethod; }
            set { m_callingMethod = value; }
        }
        private MemberExpression m_callingMethod;
        
        /// <summary>
        /// The actual delegate that is used to set the property
        /// to a new value
        /// </summary>
        public UndoSetterDelegate Setter
        {
            get { return m_setter; }
            set { m_setter = value; }
        }
        public UndoSetterDelegate m_setter;

        #endregion // Properties

        #region Constructor(s)

        public SingleUndoStackObject(Object sender, MemberExpression callingMethod, UndoSetterDelegate setter, Object oldValue, Object newValue)
        {
            this.m_sender = sender;
            this.m_oldValue = oldValue;
            this.m_newValue = newValue;
            this.m_callingMethod = callingMethod;
            this.m_setter = setter;
        }

        #endregion // Constructor(s)

    } // SingleUndoStackObject

    /// <summary>
    /// Contains the properties for multiple undo 'moves'
    /// </summary>
    public class CompositeUndoStackObject : UndoStackObject
    {
        #region Properties

        /// <summary>
        /// The indiviual stack objects that make up this composite.
        /// </summary>
        public Stack<SingleUndoStackObject> Children
        {
            get { return m_children; }
        }
        private Stack<SingleUndoStackObject> m_children;

        #endregion // Properties

        #region Constructor(s)

        public CompositeUndoStackObject()
        {
            m_children = new Stack<SingleUndoStackObject>();
        }

        #endregion // Constructor(s)

    } // CompositeUndoStackObject


} // RSG.Model.Dialogue.UndoManager
