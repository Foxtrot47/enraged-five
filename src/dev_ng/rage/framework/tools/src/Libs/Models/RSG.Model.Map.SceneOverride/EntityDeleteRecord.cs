﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Map.SceneOverride
{
    internal class EntityDeleteRecord
    {
        internal EntityDeleteRecord(int contentId, uint hash, uint guid,
                                    String modelName, float posX, float posY, float posZ, int userId, long utcFileTimeSubmitted)
        {
            ContentId = contentId;
            Hash = hash;
            Guid = guid;
            ModelName = modelName;
            PosX = posX;
            PosY = posY;
            PosZ = posZ;
            UserId = userId;
            UtcFileTimeSubmitted = utcFileTimeSubmitted;
        }

        internal int ContentId { get; private set; }
        internal uint Hash { get; private set; }
        internal uint Guid { get; private set; }
        internal String ModelName { get; private set; }
        internal float PosX { get; set; }
        internal float PosY { get; set; }
        internal float PosZ { get; set; }
        internal int UserId { get; set; }
        internal long UtcFileTimeSubmitted { get; set; }
    }
}
