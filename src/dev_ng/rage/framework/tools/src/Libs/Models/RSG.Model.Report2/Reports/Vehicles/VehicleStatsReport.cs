﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using RSG.Base.Tasks;
using RSG.Model.Common;

namespace RSG.Model.Report.Reports.Vehicles
{
    /// <summary>
    /// 
    /// </summary>
    public class VehicleStatsReport : VehicleReportBase, IReportFileProvider
    {        
        #region Constants
        private const String c_name = "Vehicle Stats Report";
        private const String c_description = "Exports the selected level vehicles into a .csv file"; 
        #endregion // Constants

        #region IRrportFileProvider Properties
        /// <summary>
        /// Filter used for the save dialog
        /// </summary>
        public string Filter
        {
            get
            {
                return "CSV (Comma delimited)|*.csv";
            }
        }

        /// <summary>
        /// Extensions that this report support
        /// </summary>
        public string SupportedExtension
        {
            get
            {
                return ".csv";
            }
        }

        /// <summary>
        /// Filename specifying where a report should be output
        /// </summary>
        public string Filename
        {
            get;
            set;
        }
        #endregion // IRerportFileProvider Properties

        #region Constructor(s)

        /// <summary>
        /// Constructor.
        /// </summary>
        public VehicleStatsReport()
            : base(c_name, c_description)
        {
        }

        #endregion // Constructor(s)

        #region VehicleReport Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        protected override void GenerateReport(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            using (StreamWriter sw = new StreamWriter(Filename))
            {
                WriteCsvHeader(sw, context.Platforms);

                IVehicleCollection vehicles = context.Level.Vehicles;

                if (vehicles != null)
                {
                    double increment = 1.0 / vehicles.Count;

                    foreach (IVehicle vehicle in vehicles)
                    {
                        progress.Report(new TaskProgress(increment, true, String.Format("Outputting {0}", vehicle.Name)));

                        // Generate the csv line
                        string scvRecord = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20}",
                            vehicle.Name, vehicle.Category.ToString(), vehicle.PolyCount, vehicle.CollisionCount,
                            vehicle.Textures.Count(), vehicle.Shaders.Count(),
                            vehicle.BoneCount, vehicle.DoorCount, vehicle.SeatCount, vehicle.WheelCount, vehicle.ExtraCount,
                            vehicle.PropellerCount, vehicle.RudderCount,
                            vehicle.LightCount, vehicle.RotorCount, vehicle.ElevatorCount, vehicle.HandlebarCount,
                            vehicle.HasLod1, vehicle.HasLod2, vehicle.HasLod3,
                            GenerateMemoryDetails(vehicle, context.Platforms));
                        sw.WriteLine(scvRecord);
                    }
                }
            }

        }
        #endregion // Task Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sw"></param>
        /// <param name="milo"></param>
        private void WriteCsvHeader(StreamWriter sw, IPlatformCollection platforms)
        {
            string header = "Name,Type,Poly Count,Collision Count,Texture Count,Shader Count,Bone Count,Door Count,Seat Count,Wheel Count,Extra Count,Propeller Count,Rudder Count,Light Count,Rotor Count,Elevator Count,Handlebars Count,Has LOD 1,Has LOD 2,Has LOD 3";

            foreach (RSG.Platform.Platform platform in platforms)
            {
                header += String.Format(",{0} Physical Size,{0} Virtual Size,{0} Total Size", platform);
            }

            foreach (RSG.Platform.Platform platform in platforms)
            {
                header += String.Format(",{0} Hi Physical Size,{0} Hi Virtual Size,{0} Hi Total Size", platform);
            }

            sw.WriteLine(header);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vehicle"></param>
        /// <param name="platforms"></param>
        /// <returns></returns>
        private string GenerateMemoryDetails(IVehicle vehicle, IPlatformCollection platforms)
        {
            StringBuilder details = new StringBuilder("");

            foreach (RSG.Platform.Platform platform in platforms)
            {
                if (vehicle.PlatformStats.ContainsKey(platform))
                {
                    IVehiclePlatformStat stat = vehicle.PlatformStats[platform];
                    details.Append(String.Format("{0},{1},{2},", stat.PhysicalSize, stat.VirtualSize, (stat.PhysicalSize + stat.VirtualSize)));
                }
                else
                {
                    details.Append("n/a,n/a,n/a,");
                }
            }

            foreach (RSG.Platform.Platform platform in platforms)
            {
                if (vehicle.PlatformStats.ContainsKey(platform))
                {
                    IVehiclePlatformStat stat = vehicle.PlatformStats[platform];
                    details.Append(String.Format("{0},{1},{2},",stat.HiPhysicalSize, stat.HiVirtualSize, (stat.HiPhysicalSize + stat.HiVirtualSize)));
                }
                else
                {
                    details.Append("n/a,n/a,n/a,");
                }
            }

            return details.ToString().TrimEnd(new char[] { ',' });
        }
        #endregion // Private Methods
    } // VehicleCSVReport
} // RSG.Model.Report.Reports
