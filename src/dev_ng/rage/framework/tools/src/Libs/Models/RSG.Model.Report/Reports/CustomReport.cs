﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.ConfigParser;
using System.Diagnostics;
using RSG.Base.Logging;
using System.IO;
using RSG.Base.Configuration.Reports;

namespace RSG.Model.Report.Reports
{
    /// <summary>
    /// Custom reports can only display in the web browser.
    /// </summary>
    public class CustomReport : UriReport, IDynamicReport
    {
        #region Properties
        /// <summary>
        /// Reference to the config report item.
        /// </summary>
        public ICustomReport Report
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="report"></param>
        public CustomReport(ICustomReport report)
            : base(report.Name, String.Empty)
        {
            Report = report;
        }
        #endregion // Constructor(s)

        #region IDynamicReport Implementation
        /// <summary>
        /// Generate the report dynamically.
        /// </summary>
        public void Generate(ConfigGameView gv)
        {
            bool success = true;

            // Check if there is an executable to run
            if (!String.IsNullOrEmpty(Report.Executable))
            {
                Log.Log__Message("Running the following custom report: {0} {1}", Report.Executable, Report.Arguments);

                try
                {
                    Process proc = new Process();
                    proc.StartInfo.FileName = Report.Executable;
                    proc.StartInfo.Arguments = Report.Arguments;
                    proc.Start();
                    proc.WaitForExit();

                    if (proc.ExitCode == 0)
                    {
                        success = true;
                        Log.Log__Message("Report was successfully executed.");
                    }
                    else
                    {
                        success = false;
                        Log.Log__Error("An error occurred while running the '{0}' report (exit code {1}).", this.Name, proc.ExitCode);
                    }
                }
                catch (System.Exception ex)
                {
                    Log.Log__Exception(ex, "An exception was thrown while running the {0} report.", this.Name);
                }
            }

            // Update the uri based on whether the report was successfully generated
            Uri = null;
            if (success && Path.GetExtension(Report.Output) == ".html")
            {
                Uri = new Uri(Report.Output);
            }
        }
        #endregion // IDynamicReport Implementation
    } // CustomReport
}
