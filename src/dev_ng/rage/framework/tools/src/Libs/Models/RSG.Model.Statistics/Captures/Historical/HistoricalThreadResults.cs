﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace RSG.Model.Statistics.Captures.Historical
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    public class HistoricalThreadResults
    {
        #region Properties
        /// <summary>
        /// Mapping of thread names to historical results.
        /// </summary>
        [DataMember]
        public IDictionary<String, HistoricalResults> Results { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public HistoricalThreadResults()
        {
            Results = new SortedDictionary<String, HistoricalResults>();
        }
        #endregion // Constructor(s)

        #region Public Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="result"></param>
        public void AddResult(Object context, ThreadResult result)
        {
            if (!Results.ContainsKey(result.Name))
            {
                Results[result.Name] = new HistoricalResults();
            }

            Results[result.Name].AddResult(context, result.Average, result.StandardDeviation);
        }
        #endregion // Public Methods
    } // HistoricalThreadResults
}
