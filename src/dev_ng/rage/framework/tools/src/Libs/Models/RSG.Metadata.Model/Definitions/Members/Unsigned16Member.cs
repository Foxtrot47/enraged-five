﻿// --------------------------------------------------------------------------------------------
// <copyright file="Unsigned16Member.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions.Members
{
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using RSG.Base;
    using RSG.Base.Extensions;
    using RSG.Base.Logging;
    using RSG.Metadata.Model.Resources;
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// Represents the &lt;u16&gt; member node in the parCodeGen system that can be
    /// instanced in a metadata file.
    /// </summary>
    public class Unsigned16Member : BaseIntegerMember<ushort>, IEquatable<Unsigned16Member>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="Unsigned16Member"/> class to be one of
        /// the members of the specified structure.
        /// </summary>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        public Unsigned16Member(IStructure structure)
            : base(structure)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Unsigned16Member"/> class as a copy of
        /// the specified instance.
        /// </summary>
        /// <param name="other">
        /// The instance to copy.
        /// </param>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        public Unsigned16Member(Unsigned16Member other, IStructure structure)
            : base(other, structure)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Unsigned16Member"/> class.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        public Unsigned16Member(XmlReader reader, IStructure structure)
            : base(reader, structure)
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a string that represents the content type of this scalar member.
        /// </summary>
        public override string ScalarContent
        {
            get { return "short"; }
        }

        /// <summary>
        /// Gets the type name for this member.
        /// </summary>
        public override string TypeName
        {
            get { return "u16"; }
        }

        /// <summary>
        /// Gets the default initial value for the integer type.
        /// </summary>
        protected override ushort DefaultInitialValue
        {
            get { return default(ushort); }
        }

        /// <summary>
        /// Gets the default maximum value for the integer type.
        /// </summary>
        protected override ushort DefaultMaximumValue
        {
            get { return ushort.MaxValue; }
        }

        /// <summary>
        /// Gets the default minimum value for the integer type.
        /// </summary>
        protected override ushort DefaultMinimumValue
        {
            get { return ushort.MinValue; }
        }

        /// <summary>
        /// Gets the default step value for the integer type.
        /// </summary>
        protected override ushort DefaultStepValue
        {
            get { return 1; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new <see cref="Unsigned16Member"/> that is a copy of this instance.
        /// </summary>
        /// <returns>
        /// A new <see cref="Unsigned16Member"/> that is a copy of this instance.
        /// </returns>
        public new Unsigned16Member Clone()
        {
            return new Unsigned16Member(this, this.Structure);
        }

        /// <summary>
        /// Creates a <see cref="ITunable"/> object that represents a instance of this member
        /// with it's default value set.
        /// </summary>
        /// <param name="parent">
        /// The tunable parent the new tunable will belong to.
        /// </param>
        /// <returns>
        /// A parCodeGen tunable that represents a instance of this member.
        /// </returns>
        public override ITunable CreateTunable(ITunableParent parent)
        {
            return new Unsigned16Tunable(this, parent);
        }

        /// <summary>
        /// Creates a <see cref="ITunable"/> object that represents a instance of this member
        /// using the specified string components to initialise it.
        /// </summary>
        /// <param name="components">
        /// The string components that provides the data used to initialise the new tunable.
        /// </param>
        /// <param name="parent">
        /// The tunable parent the new tunable will belong to.
        /// </param>
        /// <returns>
        /// A parCodeGen tunable that represents a instance of this member.
        /// </returns>
        public override ITunable CreateTunable(List<string> components, ITunableParent parent)
        {
            if (components.Count != 1)
            {
                string msg = StringTable.IntegerComponentCountError;
                msg = msg.FormatInvariant("unsigned16", components.Count.ToString());
                throw new InvalidOperationException(msg);
            }

            Unsigned16Tunable tunable = new Unsigned16Tunable(this, parent);
            TryResult<ushort> value = this.Dictionary.TryTo(components[0], default(ushort));
            if (!value.Success)
            {
                string msg = StringTable.IntegerParseError;
                msg = msg.FormatInvariant("unsigned16", components[0]);
                throw new InvalidOperationException(msg);
            }

            tunable.Value = value.Value;
            return tunable;
        }

        /// <summary>
        /// Creates a <see cref="ITunable"/> object that represents a instance of this member
        /// using the specified Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The Xml.XmlReader that provides the data used to initialise the new tunable.
        /// </param>
        /// <param name="parent">
        /// The tunable parent the new tunable will belong to.
        /// </param>
        /// <param name="log">
        /// A object that is used to log any warnings or errors produced during the parsing of
        /// the specified reader.
        /// </param>
        /// <returns>
        /// A parCodeGen tunable that represents a instance of this member.
        /// </returns>
        public override ITunable CreateTunable(
            XmlReader reader, ITunableParent parent, ILog log)
        {
            return new Unsigned16Tunable(reader, this, parent, log);
        }

        /// <summary>
        /// Creates a deep copy of the current instance.
        /// </summary>
        /// <returns>
        /// A deep copy of the current System.Object.
        /// </returns>
        public override object DeepClone()
        {
            return this.Clone();
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified
        /// <see cref="Unsigned16Member"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public bool Equals(Unsigned16Member other)
        {
            if (other == null)
            {
                return false;
            }

            return object.ReferenceEquals(other, this);
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="IMember"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public override bool Equals(IMember other)
        {
            return this.Equals(other as Unsigned16Member);
        }
        #endregion Methods
    } // RSG.Metadata.Model.Definitions.Members.Unsigned16Member {Class}
} // RSG.Metadata.Model.Definitions.Members {Namespace}
