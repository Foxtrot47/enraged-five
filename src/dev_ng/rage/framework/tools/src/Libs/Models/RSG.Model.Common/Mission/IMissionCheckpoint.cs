﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common.Mission
{
    /// <summary>
    /// Interface for mission checkpoints.
    /// </summary>
    public interface IMissionCheckpoint : IAsset
    {
        /// <summary>
        /// Number of attempts we expect this checkpoint to require.
        /// </summary>
        float ProjectedAttempts { get; set; }

        /// <summary>
        /// Minimum number of attempts we expect this checkpoint to require.
        /// </summary>
        float ProjectedAttemptsMin { get; set; }

        /// <summary>
        /// Maximum number of attempts we expect this checkpoint to require.
        /// </summary>
        float ProjectedAttemptsMax { get; set; }

        /// <summary>
        /// Whether bugstar knows about this checkpoint.
        /// </summary>
        bool InBugstar { get; set; }

        /// <summary>
        /// Checkpoint index.
        /// </summary>
        uint? Index { get; set; }
    } // IMissionCheckpoint
}
