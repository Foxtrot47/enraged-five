﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Attributes;
using System.Reflection;
using System.ComponentModel;

namespace RSG.Model.Common
{
    /// <summary>
    /// Vehicle type enumeration.
    /// </summary>
    public enum VehicleCategory
    {
        [FriendlyName("Unknown")]
        [Browsable(false)]
        Unknown,

        [RuntimeName("VEHICLE_TYPE_CAR")]
        [FriendlyName("Car")]
        Car,

        [RuntimeName("VEHICLE_TYPE_TRAILER")]
        [FriendlyName("Trailer")]
        Trailer,

        [RuntimeName("VEHICLE_TYPE_QUADBIKE")]
        [FriendlyName("Quad Bike")]
        QuadBike,

        [RuntimeName("VEHICLE_TYPE_BLIMP")]
        [FriendlyName("Blimp")]
        Blimp,

        [RuntimeName("VEHICLE_TYPE_TRAIN")]
        [FriendlyName("Train")]
        Train,

        [RuntimeName("VEHICLE_TYPE_BIKE")]
        [FriendlyName("Motorbike")]
        Bike,

        [RuntimeName("VEHICLE_TYPE_BICYCLE")]
        [FriendlyName("Bicycle")]
        Bicycle,

        [RuntimeName("VEHICLE_TYPE_HELI")]
        [FriendlyName("Helicopter")]
        Heli,

        [RuntimeName("VEHICLE_TYPE_PLANE")]
        [FriendlyName("Airplane")]
        Plane,

        [RuntimeName("VEHICLE_TYPE_BOAT")]
        [FriendlyName("Boat")]
        Boat,

        [RuntimeName("VEHICLE_TYPE_SUBMARINE")]
        [FriendlyName("Submarine")]
        Submarine
    } // VehicleCategory


    /// <summary>
    /// Vehicle category enumeration utility and extension methods.
    /// </summary>
    public static class VehicleCategoryUtils
    {
        /// <summary>
        /// Return runtime name string for a VehicleCategory enum value.
        /// </summary>
        /// <param name="metric"></param>
        /// <returns></returns>
        public static string GetRuntimeName(this VehicleCategory category)
        {
            Type type = category.GetType();

            FieldInfo fi = type.GetField(category.ToString());
            RuntimeNameAttribute[] attributes =
                fi.GetCustomAttributes(typeof(RuntimeNameAttribute), false) as RuntimeNameAttribute[];

            return (attributes.Length > 0 ? attributes[0].Name : null);
        }

        /// <summary>
        /// Return VehicleCategory value from String representation.
        /// </summary>
        /// <param name="metric">Category string.</param>
        /// <returns></returns>
        public static VehicleCategory ParseFromRuntimeString(String category)
        {
            VehicleCategory result;
            if (TryParseFromRuntimeString(category, out result))
            {
                return result;
            }
            else
            {
                throw new ArgumentException(String.Format("{0} is an unrecognised category,", category));
            }
        }

        /// <summary>
        /// Tries to parse a runtime name into a VehicleCategory value, returning whether the operation was successful.
        /// </summary>
        /// <param name="metric"></param>
        /// <param name="metric"></param>
        /// <returns></returns>
        public static bool TryParseFromRuntimeString(String metricName, out VehicleCategory metric)
        {
            foreach (VehicleCategory m in Enum.GetValues(typeof(VehicleCategory)))
            {
                if (0 == String.Compare(metricName, m.GetRuntimeName(), true))
                {
                    metric = m;
                    return true;
                }
            }

            metric = VehicleCategory.Unknown;
            return false;
        }
    } // TelemetryMetricUtils
} // RSG.Model.Common
