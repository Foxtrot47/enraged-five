﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using RSG.Model.Report.Reports.GameStatsReports.Model;
using System.Windows.Controls.DataVisualization.Charting;
using System.IO;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows;
using System.Windows.Data;

namespace RSG.Model.Report.Reports.GameStatsReports.Tests
{
    public class FpsSmokeTest : SmokeTestBase
    {
        #region Constants
        private const string c_name = "Fps";
        private const int c_lineGraphWidth = 1280;
        private const int c_lineGraphHeight = 600;
        #endregion // Constants

        #region Private Classes
        /// <summary>
        /// Contains information about a single fps stats
        /// </summary>
        private class FpsInfo
        {
            public FpsResult Result { get; private set; }
            public DateTime Timestamp { get; private set; }

            public FpsInfo(FpsResult result, DateTime time)
            {
                Result = result;
                Timestamp = time;
            }
        }
        #endregion // Private Classes

        #region Properties
        /// <summary>
        /// Fps info grouped by test name
        /// </summary>
        private IDictionary<string, IList<FpsInfo>> GroupedFpsInfo
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public FpsSmokeTest()
            : base(c_name)
        {
        }
        #endregion // Constructor(s)

        #region SmokeTestBase Methods
        /// <summary>
        /// Executes the particular test
        /// </summary>
        /// <param name="testSessions">List of test sessions</param>
        public override void RunTest(IList<TestSession> testSessions)
        {
            base.RunTest(testSessions);

            // Gather the results we are interested in
            GroupedFpsInfo = GatherFpsResults(testSessions);

            // Check if we have any errors/warnings/messages to report
            foreach (KeyValuePair<string, IList<FpsInfo>> pair in GroupedFpsInfo)
            {
                string testName = pair.Key;

                FpsInfo first = pair.Value.FirstOrDefault();
                FpsInfo next = null;
                if (pair.Value.Count > 1)
                {
                    next = pair.Value[1];
                }

                if (first != null && next != null && first.Result.Average != 0.0f && next.Result.Average != 0.0f)
                {
                    float averageGrowth = next.Result.Average / first.Result.Average;
                    if (averageGrowth > 1.05f)
                    {
                        Errors.Add(String.Format("{0} {1} test is slower by {2:0.##}%", testName, SmokeTestName.ToLower(), (averageGrowth - 1.0) * 100));
                    }

                    if (averageGrowth < 0.95f)
                    {
                        Messages.Add(String.Format("{0} {1} test is faster by {2:0.##}%", testName, SmokeTestName.ToLower(), -(averageGrowth - 1.0) * 100));
                    }
                }
            }
        }

        /// <summary>
        /// Generates a graph for this smoke test
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns>Null if the smoke test doesn't require a graph</returns>
        public override IList<GraphImage> GenerateGraphs(DateTime start, DateTime end)
        {
            IList<GraphImage> allGraphs = new List<GraphImage>();

            Chart chart = new Chart();
            chart.BeginInit();
            chart.Width = c_lineGraphWidth;
            chart.Height = c_lineGraphHeight;
            chart.Title = String.Format("Average Fps ({0} - {1})", start, end);
            chart.LegendTitle = "Individual Tests";
            chart.EndInit();

            RenderTargetBitmap renderTarget = new RenderTargetBitmap((int)chart.Width, (int)chart.Height, 96d, 96d, PixelFormats.Default);

            foreach (KeyValuePair<string, IList<FpsInfo>> pair in GroupedFpsInfo)
            {
                LineSeries lineSeries = new LineSeries
                {
                    Title = pair.Key,
                    ItemsSource = pair.Value,
                    IndependentValueBinding = new Binding("Timestamp"),
                    DependentValueBinding = new Binding("Result.Average")
                };

                chart.Series.Add(lineSeries);
            }

            DateTimeAxis xAxis = new DateTimeAxis
            {
                Orientation = AxisOrientation.X,
                Interval = 1,
                IntervalType = DateTimeIntervalType.Days,
                Title = "Date"
            };
            chart.Axes.Add(xAxis);

            LinearAxis yAxis = new LinearAxis
            {
                Orientation = AxisOrientation.Y,
                Title = "Fps",
                ShowGridLines = true
            };
            chart.Axes.Add(yAxis);

            chart.Measure(new Size(chart.Width, chart.Height));
            chart.Arrange(new Rect(new Size(chart.Width, chart.Height)));
            chart.UpdateLayout();

            renderTarget.Render(chart);

            // Save out the chart to a png file
            PngBitmapEncoder png = new PngBitmapEncoder();
            png.Frames.Add(BitmapFrame.Create(renderTarget));
            MemoryStream stream = new MemoryStream();
            png.Save(stream);
            stream.Position = 0;

            // Create and keep the graph image
            GraphImage fpsImage = new GraphImage("Fps", stream);
            CommonGraphs.Add(fpsImage);

            allGraphs.Add(fpsImage);
            return allGraphs;
        }

        /// <summary>
        /// Write this test's portion of the report
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="test">The name of the test for which we wish to write the report</param>
        /// <param name="includeHeader">Whether we should output a header for this report</param>
        /// <param name="tableResults"></param>
        public override void WriteReport(HtmlTextWriter writer, string test, bool includeHeader, uint tableResults, bool email)
        {
            // Only write out to the report if we contain information for the specified test
            if (GroupedFpsInfo.ContainsKey(test))
            {
                if (includeHeader)
                {
                    WriteSectionHeader(test, writer);
                }

                writer.AddAttribute(HtmlTextWriterAttribute.Border, "1");
                writer.RenderBeginTag(HtmlTextWriterTag.Table);
                {
                    // Timestamp row
                    writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                    writer.RenderBeginTag(HtmlTextWriterTag.Td);
                    writer.RenderEndTag();

                    foreach (FpsInfo info in GroupedFpsInfo[test].Take((int)tableResults))
                    {
                        writer.RenderBeginTag(HtmlTextWriterTag.Td);
                        writer.RenderBeginTag(HtmlTextWriterTag.I);
                        writer.Write(info.Timestamp.ToString("dd/MM/yyyy HH:mm"));
                        writer.RenderEndTag();
                        writer.RenderEndTag();
                    }
                    writer.RenderEndTag();

                    // Average row
                    writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                    writer.RenderBeginTag(HtmlTextWriterTag.Td);
                    writer.RenderBeginTag(HtmlTextWriterTag.B);
                    writer.Write("Avg");
                    writer.RenderEndTag();
                    writer.RenderEndTag();

                    foreach (FpsInfo info in GroupedFpsInfo[test].Take((int)tableResults))
                    {
                        writer.RenderBeginTag(HtmlTextWriterTag.Td);
                        writer.Write(info.Result.Average);
                        writer.RenderEndTag();
                    }
                    writer.RenderEndTag();

                    // Min row
                    writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                    writer.RenderBeginTag(HtmlTextWriterTag.Td);
                    writer.RenderBeginTag(HtmlTextWriterTag.B);
                    writer.Write("Min");
                    writer.RenderEndTag();
                    writer.RenderEndTag();

                    foreach (FpsInfo info in GroupedFpsInfo[test].Take((int)tableResults))
                    {
                        writer.RenderBeginTag(HtmlTextWriterTag.Td);
                        writer.Write(info.Result.Min);
                        writer.RenderEndTag();
                    }
                    writer.RenderEndTag();

                    // Max row
                    writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                    writer.RenderBeginTag(HtmlTextWriterTag.Td);
                    writer.RenderBeginTag(HtmlTextWriterTag.B);
                    writer.Write("Max");
                    writer.RenderEndTag();
                    writer.RenderEndTag();

                    foreach (FpsInfo info in GroupedFpsInfo[test].Take((int)tableResults))
                    {
                        writer.RenderBeginTag(HtmlTextWriterTag.Td);
                        writer.Write(info.Result.Max);
                        writer.RenderEndTag();
                    }
                    writer.RenderEndTag();
                }
                writer.RenderEndTag();
            }
        }
        #endregion // SmokeTestBase Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="testSessions"></param>
        /// <returns></returns>
        private Dictionary<string, IList<FpsInfo>> GatherFpsResults(IList<TestSession> testSessions)
        {
            Dictionary<string, IList<FpsInfo>> groupedFpsInfo = new Dictionary<string, IList<FpsInfo>>();

            foreach (TestSession session in testSessions)
            {
                ISet<string> encounteredTests = new HashSet<string>();

                foreach (TestResults test in session.TestResults)
                {
                    if (encounteredTests.Contains(test.Name))
                    {
                        continue;
                    }
                    encounteredTests.Add(test.Name);
                
                    if (!groupedFpsInfo.ContainsKey(test.Name))
                    {
                        groupedFpsInfo.Add(test.Name, new List<FpsInfo>());
                    }

                    FpsInfo info = new FpsInfo(test.FpsResult, session.Timestamp);
                    groupedFpsInfo[test.Name].Add(info);
                }
            }

            return groupedFpsInfo;
        }
        #endregion // Private Methods
    } // FpsSmokeTest
}
