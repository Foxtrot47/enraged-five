﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Collections;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using RSG.Model.Map;
using RSG.SceneXml;

namespace RSG.Model.Map.ViewModel
{
    /// <summary>
    /// The view model for the map section object used by the view
    /// </summary>
    public class MapSectionViewModel :
        RSG.Base.Editor.HierarchicalViewModelBase, IMapViewModelComponent, IMapSectionViewModel
    {
        #region Properties

        /// <summary>
        /// list of the type of sections that can be allowed
        /// </summary>
        public enum SectionType
        {
            CONTAINER_FILE,
            PROPPLACEMENT_FILE,
            PROP_FILE,
            INTERIOR_FILE
        }

        /// <summary>
        /// The model reference that this view model represents.
        /// </summary>
        public MapSection Model
        {
            get { return m_model; }
            set { m_model = value; }
        }
        private MapSection m_model;

        /// <summary>
        /// The user data that can be attached to this view model
        /// </summary>
        public UserData ViewModelUserData
        {
            get { return m_viewModelUserData; }
            set { m_viewModelUserData = value; }
        }
        private UserData m_viewModelUserData = new UserData();

        public Object ViewModelUserDataSource
        {
            get { return this.m_viewModelUserData.UserDataSource; }
        }

        /// <summary>
        /// Returns true iff this map section represents a non generic map section
        /// that also exports definitions. (i.e exports a ipl file and a ide file).
        /// </summary>
        public Boolean NonGenericWithDefinitions
        {
            get;
            private set;
        }

        public LevelViewModel Level
        {
            get;
            private set;
        }

        public String Name
        {
            get { return Model.Name.ToLower(); }
        }

        public ObservableCollection<MapInstanceViewModel> Instances
        {
            get { return m_instances; }
            set
            {
                SetPropertyValue(value, () => this.Instances,
                    new PropertySetDelegate(delegate(Object newValue) { m_instances = (ObservableCollection<MapInstanceViewModel>)newValue; }));
            }
        }
        private ObservableCollection<MapInstanceViewModel> m_instances;

        public TextureDictionarySetViewModel TextureDictionaries
        {
            get { return m_textureDictionaries; }
            set
            {
                SetPropertyValue(value, () => this.TextureDictionaries,
                    new PropertySetDelegate(delegate(Object newValue) { m_textureDictionaries = (TextureDictionarySetViewModel)newValue; }));
            }
        }
        private TextureDictionarySetViewModel m_textureDictionaries;

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Default Constructor
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="model"></param>
        public MapSectionViewModel(IViewModel parent, MapSection model)
        {
            this.Parent = parent;
            this.Model = model;

            if (parent is IMapViewModelContainer)
            {
                IMapViewModelContainer containerParent = parent as IMapViewModelContainer;
                LevelViewModel level = containerParent as LevelViewModel;
                while (containerParent != null && !(level is LevelViewModel))
                {
                    containerParent = containerParent.Parent as IMapViewModelContainer;
                    level = containerParent as LevelViewModel;
                }

                this.Level = level;
            }


            this.NonGenericWithDefinitions = Model.ExportInstances && Model.ExportDefinitions;

            this.ViewModelUserData = new Base.Collections.UserData();
        }

        #endregion // Constructors

        #region Override Functions

        protected override void OnFirstExpanded()
        {
            if (this.Model.Instances != null)
            {
                ObservableCollection<MapInstanceViewModel> instances = new ObservableCollection<MapInstanceViewModel>();
                Dictionary<String, TextureDictionary> textureDictionaries = new Dictionary<String, TextureDictionary>();
                foreach (MapInstance instance in this.Model.Instances)
                {
                    instances.Add(new MapInstanceViewModel(this, instance));
                }
                this.Instances = instances;
            }
            if (this.Model.TextureDictionaries != null)
                this.TextureDictionaries = new TextureDictionarySetViewModel(this, this.Model.TextureDictionaries);
        }

        public override IViewModel GetChildWithString(String name)
        {
            foreach (MapInstanceViewModel child in this.Instances)
            {
                if (String.Compare(child.Name, name, true) == 0)
                {
                    return child as IViewModel;
                }
            }
            if (String.IsNullOrEmpty(name))
            {
                return this.TextureDictionaries as IViewModel;
            }

            return null;
        }

        #endregion // Override Functions

        #region Public Methods

        public void UpdateChildrenWithModel()
        {
            ObservableCollection<MapInstanceViewModel> instances = new ObservableCollection<MapInstanceViewModel>();
            instances.Add(new MapInstanceViewModel());
            this.Instances = instances;
        }

        #endregion // Public Methods
    } // MapSectionViewModel
} // RSG.Model.Map.ViewModel
