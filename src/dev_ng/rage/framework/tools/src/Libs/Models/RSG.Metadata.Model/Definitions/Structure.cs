﻿//---------------------------------------------------------------------------------------------
// <copyright file="Structure.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml;
    using RSG.Base.Extensions;
    using RSG.Base.Logging;
    using RSG.Editor;
    using RSG.Editor.Model;
    using RSG.Metadata.Model.Definitions.Members;
    using RSG.Metadata.Model.Resources;

    /// <summary>
    /// Defines a parsable class or structure that can be used by the parCodeGen system. This
    /// class cannot be inherited.
    /// </summary>
    internal sealed class Structure : DefinitionBase, IStructure
    {
        #region Fields
        /// <summary>
        /// The name of the xml attribute used for the <see cref="AutoRegister"/> property.
        /// </summary>
        private const string XmlAutoRegisterAttr = "autoregister";

        /// <summary>
        /// The name of the xml attribute used for the <see cref="BaseStructureName"/>
        /// property.
        /// </summary>
        private const string XmlBaseAttr = "base";

        /// <summary>
        /// The name of the xml attribute used for the <see cref="Category"/> property.
        /// </summary>
        private const string XmlCategoryAttr = "ui_category";

        /// <summary>
        /// The name of the xml attribute used for the <see cref="Constructible"/> property.
        /// </summary>
        private const string XmlConstructibleAttr = "constructible";

        /// <summary>
        /// The name of the xml attribute used for the <see cref="Filter"/> property.
        /// </summary>
        private const string XmlFilterAttr = "ui_filter";

        /// <summary>
        /// The name of the xml attribute used for the <see cref="FullPost"/> property.
        /// </summary>
        private const string XmlFullPostAttr = "ui_fullpost";

        /// <summary>
        /// The name of the xml attribute used for the <see cref="LayoutHint"/> property.
        /// </summary>
        private const string XmlLayoutHintAttr = "layoutHint";

        /// <summary>
        /// The name of the xml attribute used for the <see cref="Name"/> property.
        /// </summary>
        private const string XmlNameAttr = "name";

        /// <summary>
        /// The name of the xml attribute used for the <see cref="OnPostAddWidgets"/> property.
        /// </summary>
        private const string XmlOnPostAddWidgetsAttr = "onPostAddWidgets";

        /// <summary>
        /// The name of the xml attribute used for the <see cref="OnPostLoad"/> property.
        /// </summary>
        private const string XmlOnPostLoadAttr = "onPostLoad";

        /// <summary>
        /// The name of the xml attribute used for the <see cref="OnPostSave"/> property.
        /// </summary>
        private const string XmlOnPostSaveAttr = "onPostSave";

        /// <summary>
        /// The name of the xml attribute used for the <see cref="OnPostSet"/> property.
        /// </summary>
        private const string XmlOnPostSetAttr = "onPostSet";

        /// <summary>
        /// The name of the xml attribute used for the <see cref="OnPreAddWidgets"/> property.
        /// </summary>
        private const string XmlOnPreAddWidgetsAttr = "onPreAddWidgets";

        /// <summary>
        /// The name of the xml attribute used for the <see cref="OnPreLoad"/> property.
        /// </summary>
        private const string XmlOnPreLoadAttr = "onPreLoad";

        /// <summary>
        /// The name of the xml attribute used for the <see cref="OnPreSave"/> property.
        /// </summary>
        private const string XmlOnPreSaveAttr = "onPreSave";

        /// <summary>
        /// The name of the xml attribute used for the <see cref="OnPreSet"/> property.
        /// </summary>
        private const string XmlOnPreSetAttr = "onPreSet";

        /// <summary>
        /// The name of the xml attribute used for the <see cref="PreserveNames"/> property.
        /// </summary>
        private const string XmlPreserveNamesAttr = "preserveNames";

        /// <summary>
        /// The name of the xml attribute used for the <see cref="IsRootStructure"/> property.
        /// </summary>
        private const string XmlRootAttr = "ui_root";

        /// <summary>
        /// The name of the xml attribute used for the <see cref="Template"/> property.
        /// </summary>
        private const string XmlSimpleAttr = "simple";

        /// <summary>
        /// The name of the xml attribute used for the <see cref="Template"/> property.
        /// </summary>
        private const string XmlTemplateAttr = "template";

        /// <summary>
        /// The name of the xml attribute used for the
        /// <see cref="UsesDataInheritance"/> property.
        /// </summary>
        private const string XmlUsesDiAttr = "usesdi";

        /// <summary>
        /// The name of the xml attribute used for the <see cref="Version"/> property.
        /// </summary>
        private const string XmlVersionAttr = "version";

        /// <summary>
        /// The private field used for the <see cref="BaseStructure"/> property.
        /// </summary>
        private IStructure _baseStructure;

        /// <summary>
        /// The private field used for the <see cref="BaseStructureName"/> property.
        /// </summary>
        private string _baseStructureName;

        /// <summary>
        /// The private field used for the <see cref="Category"/> property.
        /// </summary>
        private string _category;

        /// <summary>
        /// The private field used for the <see cref="Constructible"/> property.
        /// </summary>
        private string _constructible;

        /// <summary>
        /// The private field used for the <see cref="Filter"/> property.
        /// </summary>
        private string _filter;

        /// <summary>
        /// The private field used for the <see cref="FullPost"/> property.
        /// </summary>
        private string _fullPost;

        /// <summary>
        /// The private field used for the <see cref="ImmediateDescendants"/> property.
        /// </summary>
        private List<IStructure> _immediateDescendants;

        /// <summary>
        /// The private field used for the <see cref="IsRootStructure"/> property.
        /// </summary>
        private string _isRootStructure;

        /// <summary>
        /// The private field used for the <see cref="LayoutHint"/> property.
        /// </summary>
        private string _layoutHint;

        /// <summary>
        /// The private field used for the <see cref="Members"/> property.
        /// </summary>
        private ModelCollection<IMember> _members;

        /// <summary>
        /// The private field used for the <see cref="Name"/> property.
        /// </summary>
        private string _name;

        /// <summary>
        /// The private field used for the <see cref="OnPostAddWidgets"/> property.
        /// </summary>
        private string _onPostAddWidgets;

        /// <summary>
        /// The private field used for the <see cref="OnPostLoad"/> property.
        /// </summary>
        private string _onPostLoad;

        /// <summary>
        /// The private field used for the <see cref="OnPostSave"/> property.
        /// </summary>
        private string _onPostSave;

        /// <summary>
        /// The private field used for the <see cref="OnPostSet"/> property.
        /// </summary>
        private string _onPostSet;

        /// <summary>
        /// The private field used for the <see cref="OnPreAddWidgets"/> property.
        /// </summary>
        private string _onPreAddWidgets;

        /// <summary>
        /// The private field used for the <see cref="OnPreLoad"/> property.
        /// </summary>
        private string _onPreLoad;

        /// <summary>
        /// The private field used for the <see cref="OnPreSave"/> property.
        /// </summary>
        private string _onPreSave;

        /// <summary>
        /// The private field used for the <see cref="OnPreSet"/> property.
        /// </summary>
        private string _onPreSet;

        /// <summary>
        /// The private field used for the <see cref="PreserveNames"/> property.
        /// </summary>
        private string _preserveNames;

        /// <summary>
        /// The private field used for the <see cref="Members"/> property.
        /// </summary>
        private ReadOnlyModelCollection<IMember> _readOnlyMembers;

        /// <summary>
        /// The private field used for the <see cref="AutoRegister"/> property.
        /// </summary>
        private string _register;

        /// <summary>
        /// The private field used for the <see cref="Simple"/> property.
        /// </summary>
        private string _simple;

        /// <summary>
        /// The private field used for the <see cref="Template"/> property.
        /// </summary>
        private string _template;

        /// <summary>
        /// The private collection of attributes that have been found on the xml data that
        /// weren't recognised as valid attributes.
        /// </summary>
        private HashSet<string> _unrecognisedAttributes;

        /// <summary>
        /// The private field used for the <see cref="UsesDataInheritance"/> property.
        /// </summary>
        private string _usesdi;

        /// <summary>
        /// The private field used for the <see cref="Version"/> property.
        /// </summary>
        private string _version;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="Structure"/> class.
        /// </summary>
        /// <param name="dictionary">
        /// The dictionary that this structure will be associated with.
        /// </param>
        public Structure(IDefinitionDictionary dictionary)
            : base(dictionary)
        {
            this._members = new ModelCollection<IMember>(this);
            this._readOnlyMembers = this._members.ToReadOnly();
            this._unrecognisedAttributes = new HashSet<string>();
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Structure"/> class with the specified
        /// data type.
        /// </summary>
        /// <param name="dataType">
        /// The full data type this structure will represent.
        /// </param>
        /// <param name="dictionary">
        /// The dictionary that this structure will be associated with.
        /// </param>
        public Structure(string dataType, IDefinitionDictionary dictionary)
            : base(dataType, dictionary)
        {
            this._members = new ModelCollection<IMember>(this);
            this._readOnlyMembers = this._members.ToReadOnly();
            this._unrecognisedAttributes = new HashSet<string>();
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Structure"/> class with the specified
        /// data type and name.
        /// </summary>
        /// <param name="dataType">
        /// The full data type this structure will represent.
        /// </param>
        /// <param name="name">
        /// The name this structure will be referenced by.
        /// </param>
        /// <param name="dictionary">
        /// The dictionary that this structure will be associated with.
        /// </param>
        public Structure(string dataType, string name, IDefinitionDictionary dictionary)
            : base(dataType, dictionary)
        {
            this._name = name;
            this._members = new ModelCollection<IMember>(this);
            this._readOnlyMembers = this._members.ToReadOnly();
            this._unrecognisedAttributes = new HashSet<string>();
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Structure"/> class as a copy of the
        /// specified instance.
        /// </summary>
        /// <param name="other">
        /// The instance to copy.
        /// </param>
        /// <param name="dictionary">
        /// The dictionary that this structure will be associated with.
        /// </param>
        public Structure(Structure other, IDefinitionDictionary dictionary)
            : base(other, dictionary)
        {
            this._name = other._name;
            this._baseStructureName = other._baseStructureName;
            this._version = other._version;
            this._constructible = other._constructible;
            this._usesdi = other._usesdi;
            this._register = other._register;
            this._preserveNames = other._preserveNames;
            this._template = other._template;
            this._onPreLoad = other._onPreLoad;
            this._onPostLoad = other._onPostLoad;
            this._onPreSave = other._onPreSave;
            this._onPostSave = other._onPostSave;
            this._onPreAddWidgets = other._onPreAddWidgets;
            this._onPostAddWidgets = other._onPostAddWidgets;
            this._onPreSet = other._onPreSet;
            this._onPostSet = other._onPostSet;
            this._layoutHint = other._layoutHint;

            this._members = new ModelCollection<IMember>(this);
            this._readOnlyMembers = this._members.ToReadOnly();
            this._unrecognisedAttributes = new HashSet<string>();
            foreach (IMember member in other._members)
            {
                this._members.Add(member.Clone());
            }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Structure"/> class using the specified
        /// System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        /// <param name="dictionary">
        /// The dictionary that this structure will be associated with.
        /// </param>
        public Structure(XmlReader reader, IDefinitionDictionary dictionary)
            : base(reader, dictionary)
        {
            this._members = new ModelCollection<IMember>(this);
            this._readOnlyMembers = this._members.ToReadOnly();
            this._unrecognisedAttributes = new HashSet<string>();
            this.Deserialise(reader);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether this structure is automatically registered.
        /// </summary>
        public bool AutoRegister
        {
            get { return this.Dictionary.To<bool>(this._register, true); }
            set { this.SetProperty(ref this._register, value.ToString()); }
        }

        /// <summary>
        /// Gets the resolved <see cref="IStructure"/> used as the base type.
        /// </summary>
        public IStructure BaseStructure
        {
            get
            {
                if (this._baseStructure != null)
                {
                    return this._baseStructure;
                }

                if (this.Dictionary == null || this.Dictionary.Structures == null)
                {
                    return null;
                }

                return this._baseStructure = this.GetBaseStructure(this.Dictionary);
            }
        }

        /// <summary>
        /// Gets or sets the name of the structure that is used as the base type.
        /// </summary>
        public string BaseStructureName
        {
            get
            {
                return this._baseStructureName;
            }

            set
            {
                string[] changed = new string[]
                        {
                            "BaseStructureName",
                            "BaseStructure"
                        };

                this._baseStructure = null;
                this.SetProperty(ref this._baseStructureName, value, changed);
            }
        }

        /// <summary>
        /// Gets or sets the category that this structure is in. This is used as a filter
        /// mechanic when showing all of the structures to the user.
        /// </summary>
        public string Category
        {
            get { return this._category; }
            set { this.SetProperty(ref this._category, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this structure can be constructed.
        /// </summary>
        public bool Constructible
        {
            get { return this.Dictionary.To<bool>(this._constructible, true); }
            set { this.SetProperty(ref this._constructible, value.ToString()); }
        }

        /// <summary>
        /// Gets or sets the full data type for this structure.
        /// </summary>
        public override string DataType
        {
            get
            {
                return this.RawDataType != null ? this.RawDataType.TrimStart(':') : null;
            }

            set
            {
                string[] changedProperties = new string[]
                        {
                            "DataType",
                            "ShortDataType",
                            "Name",
                            "Namespace",
                            "MetadataName",
                            "Scope",
                            "Namespaces",
                            "ImmediateDescendants"
                        };

                this.SetRawDataType(value, changedProperties);
            }
        }

        /// <summary>
        /// Gets or sets the filter that this structure uses. This is used to hint to a open
        /// service the primary file extensions that this structure uses.
        /// </summary>
        public string Filter
        {
            get { return this._filter; }
            set { this.SetProperty(ref this._filter, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this structure needs to be posted in full
        /// when one of its value changes during live editing.
        /// </summary>
        public bool FullPost
        {
            get { return this.Dictionary.To<bool>(this._fullPost, false); }
            set { this.SetProperty(ref this._fullPost, value.ToString()); }
        }

        /// <summary>
        /// Gets an iterator over the structures that use this structure as their base type.
        /// </summary>
        public IEnumerable<IStructure> ImmediateDescendants
        {
            get
            {
                if (this.Dictionary == null || this.Dictionary.Structures == null)
                {
                    Enumerable.Empty<IStructure>();
                }

                if (this._immediateDescendants == null)
                {
                    IEnumerable<IStructure> scope = this.Dictionary.Structures;
                    this._immediateDescendants =
                        new List<IStructure>(this.GetDescendants(scope));
                }

                return this._immediateDescendants;
            }
        }

        /// <summary>
        /// Gets the number of members that this definition contains in it that can
        /// be instanced.
        /// </summary>
        public int InstancedCount
        {
            get { return this._members.Where(m => m.CanBeInstanced).Count(); }
        }

        /// <summary>
        /// Gets a value indicating whether this structure has been marked as a file root
        /// structure.
        /// </summary>
        public bool IsRootStructure
        {
            get { return this.Dictionary.To<bool>(this._isRootStructure, false); }
            set { this.SetProperty(ref this._isRootStructure, value.ToString()); }
        }

        /// <summary>
        /// Gets or sets the value used to provide hints about the memory layout.
        /// </summary>
        public string LayoutHint
        {
            get { return this._layoutHint; }
            set { this.SetProperty(ref this._layoutHint, value); }
        }

        /// <summary>
        /// Gets a collection of the members that are defined for this instance.
        /// </summary>
        public ReadOnlyModelCollection<IMember> Members
        {
            get { return this._readOnlyMembers; }
        }

        /// <summary>
        /// Gets the name for this definition as it will appear in a metadata file.
        /// </summary>
        public string MetadataName
        {
            get { return this.GetMetadataName(this.RawDataType); }
        }

        /// <summary>
        /// Gets or sets the name that this structure will be referenced by.
        /// </summary>
        public string Name
        {
            get { return this._name ?? this.GetNameFromDataType(this.RawDataType); }
            set { this.SetProperty(ref this._name, value); }
        }

        /// <summary>
        /// Gets or sets the name of the function that gets called after adding widgets.
        /// </summary>
        public string OnPostAddWidgets
        {
            get { return this._onPostAddWidgets; }
            set { this.SetProperty(ref this._onPostAddWidgets, value); }
        }

        /// <summary>
        /// Gets or sets the name of the function that gets called after loading.
        /// </summary>
        public string OnPostLoad
        {
            get { return this._onPostLoad; }
            set { this.SetProperty(ref this._onPostLoad, value); }
        }

        /// <summary>
        /// Gets or sets the name of the function that gets called after saving.
        /// </summary>
        public string OnPostSave
        {
            get { return this._onPostSave; }
            set { this.SetProperty(ref this._onPostSave, value); }
        }

        /// <summary>
        /// Gets or sets the name of the function that gets called after any member of the
        /// structure is set.
        /// </summary>
        public string OnPostSet
        {
            get { return this._onPostSet; }
            set { this.SetProperty(ref this._onPostSet, value); }
        }

        /// <summary>
        /// Gets or sets the name of the function that gets called before adding widgets.
        /// </summary>
        public string OnPreAddWidgets
        {
            get { return this._onPreAddWidgets; }
            set { this.SetProperty(ref this._onPreAddWidgets, value); }
        }

        /// <summary>
        /// Gets or sets the name of the function that gets called before loading.
        /// </summary>
        public string OnPreLoad
        {
            get { return this._onPreLoad; }
            set { this.SetProperty(ref this._onPreLoad, value); }
        }

        /// <summary>
        /// Gets or sets the name of the function that gets called before saving.
        /// </summary>
        public string OnPreSave
        {
            get { return this._onPreSave; }
            set { this.SetProperty(ref this._onPreSave, value); }
        }

        /// <summary>
        /// Gets or sets the name of the function that gets called before any member of the
        /// structure is set.
        /// </summary>
        public string OnPreSet
        {
            get { return this._onPreSet; }
            set { this.SetProperty(ref this._onPreSet, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the names for this structure will be
        /// stripped from __FINAL builds.
        /// </summary>
        public bool PreserveNames
        {
            get { return this.Dictionary.To<bool>(this._register, false); }
            set { this.SetProperty(ref this._preserveNames, value.ToString()); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this structure represents a simple class. A
        /// simple class doesn't use virtuals and won't be used polymorphically.
        /// </summary>
        public bool Simple
        {
            get { return this.Dictionary.To<bool>(this._simple, false); }
            set { this.SetProperty(ref this._simple, value.ToString()); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this structure represents a template.
        /// </summary>
        public bool Template
        {
            get { return this.Dictionary.To<bool>(this._template, false); }
            set { this.SetProperty(ref this._template, value.ToString()); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the data inheritance feature is enabled.
        /// </summary>
        public bool UsesDataInheritance
        {
            get { return this.Dictionary.To<bool>(this._usesdi, false); }
            set { this.SetProperty(ref this._usesdi, value.ToString()); }
        }

        /// <summary>
        /// Gets or sets the version of this structure.
        /// </summary>
        public string Version
        {
            get { return this._version; }
            set { this.SetProperty(ref this._version, value); }
        }

        /// <summary>
        /// Gets the member at the specified index.
        /// </summary>
        /// <param name="index">
        /// The zero-based index of the member to get.
        /// </param>
        /// <returns>
        /// The member at the specified index.
        /// </returns>
        public IMember this[int index]
        {
            get { return this._members[index]; }
        }

        /// <summary>
        /// Gets the member with the specified name.
        /// </summary>
        /// <param name="memberName">
        /// The name of the member to get.
        /// </param>
        /// <returns>
        /// The member with the specified name or null if the member doesn't exist.
        /// </returns>
        public IMember this[string memberName]
        {
            get
            {
                foreach (IMember member in this._members)
                {
                    if (String.Equals(member.Name, memberName))
                    {
                        return member;
                    }
                }

                return null;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new <see cref="Structure"/> that is a copy of this instance.
        /// </summary>
        /// <returns>
        /// A new <see cref="Structure"/> that is a copy of this instance.
        /// </returns>
        public new Structure Clone()
        {
            return new Structure(this, this.Dictionary);
        }

        /// <summary>
        /// Creates a deep copy of the current instance.
        /// </summary>
        /// <returns>
        /// A deep copy of the current System.Object.
        /// </returns>
        public override object DeepClone()
        {
            return this.Clone();
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="Structure"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public bool Equals(Structure other)
        {
            if (other == null)
            {
                return false;
            }

            if (!String.Equals(this.Name, other.Name))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="IStructure"/>.
        /// </summary>
        /// <param name="other">
        /// A <see cref="IStructure"/> to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public bool Equals(IStructure other)
        {
            return this.Equals(other as Structure);
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified
        /// <see cref="IDefinition"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public override bool Equals(IDefinition other)
        {
            return this.Equals(other as Structure);
        }

        /// <summary>
        /// Gets a read-only list containing all of the members for this structure that can be
        /// instanced.
        /// </summary>
        /// <returns>
        /// A read-only list containing all of the members for this structure.
        /// </returns>
        public IReadOnlyList<IMember> GetAllInstancedMembers()
        {
            List<IMember> members = new List<IMember>();
            foreach (IMember member in this._members)
            {
                if (!member.CanBeInstanced)
                {
                    continue;
                }

                members.Add(member);
            }

            IStructure baseStructure = this.BaseStructure;
            if (baseStructure != null)
            {
                foreach (IMember member in baseStructure.GetAllInstancedMembers())
                {
                    members.Insert(0, member);
                }
            }

            return members;
        }

        /// <summary>
        /// Attempts to get the member with the specified name that can also be instanced and
        /// returns a a index to the found member in amongst only the members that can
        /// be instanced.
        /// </summary>
        /// <param name="name">
        /// The name of the member to get.
        /// </param>
        /// <param name="member">
        /// When this method returns, contains the member with the specified name, if the
        /// member is found; otherwise, null.
        /// </param>
        /// <returns>
        /// The index to the instanced member if this structure contains a member with the
        /// specified name that can be instanced; otherwise, -1.
        /// </returns>
        public int GetInstancedMember(string name, out IMember member)
        {
            int index = -1;
            member = null;

            List<IStructure> structures = new List<IStructure>();
            structures.Add(this);

            IStructure baseStructure = this.BaseStructure;
            while (baseStructure != null)
            {
                structures.Insert(0, baseStructure);
                baseStructure = baseStructure.BaseStructure;
            }

            foreach (IStructure structure in structures)
            {
                foreach (IMember item in structure.Members)
                {
                    if (!item.CanBeInstanced)
                    {
                        continue;
                    }

                    index++;
                    if (!String.Equals(item.MetadataName, name))
                    {
                        continue;
                    }

                    member = item;
                    return index;
                }
            }

            return -1;
        }

        /// <summary>
        /// Gets a lookup table of all of the members that can be instanced in this structure,
        /// including the base structures with their position index.
        /// </summary>
        /// <returns>
        /// A lookup table of all of the members that can be instanced in this structure.
        /// </returns>
        public MemberInfo GetMemberInformation()
        {
            MemberInfo lookupTable = new MemberInfo();
            List<IStructure> structures = new List<IStructure>();
            structures.Add(this);

            IStructure baseStructure = this.BaseStructure;
            while (baseStructure != null)
            {
                structures.Insert(0, baseStructure);
                baseStructure = baseStructure.BaseStructure;
            }

            int index = 0;
            foreach (IStructure structure in structures)
            {
                foreach (IMember member in structure.Members)
                {
                    if (!member.CanBeInstanced)
                    {
                        continue;
                    }

                    string name = member.MetadataName;
                    if (!lookupTable.ContainsKey(name))
                    {
                        lookupTable.Add(name, new MemberOrderInfo(member, index));
                        index++;
                    }
                }
            }

            return lookupTable;
        }

        /// <summary>
        /// Determine whether the specified string is referencing this definition.
        /// </summary>
        /// <param name="type">
        /// The string that could be referencing this definition.
        /// </param>
        /// <returns>
        /// True if the specified string is referencing this definition; otherwise, false.
        /// </returns>
        public override bool IsReferencedBy(string type)
        {
            if (!String.IsNullOrWhiteSpace(this._name))
            {
                if (String.Equals(type, this._name))
                {
                    return true;
                }
            }

            return base.IsReferencedBy(type);
        }

        /// <summary>
        /// Determines whether this structure and the specified structure are related to each
        /// other based on the inheritance tree.
        /// </summary>
        /// <param name="structure">
        /// The structure to test against this structure.
        /// </param>
        /// <returns>
        /// True if the two structures are related; otherwise, false.
        /// </returns>
        public bool IsRelatedTo(IStructure structure)
        {
            if (Object.ReferenceEquals(this, structure) || structure == null)
            {
                return true;
            }

            IStructure baseStruct = structure.BaseStructure;
            bool related = false;
            while (baseStruct != null && !(related = Object.ReferenceEquals(this, baseStruct)))
            {
                baseStruct = baseStruct.BaseStructure;
            }

            if (related)
            {
                return true;
            }

            baseStruct = this.BaseStructure;
            related = false;
            while (baseStruct != null && !(related = Object.ReferenceEquals(this, baseStruct)))
            {
                baseStruct = baseStruct.BaseStructure;
            }

            return related;
        }

        /// <summary>
        /// Creates a new <see cref="IStructure"/> that is a copy of this instance.
        /// </summary>
        /// <returns>
        /// A new <see cref="IStructure"/> that is a copy of this instance.
        /// </returns>
        IStructure IStructure.Clone()
        {
            return this.Clone();
        }

        /// <summary>
        /// Determines whether this structure can be instanced inside a metadata file depending
        /// on any critical errors found.
        /// </summary>
        /// <param name="log">
        /// The log object that will receives any errors or warning found while validating.
        /// </param>
        /// <returns>
        /// True if this structure can be instanced inside a metadata file; otherwise false. If
        /// false the specified log object will have received the reasons why not.
        /// </returns>
        public bool IsValidForInstancing(ILog log)
        {
            bool valid = true;
            if (this._baseStructureName != null && this.BaseStructure == null)
            {
            }

            if (this._members == null)
            {
                return valid;
            }

            foreach (IMember member in this._members)
            {
                member.IsValidForInstancing(log);
            }

            return valid;
        }

        /// <summary>
        /// Writes the xml representation of this instance onto the specified
        /// System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that the xml representation of this instance should be
        /// written to.
        /// </param>
        /// <param name="saveFileInfo">
        /// If true the file, line number and line position will be written out as attributes.
        /// </param>
        public void Serialise(XmlWriter writer, bool saveFileInfo)
        {
            if (writer == null)
            {
                throw new SmartArgumentNullException(() => writer);
            }

            if (this.RawDataType != null)
            {
                writer.WriteAttributeString(DefinitionBase.XmlTypeAttr, this.RawDataType);
            }

            if (this._name != null)
            {
                writer.WriteAttributeString(XmlNameAttr, this._name);
            }

            if (this._baseStructureName != null)
            {
                writer.WriteAttributeString(XmlBaseAttr, this._baseStructureName);
            }

            if (this._version != null)
            {
                writer.WriteAttributeString(XmlVersionAttr, this._version);
            }

            if (this._constructible != null)
            {
                writer.WriteAttributeString(XmlConstructibleAttr, this._constructible);
            }

            if (this._usesdi != null)
            {
                writer.WriteAttributeString(XmlUsesDiAttr, this._usesdi);
            }

            if (this._register != null)
            {
                writer.WriteAttributeString(XmlAutoRegisterAttr, this._register);
            }

            if (this._preserveNames != null)
            {
                writer.WriteAttributeString(XmlPreserveNamesAttr, this._preserveNames);
            }

            if (this._template != null)
            {
                writer.WriteAttributeString(XmlTemplateAttr, this._template);
            }

            if (this._onPreLoad != null)
            {
                writer.WriteAttributeString(XmlOnPreLoadAttr, this._onPreLoad);
            }

            if (this._onPostLoad != null)
            {
                writer.WriteAttributeString(XmlOnPostLoadAttr, this._onPostLoad);
            }

            if (this._onPreSave != null)
            {
                writer.WriteAttributeString(XmlOnPreSaveAttr, this._onPreSave);
            }

            if (this._onPostSave != null)
            {
                writer.WriteAttributeString(XmlOnPostSaveAttr, this._onPostSave);
            }

            if (this._onPreAddWidgets != null)
            {
                writer.WriteAttributeString(XmlOnPreAddWidgetsAttr, this._onPreAddWidgets);
            }

            if (this._onPostAddWidgets != null)
            {
                writer.WriteAttributeString(XmlOnPostAddWidgetsAttr, this._onPostAddWidgets);
            }

            if (this._onPreSet != null)
            {
                writer.WriteAttributeString(XmlOnPreSetAttr, this._onPreSet);
            }

            if (this._onPostSet != null)
            {
                writer.WriteAttributeString(XmlTemplateAttr, this._onPostSet);
            }

            if (this._layoutHint != null)
            {
                writer.WriteAttributeString(XmlOnPostSetAttr, this._layoutHint);
            }

            if (this._category != null)
            {
                writer.WriteAttributeString(XmlCategoryAttr, this._category);
            }

            if (this._filter != null)
            {
                writer.WriteAttributeString(XmlFilterAttr, this._filter);
            }

            if (this._fullPost != null)
            {
                writer.WriteAttributeString(XmlFullPostAttr, this._fullPost);
            }

            if (this._isRootStructure != null)
            {
                writer.WriteAttributeString(XmlRootAttr, this._isRootStructure);
            }

            if (saveFileInfo)
            {
                writer.WriteAttributeString(DefinitionBase.XmlFileAttr, this.Filename);
                writer.WriteAttributeString(
                    DefinitionBase.XmlLineNumberAttr, this.LineNumber.ToString());
                writer.WriteAttributeString(
                    DefinitionBase.XmlLinePositionAttr, this.LinePosition.ToString());
            }

            if (this._members != null)
            {
                foreach (IMember member in this._members)
                {
                    ////writer.WriteStartElement(MemberFactory.GetNodeNameForMember(member));
                    ////member.Serialise(writer);
                    ////writer.WriteEndElement();
                }
            }
        }

        /// <summary>
        /// Validates this entity and creates a validation result object containing the errors
        /// and warnings.
        /// </summary>
        /// <param name="recursive">
        /// A value indicating whether any child models to this one should also be validated.
        /// </param>
        /// <returns>
        /// A validation result object containing all of the errors and warnings associated
        /// with this validation pass.
        /// </returns>
        public override ValidationResult Validate(bool recursive)
        {
            ValidationResult result = new ValidationResult();
            foreach (string attribute in this._unrecognisedAttributes)
            {
                string msg = StringTable.UnrecognisedStructureAttribute;
                msg = msg.FormatCurrent(attribute);
                result.AddWarning(msg, this.Location);
            }

            if (this.RawDataType == null)
            {
                string msg = StringTable.MissingTypeAttributeError;
                result.AddError(msg, this.Location);
            }
            else if (String.IsNullOrWhiteSpace(this.RawDataType))
            {
                string msg = StringTable.EmptyTypeAttributeError;
                result.AddError(msg, this.Location);
            }

            if (this._baseStructureName != null)
            {
                if (this.BaseStructure == null)
                {
                    string msg = StringTable.UnresolvedBaseTypeError;
                    msg = msg.FormatCurrent(this._baseStructureName);
                    result.AddError(msg, this.Location);
                }
            }

            if (!this.Dictionary.Validate<bool>(this._register, true))
            {
                string msg = StringTable.FormatAttributeWarning;
                msg = msg.FormatCurrent(XmlAutoRegisterAttr, this._register, "boolean");
                result.AddWarning(msg, this.Location);
            }

            if (!this.Dictionary.Validate<bool>(this._constructible, true))
            {
                string msg = StringTable.FormatAttributeWarning;
                msg = msg.FormatCurrent(XmlConstructibleAttr, this._constructible, "boolean");
                result.AddWarning(msg, this.Location);
            }

            if (!this.Dictionary.Validate<bool>(this._simple, true))
            {
                string msg = StringTable.FormatAttributeWarning;
                msg = msg.FormatCurrent(XmlSimpleAttr, this._simple, "boolean");
                result.AddWarning(msg, this.Location);
            }

            if (!this.Dictionary.Validate<bool>(this._preserveNames, true))
            {
                string msg = StringTable.FormatAttributeWarning;
                msg = msg.FormatCurrent(XmlPreserveNamesAttr, this._preserveNames, "boolean");
                result.AddWarning(msg, this.Location);
            }

            if (!this.Dictionary.Validate<bool>(this._template, true))
            {
                string msg = StringTable.FormatAttributeWarning;
                msg = msg.FormatCurrent(XmlTemplateAttr, this._template, "boolean");
                result.AddWarning(msg, this.Location);
            }

            if (!this.Dictionary.Validate<bool>(this._usesdi, true))
            {
                string msg = StringTable.FormatAttributeWarning;
                msg = msg.FormatCurrent(XmlUsesDiAttr, this._usesdi, "boolean");
                result.AddWarning(msg, this.Location);
            }

            HashSet<string> memberNames = new HashSet<string>();
            HashSet<string> memberMetadataNames = new HashSet<string>();
            foreach (IMember member in this._members)
            {
                if (member == null)
                {
                    continue;
                }

                string name = member.Name;
                if (name != null)
                {
                    if (!memberNames.Contains(name))
                    {
                        memberNames.Add(name);
                    }
                    else
                    {
                        string msg = StringTable.DuplicateMemberNameError;
                        msg = msg.FormatCurrent(this.DataType, name);
                        result.AddError(msg, member.Location);
                    }
                }

                if (member.CanBeInstanced)
                {
                    string metadataName = member.MetadataName;
                    if (metadataName != null)
                    {
                        if (!memberMetadataNames.Contains(metadataName))
                        {
                            memberMetadataNames.Add(metadataName);
                        }
                        else
                        {
                            string msg = StringTable.DuplicateMemberMetadataNameError;
                            msg = msg.FormatCurrent(this.DataType, metadataName);
                            result.AddError(msg, member.Location);
                        }
                    }
                }

                UnknownMember unknownMember = member as UnknownMember;
                if (unknownMember != null)
                {
                    string msg = StringTable.UnknownMemberTypeError;
                    msg = msg.FormatCurrent(this.DataType, unknownMember.XmlNodeName);
                    result.AddError(msg, member.Location);
                }
            }

            if (recursive)
            {
                foreach (IMember member in this._members)
                {
                    result.AddChildResults(member.Validate(true));
                }
            }

            return result;
        }

        /// <summary>
        /// Initialises this instance using the specified System.Xml.XmlReader as a
        /// data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader used as the data provider to initialise this
        /// instance with.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            bool isEmptyElement = reader.IsEmptyElement;
            for (int i = 0; i < reader.AttributeCount; i++)
            {
                reader.MoveToAttribute(i);
                string name = reader.LocalName;
                string value = reader.Value;

                if (String.Equals(name, DefinitionBase.XmlTypeAttr))
                {
                    this.SetRawDataType(value);
                }
                else if (String.Equals(name, XmlNameAttr))
                {
                    this._name = value;
                }
                else if (String.Equals(name, XmlBaseAttr))
                {
                    this._baseStructureName = value;
                }
                else if (String.Equals(name, XmlVersionAttr))
                {
                    this._version = value;
                }
                else if (String.Equals(name, XmlConstructibleAttr))
                {
                    this._constructible = value;
                }
                else if (String.Equals(name, XmlCategoryAttr))
                {
                    this._category = value;
                }
                else if (String.Equals(name, XmlFilterAttr))
                {
                    this._filter = value;
                }
                else if (String.Equals(name, XmlFullPostAttr))
                {
                    this._fullPost = value;
                }
                else if (String.Equals(name, XmlUsesDiAttr))
                {
                    this._usesdi = value;
                }
                else if (String.Equals(name, XmlAutoRegisterAttr))
                {
                    this._register = value;
                }
                else if (String.Equals(name, XmlPreserveNamesAttr))
                {
                    this._preserveNames = value;
                }
                else if (String.Equals(name, XmlSimpleAttr))
                {
                    this._simple = value;
                }
                else if (String.Equals(name, XmlTemplateAttr))
                {
                    this._template = value;
                }
                else if (String.Equals(name, XmlOnPreLoadAttr))
                {
                    this._onPreLoad = value;
                }
                else if (String.Equals(name, XmlOnPostLoadAttr))
                {
                    this._onPostLoad = value;
                }
                else if (String.Equals(name, XmlOnPreSaveAttr))
                {
                    this._onPreSave = value;
                }
                else if (String.Equals(name, XmlOnPostSaveAttr))
                {
                    this._onPostSave = value;
                }
                else if (String.Equals(name, XmlOnPreAddWidgetsAttr))
                {
                    this._onPreAddWidgets = value;
                }
                else if (String.Equals(name, XmlOnPostAddWidgetsAttr))
                {
                    this._onPostAddWidgets = value;
                }
                else if (String.Equals(name, XmlOnPreSetAttr))
                {
                    this._onPreSet = value;
                }
                else if (String.Equals(name, XmlOnPostSetAttr))
                {
                    this._onPostSet = value;
                }
                else if (String.Equals(name, XmlLayoutHintAttr))
                {
                    this._layoutHint = value;
                }
                else if (String.Equals(name, XmlRootAttr))
                {
                    this._isRootStructure = value;
                }
                else
                {
                    this._unrecognisedAttributes.Add(name);
                }
            }

            if (isEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToElementOrComment())
            {
                if (reader.NodeType == XmlNodeType.Comment)
                {
                    reader.Skip();
                    continue;
                }

                IMember member = MemberFactory.Create(reader, this);
                if (member == null)
                {
                    reader.Skip();
                    continue;
                }

                this._members.Add(member);
            }

            if (reader.NodeType == XmlNodeType.EndElement)
            {
                reader.ReadEndElement();
            }
        }

        /// <summary>
        /// Looks through the specified scope and returns the structure that this structure
        /// uses as a base.
        /// </summary>
        /// <param name="scope">
        /// A definition dictionary containing all the other types in the search scope.
        /// </param>
        /// <returns>
        /// This structures base structure.
        /// </returns>
        private IStructure GetBaseStructure(IDefinitionDictionary scope)
        {
            if (this.BaseStructureName == null || scope == null)
            {
                return null;
            }

            return scope.GetStructure(this.BaseStructureName);
        }

        /// <summary>
        /// Looks through the specified scope and returns all other structures that uses this
        /// structure as a base class.
        /// </summary>
        /// <param name="scope">
        /// A iterate over all the structures in the search scope.
        /// </param>
        /// <returns>
        /// All structures in the specified scope that derive off this structure.
        /// </returns>
        private IEnumerable<IStructure> GetDescendants(IEnumerable<IStructure> scope)
        {
            return from s in scope
                   where s.BaseStructureName == this.Name
                   select s;
        }

        /// <summary>
        /// Gets the metadata name for this structure from the specified name.
        /// </summary>
        /// <param name="name">
        /// The name to use to get the metadata name from.
        /// </param>
        /// <returns>
        /// The metadata name for this structure based on the specified name.
        /// </returns>
        private string GetMetadataName(string name)
        {
            if (String.IsNullOrWhiteSpace(name))
            {
                return String.Empty;
            }

            return name.TrimStart(':').Replace("::", "__");
        }

        /// <summary>
        /// Gets the name for this structure from the specified data type.
        /// </summary>
        /// <param name="dataType">
        /// The data type to use to get the name from.
        /// </param>
        /// <returns>
        /// The name for this structure based on the specified data type.
        /// </returns>
        private string GetNameFromDataType(string dataType)
        {
            if (String.IsNullOrWhiteSpace(dataType))
            {
                return String.Empty;
            }

            return dataType.TrimStart(':');
        }
        #endregion Methods
    } // RSG.Metadata.Model.Definitions.Structure {Class}
} // RSG.Metadata.Model.Definitions {Namespace}
