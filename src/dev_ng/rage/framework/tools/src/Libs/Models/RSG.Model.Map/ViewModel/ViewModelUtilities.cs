﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Map.ViewModel
{
    /// <summary>
    /// Contains static members to be used on the view model
    /// </summary>
    public static class ViewModelUtilities
    {
        /// <summary>
        /// Determines if the given container has the given component within it. 
        /// </summary>
        /// <param name="container"></param>
        /// <param name="?"></param>
        /// <returns></returns>
        public static Boolean ContainsComponent(IMapViewModelContainer container, IMapViewModelComponent component, Boolean recusive)
        {
            foreach (IMapViewModelComponent childComponent in container.ComponentChildren)
            {
                if (childComponent == component)
                {
                    return true;
                }
            }

            foreach (IMapViewModelContainer childContainer in container.ContainerChildren)
            {
                if (childContainer == component)
                {
                    return true;
                }

                if (recusive == true)
                {
                    if (ContainsComponent(childContainer, component, true))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Determines if the given container has a component within it with the given name. 
        /// </summary>
        /// <param name="container"></param>
        /// <param name="?"></param>
        /// <returns></returns>
        public static Boolean ContainsComponent(IMapViewModelContainer container, String name, Boolean recusive)
        {
            if (container != null)
            {
                foreach (IMapViewModelComponent childComponent in container.ComponentChildren)
                {
                    if (childComponent.Name == name)
                    {
                        return true;
                    }
                }

                foreach (IMapViewModelContainer childContainer in container.ContainerChildren)
                {
                    if (childContainer.Name == name)
                    {
                        return true;
                    }

                    if (recusive == true)
                    {
                        if (ContainsComponent(childContainer, name, true))
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Finds a container with the given name if one exists starting at the given container
        /// </summary>
        /// <param name="container"></param>
        /// <param name="name"></param>
        /// <param name="recusive"></param>
        /// <returns></returns>
        public static IMapViewModelContainer FindContainer(IMapViewModelContainer container, String name, Boolean recusive)
        {
            foreach (IMapViewModelContainer childContainer in container.ContainerChildren)
            {
                if (childContainer.Name == name)
                {
                    return childContainer;
                }

                if (recusive == true)
                {
                    IMapViewModelContainer recusiveResult = FindContainer(container, name, true);
                    if (recusiveResult != null)
                    {
                        return recusiveResult;
                    }
                }
            }

            return null;
        }

        public static IEnumerable<MapSectionViewModel> GetNonGenericWithDefinitionSections(IMapViewModelContainer container, Boolean recusive)
        {
            foreach (MapSectionViewModel section in container.SectionChildren)
            {
                if (section.Model.ExportDefinitions == true && section.Model.ExportInstances == true)
                {
                    yield return section;
                }
            }

            if (recusive == true)
            {
                foreach (IMapViewModelContainer childContainer in container.ContainerChildren)
                {
                    foreach (MapSectionViewModel section in GetNonGenericWithDefinitionSections(childContainer, true))
                    {
                        yield return section;
                    }
                }
            }
        }

        public static IEnumerable<IMapViewModelContainer> GetNonGenericWithDefinitionAreas(IMapViewModelContainer container, Boolean recusive)
        {
            if (container.ContainsNonGenericWithDefinitionChildren(recusive))
            {
                yield return container;
            }

            if (recusive == true)
            {
                foreach (IMapViewModelContainer childContainer in container.ContainerChildren)
                {
                    foreach (IMapViewModelContainer nextContainer in GetNonGenericWithDefinitionAreas(childContainer, true))
                    {
                        yield return nextContainer;
                    }
                }
            }
        }

        public static IEnumerable<IMapSectionViewModel> GetAllMapSections(IMapViewModelContainer container, Boolean recusive)
        {
            foreach (IMapSectionViewModel section in container.SectionChildren)
            {
                yield return section;
            }

            if (recusive == true)
            {
                foreach (IMapViewModelContainer childContainer in container.ContainerChildren)
                {
                    foreach (IMapSectionViewModel section in GetAllMapSections(childContainer, true))
                    {
                        yield return section;
                    }
                }
            }
        }
    }
}
