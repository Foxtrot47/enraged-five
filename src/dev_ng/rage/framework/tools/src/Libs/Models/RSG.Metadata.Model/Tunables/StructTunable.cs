﻿//---------------------------------------------------------------------------------------------
// <copyright file="StructTunable.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Tunables
{
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using System.Xml.Linq;
    using RSG.Base.Extensions;
    using RSG.Base.Logging;
    using RSG.Editor;
    using RSG.Editor.Model;
    using RSG.Metadata.Model.Definitions;
    using RSG.Metadata.Model.Definitions.Members;
    using RSG.Metadata.Model.Resources;

    /// <summary>
    /// Represents a tunable used by the parCodeGen system that is instancing a
    /// <see cref="StructMember"/> object.
    /// </summary>
    public class StructTunable : TunableBase, ITunableParent
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Tunables"/> property.
        /// </summary>
        private ReadOnlyModelCollection<ITunable> _readOnlyTunables;

        /// <summary>
        /// The private field for the <see cref="Tunables"/> property.
        /// </summary>
        private ModelCollection<ITunable> _tunables;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="StructTunable"/> class to be as
        /// instance of the specified member.
        /// </summary>
        /// <param name="member">
        /// The member that this tunable will be instancing.
        /// </param>
        /// <param name="parent">
        /// The tunable parent this tunable will belong to.
        /// </param>
        public StructTunable(StructMember member, ITunableParent parent)
            : base(member, parent)
        {
            this._tunables = new ModelCollection<ITunable>(this);
            this.CreateTunables();
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="StructTunable"/> class as a copy of
        /// the specified instance.
        /// </summary>
        /// <param name="other">
        /// The instance to copy.
        /// </param>
        /// <param name="parent">
        /// The tunable parent this tunable will belong to.
        /// </param>
        public StructTunable(StructTunable other, ITunableParent parent)
            : base(other, parent)
        {
            List<ITunable> tunables = new List<ITunable>();
            foreach (ITunable tunable in other.Tunables)
            {
                tunables.Add(tunable.Clone());
            }

            this._tunables = new ModelCollection<ITunable>(this, tunables);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="StructTunable"/> class using the
        /// specified System.Xml.XmlReader as a initialising data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <param name="member">
        /// The member that this tunable will be instancing.
        /// </param>
        /// <param name="parent">
        /// The tunable parent this tunable will belong to.
        /// </param>
        /// <param name="log">
        /// A object that is used to log any warnings or errors produced during the parsing of
        /// the specified reader.
        /// </param>
        public StructTunable(
            XmlReader reader, StructMember member, ITunableParent parent, ILog log)
            : base(reader, member, parent)
        {
            this._tunables = new ModelCollection<ITunable>(this);
            this.Deserialise(reader, log);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the array member that this tunable is instancing.
        /// </summary>
        public StructMember StructMember
        {
            get
            {
                StructMember member = this.Member as StructMember;
                if (member != null)
                {
                    return member;
                }

                return new StructMember(this.TunableStructure.Definition);
            }
        }

        /// <summary>
        /// Gets a collection of the tunables that are defined for this array.
        /// </summary>
        public ReadOnlyModelCollection<ITunable> Tunables
        {
            get
            {
                if (this._readOnlyTunables == null)
                {
                    this._readOnlyTunables = this._tunables.ToReadOnly();
                }

                return this._readOnlyTunables;
            }
        }

        /// <summary>
        /// Gets the tunable with the specified metadata name.
        /// </summary>
        /// <param name="metadataName">
        /// The metadata name of the tunable to return.
        /// </param>
        /// <returns>
        /// The tunable with the specified metadata name.
        /// </returns>
        public ITunable this[string metadataName]
        {
            get
            {
                StringComparison comparisonType = StringComparison.OrdinalIgnoreCase;
                foreach (ITunable tunable in this._tunables)
                {
                    string name = tunable.Member.MetadataName;
                    if (String.Equals(name, metadataName, comparisonType))
                    {
                        return tunable;
                    }
                }

                return null;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a XML element node object for a structure tunable with the specified
        /// content.
        /// </summary>
        /// <param name="elemName">
        /// The XML element node name to give this element.
        /// </param>
        /// <param name="content">
        /// The contents of the element created.
        /// </param>
        /// <returns>
        /// A new XML Element node object that represents a structure tunable with the
        /// specified content.
        /// </returns>
        public static XElement CreateXElement(string elemName, params object[] content)
        {
            return new XElement(elemName, content);
        }

        /// <summary>
        /// Creates a new <see cref="StructTunable"/> that is a copy of this instance.
        /// </summary>
        /// <returns>
        /// A new <see cref="StructTunable"/> that is a copy of this instance.
        /// </returns>
        public new StructTunable Clone()
        {
            return new StructTunable(this, this.TunableStructure);
        }

        /// <summary>
        /// Creates a deep copy of the current instance.
        /// </summary>
        /// <returns>
        /// A deep copy of the current System.Object.
        /// </returns>
        public override object DeepClone()
        {
            return this.Clone();
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="ITunable"/> by
        /// looking at their values.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance by their values;
        /// otherwise, false.
        /// </returns>
        public override bool EqualByValue(ITunable other)
        {
            return this.EqualByValue(other as StructTunable);
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="ITunable"/> by
        /// looking at their values.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance by their values;
        /// otherwise, false.
        /// </returns>
        public bool EqualByValue(StructTunable other)
        {
            if (other == null)
            {
                return false;
            }

            if (this.Tunables.Count != other.Tunables.Count)
            {
                return false;
            }

            for (int i = 0; i < this.Tunables.Count; i++)
            {
                if (!this.Tunables[i].EqualByValue(other.Tunables[i]))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified
        /// <see cref="StructTunable"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public bool Equals(StructTunable other)
        {
            if (other == null)
            {
                return false;
            }

            return Object.ReferenceEquals(other, this);
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="ITunable"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public override bool Equals(ITunable other)
        {
            return this.Equals(other as StructTunable);
        }

        /// <summary>
        /// Resets this tunable to have its default value. (i.e no source data with its initial
        /// values).
        /// </summary>
        public override void ResetValueToDefaultCore()
        {
            foreach (ITunable tunable in this._tunables)
            {
                tunable.ResetValueToDefault();
            }
        }

        /// <summary>
        /// Writes the xml representation of this instance onto the specified
        /// System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that the xml representation of this instance should be
        /// written to.
        /// </param>
        /// <param name="serialiseDefaultTunables">
        /// A value indicating whether the tunables that have a default value should be written
        /// to the specified writer or not.
        /// </param>
        public override void Serialise(XmlWriter writer, bool serialiseDefaultTunables)
        {
            if (writer == null)
            {
                throw new SmartArgumentNullException(() => writer);
            }

            foreach (ITunable tunable in this._tunables)
            {
                if (!serialiseDefaultTunables && tunable.HasDefaultValue)
                {
                    continue;
                }

                writer.WriteStartElement(tunable.Member.MetadataName);
                tunable.Serialise(writer, serialiseDefaultTunables);
                writer.WriteEndElement();
            }
        }

        /// <summary>
        /// Updates this tunables HasDefaultValue property.
        /// </summary>
        public void UpdateHasDefaultValue()
        {
            bool hasDefaultValue = true;
            foreach (ITunable tunable in this._tunables)
            {
                if (!tunable.HasDefaultValue)
                {
                    hasDefaultValue = false;
                    break;
                }
            }

            this.HasDefaultValue = hasDefaultValue;
        }

        /// <summary>
        /// Called whenever the tunable whose value will be used when this tunable is being
        /// inherited changes.
        /// </summary>
        protected override void InheritanceParentChanged(ITunable oldValue, ITunable newValue)
        {
            StructTunable source = newValue as StructTunable;
            if (source != null && Object.ReferenceEquals(source.Member, this.Member))
            {
                foreach (ITunable parentTunable in source.Tunables)
                {
                    string name = parentTunable.Member.MetadataName;
                    ITunable childTunable = this[name];
                    if (childTunable != null)
                    {
                        ((TunableBase)childTunable).SetInheritanceParent(parentTunable);
                    }
                }
            }
        }

        /// <summary>
        /// Creates the tunables that represent the associated definition members.
        /// </summary>
        private void CreateTunables()
        {
            MemberInfo members = this.StructMember.ReferencedStructure.GetMemberInformation();
            ITunable[] tunables = new ITunable[members.Count];
            TunableFactory factory = new TunableFactory(this);
            foreach (MemberOrderInfo memberInfo in members.Values)
            {
                ITunable tunable = factory.Create(memberInfo.Member);
                if (tunable == null)
                {
                    throw new MetadataException(
                        "Unable to create a tunable for the member {0}.",
                        memberInfo.Member.Name);
                }

                tunables[memberInfo.Position] = tunable;
            }

            this._tunables.AddRange(tunables);
        }

        /// <summary>
        /// Creates the tunables that represent the specified definition members using the
        /// specified System.Xml.XmlReader as a initialising data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// the tunables from.
        /// </param>
        /// <param name="line">
        /// The reader line number to add to any thrown exceptions.
        /// </param>
        /// <param name="pos">
        /// The reader position number to add to any thrown exceptions.
        /// </param>
        /// <param name="log">
        /// A object that is used to log any warnings or errors produced during the parsing of
        /// the specified reader.
        /// </param>
        /// <returns>
        /// A array containing the tunables that have been created.
        /// </returns>
        private ITunable[] CreateTunables(XmlReader reader, string line, string pos, ILog log)
        {
            MemberInfo members = this.StructMember.ReferencedStructure.GetMemberInformation();
            ITunable[] tunables = new ITunable[members.Count];
            TunableFactory factory = new TunableFactory(this);

            if (!reader.IsEmptyElement)
            {
                reader.ReadStartElement();
                while (reader.MoveToElementOrComment())
                {
                    if (reader.NodeType == XmlNodeType.Comment)
                    {
                        reader.Skip();
                        continue;
                    }

                    IXmlLineInfo lineInfo = reader as IXmlLineInfo;
                    string memberLine = lineInfo.LineNumber.ToStringInvariant();
                    string memberPos = lineInfo.LinePosition.ToStringInvariant();

                    MemberOrderInfo orderInfo;
                    if (!members.TryGetValue(reader.Name, out orderInfo))
                    {
                        string msg = StringTable.StructAdditionalMemberFoundError;
                        log.Warning(msg, reader.Name, memberLine, memberPos);
                        reader.Skip();
                        continue;
                    }

                    ITunable tunable = factory.Create(reader, orderInfo.Member, log);
                    if (tunable == null)
                    {
                        string type = orderInfo.Member.TypeName;
                        UnknownMember member = orderInfo.Member as UnknownMember;
                        if (member != null)
                        {
                            type = member.XmlNodeName;
                        }

                        string msg = StringTable.StructUnknownMemberError;
                        throw new MetadataException(
                            msg,
                            orderInfo.Member.Name,
                            type,
                            this.StructMember.ReferencedStructure.ShortDataType,
                            line,
                            pos);
                    }

                    tunables[orderInfo.Position] = tunable;
                }
            }

            foreach (MemberOrderInfo memberInfo in members.Values)
            {
                int index = memberInfo.Position;
                if (tunables[index] != null)
                {
                    continue;
                }

                ITunable tunable = factory.Create(memberInfo.Member);
                if (tunable == null)
                {
                    string type = memberInfo.Member.TypeName;
                    UnknownMember member = memberInfo.Member as UnknownMember;
                    if (member != null)
                    {
                        type = member.XmlNodeName;
                    }

                    string msg = StringTable.StructUnknownMemberError;
                    throw new MetadataException(
                        msg,
                        memberInfo.Member.Name,
                        type,
                        this.StructMember.ReferencedStructure.ShortDataType,
                        line,
                        pos);
                }

                tunables[index] = tunable;
            }

            return tunables;
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        /// <param name="log">
        /// A object that is used to log any warnings or errors produced during the parsing of
        /// the specified reader.
        /// </param>
        private void Deserialise(XmlReader reader, ILog log)
        {
            if (reader == null)
            {
                return;
            }

            IXmlLineInfo lineInfo = reader as IXmlLineInfo;
            string line = lineInfo.LineNumber.ToStringInvariant();
            string pos = lineInfo.LinePosition.ToStringInvariant();

            try
            {
                IStructure referenced = this.StructMember.ReferencedStructure;
                if (referenced == null)
                {
                    string msg = StringTable.StructMissingReferenceTypeError;
                    throw new MetadataException(msg, this.StructMember.Type, line, pos);
                }

                this._tunables.AddRange(this.CreateTunables(reader, line, pos, log));
            }
            catch (MetadataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                string msg = StringTable.StructTunableDeserialiseError;
                throw new MetadataException(msg, line, pos, ex.Message);
            }

            reader.Skip();
        }
        #endregion Methods
    } // RSG.Metadata.Model.Tunables.StructTunable {Class}
} // RSG.Metadata.Model.Tunables {Namespace}
