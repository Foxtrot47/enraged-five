﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Math;
using RSG.SceneXml;

namespace RSG.Model.Common.Map
{
    /// <summary>
    /// Interface for stated animations.
    /// Note that stats associated with this object represent the worst case of the child archetype stats.
    /// </summary>
    public interface IStatedAnimArchetype : IMapArchetype
    {
        #region Properties
        /// <summary>
        /// The archetypes that make up this stated anim
        /// </summary>
        ICollection<IMapArchetype> ComponentArchetypes { get; }
        #endregion // Properties
    } // IStatedAnimArchetype
}
