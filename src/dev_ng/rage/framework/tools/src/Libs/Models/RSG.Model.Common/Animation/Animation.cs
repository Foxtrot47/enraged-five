﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.Model.Common.Animation;
using System.ComponentModel;
using RSG.Base.ConfigParser;
using System.IO;
using System.Xml.Linq;
using RSG.Base.Logging;
using System.Xml.XPath;
using System.Runtime.Serialization;

namespace RSG.Model.Common.Animation
{
    [DataContract]
    public class Animation : AssetBase, IAnimation
    {
        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public Animation(String name)
            : base(name)
        {
        }
        #endregion // Constructor(s)

        #region Public Methods
        #endregion // Public Methods

        #region IComparable<IAnimation> Interface
        /// <summary>
        /// Compare this animation to another.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(IAnimation other)
        {
            if (other == null)
            {
                return 1;
            }

            return base.CompareTo(other); //(Name.CompareTo(other.Name));
        }
        #endregion // IComparable<IAnimation> Interface

        #region IEquatable<IAnimation> Interface
        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>true if the current object is equal to the other parameter; otherwise, false.</returns>
        public bool Equals(IAnimation other)
        {
            if (other == null)
            {
                return false;
            }

            return (Name == other.Name);
        }
        #endregion // IEquatable<IAnimation> Interface
 
    } //  class Animation
}
