﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.Model.Common.Map;
using RSG.Base.Math;
using System.ComponentModel;
using RSG.SceneXml;
using RSG.Statistics.Common.Dto.GameAssetStats;

namespace RSG.Model.Map3
{
    /// <summary>
    /// 
    /// </summary>
    public class CarGen : AssetBase, ICarGen
        {
        #region Properties
        /// <summary>
        /// The parent section of this archetype
        /// </summary>
        public IMapSection ParentSection
        {
            get;
            protected set;
        }

        /// <summary>
        /// 
        /// </summary>
        public Vector3f Position
        {
            get;
            set;
        }

        /// <summary>
        /// Position in world-space.
        /// </summary>
        public Quaternionf Rotation
        {
            get;
            private set;
        }

        /// <summary>
        /// Position in world-space.
        /// </summary>
        public Matrix34f Transform
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsPolice
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsFire
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsAmbulance
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsSpecificModel
        {
            get
            {
                return !String.IsNullOrEmpty(ModelName);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string ModelName
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsHighPriority
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public CarGen(string name, IMapSection parentSection)
            : base(name)
        {
            ParentSection = parentSection;
        }
        #endregion // Constructor(s)
        
        #region IEntity Implementation
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sceneObject"></param>
        public void LoadStatsFromExportData(TargetObjectDef sceneObject)
        {
            Position = sceneObject.NodeTransform.Translation;
            IsPolice = sceneObject.GetAttribute(AttrNames.CARGEN_POLICE, AttrDefaults.CARGEN_POLICE);
            IsFire = sceneObject.GetAttribute(AttrNames.CARGEN_FIRETRUCK, AttrDefaults.CARGEN_FIRETRUCK);
            IsAmbulance = sceneObject.GetAttribute(AttrNames.CARGEN_AMBULANCE, AttrDefaults.CARGEN_AMBULANCE);
            ModelName = sceneObject.GetAttribute(AttrNames.CARGEN_MODEL, AttrDefaults.CARGEN_MODEL);
            IsHighPriority = sceneObject.GetAttribute(AttrNames.CARGEN_HIGH_PRIORITY, AttrDefaults.CARGEN_HIGH_PRIORITY);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public void LoadStatsFromDatabase(CarGenStatDto dto)
        {
            Position = dto.Position;
            IsPolice = dto.IsPolice;
            IsFire = dto.IsFire;
            IsAmbulance = dto.IsAmbulance;
            ModelName = dto.ModelName;
            IsHighPriority = dto.IsHighPriority;
        }
        #endregion // IEntity Implementation

        #region Object Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return (String.Format("CarGen: [{0}]{1}", this.Name, this.Hash));
        }
        #endregion // Object Overrides
    } // CarGen
}
