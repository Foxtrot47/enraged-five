﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common.Map
{
    /// <summary>
    /// Enumeration of all streamable stat a drawable object can have.
    /// </summary>
    public sealed class StreamableDrawableArchetypeStat : StreamableSimpleMapArchetypeStat
    {
        #region Constructor(s)
        /// <summary>
        /// Private so that people can't create new ones on the fly.
        /// </summary>
        /// <param name="value"></param>
        private StreamableDrawableArchetypeStat()
            : base()
        {
        }
        #endregion // Constructor(s)
    } // StreamableDrawableArchetypeStat
}
