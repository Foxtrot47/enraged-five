﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.ConfigParser;
using RSG.Model.Common;
using RSG.Model.Map;
using RSG.Base.Logging;
using RSG.Base.Collections;

namespace RSG.Model.Report.Reports
{
    /// <summary>
    /// 
    /// </summary>
    public class ShaderCSVReport : CSVReport, IDynamicMapReport
    {
        #region Constants
        private const String NAME = "Export Shader CSV Report";
        private const String DESCRIPTION = "Exports the selected levels shader usage into a .csv file";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Determines whether the map data should be loaded
        /// </summary>
        public bool LoadMapData
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Determines whether the car generators should be loaded
        /// 
        /// </summary>
        public bool LoadCarGenarators
        {
            get { return false; }
        }
        #endregion // Properties

        #region Construtcor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public ShaderCSVReport()
            : base(NAME, DESCRIPTION)
        {
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Generate the report dynamically.
        /// </summary>
        /// <param name="level"></param>
        /// <param name="gv"></param>
        public void Generate(ILevel level, ConfigGameView gv, IPlatformCollection platforms)
        {
            string directory = Path.GetDirectoryName(Filename);
            string filename = Path.GetFileNameWithoutExtension(Filename);

            string exteriorFilename = Path.Combine(directory, filename + ".csv");
            string interiorFilename = Path.Combine(directory, filename + "_interiors.csv");

            DateTime start = DateTime.Now;
            ExportExteriorShaderCSVReport(exteriorFilename, level, gv);
            ExportInteriorShaderCSVReport(interiorFilename, level, gv);
            Log.Log__Message("Shader CSV export complete. (report took {0} milliseconds to create.)", (DateTime.Now - start).TotalMilliseconds);
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Exports all exterior shaders
        /// </summary>
        /// <param name="filepath"></param>
        /// <param name="level"></param>
        /// <param name="gv"></param>
        private static void ExportExteriorShaderCSVReport(string filepath, ILevel level, ConfigGameView gv)
        {
            RSG.Model.Map.Util.Creation.InitialiseLevelStatistics(level, gv);

            // Process the level to get the shader information
            Dictionary<string, HashSet<MapDefinition>> definitionsMap = new Dictionary<string, HashSet<MapDefinition>>();
            Dictionary<string, HashSet<MapSection>> sectionsMap = new Dictionary<string, HashSet<MapSection>>();

            foreach (MapSection section in RSG.Model.Map.Util.Enumerations.GetAllMapSections(level, null, true))
            {
                foreach (IAsset asset in section.AssetChildren)
                {
                    if (asset is MapInstance)
                    {
                        ProcessMapInstance((MapInstance)asset, section, ref definitionsMap, ref sectionsMap);
                    }
                }
            }

            // Now write the retrieved data to a csv file
            WriteCsvFile(filepath, definitionsMap, sectionsMap);
        }

        /// <summary>
        /// Exports all interior shaders
        /// </summary>
        /// <param name="filepath"></param>
        /// <param name="level"></param>
        /// <param name="gv"></param>
        private static void ExportInteriorShaderCSVReport(string filepath, ILevel level, ConfigGameView gv)
        {
            // Process the level to get the shader information
            Dictionary<string, HashSet<MapDefinition>> definitionsMap = new Dictionary<string, HashSet<MapDefinition>>();
            Dictionary<string, HashSet<MapSection>> sectionsMap = new Dictionary<string, HashSet<MapSection>>();

            foreach (MapSection section in RSG.Model.Map.Util.Enumerations.GetAllMapSections(level, null, true))
            {
                foreach (IAsset sectionChild in section.AssetChildren)
                {
                    if (sectionChild is MapDefinition)
                    {
                        MapDefinition defintion = (MapDefinition)sectionChild;

                        if (defintion.IsMilo)
                        {
                            foreach (IAsset definitionChild in defintion.AssetChildren)
                            {
                                if (definitionChild is MiloRoom)
                                {
                                    MiloRoom roomAsset = (MiloRoom)definitionChild;

                                    foreach (IAsset roomChild in roomAsset.AssetChildren)
                                    {
                                        if (roomChild is MapInstance)
                                        {
                                            ProcessMapInstance((MapInstance)roomChild, section, ref definitionsMap, ref sectionsMap);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            // Now write the retrieved data to a csv file
            WriteCsvFile(filepath, definitionsMap, sectionsMap);
        }

        /// <summary>
        /// Processes a map instance placing all found shaders into the passed in referenced dictionaries
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="section"></param>
        /// <param name="definitionsMap"></param>
        /// <param name="sectionsMap"></param>
        private static void ProcessMapInstance(MapInstance instance, MapSection section, ref Dictionary<string, HashSet<MapDefinition>> definitionsMap, ref Dictionary<string, HashSet<MapSection>> sectionsMap)
        {
            // Retrieve the referenced definition
            MapDefinition definition = instance.ReferencedDefinition;
            if (definition != null)
            {
                foreach (IShader shader in definition.Shaders)
                {
                    // Add the shader to the main count dictionary
                    if (!definitionsMap.ContainsKey(shader.Name))
                    {
                        definitionsMap[shader.Name] = new HashSet<MapDefinition>();
                    }
                    definitionsMap[shader.Name].Add(definition);

                    // Add the shader to the section dictionary
                    if (!sectionsMap.ContainsKey(shader.Name))
                    {
                        sectionsMap[shader.Name] = new HashSet<MapSection>();
                    }
                    sectionsMap[shader.Name].Add(section);
                }
            }
        }

        /// <summary>
        /// Writes out a CSV file based on the based in shader information
        /// </summary>
        /// <param name="filepath"></param>
        /// <param name="definitionsMap"></param>
        /// <param name="sectionsMap"></param>
        private static void WriteCsvFile(string filepath, Dictionary<string, HashSet<MapDefinition>> definitionsMap, Dictionary<string, HashSet<MapSection>> sectionsMap)
        {
            using (StreamWriter sw = new StreamWriter(filepath))
            {
                WriteCsvObjectHeader(sw, false);

                List<string> sortedKeys = definitionsMap.Keys.ToList();
                sortedKeys.Sort();

                foreach (string shaderName in sortedKeys)
                {
                    //string shaderName = pair.Key;
                    int definitionCount = definitionsMap[shaderName].Count;
                    int instanceCount = 0;

                    foreach (MapDefinition def in definitionsMap[shaderName])
                    {
                        instanceCount += def.Instances.Count;
                    }

                    // Get the set of sections/users for this shader
                    HashSet<string> sectionsSet = new HashSet<string>();
                    HashSet<string> usersSet = new HashSet<string>();

                    if (sectionsMap.ContainsKey(shaderName))
                    {
                        foreach (MapSection section in sectionsMap[shaderName])
                        {
                            sectionsSet.Add(section.Name.ToLower());
                            usersSet.Add(section.ContainerAttributes.ExportUser.ToLower());
                        }
                    }

                    // Convert the sections/users to lists and sort them
                    List<string> sectionsList = sectionsSet.ToList();
                    List<string> usersList = usersSet.ToList();

                    sectionsList.Sort();
                    usersList.Sort();

                    string scvRecord = string.Format("{0}, {1}, {2}, {3}, {4}, {5}, {6}",
                        shaderName, definitionCount, instanceCount,
                        sectionsList.Count.ToString(),
                        (sectionsList.Count == 0 ? "n/a" : String.Join(";", sectionsList.ToArray())),
                        usersList.Count.ToString(),
                        (usersList.Count == 0 ? "n/a" : String.Join(";", usersList.ToArray()))
                        );
                    sw.WriteLine(scvRecord);
                }
            }
        }

        /// <summary>
        /// Writes out the header for the csv file
        /// </summary>
        /// <param name="sw"></param>
        /// <param name="milo"></param>
        private static void WriteCsvObjectHeader(StreamWriter sw, Boolean milo)
        {
            string header = string.Format("Shader Name, Definition Usage Count, Instance Usage Count, Section Count, Sections, User Count, Users");
            sw.WriteLine(header);
        }
        #endregion // Private Methods
    } // ObjectCSVReport
} // RSG.Model.Report.Reports
