﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common.Map
{
    /// <summary>
    /// List of lod levels that the game supports
    /// </summary>
    public enum LodLevel
    {
        Unknown,
        Hd,
        OrphanHd,
        Lod,
        SLod1,
        SLod2,
        SLod3,
        SLod4
    } // LodLevel
}
