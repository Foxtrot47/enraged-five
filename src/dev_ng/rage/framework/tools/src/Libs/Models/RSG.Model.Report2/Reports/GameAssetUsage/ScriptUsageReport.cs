﻿namespace RSG.Model.Report2.Reports.GameAssetUsage
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Web.UI;
    using RSG.Base.Tasks;
    using RSG.ManagedRage;
    using RSG.Metadata.Model;
    using RSG.Model.Character.Game;
    using RSG.Model.Common;
    using RSG.Model.Report;
    using RSG.Model.Vehicle.Game;
    using RSG.Model.Weapon.Game;
    using RSG.Platform;

    /// <summary>
    /// 
    /// </summary>
    public class ScriptUsageReport : HTMLReport, IDynamicLevelReport
    {
        #region Fields
        /// <summary>
        /// The name of this report.
        /// </summary>
        private const string ReportName = "Script Asset Usage";

        /// <summary>
        /// The description for this report.
        /// </summary>
        private const string ReportDescription = "Provides lists detailing the character asset usage in the game.";

        /// <summary>
        /// The private field used for the <see cref="GenerationTask"/> property.
        /// </summary>
        private ITask _generationTask;

        /// <summary>
        /// The private dictionary containing the regular expressions used to track down the
        /// requested assets indexed by the asset type name.
        /// </summary>
        private Dictionary<string, Tuple<List<Regex>, Regex>> m_scriptMethods;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ScriptUsageReport"/> class.
        /// </summary>
        public ScriptUsageReport()
        {
            this.Name = ReportName;
            this.Description = ReportDescription;

            this.m_scriptMethods = new Dictionary<string, Tuple<List<Regex>, Regex>>();
            Regex requestAnimDict =
                new Regex("REQUEST_ANIM_DICT\\(\"(?<arguments>[^\"]{1,})\"\\)", RegexOptions.Compiled);
            Tuple<List<Regex>, Regex> animationDictionaries = new Tuple<List<Regex>, Regex>(new List<Regex>() { requestAnimDict }, null);
            this.m_scriptMethods.Add("Animation Dictionaries", animationDictionaries);

            Regex requestAnimSet =
                new Regex("REQUEST_ANIM_SET\\(\"(?<arguments>[^\"]{1,})\"\\)", RegexOptions.Compiled);
            Tuple<List<Regex>, Regex> animationSets = new Tuple<List<Regex>, Regex>(new List<Regex>() { requestAnimSet }, null);
            this.m_scriptMethods.Add("Animation Sets", animationSets);

            Regex requestTextureDict =
                new Regex(
                    "REQUEST_STREAMED_TEXTURE_DICT\\(\"(?<arguments>[^\"]{1,})\"\\)",
                    RegexOptions.Compiled);
            Tuple<List<Regex>, Regex> textureDictionaries = new Tuple<List<Regex>, Regex>(new List<Regex>() { requestTextureDict }, null);
            this.m_scriptMethods.Add("Texture Dictionaries", textureDictionaries);


            Regex createPed =
                new Regex(
                    @"(CREATE_PED|CREATE_GANG_PED|CREATE_PED_INSIDE_VEHICLE|CREATE_GANG_PED_INSIDE_VEHICLE)\((?<arguments>.*)\)",
                    RegexOptions.Compiled);

            Regex characterParameter =
                new Regex(
                    @"(._._._[\w_]{1,}){1,}",
                    RegexOptions.Compiled);

            Tuple<List<Regex>, Regex> characters = new Tuple<List<Regex>, Regex>(new List<Regex>() { createPed }, characterParameter);
            this.m_scriptMethods.Add("Characters", characters);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the task to run for generating this report.
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (this._generationTask == null)
                {
                    this._generationTask = new ActionTask(
                        "Generating report", this.GenerateReport);
                }

                return this._generationTask;
            }
        }

        public IEnumerable<StreamableStat> RequiredStats
        {
            get { return Enumerable.Empty<StreamableStat>(); }
        }
        #endregion Properties
        
        #region Methods
        /// <summary>
        /// Generates the report dynamically.
        /// </summary>
        /// <param name="context">
        /// The context for this asynchronous task which includes the cancellation token.
        /// </param>
        /// <param name="progress">
        /// The progress object to use to report the progress of the generation.
        /// </param>
        private void GenerateReport(ITaskContext context, IProgress<TaskProgress> progress)
        {
            DynamicLevelReportContext reportContext = context as DynamicLevelReportContext;
            Debug.Assert(reportContext != null, "Unable to generate report");
            if (reportContext == null)
            {
                return;
            }

            string root = reportContext.CoreBranch.Script;
            bool directoryExists = Directory.Exists(root);
            Debug.Assert(directoryExists, "Unable to generate, script directory missing");
            if (!directoryExists)
            {
                return;
            }

            Dictionary<string, string> weaponsLookupTable = this.CreateWeaponLookupTable(root);
            Regex createWeapon =
                new Regex(
                    @"(REQUEST_WEAPON_ASSET|GIVE_WEAPON_TO_PED|GIVE_DELAYED_WEAPON_TO_PED|SET_CURRENT_PED_WEAPON)\((?<arguments>.*)\)",
                    RegexOptions.Compiled);

            Regex allParameters = new Regex("(?<argument>([\\w]){1,}){1,}", RegexOptions.Compiled);
            Regex createVehicle =
                new Regex(
                    @"(CREATE_VEHICLE)\((?<arguments>.*)\)",
                    RegexOptions.Compiled);

            StructureDictionary structures = new StructureDictionary();
            structures.Load(reportContext.Branch, Path.Combine(reportContext.Branch.Metadata, "definitions"));
            GameWeapons gameWeapons = new GameWeapons(reportContext.Branch, structures);
            GameVehicles gameVehicles = new GameVehicles(reportContext.Branch, reportContext.Level, structures);

            string[] scripts = Directory.GetFiles(root, "*.sc", SearchOption.AllDirectories);
            string[] headers = Directory.GetFiles(root, "*.sch", SearchOption.AllDirectories);
            string[] scriptFiles = scripts.Concat(headers).ToArray();

            Dictionary<string, ISet<string>> assetMap = new Dictionary<string, ISet<string>>();
            foreach (string path in scriptFiles)
            {
                string data = File.ReadAllText(path);

                foreach (KeyValuePair<string, Tuple<List<Regex>, Regex>> kvp in this.m_scriptMethods)
                {
                    ISet<string> assets;
                    if (!assetMap.TryGetValue(kvp.Key, out assets))
                    {
                        assets = new SortedSet<string>();
                        assetMap.Add(kvp.Key, assets);
                    }

                    foreach (Regex regex in kvp.Value.Item1)
                    {
                        foreach (Match match in regex.Matches(data))
                        {
                            Group argumentsGroup = match.Groups["arguments"];
                            if (argumentsGroup == null || String.IsNullOrWhiteSpace(argumentsGroup.Value))
                            {
                                continue;
                            }

                            if (kvp.Value.Item2 != null)
                            {
                                MatchCollection arguments = kvp.Value.Item2.Matches(argumentsGroup.Value);
                                foreach (Match argument in arguments)
                                {
                                    assets.Add(argument.Value.ToLower());
                                }
                            }
                            else
                            {
                                assets.Add(argumentsGroup.Value.ToLower());
                            }
                        }
                    }
                }

                {
                    ISet<string> assets;
                    if (!assetMap.TryGetValue("Weapons", out assets))
                    {
                        assets = new SortedSet<string>();
                        assetMap.Add("Weapons", assets);
                    }

                    foreach (Match match in createWeapon.Matches(data))
                    {
                        Group argumentsGroup = match.Groups["arguments"];
                        if (argumentsGroup == null || String.IsNullOrWhiteSpace(argumentsGroup.Value))
                        {
                            continue;
                        }

                        foreach (Match argument in allParameters.Matches(argumentsGroup.Value))
                        {
                            if (String.IsNullOrWhiteSpace(argument.Value))
                            {
                                continue;
                            }

                            string metadataName = null;
                            if (weaponsLookupTable.TryGetValue(argument.Value, out metadataName))
                            {
                                GameWeapon weapon = gameWeapons[metadataName];
                                if (weapon != null && weapon.Model != null)
                                {
                                    assets.Add(weapon.Model.ToLower());
                                }
                                else
                                {
                                    assets.Add(metadataName.ToLower());
                                }
                            }
                        }
                    }
                }

                {
                    ISet<string> assets;
                    if (!assetMap.TryGetValue("Vehicles", out assets))
                    {
                        assets = new SortedSet<string>();
                        assetMap.Add("Vehicles", assets);
                    }

                    foreach (Match match in createVehicle.Matches(data))
                    {
                        Group argumentsGroup = match.Groups["arguments"];
                        if (argumentsGroup == null || String.IsNullOrWhiteSpace(argumentsGroup.Value))
                        {
                            continue;
                        }

                        foreach (Match argument in allParameters.Matches(argumentsGroup.Value))
                        {
                            if (String.IsNullOrWhiteSpace(argument.Value))
                            {
                                continue;
                            }

                            foreach (GameVehicle vehicle in gameVehicles.GetVehiclesWithGameName(argument.Value))
                            {
                                if (vehicle.ModelName == null)
                                {
                                    assets.Add(vehicle.Name.ToLower());
                                }
                                else
                                {
                                    assets.Add(vehicle.ModelName.ToLower());
                                }
                            }
                        }
                    }
                }
            }

            MemoryStream stream = new MemoryStream();
            StreamWriter streamWriter = new StreamWriter(stream);
            HtmlTextWriter writer = new HtmlTextWriter(streamWriter);

            writer.RenderBeginTag(HtmlTextWriterTag.Html);
            writer.RenderBeginTag(HtmlTextWriterTag.Head);
            writer.RenderEndTag();
            writer.RenderBeginTag(HtmlTextWriterTag.Body);

            writer.RenderBeginTag("h1");
            writer.Write("Summary");
            writer.RenderEndTag();

            foreach (KeyValuePair<string, ISet<string>> kvp in assetMap)
            {
                writer.Write(String.Format("{0:N0} referenced {1}", kvp.Value.Count, kvp.Key));
                writer.Write("<br />");
            }

            foreach (KeyValuePair<string, ISet<string>> kvp in assetMap)
            {
                writer.RenderBeginTag("h1");
                writer.Write("Requested " + kvp.Key);
                writer.RenderEndTag();

                foreach (string asset in kvp.Value)
                {
                    writer.Write(asset);
                    writer.Write("<br />");
                }
            }

            writer.RenderEndTag(); // Body
            writer.Flush();

            stream.Position = 0;
            this.Stream = stream;
        }

        private Dictionary<string, string> CreateWeaponLookupTable(string root)
        {
            Dictionary<string, string> lookupTable = new Dictionary<string, string>();
            string weaponEnumFile = Path.Combine(root, "core", "game", "data", "weapon_enums.sch");
            if (!File.Exists(weaponEnumFile))
            {
                return lookupTable;
            }

            
            Regex dataRegex =
                new Regex(
                    "(?s)(?<=ENUM WEAPON_TYPE)(?<data>.*?)(?=ENDENUM)",
                    //"(?<=ENUM WEAPON_TYPE)(?m)(?<data>.*?)(?=ENDENUM)",
                    RegexOptions.Compiled);

            Regex regex =
                new Regex(
                    "((?<definition>.*) = HASH\\(\"(?<argument>.*)\"\\)){1,}",
                    RegexOptions.Compiled);

            string data = File.ReadAllText(weaponEnumFile);
            foreach (Match dataMatch in dataRegex.Matches(data))
            {
                Group dataGroup = dataMatch.Groups["data"];
                if (String.IsNullOrWhiteSpace(dataGroup.Value))
                {
                    continue;
                }

                foreach (Match match in regex.Matches(data))
                {
                    Group definitionGroup = match.Groups["definition"];
                    Group argumentGroup = match.Groups["argument"];

                    if (String.IsNullOrWhiteSpace(definitionGroup.Value))
                    {
                        continue;
                    }

                    if (String.IsNullOrWhiteSpace(argumentGroup.Value))
                    {
                        continue;
                    }

                    if (lookupTable.ContainsKey(definitionGroup.Value))
                    {
                        continue;
                    }

                    lookupTable.Add(definitionGroup.Value.Trim(), argumentGroup.Value);
                }
            }

            return lookupTable;
        }
        #endregion Methods
    } // RSG.Model.Report2.Reports.GameAssetUsage.ScriptUsageReport {Class}
} // RSG.Model.Report2.Reports.GameAssetUsage {Namespace}
