﻿// --------------------------------------------------------------------------------------------
// <copyright file="FloatType.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions.Members
{
    /// <summary>
    /// Defines the different types of float members the parCodeGen system supports.
    /// </summary>
    public enum FloatType
    {
        /// <summary>
        /// The float type is not specified.
        /// </summary>
        Standard,

        /// <summary>
        /// The float type is not recognised as valid, though it is defined.
        /// </summary>
        Unrecognised,

        /// <summary>
        /// The float member is representing an angle and has a default minimum, maximum and
        /// step value.
        /// </summary>
        Angle,
    } // RSG.Metadata.Model.Definitions.Members.FloatType {Enum}
} // RSG.Metadata.Model.Definitions.Members {Namespace}
