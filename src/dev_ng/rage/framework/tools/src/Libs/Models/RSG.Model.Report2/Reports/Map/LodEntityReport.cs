﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.Base.ConfigParser;
using System.IO;
using RSG.Base.Tasks;
using RSG.Model.Common.Map;

namespace RSG.Model.Report.Reports.Map
{
    /// <summary>
    /// 
    /// </summary>
    public class LodEntityReport : CSVReport, IDynamicLevelReport
    {
        #region Constants
        private const String c_name = "Lod Entity Report";
        private const String c_description = "Exports the selected levels lod entities into a .csv file";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (m_task == null)
                {
                    CompositeTask task = new CompositeTask("Generate Report");
                    task.AddSubTask(new ActionTask("Loading data", (context, progress) => EnsureDataLoaded((DynamicLevelReportContext)context, progress)), 50);
                    task.AddSubTask(new ActionTask("Generating report", (context, progress) => GenerateReport((DynamicLevelReportContext)context, progress)));
                    m_task = task;
                }
                return m_task;
            }
        }
        private ITask m_task;

        /// <summary>
        /// List of stats that are required to generate this report.
        /// </summary>
        public IEnumerable<StreamableStat> RequiredStats
        {
            get { return null; }
        }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public LodEntityReport()
            : base(c_name, c_description)
        {
        }
        #endregion // Constructor(s)

        #region Task Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void EnsureDataLoaded(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            IMapHierarchy hierarchy = context.Level.MapHierarchy;

            if (hierarchy != null)
            {
                float increment = 1.0f / hierarchy.AllSections.Count();

                // Load all the map section data
                foreach (IMapSection section in hierarchy.AllSections)
                {
                    context.Token.ThrowIfCancellationRequested();

                    // Update the progress information
                    string message = String.Format("Loading {0}", section.Name);
                    progress.Report(new TaskProgress(increment, true, message));

                    section.RequestAllStatistics(false);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void GenerateReport(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            IMapHierarchy hierarchy = context.Level.MapHierarchy;

            if (hierarchy != null)
            {
                // Write out the report
                using (StreamWriter writer = new StreamWriter(Filename))
                {
                    WriteCsvHeader(writer);

                    foreach (IEntity entity in GatherLodEntities(hierarchy))
                    {
                        WriteCsvEntity(writer, entity);
                    }
                }
            }
        }
        #endregion // Task Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="hierarchy"></param>
        /// <returns></returns>
        private IEnumerable<IEntity> GatherLodEntities(IMapHierarchy hierarchy)
        {
            ISet<IEntity> encounteredEntities = new HashSet<IEntity>();

            foreach (IMapSection section in hierarchy.AllSections)
            {
                // Loop over all hd instances in the section
                foreach (IEntity entity in section.ChildEntities.Where(item => item.LodLevel == LodLevel.Hd && item.ReferencedArchetype != null))
                {
                    // Is this a prop (i.e. the definition is referenced in an external file)
                    if (entity.Parent != (entity.ReferencedArchetype as IMapArchetype).ParentSection)
                    {
                        IEntity parent = entity.LodParent;
                        while (parent != null)
                        {
                            if (!encounteredEntities.Contains(parent))
                            {
                                yield return parent;
                                encounteredEntities.Add(parent);
                            }
                            parent = parent.LodParent;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sw"></param>
        private void WriteCsvHeader(StreamWriter writer)
        {
            writer.WriteLine("Entity,Section,Parent Area,Grandparent Area,Lod Level,Lod Parent,Lod Children Count,Lod Distance,Poly Count,Shader Count,Texture Count");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="instance"></param>
        private void WriteCsvEntity(StreamWriter writer, IEntity entity)
        {
            IMapSection section = entity.Parent as IMapSection;
            IMapArea parentArea = section.ParentArea;
            IMapArea grandParentArea = (parentArea != null ? parentArea.ParentArea : null);

            string csvRecord = String.Format("{0},{1},{2},{3},", entity.Name, section.Name, parentArea != null ? parentArea.Name : "n/a", grandParentArea != null ? grandParentArea.Name : "n/a");
            csvRecord += String.Format("{0},{1},{2},{3},", entity.LodLevel, entity.LodParent != null ? entity.LodParent.Name : "n/a", entity.LodChildren.Count, entity.LodDistance);

            IMapArchetype archetype = entity.ReferencedArchetype as IMapArchetype;
            if (archetype != null)
            {
                csvRecord += String.Format("{0},{1},{2}", archetype.PolygonCount, archetype.Shaders.Count(), archetype.Textures.Count());
            }
            else
            {
                csvRecord += ",,";
            }
            writer.WriteLine(csvRecord);
        }
        #endregion // Private Methods
    } // LodEntityReport
}
