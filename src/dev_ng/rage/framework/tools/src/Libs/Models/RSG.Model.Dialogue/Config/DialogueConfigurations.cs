﻿using System;
using System.IO;
using System.Windows;
using System.Xml;
using System.Xml.XPath;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RSG.Base.Collections;
using System.ComponentModel;
using System.Reflection;
using RSG.Base.Configuration;

namespace RSG.Model.Dialogue.Config
{
    public enum DialogueLanguage
    {
        American,
    }

    public enum ExportPath
    {
        Unknown,
        MissionDialogue,
        MissionDialogueFiles,
        MissionMocap,
        RandomDialogue,
        RandomDialogueFiles,
        RandomMocap,
        RandomIdentifiers,
        NetDialogue,
        NetDialogueFiles,
        NetMocap,
    }

    /// <summary>
    /// Contains all of the configurations that the model current has on it
    /// </summary>
    public class DialogueConfigurations : INotifyPropertyChanged
    {
        #region Published Events

        /// <summary>
        /// Event raised whenever the character dictionary changes.
        /// </summary>
        public event EventHandler CharactersChanged;

        #endregion // Published Events

        #region Property change events

        /// <summary>
        /// Property changed event fired when the any of the properties change
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Create the OnPropertyChanged method to raise the event
        /// </summary>
        /// <param name="name"></param>
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        #endregion // Property change events

        #region Constants
        private static readonly String ExpectedVersion = "2.0.8.2";
        private static readonly String VersionAttribute = "version";
        private static readonly String GuidAttribute = "guid";
        private static readonly String NameAttribute = "name";
        private static readonly String VoiceAttribute = "voice";


        private static readonly XPathExpression XmlConfigurationPath = XPathExpression.Compile("/Configurations");
        private static readonly XPathExpression XmlCharacterPath = XPathExpression.Compile("Characters/Character");
        private static readonly XPathExpression XmlVoicesPath = XPathExpression.Compile("Voices/Voice");
        private static readonly XPathExpression XmlSpecialsPath = XPathExpression.Compile("Specials/Special");
        private static readonly XPathExpression XmlVoiceRootPath = XPathExpression.Compile("/Configurations/Voices");
        private static readonly XPathExpression XmlSpecialRootPath = XPathExpression.Compile("/Configurations/Specials");
        private static readonly XPathExpression XmlAmericanExportRootPath = XPathExpression.Compile("/Configurations/Export/American");
        private static readonly XPathExpression XmlAttributes = XPathExpression.Compile("@*");
        #endregion // Constants

        #region Properties

        /// <summary>
        /// This list contains the options for the mission dialogue type.
        /// </summary>
        public System.Collections.ObjectModel.ObservableCollection<String> MissionTypes
        {
            get { return m_missionTypes; }
            set { m_missionTypes = value; OnPropertyChanged("MissionTypes"); }
        }
        private System.Collections.ObjectModel.ObservableCollection<String> m_missionTypes;

        /// <summary>
        /// This list contains the options for the individual conversations
        /// determining the type the conversation
        /// </summary>
        public System.Collections.ObjectModel.ObservableCollection<String> ConversationCategories
        {
            get { return m_conversationCategories; }
            set { m_conversationCategories = value; OnPropertyChanged("ConversationCategories"); }
        }
        private System.Collections.ObjectModel.ObservableCollection<String> m_conversationCategories;

        /// <summary>
        /// The characters that the line can be said for.
        /// I'm using a observable dictionary here so that the character strings can be linked to guids
        /// that will mean that the characters can be easily renamed, merged, deleted or added
        /// </summary>
        public ObservableDictionary<Guid, String> Characters
        {
            get { return m_characters; }
            set { m_characters = value; OnPropertyChanged("Characters"); }
        }
        private ObservableDictionary<Guid, String> m_characters;

        /// <summary>
        /// This list contains the options for the individual lines of the 
        /// conversations that determines the volume setting for the line.
        /// </summary>
        public System.Collections.ObjectModel.ObservableCollection<String> VolumeSettings
        {
            get { return m_volumeSettings; }
            set { m_volumeSettings = value; OnPropertyChanged("VolumeSettings"); }
        }
        private System.Collections.ObjectModel.ObservableCollection<String> m_volumeSettings;

        /// <summary>
        /// This list contains the options that determines the audio type for
        /// the individual lines
        /// </summary>
        public System.Collections.ObjectModel.ObservableCollection<String> AudioTypes
        {
            get { return m_audioTypes; }
            set { m_audioTypes = value; OnPropertyChanged("AudioTypes"); }
        }
        private System.Collections.ObjectModel.ObservableCollection<String> m_audioTypes;

        /// <summary>
        /// This list contains the options that determines the special variable
        /// for the indiviual lines.
        /// </summary>
        public System.Collections.ObjectModel.ObservableCollection<String> Specials
        {
            get { return m_special; }
            set { m_special = value; OnPropertyChanged("Specials"); }
        }
        private System.Collections.ObjectModel.ObservableCollection<String> m_special;

        /// <summary>
        /// 
        /// </summary>
        public Voices.VoiceRepository InstalledVoices
        {
            get;
            set;
        }

        /// <summary>
        /// This maps the character IDs to an installed voice.
        /// </summary>
        public ObservableDictionary<Guid, String> CharacterVoices
        {
            get { return m_characterVoices; }
            set { m_characterVoices = value; OnPropertyChanged("CharacterVoices"); }
        }
        private ObservableDictionary<Guid, String> m_characterVoices;

        /// <summary>
        /// The voice root node to serialise back out.
        /// </summary>
        private String VoiceRootInnerMarkup
        {
            get;
            set;
        }

        /// <summary>
        /// The special root node to serialise back out.
        /// </summary>
        private String SpecialRootInnerMarkup
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the root export path for all dialogue files.
        /// </summary>
        public string RootExportPath
        {
            get;
            private set;
        }

        public Guid SfxIdentifier
        {
            get;
            private set;
        }

        IConfig _gv;
        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// Loads the configuration file given and sets all the other options to there default values
        /// </summary>
        /// <param name="configPath">The path to the config file to load</param>
        public DialogueConfigurations(String configPath, IConfig gv)
        {
            this._gv = gv;
            this.InstalledVoices = new Voices.VoiceRepository();
            this.m_characters = new ObservableDictionary<Guid, String>();
            this.m_characterVoices = new ObservableDictionary<Guid, String>();
            this.m_missionTypes = new System.Collections.ObjectModel.ObservableCollection<String>();
            this.m_conversationCategories = new System.Collections.ObjectModel.ObservableCollection<String>();
            this.m_volumeSettings = new System.Collections.ObjectModel.ObservableCollection<String>();
            this.m_audioTypes = new System.Collections.ObjectModel.ObservableCollection<String>();
            this.m_special = new System.Collections.ObjectModel.ObservableCollection<String>();

            this.Deserialise(configPath);

            this.m_missionTypes.Add("Mission");
            this.m_missionTypes.Add("Random Event");

            this.m_conversationCategories.Add("Default");
            this.m_conversationCategories.Add("Cellphone");
            this.m_conversationCategories.Add("MoCap");

            this.m_volumeSettings.Add("Normal");
            this.m_volumeSettings.Add("Shout");

            this.m_audioTypes.Add("Normal");
            this.m_audioTypes.Add("Shouted");
            this.m_audioTypes.Add("FrontEnd");

            this.RootExportPath = gv.Project.DefaultBranch.Text;
            if (!Directory.Exists(RootExportPath))
                Directory.CreateDirectory(RootExportPath);
        }

        #endregion // Constructor(s)

        #region Public Function(s)
        public bool DoesCharacterExist(string characterName)
        {
            foreach (KeyValuePair<Guid, string> character in this.Characters)
            {
                if (character.Value == characterName)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="language"></param>
        /// <returns></returns>
        public Dictionary<ExportPath, string> GetExportPaths(DialogueLanguage language)
        {
            string root = Path.Combine(this.RootExportPath, language.ToString());
            Dictionary<ExportPath, string> paths = new Dictionary<ExportPath, string>();

            string languageName = language.ToString().ToLower();
            paths.Add(ExportPath.MissionDialogue, Path.Combine(root, string.Format("{0}Dialogue.txt", languageName)));
            paths.Add(ExportPath.MissionDialogueFiles, Path.Combine(root, string.Format("{0}DialogueFiles.txt", languageName)));
            paths.Add(ExportPath.MissionMocap, Path.Combine(root, string.Format("{0}Mocap.txt", languageName)));
            paths.Add(ExportPath.RandomDialogue, Path.Combine(root, string.Format("{0}RandomEvent.txt", languageName)));
            paths.Add(ExportPath.RandomDialogueFiles, Path.Combine(root, string.Format("{0}RandomEventFiles.txt", languageName)));
            paths.Add(ExportPath.RandomMocap, Path.Combine(root, string.Format("{0}RandomMocap.txt", languageName)));
            paths.Add(ExportPath.RandomIdentifiers, Path.Combine(root, string.Format("{0}RandomEventIdentifiers.txt", languageName)));
            paths.Add(ExportPath.NetDialogue, Path.Combine(root, string.Format("{0}NetDialogue.txt", languageName)));
            paths.Add(ExportPath.NetDialogueFiles, Path.Combine(root, string.Format("{0}NetDialogueFiles.txt", languageName)));
            paths.Add(ExportPath.NetMocap, Path.Combine(root, string.Format("{0}NetMocap.txt", languageName)));

            if (!Directory.Exists(root))
            {
                Directory.CreateDirectory(root);
            }

            return paths;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="language"></param>
        /// <returns></returns>
        public Dictionary<ExportPath, string> GetExportPaths(string rootDirectory, DialogueLanguage language)
        {
            string root = Path.Combine(rootDirectory, language.ToString());
            Dictionary<ExportPath, string> paths = new Dictionary<ExportPath, string>();

            string languageName = language.ToString().ToLower();
            paths.Add(ExportPath.MissionDialogue, Path.Combine(root, string.Format("{0}Dialogue.txt", languageName)));
            paths.Add(ExportPath.MissionDialogueFiles, Path.Combine(root, string.Format("{0}DialogueFiles.txt", languageName)));
            paths.Add(ExportPath.MissionMocap, Path.Combine(root, string.Format("{0}Mocap.txt", languageName)));
            paths.Add(ExportPath.RandomDialogue, Path.Combine(root, string.Format("{0}RandomEvent.txt", languageName)));
            paths.Add(ExportPath.RandomDialogueFiles, Path.Combine(root, string.Format("{0}RandomEventFiles.txt", languageName)));
            paths.Add(ExportPath.RandomMocap, Path.Combine(root, string.Format("{0}RandomMocap.txt", languageName)));
            paths.Add(ExportPath.RandomIdentifiers, Path.Combine(root, string.Format("{0}RandomEventIdentifiers.txt", languageName)));
            paths.Add(ExportPath.NetDialogue, Path.Combine(root, string.Format("{0}NetDialogue.txt", languageName)));
            paths.Add(ExportPath.NetDialogueFiles, Path.Combine(root, string.Format("{0}NetDialogueFiles.txt", languageName)));
            paths.Add(ExportPath.NetMocap, Path.Combine(root, string.Format("{0}NetMocap.txt", languageName)));

            if (!Directory.Exists(root))
            {
                Directory.CreateDirectory(root);
            }

            return paths;
        }

        public void SetExportRootPath(string path)
        {
            this.RootExportPath = path;
        }

        public string GetExportRootPath(DialogueLanguage language)
        {
            string root = Path.Combine(this.RootExportPath, language.ToString());
            return root;
        }

        public string GetExportRootPath()
        {
            return this.RootExportPath;
        }

        /// <summary>
        /// Saves the current configurations into the given file.
        /// </summary>
        /// <param name="configPath">The file to save the configurations into</param>
        public void SaveConfigurations(String configPath)
        {
            this.Serialise(configPath);
        }

        /// <summary>
        /// Returns the guid assiocated with the given character name
        /// </summary>
        /// <param name="characterName">The character name to find the guid for</param>
        /// <returns>The guid for the given character name</returns>
        public Guid GetGuidFromCharacter(String characterName)
        {
            foreach (KeyValuePair<Guid, String> character in this.Characters)
            {
                if (character.Value == characterName)
                {
                    return character.Key;
                }
            }

            return Guid.Empty;
        }

        /// <summary>
        /// Returns the character name assiocated with the given guid
        /// </summary>
        /// <param name="characterName">The guid to find the character name for</param>
        /// <returns>The character name for the given guid</returns>
        public String GetCharacterFromGuid(Guid characterGuid)
        {
            if (this.Characters.ContainsKey(characterGuid))
            {
                return this.Characters[characterGuid];
            }

            return String.Empty;
        }

        /// <summary>
        /// Adds a new character to the character dictionary, first checks to see if
        /// the character already belongs to the character dictionary.
        /// </summary>
        /// <param name="characterName"></param>
        public void AddCharacter(String characterName)
        {
            // Check to see if the character already belongs to the dictionary
            if (this.m_characters.ContainsValue(characterName)) return;

            Guid characterGuid = Guid.NewGuid();
            this.m_characters.Add(characterGuid, characterName);
        }

        /// <summary>
        /// Renames the given character guid to the given name
        /// </summary>
        /// <param name="characterGuid">The guid for the character that will get renamed</param>
        /// <param name="newName">The new name of the character</param>
        public void RenameCharacter(Guid characterGuid, String newName)
        {
            // Check to see if the character already belongs to the dictionary
            if (this.m_characters.ContainsValue(newName)) return;

            int renamedIndex = 0;
            int index = 0;
            ObservableDictionary<Guid, String> temp = new ObservableDictionary<Guid, String>();
            foreach (KeyValuePair<Guid, String> character in this.m_characters)
            {
                if (character.Key == characterGuid) renamedIndex = index;
                temp.Add(character.Key, character.Value);
                index++;
            }
            temp[characterGuid] = newName;
            index = 0;
            this.m_characters.Clear();
            foreach (KeyValuePair<Guid, String> character in temp)
            {
                if (index == renamedIndex)
                {
                    this.m_characters.Add(characterGuid, newName);
                    this.m_characters.Add(character.Key, character.Value);
                }
                else
                {
                    if (character.Key != characterGuid)
                    {
                        this.m_characters.Add(character.Key, character.Value);
                    }
                }
                index++;
            }

            temp = null;
        }

        /// <summary>
        /// Removes the character that has the given guid from the 
        /// character dictionary
        /// </summary>
        /// <param name="characterGuid">The character guid to remove</param>
        public void RemoveCharacter(Guid characterGuid)
        {
            if (this.m_characters.ContainsKey(characterGuid))
            {
                this.m_characters.Remove(characterGuid);
            }
        }

        /// <summary>
        /// Takes a dictionary and fills the character dictionary will the information
        /// </summary>
        public void ResetCharacters(ObservableDictionary<Guid, String> characters)
        {
            this.Characters.Clear();
            foreach (KeyValuePair<Guid, String> character in characters)
            {
                this.Characters.Add(character.Key, character.Value);
            }
        }

        /// <summary>
        /// Returns a clone dictionary of the character dictionary.
        /// </summary>
        /// <returns></returns>
        public ObservableDictionary<Guid, String> CloneCharacters()
        {
            ObservableDictionary<Guid, String> characters = new ObservableDictionary<Guid, String>();
            foreach (KeyValuePair<Guid, String> character in this.Characters)
            {
                characters.Add(character.Key, character.Value);
            }
            return characters;
        }

        /// <summary>
        /// Returns a clone dictionary of the character voices dictionary.
        /// </summary>
        /// <returns></returns>
        public ObservableDictionary<Guid, String> CloneCharacterVoices()
        {
            ObservableDictionary<Guid, String> characterVoices = new ObservableDictionary<Guid, String>();
            foreach (KeyValuePair<Guid, String> voice in this.CharacterVoices)
            {
                characterVoices.Add(voice.Key, voice.Value);
            }
            return characterVoices;
        }

        /// <summary>
        /// Just calls the characters changed event so that anything that is listening has a chance to validate
        /// itself.
        /// </summary>
        public void RegisterCharacterChange()
        {
            if (CharactersChanged != null)
                CharactersChanged(this, EventArgs.Empty);
        }

        /// <summary>
        ///  Returns the audio index from a string for the export process,
        ///  this returns -1 if the string cannot be found.
        /// </summary>
        /// <param name="audioType"></param>
        public int GetAudioTypeIndexFromString(String audioType)
        {
            int index = -1;
            foreach (String audioTypes in this.AudioTypes)
            {
                index ++;
                if (audioTypes == audioType)
                {
                    return index;
                }
            }
            return -1;
        }

        #endregion // Public Function(s)
        
        #region Private Function(s)

        /// <summary>
        /// Creates the xml document and loads the given config file then loops through all the configurations deserialise them.
        /// </summary>
        /// <param name="configPath">The config file to load</param>
        private void Deserialise(String configPath)
        {
            if (!File.Exists(configPath))
            {
                MessageBox.Show("Unable to find the config file located at " + configPath + ". This will result in the character feild being empty.",
                    "Config Loading Error", MessageBoxButton.OK, MessageBoxImage.Error);
                RSG.Base.Logging.Log.Log__Warning("Unable to open the config file located at " + configPath);
                return;
            }

            XmlDocument document = new XmlDocument();
            try
            {
                document.Load(configPath);
            }
            catch (XmlException ex)
            {
                RSG.Base.Logging.Log.Log__Exception(ex, "Unable to open the config file located at " + configPath);
                return;
            }

            XPathNavigator navigator = document.CreateNavigator();
            XPathNodeIterator configurationsIterator = navigator.Select(XmlConfigurationPath);

            while (configurationsIterator.MoveNext())
            {
                XPathNavigator configurationNavigator = configurationsIterator.Current;
                XPathNodeIterator configurationAttrIterator = configurationNavigator.Select(XmlAttributes);

                while (configurationAttrIterator.MoveNext())
                {
                    if (typeof(String) != configurationAttrIterator.Current.ValueType)
                        continue;
                    String value = (configurationAttrIterator.Current.TypedValue as String);
                }

                // Load the characters
                XPathNodeIterator characterIterator = configurationNavigator.Select(XmlCharacterPath);
                while (characterIterator.MoveNext())
                {
                    XPathNavigator characterNavigator = characterIterator.Current;
                    DeserialiseCharacter(characterNavigator);
                }

                // Load the voices
                XPathNodeIterator voiceIterator = configurationNavigator.Select(XmlVoicesPath);
                while (voiceIterator.MoveNext())
                {
                    XPathNavigator voiceNavigator = voiceIterator.Current;
                    DeserialiseVoice(voiceNavigator);
                }

                XPathNodeIterator voiceRootIterator = document.CreateNavigator().Select(XmlVoiceRootPath);
                while (voiceRootIterator.MoveNext())
                {
                    VoiceRootInnerMarkup = voiceRootIterator.Current.InnerXml;
                    VoiceRootInnerMarkup = VoiceRootInnerMarkup.Replace("\r\n", "\r\n    ");
                    VoiceRootInnerMarkup += "\r\n  ";
                }

                // Load the specials
                XPathNodeIterator specialIterator = configurationNavigator.Select(XmlSpecialsPath);
                while (specialIterator.MoveNext())
                {
                    XPathNavigator specialNavigator = specialIterator.Current;
                    DeserialiseSpecial(specialNavigator);
                }

                XPathNodeIterator specialRootIterator = document.CreateNavigator().Select(XmlSpecialRootPath);
                while (specialRootIterator.MoveNext())
                {
                    SpecialRootInnerMarkup = specialRootIterator.Current.InnerXml;
                    SpecialRootInnerMarkup = SpecialRootInnerMarkup.Replace("\r\n", "\r\n    ");
                    SpecialRootInnerMarkup += "\r\n  ";
                }
            }
        }

        /// <summary>
        /// Saves the current configurations into the given file.
        /// </summary>
        /// <param name="configPath">The file to save the configurations into</param>
        private void Serialise(String configPath)
        {
            // Create the xml file and add a header to it.
            XmlDocument xmlDoc = new XmlDocument();
            XmlDeclaration xmlDecl = xmlDoc.CreateXmlDeclaration("1.0", "utf-8", String.Empty);
            xmlDoc.AppendChild(xmlDecl);

            // Add the root node with the current version in it.
            XmlElement rootNode = xmlDoc.CreateElement("Configurations");
            rootNode.SetAttribute(VersionAttribute, ExpectedVersion);

            xmlDoc.InsertBefore(xmlDecl, xmlDoc.DocumentElement);

            // Serialise all of the characters
            XmlElement characterRootNode = xmlDoc.CreateElement("Characters");
            SerialiseCharacters(xmlDoc, characterRootNode);
            rootNode.AppendChild(characterRootNode);

            // Serialise all of the voices
            XmlElement voiceRootNode = xmlDoc.CreateElement("Voices");
            voiceRootNode.InnerXml = this.VoiceRootInnerMarkup;
            rootNode.AppendChild(voiceRootNode);

            // Serialise all of the specials
            XmlElement specialRootNode = xmlDoc.CreateElement("Specials");
            specialRootNode.InnerXml = this.SpecialRootInnerMarkup;
            rootNode.AppendChild(specialRootNode);

            xmlDoc.AppendChild(rootNode);

            // Just before saving just make sure the file sn't read only (if it exists) it should have been checked out, but you never know
            FileInfo fileinfo = new FileInfo(configPath);
            if (fileinfo != null && fileinfo.IsReadOnly && File.Exists(configPath))
            {
                File.SetAttributes(configPath, FileAttributes.Normal);
            }
            xmlDoc.Save(configPath);
        }

        /// <summary>
        /// Deserialises an indiviual character xml path and adds it to the character dictionary
        /// </summary>
        /// <param name="characterNavigator"></param>
        private void DeserialiseCharacter(XPathNavigator characterNavigator)
        {
            XPathNodeIterator characterAttrIterator = characterNavigator.Select(XmlAttributes);

            Guid characterGuid = Guid.NewGuid();
            String characterName = String.Empty;
            String voiceName = String.Empty;

            while (characterAttrIterator.MoveNext())
            {
                if (typeof(String) != characterAttrIterator.Current.ValueType)
                    continue;
                String value = (characterAttrIterator.Current.TypedValue as String);

                if (characterAttrIterator.Current.Name == GuidAttribute)
                {
                    Guid.TryParse(value, out characterGuid);
                }
                else if (characterAttrIterator.Current.Name == NameAttribute)
                {
                    characterName = value;
                }
                else if (characterAttrIterator.Current.Name == VoiceAttribute)
                {
                    voiceName = value;
                }
            }

            this.m_characters.Add(characterGuid, characterName);
            if (characterName == "SFX")
            {
                SfxIdentifier = characterGuid;
            }

            if (String.IsNullOrEmpty(voiceName))
                voiceName = this.InstalledVoices.DefaultVoiceName;

            this.m_characterVoices.Add(characterGuid, voiceName);
        }

        /// <summary>
        /// Deserialises an indiviual voice xml path and adds it to the installed voices
        /// </summary>
        private void DeserialiseVoice(XPathNavigator voiceNavigator)
        {
            XPathNodeIterator voiceAttrIterator = voiceNavigator.Select("text()");
            while (voiceAttrIterator.MoveNext())
            {
                this.InstalledVoices.Voices.Add(voiceAttrIterator.Current.Value);
                break;
            }
        }

        /// <summary>
        /// Deserialises an indiviual voice xml path and adds it to the installed voices
        /// </summary>
        private void DeserialiseSpecial(XPathNavigator voiceNavigator)
        {
            XPathNodeIterator specialAttrIterator = voiceNavigator.Select("text()");
            while (specialAttrIterator.MoveNext())
            {
                this.m_special.Add(specialAttrIterator.Current.Value);
                break;
            }
        }

        /// <summary>
        /// Deserialises an indiviual voice xml path and adds it to the installed voices
        /// </summary>
        private void DeserialiseExportPath(ref Dictionary<ExportPath, string> paths, XPathNavigator navigator)
        {
            ExportPath path = ExportPath.Unknown;
            switch (navigator.Name)
            {
                case "MissionDialogue":
                    {
                        path = ExportPath.MissionDialogue;
                    }
                    break;
                case "MissionDialogueFiles":
                    {
                        path = ExportPath.MissionDialogueFiles;
                    }
                    break;
                case "MissionMocap":
                    {
                        path = ExportPath.MissionMocap;
                    }
                    break;
                case "RandomDialogue":
                    {
                        path = ExportPath.RandomDialogue;
                    }
                    break;
                case "RandomDialogueFiles":
                    {
                        path = ExportPath.RandomDialogueFiles;
                    }
                    break;
                case "RandomMocap":
                    {
                        path = ExportPath.RandomMocap;
                    }
                    break;
                case "RandomIdentifiers":
                    {
                        path = ExportPath.RandomIdentifiers;
                    }
                    break;
                default:
                    break;
            }
            if (path == ExportPath.Unknown || paths.ContainsKey(path))
                return;

            XPathNodeIterator pathAttrIterator = navigator.Select("text()");
            while (pathAttrIterator.MoveNext())
            {
                paths.Add(path, pathAttrIterator.Current.Value);
                break;
            }
        }

        /// <summary>
        /// Serialises all of the characters out to the given root node.
        /// </summary>
        /// <param name="xmlDoc">The document that will be saved out</param>
        /// <param name="parentNode">The parent node to append all of the characters in the xml file</param>
        private void SerialiseCharacters(XmlDocument xmlDoc, XmlElement parentNode)
        {
            foreach (KeyValuePair<Guid, String> character in this.Characters)
            {
                XmlElement characterNode = xmlDoc.CreateElement("Character");
                characterNode.SetAttribute(GuidAttribute, character.Key.ToString().ToUpper());
                characterNode.SetAttribute(NameAttribute, character.Value);
                if (this.m_characterVoices.ContainsKey(character.Key))
                    characterNode.SetAttribute(VoiceAttribute, this.m_characterVoices[character.Key]);
                parentNode.AppendChild(characterNode);
            }
        }

        /// <summary>
        /// Serialises all of the voices out to the given root node.
        /// </summary>
        /// <param name="xmlDoc">The document that will be saved out</param>
        /// <param name="parentNode">The parent node to append all of the voices in the xml file</param>
        private void SerialiseVoice(XmlDocument xmlDoc, XmlElement parentNode)
        {
            foreach (String voice in this.InstalledVoices.Voices)
            {
                XmlElement voiceNode = xmlDoc.CreateElement("Voice");
                XmlText xmlText = xmlDoc.CreateTextNode(voice);
                voiceNode.AppendChild(xmlText);
                parentNode.AppendChild(voiceNode);
            }
        }

        #endregion // Private Function(s)
    } // DialogueConfigurations
} // RSG.Model.Dialogue.Config
