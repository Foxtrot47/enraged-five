﻿namespace RSG.Model.Weapon.Game
{
    using System.Collections.Generic;
    using System.Diagnostics;

    /// <summary>
    /// Represents a single Weapon that is being mounted by the game through the waepons.meta
    /// data file.
    /// </summary>
    [DebuggerDisplay("Name = {Name}, Model = {Model}")]
    public class GameWeapon
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Name"/> property.
        /// </summary>
        private string _name;

        /// <summary>
        /// The private field used for the <see cref="Model"/> property.
        /// </summary>
        private string _model;

        /// <summary>
        /// The private field used for the <see cref="EffectsGroup"/> property.
        /// </summary>
        private string _effectsGroup;

        /// <summary>
        /// The private field used for the <see cref="Effects"/> property.
        /// </summary>
        private readonly List<string> _effects;

        /// <summary>
        /// The private field used for the <see cref="Components"/> property.
        /// </summary>
        private readonly Dictionary<string, GameWeaponComponent> _components;

        /// <summary>
        /// The private field used for the <see cref="Archetype"/> property.
        /// </summary>
        private GameWeaponArchetype _archetype;

        /// <summary>
        /// The private field used for the <see cref="Animations"/> property.
        /// </summary>
        private readonly List<GameWeaponAnimationSet> _animations;

        /// <summary>
        /// The private field used for the <see cref="Ammo"/> property.
        /// </summary>
        private GameWeaponAmmo _ammo;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="GameWeapon"/> class.
        /// </summary>
        public GameWeapon()
        {
            this._effects = new List<string>();
            this._components = new Dictionary<string, GameWeaponComponent>();
            this._animations = new List<GameWeaponAnimationSet>();
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the weapon archetype used for this weapon.
        /// </summary>
        public GameWeaponArchetype Archetype
        {
            get
            {
                return this._archetype;
            }

            set
            {
                if (this._archetype != null)
                {
                    this._archetype.ReferenceCount--;
                }

                this._archetype = value;
                if (this._archetype != null)
                {
                    this._archetype.ReferenceCount++;
                }
            }
        }

        /// <summary>
        /// Gets or sets a iterator around the list animation sets that are attached to this
        /// weapon.
        /// </summary>
        public IEnumerable<GameWeaponAnimationSet> Animations
        {
            get
            {
                foreach (GameWeaponAnimationSet animationSet in this._animations)
                {
                    yield return animationSet;
                }
            }
        }

        /// <summary>
        /// Gets or sets the weapon ammo used for this weapon.
        /// </summary>
        public GameWeaponAmmo Ammo
        {
            get
            {
                return this._ammo;
            }

            set
            {
                if (this._ammo != null)
                {
                    this._ammo.ReferenceCount--;
                    if (this._ammo.Archetype != null)
                    {
                        this._ammo.Archetype.ReferenceCount--;
                    }
                }

                this._ammo = value;
                if (this._ammo != null)
                {
                    this._ammo.ReferenceCount++;
                    if (this._ammo.Archetype != null)
                    {
                        this._ammo.Archetype.ReferenceCount++;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the name used for this weapon.
        /// </summary>
        public string Name
        {
            get { return this._name; }
            set { this._name = value; }
        }

        /// <summary>
        /// Gets or sets the name of the model used for this weapon.
        /// </summary>
        public string Model
        {
            get { return this._model; }
            set { this._model = value; }
        }

        /// <summary>
        /// Gets or sets the name of the texture dictionary containing the textures used on
        /// this weapon.
        /// </summary>
        public string TextureDictionaryName
        {
            get
            {
                if (this._archetype == null)
                {
                    return null;
                }

                return this._archetype.TextureDictionaryName;
            }
        }

        /// <summary>
        /// Gets or sets the name of fx asset dictionary that this weapon uses.
        /// </summary>
        public string EffectsDictionaryName
        {
            get
            {
                if (this._archetype == null)
                {
                    return null;
                }

                return this._archetype.EffectsDictionaryName;
            }
        }

        /// <summary>
        /// Gets or sets the effects group containing the effects used for this weapon.
        /// </summary>
        public string EffectsGroup
        {
            get { return this._effectsGroup; }
            set { this._effectsGroup = value; }
        }

        /// <summary>
        /// Gets or sets a iterator around the individual effects used for this weapon.
        /// </summary>
        public IEnumerable<string> Effects
        {
            get
            {
                foreach (string effect in this._effects)
                {
                    yield return effect;
                }
            }
        }

        /// <summary>
        /// Gets or sets a iterator around the individual components that this weapon can use.
        /// </summary>
        public IEnumerable<KeyValuePair<string, GameWeaponComponent>> Components
        {
            get
            {
                foreach (var component in this._components)
                {
                    yield return component;
                }
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Adds the specified effect name into this weapons effects list.
        /// </summary>
        /// <param name="effectName">
        /// The name of the effect to add.
        /// </param>
        public void AddEffect(string effectName)
        {
            this._effects.Add(effectName);
        }

        /// <summary>
        /// Adds the specified component into this weapons components list.
        /// </summary>
        /// <param name="name">
        /// The name of the component.
        /// </param>
        /// <param name="component">
        /// The component to add to this weapon.
        /// </param>
        public void AddComponent(string name, GameWeaponComponent component)
        {
            this._components.Add(name, component);
            if (component != null)
            {
                component.ReferenceCount++;
                if (component.Archetype != null)
                {
                    component.Archetype.ReferenceCount++;
                }
            }
        }

        /// <summary>
        /// Adds the specified animation set into this weapons animations.
        /// </summary>
        /// <param name="component">
        /// The animation set to add to this weapon.
        /// </param>
        public void AddAnimationSet(GameWeaponAnimationSet animationSet)
        {
            this._animations.Add(animationSet);
            animationSet.ReferenceCount++;
        }
        #endregion Methods
    } // RSG.Model.Weapon.Game.Weapon {Class}
} // RSG.Model.Weapon.Game {Namespace}
