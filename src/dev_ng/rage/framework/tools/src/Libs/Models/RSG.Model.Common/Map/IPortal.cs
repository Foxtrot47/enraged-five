﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common.Map
{
    /// <summary>
    /// 
    /// </summary>
    public interface IPortal : IAsset
    {
        /// <summary>
        /// The interior archetype that "owns" this room
        /// </summary>
        IInteriorArchetype ParentArchetype { get; }

        /// <summary>
        /// The source room for this portal
        /// </summary>
        IRoom RoomA { get; }

        /// <summary>
        /// The target room for this portal
        /// </summary>
        IRoom RoomB { get; }
    } // IPortal
}
