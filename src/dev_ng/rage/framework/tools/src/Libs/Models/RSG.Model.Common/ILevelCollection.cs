﻿using System;
using System.Collections.Generic;

namespace RSG.Model.Common
{

    /// <summary>
    /// ILevelCollection interface; defining publically accessible features of
    /// a collection of game level objects.
    /// </summary>
    public interface ILevelCollection : 
        IEnumerable<ILevel>,
        ICollection<ILevel>,
        IDisposable
    {
        #region Methods
        /// <summary>
        /// 
        /// </summary>
        void LoadAllStats();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="statsToLoad"></param>
        void LoadStats(IEnumerable<StreamableStat> statsToLoad);
        #endregion // Methods
    }

} // RSG.Model.Common namespace
