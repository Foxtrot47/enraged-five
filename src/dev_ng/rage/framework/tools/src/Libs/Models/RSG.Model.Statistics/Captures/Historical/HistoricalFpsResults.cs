﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using RSG.Base.Extensions;

namespace RSG.Model.Statistics.Captures.Historical
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class HistoricalFpsResults
    {
        #region Properties
        /// <summary>
        /// Mapping of changelist numbers to average fps values.
        /// </summary>
        [DataMember]
        public IDictionary<Object, float> AverageFps { get; private set; }

        /// <summary>
        /// Standard deviation of the results.
        /// </summary>
        [DataMember]
        public double StandardDeviation { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public HistoricalFpsResults()
        {
            AverageFps = new SortedDictionary<Object, float>();
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="result"></param>
        public void AddResult(Object context, FpsResult result)
        {
            AverageFps.Add(context, result.Average);
            StandardDeviation = AverageFps.Select(item => item.Value).CalculateStdDev();
        }
        #endregion // Public Methods
    } // HistoricalFpsResults
}
