﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Math;
using RSG.Base.ConfigParser;
using RSG.SceneXml;
using RSG.SceneXml.Material;
using RSG.SceneXml.Statistics;
using RSG.Model.Map.Statistics;

namespace RSG.Model.Map
{
    using AttributeContainer = Dictionary<String, Object>;
    using TextureContainer = Dictionary<String, Texture>;

    /// <summary>
    /// A class that represents a milo in a map section, this is
    /// a wrapper around the gta object gta_milo
    /// </summary>
    public class MapMilo : IMapDefinition
    {
        #region Properties

        /// <summary>
        /// The name of the definition, used to
        /// instance it elsewhere in the map data.
        /// </summary>
        public String Name
        {
            get;
            private set;
        }

        /// <summary>
        /// A dictionary of attributes used on the definition.
        /// This is a cc of the attributes on a ObjectDef. This
        /// dictionary is indexed by attributes names found in
        /// RSG.SceneXml.AttrNames
        /// </summary>
        public AttributeContainer Attributes
        {
            get;
            private set;
        }

        /// <summary>
        /// The local bounding box for the definition,
        /// used to create areas that is used for statistics
        /// etc. This is a cc of the bounding box on a ObjectDef
        /// </summary>
        public BoundingBox3f LocalBoundingBox
        {
            get;
            private set;
        }

        /// <summary>
        /// A dictionary of statistics on this definition,
        /// the statistics include collision statistics
        /// as well. This dictionary is indexed by statistic 
        /// names found in RSG.Model.Map.Statistics.StatNames
        /// </summary>
        public DefinitionStatistics Statistics
        {
            get;
            private set;
        }

        /// <summary>
        /// A list of all the definitions that exist in
        /// this map section
        /// </summary>
        public List<IMapDefinition> Definitions
        {
            get;
            private set;
        }

        /// <summary>
        /// A list of all the rooms that exist in
        /// this map milo
        /// </summary>
        public List<MapRoom> Rooms
        {
            get;
            private set;
        }

        /// <summary>
        /// A list of all the instances that exist in
        /// this map section
        /// </summary>
        public List<IMapInstance> Instances
        {
            get;
            private set;
        }

        /// <summary>
        /// The collasped list of textures that
        /// are attached to this definition.
        /// </summary>
        public TextureContainer Textures
        {
            get;
            private set;
        }

        #endregion // Properties

        #region Constructors

        /// <summary>
        /// Creates a new instance of the map definition
        /// object using the given object def as a base
        /// and the scene to get the other statistics
        /// needed
        /// </summary>
        public MapMilo(TargetObjectDef rootMilo, Scene scene, DefinitionList allDefinitions)
        {
            this.GetNameFromObject(rootMilo);
            this.GetAttributesFromObject(rootMilo);
            this.LocalBoundingBox = CreateChildren(rootMilo, scene, allDefinitions);
            float inverseArea = GetInverseAreaFromBoundingBox();
            this.GetStatisticsFromChildren(inverseArea);
            this.GetTexturesFromChildren();
        }

        #endregion // Constructors

        #region Public Methods

        #endregion // Public Methods

        #region Private Methods

        /// <summary>
        /// Gets the name from the given object
        /// </summary>
        private void GetNameFromObject(TargetObjectDef sceneObject)
        {
            this.Name = String.Copy(sceneObject.Name + "_milo_");
        }

        /// <summary>
        /// Gets the attributes from the given object
        /// </summary>
        private void GetAttributesFromObject(TargetObjectDef sceneObject)
        {
            this.Attributes = new AttributeContainer();
            foreach (var attribute in sceneObject.Attributes)
            {
                String attributeName = String.Copy(attribute.Key);
                Object attributeValue = attribute.Value;
                this.Attributes.Add(attributeName, attributeValue);
            }
        }

        /// <summary>
        /// Uses the local bounding box for this room and
        /// returns the inverse area for it. (Default return is 0.0f)
        /// </summary>
        private float GetInverseAreaFromBoundingBox()
        {
            float sectionWidth = this.LocalBoundingBox.Max.X - this.LocalBoundingBox.Min.X;
            float sectionLength = this.LocalBoundingBox.Max.Y - this.LocalBoundingBox.Min.Y;
            float sectionHeight = this.LocalBoundingBox.Max.Z - this.LocalBoundingBox.Min.Z;
            if (sectionWidth > 0.1f && sectionLength > 0.1f && sectionHeight > 0.1f)
            {
                return 1.0f / (sectionWidth * sectionLength * sectionHeight);
            }

            return 0.0f;
        }

        /// <summary>
        /// Goes through the given root and creates the definitions/instances/ and rooms
        /// for this room. This function also returns the bounding box that expands over
        /// all the children
        /// </summary>
        private BoundingBox3f CreateChildren(TargetObjectDef rootMilo, Scene scene, DefinitionList allDefinitions)
        {
            this.Definitions = new List<IMapDefinition>();
            this.Instances = new List<IMapInstance>();
            this.Rooms = new List<MapRoom>();

            BoundingBox3f surroundingBox = new BoundingBox3f();
            Dictionary<TargetObjectDef, MapInstance> completedObjects = new Dictionary<TargetObjectDef, MapInstance>();
            foreach (TargetObjectDef child in rootMilo.Children)
            {
                if (completedObjects.ContainsKey(child))
                    continue;
                if (child.DontExport())
                    continue;
                if (child.IsDummy())
                    continue;

                if (child.IsMloRoom())
                {
                    MapRoom room = new MapRoom(child, scene, allDefinitions);
                    surroundingBox.Expand(room.LocalBoundingBox);
                    this.Definitions.Add(room);
                    this.Rooms.Add(room);
                }
                else if (child.IsXRef() || child.IsRefObject() || child.IsInternalRef())
                {
                    MapInstance instance = new MapInstance(child, scene, allDefinitions);
                    this.Instances.Add(instance);
                    if (instance.WorldBoundingBox != null)
                        surroundingBox.Expand(instance.WorldBoundingBox);
                    completedObjects.Add(child, instance);
                }
                else if (child.IsObject())
                {
                    MapDefinition definition = new MapDefinition(child, scene);
                    this.Definitions.Add(definition);
                    MapInstance instance = new MapInstance(child, scene, definition);
                    this.Instances.Add(instance);
                    if (instance.WorldBoundingBox != null)
                        surroundingBox.Expand(instance.WorldBoundingBox);
                    completedObjects.Add(child, instance);
                }
            }
            foreach (var instance in completedObjects)
            {
                instance.Value.ResolveLodHierarchy(instance.Key, scene, completedObjects.Values.ToList());
            }

            return surroundingBox;
        }

        /// <summary>
        /// Gets all the statistics for this given room and creates
        /// a dictionary for them. It gets the statistics by adding up
        /// all of the child object statistics.
        /// </summary>
        private void GetStatisticsFromChildren(float inverseArea)
        {
            this.Statistics = new DefinitionStatistics();
            int geometrySize = 0;
            int txdSize = 0;
            int polygonCount = 0;
            int collisionCount = 0;
            List<String> uniqueTxds = new List<String>();
            foreach (MapInstance instance in this.Instances)
            {
                if (instance != null && instance.ResolvedDefinition != null)
                {
                    IMapDefinition definition = instance.ResolvedDefinition;

                    geometrySize += definition.Statistics.GeometrySize;
                    polygonCount += definition.Statistics.PolygonCount;
                    collisionCount += definition.Statistics.CollisionPolygonCount;

                    if (definition.Attributes.ContainsKey(AttrNames.OBJ_TXD))
                    {
                        String txdName = definition.Attributes[AttrNames.OBJ_TXD] as String;
                        if (!(uniqueTxds.Contains(txdName)))
                        {
                            txdSize += definition.Statistics.TXDSize;
                            uniqueTxds.Add(txdName);
                        }
                    }
                }
            }
            this.Statistics.AddGeometryStatistics(geometrySize, polygonCount, inverseArea);
            this.Statistics.AddCollisionStatistics(collisionCount, inverseArea);
            this.Statistics.AddTXDStatistics(txdSize);
        }

        /// <summary>
        /// Gets the unique textures from all the children and creates the textures
        /// from them for this definition
        /// </summary>
        private void GetTexturesFromChildren()
        {
            this.Textures = new TextureContainer();
            Dictionary<String, MapInstance> uniqueInstances = new Dictionary<String, MapInstance>();
            foreach (MapInstance instance in this.Instances)
            {
                if (instance != null && instance.ResolvedDefinition != null)
                {
                    if (!uniqueInstances.ContainsKey(instance.ResolvedDefinition.Name))
                    {
                        uniqueInstances.Add(instance.ResolvedDefinition.Name, instance);
                    }
                }
            }

            foreach (MapInstance instance in uniqueInstances.Values)
            {
                if (instance.ResolvedDefinition != null)
                {
                    foreach (KeyValuePair<String, Texture> texture in instance.ResolvedDefinition.Textures)
                    {
                        if (!this.Textures.ContainsKey(texture.Key))
                        {
                            this.Textures.Add(texture.Key, texture.Value);
                        }
                    }
                }
            }
        }

        #endregion // Private Methods
    }

    /// <summary>
    /// A class that represents a milo in a map section, this is
    /// a wrapper around the gta object gtamloroom
    /// </summary>
    public class MapRoom : IMapDefinition
    {
        #region Properties

        /// <summary>
        /// The name of the definition, used to
        /// instance it elsewhere in the map data.
        /// </summary>
        public String Name
        {
            get;
            private set;
        }

        /// <summary>
        /// A dictionary of attributes used on the definition.
        /// This is a cc of the attributes on a ObjectDef. This
        /// dictionary is indexed by attributes names found in
        /// RSG.SceneXml.AttrNames
        /// </summary>
        public AttributeContainer Attributes
        {
            get;
            private set;
        }

        /// <summary>
        /// The local bounding box for the definition,
        /// used to create areas that is used for statistics
        /// etc. This is a cc of the bounding box on a ObjectDef
        /// </summary>
        public BoundingBox3f LocalBoundingBox
        {
            get;
            private set;
        }

        /// <summary>
        /// A dictionary of statistics on this definition,
        /// the statistics include collision statistics
        /// as well. This dictionary is indexed by statistic 
        /// names found in RSG.Model.Map.Statistics.StatNames
        /// </summary>
        public DefinitionStatistics Statistics
        {
            get;
            private set;
        }

        /// <summary>
        /// A list of all the definitions that exist in
        /// this map section
        /// </summary>
        public List<IMapDefinition> Definitions
        {
            get;
            private set;
        }
        
        /// <summary>
        /// A list of all the instances that exist in
        /// this map section
        /// </summary>
        public List<IMapInstance> Instances
        {
            get;
            private set;
        }

        /// <summary>
        /// The collasped list of textures that
        /// are attached to this definition.
        /// </summary>
        public TextureContainer Textures
        {
            get;
            private set;
        }

        #endregion // Properties

        #region Constructors

        /// <summary>
        /// Creates a new instance of the map definition
        /// object using the given object def as a base
        /// and the scene to get the other statistics
        /// needed
        /// </summary>
        public MapRoom(TargetObjectDef rootRoom, Scene scene, DefinitionList allDefinitions)
        {
            this.GetNameFromObject(rootRoom);
            this.GetAttributesFromObject(rootRoom);
            this.LocalBoundingBox = this.CreateChildren(rootRoom, scene, allDefinitions);
            float inverseArea = GetInverseAreaFromBoundingBox();
            this.GetStatisticsFromChildren(inverseArea);
            this.GetTexturesFromChildren();
        }

        #endregion // Constructors

        #region Public Methods

        #endregion // Public Methods

        #region Private Methods

        /// <summary>
        /// Gets the name from the given object
        /// </summary>
        private void GetNameFromObject(TargetObjectDef sceneObject)
        {
            this.Name = String.Copy(sceneObject.Name);
        }

        /// <summary>
        /// Gets the attributes from the given object
        /// </summary>
        private void GetAttributesFromObject(TargetObjectDef sceneObject)
        {
            this.Attributes = new AttributeContainer();
            foreach (var attribute in sceneObject.Attributes)
            {
                String attributeName = String.Copy(attribute.Key);
                Object attributeValue = attribute.Value;
                this.Attributes.Add(attributeName, attributeValue);
            }
        }

        /// <summary>
        /// Uses the local bounding box for this room and
        /// returns the inverse area for it. (Default return is 0.0f)
        /// </summary>
        private float GetInverseAreaFromBoundingBox()
        {
            float sectionWidth = this.LocalBoundingBox.Max.X - this.LocalBoundingBox.Min.X;
            float sectionLength = this.LocalBoundingBox.Max.Y - this.LocalBoundingBox.Min.Y;
            float sectionHeight = this.LocalBoundingBox.Max.Z - this.LocalBoundingBox.Min.Z;
            if (sectionWidth > 0.1f && sectionLength > 0.1f && sectionHeight > 0.1f)
            {
                return 1.0f / (sectionWidth * sectionLength * sectionHeight);
            }

            return 0.0f;
        }

        /// <summary>
        /// Goes through the given root and creates the definitions/instances/ and rooms
        /// for this room. This function also returns the bounding box that expands over
        /// all the children
        /// </summary>
        private BoundingBox3f CreateChildren(TargetObjectDef rootRoom, Scene scene, DefinitionList allDefinitions)
        {
            this.Definitions = new List<IMapDefinition>();
            this.Instances = new List<IMapInstance>();

            BoundingBox3f surroundingBox = new BoundingBox3f();
            Dictionary<TargetObjectDef, MapInstance> completedObjects = new Dictionary<TargetObjectDef, MapInstance>();
            foreach (TargetObjectDef child in rootRoom.Children)
            {
                if (completedObjects.ContainsKey(child))
                    continue;
                if (child.DontExport())
                    continue;
                if (child.IsDummy())
                    continue;

                if (null != allDefinitions && (child.IsXRef() || child.IsRefObject() || child.IsInternalRef()))
                {
                    MapInstance instance = new MapInstance(child, scene, allDefinitions);
                    this.Instances.Add(instance);
                    if (instance.WorldBoundingBox != null)
                        surroundingBox.Expand(instance.WorldBoundingBox);
                    completedObjects.Add(child, instance);
                }
                else if (child.IsObject())
                {
                    MapDefinition definition = new MapDefinition(child, scene);
                    this.Definitions.Add(definition);
                    MapInstance instance = new MapInstance(child, scene, definition);
                    if (instance.WorldBoundingBox != null)
                        surroundingBox.Expand(instance.WorldBoundingBox);
                    this.Instances.Add(instance);

                    completedObjects.Add(child, instance);
                }
            }

            foreach (var instance in completedObjects)
            {
                instance.Value.ResolveLodHierarchy(instance.Key, scene, completedObjects.Values.ToList());
            }

            return surroundingBox;
        }

        /// <summary>
        /// Gets all the statistics for this given room and creates
        /// a dictionary for them. It gets the statistics by adding up
        /// all of the child object statistics.
        /// </summary>
        private void GetStatisticsFromChildren(float inverseArea)
        {
            this.Statistics = new DefinitionStatistics();
            int geometrySize = 0;
            int txdSize = 0;
            int polygonCount = 0;
            int collisionCount = 0;
            Dictionary<String, MapInstance> uniqueInstances = new Dictionary<String, MapInstance>();
            foreach (MapInstance instance in this.Instances)
            {
                if (instance != null && instance.ResolvedDefinition != null)
                {
                    if (!uniqueInstances.ContainsKey(instance.ResolvedDefinition.Name))
                    {
                        uniqueInstances.Add(instance.ResolvedDefinition.Name, instance);
                    }
                }
            }

            List<String> uniqueTxds = new List<String>();
            foreach (MapInstance instance in uniqueInstances.Values)
            {
                if (instance != null && instance.ResolvedDefinition != null)
                {
                    IMapDefinition definition = instance.ResolvedDefinition;

                        geometrySize += definition.Statistics.GeometrySize;
                        polygonCount += definition.Statistics.PolygonCount;
                        collisionCount += definition.Statistics.CollisionPolygonCount;

                    if (definition.Attributes.ContainsKey(AttrNames.OBJ_TXD))
                    {
                        String txdName = definition.Attributes[AttrNames.OBJ_TXD] as String;
                        if (!(uniqueTxds.Contains(txdName)))
                        {
                            txdSize += definition.Statistics.TXDSize;
                            uniqueTxds.Add(txdName);
                        }
                    }
                }
            }
            this.Statistics.AddGeometryStatistics(geometrySize, polygonCount, inverseArea);
            this.Statistics.AddCollisionStatistics(collisionCount, inverseArea);
            this.Statistics.AddTXDStatistics(txdSize);
        }

        /// <summary>
        /// Gets the unique textures from all the children and creates the textures
        /// from them for this definition
        /// </summary>
        private void GetTexturesFromChildren()
        {
            this.Textures = new TextureContainer();
            Dictionary<String, MapInstance> uniqueInstances = new Dictionary<String, MapInstance>();
            foreach (MapInstance instance in this.Instances)
            {
                if (instance != null && instance.ResolvedDefinition != null)
                {
                    if (!uniqueInstances.ContainsKey(instance.ResolvedDefinition.Name))
                    {
                        uniqueInstances.Add(instance.ResolvedDefinition.Name, instance);
                    }
                }
            }

            foreach (MapInstance instance in uniqueInstances.Values)
            {
                if (instance.ResolvedDefinition != null)
                {
                    foreach (KeyValuePair<String, Texture> texture in instance.ResolvedDefinition.Textures)
                    {
                        if (!this.Textures.ContainsKey(texture.Key))
                        {
                            this.Textures.Add(texture.Key, texture.Value);
                        }
                    }
                }
            }
        }

        #endregion // Private Methods
    }
}
