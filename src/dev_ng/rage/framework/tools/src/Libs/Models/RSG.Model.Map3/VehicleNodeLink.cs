﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.Model.Common.Map;

namespace RSG.Model.Map3
{
    /// <summary>
    /// Vehicle node link.
    /// </summary>
    public class VehicleNodeLink : AssetBase
    {
        /// <summary>
        /// First node reference.
        /// </summary>
        internal Guid Node1
        {
            get;
            set;
        }

        /// <summary>
        /// Second node reference.
        /// </summary>
        internal Guid Node2
        {
            get;
            set;
        }

        /// <summary>
        /// First node.
        /// </summary>
        public VehicleNode FirstNode
        {
            get;
            internal set;
        }

        /// <summary>
        /// Second node.
        /// </summary>
        public VehicleNode SecondNode
        {
            get;
            internal set;
        }

        /// <summary>
        /// True if the link is a shortcut.
        /// </summary>
        public bool IsShortCut
        {
            get;
            internal set;
        }

        /// <summary>
        /// Mirrors the node link if the nodes have been swapped.
        /// </summary>
        /// <param name="firstNode"></param>
        /// <returns></returns>
        public VehicleNodeLink MirrorNodeIfSwapped(VehicleNode firstNode)
        {
            if (firstNode == FirstNode)
            {
                return this;
            }

            VehicleNodeLink newLink = new VehicleNodeLink();
            newLink.Node1 = Node2;
            newLink.Node2 = Node1;
            newLink.FirstNode = SecondNode;
            newLink.SecondNode = FirstNode;
            return newLink;
        }
    }
}
