﻿//---------------------------------------------------------------------------------------------
// <copyright file="Dialogue.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.Model
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Xml;
    using RSG.Editor;
    using RSG.Editor.Model;

    /// <summary>
    /// Represents a set of conversations bound together for a single set of dialogue.
    /// </summary>
    public class Dialogue : ModelBase
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="AmbientFile"/> property.
        /// </summary>
        private bool _ambientFile;

        /// <summary>
        /// The private field used for the <see cref="CharacterMappings"/> property.
        /// </summary>
        private CharacterMappings _characterMappings;

        /// <summary>
        /// The private field used for the <see cref="Conversations"/> property.
        /// </summary>
        private ModelCollection<Conversation> _conversations;

        /// <summary>
        /// The private reference to the configurations that this dialogue object has been
        /// associated with.
        /// </summary>
        private DialogueConfigurations _configurations;

        /// <summary>
        /// The private field used for the <see cref="DeletedFilenames"/> property.
        /// </summary>
        private List<string> _deletedFilenames;

        /// <summary>
        /// The private field used for the <see cref="Id"/> property.
        /// </summary>
        private string _id;

        /// <summary>
        /// The private field used for the <see cref="Multiplayer"/> property.
        /// </summary>
        private bool _multiplayer;

        /// <summary>
        /// The private field used for the <see cref="Name"/> property.
        /// </summary>
        private string _name;

        /// <summary>
        /// The private field used for the <see cref="Conversations"/> property.
        /// </summary>
        private ReadOnlyModelCollection<Conversation> _readOnlyConversations;

        /// <summary>
        /// The private field used for the <see cref="SubtitleId"/> property.
        /// </summary>
        private string _subtitleId;

        /// <summary>
        /// The private field used for the <see cref="Type"/> property.
        /// </summary>
        private string _type;

        /// <summary>
        /// The private field used for the <see cref="UndoEngine"/> property.
        /// </summary>
        private UndoEngine _undoEngine;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="Dialogue"/> class.
        /// </summary>
        /// <param name="engine">
        /// The undo engine that will be associated with this dialogue model.
        /// </param>
        /// <param name="configurations">
        /// The dialogue configurations to use for this dialogue and all of the conversations
        /// and lines underneath it.
        /// </param>
        public Dialogue(UndoEngine engine, DialogueConfigurations configurations)
        {
            this._characterMappings = new CharacterMappings(this);
            this._ambientFile = false;
            this._id = String.Empty;
            this._multiplayer = false;
            this._name = String.Empty;
            this._subtitleId = String.Empty;
            this._type = String.Empty;
            this._undoEngine = engine;
            this._configurations = configurations;
            this._conversations = new ModelCollection<Conversation>(this);
            this._deletedFilenames = new List<string>();
            this._readOnlyConversations =
                new ReadOnlyModelCollection<Conversation>(this._conversations);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Dialogue"/> class as a deep copy of
        /// the specified instance.
        /// </summary>
        /// <param name="other">
        /// The instance to copy.
        /// </param>
        public Dialogue(Dialogue other)
        {
            if (other == null)
            {
                throw new SmartArgumentNullException(() => other);
            }

            this._characterMappings = new CharacterMappings(this);
            this._ambientFile = other._ambientFile;
            this._id = other._id;
            this._multiplayer = other._multiplayer;
            this._name = other._name;
            this._subtitleId = other._subtitleId;
            this._type = other._type;
            this._configurations = other._configurations;
            this._undoEngine = other._undoEngine;

            List<Conversation> conversations = new List<Conversation>();
            foreach (Conversation conversation in other.Conversations)
            {
                conversations.Add(conversation.DeepClone() as Conversation);
            }

            this._conversations = new ModelCollection<Conversation>(this, conversations);
            this._readOnlyConversations =
                new ReadOnlyModelCollection<Conversation>(this._conversations);

            this._deletedFilenames = new List<string>();
            foreach (string deletedFilename in other._deletedFilenames)
            {
                this._deletedFilenames.Add(deletedFilename.Clone() as string);
            }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Dialogue"/> class using the specified
        /// xml reader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The xml reader that contains the data used to initialise this instance.
        /// </param>
        /// <param name="configurations">
        /// The dialogue configurations to use for this dialogue and all of the conversations
        /// and lines underneath it.
        /// </param>
        public Dialogue(XmlReader reader, DialogueConfigurations configurations)
        {
            this._characterMappings = new CharacterMappings(this);
            this._ambientFile = false;
            this._id = String.Empty;
            this._multiplayer = false;
            this._name = String.Empty;
            this._subtitleId = String.Empty;
            this._type = String.Empty;
            this._configurations = configurations;
            this._deletedFilenames = new List<string>();
            this._conversations = new ModelCollection<Conversation>();
            this._readOnlyConversations =
                new ReadOnlyModelCollection<Conversation>(this._conversations);

            using (new SuspendUndoEngine(this.UndoEngine, SuspendUndoEngineMode.UndoEvents))
            {
                this.Deserialise(reader);
            }

            this._conversations.SetUndoEngineProvider(this);
            if (this._characterMappings == null)
            {
                Dictionary<Guid, int> tempCharacterSpeakerMap = new Dictionary<Guid, int>();
                Dictionary<Guid, int> validCharacterSpeakerMap = new Dictionary<Guid, int>();

                this._characterMappings = new CharacterMappings(this);
                foreach (Conversation conversation in this._conversations)
                {
                    foreach (ILine line in conversation.Lines)
                    {
                        if (Guid.Equals(line.CharacterId, Guid.Empty))
                        {
                            continue;
                        }

                        if (tempCharacterSpeakerMap.ContainsKey(line.CharacterId))
                        {
                            int tempSpeaker = tempCharacterSpeakerMap[line.CharacterId];
                            if (tempSpeaker != line.Speaker)
                            {
                                validCharacterSpeakerMap.Remove(line.CharacterId);
                            }
                        }
                        else
                        {
                            tempCharacterSpeakerMap.Add(line.CharacterId, line.Speaker);
                            validCharacterSpeakerMap.Add(line.CharacterId, line.Speaker);
                        }
                    }
                }

                foreach (var kvp in validCharacterSpeakerMap)
                {
                    this._characterMappings.AddOrModifyMapping(kvp.Key, kvp.Value, null);
                }
            }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Dialogue"/> class using the specified
        /// xml reader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The xml reader that contains the data used to initialise this instance.
        /// </param>
        /// <param name="engine">
        /// The undo engine that will be associated with this dialogue model.
        /// </param>
        /// <param name="configurations">
        /// The dialogue configurations to use for this dialogue and all of the conversations
        /// and lines underneath it.
        /// </param>
        public Dialogue(
            XmlReader reader, UndoEngine engine, DialogueConfigurations configurations)
        {
            this._ambientFile = false;
            this._id = String.Empty;
            this._multiplayer = false;
            this._name = String.Empty;
            this._subtitleId = String.Empty;
            this._type = String.Empty;
            this._undoEngine = engine;
            this._configurations = configurations;
            this._deletedFilenames = new List<string>();
            this._conversations = new ModelCollection<Conversation>();
            this._readOnlyConversations =
                new ReadOnlyModelCollection<Conversation>(this._conversations);

            using (new SuspendUndoEngine(this.UndoEngine, SuspendUndoEngineMode.Both))
            {
                this.Deserialise(reader);
            }

            this._conversations.SetUndoEngineProvider(this);
            if (this._characterMappings == null)
            {
                Dictionary<Guid, int> tempCharacterSpeakerMap = new Dictionary<Guid, int>();
                Dictionary<Guid, int> validCharacterSpeakerMap = new Dictionary<Guid, int>();

                this._characterMappings = new CharacterMappings(this);
                foreach (Conversation conversation in this._conversations)
                {
                    foreach (ILine line in conversation.Lines)
                    {
                        if (Guid.Equals(line.CharacterId, Guid.Empty))
                        {
                            continue;
                        }

                        if (tempCharacterSpeakerMap.ContainsKey(line.CharacterId))
                        {
                            int tempSpeaker = tempCharacterSpeakerMap[line.CharacterId];
                            if (tempSpeaker != line.Speaker && line.Speaker != 9)
                            {
                                validCharacterSpeakerMap.Remove(line.CharacterId);
                            }
                        }
                        else
                        {
                            tempCharacterSpeakerMap.Add(line.CharacterId, line.Speaker);
                            validCharacterSpeakerMap.Add(line.CharacterId, line.Speaker);
                        }
                    }
                }

                using (new SuspendUndoEngine(this.UndoEngine, SuspendUndoEngineMode.UndoEvents))
                {
                    foreach (var kvp in validCharacterSpeakerMap)
                    {
                        this._characterMappings.AddOrModifyMapping(kvp.Key, kvp.Value, null);
                    }
                }
            }
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a hash set containing all of the currently used audio file-paths including the
        /// ones that are containing within the deleted filename list.
        /// </summary>
        public HashSet<string> AllAudioFilepaths
        {
            get
            {
                HashSet<string> audioFilepaths = new HashSet<string>();
                foreach (Conversation conversation in this._conversations)
                {
                    foreach (ILine line in conversation.Lines)
                    {
                        audioFilepaths.Add(line.AudioFilepath);
                    }
                }

                foreach (string deletedFilename in this._deletedFilenames)
                {
                    audioFilepaths.Add(deletedFilename);
                }

                return audioFilepaths;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this dialogue is for ambient speech.
        /// </summary>
        public bool AmbientFile
        {
            get { return this._ambientFile; }
            set { this.SetProperty(ref this._ambientFile, value); }
        }

        /// <summary>
        /// Gets the character mappings that have been setup for this dialogue object.
        /// </summary>
        public CharacterMappings CharacterMappings
        {
            get { return this._characterMappings; }
        }

        /// <summary>
        /// Gets the dialogue configurations that have been associated with this dialogue.
        /// </summary>
        public DialogueConfigurations Configurations
        {
            get { return this._configurations ?? new DialogueConfigurations(); }
        }

        /// <summary>
        /// Gets the collection of conversations belonging to this dialogue.
        /// </summary>
        public ReadOnlyModelCollection<Conversation> Conversations
        {
            get { return this._readOnlyConversations; }
        }

        /// <summary>
        /// Gets the list of deleted filenames associated with this dialogue object.
        /// </summary>
        public List<string> DeletedFilenames
        {
            get { return this._deletedFilenames; }
        }

        /// <summary>
        /// Gets or sets the short id used to identify the dialogue.
        /// </summary>
        public string Id
        {
            get { return this._id; }
            set { this.SetProperty(ref this._id, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this dialogue is for a multiplayer mission
        /// or activity.
        /// </summary>
        public bool Multiplayer
        {
            get { return this._multiplayer; }
            set { this.SetProperty(ref this._multiplayer, value); }
        }

        /// <summary>
        /// Gets or sets a longer version of the id that is used to comment text files.
        /// </summary>
        public string Name
        {
            get { return this._name; }
            set { this.SetProperty(ref this._name, value); }
        }

        /// <summary>
        /// Gets or sets the subtitle group id for all of the subtitles defined in this
        /// dialogue. This is the short identifier with either AUD or AU appended onto it.
        /// </summary>
        public string SubtitleId
        {
            get { return this._subtitleId; }
            set { this.SetProperty(ref this._subtitleId, value); }
        }

        /// <summary>
        /// Gets or sets a string indicating the type of dialogue this is.
        /// </summary>
        public string Type
        {
            get { return this._type; }
            set { this.SetProperty(ref this._type, value); }
        }

        /// <summary>
        /// Gets the instance of the <see cref="RSG.Editor.Model.UndoEngine"/>class that this
        /// object is using as its undo engine.
        /// </summary>
        public override UndoEngine UndoEngine
        {
            get { return this._undoEngine; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new conversation and adds it to this dialogue before returning the newly
        /// created conversation.
        /// </summary>
        /// <param name="reader">
        /// The reader that contains the data used to create the new conversation.
        /// </param>
        /// <returns>
        /// The newly created conversation.
        /// </returns>
        public Conversation AddDeserialisedConversation(XmlReader reader)
        {
            Conversation conversation = new Conversation(reader, this);
            this._conversations.Add(conversation);
            return conversation;
        }

        /// <summary>
        /// Creates a new conversation and adds it to this dialogue at the specified index
        /// before returning the newly created conversation.
        /// </summary>
        /// <param name="reader">
        /// The reader that contains the data used to create the new conversation.
        /// </param>
        /// <param name="index">
        /// The zero-based index at which the conversation should be inserted.
        /// </param>
        /// <returns>
        /// The newly created conversation.
        /// </returns>
        public Conversation AddDeserialisedConversationAt(XmlReader reader, int index)
        {
            Conversation conversation = new Conversation(reader, this);
            this._conversations.Insert(index, conversation);
            return conversation;
        }

        /// <summary>
        /// Creates a new conversation and adds it to this dialogue before returning the newly
        /// created conversation.
        /// </summary>
        /// <returns>
        /// The newly created conversation.
        /// </returns>
        public Conversation AddNewConversation()
        {
            Conversation conversation = new Conversation(this);
            this._conversations.Add(conversation);
            return conversation;
        }

        /// <summary>
        /// Creates a new conversation and adds it to this dialogue at the specified index
        /// before returning the newly created conversation.
        /// </summary>
        /// <param name="index">
        /// The zero-based index at which the conversation should be inserted.
        /// </param>
        /// <returns>
        /// The newly created conversation.
        /// </returns>
        public Conversation AddNewConversationAt(int index)
        {
            Conversation conversation = new Conversation(this);
            this._conversations.Insert(index, conversation);
            return conversation;
        }

        /// <summary>
        /// Adds a cloned instance of the specified conversation to this dialogue.
        /// </summary>
        /// <param name="conversation">
        /// The conversation to clone and add to the collection.
        /// </param>
        public void AddCloneConversation(Conversation conversation)
        {
            this._conversations.Add(conversation.DeepClone(this));
        }

        /// <summary>
        /// Inserts the specified conversation in this conversation at the specified index.
        /// </summary>
        /// <param name="conversation">
        /// The conversation to insert into the collection.
        /// </param>
        /// <param name="index">
        /// The zero-based index at which the conversation should be inserted.
        /// </param>
        public void AddCloneConversationAt(Conversation conversation, int index)
        {
            this._conversations.Insert(index, conversation.DeepClone(this));
        }

        /// <summary>
        /// Creates a deep copy of the current instance.
        /// </summary>
        /// <returns>
        /// A deep copy of the current System.Object.
        /// </returns>
        public override object DeepClone()
        {
            return new Dialogue(this);
        }

        /// <summary>
        /// Moves the first occurrence of the specified conversation to the specified index.
        /// </summary>
        /// <param name="conversation">
        /// The conversation that should be moved.
        /// </param>
        /// <param name="newIndex">
        /// The zero-based index specifying the new location of the conversation.
        /// </param>
        public void MoveConversation(Conversation conversation, int newIndex)
        {
            using (new UndoRedoBatch(this.UndoEngine))
            {
                this._conversations.Remove(conversation);
                this._conversations.Insert(newIndex, conversation);
            }
        }

        /// <summary>
        /// Moves the conversation at the specified index to a new location in the collection.
        /// </summary>
        /// <param name="oldIndex">
        /// The zero-based index specifying the location of the conversation to be moved.
        /// </param>
        /// <param name="newIndex">
        /// The zero-based index specifying the new location of the conversation.
        /// </param>
        public void MoveConversation(int oldIndex, int newIndex)
        {
            Conversation item = this._conversations[oldIndex];
            using (new UndoRedoBatch(this.UndoEngine))
            {
                this._conversations.RemoveAt(oldIndex);
                this._conversations.Insert(newIndex, item);
            }
        }

        /// <summary>
        /// Removes the first occurrence of the specified conversation from this dialogue.
        /// </summary>
        /// <param name="conversation">
        /// The conversation to remove from this dialogue.
        /// </param>
        public void RemoveConversation(Conversation conversation)
        {
            int index = this._conversations.IndexOf(conversation);
            if (index == -1)
            {
                return;
            }

            this.RemoveConversationAt(index);
        }

        /// <summary>
        /// Removes the conversation at the specified index.
        /// </summary>
        /// <param name="index">
        /// The zero-based index of the conversation to remove.
        /// </param>
        public void RemoveConversationAt(int index)
        {
            if (index > this._conversations.Count - 1 || index < 0)
            {
                Debug.Assert(false, "Incorrect index specified, returning as not to crash");
                return;
            }

            this._conversations.RemoveAt(index);
        }

        /// <summary>
        /// Serialises the data contained within this instance onto the specified xml writer.
        /// </summary>
        /// <param name="writer">
        /// The xml writer this instance will be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                Debug.Assert(writer != null, "Unable to serialise into a null writer.");
                return;
            }

            writer.WriteStartElement("AmbientFile");
            writer.WriteAttributeString("value", this._ambientFile.ToString());
            writer.WriteEndElement();

            writer.WriteElementString("Id", this._id);

            writer.WriteStartElement("Multiplayer");
            writer.WriteAttributeString("value", this._multiplayer.ToString());
            writer.WriteEndElement();

            writer.WriteElementString("Name", this._name);
            writer.WriteElementString("SubtitleId", this._subtitleId);
            writer.WriteElementString("Type", this._type);

            writer.WriteStartElement("Conversations");
            foreach (Conversation conversation in this._conversations)
            {
                writer.WriteStartElement("Item");
                conversation.Serialise(writer);
                writer.WriteEndElement();
            }

            writer.WriteEndElement();

            writer.WriteStartElement("DeletedFilenames");
            foreach (string deletedFilename in this._deletedFilenames)
            {
                writer.WriteElementString("Item", deletedFilename);
            }

            writer.WriteEndElement();

            writer.WriteStartElement("CharacterMappings");
            foreach (CharacterMapping mapping in this._characterMappings.Mappings)
            {
                if (Guid.Equals(mapping.CharacterId, Guid.Empty))
                {
                    if (String.IsNullOrWhiteSpace(mapping.VoiceName))
                    {
                        continue;
                    }
                }

                writer.WriteStartElement("Item");
                writer.WriteElementString("CharacterId", mapping.CharacterId.ToString());

                writer.WriteStartElement("SpeakerId");
                writer.WriteAttributeString("value", mapping.SpeakerId.ToString());
                writer.WriteEndElement();

                writer.WriteElementString("VoiceName", mapping.VoiceName);
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }

        /// <summary>
        /// Indicates whether the current object is equal to the specified object. When this
        /// method is called it has already been determined that the specified object is not
        /// equal to the current object or null through reference equality.
        /// </summary>
        /// <param name="other">
        /// An object to compare with this object.
        /// </param>
        /// <returns>
        /// True if the current object is equal to the other parameter; otherwise, false.
        /// </returns>
        protected override bool EqualsCore(IModel other)
        {
            Dialogue otherDialogue = other as Dialogue;
            if (otherDialogue == null)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Initialises this instance using the data contained within the specified xml reader.
        /// </summary>
        /// <param name="reader">
        /// The xml reader that contains the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() == XmlNodeType.Element)
            {
                if (String.Equals("AmbientFile", reader.Name))
                {
                    string value = reader.GetAttribute("value");
                    if (!bool.TryParse(value, out this._ambientFile))
                    {
                        this._ambientFile = false;
                    }
                }
                else if (String.Equals("Id", reader.Name))
                {
                    this._id = reader.ReadElementContentAsString();
                }
                else if (String.Equals("Multiplayer", reader.Name))
                {
                    string value = reader.GetAttribute("value");
                    if (!bool.TryParse(value, out this._multiplayer))
                    {
                        this._multiplayer = false;
                    }
                }
                else if (String.Equals("Name", reader.Name))
                {
                    this._name = reader.ReadElementContentAsString();
                }
                else if (String.Equals("SubtitleId", reader.Name))
                {
                    this._subtitleId = reader.ReadElementContentAsString();
                }
                else if (String.Equals("Type", reader.Name))
                {
                    this._type = reader.ReadElementContentAsString();
                }
                else if (String.Equals("Conversations", reader.Name))
                {
                    this.DeserialiseConversations(reader);
                }
                else if (String.Equals("DeletedFilenames", reader.Name))
                {
                    this.DeserialiseDeletedFilenames(reader);
                }
                else if (String.Equals("CharacterMappings", reader.Name))
                {
                    this.DeserialiseCharacterMappings(reader);
                }

                reader.Skip();
            }

            if (reader.NodeType == XmlNodeType.EndElement)
            {
                reader.ReadEndElement();
            }
        }

        /// <summary>
        /// Creates the conversations for this dialogue using the data contained with the
        /// specified xml reader.
        /// </summary>
        /// <param name="reader">
        /// The xml reader that contains the data used to initialise the conversations for this
        /// dialogue.
        /// </param>
        private void DeserialiseConversations(XmlReader reader)
        {
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() == XmlNodeType.Element)
            {
                if (String.Equals("Item", reader.Name))
                {
                    this._conversations.Add(new Conversation(reader, this));
                }
                else
                {
                    reader.Skip();
                }
            }

            if (reader.NodeType == XmlNodeType.EndElement)
            {
                reader.ReadEndElement();
            }
        }

        /// <summary>
        /// Creates the deleted filename collection for this dialogue using the data contained
        /// with the specified xml reader.
        /// </summary>
        /// <param name="reader">
        /// The xml reader that contains the data used to initialise the deleted filename
        /// collection for this dialogue.
        /// </param>
        private void DeserialiseDeletedFilenames(XmlReader reader)
        {
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() == XmlNodeType.Element)
            {
                if (String.Equals("Item", reader.Name))
                {
                    this._deletedFilenames.Add(reader.ReadElementContentAsString());
                }
                else
                {
                    reader.Skip();
                }
            }

            if (reader.NodeType == XmlNodeType.EndElement)
            {
                reader.ReadEndElement();
            }
        }

        /// <summary>
        /// Creates the character mappings for this dialogue using the data contained with the
        /// specified xml reader.
        /// </summary>
        /// <param name="reader">
        /// The xml reader that contains the data used to initialise the character mappings for
        /// this dialogue.
        /// </param>
        private void DeserialiseCharacterMappings(XmlReader reader)
        {
            this._characterMappings = new CharacterMappings(this, reader);
        }
        #endregion Methods
    } // RSG.Text.Model.Dialogue {Class}
} // RSG.Text.Model {Namespace}
