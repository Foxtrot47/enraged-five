﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common.Map;
using RSG.Base.Tasks;
using System.Threading;

namespace RSG.Model.Common.Util
{
    /// <summary>
    /// 
    /// </summary>
    public class SectionTaskContext : TaskContext
    {
        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<IMapSection> Sections
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="token"></param>
        public SectionTaskContext(CancellationToken token, IEnumerable<IMapSection> sections)
            : base(token)
        {
            Sections = sections;
        }
    } // OverlaySectionContext
}
