﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.ConfigParser;
using RSG.Base.Configuration;
using RSG.Model.Common;

namespace RSG.Model.Map3
{
    /// <summary>
    /// Map hierarchy that comes from the local config/export data
    /// </summary>
    public class ExportDataMapHierarchy : MapHierarchyBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public ConfigGameView GameView { get; private set; }

        /// <summary>
        /// Configuration object.
        /// </summary>
        public IConfig Config { get; private set; }

        /// <summary>
        /// The source folder for the weapon data
        /// </summary>
        public String ExportDataPath { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        /// <param name="streamingCoordinator"></param>
        /// <param name="gv"></param>
        /// <param name="exportDataPath"></param>
        public ExportDataMapHierarchy(ILevel level, MapStatStreamingCoordinator streamingCoordinator, ConfigGameView gv, IConfig config)
            : base(level, streamingCoordinator)
        {
            GameView = gv;
            ExportDataPath = GameView.ExportDir;
            Config = config;
        }
        #endregion // Constructor(s)
    } // ExportDataMapHierarchy
}
