﻿//---------------------------------------------------------------------------------------------
// <copyright file="Mat33VTunable.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Tunables
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Text;
    using System.Xml;
    using RSG.Base;
    using RSG.Base.Extensions;
    using RSG.Base.Logging;
    using RSG.Editor;
    using RSG.Editor.Model;
    using RSG.Metadata.Model.Definitions.Members;
    using RSG.Metadata.Model.Resources;

    /// <summary>
    /// Represents a tunable used by the parCodeGen system that is instancing a
    /// <see cref="Mat33VMember"/> object.
    /// </summary>
    public class Mat33VTunable : TunableBase
    {
        #region Fields
        /// <summary>
        /// The private field used to store all of the matrices component values.
        /// </summary>
        private float[] _values;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="Mat33VTunable"/> class to be as
        /// instance of the specified member.
        /// </summary>
        /// <param name="member">
        /// The member that this tunable will be instancing.
        /// </param>
        /// <param name="parent">
        /// The tunable parent this tunable will belong to.
        /// </param>
        public Mat33VTunable(Mat33VMember member, ITunableParent parent)
            : base(member, parent)
        {
            this._values = new float[9];
            float[] initialValue = member.InitialValue;
            for (int i = 0; i < initialValue.Length && i < this._values.Length; i++)
            {
                this._values[i] = initialValue[i];
            }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Mat33VTunable"/> class as a copy of
        /// the specified instance.
        /// </summary>
        /// <param name="other">
        /// The instance to copy.
        /// </param>
        /// <param name="parent">
        /// The tunable parent this tunable will belong to.
        /// </param>
        public Mat33VTunable(Mat33VTunable other, ITunableParent parent)
            : base(other, parent)
        {
            this._values = new float[9];
            for (int i = 0; i < other._values.Length && i < this._values.Length; i++)
            {
                this._values[i] = other._values[i];
            }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Mat33VTunable"/> class using the
        /// specified System.Xml.XmlReader as a initialising data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <param name="member">
        /// The member that this tunable will be instancing.
        /// </param>
        /// <param name="parent">
        /// The tunable parent this tunable will belong to.
        /// </param>
        /// <param name="log">
        /// A object that is used to log any warnings or errors produced during the parsing of
        /// the specified reader.
        /// </param>
        public Mat33VTunable(
            XmlReader reader, Mat33VMember member, ITunableParent parent, ILog log)
            : base(reader, member, parent)
        {
            this._values = new float[9];
            float[] initialValue = member.InitialValue;
            for (int i = 0; i < initialValue.Length && i < this._values.Length; i++)
            {
                this._values[i] = initialValue[i];
            }

            this.Deserialise(reader, log);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the float member that this tunable is instancing.
        /// </summary>
        public Mat33VMember Mat33VMember
        {
            get
            {
                Mat33VMember member = this.Member as Mat33VMember;
                if (member != null)
                {
                    return member;
                }

                return new Mat33VMember(this.TunableStructure.Definition);
            }
        }

        /// <summary>
        /// Gets or sets the value of the xx component for this matrix.
        /// </summary>
        public float XX
        {
            get { return this._values[0]; }
            set { this.SetProperty(ref this._values[0], value); }
        }

        /// <summary>
        /// Gets or sets the value of the xy component for this matrix.
        /// </summary>
        public float XY
        {
            get { return this._values[1]; }
            set { this.SetProperty(ref this._values[1], value); }
        }

        /// <summary>
        /// Gets or sets the value of the xz component for this matrix.
        /// </summary>
        public float XZ
        {
            get { return this._values[2]; }
            set { this.SetProperty(ref this._values[2], value); }
        }

        /// <summary>
        /// Gets or sets the value of the yx component for this matrix.
        /// </summary>
        public float YX
        {
            get { return this._values[3]; }
            set { this.SetProperty(ref this._values[3], value); }
        }

        /// <summary>
        /// Gets or sets the value of the yy component for this matrix.
        /// </summary>
        public float YY
        {
            get { return this._values[4]; }
            set { this.SetProperty(ref this._values[4], value); }
        }

        /// <summary>
        /// Gets or sets the value of the yz component for this matrix.
        /// </summary>
        public float YZ
        {
            get { return this._values[5]; }
            set { this.SetProperty(ref this._values[5], value); }
        }

        /// <summary>
        /// Gets or sets the value of the zx component for this matrix.
        /// </summary>
        public float ZX
        {
            get { return this._values[6]; }
            set { this.SetProperty(ref this._values[6], value); }
        }

        /// <summary>
        /// Gets or sets the value of the zy component for this matrix.
        /// </summary>
        public float ZY
        {
            get { return this._values[7]; }
            set { this.SetProperty(ref this._values[7], value); }
        }

        /// <summary>
        /// Gets or sets the value of the zz component for this matrix.
        /// </summary>
        public float ZZ
        {
            get { return this._values[8]; }
            set { this.SetProperty(ref this._values[8], value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new <see cref="Mat33VTunable"/> that is a copy of this instance.
        /// </summary>
        /// <returns>
        /// A new <see cref="Mat33VTunable"/> that is a copy of this instance.
        /// </returns>
        public new Mat33VTunable Clone()
        {
            return new Mat33VTunable(this, this.TunableStructure);
        }

        /// <summary>
        /// Creates a deep copy of the current instance.
        /// </summary>
        /// <returns>
        /// A deep copy of the current System.Object.
        /// </returns>
        public override object DeepClone()
        {
            return this.Clone();
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="ITunable"/> by
        /// looking at their values.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance by their values;
        /// otherwise, false.
        /// </returns>
        public override bool EqualByValue(ITunable other)
        {
            return this.EqualByValue(other as Mat33VTunable);
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="ITunable"/> by
        /// looking at their values.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance by their values;
        /// otherwise, false.
        /// </returns>
        public bool EqualByValue(Mat33VTunable other)
        {
            if (other == null)
            {
                return false;
            }

            if (this.XX != other.XX)
            {
                return false;
            }

            if (this.XY != other.XY)
            {
                return false;
            }

            if (this.XZ != other.XZ)
            {
                return false;
            }

            if (this.YX != other.YX)
            {
                return false;
            }

            if (this.YY != other.YY)
            {
                return false;
            }

            if (this.YZ != other.YZ)
            {
                return false;
            }

            if (this.ZX != other.ZX)
            {
                return false;
            }

            if (this.ZY != other.ZY)
            {
                return false;
            }

            if (this.ZZ != other.ZZ)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified
        /// <see cref="Mat33VTunable"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public bool Equals(Mat33VTunable other)
        {
            if (other == null)
            {
                return false;
            }

            return Object.ReferenceEquals(other, this);
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="ITunable"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public override bool Equals(ITunable other)
        {
            return this.Equals(other as Mat33VTunable);
        }

        /// <summary>
        /// Resets this tunable to have its default value. (i.e no source data with its initial
        /// values).
        /// </summary>
        public override void ResetValueToDefaultCore()
        {
            Mat33VTunable source = this.InheritanceParent as Mat33VTunable;
            if (source != null)
            {
                this.XX = source.XX;
                this.XY = source.XY;
                this.XZ = source.XZ;

                this.YX = source.YX;
                this.YY = source.YY;
                this.YZ = source.YZ;

                this.ZX = source.ZX;
                this.ZY = source.ZY;
                this.ZZ = source.ZZ;
            }
            else
            {
                float[] initialValue = this.Mat33VMember.InitialValue;
                this.XX = initialValue[0];
                this.XY = initialValue[1];
                this.XZ = initialValue[2];

                this.YX = initialValue[4];
                this.YY = initialValue[5];
                this.YZ = initialValue[6];

                this.ZX = initialValue[8];
                this.ZY = initialValue[9];
                this.ZZ = initialValue[10];
            }
        }

        /// <summary>
        /// Writes the xml representation of this instance onto the specified
        /// System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that the xml representation of this instance should be
        /// written to.
        /// </param>
        /// <param name="serialiseDefaultTunables">
        /// A value indicating whether the tunables that have a default value should be written
        /// to the specified writer or not.
        /// </param>
        public override void Serialise(XmlWriter writer, bool serialiseDefaultTunables)
        {
            if (writer == null)
            {
                throw new SmartArgumentNullException(() => writer);
            }

            int hierarchyLevel = 0;
            IModel parent = this;
            TunableStructure tunableStructure = parent as TunableStructure;
            string separator = writer.Settings.NewLineChars + writer.Settings.IndentChars;
            while (parent != null && tunableStructure == null)
            {
                separator += writer.Settings.IndentChars;
                hierarchyLevel++;
                parent = parent.Parent;
                tunableStructure = parent as TunableStructure;
            }

            StringBuilder builder = new StringBuilder();
            int perLine = this.Mat33VMember.DimensionWidth;
            int total = perLine * this.Mat33VMember.DimensionHeight;
            for (int i = 0; i < total;)
            {
                builder.Append(separator);
                List<string> lineValues = new List<string>();
                for (int j = 0; j < perLine; i++, j++)
                {
                    lineValues.Add(this.GetScalarRepresentation(this._values[i]));
                }

                builder.Append(String.Join(" ", lineValues));
            }

            writer.WriteAttributeString("content", "matrix33");
            builder.Append(writer.Settings.NewLineChars);
            builder.Insert(builder.Length, writer.Settings.IndentChars, hierarchyLevel);
            writer.WriteString(builder.ToString());
        }

        /// <summary>
        /// Validates this entity and creates a validation result object containing all of the
        /// errors and warnings.
        /// </summary>
        /// <param name="recursive">
        /// A value indicating whether any child objects should also be validated.
        /// </param>
        /// <returns>
        /// A new validation object containing all of the errors and warnings.
        /// </returns>
        public override ValidationResult Validate(bool recursive)
        {
            ValidationResult result = new ValidationResult();
            string[] propertyNames = new string[]
            {
                "XX", "XY", "XZ",
                "YX", "YY", "YZ",
                "ZX", "ZY", "ZZ",
            };

            string maximum = this.Mat33VMember.Maximum.ToStringInvariant();
            string minimum = this.Mat33VMember.Minimum.ToStringInvariant();
            string maximumMsg = StringTable.GreaterThanMaximum;
            string minimumMsg = StringTable.LessThanMinimum;
            for (int i = 0; i < this._values.Length; i++)
            {
                if (this._values[i] > this.Mat33VMember.Maximum)
                {
                    string value = this._values[i].ToStringInvariant();
                    string msg = maximumMsg.FormatInvariant(value, maximum);
                    result.AddError(propertyNames[i], msg);
                }

                if (this._values[i] < this.Mat33VMember.Minimum)
                {
                    string value = this._values[i].ToStringInvariant();
                    string msg = minimumMsg.FormatInvariant(value, minimum);
                    result.AddError(propertyNames[i], msg);
                }
            }

            return result;
        }

        /// <summary>
        /// Called whenever the tunable whose value will be used when this tunable is being
        /// inherited changes.
        /// </summary>
        /// <param name="oldValue">
        /// The old inheritance parent.
        /// </param>
        /// <param name="newValue">
        /// The new inheritance parent.
        /// </param>
        protected override void InheritanceParentChanged(ITunable oldValue, ITunable newValue)
        {
            string[] propertyNames = new string[]
            {
                "XX", "XY", "XZ",
                "YX", "YY", "YZ",
                "ZX", "ZY", "ZZ",
            };

            if (oldValue != null)
            {
                foreach (string propertyName in propertyNames)
                {
                    PropertyChangedEventManager.RemoveHandler(
                        oldValue, this.OnInheritedValueChanged, propertyName);
                }
            }

            Mat33VTunable source = newValue as Mat33VTunable;
            if (source == null)
            {
                throw new NotSupportedException(
                    "Only the smae type can be an inheritance parent.");
            }

            foreach (string propertyName in propertyNames)
            {
                PropertyChangedEventManager.AddHandler(
                    source, this.OnInheritedValueChanged, propertyName);
            }

            if (this.HasDefaultValue)
            {
                this._values[0] = source.XX;
                this._values[1] = source.XY;
                this._values[2] = source.XZ;

                this._values[4] = source.YX;
                this._values[5] = source.YY;
                this._values[6] = source.YZ;

                this._values[8] = source.ZX;
                this._values[9] = source.ZY;
                this._values[10] = source.ZZ;

                foreach (string propertyName in propertyNames)
                {
                    this.NotifyPropertyChanged(propertyName);
                }
            }
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        /// <param name="log">
        /// A object that is used to log any warnings or errors produced during the parsing of
        /// the specified reader.
        /// </param>
        private void Deserialise(XmlReader reader, ILog log)
        {
            if (reader == null)
            {
                return;
            }

            IXmlLineInfo lineInfo = reader as IXmlLineInfo;
            int line = lineInfo.LineNumber;
            int pos = lineInfo.LinePosition;

            try
            {
                if (!reader.IsEmptyElement)
                {
                    string value = reader.ReadElementContentAsString();
                    char[] separator = new char[] { ' ', '\t', '\r', '\n' };
                    string[] elements = value.SafeSplit(separator, true);
                    if (elements.Length != 9)
                    {
                        string msg = StringTable.MatrixTunableDataMissingError;
                        log.Warning(msg, elements.Length, 9, line, pos);
                    }

                    for (int i = 0; i < elements.Length && i < this._values.Length; i++)
                    {
                        float fallback = this._values[i];
                        TryResult<float> parse = this.Dictionary.TryTo(elements[i], fallback);
                        if (!parse.Success)
                        {
                            string msg = StringTable.NumericalParseError;
                            log.Warning(msg, elements[i], "float", line, pos);
                        }

                        this._values[i] = parse.Value;
                    }
                }
                else
                {
                    string msg = StringTable.MatrixTunableDataMissingError;
                    log.Warning(msg, 0, 9, line, pos);
                }
            }
            catch (Exception ex)
            {
                string msg = StringTable.MatrixTunableDeserialiseError;
                msg = msg.FormatInvariant(ex.Message);

                throw new MetadataException(msg);
            }

            reader.Skip();
        }

        /// <summary>
        /// Gets the string representation of the specified string value.
        /// </summary>
        /// <param name="value">
        /// The value to convert to a string representation.
        /// </param>
        /// <returns>
        /// A string that represents this tunables value.
        /// </returns>
        private string GetScalarRepresentation(float value)
        {
            if (this.Mat33VMember.HighPrecision)
            {
                return value.ToStringInvariant("F9");
            }
            else
            {
                return value.ToStringInvariant("F6");
            }
        }

        /// <summary>
        /// Called whenever the value of the inheritance parent changes so that the values can
        /// be kept in sync.
        /// </summary>
        /// <param name="sender">
        /// The inheritance parent.
        /// </param>
        /// <param name="e">
        /// The event data.
        /// </param>
        private void OnInheritedValueChanged(object sender, PropertyChangedEventArgs e)
        {
            if (!this.HasDefaultValue)
            {
                return;
            }

            Mat33VTunable source = this.InheritanceParent as Mat33VTunable;
            if (source != null)
            {
                this._values[0] = source.XX;
                this._values[1] = source.XY;
                this._values[2] = source.XZ;

                this._values[4] = source.YX;
                this._values[5] = source.YY;
                this._values[6] = source.YZ;

                this._values[8] = source.ZX;
                this._values[9] = source.ZY;
                this._values[10] = source.ZZ;

                string[] propertyNames = new string[]
                {
                    "XX", "XY", "XZ",
                    "YX", "YY", "YZ",
                    "ZX", "ZY", "ZZ",
                };

                foreach (string propertyName in propertyNames)
                {
                    this.NotifyPropertyChanged(propertyName);
                }
            }
        }
        #endregion Methods
    } // RSG.Metadata.Model.Tunables.Mat33VTunable {Class}
} // RSG.Metadata.Model.Tunables {Namespace}
