﻿using System.Collections.Generic;
using System.Linq;
using RSG.SceneXml;
using RSG.Model.Common;

namespace RSG.Model.Map.Statistics
{
    /// <summary>
    /// 
    /// </summary>
    public class StatisticValues
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public int DrawableCost
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public uint TXDCost
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public int Count
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public int UniqueCount
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public int TXDCount
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public int ShaderCount
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public int TextureCount
        {
            get;
            set;
        }
        
        /// <summary>
        /// 
        /// </summary>
        public int PolygonCount
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public int CollisionCount
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public int MoverCollisionCount
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public int CameraCollisionCount
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public int WeaponCollisionCount
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public int RiverCollisionCount
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public float LodDistance
        {
            get;
            set;
        }
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    public class RawSectionStatistics : Dictionary<RawSectionStatistics.StatisticGroups, Dictionary<RawSectionStatistics.LodLevel, Dictionary<RawSectionStatistics.InstanceType, StatisticValues>>>
    {
        #region Enums
        /// <summary>
        /// 
        /// </summary>
        public enum LodLevel
        {
            HD,
            LOD,
            SLOD1,
            Total,
        }

        /// <summary>
        /// 
        /// </summary>
        public enum StatisticGroups
        {
            DrawableInstances,
            NonDrawableInstances,
            Interiors,
            Instances,
            Drawables
        }

        /// <summary>
        /// 
        /// </summary>
        public enum InstanceType
        {
            Dynamic,
            NonDynamic,
            Total,
        }
        #endregion

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public int TopLevelCollision
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public int TopLevelMoverCollision
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public int TopLevelWeaponCollision
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public int TopLevelCameraCollision
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public int TopLevelRiverCollision
        {
            get;
            set;
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Initialises a new instance of the
        /// RSG,Model.Map.Statistics.RawSectionStatistics
        /// class.
        /// </summary>
        public RawSectionStatistics()
        {
            ResetDrawableStats();
            ResetInstancedStats();
        }     

        /// <summary>
        /// Initialises a new instance of the
        /// RSG,Model.Map.Statistics.RawSectionStatistics
        /// class.
        /// </summary>
        /// <param name="section">
        /// The section that this set of statistics will be
        /// created from.
        /// </param>
        /// <param name="scene">
        /// The Scene instance that the section was
        /// constructed from.
        /// </param>
        public RawSectionStatistics(MapSection section, Scene scene)
        {
            var drawables = from d in section.AssetChildren.OfType<MapDefinition>()
                            select d;

            var instances = from i in section.AssetChildren.OfType<MapInstance>()
                            where i.ReferencedDefinition != null && !i.ReferencedDefinition.IsMilo
                            select i;

            var interiors = from i in section.AssetChildren.OfType<MapInstance>()
                            where i.ReferencedDefinition != null && i.ReferencedDefinition.IsMilo
                            select i;

            if (section.ExportInstances)
            {
                CreateStatistics(drawables, instances, interiors);
                CreateStatistics(drawables);
                GetTopLevelCollision(scene);
            }
            else
            {
                CreateStatistics(drawables);
                ResetInstancedStats();
            }
        }

        /// <summary>
        /// Initialises a new instance of the
        /// RSG,Model.Map.Statistics.RawSectionStatistics
        /// class.
        /// </summary>
        /// <param name="section">
        /// The section that this set of statistics will be
        /// created from.
        /// </param>
        /// <param name="scene">
        /// The Scene instance that the section was
        /// constructed from.
        /// </param>
        public RawSectionStatistics(List<MapSection> sections)
        {
            var drawables = from s in sections
                            from d in s.AssetChildren.OfType<MapDefinition>()
                            select d;

            var instances = from s in sections
                            from i in s.AssetChildren.OfType<MapInstance>()
                            where i.ReferencedDefinition != null && !i.ReferencedDefinition.IsMilo
                            select i;

            var interiors = from s in sections
                            from i in s.AssetChildren.OfType<MapInstance>()
                            where i.ReferencedDefinition != null && i.ReferencedDefinition.IsMilo
                            select i;

            CreateStatistics(drawables, instances, interiors);
            CreateStatistics(drawables);
        }
        #endregion

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        private void ResetDrawableStats()
        {
            this.Add(StatisticGroups.Drawables, new Dictionary<LodLevel, Dictionary<InstanceType, StatisticValues>>());
            this[StatisticGroups.Drawables].Add(LodLevel.HD, new Dictionary<InstanceType, StatisticValues>());
            this[StatisticGroups.Drawables][LodLevel.HD].Add(InstanceType.Dynamic, new StatisticValues());
            this[StatisticGroups.Drawables][LodLevel.HD].Add(InstanceType.NonDynamic, new StatisticValues());
            this[StatisticGroups.Drawables][LodLevel.HD].Add(InstanceType.Total, new StatisticValues());
            this[StatisticGroups.Drawables].Add(LodLevel.LOD, new Dictionary<InstanceType, StatisticValues>());
            this[StatisticGroups.Drawables][LodLevel.LOD].Add(InstanceType.Dynamic, new StatisticValues());
            this[StatisticGroups.Drawables][LodLevel.LOD].Add(InstanceType.NonDynamic, new StatisticValues());
            this[StatisticGroups.Drawables][LodLevel.LOD].Add(InstanceType.Total, new StatisticValues());
            this[StatisticGroups.Drawables].Add(LodLevel.SLOD1, new Dictionary<InstanceType, StatisticValues>());
            this[StatisticGroups.Drawables][LodLevel.SLOD1].Add(InstanceType.Dynamic, new StatisticValues());
            this[StatisticGroups.Drawables][LodLevel.SLOD1].Add(InstanceType.NonDynamic, new StatisticValues());
            this[StatisticGroups.Drawables][LodLevel.SLOD1].Add(InstanceType.Total, new StatisticValues());
            this[StatisticGroups.Drawables].Add(LodLevel.Total, new Dictionary<InstanceType, StatisticValues>());
            this[StatisticGroups.Drawables][LodLevel.Total].Add(InstanceType.Dynamic, new StatisticValues());
            this[StatisticGroups.Drawables][LodLevel.Total].Add(InstanceType.NonDynamic, new StatisticValues());
            this[StatisticGroups.Drawables][LodLevel.Total].Add(InstanceType.Total, new StatisticValues());
        }

        /// <summary>
        /// 
        /// </summary>
        private void ResetInstancedStats()
        {

            this.Add(StatisticGroups.DrawableInstances, new Dictionary<LodLevel, Dictionary<InstanceType, StatisticValues>>());
            this[StatisticGroups.DrawableInstances].Add(LodLevel.HD, new Dictionary<InstanceType, StatisticValues>());
            this[StatisticGroups.DrawableInstances][LodLevel.HD].Add(InstanceType.Dynamic, new StatisticValues());
            this[StatisticGroups.DrawableInstances][LodLevel.HD].Add(InstanceType.NonDynamic, new StatisticValues());
            this[StatisticGroups.DrawableInstances][LodLevel.HD].Add(InstanceType.Total, new StatisticValues());
            this[StatisticGroups.DrawableInstances].Add(LodLevel.LOD, new Dictionary<InstanceType, StatisticValues>());
            this[StatisticGroups.DrawableInstances][LodLevel.LOD].Add(InstanceType.Dynamic, new StatisticValues());
            this[StatisticGroups.DrawableInstances][LodLevel.LOD].Add(InstanceType.NonDynamic, new StatisticValues());
            this[StatisticGroups.DrawableInstances][LodLevel.LOD].Add(InstanceType.Total, new StatisticValues());
            this[StatisticGroups.DrawableInstances].Add(LodLevel.SLOD1, new Dictionary<InstanceType, StatisticValues>());
            this[StatisticGroups.DrawableInstances][LodLevel.SLOD1].Add(InstanceType.Dynamic, new StatisticValues());
            this[StatisticGroups.DrawableInstances][LodLevel.SLOD1].Add(InstanceType.NonDynamic, new StatisticValues());
            this[StatisticGroups.DrawableInstances][LodLevel.SLOD1].Add(InstanceType.Total, new StatisticValues());
            this[StatisticGroups.DrawableInstances].Add(LodLevel.Total, new Dictionary<InstanceType, StatisticValues>());
            this[StatisticGroups.DrawableInstances][LodLevel.Total].Add(InstanceType.Dynamic, new StatisticValues());
            this[StatisticGroups.DrawableInstances][LodLevel.Total].Add(InstanceType.NonDynamic, new StatisticValues());
            this[StatisticGroups.DrawableInstances][LodLevel.Total].Add(InstanceType.Total, new StatisticValues());

            this.Add(StatisticGroups.NonDrawableInstances, new Dictionary<LodLevel, Dictionary<InstanceType, StatisticValues>>());
            this[StatisticGroups.NonDrawableInstances].Add(LodLevel.HD, new Dictionary<InstanceType, StatisticValues>());
            this[StatisticGroups.NonDrawableInstances][LodLevel.HD].Add(InstanceType.Dynamic, new StatisticValues());
            this[StatisticGroups.NonDrawableInstances][LodLevel.HD].Add(InstanceType.NonDynamic, new StatisticValues());
            this[StatisticGroups.NonDrawableInstances][LodLevel.HD].Add(InstanceType.Total, new StatisticValues());
            this[StatisticGroups.NonDrawableInstances].Add(LodLevel.LOD, new Dictionary<InstanceType, StatisticValues>());
            this[StatisticGroups.NonDrawableInstances][LodLevel.LOD].Add(InstanceType.Dynamic, new StatisticValues());
            this[StatisticGroups.NonDrawableInstances][LodLevel.LOD].Add(InstanceType.NonDynamic, new StatisticValues());
            this[StatisticGroups.NonDrawableInstances][LodLevel.LOD].Add(InstanceType.Total, new StatisticValues());
            this[StatisticGroups.NonDrawableInstances].Add(LodLevel.SLOD1, new Dictionary<InstanceType, StatisticValues>());
            this[StatisticGroups.NonDrawableInstances][LodLevel.SLOD1].Add(InstanceType.Dynamic, new StatisticValues());
            this[StatisticGroups.NonDrawableInstances][LodLevel.SLOD1].Add(InstanceType.NonDynamic, new StatisticValues());
            this[StatisticGroups.NonDrawableInstances][LodLevel.SLOD1].Add(InstanceType.Total, new StatisticValues());
            this[StatisticGroups.NonDrawableInstances].Add(LodLevel.Total, new Dictionary<InstanceType, StatisticValues>());
            this[StatisticGroups.NonDrawableInstances][LodLevel.Total].Add(InstanceType.Dynamic, new StatisticValues());
            this[StatisticGroups.NonDrawableInstances][LodLevel.Total].Add(InstanceType.NonDynamic, new StatisticValues());
            this[StatisticGroups.NonDrawableInstances][LodLevel.Total].Add(InstanceType.Total, new StatisticValues());

            this.Add(StatisticGroups.Instances, new Dictionary<LodLevel, Dictionary<InstanceType, StatisticValues>>());
            this[StatisticGroups.Instances].Add(LodLevel.HD, new Dictionary<InstanceType, StatisticValues>());
            this[StatisticGroups.Instances][LodLevel.HD].Add(InstanceType.Dynamic, new StatisticValues());
            this[StatisticGroups.Instances][LodLevel.HD].Add(InstanceType.NonDynamic, new StatisticValues());
            this[StatisticGroups.Instances][LodLevel.HD].Add(InstanceType.Total, new StatisticValues());
            this[StatisticGroups.Instances].Add(LodLevel.LOD, new Dictionary<InstanceType, StatisticValues>());
            this[StatisticGroups.Instances][LodLevel.LOD].Add(InstanceType.Dynamic, new StatisticValues());
            this[StatisticGroups.Instances][LodLevel.LOD].Add(InstanceType.NonDynamic, new StatisticValues());
            this[StatisticGroups.Instances][LodLevel.LOD].Add(InstanceType.Total, new StatisticValues());
            this[StatisticGroups.Instances].Add(LodLevel.SLOD1, new Dictionary<InstanceType, StatisticValues>());
            this[StatisticGroups.Instances][LodLevel.SLOD1].Add(InstanceType.Dynamic, new StatisticValues());
            this[StatisticGroups.Instances][LodLevel.SLOD1].Add(InstanceType.NonDynamic, new StatisticValues());
            this[StatisticGroups.Instances][LodLevel.SLOD1].Add(InstanceType.Total, new StatisticValues());
            this[StatisticGroups.Instances].Add(LodLevel.Total, new Dictionary<InstanceType, StatisticValues>());
            this[StatisticGroups.Instances][LodLevel.Total].Add(InstanceType.Dynamic, new StatisticValues());
            this[StatisticGroups.Instances][LodLevel.Total].Add(InstanceType.NonDynamic, new StatisticValues());
            this[StatisticGroups.Instances][LodLevel.Total].Add(InstanceType.Total, new StatisticValues());

            this.Add(StatisticGroups.Interiors, new Dictionary<LodLevel, Dictionary<InstanceType, StatisticValues>>());
            this[StatisticGroups.Interiors].Add(LodLevel.HD, new Dictionary<InstanceType, StatisticValues>());
            this[StatisticGroups.Interiors][LodLevel.HD].Add(InstanceType.Dynamic, new StatisticValues());
            this[StatisticGroups.Interiors][LodLevel.HD].Add(InstanceType.NonDynamic, new StatisticValues());
            this[StatisticGroups.Interiors][LodLevel.HD].Add(InstanceType.Total, new StatisticValues());
            this[StatisticGroups.Interiors].Add(LodLevel.LOD, new Dictionary<InstanceType, StatisticValues>());
            this[StatisticGroups.Interiors][LodLevel.LOD].Add(InstanceType.Dynamic, new StatisticValues());
            this[StatisticGroups.Interiors][LodLevel.LOD].Add(InstanceType.NonDynamic, new StatisticValues());
            this[StatisticGroups.Interiors][LodLevel.LOD].Add(InstanceType.Total, new StatisticValues());
            this[StatisticGroups.Interiors].Add(LodLevel.SLOD1, new Dictionary<InstanceType, StatisticValues>());
            this[StatisticGroups.Interiors][LodLevel.SLOD1].Add(InstanceType.Dynamic, new StatisticValues());
            this[StatisticGroups.Interiors][LodLevel.SLOD1].Add(InstanceType.NonDynamic, new StatisticValues());
            this[StatisticGroups.Interiors][LodLevel.SLOD1].Add(InstanceType.Total, new StatisticValues());
            this[StatisticGroups.Interiors].Add(LodLevel.Total, new Dictionary<InstanceType, StatisticValues>());
            this[StatisticGroups.Interiors][LodLevel.Total].Add(InstanceType.Dynamic, new StatisticValues());
            this[StatisticGroups.Interiors][LodLevel.Total].Add(InstanceType.NonDynamic, new StatisticValues());
            this[StatisticGroups.Interiors][LodLevel.Total].Add(InstanceType.Total, new StatisticValues());
        }

        /// <summary>
        /// Creates the statistics from the
        /// specified section.
        /// </summary>
        /// <param name="section">
        /// The section to create the statistics
        /// from.
        /// </param>
        private void CreateStatistics(IEnumerable<MapDefinition> drawables, IEnumerable<MapInstance> instances, IEnumerable<MapInstance> interiors)
        {
            var drawableNames = (from d in drawables
                                 select d.Name).ToList();

            var drawableInstances = new List<MapInstance>();
            var nonDrawableInstances = new List<MapInstance>();

            foreach (var instance in instances)
            {
                if (drawableNames.Contains(instance.ReferencedDefinition.Name))
                    drawableInstances.Add(instance);
                else
                    nonDrawableInstances.Add(instance);
            }

            CreateDrawableInstanceStatistics(drawableInstances, drawables);
            CreateNonDrawableInstanceStatistics(nonDrawableInstances, drawables);
            CreateTotalInstanceStatistics(instances, drawables);
            CreateInteriorInstanceStatistics(interiors);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="drawables"></param>
        private void CreateStatistics(IEnumerable<MapDefinition> drawables)
        {
            this.Add(StatisticGroups.Drawables, new Dictionary<LodLevel, Dictionary<InstanceType, StatisticValues>>());

            this[StatisticGroups.Drawables][LodLevel.HD] = CreateDrawableStatistics(drawables.ToList());
            this[StatisticGroups.Drawables][LodLevel.LOD] = this[StatisticGroups.Drawables][LodLevel.HD];
            this[StatisticGroups.Drawables][LodLevel.SLOD1] = this[StatisticGroups.Drawables][LodLevel.HD];
            this[StatisticGroups.Drawables][LodLevel.Total] = this[StatisticGroups.Drawables][LodLevel.HD];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="drawableInstances"></param>
        /// <param name="drawables"></param>
        private void CreateDrawableInstanceStatistics(IEnumerable<MapInstance> drawableInstances, IEnumerable<MapDefinition> drawables)
        {
            this.Add(StatisticGroups.DrawableInstances, new Dictionary<LodLevel, Dictionary<InstanceType, StatisticValues>>());

            var hdInstances = from i in drawableInstances
                              where i.LODLevel == ObjectDef.LodLevel.HD || i.LODLevel == ObjectDef.LodLevel.ORPHANHD
                              select i;

            var lodInstances = from i in drawableInstances
                               where i.LODLevel == ObjectDef.LodLevel.LOD || i.LODLevel == ObjectDef.LodLevel.SLOD2
                               select i;

            var slodInstances = from i in drawableInstances
                                where i.LODLevel == ObjectDef.LodLevel.SLOD1 || i.LODLevel == ObjectDef.LodLevel.SLOD3
                                select i;

            this[StatisticGroups.DrawableInstances][LodLevel.HD] = CreateInstanceStatistics(hdInstances);
            this[StatisticGroups.DrawableInstances][LodLevel.LOD] = CreateInstanceStatistics(lodInstances);
            this[StatisticGroups.DrawableInstances][LodLevel.SLOD1] = CreateInstanceStatistics(slodInstances);
            this[StatisticGroups.DrawableInstances][LodLevel.Total] = CreateInstanceStatistics(drawableInstances);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nondrawableInstances"></param>
        /// <param name="drawables"></param>
        private void CreateNonDrawableInstanceStatistics(IEnumerable<MapInstance> nondrawableInstances, IEnumerable<MapDefinition> drawables)
        {
            this.Add(StatisticGroups.NonDrawableInstances, new Dictionary<LodLevel, Dictionary<InstanceType, StatisticValues>>());

            var hdInstances = from i in nondrawableInstances
                              where i.LODLevel == ObjectDef.LodLevel.HD || i.LODLevel == ObjectDef.LodLevel.ORPHANHD
                              select i;

            var lodInstances = from i in nondrawableInstances
                               where i.LODLevel == ObjectDef.LodLevel.LOD || i.LODLevel == ObjectDef.LodLevel.SLOD2
                               select i;

            var slodInstances = from i in nondrawableInstances
                                where i.LODLevel == ObjectDef.LodLevel.SLOD1 || i.LODLevel == ObjectDef.LodLevel.SLOD3
                                select i;

            this[StatisticGroups.NonDrawableInstances][LodLevel.HD] = CreateInstanceStatistics(hdInstances);
            this[StatisticGroups.NonDrawableInstances][LodLevel.LOD] = CreateInstanceStatistics(lodInstances);
            this[StatisticGroups.NonDrawableInstances][LodLevel.SLOD1] = CreateInstanceStatistics(slodInstances);
            this[StatisticGroups.NonDrawableInstances][LodLevel.Total] = CreateInstanceStatistics(nondrawableInstances);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nondrawableInstances"></param>
        /// <param name="drawables"></param>
        private void CreateTotalInstanceStatistics(IEnumerable<MapInstance> instances, IEnumerable<MapDefinition> drawables)
        {
            this.Add(StatisticGroups.Instances, new Dictionary<LodLevel, Dictionary<InstanceType, StatisticValues>>());

            var hdInstances = from i in instances
                              where i.LODLevel == ObjectDef.LodLevel.HD || i.LODLevel == ObjectDef.LodLevel.ORPHANHD
                              select i;

            var lodInstances = from i in instances
                               where i.LODLevel == ObjectDef.LodLevel.LOD || i.LODLevel == ObjectDef.LodLevel.SLOD2
                               select i;

            var slodInstances = from i in instances
                                where i.LODLevel == ObjectDef.LodLevel.SLOD1 || i.LODLevel == ObjectDef.LodLevel.SLOD3
                                select i;

            this[StatisticGroups.Instances][LodLevel.HD] = CreateInstanceStatistics(hdInstances);
            this[StatisticGroups.Instances][LodLevel.LOD] = CreateInstanceStatistics(lodInstances);
            this[StatisticGroups.Instances][LodLevel.SLOD1] = CreateInstanceStatistics(slodInstances);
            this[StatisticGroups.Instances][LodLevel.Total] = CreateInstanceStatistics(instances);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="interiors"></param>
        private void CreateInteriorInstanceStatistics(IEnumerable<MapInstance> interiors)
        {
            this.Add(StatisticGroups.Interiors, new Dictionary<LodLevel, Dictionary<InstanceType, StatisticValues>>());

            this[StatisticGroups.Interiors][LodLevel.HD] = CreateInteriorStatistics(interiors);
            this[StatisticGroups.Interiors][LodLevel.LOD] = this[StatisticGroups.Interiors][LodLevel.HD];
            this[StatisticGroups.Interiors][LodLevel.SLOD1] = this[StatisticGroups.Interiors][LodLevel.HD];
            this[StatisticGroups.Interiors][LodLevel.Total] = this[StatisticGroups.Interiors][LodLevel.HD];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="validInstances"></param>
        /// <returns></returns>
        private Dictionary<InstanceType, StatisticValues> CreateInstanceStatistics(IEnumerable<MapInstance> validInstances)
        {
            var drawables = (from i in validInstances
                             select i.ReferencedDefinition).ToList();

            var dynamicDrawables = (from d in drawables
                                    where d.IsDynamic
                                    select d).ToList();

            var nonDynamicDrawables = (from d in drawables
                                       where !d.IsDynamic
                                       select d).ToList();

            var uniqueDrawables = (from i in validInstances
                                  select i.ReferencedDefinition).Distinct().ToList();

            var uniqueDynamicDrawables = (from d in uniqueDrawables
                                          where d.IsDynamic
                                          select d).ToList();

            var uniqueNonDynamicDrawables = (from d in uniqueDrawables
                                             where !d.IsDynamic
                                             select d).ToList();

            var values = new Dictionary<InstanceType, StatisticValues>();
            values.Add(InstanceType.Dynamic, new StatisticValues());
            values.Add(InstanceType.NonDynamic, new StatisticValues());
            values.Add(InstanceType.Total, new StatisticValues());
            
            // Counts
            values[InstanceType.Total].Count = drawables.Count;
            values[InstanceType.Dynamic].Count = dynamicDrawables.Count;
            values[InstanceType.NonDynamic].Count = nonDynamicDrawables.Count;
            values[InstanceType.Total].UniqueCount = uniqueDrawables.Count;
            values[InstanceType.Dynamic].UniqueCount = uniqueDynamicDrawables.Count;
            values[InstanceType.NonDynamic].UniqueCount = uniqueNonDynamicDrawables.Count;

            // TXDCount
            var uniqueTxds = from t in
                                 (from d in uniqueDrawables select d.TextureDictionaryName).Distinct(new TXDNameComparer())
                             select t;
            values[InstanceType.Total].TXDCount = uniqueTxds.Count();

            uniqueTxds = from t in
                             (from d in uniqueDynamicDrawables select d.TextureDictionaryName).Distinct(new TXDNameComparer())
                         select t;
            values[InstanceType.Dynamic].TXDCount = uniqueTxds.Count();

            uniqueTxds = from t in
                             (from d in uniqueNonDynamicDrawables select d.TextureDictionaryName).Distinct(new TXDNameComparer())
                         select t;
            values[InstanceType.NonDynamic].TXDCount = uniqueTxds.Count();


            // ShaderCount
            var shaderCache = new List<IShader>();
            var shaders = from d in uniqueDrawables
                          from s in d.Shaders
                          select s;
            foreach (var shader in shaders)
            {
                if (!shaderCache.Contains(shader))
                {
                    shaderCache.Add(shader);
                    values[InstanceType.Total].ShaderCount++;
                }
            }
            shaderCache = new List<IShader>();
            shaders = from d in uniqueDynamicDrawables
                      from s in d.Shaders
                      select s;
            foreach (var shader in shaders)
            {
                if (!shaderCache.Contains(shader))
                {
                    shaderCache.Add(shader);
                    values[InstanceType.Dynamic].ShaderCount++;
                }
            }
            shaderCache = new List<IShader>();
            shaders = from d in uniqueNonDynamicDrawables
                      from s in d.Shaders
                      select s;
            foreach (var shader in shaders)
            {
                if (!shaderCache.Contains(shader))
                {
                    shaderCache.Add(shader);
                    values[InstanceType.NonDynamic].ShaderCount++;
                }
            }

            // TextureCount
            var textures = (from d in uniqueDrawables
                            from t in d.AssetChildren.OfType<ITexture>()
                            select t.Name).Distinct();
            values[InstanceType.Total].TextureCount = textures.Count();

            textures = (from d in uniqueDynamicDrawables
                        from t in d.AssetChildren.OfType<ITexture>()
                        select t.Name).Distinct();
            values[InstanceType.Dynamic].TextureCount = textures.Count();

            textures = (from d in uniqueNonDynamicDrawables
                        from t in d.AssetChildren.OfType<ITexture>()
                        select t.Name).Distinct();
            values[InstanceType.NonDynamic].TextureCount = textures.Count();

            // Acculumative Counts
            foreach (var drawable in drawables)
            {
                values[InstanceType.Total].DrawableCost += drawable.ExportGeometrySize;
                values[InstanceType.Total].TXDCost += drawable.ExportTxdSize;
                values[InstanceType.Total].PolygonCount += drawable.PolygonCount;
                values[InstanceType.Total].CollisionCount += drawable.CollisionPolygonCount;
                values[InstanceType.Total].MoverCollisionCount += drawable.MoverPolygonCount;
                values[InstanceType.Total].WeaponCollisionCount += drawable.WeaponsPolygonCount;
                values[InstanceType.Total].CameraCollisionCount += drawable.CameraPolygonCount;
                values[InstanceType.Total].RiverCollisionCount += drawable.RiverPolygonCount;
            }
            foreach (var drawable in dynamicDrawables)
            {
                values[InstanceType.Dynamic].DrawableCost += drawable.ExportGeometrySize;
                values[InstanceType.Dynamic].TXDCost += drawable.ExportTxdSize;
                values[InstanceType.Dynamic].PolygonCount += drawable.PolygonCount;
                values[InstanceType.Dynamic].CollisionCount += drawable.CollisionPolygonCount;
                values[InstanceType.Dynamic].MoverCollisionCount += drawable.MoverPolygonCount;
                values[InstanceType.Dynamic].WeaponCollisionCount += drawable.WeaponsPolygonCount;
                values[InstanceType.Dynamic].CameraCollisionCount += drawable.CameraPolygonCount;
                values[InstanceType.Dynamic].RiverCollisionCount += drawable.RiverPolygonCount;
            }
            foreach (var drawable in nonDynamicDrawables)
            {
                values[InstanceType.NonDynamic].DrawableCost += drawable.ExportGeometrySize;
                values[InstanceType.NonDynamic].TXDCost += drawable.ExportTxdSize;
                values[InstanceType.NonDynamic].PolygonCount += drawable.PolygonCount;
                values[InstanceType.NonDynamic].CollisionCount += drawable.CollisionPolygonCount;
                values[InstanceType.NonDynamic].MoverCollisionCount += drawable.MoverPolygonCount;
                values[InstanceType.NonDynamic].WeaponCollisionCount += drawable.WeaponsPolygonCount;
                values[InstanceType.NonDynamic].CameraCollisionCount += drawable.CameraPolygonCount;
                values[InstanceType.NonDynamic].RiverCollisionCount += drawable.RiverPolygonCount;
            }

            // LodDistance
            if (values[InstanceType.Total].Count > 0)
                values[InstanceType.Total].LodDistance = (from i in validInstances select i.LodDistance).Max();
            if (values[InstanceType.Dynamic].Count > 0)
                values[InstanceType.Dynamic].LodDistance = (from i in validInstances where i.ReferencedDefinition.IsDynamic select i.LodDistance).Max();
            if (values[InstanceType.NonDynamic].Count > 0)
                values[InstanceType.NonDynamic].LodDistance = (from i in validInstances where !i.ReferencedDefinition.IsDynamic select i.LodDistance).Max();
            return values;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="validInstances"></param>
        /// <returns></returns>
        private Dictionary<InstanceType, StatisticValues> CreateInteriorStatistics(IEnumerable<MapInstance> interiorInstances)
        {
            var drawables = (from i in interiorInstances
                             select i.ReferencedDefinition).ToList();

            var dynamicDrawables = (from d in drawables
                                    where d.IsDynamic
                                    select d).ToList();

            var nonDynamicDrawables = (from d in drawables
                                       where !d.IsDynamic
                                       select d).ToList();

            var uniqueDrawables = (from i in interiorInstances
                                   select i.ReferencedDefinition).Distinct().ToList();

            var uniqueDynamicDrawables = (from d in uniqueDrawables
                                          where d.IsDynamic
                                          select d).ToList();

            var uniqueNonDynamicDrawables = (from d in uniqueDrawables
                                             where !d.IsDynamic
                                             select d).ToList();

            var values = new Dictionary<InstanceType, StatisticValues>();
            values.Add(InstanceType.Dynamic, new StatisticValues());
            values.Add(InstanceType.NonDynamic, new StatisticValues());
            values.Add(InstanceType.Total, new StatisticValues());

            // Counts
            values[InstanceType.Total].Count = drawables.Count;
            values[InstanceType.Dynamic].Count = dynamicDrawables.Count;
            values[InstanceType.NonDynamic].Count = nonDynamicDrawables.Count;
            values[InstanceType.Total].UniqueCount = uniqueDrawables.Count;
            values[InstanceType.Dynamic].UniqueCount = uniqueDynamicDrawables.Count;
            values[InstanceType.NonDynamic].UniqueCount = uniqueNonDynamicDrawables.Count;

            // TXDCount
            if (values[InstanceType.Total].Count > 0)
                values[InstanceType.Total].TXDCount = (from d in uniqueDrawables select d.TextureDictionaryCount).Sum();
            if (values[InstanceType.Dynamic].Count > 0)
                values[InstanceType.Dynamic].TXDCount = (from d in uniqueDynamicDrawables select d.TextureDictionaryCount).Sum();
            if (values[InstanceType.NonDynamic].Count > 0)
                values[InstanceType.NonDynamic].TXDCount = (from d in uniqueNonDynamicDrawables select d.TextureDictionaryCount).Sum();

            // ShaderCount
            var uniqueDrawableShaderCache = new List<IShader>();
            var shaders = from d in uniqueDrawables
                          from s in d.Shaders
                          select s;
            foreach (var shader in shaders)
            {
                if (!uniqueDrawableShaderCache.Contains(shader))
                {
                    uniqueDrawableShaderCache.Add(shader);
                    values[InstanceType.Total].ShaderCount++;
                }
            }
            var uniqueDynamicShaderCache = new List<IShader>();
            shaders = from d in uniqueDynamicDrawables
                      from s in d.Shaders
                      select s;
            foreach (var shader in shaders)
            {
                if (!uniqueDynamicShaderCache.Contains(shader))
                {
                    uniqueDynamicShaderCache.Add(shader);
                    values[InstanceType.Dynamic].ShaderCount++;
                }
            }
            var uniqueNonDynamicShaderCache = new List<IShader>();
            shaders = from d in uniqueNonDynamicDrawables
                      from s in d.Shaders
                      select s;
            foreach (var shader in shaders)
            {
                if (!uniqueNonDynamicShaderCache.Contains(shader))
                {
                    uniqueNonDynamicShaderCache.Add(shader);
                    values[InstanceType.NonDynamic].ShaderCount++;
                }
            }

            // TextureCount
            var textures = (from s in uniqueDrawableShaderCache
                            from t in s.Textures
                            select t.Name).Distinct();
            values[InstanceType.Total].TextureCount = textures.Count();

            textures = (from s in uniqueDynamicShaderCache
                        from t in s.Textures
                        select t.Name).Distinct();
            values[InstanceType.Dynamic].TextureCount = textures.Count();

            textures = (from s in uniqueNonDynamicShaderCache
                        from t in s.Textures
                        select t.Name).Distinct();
            values[InstanceType.NonDynamic].TextureCount = textures.Count();

            // Acculumative Counts
            foreach (var drawable in drawables)
            {
                values[InstanceType.Total].DrawableCost += drawable.ExportGeometrySize;
                values[InstanceType.Total].TXDCost += drawable.ExportTxdSize;
                values[InstanceType.Total].PolygonCount += drawable.PolygonCount;
                values[InstanceType.Total].CollisionCount += drawable.CollisionPolygonCount;
                values[InstanceType.Total].MoverCollisionCount += drawable.MoverPolygonCount;
                values[InstanceType.Total].WeaponCollisionCount += drawable.WeaponsPolygonCount;
                values[InstanceType.Total].CameraCollisionCount += drawable.CameraPolygonCount;
                values[InstanceType.Total].RiverCollisionCount += drawable.RiverPolygonCount;
            }
            foreach (var drawable in dynamicDrawables)
            {
                values[InstanceType.Dynamic].DrawableCost += drawable.ExportGeometrySize;
                values[InstanceType.Dynamic].TXDCost += drawable.ExportTxdSize;
                values[InstanceType.Dynamic].PolygonCount += drawable.PolygonCount;
                values[InstanceType.Dynamic].CollisionCount += drawable.CollisionPolygonCount;
                values[InstanceType.Dynamic].MoverCollisionCount += drawable.MoverPolygonCount;
                values[InstanceType.Dynamic].WeaponCollisionCount += drawable.WeaponsPolygonCount;
                values[InstanceType.Dynamic].CameraCollisionCount += drawable.CameraPolygonCount;
                values[InstanceType.Dynamic].RiverCollisionCount += drawable.RiverPolygonCount;
            }
            foreach (var drawable in nonDynamicDrawables)
            {
                values[InstanceType.NonDynamic].DrawableCost += drawable.ExportGeometrySize;
                values[InstanceType.NonDynamic].TXDCost += drawable.ExportTxdSize;
                values[InstanceType.NonDynamic].PolygonCount += drawable.PolygonCount;
                values[InstanceType.NonDynamic].CollisionCount += drawable.CollisionPolygonCount;
                values[InstanceType.NonDynamic].MoverCollisionCount += drawable.MoverPolygonCount;
                values[InstanceType.NonDynamic].WeaponCollisionCount += drawable.WeaponsPolygonCount;
                values[InstanceType.NonDynamic].CameraCollisionCount += drawable.CameraPolygonCount;
                values[InstanceType.NonDynamic].RiverCollisionCount += drawable.RiverPolygonCount;
            }

            return values;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="validInstances"></param>
        /// <returns></returns>
        private Dictionary<InstanceType, StatisticValues> CreateDrawableStatistics(List<MapDefinition> drawables)
        {
            var dynamicDrawables = (from d in drawables
                                    where d.IsDynamic
                                    select d).ToList();

            var nonDynamicDrawables = (from d in drawables
                                       where !d.IsDynamic
                                       select d).ToList();

            var uniqueDrawables = (from d in drawables
                                   select d).Distinct().ToList();

            var uniqueDynamicDrawables = (from d in uniqueDrawables
                                          where d.IsDynamic
                                          select d).ToList();

            var uniqueNonDynamicDrawables = (from d in uniqueDrawables
                                             where !d.IsDynamic
                                             select d).ToList();

            var values = new Dictionary<InstanceType, StatisticValues>();
            values.Add(InstanceType.Dynamic, new StatisticValues());
            values.Add(InstanceType.NonDynamic, new StatisticValues());
            values.Add(InstanceType.Total, new StatisticValues());

            // Counts
            values[InstanceType.Total].Count = drawables.Count;
            values[InstanceType.Dynamic].Count = dynamicDrawables.Count;
            values[InstanceType.NonDynamic].Count = nonDynamicDrawables.Count;
            values[InstanceType.Total].UniqueCount = uniqueDrawables.Count;
            values[InstanceType.Dynamic].UniqueCount = uniqueDynamicDrawables.Count;
            values[InstanceType.NonDynamic].UniqueCount = uniqueNonDynamicDrawables.Count;

            // TXDCount
            var uniqueTxds = from t in
                                 (from d in uniqueDrawables select d.TextureDictionaryName).Distinct(new TXDNameComparer())
                             select t;
            values[InstanceType.Total].TXDCount = uniqueTxds.Count();

            uniqueTxds = from t in
                             (from d in uniqueDynamicDrawables select d.TextureDictionaryName).Distinct(new TXDNameComparer())
                         select t;
            values[InstanceType.Dynamic].TXDCount = uniqueTxds.Count();

            uniqueTxds = from t in
                             (from d in uniqueNonDynamicDrawables select d.TextureDictionaryName).Distinct(new TXDNameComparer())
                         select t;
            values[InstanceType.NonDynamic].TXDCount = uniqueTxds.Count();


            // ShaderCount
            var shaderCache = new List<IShader>();
            var shaders = from d in uniqueDrawables
                          from s in d.Shaders
                          select s;
            foreach (var shader in shaders)
            {
                if (!shaderCache.Contains(shader))
                {
                    shaderCache.Add(shader);
                    values[InstanceType.Total].ShaderCount++;
                }
            }
            shaderCache = new List<IShader>();
            shaders = from d in uniqueDynamicDrawables
                      from s in d.Shaders
                      select s;
            foreach (var shader in shaders)
            {
                if (!shaderCache.Contains(shader))
                {
                    shaderCache.Add(shader);
                    values[InstanceType.Dynamic].ShaderCount++;
                }
            }
            shaderCache = new List<IShader>();
            shaders = from d in uniqueNonDynamicDrawables
                      from s in d.Shaders
                      select s;
            foreach (var shader in shaders)
            {
                if (!shaderCache.Contains(shader))
                {
                    shaderCache.Add(shader);
                    values[InstanceType.NonDynamic].ShaderCount++;
                }
            }

            // TextureCount
            var textures = (from d in uniqueDrawables
                            from t in d.AssetChildren.OfType<ITexture>()
                            select t.Name).Distinct();
            values[InstanceType.Total].TextureCount = textures.Count();

            textures = (from d in uniqueDynamicDrawables
                        from t in d.AssetChildren.OfType<ITexture>()
                        select t.Name).Distinct();
            values[InstanceType.Dynamic].TextureCount = textures.Count();

            textures = (from d in uniqueNonDynamicDrawables
                        from t in d.AssetChildren.OfType<ITexture>()
                        select t.Name).Distinct();
            values[InstanceType.NonDynamic].TextureCount = textures.Count();

            // Acculumative Counts
            foreach (var drawable in drawables)
            {
                values[InstanceType.Total].DrawableCost += drawable.ExportGeometrySize;
                values[InstanceType.Total].TXDCost += drawable.ExportTxdSize;
                values[InstanceType.Total].PolygonCount += drawable.PolygonCount;
                values[InstanceType.Total].CollisionCount += drawable.CollisionPolygonCount;
                values[InstanceType.Total].MoverCollisionCount += drawable.MoverPolygonCount;
                values[InstanceType.Total].WeaponCollisionCount += drawable.WeaponsPolygonCount;
                values[InstanceType.Total].CameraCollisionCount += drawable.CameraPolygonCount;
                values[InstanceType.Total].RiverCollisionCount += drawable.RiverPolygonCount;
            }
            foreach (var drawable in dynamicDrawables)
            {
                values[InstanceType.Dynamic].DrawableCost += drawable.ExportGeometrySize;
                values[InstanceType.Dynamic].TXDCost += drawable.ExportTxdSize;
                values[InstanceType.Dynamic].PolygonCount += drawable.PolygonCount;
                values[InstanceType.Dynamic].CollisionCount += drawable.CollisionPolygonCount;
                values[InstanceType.Dynamic].MoverCollisionCount += drawable.MoverPolygonCount;
                values[InstanceType.Dynamic].WeaponCollisionCount += drawable.WeaponsPolygonCount;
                values[InstanceType.Dynamic].CameraCollisionCount += drawable.CameraPolygonCount;
                values[InstanceType.Dynamic].RiverCollisionCount += drawable.RiverPolygonCount;
            }
            foreach (var drawable in nonDynamicDrawables)
            {
                values[InstanceType.NonDynamic].DrawableCost += drawable.ExportGeometrySize;
                values[InstanceType.NonDynamic].TXDCost += drawable.ExportTxdSize;
                values[InstanceType.NonDynamic].PolygonCount += drawable.PolygonCount;
                values[InstanceType.NonDynamic].CollisionCount += drawable.CollisionPolygonCount;
                values[InstanceType.NonDynamic].MoverCollisionCount += drawable.MoverPolygonCount;
                values[InstanceType.NonDynamic].WeaponCollisionCount += drawable.WeaponsPolygonCount;
                values[InstanceType.NonDynamic].CameraCollisionCount += drawable.CameraPolygonCount;
                values[InstanceType.NonDynamic].RiverCollisionCount += drawable.RiverPolygonCount;
            }

            // LodDistance
            if (values[InstanceType.Total].Count > 0)
                values[InstanceType.Total].LodDistance = (from d in drawables select d.LodDistance).Max();
            if (values[InstanceType.Dynamic].Count > 0)
                values[InstanceType.Dynamic].LodDistance = (from d in drawables where d.IsDynamic select d.LodDistance).Max();
            if (values[InstanceType.NonDynamic].Count > 0)
                values[InstanceType.NonDynamic].LodDistance = (from d in drawables where !d.IsDynamic select d.LodDistance).Max();
            return values;
        }

        /// <summary>
        /// Goes through the top level objects in the scene and gets all
        /// of the collision polycounts.
        /// </summary>
        private void GetTopLevelCollision(Scene scene)
        {
            foreach (ObjectDef sceneObject in scene.Objects)
            {
                if (sceneObject.IsCollision())
                {
                    if (sceneObject.DontExport())
                        continue;
                    if (sceneObject.DontExportIDE())
                        continue;
                    if (sceneObject.IsDummy())
                        continue;


                }

                if (sceneObject.IsContainer())
                {
                    foreach (SceneXml.ObjectDef childObject in sceneObject.Children)
                    {
                        if (childObject.IsCollision())
                        {
                            if (childObject.DontExport())
                                continue;
                            if (childObject.DontExportIDE())
                                continue;
                            if (childObject.IsDummy())
                                continue;

                            GetCollisionStatistics(childObject);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="collisionObject"></param>
        private void GetCollisionStatistics(ObjectDef collisionObject)
        {
            this.TopLevelCollision += collisionObject.PolyCount;
            string defaultValue = "false";
            bool boolDefaultValue = false;
            bool river = false;
            bool.TryParse(collisionObject.GetUserProperty("river", "false"), out river);
            if (river)
            {
                this.TopLevelRiverCollision += collisionObject.PolyCount;
                defaultValue = "false";
                boolDefaultValue = false;
            }
            else
            {
                defaultValue = "true";
                boolDefaultValue = true;
            }

            bool mover = boolDefaultValue;
            bool.TryParse(collisionObject.GetUserProperty("mover", defaultValue), out mover);
            bool weapons = boolDefaultValue;
            bool.TryParse(collisionObject.GetUserProperty("weapons", defaultValue), out weapons);
            bool camera = boolDefaultValue;
            bool.TryParse(collisionObject.GetUserProperty("camera", defaultValue), out camera);

            if (mover)
                this.TopLevelMoverCollision += collisionObject.PolyCount;
            if (weapons)
                this.TopLevelWeaponCollision += collisionObject.PolyCount;
            if (camera)
                this.TopLevelCameraCollision += collisionObject.PolyCount;
        }
        #endregion
    } // RawSectionStatistics


    internal class TXDNameComparer : IEqualityComparer<string>
    {
        // Summary:
        //     Determines whether the specified objects are equal.
        //
        // Parameters:
        //   x:
        //     The first object of type T to compare.
        //
        //   y:
        //     The second object of type T to compare.
        //
        // Returns:
        //     true if the specified objects are equal; otherwise, false.
        public bool Equals(string x, string y)
        {
            if (string.Compare(x.ToLower(), y.ToLower()) == 0)
                return true;
            return false;
        }

        //
        // Summary:
        //     Returns a hash code for the specified object.
        //
        // Parameters:
        //   obj:
        //     The System.Object for which a hash code is to be returned.
        //
        // Returns:
        //     A hash code for the specified object.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     The type of obj is a reference type and obj is null.
        public int GetHashCode(string obj)
        {
            return obj.ToLower().GetHashCode();
        }
    }

} // RSG.Model.Map.Statistics
