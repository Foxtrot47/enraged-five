﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using RSG.Base.ConfigParser;
using RSG.Base.Tasks;
using RSG.Model.Common;
using RSG.Model.Common.Map;
using RSG.Model.Report;
using RSG.ManagedRage;
using RSG.Platform;
using RSG.Base.Math;
using RSG.SceneXml;

namespace RSG.Model.Report2.Reports.Map
{
    /// <summary>
    /// A report that lists the shadow proxies which have a pivot offset to their drawable hierarchy root mesh.
    /// (url:bugstar:1505411).
    /// </summary>
    public class OffsetProxiesReport : CSVReport, IDynamicLevelReport
    {
        #region Fields
        /// <summary>
        /// The name of this report.
        /// </summary>
        private const string Name = "Offset Reflection & Shadow Proxies";

        /// <summary>
        /// The description for this report.
        /// </summary>
        private const string Description =
            "Exports a csv report that lists the shadow proxies which have a pivot offset to their drawable hierarchy root mesh.";

        /// <summary>
        /// The private field used for the <see cref="GenerationTask"/> property.
        /// </summary>
        private ITask _generationTask;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="OrphanedShadowProxiesReport"/> class.
        /// </summary>
        public OffsetProxiesReport()
            : base(Name, Description)
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the task that represents the work needing to be done to generate this report.
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (this._generationTask == null)
                {
                    CompositeTask task = new CompositeTask("Generate Report");
                    task.AddSubTask(new ActionTask("Loading data", this.EnsureDataLoaded));
                    task.AddSubTask(new ActionTask("Generating report", this.GenerateReport));
                    this._generationTask = task;
                }

                return this._generationTask;
            }
        }

        /// <summary>
        /// Gets a iterator around the stats that are required to generate this report. Always
        /// returns null.
        /// </summary>
        public IEnumerable<StreamableStat> RequiredStats
        {
            get { return null; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Ensures that the data needed for this report is loaded.
        /// </summary>
        /// <param name="context">
        /// The task context for this action.
        /// </param>
        /// <param name="progress">
        /// The object used to notify a listener of the progress.
        /// </param>
        private void EnsureDataLoaded(ITaskContext context, IProgress<TaskProgress> progress)
        {
            DynamicLevelReportContext reportContext = context as DynamicLevelReportContext;
            Debug.Assert(reportContext != null, "Unable to load date for report");
            if (reportContext == null)
            {
                return;
            }

            IMapHierarchy hierarchy = reportContext.Level.MapHierarchy;
            Debug.Assert(hierarchy != null, "Unable to load date for report");
            if (hierarchy == null || hierarchy.AllSections == null)
            {
                return;
            }

            List<IMapSection> mapSections = hierarchy.AllSections.ToList();
            float increment = 1.0f / mapSections.Count;
            string fmt = "Loading {0}";
            foreach (IMapSection section in mapSections)
            {
                context.Token.ThrowIfCancellationRequested();
                string message = String.Format(fmt, section.Name);
                progress.Report(new TaskProgress(increment, true, message));

                section.LoadStats(new List<StreamableStat>() { StreamableMapSectionStat.Archetypes, StreamableMapSectionStat.Entities });
            }
        }

        /// <summary>
        /// Generates the report and saves the file associated with it.
        /// </summary>
        /// <param name="context">
        /// The task context for this action.
        /// </param>
        /// <param name="progress">
        /// The object used to notify a listener of the progress.
        /// </param>
        private void GenerateReport(ITaskContext context, IProgress<TaskProgress> progress)
        {
            DynamicLevelReportContext reportContext = context as DynamicLevelReportContext;
            Debug.Assert(reportContext != null, "Unable to generate report");
            if (reportContext == null)
            {
                return;
            }

            IMapHierarchy hierarchy = reportContext.Level.MapHierarchy;
            Debug.Assert(hierarchy != null, "Unable to generate report");
            if (hierarchy == null || hierarchy.AllSections == null)
            {
                return;
            }

            Dictionary<IMapArchetype, List<IEntity>> offsetEntities = new Dictionary<IMapArchetype, List<IEntity>>();

            try
            {
                List<IMapSection> mapSections = hierarchy.AllSections.ToList();
                float increment = 1.0f / mapSections.Count;
                string fmt = "Processing {0}";
                foreach (IMapSection section in mapSections)
                {
                    context.Token.ThrowIfCancellationRequested();
                    string message = String.Format(fmt, section.Name);
                    progress.Report(new TaskProgress(increment, true, message));
                    if (section == null || section.Archetypes == null)
                    {
                        continue;
                    }

                    foreach (IMapArchetype archetype in section.Archetypes)
                    {
                        if (archetype == null)
                        {
                            continue;
                        }

                        if (archetype.Entities == null || archetype.HasShadowProxies == false || !archetype.SceneLinks.ContainsKey("ShadowMesh"))
                        {
                            continue;
                        }

                        if (archetype.Entities.Count > 1)
                        {
                            Trace.Write(archetype.Name+" has a shadow proxy but multiple entities defined: " + String.Join<IEntity>(",",archetype.Entities));
                            continue;
                        }

                        Vector3f proxyPos = archetype.SceneLinks["ShadowMesh"].Parent.NodeTransform.Translation;
                        Matrix34f proxyTrans = archetype.SceneLinks["ShadowMesh"].Parent.NodeTransform;

                        foreach (IEntity entity in archetype.Entities)
                        {
                            if (entity == null)
                            {
                                continue;
                            }

                            //////////////////////////////////////////////////////////////////////////
                            // actual check for positional inequality
                            //////////////////////////////////////////////////////////////////////////
                            double dist = (entity.Position - proxyPos).Magnitude();
                            Quaternionf rotDiff = new Quaternionf(proxyTrans * entity.Transform.Inverse());
                            if (dist < 0.01 && rotDiff.IsIdentity())
                                continue;

                            List<IEntity> entities = null;
                            if (!offsetEntities.TryGetValue(archetype, out entities))
                            {
                                entities = new List<IEntity>();
                                offsetEntities.Add(archetype, entities);
                            }

                            entities.Add(entity);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                Trace.Write("Error occured during data anlysis: "+ex.StackTrace);
            }

            this.WriteCsvFile(offsetEntities, reportContext.GameView, reportContext.Level.Name);
        }

        /// <summary>
        /// Wtites out the CSV file using the data gathered at the start of the
        /// <see cref="GenerateReport"/> method.
        /// </summary>
        /// <param name="validEntities">
        /// The list containing the valid entities to output to the csv file.
        /// </param>
        private void WriteCsvFile(Dictionary<IMapArchetype, List<IEntity>> offsetEntities, ConfigGameView gv, string levelName)
        {
            using (StreamWriter writer = new StreamWriter(this.Filename))
            {
                writer.WriteLine(
                    "Archetype Name,\t" +
                    "Entity Name,\t" +
                    "Section,\t" +
                    "Proxy Name,\t" +
                    "Entity Position,,,\t" +
                    "Proxy Position,,,\t" +
                    "Entity Rotation,,,,\t" +
                    "Proxy Rotation,,,,");

                WriteCsvData(writer, offsetEntities, gv, levelName);
            }
        }

        /// <summary>
        /// Writes the data out to the specified stream.
        /// </summary>
        /// <param name="writer">
        /// The stream to write the comma separated data into.
        /// </param>
        /// <param name="validEntities">
        /// The list containing the valid entities to output to the csv file.
        /// </param>
        private void WriteCsvData(StreamWriter writer, Dictionary<IMapArchetype, List<IEntity>> offsetEntities, ConfigGameView gv, string levelName)
        {
            Dictionary<string, ArchetypeSizes> archetypeSizes = new Dictionary<string, ArchetypeSizes>();
            char ragebuilderCharacter = RSG.Platform.Platform.PS3.PlatformRagebuilderCharacter();
            string drawableExtension = FileType.Drawable.GetPlatformExtension().Replace('?', ragebuilderCharacter);
            string fragmentExtension = FileType.Fragment.GetPlatformExtension().Replace('?', ragebuilderCharacter);
            string textureExtension = FileType.TextureDictionary.GetPlatformExtension().Replace('?', ragebuilderCharacter);

            foreach (KeyValuePair<IMapArchetype, List<IEntity>> mapPair in offsetEntities)
            {
                if (mapPair.Key == null || mapPair.Value == null)
                {
                    continue;
                }

                IMapArchetype archetype = mapPair.Key;
                TargetObjectDef proxyObj = mapPair.Key.SceneLinks["ShadowMesh"].Parent;
                Vector3f proxyPos = proxyObj.NodeTransform.Translation;
                Quaternionf proxyRot = new Quaternionf(archetype.SceneLinks["ShadowMesh"].Parent.NodeTransform);

                foreach (IEntity entity in mapPair.Value)
                {
                    IMapSection section = entity.Parent as IMapSection;
                    if (section == null)
                    {
                        IRoom room = entity.Parent as IRoom;
                        if (room != null)
                        {
                            IInteriorArchetype interiorArchetype = room.ParentArchetype;
                            if (room.ParentArchetype != null)
                            {
                                section = room.ParentArchetype.ParentSection;
                            }
                        }
                    }

                    string csvRecord = String.Format("{0},\t{1},\t{2},\t{3},\t{4},{5},{6},\t{7},{8},{9},\t{10},{11},{12},{13},\t{14},{15},{16},{17}",
                        archetype.Name, 
                        entity.Name, 
                        section != null ? section.Name : "n/a",
                        proxyObj.Name,
                        entity.Position.X, entity.Position.Y, entity.Position.Z,
                        proxyPos.X,proxyPos.Y,proxyPos.Z,
                        entity.Rotation.X,entity.Rotation.Y,entity.Rotation.Z,entity.Rotation.W,
                        proxyRot.X,proxyRot.Y,proxyRot.Z,proxyRot.W
                        );

                    writer.WriteLine(csvRecord);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private string GetProcesedRpfPath(ConfigGameView gv, string name)
        {
            // Get the content node for this section
            ContentNodeMap contentNode = (ContentNodeMap)gv.Content.Root.FindAll(name, "map").FirstOrDefault();

            if (contentNode == null)
            {
                return null;
            }

            // Get the export node for the section
            ContentNodeMapZip exportNode = contentNode.Outputs.FirstOrDefault() as ContentNodeMapZip;
            if (exportNode == null)
            {
                return null;
            }

            ContentNodeMapProcessedZip processedZip = exportNode.Outputs.FirstOrDefault() as ContentNodeMapProcessedZip;
            if (processedZip == null)
            {
                return null;
            }

            string path = processedZip.Filename;
            if (path == null)
            {
                return null;
            }

            path = Path.ChangeExtension(path, ".rpf");
            path = Path.GetFullPath(path);
            return path.Replace(gv.ProcessedDir, Path.Combine(gv.BuildDir, "ps3"));
        }
        #endregion Methods

        #region Structures
        private struct ArchetypeSizes
        {
            public ulong VirtualGeometry { get; set; }

            public ulong VirtualTexture { get; set; }

            public ulong PhysicalGeometry { get; set; }

            public ulong PhysicalTexture { get; set; }
        }
        #endregion Structures
    }
}
