﻿// --------------------------------------------------------------------------------------------
// <copyright file="PointerMember.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions.Members
{
    using System;
    using System.Xml;
    using RSG.Base;
    using RSG.Base.Extensions;
    using RSG.Base.Logging;
    using RSG.Editor;
    using RSG.Editor.Model;
    using RSG.Metadata.Model.Resources;
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// Represents the &lt;pointer&gt; member node in the parCodeGen system that can be
    /// instanced in a metadata file.
    /// </summary>
    public class PointerMember : MemberBase, IEquatable<PointerMember>
    {
        #region Fields
        /// <summary>
        /// The string representation of the <see cref="PointerPolicy.External"/> constant.
        /// </summary>
        private const string PointerPolicyExternal = "external_named";

        /// <summary>
        /// The string representation of the <see cref="PointerPolicy.LazyLink"/> constant.
        /// </summary>
        private const string PointerPolicyLazyLink = "lazylink";

        /// <summary>
        /// The string representation of the <see cref="PointerPolicy.Link"/> constant.
        /// </summary>
        private const string PointerPolicyLink = "link";

        /// <summary>
        /// The string representation of the <see cref="PointerPolicy.Owner"/> constant.
        /// </summary>
        private const string PointerPolicyOwner = "owner";

        /// <summary>
        /// The string representation of the <see cref="PointerPolicy.SimpleOwner"/>
        /// constant.
        /// </summary>
        private const string PointerPolicySimpleOwner = "simple_owner";

        /// <summary>
        /// The name of the xml attribute containing the data for the
        /// <see cref="FromStringMethod"/> property.
        /// </summary>
        private const string XmlFromStringAttr = "fromString";

        /// <summary>
        /// The name of the xml attribute containing the data for the
        /// <see cref="AddGroupWidgets"/> property.
        /// </summary>
        private const string XmlGroupWidgetsAttr = "addGroupWidget";

        /// <summary>
        /// The name of the xml attribute containing the data for the
        /// <see cref="OnUnknownType"/> property.
        /// </summary>
        private const string XmlOnUnknownTypeAttr = "onUnknownType";

        /// <summary>
        /// The name of the xml attribute containing the data for the <see cref="Policy"/>
        /// property.
        /// </summary>
        private const string XmlPolicyAttr = "policy";

        /// <summary>
        /// The name of the xml attribute containing the data for the
        /// <see cref="ToStringMethod"/> property.
        /// </summary>
        private const string XmlToStringAttr = "toString";

        /// <summary>
        /// The name of the xml attribute containing the data for the <see cref="Type"/>
        /// property.
        /// </summary>
        private const string XmlTypeAttr = "type";

        /// <summary>
        /// The name of the xml attribute containing the data for the
        /// <see cref="UserHandlesNull"/> property.
        /// </summary>
        private const string XmlUserHandlesNullAttr = "userHandlesNull";

        /// <summary>
        /// The private field used for the <see cref="AddGroupWidgets"/> property.
        /// </summary>
        private string _addGroupWidgets;

        /// <summary>
        /// The private field used for the <see cref="FromStringMethod"/> property.
        /// </summary>
        private string _fromString;

        /// <summary>
        /// The private field used for the <see cref="OnUnknownType"/> property.
        /// </summary>
        private string _onUnknownType;

        /// <summary>
        /// The private field used for the <see cref="Policy"/> property.
        /// </summary>
        private string _policy;

        /// <summary>
        /// The private field used for the <see cref="ToStringMethod"/> property.
        /// </summary>
        private string _toString;

        /// <summary>
        /// The private field used for the <see cref="Type"/> property.
        /// </summary>
        private string _type;

        /// <summary>
        /// The private field used for the <see cref="UserHandlesNull"/> property.
        /// </summary>
        private string _userHandlesNull;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="PointerMember"/>
        /// class to be one of the members of the specified structure.
        /// </summary>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        public PointerMember(IStructure structure)
            : base(structure)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="PointerMember"/>
        /// class as a copy of the specified instance.
        /// </summary>
        /// <param name="other">
        /// The instance to copy.
        /// </param>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        public PointerMember(PointerMember other, IStructure structure)
            : base(other, structure)
        {
            this._addGroupWidgets = other._addGroupWidgets;
            this._policy = other._policy;
            this._onUnknownType = other._onUnknownType;
            this._toString = other._toString;
            this._fromString = other._fromString;
            this._type = other._type;
            this._userHandlesNull = other._userHandlesNull;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="PointerMember"/>
        /// class.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        public PointerMember(XmlReader reader, IStructure structure)
            : base(reader, structure)
        {
            this.Deserialise(reader);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether the child widgets should be displayed.
        /// </summary>
        public bool AddGroupWidgets
        {
            get { return this.Dictionary.To<bool>(this._addGroupWidgets, true); }
            set { this.SetProperty(ref this._addGroupWidgets, value.ToString()); }
        }

        /// <summary>
        /// Gets a value indicating whether or not, based on the pointer policy a instance of
        /// this member can point to a object of a dervied type or null as well as the
        /// <see cref="ReferencedStructure"/>.
        /// </summary>
        public bool CanBeDerivedType
        {
            get
            {
                if (this.Policy == PointerPolicy.Owner)
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// Gets or sets the call back that is run to convert between pointer and string
        /// on saving.
        /// </summary>
        public string FromStringMethod
        {
            get { return this._fromString; }
            set { this.SetProperty(ref this._fromString, value); }
        }

        /// <summary>
        /// Gets or sets the call back that is run if when being loaded the type that a
        /// instance points to isn't known to the Parser.
        /// </summary>
        public string OnUnknownType
        {
            get { return this._onUnknownType; }
            set { this.SetProperty(ref this._onUnknownType, value); }
        }

        /// <summary>
        /// Gets a value indicating whether or not, based on the pointer policy a instance
        /// of this member owns the object that it points to or not.
        /// </summary>
        public bool OwnsObject
        {
            get
            {
                PointerPolicy policy = this.Policy;
                if (policy == PointerPolicy.Owner || policy == PointerPolicy.SimpleOwner)
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// Gets or sets the policy that this pointer member has to adhere to.
        /// </summary>
        public PointerPolicy Policy
        {
            get { return this.GetPolicyFromString(this._policy); }
            set { this.SetProperty(ref this._policy, this.GetStringFromPolicy(value)); }
        }

        /// <summary>
        /// Gets the structure definition that this member represents.
        /// </summary>
        public IStructure ReferencedStructure
        {
            get { return this.GetStructureType(this.Structure.Dictionary); }
        }

        /// <summary>
        /// Gets or sets the call back that is run to convert between string and pointer
        /// on loading.
        /// </summary>
        public string ToStringMethod
        {
            get { return this._toString; }
            set { this.SetProperty(ref this._toString, value); }
        }

        /// <summary>
        /// Gets or sets the data type for the structure that this member represents.
        /// </summary>
        public string Type
        {
            get
            {
                return this._type;
            }

            set
            {
                this.SetProperty(ref this._type, value.ToString(), "Type", "EntityStructure");
            }
        }

        /// <summary>
        /// Gets the type name for this member.
        /// </summary>
        public override string TypeName
        {
            get { return "pointer"; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the Null values for external_named pointers
        /// be passed on to the call back when loading.
        /// </summary>
        public bool UserHandlesNull
        {
            get { return this.Dictionary.To<bool>(this._userHandlesNull, false); }
            set { this.SetProperty(ref this._userHandlesNull, value.ToString()); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new <see cref="PointerMember"/> that is a copy of this instance.
        /// </summary>
        /// <returns>
        /// A new <see cref="PointerMember"/> that is a copy of this instance.
        /// </returns>
        public new PointerMember Clone()
        {
            return new PointerMember(this, this.Structure);
        }

        /// <summary>
        /// Creates a <see cref="ITunable"/> object that represents a instance of this member
        /// with it's default value set.
        /// </summary>
        /// <param name="parent">
        /// The tunable parent the new tunable will belong to.
        /// </param>
        /// <returns>
        /// A parCodeGen tunable that represents a instance of this member.
        /// </returns>
        public override ITunable CreateTunable(ITunableParent parent)
        {
            return new PointerTunable(this, parent);
        }

        /// <summary>
        /// Creates a <see cref="ITunable"/> object that represents a instance of this member
        /// using the specified Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The Xml.XmlReader that provides the data used to initialise the new tunable.
        /// </param>
        /// <param name="parent">
        /// The tunable parent the new tunable will belong to.
        /// </param>
        /// <param name="log">
        /// A object that is used to log any warnings or errors produced during the parsing of
        /// the specified reader.
        /// </param>
        /// <returns>
        /// A parCodeGen tunable that represents a instance of this member.
        /// </returns>
        public override ITunable CreateTunable(
            XmlReader reader, ITunableParent parent, ILog log)
        {
            return new PointerTunable(reader, this, parent, log);
        }

        /// <summary>
        /// Creates a deep copy of the current instance.
        /// </summary>
        /// <returns>
        /// A deep copy of the current System.Object.
        /// </returns>
        public override object DeepClone()
        {
            return this.Clone();
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified
        /// <see cref="PointerMember"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public bool Equals(PointerMember other)
        {
            if (other == null)
            {
                return false;
            }

            return object.ReferenceEquals(other, this);
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="IMember"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public override bool Equals(IMember other)
        {
            return this.Equals(other as PointerMember);
        }

        /// <summary>
        /// Writes the xml representation of this instance onto the specified
        /// System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that the xml representation of this instance should be
        /// written to.
        /// </param>
        public override void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                throw new SmartArgumentNullException(() => writer);
            }

            this.SerialiseBaseAttributes(writer);
            if (this._type != null)
            {
                writer.WriteAttributeString(XmlTypeAttr, this._type);
            }

            if (this._policy != null)
            {
                writer.WriteAttributeString(XmlPolicyAttr, this._policy);
            }

            if (this._onUnknownType != null)
            {
                writer.WriteAttributeString(XmlOnUnknownTypeAttr, this._onUnknownType);
            }

            if (this._toString != null)
            {
                writer.WriteAttributeString(XmlToStringAttr, this._toString);
            }

            if (this._fromString != null)
            {
                writer.WriteAttributeString(XmlFromStringAttr, this._fromString);
            }

            if (this._userHandlesNull != null)
            {
                writer.WriteAttributeString(XmlUserHandlesNullAttr, this._userHandlesNull);
            }

            if (this._addGroupWidgets != null)
            {
                writer.WriteAttributeString(XmlGroupWidgetsAttr, this._addGroupWidgets);
            }
        }

        /// <summary>
        /// Validates this entity and creates a validation result object containing the errors
        /// and warnings.
        /// </summary>
        /// <param name="recursive">
        /// A value indicating whether any child models to this one should also be validated.
        /// </param>
        /// <returns>
        /// A validation result object containing all of the errors and warnings associated
        /// with this validation pass.
        /// </returns>
        public override ValidationResult Validate(bool recursive)
        {
            ValidationResult result = new ValidationResult();
            this.ValidateBaseProperties(result);

            if (this._type == null)
            {
                string msg = StringTable.MissingRequiredAttributeError;
                msg = msg.FormatCurrent("type", "pointer");
                result.AddError(msg, this.Location);
            }
            else if (String.IsNullOrWhiteSpace(this._type))
            {
                string msg = StringTable.EmptyRequiredAttributeError;
                msg = msg.FormatCurrent("type", "pointer");
                result.AddError(msg, this.Location);
            }

            if (this.Policy == PointerPolicy.Unknown)
            {
                string msg = StringTable.MissingRequiredAttributeError;
                msg = msg.FormatCurrent("policy", "pointer");
                result.AddError(msg, this.Location);
            }
            else if (this.Policy == PointerPolicy.Unrecognised)
            {
                string msg = StringTable.UnrecognisedRequiredAttributeError;
                msg = msg.FormatCurrent("policy", "pointer", this._policy);
                result.AddError(msg, this.Location);
            }

            if (this.ReferencedStructure == null)
            {
                string msg = StringTable.UnresolvedStructureError;
                msg = msg.FormatCurrent(this._type, "pointer");
                result.AddError(msg, this.Location);
            }

            return result;
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            this._type = reader.GetAttribute(XmlTypeAttr);
            this._policy = reader.GetAttribute(XmlPolicyAttr);
            this._onUnknownType = reader.GetAttribute(XmlOnUnknownTypeAttr);
            this._toString = reader.GetAttribute(XmlToStringAttr);
            this._fromString = reader.GetAttribute(XmlFromStringAttr);
            this._userHandlesNull = reader.GetAttribute(XmlUserHandlesNullAttr);
            this._addGroupWidgets = reader.GetAttribute(XmlGroupWidgetsAttr);
            reader.Skip();
        }

        /// <summary>
        /// Gets the pointer policy that is equivalent to the specified String.
        /// </summary>
        /// <param name="type">
        /// The string to determine the policy to return.
        /// </param>
        /// <returns>
        /// The policy that is equivalent to the specified String.
        /// </returns>
        private PointerPolicy GetPolicyFromString(string type)
        {
            if (type == null)
            {
                return PointerPolicy.Unknown;
            }
            else if (String.Equals(type, PointerPolicyOwner))
            {
                return PointerPolicy.Owner;
            }
            else if (String.Equals(type, PointerPolicySimpleOwner))
            {
                return PointerPolicy.SimpleOwner;
            }
            else if (String.Equals(type, PointerPolicyExternal))
            {
                return PointerPolicy.External;
            }
            else if (String.Equals(type, PointerPolicyLink))
            {
                return PointerPolicy.Link;
            }
            else if (String.Equals(type, PointerPolicyLazyLink))
            {
                return PointerPolicy.LazyLink;
            }
            else
            {
                return PointerPolicy.Unrecognised;
            }
        }

        /// <summary>
        /// Gets the string value that is equivalent to the specified policy.
        /// </summary>
        /// <param name="policy">
        /// The policy that the returned string will be equivalent to.
        /// </param>
        /// <returns>
        /// The string that is equivalent to the specified policy.
        /// </returns>
        private string GetStringFromPolicy(PointerPolicy policy)
        {
            switch (policy)
            {
                case PointerPolicy.Owner:
                    return PointerPolicyOwner;
                case PointerPolicy.SimpleOwner:
                    return PointerPolicySimpleOwner;
                case PointerPolicy.External:
                    return PointerPolicyExternal;
                case PointerPolicy.Link:
                    return PointerPolicyLink;
                case PointerPolicy.LazyLink:
                    return PointerPolicyLazyLink;
                case PointerPolicy.Unknown:
                case PointerPolicy.Unrecognised:
                default:
                    return null;
            }
        }

        /// <summary>
        /// Looks through the specified scope and returns the structure that this pointer
        /// member points to.
        /// </summary>
        /// <param name="scope">
        /// A definition dictionary containing all the other types in the search scope.
        /// </param>
        /// <returns>
        /// This pointer members represented structure.
        /// </returns>
        private IStructure GetStructureType(IDefinitionDictionary scope)
        {
            if (this._type == null || scope == null)
            {
                return null;
            }

            return scope.GetStructure(this._type);
        }
        #endregion Methods
    } // RSG.Metadata.Model.Definitions.Members.PointerMember {Class}
} // RSG.Metadata.Model.Definitions.Members {Namespace}
