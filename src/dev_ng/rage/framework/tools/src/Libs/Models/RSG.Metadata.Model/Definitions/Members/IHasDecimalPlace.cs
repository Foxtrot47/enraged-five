﻿// --------------------------------------------------------------------------------------------
// <copyright file="IHasDecimalPlace.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions.Members
{
    /// <summary>
    /// When implemented represents a member that has a decimal places property.
    /// </summary>
    public interface IHasDecimalPlace : IMember
    {
        #region Properties
        /// <summary>
        /// Gets or sets the decimal place count the value of an instance to this member has.
        /// </summary>
        short DecimalPlaces { get; set; }
        #endregion Properties
    } // RSG.Metadata.Model.Definitions.Members.IHasDecimalPlace {Interface}
} // RSG.Metadata.Model.Definitions.Members {Namespace}
