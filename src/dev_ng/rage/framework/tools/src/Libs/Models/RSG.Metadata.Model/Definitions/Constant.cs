﻿//---------------------------------------------------------------------------------------------
// <copyright file="Constant.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions
{
    using System;
    using System.IO;
    using System.Xml;
    using RSG.Editor;
    using RSG.Editor.Model;

    /// <summary>
    /// Defines a parsable constant that can be used by the parCodeGen system. This class
    /// cannot be inherited.
    /// </summary>
    internal class Constant : ModelBase, IConstant
    {
        #region Fields
        /// <summary>
        /// The name of the xml attribute used for the <see cref="Name"/> property.
        /// </summary>
        protected const string XmlNameAttr = "name";

        /// <summary>
        /// The name of the xml attribute used for the <see cref="Value"/> property.
        /// </summary>
        protected const string XmlValueAttr = "value";

        /// <summary>
        /// The private field used for the <see cref="Dictionary"/> property.
        /// </summary>
        private IDefinitionDictionary _dictionary;

        /// <summary>
        /// The private field used for the <see cref="Filename"/> property.
        /// </summary>
        private string _filename;

        /// <summary>
        /// The private field used for the <see cref="LineNumber"/> property.
        /// </summary>
        private int _lineNumber;

        /// <summary>
        /// The private field used for the <see cref="LinePosition"/> property.
        /// </summary>
        private int _linePosition;

        /// <summary>
        /// The private field used for the <see cref="Name"/> property.
        /// </summary>
        private string _name;

        /// <summary>
        /// The private field used for the <see cref="Value"/> property.
        /// </summary>
        private string _value;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="Constant"/> class.
        /// </summary>
        /// <param name="dictionary">
        /// The dictionary that this constant will be associated with.
        /// </param>
        public Constant(IDefinitionDictionary dictionary)
        {
            if (dictionary == null)
            {
                throw new SmartArgumentNullException(() => dictionary);
            }

            this._dictionary = dictionary;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Constant"/> class with the specified
        /// data type.
        /// </summary>
        /// <param name="name">
        /// The name for this constant that can be used to reference it.
        /// </param>
        /// <param name="dictionary">
        /// The dictionary that this constant will be associated with.
        /// </param>
        public Constant(string name, IDefinitionDictionary dictionary)
        {
            if (dictionary == null)
            {
                throw new SmartArgumentNullException(() => dictionary);
            }

            this._name = name;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Constant"/> class with the specified
        /// data type and name.
        /// </summary>
        /// <param name="name">
        /// The name for this constant that can be used to reference it.
        /// </param>
        /// <param name="value">
        /// The value as a string this constant has.
        /// </param>
        /// <param name="dictionary">
        /// The dictionary that this constant will be associated with.
        /// </param>
        public Constant(string name, string value, IDefinitionDictionary dictionary)
        {
            if (dictionary == null)
            {
                throw new SmartArgumentNullException(() => dictionary);
            }

            this._name = name;
            this._value = value;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Constant"/> class as a copy of the
        /// specified instance.
        /// </summary>
        /// <param name="other">
        /// The instance to copy.
        /// </param>
        /// <param name="dictionary">
        /// The dictionary that this constant will be associated with.
        /// </param>
        public Constant(Constant other, IDefinitionDictionary dictionary)
        {
            if (dictionary == null)
            {
                throw new SmartArgumentNullException(() => dictionary);
            }

            this._name = other._name;
            this._value = other._value;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Constant"/> class using the specified
        /// System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        /// <param name="dictionary">
        /// The dictionary that this constant will be associated with.
        /// </param>
        public Constant(XmlReader reader, IDefinitionDictionary dictionary)
        {
            if (dictionary == null)
            {
                throw new SmartArgumentNullException(() => dictionary);
            }

            if (reader == null)
            {
                throw new SmartArgumentNullException(() => reader);
            }

            this._dictionary = dictionary;
            IXmlLineInfo info = reader as IXmlLineInfo;
            if (info != null)
            {
                this._lineNumber = info.LineNumber;
                this._linePosition = info.LinePosition;
            }
            else
            {
                this._lineNumber = -1;
                this._linePosition = -1;
            }

            Uri uri = new Uri(reader.BaseURI);
            this._filename = Path.GetFullPath(uri.LocalPath);

            this.Deserialise(reader);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the dictionary this constant is defined inside.
        /// </summary>
        public IDefinitionDictionary Dictionary
        {
            get { return this._dictionary; }
        }

        /// <summary>
        /// Gets or sets the filename this instance is defined in or null if not applicable.
        /// </summary>
        public string Filename
        {
            get
            {
                return this._filename;
            }

            set
            {
                string[] changedProperties = new string[]
                        {
                            "Filename",
                            "Location"
                        };

                this.SetProperty(ref this._filename, value, changedProperties);
            }
        }

        /// <summary>
        /// Gets or sets the line number this instance is defined on or -1 if not available.
        /// </summary>
        public int LineNumber
        {
            get
            {
                return this._lineNumber;
            }

            set
            {
                string[] changedProperties = new string[]
                        {
                            "LineNumber",
                            "Location"
                        };

                this.SetProperty(ref this._lineNumber, value, changedProperties);
            }
        }

        /// <summary>
        /// Gets or sets the line position this instance is defined at or -1 if not available.
        /// </summary>
        public int LinePosition
        {
            get
            {
                return this._linePosition;
            }

            set
            {
                string[] changedProperties = new string[]
                        {
                            "LinePosition",
                            "Location"
                        };

                this.SetProperty(ref this._linePosition, value, changedProperties);
            }
        }

        /// <summary>
        /// Gets the location of this constant inside the parCodeGen definition data sat on the
        /// local disk.
        /// </summary>
        public FileLocation Location
        {
            get { return new FileLocation(this.LineNumber, this.LinePosition, this.Filename); }
        }

        /// <summary>
        /// Gets or sets the name that this constant can be referenced by.
        /// </summary>
        public string Name
        {
            get { return this._name; }
            set { this.SetProperty(ref this._name, value); }
        }

        /// <summary>
        /// Gets or sets the value of this constant as a String.
        /// </summary>
        public string Value
        {
            get { return this._value; }
            set { this.SetProperty(ref this._value, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new <see cref="Constant"/> that is a copy of this instance.
        /// </summary>
        /// <returns>
        /// A new <see cref="Constant"/> that is a copy of this instance.
        /// </returns>
        public new Constant Clone()
        {
            return new Constant(this, this.Dictionary);
        }

        /// <summary>
        /// Creates a deep copy of the current instance.
        /// </summary>
        /// <returns>
        /// A deep copy of the current System.Object.
        /// </returns>
        public override object DeepClone()
        {
            return this.Clone();
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="Structure"/>.
        /// </summary>
        /// <param name="other">
        /// A constant to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public bool Equals(Constant other)
        {
            if (other == null)
            {
                return false;
            }

            if (String.Equals(this.Name, other.Name))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="IConstant"/>.
        /// </summary>
        /// <param name="other">
        /// A <see cref="IStructure"/> to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public bool Equals(IConstant other)
        {
            return this.Equals(other as Constant);
        }

        /// <summary>
        /// Creates a new <see cref="IConstant"/> that is a copy of this instance.
        /// </summary>
        /// <returns>
        /// A new <see cref="IConstant"/> that is a copy of this instance.
        /// </returns>
        IConstant IConstant.Clone()
        {
            return this.Clone();
        }

        /// <summary>
        /// Writes the xml representation of this instance onto the specified
        /// System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that the xml representation of this instance should be
        /// written to.
        /// </param>
        /// <param name="saveFileInfo">
        /// If true the file, line number and line position will be written out as attributes.
        /// </param>
        public void Serialise(XmlWriter writer, bool saveFileInfo)
        {
            if (writer == null)
            {
                throw new SmartArgumentNullException(() => writer);
            }

            if (this._name != null)
            {
                writer.WriteAttributeString(XmlNameAttr, this._name);
            }

            if (this._value != null)
            {
                writer.WriteAttributeString(XmlValueAttr, this._value);
            }
        }

        /// <summary>
        /// Initialises this instance using the specified System.Xml.XmlReader as a data
        /// provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader used as the data provider to initialise this instance
        /// with.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            this._name = reader.GetAttribute(XmlNameAttr);
            this._value = reader.GetAttribute(XmlValueAttr);
            reader.Skip();
        }
        #endregion Methods
    } // RSG.Metadata.Model.Definitions.Constant {Class}
} // RSG.Metadata.Model.Definitions {Namespace}
