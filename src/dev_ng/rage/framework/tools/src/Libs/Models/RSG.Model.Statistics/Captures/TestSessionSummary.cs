﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace RSG.Model.Statistics.Captures
{
    /// <summary>
    /// Summary information aggregated from all the test results that make up a test session.
    /// </summary>
    [DataContract]
    public class TestSessionSummary
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public SummaryInfo FpsInfo { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public List<CpuSummaryInfo> CpuInfo { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public List<ThreadSummaryInfo> ThreadInfo { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public List<DrawListSummaryInfo> DrawListInfo { get; set; }
        #endregion //Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public TestSessionSummary()
        {
            CpuInfo = new List<CpuSummaryInfo>();
            ThreadInfo = new List<ThreadSummaryInfo>();
            DrawListInfo = new List<DrawListSummaryInfo>();
        }
        #endregion // Constructor(s)
    } // TestSessionSummary

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class SummaryInfo
    {
        /// <summary>
        /// Minimum value encountered.
        /// </summary>
        [DataMember]
        public float MinMin { get; set; }

        /// <summary>
        /// Maximum value encountered.
        /// </summary>
        [DataMember]
        public float MaxMin { get; set; }

        /// <summary>
        /// Average of all the values.
        /// </summary>
        [DataMember]
        public double AverageMin { get; set; }

        /// <summary>
        /// Minimum value encountered.
        /// </summary>
        [DataMember]
        public float MinAverage { get; set; }

        /// <summary>
        /// Maximum value encountered.
        /// </summary>
        [DataMember]
        public float MaxAverage { get; set; }

        /// <summary>
        /// Average of all the values.
        /// </summary>
        [DataMember]
        public double AverageAverage { get; set; }

        /// <summary>
        /// Minimum value encountered.
        /// </summary>
        [DataMember]
        public float MinMax { get; set; }

        /// <summary>
        /// Maximum value encountered.
        /// </summary>
        [DataMember]
        public float MaxMax { get; set; }

        /// <summary>
        /// Average of all the values.
        /// </summary>
        [DataMember]
        public double AverageMax { get; set; }
    } // SummaryInfo

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class CpuSummaryInfo : SummaryInfo
    {
        /// <summary>
        /// Name of the set.
        /// </summary>
        [DataMember]
        public String Set { get; set; }

        /// <summary>
        /// Name of the metric.
        /// </summary>
        [DataMember]
        public String Name { get; set; }
    } // CpuSummaryInfo

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class ThreadSummaryInfo : SummaryInfo
    {
        /// <summary>
        /// Name of the thread.
        /// </summary>
        [DataMember]
        public String Name { get; set; }
    } // ThreadSummaryInfo

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class DrawListSummaryInfo : SummaryInfo
    {
        /// <summary>
        /// Name of the draw list.
        /// </summary>
        [DataMember]
        public String Name { get; set; }
    } // DrawListSummaryInfo
}
