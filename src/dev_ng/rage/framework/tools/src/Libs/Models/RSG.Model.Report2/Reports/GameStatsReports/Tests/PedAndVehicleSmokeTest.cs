﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using RSG.Base.Extensions;
using RSG.Model.Statistics.Captures;
using System.IO;
using System.Windows.Controls.DataVisualization.Charting;
using System.Windows.Media;
using System.Windows.Data;
using System.Windows;
using System.Windows.Media.Imaging;

namespace RSG.Model.Report.Reports.GameStatsReports.Tests
{
    public class PedAndVehicleSmokeTest : SmokeTestBase
    {
        #region Constants

        private const string NAME = "Peds and Vehicles";

        #endregion

        #region Properties

        /// <summary>
        /// Test name -> list of memory results for the latest build
        /// </summary>
        public IDictionary<string, IList<PedAndVehicleResult>> LatestStats
        {
            get;
            private set;
        }

        #endregion

        public PedAndVehicleSmokeTest()
            : base(NAME)
        {

        }

        /// <summary>
        /// Executes the particular test
        /// </summary>
        /// <param name="testSessions">List of test sessions</param>
        public override void RunTest(IList<TestSession> testSessions)
        {
            base.RunTest(testSessions);

            LatestStats = GatherLatestBuildStats(testSessions.FirstOrDefault());
        }

        public override IList<GraphImage> GenerateGraphs(DateTime start, DateTime end)
        {
            IList<GraphImage> allGraphs = new List<GraphImage>();

            // Generate a couple of graphs for each  test
            foreach (KeyValuePair<string, IList<PedAndVehicleResult>> pair in LatestStats)
            {
                string testName = pair.Key;

                // Create the pie graph
                Stream imageStream = GenerateColumnGraph(testName, pair.Value);
                string graphName = String.Format("memory_column_{0}", testName.Replace("(", "").Replace(")", "").Replace(" ", ""));
                GraphImage image = new GraphImage(graphName, imageStream);

                if (!PerTestGraphs.ContainsKey(testName))
                {
                    PerTestGraphs.Add(testName, new List<GraphImage>());
                }

                PerTestGraphs[testName].Add(image);
                allGraphs.Add(image);
            }

            return allGraphs;
        }

        #region Private helper methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        private IDictionary<string, IList<PedAndVehicleResult>> GatherLatestBuildStats(TestSession session)
        {
            IDictionary<string, IList<PedAndVehicleResult>> latestStats = new Dictionary<string, IList<PedAndVehicleResult>>();

            if (session != null)
            {
                foreach (TestResults test in session.TestResults)
                {
                    if (!latestStats.ContainsKey(test.Name))
                    {
                        latestStats.Add(test.Name, new List<PedAndVehicleResult>());
                        latestStats[test.Name].Add(test.PedAndVehicleResult);
                    }
                }
            }

            return latestStats;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="testName"></param>
        /// <param name="setName"></param>
        /// <param name="stats"></param>
        /// <returns></returns>
        private Stream GenerateColumnGraph(string testName, IList<PedAndVehicleResult> stats)
        {
            Chart chart = new Chart();
            chart.BeginInit();
            chart.Width = 1280;
            chart.Height = 600;
            chart.Title = String.Format("Average Pedestrian and Vehicle Metrics for '{0}'", testName);
            chart.LegendTitle = "Individual Metrics";
            chart.EndInit();

            RenderTargetBitmap renderTarget = new RenderTargetBitmap((int)chart.Width, (int)chart.Height, 96d, 96d, PixelFormats.Default);

            ColumnSeries gvSeries = new ColumnSeries
            {
                Title = "Peds",
                ItemsSource = stats,
                IndependentValueBinding = new Binding("Name"),
                DependentValueBinding = new Binding("NumPeds.Average")
            };
            chart.Series.Add(gvSeries);

            ColumnSeries gpSeries = new ColumnSeries
            {
                Title = "Active Peds",
                ItemsSource = stats,
                IndependentValueBinding = new Binding("Name"),
                DependentValueBinding = new Binding("NumActivePeds.Average")
            };
            chart.Series.Add(gpSeries);

            ColumnSeries rvSeries = new ColumnSeries
            {
                Title = "Inactive Peds",
                ItemsSource = stats,
                IndependentValueBinding = new Binding("Name"),
                DependentValueBinding = new Binding("NumInactivePeds.Average")
            };
            chart.Series.Add(rvSeries);

            ColumnSeries rpSeries = new ColumnSeries
            {
                Title = "Event Scan Hi Peds",
                ItemsSource = stats,
                IndependentValueBinding = new Binding("Name"),
                DependentValueBinding = new Binding("NumEventScanHiPeds.Average")
            };
            chart.Series.Add(rpSeries);

            ColumnSeries rtSeries = new ColumnSeries
            {
                Title = "Event Scan Lo Peds",
                ItemsSource = stats,
                IndependentValueBinding = new Binding("Name"),
                DependentValueBinding = new Binding("NumEventScanLoPeds.Average")
            };
            chart.Series.Add(rtSeries);

            ColumnSeries rzSeries = new ColumnSeries
            {
                Title = "Motion Task Hi Peds",
                ItemsSource = stats,
                IndependentValueBinding = new Binding("Name"),
                DependentValueBinding = new Binding("NumMotionTaskHiPeds.Average")
            };
            chart.Series.Add(rzSeries);

            ColumnSeries rySeries = new ColumnSeries
            {
                Title = "Motion Task Lo Peds",
                ItemsSource = stats,
                IndependentValueBinding = new Binding("Name"),
                DependentValueBinding = new Binding("NumMotionTaskLoPeds.Average")
            };
            chart.Series.Add(rzSeries);

            ColumnSeries rkSeries = new ColumnSeries
            {
                Title = "Physics Hi Peds",
                ItemsSource = stats,
                IndependentValueBinding = new Binding("Name"),
                DependentValueBinding = new Binding("NumPhysicsHiPeds.Average")
            };
            chart.Series.Add(rkSeries);

            ColumnSeries rrSeries = new ColumnSeries
            {
                Title = "Physics Lo Peds",
                ItemsSource = stats,
                IndependentValueBinding = new Binding("Name"),
                DependentValueBinding = new Binding("NumPhysicsLoPeds.Average")
            };
            chart.Series.Add(rrSeries);

            ColumnSeries hiSeries = new ColumnSeries
            {
                Title = "Entity Scan Hi Peds",
                ItemsSource = stats,
                IndependentValueBinding = new Binding("Name"),
                DependentValueBinding = new Binding("NumEntityScanHiPeds.Average")
            };
            chart.Series.Add(hiSeries);

            ColumnSeries loSeries = new ColumnSeries
            {
                Title = "Entity Scan Lo Peds",
                ItemsSource = stats,
                IndependentValueBinding = new Binding("Name"),
                DependentValueBinding = new Binding("NumEntityScanLoPeds.Average")
            };
            chart.Series.Add(loSeries);

            ColumnSeries vehSeries = new ColumnSeries
            {
                Title = "Vehicles",
                ItemsSource = stats,
                IndependentValueBinding = new Binding("Name"),
                DependentValueBinding = new Binding("NumVehicles.Average")
            };
            chart.Series.Add(vehSeries);

            ColumnSeries vehActSeries = new ColumnSeries
            {
                Title = "Active Vehicles",
                ItemsSource = stats,
                IndependentValueBinding = new Binding("Name"),
                DependentValueBinding = new Binding("NumActiveVehicles.Average")
            };
            chart.Series.Add(vehActSeries);

            ColumnSeries vehInActSeries = new ColumnSeries
            {
                Title = "Inactive Vehicles",
                ItemsSource = stats,
                IndependentValueBinding = new Binding("Name"),
                DependentValueBinding = new Binding("NumInactiveVehicles.Average")
            };
            chart.Series.Add(vehInActSeries);

            ColumnSeries vehRealSeries = new ColumnSeries
            {
                Title = "Real Vehicles",
                ItemsSource = stats,
                IndependentValueBinding = new Binding("Name"),
                DependentValueBinding = new Binding("NumRealVehicles.Average")
            };
            chart.Series.Add(vehRealSeries);

            ColumnSeries vehDumSeries = new ColumnSeries
            {
                Title = "Dummy Vehicles",
                ItemsSource = stats,
                IndependentValueBinding = new Binding("Name"),
                DependentValueBinding = new Binding("NumDummyVehicles.Average")
            };
            chart.Series.Add(vehDumSeries);

            ColumnSeries vehSupDumSeries = new ColumnSeries
            {
                Title = "Super Dummy Vehicles",
                ItemsSource = stats,
                IndependentValueBinding = new Binding("Name"),
                DependentValueBinding = new Binding("NumSuperDummyVehicles.Average")
            };
            chart.Series.Add(vehSupDumSeries);

            ColumnSeries vehNetSeries = new ColumnSeries
            {
                Title = "Network Dummy Vehicles",
                ItemsSource = stats,
                IndependentValueBinding = new Binding("Name"),
                DependentValueBinding = new Binding("NumNetworkDummyVehicles.Average")
            };
            chart.Series.Add(vehNetSeries);

            CategoryAxis xAxis = new CategoryAxis
            {
                Orientation = AxisOrientation.X,
                Title = "Ped/Vehicle"
            };
            chart.Axes.Add(xAxis);

            LinearAxis yAxis = new LinearAxis
            {
                Orientation = AxisOrientation.Y,
                Title = "Number",
                ShowGridLines = true
            };
            chart.Axes.Add(yAxis);

            chart.Measure(new Size(chart.Width, chart.Height));
            chart.Arrange(new Rect(new Size(chart.Width, chart.Height)));
            chart.UpdateLayout();

            renderTarget.Render(chart);

            // Save out the chart to a png file
            PngBitmapEncoder png = new PngBitmapEncoder();
            png.Frames.Add(BitmapFrame.Create(renderTarget));
            MemoryStream stream = new MemoryStream();
            png.Save(stream);
            stream.Position = 0;
            return stream;
        }

        #endregion
    }
}
