﻿namespace RSG.Model.Report2.Reports.GameAssetUsage
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using RSG.Base.Tasks;
    using RSG.ManagedRage;
    using RSG.Metadata.Model;
    using RSG.Model.Common;
    using RSG.Model.Report;
    using RSG.Platform;
    using RSG.Model.Weapon.Game;
    using RSG.Base.Configuration;
    using System.Web.UI;

    /// <summary>
    /// The report that provides details of how the weapon assets are being used in the game
    /// including lists that detail assets that are not referenced or cannot be resolved.
    /// </summary>
    public class WeaponAssetUsageReport
        : RSG.Model.Report.Report, IReportFileProvider, IDynamicLevelReport
    {       
        #region Fields
        /// <summary>
        /// The name of this report.
        /// </summary>
        private const string ReportName = "Weapon Asset Usage";

        /// <summary>
        /// The description for this report.
        /// </summary>
        private const string ReportDescription = "Provides lists detailing the weapon asset usage in the game.";

        /// <summary>
        /// The private field used for the <see cref="GenerationTask"/> property.
        /// </summary>
        private ITask _generationTask;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="WeaponAssetUsageReport"/> class.
        /// </summary>
        public WeaponAssetUsageReport()
        {
            this.Name = ReportName;
            this.Description = ReportDescription;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Filter used for the save dialog
        /// </summary>
        public string Filter
        {
            get
            {
                return "HTML (Html File)|*.html";
            }
        }

        /// <summary>
        /// Extensions that this report support
        /// </summary>
        public string SupportedExtension
        {
            get
            {
                return ".html";
            }
        }

        /// <summary>
        /// Filename specifying where a report should be output
        /// </summary>
        public string Filename
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the task to run for generating this report.
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (this._generationTask == null)
                {
                    this._generationTask = new ActionTask(
                        "Generating report", this.GenerateReport);
                }

                return this._generationTask;
            }
        }

        public IEnumerable<StreamableStat> RequiredStats
        {
            get { return Enumerable.Empty<StreamableStat>(); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Generates the report dynamically.
        /// </summary>
        /// <param name="context">
        /// The context for this asynchronous task which includes the cancellation token.
        /// </param>
        /// <param name="progress">
        /// The progress object to use to report the progress of the generation.
        /// </param>
        private void GenerateReport(ITaskContext context, IProgress<TaskProgress> progress)
        {
            DynamicLevelReportContext dynamicContext = context as DynamicLevelReportContext;
            if (dynamicContext == null)
            {
                return;
            }

            IBranch branch = dynamicContext.Branch;
            ILevel level = dynamicContext.Level;
            StructureDictionary structures = new StructureDictionary();
            structures.Load(branch, Path.Combine(branch.Metadata, "definitions"));
            GameWeapons weapons = new GameWeapons(dynamicContext.Branch, structures);
            PlatformEntries platformEntires = new PlatformEntries();
            foreach (Platform platform in dynamicContext.Platforms)
            {
                platformEntires.Add(platform, this.GetEntries(platform, branch, level));
            }

            MemoryStream stream = new MemoryStream();
            StreamWriter streamWriter = new StreamWriter(stream);
            HtmlTextWriter htmlWriter = new HtmlTextWriter(streamWriter);

            htmlWriter.RenderBeginTag(HtmlTextWriterTag.Html);
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.Head);
            htmlWriter.RenderEndTag();
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.Body);

            htmlWriter.RenderBeginTag(HtmlTextWriterTag.H1);
            htmlWriter.Write("Weapon Asset Usage");
            htmlWriter.RenderEndTag();

            htmlWriter.AddStyleAttribute(HtmlTextWriterStyle.MarginBottom, "0");
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.H2);
            htmlWriter.Write("Not Referenced Assets");
            htmlWriter.RenderEndTag();

            htmlWriter.AddStyleAttribute(HtmlTextWriterStyle.FontSize, "small");
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.I);
            htmlWriter.Write("These are the assets located inside the weapon rpf file that have not been referenced inside the weapon metadata.");
            htmlWriter.RenderEndTag();

            string notReferencedHeader = "{0} Not Referenced Assets: {1} found";
            foreach (KeyValuePair<Platform, RpfEntries> kvp in platformEntires)
            {
                RpfEntries entries = kvp.Value;
                List<string> notReferenced = this.NotReferenced(kvp.Key, weapons, entries);

                string platformName = kvp.Key.PlatformToFriendlyName();
                htmlWriter.RenderBeginTag(HtmlTextWriterTag.H3);
                htmlWriter.Write(notReferencedHeader, platformName, notReferenced.Count);
                htmlWriter.RenderEndTag();

                foreach (string notReferencedAsset in notReferenced)
                {
                    htmlWriter.Write(notReferencedAsset);
                    htmlWriter.Write("<br />");
                }
            }

            htmlWriter.AddStyleAttribute(HtmlTextWriterStyle.MarginBottom, "0");
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.H2);
            htmlWriter.Write("Missing Assets");
            htmlWriter.RenderEndTag();

            htmlWriter.AddStyleAttribute(HtmlTextWriterStyle.FontSize, "small");
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.I);
            htmlWriter.Write("These are the definitions contained within the weapon metadata files whose associated assets cannot be found in the build rpf files.");
            htmlWriter.RenderEndTag();

            string notFoundHeader = "{0} Missing Assets: {1} found";
            foreach (KeyValuePair<Platform, RpfEntries> kvp in platformEntires)
            {
                List<string> entries = kvp.Value.AllEntries();
                List<string> notFound = this.NotFound(kvp.Key, weapons, entries);

                string platformName = kvp.Key.PlatformToFriendlyName();
                htmlWriter.RenderBeginTag(HtmlTextWriterTag.H3);
                htmlWriter.Write(notFoundHeader, platformName, notFound.Count);
                htmlWriter.RenderEndTag();

                foreach (string missingAsset in notFound)
                {
                    htmlWriter.Write(missingAsset);
                    htmlWriter.Write("<br />");
                }
            }

            this.CheckMetadata(weapons, htmlWriter);

            htmlWriter.RenderEndTag(); // Body
            htmlWriter.Flush();

            stream.Position = 0;
            string outputFile = Path.ChangeExtension(this.Filename, "html");
            using (Stream fileStream = new FileStream(outputFile, FileMode.Create, FileAccess.Write))
            {
                stream.CopyTo(fileStream);
            }
        }

        private RpfEntries GetEntries(Platform platform, IBranch branch, ILevel level)
        {
            ITarget target = branch.Targets[platform];
            string dir = target.ResourcePath;
            string images = Path.Combine(dir, "models", "cdimages");

            RpfEntries entries = new RpfEntries();
            this.AddEntries(images, "weapons.rpf", entries);

            return entries;
        }

        private void AddEntries(string directory, string filename, RpfEntries container)
        {
            container.Add(filename, this.GetEntries(directory, filename));
        }

        private List<string> GetEntries(string directory, string filename)
        {
            string path = Path.Combine(directory, filename);
            return this.GetEntries(path);
        }

        private List<string> GetEntries(string path)
        {
            if (!File.Exists(path))
            {
                return new List<string>();
            }

            Packfile packFile = new Packfile();
            packFile.Load(path);
            List<string> entries = new List<string>();
            foreach (PackEntry entry in packFile.Entries)
            {
                string name = entry.Name;
                if (!String.IsNullOrEmpty(entry.Path))
                {
                    name = name.Insert(0, entry.Path + "/");
                }

                entries.Add(name);
            }

            return entries;
        }

        /// <summary>
        /// Gets the extension used for drawable dictionary for the specified platform.
        /// </summary>
        /// <param name="platform">
        /// The platform to get the extension for.
        /// </param>
        /// <returns>
        /// The specified platforms extension for its drawable dictionaries.
        /// </returns>
        private string GetDrawableExtension(Platform platform)
        {
            return FileType.Drawable.CreateExtensionForPlatform(platform);
        }

        /// <summary>
        /// Gets the extension used for texture dictionary for the specified platform.
        /// </summary>
        /// <param name="platform">
        /// The platform to get the extension for.
        /// </param>
        /// <returns>
        /// The specified platforms extension for its texture dictionaries.
        /// </returns>
        private string GetTextureExtension(Platform platform)
        {
            return FileType.TextureDictionary.CreateExtensionForPlatform(platform);
        }

        private List<string> NotReferenced(Platform platform, GameWeapons weapons, RpfEntries entries)
        {
            string textureExtension = GetExtension(FileType.TextureDictionary, platform);
            string drawableExtension = GetExtension(FileType.Drawable, platform);

            HashSet<string> referenced = new HashSet<string>();
            foreach (string textureDictionary in weapons.UsedTextureDictionaries)
            {
                referenced.Add(textureDictionary.ToLower() + textureExtension);
                referenced.Add(textureDictionary.ToLower() + "+hi" + textureExtension);
            }

            foreach (string model in weapons.UsedModels)
            {
                referenced.Add(model.ToLower() + drawableExtension);
                referenced.Add(model.ToLower() + "_hi" + drawableExtension);
            }

            List<string> notReferenced = new List<string>();
            foreach (string entry in entries.AllEntries())
            {
                if (entry.StartsWith("_manifest"))
                {
                    continue;
                }

                if (!referenced.Contains(entry))
                {
                    notReferenced.Add(entry);
                }
            }

            return notReferenced;
        }

        private List<string> NotFound(Platform platform, GameWeapons weapons, List<string> entries)
        {
            string textureExtension = GetExtension(FileType.TextureDictionary, platform);
            string drawableExtension = GetExtension(FileType.Drawable, platform);

            List<string> notFound = new List<string>();
            foreach (string textureDictionary in weapons.UsedTextureDictionaries)
            {
                string name = textureDictionary.ToLower() + textureExtension;
                if (entries.Contains(name))
                {
                    continue;
                }

                name = textureDictionary.ToLower() + "+hi" + textureExtension;
                if (entries.Contains(name))
                {
                    continue;
                }

                notFound.Add(textureDictionary.ToLower() + textureExtension);
            }

            foreach (string model in weapons.UsedModels)
            {
                string name = model.ToLower() + drawableExtension;
                if (entries.Contains(name))
                {
                    continue;
                }

                name = model.ToLower() + "_hi" + drawableExtension;
                if (entries.Contains(name))
                {
                    continue;
                }

                notFound.Add(model.ToLower() + drawableExtension);
            }

            return notFound;
        }

        private void CheckMetadata(GameWeapons weapons, HtmlTextWriter htmlWriter)
        {
            htmlWriter.AddStyleAttribute(HtmlTextWriterStyle.MarginBottom, "0");
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.H2);
            htmlWriter.Write("Not Referenced Weapon Archetypes");
            htmlWriter.RenderEndTag();

            htmlWriter.AddStyleAttribute(HtmlTextWriterStyle.FontSize, "small");
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.I);
            htmlWriter.Write("These are the CWeaponModelInfo::InitData structures that have been defined within the weaponarchetypes.meta file but aren't referenced within either the CWeaponInfo or CAmmoInfo lists inside the weapons.meta file or through a referenced CWeaponComponentInfo defined in weaponcomponents.meta.");
            htmlWriter.RenderEndTag();

            List<GameWeaponArchetype> unreferencedArchetypes = weapons.UnreferencedArchetypeDefinitions.ToList();
            htmlWriter.AddStyleAttribute(HtmlTextWriterStyle.MarginBottom, "0");
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.H3);
            htmlWriter.Write("{0} found", unreferencedArchetypes.Count);
            htmlWriter.RenderEndTag();
            foreach (GameWeaponArchetype archetypes in unreferencedArchetypes)
            {
                htmlWriter.Write("{0}", archetypes.Model);
                htmlWriter.Write("<br />");
            }

            htmlWriter.AddStyleAttribute(HtmlTextWriterStyle.MarginBottom, "0");
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.H2);
            htmlWriter.Write("Not Referenced Weapon Animations");
            htmlWriter.RenderEndTag();

            htmlWriter.AddStyleAttribute(HtmlTextWriterStyle.FontSize, "small");
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.I);
            htmlWriter.Write("These are CWeaponAnimations structures that have been defined within the weaponanimations.meta file but aren't referenced within any CWeaponInfo definition defined in the weapons.meta file.");
            htmlWriter.RenderEndTag();

            List<GameWeaponAnimationSet> unreferencedAnimations = weapons.UnreferencedAnimationDefinitions.ToList();
            htmlWriter.AddStyleAttribute(HtmlTextWriterStyle.MarginBottom, "0");
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.H3);
            htmlWriter.Write("{0} found", unreferencedAnimations.Count);
            htmlWriter.RenderEndTag();
            foreach (GameWeaponAnimationSet animationSet in unreferencedAnimations)
            {
                htmlWriter.Write("{0}", animationSet.Name);
                htmlWriter.Write("<br />");
            }

            htmlWriter.AddStyleAttribute(HtmlTextWriterStyle.MarginBottom, "0");
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.H2);
            htmlWriter.Write("Not Referenced Weapon Ammo Types");
            htmlWriter.RenderEndTag();

            htmlWriter.AddStyleAttribute(HtmlTextWriterStyle.FontSize, "small");
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.I);
            htmlWriter.Write("These are CAmmoInfo structures that have been defined within the weapon.meta file that aren't referenced within any CWeaponInfo definition defined within the same file.");
            htmlWriter.RenderEndTag();

            List<GameWeaponAmmo> unreferencedAmmo = weapons.UnreferencedAmmoDefinitions.ToList();
            htmlWriter.AddStyleAttribute(HtmlTextWriterStyle.MarginBottom, "0");
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.H3);
            htmlWriter.Write("{0} found", unreferencedAmmo.Count);
            htmlWriter.RenderEndTag();
            foreach (GameWeaponAmmo ammo in unreferencedAmmo)
            {
                htmlWriter.Write("{0}", ammo.Name);
                htmlWriter.Write("<br />");
            }

            htmlWriter.AddStyleAttribute(HtmlTextWriterStyle.MarginBottom, "0");
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.H2);
            htmlWriter.Write("Not Referenced Weapon Components");
            htmlWriter.RenderEndTag();

            htmlWriter.AddStyleAttribute(HtmlTextWriterStyle.FontSize, "small");
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.I);
            htmlWriter.Write("These are CWeaponComponentInfo structures that have been defined within the weaponcomponents.meta file that aren't referenced within any CWeaponInfo definition defined within the same file.");
            htmlWriter.RenderEndTag();

            List<GameWeaponComponent> unreferencedComponents = weapons.UnreferencedComponentDefinitions.ToList();
            htmlWriter.AddStyleAttribute(HtmlTextWriterStyle.MarginBottom, "0");
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.H3);
            htmlWriter.Write("{0} found", unreferencedComponents.Count);
            htmlWriter.RenderEndTag();
            foreach (GameWeaponComponent component in unreferencedComponents)
            {
                htmlWriter.Write("{0}", component.Name);
                htmlWriter.Write("<br />");
            }
        }

        private string GetExtension(FileType type, Platform platform)
        {
            return "." + type.CreateExtensionForPlatform(platform);
        }
        #endregion Methods

        private class RpfEntries : Dictionary<string, List<string>>
        {
            public List<string> AllEntries()
            {
                List<string> entries = new List<string>();
                foreach (List<string> values in this.Values)
                {
                    entries.AddRange(values);
                }

                return entries;
            }
        }

        private class PlatformEntries : Dictionary<Platform, RpfEntries>
        {
        }
    } // RSG.Model.Report2.Reports.GameAssetUsage.WeaponAssetUsageReport {Class}
} // RSG.Model.Report2.Reports.GameAssetUsage {Namespace}
