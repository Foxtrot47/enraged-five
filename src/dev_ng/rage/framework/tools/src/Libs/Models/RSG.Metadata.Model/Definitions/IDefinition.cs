﻿// --------------------------------------------------------------------------------------------
// <copyright file="IDefinition.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions
{
    using System;
    using System.Collections.Generic;
    using RSG.Editor.Model;

    /// <summary>
    /// When implemented defines a parsable definition that the parCodeGen system can use.
    /// </summary>
    public interface IDefinition : IModel, IEquatable<IDefinition>
    {
        #region Properties
        /// <summary>
        /// Gets or sets the full data type for this definition.
        /// </summary>
        string DataType { get; set; }

        /// <summary>
        /// Gets the dictionary this definition is defined inside.
        /// </summary>
        IDefinitionDictionary Dictionary { get; }

        /// <summary>
        /// Gets or sets the filename this instance is defined in or null if not applicable.
        /// </summary>
        string Filename { get; set; }

        /// <summary>
        /// Gets or sets the line number this instance is defined on or -1 if not available.
        /// </summary>
        int LineNumber { get; set; }

        /// <summary>
        /// Gets or sets the line position this instance is defined at or -1 if not available.
        /// </summary>
        int LinePosition { get; set; }

        /// <summary>
        /// Gets the location of this definition inside the parCodeGen definition data sat on
        /// the local disk.
        /// </summary>
        FileLocation Location { get; }

        /// <summary>
        /// Gets the namespace this definition is currently inside by looking at the data type.
        /// </summary>
        string Namespace { get; }

        /// <summary>
        /// Gets an iterator over the namespace hierarchy starting at the root.
        /// </summary>
        IEnumerable<string> Namespaces { get; }

        /// <summary>
        /// Gets the scope for this definition.
        /// </summary>
        string Scope { get; }

        /// <summary>
        /// Gets the short data type for this definition.
        /// </summary>
        string ShortDataType { get; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Determine whether the specified string is referencing this definition.
        /// </summary>
        /// <param name="type">
        /// The string that could be referencing this definition.
        /// </param>
        /// <returns>
        /// True if the specified string is referencing this definition; otherwise, false.
        /// </returns>
        bool IsReferencedBy(string type);
        #endregion Methods
    } // RSG.Metadata.Model.Definitions.IDefinition {Interface}
} // RSG.Metadata.Model.Definitions {Namespace}
