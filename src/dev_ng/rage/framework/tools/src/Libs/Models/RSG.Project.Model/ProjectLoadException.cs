﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProjectLoadException.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.Model
{
    using System;
    using System.Runtime.Serialization;
    using System.Xml;
    using RSG.Base.Extensions;
    using RSG.Project.Model.Resources;

    /// <summary>
    /// Represents errors that occur during the load of a project file, either a single project
    /// or multiple projects.
    /// </summary>
    [Serializable]
    public class ProjectLoadException : Exception
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="FullPath"/> property.
        /// </summary>
        private string _fullPath;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ProjectLoadException"/> class
        /// using the default
        /// message.
        /// </summary>
        public ProjectLoadException()
            : base()
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ProjectLoadException"/> class
        /// with the specified message.
        /// </summary>
        /// <param name="message">
        /// The message that describes the error.
        /// </param>
        public ProjectLoadException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ProjectLoadException"/> class
        /// with the specified inner exception and the default message.
        /// </summary>
        /// <param name="innerException">
        /// The exception that is the cause of this current exception, or a null reference if
        /// no inner exception is specified.
        /// </param>
        public ProjectLoadException(Exception innerException)
            : base(String.Empty, innerException)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ProjectLoadException"/> class
        /// with the specified message and inner exception.
        /// </summary>
        /// <param name="message">
        /// The message that describes the error.
        /// </param>
        /// <param name="innerException">
        /// The exception that is the cause of this current exception, or a null reference if
        /// no inner exception is specified.
        /// </param>
        public ProjectLoadException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ProjectLoadException"/> class
        /// with serialised data.
        /// </summary>
        /// <param name="info">
        /// The System.Runtime.Serialization.SerializationInfo that holds the serialised
        /// object data about the exception being thrown.
        /// </param>
        /// <param name="context">
        /// The System.Runtime.Serialization.StreamingContext that contains contextual
        /// information about the source or destination.
        /// </param>
        protected ProjectLoadException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the full path to the location of the file that was being loaded when the error
        /// occurred.
        /// </summary>
        public string FullPath
        {
            get { return this._fullPath; }
        }

        /// <summary>
        /// Gets the message that should be outputted to a log object to log this error.
        /// </summary>
        public string LogMessage
        {
            get
            {
                return ErrorStringTable.LogMessage.FormatInvariant(
                    this.Message, this._fullPath);
            }
        }

        /// <summary>
        /// Gets the message that should be shown to the user inside a message box when this
        /// error occurs.
        /// </summary>
        public string MessageBoxText
        {
            get { return this.Message; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a instance of the <see cref="ProjectLoadException"/> class representing
        /// when a built in metadata value is being overridden.
        /// </summary>
        /// <param name="name">
        /// The name of the metadata value being overridden.
        /// </param>
        /// <param name="fullPath">
        /// The full path to the file being loaded.
        /// </param>
        /// <returns>
        /// A new instance of the <see cref="ProjectLoadException"/> class.
        /// </returns>
        public static ProjectLoadException BuiltinOverride(string name, string fullPath)
        {
            string msg = ErrorStringTable.BuiltinOverride.FormatInvariant(name, fullPath);
            ProjectLoadException exception = new ProjectLoadException(msg);
            exception._fullPath = fullPath;
            return exception;
        }

        /// <summary>
        /// Creates a instance of the <see cref="ProjectLoadException"/> class representing
        /// when a project item is missing its include attribute or its empty.
        /// </summary>
        /// <param name="elementName">
        /// The element name that is missing the include attribute.
        /// </param>
        /// <param name="fullPath">
        /// The full path to the file being loaded.
        /// </param>
        /// <returns>
        /// A new instance of the <see cref="ProjectLoadException"/> class.
        /// </returns>
        public static ProjectLoadException MissingInclude(string elementName, string fullPath)
        {
            string msg =
                ErrorStringTable.IncludeMissing.FormatInvariant(elementName, fullPath);
            ProjectLoadException exception = new ProjectLoadException(msg);
            exception._fullPath = fullPath;
            return exception;
        }

        /// <summary>
        /// Creates a instance of the <see cref="ProjectLoadException"/> class representing
        /// when a xml exception has been thrown.
        /// </summary>
        /// <param name="fullPath">
        /// The full path to the file being loaded.
        /// </param>
        /// <param name="ex">
        /// The xml exception that was thrown.
        /// </param>
        /// <returns>
        /// A new instance of the <see cref="ProjectLoadException"/> class.
        /// </returns>
        public static ProjectLoadException XmlException(string fullPath, XmlException ex)
        {
            string msg = ErrorStringTable.XmlParse.FormatInvariant(ex.Message, fullPath);
            ProjectLoadException exception = new ProjectLoadException(msg);
            exception._fullPath = fullPath;
            return exception;
        }

        /// <summary>
        /// Creates a instance of the <see cref="ProjectLoadException"/> class representing
        /// when a generic exception has been thrown.
        /// </summary>
        /// <param name="fullPath">
        /// The full path to the file being loaded.
        /// </param>
        /// <param name="ex">
        /// The exception that was thrown.
        /// </param>
        /// <returns>
        /// A new instance of the <see cref="ProjectLoadException"/> class.
        /// </returns>
        public static ProjectLoadException GenericException(string fullPath, Exception ex)
        {
            string msg =
                ErrorStringTable.GenericException.FormatInvariant(ex.Message, fullPath);
            ProjectLoadException exception = new ProjectLoadException(msg);
            exception._fullPath = fullPath;
            return exception;
        }
        #endregion Methods
    } // RSG.Project.Model.ProjectLoadException {Class}
} // RSG.Project.Model {Namespace}
