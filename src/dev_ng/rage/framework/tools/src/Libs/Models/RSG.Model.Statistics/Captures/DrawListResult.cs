﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Runtime.Serialization;

namespace RSG.Model.Statistics.Captures
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class DrawListResult
    {
        #region Properties
        /// <summary>
        /// Name of the draw list this result is for.
        /// </summary>
        [DataMember]
        public String Name { get; set; }

        /// <summary>
        /// Minimum value recorded for this result.
        /// </summary>
        [DataMember]
        public uint Min { get; set; }

        /// <summary>
        /// Maximum value recorded for this result.
        /// </summary>
        [DataMember]
        public uint Max { get; set; }

        /// <summary>
        /// Average value recorded for this result.
        /// </summary>
        [DataMember]
        public uint Average { get; set; }

        /// <summary>
        /// Standard deviation for this result.
        /// </summary>
        [DataMember]
        public float StandardDeviation { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public DrawListResult()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public DrawListResult(XElement element)
        {
            // Extract the various stats
            XElement nameElement = element.Element("name");
            if (nameElement != null)
            {
                Name = nameElement.Value;
            }

            XElement minElement = element.Element("min");
            if (minElement != null && minElement.Attribute("value") != null)
            {
                Min = UInt32.Parse(minElement.Attribute("value").Value);
            }

            XElement maxElement = element.Element("max");
            if (maxElement != null && maxElement.Attribute("value") != null)
            {
                Max = UInt32.Parse(maxElement.Attribute("value").Value);
            }

            XElement avgElement = element.Element("average");
            if (avgElement != null && avgElement.Attribute("value") != null)
            {
                Average = UInt32.Parse(avgElement.Attribute("value").Value);
            }

            XElement stdElement = element.Element("std");
            if (stdElement != null && stdElement.Attribute("value") != null)
            {
                StandardDeviation = Single.Parse(stdElement.Attribute("value").Value);
            }
        }
        #endregion // Constructor(s)
    } // DrawListResult
}
