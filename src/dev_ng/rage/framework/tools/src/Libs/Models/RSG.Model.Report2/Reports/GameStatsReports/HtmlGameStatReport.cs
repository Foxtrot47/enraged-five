﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Report.Reports.GameStatsReports.Tests;
using RSG.SourceControl.Perforce;
using RSG.Model.Statistics.Captures;
using System.IO;
using System.Web.UI;
using RSG.Base.Configuration.Reports;
using System.Windows;
using RSG.Base.Extensions;

namespace RSG.Model.Report.Reports.GameStatsReports
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class HtmlGameStatReport : GameStatReportBase, IReportStreamProvider, IDisposable
    {
        #region Properties
        /// <summary>
        /// Stream containing the html report data
        /// </summary>
        public Stream Stream
        {
            get;
            private set;
        }

        /// <summary>
        /// Smoke tests that will be run against the data
        /// </summary>
        private ISmokeTest SmokeTest
        {
            get;
            set;
        }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="desc"></param>
        public HtmlGameStatReport(string name, string desc, ISmokeTest smokeTest)
            : base(name, desc)
        {
            SmokeTest = smokeTest;
        }
        #endregion // Constructor(s)

        #region GameStatReportBase Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="testSessions"></param>
        /// <param name="uniqueTestNames"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="cacheDir"></param>
        protected override void GenerateReport(IList<TestSession> testSessions, IEnumerable<string> uniqueTestNames, DateTime start, DateTime end, string cacheDir)
        {
            // Run the smoke test first
            SmokeTest.RunTest(testSessions);

            // Generate any graphs that will be part of the report and save them out to the cache directory
            IList<GraphImage> allImages = new List<GraphImage>();

            // Need to invoke creation of the graphs on the UI thread.
            Application.Current.Dispatcher.Invoke(
                    new Action
                    (
                        delegate()
                        {
                            allImages.AddRange(SmokeTest.GenerateGraphs(start, end));
                        }
                    ),
                    System.Windows.Threading.DispatcherPriority.ContextIdle
                );
            SaveImagesToCache(allImages, cacheDir);

            // Finally generate the report based on the smoke test's results
            MemoryStream stream = new MemoryStream();
            StreamWriter sw = new StreamWriter(stream);
            HtmlTextWriter writer = new HtmlTextWriter(sw);

            writer.RenderBeginTag(HtmlTextWriterTag.Html);
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Body);
                {
                    WriteReportHeader(writer, null, SmokeTest.Errors, SmokeTest.Warnings, SmokeTest.Messages);

                    if (SmokeTest.CommonGraphs != null && SmokeTest.CommonGraphs.Count > 0)
                    {
                        WriteReportCommonGraphs(writer, SmokeTest.CommonGraphs, false, false);
                    }

                    WriteReportTestResults(writer, uniqueTestNames);
                }
                writer.RenderEndTag();
            }
            writer.RenderEndTag();
            writer.Flush();

            // Reset the local stream and set the property
            stream.Position = 0;
            Stream = stream;

            // Dispose of the image streams
            foreach (GraphImage image in allImages)
            {
                image.Dispose();
            }
        }
        #endregion // GameStatReportBase Overrides

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="testName"></param>
        private void WriteReportTestResults(HtmlTextWriter writer, IEnumerable<string> uniqueTestNames)
        {
            // Output a header for the section
            writer.RenderBeginTag(HtmlTextWriterTag.H1);
            writer.AddAttribute(HtmlTextWriterAttribute.Name, "Individual Tests");
            writer.RenderBeginTag(HtmlTextWriterTag.A);
            writer.Write("Individual Tests");
            writer.RenderEndTag();
            writer.RenderEndTag();

            // Go over all the tests that ran
            foreach (string testName in uniqueTestNames)
            {
                // Output a header for this test
                writer.RenderBeginTag(HtmlTextWriterTag.H2);
                writer.AddAttribute(HtmlTextWriterAttribute.Name, testName);
                writer.RenderBeginTag(HtmlTextWriterTag.A);
                writer.Write(testName);
                writer.RenderEndTag();
                writer.RenderEndTag();

                SmokeTest.WriteReport(writer, testName, false, MaxTableResults, false);
            }
        }

        /// <summary>
        /// Saves all the images to the cache dir
        /// </summary>
        /// <param name="images"></param>
        /// <param name="cacheDir"></param>
        private void SaveImagesToCache(IList<GraphImage> images, string cacheDir)
        {
            foreach (GraphImage image in images)
            {
                image.Filepath = Path.Combine(cacheDir, String.Format("{0}.png", image.Name));

                using (Stream fileStream = File.Create(image.Filepath))
                {
                    image.DataStream.CopyTo(fileStream);
                    fileStream.Flush();
                }
            }
        }
        #endregion // Private Methods

        #region IDisposable Implementation
        /// <summary>
        /// Ensures that the stream is correctly disposed of
        /// </summary>
        public override void Dispose()
        {
            base.Dispose();

            if (Stream != null)
            {
                Stream.Dispose();
            }
        }
        #endregion // IDisposable Implementation
    } // SingleGameStatReportBase
}
