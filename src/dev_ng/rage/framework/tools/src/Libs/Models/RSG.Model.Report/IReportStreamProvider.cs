﻿using System.IO;

namespace RSG.Model.Report
{
    /// <summary>
    /// 
    /// </summary>
    public interface IReportStreamProvider
    {
        /// <summary>
        /// Stream containing the report data
        /// </summary>
        Stream Stream
        {
            get;
        }
    } // IReportStream
} // RSG.Model.Report
