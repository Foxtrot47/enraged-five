﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using RSG.Base.ConfigParser;
using RSG.Model.Common;
using RSG.Model.Map;
using RSG.Model.Map.Statistics;

namespace RSG.Model.Report.Reports
{
    /// <summary>
    /// 
    /// </summary>
    public class IsolatedReferenceReport : CSVReport, IDynamicMapReport
    {
        #region Constants
        private const String NAME = "Isolated Reference Report";
        private const String DESCRIPTION = "Generate and display a isolated reference report.";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Determines whether the map data should be loaded
        /// </summary>
        public bool LoadMapData
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Determines whether the car generators should be loaded
        /// 
        /// </summary>
        public bool LoadCarGenarators
        {
            get { return false; }
        }
        #endregion // Properties

        #region Construtcor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public IsolatedReferenceReport()
            : base(NAME, DESCRIPTION)
        {
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        /// <param name="gv"></param>
        public void Generate(ILevel level, ConfigGameView gv, IPlatformCollection platforms)
        {
            GenerateIsolatedReferenceReport(level, gv);
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        /// <param name="gv"></param>
        public void GenerateIsolatedReferenceReport(ILevel level, ConfigGameView gv)
        {
            RSG.Model.Map.Util.Creation.InitialiseLevelStatistics(level, gv);

            var isolatedReferences = new Dictionary<MapSection, Dictionary<IMapContainer, List<MapInstance>>>();
            foreach (MapSection section in RSG.Model.Map.Util.Enumerations.GetAllMapSections(level, null, true))
            {
                if (!section.ExportInstances)
                {
                    foreach (IAsset asset in section.AssetChildren.OfType<MapDefinition>())
                    {
                        var definition = asset as MapDefinition;
                        if (definition.Instances.Count > 1 && !definition.IsMilo)
                        {
                            var notIsolated = new Dictionary<MapInstance, bool>();
                            for (int i = 0; i < definition.Instances.Count - 1; i++)
                            {
                                var instance = definition.Instances[i];
                                if (notIsolated.ContainsKey(instance))
                                    continue;
                                for (int j = i + 1; j < definition.Instances.Count; j++)
                                {
                                    float fadeDistance = 20.0f;

                                    float instanceRadius = instance.LodDistance + fadeDistance;
                                    var instancePosition = instance.Position;

                                    float siblingRadius = definition.Instances[j].LodDistance + fadeDistance;
                                    var siblingPosition = definition.Instances[j].Position;

                                    float testDistance = (instanceRadius * instanceRadius) + (2 * (instanceRadius * siblingRadius)) + (siblingRadius * siblingRadius);
                                    float dx = siblingPosition.X - instancePosition.X;
                                    float dy = siblingPosition.Y - instancePosition.Y;
                                    float dz = siblingPosition.Z - instancePosition.Z;
                                    float squaredDistance = (dx * dx) + (dy * dy) + (dz * dz);
                                    if (squaredDistance <= testDistance)
                                    {
                                        if (!notIsolated.ContainsKey(instance))
                                            notIsolated.Add(instance, true);

                                        if (!notIsolated.ContainsKey(definition.Instances[j]))
                                            notIsolated.Add(definition.Instances[j], true);
                                    }
                                }
                            }
                            var isolated = new List<MapInstance>();
                            foreach (var instance in definition.Instances)
                            {
                                if (!notIsolated.ContainsKey(instance))
                                    isolated.Add(instance);
                            }

                            if (isolated.Count > 0)
                            {
                                if (!isolatedReferences.ContainsKey(section))
                                    isolatedReferences.Add(section, new Dictionary<IMapContainer, List<MapInstance>>());
                                foreach (var instance in isolated)
                                {
                                    if (!isolatedReferences[section].ContainsKey(instance.Container))
                                        isolatedReferences[section].Add(instance.Container, new List<MapInstance>());
                                    isolatedReferences[section][instance.Container].Add(instance);
                                }
                            }
                        }
                    }
                }
            }

            using (StreamWriter sw = new StreamWriter(Filename))
            {
                WriteCsvObjectHeader(sw);
                foreach (var definitionSection in isolatedReferences)
                {
                    foreach (var instanceSection in definitionSection.Value)
                    {
                        foreach (var instance in instanceSection.Value)
                        {
                            WriteCsvInstance(sw, instance);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sw"></param>
        /// <param name="milo"></param>
        private void WriteCsvObjectHeader(StreamWriter sw)
        {
            string header = string.Format("Instance Name, Owning Section, Parent Area, Definition Name");
            sw.WriteLine(header);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objectSw"></param>
        /// <param name="interiorSw"></param>
        /// <param name="def"></param>
        /// <param name="section"></param>
        /// <param name="milo"></param>
        private void WriteCsvInstance(StreamWriter sw, MapInstance instance)
        {
            IMapContainer parent = instance.Container;
            IMapContainer grandParent = null;
            if (parent is MapSection)
                grandParent = (parent as MapSection).Container;

            string csvRecord = string.Format("{0}, {1}, {2}, {3} ", instance.Name, parent != null ? parent.Name : "n/a", grandParent != null ? grandParent.Name : "n/a", instance.ReferencedDefinition.Name);
            sw.WriteLine(csvRecord);       
        }
        #endregion // Private Methods
    } // MapOptimiseReport
} // RSG.Model.Report.Reports
