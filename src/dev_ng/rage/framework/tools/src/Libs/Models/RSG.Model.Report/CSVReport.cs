﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using System.ComponentModel;
using System.IO;

namespace RSG.Model.Report
{
    /// <summary>
    /// CSVReport - A report that generates a CSV file
    /// </summary>
    public abstract class CSVReport : Report, IReportFileProvider
    {
        #region Properties
        /// <summary>
        /// Filter used for the save dialog
        /// </summary>
        public string Filter
        {
            get
            {
                return "CSV (Comma delimited)|*.csv";
            }
        }

        /// <summary>
        /// Extensions that this report support
        /// </summary>
        public string SupportedExtension
        {
            get
            {
                return ".csv";
            }
        }

        /// <summary>
        /// Filename specifying where a report should be output
        /// </summary>
        public string Filename
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="desc"></param>
        public CSVReport(string name, string desc)
            : base(name, desc)
        {
        }
        #endregion // Constructor(s)
    }
} // RSG.Model.Report namespace
