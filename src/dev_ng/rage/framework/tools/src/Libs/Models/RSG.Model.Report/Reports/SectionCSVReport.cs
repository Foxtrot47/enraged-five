﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.ConfigParser;
using RSG.Model.Common;
using RSG.Model.Map;
using RSG.Model.Map.Statistics;

namespace RSG.Model.Report.Reports
{
    /// <summary>
    /// 
    /// </summary>
    public class SectionCSVReport : CSVReport, IDynamicMapReport
    {
        #region Constants
        private const String NAME = "Export Section CSV Report";
        private const String DESCRIPTION = "Exports the selected levels map sections into a .csv file";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Determines whether the map data should be loaded
        /// </summary>
        public bool LoadMapData
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Determines whether the car generators should be loaded
        /// 
        /// </summary>
        public bool LoadCarGenarators
        {
            get { return false; }
        }
        #endregion // Properties

        #region Construtcor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public SectionCSVReport()
            : base(NAME, DESCRIPTION)
        {
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Generate the report dynamically.
        /// </summary>
        /// <param name="level"></param>
        /// <param name="gv"></param>
        public void Generate(ILevel level, ConfigGameView gv, IPlatformCollection platforms)
        {
            string directory = System.IO.Path.GetDirectoryName(Filename);
            string filename = System.IO.Path.GetFileNameWithoutExtension(Filename);
            string genericFilename = System.IO.Path.Combine(directory, filename + "_generic.csv");
            string nongenericFilename = System.IO.Path.Combine(directory, filename + "_nongeneric.csv");
            ExportSectionCSVReport(genericFilename, nongenericFilename, level, gv);
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="objectFilename"></param>
        /// <param name="level"></param>
        /// <param name="gv"></param>
        public static void ExportSectionCSVReport(String genericFilename, String nonGenericFilename, ILevel level, ConfigGameView gv)
        {
            RSG.Model.Map.Util.Creation.InitialiseLevelStatistics(level, gv);

            using (StreamWriter nonGenericSw = new StreamWriter(nonGenericFilename))
            {
                WriteCsvSectionHeader(nonGenericSw);
                using (StreamWriter genericSw = new StreamWriter(genericFilename))
                {
                    WriteCsvSectionHeader(genericSw);
                    foreach (MapSection section in RSG.Model.Map.Util.Enumerations.GetAllMapSections(level, null, true))
                    {
                        WriteCsvSection(genericSw, nonGenericSw, section);
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sw"></param>
        /// <param name="milo"></param>
        private static void WriteCsvSectionHeader(StreamWriter sw)
        {
            string header = "Name, Parent Area, Grandparent Area, ";
            header += "Drawable Instance Count, Non-Drawable Instance Count, Interior Instance Count, ";
            header += "Polygon Count, Texture Count, Shader Count, ";
            header += "Collision Group Count, ";
            header += "Texture Density, Shader Density, Drawable Instance Density, Non-Drawable Instance Density, Polygon Density, ";
            header += "Mover Collision, Weapons Collision, Camera Collision, River Collision";
            sw.WriteLine(header);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objectSw"></param>
        /// <param name="interiorSw"></param>
        /// <param name="def"></param>
        /// <param name="section"></param>
        /// <param name="milo"></param>
        private static void WriteCsvSection(StreamWriter genericSw, StreamWriter nonGenericSw, MapSection section)
        {
            if (section.ExportInstances == true)
            {
                WriteInstanceSection(nonGenericSw, section);
            }
            else
            {
                WriteNonInstanceSection(genericSw, section);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sw"></param>
        /// <param name="section"></param>
        private static void WriteInstanceSection(StreamWriter sw, MapSection section)
        {
            IMapContainer grandParent = null;
            if (section.Container is MapArea)
                grandParent = (section.Container as MapArea).Container;

            int polycount = 0;
            int textureCount = 0;
            int drawableInstanceCount = 0;
            int nonDrawableInstanceCount = 0;
            int shaderCount = 0;
            int moverCount = 0;
            int weaponsCount = 0;
            int cameraCount = 0;
            int riverCount = 0;
            int interiorCount = 0;

            var stats = section.RawSectionStatistics;
            if (stats != null)
            {
                polycount = stats[RawSectionStatistics.StatisticGroups.Instances][RawSectionStatistics.LodLevel.Total][RawSectionStatistics.InstanceType.Total].PolygonCount;
                textureCount = stats[RawSectionStatistics.StatisticGroups.Instances][RawSectionStatistics.LodLevel.Total][RawSectionStatistics.InstanceType.Total].TextureCount;

                drawableInstanceCount = stats[RawSectionStatistics.StatisticGroups.DrawableInstances][RawSectionStatistics.LodLevel.Total][RawSectionStatistics.InstanceType.Total].Count;
                nonDrawableInstanceCount = stats[RawSectionStatistics.StatisticGroups.NonDrawableInstances][RawSectionStatistics.LodLevel.Total][RawSectionStatistics.InstanceType.Total].Count;
                interiorCount = stats[RawSectionStatistics.StatisticGroups.Interiors][RawSectionStatistics.LodLevel.Total][RawSectionStatistics.InstanceType.Total].Count;

                shaderCount = stats[RawSectionStatistics.StatisticGroups.Instances][RawSectionStatistics.LodLevel.Total][RawSectionStatistics.InstanceType.Total].ShaderCount;

                moverCount = stats[RawSectionStatistics.StatisticGroups.Instances][RawSectionStatistics.LodLevel.Total][RawSectionStatistics.InstanceType.Total].MoverCollisionCount;
                weaponsCount = stats[RawSectionStatistics.StatisticGroups.Instances][RawSectionStatistics.LodLevel.Total][RawSectionStatistics.InstanceType.Total].WeaponCollisionCount;
                cameraCount = stats[RawSectionStatistics.StatisticGroups.Instances][RawSectionStatistics.LodLevel.Total][RawSectionStatistics.InstanceType.Total].CameraCollisionCount;
                riverCount = stats[RawSectionStatistics.StatisticGroups.Instances][RawSectionStatistics.LodLevel.Total][RawSectionStatistics.InstanceType.Total].RiverCollisionCount;
            }

            string textureDensity = "n/a";
            string shaderDensity = "n/a";
            string objDensity = "n/a";
            string propDensity = "n/a";
            string polyDensity = "n/a";
            if (section.Area != 0.0f)
            {
                textureDensity = string.Format("{0:0.######}", (textureCount / section.Area));
                shaderDensity = string.Format("{0:0.######}", (shaderCount / section.Area));
                objDensity = string.Format("{0:0.######}", (drawableInstanceCount / section.Area));
                propDensity = string.Format("{0:0.######}", (nonDrawableInstanceCount / section.Area));
                polyDensity = string.Format("{0:0.######}", (polycount / section.Area));
            }

            var collisionGroups = new List<string>();
            foreach (IAsset asset in section.AssetChildren)
            {
                if (asset is MapInstance && (asset as MapInstance).ReferencedDefinition != null)
                {
                    if ((asset as MapInstance).ReferencedDefinition.HasDefaultCollisionGroup == false)
                    {
                        if (!collisionGroups.Contains((asset as MapInstance).ReferencedDefinition.CollisionGroupName.ToLower()))
                            collisionGroups.Add((asset as MapInstance).ReferencedDefinition.CollisionGroupName.ToLower());
                    }
                }
            }

            string scvRecord = string.Format("{0}, {1}, {2}, ", section.Name, section.Container != null ? section.Container.Name : "n/a", grandParent != null ? grandParent.Name : "n/a");
            scvRecord += string.Format("{0}, {1}, {2}, ", drawableInstanceCount, nonDrawableInstanceCount, interiorCount);
            scvRecord += string.Format("{0}, {1}, {2}, ", polycount, textureCount, shaderCount);
            scvRecord += string.Format("{0}, ", collisionGroups.Count);
            scvRecord += string.Format("{0}, {1}, {2}, {3}, {4}, ", textureDensity, shaderDensity, objDensity, propDensity, polyDensity);
            scvRecord += string.Format("{0}, {1}, {2}, {3}", moverCount, weaponsCount, cameraCount, riverCount);

            sw.WriteLine(scvRecord);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sw"></param>
        /// <param name="section"></param>
        private static void WriteNonInstanceSection(StreamWriter sw, MapSection section)
        {
            IMapContainer grandParent = null;
            if (section.Container is MapArea)
                grandParent = (section.Container as MapArea).Container;

            int polycount = 0;
            int textureCount = 0;
            int drawableInstanceCount = 0;
            int nonDrawableInstanceCount = 0;
            int shaderCount = 0;
            int moverCount = 0;
            int weaponsCount = 0;
            int cameraCount = 0;
            int riverCount = 0;
            int interiorCount = 0;

            var stats = section.RawSectionStatistics;
            if (stats != null)
            {
                polycount = stats[RawSectionStatistics.StatisticGroups.Drawables][RawSectionStatistics.LodLevel.Total][RawSectionStatistics.InstanceType.Total].PolygonCount;
                textureCount = stats[RawSectionStatistics.StatisticGroups.Drawables][RawSectionStatistics.LodLevel.Total][RawSectionStatistics.InstanceType.Total].TextureCount;

                drawableInstanceCount = stats[RawSectionStatistics.StatisticGroups.Drawables][RawSectionStatistics.LodLevel.Total][RawSectionStatistics.InstanceType.Total].Count;
                nonDrawableInstanceCount = stats[RawSectionStatistics.StatisticGroups.Drawables][RawSectionStatistics.LodLevel.Total][RawSectionStatistics.InstanceType.Total].Count;
                interiorCount = stats[RawSectionStatistics.StatisticGroups.Drawables][RawSectionStatistics.LodLevel.Total][RawSectionStatistics.InstanceType.Total].Count;

                shaderCount = stats[RawSectionStatistics.StatisticGroups.Drawables][RawSectionStatistics.LodLevel.Total][RawSectionStatistics.InstanceType.Total].ShaderCount;

                moverCount = stats[RawSectionStatistics.StatisticGroups.Drawables][RawSectionStatistics.LodLevel.Total][RawSectionStatistics.InstanceType.Total].MoverCollisionCount;
                weaponsCount = stats[RawSectionStatistics.StatisticGroups.Drawables][RawSectionStatistics.LodLevel.Total][RawSectionStatistics.InstanceType.Total].WeaponCollisionCount;
                cameraCount = stats[RawSectionStatistics.StatisticGroups.Drawables][RawSectionStatistics.LodLevel.Total][RawSectionStatistics.InstanceType.Total].CameraCollisionCount;
                riverCount = stats[RawSectionStatistics.StatisticGroups.Drawables][RawSectionStatistics.LodLevel.Total][RawSectionStatistics.InstanceType.Total].RiverCollisionCount;
            }

            string textureDensity = "n/a";
            string shaderDensity = "n/a";
            string objDensity = "n/a";
            string propDensity = "n/a";
            string polyDensity = "n/a";
            if (section.Area != 0.0f)
            {
                textureDensity = string.Format("{0:0.######}", (textureCount / section.Area));
                shaderDensity = string.Format("{0:0.######}", (shaderCount / section.Area));
                objDensity = string.Format("{0:0.######}", (drawableInstanceCount / section.Area));
                propDensity = string.Format("{0:0.######}", (nonDrawableInstanceCount / section.Area));
                polyDensity = string.Format("{0:0.######}", (polycount / section.Area));
            }

            var collisionGroups = new List<string>();
            foreach (IAsset asset in section.AssetChildren)
            {
                if (asset is MapDefinition)
                {
                    if ((asset as MapDefinition).HasDefaultCollisionGroup == false)
                    {
                        if (!collisionGroups.Contains((asset as MapDefinition).CollisionGroupName.ToLower()))
                            collisionGroups.Add((asset as MapDefinition).CollisionGroupName.ToLower());
                    }
                }
            }

            string scvRecord = string.Format("{0}, {1}, {2}, ", section.Name, section.Container != null ? section.Container.Name : "n/a", grandParent != null ? grandParent.Name : "n/a");
            scvRecord += string.Format("{0}, {1}, {2}, ", drawableInstanceCount, nonDrawableInstanceCount, interiorCount);
            scvRecord += string.Format("{0}, {1}, {2}, ", polycount, textureCount, shaderCount);
            scvRecord += string.Format("{0}, ", collisionGroups.Count);
            scvRecord += string.Format("{0}, {1}, {2}, {3}, {4}, ", textureDensity, shaderDensity, objDensity, propDensity, polyDensity);
            scvRecord += string.Format("{0}, {1}, {2}, {3}", moverCount, weaponsCount, cameraCount, riverCount);

            sw.WriteLine(scvRecord);
        }
        #endregion // Private Methods
    } // ObjectCSVReport
} // RSG.Model.Report.Reports
