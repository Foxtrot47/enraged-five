﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Runtime.Serialization;

namespace RSG.Model.Statistics.Captures
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class CpuResult
    {
        #region Properties
        /// <summary>
        /// Name of this cpu result.
        /// </summary>
        [DataMember]
        public String Name { get; set; }

        /// <summary>
        /// Set this cpu result is a part of.
        /// </summary>
        [DataMember]
        public String Set { get; set; }

        /// <summary>
        /// Minimum value recorded for this result.
        /// </summary>
        [DataMember]
        public float Min { get; set; }

        /// <summary>
        /// Maximum value recorded for this result.
        /// </summary>
        [DataMember]
        public float Max { get; set; }

        /// <summary>
        /// Average value recorded for this result.
        /// </summary>
        [DataMember]
        public float Average { get; set; }

        /// <summary>
        /// Standard deviation for this result.
        /// </summary>
        [DataMember]
        public float StandardDeviation { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public CpuResult()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public CpuResult(XElement element)
        {
            // Extract the various stats
            XElement nameElement = element.Element("name");
            if (nameElement != null)
            {
                Name = nameElement.Value;
            }

            XElement setElement = element.Element("set");
            if (setElement != null)
            {
                Set = setElement.Value;
            }

            XElement minElement = element.Element("min");
            if (minElement != null && minElement.Attribute("value") != null)
            {
                Min = Single.Parse(minElement.Attribute("value").Value);
            }

            XElement maxElement = element.Element("max");
            if (maxElement != null && maxElement.Attribute("value") != null)
            {
                Max = Single.Parse(maxElement.Attribute("value").Value);
            }

            XElement avgElement = element.Element("average");
            if (avgElement != null && avgElement.Attribute("value") != null)
            {
                Average = Single.Parse(avgElement.Attribute("value").Value);
            }

            XElement stdElement = element.Element("std");
            if (stdElement != null && stdElement.Attribute("value") != null)
            {
                StandardDeviation = Single.Parse(stdElement.Attribute("value").Value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="average"></param>
        public CpuResult(String name, float average)
        {
            Name = name;
            Average = average;
        }
        #endregion // Constructor(s)
    } // CpuResult

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class CpuResultKey : IEquatable<CpuResultKey>, IComparable<CpuResultKey>
    {
        #region Properties
        /// <summary>
        /// Name of the set this result is a part of.
        /// </summary>
        [DataMember]
        public String SetName { get; private set; }

        /// <summary>
        /// Name of the metric this result is for.
        /// </summary>
        [DataMember]
        public String MetricName { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="setName"></param>
        /// <param name="metricName"></param>
        public CpuResultKey(String setName, String metricName)
        {
            SetName = setName;
            MetricName = metricName;
        }
        #endregion // Constructor(s)

        #region Object Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return String.Format("{0}/{1}", SetName, MetricName);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return base.Equals(obj);
            }

            return ((obj is CpuResultKey) && Equals(obj as CpuResultKey));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return (SetName.GetHashCode() + MetricName.GetHashCode());
        }
        #endregion // Object Overrides

        #region IEquatable<CpuResultKey> Implementation
        /// <summary>
        /// 
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(CpuResultKey other)
        {
            if (other == null)
            {
                return false;
            }

            return (SetName == other.SetName && MetricName == other.MetricName);
        }
        #endregion // IEquatable<CpuResultKey> Implementation

        #region IComparable<CpuResultKey> Interface
        /// <summary>
        /// Compare this ModificationInfo to another.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(CpuResultKey other)
        {
            if (other == null)
            {
                return 1;
            }

            if (SetName == other.SetName)
            {
                return MetricName.CompareTo(other.MetricName);
            }

            return SetName.CompareTo(other.SetName);
        }
        #endregion // IComparable<IVehicle> Interface
    } // CpuResultKey

}
