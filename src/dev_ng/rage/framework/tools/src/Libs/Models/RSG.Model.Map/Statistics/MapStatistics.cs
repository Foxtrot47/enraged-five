﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RSG.Model.Map.Utilities;
using RSG.Model.Map;
using RSG.SceneXml;
using RSG.Base.Math;

namespace RSG.Model.Map.Statistics
{
    public class MapStatistics
    {
        public class StaticStatistics
        {
            public int StatisticCount
            {
                get { return m_statisticCount; }
                set { m_statisticCount = value; }
            }
            private int m_statisticCount;

            public double TotalValue
            {
                get { return m_totalValue; }
                set { m_totalValue = value; }
            }
            private double m_totalValue;

            public SortedDictionary<double, int> SortedValues
            {
                get { return m_sortedValues; }
                set { m_sortedValues = value; }
            }
            private SortedDictionary<double, int> m_sortedValues;

            public double MaximumValue
            {
                get { return m_maximumValue; }
                set { m_maximumValue = value; }
            }
            private double m_maximumValue;

            public double MinimumValue
            {
                get { return m_minimumValue; }
                set { m_minimumValue = value; }
            }
            private double m_minimumValue;

            public double AverageValue
            {
                get { return m_averageValue; }
                set { m_averageValue = value; }
            }
            private double m_averageValue;

            public double MedianValue
            {
                get { return m_medianValue; }
                set { m_medianValue = value; }
            }
            private double m_medianValue;

            public double ModeValue
            {
                get { return m_modeValue; }
                set { m_modeValue = value; }
            }
            private double m_modeValue;

            public StaticStatistics()
            {
                this.MaximumValue = 0.0;
                this.MinimumValue = double.MaxValue;
                this.AverageValue = 0.0;
                this.StatisticCount = 0;
                this.TotalValue = 0.0;
                this.MedianValue = 0.0;
                this.ModeValue = 0.0;

                this.SortedValues = new SortedDictionary<double, int>();
            }

            public void AddStatistic(double value)
            {
                if (value != 0.0f)
                {
                    if (this.SortedValues.ContainsKey(value))
                    {
                        this.SortedValues[value] += 1;
                    }
                    else
                    {
                        this.SortedValues.Add(value, 1);
                    }                

                    if (this.MaximumValue < value)
                        this.MaximumValue = value;

                    if (this.MinimumValue > value)
                        this.MinimumValue = value;

                    this.TotalValue += value;
                    this.StatisticCount++;

                    if (this.StatisticCount > 0)
                    {
                        this.AverageValue = (float)this.TotalValue / (float)this.StatisticCount;

                        int medianIndex = this.StatisticCount / 2;
                        List<double> sortedList = new List<double>();
                        foreach (KeyValuePair<double, int> sortedValue in this.SortedValues)
                        {
                            for (int i = 0; i < sortedValue.Value; i++)
                            {
                                sortedList.Add(sortedValue.Key);
                            }
                        }
                        this.MedianValue = sortedList[medianIndex];

                        int maxOccurences = 0;

                        foreach (KeyValuePair<double, int> sortedValue in this.SortedValues)
                        {
                            if (sortedValue.Value > maxOccurences)
                            {
                                this.ModeValue = sortedValue.Key;
                                maxOccurences = sortedValue.Value;
                            }
                        }
                    }
                }
            }
        }

        public class SingleStatistic
        {
            public int High
            {
                get { return m_high; }
                set { m_high = value; }
            }
            private int m_high;

            public int Lod
            {
                get { return m_lod; }
                set { m_lod = value; }
            }
            private int m_lod;

            public int SuperLod
            {
                get { return m_superLod; }
                set { m_superLod = value; }
            }
            private int m_superLod;

            public int Total
            {
                get { return m_total; }
                set { m_total = value; }
            }
            private int m_total;

            public SingleStatistic(int high, int lod, int superlod)
            {
                this.High = high;
                this.Lod = lod;
                this.SuperLod = superlod;
                this.Total = high + lod + superlod;
            }

            public int GetStatistic(StatisticLevel type)
            {
                switch (type)
                {
                    case StatisticLevel.HIGH_DETAIL:
                        return this.High;

                    case StatisticLevel.LOD:
                        return this.Lod;

                    case StatisticLevel.SUPER_LOD:
                        return this.SuperLod;

                    case StatisticLevel.TOTAL:
                        return this.Total;
                }

                return this.High;
            }

            public void AddStatistic(int stat, StatisticLevel type)
            {
                switch (type)
                {
                    case StatisticLevel.HIGH_DETAIL:
                        this.High = stat;
                        break;

                    case StatisticLevel.LOD:
                        this.Lod = stat;
                        break;

                    case StatisticLevel.SUPER_LOD:
                        this.SuperLod = stat;
                        break;

                    default:
                        break;
                }
            }
        }

        public class SingleStaticStatistic
        {
            public StaticStatistics High
            {
                get { return m_staticHigh; }
                set { m_staticHigh = value; }
            }
            private StaticStatistics m_staticHigh;

            public StaticStatistics Lod
            {
                get { return m_staticLod; }
                set { m_staticLod = value; }
            }
            private StaticStatistics m_staticLod;

            public StaticStatistics SuperLod
            {
                get { return m_staticSuperLod; }
                set { m_staticSuperLod = value; }
            }
            private StaticStatistics m_staticSuperLod;

            public StaticStatistics Total
            {
                get { return m_staticTotal; }
                set { m_staticTotal = value; }
            }
            private StaticStatistics m_staticTotal;

            public SingleStaticStatistic()
            {
                High = new StaticStatistics();
                Lod = new StaticStatistics();
                SuperLod = new StaticStatistics();
                Total = new StaticStatistics();
            }

            public void AddStatistics(int high, int lod, int superlod)
            {
                High.AddStatistic(high);
                Lod.AddStatistic(lod);
                SuperLod.AddStatistic(superlod);
                Total.AddStatistic(high + lod + superlod);
            }

            public StaticStatistics GetStatistic(StatisticLevel type)
            {
                switch (type)
                {
                    case StatisticLevel.HIGH_DETAIL:
                        return this.High;

                    case StatisticLevel.LOD:
                        return this.Lod;

                    case StatisticLevel.SUPER_LOD:
                        return this.SuperLod;

                    case StatisticLevel.TOTAL:
                        return this.Total;
                }

                return this.High;
            }
        }

        #region Enum

        public enum DrawableType
        {
            UNKNOWN,
            HIGH_DETAIL,
            LOD,
            SUPER_LOD
        }

        public enum StatisticLevel
        {
            HIGH_DETAIL,
            LOD,
            SUPER_LOD,
            TOTAL
        }

        #endregion // Enum

        #region Constants

        public static readonly String TextureDictionaryStat = "Txd Size";
        public static readonly String HighGeoSizeStat = "High Geometry Size";
        public static readonly String HighPolyCountStat = "High Polygon Count";
        public static readonly String LodPolyCountStat = "Lod Polygon Count";
        public static readonly String SuperPolyCountStat = "Super Lod Polygon Count";


        public static readonly List<String> StatisticStrings = new List<String>
        {
            "Txd Size"
            , "High Geometry Size"
            , "High Polygon Count"
            , "Lod Polygon Count"
            , "Super Lod Polygon Count"
        };

        #endregion // Constants

        #region Properties

        /// <summary>
        /// True iff the statistics were present in the scene xml file
        /// </summary>
        public Boolean ValidStatistics
        {
            get { return m_validStatistics; }
            set { m_validStatistics = value; }
        }
        public Boolean m_validStatistics;

        public Dictionary<String, int> UniqueProps
        {
            get { return m_uniqueProps; }
            set { m_uniqueProps = value; }
        }
        private Dictionary<String, int> m_uniqueProps;

        public RSG.Base.Math.BoundingBox2f BoundingBox
        {
            get { return m_boundingBox; }
            set { m_boundingBox = value; }
        }
        private RSG.Base.Math.BoundingBox2f m_boundingBox;

        public List<MapSection> Neighbours
        {
            get { return m_neighbours; }
            set { m_neighbours = value; }
        }
        private List<MapSection> m_neighbours;

        #region Statistics

        /// <summary>
        /// Gets the texture dictionary size for the map component, this size
        /// is got from the uncompressed size in bytes of the itd file in the 
        /// independent data.
        /// </summary>
        /// <remarks>Since the export process strips any textures that have been
        /// promoted out of the itd files this value represents the texture 
        /// dictionary size taking into account the parent txds</remarks>
        public int TxdSize
        {
            get { return m_txdSize; }
            set { m_txdSize = value; }
        }
        private int m_txdSize;

        public SingleStatistic GeometrySize
        {
            get { return m_geometrySize; }
            set { m_geometrySize = value; }
        }
        private SingleStatistic m_geometrySize;

        public SingleStatistic PolygonCount
        {
            get { return m_polygonCount; }
            set { m_polygonCount = value; }
        }
        private SingleStatistic m_polygonCount;

        public SingleStatistic CollisionPolygonCount
        {
            get { return m_collisionPolygonCount; }
            set { m_collisionPolygonCount = value; }
        }
        private SingleStatistic m_collisionPolygonCount;

        public SingleStatistic ObjectCount
        {
            get { return m_objectCount; }
            set { m_objectCount = value; }
        }
        private SingleStatistic m_objectCount;

        public SingleStatistic TxdCount
        {
            get { return m_txdCount; }
            set { m_txdCount = value; }
        }
        private SingleStatistic m_txdCount;

        public SingleStatistic PropCount
        {
            get { return m_propCount; }
            set { m_propCount = value; }
        }
        private SingleStatistic m_propCount;

        public SingleStatistic UniquePropCount
        {
            get { return m_uniquePropCount; }
            set { m_uniquePropCount = value; }
        }
        private SingleStatistic m_uniquePropCount;

        public SingleStatistic MemoryTotal
        {
            get { return m_memoryTotal; }
            set { m_memoryTotal = value; }
        }
        private SingleStatistic m_memoryTotal;

        #region Static Statistics

        public static StaticStatistics TxdStaticSizes
        {
            get { return m_txdStaticSizes; }
            set { m_txdStaticSizes = value; }
        }
        private static StaticStatistics m_txdStaticSizes;

        public static SingleStaticStatistic GeometrySizes
        {
            get { return m_geometrySizes; }
            set { m_geometrySizes = value; }
        }
        private static SingleStaticStatistic m_geometrySizes;

        public static SingleStaticStatistic PolygonCounts
        {
            get { return m_polygonCounts; }
            set { m_polygonCounts = value; }
        }
        private static SingleStaticStatistic m_polygonCounts;

        public static SingleStaticStatistic CollisionPolygonCounts
        {
            get { return m_collisionPolygonCounts; }
            set { m_collisionPolygonCounts = value; }
        }
        private static SingleStaticStatistic m_collisionPolygonCounts;

        public static SingleStaticStatistic ObjectCounts
        {
            get { return m_objectCounts; }
            set { m_objectCounts = value; }
        }
        private static SingleStaticStatistic m_objectCounts;

        public static SingleStaticStatistic TxdCounts
        {
            get { return m_txdCounts; }
            set { m_txdCounts = value; }
        }
        private static SingleStaticStatistic m_txdCounts;

        public static SingleStaticStatistic PropCounts
        {
            get { return m_propCounts; }
            set { m_propCounts = value; }
        }
        private static SingleStaticStatistic m_propCounts;

        public static SingleStaticStatistic UniquePropCounts
        {
            get { return m_uniquePropCounts; }
            set { m_uniquePropCounts = value; }
        }
        private static SingleStaticStatistic m_uniquePropCounts;

        public static SingleStaticStatistic MemoryTotals
        {
            get { return m_memoryTotals; }
            set { m_memoryTotals = value; }
        }
        private static SingleStaticStatistic m_memoryTotals;

        #endregion // Static Statistics

        #endregion // Statistics

        #endregion // Properties

        #region Constructor(s)
        
        /// <summary>
        /// 
        /// </summary>
        static MapStatistics()
        {
            TxdStaticSizes = new StaticStatistics();
            GeometrySizes = new SingleStaticStatistic();
            PolygonCounts = new SingleStaticStatistic();
            ObjectCounts = new SingleStaticStatistic();
            TxdCounts = new SingleStaticStatistic();
            MemoryTotals = new SingleStaticStatistic();
            PropCounts = new SingleStaticStatistic();
            UniquePropCounts = new SingleStaticStatistic();
            CollisionPolygonCounts = new SingleStaticStatistic();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scene"></param>
        public MapStatistics(RSG.SceneXml.Scene scene, MapSection mapSection)
        {
            this.ValidStatistics = scene.Statistics.ValidStatistics;
            this.Neighbours = new List<MapSection>();

            // Create texture dictionary statistics
            this.TxdSize = 0;
            foreach (RSG.SceneXml.Statistics.TxdStats txdStat in scene.Statistics.TxdStats.Values)
            {
                this.TxdSize += (int)txdStat.Size;
            }

            this.ObjectCount = new SingleStatistic(scene.Statistics.TxdStats.Count, 0, 0);
            this.TxdCount = new SingleStatistic(scene.Statistics.TxdStats.Count, 0, 0);

            // Create geometry statistics
            int highGeometrySize = 0;
            int highPolyCount = 0;
            int lodGeometrySize = 0;
            int lodPolyCount = 0;
            int superLodGeometrySize = 0;
            int superLodPolyCount = 0;
            int highObjectCount = 0;
            int lodObjectCount = 0;
            int superLodObjectCount = 0;
            foreach (RSG.SceneXml.Statistics.DrawableStats drawableStat in scene.Statistics.GeometryStats.Values)
            {
                TargetObjectDef drawable = scene.FindObject(drawableStat.Guid);
                DrawableType drawableType = GetDrawableType(drawable);
                switch (drawableType)
                {
                    case DrawableType.HIGH_DETAIL:
                        highGeometrySize += (int)drawableStat.Size;
                        highPolyCount += (int)drawableStat.PolyCount;
                        highObjectCount++;
                        break;
                    case DrawableType.LOD:
                        lodGeometrySize += (int)drawableStat.Size;
                        lodPolyCount += (int)drawableStat.PolyCount;
                        lodObjectCount++;
                        break;
                    case DrawableType.SUPER_LOD:
                        superLodGeometrySize += (int)drawableStat.Size;
                        superLodPolyCount += (int)drawableStat.PolyCount;
                        superLodObjectCount++;
                        break;
                    case DrawableType.UNKNOWN:
                        break;
                }
            }
            this.GeometrySize = new SingleStatistic(highGeometrySize, lodGeometrySize, superLodGeometrySize);
            this.PolygonCount = new SingleStatistic(highPolyCount, lodPolyCount, superLodPolyCount);
            this.ObjectCount = new SingleStatistic(highObjectCount, lodObjectCount, superLodObjectCount);

            // Collision Polygon Count
            int collisionPolygonCount = 0;
            foreach (RSG.SceneXml.Statistics.DrawableStats collisionStat in scene.Statistics.CollisionStats.Values)
            {
                collisionPolygonCount += (int)collisionStat.PolyCount;
            }
            this.CollisionPolygonCount = new SingleStatistic(collisionPolygonCount, 0, 0);

            // Walk the scene and calculate the bounding box for the section, any xref/rsrefs that belong to this section and store the names
            // so that the stats can be filled in later.
            int propCount = 0;
            this.UniqueProps = new Dictionary<String, int>();
            this.BoundingBox = new RSG.Base.Math.BoundingBox2f();
            RSG.Base.Math.Vector2f min = new RSG.Base.Math.Vector2f(float.MaxValue, float.MaxValue);
            RSG.Base.Math.Vector2f max = new RSG.Base.Math.Vector2f(float.MinValue, float.MinValue);
            Boolean validBox = false;
            if (mapSection.ExportInstances == true)
            {
                foreach (TargetObjectDef sceneObject in scene.Objects)
                {
                    if (sceneObject.IsContainer())
                    {
                        foreach (SceneXml.TargetObjectDef child in sceneObject.Children)
                        {
                            if (sceneObject.IsXRef() || sceneObject.IsRefObject())
                            {
                                propCount++;
                                if (!this.UniqueProps.ContainsKey(sceneObject.RefName))
                                {
                                    this.UniqueProps.Add(sceneObject.RefName, 1);
                                }
                                else
                                {
                                    this.UniqueProps[sceneObject.RefName]++;
                                }
                            }

                            if (child.IsObject() && child.WorldBoundingBox != null && mapSection.ExportDefinitions == true)
                            {
                                RSG.Base.Math.Vector3f thisMin = child.WorldBoundingBox.Min;
                                RSG.Base.Math.Vector3f thisMax = child.WorldBoundingBox.Max;

                                if (min.X > thisMin.X)
                                    min.X = thisMin.X;
                                if (min.Y > thisMin.Y)
                                    min.Y = thisMin.Y;

                                if (max.X < thisMax.X)
                                    max.X = thisMax.X;
                                if (max.Y < thisMax.Y)
                                    max.Y = thisMax.Y;

                                validBox = true;
                            }
                        }
                    }

                    if (sceneObject.IsXRef() || sceneObject.IsRefObject())
                    {
                        propCount++;
                        if (!this.UniqueProps.ContainsKey(sceneObject.RefName))
                        {
                            this.UniqueProps.Add(sceneObject.RefName, 1);
                        }
                        else
                        {
                            this.UniqueProps[sceneObject.RefName]++;
                        }
                    }

                    if (sceneObject.IsObject() && sceneObject.WorldBoundingBox != null && mapSection.ExportDefinitions == true)
                    {
                        RSG.Base.Math.Vector2f thisMin = new RSG.Base.Math.Vector2f(sceneObject.WorldBoundingBox.Min.X, sceneObject.WorldBoundingBox.Min.Y);
                        RSG.Base.Math.Vector2f thisMax = new RSG.Base.Math.Vector2f(sceneObject.WorldBoundingBox.Max.X, sceneObject.WorldBoundingBox.Max.Y);

                        if (min.X > thisMin.X)
                            min.X = thisMin.X;
                        if (min.Y > thisMin.Y)
                            min.Y = thisMin.Y;

                        if (max.X < thisMax.X)
                            max.X = thisMax.X;
                        if (max.Y < thisMax.Y)
                            max.Y = thisMax.Y;

                        validBox = true;
                    }
                }
            }
            else
            {
            }

            if (mapSection.ExportInstances == true && mapSection.ExportDefinitions == true && validBox == true)
            {
                this.BoundingBox = new RSG.Base.Math.BoundingBox2f(min, max);
            }
            this.PropCount = new SingleStatistic(0, 0, 0);
            this.UniquePropCount = new SingleStatistic(0, 0, 0);

            this.MemoryTotal = new SingleStatistic(highGeometrySize + lodGeometrySize + superLodGeometrySize + this.TxdSize, 0, 0);

            // Only have static stats for the container files (map containers) so that other files
            // don;t interfer with the averages.
            if (mapSection.ExportInstances == true && mapSection.ExportDefinitions == true)
            {
                MapStatistics.TxdStaticSizes.AddStatistic(this.TxdSize);
                MapStatistics.TxdCounts.AddStatistics(scene.Statistics.TxdStats.Count, 0, 0);
                MapStatistics.GeometrySizes.AddStatistics(highGeometrySize, lodGeometrySize, superLodGeometrySize);
                MapStatistics.PolygonCounts.AddStatistics(highPolyCount, lodPolyCount, superLodPolyCount);
                MapStatistics.ObjectCounts.AddStatistics(highObjectCount, lodObjectCount, superLodObjectCount);
                MapStatistics.MemoryTotals.AddStatistics(highGeometrySize + lodGeometrySize + superLodGeometrySize + this.TxdSize, 0, 0);
            }
        }

        #endregion // Constructor(s)

        /// <summary>
        /// Adds a prop into this map statistics
        /// </summary>
        /// <param name="drawableStat"></param>
        /// <param name="txdStat"></param>
        /// <param name="collisionStat"></param>
        public void AddPropStatisticToSection(String propName, int instanceCount, SceneXml.Statistics.DrawableStats drawableStat, SceneXml.Statistics.DrawableStats collisionStat)
        {
            int propCount = this.PropCount.High + instanceCount;

            if (!this.UniqueProps.ContainsKey(propName))
            {
                this.UniqueProps.Add(propName, 1);
            }
            else
            {
                this.UniqueProps[propName]++;
            }

            this.PropCount = new SingleStatistic(propCount, 0, 0);
            this.UniquePropCount = new SingleStatistic(this.UniqueProps.Count, 0, 0);

            if (collisionStat != null)
            {
                this.CollisionPolygonCount = new SingleStatistic((int)this.CollisionPolygonCount.High, (int)collisionStat.PolyCount * instanceCount, 0);
            }
        }

        /// <summary>
        /// Determine is a object def is a high detail drawable, a lod, or a super lod,
        /// by using the lod hierarchy
        /// </summary>
        /// <returns></returns>
        private DrawableType GetDrawableType(TargetObjectDef drawable)
        {
            try
            {
                if (drawable != null)
                {
                    if (!drawable.HasLODChildren())
                    {
                        return DrawableType.HIGH_DETAIL;
                    }
                    else if (drawable.HasLODParent())
                    {
                        return DrawableType.LOD;
                    }
                    else if (drawable.LOD.Children[0] != null)
                    {
                        if (drawable.LOD.Children[0].HasLODChildren())
                        {
                            return DrawableType.SUPER_LOD;
                        }
                        else if (!drawable.LOD.Children[0].HasLODChildren())
                        {
                            return DrawableType.LOD;
                        }
                    }
                    else if (drawable.LOD.Children[0] == null)
                    {
                        foreach (TargetObjectDef child in drawable.LOD.Children)
                        {
                            if (child != null)                                
                            {
                                if (drawable.LOD.Children[0].HasLODChildren())
                                {
                                    return DrawableType.SUPER_LOD;
                                }
                                else
                                {
                                    return DrawableType.LOD;
                                }
                            }
                        }
                        // Cannot find any of the children in the scene xml guess at lod
                        return DrawableType.LOD;
                    }
                }

                return DrawableType.UNKNOWN;
            }
            catch
            {
                return DrawableType.UNKNOWN;
            }
        }
    }
}
