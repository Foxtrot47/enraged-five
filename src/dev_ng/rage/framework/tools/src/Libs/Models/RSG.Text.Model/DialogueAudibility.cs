﻿//---------------------------------------------------------------------------------------------
// <copyright file="DialogueAudibility.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.Model
{
    using System;
    using System.Diagnostics;
    using System.Xml;
    using RSG.Base.Extensions;
    using RSG.Editor;
    using RSG.Editor.Model;

    /// <summary>
    /// Represents a single audibility value that can be assigned to a audio line.
    /// </summary>
    [DebuggerDisplay("{_name}")]
    public sealed class DialogueAudibility : ModelBase
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="ExportIndex"/> property.
        /// </summary>
        private int _exportIndex;

        /// <summary>
        /// The private field used for the <see cref="ExportString"/> property.
        /// </summary>
        private string _exportString;

        /// <summary>
        /// The private field used for the <see cref="Name"/> property.
        /// </summary>
        private string _name;

        /// <summary>
        /// The private field used for the <see cref="Id"/> property.
        /// </summary>
        private Guid _id;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DialogueAudibility"/> class.
        /// </summary>
        /// <param name="id">
        /// The static id for this audibility that cannot be changed.
        /// </param>
        /// <param name="name">
        /// The name for this audibility.
        /// </param>
        /// <param name="exportIndex">
        /// The index that is used for this value during the export process.
        /// </param>
        /// <param name="exportString">
        /// The string that is used for this value during the export process.
        /// </param>
        public DialogueAudibility(Guid id, string name, int exportIndex, string exportString)
        {
            this._id = id;
            this._name = name;
            this._exportIndex = exportIndex;
            this._exportString = exportString;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="DialogueAudibility"/> class.
        /// </summary>
        /// <param name="reader">
        /// Contains data used to initialise this instance.
        /// </param>
        public DialogueAudibility(XmlReader reader)
        {
            if (reader == null)
            {
                throw new SmartArgumentNullException(() => reader);
            }

            this._exportIndex = -1;
            this.Deserialise(reader);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the index that this audibility is exported with.
        /// </summary>
        public int ExportIndex
        {
            get { return this._exportIndex; }
            set { this.SetProperty(ref this._exportIndex, value); }
        }

        /// <summary>
        /// Gets or sets the string that this audibility is exported with. This string is in
        /// line with the g_SpeechAudNames list in /game/audio/speech/vocalization.cpp.
        /// </summary>
        public string ExportString
        {
            get { return this._exportString; }
            set { this.SetProperty(ref this._exportString, value); }
        }

        /// <summary>
        /// Gets or sets the name for this audibility.
        /// </summary>
        public string Name
        {
            get { return this._name; }
            set { this.SetProperty(ref this._name, value); }
        }

        /// <summary>
        /// Gets the id used by this audibility. This cannot be changed once setup and is how
        /// the audibility can be renamed without any data being lost or manipulated.
        /// </summary>
        public Guid Id
        {
            get { return this._id; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a deep copy of the current instance.
        /// </summary>
        /// <returns>
        /// A deep copy of the current System.Object.
        /// </returns>
        public override object DeepClone()
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Serialises the data contained within this instance onto the specified xml writer.
        /// </summary>
        /// <param name="writer">
        /// The xml writer this instance will be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                Debug.Assert(writer != null, "Unable to serialise into a null writer.");
                return;
            }

            writer.WriteElementString("Name", this._name);
            writer.WriteElementString("Id", this._id.ToString("D"));

            writer.WriteStartElement("ExportIndex");
            writer.WriteAttributeString("value", this._exportIndex.ToStringInvariant());
            writer.WriteEndElement();

            writer.WriteElementString("ExportString", this._exportString);
        }

        /// <summary>
        /// Initialises this instance using the data contained within the specified xml reader.
        /// </summary>
        /// <param name="reader">
        /// The xml reader that contains the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() == XmlNodeType.Element)
            {
                if (String.Equals("Name", reader.Name))
                {
                    this._name = reader.ReadElementContentAsString();
                }
                else if (String.Equals("Id", reader.Name))
                {
                    string value = reader.ReadElementContentAsString();
                    if (!Guid.TryParseExact(value, "D", out this._id))
                    {
                        this._id = Guid.Empty;
                    }
                }
                else if (String.Equals("ExportIndex", reader.Name))
                {
                    string value = reader.GetAttribute("value");
                    if (!int.TryParse(value, out this._exportIndex))
                    {
                        this._exportIndex = -1;
                    }
                }
                else if (String.Equals("ExportString", reader.Name))
                {
                    this._exportString = reader.ReadElementContentAsString();
                }

                reader.Skip();
            }

            if (reader.NodeType == XmlNodeType.EndElement)
            {
                reader.ReadEndElement();
            }
        }
        #endregion Methods
    } // RSG.Text.Model.DialogueAudibility {Class}
} // RSG.Text.Model {Namespace}
