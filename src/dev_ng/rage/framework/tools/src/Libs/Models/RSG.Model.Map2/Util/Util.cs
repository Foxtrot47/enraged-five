﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.ConfigParser;
using RSG.Model.Map;

namespace RSG.Model.Map2.Util
{
    /// <summary>
    /// 
    /// </summary>
    public static class Util
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="gv"></param>
        /// <returns></returns>
        public static ContentNodeMap GetContentNodeForMapSection(MapSection section, ConfigGameView gv)
        {
            return (ContentNodeMap)gv.Content.Root.FindAll(section.Name, "map").FirstOrDefault();
        }
    }
}
