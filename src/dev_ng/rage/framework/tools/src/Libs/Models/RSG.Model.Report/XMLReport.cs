﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace RSG.Model.Report
{
    /// <summary>
    /// XMLReport - A report that generates xml output
    /// </summary>
    public abstract class XMLReport : Report, IReportStreamProvider, IDisposable
    {
        #region Properties
        /// <summary>
        /// Stream containing the report data
        /// </summary>
        public Stream Stream
        {
            get;
            protected set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="desc"></param>
        public XMLReport(string name, string desc)
            : base(name, desc)
        {
        }
        #endregion // Constructor(s)

        #region IDisposable Implementation
        /// <summary>
        /// Ensures that the stream is correctly disposed of
        /// </summary>
        public virtual void Dispose()
        {
            if (Stream != null)
            {
                Stream.Dispose();
            }
        }
        #endregion // IDisposable Implementation
    } // XMLReport
}
