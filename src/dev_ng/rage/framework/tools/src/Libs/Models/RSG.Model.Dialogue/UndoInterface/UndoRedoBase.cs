﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Linq.Expressions;
using RSG.Model.Dialogue.UndoManager;
using RSG.Model.Dialogue.Search;
using RSG.Base.Collections;


namespace RSG.Model.Dialogue.UndoInterface
{
    /// <summary>
    /// The arguments for an undo event handler, including a static empty property
    /// </summary>
    public class UndoEventArgs
    {
        public MemberExpression CallingMethod;
        public UndoSetterDelegate Setter;
        public Object OldValue;
        public Object NewValue;
        public Boolean Collapsable;

        public UndoEventArgs(MemberExpression callingMethod, UndoSetterDelegate setter, Object oldValue, Object newValue, Boolean collapsable)
        {
            this.CallingMethod = callingMethod;
            this.Setter = setter;
            this.OldValue = oldValue;
            this.NewValue = newValue;
            this.Collapsable = collapsable;
        }

        public static UndoEventArgs Empty = new UndoEventArgs(null, null, null, null, false);
    }

    /// <summary>
    /// The delegate function for the undo event handler
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="args"></param>
    public delegate void UndoEventHandler(Object sender, UndoEventArgs args);

    /// <summary>
    /// The base class for undo and redo operations, all has property change events such
    /// that if a property uses the SetPropertyValue function the right changing events 
    /// will be fired to any listeners
    /// </summary>
    public abstract class UndoRedoBase : IUndoRedo , ISearchable
    {
        #region Events

        /// <summary>
        /// Event raised when a property in the ViewModel is about to change.
        /// </summary>
        public event PropertyChangingEventHandler PropertyChanging;

        /// <summary>
        /// Event raised when a property in the ViewModel has changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Dirty State changed
        /// </summary>
        public event PropertyChangedEventHandler DirtyPropertyChanged;

        /// <summary>
        /// Event raised when a undo stack object needs creating
        /// </summary>
        public event UndoEventHandler UndoCreating;

        /// <summary>
        /// Gets called when an undo operation has been reqested
        /// </summary>
        public event UndoEventHandler PerformUndoAction;

        /// <summary>
        /// Gets called when an redo operation has been reqested
        /// </summary>
        public event UndoEventHandler PerformRedoAction;

        #endregion // Events

        #region Properties

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// 
        protected UndoRedoBase()
        {
        }

        #endregion // Constructor(s)

        #region Protected Function(s)

        /// <summary>
        /// This updates a property and makes sure that the current property change events are fired.
        /// It also looks into the attributes of the property and if the property in undoable fires off the correct
        /// undo events so that any undo managers listening will be able to create the undo stack object
        /// </summary>
        /// <typeparam name="T">The type of the property that has changed</typeparam>
        /// <param name="newValue">The new value of the property</param>
        /// <param name="oldValueExpression">The expression of the property so we can get the old value</param>
        /// <param name="setter">The delegate function that will be called to actually update the property</param>
        /// <returns></returns>
        protected T SetPropertyValue<T>(T newValue, Expression<Func<T>> oldValueExpression, UndoSetterDelegate setter)
        {
            // Retrieve the old value for the property
            Func<T> getter = oldValueExpression.Compile();
            T oldValue = getter();

            // In case new and old value both are equal to default 
            // values for that type or are same return 
            if ((Equals(oldValue, default(T)) && Equals(newValue, default(T)))
            || Equals(oldValue, newValue))
            {
                return newValue;
            }

            // Retrieve the property that has changed 
            var method = oldValueExpression.Body as MemberExpression;
            var propertyInfo = method.Member as PropertyInfo;
            String propertyName = propertyInfo.Name;

            // Determine if we need to produce a undo event for this change
            var undoRedoAttributes = propertyInfo.GetCustomAttributes(typeof(Undoable), true);
            if (undoRedoAttributes.Length > 0)
            {
                Boolean collapsable = false;
                var collapsableAttributes = propertyInfo.GetCustomAttributes(typeof(Collapsable), true);
                if (collapsableAttributes.Length > 0)
                {
                    collapsable = true;
                }

                if (UndoCreating != null)
                    UndoCreating(this, new UndoEventArgs(method, setter, oldValue, newValue, collapsable));
            }

            OnPropertyChanging(propertyName);
            setter(newValue);
            OnPropertyChanged(propertyName);

            if (DirtyPropertyChanged != null)
                DirtyPropertyChanged(this, new PropertyChangedEventArgs(propertyName));

            return newValue;
        }

        /// <summary>
        /// Fires the PerformUndoEvent such that any undo managers listening to it 
        /// can perform their undo action
        /// </summary>
        public void Undo()
        {
            if (this.PerformUndoAction != null)
                PerformUndoAction(this, UndoEventArgs.Empty);
        }

        /// <summary>
        /// Fires the PerformRedoEvent such that any undo managers listening to it 
        /// can perform their redo action
        /// </summary>
        public void Redo()
        {
            if (this.PerformRedoAction != null)
                PerformRedoAction(this, UndoEventArgs.Empty);
        }

        #endregion // Protected Function(s)

        #region Protected Helper Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnPropertyChanging(PropertyChangingEventArgs e)
        {
            PropertyChangingEventHandler handler = this.PropertyChanging;
            if (null != handler)
                handler(this, e);
        }

        /// <summary>
        /// Method to allow easy raising of the OnPropertyChanging event.
        /// </summary>
        /// <param name="propertyName"></param>
        public void OnPropertyChanging(String propertyName)
        {
            VerifyPropertyName(propertyName);
            OnPropertyChanging(new PropertyChangingEventArgs(propertyName));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (null != handler)
                handler(this, e);
        }

        /// <summary>
        /// Method to allow easy raising of the OnPropertyChanged event.
        /// </summary>
        /// <param name="propertyName"></param>
        public void OnPropertyChanged(String propertyName)
        {
            VerifyPropertyName(propertyName);
            OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyName"></param>
        protected void VerifyPropertyName(String propertyName)
        {
            if (null == TypeDescriptor.GetProperties(this)[propertyName])
            {
                String msg = String.Format("Invalid property name: '{0}'.", propertyName);
            }
        }

        /// <summary>
        /// Call this method to raise the PropertyChanged event when
        /// a property changes.  Note that you should use the
        /// NotifyPropertyChangedHelper class to create a cached
        /// copy of the PropertyChangedEventArgs object to pass
        /// into this method.  Usage:
        /// 
        /// static readonly PropertyChangedEventArgs m_$PropertyName$Args = 
        ///     NotifyPropertyChangedHelper.CreateArgs<$ClassName$>(o => o.$PropertyName$);
        /// 
        /// In your property setter:
        ///     PropertyChanged(this, m_$PropertyName$Args)
        /// 
        /// </summary>
        /// <param name="e">A cached event args object</param>
        protected void NotifyPropertyChanged(PropertyChangedEventArgs e)
        {
            var evt = PropertyChanged;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        #endregion // Protected Helper Methods

        #region ISearchable Implementation

        /// <summary>
        /// Cycles through the properties in this object and determines if any of them match the given search parameters. This 
        /// function also goes through all the children objects that are of type Isearchable and does the same thing with them.
        /// </summary>
        /// <param name="searchField">The name of the propery (fixed name or display name) that we are looking for.</param>
        /// <param name="searchType">The type of the search, whether it be a numeric search or a string search etc.</param>
        /// <param name="comparisonOptions">What sort of comparison should be done to determine if a property is the correct value</param>
        /// <param name="searchOptions">Any additional options that this search should have</param>
        /// <param name="searchText">The actual string that we are searching for, this could be a string represetation of a numeric value or a boolean value</param>
        /// <returns>A list of models that have properties in them that match the search parameters.</returns>
        public List<SearchResult> FindAll(String searchField, SearchType searchType, ComparisonOptions comparisonOptions, SearchOptions searchOptions, String searchText)
        {
            List<SearchResult> results = new List<SearchResult>();

            Boolean ignoreCase = ((searchOptions & SearchOptions.IgnoreCase) == SearchOptions.IgnoreCase) ? true : false;

            // Search text could be a pattern as we support wildcard searchs so change this into the reg version
            // so we can use expression.
            if (ignoreCase == true)
                searchText = searchText.ToLower();
            String pattern = Regex.Escape(searchText).Replace("*", ".*").Replace("?", ".");
            Regex expression = null;
            if (String.Compare(pattern, searchText) != 0)
            {
                pattern.Insert(0, "^");
                pattern.Insert(pattern.Length - 1, "$");
                if (ignoreCase == true)
                {
                    expression = new Regex(pattern, RegexOptions.IgnoreCase);
                }
                else
                {
                    expression = new Regex(pattern);
                }
            }

            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(this);
            foreach (PropertyDescriptor property in properties)
            {
                String displayName = property.Name;
                if (property.Attributes[typeof(System.ComponentModel.DisplayNameAttribute)] != null)
                {
                    System.ComponentModel.DisplayNameAttribute displayNameAttr = (System.ComponentModel.DisplayNameAttribute)property.Attributes[typeof(System.ComponentModel.DisplayNameAttribute)];
                    if (displayNameAttr.DisplayName != "")
                    {
                        displayName = displayNameAttr.DisplayName;
                    }
                }

                if (searchField == property.Name || searchField == displayName)
                {
                    // Make sure the non searchable property attribute isn't set
                    var nonSearchableAttribute = property.ComponentType.GetProperty(property.Name).GetCustomAttributes(typeof(NonSearchable), true);
                    if (nonSearchableAttribute.Count() == 0)
                    {
                        // Get the value for this property
                        Object value = property.GetValue(this);

                        switch (comparisonOptions)
                        {
                            case ComparisonOptions.Contains:
                                TestPropertyContains(property, searchType, ignoreCase, expression, searchText, results);
                                break;

                            case ComparisonOptions.StartsWith:
                                TestPropertyStartsWith(property, searchType, ignoreCase, expression, searchText, results);
                                break;

                            case ComparisonOptions.EndsWith:
                                TestPropertyEndsWith(property, searchType, ignoreCase, expression, searchText, results);
                                break;

                            case ComparisonOptions.EqualTo:
                                TestPropertyEquals(property, searchType, ignoreCase, expression, searchText, results);
                                break;

                            case ComparisonOptions.NotEqualTo:
                                TestPropertyDoesntEqual(property, searchType, ignoreCase, expression, searchText, results);
                                break;

                            case ComparisonOptions.GreaterThen:
                                TestPropertyGreaterThan(property, searchType, ignoreCase, expression, searchText, results);
                                break;

                            case ComparisonOptions.LessThan:
                                TestPropertyLessThan(property, searchType, ignoreCase, expression, searchText, results);
                                break;

                            default:
                                break;
                        }
                        break;
                    }
                }

                if (typeof(System.Collections.ICollection).IsAssignableFrom(property.PropertyType) ||
                    typeof(ICollection<>).IsAssignableFrom(property.PropertyType))
                {
                    Type[] genericTypes = property.PropertyType.GetGenericArguments();
                    foreach (Type genericType in genericTypes)
                    {
                        var propertyValue = property.GetValue(this);
                        foreach (var item in propertyValue as System.Collections.ICollection)
                        {
                            if (item.GetType().GetInterface(typeof(ISearchable).FullName) != null)
                            {
                                List<SearchResult> childrenResults = (item as ISearchable).FindAll(searchField, searchType, comparisonOptions, searchOptions, searchText);
                                results.AddRange(childrenResults);
                            }
                        }
                    }
                }
            }
 
            return results;
        }

        /// <summary>
        /// Determines if the given property contains the search string by using the given search type.
        /// </summary>
        /// <param name="property">The property to test, got by using reflection</param>
        /// <param name="searchType">The type of search we are doing</param>
        /// <param name="ignoreCase">Boolean value represeting if any string comparison should be done using case sensitive equations</param>
        /// <param name="expression">The expression if we are using wildcards</param>
        /// <param name="searchText">The actual string that we are searching for, this could be a string represetation of a numeric value or a boolean value</param>
        /// <param name="results">The list that if this property passes will have the search result added to it.</param>
        private void TestPropertyContains(PropertyDescriptor property, SearchType searchType, Boolean ignoreCase, Regex expression, String searchText, List<SearchResult> results)
        {
            var value = property.GetValue(this);

            // Make sure that this properties value is actually a string
            if (value is String)
            {
                if (expression != null)
                {
                    if (ignoreCase == false)
                    {
                        if (expression.IsMatch((value as String)))
                        {
                            results.Add(new SearchResult(this));
                            return;
                        }
                    }
                    else
                    {
                        String lowerValue = (value as String).ToLower();
                        if (expression.IsMatch(lowerValue))
                        {
                            results.Add(new SearchResult(this));
                            return;
                        }
                    }
                }
                else
                {
                    if (ignoreCase == false)
                    {
                        if ((value as String).Contains(searchText))
                        {
                            results.Add(new SearchResult(this));
                            return;
                        }
                    }
                    else
                    {
                        String lowerValue = (value as String).ToLower();
                        if (lowerValue.Contains(searchText))
                        {
                            results.Add(new SearchResult(this));
                            return;
                        }
                    }
                }
            }

            return;
        }

        /// <summary>
        /// Determines if the given property starts with the search string by using the given search type.
        /// </summary>
        /// <param name="property">The property to test, got by using reflection</param>
        /// <param name="searchType">The type of search we are doing</param>
        /// <param name="ignoreCase">Boolean value represeting if any string comparison should be done using case sensitive equations</param>
        /// <param name="expression">The expression if we are using wildcards</param>
        /// <param name="searchText">The actual string that we are searching for, this could be a string represetation of a numeric value or a boolean value</param>
        /// <param name="results">The list that if this property passes will have the search result added to it.</param>
        private void TestPropertyStartsWith(PropertyDescriptor property, SearchType searchType, Boolean ignoreCase, Regex expression, String searchText, List<SearchResult> results)
        {
            var value = property.GetValue(this);

            // Make sure that this properties value is actually a string
            if (value is String)
            {
                if (expression != null)
                {
                    if (expression.IsMatch((value as String)))
                    {
                        results.Add(new SearchResult(this));
                        return;
                    }
                }
                else
                {
                    if ((value as String).StartsWith(searchText, ignoreCase, null))
                    {
                        results.Add(new SearchResult(this));
                        return;
                    }
                }
            }

            return;
        }

        /// <summary>
        /// Determines if the given property ends with the search string by using the given search type.
        /// </summary>
        /// <param name="property">The property to test, got by using reflection</param>
        /// <param name="searchType">The type of search we are doing</param>
        /// <param name="ignoreCase">Boolean value represeting if any string comparison should be done using case sensitive equations</param>
        /// <param name="expression">The expression if we are using wildcards</param>
        /// <param name="searchText">The actual string that we are searching for, this could be a string represetation of a numeric value or a boolean value</param>
        /// <param name="results">The list that if this property passes will have the search result added to it.</param>
        private void TestPropertyEndsWith(PropertyDescriptor property, SearchType searchType, Boolean ignoreCase, Regex expression, String searchText, List<SearchResult> results)
        {
            var value = property.GetValue(this);

            // Make sure that this properties value is actually a string
            if (value is String)
            {
                if (expression != null)
                {
                    if (expression.IsMatch((value as String)))
                    {
                        results.Add(new SearchResult(this));
                        return;
                    }
                }
                else
                {
                    if ((value as String).EndsWith(searchText, ignoreCase, null))
                    {
                        results.Add(new SearchResult(this));
                        return;
                    }
                }
            }

            return;
        }

        /// <summary>
        /// Determines if the given property is equal to the search string by using the given search type.
        /// </summary>
        /// <param name="property">The property to test, got by using reflection</param>
        /// <param name="searchType">The type of search we are doing</param>
        /// <param name="ignoreCase">Boolean value represeting if any string comparison should be done using case sensitive equations</param>
        /// <param name="expression">The expression if we are using wildcards</param>
        /// <param name="searchText">The actual string that we are searching for, this could be a string represetation of a numeric value or a boolean value</param>
        /// <param name="results">The list that if this property passes will have the search result added to it.</param>
        private void TestPropertyEquals(PropertyDescriptor property, SearchType searchType, Boolean ignoreCase, Regex expression, String searchText, List<SearchResult> results)
        {
            var value = property.GetValue(this);

            switch (searchType)
            {
                case SearchType.Search_Boolean_Type:
                    if (value is Boolean)
                    {
                        Boolean searchBoolean;
                        Boolean.TryParse(searchText, out searchBoolean);
                        if (searchBoolean == value as Boolean?)
                        {
                            results.Add(new SearchResult(this));
                            return;
                        }
                    }
                    break;

                case SearchType.Search_Enum_Type:
                    break;

                case SearchType.Search_Numeric_Type:
                    Decimal searchDecimal = new Decimal();
                    Decimal decimalValue = new Decimal();
                    Decimal.TryParse(searchText, out searchDecimal);
                    Boolean valueFound = false;
                    if (value is Int16)
                    {
                        decimalValue = new Decimal((Int16)value);
                        valueFound = true;
                    }
                    else if (value is UInt16)
                    {
                        decimalValue = new Decimal((UInt16)value);
                        valueFound = true;
                    }
                    else if (value is Int32)
                    {
                        decimalValue = new Decimal((Int32)value);
                        valueFound = true;
                    }
                    else if (value is UInt32)
                    {
                        decimalValue = new Decimal((UInt32)value);
                        valueFound = true;
                    }
                    else if (value is Int64)
                    {
                        decimalValue = new Decimal((Int64)value);
                        valueFound = true;
                    }
                    else if (value is UInt64)
                    {
                        decimalValue = new Decimal((UInt64)value);
                        valueFound = true;
                    }
                    else if (value is float)
                    {
                        decimalValue = new Decimal((float)value);
                        valueFound = true;
                    }
                    else if (value is double)
                    {
                        decimalValue = new Decimal((double)value);
                        valueFound = true;
                    }
                    if (valueFound == true)
                    {
                        int comparison = decimalValue.CompareTo(searchDecimal);
                        if (comparison == 0)
                        {
                            results.Add(new SearchResult(this));
                            return;
                        }
                    }
                    break;

                case SearchType.Search_String_Type:
                    if (value is String)
                    {
                        if (expression != null)
                        {
                            if (expression.IsMatch((value as String)))
                            {
                                results.Add(new SearchResult(this));
                                return;
                            }
                        }
                        else
                        {
                            if (String.Compare((value as String), searchText, ignoreCase) == 0)
                            {
                                results.Add(new SearchResult(this));
                                return;
                            }
                        }
                    }
                    break;

                case SearchType.Search_None_Type:
                    break;

                default:
                    break;
            }
            return;
        }

        /// <summary>
        /// Determines if the given property isn't equal to the search string by using the given search type.
        /// </summary>
        /// <param name="property">The property to test, got by using reflection</param>
        /// <param name="searchType">The type of search we are doing</param>
        /// <param name="ignoreCase">Boolean value represeting if any string comparison should be done using case sensitive equations</param>
        /// <param name="expression">The expression if we are using wildcards</param>
        /// <param name="searchText">The actual string that we are searching for, this could be a string represetation of a numeric value or a boolean value</param>
        /// <param name="results">The list that if this property passes will have the search result added to it.</param>
        private void TestPropertyDoesntEqual(PropertyDescriptor property, SearchType searchType, Boolean ignoreCase, Regex expression, String searchText, List<SearchResult> results)
        {
            var value = property.GetValue(this);

            switch (searchType)
            {
                case SearchType.Search_Boolean_Type:
                    if (value is Boolean)
                    {
                        Boolean searchBoolean;
                        Boolean.TryParse(searchText, out searchBoolean);
                        if (searchBoolean != value as Boolean?)
                        {
                            results.Add(new SearchResult(this));
                            return;
                        }
                    }
                    break;

                case SearchType.Search_Enum_Type:
                    break;

                case SearchType.Search_Numeric_Type:
                    Decimal searchDecimal = new Decimal();
                    Decimal decimalValue = new Decimal();
                    Decimal.TryParse(searchText, out searchDecimal);
                    Boolean valueFound = false;
                    if (value is Int16)
                    {
                        decimalValue = new Decimal((Int16)value);
                        valueFound = true;
                    }
                    else if (value is UInt16)
                    {
                        decimalValue = new Decimal((UInt16)value);
                        valueFound = true;
                    }
                    else if (value is Int32)
                    {
                        decimalValue = new Decimal((Int32)value);
                        valueFound = true;
                    }
                    else if (value is UInt32)
                    {
                        decimalValue = new Decimal((UInt32)value);
                        valueFound = true;
                    }
                    else if (value is Int64)
                    {
                        decimalValue = new Decimal((Int64)value);
                        valueFound = true;
                    }
                    else if (value is UInt64)
                    {
                        decimalValue = new Decimal((UInt64)value);
                        valueFound = true;
                    }
                    else if (value is float)
                    {
                        decimalValue = new Decimal((float)value);
                        valueFound = true;
                    }
                    else if (value is double)
                    {
                        decimalValue = new Decimal((double)value);
                        valueFound = true;
                    }
                    if (valueFound == true)
                    {
                        int comparison = decimalValue.CompareTo(searchDecimal);
                        if (comparison != 0)
                        {
                            results.Add(new SearchResult(this));
                            return;
                        }
                    }
                    break;

                case SearchType.Search_String_Type:
                    if (value is String)
                    {
                        if (expression != null)
                        {
                            if (!expression.IsMatch((value as String)))
                            {
                                results.Add(new SearchResult(this));
                                return;
                            }
                        }
                        else
                        {
                            if (String.Compare((value as String), searchText, ignoreCase) != 0)
                            {
                                results.Add(new SearchResult(this));
                                return;
                            }
                        }
                    }
                    break;

                case SearchType.Search_None_Type:
                    break;

                default:
                    break;
            }
            return;
        }

        /// <summary>
        /// Determines if the given property is less than the search string by using the given search type.
        /// </summary>
        /// <param name="property">The property to test, got by using reflection</param>
        /// <param name="searchType">The type of search we are doing</param>
        /// <param name="ignoreCase">Boolean value represeting if any string comparison should be done using case sensitive equations</param>
        /// <param name="expression">The expression if we are using wildcards</param>
        /// <param name="searchText">The actual string that we are searching for, this could be a string represetation of a numeric value or a boolean value</param>
        /// <param name="results">The list that if this property passes will have the search result added to it.</param>
        private void TestPropertyLessThan(PropertyDescriptor property, SearchType searchType, Boolean ignoreCase, Regex expression, String searchText, List<SearchResult> results)
        {
            var value = property.GetValue(this);

            switch (searchType)
            {
                case SearchType.Search_Boolean_Type:
                    break;

                case SearchType.Search_Enum_Type:
                    break;

                case SearchType.Search_Numeric_Type:
                    Decimal searchDecimal = new Decimal();
                    Decimal decimalValue = new Decimal();
                    Decimal.TryParse(searchText, out searchDecimal);
                    Boolean valueFound = false;
                    if (value is Int16)
                    {
                        decimalValue = new Decimal((Int16)value);
                        valueFound = true;
                    }
                    else if (value is UInt16)
                    {
                        decimalValue = new Decimal((UInt16)value);
                        valueFound = true;
                    }
                    else if (value is Int32)
                    {
                        decimalValue = new Decimal((Int32)value);
                        valueFound = true;
                    }
                    else if (value is UInt32)
                    {
                        decimalValue = new Decimal((UInt32)value);
                        valueFound = true;
                    }
                    else if (value is Int64)
                    {
                        decimalValue = new Decimal((Int64)value);
                        valueFound = true;
                    }
                    else if (value is UInt64)
                    {
                        decimalValue = new Decimal((UInt64)value);
                        valueFound = true;
                    }
                    else if (value is float)
                    {
                        decimalValue = new Decimal((float)value);
                        valueFound = true;
                    }
                    else if (value is double)
                    {
                        decimalValue = new Decimal((double)value);
                        valueFound = true;
                    }
                    if (valueFound == true)
                    {
                        int comparison = decimalValue.CompareTo(searchDecimal);
                        if (comparison < 0)
                        {
                            results.Add(new SearchResult(this));
                            return;
                        }
                    }
                    break;

                case SearchType.Search_String_Type:
                    break;

                case SearchType.Search_None_Type:
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// Determines if the given property is greater than the search string by using the given search type.
        /// </summary>
        /// <param name="property">The property to test, got by using reflection</param>
        /// <param name="searchType">The type of search we are doing</param>
        /// <param name="ignoreCase">Boolean value represeting if any string comparison should be done using case sensitive equations</param>
        /// <param name="expression">The expression if we are using wildcards</param>
        /// <param name="searchText">The actual string that we are searching for, this could be a string represetation of a numeric value or a boolean value</param>
        /// <param name="results">The list that if this property passes will have the search result added to it.</param>
        private void TestPropertyGreaterThan(PropertyDescriptor property, SearchType searchType, Boolean ignoreCase, Regex expression, String searchText, List<SearchResult> results)
        {
            var value = property.GetValue(this);

            switch (searchType)
            {
                case SearchType.Search_Boolean_Type:
                    break;

                case SearchType.Search_Enum_Type:
                    break;

                case SearchType.Search_Numeric_Type:
                    Decimal searchDecimal = new Decimal();
                    Decimal decimalValue = new Decimal();
                    Decimal.TryParse(searchText, out searchDecimal);
                    Boolean valueFound = false;
                    if (value is Int16)
                    {
                        decimalValue = new Decimal((Int16)value);
                        valueFound = true;
                    }
                    else if (value is UInt16)
                    {
                        decimalValue = new Decimal((UInt16)value);
                        valueFound = true;
                    }
                    else if (value is Int32)
                    {
                        decimalValue = new Decimal((Int32)value);
                        valueFound = true;
                    }
                    else if (value is UInt32)
                    {
                        decimalValue = new Decimal((UInt32)value);
                        valueFound = true;
                    }
                    else if (value is Int64)
                    {
                        decimalValue = new Decimal((Int64)value);
                        valueFound = true;
                    }
                    else if (value is UInt64)
                    {
                        decimalValue = new Decimal((UInt64)value);
                        valueFound = true;
                    }
                    else if (value is float)
                    {
                        decimalValue = new Decimal((float)value);
                        valueFound = true;
                    }
                    else if (value is double)
                    {
                        decimalValue = new Decimal((double)value);
                        valueFound = true;
                    }
                    if (valueFound == true)
                    {
                        int comparison = decimalValue.CompareTo(searchDecimal);
                        if (comparison > 0)
                        {
                            results.Add(new SearchResult(this));
                            return;
                        }
                    }
                    break;

                case SearchType.Search_String_Type:
                    break;

                case SearchType.Search_None_Type:
                    break;

                default:
                    break;
            }
        }

        #endregion // ISearchable Implementation
    }
}
