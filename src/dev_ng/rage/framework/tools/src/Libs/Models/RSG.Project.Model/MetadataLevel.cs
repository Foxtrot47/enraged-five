﻿//---------------------------------------------------------------------------------------------
// <copyright file="MetadataLevel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.Model
{
    /// <summary>
    /// Defines the different levels to a projects metadata where the integer value determines
    /// the override ordering.
    /// </summary>
    public enum MetadataLevel
    {
        /// <summary>
        /// Defines the metadata attached to the core project file.
        /// </summary>
        [MetadataLevelExtension("")]
        [MetadataLevelExcluded("Filter")]
        Core = 0,

        /// <summary>
        /// Defines the metadata attached to the filters project file.
        /// </summary>
        [MetadataLevelExtension(".filterdata")]
        Filter = 1,

        /// <summary>
        /// Defines the metadata attached to the users project file.
        /// </summary>
        [MetadataLevelExtension(".userdata")]
        User = 2,
    } // RSG.Project.Model.MetadataLevel {Enum}
} // RSG.Project.Model {Namespace}
