﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Report.Reports.GameStatsReports.Tests;
using RSG.Model.Report.Reports.GameStatsReports;
using RSG.Model.Statistics.Captures;

namespace RSG.Model.Report.Reports.GameStatsReports.Tests
{
    /// <summary>
    /// 
    /// </summary>
    public class ScriptMemSmokeTest : SmokeTestBase
    {
        #region Constants
        private const string c_name = "Script Memory";
        #endregion // Constants

        #region Private Classes
        /// <summary>
        /// Contains information about a single script mem stat
        /// </summary>
        public class ScriptMemInfo
        {
            public ScriptMemResult Result { get; private set; }
            public DateTime Timestamp { get; private set; }

            public ScriptMemInfo(ScriptMemResult result, DateTime time)
            {
                Result = result;
                Timestamp = time;
            }
        }
        #endregion // Private Classes

        #region Properties
        /// <summary>
        /// Fps info grouped by test name
        /// </summary>
        public IDictionary<string, IList<ScriptMemInfo>> GroupedMemStatInfo
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public ScriptMemSmokeTest()
            : base(c_name)
        {
        }
        #endregion // Constructor(s)

        #region SmokeTestBase Methods
        /// <summary>
        /// Executes the particular test
        /// </summary>
        /// <param name="testSessions">List of test sessions</param>
        public override void RunTest(IList<TestSession> testSessions)
        {
            base.RunTest(testSessions);

            // Gather the results we are interested in
            GroupedMemStatInfo = GatherMemStatResults(testSessions);

            // Check if we have any errors/warnings/messages to report
            foreach (KeyValuePair<string, IList<ScriptMemInfo>> pair in GroupedMemStatInfo)
            {
                string testName = pair.Key;

                ScriptMemInfo first = pair.Value.FirstOrDefault();
                ScriptMemInfo next = null;
                if (pair.Value.Count > 1)
                {
                    next = pair.Value[1];
                }

                if (first != null && next != null)
                {
                    if (first.Result.Physical.Average != 0.0f && next.Result.Physical.Average != 0.0f)
                    {
                        float averageGrowth = next.Result.Physical.Average / first.Result.Physical.Average;
                        if (averageGrowth > 1.05f)
                        {
                            Messages.Add(String.Format("{0} uses {1:0.##}% less script memory", testName, -(averageGrowth - 1.0) * 100));
                        }

                        if (averageGrowth < 0.95f)
                        {
                            Errors.Add(String.Format("{0} uses {1:0.##}% more physical script memory", testName, (averageGrowth - 1.0) * 100));
                        }
                    }

                    if (first.Result.Virtual.Average != 0.0f && next.Result.Virtual.Average != 0.0f)
                    {
                        float averageGrowth = next.Result.Virtual.Average / first.Result.Virtual.Average;
                        if (averageGrowth > 1.05f)
                        {
                            Messages.Add(String.Format("{0} uses {1:0.##}% less script memory", testName, -(averageGrowth - 1.0) * 100));
                        }

                        if (averageGrowth < 0.95f)
                        {
                            Errors.Add(String.Format("{0} uses {1:0.##}% more virtual script memory", testName, (averageGrowth - 1.0) * 100));
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Generates a graph for this smoke test
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns>Null if the smoke test doesn't require a graph</returns>
        public override IList<GraphImage> GenerateGraphs(DateTime start, DateTime end)
        {
            return null;
        }
        #endregion // SmokeTestBase Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="testSessions"></param>
        /// <returns></returns>
        private Dictionary<string, IList<ScriptMemInfo>> GatherMemStatResults(IList<TestSession> testSessions)
        {
            Dictionary<string, IList<ScriptMemInfo>> groupedMemStatInfo = new Dictionary<string, IList<ScriptMemInfo>>();

            foreach (TestSession session in testSessions)
            {
                ISet<string> encounteredTests = new HashSet<string>();

                foreach (TestResults test in session.TestResults)
                {
                    if (encounteredTests.Contains(test.Name))
                    {
                        continue;
                    }
                    encounteredTests.Add(test.Name);

                    if (!groupedMemStatInfo.ContainsKey(test.Name))
                    {
                        groupedMemStatInfo.Add(test.Name, new List<ScriptMemInfo>());
                    }

                    ScriptMemInfo info = new ScriptMemInfo(test.ScriptMemResult, session.Timestamp);
                    groupedMemStatInfo[test.Name].Add(info);
                }
            }

            return groupedMemStatInfo;
        }
        #endregion // Private Methods
    } // ScriptMemSmokeTest
}
