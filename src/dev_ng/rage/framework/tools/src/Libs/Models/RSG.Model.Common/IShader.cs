﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Collections;

namespace RSG.Model.Common
{
    /// <summary>
    /// 
    /// </summary>
    public interface IShader : IAsset, IEquatable<IShader>, IComparable<IShader>
    {
        /// <summary>
        /// List of textures the shader uses
        /// </summary>
        ObservableCollection<ITexture> Textures { get; }
    }
}
