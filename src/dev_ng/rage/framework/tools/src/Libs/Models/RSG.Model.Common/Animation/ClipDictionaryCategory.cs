﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Model.Common.Animation
{
    /// <summary>
    /// Clip dictionary type enumeration.
    /// </summary>
    [DataContract]
    public enum ClipDictionaryCategory
    {
        [EnumMember]
        ClipMax,

        [EnumMember]
        Cutscene,

        [EnumMember]
        InGame,

        [EnumMember]
        Other
    } // ClipDictionaryCategory
}
