﻿//---------------------------------------------------------------------------------------------
// <copyright file="ILine.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.Model
{
    using System;
using System.Xml;
using RSG.Editor;
using RSG.Editor.Model;

    /// <summary>
    /// When implemented represents a single line of dialogue within a
    /// <see cref="Conversation"/> that is tied to an audio file.
    /// </summary>
    public interface ILine : IModel
    {
        #region Events
        /// <summary>
        /// Occurs whenever the character id for this line changes.
        /// </summary>
        event EventHandler<ValueChangedEventArgs<Guid>> CharacterIdChanged;
        #endregion Events
        
        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether this line always gets subtitled even when
        /// they are turned off.
        /// </summary>
        bool AlwaysSubtitled { get; set; }

        /// <summary>
        /// Gets or sets the identifier of the audibility value for this line.
        /// </summary>
        Guid Audibility { get; set; }

        /// <summary>
        /// Gets or sets the file path to the audio file containing the data for this line.
        /// </summary>
        string AudioFilepath { get; set; }

        /// <summary>
        /// Gets or sets the identifier of the type of audio this line uses.
        /// </summary>
        Guid AudioType { get; set; }

        /// <summary>
        /// Gets or sets the identifier of the character that is speaking this line.
        /// </summary>
        Guid CharacterId { get; set; }

        /// <summary>
        /// Gets the dialogue configurations that have been associated with this line.
        /// </summary>
        DialogueConfigurations Configurations { get; }

        /// <summary>
        /// Gets the conversation this line belongs to.
        /// </summary>
        Conversation Conversation { get; }

        /// <summary>
        /// Gets or sets the actual dialogue for this line.
        /// </summary>
        string Dialogue { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not the audio for this line can be
        /// interrupted by the special ability.
        /// </summary>
        bool DontInterruptForSpecialAbility { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not this line ducks the radio.
        /// </summary>
        bool DucksRadio { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not this line ducks the score.
        /// </summary>
        bool DucksScore { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not this line goes through the headset
        /// sub-mix.
        /// </summary>
        bool HeadsetSubmix { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the audio for this line can be interrupt.
        /// </summary>
        bool Interruptible { get; set; }

        /// <summary>
        /// Gets or sets the date and time this line was last modified.
        /// </summary>
        DateTime LastModifiedTime { get; set; }

        /// <summary>
        /// Gets or sets the index value of the listener of this line.
        /// </summary>
        int Listener { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the audio file-path has been entered
        /// manually by the user as suppose to being automatically generated.
        /// </summary>
        bool ManualFilename { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this line should be spoken out of the
        /// pad speaker.
        /// </summary>
        bool PadSpeaker { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this line has been recorded.
        /// </summary>
        bool Recorded { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this line has been sent to be recorded.
        /// </summary>
        bool SentForRecording { get; set; }

        /// <summary>
        /// Gets the index value of the speaker for this line.
        /// </summary>
        int Speaker { get; }

        /// <summary>
        /// Gets or sets the identifier of the special field for this line.
        /// </summary>
        Guid Special { get; set; }

        /// <summary>
        /// Gets the voice name that has been assigned to the line. This is the voice that is
        /// used while recording the line.
        /// </summary>
        string VoiceName { get; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Clones this line object and sets up its parent to be the specified conversation.
        /// </summary>
        /// <param name="conversation">
        /// The conversation that is to be the cloned objects parent.
        /// </param>
        /// <returns>
        /// The newly cloned line object.
        /// </returns>
        ILine DeepClone(Conversation conversation);

        /// <summary>
        /// Serialises the data contained within this instance onto the specified xml writer.
        /// </summary>
        /// <param name="writer">
        /// The xml writer this instance will be serialised to.
        /// </param>
        void Serialise(XmlWriter writer);

        /// <summary>
        /// Gets a disposable object that is used to suspend the update to the modified time
        /// property due to a property change.
        /// </summary>
        /// <returns>
        /// The disposable object that is used to suspend the update to the modified time.
        /// </returns>
        DisposableObject SuspendModifiedTimeUpdater();
        #endregion Methods
    } // RSG.Text.Model.ILine {Interface}
} // RSG.Text.Model {Namespace}
