﻿namespace RSG.Model.Report2.Reports.GameAssetUsage
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Web.UI;
    using RSG.Base.Configuration;
    using RSG.Base.Tasks;
    using RSG.ManagedRage;
    using RSG.Metadata.Model;
    using RSG.Model.Character.Game;
    using RSG.Model.Common;
    using RSG.Model.Report;
    using RSG.Platform;

    /// <summary>
    /// The report that provides details of how the vehicle assets are being used in the game
    /// including lists that detail assets that are not referenced or cannot be resolved.
    /// </summary>
    public class CharacterAssetUsageReport
        : RSG.Model.Report.Report, IReportFileProvider, IDynamicLevelReport
    {
        #region Fields
        /// <summary>
        /// The name of this report.
        /// </summary>
        private const string ReportName = "Character Asset Usage";

        /// <summary>
        /// The description for this report.
        /// </summary>
        private const string ReportDescription = "Provides lists detailing the character asset usage in the game.";

        /// <summary>
        /// The private field used for the <see cref="GenerationTask"/> property.
        /// </summary>
        private ITask _generationTask;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CharacterAssetUsageReport"/> class.
        /// </summary>
        public CharacterAssetUsageReport()
        {
            this.Name = ReportName;
            this.Description = ReportDescription;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Filter used for the save dialog
        /// </summary>
        public string Filter
        {
            get
            {
                return "HTML (Html File)|*.html";
            }
        }

        /// <summary>
        /// Extensions that this report support
        /// </summary>
        public string SupportedExtension
        {
            get
            {
                return ".html";
            }
        }

        /// <summary>
        /// Filename specifying where a report should be output
        /// </summary>
        public string Filename
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the task to run for generating this report.
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (this._generationTask == null)
                {
                    this._generationTask = new ActionTask(
                        "Generating report", this.GenerateReport);
                }

                return this._generationTask;
            }
        }

        public IEnumerable<StreamableStat> RequiredStats
        {
            get { return Enumerable.Empty<StreamableStat>(); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Generates the report dynamically.
        /// </summary>
        /// <param name="context">
        /// The context for this asynchronous task which includes the cancellation token.
        /// </param>
        /// <param name="progress">
        /// The progress object to use to report the progress of the generation.
        /// </param>
        private void GenerateReport(ITaskContext context, IProgress<TaskProgress> progress)
        {
            DynamicLevelReportContext dynamicContext = context as DynamicLevelReportContext;
            if (dynamicContext == null)
            {
                return;
            }

            IBranch branch = dynamicContext.Branch;
            ILevel level = dynamicContext.Level;
            StructureDictionary structures = new StructureDictionary();
            structures.Load(branch, Path.Combine(branch.Metadata, "definitions"));
            GameCharacters characters = new GameCharacters(branch, structures);
            PlatformEntries platformEntires = new PlatformEntries();
            foreach (Platform platform in dynamicContext.Platforms)
            {
                platformEntires.Add(platform, this.GetEntries(platform, branch, level));
            }

            MemoryStream stream = new MemoryStream();
            StreamWriter streamWriter = new StreamWriter(stream);
            HtmlTextWriter htmlWriter = new HtmlTextWriter(streamWriter);

            htmlWriter.RenderBeginTag(HtmlTextWriterTag.Html);
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.Head);
            htmlWriter.RenderEndTag();
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.Body);

            htmlWriter.RenderBeginTag(HtmlTextWriterTag.H1);
            htmlWriter.Write("Character Asset Usage");
            htmlWriter.RenderEndTag();

            htmlWriter.AddStyleAttribute(HtmlTextWriterStyle.MarginBottom, "0");
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.H2);
            htmlWriter.Write("Not Referenced Assets");
            htmlWriter.RenderEndTag();

            htmlWriter.AddStyleAttribute(HtmlTextWriterStyle.FontSize, "small");
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.I);
            htmlWriter.Write("These are the assets located inside the character rpf files that have not been referenced inside the ped metadata.");
            htmlWriter.RenderEndTag();

            string notReferencedHeader = "{0} Not Referenced Assets: {1} found";
            foreach (KeyValuePair<Platform, RpfEntries> kvp in platformEntires)
            {
                RpfEntries entries = kvp.Value;
                List<string> notReferenced = this.NotReferenced(kvp.Key, characters, entries);

                string platformName = kvp.Key.PlatformToFriendlyName();
                htmlWriter.RenderBeginTag(HtmlTextWriterTag.H3);
                htmlWriter.Write(notReferencedHeader, platformName, notReferenced.Count);
                htmlWriter.RenderEndTag();

                foreach (string notReferencedAsset in notReferenced)
                {
                    htmlWriter.Write(notReferencedAsset);
                    htmlWriter.Write("<br />");
                }
            }

            htmlWriter.AddStyleAttribute(HtmlTextWriterStyle.MarginBottom, "0");
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.H2);
            htmlWriter.Write("Missing Assets");
            htmlWriter.RenderEndTag();

            htmlWriter.AddStyleAttribute(HtmlTextWriterStyle.FontSize, "small");
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.I);
            htmlWriter.Write("These are the definitions contained within the ped metadata files whose associated assets cannot be found in the build rpf files.");
            htmlWriter.RenderEndTag();

            string notFoundHeader = "{0} Missing Assets: {1} found";
            foreach (KeyValuePair<Platform, RpfEntries> kvp in platformEntires)
            {
                List<string> entries = kvp.Value.AllEntries();
                List<string> notFound = this.NotFound(kvp.Key, characters, entries);

                string platformName = kvp.Key.PlatformToFriendlyName();
                htmlWriter.RenderBeginTag(HtmlTextWriterTag.H3);
                htmlWriter.Write(notFoundHeader, platformName, notFound.Count);
                htmlWriter.RenderEndTag();

                foreach (string missingAsset in notFound)
                {
                    htmlWriter.Write(missingAsset);
                    htmlWriter.Write("<br />");
                }
            }

            htmlWriter.RenderEndTag(); // Body
            htmlWriter.Flush();

            stream.Position = 0;
            string outputFile = Path.ChangeExtension(this.Filename, "html");
            using (Stream fileStream = new FileStream(outputFile, FileMode.Create, FileAccess.Write))
            {
                stream.CopyTo(fileStream);
            }
        }

        private RpfEntries GetEntries(Platform platform, IBranch branch, ILevel level)
        {
            ITarget target = branch.Targets[platform];
            string dir = target.ResourcePath;
            string images = Path.Combine(dir, "models", "cdimages");
            string generic = Path.Combine(dir, "levels", level.Name, "generic");

            RpfEntries entries = new RpfEntries();
            this.AddEntriesWithPattern(images, "componentpeds_*.rpf", entries);
            this.AddEntriesWithPattern(images, "streamedpeds_*.rpf", entries);
            this.AddEntries(images, "pedprops.rpf", entries);
            this.AddEntries(images, "streamedpedprops.rpf", entries);
            this.AddEntries(generic, "cutspeds.rpf", entries);

            return entries;
        }

        private void AddEntriesWithPattern(string directory, string pattern, RpfEntries container)
        {
            if (!Directory.Exists(directory))
            {
                return;
            }

            SearchOption option = SearchOption.TopDirectoryOnly;
            string[] files = Directory.GetFiles(directory, pattern, option);
            foreach (string path in files)
            {
                this.AddEntries(path, container);
            }
        }

        private void AddEntries(string directory, string filename, RpfEntries container)
        {
            container.Add(filename, this.GetEntries(directory, filename));
        }

        private void AddEntries(string path, Dictionary<string, List<string>> container)
        {
            string filename = Path.GetFileNameWithoutExtension(path);
            container.Add(filename, this.GetEntries(path));
        }

        private List<string> GetEntries(string directory, string filename)
        {
            string path = Path.Combine(directory, filename);
            return this.GetEntries(path);
        }

        private List<string> GetEntries(string path)
        {
            if (!File.Exists(path))
            {
                return new List<string>();
            }

            Packfile packFile = new Packfile();
            packFile.Load(path);
            List<string> entries = new List<string>();
            foreach (PackEntry entry in packFile.Entries)
            {
                string name = entry.Name;
                if (!String.IsNullOrEmpty(entry.Path))
                {
                    name = name.Insert(0, entry.Path + "/");
                }

                entries.Add(name);
            }

            return entries;
        }

        private List<string> NotFound(Platform platform, GameCharacters characters, IEnumerable<string> entries)
        {
            string textureExtension = GetExtension(FileType.TextureDictionary, platform);
            string drawableExtension = GetExtension(FileType.DrawableDictionary, platform);
            string fragmentExtension = GetExtension(FileType.Fragment, platform);
            string metaExtension = GetExtension(FileType.Metadata, platform);

            HashSet<string> lookup = new HashSet<string>(entries);
            HashSet<string> pathLookup = new HashSet<string>();
            foreach (string entry in entries)
            {
                if (!entry.Contains("/"))
                {
                    continue;
                }

                pathLookup.Add(entry.Substring(0, entry.IndexOf("/")));
            }

            List<string> notFound = new List<string>();
            foreach (GameCharacter character in characters.Characters)
            {
                string name = character.Name;
                string propsName = character.PropsName;

                string drawable = name + drawableExtension;
                if (!lookup.Contains(drawable))
                {
                    if (!pathLookup.Contains(name))
                    {
                        notFound.Add(drawable);
                    }
                }

                string texture = name + textureExtension;
                if (!lookup.Contains(texture))
                {
                    if (!pathLookup.Contains(name))
                    {
                        notFound.Add(texture);
                    }
                }

                string metadata = name + metaExtension;
                if (!lookup.Contains(metadata))
                {
                    notFound.Add(metadata);
                }

                string fragment = name + fragmentExtension;
                if (!lookup.Contains(fragment))
                {
                    notFound.Add(fragment);
                }
            }

            return notFound;
        }

        private List<string> NotReferenced(Platform platform, GameCharacters characters, RpfEntries entries)
        {
            List<string> notReferenced = new List<string>();
            HashSet<string> referenced = new HashSet<string>();
            foreach (GameCharacter character in characters.Characters)
            {
                referenced.Add(character.Name);
                if (character.PropsName != null)
                {
                    referenced.Add(character.PropsName);
                }
            }

            foreach (KeyValuePair<string, List<string>> entryGroup in entries)
            {
                foreach (string entry in entryGroup.Value)
                {
                    if (entry.StartsWith("_manifest"))
                    {
                        continue;
                    }

                    string name = entry;
                    string path = null;
                    int pathIndex = name.IndexOf("/");
                    if (pathIndex != -1)
                    {
                        name = entry.Substring(pathIndex, name.Length - pathIndex);
                        path = entry.Substring(0, pathIndex);
                    }

                    name = Path.GetFileNameWithoutExtension(name);
                    name = name.Replace("+hi", "");

                    if (!referenced.Contains(name))
                    {
                        if (path == null || !referenced.Contains(path))
                        {
                            notReferenced.Add(entry);
                        }
                    }
                }
            }

            return notReferenced;
        }

        private string GetExtension(FileType type, Platform platform)
        {
            return "." + type.CreateExtensionForPlatform(platform);
        }
        #endregion

        private class RpfEntries : Dictionary<string, List<string>>
        {
            public List<string> AllEntries()
            {
                List<string> entries = new List<string>();
                foreach (List<string> values in this.Values)
                {
                    entries.AddRange(values);
                }

                return entries;
            }
        }

        private class PlatformEntries : Dictionary<Platform, RpfEntries>
        {
        }
    } // RSG.Model.Report2.Reports.GameAssetUsage.WeaponAssetUsageReport {Class}
} // RSG.Model.Report2.Reports.GameAssetUsage {Namespace}
