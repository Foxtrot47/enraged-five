﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Collections;

namespace RSG.Model.Common
{
    /// <summary>
    /// 
    /// </summary>
    public interface ICharacterCollection :
        IAsset,
        ICollection<ICharacterCategoryCollection>,
        IEnumerable<ICharacterCategoryCollection>,
        IDisposable
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        ObservableCollection<ICharacterCategoryCollection> CharacterCategories { get; }

        /// <summary>
        /// Returns all characters in this collection.
        /// </summary>
        IEnumerable<ICharacter> AllCharacters { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        void LoadAllStats();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="statsToLoad"></param>
        void LoadStats(IEnumerable<StreamableStat> statsToLoad);
        #endregion // Methods
    } // ICharacterCollection
} // RSG.Model.Common
