﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace RSG.Model.Common
{
    /// <summary>
    /// Enumeration of all possible source of the asset data
    /// </summary>
    public enum DataSource
    {
        /// <summary>
        /// This enum should really be called local as it's a mishmash of local data.
        /// i.e. it's not just from scene xml files.
        /// </summary>
        [Description("Local Only")]
        SceneXml,

        [Description("Database Only")]
        Database,

        //[Description("Local and Database")]
        //Mixed
    }
}
