﻿namespace RSG.Model.Report2.Reports.Map
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using RSG.Base.ConfigParser;
    using RSG.Base.Tasks;
    using Base.Math;
    using RSG.Model.Common;
    using RSG.Model.Common.Map;
    using RSG.Model.Report;
    using RSG.ManagedRage;
    using RSG.Platform;
    using RSG.Model.Map.SceneOverride;

    /// <summary>
    /// A report that lists the entities with columns that can be used to filter to show
    /// invlaid entities based on a number of attributes.
    /// </summary>
    public class LowPriorityMixReport : CSVReport, IDynamicLevelReport
    {
        #region Fields
        /// <summary>
        /// The name of this report.
        /// </summary>
        private const string Name = "Low Priority Mix";

        /// <summary>
        /// The description for this report.
        /// </summary>
        private const string Description =
            "Exports a csv report that lists all LOD siblings with a mixture of 'Low Priority' settings.";

        /// <summary>
        /// The private field used for the <see cref="GenerationTask"/> property.
        /// </summary>
        private ITask _generationTask;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="InvalidEntityReport"/> class.
        /// </summary>
        public LowPriorityMixReport()
            : base(Name, Description)
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the task that represents the work needing to be done to generate this report.
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (this._generationTask == null)
                {
                    CompositeTask task = new CompositeTask("Generate Report");
                    task.AddSubTask(new ActionTask("Loading data", this.EnsureDataLoaded));
                    task.AddSubTask(new ActionTask("Generating report", this.GenerateReport));
                    this._generationTask = task;
                }

                return this._generationTask;
            }
        }

        /// <summary>
        /// Gets a iterator around the stats that are required to generate this report. Always
        /// returns null.
        /// </summary>
        public IEnumerable<StreamableStat> RequiredStats
        {
            get { return null; }
        }
        #endregion Properties

        #region Private Classes
        /// <summary>
        /// This class represents a collection of LOD siblings that have the 'Low Priority' flag set
        /// but also have siblings that don't have the 'Low Priority' flag set.
        /// </summary>
        private class LowPriorityMixture
        {
            internal LowPriorityMixture(IEntity parentEntity, IEntity[] lowPriorityChildEntities, IEntity[] otherChildEntities)
            {
                ParentEntity = parentEntity;
                LowPriorityChildEntities = lowPriorityChildEntities;
                OtherChildEntities = otherChildEntities;
            }

            internal IEntity ParentEntity { get; private set; }
            internal IEntity[] LowPriorityChildEntities { get; private set; }
            internal IEntity[] OtherChildEntities { get; private set; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Ensures that the data needed for this report is loaded.
        /// </summary>
        /// <param name="context">
        /// The task context for this action.
        /// </param>
        /// <param name="progress">
        /// The object used to notify a listener of the progress.
        /// </param>
        private void EnsureDataLoaded(ITaskContext context, IProgress<TaskProgress> progress)
        {
            DynamicLevelReportContext reportContext = context as DynamicLevelReportContext;
            Debug.Assert(reportContext != null, "Unable to load data for report");
            if (reportContext == null)
            {
                return;
            }

            IMapHierarchy hierarchy = reportContext.Level.MapHierarchy;
            Debug.Assert(hierarchy != null, "Unable to load data for report");
            if (hierarchy == null || hierarchy.AllSections == null)
            {
                return;
            }

            List<IMapSection> mapSections = hierarchy.AllSections.ToList();
            float increment = 1.0f / mapSections.Count;
            string fmt = "Loading {0}";
            int index = 1;
            foreach (IMapSection section in mapSections)
            {
                Debug.WriteLine("{0} of {1}", index++, mapSections.Count);
                context.Token.ThrowIfCancellationRequested();
                string message = String.Format(fmt, section.Name);
                progress.Report(new TaskProgress(increment, true, message));

                section.LoadStats(new List<StreamableStat>() { StreamableMapSectionStat.Archetypes, StreamableMapSectionStat.Entities });
            }
        }

        /// <summary>
        /// Generates the report and saves the file associated with it.
        /// </summary>
        /// <param name="context">
        /// The task context for this action.
        /// </param>
        /// <param name="progress">
        /// The object used to notify a listener of the progress.
        /// </param>
        private void GenerateReport(ITaskContext context, IProgress<TaskProgress> progress)
        {
            DynamicLevelReportContext reportContext = context as DynamicLevelReportContext;
            Debug.Assert(reportContext != null, "Unable to generate report");
            if (reportContext == null)
            {
                return;
            }

            IMapHierarchy hierarchy = reportContext.Level.MapHierarchy;
            Debug.Assert(hierarchy != null, "Unable to generate report");
            if (hierarchy == null || hierarchy.AllSections == null)
            {
                return;
            }

            List<LowPriorityMixture> lowPriorityMixtures = new List<LowPriorityMixture>();
            foreach (IMapSection section in hierarchy.AllSections.Where(sec => sec.ChildEntities != null))
            {
                foreach (IEntity entity in section.ChildEntities.Where(en => en.LodChildren.Count > 0))
                {
                    IEntity[] lowPrioritySiblings = entity.LodChildren.Where(en => en.IsLowPriority).ToArray();
                    IEntity[] otherSiblings = entity.LodChildren.Where(en => !en.IsLowPriority).ToArray();

                    if (lowPrioritySiblings.Length > 0 && otherSiblings.Length > 0)
                    {
                        lowPriorityMixtures.Add(new LowPriorityMixture(entity, lowPrioritySiblings, otherSiblings));
                    }
                }
            }

            this.WriteCsvFile(lowPriorityMixtures);
        }

        private void WriteCsvFile(IEnumerable<LowPriorityMixture> lowPriorityMixtures)
        {
            using (StreamWriter writer = new StreamWriter(this.Filename))
            {
                writer.WriteLine(
                    "Section Name," +
                    "Parent Name," +
                    "Low Priority Children," +
                    "Other Children");

                WriteCsvData(writer, lowPriorityMixtures);
            }
        }

        private void WriteCsvData(StreamWriter writer, IEnumerable<LowPriorityMixture> lowPriorityMixtures)
        {
            foreach (LowPriorityMixture lowPriorityMixture in lowPriorityMixtures)
            {
                writer.WriteLine("{0},{1},{2},{3}",
                    lowPriorityMixture.ParentEntity.ContainingSection.Name,
                    lowPriorityMixture.ParentEntity.Name,
                    String.Join(" ; ", lowPriorityMixture.LowPriorityChildEntities.Select(en => en.Name)),
                    String.Join(" ; ", lowPriorityMixture.OtherChildEntities.Select(en => en.Name)));
            }
        }
        #endregion Methods
    }
}
