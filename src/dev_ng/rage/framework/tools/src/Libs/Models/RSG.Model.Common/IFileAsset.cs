﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common
{
    /// <summary>
    /// File asset interface.
    /// </summary>
    public interface IFileAsset : IAsset
    {
        #region Properties
        
        /// <summary>
        /// Asset filename (absolute).
        /// </summary>
        String Filename { get; }

        #endregion // Properties
    } // IFileAsset
} // RSG.Model.Common
