﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using RSG.Base.ConfigParser;
using RSG.Base.Tasks;
using RSG.Model.Common;

namespace RSG.Model.Report.Reports.Weapons
{
    /// <summary>
    /// 
    /// </summary>
    public class WeaponStatsReport : CSVReport, IDynamicLevelReport
    {
        #region Constants
        private const String c_name = "Weapon Stats Report";
        private const String c_description = "Exports the selected level vehicles into a .csv file"; 
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Task to run for generating this report.
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (m_task == null)
                {
                    //m_task = new ActionTask("Generating report", (context, progress) => GenerateReport((DynamicLevelReportContext)context, progress));
                    CompositeTask task = new CompositeTask("Generate Report");
                    task.AddSubTask(new ActionTask("Loading data", (context, progress) => EnsureDataLoaded((DynamicLevelReportContext)context, progress)), 10);
                    task.AddSubTask(new ActionTask("Generating report", (context, progress) => GenerateReport((DynamicLevelReportContext)context, progress)));
                    m_task = task;
                }
                return m_task;
            }
        }
        private ITask m_task;

        /// <summary>
        /// List of stats that are required to generate this report.
        /// </summary>
        public IEnumerable<StreamableStat> RequiredStats
        {
            get
            {
                if (m_requiredStats == null)
                {
                    //m_requiredStats = StreamableStatUtils.GetValues<StreamableWeaponStat>().ToArray();
                }
                return m_requiredStats;
            }
        }
        private StreamableStat[] m_requiredStats;
        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// Constructor.
        /// </summary>
        public WeaponStatsReport()
            : base(c_name, c_description)
        {
        }

        #endregion // Constructor(s)

        #region Task Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void EnsureDataLoaded(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            IWeaponCollection weapons = context.Level.Weapons;
            if (weapons != null)
            {
                weapons.LoadAllStats();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void GenerateReport(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            ExportVehicleCSVReport(context.Level, context.GameView, context.Platforms);
        }
        #endregion // Task Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        /// <param name="gv"></param>
        private void ExportVehicleCSVReport(ILevel level, ConfigGameView gv, IPlatformCollection platforms)
        {
            using (StreamWriter sw = new StreamWriter(Filename))
            {
                WriteCsvHeader(sw, platforms);

                IWeaponCollection weapons = level.Weapons;
                if (weapons != null)
                {
                    foreach (IWeapon weapon in weapons)
                    {
                        // Generate the csv line
                        string scvRecord = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11}",
                            weapon.Name, weapon.PolyCount, weapon.CollisionCount,
                            weapon.Textures.Count(), weapon.Shaders.Count(),
                            weapon.BoneCount,
                            weapon.GripCount, weapon.MagazineCount, weapon.AttachmentCount,
                            weapon.HasLod,
                            (weapon.HasLod == true ? weapon.LodPolyCount.ToString() : "n/a"),
                            GenerateWeaponMemoryDetails(weapon, platforms));
                        sw.WriteLine(scvRecord);
                    }
                }
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sw"></param>
        /// <param name="platforms"></param>
        private void WriteCsvHeader(StreamWriter sw, IPlatformCollection platforms)
        {
            string header = "Name,Poly Count,Collision Count,Texture Count,Shader Count,Bone Count,Grip Count,Magazine Count,Attachment Count,Has LOD,LOD Poly Count";

            foreach (RSG.Platform.Platform platform in platforms)
            {
                header += String.Format(",{0} Physical Size,{0} Virtual Size,{0} Total Size", platform);
            }

            sw.WriteLine(header);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="weapon"></param>
        /// <param name="platforms"></param>
        /// <returns></returns>
        private string GenerateWeaponMemoryDetails(IWeapon weapon, IPlatformCollection platforms)
        {
            StringBuilder details = new StringBuilder("");

            foreach (RSG.Platform.Platform platform in platforms)
            {
                if (weapon.PlatformStats.ContainsKey(platform))
                {
                    IWeaponPlatformStat stat = weapon.PlatformStats[platform];
                    details.Append(String.Format("{0},{1},{2},", stat.PhysicalSize, stat.VirtualSize, stat.PhysicalSize + stat.VirtualSize));
                }
                else
                {
                    details.Append("n/a,n/a,n/a,");
                }
            }

            return details.ToString().TrimEnd(new char[] {','});
        }
        #endregion // Private Methods
    } // WeaponStatsReport
}
