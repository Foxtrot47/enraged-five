﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;

namespace RSG.Model.Weapon
{
    /// <summary>
    /// 
    /// </summary>
    public class WeaponPlatformStat : IWeaponPlatformStat
    {
        #region Properties
        /// <summary>
        /// Platform for this stat
        /// </summary>
        public RSG.Platform.Platform Platform
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public uint PhysicalSize
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public uint VirtualSize
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default Constructor
        /// </summary>
        public WeaponPlatformStat()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="physicalSize"></param>
        /// <param name="virtualSize"></param>
        public WeaponPlatformStat(RSG.Platform.Platform platform, uint physicalSize, uint virtualSize)
        {
            Platform = platform;
            PhysicalSize = physicalSize;
            VirtualSize = virtualSize;
        }
        #endregion // Constructor(s)
    } // WeaponPlatformStat
}
