﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.SceneXml;
using RSG.SceneXml.Statistics;
using RSG.Base.Math;

namespace RSG.Model.Map.Statistics
{
    /// <summary>
    /// Contains the statistics for a prop group (either inside a container using rsrefs or outside using xrefs)
    /// </summary>
    public class PropGroup
        : Base
    {
        #region Properties

        public Dictionary<String, LevelReferenceStatistics> ReferenceStats
        {
            get;
            set;
        }

        public Dictionary<String, int> ReferenceCounts
        {
            get;
            set;
        }

        /// <summary>
        /// The maximum draw distance for all the references
        /// </summary>
        public float MaxReferenceDistance
        {
            get;
            set;
        }

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// Default constructor
        /// </summary>
        public PropGroup(MapSection section, LevelReferences references)
        {
            this.ReferenceStats = new Dictionary<String, LevelReferenceStatistics>();
            this.ReferenceCounts = new Dictionary<String, int>();

            Scene scene = SceneManager.GetScene(section.SceneXmlFilename);
            if (scene != null)
            {
                Stats sceneStatistics = scene.Statistics;
                if (sceneStatistics != null)
                {
                    Boolean validGeometryStatistics = sceneStatistics.GeometryStats.Count > 0 ? true : false;

                    if (validGeometryStatistics == true)
                    {
                        GenerateReferenceStatistics(scene, references);
                    }
                }
            }
        }

        #endregion // Constructor(s)

        #region Statistics Creation

        /// <summary>
        /// Generates all the statistics for this container using the geometry statistics
        /// </summary>
        /// <param name="scene"></param>
        private void GenerateReferenceStatistics(Scene scene, LevelReferences references)
        {
            IDictionary<Guid, DrawableStats> statistics = scene.Statistics.GeometryStats;

            foreach (DrawableStats drawableStat in statistics.Values)
            {
                TargetObjectDef drawable = scene.FindObject(drawableStat.Guid);
                if (drawable != null && drawable.DontExport() == false && drawable.DontExportIDE() == false && drawable.DontExportIPL() == false)
                {
                    if (drawable.IsXRef() || drawable.IsInternalRef() || drawable.IsRefObject())
                    {
                        this.GenerateReference(drawable, references);
                    }
                }
            }
        }

        /// <summary>
        /// Creates a dictionary of references that belong to this map section so that
        /// the statistics can be generated later in a second pass
        /// </summary>
        private void GenerateReference(TargetObjectDef reference, LevelReferences references)
        {
            LevelReferenceStatistics resolvedReference = references.GetReferenceStatistics(reference.RefFile, reference.RefName);
            if (resolvedReference != null)
            {
                UniqueLevelReference levelReference = references.ReferenceFiles[reference.RefFile][reference.RefName];
                levelReference.AppearenceCount++;
                levelReference.AppearenceLocations.Add(reference.WorldBoundingBox.Centre());

                if (this.ReferenceStats.ContainsKey(reference.RefName))
                {
                    this.ReferenceCounts[reference.RefName]++;
                }
                else
                {
                    this.ReferenceCounts.Add(reference.RefName, 1);
                    this.ReferenceStats.Add(reference.RefName, resolvedReference); 
                }
            }
            float lodDistance = reference.GetLODDistance();
            if (lodDistance > this.MaxReferenceDistance)
            {
                this.MaxReferenceDistance = lodDistance;
            }
        }

        #endregion // Statistics Creation

        #region Override Functions

        #region Total Cost Statistics

        public override int GetHighTotalCost()
        {
            return -1;
        }

        public override int GetLodTotalCost()
        {
            return -1;
        }

        public override int GetSLodTotalCost()
        {
            return -1;
        }

        #endregion // Total Cost Statistics

        #region Geometry Cost Statistics

        public override int GetHighGeometryCost()
        {
            return -1;
        }

        public override int GetLodGeometryCost()
        {
            return -1;
        }

        public override int GetSLodGeometryCost()
        {
            return -1;
        }

        #endregion // Geometry Cost Statistics

        #region TXD Cost Statistics

        public override int GetHighTXDCost()
        {
            return -1;
        }

        public override int GetLodTXDCost()
        {
            return -1;
        }

        public override int GetSLodTXDCost()
        {
            return -1;
        }

        #endregion // TXD Cost Statistics

        #region Geometry Count Statistics

        public override int GetHighGeometryCount()
        {
            return -1;
        }

        public override int GetLodGeometryCount()
        {
            return -1;
        }

        public override int GetSLodGeometryCount()
        {
            return -1;
        }

        #endregion // Geometry Count Statistics

        #region TXD Count Statistics

        public override int GetHighTXDCount()
        {
            return -1;
        }

        public override int GetLodTXDCount()
        {
            return -1;
        }

        public override int GetSLodTXDCount()
        {
            return -1;
        }

        #endregion // TXD Count Statistics

        #region Polygon Count Statistics

        public override int GetHighPolygonCount()
        {
            return -1;
        }

        public override int GetLodPolygonCount()
        {
            return -1;
        }

        public override int GetSLodPolygonCount()
        {
            return -1;
        }

        #endregion // Polygon Count Statistics

        #region Polygon Density Statistics

        public override float GetHighPolygonDensity()
        {
            return -1;
        }

        public override float GetLodPolygonDensity()
        {
            return -1;
        }

        public override float GetSLodPolygonDensity()
        {
            return -1;
        }

        #endregion // Polygon Density Statistics

        #region Collision Statistics

        public override int GetCollisionPolygonCount()
        {
            return -1;
        }

        public override float GetCollisionPolygonDensity()
        {
            return -1;
        }

        public override float GetCollisionPolygonRatio()
        {
            return -1;
        }

        #endregion // Collision Statistics

        #region LOD Distance Statistics

        public override int GetHighLodDistance()
        {
            return -1;
        }

        public override int GetLodLodDistance()
        {
            return -1;
        }

        public override int GetSLodLodDistance()
        {
            return -1;
        }

        #endregion // LOD Distance Statistics

        #region Reference Total Cost Statistics

        public override int GetAllRefTotalCost()
        {
            return -1;
        }

        public override int GetUniqueRefTotalCost()
        {
            return -1;
        }

        #endregion // Reference Cost Statistics

        #region Reference Geometry Cost Statistics

        public override int GetAllRefGeometryCost()
        {
            return -1;
        }

        public override int GetUniqueRefGeometryCost()
        {
            return -1;
        }

        #endregion // Reference Geometry Cost Statistics

        #region Reference TXD Statistics

        public override int GetAllRefTXDCost()
        {
            return -1;
        }

        public override int GetAllRefTXDCount()
        {
            return -1;
        }

        #endregion // Reference TXD Statistics

        #region Reference Count Statistics

        public override int GetAllRefCount()
        {
            return -1;
        }

        public override int GetUniqueRefCount()
        {
            return -1;
        }

        #endregion // Reference Count Statistics

        #region Reference Polygon Count Statistics

        public override int GetReferencePolygonCountAll()
        {
            return -1;
        }

        public override int GetReferencePolygonCountUnique()
        {
            return -1;
        }

        #endregion // Reference Polygon Count Statistics

        #region Reference Polygon Density Statistics

        public override float GetReferencePolygonDensityAll()
        {
            return -1;
        }

        public override float GetReferencePolygonDensityUnique()
        {
            return -1;
        }

        #endregion // Reference Polygon Density Statistics

        #region Reference Collision Count Statistics

        public override int GetReferenceCollisionPolygonCountAll()
        {
            return -1;
        }

        public override int GetReferenceCollisionPolygonCountUnique()
        {
            return -1;
        }

        #endregion // Reference Collision Count Statistics

        #region Reference Collision Density Statistics

        public override float GetReferenceCollisionPolygonDensityAll()
        {
            return -1;
        }

        public override float GetReferenceCollisionPolygonDensityUnique()
        {
            return -1;
        }

        #endregion // Reference Collision Density Statistics

        #region Reference Collision Ratio Statistics

        public override float GetReferenceCollisionPolygonRatioAll()
        {
            return -1;
        }

        public override float GetReferenceCollisionPolygonRatioUnique()
        {
            return -1;
        }

        #endregion // Reference Collision Ratio Statistics

        #region Reference LOD Distance Statistics

        public override float GetReferenceLodDistance()
        {
            return -1;
        }
        
        #endregion Reference // LOD Distance Statistics

        #endregion // Override Functions
    }
}
