﻿//---------------------------------------------------------------------------------------------
// <copyright file="NumericalConverter{T}.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Converters
{
    using System;
    using System.Globalization;
    using System.Text;
    using RSG.Base;

    /// <summary>
    /// Provides a abstract base to any converter class that can convert a string instance into
    /// a single or an array of numerical values of the specified type.
    /// </summary>
    /// <typeparam name="T">
    /// The type that this converter can convert a string instance to.
    /// </typeparam>
    public abstract class NumericalConverter<T> : ConverterBase<T>
    {
        #region Fields
        /// <summary>
        /// A reference to the numerical constants to use during the conversion.
        /// </summary>
        private NumericalConstants _constants;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="NumericalConverter{T}"/> class.
        /// </summary>
        protected NumericalConverter()
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="NumericalConverter{T}"/> class.
        /// </summary>
        /// <param name="constants">
        /// The numerical constant class that this converter can use.
        /// </param>
        protected NumericalConverter(NumericalConstants constants)
        {
            this._constants = constants;
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Sets the numerical constants that are used by this converter in the conversion
        /// operation.
        /// </summary>
        /// <param name="constants">
        /// The numerical constants this converter will use.
        /// </param>
        public void SetConstants(NumericalConstants constants)
        {
            this._constants = constants;
        }

        /// <summary>
        /// Converts the double representation of the numerical number to its specified type
        /// equivalent.
        /// </summary>
        /// <param name="d">
        /// A double that contains the number to convert.
        /// </param>
        /// <param name="result">
        /// When this method returns, contains the numerical value equivalent to the number
        /// contained in d, if the conversion succeeded, or zero if the conversion failed.
        /// </param>
        /// <returns>
        /// True if d was converted successfully; otherwise, false.
        /// </returns>
        protected abstract bool FromDouble(double d, out T result);

        /// <summary>
        /// Implements the logic of the conversion between a single string instance and a
        /// numerical value of the specified type parameter.
        /// </summary>
        /// <param name="value">
        /// The string instance to convert.
        /// </param>
        /// <param name="fallback">
        /// Specifies the value that is returned if the conversion fails.
        /// </param>
        /// <param name="methods">
        /// The conversion methods that can be used during this conversion.
        /// </param>
        /// <returns>
        /// A structure containing the result of the conversion and a value indicating whether
        /// the conversion was successful.
        /// </returns>
        protected override TryResult<T> Try(string value, T fallback, Methods methods)
        {
            if (String.IsNullOrWhiteSpace(value))
            {
                return new TryResult<T>(fallback, false);
            }

            value = value.Trim();
            if ((methods & Methods.Number) == Methods.Number)
            {
                T result;
                if (this.TryParse(value, out result))
                {
                    return new TryResult<T>(result, true);
                }
            }

            if ((methods & Methods.Hex) == Methods.Hex)
            {
                T result;
                string hexString = value.Replace("0x", String.Empty);
                if (this.TryParse(hexString, NumberStyles.HexNumber, out result))
                {
                    return new TryResult<T>(result, true);
                }
            }

            if ((methods & Methods.Constant) == Methods.Constant && this._constants != null)
            {
                TryResult<T> result = this._constants.GetConstant<T>(value);
                if (result.Success)
                {
                    return result;
                }
            }

            if ((methods & Methods.Expression) == Methods.Expression)
            {
                TryResult<string> expressionResult = this.EnsureExpressionFormat(value);
                if (expressionResult.Success)
                {
                    string expression = expressionResult.Value;
                    TryResult<double> result = this.ParseExpression(ref expression, 0.0);
                    if (result.Success)
                    {
                        T actualResult;
                        if (this.FromDouble(result.Value, out actualResult))
                        {
                            return new TryResult<T>(actualResult, true);
                        }
                    }
                }
            }

            return new TryResult<T>(fallback, false);
        }

        /// <summary>
        /// Tries to convert the string representation of a number to its numerical equivalent,
        /// and returns a value that indicates whether the conversion succeeded.
        /// </summary>
        /// <param name="s">
        /// A string that contains a number to convert.
        /// </param>
        /// <param name="result">
        /// When this method returns, contains the numerical value equivalent to the number
        /// contained in s, if the conversion succeeded, or zero if the conversion failed.
        /// </param>
        /// <returns>
        /// True if s was converted successfully; otherwise, false.
        /// </returns>
        protected abstract bool TryParse(string s, out T result);

        /// <summary>
        /// Tries to convert the string representation of a number to its numerical equivalent,
        /// and returns a value that indicates whether the conversion succeeded.
        /// </summary>
        /// <param name="s">
        /// A string that contains a number to convert.
        /// </param>
        /// <param name="style">
        /// A bitwise combination of enumeration values that indicates the permitted format of
        /// s.
        /// </param>
        /// <param name="result">
        /// When this method returns, contains the numerical value equivalent to the number
        /// contained in s, if the conversion succeeded, or zero if the conversion failed.
        /// </param>
        /// <returns>
        /// True if s was converted successfully; otherwise, false.
        /// </returns>
        protected abstract bool TryParse(string s, NumberStyles style, out T result);

        /// <summary>
        /// Makes sure the specified string is in the correct format for expression parsing.
        /// </summary>
        /// <param name="s">
        /// A string to ensure is in the correct format for expression parsing.
        /// </param>
        /// <returns>
        /// A structure containing the formatted string ready to be parsed and a boolean value
        /// indicating whether that string is valid.
        /// </returns>
        private TryResult<string> EnsureExpressionFormat(string s)
        {
            if (s == null)
            {
                return new TryResult<string>(null, false);
            }

            s = s.Trim();
            StringBuilder sb = new StringBuilder(s.Length);
            bool previousWasOperator = false;
            bool previousWasValue = false;
            bool previousWasOpenBracket = false;
            int openBracketCount = 0;
            int closingBracketCount = 0;
            for (int i = 0; i < s.Length; i++)
            {
                char c = s[i];
                switch (c)
                {
                    case '\r':
                    case '\n':
                    case '\t':
                    case ' ':
                        continue;

                    case '.':
                        if (!previousWasValue)
                        {
                            sb.Append("0.");
                        }
                        else
                        {
                            sb.Append('.');
                        }

                        previousWasOperator = false;
                        previousWasValue = true;
                        break;

                    case '+':
                        {
                            previousWasValue = false;
                            if (i == 0)
                            {
                                sb.Append("0 + ");
                                previousWasOperator = true;
                            }
                            else if (i == s.Length - 1 || previousWasOperator)
                            {
                                return new TryResult<string>(null, false);
                            }
                            else
                            {
                                sb.Append(" + ");
                                previousWasOperator = true;
                            }
                        }

                        continue;
                    case '-':
                        {
                            if (i == 0)
                            {
                                sb.Append("0 - ");
                            }
                            else if (i == s.Length - 1 || previousWasOperator)
                            {
                                return new TryResult<string>(null, false);
                            }
                            else if (previousWasOperator)
                            {
                                sb.Append(" -");
                            }
                            else if (previousWasOpenBracket)
                            {
                                sb.Append("-");
                            }
                            else
                            {
                                sb.Append(" - ");
                            }

                            previousWasOperator = true;
                            previousWasValue = false;
                        }

                        continue;
                    case '*':
                        {
                            previousWasValue = false;
                            if (i == 0)
                            {
                                return new TryResult<string>(null, false);
                            }
                            else if (i == s.Length - 1 || previousWasOperator)
                            {
                                return new TryResult<string>(null, false);
                            }
                            else
                            {
                                sb.AppendFormat(" * ");
                                previousWasOperator = true;
                            }
                        }

                        continue;
                    case '/':
                        {
                            previousWasValue = false;
                            if (i == 0)
                            {
                                return new TryResult<string>(null, false);
                            }
                            else if (i == s.Length - 1 || previousWasOperator)
                            {
                                return new TryResult<string>(null, false);
                            }
                            else
                            {
                                sb.Append(" / ");
                                previousWasOperator = true;
                            }
                        }

                        continue;
                    case '(':
                        if (previousWasValue)
                        {
                            sb.Append(" * (");
                        }
                        else
                        {
                            sb.Append(c);
                        }

                        previousWasValue = false;
                        previousWasOperator = false;
                        previousWasOpenBracket = true;
                        openBracketCount++;
                        break;
                    case ')':
                        sb.Append(c);
                        previousWasValue = false;
                        previousWasOperator = false;
                        closingBracketCount++;
                        if (closingBracketCount > openBracketCount)
                        {
                            return new TryResult<string>(null, false);
                        }

                        break;
                    default:
                        previousWasOperator = false;
                        previousWasValue = true;
                        if (i != 0)
                        {
                            if (s[i - 1] == ')')
                            {
                                sb.AppendFormat(" * {0}", c);
                                break;
                            }
                        }

                        sb.Append(c);
                        break;
                }
            }

            if (openBracketCount != closingBracketCount)
            {
                return new TryResult<string>(null, false);
            }

            return new TryResult<string>(sb.ToString(), true);
        }

        /// <summary>
        /// Tries to convert the string representation of a number to its numerical equivalent,
        /// and returns a structure containing both the resulting value and a value indicating
        /// whether the conversion was successful.
        /// </summary>
        /// <param name="s">
        /// A string that contains a number to convert.
        /// </param>
        /// <param name="fallback">
        /// Specifies the value that is returned if the operation fails.
        /// </param>
        /// <returns>
        /// A structure containing the result of the operation and a value indicating whether
        /// the operation was successful.
        /// </returns>
        private TryResult<double> ParseExpression(ref string s, double fallback)
        {
            double result = fallback;
            TryResult<double> factorResult = this.ParseFactor(ref s, fallback);
            if (!factorResult.Success)
            {
                return factorResult;
            }
            else
            {
                result = factorResult.Value;
            }

            while (s.Length != 0)
            {
                if (s[0] == '+')
                {
                    s = s.Substring(2);
                    factorResult = this.ParseFactor(ref s, fallback);
                    if (!factorResult.Success)
                    {
                        return factorResult;
                    }

                    result += factorResult.Value;
                }
                else if (s[0] == '-')
                {
                    s = s.Substring(2);
                    factorResult = this.ParseFactor(ref s, fallback);
                    if (!factorResult.Success)
                    {
                        return factorResult;
                    }

                    result -= factorResult.Value;
                }

                if (s.Length != 0 && s[0] == ')')
                {
                    while (s.Length > 0 && s[0] == ')')
                    {
                        s = s.Substring(1).TrimStart();
                    }

                    break;
                }
            }

            return new TryResult<double>(result, true);
        }

        /// <summary>
        /// Tries to convert the string representation of a number to its numerical equivalent,
        /// and returns a structure containing both the resulting value and a value indicating
        /// whether the conversion was successful.
        /// </summary>
        /// <param name="s">
        /// A string that contains a number to convert.
        /// </param>
        /// <param name="fallback">
        /// Specifies the value that is returned if the operation fails.
        /// </param>
        /// <returns>
        /// A structure containing the result of the operation and a value indicating whether
        /// the operation was successful.
        /// </returns>
        private TryResult<double> ParseFactor(ref string s, double fallback)
        {
            double result = fallback;
            if (s.Length != 0)
            {
                if (s[0] == '(')
                {
                    s = s.Substring(1).TrimStart();
                    TryResult<double> parseResult = this.ParseExpression(ref s, fallback);
                    if (!parseResult.Success)
                    {
                        return parseResult;
                    }

                    if (s.Length == 0)
                    {
                        return parseResult;
                    }

                    result = parseResult.Value;
                }
                else
                {
                    TryResult<double> termResult = this.ParseTerm(ref s, fallback);
                    if (!termResult.Success)
                    {
                        return termResult;
                    }

                    result = termResult.Value;
                }
            }

            if (s.Length != 0)
            {
                if (s[0] == '(')
                {
                    s = s.Substring(1).TrimStart();
                    TryResult<double> parseResult = this.ParseExpression(ref s, fallback);
                    if (!parseResult.Success)
                    {
                        return parseResult;
                    }

                    result *= parseResult.Value;
                }
            }

            if (s.Length != 0)
            {
                if (s[0] == '*')
                {
                    s = s.Substring(2);
                    TryResult<double> factorResult = this.ParseFactor(ref s, fallback);
                    if (!factorResult.Success)
                    {
                        return factorResult;
                    }

                    result *= factorResult.Value;
                }
                else if (s[0] == '/')
                {
                    s = s.Substring(2);
                    TryResult<double> factorResult = this.ParseFactor(ref s, fallback);
                    if (!factorResult.Success)
                    {
                        return factorResult;
                    }

                    result /= factorResult.Value;
                }
            }

            return new TryResult<double>(result, true);
        }

        /// <summary>
        /// Tries to convert the string representation of a number to its numerical equivalent,
        /// and returns a structure containing both the resulting value and a value indicating
        /// whether the conversion was successful.
        /// </summary>
        /// <param name="s">
        /// A string that contains a number to convert.
        /// </param>
        /// <param name="fallback">
        /// Specifies the value that is returned if the operation fails.
        /// </param>
        /// <returns>
        /// A structure containing the result of the operation and a value indicating whether
        /// the operation was successful.
        /// </returns>
        private TryResult<double> ParseTerm(ref string s, double fallback)
        {
            if (String.IsNullOrWhiteSpace(s))
            {
                return new TryResult<double>(fallback, false);
            }

            int firstValueLength = s.Length;
            int spaceIndex = s.IndexOf(' ');
            if (spaceIndex != -1)
            {
                firstValueLength = System.Math.Min(firstValueLength, spaceIndex);
            }
            else
            {
                int openIndex = s.IndexOf('(');
                if (openIndex != -1)
                {
                    firstValueLength = System.Math.Min(firstValueLength, openIndex);
                }
                else
                {
                    int closeIndex = s.IndexOf(')');
                    if (closeIndex != -1)
                    {
                        firstValueLength = System.Math.Min(firstValueLength, closeIndex);
                    }
                }
            }

            string value = s.Substring(0, firstValueLength);
            TryResult<double> convertResult = this.TryParseDouble(value);
            if (!convertResult.Success)
            {
                return convertResult;
            }
            else
            {
                s = s.Substring(firstValueLength).TrimStart();
            }

            return convertResult;
        }

        /// <summary>
        /// Implements the logic of the conversion between a single string instance and a
        /// numerical value of the specified type parameter.
        /// </summary>
        /// <param name="s">
        /// The string instance to convert.
        /// </param>
        /// <returns>
        /// A structure containing the result of the conversion and a value indicating whether
        /// the conversion was successful.
        /// </returns>
        private TryResult<double> TryParseDouble(string s)
        {
            if (String.IsNullOrWhiteSpace(s))
            {
                return new TryResult<double>(0.0, false);
            }

            s = s.Trim();
            double doubleResult;
            if (double.TryParse(s, out doubleResult))
            {
                return new TryResult<double>(doubleResult, true);
            }

            long longResult;
            NumberStyles style = NumberStyles.HexNumber;
            IFormatProvider provider = CultureInfo.CurrentCulture;
            string hexString = s.Replace("0x", String.Empty);
            if (long.TryParse(hexString, style, provider, out longResult))
            {
                return new TryResult<double>((double)longResult, true);
            }

            TryResult<double> result = this._constants.GetConstant<double>(s);
            if (result.Success)
            {
                return result;
            }

            return new TryResult<double>(0.0, false);
        }
        #endregion Methods
    } // RSG.Metadata.Model.Converters.NumericalConverter{T} {Class}
} // RSG.Metadata.Model.Converters {Namespace}
