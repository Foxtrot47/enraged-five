﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using System.Collections;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using RSG.Base.Collections;
using RSG.Model.Asset;
using RSG.Model.Common;

namespace RSG.Model.GlobalTXD
{
    public class SourceTextureDictionary : ModelBase, IEnumerable<SourceTexture>, IDictionaryChild, ITextureContainer
    {
        #region Constants

        private static readonly String NameAttribute = "name";
        private static readonly String SectionNameAttribute = "section";
        private static readonly String TextureCountAttribute = "textureCount";

        #region XPath Compiled Expressions

        private static readonly XPathExpression sC_s_xpath_XPathAttributes = XPathExpression.Compile("@*");

        private static readonly XPathExpression sC_s_xpath_XPathTextures = XPathExpression.Compile("sourceTexture");
        private static readonly XPathExpression sC_s_xpath_XPathTexturesOld = XPathExpression.Compile("Textures/Texture");

        #endregion // XPath Compiled Expressions

        #endregion // Constants

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public string SourceSectionName
        {
            get;
            set;
        }

        /// <summary>
        /// If this is true it means that source textures can be added to
        /// this container as children
        /// </summary>
        public Boolean CanAcceptTextures
        {
            get;
            private set;
        }

        /// <summary>
        /// The name of the texture dictionary, this name will be exported and used at runtime,
        /// which means that this needs to be unique.
        /// </summary>
        public String Name
        {
            get { return m_name; }
            set
            {
                this.SetPropertyValue(ref this.m_name, value, "Name");
            }
        }
        private String m_name;

        /// <summary>
        /// The parent global dictionary if it has one
        /// </summary>
        public IDictionaryContainer Parent
        {
            get { return m_parent; }
            set
            {
                this.SetPropertyValue(ref this.m_parent, value, "Parent");
            }
        }
        private IDictionaryContainer m_parent;

        /// <summary>
        /// The global root object that this dictionary belongs to
        /// </summary>
        public GlobalRoot Root
        {
            get { return m_root; }
            set
            {
                this.SetPropertyValue(ref this.m_root, value, "Root");
            }
        }
        private GlobalRoot m_root;

        /// <summary>
        /// A dictionary of child texture dictionaries belong to this global texture dictionary.
        /// The dictionary is indexed by names and these names need to be unique.
        /// </summary>
        public ObservableDictionary<String, ITextureChild> Textures
        {
            get { return m_textures; }
            private set
            {
                this.SetPropertyValue(ref this.m_textures, value, "Textures");
            }
        }
        private ObservableDictionary<String, ITextureChild> m_textures;

        /// <summary>
        /// Returns the texture with the given streamname if it exists and null otherwise
        /// </summary>
        public SourceTexture this[String name]
        {
            get
            {
                ITextureChild texture = null;
                this.Textures.TryGetValue(name, out texture);
                return texture as SourceTexture;
            }
        }

        public int StreamSize
        {
            get
            {
                int size = 0;
                foreach (SourceTexture texture in this.Textures.Values)
                {
                    size += texture.StreamSize;
                }
                return size;
            }
        }

        /// <summary>
        /// The current size of the container based on the sum of the texture
        /// sizes inside it
        /// </summary>
        public int CurrentSize
        {
            get
            {
                int size = 0;
                foreach (SourceTexture texture in this.Textures.Values)
                {
                    if (texture.Promoted == false)
                    {
                        size += texture.StreamSize;
                    }
                }
                return size;
            }
        }

        /// <summary>
        /// The number of bytes that have currently been saved due to promoting textures
        /// </summary>
        public int SavingSize
        {
            get
            {
                int size = 0;
                foreach (SourceTexture texture in this.Textures.Values)
                {
                    if (texture.Promoted == true)
                    {
                        size += texture.StreamSize;
                    }
                }
                this.IsSavingLessThanParent = size < (this.Parent as GlobalTextureDictionary).CurrentSize;
                return size;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsSavingLessThanParent
        {
            get { return m_isSavingLessThanParent; }
            set
            {
                this.SetPropertyValue(ref this.m_isSavingLessThanParent, value, "IsSavingLessThanParent");
            }
        }
        private bool m_isSavingLessThanParent;
        #endregion // Properties

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        public SourceTextureDictionary(TextureDictionary dictionary, GlobalTextureDictionary parent)
        {
            this.Name = dictionary.Name.ToLower();
            this.Root = parent.Root;
            this.SourceSectionName = dictionary.SourceSectionName;
            this.Parent = parent;
            this.Textures = new ObservableDictionary<String, ITextureChild>();
            this.CanAcceptTextures = false;

            foreach (IAsset asset in dictionary.AssetChildren)
            {
                if (asset is ITexture)
                {
                    this.Textures.Add((asset as ITexture).Name, new SourceTexture((asset as ITexture), this));
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public SourceTextureDictionary(SourceTextureDictionary clone)
        {
            this.Name = clone.Name.ToLower();
            this.Root = clone.Root;
            this.SourceSectionName = clone.SourceSectionName;
            this.Parent = clone.Parent;
            this.Textures = new ObservableDictionary<String, ITextureChild>();
            this.CanAcceptTextures = clone.CanAcceptTextures;

            foreach (SourceTexture child in clone.Textures.Values)
            {
                this.Textures.Add(child.StreamName, new SourceTexture(child, this));
            }
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public SourceTextureDictionary(XmlReader reader, GlobalTextureDictionary parent)
        {
            this.Root = parent.Root;
            this.Parent = parent;
            this.Textures = new ObservableDictionary<String, ITextureChild>();
            this.CanAcceptTextures = false;

            this.Deserialise(reader);
        }

        #endregion // Constructors

        #region IEnumerable<GlobalTexture>

        /// <summary>
        /// 
        /// </summary>
        IEnumerator<SourceTexture> IEnumerable<SourceTexture>.GetEnumerator()
        {
            foreach (SourceTexture texture in this.m_textures.Values)
            {
                yield return texture;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerator)this);
        }

        #endregion // IEnumerable<GlobalTexture>

        #region ITextureContainer Implementation

        /// <summary>
        /// Returns true iff the container has the texture with the given stream name in it
        /// </summary>
        public Boolean ContainsTexture(String streamName)
        {
            return this.Textures.ContainsKey(streamName);
        }

        /// <summary>
        /// Promotes the given texture (source or global) to here and makes sure that
        /// validation is done so that the texture is removed elsewhere
        /// </summary>
        public Boolean PromoteTextureHere(SourceTexture texture)
        {
            return false;
        }

        /// <summary>
        /// Moves the given global texture from its previous global dictionary to this 
        /// global dictionary
        /// </summary>
        public Boolean MoveTextureHere(GlobalTexture texture)
        {
            return false;
        }

        /// <summary>
        /// Moves the given global texture from this dictionary before it gets
        /// added to another dictionary
        /// </summary>
        public Boolean MoveTextureFromHere(GlobalTexture texture)
        {
            return false;
        }

        /// <summary>
        /// Gets called whenever a texture inside of this container has their promotion
        /// state changed
        /// </summary>
        public void OnPromotionChanged(ITextureChild sender, Boolean promoted)
        {
            OnPropertyChanged("CurrentSize");
            OnPropertyChanged("SavingSize");
        }

        /// <summary>
        /// Gets called whenever a texture inside of this container has their stream 
        /// size changed
        /// </summary>
        public void OnSizeChanged(ITextureChild sender, int size)
        {
            OnPropertyChanged("StreamSize");
            OnPropertyChanged("CurrentSize");
            OnPropertyChanged("SavingSize");
        }

        #endregion // ITextureContainer Implementation

        #region Serialisation / Deserialisation Methods

        /// <summary>
        /// 
        /// </summary>
        public void Serialise(XmlDocument xmlDoc, XmlElement parentNode)
        {
            XmlElement dictionaryNode = xmlDoc.CreateElement("sourceDictionary");

            dictionaryNode.SetAttribute(NameAttribute, this.Name);
            dictionaryNode.SetAttribute(TextureCountAttribute, this.Textures.Count.ToString());
            dictionaryNode.SetAttribute(SectionNameAttribute, this.SourceSectionName);

            //XmlElement SourceTexturesRootNode = xmlDoc.CreateElement("sourceTextures");
            foreach (SourceTexture texture in this.Textures.Values)
            {
                texture.Serialise(xmlDoc, dictionaryNode);
            }
            //dictionaryNode.AppendChild(SourceTexturesRootNode);

            parentNode.AppendChild(dictionaryNode);
        }

        /// <summary>
        /// 
        /// </summary>
        public void Deserialise(XmlReader reader)
        {
            this.Name = reader.GetAttribute(NameAttribute);
            this.SourceSectionName = reader.GetAttribute(SectionNameAttribute);
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            bool preLoaded = false;
            if (this.Root.SourceTXDs != null && this.Root.SourceTXDs.Count != 0)
            {
                if (this.Root.SourceTXDs.ContainsKey(this.Name.ToLower()))
                {
                    foreach (var texture in this.Root.SourceTXDs[this.Name.ToLower()])
                    {
                        SourceTexture newTexture = new SourceTexture(texture, this);
                        this.Textures.Add(newTexture.StreamName, newTexture);
                    }
                    preLoaded = true;
                    reader.Skip();
                }
            }

            if (!preLoaded)
            {
                reader.Read();
                while (reader.MoveToContent() != XmlNodeType.EndElement)
                {
                    if (reader.IsStartElement())
                    {
                        if (string.Compare(reader.Name, "sourceTexture") == 0 && this.Root.LoadSourceTextures)
                        {
                            SourceTexture newTexture = new SourceTexture(reader, this);
                            this.Textures.Add(newTexture.StreamName, newTexture);
                            continue;
                        }
                    }

                    reader.Read();
                }

                reader.Read();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        private void LoadSourceTextures(XmlReader reader)
        {
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.Read();
            while (reader.MoveToContent() != XmlNodeType.EndElement)
            {
                if (reader.IsStartElement())
                {
                    if (string.Compare(reader.Name, "sourceTexture") == 0)
                    {
                        SourceTexture newTexture = new SourceTexture(reader, this);
                        this.Textures.Add(newTexture.StreamName, newTexture);
                        continue;
                    }
                }

                reader.Read();
            }

            reader.Read();
        }

        #endregion // Serialisation / Deserialisation Methods
    }
}
