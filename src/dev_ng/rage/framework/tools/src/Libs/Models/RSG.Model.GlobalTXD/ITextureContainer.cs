﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Collections;

namespace RSG.Model.GlobalTXD
{
    public interface ITextureContainer
    {
        /// <summary>
        /// If this is true it means that source textures can be added to
        /// this container as children
        /// </summary>
        Boolean CanAcceptTextures { get; }

        /// <summary>
        /// The current size of the container based on the sum of the texture
        /// sizes inside it
        /// </summary>
        int CurrentSize { get; }

        /// <summary>
        /// A dictionary of uniquely named textures that are children of this texture container
        /// The textures are indexed by stream names and these names need to be unique.
        /// </summary>
        ObservableDictionary<String, ITextureChild> Textures { get; }

        /// <summary>
        /// Promotes the given texture (source or global) to here and makes sure that
        /// validation is done so that the texture is removed elsewhere
        /// </summary>
        Boolean PromoteTextureHere(SourceTexture texture);

        /// <summary>
        /// Moves the given global texture from its previous global dictionary to this 
        /// global dictionary
        /// </summary>
        Boolean MoveTextureHere(GlobalTexture texture);

        /// <summary>
        /// Moves the given global texture from this dictionary before it gets
        /// added to another dictionary
        /// </summary>
        Boolean MoveTextureFromHere(GlobalTexture texture);

        /// <summary>
        /// Gets called whenever a texture inside of this container has their promotion
        /// state changed
        /// </summary>
        void OnPromotionChanged(ITextureChild sender, Boolean promoted);

        /// <summary>
        /// Gets called whenever a texture inside of this container has their stream 
        /// size changed
        /// </summary>
        void OnSizeChanged(ITextureChild sender, int size);

        /// <summary>
        /// Returns true iff the container has the texture with the given stream name in it
        /// </summary>
        Boolean ContainsTexture(String streamName);
    }
}
