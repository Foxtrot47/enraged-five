﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Linq;
using RSG.Model.Common.Report;

namespace RSG.Model.Statistics.Platform
{
    /// <summary>
    /// A single resource bucket
    /// </summary>
    [Serializable]   
    [DataContract]
    [ReportType]
    public class ResourceBucket
    {
        /// <summary>
        /// Bucket size in bytes.
        /// </summary>
        [DataMember]
        public UInt32 Size { get; set; }

        /// <summary>
        /// Bucket capacity in bytes.
        /// </summary>
        [DataMember]
        public UInt32 Capacity { get; set; }

        /// <summary>
        /// Bucket ID
        /// </summary>
        [DataMember]
        public UInt32 ID { get; set; }

        /// <summary>
        /// Bucket Type
        /// </summary>
        [DataMember]
        public ResourceBucketType BucketType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ResourceBucket()
        {
        }

        /// <summary>
        /// Parameterised constructor
        /// </summary>
        /// <param name="bucketType"></param>
        /// <param name="id"></param>
        /// <param name="size"></param>
        /// <param name="capacity"></param>
        public ResourceBucket(ResourceBucketType bucketType, uint id, uint size, uint capacity)
        {
            BucketType = bucketType;
            ID = id;
            Size = size;
            Capacity = capacity;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public XElement Serialise()
        {
            XElement xmlBucket = new XElement("Bucket",
                new XElement("ID", this.ID),
                new XElement("BucketType", this.BucketType),
                new XElement("Size", this.Size),
                new XElement("Capacity", this.Capacity));
            return (xmlBucket);
        }
    }
}
