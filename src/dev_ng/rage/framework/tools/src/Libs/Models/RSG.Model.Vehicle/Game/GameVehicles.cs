﻿namespace RSG.Model.Vehicle.Game
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using RSG.Base.Configuration;
    using RSG.Metadata.Data;
    using RSG.Metadata.Model;
    using RSG.Metadata.Parser;
    using RSG.Model.Common;

    /// <summary>
    /// Represents all of the vehicles that are being mounted by the game through the
    /// vehicles.meta data file and its associated meta files.
    /// </summary>
    public class GameVehicles
    {
        #region Fields
        /// <summary>
        /// The name of the vehicles metadata file that contains the data mounted by the game
        /// that defines all of the vehicles used in the game.
        /// </summary>
        private const string vehicleFilename = @"data\levels\{0}\vehicles.meta";

        /// <summary>
        /// The name of the vehicles metadata file that contains the data defining the link
        /// between vehcile and vehicle kits.
        /// </summary>
        private const string variationFilename = @"data\carvariations.pso.meta";

        /// <summary>
        /// The name of the vehicles metadata file that contains the data defining the link
        /// between vehcile and vehicle kits.
        /// </summary>
        private const string modelSetFilename = @"data\ai\vehiclemodelsets.meta";

        /// <summary>
        /// The name of the vehicles metadata file that contains the data defining the
        /// individual kits that can be attached to each vehicle.
        /// </summary>
        private const string carcolsFilename = @"data\carcols.pso.meta";

        /// <summary>
        /// The private field used for the <see cref="Vehicles"/> property.
        /// </summary>
        private List<GameVehicle> _vehicles;

        /// <summary>
        /// The private field used for the <see cref="VehicleKits"/> property.
        /// </summary>
        private List<GameVehicleKit> _vehicleKits;

        /// <summary>
        /// The private field used for the <see cref="VehicleVariations"/> property.
        /// </summary>
        private List<GameVehicleVariation> _vehicleVariations;

        /// <summary>
        /// The private field used for the <see cref="TextureDictionaries"/> property.
        /// </summary>
        private List<string> _textureDictionaries;

        /// <summary>
        /// The private field used for the <see cref="ModelSets"/> property.
        /// </summary>
        private Dictionary<string, List<string>> _modelSets;

        /// <summary>
        /// The private field used for the <see cref="WheelModels"/> property.
        /// </summary>
        private List<string> _wheelModels;
        #endregion Fields
        
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="GameWeapons"/> class using the
        /// specified game branch and level to locate the data files used during loading.
        /// </summary>
        /// <param name="branch">
        /// The branch that the data should be loaded from.
        /// </param>
        /// <param name="structures">
        /// The structure dictionary containing the metadata definitions to use to parse the
        /// metadata files.
        /// </param>
        public GameVehicles(IBranch branch, ILevel level, StructureDictionary structures)
        {
            this._vehicles = new List<GameVehicle>();
            this.ParseCarColsMetadata(branch, structures);
            this.ParseVariationMetadata(branch, structures);
            this.ParseVehicleMetadata(branch, level, structures);
            this.ParseModelSetMetadata(branch, structures);
        }
        #endregion Constructors

        #region Properties
        public List<GameVehicleVariation> VehicleVariations
        {
            get { return this._vehicleVariations; }
        }

        public List<GameVehicleKit> VehicleKits
        {
            get { return this._vehicleKits; }
        }

        public List<GameVehicle> Vehicles
        {
            get { return this._vehicles; }
        }

        public List<string> WheelModels
        {
            get { return this._wheelModels; }
        }

        public List<string> TextureDictionaries
        {
            get { return this._textureDictionaries; }
        }

        public Dictionary<string, List<string>> ModelSets
        {
            get { return this._modelSets; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// A iterator over the vehciles that have been defined in the metadata that have the
        /// specified game name.
        /// </summary>
        /// <param name="gameName">
        /// The name of the vehicles to walk over.
        /// </param>
        /// <returns>
        /// A iterator around the defined vehicles that have the specified game name.
        /// </returns>
        public IEnumerable<GameVehicle> GetVehiclesWithGameName(string gameName)
        {
            foreach (GameVehicle vehicle in this._vehicles)
            {
                if (String.CompareOrdinal(vehicle.Name, gameName) == 0)
                {
                    yield return vehicle;
                }
            }
        }

        /// <summary>
        /// Parses the carcols metadata file.
        /// </summary>
        /// <param name="filename">
        /// The name of the file to parse.
        /// </param>
        /// <param name="structures">
        /// The structure dictionary containing the metadata definitions to use to parse the
        /// specified file.
        /// </param>
        private void ParseCarColsMetadata(IBranch branch, StructureDictionary structures)
        {
            string path = Path.Combine(branch.Export, carcolsFilename);
            if (!File.Exists(path))
            {
                return;
            }

            MetaFile metadata = new MetaFile(path, structures, branch);

            Structure kitStructure = structures["CVehicleKit"];
            Structure visibleModStructure = structures["CVehicleModVisible"];
            Structure modLinkStructure = structures["CVehicleModLink"];
            List<StructureTunable> kits = new List<StructureTunable>();
            foreach (StructureTunable t in metadata.AllMembers.OfType<StructureTunable>())
            {
                StructMember m = t.Definition as StructMember;
                if (m == null || m.Definition == null)
                {
                    continue;
                }

                if (!m.Definition.IsA(kitStructure))
                {
                    continue;
                }

                kits.Add(t);
            }

            this._vehicleKits = new List<GameVehicleKit>(kits.Count);
            foreach (StructureTunable kit in kits)
            {
                GameVehicleKit newKit = new GameVehicleKit();
                foreach (StringTunable child in kit.Values.OfType<StringTunable>())
                {
                    if (string.Compare(child.Name, "kitName", true) == 0)
                    {
                        newKit.Name = child.Value.ToLower();
                    }
                }

                foreach (ArrayTunable child in kit.Values.OfType<ArrayTunable>())
                {
                    if (string.Compare(child.Name, "visibleMods", true) != 0 && string.Compare(child.Name, "linkMods", true) != 0)
                    {
                        continue;
                    }

                    foreach (StructureTunable t in child.Items.OfType<StructureTunable>())
                    {
                        StructMember m = t.Definition as StructMember;
                        if (m == null || m.Definition == null)
                        {
                            continue;
                        }

                        if (!m.Definition.IsA(visibleModStructure) && !m.Definition.IsA(modLinkStructure))
                        {
                            continue;
                        }

                        foreach (StringTunable itemChild in t.Values.OfType<StringTunable>())
                        {
                            if (string.Compare(itemChild.Name, "modelName", true) == 0)
                            {
                                newKit.AddModel(itemChild.Value.ToLower());
                                break;
                            }
                        }
                    }
                }

                this._vehicleKits.Add(newKit);
            }

            Structure wheelStructure = structures["CVehicleWheel"];
            List<StructureTunable> wheels = new List<StructureTunable>();
            foreach (StructureTunable t in metadata.AllMembers.OfType<StructureTunable>())
            {
                StructMember m = t.Definition as StructMember;
                if (m == null || m.Definition == null)
                {
                    continue;
                }

                if (!m.Definition.IsA(wheelStructure))
                {
                    continue;
                }

                wheels.Add(t);
            }

            this._wheelModels = new List<string>(wheels.Count);
            foreach (StructureTunable wheel in wheels)
            {
                foreach (StringTunable child in wheel.Values.OfType<StringTunable>())
                {
                    if (string.Compare(child.Name, "wheelName", true) == 0)
                    {
                        this._wheelModels.Add(child.Value.ToLower());
                    }
                    else if (string.Compare(child.Name, "wheelVariation", true) == 0)
                    {
                        this._wheelModels.Add(child.Value.ToLower());
                    }
                }
            }
        }

        /// <summary>
        /// Parses the variation metadata file.
        /// </summary>
        /// <param name="filename">
        /// The name of the file to parse.
        /// </param>
        /// <param name="structures">
        /// The structure dictionary containing the metadata definitions to use to parse the
        /// specified file.
        /// </param>
        private void ParseVariationMetadata(IBranch branch, StructureDictionary structures)
        {
            string path = Path.Combine(branch.Export, variationFilename);
            if (!File.Exists(path))
            {
                return;
            }

            MetaFile metadata = new MetaFile(path, structures, branch);

            Structure dataStructure = structures["CVehicleVariationData"];
            List<StructureTunable> variations = new List<StructureTunable>();
            foreach (StructureTunable t in metadata.AllMembers.OfType<StructureTunable>())
            {
                StructMember m = t.Definition as StructMember;
                if (m == null || m.Definition == null)
                {
                    continue;
                }

                if (!m.Definition.IsA(dataStructure))
                {
                    continue;
                }

                variations.Add(t);
            }

            this._vehicleVariations = new List<GameVehicleVariation>(variations.Count);
            foreach (StructureTunable variation in variations)
            {
                GameVehicleVariation newVariation = new GameVehicleVariation();
                foreach (StringTunable child in variation.Values.OfType<StringTunable>())
                {
                    if (string.Compare(child.Name, "modelName", true) == 0)
                    {
                        if (child.Value != null)
                        {
                            newVariation.ModelName = child.Value.ToLower();
                        }
                        else
                        {
                            newVariation.ModelName = child.Value;
                        }
                    }
                }

                foreach (ArrayTunable child in variation.Values.OfType<ArrayTunable>())
                {
                    if (string.Compare(child.Name, "kits", true) != 0)
                    {
                        continue;
                    }

                    foreach (StringTunable item in child.Items.OfType<StringTunable>())
                    {
                        GameVehicleKit kit = (from k in this._vehicleKits
                                              where k.Name == item.Value.ToLower()
                                              select k).FirstOrDefault();

                        newVariation.AddVehicleKit(item.Value.ToLower(), kit);
                    }
                    break;
                }

                this._vehicleVariations.Add(newVariation);
            }
        }

        /// <summary>
        /// Parses the vehicle metadata file.
        /// </summary>
        /// <param name="filename">
        /// The name of the file to parse.
        /// </param>
        /// <param name="structures">
        /// The structure dictionary containing the metadata definitions to use to parse the
        /// specified file.
        /// </param>
        private void ParseVehicleMetadata(IBranch branch, ILevel level, StructureDictionary structures)
        {
            string path = Path.Combine(branch.Common, String.Format(vehicleFilename, level.Name));
            if (!File.Exists(path))
            {
                return;
            }

            MetaFile metadata = new MetaFile(path, structures, branch);

            Structure dataStructure = structures["CVehicleModelInfo::InitData"];
            Structure textureStructure = structures["CTxdRelationship"];
            List<StructureTunable> vehicles = new List<StructureTunable>();
            List<StructureTunable> textureRelationships = new List<StructureTunable>();
            foreach (StructureTunable t in metadata.AllMembers.OfType<StructureTunable>())
            {
                StructMember m = t.Definition as StructMember;
                if (m == null || m.Definition == null)
                {
                    continue;
                }

                if (m.Definition.IsA(dataStructure))
                {
                    vehicles.Add(t);
                }

                if (m.Definition.IsA(textureStructure))
                {
                    textureRelationships.Add(t);
                }
            }

            this._vehicles = new List<GameVehicle>(vehicles.Count);
            List<string> vehicleTextureDictionaries = new List<string>();
            foreach (StructureTunable vehicle in vehicles)
            {
                GameVehicle newVehicle = new GameVehicle();
                foreach (StringTunable child in vehicle.Values.OfType<StringTunable>())
                {
                    if (string.Compare(child.Name, "modelName", true) == 0)
                    {
                        newVehicle.ModelName = child.Value.ToLower();
                    }
                    else if (string.Compare(child.Name, "gameName", true) == 0)
                    {
                        newVehicle.Name = child.Value.ToLower();
                    }
                    else if (string.Compare(child.Name, "txdName", true) == 0)
                    {
                        newVehicle.TextureDictionary = child.Value.ToLower();
                        vehicleTextureDictionaries.Add(newVehicle.TextureDictionary);
                    }
                    else if (string.Compare(child.Name, "animConvRoofDictName", true) == 0)
                    {
                        if (child.Value != "null")
                        {
                            newVehicle.RoofClipDictionary = child.Value.ToLower();
                        }
                    }
                }

                newVehicle.Variation = (from v in this._vehicleVariations
                                        where v.ModelName == newVehicle.ModelName
                                        select v).FirstOrDefault();

                this._vehicles.Add(newVehicle);
            }

            this._textureDictionaries = new List<string>();
            foreach (StructureTunable vehicle in textureRelationships)
            {
                foreach (StringTunable child in vehicle.Values.OfType<StringTunable>())
                {
                    string txdName = null;
                    if (string.Compare(child.Name, "parent", true) == 0)
                    {
                        txdName = child.Value.ToLower();
                    }
                    else if (string.Compare(child.Name, "child", true) == 0)
                    {
                        txdName = child.Value.ToLower();
                    }

                    if (txdName != null && !this._textureDictionaries.Contains(txdName))
                    {
                        if (!vehicleTextureDictionaries.Contains(txdName))
                        {
                            this._textureDictionaries.Add(txdName);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Parses the vehicle metadata file.
        /// </summary>
        /// <param name="filename">
        /// The name of the file to parse.
        /// </param>
        /// <param name="structures">
        /// The structure dictionary containing the metadata definitions to use to parse the
        /// specified file.
        /// </param>
        private void ParseModelSetMetadata(IBranch branch, StructureDictionary structures)
        {
            string path = Path.Combine(branch.Common, modelSetFilename);
            if (!File.Exists(path))
            {
                return;
            }

            MetaFile metadata = new MetaFile(path, structures, branch);

            Structure dataStructure = structures["CAmbientModelSet"];
            List<StructureTunable> modelSets = new List<StructureTunable>();
            foreach (StructureTunable t in metadata.AllMembers.OfType<StructureTunable>())
            {
                StructMember m = t.Definition as StructMember;
                if (m == null || m.Definition == null)
                {
                    continue;
                }

                if (!m.Definition.IsA(dataStructure))
                {
                    continue;
                }

                modelSets.Add(t);
            }

            this._modelSets = new Dictionary<string, List<string>>();
            foreach (StructureTunable modelSet in modelSets)
            {
                string setName = null;
                foreach (StringTunable child in modelSet.Values.OfType<StringTunable>())
                {
                    if (string.Compare(child.Name, "Name", true) == 0)
                    {
                        setName = child.Value;
                        break;
                    }
                }

                List<string> modelNames = new List<string>();
                foreach (ArrayTunable child in modelSet.Values.OfType<ArrayTunable>())
                {
                    if (string.Compare(child.Name, "Models", true) != 0)
                    {
                        continue;
                    }

                    foreach (StructureTunable item in child.Items.OfType<StructureTunable>())
                    {
                        foreach (StringTunable itemChild in item.Values.OfType<StringTunable>())
                        {
                            if (string.Compare(itemChild.Name, "Name", true) == 0)
                            {
                                modelNames.Add(itemChild.Value.ToLower());
                                break;
                            }
                        }
                    }

                    break;
                }

                this._modelSets.Add(setName, modelNames);
            }
        }
        #endregion Methods
    } // RSG.Model.Vehicle.Game.GameVehicles {Class}
} // RSG.Model.Vehicle.Game {Namespace}
