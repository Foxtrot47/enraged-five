﻿// --------------------------------------------------------------------------------------------
// <copyright file="IHasStep{T}.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions.Members
{
    /// <summary>
    /// When implemented represents a member that has a step property.
    /// </summary>
    /// <typeparam name="T">
    /// The type of the members step value.
    /// </typeparam>
    public interface IHasStep<T> : IMember
    {
        #region Properties
        /// <summary>
        /// Gets or sets the increase and decrease amount for a single step.
        /// </summary>
        T Step { get; set; }
        #endregion Properties
    } // RSG.Metadata.Model.Definitions.Members.IHasStep{T} {Interface}
} // RSG.Metadata.Model.Definitions.Members {Namespace}
