﻿//---------------------------------------------------------------------------------------------
// <copyright file="ItemIncludeComparer.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.Model
{
    using System;

    /// <summary>
    /// Supports the comparison between include values in a project item object.
    /// </summary>
    public class ItemIncludeComparer
    {
        #region Methods
        /// <summary>
        /// Determines whether the specified objects are equal.
        /// </summary>
        /// <param name="x">
        /// The first object of type T to compare.
        /// </param>
        /// <param name="y">
        /// The second object of type T to compare.
        /// </param>
        /// <returns>
        /// True if the specified objects are equal; otherwise, false.
        /// </returns>
        public bool Equals(string x, string y)
        {
            return String.Equals(x, y, StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Determines whether the specified objects are equal.
        /// </summary>
        /// <param name="x">
        /// The first object of type T to compare.
        /// </param>
        /// <param name="y">
        /// The second object of type T to compare.
        /// </param>
        /// <returns>
        /// True if the specified objects are equal; otherwise, false.
        /// </returns>
        public bool Equals(ProjectItem x, string y)
        {
            if (x == null)
            {
                return false;
            }

            return String.Equals(x.Include, y, StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Determines whether the specified objects are equal.
        /// </summary>
        /// <param name="x">
        /// The first object of type T to compare.
        /// </param>
        /// <param name="y">
        /// The second object of type T to compare.
        /// </param>
        /// <returns>
        /// True if the specified objects are equal; otherwise, false.
        /// </returns>
        public bool Equals(string x, ProjectItem y)
        {
            if (y == null)
            {
                return false;
            }

            return String.Equals(x, y.Include, StringComparison.OrdinalIgnoreCase);
        }
        #endregion Methods
    } // RSG.Project.Model.ItemIncludeComparer {Class}
} // RSG.Project.Model {Namespace}
