﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.IO;
using RSG.Model.Statistics.Captures;

namespace RSG.Model.Report.Reports.GameStatsReports.Tests
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class SmokeTestBase : ISmokeTest
    {
        #region Properties
        /// <summary>
        /// Name of the test
        /// </summary>
        public string SmokeTestName
        {
            get;
            private set;
        }

        /// <summary>
        /// List of errors generated by running the test
        /// </summary>
        public IList<string> Errors
        {
            get;
            private set;
        }

        /// <summary>
        /// List of warnings generated by running the test
        /// </summary>
        public IList<string> Warnings
        {
            get;
            private set;
        }

        /// <summary>
        /// List of messages generated by running the test
        /// </summary>
        public IList<string> Messages
        {
            get;
            private set;
        }

        /// <summary>
        /// Graphs that are common to all tests
        /// </summary>
        public IList<GraphImage> CommonGraphs
        {
            get;
            private set;
        }

        /// <summary>
        /// Graphs related to only a single test
        /// </summary>
        public IDictionary<string, IList<GraphImage>> PerTestGraphs
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        public SmokeTestBase(string name)
        {
            SmokeTestName = name;
            Errors = new List<string>();
            Warnings = new List<string>();
            Messages = new List<string>();
            CommonGraphs = new List<GraphImage>();
            PerTestGraphs = new Dictionary<string, IList<GraphImage>>();
        }
        #endregion // Constructor(s)

        #region ISmokeTest Methods
        /// <summary>
        /// Executes the particular test
        /// </summary>
        /// <param name="testSessions">List of test sessions</param>
        public virtual void RunTest(IList<TestSession> testSessions)
        {
            Messages.Clear();
            Warnings.Clear();
            Errors.Clear();
            CommonGraphs.Clear();
            PerTestGraphs.Clear();
        }

        /// <summary>
        /// Generates common graphs for this smoke test
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns>Null if the smoke test doesn't generate any graph</returns>
        public abstract IList<GraphImage> GenerateGraphs(DateTime start, DateTime end);

        /// <summary>
        /// Write this test's portion of the report
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="test">The name of the test for which we wish to write the report</param>
        /// <param name="tableResults"></param>
        public virtual void WriteReport(HtmlTextWriter writer, string test, bool includeHeader, uint tableResults, bool email)
        {
            if (includeHeader)
            {
                WriteSectionHeader(test, writer);
            }

            // Output the graphs for this test
            if (PerTestGraphs.ContainsKey(test))
            {
                foreach (GraphImage image in PerTestGraphs[test])
                {
                    string imageSource = image.Filepath;
                    if (email)
                    {
                        imageSource = String.Format("cid:{0}", image.Name);
                    }

                    // Write out the image itself
                    writer.AddAttribute(HtmlTextWriterAttribute.Src, imageSource);
                    writer.RenderBeginTag(HtmlTextWriterTag.Img);
                    writer.RenderEndTag();
                    //writer.RenderBeginTag(HtmlTextWriterTag.Br);
                    //writer.RenderEndTag();
                }
            }
        }
        #endregion // Methods

        #region Protected Methods
        /// <summary>
        /// Writes a generic header to the report (including an anchor)
        /// </summary>
        /// <param name="test"></param>
        /// <param name="writer"></param>
        protected void WriteSectionHeader(string test, HtmlTextWriter writer)
        {
            writer.RenderBeginTag(HtmlTextWriterTag.H3);
            writer.AddAttribute(HtmlTextWriterAttribute.Name, String.Format("{0}_{1}", test, SmokeTestName));
            writer.RenderBeginTag(HtmlTextWriterTag.A);
            writer.Write(SmokeTestName);
            writer.RenderEndTag();
            writer.RenderEndTag();
        }
        #endregion // Protected Methods
    } // SmokeTestBase
}
