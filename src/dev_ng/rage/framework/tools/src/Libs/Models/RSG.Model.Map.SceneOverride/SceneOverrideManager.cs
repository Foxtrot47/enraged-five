﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using RSG.Base.Configuration;
using RSG.SceneXml;
using RSG.Pipeline.Core;
using RSG.Pipeline.Content;

namespace RSG.Model.Map.SceneOverride
{
    /// <summary>
    /// The default implementation of the main LOD Override subsystem interface
    /// </summary>
    internal class SceneOverrideManager : ISceneOverrideManager
    {
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="treeHelper"></param>
        /// <param name="repository"></param>
        internal SceneOverrideManager(IBranch branch, ContentTreeHelper treeHelper,
            ISceneOverrideRepository repository)
        {
            branch_ = branch;
            treeHelper_ = treeHelper;
            repository_ = repository;
            objectEntityKeyMap_ = new Dictionary<TargetObjectDef, EntityKey>();
            sceneProcessedContentNameMap_ = new Dictionary<Scene, string>();
        }
        #endregion // Constructor(s)

        #region ISceneOverrideManager Members

        public void Submit(IEnumerable<RuntimeLODOverride> runtimeLODOverrides, 
                           IEnumerable<RuntimeEntityDeleteItem> runtimeDeleteItems,
                           IEnumerable<RuntimeAttributeOverride> runtimeAttributeOverrides)
        {
            int userId = repository_.FindUserKey(Environment.UserName);
            if (userId == -1)
                userId = repository_.CreateUser(new UserRecord(userId, Environment.UserName));

            // LOD overrides
            List<LODOverrideRecord> lodOverrideRecordsToUpdate = new List<LODOverrideRecord>();

            foreach (RuntimeLODOverride runtimeLODOverride in runtimeLODOverrides)
            {
                string processedContentName = GetProcessedContentNameFromIMAPName(runtimeLODOverride.IMAPName);
                int processedContentId = repository_.FindProcessedContentKey(processedContentName);
                if (processedContentId == -1)
                    processedContentId = repository_.CreateProcessedContent(new ProcessedContentRecord(processedContentId, processedContentName));
                
                LODOverrideRecord newRecord =
                    new LODOverrideRecord(
                        processedContentId,
                        runtimeLODOverride.Hash,
                        runtimeLODOverride.Guid,
                        runtimeLODOverride.ModelName,
                        runtimeLODOverride.PosX,
                        runtimeLODOverride.PosY,
                        runtimeLODOverride.PosZ,
                        userId,
                        runtimeLODOverride.Distance,
                        runtimeLODOverride.ChildDistance,
                        DateTime.UtcNow.ToFileTime());
                lodOverrideRecordsToUpdate.Add(newRecord);
            }

            repository_.CreateOrUpdateLODOverrides(lodOverrideRecordsToUpdate);

            // Entity deletes
            List<EntityDeleteRecord> entityDeleteRecordsToUpdate = new List<EntityDeleteRecord>();

            foreach (RuntimeEntityDeleteItem runtimeDeleteItem in runtimeDeleteItems)
            {
                string processedContentName = GetProcessedContentNameFromIMAPName(runtimeDeleteItem.IMAPName);
                int processedContentId = repository_.FindProcessedContentKey(processedContentName);
                if (processedContentId == -1)
                    processedContentId = repository_.CreateProcessedContent(new ProcessedContentRecord(processedContentId, processedContentName));

                EntityDeleteRecord newRecord =
                    new EntityDeleteRecord(
                        processedContentId,
                        runtimeDeleteItem.Hash,
                        runtimeDeleteItem.Guid,
                        runtimeDeleteItem.ModelName,
                        runtimeDeleteItem.PosX,
                        runtimeDeleteItem.PosY,
                        runtimeDeleteItem.PosZ,
                        userId,
                        DateTime.UtcNow.ToFileTime());
                entityDeleteRecordsToUpdate.Add(newRecord);
            }

            repository_.CreateOrUpdateEntityDeletes(entityDeleteRecordsToUpdate);

            // Atrtibute overrides
            List<AttributeOverrideRecord> attributeOverrideRecordsToUpdate = new List<AttributeOverrideRecord>();

            foreach (RuntimeAttributeOverride runtimeAttributeOverride in runtimeAttributeOverrides)
            {
                string processedContentName = GetProcessedContentNameFromIMAPName(runtimeAttributeOverride.IMAPName);
                int processedContentId = repository_.FindProcessedContentKey(processedContentName);
                if (processedContentId == -1)
                    processedContentId = repository_.CreateProcessedContent(new ProcessedContentRecord(processedContentId, processedContentName));

                AttributeOverrideRecord newRecord =
                    new AttributeOverrideRecord(
                        processedContentId,
                        runtimeAttributeOverride.Hash,
                        runtimeAttributeOverride.Guid,
                        runtimeAttributeOverride.ModelName,
                        runtimeAttributeOverride.PosX,
                        runtimeAttributeOverride.PosY,
                        runtimeAttributeOverride.PosZ,
                        userId,
                        runtimeAttributeOverride.DontCastShadows,
                        runtimeAttributeOverride.DontRenderInShadows,
                        runtimeAttributeOverride.DontRenderInReflections,
                        runtimeAttributeOverride.OnlyRenderInReflections,
                        runtimeAttributeOverride.DontRenderInWaterReflections,
                        runtimeAttributeOverride.OnlyRenderInWaterReflections,
                        runtimeAttributeOverride.DontRenderInMirrorReflections,
                        runtimeAttributeOverride.OnlyRenderInMirrorReflections,
                        runtimeAttributeOverride.StreamingPriorityLow,
                        runtimeAttributeOverride.Priority,
                        DateTime.UtcNow.ToFileTime());
                attributeOverrideRecordsToUpdate.Add(newRecord);
            }

            repository_.CreateOrUpdateAttributeOverrides(attributeOverrideRecordsToUpdate);
        }

        public bool HasLODDistanceOverrideForObject(TargetObjectDef objectDef)
        {
            EntityKey key = GetObjectEntityKey(objectDef);
            LODOverrideRecord record = repository_.GetLODOverride(key);

            return (record != null && record.HasDistance);
        }

        public bool HasChildLODDistanceOverrideForObject(TargetObjectDef objectDef)
        {
            EntityKey key = GetObjectEntityKey(objectDef);
            LODOverrideRecord record = repository_.GetLODOverride(key);

            return (record != null && record.HasChildDistance);
        }

        public bool IsEntityMarkedForDeletion(TargetObjectDef objectDef)
        {
            EntityKey key = GetObjectEntityKey(objectDef);
            EntityDeleteRecord record = repository_.GetEntityDelete(key);

            return (record != null);
        }

        public bool HasAttributeOverridesForObject(TargetObjectDef objectDef)
        {
            EntityKey key = GetObjectEntityKey(objectDef);
            AttributeOverrideRecord record = repository_.GetAttributeOverride(key);

            return (record != null);
        }

        public float GetLODDistanceOverrideForObject(TargetObjectDef objectDef)
        {
            EntityKey key = GetObjectEntityKey(objectDef);
            LODOverrideRecord record = repository_.GetLODOverride(key);

            return record.Distance;
        }

        public float GetChildLODDistanceOverrideForObject(TargetObjectDef objectDef)
        {
            EntityKey key = GetObjectEntityKey(objectDef);
            LODOverrideRecord record = repository_.GetLODOverride(key);

            return record.ChildDistance;
        }

        public bool GetDontCastShadowsAttributeValue(TargetObjectDef objectDef)
        {
            EntityKey key = GetObjectEntityKey(objectDef);
            AttributeOverrideRecord record = repository_.GetAttributeOverride(key);

            return record.DontCastShadows;
        }

        public bool GetDontRenderInShadowsAttributeValue(TargetObjectDef objectDef)
        {
            EntityKey key = GetObjectEntityKey(objectDef);
            AttributeOverrideRecord record = repository_.GetAttributeOverride(key);

            return record.DontRenderInShadows;
        }

        public bool GetDontRenderInReflectionsAttributeValue(TargetObjectDef objectDef)
        {
            EntityKey key = GetObjectEntityKey(objectDef);
            AttributeOverrideRecord record = repository_.GetAttributeOverride(key);

            return record.DontRenderInReflections;
        }

        public bool GetOnlyRenderInReflectionsAttributeValue(TargetObjectDef objectDef)
        {
            EntityKey key = GetObjectEntityKey(objectDef);
            AttributeOverrideRecord record = repository_.GetAttributeOverride(key);

            return record.OnlyRenderInReflections;
        }

        public bool GetDontRenderInWaterReflectionsAttributeValue(TargetObjectDef objectDef)
        {
            EntityKey key = GetObjectEntityKey(objectDef);
            AttributeOverrideRecord record = repository_.GetAttributeOverride(key);

            return record.DontRenderInWaterReflections;
        }

        public bool GetOnlyRenderInWaterReflectionsAttributeValue(TargetObjectDef objectDef)
        {
            EntityKey key = GetObjectEntityKey(objectDef);
            AttributeOverrideRecord record = repository_.GetAttributeOverride(key);

            return record.OnlyRenderInWaterReflections;
        }

        public bool GetDontRenderInMirrorReflectionsAttributeValue(TargetObjectDef objectDef)
        {
            EntityKey key = GetObjectEntityKey(objectDef);
            AttributeOverrideRecord record = repository_.GetAttributeOverride(key);

            return record.DontRenderInMirrorReflections;
        }

        public bool GetOnlyRenderInMirrorReflectionsAttributeValue(TargetObjectDef objectDef)
        {
            EntityKey key = GetObjectEntityKey(objectDef);
            AttributeOverrideRecord record = repository_.GetAttributeOverride(key);

            return record.OnlyRenderInMirrorReflections;
        }

        public bool GetStreamingPriorityLowAttributeValue(TargetObjectDef objectDef)
        {
            EntityKey key = GetObjectEntityKey(objectDef);
            AttributeOverrideRecord record = repository_.GetAttributeOverride(key);

            return record.StreamingPriorityLow;
        }

        public int GetPriorityAttributeValue(TargetObjectDef objectDef)
        {
            EntityKey key = GetObjectEntityKey(objectDef);
            AttributeOverrideRecord record = repository_.GetAttributeOverride(key);

            return record.Priority;
        }

        public bool HasLODDistanceOverrideForObject(DCCEntityKey entityKey)
        {
            EntityKey key = GetObjectEntityKey(entityKey);
            LODOverrideRecord record = repository_.GetLODOverride(key);

            return (record != null && record.HasDistance);
        }

        public bool HasChildLODDistanceOverrideForObject(DCCEntityKey entityKey)
        {
            EntityKey key = GetObjectEntityKey(entityKey);
            LODOverrideRecord record = repository_.GetLODOverride(key);

            return (record != null && record.HasChildDistance);
        }

        public bool IsEntityMarkedForDeletion(DCCEntityKey entityKey)
        {
            EntityKey key = GetObjectEntityKey(entityKey);
            EntityDeleteRecord record = repository_.GetEntityDelete(key);

            return (record != null);
        }

        public bool HasAttributeOverridesForObject(DCCEntityKey entityKey)
        {
            EntityKey key = GetObjectEntityKey(entityKey);
            AttributeOverrideRecord record = repository_.GetAttributeOverride(key);

            return (record != null);
        }

        public float GetLODDistanceOverrideForObject(DCCEntityKey entityKey)
        {
            EntityKey key = GetObjectEntityKey(entityKey);
            LODOverrideRecord record = repository_.GetLODOverride(key);

            return record.Distance;
        }

        public float GetChildLODDistanceOverrideForObject(DCCEntityKey entityKey)
        {
            EntityKey key = GetObjectEntityKey(entityKey);
            LODOverrideRecord record = repository_.GetLODOverride(key);

            return record.ChildDistance;
        }

        public bool GetDontCastShadowsAttributeValue(DCCEntityKey entityKey)
        {
            EntityKey key = GetObjectEntityKey(entityKey);
            AttributeOverrideRecord record = repository_.GetAttributeOverride(key);

            return record.DontCastShadows;
        }

        public bool GetDontRenderInShadowsAttributeValue(DCCEntityKey entityKey)
        {
            EntityKey key = GetObjectEntityKey(entityKey);
            AttributeOverrideRecord record = repository_.GetAttributeOverride(key);

            return record.DontRenderInShadows;
        }

        public bool GetDontRenderInReflectionsAttributeValue(DCCEntityKey entityKey)
        {
            EntityKey key = GetObjectEntityKey(entityKey);
            AttributeOverrideRecord record = repository_.GetAttributeOverride(key);

            return record.DontRenderInReflections;
        }

        public bool GetOnlyRenderInReflectionsAttributeValue(DCCEntityKey entityKey)
        {
            EntityKey key = GetObjectEntityKey(entityKey);
            AttributeOverrideRecord record = repository_.GetAttributeOverride(key);

            return record.OnlyRenderInReflections;
        }

        public bool GetDontRenderInWaterReflectionsAttributeValue(DCCEntityKey entityKey)
        {
            EntityKey key = GetObjectEntityKey(entityKey);
            AttributeOverrideRecord record = repository_.GetAttributeOverride(key);

            return record.DontRenderInWaterReflections;
        }

        public bool GetOnlyRenderInWaterReflectionsAttributeValue(DCCEntityKey entityKey)
        {
            EntityKey key = GetObjectEntityKey(entityKey);
            AttributeOverrideRecord record = repository_.GetAttributeOverride(key);

            return record.OnlyRenderInWaterReflections;
        }

        public bool GetDontRenderInMirrorReflectionsAttributeValue(DCCEntityKey entityKey)
        {
            EntityKey key = GetObjectEntityKey(entityKey);
            AttributeOverrideRecord record = repository_.GetAttributeOverride(key);

            return record.DontRenderInMirrorReflections;
        }

        public bool GetOnlyRenderInMirrorReflectionsAttributeValue(DCCEntityKey entityKey)
        {
            EntityKey key = GetObjectEntityKey(entityKey);
            AttributeOverrideRecord record = repository_.GetAttributeOverride(key);

            return record.OnlyRenderInMirrorReflections;
        }

        public bool GetStreamingPriorityLowAttributeValue(DCCEntityKey entityKey)
        {
            EntityKey key = GetObjectEntityKey(entityKey);
            AttributeOverrideRecord record = repository_.GetAttributeOverride(key);

            return record.StreamingPriorityLow;
        }

        public int GetPriorityAttributeValue(DCCEntityKey entityKey)
        {
            EntityKey key = GetObjectEntityKey(entityKey);
            AttributeOverrideRecord record = repository_.GetAttributeOverride(key);

            return record.Priority;
        }

        #endregion

        #region Private Methods
        private static string GetProcessedContentNameFromIMAPName(string imapName)
        {
            if (imapName.Contains(streamingImapSuffix_))
            {
                return imapName.Substring(0, imapName.IndexOf(streamingImapSuffix_));
            }
            else if (imapName.Contains(streamingLongImapSuffix_))
            {
                return imapName.Substring(0, imapName.IndexOf(streamingLongImapSuffix_));
            }

            return imapName;
        }

        private EntityKey GetObjectEntityKey(TargetObjectDef objectDef)
        {
            if (!objectEntityKeyMap_.ContainsKey(objectDef))
            {
                string processedContentName = GetProcessedContentNameFromScene(objectDef.MyScene);
                int processedContentId = repository_.FindProcessedContentKey(processedContentName);

#warning DHM FIX ME: remove RSG.ManagedRage dependency but OneAtATime partial hashes are producing different results.
#if false
                
                uint guid = RSG.Base.Security.Cryptography.OneAtATime.ComputePartialHash(
                    objectDef.AttributeGuid.ToString(), objectDef.MyScene.Hash32);
                guid = RSG.Base.Security.Cryptography.OneAtATime.ComputeFinalHash(guid);

                uint hash = RSG.Base.Security.Cryptography.OneAtATime.ComputeHash(
                    objectDef.GetObjectName().ToLower());
#endif

                uint guid = RSG.ManagedRage.StringHashUtil.atStringHash(objectDef.AttributeGuid.ToString(), objectDef.MyScene.Hash32);
                uint hash = RSG.ManagedRage.StringHashUtil.atStringHash(objectDef.GetObjectName().ToLower(), 0);

                objectEntityKeyMap_.Add(objectDef, new EntityKey(processedContentId, guid, hash));
            }

            return objectEntityKeyMap_[objectDef];
        }

        private EntityKey GetObjectEntityKey(DCCEntityKey entityKey)
        {
            // Lookup the mapContentSeed and processedContentName from the content tree.
            // Failure to look up either simply leads to a degenerate key that subsequently fails to look up overrides
            uint mapContentSeed = 0;
            string processedContentName = "";

            String mapName = entityKey.MapContentName;
            IContentNode mapMaxNode = GetMapMaxNode(mapName);
            if (mapMaxNode != null)
            {
                // Get seed.
                // FD : need to cast to int before casting to uint.
                // It's a matter of 'cast into' vs 'convert into' here (see : http://stackoverflow.com/a/13290776 )
                if (mapMaxNode.Parameters.ContainsKey(Constants.ParamMap_Seed))
                    mapContentSeed = (uint)(int)mapMaxNode.Parameters[Constants.ParamMap_Seed];

                // Get processed node.
                IContentNode processedZipNode = GetMapProcessedNode(mapMaxNode);
                if (null != processedZipNode)
                    processedContentName = processedZipNode.Name;
            }

            int processedContentId = repository_.FindProcessedContentKey(processedContentName);

#warning DHM FIX ME: remove RSG.ManagedRage dependency but OneAtATime partial hashes are producing different results.
#if false
            uint containerHash = RSG.Base.Security.Cryptography.OneAtATime.ComputePartialHash(
                entityKey.ContainerAttributeGuid, mapContentSeed);
            containerHash = RSG.Base.Security.Cryptography.OneAtATime.ComputeFinalHash(containerHash);
            uint guid = RSG.Base.Security.Cryptography.OneAtATime.ComputePartialHash(
                entityKey.NodeAttributeGuid, containerHash);
            guid = RSG.Base.Security.Cryptography.OneAtATime.ComputeFinalHash(guid);
            uint hash = RSG.Base.Security.Cryptography.OneAtATime.ComputeHash(entityKey.ArchetypeName);
#endif

            uint containerHash = RSG.ManagedRage.StringHashUtil.atStringHash(entityKey.ContainerAttributeGuid, mapContentSeed);
            uint guid = RSG.ManagedRage.StringHashUtil.atStringHash(entityKey.NodeAttributeGuid, containerHash);
            uint hash = RSG.ManagedRage.StringHashUtil.atStringHash(entityKey.ArchetypeName, 0);

            return new EntityKey(processedContentId, guid, hash);
        }

        private string GetProcessedContentNameFromScene(Scene scene)
        {
            if (!sceneProcessedContentNameMap_.ContainsKey(scene))
            {
                try
                {
                    string contentName = Path.GetFileNameWithoutExtension(scene.Filename);
                    IContentNode mapMaxNode = GetMapMaxNode(contentName);
                    IContentNode mapProcessedNode = GetMapProcessedNode(mapMaxNode);

                    string processedContentName = mapProcessedNode.Name;
                    sceneProcessedContentNameMap_.Add(scene, processedContentName);
                }
                catch (Exception)
                {
                    sceneProcessedContentNameMap_.Add(scene, "");
                }
            }

            return sceneProcessedContentNameMap_[scene];
        }

        /// <summary>
        /// Return 3dsmax map Content-Node for a given name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private IContentNode GetMapMaxNode(String name)
        {
            IEnumerable<IProcess> exportProcesses = treeHelper_.GetAllMapExportProcesses();
            IEnumerable<IContentNode> exportProcessInputs = exportProcesses.SelectMany(p => p.Inputs);
            IContentNode mapExportNode = exportProcessInputs.Where(n => 
                0 == String.Compare(name, n.Name, true)).FirstOrDefault();

            return (mapExportNode);
        }

        /// <summary>
        /// Return processed map zip node.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private IContentNode GetMapProcessedNode(IContentNode mapExportNode)
        {
            IContentNode exportZipNode = treeHelper_.GetExportZipNodeFromMaxFileNode(mapExportNode);
            IContentNode processedZipNode = treeHelper_.GetProcessedZipNodeFromExportZipNode(exportZipNode);

            return (processedZipNode);
        }
        #endregion // Private Methods

        #region Member Data
        private static readonly string streamingImapSuffix_ = "_strm_";
        private static readonly string streamingLongImapSuffix_ = "_long_";

        private IBranch branch_;
        ContentTreeHelper treeHelper_;
        private ISceneOverrideRepository repository_;
        private Dictionary<TargetObjectDef, EntityKey> objectEntityKeyMap_;
        private Dictionary<Scene, string> sceneProcessedContentNameMap_;
        #endregion // Member Data
    }

} // RSG.Model.Map.SceneOverride namespace
