﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using RSG.Base.ConfigParser;
using RSG.SceneXml;
using RSG.Base.Logging;

namespace RSG.Model.Map
{
    /// <summary>
    /// A static class that is used to manage the parsing of the scene xml
    /// using this class will make sure that any single scene xml
    /// will only ever be parsed once.
    /// </summary>
    public static class SceneManager
    {
        #region Properties

        /// <summary>
        /// The main scene dictionary that references the scene xml objects by
        /// the scene xml filename
        /// </summary>
        private static Dictionary<String, SceneXml.Scene> Scenes
        {
            get { return m_scenes; }
            set { m_scenes = value; }
        }
        private static Dictionary<String, SceneXml.Scene> m_scenes;

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// Default constructor that creates the scene dictionary
        /// </summary>
        static SceneManager()
        {
            Scenes = new Dictionary<String, SceneXml.Scene>();
        }

        #endregion // Constructor(s)

        #region Public Static Function(s)

        /// <summary>
        /// This is the main function to retrive the scene file object. This
        /// function either returns a pre existing scene object or creates it
        /// and then returns it.
        /// </summary>
        /// <param name="rpfFilename"></param>
        /// <returns></returns>
        public static SceneXml.Scene GetScene(String sceneXmlFilename)
        {
            if (Scenes.ContainsKey(sceneXmlFilename))
            {
                return Scenes[sceneXmlFilename];
            }
            else
            {
                Scenes[sceneXmlFilename] = new Scene(sceneXmlFilename, null, LoadOptions.All, true);

                if (Scenes.ContainsKey(sceneXmlFilename))
                {
                    return Scenes[sceneXmlFilename];
                }
            }
            return null;
        }

        /// <summary>
        /// Enumerable that loops through all the currently existing 
        /// scene xml objects.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Scene> GetScenes()
        {
            foreach (Scene scene in Scenes.Values)
            {
                yield return scene;
            }
        }

        public static void Reset()
        {
            Scenes = null;
            Scenes = new Dictionary<String, SceneXml.Scene>();
        }

        public static void ResetScene(String sceneXmlFilename)
        {
            if (Scenes.ContainsKey(sceneXmlFilename))
            {
                Scenes.Remove(sceneXmlFilename);
            }
        }

        #endregion // Public Static Function(s)

    }
}
