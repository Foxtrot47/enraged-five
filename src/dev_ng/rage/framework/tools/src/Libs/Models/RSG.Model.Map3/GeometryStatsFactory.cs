﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;

using RSG.Model.Common;

namespace RSG.Model.Map3
{
    public class GeometryStatsFactory : IGeometryStatsFactory
    {
        public GeometryStatsFactory()
        {

        }

        public IGeometryStatsCollection CreateFromFile(string pathname)
        {
            try
            {
                return new GeometryStatsCollection(pathname);
            }
            catch
            {
                return null;
            }
        }
    }
}
