﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using RSG.Base.Attributes;
using System.Diagnostics;
using RSG.Model.Common.Map;
using System.Runtime.Serialization;
using RSG.Model.Common.Animation;

namespace RSG.Model.Common
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    public sealed class StreamableStat : IEquatable<StreamableStat>, IComparable<StreamableStat>
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public Guid Guid { get; set; }

        /// <summary>
        /// Flag indicating whether a particular stat is in the database or not.
        /// </summary>
        [DataMember]
        public bool InDatabase { get; set; }

        /// Whether the property requires processing when going from a db object to a dto object.
        /// </summary>
        [DataMember]
        public bool DbToDtoRequiresProcessing { get; set; }

        /// <summary>
        /// Whether the property requires processing when going from a dto object to a db object.
        /// </summary>
        [DataMember]
        public bool DtoToModelRequiresProcessing { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// For serialisation purposes.
        /// </summary>
        private StreamableStat()
        {
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public StreamableStat(String guid, bool inDatabase)
            : this(guid, inDatabase, false, false)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="allMappingsRequireProcessing"></param>
        public StreamableStat(String guid, bool inDatabase, bool allMappingsRequireProcessing = false)
            : this(guid, inDatabase, allMappingsRequireProcessing, allMappingsRequireProcessing)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="guidString"></param>
        /// <param name="dbToDto"></param>
        /// <param name="dtoToDb"></param>
        public StreamableStat(String guidString, bool inDatabase, bool dbToDto, bool dtoToModel)
        {
            Guid = new Guid(guidString);
            InDatabase = inDatabase;
            DbToDtoRequiresProcessing = dbToDto;
            DtoToModelRequiresProcessing = dtoToModel;
        }
        #endregion // Constructor(s)

        #region Implicit Conversions
        /// <summary>
        /// 
        /// </summary>
        /// <param name="stat"></param>
        /// <returns></returns>
        public static implicit operator Guid(StreamableStat stat)
        {
            return stat.Guid;
        }
        #endregion // Implicit Conversions

        #region IEquatable<StreamableStat>
        /// <summary>
        /// 
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(StreamableStat other)
        {
            return (other == null ? false : Guid == other.Guid);
        }
        #endregion // IEquatable<StreamableStat>
            
        #region IComparable<StreamableStat> Interface
        /// <summary>
        /// Compare this StreamableStat to another.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(StreamableStat other)
        {
            if (other == null)
            {
                return 1;
            }

            return (Guid.CompareTo(other.Guid));
        }
        #endregion // IComparable<IVehicle> Interface

        #region Object Overrides
        /// <summary>
        /// Compare
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(Object obj)
        {
            if (obj == null)
            {
                return base.Equals(obj);
            }

            return ((obj is StreamableStat) && Equals(obj as StreamableStat));
        }

        /// <summary>
        /// Return String representation of the Vehicle.
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return Guid.ToString();
        }

        /// <summary>
        /// Return hash code of Vehicle (based on Name).
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return Guid.GetHashCode();
        }
        #endregion // Object Overrides

        #region Operator Overloads
        /// <summary>
        /// 
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool operator ==(StreamableStat a, StreamableStat b)
        {
            // If both are null, or both are same instance, return true.
            if (System.Object.ReferenceEquals(a, b))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)a == null) || ((object)b == null))
            {
                return false;
            }

            // Return true if the fields match:
            return a.Equals(b);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool operator !=(StreamableStat a, StreamableStat b)
        {
            return !(a == b);
        }
        #endregion // Operator Overloads
    } // StreamableStat

    /// <summary>
    /// Attribute to use for tagging up properties.
    /// </summary>
    [AttributeUsageAttribute(AttributeTargets.Property, Inherited = true, AllowMultiple = true)]
    public class StreamableStatAttribute : Attribute
    {
        #region Properties
        /// <summary>
        /// Guid.
        /// </summary>
        public Guid Guid { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="guidString"></param>
        public StreamableStatAttribute(String guidString)
        {
            Guid = new Guid(guidString);
        }
        #endregion // Constructor(s)
    } // StreamableStatAttribute

    /// <summary>
    /// Attribute to specify that a particular stat is dependent on another one.
    /// </summary>
    [AttributeUsageAttribute(AttributeTargets.Field | AttributeTargets.Class, Inherited = true, AllowMultiple = true)]
    public class DependentStatAttribute : Attribute
    {
        #region Properties
        /// <summary>
        /// Guid.
        /// </summary>
        public Guid DependentStat { get; private set; }

        /// <summary>
        /// Whether this dependency is for a specific data source only.
        /// </summary>
        public bool SpecificMode { get; private set; }

        /// <summary>
        /// The source mode if this dependency is only for a particular one.
        /// </summary>
        public DataSource Source { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="guidString"></param>
        public DependentStatAttribute(String guidString)
        {
            DependentStat = new Guid(guidString);
            SpecificMode = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="guidString"></param>
        public DependentStatAttribute(String guidString, DataSource source)
        {
            DependentStat = new Guid(guidString);
            SpecificMode = true;
            Source = source;
        }
        #endregion // Constructor(s)
    } // DependentStatAttribute

    /// <summary>
    /// Attribute used to provide more info on a particular streamable stat.
    /// </summary>
    [AttributeUsageAttribute(AttributeTargets.Property, Inherited = true)]
    public class StreamableStatInfoAttribute : Attribute
    {
        #region Properties
        /// <summary>
        /// Whether the property requires processing when going from a db object to a dto object.
        /// </summary>
        public bool DbToDtoRequiresProcessing { get; set; }

        /// <summary>
        /// Whether the property requires processing when going from a dto object to a db object.
        /// </summary>
        public bool DtoToModelRequiresProcessing { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="guidString"></param>
        public StreamableStatInfoAttribute()
        {
        }
        #endregion // Constructor(s)
    } // StreamableStatInfoAttribute

    /// <summary>
    /// Utils for streamable stats.
    /// </summary>
    public static class StreamableStatUtils
    {
        /// <summary>
        /// Cache the guids on a per type basis for quicker lookup.
        /// </summary>
        private static IDictionary<Type, IDictionary<StreamableStat, PropertyInfo>> s_typePropertyInfoLookup = new Dictionary<Type, IDictionary<StreamableStat, PropertyInfo>>();
        
        /// <summary>
        /// Cache guid to streamable stat information.
        /// </summary>
        private static IDictionary<Guid, StreamableStat> s_streamableStatLookup = new Dictionary<Guid, StreamableStat>();

        /// <summary>
        /// List of all streamable stat classes (used when looking up mapping information).
        /// </summary>
        private static readonly Type[] s_streamableStatTypes = new Type[]
            {
                typeof(StreamableLevelStat),
                typeof(StreamableCharacterStat),
                typeof(StreamableVehicleStat),
                typeof(StreamableWeaponStat),
                typeof(StreamableMapSectionStat),
                typeof(StreamableEntityStat),
                typeof(StreamableRoomStat),
                typeof(StreamableArchetypeStat),
                typeof(StreamableMapArchetypeStat),
                typeof(StreamableSimpleMapArchetypeStat),
                //typeof(StreamableDrawableArchetypeStat),      // No special stats exist for drawables (yet).
                typeof(StreamableFragmentArchetypeStat),
                typeof(StreamableInteriorArchetypeStat),
                typeof(StreamableStatedAnimArchetypeStat),
                typeof(StreamableCutsceneStat)
            };

        /// <summary>
        /// Retrieves the list of all streamable stat values assigned to properties of a particular type.
        /// </summary>
        /// <returns></returns>
        public static ICollection<StreamableStat> GetValuesAssignedToProperties(Type type)
        {
            AddTypeToPropertyInfoLookup(type);
            return s_typePropertyInfoLookup[type].Keys;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="stats"></param>
        /// <param name="type"></param>
        [Conditional("DEBUG")]
        public static void ValidateStatsAreOfType(IEnumerable<StreamableStat> stats, Type type)
        {
#warning Add this back in once all streamable stats have been marked up.
            //IEnumerable<Guid> invalidStats = stats.Except(GetValues(type));
            //Debug.Assert(!invalidStats.Any(), String.Format("Requesting the following incorrect stats for type '{0}':\n\n{1}", type.FullName, String.Join("\n", stats)));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static IDictionary<StreamableStat, PropertyInfo> GetPropertyInfoLookup(Type type)
        {
            AddTypeToPropertyInfoLookup(type);
            return s_typePropertyInfoLookup[type];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static IList<StreamableStat> GetStreamableStatsForType(Type type)
        {
            IList<StreamableStat> stats = new List<StreamableStat>();

            foreach (FieldInfo pInfo in type.GetFields(BindingFlags.Public | BindingFlags.Static))
            {
                if (pInfo.FieldType == typeof(StreamableStat))
                {
                    stats.Add((StreamableStat)pInfo.GetValue(null));
                }
            }

            return stats;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        private static void AddTypeToPropertyInfoLookup(Type type)
        {
            // Check whether we've previously retrieved the values.
            if (!s_typePropertyInfoLookup.ContainsKey(type))
            {
                IDictionary<StreamableStat, PropertyInfo> stats = new Dictionary<StreamableStat, PropertyInfo>();
                foreach (PropertyInfo pInfo in type.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.FlattenHierarchy | BindingFlags.NonPublic))
                {
                    StreamableStatAttribute[] attributes =
                        pInfo.GetCustomAttributes(typeof(StreamableStatAttribute), true) as StreamableStatAttribute[];

                    foreach (StreamableStatAttribute attribute in attributes)
                    {
                        StreamableStat stat = GetStreamableStatForGuid(attribute.Guid);

                        if (!stats.ContainsKey(stat))
                        {
                            stats.Add(stat, pInfo);
                        }
                    }
                }
                s_typePropertyInfoLookup[type] = stats;
            }
        }

        /// <summary>
        /// Retrieves the list of all streamable stat values associated with a particular type.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private static StreamableStat GetStreamableStatForGuid(Guid guid)
        {
            if (!s_streamableStatLookup.Any())
            {
                CreateStreamableStatLookup();
            }
            return s_streamableStatLookup[guid];
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        private static void CreateStreamableStatLookup()
        {
            foreach (Type type in s_streamableStatTypes)
            {
                foreach (FieldInfo pInfo in type.GetFields(BindingFlags.Public | BindingFlags.Static))
                {
                    if (pInfo.FieldType == typeof(StreamableStat))
                    {
                        StreamableStat stat = (StreamableStat)pInfo.GetValue(null);
                        Debug.Assert(!s_streamableStatLookup.ContainsKey(stat.Guid), "Duplicate guids exist for streamable stats.  Have you recently added a new one but not assigned an unique Guid?");
                        s_streamableStatLookup.Add(stat.Guid, stat);
                    }
                }
            }
        }
    } // StreamableStatUtils
}
