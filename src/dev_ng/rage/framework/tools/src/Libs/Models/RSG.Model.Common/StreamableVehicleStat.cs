﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common
{
    /// <summary>
    /// Enumeration of all statistics that a vehicle can have associated with it
    /// </summary>
    [DependentStat(StreamableLevelStat.VehiclesString)]
    public static class StreamableVehicleStat
    {
        public const String FriendlyNameString = "3D89F3A4-EBD3-4F05-BA1E-99A75E137265";
        public const String GameNameString = "338C8451-7593-482B-A349-34228D0C7325";
        public const String CategoryString = "3324B877-694C-4D38-A5E1-851563127DE7";
        public const String ShadersString = "B353CC6D-CDB4-44BB-9ECF-C2377CA23BD8";
        public const String PolyCountString = "9DD6AC8A-E9FA-4B23-93D2-E1E659B695F2";
        public const String CollisionCountString = "B3A20F7E-CA6A-4B97-B51D-B2A8A50C8978";
        public const String BoneCountString = "592F7FD8-195D-40DF-930F-120330D04D43";
        public const String HasLod1String = "442AC013-8A78-4FC7-87F9-42259369CFD2";
        public const String HasLod2String = "54E8565B-F9AD-4105-B649-73F615EA8865";
        public const String HasLod3String = "31F606F1-C34D-43A6-8F45-12A3CC1BC5E3";
        public const String WheelCountString = "7634A80C-E852-45A9-923F-E1EF81675D61";
        public const String SeatCountString = "BAF2A812-A28E-4B10-92F0-E593E67BE006";
        public const String DoorCountString = "168ACD64-F48D-4AC7-B8EE-7F3E77DA337A";
        public const String LightCountString = "E1DB580A-9C50-469D-B0FE-4FC2E7C85F83";
        public const String ExtraCountString = "5ADE30E5-4398-4094-83DE-6DEBB0CAD144";
        public const String RudderCountString = "E4F08D29-E487-4754-9CB0-FD40E5962FB6";
        public const String RotorCountString = "B67462DD-A2EE-4745-9580-533BE260D52D";
        public const String PropellerCountString = "46CCD1D0-791A-405B-BD84-73B4FB69DDF7";
        public const String ElevatorCountString = "92C2AFD1-6D23-46A1-9FB8-684EC5BFAB80";
        public const String HandlebarCountString = "4AD67648-3A30-4D75-BD5C-F844592AC88A";
        public const String PlatformStatsString = "654BAB86-45CD-4445-AE0A-6E4FB3A15662";

        public static readonly StreamableStat FriendlyName = new StreamableStat(FriendlyNameString, true);
        public static readonly StreamableStat GameName = new StreamableStat(GameNameString, true);
        public static readonly StreamableStat Category = new StreamableStat(CategoryString, true);
        public static readonly StreamableStat Shaders = new StreamableStat(ShadersString, true, true);
        public static readonly StreamableStat PolyCount = new StreamableStat(PolyCountString, true);
        public static readonly StreamableStat CollisionCount = new StreamableStat(CollisionCountString, true);
        public static readonly StreamableStat BoneCount = new StreamableStat(BoneCountString, true);
        public static readonly StreamableStat HasLod1 = new StreamableStat(HasLod1String, true);
        public static readonly StreamableStat HasLod2 = new StreamableStat(HasLod2String, true);
        public static readonly StreamableStat HasLod3 = new StreamableStat(HasLod3String, true);
        public static readonly StreamableStat WheelCount = new StreamableStat(WheelCountString, true);
        public static readonly StreamableStat SeatCount = new StreamableStat(SeatCountString, true);
        public static readonly StreamableStat DoorCount = new StreamableStat(DoorCountString, true);
        public static readonly StreamableStat LightCount = new StreamableStat(LightCountString, true);
        public static readonly StreamableStat ExtraCount = new StreamableStat(ExtraCountString, true);
        public static readonly StreamableStat RudderCount = new StreamableStat(RudderCountString, true);
        public static readonly StreamableStat RotorCount = new StreamableStat(RotorCountString, true);
        public static readonly StreamableStat PropellerCount = new StreamableStat(PropellerCountString, true);
        public static readonly StreamableStat ElevatorCount = new StreamableStat(ElevatorCountString, true);
        public static readonly StreamableStat HandlebarCount = new StreamableStat(HandlebarCountString, true);
        public static readonly StreamableStat PlatformStats = new StreamableStat(PlatformStatsString, true, true);
    } // StreamableVehicleStat
}
