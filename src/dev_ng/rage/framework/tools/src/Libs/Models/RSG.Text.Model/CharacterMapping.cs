﻿//---------------------------------------------------------------------------------------------
// <copyright file="CharacterMapping.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.Model
{
    using System;
    using System.Xml;
    using RSG.Editor.Model;

    /// <summary>
    /// Represents a mapping between a character and a voice name.
    /// </summary>
    public class CharacterMapping : ModelBase
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="CharacterId"/> property.
        /// </summary>
        private Guid _characterId;

        /// <summary>
        /// The private field used for the <see cref="SpeakerId"/> property.
        /// </summary>
        private int _speakerId;

        /// <summary>
        /// The private field used for the <see cref="VoiceName"/> property.
        /// </summary>
        private string _voiceName;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CharacterMapping"/> class.
        /// </summary>
        /// <param name="mappings">
        /// The mappings object this mapping belongs to.
        /// </param>
        public CharacterMapping(CharacterMappings mappings)
            : base(mappings)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="CharacterMapping"/> class.
        /// </summary>
        /// <param name="mappings">
        /// The mappings object this mapping belongs to.
        /// </param>
        /// <param name="reader">
        /// The xml reader that contains the data used to initialise this instance.
        /// </param>
        public CharacterMapping(CharacterMappings mappings, XmlReader reader)
            : base(mappings)
        {
            this.Deserialise(reader);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public Guid CharacterId
        {
            get { return this._characterId; }
            set { this.SetProperty(ref this._characterId, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public int SpeakerId
        {
            get { return this._speakerId; }
            set { this.SetProperty(ref this._speakerId, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public string VoiceName
        {
            get { return this._voiceName; }
            set { this.SetProperty(ref this._voiceName, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Initialises this instance using the data contained within the specified xml reader.
        /// </summary>
        /// <param name="reader">
        /// The xml reader that contains the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() == XmlNodeType.Element)
            {
                if (String.Equals("CharacterId", reader.Name))
                {
                    string input = reader.ReadElementContentAsString();
                    if (!Guid.TryParseExact(input, "D", out this._characterId))
                    {
                        this._characterId = Guid.Empty;
                    }
                }
                else if (String.Equals("SpeakerId", reader.Name))
                {
                    string value = reader.GetAttribute("value");
                    if (!int.TryParse(value, out this._speakerId))
                    {
                        this._speakerId = 9;
                    }
                }
                else if (String.Equals("VoiceName", reader.Name))
                {
                    this._voiceName = reader.ReadElementContentAsString();
                }

                reader.Skip();
            }

            if (reader.NodeType == XmlNodeType.EndElement)
            {
                reader.ReadEndElement();
            }
        }
        #endregion Methods
    } // RSG.Text.Model.CharacterMapping {Class}
} // RSG.Text.Model {Namespace}
