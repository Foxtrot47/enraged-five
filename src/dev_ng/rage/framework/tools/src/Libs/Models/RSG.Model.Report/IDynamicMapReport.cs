﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.ConfigParser;
using RSG.Model.Common;

namespace RSG.Model.Report
{
    /// <summary>
    /// A report that generates it's content on the fly based on level data
    /// </summary>
    public interface IDynamicMapReport : IReport
    {
        /// <summary>
        /// Determines whether the map data should be loaded
        /// 
        /// </summary>
        bool LoadMapData
        {
            get;
        }

        /// <summary>
        /// Determines whether the car generators should be loaded
        /// 
        /// </summary>
        bool LoadCarGenarators
        {
            get;
        }

        /// <summary>
        /// Generate the report dynamically.
        /// </summary>
        /// <param name="level">Level to use while generating the report</param>
        /// <param name="gv">Reference to the game config</param>
        /// <param name="platforms">List of all the platforms</param>
        void Generate(ILevel level, ConfigGameView gv, IPlatformCollection platforms);
    } // IDynamicMapReport
} // RSG.Model.Report
