﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using RSG.Base.Tasks;
using System.Web.UI;
using RSG.Model.Common.Map;
using System.Xml;
using RSG.Model.Common;
using RSG.Model.GlobalTXD;
using System.Diagnostics;
using RSG.Base.ConfigParser;
using RSG.Platform;
using RSG.ManagedRage;
using RSG.SceneXml;

namespace RSG.Model.Report.Reports.Map
{
    /// <summary>
    /// Map archetype report. 
    /// Create report for verifying map archetypes
    /// - Drawables, fragments that do not have an archetype
    /// - Archetypes that don't have a drawable/fragment
    /// - Texture dictionaries missing or redundant (see also Parent TXD XML)
    /// </summary>
    public class MapArchetypeUsageReport : CSVReport, IReportFileProvider, IDynamicLevelReport
    {
        #region Fields
        /// The name of this report.
        /// </summary>
        private const string ReportName = "Map Asset Usage";

        /// <summary>
        /// The description for this report.
        /// </summary>
        private const string ReportDescription = "Provides lists detailing the map asset usage in the game.";

        /// <summary>
        /// The private field used for the <see cref="GenerationTask"/> property.
        /// </summary>
        private ITask _generationTask;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MapArchetypeUsageReport"/> class.
        /// </summary>
        public MapArchetypeUsageReport()
        {
            this.Name = ReportName;
            this.Description = ReportDescription;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the task to run for generating this report.
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (this._generationTask == null)
                {
                    CompositeTask task = new CompositeTask("Generate Report");
                    task.AddSubTask(new ActionTask("Loading data", this.EnsureDataLoaded));
                    task.AddSubTask(new ActionTask("Generating report", this.GenerateReport));
                    this._generationTask = task;
                }

                return this._generationTask;
            }
        }

        public IEnumerable<StreamableStat> RequiredStats
        {
            get { return Enumerable.Empty<StreamableStat>(); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Ensures that the data needed for this report is loaded.
        /// </summary>
        /// <param name="context">
        /// The task context for this action.
        /// </param>
        /// <param name="progress">
        /// The object used to notify a listener of the progress.
        /// </param>
        private void EnsureDataLoaded(ITaskContext context, IProgress<TaskProgress> progress)
        {
            DynamicLevelReportContext reportContext = context as DynamicLevelReportContext;
            Debug.Assert(reportContext != null, "Unable to load date for report");
            if (reportContext == null)
            {
                return;
            }

            IMapHierarchy hierarchy = reportContext.Level.MapHierarchy;
            Debug.Assert(hierarchy != null, "Unable to load date for report");
            if (hierarchy == null || hierarchy.AllSections == null)
            {
                return;
            }

            List<IMapSection> mapSections = hierarchy.AllSections.ToList();
            float increment = 1.0f / mapSections.Count;
            string total = mapSections.Count.ToString();
            string fmt = "Loading {0} {1} of " + total;
            int index = 0;
            foreach (IMapSection section in mapSections)
            {
                context.Token.ThrowIfCancellationRequested();
                string message = String.Format(fmt, section.Name, index.ToString());
                Debug.WriteLine(message);
                progress.Report(new TaskProgress(increment, true, message));

                section.LoadStats(new List<StreamableStat>() { StreamableMapSectionStat.Archetypes, StreamableMapSectionStat.Entities });
                index++;
            }
        }

        /// <summary>
        /// Generates the report and saves the file associated with it.
        /// </summary>
        /// <param name="context">
        /// The task context for this action.
        /// </param>
        /// <param name="progress">
        /// The object used to notify a listener of the progress.
        /// </param>
        private void GenerateReport(ITaskContext context, IProgress<TaskProgress> progress)
        {
            DynamicLevelReportContext reportContext = context as DynamicLevelReportContext;
            Debug.Assert(reportContext != null, "Unable to generate report");
            if (reportContext == null)
            {
                return;
            }

            IMapHierarchy hierarchy = reportContext.Level.MapHierarchy;
            Debug.Assert(hierarchy != null, "Unable to generate report");
            if (hierarchy == null || hierarchy.AllSections == null)
            {
                return;
            }

            List<IMapArchetype> validArchetypes = new List<IMapArchetype>();

            try
            {
                List<IMapSection> mapSections = hierarchy.AllSections.ToList();
                float increment = 1.0f / mapSections.Count;
                string fmt = "Processing {0}";
                foreach (IMapSection section in mapSections)
                {
                    context.Token.ThrowIfCancellationRequested();
                    string message = String.Format(fmt, section.Name);
                    progress.Report(new TaskProgress(increment, true, message));
                    Debug.WriteLine(message);
                    if (section == null || section.Archetypes == null)
                    {
                        continue;
                    }

                    foreach (IMapArchetype archetype in section.Archetypes)
                    {
                        if (archetype == null)
                        {
                            continue;
                        }

                        if (archetype.Entities == null || archetype.Entities.Count > 0)
                        {
                            continue;
                        }

                        validArchetypes.Add(archetype);
                        if (archetype is IStatedAnimArchetype)
                        {
                            foreach (IMapArchetype child in (archetype as IStatedAnimArchetype).ComponentArchetypes)
                            {
                                validArchetypes.Add(child);
                            }
                        }
                    }
                }
            }
            catch
            {
                Trace.Write("fdsfadsafd");
            }

            this.WriteCsvFile(validArchetypes, reportContext.GameView, reportContext.Level.Name);
        }

        /// <summary>
        /// Wtites out the CSV file using the data gathered at the start of the
        /// <see cref="GenerateReport"/> method.
        /// </summary>
        /// <param name="validEntities">
        /// The list containing the valid entities to output to the csv file.
        /// </param>
        private void WriteCsvFile(List<IMapArchetype> validArchetypes, ConfigGameView gv, string levelName)
        {
            using (StreamWriter writer = new StreamWriter(this.Filename))
            {
                writer.WriteLine(
                    "Archetype Name," +
                    "Section," +
                    "Parent Area," +
                    "Grandparent Area," +
                    "Dont Add To IPL," +
                    "Permanent");

                WriteCsvData(writer, validArchetypes, gv, levelName);
            }
        }

        /// <summary>
        /// Writes the data out to the specified stream.
        /// </summary>
        /// <param name="writer">
        /// The stream to write the comma separated data into.
        /// </param>
        /// <param name="validEntities">
        /// The list containing the valid entities to output to the csv file.
        /// </param>
        private void WriteCsvData(StreamWriter writer, List<IMapArchetype> validArchetypes, ConfigGameView gv, string levelName)
        {
            List<IMapArchetype> archetypes = new List<IMapArchetype>();
            foreach (IMapArchetype archetype in validArchetypes)
            {
                if (archetype.Name == "Prop_Plant_Cane_02aSHAD")
                {

                }

                if (archetype == null)
                {
                    continue;
                }

                if (archetype.SceneLinks != null && archetype.SceneLinks.ContainsKey("ShadowMesh"))
                {
                    continue;
                }

                archetypes.Add(archetype);
            }

            foreach (IMapArchetype archetype in archetypes)
            {
                IMapSection section = archetype.ParentSection;
                if (section == null)
                {
                    IRoom room = archetype.ParentSection as IRoom;
                    if (room != null)
                    {
                        IInteriorArchetype interiorArchetype = room.ParentArchetype;
                        if (room.ParentArchetype != null)
                        {
                            section = room.ParentArchetype.ParentSection;
                        }
                    }
                }

                IMapArea parentArea = (section != null ? section.ParentArea : null);
                IMapArea grandParentArea = (parentArea != null ? parentArea.ParentArea : null);

                string csvRecord = String.Format("{0},{1},{2},{3},", archetype.Name, section != null ? section.Name : "n/a", parentArea != null ? parentArea.Name : "n/a", grandParentArea != null ? grandParentArea.Name : "n/a");
                csvRecord += String.Format("{0},{1},", archetype.DontAddToIpl, archetype.IsPermanentArchetype);

                writer.WriteLine(csvRecord);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private string GetProcesedRpfPath(ConfigGameView gv, string name)
        {
            // Get the content node for this section
            ContentNodeMap contentNode = (ContentNodeMap)gv.Content.Root.FindAll(name, "map").FirstOrDefault();

            if (contentNode == null)
            {
                return null;
            }

            // Get the export node for the section
            ContentNodeMapZip exportNode = contentNode.Outputs.FirstOrDefault() as ContentNodeMapZip;
            if (exportNode == null)
            {
                return null;
            }

            ContentNodeMapProcessedZip processedZip = exportNode.Outputs.FirstOrDefault() as ContentNodeMapProcessedZip;
            if (processedZip == null)
            {
                return null;
            }

            string path = processedZip.Filename;
            if (path == null)
            {
                return null;
            }

            path = Path.ChangeExtension(path, ".rpf");
            path = Path.GetFullPath(path);
            return path.Replace(gv.ProcessedDir, Path.Combine(gv.BuildDir, "ps3"));
        }
        #endregion Methods

        #region Structures
        private struct ArchetypeSizes
        {
            public ulong VirtualGeometry { get; set; }

            public ulong VirtualTexture { get; set; }

            public ulong PhysicalGeometry { get; set; }

            public ulong PhysicalTexture { get; set; }
        }
        #endregion Structures
    }
}
