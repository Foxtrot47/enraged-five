﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.SceneXml;

namespace RSG.Model.Map.SceneOverride
{
    /// <summary>
    /// The main public interface for the Scene Override subsystem
    /// </summary>
    public interface ISceneOverrideManager
    {
        /// <summary>
        /// Submit a collection of records (add or update)
        /// </summary>
        /// <param name="overrides">The lod overrides to submit</param>
        /// /// <param name="overrides">The delete items to submit</param>
        void Submit(IEnumerable<RuntimeLODOverride> lodOverrides, 
                    IEnumerable<RuntimeEntityDeleteItem> deleteItems,
                    IEnumerable<RuntimeAttributeOverride> attributeOverrides);

        /// Interface methods for scene xml based queries
        bool HasLODDistanceOverrideForObject(TargetObjectDef objectDef);
        bool HasChildLODDistanceOverrideForObject(TargetObjectDef objectDef);
        bool IsEntityMarkedForDeletion(TargetObjectDef objectDef);
        bool HasAttributeOverridesForObject(TargetObjectDef objectDef);

        float GetLODDistanceOverrideForObject(TargetObjectDef objectDef);
        float GetChildLODDistanceOverrideForObject(TargetObjectDef objectDef);
        bool GetDontCastShadowsAttributeValue(TargetObjectDef objectDef);
        bool GetDontRenderInShadowsAttributeValue(TargetObjectDef objectDef);
        bool GetDontRenderInReflectionsAttributeValue(TargetObjectDef objectDef);
        bool GetOnlyRenderInReflectionsAttributeValue(TargetObjectDef objectDef);
        bool GetDontRenderInWaterReflectionsAttributeValue(TargetObjectDef objectDef);
        bool GetOnlyRenderInWaterReflectionsAttributeValue(TargetObjectDef objectDef);
        bool GetDontRenderInMirrorReflectionsAttributeValue(TargetObjectDef objectDef);
        bool GetOnlyRenderInMirrorReflectionsAttributeValue(TargetObjectDef objectDef);
        bool GetStreamingPriorityLowAttributeValue(TargetObjectDef objectDef);
        int GetPriorityAttributeValue(TargetObjectDef objectDef);

        /// Interface methods for DCC queries
        bool HasLODDistanceOverrideForObject(DCCEntityKey entityKey);
        bool HasChildLODDistanceOverrideForObject(DCCEntityKey entityKey);
        bool IsEntityMarkedForDeletion(DCCEntityKey entityKey);
        bool HasAttributeOverridesForObject(DCCEntityKey entityKey);

        float GetLODDistanceOverrideForObject(DCCEntityKey entityKey);
        float GetChildLODDistanceOverrideForObject(DCCEntityKey entityKey);
        bool GetDontCastShadowsAttributeValue(DCCEntityKey entityKey);
        bool GetDontRenderInShadowsAttributeValue(DCCEntityKey entityKey);
        bool GetDontRenderInReflectionsAttributeValue(DCCEntityKey entityKey);
        bool GetOnlyRenderInReflectionsAttributeValue(DCCEntityKey entityKey);
        bool GetDontRenderInWaterReflectionsAttributeValue(DCCEntityKey entityKey);
        bool GetOnlyRenderInWaterReflectionsAttributeValue(DCCEntityKey entityKey);
        bool GetDontRenderInMirrorReflectionsAttributeValue(DCCEntityKey entityKey);
        bool GetOnlyRenderInMirrorReflectionsAttributeValue(DCCEntityKey entityKey);
        bool GetStreamingPriorityLowAttributeValue(DCCEntityKey entityKey);
        int GetPriorityAttributeValue(DCCEntityKey entityKey);
    }
}
