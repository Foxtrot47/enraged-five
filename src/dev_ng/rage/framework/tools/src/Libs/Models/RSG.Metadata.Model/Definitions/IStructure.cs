﻿// --------------------------------------------------------------------------------------------
// <copyright file="IStructure.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions
{
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using RSG.Base.Logging;
    using RSG.Editor.Model;
    using RSG.Metadata.Model.Definitions.Members;

    /// <summary>
    /// When implemented defines a parsable structure that the parCodeGen system can use.
    /// </summary>
    public interface IStructure : IDefinition, IEquatable<IStructure>
    {
        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether this structure is automatically registered.
        /// </summary>
        bool AutoRegister { get; set; }

        /// <summary>
        /// Gets the resolved <see cref="IStructure"/> used as the base type.
        /// </summary>
        IStructure BaseStructure { get; }

        /// <summary>
        /// Gets or sets the name of the structure that is used as the base type.
        /// </summary>
        string BaseStructureName { get; set; }

        /// <summary>
        /// Gets or sets the category that this structure is in. This is used as a filter
        /// mechanic when showing all of the structures to the user.
        /// </summary>
        string Category { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this structure can be constructed.
        /// </summary>
        bool Constructible { get; set; }

        /// <summary>
        /// Gets or sets the filter that this structure uses. This is used to hint to a open
        /// service the primary file extensions that this structure uses.
        /// </summary>
        string Filter { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this structure needs to be posted in full
        /// when one of its value changes during live editing.
        /// </summary>
        bool FullPost { get; set; }

        /// <summary>
        /// Gets an iterator over the structures that use this structure as their base type.
        /// </summary>
        IEnumerable<IStructure> ImmediateDescendants { get; }

        /// <summary>
        /// Gets the number of members that this definition contains in it that can
        /// be instanced.
        /// </summary>
        int InstancedCount { get; }

        /// <summary>
        /// Gets a value indicating whether this structure has been marked as a file root
        /// structure.
        /// </summary>
        bool IsRootStructure { get; }

        /// <summary>
        /// Gets or sets the value used to provide hints about the memory layout.
        /// </summary>
        string LayoutHint { get; set; }

        /// <summary>
        /// Gets a collection of the members that are defined for this instance.
        /// </summary>
        ReadOnlyModelCollection<IMember> Members { get; }

        /// <summary>
        /// Gets the name for this definition as it will appear in a metadata file.
        /// </summary>
        string MetadataName { get; }

        /// <summary>
        /// Gets or sets the name that this structure will be referenced by.
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Gets or sets the name of the function that gets called after adding widgets.
        /// </summary>
        string OnPostAddWidgets { get; set; }

        /// <summary>
        /// Gets or sets the name of the function that gets called after loading.
        /// </summary>
        string OnPostLoad { get; set; }

        /// <summary>
        /// Gets or sets the name of the function that gets called after saving.
        /// </summary>
        string OnPostSave { get; set; }

        /// <summary>
        /// Gets or sets the name of the function that gets called after any member of the
        /// structure is set.
        /// </summary>
        string OnPostSet { get; set; }

        /// <summary>
        /// Gets or sets the name of the function that gets called before adding widgets.
        /// </summary>
        string OnPreAddWidgets { get; set; }

        /// <summary>
        /// Gets or sets the name of the function that gets called before loading.
        /// </summary>
        string OnPreLoad { get; set; }

        /// <summary>
        /// Gets or sets the name of the function that gets called before saving.
        /// </summary>
        string OnPreSave { get; set; }

        /// <summary>
        /// Gets or sets the name of the function that gets called before any member of the
        /// structure is set.
        /// </summary>
        string OnPreSet { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the names for this structure will be
        /// stripped from __FINAL builds.
        /// </summary>
        bool PreserveNames { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this structure represents a simple class. A
        /// simple class doesn't use virtuals and won't be used polymorphically.
        /// </summary>
        bool Simple { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this structure represents a template.
        /// </summary>
        bool Template { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the data inheritance feature is enabled.
        /// </summary>
        bool UsesDataInheritance { get; set; }

        /// <summary>
        /// Gets or sets the version for this instance.
        /// </summary>
        string Version { get; set; }

        /// <summary>
        /// Gets the member at the specified index.
        /// </summary>
        /// <param name="index">
        /// The zero-based index of the member to get.
        /// </param>
        /// <returns>
        /// The member at the specified index or null if the member doesn't exist.
        /// </returns>
        IMember this[int index] { get; }

        /// <summary>
        /// Gets the member with the specified name.
        /// </summary>
        /// <param name="memberName">
        /// The name of the member to get.
        /// </param>
        /// <returns>
        /// The member with the specified name or null if the member doesn't exist.
        /// </returns>
        IMember this[string memberName] { get; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new <see cref="IStructure"/> that is a copy of this instance.
        /// </summary>
        /// <returns>
        /// A new <see cref="IStructure"/> that is a copy of this instance.
        /// </returns>
        new IStructure Clone();

        /// <summary>
        /// Gets a read-only list containing all of the members for this structure that can be
        /// instanced.
        /// </summary>
        /// <returns>
        /// A read-only list containing all of the members for this structure.
        /// </returns>
        IReadOnlyList<IMember> GetAllInstancedMembers();

        /// <summary>
        /// Attempts to get the member with the specified name that can also be instanced and
        /// returns a a index to the found member in amongst only the members that can
        /// be instanced.
        /// </summary>
        /// <param name="name">
        /// The name of the member to get.
        /// </param>
        /// <param name="member">
        /// When this method returns, contains the member with the specified name, if the
        /// member is found; otherwise, null.
        /// </param>
        /// <returns>
        /// The index to the instanced member if this structure contains a member with the
        /// specified name that can be instanced; otherwise, -1.
        /// </returns>
        int GetInstancedMember(string name, out IMember member);

        /// <summary>
        /// Gets a lookup table of all of the members that can be instanced in this structure,
        /// including the base structures with their position index.
        /// </summary>
        /// <returns>
        /// A lookup table of all of the members that can be instanced in this structure.
        /// </returns>
        MemberInfo GetMemberInformation();

        /// <summary>
        /// Determines whether this structure and the specified structure are related to each
        /// other based on the inheritance tree.
        /// </summary>
        /// <param name="structure">
        /// The structure to test against this structure.
        /// </param>
        /// <returns>
        /// True if the two structures are related; otherwise, false.
        /// </returns>
        bool IsRelatedTo(IStructure structure);

        /// <summary>
        /// Determines whether this structure can be instanced inside a metadata file depending
        /// on any critical errors found.
        /// </summary>
        /// <param name="log">
        /// The log object that will receives any errors or warning found while validating.
        /// </param>
        /// <returns>
        /// True if this structure can be instanced inside a metadata file; otherwise false. If
        /// false the specified log object will have received the reasons why not.
        /// </returns>
        bool IsValidForInstancing(ILog log);

        /// <summary>
        /// Writes the xml representation of this instance onto the specified
        /// System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that the xml representation of this instance should be
        /// written to.
        /// </param>
        /// <param name="saveFileInfo">
        /// If true the file, line number and line position will be written out as attributes.
        /// </param>
        void Serialise(XmlWriter writer, bool saveFileInfo);
        #endregion Methods
    } // RSG.Metadata.Model.Definitions.IStructure {Interface}
} // RSG.Metadata.Model.Definitions {Namespace}
