﻿using System;

namespace RSG.Model.Common.Map
{
    /// <summary>
    /// Interface for creating Map Hierarchies
    /// </summary>
    public interface IMapFactory : IDisposable
    {
        #region Properties
        /// <summary>
        /// The streaming coordinator
        /// </summary>
        IStatStreamingCoordinator StreamingCoordinator { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Creates a new map hierarchy from the specified source and for a particular level.
        /// </summary>
        /// <param name="source">Source to use for creating the collection.</param>
        /// <param name="level">Level to create the hierarchy for.</param>
        /// <param name="buildIdentifier">Optional build identifier for when the source is Database.</param>
        /// <returns></returns>
        IMapHierarchy CreateHierarchy(DataSource source, ILevel level, string buildIdentifier = null);
        #endregion // Methods
    } // IMapFactory
}
