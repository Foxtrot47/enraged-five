﻿namespace RSG.Model.Report2.Reports.Map
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using RSG.Base.ConfigParser;
    using RSG.Base.Tasks;
    using RSG.Model.Common;
    using RSG.Model.Common.Map;
    using RSG.Model.Report;
    using RSG.ManagedRage;
    using RSG.Platform;

    /// <summary>
    /// A report that lists the objects with drawable lods with their memory footprint and
    /// draw distances (url:bugstar:1346056).
    /// </summary>
    public class OrphanedShadowProxiesReport : CSVReport, IDynamicLevelReport
    {
        #region Fields
        /// <summary>
        /// The name of this report.
        /// </summary>
        private const string Name = "Orphaned Reflection & Shadow Proxies";

        /// <summary>
        /// The description for this report.
        /// </summary>
        private const string Description =
            "Exports a csv report that lists the orphaned objects that are setup as shadow " +
            "or reflection proxies";

        /// <summary>
        /// The private field used for the <see cref="GenerationTask"/> property.
        /// </summary>
        private ITask _generationTask;
        #endregion Fields
        
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="OrphanedShadowProxiesReport"/> class.
        /// </summary>
        public OrphanedShadowProxiesReport() : base(Name, Description)
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the task that represents the work needing to be done to generate this report.
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (this._generationTask == null)
                {
                    CompositeTask task = new CompositeTask("Generate Report");
                    task.AddSubTask(new ActionTask("Loading data", this.EnsureDataLoaded));
                    task.AddSubTask(new ActionTask("Generating report", this.GenerateReport));
                    this._generationTask = task;
                }

                return this._generationTask;
            }
        }

        /// <summary>
        /// Gets a iterator around the stats that are required to generate this report. Always
        /// returns null.
        /// </summary>
        public IEnumerable<StreamableStat> RequiredStats
        {
            get { return null; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Ensures that the data needed for this report is loaded.
        /// </summary>
        /// <param name="context">
        /// The task context for this action.
        /// </param>
        /// <param name="progress">
        /// The object used to notify a listener of the progress.
        /// </param>
        private void EnsureDataLoaded(ITaskContext context, IProgress<TaskProgress> progress)
        {
            DynamicLevelReportContext reportContext = context as DynamicLevelReportContext;
            Debug.Assert(reportContext != null, "Unable to load date for report");
            if (reportContext == null)
            {
                return;
            }

            IMapHierarchy hierarchy = reportContext.Level.MapHierarchy;
            Debug.Assert(hierarchy != null, "Unable to load date for report");
            if (hierarchy == null || hierarchy.AllSections == null)
            {
                return;
            }

            List<IMapSection> mapSections = hierarchy.AllSections.ToList();
            float increment = 1.0f / mapSections.Count;
            string fmt = "Loading {0}";
            foreach (IMapSection section in mapSections)
            {
                context.Token.ThrowIfCancellationRequested();
                string message = String.Format(fmt, section.Name);
                progress.Report(new TaskProgress(increment, true, message));

                section.LoadStats(new List<StreamableStat>() { StreamableMapSectionStat.Archetypes, StreamableMapSectionStat.Entities });
            }
        }

        /// <summary>
        /// Generates the report and saves the file associated with it.
        /// </summary>
        /// <param name="context">
        /// The task context for this action.
        /// </param>
        /// <param name="progress">
        /// The object used to notify a listener of the progress.
        /// </param>
        private void GenerateReport(ITaskContext context, IProgress<TaskProgress> progress)
        {
            DynamicLevelReportContext reportContext = context as DynamicLevelReportContext;
            Debug.Assert(reportContext != null, "Unable to generate report");
            if (reportContext == null)
            {
                return;
            }

            IMapHierarchy hierarchy = reportContext.Level.MapHierarchy;
            Debug.Assert(hierarchy != null, "Unable to generate report");
            if (hierarchy == null || hierarchy.AllSections == null)
            {
                return;
            }

            Dictionary<IMapArchetype, List<IEntity>> validEntities = new Dictionary<IMapArchetype, List<IEntity>>();

            try
            {
                List<IMapSection> mapSections = hierarchy.AllSections.ToList();
                float increment = 1.0f / mapSections.Count;
                string fmt = "Processing {0}";
                foreach (IMapSection section in mapSections)
                {
                    context.Token.ThrowIfCancellationRequested();
                    string message = String.Format(fmt, section.Name);
                    progress.Report(new TaskProgress(increment, true, message));
                    if (section == null || section.Archetypes == null)
                    {
                        continue;
                    }

                    foreach (IMapArchetype archetype in section.Archetypes)
                    {
                        if (archetype == null)
                        {
                            continue;
                        }

                        if (archetype.Entities == null || archetype.HasShadowProxies == false)
                        {
                            continue;
                        }

                        foreach (IEntity entity in archetype.Entities)
                        {
                            if (entity == null)
                            {
                                continue;
                            }

                            List<IEntity> entities = null;
                            if (!validEntities.TryGetValue(archetype, out entities))
                            {
                                entities = new List<IEntity>();
                                validEntities.Add(archetype, entities);
                            }

                            entities.Add(entity);
                        }
                    }
                }
            }
            catch
            {
                Trace.Write("fdsfadsafd");
            }

            this.WriteCsvFile(validEntities, reportContext.GameView, reportContext.Level.Name);
        }

        /// <summary>
        /// Wtites out the CSV file using the data gathered at the start of the
        /// <see cref="GenerateReport"/> method.
        /// </summary>
        /// <param name="validEntities">
        /// The list containing the valid entities to output to the csv file.
        /// </param>
        private void WriteCsvFile(Dictionary<IMapArchetype, List<IEntity>> validEntities, ConfigGameView gv, string levelName)
        {
            using (StreamWriter writer = new StreamWriter(this.Filename))
            {
                writer.WriteLine(
                    "Entity Name," +
                    "Section," +
                    "Parent Area," +
                    "Grandparent Area," +
                    "X,Y,Z," +
                    "Archetype Name," +
                    "Virtual PS3 Geomerty Size," +
                    "Virtual PS3 txd Size," +
                    "Total Virtual PS3 Size," +
                    "Physical PS3 Geomerty Size," +
                    "Physical PS3 txd Size," +
                    "Total Physical PS3 Size," +
                    "Lod Distance," +
                    "Is Orphaned HD");

                WriteCsvData(writer, validEntities, gv, levelName);
            }
        }

        /// <summary>
        /// Writes the data out to the specified stream.
        /// </summary>
        /// <param name="writer">
        /// The stream to write the comma separated data into.
        /// </param>
        /// <param name="validEntities">
        /// The list containing the valid entities to output to the csv file.
        /// </param>
        private void WriteCsvData(StreamWriter writer, Dictionary<IMapArchetype, List<IEntity>> validEntities, ConfigGameView gv, string levelName)
        {
            Dictionary<IMapSection, List<IMapArchetype>> sections = new Dictionary<IMapSection, List<IMapArchetype>>();
            foreach (KeyValuePair<IMapArchetype, List<IEntity>> archetype in validEntities)
            {
                List<IMapArchetype> archetypes = null;
                if (!sections.TryGetValue(archetype.Key.ParentSection, out archetypes))
                {
                    archetypes = new List<IMapArchetype>();
                    sections.Add(archetype.Key.ParentSection, archetypes);
                }

                archetypes.Add(archetype.Key);
            }

            Dictionary<string, ArchetypeSizes> archetypeSizes = new Dictionary<string, ArchetypeSizes>();
            char ragebuilderCharacter = RSG.Platform.Platform.PS3.PlatformRagebuilderCharacter();
            string drawableExtension = FileType.Drawable.GetPlatformExtension().Replace('?', ragebuilderCharacter);
            string fragmentExtension = FileType.Fragment.GetPlatformExtension().Replace('?', ragebuilderCharacter);
            string textureExtension = FileType.TextureDictionary.GetPlatformExtension().Replace('?', ragebuilderCharacter);

            foreach (KeyValuePair<IMapSection, List<IMapArchetype>> section in sections)
            {
                string sectionName = section.Key.Name;
                string processedRpfPath = this.GetProcesedRpfPath(gv, sectionName);
                if (processedRpfPath != null && File.Exists(processedRpfPath))
                {
                    Packfile packFile = new Packfile();
                    packFile.Load(processedRpfPath);
                    foreach (IMapArchetype archetype in section.Value)
                    {
                        ArchetypeSizes sizes = new ArchetypeSizes();
                        string geometryName = archetype.Name + ".";
                        if (archetype is IFragmentArchetype)
                        {
                            geometryName += fragmentExtension;
                        }
                        else
                        {
                            geometryName += drawableExtension;
                        }

                        PackEntry geometryEntry = packFile.FindEntry(geometryName, true);
                        if (geometryEntry != null)
                        {
                            sizes.PhysicalGeometry = geometryEntry.PhysicalSize;
                            sizes.VirtualGeometry = geometryEntry.VirtualSize;
                        }

                        foreach (string txd in archetype.TxdExportSizes.Keys)
                        {
                            string txdName = txd + "." + textureExtension;
                            PackEntry textureEntry = packFile.FindEntry(txdName, true);
                            if (textureEntry != null)
                            {
                                sizes.PhysicalTexture += textureEntry.PhysicalSize;
                                sizes.VirtualTexture += textureEntry.VirtualSize;
                            }
                        }

                        archetypeSizes.Add(archetype.Name, sizes);
                    }
                }
            }

            foreach (KeyValuePair<IMapArchetype, List<IEntity>> archetype in validEntities)
            {
                if (archetype.Key == null || archetype.Value == null)
                {
                    continue;
                }

                ArchetypeSizes sizes;
                archetypeSizes.TryGetValue(archetype.Key.Name, out sizes);

                foreach (IEntity entity in archetype.Value)
                {
                    IMapSection section = entity.Parent as IMapSection;
                    if (section == null)
                    {
                        IRoom room = entity.Parent as IRoom;
                        if (room != null)
                        {
                            IInteriorArchetype interiorArchetype = room.ParentArchetype;
                            if (room.ParentArchetype != null)
                            {
                                section = room.ParentArchetype.ParentSection;
                            }
                        }
                    }

                    IMapArea parentArea = (section != null ? section.ParentArea : null);
                    IMapArea grandParentArea = (parentArea != null ? parentArea.ParentArea : null);

                    string csvRecord = String.Format("{0},{1},{2},{3},", entity.Name, section != null ? section.Name : "n/a", parentArea != null ? parentArea.Name : "n/a", grandParentArea != null ? grandParentArea.Name : "n/a");
                    csvRecord += String.Format("{0},{1},{2},", entity.Position.X, entity.Position.Y, entity.Position.Z);
                    csvRecord += String.Format("{0},", archetype.Key.Name);
                    csvRecord += String.Format("{0},{1},{2},", sizes.VirtualGeometry, sizes.VirtualTexture, sizes.VirtualGeometry + sizes.VirtualTexture);
                    csvRecord += String.Format("{0},{1},{2},", sizes.PhysicalGeometry, sizes.PhysicalTexture, sizes.PhysicalGeometry + sizes.PhysicalTexture);
                    csvRecord += String.Format("{0},{1}", entity.LodDistance, entity.LodLevel == LodLevel.OrphanHd ? "True" : "False");


                    writer.WriteLine(csvRecord);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private string GetProcesedRpfPath(ConfigGameView gv, string name)
        {
            // Get the content node for this section
            ContentNodeMap contentNode = (ContentNodeMap)gv.Content.Root.FindAll(name, "map").FirstOrDefault();

            if (contentNode == null)
            {
                return null;
            }

            // Get the export node for the section
            ContentNodeMapZip exportNode = contentNode.Outputs.FirstOrDefault() as ContentNodeMapZip;
            if (exportNode == null)
            {
                return null;
            }

            ContentNodeMapProcessedZip processedZip = exportNode.Outputs.FirstOrDefault() as ContentNodeMapProcessedZip;
            if (processedZip == null)
            {
                return null;
            }

            string path = processedZip.Filename;
            if (path == null)
            {
                return null;
            }

            path = Path.ChangeExtension(path, ".rpf");
            path = Path.GetFullPath(path);
            return path.Replace(gv.ProcessedDir, Path.Combine(gv.BuildDir, "ps3"));
        }
        #endregion Methods

        #region Structures
        private struct ArchetypeSizes
        {
            public ulong VirtualGeometry { get; set; }

            public ulong VirtualTexture { get; set; }

            public ulong PhysicalGeometry { get; set; }

            public ulong PhysicalTexture { get; set; }
        }
        #endregion Structures
    }
}
