﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.Base.Collections;
using RSG.Statistics.Common.Dto.GameAssets.Bundles;

namespace RSG.Model.Asset
{
    /// <summary>
    /// 
    /// </summary>
    public class Shader : FileAssetBase, IShader
    {
        #region Properties
        /// <summary>
        /// List of textures the shader uses
        /// </summary>
        public ObservableCollection<ITexture> Textures
        {
            get;
            protected set;
        }
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// Default Constructor
        /// </summary>
        public Shader()
            : base()
        {
        }

        /// <summary>
        /// Default Constructor
        /// </summary>
        public Shader(String name)
            : this(name, null)
        {
        }

        /// <summary>
        /// Default Constructor
        /// </summary>
        public Shader(String name, IEnumerable<ITexture> textures)
            : base(name, String.Empty)
        {
            Textures = new ObservableCollection<ITexture>();

            if (textures != null)
            {
                foreach (ITexture texture in textures)
                {
                    if (!Textures.Contains(texture))
                    {
                        Textures.Add(texture);
                    }
                }
            }
        }

        /// <summary>
        /// Internal constructor for creating a shader from the stats server
        /// </summary>
        /// <param name="shaderBundle"></param>
        public Shader(ShaderStatBundleDto shaderBundle)
            : base(shaderBundle.Shader.Name, String.Empty)
        {
            Textures = new ObservableCollection<ITexture>();

            foreach (TextureStatBundleDto textureBundle in shaderBundle.TextureStats)
            {
                Textures.Add(new Texture(textureBundle.Texture));
            }
        }
        #endregion // Constructors

        #region IEquatable<IShader> Implementation
        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>true if the current object is equal to the other parameter; otherwise, false.</returns>
        public bool Equals(IShader other)
        {
            if (other == null)
            {
                return false;
            }

            if (Hash == other.Hash)
            {
                // Get any items that are in one set of textures but not the other
                if (this.Textures.Except(other.Textures).Any() || other.Textures.Except(this.Textures).Any())
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }

            return false;
        }
        #endregion // IEquatable<IShader> Implementation

        #region IComparable<IShader> Implementation
        /// <summary>
        /// Compares the current object with another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns></returns>
        public int CompareTo(IShader other)
        {
            return Name.CompareTo(other.Name);
        }
        #endregion // IComparable<IShader> Implementation

        #region Object Overrides
        /// <summary>
        /// Compare
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(Object obj)
        {
            if (obj == null)
            {
                return base.Equals(obj);
            }

            return ((obj is IShader) && Equals(obj as IShader));
        }

        /// <summary>
        /// Return String representation of the Vehicle.
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return Name;
        }

        /// <summary>
        /// Return hash code of Vehicle (based on Name).
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }
        #endregion // Object Overrides
    }
}
