﻿//---------------------------------------------------------------------------------------------
// <copyright file="EnumTunable.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Tunables
{
    using System;
    using System.ComponentModel;
    using System.Xml;
    using System.Xml.Linq;
    using RSG.Base.Extensions;
    using RSG.Base.Logging;
    using RSG.Editor;
    using RSG.Metadata.Model.Definitions.Members;
    using RSG.Metadata.Model.Resources;

    /// <summary>
    /// Represents a tunable used by the parCodeGen system that is instancing a
    /// <see cref="EnumMember"/> object.
    /// </summary>
    public class EnumTunable : TunableBase
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Value"/> property.
        /// </summary>
        private string _value;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="EnumTunable"/> class to be as
        /// instance of the specified member.
        /// </summary>
        /// <param name="member">
        /// The member that this tunable will be instancing.
        /// </param>
        /// <param name="parent">
        /// The tunable parent this tunable will belong to.
        /// </param>
        public EnumTunable(EnumMember member, ITunableParent parent)
            : base(member, parent)
        {
            this._value = member.InitialValue != null ? member.InitialValue.Name : null;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="EnumTunable"/> class as a copy of the
        /// specified instance.
        /// </summary>
        /// <param name="other">
        /// The instance to copy.
        /// </param>
        /// <param name="parent">
        /// The tunable parent this tunable will belong to.
        /// </param>
        public EnumTunable(EnumTunable other, ITunableParent parent)
            : base(other, parent)
        {
            this._value = other._value;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="EnumTunable"/> class using the
        /// specified System.Xml.XmlReader as a initialising data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <param name="member">
        /// The member that this tunable will be instancing.
        /// </param>
        /// <param name="parent">
        /// The tunable parent this tunable will belong to.
        /// </param>
        /// <param name="log">
        /// A object that is used to log any warnings or errors produced during the parsing of
        /// the specified reader.
        /// </param>
        public EnumTunable(
            XmlReader reader, EnumMember member, ITunableParent parent, ILog log)
            : base(reader, member, parent)
        {
            this._value = member.InitialValue != null ? member.InitialValue.Name : null;
            this.Deserialise(reader, log);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the bool member that this tunable is instancing.
        /// </summary>
        public EnumMember EnumMember
        {
            get
            {
                EnumMember member = this.Member as EnumMember;
                if (member != null)
                {
                    return member;
                }

                return new EnumMember(this.TunableStructure.Definition);
            }
        }

        /// <summary>
        /// Gets or sets the value assigned to this enumeration tunable.
        /// </summary>
        public string Value
        {
            get { return this._value; }
            set { this.SetProperty(ref this._value, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a XML element node object that represents a enum tunable with the specified
        /// value.
        /// </summary>
        /// <param name="elemName">
        /// The XML element node name to give this element.
        /// </param>
        /// <param name="value">
        /// The value of the enum tunable whose XML element node is created.
        /// </param>
        /// <returns>
        /// A new XML Element node object that represents a enum tunable with the specified
        /// value.
        /// </returns>
        public static XElement CreateXElement(string elemName, string value)
        {
            return new XElement(elemName, value);
        }

        /// <summary>
        /// Creates a new <see cref="EnumTunable"/> that is a copy of this instance.
        /// </summary>
        /// <returns>
        /// A new <see cref="EnumTunable"/> that is a copy of this instance.
        /// </returns>
        public new EnumTunable Clone()
        {
            return new EnumTunable(this, this.TunableStructure);
        }

        /// <summary>
        /// Creates a deep copy of the current instance.
        /// </summary>
        /// <returns>
        /// A deep copy of the current System.Object.
        /// </returns>
        public override object DeepClone()
        {
            return this.Clone();
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="ITunable"/> by
        /// looking at their values.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance by their values;
        /// otherwise, false.
        /// </returns>
        public override bool EqualByValue(ITunable other)
        {
            return this.EqualByValue(other as EnumTunable);
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="ITunable"/> by
        /// looking at their values.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance by their values;
        /// otherwise, false.
        /// </returns>
        public bool EqualByValue(EnumTunable other)
        {
            if (other == null)
            {
                return false;
            }

            return this.Value == other.Value;
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified
        /// <see cref="EnumTunable"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public bool Equals(EnumTunable other)
        {
            if (other == null)
            {
                return false;
            }

            return Object.ReferenceEquals(other, this);
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="ITunable"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public override bool Equals(ITunable other)
        {
            return this.Equals(other as EnumTunable);
        }

        /// <summary>
        /// Resets this tunable to have its default value. (i.e no source data with its initial
        /// values).
        /// </summary>
        public override void ResetValueToDefaultCore()
        {
            EnumTunable source = this.InheritanceParent as EnumTunable;
            if (source != null)
            {
                this.Value = source.Value;
            }
            else
            {
                if (this.EnumMember.InitialValue != null)
                {
                    this.Value = this.EnumMember.InitialValue.Name;
                }
                else
                {
                    this.Value = null;
                }
            }
        }

        /// <summary>
        /// Writes the xml representation of this instance onto the specified
        /// System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that the xml representation of this instance should be
        /// written to.
        /// </param>
        /// <param name="serialiseDefaultTunables">
        /// A value indicating whether the tunables that have a default value should be written
        /// to the specified writer or not.
        /// </param>
        public override void Serialise(XmlWriter writer, bool serialiseDefaultTunables)
        {
            if (writer == null)
            {
                throw new SmartArgumentNullException(() => writer);
            }

            writer.WriteString(this.Value);
        }

        /// <summary>
        /// Called whenever the tunable whose value will be used when this tunable is being
        /// inherited changes.
        /// </summary>
        /// <param name="oldValue">
        /// The old inheritance parent.
        /// </param>
        /// <param name="newValue">
        /// The new inheritance parent.
        /// </param>
        protected override void InheritanceParentChanged(ITunable oldValue, ITunable newValue)
        {
            string name = "Value";
            if (oldValue != null)
            {
                PropertyChangedEventManager.RemoveHandler(
                    oldValue, this.OnInheritedValueChanged, name);
            }

            EnumTunable source = newValue as EnumTunable;
            if (source == null)
            {
                throw new NotSupportedException(
                    "Only the smae type can be an inheritance parent.");
            }

            PropertyChangedEventManager.AddHandler(source, this.OnInheritedValueChanged, name);
            if (this.HasDefaultValue)
            {
                this._value = source.Value;
                this.NotifyPropertyChanged("Value");
            }
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        /// <param name="log">
        /// A object that is used to log any warnings or errors produced during the parsing of
        /// the specified reader.
        /// </param>
        private void Deserialise(XmlReader reader, ILog log)
        {
            if (reader == null)
            {
                return;
            }

            IXmlLineInfo lineInfo = reader as IXmlLineInfo;
            int line = lineInfo.LineNumber;
            int pos = lineInfo.LinePosition;

            try
            {
                if (reader.HasAttributes)
                {
                    log.Warning(StringTable.EnumTunableDeserialiseAttributeError, line, pos);
                }

                if (!reader.IsEmptyElement)
                {
                    this._value = reader.ReadElementContentAsString();
                }
                else
                {
                    reader.ReadStartElement();
                }
            }
            catch (Exception ex)
            {
                string msg = StringTable.EnumTunableDeserialiseError;
                throw new MetadataException(msg, line, pos, ex.Message);
            }

            reader.Skip();
        }

        /// <summary>
        /// Called whenever the value of the inheritance parent changes so that the values can
        /// be kept in sync.
        /// </summary>
        /// <param name="sender">
        /// The inheritance parent.
        /// </param>
        /// <param name="e">
        /// The event data.
        /// </param>
        private void OnInheritedValueChanged(object sender, PropertyChangedEventArgs e)
        {
            if (!this.HasDefaultValue)
            {
                return;
            }

            EnumTunable source = this.InheritanceParent as EnumTunable;
            if (source != null)
            {
                this._value = source.Value;
                this.NotifyPropertyChanged("Value");
            }
        }
        #endregion Methods
    } // RSG.Metadata.Model.Tunables.EnumTunable {Class}
} // RSG.Metadata.Model.Tunables {Namespace}
