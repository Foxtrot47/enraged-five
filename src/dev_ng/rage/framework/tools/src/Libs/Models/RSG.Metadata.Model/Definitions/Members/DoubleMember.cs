﻿// --------------------------------------------------------------------------------------------
// <copyright file="DoubleMember.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions.Members
{
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using RSG.Base;
    using RSG.Base.Extensions;
    using RSG.Base.Logging;
    using RSG.Editor;
    using RSG.Editor.Model;
    using RSG.Metadata.Model.Resources;
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// Represents the &lt;double&gt; member node in the parCodeGen system that can be
    /// instanced in a metadata file.
    /// </summary>
    public class DoubleMember :
        MemberBase,
        IEquatable<DoubleMember>,
        IHasInitialValue<double>,
        IHasRange<double>,
        IHasStep<double>,
        IHasPrecision,
        IHasDecimalPlace,
        IScalarMember
    {
        #region Fields
        /// <summary>
        /// The string representation of the <see cref="FloatType.Angle"/> constant.
        /// </summary>
        private const string FloatTypeAngle = "angle";

        /// <summary>
        /// The name of the xml attribute containing the data for the
        /// <see cref="DecimalPlaces"/> property.
        /// </summary>
        private const string XmlDecimalPlacesAttr = "ui_decimalPlaces";

        /// <summary>
        /// The name of the xml attribute containing the data for the
        /// <see cref="HighPrecision"/> property.
        /// </summary>
        private const string XmlHighPrecisionAttr = "highPrecision";

        /// <summary>
        /// The name of the xml attribute containing the data for the
        /// <see cref="InitialValue"/> property.
        /// </summary>
        private const string XmlInitialAttr = "init";

        /// <summary>
        /// The name of the xml attribute containing the data for the <see cref="Maximum"/>
        /// property.
        /// </summary>
        private const string XmlMaxAttr = "max";

        /// <summary>
        /// The name of the xml attribute containing the data for the <see cref="Minimum"/>
        /// property.
        /// </summary>
        private const string XmlMinAttr = "min";

        /// <summary>
        /// The name of the xml attribute containing the data for the <see cref="Step"/>
        /// property.
        /// </summary>
        private const string XmlStepAttr = "step";

        /// <summary>
        /// The name of the xml attribute containing the data for the <see cref="Type"/>
        /// property.
        /// </summary>
        private const string XmlTypeAttr = "type";

        /// <summary>
        /// The private field used for the <see cref="DecimalPlaces"/> property.
        /// </summary>
        private string _decimalPlaces;

        /// <summary>
        /// The private field used for the <see cref="HighPrecision"/> property.
        /// </summary>
        private string _highPrecision;

        /// <summary>
        /// The private field used for the <see cref="InitialValue"/> property.
        /// </summary>
        private string _initialValue;

        /// <summary>
        /// The private field used for the <see cref="Maximum"/> property.
        /// </summary>
        private string _maximum;

        /// <summary>
        /// The private field used for the <see cref="Minimum"/> property.
        /// </summary>
        private string _minimum;

        /// <summary>
        /// The private field used for the <see cref="Step"/> property.
        /// </summary>
        private string _step;

        /// <summary>
        /// The private field used for the <see cref="Type"/> property.
        /// </summary>
        private string _type;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DoubleMember"/> class to be one of the
        /// members of the specified structure.
        /// </summary>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        public DoubleMember(IStructure structure)
            : base(structure)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="DoubleMember"/> class as a copy of the
        /// specified instance.
        /// </summary>
        /// <param name="other">
        /// The instance to copy.
        /// </param>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        public DoubleMember(DoubleMember other, IStructure structure)
            : base(other, structure)
        {
            this._initialValue = other._initialValue;
            this._minimum = other._minimum;
            this._maximum = other._maximum;
            this._step = other._step;
            this._decimalPlaces = other._decimalPlaces;
            this._highPrecision = other._highPrecision;
            this._type = other._type;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="DoubleMember"/> class.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        public DoubleMember(XmlReader reader, IStructure structure)
            : base(reader, structure)
        {
            this.Deserialise(reader);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the decimal place count the value of an instance to this member has.
        /// </summary>
        public short DecimalPlaces
        {
            get { return this.Dictionary.To<short>(this._decimalPlaces, -1); }
            set { this.SetProperty(ref this._decimalPlaces, value.ToString()); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this member has high precision on or off.
        /// </summary>
        public bool HighPrecision
        {
            get { return this.Dictionary.To<bool>(this._highPrecision, false); }
            set { this.SetProperty(ref this._highPrecision, value.ToString()); }
        }

        /// <summary>
        /// Gets or sets the initial value that any instance of this member is set to.
        /// </summary>
        public double InitialValue
        {
            get { return this.Dictionary.To(this._initialValue, 0.0d); }
            set { this.SetProperty(ref this._initialValue, value.ToString()); }
        }

        /// <summary>
        /// Gets or sets the maximum value that a instance of this member can be set to.
        /// </summary>
        public double Maximum
        {
            get { return this.Dictionary.To(this._maximum, double.MaxValue * 0.001d); }
            set { this.SetProperty(ref this._maximum, value.ToString()); }
        }

        /// <summary>
        /// Gets or sets the minimum value that a instance of this member can be set to.
        /// </summary>
        public double Minimum
        {
            get { return this.Dictionary.To(this._minimum, double.MaxValue * -0.001d); }
            set { this.SetProperty(ref this._minimum, value.ToString()); }
        }

        /// <summary>
        /// Gets the number of string components separated by whitespaces the scalar value
        /// contains.
        /// </summary>
        public int ScalarComponentCount
        {
            get { return 1; }
        }

        /// <summary>
        /// Gets a string that represents the content type of this scalar member.
        /// </summary>
        public string ScalarContent
        {
            get { return this.TypeName; }
        }

        /// <summary>
        /// Gets or sets the increase and decrease amount for a single step.
        /// </summary>
        public double Step
        {
            get { return this.Dictionary.To(this._step, 0.1d); }
            set { this.SetProperty(ref this._step, value.ToString()); }
        }

        /// <summary>
        /// Gets or sets the type of double this member represents in the c++ code.
        /// </summary>
        public FloatType Type
        {
            get { return this.GetTypeFromString(this._type); }
            set { this.SetProperty(ref this._type, this.GetStringFromType(value)); }
        }

        /// <summary>
        /// Gets the type name for this member.
        /// </summary>
        public override string TypeName
        {
            get { return "double"; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new <see cref="DoubleMember"/> that is a copy of this instance.
        /// </summary>
        /// <returns>
        /// A new <see cref="DoubleMember"/> that is a copy of this instance.
        /// </returns>
        public new DoubleMember Clone()
        {
            return new DoubleMember(this, this.Structure);
        }

        /// <summary>
        /// Creates a <see cref="ITunable"/> object that represents a instance of this member
        /// with it's default value set.
        /// </summary>
        /// <param name="parent">
        /// The tunable parent the new tunable will belong to.
        /// </param>
        /// <returns>
        /// A parCodeGen tunable that represents a instance of this member.
        /// </returns>
        public override ITunable CreateTunable(ITunableParent parent)
        {
            return new DoubleTunable(this, parent);
        }

        /// <summary>
        /// Creates a <see cref="ITunable"/> object that represents a instance of this member
        /// using the specified string components to initialise it.
        /// </summary>
        /// <param name="components">
        /// The string components that provides the data used to initialise the new tunable.
        /// </param>
        /// <param name="parent">
        /// The tunable parent the new tunable will belong to.
        /// </param>
        /// <returns>
        /// A parCodeGen tunable that represents a instance of this member.
        /// </returns>
        public ITunable CreateTunable(List<string> components, ITunableParent parent)
        {
            if (components.Count != 1)
            {
                string msg = StringTable.DoubleComponentCountError;
                msg = msg.FormatInvariant(components.Count.ToString());
                throw new InvalidOperationException(msg);
            }

            DoubleTunable tunable = new DoubleTunable(this, parent);
            TryResult<double> value = this.Dictionary.TryTo(components[0], default(double));
            if (!value.Success)
            {
                string msg = StringTable.DoubleParseError;
                msg = msg.FormatInvariant(components[0]);
                throw new InvalidOperationException(msg);
            }

            tunable.Value = value.Value;
            return tunable;
        }

        /// <summary>
        /// Creates a <see cref="ITunable"/> object that represents a instance of this member
        /// using the specified Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The Xml.XmlReader that provides the data used to initialise the new tunable.
        /// </param>
        /// <param name="parent">
        /// The tunable parent the new tunable will belong to.
        /// </param>
        /// <param name="log">
        /// A object that is used to log any warnings or errors produced during the parsing of
        /// the specified reader.
        /// </param>
        /// <returns>
        /// A parCodeGen tunable that represents a instance of this member.
        /// </returns>
        public override ITunable CreateTunable(
            XmlReader reader, ITunableParent parent, ILog log)
        {
            return new DoubleTunable(reader, this, parent, log);
        }

        /// <summary>
        /// Creates a deep copy of the current instance.
        /// </summary>
        /// <returns>
        /// A deep copy of the current System.Object.
        /// </returns>
        public override object DeepClone()
        {
            return this.Clone();
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified
        /// <see cref="DoubleMember"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public bool Equals(DoubleMember other)
        {
            if (other == null)
            {
                return false;
            }

            return object.ReferenceEquals(other, this);
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="IMember"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public override bool Equals(IMember other)
        {
            return this.Equals(other as DoubleMember);
        }

        /// <summary>
        /// Writes the xml representation of this instance onto the specified
        /// System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that the xml representation of this instance should be
        /// written to.
        /// </param>
        public override void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                throw new SmartArgumentNullException(() => writer);
            }

            this.SerialiseBaseAttributes(writer);
            if (this._initialValue != null)
            {
                writer.WriteAttributeString(XmlInitialAttr, this._initialValue);
            }

            if (this._minimum != null)
            {
                writer.WriteAttributeString(XmlMinAttr, this._minimum);
            }

            if (this._maximum != null)
            {
                writer.WriteAttributeString(XmlMaxAttr, this._maximum);
            }

            if (this._step != null)
            {
                writer.WriteAttributeString(XmlStepAttr, this._step);
            }

            if (this._decimalPlaces != null)
            {
                writer.WriteAttributeString(XmlDecimalPlacesAttr, this._decimalPlaces);
            }

            if (this._highPrecision != null)
            {
                writer.WriteAttributeString(XmlHighPrecisionAttr, this._highPrecision);
            }

            if (this._type != null)
            {
                writer.WriteAttributeString(XmlTypeAttr, this._type);
            }
        }

        /// <summary>
        /// Validates this entity and creates a validation result object containing the errors
        /// and warnings.
        /// </summary>
        /// <param name="recursive">
        /// A value indicating whether any child models to this one should also be validated.
        /// </param>
        /// <returns>
        /// A validation result object containing all of the errors and warnings associated
        /// with this validation pass.
        /// </returns>
        public override ValidationResult Validate(bool recursive)
        {
            ValidationResult result = new ValidationResult();
            this.ValidateBaseProperties(result);

            if (!this.Dictionary.Validate<double>(this._minimum, true))
            {
                string msg = StringTable.FormatAttributeWarning;
                msg = msg.FormatCurrent(XmlMinAttr, this._minimum, "double");
                result.AddWarning(msg, this.Location);
            }

            if (!this.Dictionary.Validate<double>(this._maximum, true))
            {
                string msg = StringTable.FormatAttributeWarning;
                msg = msg.FormatCurrent(XmlMaxAttr, this._maximum, "double");
                result.AddWarning(msg, this.Location);
            }

            if (!this.Dictionary.Validate<double>(this._step, true))
            {
                string msg = StringTable.FormatAttributeWarning;
                msg = msg.FormatCurrent(XmlStepAttr, this._step, "double");
                result.AddWarning(msg, this.Location);
            }

            if (!this.Dictionary.Validate<double>(this._initialValue, true))
            {
                string msg = StringTable.FormatAttributeWarning;
                msg = msg.FormatCurrent(XmlInitialAttr, this._initialValue, "double");
                result.AddWarning(msg, this.Location);
            }

            if (!this.Dictionary.Validate<bool>(this._highPrecision, true))
            {
                string msg = StringTable.FormatAttributeWarning;
                msg = msg.FormatCurrent(XmlHighPrecisionAttr, this._highPrecision, "boolean");
                result.AddWarning(msg, this.Location);
            }

            if (!this.Dictionary.Validate<short>(this._decimalPlaces, true))
            {
                string msg = StringTable.FormatAttributeWarning;
                msg = msg.FormatCurrent(XmlDecimalPlacesAttr, this._decimalPlaces, "short");
                result.AddWarning(msg, this.Location);
            }

            double min = this.Minimum;
            double max = this.Maximum;
            double step = this.Step;
            double init = this.InitialValue;
            if (min.CompareTo(max) > 0)
            {
                string msg = StringTable.MinGreaterThanMaxWarning;
                msg = msg.FormatCurrent(min.ToString(), max.ToString());
                result.AddWarning(msg, this.Location);
            }

            if (init.CompareTo(max) > 0)
            {
                string msg = StringTable.InitialOutsideOfRangeWarning;
                msg = msg.FormatCurrent(init.ToString(), min.ToString(), max.ToString());
                result.AddWarning(msg, this.Location);
            }

            return result;
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            this._initialValue = reader.GetAttribute(XmlInitialAttr);
            this._minimum = reader.GetAttribute(XmlMinAttr);
            this._maximum = reader.GetAttribute(XmlMaxAttr);
            this._step = reader.GetAttribute(XmlStepAttr);
            this._decimalPlaces = reader.GetAttribute(XmlDecimalPlacesAttr);
            this._highPrecision = reader.GetAttribute(XmlHighPrecisionAttr);
            this._type = reader.GetAttribute(XmlTypeAttr);

            reader.Skip();
        }

        /// <summary>
        /// Gets the string value that is equivalent to the specified type.
        /// </summary>
        /// <param name="type">
        /// The type that the returned string will be equivalent to.
        /// </param>
        /// <returns>
        /// The string that is equivalent to the specified type.
        /// </returns>
        private string GetStringFromType(FloatType type)
        {
            switch (type)
            {
                case FloatType.Angle:
                    return FloatTypeAngle;
                case FloatType.Standard:
                case FloatType.Unrecognised:
                default:
                    return null;
            }
        }

        /// <summary>
        /// Gets the double type that is equivalent to the specified String.
        /// </summary>
        /// <param name="type">
        /// The string to determine the type to return.
        /// </param>
        /// <returns>
        /// The type that is equivalent to the specified string.
        /// </returns>
        private FloatType GetTypeFromString(string type)
        {
            if (type == null)
            {
                return FloatType.Standard;
            }
            else if (String.Equals(type, FloatTypeAngle))
            {
                return FloatType.Angle;
            }
            else
            {
                return FloatType.Unrecognised;
            }
        }
        #endregion Methods
    } // RSG.Metadata.Model.Definitions.Members.DoubleMember {Class}
} // RSG.Metadata.Model.Definitions.Members {Namespace}
