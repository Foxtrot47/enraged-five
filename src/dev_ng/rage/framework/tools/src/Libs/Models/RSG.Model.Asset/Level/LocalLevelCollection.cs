﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.ConfigParser;
using RSG.Base.Configuration;
using RSG.Base.Configuration.Bugstar;
using RSG.Model.Common;

namespace RSG.Model.Asset.Level
{
    /// <summary>
    /// Collection of vehicles that come from the local export data
    /// </summary>
    public class LocalLevelCollection : LevelCollectionBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public ConfigGameView GameView { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IConfig Config
        {
            get { return _config; }
        }
        private IConfig _config;

        /// <summary>
        /// 
        /// </summary>
        public IBugstarConfig BugstarConfig
        {
            get { return _bugstarConfig; }
        }
        private IBugstarConfig _bugstarConfig;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="exportDataPath"></param>
        public LocalLevelCollection(ConfigGameView gv, IConfig config, IBugstarConfig bugstarConfig)
            : base()
        {
            GameView = gv;
            _config = config;
            _bugstarConfig = bugstarConfig;
        }
        #endregion // Constructor(s)
        
        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="statsToLoad"></param>
        public override void LoadStats(IEnumerable<StreamableStat> statsToLoad)
        {
            foreach (ILevel level in Levels)
            {
                level.LoadStats(statsToLoad);
            }
        }
        #endregion // Overrides
    } // ExportDataLevelCollection
}
