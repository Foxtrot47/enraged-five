﻿// --------------------------------------------------------------------------------------------
// <copyright file="PadMember.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions.Members
{
    using System;
    using System.Xml;
    using RSG.Base.Extensions;
    using RSG.Editor;
    using RSG.Editor.Model;
    using RSG.Metadata.Model.Resources;

    /// <summary>
    /// Represents the &lt;pad&gt; member node in the parCodeGen system that can be instanced
    /// in a parCodeGen metadata file.
    /// </summary>
    public class PadMember : MemberBase, IEquatable<PadMember>
    {
        #region Fields
        /// <summary>
        /// The name of the xml attribute containing the data for the
        /// <see cref="Bytes"/> property.
        /// </summary>
        private const string XmlBytesAttr = "bytes";

        /// <summary>
        /// The private field used for the <see cref="Bytes"/> property.
        /// </summary>
        private string _bytes;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="PadMember"/> class to be one of the
        /// members of the specified structure.
        /// </summary>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        public PadMember(IStructure structure)
            : base(structure)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="PadMember"/> class as a copy of the
        /// specified instance.
        /// </summary>
        /// <param name="other">
        /// The instance to copy.
        /// </param>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        public PadMember(PadMember other, IStructure structure)
            : base(other, structure)
        {
            this._bytes = other._bytes;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="PadMember"/> class.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        public PadMember(XmlReader reader, IStructure structure)
            : base(reader, structure)
        {
            this.Deserialise(reader);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the number of bytes that this member is representing as padding.
        /// </summary>
        public int Bytes
        {
            get { return this.Dictionary.To<int>(this._bytes, 0); }
            set { this.SetProperty(ref this._bytes, value.ToString()); }
        }

        /// <summary>
        /// Gets a value indicating whether this member can have instances of it defined in
        /// metadata, i.e whether or not a tunable can be created for it.
        /// </summary>
        public override bool CanBeInstanced
        {
            get { return false; }
        }

        /// <summary>
        /// Gets the type name for this member.
        /// </summary>
        public override string TypeName
        {
            get { return "pad"; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new <see cref="PadMember"/> that is a copy of this instance.
        /// </summary>
        /// <returns>
        /// A new <see cref="PadMember"/> that is a copy of this instance.
        /// </returns>
        public new PadMember Clone()
        {
            return new PadMember(this, this.Structure);
        }

        /// <summary>
        /// Creates a deep copy of the current instance.
        /// </summary>
        /// <returns>
        /// A deep copy of the current System.Object.
        /// </returns>
        public override object DeepClone()
        {
            return this.Clone();
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="PadMember"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public bool Equals(PadMember other)
        {
            if (other == null)
            {
                return false;
            }

            return object.ReferenceEquals(other, this);
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="IMember"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public override bool Equals(IMember other)
        {
            return this.Equals(other as PadMember);
        }

        /// <summary>
        /// Writes the xml representation of this instance onto the specified
        /// System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that the xml representation of this instance should be
        /// written to.
        /// </param>
        public override void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                throw new SmartArgumentNullException(() => writer);
            }

            this.SerialiseBaseAttributes(writer);
            if (this._bytes != null)
            {
                writer.WriteAttributeString(XmlBytesAttr, this._bytes);
            }
        }

        /// <summary>
        /// Validates this entity and creates a validation result object containing the errors
        /// and warnings.
        /// </summary>
        /// <param name="recursive">
        /// A value indicating whether any child models to this one should also be validated.
        /// </param>
        /// <returns>
        /// A validation result object containing all of the errors and warnings associated
        /// with this validation pass.
        /// </returns>
        public override ValidationResult Validate(bool recursive)
        {
            ValidationResult result = new ValidationResult();
            this.ValidateBaseProperties(result);

            if (!this.Dictionary.Validate<int>(this._bytes, true))
            {
                string msg = StringTable.FormatAttributeWarning;
                msg = msg.FormatCurrent(XmlBytesAttr, this._bytes, "int");
                result.AddWarning(msg, this.Location);
            }

            return result;
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            this._bytes = reader.GetAttribute(XmlBytesAttr);
            reader.Skip();
        }
        #endregion Methods
    } // RSG.Metadata.Model.Definitions.Members.PadMember {Class}
} // RSG.Metadata.Model.Definitions.Members {Namespace}
