﻿//---------------------------------------------------------------------------------------------
// <copyright file="EnumConstant.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Xml;
    using RSG.Base;
    using RSG.Base.Extensions;
    using RSG.Base.Logging;
    using RSG.Editor;
    using RSG.Editor.Model;
    using RSG.Metadata.Model.Resources;

    /// <summary>
    /// Defines a constant that is stored in a parCodeGen enumeration definition. This class
    /// cannot be inherited.
    /// </summary>
    internal class EnumConstant : ModelBase, IEnumConstant
    {
        #region Fields
        /// <summary>
        /// The name of the xml attribute used for the <see cref="Description"/> property.
        /// </summary>
        private const string XmlDescriptionAttr = "description";

        /// <summary>
        /// The name of the xml attribute used for the <see cref="HideFrom"/> property.
        /// </summary>
        private const string XmlHideFromAttr = "hideFrom";

        /// <summary>
        /// The name of the xml attribute used for the <see cref="Name"/> property.
        /// </summary>
        private const string XmlNameAttr = "name";

        /// <summary>
        /// The name of the xml attribute used for the <see cref="Value"/> property.
        /// </summary>
        private const string XmlValueAttr = "value";

        /// <summary>
        /// The private field used for the <see cref="Description"/> property.
        /// </summary>
        private string _description;

        /// <summary>
        /// The private field used for the <see cref="Enumeration"/> property.
        /// </summary>
        private IEnumeration _enumeration;

        /// <summary>
        /// The private field used for the <see cref="Filename"/> property.
        /// </summary>
        private string _filename;

        /// <summary>
        /// The private field used for the <see cref="HideFrom"/> property.
        /// </summary>
        private string _hideFrom;

        /// <summary>
        /// A private value indicating whether this constants value has been calculated at
        /// least once.
        /// </summary>
        private bool _initialisedValue;

        /// <summary>
        /// The private field used for the <see cref="LineNumber"/> property.
        /// </summary>
        private int _lineNumber;

        /// <summary>
        /// The private field used for the <see cref="LinePosition"/> property.
        /// </summary>
        private int _linePosition;

        /// <summary>
        /// The private field used for the <see cref="Name"/> property.
        /// </summary>
        private string _name;

        /// <summary>
        /// The private field used for the <see cref="Value"/> property.
        /// </summary>
        private string _rawValue;

        /// <summary>
        /// The private collection of attributes that have been found on the xml data that
        /// weren't recognised as valid attributes.
        /// </summary>
        private HashSet<string> _unrecognisedAttributes;

        /// <summary>
        /// A private value indicating whether the raw value property is valid (i.e. can be
        /// parsed as a long value).
        /// </summary>
        private bool _validValue;

        /// <summary>
        /// The private field used for the <see cref="Value"/> property.
        /// </summary>
        private long _value;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="EnumConstant"/> class.
        /// </summary>
        /// <param name="enumeration">
        /// The enumeration that this constant will be associated with.
        /// </param>
        public EnumConstant(IEnumeration enumeration)
        {
            if (enumeration == null)
            {
                throw new SmartArgumentNullException(() => enumeration);
            }

            this._enumeration = enumeration;
            this._unrecognisedAttributes = new HashSet<string>();
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="EnumConstant"/> class.
        /// </summary>
        /// <param name="name">
        /// The name of the constant.
        /// </param>
        /// <param name="enumeration">
        /// The enumeration that this constant will be associated with.
        /// </param>
        public EnumConstant(string name, IEnumeration enumeration)
        {
            if (enumeration == null)
            {
                throw new SmartArgumentNullException(() => enumeration);
            }

            this._enumeration = enumeration;
            this._name = name;
            this._unrecognisedAttributes = new HashSet<string>();
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="EnumConstant"/> class as a copy of the
        /// specified instance.
        /// </summary>
        /// <param name="other">
        /// The instance to copy.
        /// </param>
        /// <param name="enumeration">
        /// The enumeration that this constant will be associated with.
        /// </param>
        public EnumConstant(EnumConstant other, IEnumeration enumeration)
        {
            if (enumeration == null)
            {
                throw new SmartArgumentNullException(() => enumeration);
            }

            if (other == null)
            {
                throw new SmartArgumentNullException(() => other);
            }

            this._name = other._name;
            this._description = other._description;
            this._rawValue = other._rawValue;
            this._value = other._value;
            if (other.HideFrom != null)
            {
                this.HideFrom = other.HideFrom.Clone() as string[];
            }

            this._enumeration = enumeration;
            this._unrecognisedAttributes = new HashSet<string>();
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="EnumConstant"/> class using the
        /// specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        /// <param name="enumeration">
        /// The enumeration that this structure is associated with.
        /// </param>
        public EnumConstant(XmlReader reader, IEnumeration enumeration)
        {
            if (enumeration == null)
            {
                throw new SmartArgumentNullException(() => enumeration);
            }

            if (reader == null)
            {
                throw new SmartArgumentNullException(() => reader);
            }

            this._enumeration = enumeration;
            IXmlLineInfo info = reader as IXmlLineInfo;
            if (info != null)
            {
                this._lineNumber = info.LineNumber;
                this._linePosition = info.LinePosition;
            }
            else
            {
                this._lineNumber = -1;
                this._linePosition = -1;
            }

            Uri uri = new Uri(reader.BaseURI);
            this._filename = Path.GetFullPath(uri.LocalPath);
            this._unrecognisedAttributes = new HashSet<string>();
            this.Deserialise(reader);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the description for this constant.
        /// </summary>
        public string Description
        {
            get { return this._description; }
            set { this.SetProperty(ref this._description, value); }
        }

        /// <summary>
        /// Gets the dictionary this definition is defined inside.
        /// </summary>
        public IDefinitionDictionary Dictionary
        {
            get
            {
                if (this._enumeration == null)
                {
                    return null;
                }

                return this._enumeration.Dictionary;
            }
        }

        /// <summary>
        /// Gets or sets a reference to the enumeration that owns this constant.
        /// </summary>
        public IEnumeration Enumeration
        {
            get
            {
                return this._enumeration;
            }

            set
            {
                if (this.SetProperty(ref this._enumeration, value))
                {
                    // Need to re-calculate the value as the generator might have changed.
                    if (String.IsNullOrWhiteSpace(this._rawValue))
                    {
                        this.Value = this.GetValue(this._rawValue, value);
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the filename this instance is defined in or null if not applicable.
        /// </summary>
        public string Filename
        {
            get { return this._filename; }
            set { this.SetProperty(ref this._filename, value); }
        }

        /// <summary>
        /// Gets or sets the systems that this value will be hidden from.
        /// </summary>
        public string[] HideFrom
        {
            get
            {
                return this._hideFrom != null ? this._hideFrom.Split(',') : new string[0];
            }

            set
            {
                if (value == null)
                {
                    this.SetProperty(ref this._hideFrom, null);
                }
                else
                {
                    this.SetProperty(ref this._hideFrom, String.Join(", ", value));
                }
            }
        }

        /// <summary>
        /// Gets or sets the line number this instance is defined on or -1 if not available.
        /// </summary>
        public int LineNumber
        {
            get { return this._lineNumber; }
            set { this.SetProperty(ref this._lineNumber, value); }
        }

        /// <summary>
        /// Gets or sets the line position this instance is defined at or -1 if not available.
        /// </summary>
        public int LinePosition
        {
            get { return this._linePosition; }
            set { this.SetProperty(ref this._linePosition, value); }
        }

        /// <summary>
        /// Gets the location of this definition inside the parCodeGen definition data sat on
        /// the local disk.
        /// </summary>
        public FileLocation Location
        {
            get { return new FileLocation(this.LineNumber, this.LinePosition, this.Filename); }
        }

        /// <summary>
        /// Gets or sets the name of the constant.
        /// </summary>
        public string Name
        {
            get { return this._name; }
            set { this.SetProperty(ref this._name, value); }
        }

        /// <summary>
        /// Gets the raw string representation of the value this constant represents.
        /// </summary>
        public string RawValue
        {
            get { return this._rawValue; }
        }

        /// <summary>
        /// Gets or sets the value this constant represents.
        /// </summary>
        public long Value
        {
            get
            {
                if (this._initialisedValue == false)
                {
                    this._value = this.GetValue(this._rawValue, this.Enumeration);
                }

                return this._value;
            }

            set
            {
                if (this.SetProperty(ref this._value, value))
                {
                    this._rawValue = value.ToString();
                }
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new <see cref="EnumConstant"/> that is a copy of this instance.
        /// </summary>
        /// <returns>
        /// A new <see cref="EnumConstant"/> that is a copy of this instance.
        /// </returns>
        public new EnumConstant Clone()
        {
            return new EnumConstant(this, this.Enumeration);
        }

        /// <summary>
        /// Creates a deep copy of the current instance.
        /// </summary>
        /// <returns>
        /// A deep copy of the current System.Object.
        /// </returns>
        public override object DeepClone()
        {
            return this.Clone();
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified object.
        /// </summary>
        /// <param name="obj">
        /// The object to compare with this instance.
        /// </param>
        /// <returns>
        /// True if the specified object is equal to this instance; otherwise, false.
        /// </returns>
        public override bool Equals(object obj)
        {
            return this.Equals(obj as IEnumConstant);
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified
        /// <see cref="EnumConstant"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public bool Equals(EnumConstant other)
        {
            if (other == null)
            {
                return false;
            }

            if (!String.Equals(this.Name, other.Name, StringComparison.OrdinalIgnoreCase))
            {
                return false;
            }

            if (this.Value != other.Value)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified
        /// <see cref="IEnumConstant"/>.
        /// </summary>
        /// <param name="other">
        /// A <see cref="IEnumConstant"/> to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public bool Equals(IEnumConstant other)
        {
            return this.Equals(other as EnumConstant);
        }

        /// <summary>
        /// Serves as a hash function for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for the current instance.
        /// </returns>
        public override int GetHashCode()
        {
            if (this._name == null)
            {
                return base.GetHashCode();
            }

            return this._name.GetHashCode();
        }

        /// <summary>
        /// Creates a new <see cref="IEnumConstant"/> that is a copy of this instance.
        /// </summary>
        /// <returns>
        /// A new <see cref="IEnumConstant"/> that is a copy of this instance.
        /// </returns>
        IEnumConstant IEnumConstant.Clone()
        {
            return this.Clone();
        }

        /// <summary>
        /// Writes the xml representation of this instance onto the specified
        /// System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that the xml representation of this instance should be
        /// written to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                throw new SmartArgumentNullException(() => writer);
            }

            if (this._name != null)
            {
                writer.WriteAttributeString(XmlNameAttr, this._name);
            }

            if (this._description != null)
            {
                writer.WriteAttributeString(XmlDescriptionAttr, this._description);
            }

            if (this._rawValue != null)
            {
                writer.WriteAttributeString(XmlValueAttr, this._rawValue);
            }

            if (this._hideFrom != null)
            {
                writer.WriteAttributeString(XmlHideFromAttr, this._hideFrom);
            }
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A string that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return this._name ?? base.ToString();
        }

        /// <summary>
        /// Validates this entity and creates a validation result object containing the errors
        /// and warnings.
        /// </summary>
        /// <param name="recursive">
        /// A value indicating whether any child models to this one should also be validated.
        /// </param>
        /// <returns>
        /// A validation result object containing all of the errors and warnings associated
        /// with this validation pass.
        /// </returns>
        public override ValidationResult Validate(bool recursive)
        {
            ValidationResult result = new ValidationResult();
            foreach (string attribute in this._unrecognisedAttributes)
            {
                string msg = StringTable.UnrecognisedEnumConstantAttribute;
                msg = msg.FormatCurrent(attribute);
                result.AddWarning(msg, this.Location);
            }

            if (this._name == null)
            {
                string msg = StringTable.MissingEnumConstantNameError;
                result.AddError(msg, this.Location);
            }
            else if (String.IsNullOrWhiteSpace(this._name))
            {
                string msg = StringTable.EmptyEnumConstantNameError;
                result.AddError(msg, this.Location);
            }

            if (!this._validValue)
            {
                string msg = StringTable.EnumConstantValueParseError;
                msg = msg.FormatCurrent(this._rawValue);
                result.AddError(msg, this.Location);
            }

            return result;
        }

        /// <summary>
        /// Initialises this instance using the specified System.Xml.XmlReader as a data
        /// provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader used as the data provider to initialise this instance
        /// with.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            for (int i = 0; i < reader.AttributeCount; i++)
            {
                reader.MoveToAttribute(i);
                string name = reader.LocalName;
                string value = reader.Value;

                if (String.Equals(name, XmlNameAttr))
                {
                    this._name = value;
                }
                else if (String.Equals(name, XmlDescriptionAttr))
                {
                    this._description = value;
                }
                else if (String.Equals(name, XmlValueAttr))
                {
                    this._rawValue = value;
                }
                else if (String.Equals(name, XmlHideFromAttr))
                {
                    this._hideFrom = value;
                }
                else
                {
                    this._unrecognisedAttributes.Add(name);
                }
            }

            reader.Skip();
        }

        /// <summary>
        /// Determines the value for this constant based on the specified string value
        /// and enumeration.
        /// </summary>
        /// <param name="value">
        /// The string value used to determine the value of the constant.
        /// </param>
        /// <param name="enumeration">
        /// The enumeration to use to determine the value if the string value isn't valid.
        /// </param>
        /// <returns>
        /// The value for this constant.
        /// </returns>
        private long GetValue(string value, IEnumeration enumeration)
        {
            this._initialisedValue = true;
            this._validValue = true;
            if (!String.IsNullOrWhiteSpace(value))
            {
                TryResult<long> tryResult = this.Dictionary.TryTo<long>(value, 0);
                if (tryResult.Success)
                {
                    return tryResult.Value;
                }
                else if (enumeration != null)
                {
                    Dictionary<string, IEnumConstant> valueLookup =
                        new Dictionary<string, IEnumConstant>();
                    foreach (IEnumConstant constant in enumeration.EnumConstants)
                    {
                        if (!valueLookup.ContainsKey(constant.Name))
                        {
                            valueLookup.Add(constant.Name, constant);
                        }
                    }

                    string[] parts = value.SafeSplit('|', true);
                    long pipedValue = 0;
                    string dataType = this._enumeration.DataType;
                    foreach (string part in parts)
                    {
                        IEnumConstant constant = null;
                        string lookupName = part.Replace(dataType, String.Empty);
                        lookupName = lookupName.TrimStart(':');
                        if (!valueLookup.TryGetValue(part, out constant))
                        {
                            this._validValue = false;
                            return 0;
                        }

                        pipedValue |= constant.Value;
                    }

                    return pipedValue;
                }
                else
                {
                    this._validValue = false;
                    return 0;
                }
            }

            if (enumeration == null)
            {
                this._validValue = false;
                return 0;
            }

            switch (enumeration.ValueGenerator)
            {
                case EnumValueGenerator.Unrecognised:
                case EnumValueGenerator.Increment:
                    {
                        int index = enumeration.GetIndexOf(this.Name);
                        if (index < 0)
                        {
                            return enumeration.EnumConstants.Count;
                        }

                        if (index == 0)
                        {
                            return 0;
                        }

                        return enumeration[index - 1].Value + 1;
                    }

                default:
                    {
                        if (this.Name == null)
                        {
                            this._validValue = false;
                            return 0;
                        }

                        return (long)this.GetHashCode();
                    }
            }
        }
        #endregion Methods
    } // RSG.Metadata.Model.Definitions.EnumConstant {Class}
} // RSG.Metadata.Model.Definitions {Namespace}
