﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor;

namespace RSG.Model.Map.ViewModel
{
    /// <summary>
    /// The view model for the LevelDictionary object used by the view
    /// </summary>
    public class LevelDictionaryViewModel :
        RSG.Base.Editor.HierarchicalViewModelBase
    {
        /// <summary>
        /// The model reference that this view model represents.
        /// </summary>
        public LevelDictionary Model
        {
            get { return m_model; }
            set { m_model = value; }
        }
        private LevelDictionary m_model;

        /// <summary>
        /// Returns a enumerable that goes through all the levels in this
        /// level dictionary
        /// </summary>
        public IEnumerable<LevelViewModel> Levels
        {
            get
            {
                foreach (LevelViewModel level in this.Children.Where(c => c is LevelViewModel == true))
                {
                    yield return level;
                }
            }
        }

        public LevelViewModel this[String name]
        {
            get
            {
                foreach (LevelViewModel level in this.Children.Where(c => c is LevelViewModel == true && (c as LevelViewModel).Name == name))
                {
                    return level;
                }
                return null;
            }
        }

        /// <summary>
        /// Default Constructor
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="model"></param>
        public LevelDictionaryViewModel(IViewModel parent, LevelDictionary model)
        {
            this.Parent = parent;
            this.Model = model;

            foreach (Level level in model.Levels)
            {
                LevelViewModel newLevel = new LevelViewModel(this, level);
                this.Children.Add(newLevel);
            }
        }

    }
}
