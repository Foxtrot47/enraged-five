﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using RSG.Base.ConfigParser;
using RSG.Base.Logging;
using RSG.ManagedRage;
using RSG.Model.Common;
using System.Text;

namespace RSG.Model.Report.Reports
{
    /// <summary>
    /// Generates a CSV report containing character information
    /// </summary>
    public class CharacterCSVReport : CSVReport, IDynamicMapReport
    {
        #region Constants
        private const String NAME = "Export Character CSV Report";
        private const String DESCRIPTION = "Exports the selected level characters into a .csv file"; 
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Determines whether the map data should be loaded
        /// </summary>
        public bool LoadMapData
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Determines whether the car generators should be loaded
        /// 
        /// </summary>
        public bool LoadCarGenarators
        {
            get { return false; }
        }
        #endregion // Properties

        #region Construtcor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public CharacterCSVReport()
            : base(NAME, DESCRIPTION)
        {
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Generate the report dynamically.
        /// </summary>
        public void Generate(ILevel level, RSG.Base.ConfigParser.ConfigGameView gv, IPlatformCollection platforms)
        {
            DateTime start = DateTime.Now;
            ExportCharacterCSVReport(level, gv, platforms);
            Log.Log__Message("Character CSV export complete. (report took {0} milliseconds to create.)", (DateTime.Now - start).TotalMilliseconds);
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="objectFilename"></param>
        /// <param name="interiorFilename"></param>
        /// <param name="level"></param>
        /// <param name="gv"></param>
        private void ExportCharacterCSVReport(ILevel level, RSG.Base.ConfigParser.ConfigGameView gv, IPlatformCollection platforms)
        {
            try
            {
                using (StreamWriter sw = new StreamWriter(Filename))
                {
                    WriteCsvHeader(sw, platforms);
                    IAsset characterContainer = null;
                    foreach (IAsset asset in level.AssetChildren)
                    {
                        if (0 == String.Compare("characters", asset.Name, true))
                        {
                            characterContainer = asset;
                            break;
                        }
                    }
                    if (null != characterContainer)
                    {
                        System.Diagnostics.Debug.Assert(characterContainer is IHasAssetChildren);
                        foreach (IAsset asset in (characterContainer as IHasAssetChildren).AssetChildren)
                        {
                            // Skip all non-Character assets
                            ICharacterCategoryCollection characterCollection = asset as ICharacterCategoryCollection;
                            if (characterCollection != null)
                            {
                                foreach (IAsset child in characterCollection.AssetChildren)
                                {
                                    // Skip all non-Character assets
                                    ICharacter character = child as ICharacter;
                                    if (character != null)
                                    {
                                        // Ensure that all the data is loaded
                                        character.RequestAllStatistics(true, false);

                                        string scvRecord = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}",
                                            character.Name, character.Category.ToString(), character.PolyCount, character.CollisionCount, character.ClothPolycount, character.ClothCount,
                                            character.Textures.Count(), character.Shaders.Count(),
                                            character.BoneCount,
                                            GenerateMemoryDetails(character, platforms));

                                        sw.WriteLine(scvRecord);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                RSG.Base.Logging.Log.Log__Exception(ex, "Error creating csv file.");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sw"></param>
        /// <param name="milo"></param>
        private void WriteCsvHeader(StreamWriter sw, IPlatformCollection platforms)
        {
            string header = "Name,Category,Poly Count,Collision Count,Cloth Poly Count,Cloth Count,Texture Count,Shader Count,Bone Count";

            foreach (IPlatform platform in platforms)
            {
                header += String.Format(",{0} Physical Size,{0} Virtual Size,{0} Total Size", platform.Name);
            }

            sw.WriteLine(header);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="character"></param>
        /// <param name="platforms"></param>
        /// <returns></returns>
        private string GenerateMemoryDetails(ICharacter character, IPlatformCollection platforms)
        {
            StringBuilder details = new StringBuilder("");

            foreach (IPlatform platform in platforms)
            {
                ICharacterPlatformStat stat = character.GetPlatformStat(platform.Platform);

                if (stat != null)
                {
                    details.Append(String.Format("{0},{1},{2},", stat.PhysicalSize, stat.VirtualSize, stat.PhysicalSize + stat.VirtualSize));
                }
                else
                {
                    details.Append("n/a,n/a,n/a,");
                }
            }

            return details.ToString().TrimEnd(new char[] { ',' });
        }
        #endregion // Private Methods
    }
}
