﻿namespace RSG.Model.Report2.Reports.Map
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using RSG.Base.ConfigParser;
    using RSG.Base.Math;
    using RSG.Base.Tasks;
    using RSG.Model.Common;
    using RSG.Model.Common.Map;
    using RSG.Model.Report;
    using RSG.SceneXml;

    /// <summary>
    /// A report that lists the entities and collision that are define outside of IMAP/IPL
    /// files for the prologue area." (url:bugstar:1027494).
    /// </summary>
    public class GhostPrologueReport : CSVReport, IDynamicLevelReport
    {
        #region Fields
        /// <summary>
        /// The name of this report.
        /// </summary>
        private const string Name = "Ghost Prologue";

        /// <summary>
        /// The description for this report.
        /// </summary>
        private const string Description =
            "Exports a csv report that lists the entities and collision that are defined " +
            "outside of IMAP/IPL files.";

        /// <summary>
        /// The private field used for the <see cref="GenerationTask"/> property.
        /// </summary>
        private ITask _generationTask;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="GhostPrologueReport"/> class.
        /// </summary>
        public GhostPrologueReport()
            : base(Name, Description)
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the task that represents the work needing to be done to generate this report.
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (this._generationTask == null)
                {
                    CompositeTask task = new CompositeTask("Generate Report");
                    task.AddSubTask(new ActionTask("Loading data", this.EnsureDataLoaded));
                    task.AddSubTask(new ActionTask("Generating report", this.GenerateReport));
                    this._generationTask = task;
                }

                return this._generationTask;
            }
        }

        /// <summary>
        /// Gets a iterator around the stats that are required to generate this report. Always
        /// returns null.
        /// </summary>
        public IEnumerable<StreamableStat> RequiredStats
        {
            get { return null; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Ensures that the data needed for this report is loaded.
        /// </summary>
        /// <param name="context">
        /// The task context for this action.
        /// </param>
        /// <param name="progress">
        /// The object used to notify a listener of the progress.
        /// </param>
        private void EnsureDataLoaded(ITaskContext context, IProgress<TaskProgress> progress)
        {
            DynamicLevelReportContext reportContext = context as DynamicLevelReportContext;
            Debug.Assert(reportContext != null, "Unable to load date for report");
            if (reportContext == null)
            {
                return;
            }

            IMapHierarchy hierarchy = reportContext.Level.MapHierarchy;
            Debug.Assert(hierarchy != null, "Unable to load date for report");
            if (hierarchy == null || hierarchy.AllSections == null)
            {
                return;
            }

            List<IMapSection> mapSections = hierarchy.AllSections.ToList();
            float increment = 1.0f / mapSections.Count;
            string fmt = "Loading {0}";
            foreach (IMapSection section in mapSections)
            {
                context.Token.ThrowIfCancellationRequested();
                if (section.ParentArea == null || section.ParentArea.Name != "_prologue")
                {
                    continue;
                }                

                string message = String.Format(fmt, section.Name);
                progress.Report(new TaskProgress(increment, true, message));

                section.LoadStats(new List<StreamableStat>() { StreamableMapSectionStat.Archetypes, StreamableMapSectionStat.Entities });
            }
        }

        /// <summary>
        /// Generates the report and saves the file associated with it.
        /// </summary>
        /// <param name="context">
        /// The task context for this action.
        /// </param>
        /// <param name="progress">
        /// The object used to notify a listener of the progress.
        /// </param>
        private void GenerateReport(ITaskContext context, IProgress<TaskProgress> progress)
        {
            DynamicLevelReportContext reportContext = context as DynamicLevelReportContext;
            Debug.Assert(reportContext != null, "Unable to generate report");
            if (reportContext == null)
            {
                return;
            }

            IMapHierarchy hierarchy = reportContext.Level.MapHierarchy;
            Debug.Assert(hierarchy != null, "Unable to generate report");
            if (hierarchy == null || hierarchy.AllSections == null)
            {
                return;
            }

            using (StreamWriter writer = new StreamWriter(this.Filename))
            {
                writer.WriteLine(
                    "Object Name," +
                    "Section," +
                    "Parent Area," +
                    "Grandparent Area," +
                    "X,Y,Z");

                List<TargetObjectDef> nonIplGroup = new List<TargetObjectDef>();
                foreach (IMapSection section in hierarchy.AllSections)
                {
                    context.Token.ThrowIfCancellationRequested();
                    if (section.ParentArea == null || section.ParentArea.Name != "_prologue")
                    {
                        continue;
                    }

                    string filename = Path.Combine(reportContext.GameView.ExportDir, "levels", reportContext.Level.Name, "_prologue", section.Name + ".xml");
                    if (!File.Exists(filename))
                    {
                        continue;
                    }

                    Scene scene = new Scene(filename, LoadOptions.Objects, true);
                    int passIndex = -1;
                    int validIndex = 1;
                    if (!scene.Objects[0].IsContainer())
                    {
                        validIndex = 0;
                    }

                    foreach (TargetObjectDef objectDef in scene.Walk(Scene.WalkMode.BreadthFirst))
                    {
                        passIndex++;
                        if (passIndex != validIndex)
                        {
                            continue;
                        }

                        if (!objectDef.IsObject() || objectDef.DontExport() == true)
                        {
                            continue;
                        }

                        string IplGroup = objectDef.GetAttribute<string>(AttrNames.OBJ_IPL_GROUP, null);
                        if (IplGroup == null)
                        {
                            IMapArea parentArea = (section != null ? section.ParentArea : null);
                            IMapArea grandParentArea = (parentArea != null ? parentArea.ParentArea : null);
                            string csvRecord = String.Format("{0},{1},{2},{3},", objectDef.Name, section != null ? section.Name : "n/a", parentArea != null ? parentArea.Name : "n/a", grandParentArea != null ? grandParentArea.Name : "n/a");
                            Vector3f position = objectDef.NodeTransform.Translation;
                            csvRecord += String.Format("{0},{1},{2}", position.X, position.Y, position.Z);

                            nonIplGroup.Add(objectDef);
                        }
                    }
                }
            }
        }
        #endregion Methods
    } // RSG.Model.Report2.Reports.Map.GhostPrologueReport {Class}
} // RSG.Model.Report2.Reports.Map {Namespace}
