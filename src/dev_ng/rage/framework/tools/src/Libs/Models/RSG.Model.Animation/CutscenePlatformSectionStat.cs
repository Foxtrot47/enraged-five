﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Animation;

namespace RSG.Model.Animation
{
    /// <summary>
    /// Physical/virtual sizes for a section of a cutscene.
    /// </summary>
    [DataContract]
    [Serializable]
    public class CutscenePlatformSectionStat : ICutscenePlatformSectionStat
    {
        #region Properties
        /// <summary>
        /// Index of this item.
        /// </summary>
        [DataMember]
        public uint Index { get; private set; }

        /// <summary>
        /// Total physical size.
        /// </summary>
        [DataMember]
        public ulong PhysicalSize { get; private set; }

        /// <summary>
        /// Total virtual size.
        /// </summary>
        [DataMember]
        public ulong VirtualSize { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// For serialisation purposes.
        /// </summary>
        private CutscenePlatformSectionStat()
        {
        }
        
        /// <summary>
        /// Main constructor.
        /// </summary>
        public CutscenePlatformSectionStat(uint index, ulong physicalSize, ulong virtualSize)
        {
            Index = index;
            PhysicalSize = physicalSize;
            VirtualSize = virtualSize;
        }
        #endregion // Constructor(s)
    } // CutscenePlatformSectionStat
}
