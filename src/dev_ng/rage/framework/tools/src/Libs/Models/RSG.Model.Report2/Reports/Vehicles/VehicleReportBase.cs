﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.Base.Tasks;

namespace RSG.Model.Report.Reports.Vehicles
{
    /// <summary>
    /// For now all vehicle reports are IDynamicMapReport ones...
    /// </summary>
    public abstract class VehicleReportBase : Report, IDynamicLevelReport
    {
        #region Properties
        /// <summary>
        /// Task to run for generating this report.
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (m_task == null)
                {
                    //m_task = new ActionTask("Generating report", (context, progress) => GenerateReport((DynamicLevelReportContext)context, progress));
                    CompositeTask task = new CompositeTask("Generate Report");
                    task.AddSubTask(new ActionTask("Loading data", (context, progress) => EnsureDataLoaded((DynamicLevelReportContext)context, progress)), 10);
                    task.AddSubTask(new ActionTask("Generating report", (context, progress) => GenerateReport((DynamicLevelReportContext)context, progress)));
                    m_task = task;
                }
                return m_task;
            }
        }
        private ITask m_task;

        /// <summary>
        /// List of stats that are required to generate this report.
        /// </summary>
        public IEnumerable<StreamableStat> RequiredStats
        {
            get
            {
                if (m_requiredStats == null)
                {
                    //m_requiredStats = StreamableStatUtils.GetValues<StreamableVehicleStat>().ToArray();
                }
                return m_requiredStats;
            }
        }
        private StreamableStat[] m_requiredStats;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public VehicleReportBase(string name, string description)
            : base(name, description)
        {
        }
        #endregion // Constructor(s)

        #region Abstract Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void EnsureDataLoaded(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            IVehicleCollection vehicles = context.Level.Vehicles;

            if (vehicles != null)
            {
                vehicles.LoadAllStats();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="progress"></param>
        protected abstract void GenerateReport(DynamicLevelReportContext context, IProgress<TaskProgress> progress);
        #endregion // Abstract Methods
    } // VehicleReport
}
