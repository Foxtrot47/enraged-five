﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common.Map;
using RSG.Model.Common;
using RSG.Base.Math;
using System.ComponentModel;
using RSG.SceneXml;
using RSG.Model.Map3.Util;
using RSG.Base.Collections;
using RSG.Base.ConfigParser;
using System.Diagnostics;
using RSG.Statistics.Common.Dto.GameAssetStats;
using RSG.Statistics.Common.Dto.GameAssetStats.Bundles;
using RSG.Base.Logging;

namespace RSG.Model.Map3
{
    /// <summary>
    /// 
    /// </summary>
    public class InteriorArchetype : MapArchetypeBase, IInteriorArchetype
    {
        #region Properties
        /// <summary>
        /// List of rooms this interior has
        /// </summary>
        [Browsable(false)]
        [StreamableStatAttribute(StreamableInteriorArchetypeStat.RoomsString)]
        public ObservableCollection<IRoom> Rooms
        {
            get
            {
                CheckStatLoaded(StreamableInteriorArchetypeStat.Rooms);
                return m_rooms;
            }
            private set
            {
                SetPropertyValue(ref m_rooms, value, "Rooms");
                SetStatLodaded(StreamableInteriorArchetypeStat.Rooms, true);
            }
        }
        private ObservableCollection<IRoom> m_rooms;

        /// <summary>
        /// List of portals this interior has
        /// </summary>
        [Browsable(false)]
        [StreamableStatAttribute(StreamableInteriorArchetypeStat.PortalsString)]
        public ObservableCollection<IPortal> Portals
        {
            get
            {
                CheckStatLoaded(StreamableInteriorArchetypeStat.Portals);
                return m_portals;
            }
            private set
            {
                SetPropertyValue(ref m_portals, value, "Portals");
                SetStatLodaded(StreamableInteriorArchetypeStat.Portals, true);
            }
        }
        private ObservableCollection<IPortal> m_portals;

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        [StreamableStatAttribute(StreamableInteriorArchetypeStat.EntitiesString)]
        public ObservableCollection<IEntity> ChildEntities
        {
            get
            {
                CheckStatLoaded(StreamableInteriorArchetypeStat.Entities);
                return m_childEntities;
            }
            private set
            {
                SetPropertyValue(ref m_childEntities, value, "ChildEntities");
                SetStatLodaded(StreamableInteriorArchetypeStat.Entities, true);
            }
        }
        private ObservableCollection<IEntity> m_childEntities;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public InteriorArchetype(string name, IMapSection parent)
            : base(name, parent)
        {
        }
        #endregion // Constructor(s)

        #region IMapArchetype Implementation
        /// <summary>
        /// Load basic stats from the scene xml object def
        /// </summary>
        /// <param name="sceneObject"></param>
        /// <param name="scene"></param>
        public override void LoadStatsFromExportData(TargetObjectDef sceneObject, Scene scene)
        {
            if (this.Name == "V_Franklins")
            {

            }

            // Empty out the properties
            LodDistance = sceneObject.GetLODDistance();
            ChildEntities = new ObservableCollection<IEntity>();

            // First process any child drawables
            foreach (TargetObjectDef childDef in sceneObject.Children.Where(item => !(item.IsMloRoom() || item.IsMloPortal())))
            {
                if (!childDef.IsObject() || childDef.DontExport() || childDef.DontExportIDE())
                    continue;

                if (childDef.IsRefInternalObject() || childDef.IsRefObject() || childDef.IsXRef() || childDef.IsInternalRef())
                {
                    this.ProcessEntity(childDef);
                    continue;
                }

                IDrawableArchetype drawable = new DrawableArchetype(childDef.Name, ParentSection);
                (drawable as DrawableArchetype).LoadStatsFromExportData(childDef, scene);
                ParentSection.Archetypes.Add(drawable);

                this.ProcessEntity(childDef);
            }

            // Next process the rooms
            Rooms = new ObservableCollection<IRoom>();

            Dictionary<Guid, IRoom> roomLookup = new Dictionary<Guid, IRoom>();

            foreach (TargetObjectDef roomObjectDef in sceneObject.Children.Where(item => item.IsMloRoom()))
            {
                IRoom room = new Room(roomObjectDef.Name, this);
                (room as Room).LoadStatsFromExportData(roomObjectDef, scene);
                Rooms.Add(room);

                roomLookup.Add(roomObjectDef.Guid, room);
            }

            // Next process the portals
            Portals = new ObservableCollection<IPortal>();

            foreach (TargetObjectDef child in sceneObject.Children.Where(item => item.IsMloPortal()))
            {
                Guid guidA = child.GetParameter(ParamNames.MLOPORTAL_FIRST_ROOM, Guid.Empty);
                Guid guidB = child.GetParameter(ParamNames.MLOPORTAL_SECOND_ROOM, Guid.Empty);

                IRoom roomA = null;
                if (roomLookup.ContainsKey(guidA))
                {
                    roomA = roomLookup[guidA];
                }

                IRoom roomB = null;
                if (roomLookup.ContainsKey(guidB))
                {
                    roomB = roomLookup[guidB];
                }

                if (roomA != null || roomB != null)
                {
                    Portals.Add(new Portal(child.Name, this, roomA, roomB));
                }
            }

            // Calculate the bounding box and statistics based on the room's entities
            ComputeBoundingBox();
            GatherStatistics();
        }
        #endregion // IInteriorArchetype Implementation

        #region SimpleMapArchetypeBase Overrides
        /// <summary>
        /// Load specific statistics for this map section
        /// </summary>
        /// <param name="statsToLoad"></param>
        /// <param name="async"></param>
        public override void RequestStatistics(IEnumerable<StreamableStat> statsToLoad, bool async)
        {
            ParentSection.MapHierarchy.RequestStatisticsForInterior(this, statsToLoad, async);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="stat"></param>
        protected override void ProcessComplexArchetypeStat(ArchetypeStatDto dto, StreamableStat stat)
        {
            InteriorArchetypeStatDto interiorDto = dto as InteriorArchetypeStatDto;
            Debug.Assert(interiorDto != null, "Incorrect dto type encountered.");

            if (stat == StreamableInteriorArchetypeStat.Rooms)
            {
                ProcessRoomStats(interiorDto.RoomStats);
            }
            else if (stat == StreamableInteriorArchetypeStat.Portals)
            {
                ProcessPortalStats(interiorDto.PortalStats);
            }
            else if (stat == StreamableInteriorArchetypeStat.Entities)
            {
                ProcessEntityStats(interiorDto.Entities);
            }
            else
            {
                base.ProcessComplexArchetypeStat(dto, stat);
            }
        }
        #endregion // SimpleMapArchetypeBase Overrides

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sceneObjects"></param>
        /// <returns></returns>
        private CategorisedObjects CategoriseSceneObjects(IEnumerable<TargetObjectDef> sceneObjects)
        {
            CategorisedObjects objects = new CategorisedObjects();

            foreach (TargetObjectDef sceneObject in sceneObjects)
            {
                if (sceneObject.IsFragment())
                {
                    if (sceneObject.DrawableLOD != null && (sceneObject.DrawableLOD.Children != null && sceneObject.DrawableLOD.Children.Count() != 0))
                    {
                        continue;
                    }

                    objects.Fragments.Add(sceneObject);
                }
                else if (sceneObject.IsObject())
                {
                    // Determine if this is a map archetype or entity (or both)
                    if (sceneObject.IsXRef() || sceneObject.IsRefObject() || sceneObject.IsInternalRef() || sceneObject.IsRefInternalObject()) // A instance
                    {
                        if (sceneObject.LOD != null && sceneObject.LOD.Parent != null)
                        {
                            continue;
                        }

                        objects.Entities.Add(sceneObject);
                    }
                    else
                    {
                        if (sceneObject.DrawableLOD != null && sceneObject.DrawableLOD.Parent != null)
                        {
                            continue;
                        }

                        objects.Drawables.Add(sceneObject);
                        objects.Entities.Add(sceneObject);
                    }
                }
                else if (sceneObject.IsStatedAnim())
                {
                    if (sceneObject.Attributes.ContainsKey(AttrNames.OBJ_GROUP_NAME))
                    {
                        String animGroup = sceneObject.Attributes[AttrNames.OBJ_GROUP_NAME] as String;
                        if (animGroup != null)
                        {
                            if (!objects.AnimGroups.ContainsKey(animGroup.ToLower()))
                                objects.AnimGroups.Add(animGroup.ToLower(), new List<TargetObjectDef>());

                            objects.AnimGroups[animGroup.ToLower()].Add(sceneObject);
                        }
                    }
                }
            }

            return objects;
        }

        /// <summary>
        /// Helper function to assist in computing the interior's bounding box
        /// </summary>
        private void ComputeBoundingBox()
        {
            BoundingBox3f bbox = new BoundingBox3f();
            
            foreach (IRoom room in Rooms)
            {
                bbox.Expand(room.BoundingBox);
            }
            foreach (IEntity entity in ChildEntities)
            {
                bbox.Expand(entity.BoundingBox);
            }

            BoundingBox = bbox;
        }

        /// <summary>
        /// Helper function used to assist in computing the various stats
        /// </summary>
        private void GatherStatistics()
        {
            Shaders = new IShader[0];
            ExportGeometrySize = 0;
            PolygonCount = 0;
            CollisionPolygonCount = 0;
            CollisionTypePolygonCounts = new Dictionary<CollisionType, uint>();
            TxdExportSizes = new Dictionary<string, uint>();
            HasAttachedLight = false;
            HasExplosiveEffect = false;

            // The interior archetype stats is just the sum of all the "child" room archetype stats and entity stats
            List<IShader> shaders = new List<IShader>();
            
            foreach (IRoom room in Rooms)
            {
                shaders.AddRange(room.Shaders);

                PolygonCount += room.PolygonCount;
                CollisionPolygonCount += room.CollisionPolygonCount;
                ExportGeometrySize += room.ExportGeometrySize;

                foreach (KeyValuePair<CollisionType, uint> pair in room.CollisionTypePolygonCounts)
                {
                    if (!CollisionTypePolygonCounts.ContainsKey(pair.Key))
                    {
                        CollisionTypePolygonCounts.Add(pair.Key, 0);
                    }
                    CollisionTypePolygonCounts[pair.Key] += pair.Value;
                }

                foreach (KeyValuePair<string, uint> txdPair in room.TxdExportSizes)
                {
                    if (!TxdExportSizes.ContainsKey(txdPair.Key))
                    {
                        TxdExportSizes.Add(txdPair);
                    }
                }
            }

            foreach (IEntity entity in ChildEntities.Where(item => item.ReferencedArchetype != null))
            {
                IMapArchetype mapArchetype = (IMapArchetype)entity.ReferencedArchetype;

                shaders.AddRange(mapArchetype.Shaders);

                PolygonCount += mapArchetype.PolygonCount;
                CollisionPolygonCount += mapArchetype.CollisionPolygonCount;
                ExportGeometrySize += mapArchetype.ExportGeometrySize;

                foreach (KeyValuePair<CollisionType, uint> pair in mapArchetype.CollisionTypePolygonCounts)
                {
                    if (!CollisionTypePolygonCounts.ContainsKey(pair.Key))
                    {
                        CollisionTypePolygonCounts.Add(pair.Key, 0);
                    }
                    CollisionTypePolygonCounts[pair.Key] += pair.Value;
                }

                if (entity.IsReference == false)
                {
                    foreach (KeyValuePair<string, uint> txdPair in mapArchetype.TxdExportSizes)
                    {
                        if (!TxdExportSizes.ContainsKey(txdPair.Key))
                        {
                            TxdExportSizes.Add(txdPair);
                        }
                    }
                }
            }

            Shaders = shaders.Distinct().ToArray();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="roomDtos"></param>
        private void ProcessRoomStats(List<BasicRoomStatDto> roomDtos)
        {
            Rooms = new ObservableCollection<IRoom>();
            Rooms.BeginUpdate();

            foreach (BasicRoomStatDto roomDto in roomDtos)
            {
                IRoom room = new Room(roomDto.Name, this);
                Rooms.Add(room);
            }

            Rooms.EndUpdate();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="portalDtos"></param>
        private void ProcessPortalStats(List<PortalStatDto> portalDtos)
        {
            Portals = new ObservableCollection<IPortal>();
            Portals.BeginUpdate();

            Dictionary<uint, IRoom> roomLookup = Rooms.ToDictionary(item => item.Hash);

            foreach (PortalStatDto portalDto in portalDtos)
            {
                IRoom roomA = null;
                if (roomLookup.ContainsKey(portalDto.RoomAIdentifier))
                {
                    roomA = roomLookup[portalDto.RoomAIdentifier];
                }

                IRoom roomB = null;
                if (roomLookup.ContainsKey(portalDto.RoomBIdentifier))
                {
                    roomB = roomLookup[portalDto.RoomBIdentifier];
                }

                if (roomA != null || roomB != null)
                {
                    // TODO: Need to add portal names to the db!
                    Portals.Add(new Portal(portalDto.Name, this, roomA, roomB));
                }
            }

            Portals.EndUpdate();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entityDtos"></param>
        private void ProcessEntityStats(List<BasicEntityStatDto> entityDtos)
        {
            ChildEntities = new ObservableCollection<IEntity>();
            ChildEntities.BeginUpdate();

            lock (ParentSection.MapHierarchy.ArchetypeLookup)
            {
                foreach (BasicEntityStatDto entityDto in entityDtos)
                {
                    if (ParentSection.MapHierarchy.ArchetypeLookup.ContainsKey(entityDto.ArchetypeName.ToLower()))
                    {
                        IArchetype archetype = ParentSection.MapHierarchy.ArchetypeLookup[entityDto.ArchetypeName.ToLower()];
                        IEntity entity = new Entity(entityDto.Name, archetype, this);
                    }
                    else
                    {
                        Log.Log__Warning("Archetype '{0}' wasn't found in the archetype look up for entity '{1}'.", entityDto.ArchetypeName, entityDto.Name);
                    }
                }
            }

            ChildEntities.EndUpdate();
        }
        #endregion // Private Methods

        #region Object Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return (String.Format("Interior Archetype: [{0}]{1}", this.Name, this.Hash));
        }
        #endregion // Object Overrides
    } // InteriorArchetype
}
