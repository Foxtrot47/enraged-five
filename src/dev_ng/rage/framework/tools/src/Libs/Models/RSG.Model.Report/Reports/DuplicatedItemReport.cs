﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.ConfigParser;
using RSG.Model.Map;
using System.IO;
using System.Web.UI;
using RSG.Model.Common;

namespace RSG.Model.Report.Reports
{
    /// <summary>
    /// 
    /// </summary>
    public class DuplicatedItemReport : HTMLReport, IDynamicMapReport
    {
        #region Constants
        private const String NAME = "Duplicated Item Report";
        private const String DESCRIPTION = "Generate and display a duplicated item report.";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Determines whether the map data should be loaded
        /// </summary>
        public bool LoadMapData
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Determines whether the car generators should be loaded
        /// 
        /// </summary>
        public bool LoadCarGenarators
        {
            get { return false; }
        }
        #endregion // Properties

        #region Construtcor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public DuplicatedItemReport()
            : base(NAME, DESCRIPTION)
        {
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        /// <param name="gv"></param>
        public void Generate(ILevel level, ConfigGameView gv, IPlatformCollection platforms)
        {
            GenerateDuplicatedItemReport(level, gv);
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        /// <param name="gv"></param>
        public void GenerateDuplicatedItemReport(ILevel level, ConfigGameView gv)
        {
            RSG.Model.Map.Util.Creation.InitialiseLevelStatistics(level, gv);

            var textureDictionaries = new Dictionary<string, List<string>>();
            var definitions = new Dictionary<string, List<string>>();
            foreach (MapSection section in RSG.Model.Map.Util.Enumerations.GetAllMapSections(level, null, true))
            {
                foreach (var txdName in section.UniqueTextureDictionaryNames())
                {
                    if (!textureDictionaries.ContainsKey(txdName))
                        textureDictionaries.Add(txdName, new List<string>());
                    if (!textureDictionaries[txdName].Contains(section.Name))
                        textureDictionaries[txdName].Add(section.Name);
                }
                foreach (var definitionName in section.UniqueDefinitionNames())
                {
                    if (!definitions.ContainsKey(definitionName))
                        definitions.Add(definitionName, new List<string>());
                    if (!definitions[definitionName].Contains(section.Name))
                        definitions[definitionName].Add(section.Name);
                }
            }

            var dupTextureDictionaries = new Dictionary<string, List<string>>();
            var dupDefinitions = new Dictionary<string, List<string>>();
            foreach (var txd in textureDictionaries)
            {
                if (txd.Value.Count > 1 && txd.Key != "Missing TXD Attribute")
                {
                    dupTextureDictionaries.Add(txd.Key, txd.Value);
                }
            }
            foreach (var definition in definitions)
            {
                if (definition.Value.Count > 1)
                {
                    dupDefinitions.Add(definition.Key, definition.Value);
                }
            }

            MemoryStream stream = new MemoryStream();
            StreamWriter reportFile = new StreamWriter(stream);

            HtmlTextWriter writer = new HtmlTextWriter(reportFile);
            writer.RenderBeginTag(HtmlTextWriterTag.Html);
            writer.RenderBeginTag(HtmlTextWriterTag.Head);
            writer.RenderEndTag();
            writer.RenderBeginTag(HtmlTextWriterTag.Body);

            if (dupTextureDictionaries.Count > 0)
            {
                writer.RenderBeginTag(HtmlTextWriterTag.H1);
                writer.Write("Duplicated Texture Dictionaries:");
                writer.RenderEndTag();

                foreach (var dup in dupTextureDictionaries)
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.H2);
                    writer.Write("{0}", dup.Key);
                    writer.RenderEndTag();

                    writer.AddAttribute(HtmlTextWriterAttribute.Border, "1");
                    writer.AddAttribute(HtmlTextWriterAttribute.Width, "40%");
                    writer.RenderBeginTag(HtmlTextWriterTag.Table);

                    writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                    writer.RenderBeginTag(HtmlTextWriterTag.Th);
                    writer.Write("Sections");
                    writer.RenderEndTag();
                    foreach (var txd in dup.Value)
                    {
                        writer.RenderBeginTag(HtmlTextWriterTag.Tr);

                        writer.RenderBeginTag(HtmlTextWriterTag.Td);
                        writer.Write(String.Format("{0}", txd));
                        writer.RenderEndTag();

                        writer.RenderEndTag();
                    }
                    writer.RenderEndTag();
                    writer.RenderEndTag();
                }
            }
            else
            {
                writer.RenderBeginTag(HtmlTextWriterTag.H2);
                writer.Write("No Duplicated Texture Dictionaries");
                writer.RenderEndTag();
            }

            if (dupDefinitions.Count > 0)
            {
                writer.RenderBeginTag(HtmlTextWriterTag.H1);
                writer.Write("Duplicated Map Objects:");
                writer.RenderEndTag();

                foreach (var dup in dupDefinitions)
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.H2);
                    writer.Write("{0}", dup.Key);
                    writer.RenderEndTag();

                    writer.AddAttribute(HtmlTextWriterAttribute.Border, "1");
                    writer.AddAttribute(HtmlTextWriterAttribute.Width, "40%");
                    writer.RenderBeginTag(HtmlTextWriterTag.Table);

                    writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                    writer.RenderBeginTag(HtmlTextWriterTag.Th);
                    writer.Write("Sections");
                    writer.RenderEndTag();
                    foreach (var def in dup.Value)
                    {
                        writer.RenderBeginTag(HtmlTextWriterTag.Tr);

                        writer.RenderBeginTag(HtmlTextWriterTag.Td);
                        writer.Write(String.Format("{0}", def));
                        writer.RenderEndTag();

                        writer.RenderEndTag();
                    }
                    writer.RenderEndTag();
                    writer.RenderEndTag();
                }
            }
            else
            {
                writer.RenderBeginTag(HtmlTextWriterTag.H2);
                writer.Write("No Duplicated Map Objects");
                writer.RenderEndTag();
            }

            writer.RenderEndTag();
            writer.RenderEndTag();
            writer.Flush();

            stream.Position = 0;
            Stream = stream;
        }
        #endregion // Private Methods
    } // DuplicatedItemReport
} // RSG.Model.Report.Reports
