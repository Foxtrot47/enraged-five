﻿using System;
using System.Collections.Generic;
using RSG.Model.Common;
using RSG.SourceControl.Perforce;

namespace RSG.Model.Perforce
{

    /// <summary>
    /// Perforce revision object.
    /// </summary>
    public class Revision : AssetBase
    {
    }

} // RSG.Model.Perforce namespace
