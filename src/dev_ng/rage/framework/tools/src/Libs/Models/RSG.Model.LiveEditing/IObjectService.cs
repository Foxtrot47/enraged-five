﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;

namespace RSG.Model.LiveEditing
{
    /// <summary>
    /// 
    /// </summary>
    public interface IObjectService : IAsset
    {
        /// <summary>
        /// The Uri to get information about this item
        /// </summary>
        Uri Url { get; }

        /// <summary>
        /// The file path for this particular service
        /// </summary>
        string AssociatedFilePath { get; }
    } // IObjectService
}
