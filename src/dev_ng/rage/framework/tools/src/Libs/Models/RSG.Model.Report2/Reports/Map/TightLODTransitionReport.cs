﻿namespace RSG.Model.Report2.Reports.Map
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using RSG.Base.ConfigParser;
    using RSG.Base.Tasks;
    using Base.Math;
    using RSG.Model.Common;
    using RSG.Model.Common.Map;
    using RSG.Model.Report;
    using RSG.ManagedRage;
    using RSG.Platform;
    using RSG.Model.Map.SceneOverride;

    /// <summary>
    /// A report that finds potential issues with distances between LOD levels that are too short
    /// </summary>
    public class TightLODTransitionReport : CSVReport, IDynamicLevelReport
    {
        #region Fields
        /// <summary>
        /// The name of this report.
        /// </summary>
        private const string Name = "Tight LOD Transition";

        /// <summary>
        /// The description for this report.
        /// </summary>
        private const string Description =
            "Exports a csv report that lists all lists potential LOD transition issues.";

        /// <summary>
        /// The private field used for the <see cref="GenerationTask"/> property.
        /// </summary>
        private ITask _generationTask;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="InvalidEntityReport"/> class.
        /// </summary>
        public TightLODTransitionReport()
            : base(Name, Description)
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the task that represents the work needing to be done to generate this report.
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (this._generationTask == null)
                {
                    CompositeTask task = new CompositeTask("Generate Report");
                    task.AddSubTask(new ActionTask("Loading data", this.EnsureDataLoaded));
                    task.AddSubTask(new ActionTask("Generating report", this.GenerateReport));
                    this._generationTask = task;
                }

                return this._generationTask;
            }
        }

        /// <summary>
        /// Gets a iterator around the stats that are required to generate this report. Always
        /// returns null.
        /// </summary>
        public IEnumerable<StreamableStat> RequiredStats
        {
            get { return null; }
        }
        #endregion Properties

        #region Private Classes
        /// <summary>
        /// This class represents a collection of LOD siblings that have the 'Low Priority' flag set
        /// but also have siblings that don't have the 'Low Priority' flag set.
        /// </summary>
        private class TightLODTransition
        {
            internal TightLODTransition(
                IEntity entity, 
                IEntity parentEntity, 
                IEntity[] childEntities, 
                float lodDistance, 
                float transitionDistance, 
                float offsetFromParent, 
                float effectiveTransitionDistance)
            {
                Entity = entity;
                ParentEntity = parentEntity;
                ChildEntities = childEntities;
                LODDistance = lodDistance;
                TransitionDistance = transitionDistance;
                OffsetFromParent = offsetFromParent;
                EffectiveTransitionDistance = effectiveTransitionDistance;
            }

            internal IEntity Entity { get; private set; }
            internal IEntity ParentEntity { get; private set; }
            internal IEntity[] ChildEntities { get; private set; }
            internal float LODDistance { get; private set; }
            internal float TransitionDistance { get; private set; }
            internal float OffsetFromParent { get; private set; }
            internal float EffectiveTransitionDistance { get; private set; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Ensures that the data needed for this report is loaded.
        /// </summary>
        /// <param name="context">
        /// The task context for this action.
        /// </param>
        /// <param name="progress">
        /// The object used to notify a listener of the progress.
        /// </param>
        private void EnsureDataLoaded(ITaskContext context, IProgress<TaskProgress> progress)
        {
            DynamicLevelReportContext reportContext = context as DynamicLevelReportContext;
            Debug.Assert(reportContext != null, "Unable to load data for report");
            if (reportContext == null)
            {
                return;
            }

            IMapHierarchy hierarchy = reportContext.Level.MapHierarchy;
            Debug.Assert(hierarchy != null, "Unable to load data for report");
            if (hierarchy == null || hierarchy.AllSections == null)
            {
                return;
            }

            List<IMapSection> mapSections = hierarchy.AllSections.ToList();
            float increment = 1.0f / mapSections.Count;
            string fmt = "Loading {0}";
            int index = 1;
            foreach (IMapSection section in mapSections)
            {
                Debug.WriteLine("{0} of {1}", index++, mapSections.Count);
                context.Token.ThrowIfCancellationRequested();
                string message = String.Format(fmt, section.Name);
                progress.Report(new TaskProgress(increment, true, message));

                section.LoadStats(new List<StreamableStat>() { StreamableMapSectionStat.Archetypes, StreamableMapSectionStat.Entities });
            }
        }

        /// <summary>
        /// Generates the report and saves the file associated with it.
        /// </summary>
        /// <param name="context">
        /// The task context for this action.
        /// </param>
        /// <param name="progress">
        /// The object used to notify a listener of the progress.
        /// </param>
        private void GenerateReport(ITaskContext context, IProgress<TaskProgress> progress)
        {
            DynamicLevelReportContext reportContext = context as DynamicLevelReportContext;
            Debug.Assert(reportContext != null, "Unable to generate report");
            if (reportContext == null)
            {
                return;
            }

            IMapHierarchy hierarchy = reportContext.Level.MapHierarchy;
            Debug.Assert(hierarchy != null, "Unable to generate report");
            if (hierarchy == null || hierarchy.AllSections == null)
            {
                return;
            }

            List<TightLODTransition> tightLODTransitions = new List<TightLODTransition>();
            foreach (IMapSection section in hierarchy.AllSections.Where(sec => sec.ChildEntities != null))
            {
                foreach (IEntity entity in section.ChildEntities.Where(en => en.LodLevel == LodLevel.Lod && en.LodChildren.Count > 0 && en.LodDistanceForChildren.HasValue))
                {
                    float lodDistance = entity.LodParent != null ? entity.LodParent.LodDistanceForChildren.Value : entity.LodDistance;
                    float transitionDistance = entity.LodDistance - entity.LodDistanceForChildren.Value;
                    float offsetFromParent = 0.0f;
                    if (entity.LodParent != null)
                        offsetFromParent = (float)entity.LodParent.Position.Dist(entity.Position);
                    float effectiveTransitionDistance = transitionDistance - offsetFromParent;

                    if (transitionDistance < minimumTransitionDistance_ || effectiveTransitionDistance < minimumTransitionDistance_)
                    {
                        IEntity[] childEntities = entity.LodChildren.ToArray();

                        tightLODTransitions.Add(
                            new TightLODTransition(entity, entity.LodParent, childEntities, lodDistance, transitionDistance, offsetFromParent, effectiveTransitionDistance));
                    }
                }
            }

            this.WriteCsvFile(tightLODTransitions);
        }

        private void WriteCsvFile(IEnumerable<TightLODTransition> tightLODTransitions)
        {
            using (StreamWriter writer = new StreamWriter(this.Filename))
            {
                writer.WriteLine(
                    "Section Name," +
                    "Name," +
                    "LOD Level," + 
                    "LOD Distance," +
                    "Child LOD Distance," +
                    "Transition Distance," +
                    "Offset from Parent," +
                    "Effective Transition Distance," +
                    "Parent," +
                    "Children");

                WriteCsvData(writer, tightLODTransitions);
            }
        }

        private void WriteCsvData(StreamWriter writer, IEnumerable<TightLODTransition> tightLODTransitions)
        {
            foreach (TightLODTransition tightLODTransition in tightLODTransitions)
            {
                String parentName = tightLODTransition.ParentEntity != null ? tightLODTransition.ParentEntity.Name : String.Empty;
                writer.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}",
                    tightLODTransition.Entity.ContainingSection.Name,
                    tightLODTransition.Entity.Name,
                    tightLODTransition.Entity.LodLevel.ToString(),
                    tightLODTransition.LODDistance.ToString("0.00"),
                    tightLODTransition.Entity.LodDistanceForChildren.Value.ToString("0.00"),
                    tightLODTransition.TransitionDistance.ToString("0.00"),
                    tightLODTransition.OffsetFromParent.ToString("0.00"),
                    tightLODTransition.EffectiveTransitionDistance.ToString("0.00"),
                    parentName,
                    String.Join(" ; ", tightLODTransition.ChildEntities.Select(en => en.Name)));
            }
        }
        #endregion Methods

        #region Constant Data
        private const float minimumTransitionDistance_ = 101;
        #endregion
    }
}
