﻿using System;

namespace RSG.Model.Report
{
    /// <summary>
    /// 
    /// </summary>
    public interface IReportUriProvider
    {
        /// <summary>
        /// Uri of the report.
        /// </summary>
        Uri Uri
        {
            get;
        }
    } // IReportUri
} // RSG.Model.Report
