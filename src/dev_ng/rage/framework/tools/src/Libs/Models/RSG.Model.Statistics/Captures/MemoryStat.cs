﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace RSG.Model.Statistics.Captures
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class MemoryStat
    {
        #region Properties
        /// <summary>
        /// Minimum value recorded for this memory result.
        /// </summary>
        [DataMember]
        public uint Min { get; private set; }

        /// <summary>
        /// Maximum value recorded for this memory result.
        /// </summary>
        [DataMember]
        public uint Max { get; private set; }

        /// <summary>
        /// Average value recorded for this memory result.
        /// </summary>
        [DataMember]
        public uint Average { get; private set; }

        /// <summary>
        /// Standard deviation for this memory result.
        /// </summary>
        [DataMember]
        public float StandardDeviation { get; private set; }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <param name="average"></param>
        public MemoryStat(uint min, uint max, uint average, float std)
        {
            Min = min;
            Max = max;
            Average = average;
            StandardDeviation = std;
        }
        #endregion // Constructor(s)
    } // MemoryStat
}
