﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RSG.Model.Common
{
    /// <summary>
    /// Interface for any object that want to contain
    /// a collection of assets
    /// </summary>
    public interface IHasAssetChildren : IAsset
    {
        #region Properties

        /// <summary>
        /// The collection of assets belonging to this container
        /// </summary>
        RSG.Base.Collections.ObservableCollection<IAsset> AssetChildren { get; }

        #endregion // Properties

        #region Methods
        
        /// <summary>
        /// Returns true if this container contains a asset in it
        /// with the given name
        /// </summary>
        Boolean ContainsAsset(String name);

        #endregion // Methods
    } // IHasAssetChildren
} // RSG.Model.Common
