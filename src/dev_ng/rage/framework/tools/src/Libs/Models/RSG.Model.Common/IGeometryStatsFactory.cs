﻿using System;

namespace RSG.Model.Common
{
    public interface IGeometryStatsFactory
    {
        IGeometryStatsCollection CreateFromFile(string pathname);
    }
}
