﻿using System;
using System.Collections.Generic;

namespace RSG.Model.Common
{
    /// <summary>
    /// Interface for character assets
    /// </summary>
    public interface ICharacter : IAsset, IHasAssetChildren, IComparable<ICharacter>, IEquatable<ICharacter>, IStreamableObject
    {
        #region Properties
        /// <summary>
        /// The collection this character is a part of
        /// </summary>
        ICharacterCollection ParentCollection { get; }

        /// <summary>
        /// Character category classification
        /// </summary>
        CharacterCategory Category { get; }

        /// <summary>
        /// Array of textures the character uses
        /// </summary>
        ITexture[] Textures { get; }

        /// <summary>
        /// Array of character shaders.
        /// </summary>
        IShader[] Shaders { get; }
        
        /// <summary>
        /// 
        /// </summary>
        int PolyCount { get; }

        /// <summary>
        /// 
        /// </summary>
        int CollisionCount { get; }

        /// <summary>
        /// 
        /// </summary>
        int BoneCount { get; }

        /// <summary>
        /// 
        /// </summary>
        int ClothCount { get; }

        /// <summary>
        /// 
        /// </summary>
        int ClothPolyCount { get; }

        /// <summary>
        /// 
        /// </summary>
        IDictionary<RSG.Platform.Platform, ICharacterPlatformStat> PlatformStats { get; }
        #endregion // Properties
    } // ICharacter

    /// <summary>
    /// Interface for character based platform stats
    /// </summary>
    public interface ICharacterPlatformStat
    {
        /// <summary>
        /// Platform for this stat
        /// </summary>
        RSG.Platform.Platform Platform { get; }

        /// <summary>
        /// PLysical size of the character
        /// </summary>
        uint PhysicalSize { get; }

        /// <summary>
        /// Virtual size of the character
        /// </summary>
        uint VirtualSize { get; }
    } // ICharacterPlatformStat
} // RSG.Model.Common
