﻿// --------------------------------------------------------------------------------------------
// <copyright file="ShortMember.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions.Members
{
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using RSG.Base;
    using RSG.Base.Extensions;
    using RSG.Base.Logging;
    using RSG.Metadata.Model.Resources;
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// Represents the &lt;short&gt; member node in the parCodeGen system that can be instanced
    /// in a metadata file.
    /// </summary>
    public class ShortMember : BaseIntegerMember<short>, IEquatable<ShortMember>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ShortMember"/> class to be one of the
        /// members of the specified structure.
        /// </summary>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        public ShortMember(IStructure structure)
            : base(structure)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ShortMember"/> class as a copy of the
        /// specified instance.
        /// </summary>
        /// <param name="other">
        /// The instance to copy.
        /// </param>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        public ShortMember(ShortMember other, IStructure structure)
            : base(other, structure)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ShortMember"/> class.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        public ShortMember(XmlReader reader, IStructure structure)
            : base(reader, structure)
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a string that represents the content type of this scalar member.
        /// </summary>
        public override string ScalarContent
        {
            get { return "short"; }
        }

        /// <summary>
        /// Gets the type name for this member.
        /// </summary>
        public override string TypeName
        {
            get { return "short"; }
        }

        /// <summary>
        /// Gets the default initial value for the integer type.
        /// </summary>
        protected override short DefaultInitialValue
        {
            get { return default(short); }
        }

        /// <summary>
        /// Gets the default maximum value for the integer type.
        /// </summary>
        protected override short DefaultMaximumValue
        {
            get { return short.MaxValue; }
        }

        /// <summary>
        /// Gets the default minimum value for the integer type.
        /// </summary>
        protected override short DefaultMinimumValue
        {
            get { return short.MinValue; }
        }

        /// <summary>
        /// Gets the default step value for the integer type.
        /// </summary>
        protected override short DefaultStepValue
        {
            get { return 1; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new <see cref="ShortMember"/> that is a copy of this instance.
        /// </summary>
        /// <returns>
        /// A new <see cref="ShortMember"/> that is a copy of this instance.
        /// </returns>
        public new ShortMember Clone()
        {
            return new ShortMember(this, this.Structure);
        }

        /// <summary>
        /// Creates a <see cref="ITunable"/> object that represents a instance of this member
        /// with it's default value set.
        /// </summary>
        /// <param name="parent">
        /// The tunable parent the new tunable will belong to.
        /// </param>
        /// <returns>
        /// A parCodeGen tunable that represents a instance of this member.
        /// </returns>
        public override ITunable CreateTunable(ITunableParent parent)
        {
            return new ShortTunable(this, parent);
        }

        /// <summary>
        /// Creates a <see cref="ITunable"/> object that represents a instance of this member
        /// using the specified string components to initialise it.
        /// </summary>
        /// <param name="components">
        /// The string components that provides the data used to initialise the new tunable.
        /// </param>
        /// <param name="parent">
        /// The tunable parent the new tunable will belong to.
        /// </param>
        /// <returns>
        /// A parCodeGen tunable that represents a instance of this member.
        /// </returns>
        public override ITunable CreateTunable(List<string> components, ITunableParent parent)
        {
            if (components.Count != 1)
            {
                string msg = StringTable.IntegerComponentCountError;
                msg = msg.FormatInvariant("short", components.Count.ToString());
                throw new InvalidOperationException(msg);
            }

            ShortTunable tunable = new ShortTunable(this, parent);
            TryResult<short> value = this.Dictionary.TryTo(components[0], default(short));
            if (!value.Success)
            {
                string msg = StringTable.IntegerParseError;
                msg = msg.FormatInvariant("short", components[0]);
                throw new InvalidOperationException(msg);
            }

            tunable.Value = value.Value;
            return tunable;
        }

        /// <summary>
        /// Creates a <see cref="ITunable"/> object that represents a instance of this member
        /// using the specified Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The Xml.XmlReader that provides the data used to initialise the new tunable.
        /// </param>
        /// <param name="parent">
        /// The tunable parent the new tunable will belong to.
        /// </param>
        /// <param name="log">
        /// A object that is used to log any warnings or errors produced during the parsing of
        /// the specified reader.
        /// </param>
        /// <returns>
        /// A parCodeGen tunable that represents a instance of this member.
        /// </returns>
        public override ITunable CreateTunable(
            XmlReader reader, ITunableParent parent, ILog log)
        {
            return new ShortTunable(reader, this, parent, log);
        }

        /// <summary>
        /// Creates a deep copy of the current instance.
        /// </summary>
        /// <returns>
        /// A deep copy of the current System.Object.
        /// </returns>
        public override object DeepClone()
        {
            return this.Clone();
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified
        /// <see cref="ShortMember"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public bool Equals(ShortMember other)
        {
            if (other == null)
            {
                return false;
            }

            return object.ReferenceEquals(other, this);
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="IMember"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public override bool Equals(IMember other)
        {
            return this.Equals(other as ShortMember);
        }
        #endregion Methods
    } // RSG.Metadata.Model.Definitions.Members.ShortMember {Class}
} // RSG.Metadata.Model.Definitions.Members {Namespace}
