﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.Model.Map;
using RSG.ManagedRage;
using RSG.Base.Extensions;
using RSG.Base.ConfigParser;
using RSG.Platform;
using RSG.Base.Logging;

namespace RSG.Model.Report.Reports
{
    /// <summary>
    /// 
    /// </summary>
    public class VehicleCSVReport : CSVReport, IDynamicMapReport
    {        
        #region Constants
        private const String NAME = "Export Vehicle CSV Report";
        private const String DESCRIPTION = "Exports the selected level vehicles into a .csv file"; 
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Determines whether the map data should be loaded
        /// </summary>
        public bool LoadMapData
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Determines whether the car generators should be loaded
        /// 
        /// </summary>
        public bool LoadCarGenarators
        {
            get { return false; }
        }
        #endregion // Properties

        #region Construtcor(s)

        /// <summary>
        /// Constructor.
        /// </summary>
        public VehicleCSVReport()
            : base(NAME, DESCRIPTION)
        {
        }

        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Generate the report dynamically.
        /// </summary>
        /// <param name="level"></param>
        /// <param name="gv"></param>
        public void Generate(ILevel level, ConfigGameView gv, IPlatformCollection platforms)
        {
            DateTime start = DateTime.Now;
            ExportVehicleCSVReport(level, gv, platforms);
            Log.Log__Message("Vehicle CSV export complete. (report took {0} milliseconds to create.)", (DateTime.Now - start).TotalMilliseconds);
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        /// <param name="gv"></param>
        private void ExportVehicleCSVReport(ILevel level, ConfigGameView gv, IPlatformCollection platforms)
        {
            using (StreamWriter sw = new StreamWriter(Filename))
            {
                WriteCsvHeader(sw, platforms);
                IAsset vehicleContainer = null;
                foreach (IAsset asset in level.AssetChildren)
                {
                    if (0 == String.Compare("vehicles", asset.Name, true))
                    {
                        vehicleContainer = asset;
                        break;
                    }
                }
                if (null != vehicleContainer)
                {
                    System.Diagnostics.Debug.Assert(vehicleContainer is IHasAssetChildren);
                    foreach (IAsset asset in (vehicleContainer as IHasAssetChildren).AssetChildren)
                    {
                        IVehicle vehicle = asset as IVehicle;

                        // Skip all non-Vehicle assets
                        if (vehicle != null)
                        {
                            // Ensure that all the data is loaded
                            vehicle.RequestAllStatistics(true, false);

                            // Generate the csv line
                            string scvRecord = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19}",
                                vehicle.Name, vehicle.Category.ToString(), vehicle.PolyCount, vehicle.CollisionCount,
                                vehicle.Textures.Count(), vehicle.Shaders.Count(),
                                vehicle.BoneCount, vehicle.DoorCount, vehicle.SeatCount, vehicle.WheelCount, vehicle.ExtraCount,
                                vehicle.PropellerCount, vehicle.RudderCount,
                                vehicle.LightCount, vehicle.RotorCount, vehicle.ElevatorCount, vehicle.HandlebarCount,
                                vehicle.HasLod1, vehicle.HasLod2,
                                GenerateMemoryDetails(vehicle, platforms));
                            sw.WriteLine(scvRecord);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sw"></param>
        /// <param name="milo"></param>
        private void WriteCsvHeader(StreamWriter sw, IPlatformCollection platforms)
        {
            string header = "Name,Type,Poly Count,Collision Count,Texture Count,Shader Count,Bone Count,Door Count,Seat Count,Wheel Count,Extra Count,Propeller Count,Rudder Count,Light Count,Rotor Count,Elevator Count,Handlebars Count,Has LOD 1,Has LOD 2";

            foreach (IPlatform platform in platforms)
            {
                header += String.Format(",{0} Physical Size,{0} Virtual Size,{0} Total Size", platform.Name);
            }

            foreach (IPlatform platform in platforms)
            {
                header += String.Format(",{0} Hi Physical Size,{0} Hi Virtual Size,{0} Hi Total Size", platform.Name);
            }

            sw.WriteLine(header);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vehicle"></param>
        /// <param name="platforms"></param>
        /// <returns></returns>
        private string GenerateMemoryDetails(IVehicle vehicle, IPlatformCollection platforms)
        {
            StringBuilder details = new StringBuilder("");

            foreach (IPlatform platform in platforms)
            {
                IVehiclePlatformStat stat = vehicle.GetPlatformStat(platform.Platform);

                if (stat != null)
                {
                    details.Append(String.Format("{0},{1},{2},", stat.PhysicalSize, stat.VirtualSize, (stat.PhysicalSize + stat.VirtualSize)));
                }
                else
                {
                    details.Append("n/a,n/a,n/a,");
                }
            }

            foreach (IPlatform platform in platforms)
            {
                IVehiclePlatformStat stat = vehicle.GetPlatformStat(platform.Platform);

                if (stat != null)
                {
                    details.Append(String.Format("{0},{1},{2},",stat.HiPhysicalSize, stat.HiVirtualSize, (stat.HiPhysicalSize + stat.HiVirtualSize)));
                }
                else
                {
                    details.Append("n/a,n/a,n/a,");
                }
            }

            return details.ToString().TrimEnd(new char[] { ',' });
        }
        #endregion // Private Methods
    } // VehicleCSVReport
} // RSG.Model.Report.Reports
