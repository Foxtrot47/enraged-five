﻿using System;
using System.IO;
using Ionic.Zip;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.Base.ConfigParser;
using RSG.SceneXml;
using RSG.Base.Math;
using RSG.SceneXml.Material;
using RSG.SceneXml.Statistics;
using RSG.Model.Map.Statistics;
using RSG.Model.Map.Util;
using RSG.Model.GlobalTXD;
using RSG.Model.Asset;
using RSG.Model.Common.Map;

namespace RSG.Model.Map
{
    /// <summary>
    /// Represent a non/generic map object that can be instanced to be
    /// displayed in the game world
    /// </summary>
    public class MapDefinition : FileAssetContainerBase, IMapObject, IMapContainer, IHasTextureChildren, IHasAssetChildren
    {
        #region Members

        private ILevel m_level;
        private IMapContainer m_container;
        private BoundingBox3f m_boundingBox;
        private Dictionary<int, Object> m_attributes;
        private Boolean m_isMilo;
        private float m_lodDistance;
        private int m_exportGeometrySize = 0;
        private uint m_exportTxdSize = 0;
        private int m_polygonCount = 0;
        private int m_collisionPolygonCount = 0;
        private int m_moverPolygonCount = 0;
        private int m_weaponsPolygonCount = 0;
        private int m_cameraPolygonCount = 0;
        private int m_riverPolygonCount = 0;
        private List<MapInstance> m_instances = new List<MapInstance>();
        private List<IShader> m_shaders = new List<IShader>();
        private List<MapInstance> m_isolatedInstances;
        private bool m_hasAttachedLight;
        private bool m_hasExplosiveEffect;

        #endregion // Members

        #region Properties

        /// <summary>
        /// Reference to the owner Level object.
        /// </summary>
        public ILevel Level
        {
            get { return m_level; }
            set
            {
                if (m_level == value)
                    return;

                SetPropertyValue(value, () => Level,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_level = (ILevel)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// The immediate parent map container, or
        /// null if non exist
        /// </summary>
        public IMapContainer Container
        {
            get { return m_container; }
            set
            {
                if (m_container == value)
                    return;

                SetPropertyValue(value, () => Container,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_container = (IMapContainer)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// The tightest box surrounding the map
        /// object, used for density statistics
        /// </summary>
        public BoundingBox3f BoundingBox
        {
            get { return m_boundingBox; }
            set
            {
                if (m_boundingBox == value)
                    return;

                SetPropertyValue(value, () => BoundingBox,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_boundingBox = (BoundingBox3f)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// The dictionary of attributes on this definition
        /// </summary>
        private Dictionary<int, Object> Attributes
        {
            get { return m_attributes; }
            set
            {
                if (m_attributes == value)
                    return;

                SetPropertyValue(value, () => Attributes,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_attributes = (Dictionary<int, Object>)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// The name of the texture dictionary this
        /// definitions textures belong to
        /// </summary>
        public String TextureDictionaryName
        {
            get
            {
                return this.GetAttributeValue(AttrNames.OBJ_TXD, "Missing TXD Attribute");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int TextureDictionaryCount
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public string CollisionGroupName
        {
            get
            {
                if (this.Container is MapSection && (this.Container as MapSection).ExportInstances == true)
                {
                    return AttrDefaults.OBJ_COLLISION_GROUP;
                }
                string group = this.GetAttributeValue(AttrNames.OBJ_COLLISION_GROUP, AttrDefaults.OBJ_COLLISION_GROUP);
                if (group == AttrDefaults.OBJ_COLLISION_GROUP)
                {
                    if (this.Container is MapSection && (this.Container as MapSection).ExportInstances == false)
                    {
                        group = this.Container.Name;
                    }
                }
                return group;
            }
        }

        public bool HasDefaultCollisionGroup
        {
            get
            {
                if (this.CollisionGroupName == AttrDefaults.OBJ_COLLISION_GROUP)
                    return true;

                return false;
            }
        }

        /// <summary>
        /// Flag that is set if this map definition is infact
        /// a milo interior
        /// </summary>
        public Boolean IsMilo
        {
            get { return m_isMilo; }
            set
            {
                if (m_isMilo == value)
                    return;

                SetPropertyValue(value, () => IsMilo,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_isMilo = (Boolean)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Boolean IsDynamic
        {
            get { return GetAttributeValue(AttrNames.OBJ_IS_DYNAMIC, false); }
        }

        /// <summary>
        /// 
        /// </summary>
        public Boolean IsCloth
        {
            get { return GetAttributeValue(AttrNames.OBJ_IS_CLOTH, AttrDefaults.OBJ_IS_CLOTH); }
        }

        /// <summary>
        /// The lod distance for this definition
        /// </summary>
        public float LodDistance
        {
            get { return m_lodDistance; }
            set
            {
                if (m_lodDistance == value)
                    return;

                SetPropertyValue(value, () => LodDistance,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_lodDistance = (float)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// The size in bytes of the definitions idr.zip file
        /// that is produce during the map export
        /// </summary>
        public int ExportGeometrySize
        {
            get { return m_exportGeometrySize; }
            set
            {
                if (m_exportGeometrySize == value)
                    return;

                SetPropertyValue(value, () => ExportGeometrySize,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_exportGeometrySize = (int)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// The size in bytes of the definitions txd itd.zip file
        /// that is produce during the map export
        /// </summary>
        public uint ExportTxdSize
        {
            get { return m_exportTxdSize; }
            set
            {
                if (m_exportTxdSize == value)
                    return;

                SetPropertyValue(value, () => ExportTxdSize,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_exportTxdSize = (uint)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// The number of polygons this definition is
        /// made up of
        /// </summary>
        public int PolygonCount
        {
            get { return m_polygonCount; }
            set
            {
                if (m_polygonCount == value)
                    return;

                SetPropertyValue(value, () => PolygonCount,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_polygonCount = (int)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// The number of polygons this definitions collision
        /// is made up of.
        /// </summary>
        public int CollisionPolygonCount
        {
            get { return m_collisionPolygonCount; }
            set
            {
                if (m_collisionPolygonCount == value)
                    return;

                SetPropertyValue(value, () => CollisionPolygonCount,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_collisionPolygonCount = (int)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// The number of polygons this definitions collision
        /// is made up of that is set as mover collision.
        /// </summary>
        public int MoverPolygonCount
        {
            get { return m_moverPolygonCount; }
            set
            {
                if (m_moverPolygonCount == value)
                    return;

                SetPropertyValue(value, () => MoverPolygonCount,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_moverPolygonCount = (int)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// The number of polygons this definitions collision
        /// is made up of that is set as mover collision.
        /// </summary>
        public int WeaponsPolygonCount
        {
            get { return m_weaponsPolygonCount; }
            set
            {
                if (m_weaponsPolygonCount == value)
                    return;

                SetPropertyValue(value, () => WeaponsPolygonCount,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_weaponsPolygonCount = (int)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// The number of polygons this definitions collision
        /// is made up of that is set as camera collision.
        /// </summary>
        public int CameraPolygonCount
        {
            get { return m_cameraPolygonCount; }
            set
            {
                if (m_cameraPolygonCount == value)
                    return;

                SetPropertyValue(value, () => CameraPolygonCount,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_cameraPolygonCount = (int)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// The number of polygons this definitions collision
        /// is made up of that is set as river collision.
        /// </summary>
        public int RiverPolygonCount
        {
            get { return m_riverPolygonCount; }
            set
            {
                if (m_riverPolygonCount == value)
                    return;

                SetPropertyValue(value, () => RiverPolygonCount,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_riverPolygonCount = (int)newValue;
                        }
                ));
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        public List<MapInstance> Instances
        {
            get { return m_instances; }
            set { m_instances = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public List<IShader> Shaders
        {
            get { return m_shaders; }
            set
            {
                if (m_shaders == value)
                    return;

                SetPropertyValue(value, () => Shaders,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_shaders = (List<IShader>)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<ITexture> Textures
        {
            get
            {
                return this.AssetChildren.OfType<ITexture>();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public List<MapInstance> IsolatedInstances
        {
            get { return m_isolatedInstances; }
            set { m_isolatedInstances = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool HasAttachedLight
        {
            get { return m_hasAttachedLight; }
            set { m_hasAttachedLight = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool HasExplosiveEffect
        {
            get { return m_hasExplosiveEffect; }
            set { m_hasExplosiveEffect = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public IList<ISpawnPoint> SpawnPoints
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public MapDefinition(IMapContainer container)
        {
            this.m_container = container;
            this.m_level = container.Level;
            this.TextureDictionaryCount = 1;
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public MapDefinition(ObjectDef sceneObject, IMapContainer container, Scene scene)
        {
            this.m_container = container;
            this.m_level = container.Level;
            this.TextureDictionaryCount = 1;

            this.LodDistance = sceneObject.GetLODDistance();
            GetNameFromObject(sceneObject, this);
            GetAttributesFromObject(sceneObject, this);
            GetBoundingBoxFromObject(sceneObject, this);
            GetTexturesFromObject(sceneObject, scene, this);
            GetStatisticsFromObject(sceneObject, scene, this);
        }

        ~MapDefinition()
        {
        }

        #endregion // Constructor

        #region Object Overrides

        /// <summary>
        /// Make sure the name is returned on ToString
        /// </summary>
        public override String ToString()
        {
            return this.Name;
        }

        /// <summary>
        /// Gets the hash code for this level
        /// using the name property
        /// </summary>
        public override int GetHashCode()
        {
            return this.Name.GetHashCode();
        }

        #endregion // Object Overrides

        #region Public Functions

        public bool UsesPresetShader(string preset)
        {
            if (Shaders == null)
                return false;

            foreach (Shader shader in this.Shaders)
            {
                if (shader.Name == preset)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Returns the attribute with the given identifier or the given
        /// default value if the attribute doesn't exist
        /// </summary>
        public T GetAttributeValue<T>(String attributeName, T defaultValue)
        {
            if (this.Attributes != null)
            {
                int hashCode = attributeName.ToLower().GetHashCode();
                if (this.Attributes.ContainsKey(hashCode))
                {
                    return (T)this.Attributes[hashCode];
                }
            }
            return defaultValue;
        }

        /// <summary>
        /// Returns true if and only if the attributes for this definition contains
        /// the given attribute name
        /// </summary>
        public Boolean HasAttribute(String attributeName)
        {
            if (this.Attributes != null)
            {
                int hashCode = attributeName.ToLower().GetHashCode();
                if (this.Attributes.ContainsKey(hashCode))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<MapInstance> GetInstances()
        {
            foreach (MapInstance instance in this.Instances)
            {
                yield return instance;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<MapInstance> CreateIsolatedReferences()
        {
            this.IsolatedInstances = new List<MapInstance>();

            int instanceIndex = 0;
            Dictionary<MapInstance, Dictionary<Boolean, List<MapInstance>>> isolated = new Dictionary<MapInstance, Dictionary<bool, List<MapInstance>>>();
            foreach (MapInstance instance in this.Instances)
            {
                if (instanceIndex >= this.Instances.Count)
                {
                    instanceIndex++;
                    continue;
                }

                for (int i = instanceIndex + 1; i < this.Instances.Count; i++)
                {
                    MapInstance sibling = this.Instances[i];
                    bool areIsolated = AreInstancesIsolated(instance, sibling);
                    if (!isolated.ContainsKey(instance))
                        isolated.Add(instance, new Dictionary<bool, List<MapInstance>>());
                    if (!isolated[instance].ContainsKey(areIsolated))
                        isolated[instance].Add(areIsolated, new List<MapInstance>());
                    if (!isolated[instance][areIsolated].Contains(sibling))
                        isolated[instance][areIsolated].Add(sibling);

                    if (!isolated.ContainsKey(sibling))
                        isolated.Add(sibling, new Dictionary<bool, List<MapInstance>>());
                    if (!isolated[sibling].ContainsKey(areIsolated))
                        isolated[sibling].Add(areIsolated, new List<MapInstance>());
                    if (!isolated[sibling][areIsolated].Contains(instance))
                        isolated[sibling][areIsolated].Add(instance);
                }

                if (!isolated.ContainsKey(instance))
                {
                    this.IsolatedInstances.Add(instance);
                }
                else
                {
                    if (!isolated[instance].ContainsKey(false))
                    {
                        this.IsolatedInstances.Add(instance);
                    }
                }
                yield return instance;
                instanceIndex++;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<MapInstance> GetIsolatedInstances()
        {
            if (IsolatedInstances != null)
            {
                foreach (MapInstance instance in IsolatedInstances)
                {
                    yield return instance;
                }
            }
        }

        #endregion // Public Functions

        #region Private Functions


        /// <summary>
        /// 
        /// </summary>
        /// <param name="i1"></param>
        /// <param name="i2"></param>
        /// <returns></returns>
        private static Boolean AreInstancesIsolated(MapInstance i1, MapInstance i2)
        {
            float fadeDistance = 20.0f;

            float instanceRadius = i1.LodDistance + fadeDistance;
            Vector3f instancePosition = i1.Position;

            float siblingRadius = i2.LodDistance + fadeDistance;
            Vector3f siblingPosition = i2.Position;
            float testDistance = (instanceRadius * instanceRadius) + (2 * (instanceRadius * siblingRadius)) + (siblingRadius * siblingRadius);
            float dx = siblingPosition.X - instancePosition.X;
            float dy = siblingPosition.Y - instancePosition.Y;
            float dz = siblingPosition.Z - instancePosition.Z;
            float squaredDistance = (dx * dx) + (dy * dy) + (dz * dz);
            if (squaredDistance <= testDistance)
                return false;

            return true;
        }

        /// <summary>
        /// Gets the name from the given object
        /// </summary>
        private static void GetNameFromObject(ObjectDef sceneObject, MapDefinition definition)
        {
            definition.m_name = sceneObject.Name;
        }

        /// <summary>
        /// Gets the attributes from the given object
        /// </summary>
        private static void GetAttributesFromObject(ObjectDef sceneObject, MapDefinition definition)
        {
            definition.m_attributes = new Dictionary<int, Object>(sceneObject.Attributes.Count);
            foreach (var attribute in sceneObject.Attributes)
            {
                definition.m_attributes.Add(attribute.Key.ToLower().GetHashCode(), attribute.Value);
            }
        }

        /// <summary>
        /// Gets the local bounding box for the given scene object
        /// </summary>
        private static void GetBoundingBoxFromObject(ObjectDef sceneObject, MapDefinition definition)
        {
            definition.m_boundingBox = null;
            if (sceneObject.Children != null && sceneObject.Children.Count() > 0)
            {
                if (sceneObject.WorldBoundingBox != null)
                {
                    definition.m_boundingBox = new BoundingBox3f(sceneObject.WorldBoundingBox);
                }
                int validChildCount = 0;
                foreach (ObjectDef child in GetChildren(sceneObject))
                {
                    if (!child.IsCollision())
                    {
                        if (child.WorldBoundingBox != null)
                        {
                            if (definition.BoundingBox == null)
                            {
                                definition.m_boundingBox = new BoundingBox3f(child.WorldBoundingBox);
                            }
                            else
                            {
                                definition.BoundingBox.Expand(child.WorldBoundingBox);
                            }
                        }
                        validChildCount++;
                    }
                }
                if (definition.BoundingBox != null && validChildCount != 0)
                {
                    float halfWidth = (definition.BoundingBox.Max.X - definition.BoundingBox.Min.X) * 0.5f;
                    float halfDepth = (definition.BoundingBox.Max.Y - definition.BoundingBox.Min.Y) * 0.5f;
                    float halfHeight = (definition.BoundingBox.Max.Z - definition.BoundingBox.Min.Z) * 0.5f;
                    definition.m_boundingBox = new BoundingBox3f(new Vector3f(-halfWidth, -halfDepth, -halfHeight), new Vector3f(halfWidth, halfDepth, halfHeight));
                }
            }
            else
            {
                if (sceneObject.LocalBoundingBox != null)
                {
                    definition.m_boundingBox = new BoundingBox3f(sceneObject.LocalBoundingBox);
                }
            }
        }

        /// <summary>
        /// Gets all the textures the are attached to the given object in
        /// all its materials
        /// </summary>
        private static void GetTexturesFromObject(ObjectDef sceneObject, Scene scene, MapDefinition definition)
        {
            MaterialDef material = null;
            if (scene.MaterialLookup.TryGetValue(sceneObject.Material, out material))
            {
                definition.GetTexturesFromMaterial(material, true);
            }
            foreach (ObjectDef child in GetChildren(sceneObject))
            {
                if (!child.IsCollision() && !child.DontExport() && !child.DontExportIDE())
                {
                    if (scene.MaterialLookup.TryGetValue(child.Material, out material))
                    {
                        definition.GetTexturesFromMaterial(material, true);
                    }
                }
            }
        }

        /// <summary>
        /// Goes through the material def and adds all the textures into the referenced textures list, if the
        /// parameter recursive is true it will also go through all the sub materials if any exist
        /// </summary>
        private void GetTexturesFromMaterial(MaterialDef material, Boolean recursive)
        {
            Boolean hasDiffuse = false;
            Boolean hasAlpha = false;
            Boolean hasSpecular = false;
            Boolean hasBump = false;
            List<ITexture> materialTextures = new List<ITexture>();
            if (material.HasTextures)
            {
                for (int i = 0; i < material.Textures.Length; i++)
                {
                    TextureDef texture = material.Textures[i];

                    String filename = texture.FilePath;
                    String name = System.IO.Path.GetFileNameWithoutExtension(texture.FilePath);
                    TextureTypes type = texture.Type;

                    if (type != TextureTypes.None)
                    {
                        ITexture newTexture = null;
                        if (type == TextureTypes.DiffuseMap || type == TextureTypes.SpecularMap || type == TextureTypes.BumpMap)
                        {
                            // We need to determine if the next texture is an appropriate alpha texture, if a next texture exists
                            int nextIndex = i + 1;
                            if (nextIndex < material.Textures.Length)
                            {
                                TextureDef nextTexture = material.Textures[i + 1];
                                if (type == TextureTypes.DiffuseMap && nextTexture.Type == TextureTypes.DiffuseAlpha ||
                                    type == TextureTypes.SpecularMap && nextTexture.Type == TextureTypes.SpecularAlpha ||
                                    type == TextureTypes.BumpMap && nextTexture.Type == TextureTypes.BumpAlpha)
                                {
                                    newTexture = new Texture(filename, nextTexture.FilePath, type, this);
                                    if (!this.ContainsAsset(newTexture.Name))
                                    {
                                        materialTextures.Add(newTexture);
                                        this.AssetChildren.Add(newTexture);
                                    }
                                    i++;
                                    continue;
                                }
                            }
                        }
                        else if (type == TextureTypes.DiffuseAlpha || type == TextureTypes.BumpAlpha || type == TextureTypes.SpecularAlpha)
                        {
                            // We need to determine if the next texture is an appropriate base texture, if a next texture exists
                            int nextIndex = i + 1;
                            if (nextIndex < material.Textures.Length)
                            {
                                TextureDef nextTexture = material.Textures[i + 1];
                                if (type == TextureTypes.DiffuseAlpha && nextTexture.Type == TextureTypes.DiffuseMap ||
                                    type == TextureTypes.SpecularAlpha && nextTexture.Type == TextureTypes.SpecularMap ||
                                    type == TextureTypes.BumpAlpha && nextTexture.Type == TextureTypes.BumpMap)
                                {
                                    newTexture = new Texture(nextTexture.FilePath, filename, nextTexture.Type, this);
                                    if (!this.ContainsAsset(newTexture.Name))
                                    {
                                        materialTextures.Add(newTexture);
                                        this.AssetChildren.Add(newTexture);
                                    }
                                    i++;
                                    continue;
                                }
                            }
                            continue;
                        }

                        newTexture = new Texture(filename, null, type, this);
                        if (!this.ContainsAsset(newTexture.Name))
                        {
                            materialTextures.Add(newTexture);
                            this.AssetChildren.Add(newTexture);
                        }
                    }
                }
            }

            String preset = String.Empty;
            switch (material.MaterialType)
            {
                case MaterialTypes.Rage:
                    if (!String.IsNullOrEmpty(material.Preset))
                    {
                        preset = material.Preset;
                    }
                    break;
                case MaterialTypes.Standard:
                    if (true)
                    {
                        if (hasDiffuse == true)
                        {
                            if (hasAlpha == true)
                            {
                                if (hasSpecular == true)
                                {
                                    if (hasBump == true)
                                    {
                                    }
                                }
                            }
                        }
                        else
                        {
                        }
                    }
                    break;

                case MaterialTypes.MultiMaterial:
                case MaterialTypes.None:
                default:
                    break;
            }

            if (!String.IsNullOrEmpty(preset))
            {
                Shader newShader = new Shader(preset, materialTextures);
                if (!Shaders.Contains(newShader))
                    this.Shaders.Add(newShader);
            }

            if (material.HasSubMaterials)
            {
                foreach (MaterialDef child in material.SubMaterials)
                {
                    GetTexturesFromMaterial(child, true);
                }
            }
        }

        /// <summary>
        /// Creates the statistics for this specific map definition
        /// </summary>
        private static void GetStatisticsFromObject(ObjectDef sceneObject, Scene scene, MapDefinition definition)
        {
            definition.m_exportGeometrySize = 0;
            definition.m_exportTxdSize = 0;
            definition.m_polygonCount = 0;
            definition.m_collisionPolygonCount = 0;
            definition.m_moverPolygonCount = 0;
            definition.m_weaponsPolygonCount = 0;

            definition.GetGeometryStatistics(sceneObject, scene);
            definition.GetTxdStatistics(sceneObject, scene);
            definition.GetPolygonStatistics(sceneObject);
            definition.GetChildStatistics(sceneObject, scene);
        }

        /// <summary>
        /// Gets all the geometry statistics for the given scene object
        /// </summary>
        private void GetGeometryStatistics(ObjectDef sceneObject, Scene scene)
        {
            DrawableStats geometryStat = null;
            scene.GeometryStatsLookup.TryGetValue(sceneObject.Guid, out geometryStat);
            if (geometryStat != null)
            {
                this.m_exportGeometrySize += (int)geometryStat.Size;
            }
        }

        /// <summary>
        /// Gets all the TXD statistics for the given scene object
        /// </summary>
        private void GetTxdStatistics(ObjectDef sceneObject, Scene scene)
        {
            TxdStats txdStat = null;
            scene.TxdStatsLookup.TryGetValue(this.TextureDictionaryName.ToLower(), out txdStat);
            if (txdStat != null)
            {
                this.m_exportTxdSize += txdStat.Size;
            }
        }

        /// <summary>
        /// Gets all the polygon statistics for the given scene object
        /// </summary>
        private void GetPolygonStatistics(ObjectDef sceneObject)
        {
            this.m_polygonCount += sceneObject.PolyCount;
        }

        /// <summary>
        /// Goes through all the children for the given scene object and adds they stats
        /// if the definition statistics
        /// </summary>
        private void GetChildStatistics(ObjectDef sceneObject, Scene scene)
        {
            foreach (ObjectDef child in sceneObject.Children)
            {
                if (child.DontExport())
                    continue;
                if (child.IsDummy())
                    continue;

                if (child.IsCollision() == true)
                {
                    this.m_collisionPolygonCount += child.PolyCount;
                    String defaultValue = "false";
                    Boolean boolDefaultValue = false;
                    Boolean river = false;
                    Boolean.TryParse(child.GetUserProperty("river", "false"), out river);
                    if (river)
                    {
                        this.m_riverPolygonCount += child.PolyCount;
                        defaultValue = "false";
                        boolDefaultValue = false;
                    }
                    else
                    {
                        defaultValue = "true";
                        boolDefaultValue = true;
                    }

                    Boolean mover = boolDefaultValue;
                    Boolean.TryParse(child.GetUserProperty("mover", defaultValue), out mover);
                    Boolean weapons = boolDefaultValue;
                    Boolean.TryParse(child.GetUserProperty("weapons", defaultValue), out weapons);
                    Boolean camera = boolDefaultValue;
                    Boolean.TryParse(child.GetUserProperty("camera", defaultValue), out camera);
                    if (mover)
                        this.m_moverPolygonCount += child.PolyCount;
                    if (weapons)
                        this.m_weaponsPolygonCount += child.PolyCount;
                    if (camera)
                        this.m_cameraPolygonCount += child.PolyCount;

                }
                else if (child.IsObject())
                {
                    this.GetGeometryStatistics(child, scene);
                    this.GetPolygonStatistics(child);
                    this.GetChildStatistics(child, scene);
                }
            }
        }

        /// <summary>
        /// Gets all the children for the given root object
        /// </summary>
        private static IEnumerable<ObjectDef> GetChildren(ObjectDef root)
        {
            foreach (ObjectDef child in root.Children)
            {
                yield return child;
                foreach (ObjectDef grandChild in GetChildren(child))
                {
                    yield return grandChild;
                }
            }
        }

        private static bool HasLightAttached(ObjectDef root)
        {
            foreach (ObjectDef child in root.Children)
            {
                if (child.Superclass == "light")
                    return true;

                if (HasLightAttached(child))
                    return true;
            }
            return false;
        }

        private static bool IsExplosive(ObjectDef root)
        {
            foreach (ObjectDef child in root.Children)
            {
                if (child.Is2dfxExplosionEffect())
                    return true;

                if (IsExplosive(child))
                    return true;
            }
            return false; 
        }

        #endregion // Private Functions

        #region Private Creation

        #endregion // Private Creation

        #region IEnumerable<IMapComponent>

        /// <summary>
        /// 
        /// </summary>
        IEnumerator<IMapComponent> IEnumerable<IMapComponent>.GetEnumerator()
        {
            foreach (IMapComponent component in this.AssetChildren.Where(c => c is IMapComponent == true))
            {
                yield return component;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerator)this);
        }

        #endregion // IEnumerable<IMapContainer>

        #region Static Functions

        /// <summary>
        /// Creates a definition from a scene object that is a milo
        /// </summary>
        public static MapDefinition CreateDefinitionForMilo(ObjectDef sceneObject, MapSection container, Scene scene)
        {
            Debug.Assert(sceneObject.IsMilo() == true, "Scene object needs to be a milo to create a milo definition");
            if (sceneObject.IsMilo() == false)
                return null;

            MapDefinition miloDefinition = new MapDefinition(container);
            miloDefinition.LodDistance = sceneObject.GetLODDistance();
            miloDefinition.Attributes = new Dictionary<int, Object>();
            GetNameFromObject(sceneObject, miloDefinition);
            miloDefinition.Name += "_milo_";
            miloDefinition.IsMilo = true;

            BoundingBox3f boundingBox = null;
            List<String> txds = new List<String>();
            foreach (ObjectDef child in sceneObject.Children)
            {
                if (child.IsMloRoom())
                {
                    MiloRoom newRoom = new MiloRoom(child, miloDefinition, scene);
                    if (newRoom.BoundingBox != null)
                    {
                        if (boundingBox == null)
                            boundingBox = new BoundingBox3f(newRoom.BoundingBox);
                        else
                            boundingBox.Expand(newRoom.BoundingBox);
                    }
                    miloDefinition.AssetChildren.Add(newRoom);
                    if (newRoom == null)
                        continue;

                    miloDefinition.m_polygonCount += newRoom.PolygonCount;
                    miloDefinition.m_collisionPolygonCount += newRoom.CollisionPolygonCount;
                    miloDefinition.m_moverPolygonCount += newRoom.MoverPolygonCount;
                    miloDefinition.m_weaponsPolygonCount += newRoom.WeaponsPolygonCount;
                    miloDefinition.m_cameraPolygonCount += newRoom.CameraPolygonCount;
                    miloDefinition.m_riverPolygonCount += newRoom.RiverPolygonCount;
                    miloDefinition.m_exportGeometrySize += newRoom.ExportGeometrySize;
                    foreach (KeyValuePair<String, uint> textureDictionary in newRoom.TextureDictionaries)
                    {
                        if (!txds.Contains(textureDictionary.Key))
                        {
                            txds.Add(textureDictionary.Key);
                            miloDefinition.m_exportTxdSize += textureDictionary.Value;
                        }
                    }
                    foreach (Shader shader in newRoom.Shaders)
                    {
                        if (!miloDefinition.Shaders.Contains(shader))
                            miloDefinition.Shaders.Add(shader);
                    }
                }
                else if (!child.IsMloPortal())
                {
                    if (!child.IsObject() || child.DontExport() || child.DontExportIDE())
                        continue;
                    if (child.IsRefInternalObject() || child.IsRefObject() || child.IsXRef() || child.IsInternalRef())
                        continue;

                    MapDefinition newDefinition = CreateDefinitionForObject(child, container, scene);
                    miloDefinition.AssetChildren.Add(newDefinition);

                    if (child.DontExportIPL())
                        continue;

                    MapDefinition stats = newDefinition;
                    if (stats != null)
                    {
                        miloDefinition.m_polygonCount += stats.PolygonCount;
                        miloDefinition.m_collisionPolygonCount += stats.CollisionPolygonCount;
                        miloDefinition.m_moverPolygonCount += stats.MoverPolygonCount;
                        miloDefinition.m_weaponsPolygonCount += stats.WeaponsPolygonCount;
                        miloDefinition.m_cameraPolygonCount += stats.CameraPolygonCount;
                        miloDefinition.m_riverPolygonCount += stats.RiverPolygonCount;
                        miloDefinition.m_exportGeometrySize += stats.ExportGeometrySize;
                        if (!txds.Contains(newDefinition.TextureDictionaryName))
                        {
                            txds.Add(newDefinition.TextureDictionaryName);
                            miloDefinition.m_exportTxdSize += stats.ExportTxdSize;
                        }
                        foreach (Shader shader in stats.Shaders)
                        {
                            if (!miloDefinition.Shaders.Contains(shader))
                                miloDefinition.Shaders.Add(shader);
                        }
                    }
                }
            }

            miloDefinition.TextureDictionaryCount = txds.Count;
            return miloDefinition;
        }

        /// <summary>
        /// Create a normal map definition from a root scene object, this goes through both the children and
        /// the drawable hierarchy children (if present) to create the definition.
        /// </summary>
        public static MapDefinition CreateDefinitionForObject(ObjectDef sceneObject, IMapContainer container, Scene scene)
        {
            MapDefinition definition = new MapDefinition(container);
            ObjectDef definitionSceneObject = sceneObject;
            definition.m_lodDistance = sceneObject.GetLODDistance();
            if (sceneObject.DrawableLOD != null)
            {
                // Get the highest definition object in the hierarchy by walking the children
                definitionSceneObject = GetHighestDefinition(sceneObject);
            }
            GetNameFromObject(definitionSceneObject, definition);
            GetAttributesFromObject(definitionSceneObject, definition);
            GetBoundingBoxFromObject(definitionSceneObject, definition);
            GetStatisticsFromObject(definitionSceneObject, scene, definition);
            GetTexturesFromObject(definitionSceneObject, scene, definition);
            definition.HasAttachedLight = HasLightAttached(definitionSceneObject);
            definition.HasExplosiveEffect = IsExplosive(definitionSceneObject);
            
            IEnumerable<ISpawnPoint> spawnPoints = ProcessChildSpawnPoints(sceneObject);
            if (spawnPoints.Any())
            {
                definition.SpawnPoints = new List<ISpawnPoint>(spawnPoints);
            }

            return definition;
        }

        /// <summary>
        /// Create a normal map definition from a root scene object, this goes through both the children and
        /// the drawable hierarchy children (if present) to create the definition.
        /// </summary>
        public static MapDefinition CreateDefinitionForFragment(ObjectDef rootSceneObject, IMapContainer container, Scene scene)
        {
            MapDefinition fragment = new MapDefinition(container);
            ObjectDef fragmentSceneObject = rootSceneObject;
            fragment.m_lodDistance = rootSceneObject.GetLODDistance();
            if (rootSceneObject.DrawableLOD != null)
            {
                // Get the highest definition object in the hierarchy by walking the children
                fragmentSceneObject = GetHighestDefinition(rootSceneObject);
            }
            GetNameFromObject(fragmentSceneObject, fragment);
            GetAttributesFromObject(fragmentSceneObject, fragment);
            GetBoundingBoxFromObject(fragmentSceneObject, fragment);
            GetTexturesFromObject(fragmentSceneObject, scene, fragment);
            GetStatisticsFromObject(fragmentSceneObject, scene, fragment);
            fragment.HasAttachedLight = HasLightAttached(fragmentSceneObject);
            fragment.HasExplosiveEffect = IsExplosive(fragmentSceneObject);

            IEnumerable<ISpawnPoint> spawnPoints = ProcessChildSpawnPoints(rootSceneObject);
            if (spawnPoints.Any())
            {
                fragment.SpawnPoints = new List<ISpawnPoint>(spawnPoints);
            }

            return fragment;
        }

        /// <summary>
        /// Create a normal map definition from a root scene object, this goes through both the children and
        /// the drawable hierarchy children (if present) to create the definition.
        /// </summary>
        public static MapDefinition CreateDefinitionForAnimObject(List<ObjectDef> animationObjects, IMapContainer container, Scene scene)
        {
            MapDefinition animation = new MapDefinition(container);
            if (animationObjects != null)
            {
                animation.Attributes = new Dictionary<int, Object>();
                int exportGeometrySize = 0;
                uint exportTxdSize = 0;
                int polygonCount = 0;
                int collisionPolygonCount = 0;
                List<String> txdNames = new List<String>();
                foreach (ObjectDef sceneObject in animationObjects)
                {
                    if (sceneObject.Attributes.ContainsKey(AttrNames.OBJ_ANIM_STATE))
                    {
                        String animState = sceneObject.Attributes[AttrNames.OBJ_ANIM_STATE] as String;
                        if (animState != null)
                        {
                            MapDefinition animStateDefinition = new MapDefinition(animation);
                            if (HasLightAttached(sceneObject))
                                animation.HasAttachedLight = true;
                            if (IsExplosive(sceneObject))
                                animation.HasExplosiveEffect = true;

                            GetNameFromObject(sceneObject, animStateDefinition);
                            GetAttributesFromObject(sceneObject, animStateDefinition);
                            GetBoundingBoxFromObject(sceneObject, animStateDefinition);
                            GetTexturesFromObject(sceneObject, scene, animStateDefinition);
                            GetStatisticsFromObject(sceneObject, scene, animStateDefinition);

                            foreach (var attribute in sceneObject.Attributes)
                            {
                                if (!animation.Attributes.ContainsKey(attribute.Key.ToLower().GetHashCode()))
                                {
                                    animation.Attributes.Add(attribute.Key.ToLower().GetHashCode(), attribute.Value);
                                }
                            }

                            if (animStateDefinition.PolygonCount > polygonCount)
                            {
                                polygonCount = animStateDefinition.PolygonCount;
                                collisionPolygonCount = animStateDefinition.CollisionPolygonCount;

                                GetBoundingBoxFromObject(sceneObject, animStateDefinition);
                                animation.BoundingBox = animStateDefinition.BoundingBox;
                            }
                            exportGeometrySize += animStateDefinition.ExportGeometrySize;
                            if (!txdNames.Contains(animStateDefinition.TextureDictionaryName))
                            {
                                txdNames.Add(animStateDefinition.TextureDictionaryName);
                                exportTxdSize += animStateDefinition.ExportTxdSize;
                            }

                            if (sceneObject.Attributes.ContainsKey(AttrNames.OBJ_GROUP_NAME))
                            {
                                String groupName = sceneObject.Attributes[AttrNames.OBJ_GROUP_NAME] as String;
                                animation.Name = groupName + "_anim";
                            }

                            if (!animation.ContainsAsset(animStateDefinition.Name))
                                animation.AssetChildren.Add(animStateDefinition);

                            if (sceneObject.GetLODDistance() > animation.LodDistance)
                                animation.LodDistance = sceneObject.GetLODDistance();
                        }
                    }
                }
                animation.m_exportGeometrySize = exportGeometrySize;
                animation.m_exportTxdSize = exportTxdSize;
                animation.m_polygonCount = polygonCount;
                animation.m_collisionPolygonCount = collisionPolygonCount;
            }

            return animation;
        }

        /// <summary>
        /// Returns the highest definition in a scene object using the scene objects drawable
        /// hierarchy
        /// </summary>
        private static ObjectDef GetHighestDefinition(ObjectDef sceneObject)
        {
            if (sceneObject.DrawableLOD != null)
            {
                if (sceneObject.DrawableLOD.Children == null || sceneObject.DrawableLOD.Children.Count() == 0)
                {
                    return sceneObject;
                }
                foreach (ObjectDef child in sceneObject.DrawableLOD.Children)
                {
                    return GetHighestDefinition(child);
                }
            }

            return sceneObject;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sceneObject"></param>
        private static IEnumerable<ISpawnPoint> ProcessChildSpawnPoints(ObjectDef sceneObject)
        {
            foreach (ObjectDef child in sceneObject.Children.Where(item => item.Is2dfxSpawnPoint()))
            {
                yield return new SpawnPoint(child);
            }
        }

        #endregion // Static Functions

        #region IHasTextureChildren Interface

        public String GetTcsFilenameForTexture(ITexture texture, RSG.Base.ConfigParser.ConfigGameView gv)
        {
            String tcsPath = String.Empty;

            if (this.Container is MapSection)
            {
                tcsPath = RSG.Model.Map2.Util.Metadata.GetTcsFilename(texture.Name,
                                                                      (this.Container as MapSection).ZipFilename,
                                                                      this.Name,
                                                                      this.TextureDictionaryName != null ? this.TextureDictionaryName : null,
                                                                      gv);
            }
            return tcsPath;
        }
        #endregion // IHasTextureChildren Interface
    } // MapDefinition
} // RSG.Model.Map

