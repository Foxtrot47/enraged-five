﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Math;
using RSG.SceneXml;

namespace RSG.Model.Common.Map
{
    /// <summary>
    /// Represents either a drawable or fragment/cloth.
    /// </summary>
    public interface IMapArchetype : IArchetype, IHasTextureChildren
    {
        #region Properties
        /// <summary>
        /// The parent section of this archetype
        /// </summary>
        IMapSection ParentSection { get; }

        /// <summary>
        /// 
        /// </summary>
        BoundingBox3f BoundingBox { get; }

        /// <summary>
        /// 
        /// </summary>
        IShader[] Shaders { get; }

        /// <summary>
        /// 
        /// </summary>
        ITexture[] Textures { get; }

        /// <summary>
        /// 
        /// </summary>
        uint ExportGeometrySize { get; }

        /// <summary>
        /// Map of texture dictionary names to their export size (used for section stat calculations)
        /// </summary>
        IDictionary<String, uint> TxdExportSizes { get; }

        /// <summary>
        /// 
        /// </summary>
        uint PolygonCount { get; }

        /// <summary>
        /// 
        /// </summary>
        uint CollisionPolygonCount { get; }

        /// <summary>
        /// 
        /// </summary>
        IDictionary<CollisionType, uint> CollisionTypePolygonCounts { get; }

        /// <summary>
        /// 
        /// </summary>
        bool HasAttachedLight { get; }

        /// <summary>
        /// 
        /// </summary>
        bool HasExplosiveEffect { get; }

        /// <summary>
        /// List of extensions this archetype has
        /// </summary>
#warning TODO: IMapArchetype archetype extensions
        //IEnumerable<IArchetypeExtension> Extensions { get; }

        /// <summary>
        /// Filters the list of entities to return only the exterior entities.
        /// </summary>
        IEnumerable<IEntity> ExteriorEntities { get; }

        /// <summary>
        /// Filters the list of entities to return only the interior entities.
        /// </summary>
        IEnumerable<IEntity> InteriorEntities { get; }

        /// <summary>
        /// Gets a value indicating whether this archetype represents the highest detailed
        /// object inside a drawable lod hierarchy.
        /// </summary>
        bool IsInDrawableLodHierarchy { get; }

        /// <summary>
        /// Gets a value indicating whether this archetype is linked to a shadow mesh.
        /// </summary>
        bool HasShadowProxies { get; }

        /// <summary>
        /// 
        /// </summary>
        IDictionary<string, LODHierarchyDef> SceneLinks { get; }

        /// <summary>
        /// Gets a value indicating whether this archetype gets added to the ipl data or not.
        /// </summary>
        bool DontAddToIpl { get; }

        /// <summary>
        /// Gets a value indicating whether this archetype is a permanent archetype and will
        /// always be in memory.
        /// </summary>
        bool IsPermanentArchetype { get; }
        #endregion // Properties

        #region Functions
        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        uint GetPolyCountForCollisionType(CollisionType type);
        #endregion // Functions
    } // IMapArchetype
}
