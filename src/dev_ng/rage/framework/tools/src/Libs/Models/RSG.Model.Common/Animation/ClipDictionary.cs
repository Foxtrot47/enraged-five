﻿using System;
using System.Collections.Generic;
using RSG.Base.Collections;
using System.Collections;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.Model.Common.Animation;
using System.ComponentModel;
using RSG.Base.ConfigParser;
using System.IO;
using System.Xml.Linq;
using RSG.Base.Logging;
using System.Xml.XPath;
using System.Runtime.Serialization;


namespace RSG.Model.Common.Animation
{
    /// <summary>
    /// ClipDictionary
    /// </summary>
    [DataContract]
    [KnownType(typeof(Clip))]
    public class ClipDictionary : AssetBase, IClipDictionary
    {
        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public ClipDictionary(String name, ClipDictionaryCategory category)
            : base(name)
        {
            Category = category;
            Clips = new List<IClip>();
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// Type of clip dictionary this is.
        /// </summary>
        [DataMember]
        public ClipDictionaryCategory Category { get; set; }

        /// <summary>
        /// Collection of clips
        /// </summary>
        [DataMember]
        public ICollection<IClip> Clips { get; private set; }
        #endregion // Properties

        #region Public Methods
        #endregion // Public Methods

        #region IEnumerable<IClip> Implementation
        /// <summary>
        /// Loops through the clips and returns each one in turn
        /// </summary>
        IEnumerator<IClip> IEnumerable<IClip>.GetEnumerator()
        {
            foreach (IClip clip in this.Clips)
            {
                yield return clip;
            }
        }

        /// <summary>
        /// Returns the Enumerator for this object
        /// </summary>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerator)this);
        }
        #endregion // IEnumerable<IClip> Implementation

        #region ICollection<IClip> Interface
        #region ICollection<IClip> Properties
        /// <summary>
        /// Returns whether the collection is read-only
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Return number of items in the set.
        /// </summary>
        public int Count
        {
            get
            {
                return Clips.Count();
            }
        }
        #endregion // ICollection<IClip> Properties

        #region ICollection<IClip> Methods
        /// <summary>
        /// Add a clip to the collection.
        /// </summary>
        /// <param name="item"></param>
        public void Add(IClip item)
        {
            Clips.Add(item);
        }

        /// <summary>
        /// Removes all clip from the collection.
        /// </summary>
        public void Clear()
        {
            Clips.Clear();
        }

        /// <summary>
        /// Determine whether the collection contains a specific clip
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Contains(IClip item)
        {
            return Clips.Contains(item);
        }

        /// <summary>
        /// Copy items in the collection to an array, starting at a particular index.
        /// </summary>
        /// <param name="output"></param>
        /// <param name="index"></param>
        public void CopyTo(IClip[] output, int index)
        {
            Clips.CopyTo(output, index);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        public bool Remove(IClip item)
        {
            return Clips.Remove(item);
        }
        #endregion // ICollection<IClip> Methods

        #endregion ICollection<IClip> Interface

        #region IComparable<IClipDictionary> Interface
        /// <summary>
        /// Compare this clip dictionary to another.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(IClipDictionary other)
        {
            if (other == null)
            {
                return 1;
            }

            return (Name.CompareTo(other.Name));
        }
        #endregion // IComparable<IClip> Interface

        #region IEquatable<IClipDictionary> Interface
        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>true if the current object is equal to the other parameter; otherwise, false.</returns>
        public bool Equals(IClipDictionary other)
        {
            if (other == null)
            {
                return false;
            }

            return (Name == other.Name);
        }
        #endregion // IEquatable<IClipDictionary> Interface

    } // class ClipDictionary
} // namespace RSG.Model.Animation
