﻿using System;
using System.Xml;
using System.Xml.XPath;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;

namespace RSG.Model.Map.Util
{
    /// <summary>
    /// List of static functions used to walk the map data
    /// </summary>
    public static class Enumerations
    {
        /// <summary>
        /// 
        /// </summary>
        public static List<MapSection> GetAllMapSections(ILevel rootLevel, Func<MapSection, Boolean> predict, Boolean recursive)
        {
            List<MapSection> mapSections = new List<MapSection>();

            foreach (IAsset child in rootLevel.AssetChildren)
            {
                if (child is MapSection)
                {
                    if (predict == null || (predict != null && predict(child as MapSection)))
                        mapSections.Add(child as MapSection);
                }
                else if (child is MapArea && recursive == true)
                {
                    mapSections.AddRange(GetAllMapSections(child as MapArea, predict, true));
                }
                else if (child is ILevel && recursive == true)
                {
                    mapSections.AddRange(GetAllMapSections(child as ILevel, predict, true));
                }
                else if (child is IHasAssetChildren && recursive == true)
                {
                    mapSections.AddRange(GetAllMapSections(child as IHasAssetChildren, predict, true));
                }
            }

            return mapSections;
        }

        /// <summary>
        /// 
        /// </summary>
        public static List<MapSection> GetAllMapSections(MapArea rootArea, Func<MapSection, Boolean> predict, Boolean recursive)
        {
            List<MapSection> mapSections = new List<MapSection>();

            foreach (IAsset child in rootArea.AssetChildren)
            {
                if (child is MapSection)
                {
                    if (predict == null || (predict != null && predict(child as MapSection)))
                        mapSections.Add(child as MapSection);
                }
                else if (child is MapArea && recursive == true)
                {
                    mapSections.AddRange(GetAllMapSections(child as MapArea, predict, true));
                }
                else if (child is ILevel && recursive == true)
                {
                    mapSections.AddRange(GetAllMapSections(child as ILevel, predict, true));
                }
            }

            return mapSections;
        }

        /// <summary>
        /// 
        /// </summary>
        public static IEnumerable<MapSection> GetAllMapSections(IHasAssetChildren rootAsset, Func<MapSection, Boolean> predict, Boolean recursive)
        {
            if (rootAsset is MapSection)
            {
                if (predict == null || (predict != null && predict(rootAsset as MapSection)))
                    yield return rootAsset as MapSection;
            }
            else
            {
                foreach (IAsset child in rootAsset.AssetChildren)
                {
                    if (child is MapSection)
                    {
                        if (predict == null || (predict != null && predict(child as MapSection)))
                            yield return child as MapSection;
                    }
                    else if (child is IHasAssetChildren && recursive == true)
                    {
                        foreach (MapSection section in GetAllMapSections(child as IHasAssetChildren, predict, true))
                            yield return section as MapSection;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static MapSection GetMapSectionByName(ILevel rootLevel, String name, Boolean recursive)
        {
            foreach (IAsset child in rootLevel.AssetChildren)
            {
                if (child is MapSection)
                {
                    if (String.Compare(name, child.Name, true) == 0)
                        return child as MapSection;
                }
                else if (child is MapArea && recursive == true)
                {
                    MapSection result = GetMapSectionByName(child as MapArea, name, true);
                    if (result != null)
                        return result;
                }
                else if (child is ILevel && recursive == true)
                {
                    MapSection result = GetMapSectionByName(child as ILevel, name, true);
                    if (result != null)
                        return result;
                }
            }

            return null;
        }
        
        /// <summary>
        /// 
        /// </summary>
        public static MapSection GetMapSectionByName(MapArea rootArea, String name, Boolean recursive)
        {
            foreach (IAsset child in rootArea.AssetChildren)
            {
                if (child is MapSection)
                {
                    if (String.Compare(name, child.Name, true) == 0)
                        return child as MapSection;
                }
                else if (child is MapArea && recursive == true)
                {
                    MapSection result = GetMapSectionByName(child as MapArea, name, true);
                    if (result != null)
                        return result;
                }
                else if (child is ILevel && recursive == true)
                {
                    MapSection result = GetMapSectionByName(child as ILevel, name, true);
                    if (result != null)
                        return result;
                }
            }

            return null;
        }

    } // Enumerations
} // RSG.Model.Map2.Util
