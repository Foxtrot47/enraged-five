﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common
{
    /// <summary>
    /// Enumeration of all statistics that a vehicle can have associated with it
    /// </summary>
    public static class StreamableLevelStat
    {
        public const String MapHierarchyString = "0F54C347-7A82-4D58-BC68-527B293724B8";
        public const String CharactersString = "012E6360-A213-4DBE-9CEA-DF1787A92073";
        public const String VehiclesString = "3AC53DAE-980B-411D-B660-E08AD08C1F0E";
        public const String WeaponsString = "8D832270-D70B-4A0B-BB7F-EA39AFAA6B4D";

        public const String ExportDataPathString = "DABAB0F0-A7CD-4B6C-89D5-AB182B8082CB";
        public const String ProcessedDataPathString = "0FD27D71-3156-4B8D-A128-D17FC4D74AF5";
        public const String ImageBoundsString = "CB8C57F7-6D10-4BC3-9B72-CEA129072D46";
        public const String BackgroundImageString = "16A4E4A3-0AA7-4269-8B96-738E18DCF9A4";
        public const String SpawnPointsString = "6D19DCAE-9244-4CC4-83A2-504A14384BBB";
        public const String SpawnPointClustersString = "E8303273-D6FE-4A85-A2F9-593E53D0DE1D";
        public const String ChainingGraphsString = "A3EB7CB1-4297-48D8-92E4-A11D18EA7FF3";

        public static readonly StreamableStat MapHierarchy = new StreamableStat(MapHierarchyString, true);
        public static readonly StreamableStat Characters = new StreamableStat(CharactersString, true);
        public static readonly StreamableStat Vehicles = new StreamableStat(VehiclesString, true);
        public static readonly StreamableStat Weapons = new StreamableStat(WeaponsString, true);

        public static readonly StreamableStat ExportDataPath = new StreamableStat(ExportDataPathString, true);
        public static readonly StreamableStat ProcessedDataPath = new StreamableStat(ProcessedDataPathString, true);
        public static readonly StreamableStat ImageBounds = new StreamableStat(ImageBoundsString, true, true, false);
        public static readonly StreamableStat BackgroundImage = new StreamableStat(BackgroundImageString, true, true);
        public static readonly StreamableStat SpawnPoints = new StreamableStat(SpawnPointsString, true, true);
        public static readonly StreamableStat SpawnPointClusters = new StreamableStat(SpawnPointClustersString, false, true);
        public static readonly StreamableStat ChainingGraphs = new StreamableStat(ChainingGraphsString, false, true);
    } // StreamableLevelStat
}
