﻿using System;
using System.ComponentModel;
using System.Windows.Media.Imaging;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Collections;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;

namespace RSG.Model.Map.ViewModel
{
    /// <summary>
    /// 
    /// </summary>
    public class MapInstanceViewModel :
        RSG.Base.Editor.HierarchicalViewModelBase, IMapViewModelComponent
    {
        #region Properties

        /// <summary>
        /// The name of the map section
        /// </summary>
        public String Name
        {
            get { return Model.Name.ToLower(); }
        }

        /// <summary>
        /// The model reference that this view model represents.
        /// </summary>
        public MapInstance Model
        {
            get { return m_model; }
            set { m_model = value; }
        }
        private MapInstance m_model;

        /// <summary>
        /// The level that this section belongs to.
        /// </summary>
        public LevelViewModel Level
        {
            get;
            private set;
        }
        
        /// <summary>
        /// The user data that can be attached to this view model
        /// </summary>
        public UserData ViewModelUserData
        {
            get { return m_viewModelUserData; }
            set { m_viewModelUserData = value; }
        }
        private UserData m_viewModelUserData = new UserData();

        /// <summary>
        /// A list of textures that this definition has attached to
        /// it. This list only has unique textures in it, no duplicates
        /// </summary>
        public ObservableCollection<TextureViewModel> Textures
        {
            get { return m_textures; }
            set
            {
                SetPropertyValue(value, () => this.Textures,
                    new PropertySetDelegate(delegate(Object newValue) { m_textures = (ObservableCollection<TextureViewModel>)newValue; }));
            }
        }
        private ObservableCollection<TextureViewModel> m_textures;

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Default Constructor
        /// </summary>
        public MapInstanceViewModel()
        {
        }

        /// <summary>
        /// Default Constructor
        /// </summary>
        public MapInstanceViewModel(IViewModel parent, MapInstance model)
        {
            this.Parent = parent;
            this.Model = model;
            this.Textures = new ObservableCollection<TextureViewModel>();
            this.Textures.Add(new TextureViewModel());
        }

        #endregion // Constructors

        #region Override Functions

        protected override void OnFirstExpanded()
        {
            ObservableCollection<TextureViewModel> textures = new ObservableCollection<TextureViewModel>();
            if (this.Model.ResolvedDefinition != null && this.Model.ResolvedDefinition.Textures != null)
            {
                foreach (Texture texture in this.Model.ResolvedDefinition.Textures.Values)
                {
                    textures.Add(new TextureViewModel(this, texture));
                }
            }
            this.Textures = textures;
        }

        public override IViewModel GetChildWithString(String name)
        {
            foreach (TextureViewModel child in this.Textures)
            {
                if (String.Compare(child.Model.StreamName, name, true) == 0)
                {
                    return child as IViewModel;
                }
            }
            return null;
        }

        #endregion // Override Functions
    } // MapInstanceViewModel
} // RSG.Model.Map.ViewModel
