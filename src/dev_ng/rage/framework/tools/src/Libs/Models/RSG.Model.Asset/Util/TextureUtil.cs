﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;

namespace RSG.Model.Asset.Util
{
    public static class TextureUtil
    {
        #region Methods

        public static String GetTcsFilenameForTexture(ITexture texture, RSG.Base.ConfigParser.ConfigGameView gv)
        {
            String tcsPath = String.Empty;

            if (texture.Parent != null)
            {
                tcsPath = texture.Parent.GetTcsFilenameForTexture(texture, gv);
            }
            else
            {
                RSG.Base.Logging.Log.Log__Error("texture.Parent == null");
            }

            return tcsPath;
        }

        public static String GetTcsFilenameForTextures(IEnumerable<ITexture> textures, RSG.Base.ConfigParser.ConfigGameView gv)
        {
            String tcsPath = String.Empty;

            Dictionary<IHasTextureChildren, List<ITexture>> parents = new Dictionary<IHasTextureChildren, List<ITexture>>();
            foreach (ITexture texture in textures)
            {
                if (texture.Parent != null)
                {
                    if (!parents.ContainsKey(texture.Parent))
                        parents.Add(texture.Parent, new List<ITexture>());

                    parents[texture.Parent].Add(texture);
                }
            }

            return tcsPath;
        }

        #endregion // Methods
    } // TextureUtil
} // RSG.Model.Common.Util
