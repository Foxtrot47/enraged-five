﻿// --------------------------------------------------------------------------------------------
// <copyright file="IConstant.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions
{
    using System;
    using System.Xml;
    using RSG.Editor.Model;

    /// <summary>
    /// When implemented defines a parsable constant that the parCodeGen system can use inside
    /// a structure or enumeration definition.
    /// </summary>
    public interface IConstant : IModel, IEquatable<IConstant>
    {
        #region Properties
        /// <summary>
        /// Gets the dictionary this constant is defined inside.
        /// </summary>
        IDefinitionDictionary Dictionary { get; }

        /// <summary>
        /// Gets or sets the filename this instance is defined in or null if not applicable.
        /// </summary>
        string Filename { get; set; }

        /// <summary>
        /// Gets or sets the line number this instance is defined on or -1 if not available.
        /// </summary>
        int LineNumber { get; set; }

        /// <summary>
        /// Gets or sets the line position this instance is defined at or -1 if not available.
        /// </summary>
        int LinePosition { get; set; }

        /// <summary>
        /// Gets the location of this constant inside the parCodeGen definition data sat on the
        /// local disk.
        /// </summary>
        FileLocation Location { get; }

        /// <summary>
        /// Gets or sets the name that this constant can be referenced by.
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Gets or sets the value of this constant as a string.
        /// </summary>
        string Value { get; set; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new <see cref="IConstant"/> that is a copy of this instance.
        /// </summary>
        /// <returns>
        /// A new <see cref="IConstant"/> that is a copy of this instance.
        /// </returns>
        new IConstant Clone();

        /// <summary>
        /// Writes the xml representation of this instance onto the specified
        /// System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that the xml representation of this instance should be
        /// written to.
        /// </param>
        /// <param name="saveFileInfo">
        /// If true the file, line number and line position will be written out as attributes.
        /// </param>
        void Serialise(XmlWriter writer, bool saveFileInfo);
        #endregion Methods
    } // RSG.Metadata.Model.Definitions.IConstant {Interface}
} // RSG.Metadata.Model.Definitions {Namespace}
