﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Map3.Util
{
    /// <summary>
    /// Helper class for comparing txd names in a case insensitive manner
    /// </summary>
    public class TXDNameComparer : IEqualityComparer<string>
    {
        // Summary:
        //     Determines whether the specified objects are equal.
        //
        // Parameters:
        //   x:
        //     The first object of type T to compare.
        //
        //   y:
        //     The second object of type T to compare.
        //
        // Returns:
        //     true if the specified objects are equal; otherwise, false.
        public bool Equals(string x, string y)
        {
            if (string.Compare(x.ToLower(), y.ToLower()) == 0)
                return true;
            return false;
        }

        //
        // Summary:
        //     Returns a hash code for the specified object.
        //
        // Parameters:
        //   obj:
        //     The System.Object for which a hash code is to be returned.
        //
        // Returns:
        //     A hash code for the specified object.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     The type of obj is a reference type and obj is null.
        public int GetHashCode(string obj)
        {
            return obj.ToLower().GetHashCode();
        }
    } // TXDNameComparer
}
