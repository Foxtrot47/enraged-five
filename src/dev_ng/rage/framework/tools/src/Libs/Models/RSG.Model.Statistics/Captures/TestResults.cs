﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Collections.ObjectModel;
using RSG.Base.Logging;
using System.Reflection;
using System.Runtime.Serialization;
using System.Diagnostics;

namespace RSG.Model.Statistics.Captures
{
    /// <summary>
    /// Contains all capture information relating to a single capture zone.
    /// </summary>
    [DataContract]
    public class TestResults
    {
        #region Properties
        /// <summary>
        /// Name of the zone these results are for.
        /// </summary>
        [DataMember]
        public String Name { get; private set; }

        /// <summary>
        /// Fps results for this test.
        /// </summary>
        [DataMember]
        public FpsResult FpsResult { get; set; }

        /// <summary>
        /// Frametime results for this test.
        /// </summary>
        [DataMember]
        public FrametimeResult FrametimeResult { get; set; }

        /// <summary>
        /// Gpu results for this test.
        /// </summary>
        [DataMember]
        public ICollection<GpuResult> GpuResults { get; private set; }

        /// <summary>
        /// Cpu results for this test.
        /// </summary>
        [DataMember]
        public ICollection<CpuResult> CpuResults { get; private set; }

        /// <summary>
        /// Memory results for this test.
        /// </summary>
        [DataMember]
        public ICollection<MemoryResult> MemoryResults { get; private set; }

        /// <summary>
        /// Streaming memory results for this test.
        /// </summary>
        [DataMember]
        public ICollection<StreamingMemoryResult> StreamingMemoryResults { get; private set; }

        /// <summary>
        /// Draw list results for this test.
        /// </summary>
        [DataMember]
        public ICollection<DrawListResult> DrawListResults { get; private set; }

        /// <summary>
        /// Thread results for this test.
        /// </summary>
        [DataMember]
        public ICollection<ThreadResult> ThreadResults { get; private set; }

        /// <summary>
        /// Script memory results for this test.
        /// </summary>
        [DataMember]
        public ScriptMemResult ScriptMemResult { get; set; }

        /// <summary>
        /// Pedestrian and vehicle results for this test.
        /// </summary>
        [DataMember]
        public PedAndVehicleResult PedAndVehicleResult { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public TestResults()
        {
            GpuResults = new Collection<GpuResult>();
            CpuResults = new Collection<CpuResult>();
            MemoryResults = new Collection<MemoryResult>();
            StreamingMemoryResults = new Collection<StreamingMemoryResult>();
            DrawListResults = new Collection<DrawListResult>();
            ThreadResults = new Collection<ThreadResult>();
        }

        /// <summary>
        /// 
        /// </summary>
        public TestResults(XElement element)
            : this()
        {
            Parse(element);
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="element"></param>
        private void Parse(XElement testElement)
        {
            // Extract the name
            XElement nameElement = testElement.Element("name");
            if (nameElement != null)
            {
                Name = nameElement.Value;
            }

            XElement fpsElement = testElement.Element("fpsResult");
            Debug.Assert(fpsElement != null, "fpsResult is null.");
            if (fpsElement != null)
            {
                try
                {
                    FpsResult = FpsResult.CreateFromElement(fpsElement);
                }
                catch (System.Exception ex)
                {
                    Log.Log__Exception(ex, "Failed to create FpsResult {0}.", Name);
                }
            }

            XElement ftElement = testElement.Element("msPFResult");
            Debug.Assert(ftElement != null, "ftElement is null.");
            if (ftElement != null)
            {
                try
                {
                    FrametimeResult = FrametimeResult.CreateFromElement(ftElement);
                }
                catch (System.Exception ex)
                {
                    Log.Log__Exception(ex, "Failed to create FrametimeResult for {0}.", Name);
                }
            }

            XElement gpuElement = testElement.Element("gpuResults");
            if (gpuElement != null)
            {
                foreach (XElement item in gpuElement.Elements("Item"))
                {
                    try
                    {
                        // Ignore the totals one.
                        GpuResult result = new GpuResult(item);
                        if (result.Name != "TOTALS")
                        {
                            GpuResults.Add(result);
                        }
                    }
                    catch (System.Exception ex)
                    {
                        Log.Log__Exception(ex, "Failed to create GpuResult for {0}.", Name);
                    }
                }
            }

            XElement cpuElement = testElement.Element("cpuResults");
            if (cpuElement != null)
            {
                foreach (XElement item in cpuElement.Elements("Item"))
                {
                    try
                    {
                        CpuResults.Add(new CpuResult(item));
                    }
                    catch (System.Exception ex)
                    {
                        Log.Log__Exception(ex, "Failed to create CpuResult for {0}.", Name);
                    }
                }
            }

            XElement memoryElement = testElement.Element("memoryResults");
            if (memoryElement != null)
            {
                foreach (XElement item in memoryElement.Elements("Item"))
                {
                    try
                    {
                        MemoryResults.Add(new MemoryResult(item));
                    }
                    catch (System.Exception ex)
                    {
                        Log.Log__Exception(ex, "Failed to create MemoryResult for {0}.", Name);
                    }
                }
            }

            XElement streamingMemoryElement = testElement.Element("streamingMemoryResults");
            if (streamingMemoryElement != null)
            {
                foreach (XElement item in streamingMemoryElement.Elements("Item"))
                {
                    try
                    {
                        StreamingMemoryResults.Add(new StreamingMemoryResult(item));
                    }
                    catch (System.Exception ex)
                    {
                        Log.Log__Exception(ex, "Failed to create StreamingMemoryResult for {0}.", Name);
                    }
                }
            }

            XElement drawListElement = testElement.Element("drawListResults");
            if (drawListElement != null)
            {
                foreach (XElement item in drawListElement.Elements("Item"))
                {
                    try
                    {
                        DrawListResults.Add(new DrawListResult(item));
                    }
                    catch (System.Exception ex)
                    {
                        Log.Log__Exception(ex, "Failed to create DrawListResults for {0}.", Name);
                    }
                }
            }

            XElement threadElement = testElement.Element("threadResults");
            if (threadElement != null)
            {
                foreach (XElement item in threadElement.Elements("Item"))
                {
                    try
                    {
                        ThreadResults.Add(new ThreadResult(item));
                    }
                    catch (System.Exception ex)
                    {
                        Log.Log__Exception(ex, "Failed to create ThreadResult for {0}.", Name);
                    }
                }
            }

            XElement scriptMemElement = testElement.Element("scriptMemResult");
            if (scriptMemElement != null)
            {
                try
                {
                    ScriptMemResult = new ScriptMemResult(scriptMemElement);
                }
                catch (System.Exception ex)
                {
                    Log.Log__Exception(ex, "Failed to create ScriptMemResult for {0}.", Name);
                }
            }

            XElement pedVehicleElement = testElement.Element("pedAndVehicleResults");
            if (pedVehicleElement != null)
            {
                try
                {
                    PedAndVehicleResult = new PedAndVehicleResult(pedVehicleElement);
                }
                catch (System.Exception ex)
                {
                    Log.Log__Exception(ex, "Failed to create PedAndVehicleResult for {0}.", Name);
                }
            }
        }
        #endregion // Private Methods
    } // Test
}
