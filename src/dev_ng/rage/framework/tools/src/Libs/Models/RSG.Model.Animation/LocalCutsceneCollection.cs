﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common;

namespace RSG.Model.Animation
{
    /// <summary>
    /// Local cutscene collection.
    /// </summary>
    [DataContract]
    [KnownType(typeof(LocalCutscene))]
    public class LocalCutsceneCollection : CutsceneCollectionBase
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public LocalCutsceneCollection()
            : base()
        {
        }
        #endregion // Constructor(s)
    } // LocalCutsceneCollection
}
