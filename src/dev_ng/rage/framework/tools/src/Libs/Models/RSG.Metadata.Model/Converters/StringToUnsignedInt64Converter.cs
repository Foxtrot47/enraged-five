﻿//---------------------------------------------------------------------------------------------
// <copyright file="StringToUnsignedInt64Converter.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Converters
{
    using System.Globalization;

    /// <summary>
    /// Represents a converter that can convert a string instance into a single or an array of
    /// unsigned long values. This class cannot be inherited.
    /// </summary>
    public sealed class StringToUnsignedInt64Converter : NumericalConverter<ulong>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="StringToUnsignedInt64Converter"/>
        /// class.
        /// </summary>
        public StringToUnsignedInt64Converter()
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="StringToUnsignedInt64Converter"/>
        /// class.
        /// </summary>
        /// <param name="constants">
        /// The numerical constant class that this converter can use.
        /// </param>
        public StringToUnsignedInt64Converter(NumericalConstants constants)
            : base(constants)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Converts the double representation of the numerical number to its specified type
        /// equivalent.
        /// </summary>
        /// <param name="d">
        /// A double that contains the number to convert.
        /// </param>
        /// <param name="result">
        /// When this method returns, contains the numerical value equivalent to the number
        /// contained in d, if the conversion succeeded, or zero if the conversion failed.
        /// </param>
        /// <returns>
        /// True if d was converted successfully; otherwise, false.
        /// </returns>
        protected override bool FromDouble(double d, out ulong result)
        {
            result = (ulong)d;
            return double.Equals(d, (double)result);
        }

        /// <summary>
        /// Tries to convert the string representation of a number to its numerical equivalent,
        /// and returns a value that indicates whether the conversion succeeded.
        /// </summary>
        /// <param name="s">
        /// A string that contains a number to convert.
        /// </param>
        /// <param name="result">
        /// When this method returns, contains the numerical value equivalent to the number
        /// contained in s, if the conversion succeeded, or zero if the conversion failed.
        /// </param>
        /// <returns>
        /// True if s was converted successfully; otherwise, false.
        /// </returns>
        protected override bool TryParse(string s, out ulong result)
        {
            return ulong.TryParse(s, out result);
        }

        /// <summary>
        /// Tries to convert the string representation of a number to its numerical equivalent,
        /// and returns a value that indicates whether the conversion succeeded.
        /// </summary>
        /// <param name="s">
        /// A string that contains a number to convert.
        /// </param>
        /// <param name="style">
        /// A bitwise combination of enumeration values that indicates the permitted format of
        /// s.
        /// </param>
        /// <param name="result">
        /// When this method returns, contains the numerical value equivalent to the number
        /// contained in s, if the conversion succeeded, or zero if the conversion failed.
        /// </param>
        /// <returns>
        /// True if s was converted successfully; otherwise, false.
        /// </returns>
        protected override bool TryParse(string s, NumberStyles style, out ulong result)
        {
            return ulong.TryParse(s, style, CultureInfo.CurrentCulture, out result);
        }
        #endregion Methods
    } // RSG.Metadata.Model.Converters.StringToUnsignedInt64Converter {Class}
} // RSG.Metadata.Model.Converters {Namespace}
