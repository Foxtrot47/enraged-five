﻿using System;
using System.Windows;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.Base.ConfigParser;
using RSG.SceneXml;
using RSG.Base.Math;

namespace RSG.Model.Map
{
    /// <summary>
    /// Represents a single instance of a definition
    /// </summary>
    public class MapInstance : FileAssetContainerBase, IMapObject, IMapComponent
    {
        #region Members

        private ILevel m_level;
        private IMapContainer m_container;
        private BoundingBox3f m_boundingBox;
        private Vector3f m_position;
        private MapDefinition m_referencedDefinition;
        private RSG.SceneXml.ObjectDef.LodLevel m_lodLevel;
        private int m_priorityLevel;
        private bool m_isLowPriority;
        private Boolean m_isReference;
        private Boolean m_isInteriorReference;
        private Boolean m_usingLodDistanceFromInstance;
        private float m_savedLodDistance;
        private float m_lodDistance;
        private MapInstance m_lodParent;
        private List<MapInstance> m_lodChildren;

        #endregion // Members

        #region Properties

        /// <summary>
        /// Reference to the owner Level object.
        /// </summary>
        public ILevel Level
        {
            get { return m_level; }
            set
            {
                if (m_level == value)
                    return;

                SetPropertyValue(value, () => Level,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_level = (ILevel)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// The immediate parent map container, or
        /// null if non exist
        /// </summary>
        public IMapContainer Container
        {
            get { return m_container; }
            set
            {
                if (m_container == value)
                    return;

                SetPropertyValue(value, () => Container,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_container = (IMapContainer)newValue;
                        }
                ));

                this.Level = value.Level;
            }
        }

        /// <summary>
        /// The definition that this instance is referencing
        /// </summary>
        public MapDefinition ReferencedDefinition
        {
            get { return m_referencedDefinition; }
            set
            {
                if (m_referencedDefinition == value)
                    return;

                SetPropertyValue(value, () => ReferencedDefinition,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_referencedDefinition = (MapDefinition)newValue;
                        }
                ));

                OnReferencedDefinitionChanged();
            }
        }

        /// <summary>
        /// The tightest box surrounding the map
        /// object, used for density statistics
        /// </summary>
        public BoundingBox3f BoundingBox
        {
            get { return m_boundingBox; }
            set
            {
                if (m_boundingBox == value)
                    return;

                SetPropertyValue(value, () => BoundingBox,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_boundingBox = (BoundingBox3f)newValue;
                        }
                ));
            }
        }
        
        /// <summary>
        /// Instance's position in world-space.
        /// </summary>
        public Vector3f Position
        {
            get { return m_position; }
            set
            {
                if (m_position == value)
                    return;

                SetPropertyValue(value, () => Position,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_position = (Vector3f)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// The instances LOD level.
        /// </summary>
        public RSG.SceneXml.ObjectDef.LodLevel LODLevel
        {
            get { return m_lodLevel; }
            set
            {
                if (m_lodLevel == value)
                    return;

                SetPropertyValue(value, () => LODLevel,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_lodLevel = (RSG.SceneXml.ObjectDef.LodLevel)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// The priority level for the instance 
        /// </summary>
        public int PriorityLevel
        {
            get { return m_priorityLevel; }
            set
            {
                if (m_priorityLevel == value)
                    return;

                SetPropertyValue(value, () => PriorityLevel,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_priorityLevel = (int)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// Whether the instance has been flagged as being low priority
        /// </summary>
        public bool IsLowPriority
        {
            get { return m_isLowPriority; }
            set
            {
                SetPropertyValue(value, () => IsLowPriority,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_isLowPriority = (bool)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// A flag that represents that this is a reference
        /// </summary>
        public Boolean IsReference
        {
            get { return m_isReference; }
            set
            {
                if (m_isReference == value)
                    return;

                SetPropertyValue(value, () => IsReference,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_isReference = (Boolean)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// A flag that represents that this is a reference to a interior
        /// </summary>
        public Boolean IsInteriorReference
        {
            get { return m_isInteriorReference; }
            set
            {
                if (m_isInteriorReference == value)
                    return;

                SetPropertyValue(value, () => IsInteriorReference,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_isInteriorReference = (Boolean)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// A flag that represents whether the lod distance on the instance is used or
        /// the definition
        /// </summary>
        private Boolean UsingLodDistanceFromInstance
        {
            get { return m_usingLodDistanceFromInstance; }
            set
            {
                if (m_usingLodDistanceFromInstance == value)
                    return;

                SetPropertyValue(value, () => UsingLodDistanceFromInstance,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_usingLodDistanceFromInstance = (Boolean)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// The lod distance from the instance (might not be the lod distance)
        /// </summary>
        private float SavedLodDistance
        {
            get { return m_savedLodDistance; }
            set
            {
                if (m_savedLodDistance == value)
                    return;

                SetPropertyValue(value, () => SavedLodDistance,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_savedLodDistance = (float)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// The lod distance for this instance
        /// </summary>
        public float LodDistance
        {
            get { return m_lodDistance; }
            set
            {
                if (m_lodDistance == value)
                    return;

                SetPropertyValue(value, () => LodDistance,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_lodDistance = (float)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public MapInstance LodParent
        {
            get { return m_lodParent; }
            set
            {
                if (m_lodParent == value)
                    return;

                SetPropertyValue(value, () => LodParent,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_lodParent = (MapInstance)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public List<MapInstance> LodChildren
        {
            get { return m_lodChildren; }
            set
            {
                if (m_lodChildren == value)
                    return;

                SetPropertyValue(value, () => LodChildren,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_lodChildren = (List<MapInstance>)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string ContainerLods
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public MapInstance()
        {
            this.LodChildren = new List<MapInstance>();
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public MapInstance(ObjectDef sceneObject, IMapContainer container)
        {
            this.m_lodChildren = new List<MapInstance>();
            this.m_position = sceneObject.NodeTransform.Translation;
            this.m_container = container;
            this.m_level = container.Level;
            this.m_lodLevel = sceneObject.LODLevel;
            if (sceneObject.Attributes.ContainsKey(AttrNames.OBJ_PRIORITY))
            {
                this.m_priorityLevel = (int)sceneObject.Attributes[AttrNames.OBJ_PRIORITY];
            }
            else
            {
                this.m_priorityLevel = 0;
            }
            this.m_isLowPriority = sceneObject.GetAttribute(AttrNames.OBJ_LOW_PRIORITY, AttrDefaults.OBJ_LOW_PRIORITY);
            if (sceneObject.IsXRef() || sceneObject.IsRefInternalObject() || sceneObject.IsRefObject() || sceneObject.IsInternalRef())
            {
                this.m_isReference = true;
            }

            if (sceneObject.WorldBoundingBox != null)
            {
                this.m_boundingBox = new BoundingBox3f(sceneObject.WorldBoundingBox);
            }
            else
            {
                this.m_boundingBox = null;
            }

            this.m_usingLodDistanceFromInstance = sceneObject.Attributes.ContainsKey(AttrNames.OBJ_INSTANCE_LOD_DISTANCE);
            if (this.UsingLodDistanceFromInstance)
            {
                this.m_savedLodDistance = sceneObject.GetLODDistance();
            }
            this.m_lodDistance = sceneObject.GetLODDistance();

            this.m_name = sceneObject.Name;
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public MapInstance(ObjectDef sceneObject, IMapContainer container, MapDefinition referenced)
        {
            this.m_lodChildren = new List<MapInstance>();
            this.m_position = sceneObject.NodeTransform.Translation;
            this.m_container = container;
            this.m_level = container.Level;
            this.m_lodLevel = sceneObject.LODLevel;
            if (sceneObject.Attributes.ContainsKey(AttrNames.OBJ_LOD_FOR_CONTAINERS))
            {
                ContainerLods = (string)sceneObject.Attributes[AttrNames.OBJ_LOD_FOR_CONTAINERS];
            }
            if (sceneObject.Attributes.ContainsKey(AttrNames.OBJ_PRIORITY))
            {
                this.m_priorityLevel = (int)sceneObject.Attributes[AttrNames.OBJ_PRIORITY];
            }
            else
            {
                this.m_priorityLevel = 0;
            }
            this.m_isLowPriority = sceneObject.GetAttribute(AttrNames.OBJ_LOW_PRIORITY, AttrDefaults.OBJ_LOW_PRIORITY);
            if (sceneObject.IsXRef() || sceneObject.IsRefInternalObject() || sceneObject.IsRefObject() || sceneObject.IsInternalRef())
            {
                this.m_isReference = true;
            }

            if (sceneObject.WorldBoundingBox != null)
            {
                this.m_boundingBox = new BoundingBox3f(sceneObject.WorldBoundingBox);
            }
            else
            {
                this.m_boundingBox = null;
            }

            this.m_usingLodDistanceFromInstance = sceneObject.Attributes.ContainsKey(AttrNames.OBJ_INSTANCE_LOD_DISTANCE);
            if (this.UsingLodDistanceFromInstance)
            {
                this.m_savedLodDistance = sceneObject.GetLODDistance();
            }
            this.LodDistance = sceneObject.GetLODDistance();

            this.m_name = sceneObject.Name;
            this.m_referencedDefinition = referenced;
            if (this.ReferencedDefinition != null)
            {
                this.m_assetChildren = this.ReferencedDefinition.AssetChildren;
                this.m_isInteriorReference = this.ReferencedDefinition.IsMilo;

                if (this.m_usingLodDistanceFromInstance == true)
                {
                    this.m_lodDistance = this.SavedLodDistance;
                }
                else
                {
                    this.m_lodDistance = this.ReferencedDefinition.LodDistance;
                }
            }
            else
            {
                this.m_assetChildren = new RSG.Base.Collections.ObservableCollection<IAsset>();
                this.m_isInteriorReference = false;

                if (this.m_usingLodDistanceFromInstance == true)
                {
                    this.m_lodDistance = this.SavedLodDistance;
                }
                else
                {
                    this.m_lodDistance = AttrDefaults.OBJ_LOD_DISTANCE;
                }
            }
        }

        #endregion // Constructor

        #region Object Overrides

        /// <summary>
        /// Make sure the name is returned on ToString
        /// </summary>
        public override String ToString()
        {
            return this.Name;
        }

        /// <summary>
        /// Gets the hash code for this level
        /// using the name property
        /// </summary>
        public override int GetHashCode()
        {
            return this.Name.GetHashCode();
        }

        #endregion // Object Overrides

        #region Property Changes

        /// <summary>
        /// 
        /// </summary>
        public void OnReferencedDefinitionChanged()
        {
            if (this.ReferencedDefinition != null)
            {
                this.AssetChildren = this.ReferencedDefinition.AssetChildren;
                this.IsInteriorReference = this.ReferencedDefinition.IsMilo;

                if (this.UsingLodDistanceFromInstance == true)
                {
                    this.LodDistance = this.SavedLodDistance;
                }
                else
                {
                    this.LodDistance = this.ReferencedDefinition.LodDistance;
                }
            }
            else
            {
                this.AssetChildren = new RSG.Base.Collections.ObservableCollection<IAsset>();
                this.IsInteriorReference = false;

                if (this.UsingLodDistanceFromInstance == true)
                {
                    this.LodDistance = this.SavedLodDistance;
                }
                else
                {
                    this.LodDistance = AttrDefaults.OBJ_LOD_DISTANCE;
                }
            }
        }

        #endregion // Property Changes

        #region Public Methods
        /// <summary>
        /// Return the streaming extents of this Entity object.
        /// </summary>
        /// The streaming extents is defined by the LOD distance of this 
        /// instance; and depends on whether its in a LOD hierarchy or not.
        /// If we have a LOD parent then its relative to the parent's pivot.
        /// <returns></returns>
        public BoundingBox3f GetStreamingExtents()
        {
            // Note that this doesn't take streaming extent overrides into account :/

            BoundingBox3f streamingExtents = new BoundingBox3f();
            float lod = this.LodDistance;
            Vector3f vlod = new Vector3f(lod, lod, lod);

            if (this.LodParent != null)
            {
                streamingExtents.Expand(this.LodParent.Position + vlod);
                streamingExtents.Expand(this.LodParent.Position - vlod);
            }
            else
            {
                streamingExtents.Expand(this.Position + vlod);
                streamingExtents.Expand(this.Position - vlod);
            }

            return streamingExtents;
        }
        #endregion // Public Methods
    } // MapInstance
} // RSG.Model.Map

