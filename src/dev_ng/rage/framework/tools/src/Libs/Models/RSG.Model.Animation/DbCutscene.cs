﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common;
using RSG.Model.Common.Animation;

namespace RSG.Model.Animation
{
    /// <summary>
    /// Database cutscene model.
    /// </summary>
    [DataContract]
    [Serializable]
    public class DbCutscene : Cutscene
    {
        #region Properties
        /// <summary>
        /// Build that this cutscene is for.
        /// </summary>
        [DataMember]
        public String BuildIdentifier { get; set; }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public DbCutscene(String name, String buildIdentifier, String friendlyName, String missionId, String exportZipPath, bool isConcat,
            bool hasBranch, IList<ICutscenePart> parts, IDictionary<RSG.Platform.Platform, ICutscenePlatformStat> platformStats)
            : base(name, friendlyName, missionId)
        {
            BuildIdentifier = buildIdentifier;
            ExportZipFilepath = exportZipPath;
            IsConcat = isConcat;
            HasBranch = hasBranch;
            Parts = parts;
            PlatformStats = platformStats;
        }
        #endregion // Constructor(s)
        
        #region StreamableAssetContainerBase Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="statsToLoad"></param>
        protected override void LoadStatsInternal(IEnumerable<StreamableStat> statsToLoad)
        {
            // Nothing to do.  All data is loaded up front from the db.
        }
        #endregion // StreamableAssetContainerBase Overrides
    } // DbCutscene
}
