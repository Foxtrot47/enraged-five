﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.ConfigParser;
using System.IO;
using System.Web.UI;
using RSG.Model.Common;
using RSG.Base.Threading;
using RSG.Base.Tasks;
using RSG.Model.Common.Map;

namespace RSG.Model.Report.Reports.Map
{
    /// <summary>
    /// 
    /// </summary>
    public class DuplicatedItemReport : HTMLReport, IDynamicLevelReport
    {
        #region Constants
        private const String c_name = "Duplicated Item Report";
        private const String c_description = "Generate and display a duplicated item report.";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (m_task == null)
                {
                    CompositeTask task = new CompositeTask("Generate Report");
                    task.AddSubTask(new ActionTask("Loading data", (context, progress) => EnsureDataLoaded((DynamicLevelReportContext)context, progress)), 50);
                    task.AddSubTask(new ActionTask("Generating report", (context, progress) => GenerateReport((DynamicLevelReportContext)context, progress)));
                    m_task = task;
                }
                return m_task;
            }
        }
        private ITask m_task;

        /// <summary>
        /// List of stats that are required to generate this report.
        /// </summary>
        public IEnumerable<StreamableStat> RequiredStats
        {
            get { return null; }
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public DuplicatedItemReport()
            : base(c_name, c_description)
        {
        }
        #endregion // Constructor(s)

        #region Task Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void EnsureDataLoaded(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            IMapHierarchy hierarchy = context.Level.MapHierarchy;

            if (hierarchy != null)
            {
                float increment = 1.0f / hierarchy.AllSections.Count();

                // Load all the map section data
                foreach (IMapSection section in hierarchy.AllSections)
                {
                    context.Token.ThrowIfCancellationRequested();

                    // Update the progress information
                    string message = String.Format("Loading {0}", section.Name);
                    progress.Report(new TaskProgress(increment, true, message));

                    section.RequestAllStatistics(false);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void GenerateReport(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            GenerateDuplicatedItemReport(context.Level, context.GameView);
        }
        #endregion // Task Methods
        
        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        /// <param name="gv"></param>
        public void GenerateDuplicatedItemReport(ILevel level, ConfigGameView gv)
        {
            IMapHierarchy hierarchy = level.MapHierarchy;

            if (hierarchy != null)
            {
                // Map of txd names to section names
                IDictionary<string, ISet<IMapSection>> textureDictionaries = new Dictionary<string, ISet<IMapSection>>();

                // Map of archetype names to map sections
                IDictionary<string, ISet<IMapSection>> archetypes = new Dictionary<string, ISet<IMapSection>>();

                // Loop over all sections gatherin the information for generating the report
                foreach (IMapSection section in hierarchy.AllSections)
                {
                    foreach (IMapArchetype archetype in section.Archetypes)
                    {
                        // Keep track of the txd names
                        foreach (string txdName in archetype.TxdExportSizes.Keys)
                        {
                            if (!textureDictionaries.ContainsKey(txdName))
                            {
                                textureDictionaries[txdName] = new HashSet<IMapSection>();
                            }
                            textureDictionaries[txdName].Add(section);
                        }

                        // Keep track of the archetype names
                        if (!archetypes.ContainsKey(archetype.Name))
                        {
                            archetypes[archetype.Name] = new HashSet<IMapSection>();
                        }
                        archetypes[archetype.Name].Add(section);
                    }
                }

                // Get the duplicated txd/archetype names
                IEnumerable<KeyValuePair<string, ISet<IMapSection>>> dupedTextureDictionaries =
                    textureDictionaries.Where(pair => (pair.Value.Count > 1 && pair.Key  != "Missing TXD Attribute"));
                IEnumerable<KeyValuePair<string, ISet<IMapSection>>> dupedArchetypes = archetypes.Where(pair => pair.Value.Count > 1);

                // Write the report
                MemoryStream stream = new MemoryStream();
                StreamWriter reportFile = new StreamWriter(stream);

                HtmlTextWriter writer = new HtmlTextWriter(reportFile);
                writer.RenderBeginTag(HtmlTextWriterTag.Html);
                writer.RenderBeginTag(HtmlTextWriterTag.Head);
                writer.RenderEndTag();
                writer.RenderBeginTag(HtmlTextWriterTag.Body);

                if (dupedTextureDictionaries.Any())
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.H1);
                    writer.Write("Duplicated Texture Dictionaries:");
                    writer.RenderEndTag();

                    foreach (KeyValuePair<string, ISet<IMapSection>> dupe in dupedTextureDictionaries)
                    {
                        writer.RenderBeginTag(HtmlTextWriterTag.H2);
                        writer.Write("{0}", dupe.Key);
                        writer.RenderEndTag();

                        writer.AddAttribute(HtmlTextWriterAttribute.Border, "1");
                        writer.AddAttribute(HtmlTextWriterAttribute.Width, "40%");
                        writer.RenderBeginTag(HtmlTextWriterTag.Table);

                        writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                        writer.RenderBeginTag(HtmlTextWriterTag.Th);
                        writer.Write("Sections");
                        writer.RenderEndTag();
                        foreach (IMapSection section in dupe.Value)
                        {
                            writer.RenderBeginTag(HtmlTextWriterTag.Tr);

                            writer.RenderBeginTag(HtmlTextWriterTag.Td);
                            writer.Write(String.Format("{0}", section.Name));
                            writer.RenderEndTag();

                            writer.RenderEndTag();
                        }
                        writer.RenderEndTag();
                        writer.RenderEndTag();
                    }
                }
                else
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.H2);
                    writer.Write("No Duplicated Texture Dictionaries");
                    writer.RenderEndTag();
                }

                if (dupedArchetypes.Any())
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.H1);
                    writer.Write("Duplicated Map Objects:");
                    writer.RenderEndTag();

                    foreach (KeyValuePair<string, ISet<IMapSection>> dupe in dupedArchetypes)
                    {
                        writer.RenderBeginTag(HtmlTextWriterTag.H2);
                        writer.Write("{0}", dupe.Key);
                        writer.RenderEndTag();

                        writer.AddAttribute(HtmlTextWriterAttribute.Border, "1");
                        writer.AddAttribute(HtmlTextWriterAttribute.Width, "40%");
                        writer.RenderBeginTag(HtmlTextWriterTag.Table);

                        writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                        writer.RenderBeginTag(HtmlTextWriterTag.Th);
                        writer.Write("Sections");
                        writer.RenderEndTag();
                        foreach (IMapSection section in dupe.Value)
                        {
                            writer.RenderBeginTag(HtmlTextWriterTag.Tr);

                            writer.RenderBeginTag(HtmlTextWriterTag.Td);
                            writer.Write(String.Format("{0}", section.Name));
                            writer.RenderEndTag();

                            writer.RenderEndTag();
                        }
                        writer.RenderEndTag();
                        writer.RenderEndTag();
                    }
                }
                else
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.H2);
                    writer.Write("No Duplicated Map Objects");
                    writer.RenderEndTag();
                }

                writer.RenderEndTag();
                writer.RenderEndTag();
                writer.Flush();

                stream.Position = 0;
                Stream = stream;
            }
        }
        #endregion // Private Methods
    } // DuplicatedItemReport
} // RSG.Model.Report.Reports
