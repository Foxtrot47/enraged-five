﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Map.SceneOverride
{
    /// <summary>
    /// The interface common to all Scene override data storage repositories (Xml, MySql, etc.)
    /// </summary>
    internal interface ISceneOverrideRepository
    {
        // Get a record (or NULL) given a key
        ProcessedContentRecord GetProcessedContent(int processedContentId);
        LODOverrideRecord GetLODOverride(EntityKey entityKey);
        EntityDeleteRecord GetEntityDelete(EntityKey entityKey);
        AttributeOverrideRecord GetAttributeOverride(EntityKey entityKey);
        UserRecord GetUser(int userId);

        // Create a record, yielding a key
        int CreateProcessedContent(ProcessedContentRecord processedContent);
        EntityKey CreateOrUpdateLODOverride(LODOverrideRecord record);
        EntityKey CreateOrUpdateEntityDelete(EntityDeleteRecord record);
        EntityKey CreateOrUpdateAttributeOverride(AttributeOverrideRecord record);
        int CreateUser(UserRecord user);

        // Batch update (for performance)
        IEnumerable<EntityKey> CreateOrUpdateLODOverrides(IEnumerable<LODOverrideRecord> record);
        IEnumerable<EntityKey> CreateOrUpdateEntityDeletes(IEnumerable<EntityDeleteRecord> record);
        IEnumerable<EntityKey> CreateOrUpdateAttributeOverrides(IEnumerable<AttributeOverrideRecord> record);

        // Helper methods
        int FindProcessedContentKey(string processedContentName);
        int FindUserKey(string userName);
        LODOverrideRecord[] GetLODOverridesForProcessedContent(int processedContentId);
        EntityDeleteRecord[] GetEntityDeletesForProcessedContent(int processedContentId);
        AttributeOverrideRecord[] GetAttributeOverridesForProcessedContent(int processedContentId);
    }
}
