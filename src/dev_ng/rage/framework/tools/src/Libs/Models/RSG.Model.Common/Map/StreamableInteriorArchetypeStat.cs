﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common.Map
{
    /// <summary>
    /// Enumeration of all streamable stats an interior object can have.
    /// </summary>
    [DependentStat(StreamableMapSectionStat.ArchetypesString)]
    public static class StreamableInteriorArchetypeStat
    {
        public const String RoomsString = "4B560BA4-07E2-43B3-A683-F576E27EC5F8";
        public const String PortalsString = "07B32B8D-6F84-4A97-9C1A-F5111DC29A81";
        public const String EntitiesString = "5A5008F0-82CD-4A00-A7CB-EACB4061C9FC";

        public static readonly StreamableStat Rooms = new StreamableStat(RoomsString, true, true);
        public static readonly StreamableStat Portals = new StreamableStat(PortalsString, true, true);
        public static readonly StreamableStat Entities = new StreamableStat(EntitiesString, true, true);
    } // StreamableInteriorArchetypeStat
}
