﻿using System;
using System.Collections.Generic;
using RSG.Base.Collections;
using RSG.Model.Common;

namespace RSG.Model.Report
{
    /// <summary>
    /// Report Category Model Interface
    /// </summary>
    public interface IReportCategory :
        IReportItem,
        IHasAssetChildren
    {
        #region Properties
        /// <summary>
        /// List of reports that make up this category
        /// </summary>
        IEnumerable<IReportItem> Reports { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Method that gets called to intialise the category's reports.
        /// </summary>
        void InitialiseReports();
        #endregion // Methods
    } // IReportCategory
} // RSG.Model.Report namespace
