﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Runtime.Serialization;

namespace RSG.Model.Statistics.Captures
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class MemoryResult
    {
        #region Properties
        /// <summary>
        /// Name of this memory result.
        /// </summary>
        [DataMember]
        public String Name { get; set; }

        /// <summary>
        /// Virtual game memory stats.
        /// </summary>
        [DataMember]
        public MemoryStat GameVirtual { get; set; }

        /// <summary>
        /// Physical game memory stats.
        /// </summary>
        [DataMember]
        public MemoryStat GamePhysical { get; set; }

        /// <summary>
        /// Virtual resource memory stats.
        /// </summary>
        [DataMember]
        public MemoryStat ResourceVirtual { get; set; }

        /// <summary>
        /// Physical resource memory stats.
        /// </summary>
        [DataMember]
        public MemoryStat ResourcePhysical { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public MemoryResult()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public MemoryResult(XElement element)
        {
            // Extract the various stats
            XElement nameElement = element.Element("name");
            if (nameElement != null)
            {
                Name = nameElement.Value;
            }

            XElement gvElement = element.Element("gameVirtual");
            if (gvElement != null)
            {
                GameVirtual = GetMemoryStat(gvElement);
            }

            XElement gpElement = element.Element("gamePhysical");
            if (gpElement != null)
            {
                GamePhysical = GetMemoryStat(gpElement);
            }

            XElement rvElement = element.Element("resourceVirtual");
            if (rvElement != null)
            {
                ResourceVirtual = GetMemoryStat(rvElement);
            }

            XElement rpElement = element.Element("resourcePhysical");
            if (rpElement != null)
            {
                ResourcePhysical = GetMemoryStat(rpElement);
            }
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        private MemoryStat GetMemoryStat(XElement element)
        {
            uint min = 0;
            uint max = 0;
            uint average = 0;
            float std = 0;

            XElement minElement = element.Element("min");
            if (minElement != null && minElement.Attribute("value") != null)
            {
                min = UInt32.Parse(minElement.Attribute("value").Value);
            }

            XElement maxElement = element.Element("max");
            if (maxElement != null && maxElement.Attribute("value") != null)
            {
                max = UInt32.Parse(maxElement.Attribute("value").Value);
            }

            XElement avgElement = element.Element("average");
            if (avgElement != null && avgElement.Attribute("value") != null)
            {
                average = UInt32.Parse(avgElement.Attribute("value").Value);
            }

            XElement stdElement = element.Element("std");
            if (stdElement != null && stdElement.Attribute("value") != null)
            {
                std = Single.Parse(stdElement.Attribute("value").Value);
            }

            return new MemoryStat(min, max, average, std);
        }
        #endregion // Private Methods
    } // MemoryResult
}
