﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common.Map;
using RSG.Model.Common;
using RSG.Base.Math;
using System.ComponentModel;
using RSG.SceneXml;
using RSG.Model.Map3.Util;
using RSG.Base.Collections;
using RSG.Statistics.Common.Dto.GameAssetStats;
using System.Reflection;
using RSG.Base.Logging;
using RSG.Model.Asset;
using RSG.Statistics.Common.Dto.GameAssetStats.Bundles;

namespace RSG.Model.Map3
{
    /// <summary>
    /// 
    /// </summary>
    public class Room : StreamableAssetBase, IRoom
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public IInteriorArchetype ParentArchetype
        {
            get;
            protected set;
        }

        /// <summary>
        /// 
        /// </summary>
        [StreamableStatAttribute(StreamableRoomStat.BoundingBoxString)]
        public BoundingBox3f BoundingBox
        {
            get
            {
                CheckStatLoaded(StreamableRoomStat.BoundingBox);
                return m_boundingBox;
            }
            protected set
            {
                SetPropertyValue(ref m_boundingBox, value, "BoundingBox");
                SetStatLodaded(StreamableRoomStat.BoundingBox, true);
            }
        }
        private BoundingBox3f m_boundingBox;

        /// <summary>
        /// Array of vehicle textures. (Non-streamable, request shaders if you want to get this property.)
        /// </summary>
        [Browsable(false)]
        public ITexture[] Textures
        {
            get
            {
                CheckStatLoaded(StreamableRoomStat.Shaders);
                return m_textures;
            }
            private set
            {
                SetPropertyValue(ref m_textures, value, "Textures", "TextureCount");
            }
        }
        private ITexture[] m_textures;

        /// <summary>
        /// Texture count (exposed for the property grid)
        /// </summary>
        public int TextureCount
        {
            get
            {
                CheckStatLoaded(StreamableRoomStat.Shaders);
                return (Textures != null ? Textures.Count() : 0);
            }
        }

        /// <summary>
        /// Array of vehicle shaders.
        /// </summary>
        [Browsable(false)]
        [StreamableStatAttribute(StreamableRoomStat.ShadersString)]
        public IShader[] Shaders
        {
            get
            {
                CheckStatLoaded(StreamableRoomStat.Shaders);
                return m_shaders;
            }
            protected set
            {
                if (SetPropertyValue(ref m_shaders, value, "Shaders", "ShaderCount"))
                {
                    if (m_shaders != null)
                        Textures = m_shaders.SelectMany(shader => shader.Textures).Distinct().OrderBy(item => item.Name).ToArray();
                    else
                        Textures = null;
                }
                SetStatLodaded(StreamableRoomStat.Shaders, true);
            }
        }
        private IShader[] m_shaders;

        /// <summary>
        /// Shader count (exposed for the property grid)
        /// </summary>
        public int ShaderCount
        {
            get
            {
                CheckStatLoaded(StreamableRoomStat.Shaders);
                return (Shaders != null ? Shaders.Count() : 0);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [StreamableStatAttribute(StreamableRoomStat.ExportGeometrySizeString)]
        public uint ExportGeometrySize
        {
            get
            {
                CheckStatLoaded(StreamableRoomStat.ExportGeometrySize);
                return m_exportGeometrySize;
            }
            protected set
            {
                SetPropertyValue(ref m_exportGeometrySize, value, "ExportGeometrySize");
                SetStatLodaded(StreamableRoomStat.ExportGeometrySize, true);
            }
        }
        private uint m_exportGeometrySize;

        /// <summary>
        /// 
        /// </summary>
        [StreamableStatAttribute(StreamableRoomStat.TxdExportSizesString)]
        public IDictionary<String, uint> TxdExportSizes
        {
            get
            {
                CheckStatLoaded(StreamableRoomStat.TxdExportSizes);
                return m_txdExportSizes;
            }
            protected set
            {
                SetPropertyValue(ref m_txdExportSizes, value, "TxdExportSizes");
                SetStatLodaded(StreamableRoomStat.TxdExportSizes, true);
            }
        }
        private IDictionary<String, uint> m_txdExportSizes;

        /// <summary>
        /// 
        /// </summary>
        [StreamableStatAttribute(StreamableRoomStat.PolygonCountString)]
        public uint PolygonCount
        {
            get
            {
                CheckStatLoaded(StreamableRoomStat.PolygonCount);
                return m_polygonCount;
            }
            protected set
            {
                SetPropertyValue(ref m_polygonCount, value, "PolygonCount");
                SetStatLodaded(StreamableRoomStat.PolygonCount, true);
            }
        }
        private uint m_polygonCount;

        /// <summary>
        /// 
        /// </summary>
        [StreamableStatAttribute(StreamableRoomStat.CollisionPolygonCountString)]
        public uint CollisionPolygonCount
        {
            get
            {
                CheckStatLoaded(StreamableRoomStat.CollisionPolygonCount);
                return m_collisionPolygonCount;
            }
            protected set
            {
                SetPropertyValue(ref m_collisionPolygonCount, value, "CollisionPolygonCount");
                SetStatLodaded(StreamableRoomStat.CollisionPolygonCount, true);
            }
        }
        private uint m_collisionPolygonCount;

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        [StreamableStatAttribute(StreamableRoomStat.CollisionTypePolygonCountsString)]
        public IDictionary<CollisionType, uint> CollisionTypePolygonCounts
        {
            get
            {
                CheckStatLoaded(StreamableRoomStat.CollisionTypePolygonCounts);
                return m_collisionTypePolygonCounts;
            }
            protected set
            {
                SetPropertyValue(ref m_collisionTypePolygonCounts, value, "CollisionTypePolygonCounts");
                SetStatLodaded(StreamableRoomStat.CollisionTypePolygonCounts, true);
            }
        }
        private IDictionary<CollisionType, uint> m_collisionTypePolygonCounts;

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        [StreamableStatAttribute(StreamableRoomStat.EntitiesString)]
        public ObservableCollection<IEntity> ChildEntities
        {
            get
            {
                CheckStatLoaded(StreamableRoomStat.Entities);
                return m_childEntities;
            }
            private set
            {
                SetPropertyValue(ref m_childEntities, value, "ChildEntities");
                SetStatLodaded(StreamableRoomStat.Entities, true);
            }
        }
        private ObservableCollection<IEntity> m_childEntities;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public Room(string name, IInteriorArchetype parent)
            : base(name)
        {
            ParentArchetype = parent;
        }
        #endregion // Constructor(s)

        #region IRoom Implementation
        /// <summary>
        /// Load basic stats from the scene xml object def
        /// </summary>
        /// <param name="sceneObject"></param>
        public void LoadStatsFromExportData(TargetObjectDef sceneObject, Scene scene)
        {
            // Empty out the properties
            Shaders = new IShader[0];
            ExportGeometrySize = 0;
            PolygonCount = 0;
            CollisionPolygonCount = 0;
            CollisionTypePolygonCounts = new Dictionary<CollisionType, uint>();

            // Process the object's children categorising the objects we found
            CategorisedObjects categorisedChildObjectDefs = CategoriseSceneObjectChildren(sceneObject);

            foreach (TargetObjectDef drawableDef in categorisedChildObjectDefs.Drawables)
            {
                IDrawableArchetype drawable = new DrawableArchetype(drawableDef.Name, ParentArchetype.ParentSection);
                (drawable as DrawableArchetype).LoadStatsFromExportData(drawableDef, scene);
                ParentArchetype.ParentSection.Archetypes.Add(drawable);
            }

            foreach (TargetObjectDef fragmentDef in categorisedChildObjectDefs.Fragments)
            {
                IFragmentArchetype fragment = new FragmentArchetype(fragmentDef.Name, ParentArchetype.ParentSection);
                (fragment as FragmentArchetype).LoadStatsFromExportData(fragmentDef, scene);
                ParentArchetype.ParentSection.Archetypes.Add(fragment);
            }

            foreach (KeyValuePair<string, List<TargetObjectDef>> animPair in categorisedChildObjectDefs.AnimGroups)
            {
                StatedAnimArchetype anim = new StatedAnimArchetype(animPair.Key, ParentArchetype.ParentSection);
                anim.LoadStatsFromExportData(animPair.Value, scene);
                ParentArchetype.ParentSection.Archetypes.Add(anim);
            }

            ChildEntities = new ObservableCollection<IEntity>();
            foreach (TargetObjectDef entityDef in categorisedChildObjectDefs.Entities)
            {
                this.ProcessEntity(entityDef);
            }

            // Calculate the bounding box and statistics based on the room's entities
            ComputeBoundingBox();
            GatherStatistics();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="requestedStats"></param>
        public void LoadStatsFromDatabase(RoomStatDto dto, IList<StreamableStat> requestedStats)
        {
            IDictionary<StreamableStat, PropertyInfo> modelLookup = StreamableStatUtils.GetPropertyInfoLookup(GetType());
            IDictionary<StreamableStat, PropertyInfo> dtoLookup = StreamableStatUtils.GetPropertyInfoLookup(dto.GetType());

            // Map the properties we are after.
            foreach (StreamableStat stat in requestedStats.Where(item => !IsStatLoaded(item)))
            {
                if (dtoLookup.ContainsKey(stat) && modelLookup.ContainsKey(stat))
                {
                    // Is this a complex item?
                    if (stat.DtoToModelRequiresProcessing)
                    {
                        ProcessComplexRoomStat(dto, stat);
                    }
                    else
                    {
                        object value = dtoLookup[stat].GetValue(dto, null);
                        modelLookup[stat].SetValue(this, value, null);
                    }
                }
                else
                {
#warning Readd assert once everything is marked up.
                    //Debug.Assert(false, String.Format("One of the MapSectionStat dto class or the model class doesn't have the '{0}' stat marked up.", stat));
                    Log.Log__Error(String.Format("One of the MapSectionStat dto class or the model class doesn't have the '{0}' stat marked up.", stat));
                }
            }
        }
        #endregion // IRoom Implementation

        #region StreamableAssetBase Overrides
        /// <summary>
        /// Load specific statistics for this map section
        /// </summary>
        /// <param name="statsToLoad"></param>
        /// <param name="async"></param>
        public override void RequestStatistics(IEnumerable<StreamableStat> statsToLoad, bool async)
        {
            ParentArchetype.ParentSection.MapHierarchy.RequestStatisticsForRoom(this, statsToLoad, async);
        }
        #endregion // SimpleMapArchetypeBase Overrides

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sceneObject"></param>
        /// <returns></returns>
        private CategorisedObjects CategoriseSceneObjectChildren(TargetObjectDef sceneObject)
        {
            CategorisedObjects objects = new CategorisedObjects();

            foreach (TargetObjectDef childObject in sceneObject.Children.Where(item => IsSceneObjectValidForRoom(item)))
            {
                if (childObject.IsFragment())
                {
                    if (childObject.DrawableLOD != null && (childObject.DrawableLOD.Children != null && childObject.DrawableLOD.Children.Count() != 0))
                    {
                        continue;
                    }

                    // Check if its an entity or an archetype
                    if (childObject.IsXRef() || childObject.IsRefObject() || childObject.IsInternalRef() || childObject.IsRefInternalObject())
                    {
                        objects.Entities.Add(childObject);
                    }
                    else
                    {
                        objects.Fragments.Add(childObject);
                    }
                }
                else if (childObject.IsObject())
                {
                    // Determine if this is a map archetype or entity (or both)
                    if (childObject.IsXRef() || childObject.IsRefObject() || childObject.IsInternalRef() || childObject.IsRefInternalObject()) // A instance
                    {
                        if (childObject.LOD != null && childObject.LOD.Parent != null)
                        {
                            continue;
                        }

                        objects.Entities.Add(childObject);
                    }
                    else
                    {
                        if (childObject.DrawableLOD != null && childObject.DrawableLOD.Parent != null)
                        {
                            continue;
                        }

                        objects.Drawables.Add(childObject);
                        objects.Entities.Add(childObject);
                    }
                }
                else if (childObject.IsStatedAnim())
                {
                    if (childObject.Attributes.ContainsKey(AttrNames.OBJ_GROUP_NAME))
                    {
                        String animGroup = childObject.Attributes[AttrNames.OBJ_GROUP_NAME] as String;
                        if (animGroup != null)
                        {
                            if (!objects.AnimGroups.ContainsKey(animGroup.ToLower()))
                                objects.AnimGroups.Add(animGroup.ToLower(), new List<TargetObjectDef>());

                            objects.AnimGroups[animGroup.ToLower()].Add(childObject);
                        }
                    }
                }
            }

            return objects;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sceneObject"></param>
        /// <returns></returns>
        private bool IsSceneObjectValidForRoom(TargetObjectDef sceneObject)
        {
            return (!sceneObject.DontExport() && !sceneObject.DontExportIDE() && !sceneObject.DontExportIPL() && !sceneObject.IsDummy());
        }

        /// <summary>
        /// Calculates the room's bounding box based on the child entities
        /// </summary>
        /// <returns></returns>
        private void ComputeBoundingBox()
        {
            BoundingBox3f bbox = new BoundingBox3f();
            foreach (IEntity entity in ChildEntities)
            {
                bbox.Expand(entity.BoundingBox);
            }

            // Convert the world space bounding box to local space
            if (!bbox.IsEmpty)
            {
                float halfWidth = (bbox.Max.X - bbox.Min.X) * 0.5f;
                float halfDepth = (bbox.Max.Y - bbox.Min.Y) * 0.5f;
                float halfHeight = (bbox.Max.Z - bbox.Min.Z) * 0.5f;
                bbox = new BoundingBox3f(new Vector3f(-halfWidth, -halfDepth, -halfHeight), new Vector3f(halfWidth, halfDepth, halfHeight));
            }

            BoundingBox = bbox;
        }

        /// <summary>
        /// 
        /// </summary>
        private void GatherStatistics()
        {
            List<IArchetype> geometryArchetypes = new List<IArchetype>();
            List<string> txds = new List<string>();
            List<IAsset> polycountZero = new List<IAsset>();
            List<IShader> shaders = new List<IShader>();
            TxdExportSizes = new Dictionary<string, uint>();

            foreach (IEntity entity in ChildEntities)
            {
                IArchetype archetype = entity.ReferencedArchetype;

                if (archetype is IMapArchetype)
                {
                    IMapArchetype mapArchetype = (IMapArchetype)archetype;

                    PolygonCount += mapArchetype.PolygonCount;
                    CollisionPolygonCount += mapArchetype.CollisionPolygonCount;
                    if (mapArchetype.PolygonCount == 0)
                    {
                        polycountZero.Add(mapArchetype);
                    }

                    if (!geometryArchetypes.Contains(mapArchetype))
                    {
                        ExportGeometrySize += mapArchetype.ExportGeometrySize;
                        geometryArchetypes.Add(mapArchetype);
                    }

                    if (entity.IsReference == false)
                    {
                        foreach (KeyValuePair<string, uint> txdPair in mapArchetype.TxdExportSizes)
                        {
                            if (!TxdExportSizes.ContainsKey(txdPair.Key))
                            {
                                TxdExportSizes.Add(txdPair);
                            }
                        }
                    }

                    foreach (KeyValuePair<CollisionType, uint> pair in mapArchetype.CollisionTypePolygonCounts)
                    {
                        if (!CollisionTypePolygonCounts.ContainsKey(pair.Key))
                        {
                            CollisionTypePolygonCounts.Add(pair.Key, 0);
                        }
                        CollisionTypePolygonCounts[pair.Key] += pair.Value;
                    }

                    foreach (IShader shader in mapArchetype.Shaders)
                    {
                        if (!shaders.Contains(shader))
                        {
                            shaders.Add(shader);
                        }
                    }
                }
                else if (archetype is IStatedAnimArchetype)
                {
                    IStatedAnimArchetype animArchetype = (IStatedAnimArchetype)archetype;

                    PolygonCount += animArchetype.PolygonCount;
                    CollisionPolygonCount += animArchetype.CollisionPolygonCount;
                    if (animArchetype.PolygonCount == 0)
                    {
                        polycountZero.Add(animArchetype);
                    }

                    if (!geometryArchetypes.Contains(animArchetype))
                    {
                        ExportGeometrySize += animArchetype.ExportGeometrySize;
                        geometryArchetypes.Add(animArchetype);
                    }
                }
            }

            Shaders = shaders.ToArray();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="stat"></param>
        private void ProcessComplexRoomStat(RoomStatDto dto, StreamableStat stat)
        {
            if (stat == StreamableRoomStat.Shaders)
            {
                ProcessShaderStats(dto.ShaderStats);
            }
            else if (stat == StreamableRoomStat.TxdExportSizes)
            {
                ProcessTxdExportSizeStats(dto.TxdExportSizes);
            }
            else if (stat == StreamableRoomStat.CollisionTypePolygonCounts)
            {
                ProcessCollisionPolyCountStats(dto.CollisionPolygonCountBreakdown);
            }
            else if (stat == StreamableRoomStat.Entities)
            {
                ProcessEntityStats(dto.Entities);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="shaderDtos"></param>
        private void ProcessShaderStats(List<ShaderStatDto> shaderDtos)
        {
            List<IShader> shaders = new List<IShader>();

            foreach (ShaderStatDto shaderDto in shaderDtos)
            {
                Shader shader = new Shader(shaderDto.Name);

                foreach (TextureStatDto textureDto in shaderDto.TextureStats)
                {
                    shader.Textures.Add(new Texture(textureDto));
                }
                shaders.Add(shader);
            }

            Shaders = shaders.ToArray();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="txdDtos"></param>
        private void ProcessTxdExportSizeStats(List<TxdExportStatDto> txdDtos)
        {
            TxdExportSizes = new Dictionary<String, uint>();

            foreach (TxdExportStatDto txdDto in txdDtos)
            {
                TxdExportSizes.Add(txdDto.Name, txdDto.Size);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dtos"></param>
        private void ProcessCollisionPolyCountStats(List<CollisionPolyStatDto> collisionDtos)
        {
            CollisionTypePolygonCounts = new Dictionary<CollisionType, uint>();

            foreach (CollisionPolyStatDto collisionDto in collisionDtos)
            {
                CollisionTypePolygonCounts.Add((CollisionType)collisionDto.CollisionFlags, collisionDto.PolygonCount);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entityDtos"></param>
        private void ProcessEntityStats(List<BasicEntityStatDto> entityDtos)
        {
            ChildEntities = new ObservableCollection<IEntity>();
            ChildEntities.BeginUpdate();

            lock (ParentArchetype.ParentSection.MapHierarchy.ArchetypeLookup)
            {
                foreach (BasicEntityStatDto entityDto in entityDtos)
                {
                    if (ParentArchetype.ParentSection.MapHierarchy.ArchetypeLookup.ContainsKey(entityDto.ArchetypeName.ToLower()))
                    {
                        IArchetype archetype = ParentArchetype.ParentSection.MapHierarchy.ArchetypeLookup[entityDto.ArchetypeName.ToLower()];
                        IEntity entity = new Entity(entityDto.Name, archetype, this);
                    }
                    else
                    {
                        Log.Log__Warning("Archetype '{0}' wasn't found in the archetype look up for entity '{1}'.", entityDto.ArchetypeName, entityDto.Name);
                    }
                }
            }

            ChildEntities.EndUpdate();
        }
        #endregion // Private Methods
    } // Room
}
