﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Logging;
using RSG.Model.Common;
using RSG.Statistics.Client.ServiceClients;
using RSG.Statistics.Common.Dto.GameAssets;
using RSG.Statistics.Common.Dto.GameAssets.Bundles;

namespace RSG.Model.Weapon
{
    /// <summary>
    /// Collection of vehicles that come from a single build
    /// </summary>
    public class SingleBuildWeaponCollection : WeaponCollectionBase
    {
        #region Properties
        /// <summary>
        /// The build this vehicle collection is for.
        /// </summary>
        public string BuildIdentifier { get; protected set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="streamingCoordinator"></param>
        /// <param name="buildIdentifier"></param>
        public SingleBuildWeaponCollection(string buildIdentifier)
            : base()
        {
            BuildIdentifier = buildIdentifier;
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="statsToLoad"></param>
        public override void LoadStats(IEnumerable<StreamableStat> statsToLoad)
        {
            try
            {
                using (BuildClient client = new BuildClient())
                {
                    foreach (IWeapon weapon in Weapons)
                    {
                        // Check whether the stats are already loaded
                        if (!weapon.AreStatsLoaded(statsToLoad))
                        {
                            WeaponStatBundleDto bundle = client.GetWeaponStat(BuildIdentifier, weapon.Hash.ToString());
                            (weapon as DbWeapon).LoadStatsFromDatabase(bundle);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Log.Log__Exception(e, "Unhandled exception while retrieving stats from the database.");
            }
        }
        #endregion // Overrides
    } // SingleBuildWeaponCollection
} // RSG.Model.Weapon
