﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.IO;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace RSG.Model.Statistics.Captures
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class TestSession
    {
        #region Properties
        /// <summary>
        /// Platform this test session is for.
        /// </summary>
        [DataMember]
        public String Platform { get; private set; }

        /// <summary>
        /// Build config used for running this test session.
        /// </summary>
        [DataMember]
        public String BuildConfig { get; set; }

        /// <summary>
        /// Name of the level this test session was run in.
        /// </summary>
        [DataMember]
        public String LevelName { get; set; }

        /// <summary>
        /// Build this test session's results are from.
        /// </summary>
        [DataMember]
        public String Build { get; private set; }

        /// <summary>
        /// List of test results for the various zones that were captured as part of this session.
        /// </summary>
        [DataMember]
        public ICollection<TestResults> TestResults { get; private set; }

        /// <summary>
        /// Time when this test session ran
        /// </summary>
        [DataMember]
        public DateTime Timestamp { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="timestamp"></param>
        public TestSession(String filename, DateTime timestamp)
        {
            TestResults = new Collection<TestResults>();
            Timestamp = timestamp;

            using (Stream stream = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                Parse(stream);
            }
        }

        /// <summary>
        /// List of testsessions - parameterised merge constructor
        /// </summary>
        /// <param name="testSessions"></param>
        public TestSession(List<TestSession> testSessions)
        {
            TestResults = new Collection<TestResults>();

            if (testSessions.Count > 0)
            {
                foreach (TestSession testSession in testSessions)
                {
                    foreach (TestResults testResult in testSession.TestResults)
                    {
                        TestResults.Add(testResult);
                    }
                }
                Timestamp = testSessions.First().Timestamp;
                Build = testSessions.First().Build;
                Platform = testSessions.First().Platform;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="timestamp"></param>
        public TestSession(Stream stream, DateTime timestamp)
        {
            TestResults = new Collection<TestResults>();
            Timestamp = timestamp;
            Parse(stream);
        }
        #endregion // TestSession

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="stream"></param>
        private void Parse(Stream stream)
        {
            // Open up the document and start reading in the data it contains
            XDocument doc = XDocument.Load(stream);

            // Extract the platform
            XElement platformElement = doc.Root.Element("platform");
            if (platformElement != null)
            {
                Platform = platformElement.Value;
            }

            // Extract the build
            XElement buildElement = doc.Root.Element("buildversion");
            if (buildElement != null)
            {
                Build = buildElement.Value;
            }

            // Extract the individual tests
            XElement resultsElement = doc.Root.Element("results");
            if (resultsElement != null)
            {
                foreach (XElement testElement in resultsElement.Elements("Item"))
                {
                    TestResults test = new TestResults(testElement);
                    TestResults.Add(test);
                }
            }
        }
        #endregion // Private Methods
    } // TestSession
}
