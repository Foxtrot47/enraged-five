﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using RSG.Base.ConfigParser;
using RSG.Base.Logging;
using RSG.ManagedRage;
using RSG.Model.Common;
using RSG.Model.Map;

namespace RSG.Model.Report.Reports
{
    public class CarGenCSVReport : CSVReport, IDynamicMapReport
    {
        #region Constants
        private const String NAME = "Car Generators CSV Report";
        private const String DESCRIPTION = "Exports all the car gens (forced and not forced) into a .csv file."; 
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Determines whether the map data should be loaded
        /// </summary>
        public bool LoadMapData
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Determines whether the car generators should be loaded
        /// 
        /// </summary>
        public bool LoadCarGenarators
        {
            get { return true; }
        }
        #endregion // Properties

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public CarGenCSVReport()
            : base(NAME, DESCRIPTION)
        {
        }
        #endregion // Constructor

        #region Controller Methods
        /// <summary>
        /// Generate the report dynamically.
        /// </summary>
        public void Generate(ILevel level, RSG.Base.ConfigParser.ConfigGameView gv, IPlatformCollection platforms)
        {
            DateTime start = DateTime.Now;
            ExportCarCSVReport(Filename, level, gv);
            Log.Log__Message("Car gen CSV export complete. (report took {0} milliseconds to create.)", (DateTime.Now - start).TotalMilliseconds);
        }

        private void ExportCarCSVReport(String filename, ILevel level, ConfigGameView gv)
        {
            using (StreamWriter sw = new StreamWriter(filename))
            {
                WriteCsvHeader(sw);
                foreach (MapSection section in RSG.Model.Map.Util.Creation.InitialiseLevelCarGens(level, gv))
                {
                }
                foreach (MapSection section in RSG.Model.Map.Util.Enumerations.GetAllMapSections(level, null, true))
                {
                    if (section.CarGens == null)
                        continue;

                    foreach (CarGen carGen in section.CarGens)
                    {
                        WriteCsvCarGen(carGen, sw);
                    }   
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sw"></param>
        /// <param name="milo"></param>
        private static void WriteCsvHeader(StreamWriter sw)
        {
            string header = "Name, Container, Co-ords, Vehicle Type, High Priority";
            sw.WriteLine(header);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sw"></param>
        /// <param name="milo"></param>
        private static void WriteCsvCarGen(CarGen carGen, StreamWriter sw)
        {
            string fmt = "{0},{1},{2},{3},{4}";
            string coords = string.Format("{0}:{1}:{2}", carGen.Position.X, carGen.Position.Y, carGen.Position.Z);

            string type = null;
            if (!carGen.IsSpecificModel)
            {
                if (carGen.IsPolice)
                    type = "Generic Police";
                else if (carGen.IsAmbulance)
                    type = "Generic Ambulance";
                else if (carGen.IsFire)
                    type = "Generic Fire";
                else
                    type = "Generic";
            }
            else
            {
                type = carGen.ModelName;
            }

            string highPrority = carGen.GetAttributeValue("High Priority".ToLower(), false) == false ? "false" : "true";

            sw.WriteLine(string.Format(fmt, carGen.Name, carGen.Container.Name, coords, type, highPrority));
        }
        #endregion // Controller Methods
    } // RSG.Model.Report.Reports.CarGenCSVReport
} // RSG.Model.Report.Reports
