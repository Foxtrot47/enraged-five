﻿// --------------------------------------------------------------------------------------------
// <copyright file="IHasInitialValue{T}.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions.Members
{
    /// <summary>
    /// When implemented specifies that an instance of the parCodeGen member has an initial
    /// value when created.
    /// </summary>
    /// <typeparam name="T">
    /// The type that the initial value takes.
    /// </typeparam>
    public interface IHasInitialValue<T> : IMember
    {
        #region Properties
        /// <summary>
        /// Gets or sets the initial value that any instance of this member is set to.
        /// </summary>
        T InitialValue { get; set; }
        #endregion Properties
    } // RSG.Metadata.Model.Definitions.Members.IHasInitialValue{T} {Interface}
} // RSG.Metadata.Model.Definitions.Members {Namespace}
