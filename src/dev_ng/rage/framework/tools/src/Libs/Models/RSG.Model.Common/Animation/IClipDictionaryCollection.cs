﻿using System;
using System.Collections;
using System.Collections.Generic;
using RSG.Base.Collections;
using RSG.Model.Common;

namespace RSG.Model.Common.Animation
{
    /// <summary>
    /// IClipDictionaryCollection
    /// </summary>
    public interface IClipDictionaryCollection : IAsset, IEnumerable<IClipDictionary>, ICollection<IClipDictionary>
    {
        #region Properties
        /// <summary>
        /// Collection of ClipDictionaries
        /// </summary>
        ICollection<IClipDictionary> ClipDictionaries { get; }
        #endregion // Properties
    } // interface IClipDictionaryCollection
} // namespace RSG.Model.Common.Animation
