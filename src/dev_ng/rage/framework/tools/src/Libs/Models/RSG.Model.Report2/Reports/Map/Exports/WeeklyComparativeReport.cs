﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Report;
using RSG.Base.ConfigParser;
using RSG.Statistics.Common.Dto.ExportStats;
using RSG.Statistics.Common.Config;
using System.ServiceModel;
using RSG.Statistics.Client.ServiceClients;
using System.ServiceModel.Description;
using RSG.Base.Logging;
using System.IO;
using RSG.Base.Tasks;

namespace RSG.Model.Report.Reports.Map.Exports
{
    /// <summary>
    /// 
    /// </summary>
    public class WeeklyComparativeReport : CSVReport, IDynamicReport
    {
        #region Constants
        /// <summary>
        /// Report name/description.
        /// </summary>
        private const String c_name = "Weekly Export Stat Report";
        private const String c_description = "Report showing comparisons of section export statistics on a per week basis.";
        #endregion // Constants

        #region Properties

        /// <summary>
        /// Task to run for generating this report.
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (m_task == null)
                {
                    m_task = new ActionTask("Generating report", (context, progress) => GenerateReport((DynamicReportContext)context, progress));
                }
                return m_task;
            }
        }
        private ITask m_task;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public WeeklyComparativeReport()
            : base(c_name, c_description)
        {
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// Generate the report dynamically.
        /// </summary>
        private void GenerateReport(DynamicReportContext context, IProgress<TaskProgress> progress)
        {
            WeeklyExportStatDtos weeklyExportStatDtos = null;

            try
            {
                IStatisticsConfig config = new StatisticsConfig();

                EndpointAddress address = new EndpointAddress(String.Format("http://{0}:{1}/MapExportStats", config.DefaultServer.Address, config.DefaultServer.HttpPort));
                WebHttpBinding binding = new WebHttpBinding(WebHttpSecurityMode.None);
                binding.MaxReceivedMessageSize = 20971520;      // 20mb
                MapExportStatClient statClient = new MapExportStatClient(binding, address);
                statClient.Endpoint.Behaviors.Add(new WebHttpBehavior());

                // Get the data required to build the report from the stats server.
                weeklyExportStatDtos = statClient.GetWeeklyExportStats();

                statClient.Close();
            }
            catch (Exception e)
            {
                Log.Log__Exception(e, "Exception occurred while attempting to retrieve the daily map export stats from the statistics server.");
            }

            // Did we managed to retrieve the data?
            if (weeklyExportStatDtos != null)
            {
                // Generate the csv data streams
                Stream weeklyCsvStream = GenerateWeeklyCsv(weeklyExportStatDtos);

                // Save the users csv data to disk
                if (Filename != null)
                {
                    // Save the exports csv data to disk
                    string weeklyFilename = Path.Combine(Path.GetDirectoryName(Filename), Path.GetFileNameWithoutExtension(Filename) + "_weekly" + Path.GetExtension(Filename));
                    using (Stream fileStream = new FileStream(Filename, FileMode.Create))
                    {
                        weeklyCsvStream.Position = 0;
                        weeklyCsvStream.CopyTo(fileStream);
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="comparativeStats"></param>
        /// <returns></returns>
        private Stream GenerateWeeklyCsv(WeeklyExportStatDtos weeklyStats)
        {
            Stream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);

            // Group the results
            if (weeklyStats != null)
            {
                IList<DateTime> allDates = weeklyStats.Items.Select(item => item.StartOfWeek).ToList();
                IList<String> allSectionNames = weeklyStats.Items.SelectMany(item => item.SectionStats).Select(item => item.SectionName).Distinct().OrderBy(item => item).ToList();
                IDictionary<DateTime, Dictionary<String, double>> datesToStats = weeklyStats.Items.ToDictionary(item => item.StartOfWeek, item => item.SectionStats.ToDictionary(sitem => sitem.SectionName, sitem => sitem.AverageExportTime));

                // Create and output the header row
                StringBuilder sb = new StringBuilder();
                sb.Append("Section Name");

                foreach (DateTime startDate in allDates)
                {
                    DateTime endDate = startDate.AddDays(6);
                    sb.Append(String.Format(",{0}-{1}", startDate.ToString("d"), endDate.ToString("d")));
                }

                writer.WriteLine(sb.ToString());

                // Now output a row per section
                foreach (String sectionName in allSectionNames)
                {
                    sb.Clear();
                    sb.Append(sectionName);

                    foreach (DateTime date in allDates)
                    {
                        sb.Append(",");

                        if (datesToStats.ContainsKey(date) & datesToStats[date].ContainsKey(sectionName))
                        {
                            TimeSpan averageTime = new TimeSpan(0, 0, (int)datesToStats[date][sectionName]);
                            sb.Append(TruncateTimeSpan(averageTime));
                        }
                    }

                    writer.WriteLine(sb.ToString());
                }

                writer.Flush();
                stream.Position = 0;
            }

            return stream;
        }
        #endregion // Private Methods
    } // WeeklyComparativeReport
}
