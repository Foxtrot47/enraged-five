﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common
{
    /// <summary>
    /// Enumeration of all statistics that a character can have associated with it.
    /// </summary>
    [DependentStat(StreamableLevelStat.CharactersString)]
    public static class StreamableCharacterStat
    {
        public const String CategoryString = "89BECADF-F0F6-4BF1-9143-8551C844D551";
        public const String ShadersString = "51CBB965-AF4F-4815-B502-4C65B6B16CFF";
        public const String PolyCountString = "185B504E-849E-46EF-8B59-27AC4D5EA2B3";
        public const String CollisionCountString = "05ECCB33-8D6B-4626-A937-70DFE5F65E05";
        public const String BoneCountString = "6368A3AD-A0C2-4005-A300-1C483DF93586";
        public const String ClothCountString = "69495A7F-34C3-4276-BC9D-0B2193CB1E12";
        public const String ClothPolyCountString = "F5D1CECC-9065-4402-810A-D4EEB41E0297";
        public const String PlatformStatsString = "EAD79F93-F0AB-40F7-B573-9E1B06AFE867";

        public static readonly StreamableStat Category = new StreamableStat(CategoryString, true);
        public static readonly StreamableStat Shaders = new StreamableStat(ShadersString, true, true);
        public static readonly StreamableStat PolyCount = new StreamableStat(PolyCountString, true);
        public static readonly StreamableStat CollisionCount = new StreamableStat(CollisionCountString, true);
        public static readonly StreamableStat BoneCount = new StreamableStat(BoneCountString, true);
        public static readonly StreamableStat ClothCount = new StreamableStat(ClothCountString, true);
        public static readonly StreamableStat ClothPolyCount = new StreamableStat(ClothPolyCountString, true);
        public static readonly StreamableStat PlatformStats = new StreamableStat(PlatformStatsString, true, true);
    } // StreamableCharacterStat
}
