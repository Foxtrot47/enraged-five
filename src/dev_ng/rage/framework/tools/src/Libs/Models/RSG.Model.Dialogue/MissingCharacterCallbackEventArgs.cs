﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Dialogue
{
    public class MissingCharacterCallbackEventArgs
    {
        #region Properties

        /// <summary>
        /// Gets the name of the missing character
        /// </summary>
        public String Name
        { 
            get;
            protected set;
        }

        #endregion // Properties

        #region Constructor

        /// <summary>
        /// Default Constructor
        /// </summary>
        public MissingCharacterCallbackEventArgs(String characterName)
        {
            this.Name = characterName;
        }

        #endregion // Constructor
    } // MissingCharacterCallbackEventArgs
} // RSG.Model.Dialogue
