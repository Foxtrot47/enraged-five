﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common
{
    /// <summary>
    /// Enumeration of all statistics that a weapon can have associated with it
    /// </summary>
    [DependentStat(StreamableLevelStat.WeaponsString)]
    public static class StreamableWeaponStat
    {
        public const String CategoryString = "61FAC77D-6477-4FB4-9D69-D6F86D521376";
        public const String ShadersString = "435231EB-D63E-4717-8970-9E29EC011756";
        public const String PolyCountString = "8E23BC14-583C-4FCA-B16B-311DC2C722C5";
        public const String CollisionCountString = "2196F52F-6CB1-4057-9C33-7C872821CE50";
        public const String BoneCountString = "AB8687E9-F910-459B-967A-D5DDD8C0EF8F";
        public const String HasLodString = "5D5F8DA3-23F5-41F7-AA01-65E12789EC4D";
        public const String LodPolyCountString = "7B09768F-39D1-4525-BE99-0CB2E2928E8E";
        public const String GripCountString = "4664FFE3-D0BE-4BF2-92CE-6614260773A8";
        public const String MagazineCountString = "9644F4D1-F229-4BB8-BE33-8F1D81BBCED4";
        public const String AttachmentCountString = "D4B5280D-9926-4DBF-A013-6752E4311214";
        public const String PlatformStatsString = "8F668255-844A-4FD6-8526-50A13289DC53";

        public static readonly StreamableStat Category = new StreamableStat(CategoryString, true);
        public static readonly StreamableStat Shaders = new StreamableStat(ShadersString, true, true);
        public static readonly StreamableStat PolyCount = new StreamableStat(PolyCountString, true);
        public static readonly StreamableStat CollisionCount = new StreamableStat(CollisionCountString, true);
        public static readonly StreamableStat BoneCount = new StreamableStat(BoneCountString, true);
        public static readonly StreamableStat HasLod = new StreamableStat(HasLodString, true);
        public static readonly StreamableStat LodPolyCount = new StreamableStat(LodPolyCountString, true);
        public static readonly StreamableStat GripCount = new StreamableStat(GripCountString, true);
        public static readonly StreamableStat MagazineCount = new StreamableStat(MagazineCountString, true);
        public static readonly StreamableStat AttachmentCount = new StreamableStat(AttachmentCountString, true);
        public static readonly StreamableStat PlatformStats = new StreamableStat(PlatformStatsString, true, true);
    } // StreamableWeaponStat
}
