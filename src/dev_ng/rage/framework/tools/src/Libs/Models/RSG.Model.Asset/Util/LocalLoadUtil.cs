﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using RSG.Base.ConfigParser;
using RSG.Base.Configuration;
using RSG.ManagedRage;
using RSG.Platform;

namespace RSG.Model.Asset.Util
{
    /// <summary>
    /// 
    /// </summary>
    public static class LocalLoadUtil
    {
        /// <summary>
        /// Uses the config's target info to convert the independent file path to per platform files and loads the resulting pack files.
        /// </summary>
        /// <param name="independentFilePath"></param>
        /// <param name="gv"></param>
        /// <returns>Dictionary mapping platform -> packfile containing all the loaded files</returns>
        public static IDictionary<RSG.Platform.Platform, Packfile> OpenPackFiles(String independentFilePath, ConfigGameView gv, IConfig config)
        {
            Dictionary<RSG.Platform.Platform, Packfile> packFiles = new Dictionary<RSG.Platform.Platform, Packfile>();
            List<ConfigTargetInfo> allTargets = gv.Targets(gv.Branch);

            foreach (ConfigTargetInfo targetInfo in allTargets)
            {
                RSG.Platform.Platform platform = PlatformUtils.PlatformFromString(targetInfo.Name);
                var target = config.Project.DefaultBranch.Targets[platform];

                // Convert the platform independent path to a per platform build path
                string platformFilePath = RSG.Pipeline.Services.Platform.PlatformPathConversion.ConvertAndRemapFilenameToPlatform(target, independentFilePath);

                // Load the pack file
                packFiles[platform] = new Packfile();
                packFiles[platform].Load(platformFilePath);
            }
            return packFiles;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="packFiles"></param>
        public static void ClosePackFiles(IDictionary<RSG.Platform.Platform, Packfile> packFiles)
        {
            foreach (KeyValuePair<RSG.Platform.Platform, Packfile> pair in packFiles)
            {
                pair.Value.Close();
            }
        }

        /// <summary>
        /// Generates the path to a particular node by working its way up the tree
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public static string GeneratePathForNode(ContentNode node)
        {
            string result = node.Name + ".zip";

            while (node != null)
            {
                if (node is ContentNodeGroup)
                {
                    ContentNodeGroup groupNode = node as ContentNodeGroup;

                    if (!String.IsNullOrEmpty(groupNode.RelativePath))
                    {
                        result = Path.Combine(groupNode.RelativePath, result);
                    }
                }

                node = node.Parent;
            }

            return Path.GetFullPath(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static string GeneratePathForNodes(IList<ContentNode> nodes)
        {
            if (nodes.Count > 0)
            {
                ContentNode node = nodes[0];
                string result = node.Name + ".zip";

                while (node != null)
                {
                    if (node is ContentNodeGroup)
                    {
                        ContentNodeGroup groupNode = node as ContentNodeGroup;

                        if (!String.IsNullOrEmpty(groupNode.RelativePath))
                        {
                            result = Path.Combine(groupNode.RelativePath, result);
                        }
                    }

                    node = node.Parent;
                }

                return Path.GetFullPath(result);
            }
            else
            {
                return "";
            }
        }
    }
}
