﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.Model.Map.SceneOverride
{
    /// <summary>
    /// An XML based implementation of ISceneOverrideRepository
    /// </summary>
    internal class XmlSceneOverrideRepository : ISceneOverrideRepository
    {
        internal XmlSceneOverrideRepository(string dataDirectory)
        {
            if (!Directory.Exists(dataDirectory))
                Directory.CreateDirectory(dataDirectory);

            processedContentPathname_ = Path.Combine(dataDirectory, "processed_content.xml");
            lodOverridesPathname_ = Path.Combine(dataDirectory, "lod_overrides.xml");
            entityDeletesPathname_ = Path.Combine(dataDirectory, "orphan_hd_delete_overrides.xml");
            attributeOverridesPathname_ = Path.Combine(dataDirectory, "attribute_overrides.xml");
            usersPathname_ = Path.Combine(dataDirectory, "users.xml");
        }

        // Get a record (or NULL) given a key
        public ProcessedContentRecord GetProcessedContent(int processedContentId)
        {
            Dictionary<int, ProcessedContentRecord> records = LoadProcessedContentRecords();
            if (records.ContainsKey(processedContentId))
                return records[processedContentId];

            return null;
        }

        public LODOverrideRecord GetLODOverride(EntityKey entityKey)
        {
            Dictionary<EntityKey, LODOverrideRecord> records = LoadLODOverrideRecords();
            if (records.ContainsKey(entityKey))
                return records[entityKey];

            return null;
        }

        public EntityDeleteRecord GetEntityDelete(EntityKey entityKey)
        {
            Dictionary<EntityKey, EntityDeleteRecord> records = LoadEntityDeleteRecords();
            if (records.ContainsKey(entityKey))
                return records[entityKey];

            return null;
        }

        public AttributeOverrideRecord GetAttributeOverride(EntityKey entityKey)
        {
            Dictionary<EntityKey, AttributeOverrideRecord> records = LoadAttributeOverrideRecords();
            if (records.ContainsKey(entityKey))
                return records[entityKey];

            return null;
        }

        public UserRecord GetUser(int userId)
        {
            Dictionary<int, UserRecord> records = LoadUserRecords();
            if (records.ContainsKey(userId))
                return records[userId];

            return null;
        }

        // Create a record, yielding a key
        public int CreateProcessedContent(ProcessedContentRecord processedContent)
        {
            Dictionary<int, ProcessedContentRecord> records = LoadProcessedContentRecords();

            // Determine key
            int newKey = 1;
            if (records.Count > 0)
                newKey = records.Keys.Max() + 1;

            processedContent.Id = newKey;
            records.Add(newKey, processedContent);

            SaveProcessedContentRecords(records);

            return newKey;
        }

        public EntityKey CreateOrUpdateLODOverride(LODOverrideRecord record)
        {
            Dictionary<EntityKey, LODOverrideRecord> records = LoadLODOverrideRecords();

            EntityKey newKey = new EntityKey(record.ContentId, record.Guid, record.Hash);

            if (records.ContainsKey(newKey))
            {
                records[newKey] = record;
            }
            else
            {
                records.Add(newKey, record);
            }

            SaveLODOverrideRecords(records);

            return newKey; 
        }

        public IEnumerable<EntityKey> CreateOrUpdateLODOverrides(IEnumerable<LODOverrideRecord> recordsToUpdate)
        {
            Dictionary<EntityKey, LODOverrideRecord> records = LoadLODOverrideRecords();

            List<EntityKey> newKeys = new List<EntityKey>();
            foreach (LODOverrideRecord recordToUpdate in recordsToUpdate)
            {
                EntityKey newKey = new EntityKey(recordToUpdate.ContentId, recordToUpdate.Guid, recordToUpdate.Hash);

                if (records.ContainsKey(newKey))
                {
                    if (recordToUpdate.Distance != -1)
                        records[newKey].Distance = recordToUpdate.Distance;
                    if (recordToUpdate.ChildDistance != -1)
                        records[newKey].ChildDistance = recordToUpdate.ChildDistance;
                }
                else
                {
                    records.Add(newKey, recordToUpdate);
                }

                newKeys.Add(newKey);
            }

            SaveLODOverrideRecords(records);

            return newKeys; 
        }

        public EntityKey CreateOrUpdateEntityDelete(EntityDeleteRecord record)
        {
            Dictionary<EntityKey, EntityDeleteRecord> records = LoadEntityDeleteRecords();

            EntityKey newKey = new EntityKey(record.ContentId, record.Guid, record.Hash);

            if (records.ContainsKey(newKey))
            {
                records[newKey] = record;
            }
            else
            {
                records.Add(newKey, record);
            }

            SaveEntityDeleteRecords(records);

            return newKey;
        }

        public IEnumerable<EntityKey> CreateOrUpdateEntityDeletes(IEnumerable<EntityDeleteRecord> recordsToUpdate)
        {
            Dictionary<EntityKey, EntityDeleteRecord> records = LoadEntityDeleteRecords();

            List<EntityKey> newKeys = new List<EntityKey>();
            foreach (EntityDeleteRecord recordToUpdate in recordsToUpdate)
            {
                EntityKey newKey = new EntityKey(recordToUpdate.ContentId, recordToUpdate.Guid, recordToUpdate.Hash);

                if (records.ContainsKey(newKey))
                {
                    records[newKey] = recordToUpdate;
                }
                else
                {
                    records.Add(newKey, recordToUpdate);
                }

                newKeys.Add(newKey);
            }

            SaveEntityDeleteRecords(records);

            return newKeys;
        }

        public EntityKey CreateOrUpdateAttributeOverride(AttributeOverrideRecord record)
        {
            Dictionary<EntityKey, AttributeOverrideRecord> records = LoadAttributeOverrideRecords();

            EntityKey newKey = new EntityKey(record.ContentId, record.Guid, record.Hash);

            if (records.ContainsKey(newKey))
            {
                records[newKey] = record;
            }
            else
            {
                records.Add(newKey, record);
            }

            SaveAttributeOverrideRecords(records);

            return newKey;
        }

        public IEnumerable<EntityKey> CreateOrUpdateAttributeOverrides(IEnumerable<AttributeOverrideRecord> recordsToUpdate)
        {
            Dictionary<EntityKey, AttributeOverrideRecord> records = LoadAttributeOverrideRecords();

            List<EntityKey> newKeys = new List<EntityKey>();
            foreach (AttributeOverrideRecord recordToUpdate in recordsToUpdate)
            {
                EntityKey newKey = new EntityKey(recordToUpdate.ContentId, recordToUpdate.Guid, recordToUpdate.Hash);

                if (records.ContainsKey(newKey))
                {
                    records[newKey] = recordToUpdate;
                }
                else
                {
                    records.Add(newKey, recordToUpdate);
                }

                newKeys.Add(newKey);
            }

            SaveAttributeOverrideRecords(records);

            return newKeys;
        }

        public int CreateUser(UserRecord user)
        {
            Dictionary<int, UserRecord> records = LoadUserRecords();

            // Determine key
            int newKey = 1;
            if (records.Count > 0)
                newKey = records.Keys.Max() + 1;

            user.Id = newKey;
            records.Add(newKey, user);

            SaveUserRecords(records);

            return newKey;
        }

        // Helper methods
        public int FindProcessedContentKey(string processedContentName)
        {
            Dictionary<int, ProcessedContentRecord> records = LoadProcessedContentRecords();

            if (records.Where(kvp => kvp.Value.Name == processedContentName).Count() == 0)
                return -1;

            return records.First(kvp => kvp.Value.Name == processedContentName).Value.Id;
        }

        public int FindUserKey(string userName)
        {
            Dictionary<int, UserRecord> records = LoadUserRecords();

            if (records.Where(kvp => kvp.Value.Name == userName).Count() == 0)
                return -1;

            return records.First(kvp => kvp.Value.Name == userName).Value.Id;
        }

        public LODOverrideRecord[] GetLODOverridesForProcessedContent(int processedContentId)
        {
            Dictionary<EntityKey, LODOverrideRecord> records = LoadLODOverrideRecords();

            var query = from recordPair in records
                        where recordPair.Key.ContentId == processedContentId
                        select recordPair.Value;

            return query.ToArray();
        }

        public EntityDeleteRecord[] GetEntityDeletesForProcessedContent(int processedContentId)
        {
            Dictionary<EntityKey, EntityDeleteRecord> records = LoadEntityDeleteRecords();

            var query = from recordPair in records
                        where recordPair.Key.ContentId == processedContentId
                        select recordPair.Value;

            return query.ToArray();
        }

        public AttributeOverrideRecord[] GetAttributeOverridesForProcessedContent(int processedContentId)
        {
            Dictionary<EntityKey, AttributeOverrideRecord> records = LoadAttributeOverrideRecords();

            var query = from recordPair in records
                        where recordPair.Key.ContentId == processedContentId
                        select recordPair.Value;

            return query.ToArray();
        }

        #region IO
        private Dictionary<int, ProcessedContentRecord> LoadProcessedContentRecords()
        {
            Dictionary<int, ProcessedContentRecord> processedContent = new Dictionary<int, ProcessedContentRecord>();

            try
            {
                XDocument document = XDocument.Load(processedContentPathname_);
                foreach (XElement recordElement in document.Root.Elements("ProcessedContent"))
                {
                    processedContent.Add(
                        Int32.Parse(recordElement.Attribute("Id").Value),
                        new ProcessedContentRecord(Int32.Parse(recordElement.Attribute("Id").Value), recordElement.Attribute("Name").Value));
                }
            }
            catch (System.IO.FileNotFoundException) { }

            return processedContent;
        }

        private Dictionary<EntityKey, LODOverrideRecord> LoadLODOverrideRecords()
        {
            Dictionary<EntityKey, LODOverrideRecord> overrides = new Dictionary<EntityKey, LODOverrideRecord>(new EntityKey.EntityKeyComparer());

            try
            {
                XDocument document = XDocument.Load(lodOverridesPathname_);
                foreach (XElement recordElement in document.Root.Elements("LODOverride"))
                {
                    String modelName = "????";
                    if (recordElement.Attribute("ModelName") != null)
                        modelName = recordElement.Attribute("ModelName").Value;

                    overrides.Add(
                        new EntityKey(
                            Int32.Parse(recordElement.Attribute("ContentId").Value),
                            UInt32.Parse(recordElement.Attribute("Guid").Value),
                            UInt32.Parse(recordElement.Attribute("Hash").Value)),
                        new LODOverrideRecord(
                            Int32.Parse(recordElement.Attribute("ContentId").Value),
                            UInt32.Parse(recordElement.Attribute("Hash").Value),
                            UInt32.Parse(recordElement.Attribute("Guid").Value),
                            modelName,
                            Single.Parse(recordElement.Attribute("PosX").Value),
                            Single.Parse(recordElement.Attribute("PosY").Value),
                            Single.Parse(recordElement.Attribute("PosZ").Value),
                            Int32.Parse(recordElement.Attribute("UserId").Value),
                            Single.Parse(recordElement.Attribute("Distance").Value),
                            Single.Parse(recordElement.Attribute("ChildDistance").Value),
                            Int64.Parse(recordElement.Attribute("DateTimeSubmitted").Value)));
                }
            }
            catch (System.IO.FileNotFoundException) { }

            return overrides;
        }

        private Dictionary<EntityKey, EntityDeleteRecord> LoadEntityDeleteRecords()
        {
            Dictionary<EntityKey, EntityDeleteRecord> overrides = new Dictionary<EntityKey, EntityDeleteRecord>(new EntityKey.EntityKeyComparer());

            try
            {
                XDocument document = XDocument.Load(entityDeletesPathname_);
                foreach (XElement recordElement in document.Root.Elements("EntityDelete"))
                {
                    String modelName = "????";
                    if (recordElement.Attribute("ModelName") != null)
                        modelName = recordElement.Attribute("ModelName").Value;

                    overrides.Add(
                        new EntityKey(
                            Int32.Parse(recordElement.Attribute("ContentId").Value),
                            UInt32.Parse(recordElement.Attribute("Guid").Value),
                            UInt32.Parse(recordElement.Attribute("Hash").Value)),
                        new EntityDeleteRecord(
                            Int32.Parse(recordElement.Attribute("ContentId").Value),
                            UInt32.Parse(recordElement.Attribute("Hash").Value),
                            UInt32.Parse(recordElement.Attribute("Guid").Value),
                            modelName,
                            Single.Parse(recordElement.Attribute("PosX").Value),
                            Single.Parse(recordElement.Attribute("PosY").Value),
                            Single.Parse(recordElement.Attribute("PosZ").Value),
                            Int32.Parse(recordElement.Attribute("UserId").Value),
                            Int64.Parse(recordElement.Attribute("DateTimeSubmitted").Value)));
                }
            }
            catch (System.IO.FileNotFoundException) { }

            return overrides;
        }

        private Dictionary<EntityKey, AttributeOverrideRecord> LoadAttributeOverrideRecords()
        {
            Dictionary<EntityKey, AttributeOverrideRecord> overrides = new Dictionary<EntityKey, AttributeOverrideRecord>(new EntityKey.EntityKeyComparer());

            try
            {
                XDocument document = XDocument.Load(attributeOverridesPathname_);
                foreach (XElement recordElement in document.Root.Elements("AttributeOverride"))
                {
                    String modelName = "????";
                    if (recordElement.Attribute("ModelName") != null)
                        modelName = recordElement.Attribute("ModelName").Value;

                    bool dontRenderInReflections = false;
                    if (recordElement.Attribute("DontRenderInReflections") != null)
                        dontRenderInReflections = Boolean.Parse(recordElement.Attribute("DontRenderInReflections").Value);
                    bool onlyRenderInReflections = false;
                    if (recordElement.Attribute("OnlyRenderInReflections") != null)
                        onlyRenderInReflections = Boolean.Parse(recordElement.Attribute("OnlyRenderInReflections").Value);
                    bool dontRenderInWaterReflections = false;
                    if (recordElement.Attribute("DontRenderInWaterReflections") != null)
                        dontRenderInWaterReflections = Boolean.Parse(recordElement.Attribute("DontRenderInWaterReflections").Value);
                    bool onlyRenderInWaterReflections = false;
                    if (recordElement.Attribute("OnlyRenderInWaterReflections") != null)
                        onlyRenderInWaterReflections = Boolean.Parse(recordElement.Attribute("OnlyRenderInWaterReflections").Value);
                    bool dontRenderInMirrorReflections = false;
                    if (recordElement.Attribute("DontRenderInMirrorReflections") != null)
                        dontRenderInMirrorReflections = Boolean.Parse(recordElement.Attribute("DontRenderInMirrorReflections").Value);
                    bool onlyRenderInMirrorReflections = false;
                    if (recordElement.Attribute("OnlyRenderInMirrorReflections") != null)
                        onlyRenderInMirrorReflections = Boolean.Parse(recordElement.Attribute("OnlyRenderInMirrorReflections").Value);

                    overrides.Add(
                        new EntityKey(
                            Int32.Parse(recordElement.Attribute("ContentId").Value),
                            UInt32.Parse(recordElement.Attribute("Guid").Value),
                            UInt32.Parse(recordElement.Attribute("Hash").Value)),
                        new AttributeOverrideRecord(
                            Int32.Parse(recordElement.Attribute("ContentId").Value),
                            UInt32.Parse(recordElement.Attribute("Hash").Value),
                            UInt32.Parse(recordElement.Attribute("Guid").Value),
                            modelName,
                            Single.Parse(recordElement.Attribute("PosX").Value),
                            Single.Parse(recordElement.Attribute("PosY").Value),
                            Single.Parse(recordElement.Attribute("PosZ").Value),
                            Int32.Parse(recordElement.Attribute("UserId").Value),
                            Boolean.Parse(recordElement.Attribute("DontCastShadows").Value),
                            Boolean.Parse(recordElement.Attribute("DontRenderInShadows").Value),
                            dontRenderInReflections,
                            onlyRenderInReflections,
                            dontRenderInWaterReflections,
                            onlyRenderInWaterReflections,
                            dontRenderInMirrorReflections,
                            onlyRenderInMirrorReflections,
                            Boolean.Parse(recordElement.Attribute("StreamingPriorityLow").Value),
                            Int32.Parse(recordElement.Attribute("Priority").Value),
                            Int64.Parse(recordElement.Attribute("DateTimeSubmitted").Value)));
                }
            }
            catch (System.IO.FileNotFoundException) { }

            return overrides;
        }

        private Dictionary<int, UserRecord> LoadUserRecords()
        {
            Dictionary<int, UserRecord> users = new Dictionary<int, UserRecord>();

            try
            {
                XDocument document = XDocument.Load(usersPathname_);
                foreach (XElement recordElement in document.Root.Elements("User"))
                {
                    users.Add(
                        Int32.Parse(recordElement.Attribute("Id").Value),
                        new UserRecord(
                            Int32.Parse(recordElement.Attribute("Id").Value),
                            recordElement.Attribute("Name").Value));
                }
            }
            catch (System.IO.FileNotFoundException) { }

            return users;
        }

        private void SaveProcessedContentRecords(Dictionary<int, ProcessedContentRecord> records)
        {
            XDocument usersDocument = new XDocument(
                new XElement("Root",
                    records.Select(kvp => new XElement("ProcessedContent", new XAttribute("Id", kvp.Key), new XAttribute("Name", kvp.Value.Name)))
                )
            );
            usersDocument.Save(processedContentPathname_);
        }

        private void SaveLODOverrideRecords(Dictionary<EntityKey, LODOverrideRecord> records)
        {
            XDocument usersDocument = new XDocument(
                new XElement("Root",
                    records.Select(kvp => new XElement("LODOverride",
                        new XAttribute("ContentId", kvp.Key.ContentId),
                        new XAttribute("Guid", kvp.Key.Guid),
                        new XAttribute("Hash", kvp.Key.Hash),
                        new XAttribute("ModelName", kvp.Value.ModelName),
                        new XAttribute("PosX", kvp.Value.PosX),
                        new XAttribute("PosY", kvp.Value.PosY),
                        new XAttribute("PosZ", kvp.Value.PosZ),
                        new XAttribute("Distance", kvp.Value.Distance),
                        new XAttribute("ChildDistance", kvp.Value.ChildDistance),
                        new XAttribute("UserId", kvp.Value.UserId),
                        new XAttribute("DateTimeSubmitted", kvp.Value.UtcFileTimeSubmitted.ToString())))
                )
            );
            usersDocument.Save(lodOverridesPathname_);
        }

        private void SaveEntityDeleteRecords(Dictionary<EntityKey, EntityDeleteRecord> records)
        {
            XDocument usersDocument = new XDocument(
                new XElement("Root",
                    records.Select(kvp => new XElement("EntityDelete",
                        new XAttribute("ContentId", kvp.Key.ContentId),
                        new XAttribute("Guid", kvp.Key.Guid),
                        new XAttribute("Hash", kvp.Key.Hash),
                        new XAttribute("ModelName", kvp.Value.ModelName),
                        new XAttribute("PosX", kvp.Value.PosX),
                        new XAttribute("PosY", kvp.Value.PosY),
                        new XAttribute("PosZ", kvp.Value.PosZ),
                        new XAttribute("UserId", kvp.Value.UserId),
                        new XAttribute("DateTimeSubmitted", kvp.Value.UtcFileTimeSubmitted.ToString())))
                )
            );
            usersDocument.Save(entityDeletesPathname_);
        }

        private void SaveAttributeOverrideRecords(Dictionary<EntityKey, AttributeOverrideRecord> records)
        {
            XDocument usersDocument = new XDocument(
                new XElement("Root",
                    records.Select(kvp => new XElement("AttributeOverride",
                        new XAttribute("ContentId", kvp.Key.ContentId),
                        new XAttribute("Guid", kvp.Key.Guid),
                        new XAttribute("Hash", kvp.Key.Hash),
                        new XAttribute("ModelName", kvp.Value.ModelName),
                        new XAttribute("PosX", kvp.Value.PosX),
                        new XAttribute("PosY", kvp.Value.PosY),
                        new XAttribute("PosZ", kvp.Value.PosZ),
                        new XAttribute("DontCastShadows", kvp.Value.DontCastShadows),
                        new XAttribute("DontRenderInShadows", kvp.Value.DontRenderInShadows),
                        new XAttribute("DontRenderInReflections", kvp.Value.DontRenderInReflections),
                        new XAttribute("OnlyRenderInReflections", kvp.Value.OnlyRenderInReflections),
                        new XAttribute("DontRenderInWaterReflections", kvp.Value.DontRenderInWaterReflections),
                        new XAttribute("OnlyRenderInWaterReflections", kvp.Value.OnlyRenderInWaterReflections),
                        new XAttribute("DontRenderInMirrorReflections", kvp.Value.DontRenderInMirrorReflections),
                        new XAttribute("OnlyRenderInMirrorReflections", kvp.Value.OnlyRenderInMirrorReflections),
                        new XAttribute("StreamingPriorityLow", kvp.Value.StreamingPriorityLow),
                        new XAttribute("Priority", kvp.Value.Priority),
                        new XAttribute("UserId", kvp.Value.UserId),
                        new XAttribute("DateTimeSubmitted", kvp.Value.UtcFileTimeSubmitted.ToString())))
                )
            );
            usersDocument.Save(attributeOverridesPathname_);
        }

        private void SaveUserRecords(Dictionary<int, UserRecord> records)
        {
            XDocument usersDocument = new XDocument(
                new XElement("Root",
                    records.Select(kvp => new XElement("User", new XAttribute("Id", kvp.Key), new XAttribute("Name", kvp.Value.Name)))
                )
            );
            usersDocument.Save(usersPathname_);
        }
        #endregion IO

        private string processedContentPathname_;
        private string lodOverridesPathname_;
        private string entityDeletesPathname_;
        private string attributeOverridesPathname_;
        private string usersPathname_;
    }
}
