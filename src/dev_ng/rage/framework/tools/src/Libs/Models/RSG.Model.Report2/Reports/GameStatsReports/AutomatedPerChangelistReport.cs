﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using RSG.Model.Statistics.Captures;
using RSG.SourceControl.Perforce;
using RSG.Model.Report.Reports.GameStatsReports.Tests;

namespace RSG.Model.Report.Reports.GameStatsReports
{
    /// <summary>
    /// 
    /// </summary>
    public class AutomatedPerChangelistReport : AutomatedGameStatsReportBase
    {
        #region Constants
        private const String NAME = "Automated Report (Per Changelist)";
        private const String DESC = "Automated report that shows information regarding various stats on a per changelist basis.";
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="desc"></param>
        public AutomatedPerChangelistReport()
            : base(NAME, DESC)
        {
            Mode = GameStatReportMode.PerChangelist;

            // Add the list of smoke tests
            SmokeTests.Add(new FpsSmokeTest());
            SmokeTests.Add(new CpuSmokeTest());
            SmokeTests.Add(new GpuSmokeTest());
        }
        #endregion // Constructor(s)

        #region Methods
        protected override P4 GetPerforceConnection()
        {
            return null;
        }
        #endregion
    } // AutomatedPerChangelistReport
}
