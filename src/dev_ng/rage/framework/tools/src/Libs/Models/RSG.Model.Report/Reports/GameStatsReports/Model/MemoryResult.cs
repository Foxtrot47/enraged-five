﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.Model.Report.Reports.GameStatsReports.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class MemoryResult
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public string Name
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public MemoryStat GameVirtual
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public MemoryStat GamePhysical
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public MemoryStat ResourceVirtual
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public MemoryStat ResourcePhysical
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public MemoryResult(XElement element)
        {
            // Extract the various stats
            XElement nameElement = element.Element("name");
            if (nameElement != null)
            {
                Name = nameElement.Value;
            }

            XElement gvElement = element.Element("gameVirtual");
            if (gvElement != null)
            {
                GameVirtual = GetMemoryStat(gvElement);
            }

            XElement gpElement = element.Element("gamePhysical");
            if (gpElement != null)
            {
                GamePhysical = GetMemoryStat(gpElement);
            }

            XElement rvElement = element.Element("resourceVirtual");
            if (rvElement != null)
            {
                ResourceVirtual = GetMemoryStat(rvElement);
            }

            XElement rpElement = element.Element("resourcePhysical");
            if (rpElement != null)
            {
                ResourcePhysical = GetMemoryStat(rpElement);
            }
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        private MemoryStat GetMemoryStat(XElement element)
        {
            uint min = 0;
            uint max = 0;
            float average = 0.0f;

            XElement minElement = element.Element("min");
            if (minElement != null && minElement.Attribute("value") != null)
            {
                min = UInt32.Parse(minElement.Attribute("value").Value);
            }

            XElement maxElement = element.Element("max");
            if (maxElement != null && maxElement.Attribute("value") != null)
            {
                max = UInt32.Parse(maxElement.Attribute("value").Value);
            }

            XElement avgElement = element.Element("average");
            if (avgElement != null && avgElement.Attribute("value") != null)
            {
                average = Single.Parse(avgElement.Attribute("value").Value);
            }

            return new MemoryStat(min, max, average);
        }
        #endregion // Private Methods
    } // MemoryResult
}
