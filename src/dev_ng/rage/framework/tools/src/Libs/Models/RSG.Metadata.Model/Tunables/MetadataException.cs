﻿// --------------------------------------------------------------------------------------------
// <copyright file="MetadataException.cs" company="Rockstar">
//     Copyright © Rockstar Games 2012. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Tunables
{
    using System;
    using System.Linq.Expressions;
    using System.Xml;
    using RSG.Base.Extensions;
    using RSG.Editor;

    /// <summary>
    /// Returns detailed information about the last exception thrown in the metadata library.
    /// </summary>
    public class MetadataException : Exception
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MetadataException"/> class.
        /// </summary>
        /// <param name="message">
        /// The error message that explains the reason for the exception.
        /// </param>
        public MetadataException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="MetadataException"/> class.
        /// </summary>
        /// <param name="message">
        /// The error message that explains the reason for the exception.
        /// </param>
        /// <param name="innerException">
        /// The exception that is a cause of this exception.
        /// </param>
        internal MetadataException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="MetadataException"/> class.
        /// </summary>
        /// <param name="message">
        /// The error message that explains the reason for the exception.
        /// </param>
        /// <param name="args">
        /// An object array that contains zero or more objects to format.
        /// </param>
        public MetadataException(string message, params object[] args)
            : base(message.FormatInvariant(args))
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="MetadataException"/> class for a null
        /// reference exception.
        /// </summary>
        /// <param name="argument">
        /// A reference to the argument that is null.
        /// </param>
        public MetadataException(Expression<SimpleFunctionDelegate> argument)
            : base(GetMessage(argument))
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Gets the name of the specified expressions body.
        /// </summary>
        /// <param name="expression">
        /// The expression that the name will be returned for.
        /// </param>
        /// <returns>
        /// The name of the specified expressions body.
        /// </returns>
        private static string GetMemberName(Expression<SimpleFunctionDelegate> expression)
        {
            MemberExpression expressionBody = (MemberExpression)expression.Body;
            return expressionBody.Member.Name;
        }

        /// <summary>
        /// Gets the message to go with this exception.
        /// </summary>
        /// <param name="expression">
        /// The argument whose name will be included in the message.
        /// </param>
        /// <returns>
        /// The message to go with this exception.
        /// </returns>
        private static string GetMessage(Expression<SimpleFunctionDelegate> expression)
        {
            string memberName = GetMemberName(expression);
            return "Value cannot be null. Argument name: {0}.".FormatInvariant(memberName);
        }
        #endregion Methods
    } // RSG.Metadata.Model.Tunables.MetadataException {Class}
} // RSG.Metadata.Model.Tunables {Namespace}
