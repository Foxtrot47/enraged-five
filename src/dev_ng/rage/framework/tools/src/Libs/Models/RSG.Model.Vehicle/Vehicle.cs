﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using RSG.Base.Attributes;
using RSG.Base.Extensions;
using RSG.ManagedRage;
using RSG.Model.Asset;
using RSG.Model.Common;
using RSG.Platform;
using RSG.SceneXml;
using RSG.SceneXml.Material;
using RSG.Statistics.Common.Dto.GameAssets.Bundles;

namespace RSG.Model.Vehicle
{
    /// <summary>
    /// Vehicle asset.
    /// </summary>
    public abstract class Vehicle : StreamableAssetContainerBase, IVehicle
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// The collection this character is a part of
        /// </summary>
        [Browsable(false)]
        public IVehicleCollection ParentCollection
        {
            get;
            protected set;
        }

        /// <summary>
        /// Name of the vehicle as it appears in game.
        /// </summary>
        [StreamableStat(StreamableVehicleStat.FriendlyNameString)]
        public String FriendlyName
        {
            get
            {
                CheckStatLoaded(StreamableVehicleStat.FriendlyName);
                return m_friendlyName;
            }
            protected set
            {
                SetPropertyValue(ref m_friendlyName, value, "FriendlyName");
                SetStatLodaded(StreamableVehicleStat.FriendlyName, true);
            }
        }
        private String m_friendlyName;

        /// <summary>
        /// Name of the vehicle as it appears in game.
        /// </summary>
        [StreamableStat(StreamableVehicleStat.GameNameString)]
        public String GameName
        {
            get
            {
                CheckStatLoaded(StreamableVehicleStat.GameName);
                return m_gameName;
            }
            protected set
            {
                SetPropertyValue(ref m_gameName, value, "GameName");
                SetStatLodaded(StreamableVehicleStat.GameName, true);
            }
        }
        private String m_gameName;

        /// <summary>
        /// Vehicle category classification.
        /// </summary>
        [StreamableStat(StreamableVehicleStat.CategoryString)]
        public VehicleCategory Category
        {
            get
            {
                CheckStatLoaded(StreamableVehicleStat.Category);
                return m_category;
            }
            protected set
            {
                SetPropertyValue(ref m_category, value, "Category");
                SetStatLodaded(StreamableVehicleStat.Category, true);
            }
        }
        private VehicleCategory m_category;

        /// <summary>
        /// Array of vehicle textures.
        /// </summary>
        [Browsable(false)]
        public ITexture[] Textures
        {
            get
            {
                return m_textures;
            }
            private set
            {
                SetPropertyValue(ref m_textures, value, "Textures", "TextureCount");
            }
        }
        private ITexture[] m_textures;

        /// <summary>
        /// Texture count (exposed for the property grid)
        /// </summary>
        public int TextureCount
        {
            get
            {
                return (Textures != null ? Textures.Count() : 0);
            }
        }

        /// <summary>
        /// Array of vehicle shaders.
        /// </summary>
        [Browsable(false)]
        [StreamableStat(StreamableVehicleStat.ShadersString)]
        public IShader[] Shaders
        {
            get
            {
                CheckStatLoaded(StreamableVehicleStat.Shaders);
                return m_shaders;
            }
            protected set
            {
                if (SetPropertyValue(ref m_shaders, value, "Shaders", "ShaderCount"))
                {
                    if (m_shaders != null)
                        Textures = m_shaders.SelectMany(shader => shader.Textures).Distinct().OrderBy(item => item.Name).ToArray();
                    else
                        Textures = null;
                }
                SetStatLodaded(StreamableVehicleStat.Shaders, true);
            }
        }
        private IShader[] m_shaders;

        /// <summary>
        /// Shader count (exposed for the property grid)
        /// </summary>
        public int ShaderCount
        {
            get
            {
                return (Shaders != null ? Shaders.Count() : 0);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [StreamableStat(StreamableVehicleStat.PolyCountString)]
        public int PolyCount
        {
            get
            {
                CheckStatLoaded(StreamableVehicleStat.PolyCount);
                return m_polyCount;
            }
            protected set
            {
                SetPropertyValue(ref m_polyCount, value, "PolyCount");
                SetStatLodaded(StreamableVehicleStat.PolyCount, true);
            }
        }
        private int m_polyCount;

        /// <summary>
        /// 
        /// </summary>
        [StreamableStat(StreamableVehicleStat.CollisionCountString)]
        public int CollisionCount
        {
            get
            {
                CheckStatLoaded(StreamableVehicleStat.CollisionCount);
                return m_collisionCount;
            }
            protected set
            {
                SetPropertyValue(ref m_collisionCount, value, "CollisionCount");
                SetStatLodaded(StreamableVehicleStat.CollisionCount, true);
            }
        }
        private int m_collisionCount;

        /// <summary>
        /// 
        /// </summary>
        [StreamableStat(StreamableVehicleStat.BoneCountString)]
        public int BoneCount
        {
            get
            {
                CheckStatLoaded(StreamableVehicleStat.BoneCount);
                return m_boneCount;
            }
            protected set
            {
                SetPropertyValue(ref m_boneCount, value, "BoneCount");
                SetStatLodaded(StreamableVehicleStat.BoneCount, true);
            }
        }
        private int m_boneCount;

        /// <summary>
        /// 
        /// </summary>
        [StreamableStat(StreamableVehicleStat.HasLod1String)]
        public bool HasLod1
        {
            get
            {
                CheckStatLoaded(StreamableVehicleStat.HasLod1);
                return m_hasLod1Count;
            }
            protected set
            {
                SetPropertyValue(ref m_hasLod1Count, value, "HasLod1");
                SetStatLodaded(StreamableVehicleStat.HasLod1, true);
            }
        }
        private bool m_hasLod1Count;

        /// <summary>
        /// 
        /// </summary>
        [StreamableStat(StreamableVehicleStat.HasLod2String)]
        public bool HasLod2
        {
            get
            {
                CheckStatLoaded(StreamableVehicleStat.HasLod2);
                return m_hasLod2Count;
            }
            protected set
            {
                SetPropertyValue(ref m_hasLod2Count, value, "HasLod2");
                SetStatLodaded(StreamableVehicleStat.HasLod2, true);
            }
        }
        private bool m_hasLod2Count;

        /// <summary>
        /// 
        /// </summary>
        [StreamableStat(StreamableVehicleStat.HasLod3String)]
        public bool HasLod3
        {
            get
            {
                CheckStatLoaded(StreamableVehicleStat.HasLod3);
                return m_hasLod3Count;
            }
            protected set
            {
                SetPropertyValue(ref m_hasLod3Count, value, "HasLod3");
                SetStatLodaded(StreamableVehicleStat.HasLod3, true);
            }
        }
        private bool m_hasLod3Count;

        /// <summary>
        /// 
        /// </summary>
        [StreamableStat(StreamableVehicleStat.WheelCountString)]
        public int WheelCount
        {
            get
            {
                CheckStatLoaded(StreamableVehicleStat.WheelCount);
                return m_wheelCount;
            }
            protected set
            {
                SetPropertyValue(ref m_wheelCount, value, "WheelCount");
                SetStatLodaded(StreamableVehicleStat.WheelCount, true);
            }
        }
        private int m_wheelCount;

        /// <summary>
        /// 
        /// </summary>
        [StreamableStat(StreamableVehicleStat.SeatCountString)]
        public int SeatCount
        {
            get
            {
                CheckStatLoaded(StreamableVehicleStat.SeatCount);
                return m_seatCount;
            }
            protected set
            {
                SetPropertyValue(ref m_seatCount, value, "SeatCount");
                SetStatLodaded(StreamableVehicleStat.SeatCount, true);
            }
        }
        private int m_seatCount;

        /// <summary>
        /// 
        /// </summary>
        [StreamableStat(StreamableVehicleStat.DoorCountString)]
        public int DoorCount
        {
            get
            {
                CheckStatLoaded(StreamableVehicleStat.DoorCount);
                return m_doorCount;
            }
            protected set
            {
                SetPropertyValue(ref m_doorCount, value, "DoorCount");
                SetStatLodaded(StreamableVehicleStat.DoorCount, true);
            }
        }
        private int m_doorCount;

        /// <summary>
        /// 
        /// </summary>
        [StreamableStat(StreamableVehicleStat.LightCountString)]
        public int LightCount
        {
            get
            {
                CheckStatLoaded(StreamableVehicleStat.LightCount);
                return m_lightCount;
            }
            protected set
            {
                SetPropertyValue(ref m_lightCount, value, "LightCount");
                SetStatLodaded(StreamableVehicleStat.LightCount, true);
            }
        }
        private int m_lightCount;

        /// <summary>
        /// 
        /// </summary>
        [StreamableStat(StreamableVehicleStat.ExtraCountString)]
        public int ExtraCount
        {
            get
            {
                CheckStatLoaded(StreamableVehicleStat.ExtraCount);
                return m_extraCount;
            }
            protected set
            {
                SetPropertyValue(ref m_extraCount, value, "ExtraCount");
                SetStatLodaded(StreamableVehicleStat.ExtraCount, true);
            }
        }
        private int m_extraCount;

        /// <summary>
        /// 
        /// </summary>
        [StreamableStat(StreamableVehicleStat.RudderCountString)]
        public int RudderCount
        {
            get
            {
                CheckStatLoaded(StreamableVehicleStat.RudderCount);
                return m_rudderCount;
            }
            protected set
            {
                SetPropertyValue(ref m_rudderCount, value, "RudderCount");
                SetStatLodaded(StreamableVehicleStat.RudderCount, true);
            }
        }
        private int m_rudderCount;

        /// <summary>
        /// 
        /// </summary>
        [StreamableStat(StreamableVehicleStat.RotorCountString)]
        public int RotorCount
        {
            get
            {
                CheckStatLoaded(StreamableVehicleStat.RotorCount);
                return m_rotorCount;
            }
            protected set
            {
                SetPropertyValue(ref m_rotorCount, value, "RotorCount");
                SetStatLodaded(StreamableVehicleStat.RotorCount, true);
            }
        }
        private int m_rotorCount;

        /// <summary>
        /// 
        /// </summary>
        [StreamableStat(StreamableVehicleStat.PropellerCountString)]
        public int PropellerCount
        {
            get
            {
                CheckStatLoaded(StreamableVehicleStat.PropellerCount);
                return m_propellerCount;
            }
            protected set
            {
                SetPropertyValue(ref m_propellerCount, value, "PropellerCount");
                SetStatLodaded(StreamableVehicleStat.PropellerCount, true);
            }
        }
        private int m_propellerCount;

        /// <summary>
        /// 
        /// </summary>
        [StreamableStat(StreamableVehicleStat.ElevatorCountString)]
        public int ElevatorCount
        {
            get
            {
                CheckStatLoaded(StreamableVehicleStat.ElevatorCount);
                return m_elevatorCount;
            }
            protected set
            {
                SetPropertyValue(ref m_elevatorCount, value, "ElevatorCount");
                SetStatLodaded(StreamableVehicleStat.ElevatorCount, true);
            }
        }
        private int m_elevatorCount;

        /// <summary>
        /// 
        /// </summary>
        [StreamableStat(StreamableVehicleStat.HandlebarCountString)]
        public int HandlebarCount
        {
            get
            {
                CheckStatLoaded(StreamableVehicleStat.HandlebarCount);
                return m_handlebarCount;
            }
            protected set
            {
                SetPropertyValue(ref m_handlebarCount, value, "HandlebarCount");
                SetStatLodaded(StreamableVehicleStat.HandlebarCount, true);
            }
        }
        private int m_handlebarCount;

        /// <summary>
        /// Dictionary mapping platforms to platform stats.
        /// </summary>
        [Browsable(false)]
        [StreamableStat(StreamableVehicleStat.PlatformStatsString)]
        public IDictionary<RSG.Platform.Platform, IVehiclePlatformStat> PlatformStats
        {
            get
            {
                CheckStatLoaded(StreamableVehicleStat.PlatformStats);
                return m_platformStats;
            }
            protected set
            {
                SetPropertyValue(ref m_platformStats, value, "PlatformStats");
                SetStatLodaded(StreamableVehicleStat.PlatformStats, true);
            }
        }
        private IDictionary<RSG.Platform.Platform, IVehiclePlatformStat> m_platformStats;
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public Vehicle(String name, String friendlyName, String gameName, VehicleCategory category, IVehicleCollection parentCollection)
            : base(name)
        {
            ParentCollection = parentCollection;
            Category = category;
            FriendlyName = friendlyName;
            GameName = gameName;
        }
        #endregion // Constructor(s)

        #region IVehicle Implementation
        /// <summary>
        /// Returns a platform stat for a particular platform
        /// </summary>
        /// <param name="platform"></param>
        /// <returns></returns>
        public IVehiclePlatformStat GetPlatformStat(RSG.Platform.Platform platform)
        {
            IVehiclePlatformStat platformStat = null;
            if (PlatformStats.ContainsKey(platform))
            {
                platformStat = PlatformStats[platform];
            }
            return platformStat;
        }
        #endregion // IVehicle Implementation

        #region IComparable<IVehicle> Interface
        /// <summary>
        /// Compare this Vehicle to another.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(IVehicle other)
        {
            if (other == null)
            {
                return 1;
            }

            return (Name.CompareTo(other.Name));
        }
        #endregion // IComparable<IVehicle> Interface

        #region IEquatable<IVehicle> Interface
        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>true if the current object is equal to the other parameter; otherwise, false.</returns>
        public bool Equals(IVehicle other)
        {
            if (other == null)
            {
                return false;
            }

            return (Name == other.Name);
        }
        #endregion // IEquatable<IVehicle> Interface

        #region Object Overrides
        /// <summary>
        /// Compare
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(Object obj)
        {
            if (obj == null)
            {
                return base.Equals(obj);
            }

            return ((obj is IVehicle) && Equals(obj as IVehicle));
        }

        /// <summary>
        /// Return String representation of the Vehicle.
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return Name;
        }

        /// <summary>
        /// Return hash code of Vehicle (based on Name).
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return (this.Name.GetHashCode());
        }
        #endregion // Object Overrides
    }

} // RSG.Model.Vehicle namespace
