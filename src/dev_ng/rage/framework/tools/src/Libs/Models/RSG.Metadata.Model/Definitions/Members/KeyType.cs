﻿// --------------------------------------------------------------------------------------------
// <copyright file="KeyType.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions.Members
{
    /// <summary>
    /// Specifies the different types of map members the parCodeGen system supports.
    /// </summary>
    public enum KeyType
    {
        /// <summary>
        /// The map key is unknown.
        /// </summary>
        Unknown,

        /// <summary>
        /// The map key is not recognised as valid, though it is defined as something.
        /// </summary>
        Unrecognised,

        /// <summary>
        /// The map key is of the type System.SByte.
        /// </summary>
        Signed8,

        /// <summary>
        /// The map key is of the type System.Byte.
        /// </summary>
        Unsigned8,

        /// <summary>
        /// The map key is of the type System.Int16.
        /// </summary>
        Signed16,

        /// <summary>
        /// The map key is of the type System.UInt16.
        /// </summary>
        Unsigned16,

        /// <summary>
        /// The map key is of the type System.Int32.
        /// </summary>
        Signed32,

        /// <summary>
        /// The map key is of the type System.UInt32.
        /// </summary>
        Unsigned32,

        /// <summary>
        /// The map key is got from a defined enumeration in the parCodeGen scope.
        /// </summary>
        Enum,

        /// <summary>
        /// The map key is a string hash value.
        /// </summary>
        AtHashString,

        /// <summary>
        /// The map key is a string hash value.
        /// </summary>
        AtHashValue,
    } // RSG.Metadata.Model.Definitions.Members.KeyType {Enum}
} // RSG.Metadata.Model.Definitions.Members {Namespace}
