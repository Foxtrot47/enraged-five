﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProjectBuilder.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.Model
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.IO.Compression;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using System.Xml;
    using System.Xml.Linq;
    using RSG.Base.Extensions;
    using RSG.Base.Logging;
    using RSG.Editor;
    using RSG.Metadata.Model.Tunables;
    using RSG.Project.Model;
    using RSG.Text.Model.Resources;

    /// <summary>
    /// Provides the ability to load a project file and build it to produce the data needed for
    /// the text maker.
    /// </summary>
    public class ProjectBuilder : IProjectBuilder
    {
        #region Fields
        /// <summary>
        /// The projects that have been associated with this builder and will be built whenever
        /// the build methods are called.
        /// </summary>
        private IEnumerable<Project> _projects;

        /// <summary>
        /// The private field used for the <see cref="FilesWrittenToDuringBuild"/> property.
        /// </summary>
        private IList<string> _filesWrittenToDuringBuild;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ProjectBuilder"/> class for the
        /// specified projects.
        /// </summary>
        public ProjectBuilder()
        {
            this._projects = Enumerable.Empty<Project>();
            this._filesWrittenToDuringBuild = new List<string>();
        }
        #endregion Constructors

        #region Enumerations
        /// <summary>
        /// Defines the different dialogue sources that are built using this builder.
        /// </summary>
        private enum Source
        {
            /// <summary>
            /// Specifies that the dialogue objects represent the multiplayer files.
            /// </summary>
            Multiplayer,

            /// <summary>
            /// Specifies that the dialogue objects represent the single player files.
            /// </summary>
            Singleplayer,
        } // ProjectBuilder.Source {Enum}
        #endregion Enumerations

        #region Properties
        /// <summary>
        /// Gets the full path to all of the files that will be read from during the build.
        /// </summary>
        public IList<string> SourceFiles
        {
            get
            {
                List<string> paths = new List<string>();
                foreach (Project project in this._projects)
                {
                    Settings settings = new Settings();
                    settings.InitialiseWithProject(project);
                    string configurationPath = project.GetPropertyAsFullPath("Configurations");
                    if (configurationPath != null)
                    {
                        if (!paths.Contains(configurationPath))
                        {
                            paths.Add(configurationPath);
                        }
                    }

                    foreach (ProjectItem item in project["File"])
                    {
                        string fullPath = item.GetMetadata("FullPath");
                        if (fullPath != null)
                        {
                            if (!paths.Contains(fullPath))
                            {
                                paths.Add(fullPath);
                            }
                        }
                    }
                    
                    if (settings.BuildPsoData)
                    {
                        if (settings.EdlTimingsDirectory != null)
                        {
                            string timingFiles = settings.EdlTimingsDirectory + "....txt";
                            if (!paths.Contains(timingFiles))
                            {
                                paths.Add(timingFiles);
                            }
                        }
                    }
                }

                return paths;
            }
        }

        /// <summary>
        /// Gets the full path to all of the files that will be written to on build.
        /// </summary>
        public IList<string> TargetFiles
        {
            get
            {
                List<string> paths = new List<string>();
                foreach (Project project in this._projects)
                {
                    Settings settings = new Settings();
                    settings.InitialiseWithProject(project);

                    paths.AddRange(settings.AllTargetPaths);
                }

                return paths;
            }
        }

        /// <summary>
        /// Gets a list containing the absolute path to all of the files that were written to
        /// during the last build process.
        /// </summary>
        public IList<string> FilesWrittenToDuringBuild
        {
            get { return this._filesWrittenToDuringBuild; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Builds the projects that have been setup to build through the
        /// <see cref="SetProjects"/> method.
        /// </summary>
        /// <param name="log">
        /// The log object that build messages, warnings, and errors get outputted to.
        /// </param>
        public void Build(ILog log)
        {
            if (log == null)
            {
                throw new SmartArgumentNullException(() => log);
            }

            this._filesWrittenToDuringBuild.Clear();
            foreach (Project project in this._projects)
            {
                this.BuildProject(project, log);
            }
        }

        /// <summary>
        /// Sets the projects that this builder will build on calling its <see cref="Build"/>
        /// method.
        /// </summary>
        /// <param name="projects">
        /// A collection of projects that will be built using this builder.
        /// </param>
        public void SetProjects(IEnumerable<Project> projects)
        {
            if (projects != null)
            {
                this._projects = projects;
            }
            else
            {
                this._projects = Enumerable.Empty<Project>();
            }
        }

        /// <summary>
        /// Builds the specified project, outputting any logging messages into the specified
        /// log object.
        /// </summary>
        /// <param name="project">
        /// The project that needs building.
        /// </param>
        /// <param name="log">
        /// The log object that build messages, warnings, and errors get outputted to.
        /// </param>
        private void BuildProject(Project project, ILog log)
        {
            List<Guid> typeIds = project.GetProjectTypeIds();
            if (!typeIds.Contains(new Guid("D537245E-B7B4-4B18-BAEC-8D830A1AEE7C")))
            {
                log.Error(StringTable.UnrecognisedProjectType);
                return;
            }

            Settings settings = new Settings();
            settings.Log = log;
            settings.InitialiseWithProject(project);
            if (!settings.Validate(log))
            {
                return;
            }

            string configurationPath = project.GetPropertyAsFullPath("Configurations");
            if (configurationPath == null)
            {
                log.Error(StringTable.MissingConfigurations);
                return;
            }

            DialogueConfigurations configurations = new DialogueConfigurations();
            try
            {
                configurations.LoadConfigurations(configurationPath);
            }
            catch (Exception ex)
            {
                log.Error(StringTable.ConfigurationLoadError, configurationPath, ex.Message);
                return;
            }

            settings.InitialiseCharacters(configurations);
            var singleplayerDialogues = new Dictionary<Dialogue, string>();
            var multiplayerDialogues = new Dictionary<Dialogue, string>();
            foreach (ProjectItem item in project["File"])
            {
                string filename = item.GetMetadata("FullPath");
                FileInfo info = new FileInfo(filename);
                if (info.Exists)
                {
                    using (XmlReader reader = XmlReader.Create(filename))
                    {
                        Dialogue newDialogue = new Dialogue(reader, configurations);
                        if (newDialogue.AmbientFile && !settings.BuildAmbient)
                        {
                            continue;
                        }

                        if (newDialogue.Multiplayer)
                        {
                            multiplayerDialogues.Add(newDialogue, filename);
                        }
                        else
                        {
                            singleplayerDialogues.Add(newDialogue, filename);
                        }
                    }
                }
                else
                {
                    log.Error(StringTable.MissingFile, filename);
                    return;
                }
            }

            var validate = new Dictionary<Dialogue, string>();
            if (settings.BuildSingleplayer)
            {
                validate.AddRange(singleplayerDialogues);
            }

            if (settings.BuildMultiplayer)
            {
                validate.AddRange(multiplayerDialogues);
            }

            if (!this.Validate(validate, settings))
            {
                return;
            }

            TrackListings trackListings = new TrackListings();
            if (settings.BuildPsoData)
            {
                string[] timingFiles = new string[0];
                if (Directory.Exists(settings.EdlTimingsDirectory))
                {
                    timingFiles = Directory.GetFiles(
                        settings.EdlTimingsDirectory, "*.txt", SearchOption.AllDirectories);
                }

                foreach (string timingFile in timingFiles)
                {
                    trackListings.LoadRecordingFile(timingFile);
                }
            }

            if (settings.BuildSingleplayer)
            {
                this.WriteData(singleplayerDialogues.Keys, settings, Source.Singleplayer);
                this.WriteText(singleplayerDialogues.Keys, settings, Source.Singleplayer);
            }

            if (settings.BuildSingleplayer && settings.BuildPsoData)
            {
                Dictionary<string, PsoSource> idMap = new Dictionary<string, PsoSource>();
                foreach (var d in singleplayerDialogues)
                {
                    PsoSource temp;
                    if (!idMap.TryGetValue(d.Key.Id, out temp))
                    {
                        temp = new PsoSource();
                        temp.Conversations = new List<Conversation>();
                        temp.SourceFilenames = new List<string>();
                        temp.Id = d.Key.Id;
                        idMap.Add(d.Key.Id, temp);
                    }

                    temp.Conversations.AddRange(d.Key.Conversations);
                    temp.SourceFilenames.Add(d.Value);
                }

                foreach (PsoSource item in idMap.Values)
                {
                    string zipPath = Path.Combine(settings.PsoDataOutputPath, item.Id);
                    zipPath = Path.ChangeExtension(zipPath, ".zip");
                    FileInfo targetInfo = new FileInfo(zipPath);
                    foreach (string source in item.SourceFilenames)
                    {
                        FileInfo sourceInfo = new FileInfo(source);
                        if (sourceInfo.LastWriteTime > targetInfo.LastWriteTime)
                        {
                            this.WritePsoSourceData(item, settings, trackListings);
                            break;
                        }
                    }
                }
            }

            if (settings.BuildMultiplayer)
            {
                this.WriteData(multiplayerDialogues.Keys, settings, Source.Multiplayer);
                this.WriteText(multiplayerDialogues.Keys, settings, Source.Multiplayer);
            }

            if (settings.BuildMultiplayer && settings.BuildPsoData)
            {
                Dictionary<string, PsoSource> idMap = new Dictionary<string, PsoSource>();
                foreach (var d in multiplayerDialogues)
                {
                    PsoSource temp;
                    if (!idMap.TryGetValue(d.Key.Id, out temp))
                    {
                        temp = new PsoSource();
                        temp.Conversations = new List<Conversation>();
                        temp.SourceFilenames = new List<string>();
                        temp.Id = d.Key.Id;
                        idMap.Add(d.Key.Id, temp);
                    }

                    temp.Conversations.AddRange(d.Key.Conversations);
                    temp.SourceFilenames.Add(d.Value);
                }

                foreach (PsoSource item in idMap.Values)
                {
                    string zipPath = Path.Combine(settings.PsoDataOutputPath, item.Id);
                    zipPath = Path.ChangeExtension(zipPath, ".zip");
                    FileInfo targetInfo = new FileInfo(zipPath);
                    foreach (string source in item.SourceFilenames)
                    {
                        FileInfo sourceInfo = new FileInfo(source);
                        if (sourceInfo.LastWriteTime > targetInfo.LastWriteTime)
                        {
                            this.WritePsoSourceData(item, settings, trackListings);
                            break;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dialogues"></param>
        /// <param name="settings"></param>
        /// <returns></returns>
        private bool Validate(Dictionary<Dialogue, string> dialogues, Settings settings)
        {
            bool result = true;
            HashSet<string> filenames = new HashSet<string>();
            HashSet<string> missionIds = new HashSet<string>();
            foreach (KeyValuePair<Dialogue, string> kvp in dialogues)
            {
                if (String.IsNullOrWhiteSpace(kvp.Key.Id))
                {
                    settings.Log.Error("The mission id is missing '{0}'", kvp.Value);
                    result = false;
                }
                else if (kvp.Key.Id.Any(x => Char.IsWhiteSpace(x)))
                {
                    settings.Log.Error("The mission id contains whitespaces '{0}'", kvp.Value);
                    result = false;
                }
                else
                {
                    missionIds.Add(kvp.Key.Id);
                }

                HashSet<string> roots = new HashSet<string>();
                foreach (var c in kvp.Key.Conversations)
                {
                    if (String.IsNullOrWhiteSpace(c.Root))
                    {
                        settings.Log.Error("The conversation root is missing '{0}'", kvp.Value);
                        result = false;
                    }
                    else if (c.Root.Any(x => Char.IsWhiteSpace(x)))
                    {
                        settings.Log.Error("The conversation root contains whitespaces '{0}\\{1}'", kvp.Value, c.Root);
                        result = false;
                    }
                    else if (roots.Contains(c.Root))
                    {
                        settings.Log.Error("The conversation root has already been defined '{0}\\{1}'", kvp.Value, c.Root);
                        result = false;
                    }
                    else
                    {
                        roots.Add(c.Root);
                    }

                    foreach (var line in c.Lines)
                    {
                        if (!settings.ShouldExportCharacter(line.CharacterId))
                        {
                            continue;
                        }

                        if (String.IsNullOrWhiteSpace(line.AudioFilepath))
                        {
                            settings.Log.Error("The audio filename is missing '{0}\\{1}'", kvp.Value, c.Root);
                            result = false;
                        }
                        else if (filenames.Contains(line.AudioFilepath))
                        {
                            settings.Log.Warning("The audio filename isn't unique '{0}\\{1}\\{2}'", kvp.Value, c.Root, line.AudioFilepath);
                        }
                        else
                        {
                            filenames.Add(line.AudioFilepath);
                        }
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Calculates the specified lines headset submix export value based on its data and
        /// the data contained inside the given special object.
        /// </summary>
        /// <param name="line">
        /// The line whose headset submix value should be calculated.
        /// </param>
        /// <param name="special">
        /// The special object that is used during the calculation.
        /// </param>
        /// <returns>
        /// The export value of the given lines headset submix.
        /// </returns>
        private char GetHeadsetSubmixValue(ILine line, DialogueSpecial special)
        {
            if (line == null)
            {
                return '0';
            }

            bool headsetSubmix = line.HeadsetSubmix;
            if (headsetSubmix == false && special != null)
            {
                headsetSubmix = headsetSubmix || special.HasHeadsetFlag;
            }

            return headsetSubmix.ToNumericalChar();
        }

        /// <summary>
        /// Gets the string that should be exported for the specified lines listener value.
        /// </summary>
        /// <param name="line">
        /// The line whose listener export string should be returned.
        /// </param>
        /// <returns>
        /// The string that's written out for the specified lines listener value.
        /// </returns>
        private string GetListenerValueAsString(ILine line)
        {
            int value = line.Listener;
            if (value < 0)
            {
                return "0";
            }

            if (value >= 0 && value <= 9)
            {
                return value.ToStringInvariant();
            }

            if (value >= 10 && value <= 35)
            {
                return new String((char)((value - 10) + 65), 1);
            }

            return "0";
        }

        /// <summary>
        /// Gets the string that should be exported for the specified lines speaker value.
        /// </summary>
        /// <param name="line">
        /// The line whose speaker export string should be returned.
        /// </param>
        /// <returns>
        /// The string that's written out for the specified lines speaker value.
        /// </returns>
        private string GetSpeakerValueAsString(ILine line)
        {
            int value = line.Speaker;
            if (value < 0)
            {
                return "0";
            }

            if (value >= 0 && value <= 9)
            {
                return value.ToStringInvariant();
            }

            if (value >= 10 && value <= 35)
            {
                return new String((char)((value - 10) + 65), 1);
            }

            return "0";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="conversation"></param>
        /// <param name="settings"></param>
        /// <param name="trackListings"></param>
        /// <returns></returns>
        private XDocument CreateConversationPsoDocument(
            Conversation conversation, Settings settings, TrackListings trackListings)
        {
            XElement array = new XElement("LineList");
            for (int i = 0; i < conversation.Lines.Count; i++)
            {
                ILine line = conversation.Lines[i];
                if (!settings.ShouldExportCharacter(line.CharacterId))
                {
                    continue;
                }

                TimeSpan startTime = TimeSpan.FromMilliseconds(0);
                string startTimeString = null;
                foreach (Track track in trackListings.Tracks)
                {
                    foreach (LineRecordedData recordedData in track.RecordedData)
                    {
                        if (String.Equals(line.AudioFilepath, recordedData.ClipName))
                        {
                            startTimeString = recordedData.StartTime;
                        }
                    }
                }

                if (startTimeString != null)
                {
                    DateTimeStyles style = DateTimeStyles.AllowWhiteSpaces;
                    string[] formats = new string[] { "mm:ss.fff", "m:ss.fff" };
                    DateTime dt;
                    if (!DateTime.TryParseExact(startTimeString, formats, null, style, out dt))
                    {
                        startTime = TimeSpan.FromMilliseconds(0);
                    }
                    else
                    {
                        startTime = dt.TimeOfDay;
                    }
                }

                array.Add(this.CreateLineMetadataArrayItem(line, startTime));
                if (conversation.IsRandom)
                {
                    break;
                }
            }

            string subtitleType =
                conversation.Configurations.GetSubtitleTypeExportString(
                conversation.SubtitleType);

            return TunableStructure.CreateXDocument("naConvMetadata",
                array,
                BoolTunable.CreateXElement("IsInterruptible", conversation.Interruptible),
                BoolTunable.CreateXElement("IsRandom", conversation.IsRandom),
                BoolTunable.CreateXElement("IsPlaceholder", conversation.IsPlaceholder),
                EnumTunable.CreateXElement("SubtitleType", subtitleType));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="settings"></param>
        /// <param name="trackListings"></param>
        private void WritePsoSourceData(
            PsoSource source, Settings settings, TrackListings trackListings)
        {
            string id = source.Id;
            string zipPath = Path.Combine(settings.PsoDataOutputPath, id);
            zipPath = Path.ChangeExtension(zipPath, ".zip");
            using (var memoryStream = new MemoryStream())
            {
                using (var archive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                {
                    foreach (Conversation conversation in source.Conversations)
                    {
                        string name = "{0}.pso.meta".FormatCurrent(conversation.Root);
                        var metaFile = archive.CreateEntry(name, CompressionLevel.NoCompression);

                        using (var entryStream = metaFile.Open())
                        {
                            XDocument doc = this.CreateConversationPsoDocument(conversation, settings, trackListings);
                            doc.Save(entryStream);
                        }
                    }
                }

                using (var fileStream = new FileStream(zipPath, FileMode.Create))
                {
                    memoryStream.Seek(0, SeekOrigin.Begin);
                    memoryStream.CopyTo(fileStream);
                }

                this._filesWrittenToDuringBuild.Add(zipPath);
            }
        }

        /// <summary>
        /// Writes the dialogue data into the associated data file.
        /// </summary>
        /// <param name="dialogues">
        /// The dialogue objects whose data should be written out.
        /// </param>
        /// <param name="settings">
        /// The settings object that contains all of the build settings as well as the log
        /// object to write build messages to.
        /// </param>
        /// <param name="source">
        /// A value specifying the source of the given dialogue files.
        /// </param>
        private void WriteData(IEnumerable<Dialogue> dialogues, Settings settings, Source source)
        {
            string dummyLabel = String.Empty;
            if (source == Source.Singleplayer)
            {
                dummyLabel = "[DUMMY_DLGFILE]";
            }
            else
            {
                dummyLabel = "[DUMMY_MPDLGFILE]";
            }

            MemoryStream stream = new MemoryStream();
            using (TextWriter writer = new StreamWriter(stream, Encoding.ASCII, 512, true))
            {
                if (dialogues.Count() > 0)
                {
                    writer.WriteLine("start");
                    foreach (Dialogue dialogue in dialogues)
                    {
                        this.WriteSubtitleId(dialogue, writer);
                    }

                    writer.WriteLine("end");
                    foreach (Dialogue dialogue in dialogues)
                    {
                        this.WriteSubtitleData(dialogue, writer, settings);
                    }

                    writer.WriteLine();
                }

                writer.WriteLine(dummyLabel);
                writer.Write("Dummy label.");
            }

            string filename = String.Empty;
            if (source == Source.Singleplayer)
            {
                filename = settings.DialogueDataFilename;
            }
            else
            {
                filename = settings.DialogueNetDataFilename;
            }

            this.WriteToFile(settings.GetFullPathFromFilename(filename), settings.Log, stream);
        }

        /// <summary>
        /// Creates the metadata XML element for the line metadata array item in the
        /// conversations pso.meta data.
        /// </summary>
        /// <param name="line">
        /// The line whose data will be written to the created XML element.
        /// </param>
        /// <param name="startTime">
        /// The lines recorded start time within its track and channel.
        /// </param>
        /// <returns>
        /// A new XML Element that contains the specified lines metadata.
        /// </returns>
        private XElement CreateLineMetadataArrayItem(ILine line, TimeSpan startTime)
        {
            int varationStartIndex = Math.Max(line.AudioFilepath.LastIndexOf('_'), 0);
            string context = line.AudioFilepath.Substring(0, varationStartIndex);
            string subtitle = line.Conversation.Root;
            string audibility = line.Configurations.GetAudibilityExportString(line.Audibility);
            string audioType = line.Configurations.GetAudioTypeExportString(line.AudioType);
            uint startTimeInMilliseconds = (uint)startTime.TotalMilliseconds;

            XElement soundMetadata = StructTunable.CreateXElement("SoundMetadata",
                StringTunable.CreateXElement("Context", context),
                StringTunable.CreateXElement("Subtitle", subtitle),
                StringTunable.CreateXElement("VolumeType", audioType),
                Unsigned32Tunable.CreateXElement("StartTime", startTimeInMilliseconds));

            return ArrayTunable.CreateItemXElement(null,
                soundMetadata,
                StringTunable.CreateXElement("Audibility", audibility),
                Unsigned32Tunable.CreateXElement("SpeakerIdx", (uint)line.Speaker),
                Unsigned32Tunable.CreateXElement("ListenerIdx", (uint)line.Listener),
                BoolTunable.CreateXElement("DucksRadio", line.DucksRadio),
                BoolTunable.CreateXElement("DucksScore", line.DucksScore),
                BoolTunable.CreateXElement(
                "DoNotPauseForSpecialAbility", line.DontInterruptForSpecialAbility));
        }

        /// <summary>
        /// Writes out the specified dialogues subtitle data to the given writer.
        /// </summary>
        /// <param name="dialogue">
        /// The dialogue object whose subtitle data should be written to the specified text
        /// writer.
        /// </param>
        /// <param name="writer">
        /// The object that the subtitle data will be written to.
        /// </param>
        /// <param name="settings">
        /// The settings object that contains all of the build settings as well as the log
        /// object to write build messages to.
        /// </param>
        private void WriteSubtitleData(Dialogue dialogue, TextWriter writer, Settings settings)
        {
            writer.WriteLine();
            writer.WriteLine("{" + dialogue.Name + "}");

            string subtitleId = dialogue.SubtitleId;
            string filenameHeaderFormat = "[{0}{1}A:" + subtitleId + "]";
            string dataHeaderFormat = "[{0}{1}:" + subtitleId + "]";
            DialogueConfigurations configurations = dialogue.Configurations;
            Regex regex = new Regex("_[0-9]{2}\\Z");
            foreach (Conversation conversation in dialogue.Conversations)
            {
                if (conversation.IsPlaceholder && !settings.BuildPlaceholder)
                {
                    continue;
                }

                if (!conversation.IsPlaceholder && !settings.BuildNonPlaceholder)
                {
                    continue;
                }

                string root = conversation.Root;
                writer.WriteLine();
                StringBuilder slBuilder = new StringBuilder(6);
                StringBuilder ifBuilder = new StringBuilder(3);
                StringBuilder lfBuilder = new StringBuilder(6);

                ifBuilder.Append(conversation.Interruptible.ToNumericalChar());
                ifBuilder.Append(conversation.IsTriggeredByAnimation.ToNumericalChar());
                ifBuilder.Append(conversation.IsPlaceholder.ToNumericalChar());

                for (int i = 0, lineIndex = 1; i < conversation.Lines.Count; i++, lineIndex++)
                {
                    ILine line = conversation.Lines[i];
                    DialogueSpecial special = configurations.GetSpecial(line.Special);

                    slBuilder.Append(this.GetSpeakerValueAsString(line));
                    slBuilder.Append(this.GetListenerValueAsString(line));
                    slBuilder.Append(configurations.GetAudioTypeIndex(line.AudioType));

                    lfBuilder.Append(line.Interruptible.ToNumericalChar());
                    lfBuilder.Append(line.DucksRadio.ToNumericalChar());
                    lfBuilder.Append(line.DucksScore.ToNumericalChar());
                    lfBuilder.Append(configurations.GetAudibilityIndex(line.Audibility));
                    lfBuilder.Append(this.GetHeadsetSubmixValue(line, special));
                    lfBuilder.Append(line.DontInterruptForSpecialAbility.ToNumericalChar());

                    string index = "_" + lineIndex.ToStringInvariant();
                    if (conversation.IsRandom)
                    {
                        index = String.Empty;
                    }

                    writer.WriteLine(filenameHeaderFormat, root, index);

                    string filename = line.AudioFilepath;
                    if (!line.ManualFilename && regex.IsMatch(filename))
                    {
                        int length = line.AudioFilepath.LastIndexOf('_');
                        filename = filename.Substring(0, length);
                    }

                    writer.WriteLine(filename);
                    if (conversation.IsRandom)
                    {
                        break;
                    }
                }

                writer.WriteLine(dataHeaderFormat, root, "SL");
                writer.WriteLine(slBuilder.ToString());

                writer.WriteLine(dataHeaderFormat, root, "IF");
                writer.WriteLine(ifBuilder.ToString());

                writer.WriteLine(dataHeaderFormat, root, "LF");
                writer.WriteLine(lfBuilder.ToString());
            }
        }

        /// <summary>
        /// Writes out the specified dialogues subtitle id to the given writer.
        /// </summary>
        /// <param name="dialogue">
        /// The dialogue object whose subtitle ids should be written to the specified text
        /// writer.
        /// </param>
        /// <param name="writer">
        /// The object that the subtitle id will be written to.
        /// </param>
        private void WriteSubtitleId(Dialogue dialogue, TextWriter writer)
        {
            writer.WriteLine(dialogue.SubtitleId);
        }

        /// <summary>
        /// Writes out the specified dialogues subtitle text to the given writer.
        /// </summary>
        /// <param name="dialogue">
        /// The dialogue object whose subtitle text should be written to the specified text
        /// writer.
        /// </param>
        /// <param name="writer">
        /// The object that the subtitle text will be written to.
        /// </param>
        /// <param name="settings">
        /// The settings object that contains all of the build settings as well as the log
        /// object to write build messages to.
        /// </param>
        private void WriteSubtitleText(Dialogue dialogue, TextWriter writer, Settings settings)
        {
            writer.WriteLine("{" + dialogue.Name + "}");
            writer.WriteLine();

            string subtitleId = dialogue.SubtitleId;
            string lineHeaderFormat = "[{0}_{1}:" + subtitleId + "]";
            DialogueConfigurations configurations = dialogue.Configurations;
            foreach (Conversation conversation in dialogue.Conversations)
            {
                if (conversation.IsPlaceholder && !settings.BuildPlaceholder)
                {
                    continue;
                }

                if (!conversation.IsPlaceholder && !settings.BuildNonPlaceholder)
                {
                    continue;
                }

                string root = conversation.Root;
                for (int i = 0, lineIndex = 1; i < conversation.Lines.Count; i++, lineIndex++)
                {
                    ILine line = conversation.Lines[i];
                    if (!settings.ShouldExportCharacter(line.CharacterId))
                    {
                        continue;
                    }

                    string format = String.Empty;
                    if (conversation.IsRandom)
                    {
                        format = "D2";
                    }

                    string index = lineIndex.ToStringInvariant(format);
                    writer.WriteLine(lineHeaderFormat, root, index);
                    if (line.AlwaysSubtitled)
                    {
                        writer.WriteLine("~t~" + line.Dialogue);
                    }
                    else
                    {
                        writer.WriteLine("~z~" + line.Dialogue);
                    }
                }

                writer.WriteLine();
            }
        }

        /// <summary>
        /// Writes the dialogue text into the associated file.
        /// </summary>
        /// <param name="dialogues">
        /// The dialogue objects whose text should be written out.
        /// </param>
        /// <param name="settings">
        /// The settings object that contains all of the build settings as well as the log
        /// object to write build messages to.
        /// </param>
        /// <param name="source">
        /// A value specifying the source of the given dialogue files.
        /// </param>
        private void WriteText(IEnumerable<Dialogue> dialogues, Settings settings, Source source)
        {
            string dummyLabel = String.Empty;
            if (source == Source.Singleplayer)
            {
                dummyLabel = "[DUMMY_DLG]";
            }
            else
            {
                dummyLabel = "[DUMMY_MPDLG]";
            }

            MemoryStream stream = new MemoryStream();
            using (TextWriter writer = new StreamWriter(stream, Encoding.ASCII, 512, true))
            {
                if (dialogues.Count() > 0)
                {
                    writer.WriteLine("start");
                    foreach (Dialogue dialogue in dialogues)
                    {
                        this.WriteSubtitleId(dialogue, writer);
                    }

                    writer.WriteLine("end");
                    writer.WriteLine();
                    foreach (Dialogue dialogue in dialogues)
                    {
                        this.WriteSubtitleText(dialogue, writer, settings);
                    }
                }

                writer.WriteLine(dummyLabel);
                writer.Write("Dummy label.");
            }

            string filename = String.Empty;
            if (source == Source.Singleplayer)
            {
                filename = settings.DialogueFilename;
            }
            else
            {
                filename = settings.DialogueNetFilename;
            }

            this.WriteToFile(settings.GetFullPathFromFilename(filename), settings.Log, stream);
        }

        /// <summary>
        /// Writes the data contained within the specified memory stream to a file located at
        /// the given full path.
        /// </summary>
        /// <param name="fullPath">
        /// The full path to the file that the data should be written to.
        /// </param>
        /// <param name="log">
        /// The log object that warnings and errors get outputted to.
        /// </param>
        /// <param name="stream">
        /// The stream containing the data to write to the file.
        /// </param>
        private void WriteToFile(string fullPath, ILog log, MemoryStream stream)
        {
            try
            {
                FileInfo fileInfo = new FileInfo(fullPath);
                if (fileInfo.Exists && fileInfo.IsReadOnly)
                {
                    log.Error(StringTable.WriteProtectedFile, fullPath);
                    return;
                }

                using (FileStream file = File.Create(fullPath))
                {
                    using (stream)
                    {
                        stream.WriteTo(file);
                    }
                }

                this._filesWrittenToDuringBuild.Add(fullPath);
            }
            catch (Exception ex)
            {
                log.Error(StringTable.UnknownWriteError, fullPath, ex.Message);
            }
        }
        #endregion Methods

        #region Structures
        /// <summary>
        /// A structure containing the settings that a single <see cref="Dialogue"/> instance
        /// should be built under.
        /// </summary>
        private struct Settings
        {
            #region Fields
            /// <summary>
            /// A private dictionary containing the should export values for all of the
            /// characters specified in the project being built.
            /// </summary>
            private Dictionary<Guid, bool> _characterExportValues;
            #endregion Fields

            #region Properties
            /// <summary>
            /// Gets all of the full paths of the files that will be written to using these
            /// build settings.
            /// </summary>
            public IEnumerable<string> AllTargetPaths
            {
                get
                {
                    List<string> paths = new List<string>();
                    if (this.BuildSingleplayer)
                    {
                        paths.Add(this.GetFullPathFromFilename(this.DialogueDataFilename));
                        paths.Add(this.GetFullPathFromFilename(this.DialogueFilename));
                    }

                    if (this.BuildMultiplayer)
                    {
                        paths.Add(this.GetFullPathFromFilename(this.DialogueNetDataFilename));
                        paths.Add(this.GetFullPathFromFilename(this.DialogueNetFilename));
                    }

                    if (this.BuildSingleplayer || this.BuildMultiplayer)
                    {
                        if (this.BuildPsoData)
                        {
                            if (!String.IsNullOrWhiteSpace(this.PsoDataOutputPath))
                            {
                                paths.Add(this.PsoDataOutputPath + "....zip");
                            }
                        }
                    }

                    return paths;
                }
            }

            /// <summary>
            /// Gets a value indicating whether the ambient dialogue instances should be built.
            /// </summary>
            public bool BuildAmbient
            {
                get;
                private set;
            }

            /// <summary>
            /// Gets a value indicating whether the multiplayer dialogue instances should be
            /// built.
            /// </summary>
            public bool BuildMultiplayer
            {
                get;
                private set;
            }

            /// <summary>
            /// Gets a value indicating whether the non-placeholder conversations should be
            /// built.
            /// </summary>
            public bool BuildNonPlaceholder
            {
                get;
                private set;
            }

            /// <summary>
            /// Gets a value indicating whether the placeholder conversations should be built.
            /// </summary>
            public bool BuildPlaceholder
            {
                get;
                private set;
            }

            /// <summary>
            /// Gets a value indicating whether each conversation should have a pso metadata
            /// file exported for it.
            /// </summary>
            public bool BuildPsoData
            {
                get;
                private set;
            }

            /// <summary>
            /// Gets a value indicating whether the single player dialogue instances should be
            /// built.
            /// </summary>
            public bool BuildSingleplayer
            {
                get;
                private set;
            }

            /// <summary>
            /// Gets the filename for the dialogue data.
            /// </summary>
            public string DialogueDataFilename
            {
                get;
                private set;
            }

            /// <summary>
            /// Gets the filename for the dialogue text.
            /// </summary>
            public string DialogueFilename
            {
                get;
                private set;
            }

            /// <summary>
            /// Gets the filename for the dialogue data for multiplayer files.
            /// </summary>
            public string DialogueNetDataFilename
            {
                get;
                private set;
            }

            /// <summary>
            /// Gets the filename for the dialogue text for multiplayer files.
            /// </summary>
            public string DialogueNetFilename
            {
                get;
                private set;
            }

            /// <summary>
            /// Gets or sets the log object that should be used to output build messages.
            /// </summary>
            public ILog Log
            {
                get;
                set;
            }

            /// <summary>
            /// Gets the main directory that the output files should be saved in.
            /// </summary>
            public string OutputPath
            {
                get;
                private set;
            }

            /// <summary>
            /// Gets the output path for the conversation pso metadata files.
            /// </summary>
            public string PsoDataOutputPath
            {
                get;
                private set;
            }

            /// <summary>
            /// Gets the directory where the new york edl timings can be found.
            /// </summary>
            public string EdlTimingsDirectory
            {
                get;
                private set;
            }
            #endregion Properties

            #region Methods
            /// <summary>
            /// Gets the full file path from the filename by appending it to the specified
            /// output path.
            /// </summary>
            /// <param name="filename">
            /// The filename whose full path should be retrieved.
            /// </param>
            /// <returns>
            /// The full file path for the specified filename.
            /// </returns>
            public string GetFullPathFromFilename(string filename)
            {
                string fullPath = Path.Combine(this.OutputPath, filename);
                string extension = Path.GetExtension(fullPath);
                if (String.IsNullOrWhiteSpace(extension))
                {
                    fullPath = Path.ChangeExtension(fullPath, ".txt");
                }

                return fullPath;
            }

            /// <summary>
            /// Initialises the character should export lookup table from the given
            /// configuration object.
            /// </summary>
            /// <param name="configurations">
            /// The configuration object whose characters should be transposed to a should
            /// export lookup table.
            /// </param>
            public void InitialiseCharacters(DialogueConfigurations configurations)
            {
                this._characterExportValues = new Dictionary<Guid, bool>();
                foreach (DialogueCharacter character in configurations.Characters)
                {
                    this._characterExportValues.Add(character.Id, !character.DontExport);
                }
            }

            /// <summary>
            /// Initialises all of the setting properties from the properties contained within
            /// the specified project.
            /// </summary>
            /// <param name="project">
            /// The project whose properties initialise the properties in this settings object.
            /// </param>
            public void InitialiseWithProject(Project project)
            {
                this.BuildSingleplayer = project.GetPropertyAsBool("Singleplayer", false);
                this.BuildMultiplayer = project.GetPropertyAsBool("Multiplayer", false);
                this.BuildAmbient = project.GetPropertyAsBool("Ambient", false);
                this.BuildPlaceholder = project.GetPropertyAsBool("Placeholder", false);
                this.BuildNonPlaceholder = project.GetPropertyAsBool("NonPlaceholder", false);
                this.BuildPsoData = project.GetPropertyAsBool("PsoData", false);

                this.OutputPath = project.GetPropertyAsFullPath("OutputPath");
                this.DialogueDataFilename = project.GetProperty("DialogueDataFilename");
                this.DialogueFilename = project.GetProperty("DialogueFilename");
                this.DialogueNetDataFilename = project.GetProperty("DialogueNetDataFilename");
                this.DialogueNetFilename = project.GetProperty("DialogueNetFilename");

                this.PsoDataOutputPath = project.GetPropertyAsFullPath("PsoDataOutputPath");
                this.EdlTimingsDirectory = project.GetPropertyAsFullPath("EdlTimingsDirectory");
            }

            /// <summary>
            /// Gets a value indicating whether the character with the specified id should be
            /// exported or not.
            /// </summary>
            /// <param name="id">
            /// The id of the character to test.
            /// </param>
            /// <returns>
            /// True if the character with the specified id should be exported; otherwise,
            /// false.
            /// </returns>
            public bool ShouldExportCharacter(Guid id)
            {
                bool shouldExport;
                if (!this._characterExportValues.TryGetValue(id, out shouldExport))
                {
                    return true;
                }

                return shouldExport;
            }

            /// <summary>
            /// Validates this settings object by looking at all of the paths and build options
            /// and returns a value indicating whether the build process can proceed.
            /// </summary>
            /// <param name="log">
            /// The log object which the validation warnings and errors get outputted to.
            /// </param>
            /// <returns>
            /// True if this object is valid and the build can proceed; otherwise, false.
            /// </returns>
            public bool Validate(ILog log)
            {
                bool result = true;
                if (String.IsNullOrWhiteSpace(this.OutputPath))
                {
                    log.Error(StringTable.OutputDirectoryMissing);
                    result = false;
                }

                if (this.BuildSingleplayer)
                {
                    if (String.IsNullOrWhiteSpace(this.DialogueFilename))
                    {
                        log.Error(StringTable.SingleplayerTextTargetMissing);
                        result = false;
                    }

                    if (String.IsNullOrWhiteSpace(this.DialogueDataFilename))
                    {
                        log.Error(StringTable.SingleplayerDataTargetMissing);
                        result = false;
                    }
                }

                if (this.BuildMultiplayer)
                {
                    if (String.IsNullOrWhiteSpace(this.DialogueNetFilename))
                    {
                        log.Error(StringTable.MultiplayerTextTargetMissing);
                        result = false;
                    }

                    if (String.IsNullOrWhiteSpace(this.DialogueNetDataFilename))
                    {
                        log.Error(StringTable.MultiplayerDataTargetMissing);
                        result = false;
                    }
                }

                if (result)
                {
                    if (!this.ValidateReadOnlyPaths(log))
                    {
                        return false;
                    }
                }

                return result;
            }

            /// <summary>
            /// Validates that the specified file can be written to.
            /// </summary>
            /// <param name="filename">
            /// The filename that is being tested.
            /// </param>
            /// <param name="log">
            /// The log object which the validation warnings and errors get outputted to.
            /// </param>
            /// <returns>
            /// True if the specified file can be written to; otherwise, false.
            /// </returns>
            private bool ValidateReadOnlyPath(string filename, ILog log)
            {
                if (String.IsNullOrWhiteSpace(filename))
                {
                    return true;
                }

                if (String.IsNullOrWhiteSpace(this.OutputPath))
                {
                    return true;
                }

                bool result = true;
                try
                {
                    string fullPath = this.GetFullPathFromFilename(filename);
                    FileInfo fileInfo = new FileInfo(fullPath);
                    if (fileInfo.Exists && fileInfo.IsReadOnly)
                    {
                        log.Error(StringTable.WriteProtectedFile, filename);
                        result = false;
                    }
                }
                catch (Exception ex)
                {
                    log.Error(StringTable.UnknownWriteTestError, ex.Message);
                    result = false;
                }

                return result;
            }

            /// <summary>
            /// Validates that all of the target paths pointed to by this settings object
            /// aren't currently write protected.
            /// </summary>
            /// <param name="log">
            /// The log object which the validation warnings and errors get outputted to.
            /// </param>
            /// <returns>
            /// True if all of the targeted files can be written to; otherwise, false.
            /// </returns>
            private bool ValidateReadOnlyPaths(ILog log)
            {
                bool result = true;
                if (this.BuildSingleplayer)
                {
                    if (!this.ValidateReadOnlyPath(this.DialogueFilename, log))
                    {
                        result = false;
                    }

                    if (!this.ValidateReadOnlyPath(this.DialogueDataFilename, log))
                    {
                        result = false;
                    }
                }

                if (this.BuildMultiplayer)
                {
                    if (!this.ValidateReadOnlyPath(this.DialogueNetFilename, log))
                    {
                        result = false;
                    }

                    if (!this.ValidateReadOnlyPath(this.DialogueNetDataFilename, log))
                    {
                        result = false;
                    }
                }

                return result;
            }
            #endregion Methods
        } // ProjectBuilder.Settings {Structure}

        private struct PsoSource
        {
            public List<string> SourceFilenames
            {
                get;
                set;
            }

            public string Id
            {
                get;
                set;
            }

            public List<Conversation> Conversations
            {
                get;
                set;
            }
        }
        #endregion Structures
    } // RSG.Text.Model.ProjectBuilder {Class}
} // RSG.Text.Model {Namespace}
