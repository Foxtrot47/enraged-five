﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.ConfigParser;
using RSG.Model.Common;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Threading;
using RSG.Base.Tasks;

namespace RSG.Model.Report
{
    /// <summary>
    /// A report that generates it's content on the fly based on level data.
    /// </summary>
    public interface IDynamicLevelReport : IDynamicReport
    {
        #region Properties
        /// <summary>
        /// List of specific streamable stats that are required to generate this report.
        /// </summary>
        IEnumerable<StreamableStat> RequiredStats { get; }
        #endregion // Properties
    } // IDynamicLevelReport
} // RSG.Model.Report
