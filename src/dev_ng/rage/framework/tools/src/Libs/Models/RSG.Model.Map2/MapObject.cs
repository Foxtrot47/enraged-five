﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.Base.ConfigParser;
using RSG.SceneXml;
using RSG.Base.Math;

namespace RSG.Model.Map
{
    /// <summary>
    /// Base interface for any map object, definition or
    /// reference
    /// </summary>
    public interface IMapObject : IFileAsset
    {
        #region Properties
        
        /// <summary>
        /// The tightest box surrounding the map
        /// object, used for density statistics
        /// </summary>
        BoundingBox3f BoundingBox { get; }

        /// <summary>
        /// Reference to the owner Level object.
        /// </summary>
        ILevel Level { get; }

        /// <summary>
        /// The immediate parent map container, or
        /// null if non exist
        /// </summary>
        IMapContainer Container { get; }

        #endregion // Properties
    } // MapObject
} // RSG.Model.Map
