﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Model.Common.Mission
{
    /// <summary>
    /// Mission checkpoint asset.
    /// </summary>
    [DataContract]
    public class MissionCheckpoint : AssetBase, IMissionCheckpoint
    {
        #region Properties
        /// <summary>
        /// Number of attempts we expect this checkpoint to require.
        /// </summary>
        [DataMember]
        public float ProjectedAttempts { get; set; }

        /// <summary>
        /// Minimum number of attempts we expect this checkpoint to require.
        /// </summary>
        [DataMember]
        public float ProjectedAttemptsMin { get; set; }

        /// <summary>
        /// Maximum number of attempts we expect this checkpoint to require.
        /// </summary>
        [DataMember]
        public float ProjectedAttemptsMax { get; set; }

        /// <summary>
        /// Whether bugstar knows about this checkpoint.
        /// </summary>
        [DataMember]
        public bool InBugstar { get; set; }

        /// <summary>
        /// Index of this checkpoint
        /// </summary>
        [DataMember]
        public uint? Index { get; set; }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// For serialisation purposes.
        /// </summary>
        private MissionCheckpoint()
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public MissionCheckpoint(String name)
            : base(name)
        {
        }
        #endregion // Constructor(s)
    } // MissionCheckpoint
}
