﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.ManagedRage;
using System.IO;
using System.Reflection;
using System.ComponentModel;
using System.Text.RegularExpressions;
using RSG.Platform;
using RSG.Base.Extensions;
using RSG.Base.ConfigParser;

namespace RSG.Model.Report.Reports
{
    /// <summary>
    /// Utility methods used by the various reports
    /// </summary>
    public static class ReportUtils
    {
        /// <summary>
        /// Returns a list of content nodes that satisfy the passed in predicate
        /// </summary>
        /// <param name="nodeGroup"></param>
        /// <param name="predicate"></param>
        /// <param name="results"></param>
        public static void GetContentNodes(ContentNodeGroup nodeGroup, Func<ContentNode, Boolean> predicate, ref List<ContentNode> results)
        {
            foreach (ContentNode child in nodeGroup.Children)
            {
                if (predicate.Invoke(child))
                {
                    results.Add(child);
                }

                if (child is ContentNodeGroup)
                {
                    GetContentNodes(child as ContentNodeGroup, predicate, ref results);
                }
            }
        }

        /// <summary>
        /// Generates the path to a particular node by working its way up the tree
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public static string GeneratePathForNode(ContentNode node)
        {
            string result = node.Name + ".zip";

            while (node != null)
            {
                if (node is ContentNodeGroup)
                {
                    ContentNodeGroup groupNode = node as ContentNodeGroup;

                    if (!String.IsNullOrEmpty(groupNode.RelativePath))
                    {
                        result = Path.Combine(groupNode.RelativePath, result);
                    }
                }

                node = node.Parent;
            }

            return Path.GetFullPath(result);
        }

        /// <summary>
        /// Uses the config's target info to convert the independent file path to per platform files and loads the resulting pack files.
        /// </summary>
        /// <param name="independentFilePath"></param>
        /// <param name="gv"></param>
        /// <returns>Dictionary mapping platform -> packfile containing all the loaded files</returns>
        public static Dictionary<BuildPlatform, Packfile> OpenPackFiles(string independentFilePath, ConfigGameView gv)
        {
            Dictionary<BuildPlatform, Packfile> packFiles = new Dictionary<BuildPlatform, Packfile>();
            List<ConfigTargetInfo> allTargets = gv.Targets(gv.Branch);

            foreach (ConfigTargetInfo targetInfo in allTargets)
            {
                BuildPlatform platform;

                if (Enum.TryParse(targetInfo.Name, true, out platform))
                {
                    // Convert the platform independent path to a per platform build path
                    string platformFilePath = PathConversion.ConvertIndependentFilenameToPlatform(independentFilePath, targetInfo, gv.ExportDir, gv.ProcessedDir);

                    // Load the pack file
                    packFiles[platform] = new Packfile();
                    packFiles[platform].Load(platformFilePath);
                }
            }

            return packFiles;
        }

        /// <summary>
        /// Generates the memory information for an asset from a single pack file
        /// </summary>
        /// <param name="packFile">Pack file to get the memory details from</param>
        /// <param name="assetName">Name of the asset we are looking for inside the pack file</param>
        /// <param name="extensions">Array containing the possible extensions to search for</param>
        /// <returns>Comma separated string containing physical, virtual and total memory values</returns>
        public static string GenerateAssetMemoryDetails(Packfile packFile, string assetName, string[] extensions)
        {
            // Early out
            if (packFile == null || packFile.Entries == null)
            {
                return "n/a, n/a, n/a";
            }

            uint physicalSize = 0;
            uint virtualSize = 0;

            // Process all of the extensions we were provided
            foreach (string extension in extensions)
            {
                PackEntry entry = packFile.FindEntry(assetName + "." + extension);
                if (entry != null)
                {
                    physicalSize += entry.PhysicalSize;
                    virtualSize += entry.VirtualSize;
                }
            }

            // If both sizes are 0 then we couldn't find any entries for the character in the pack file
            if (physicalSize == 0 && virtualSize == 0)
            {
                return "n/a, n/a, n/a";
            }
            else
            {
                return String.Format("{0}, {1}, {2}", physicalSize, virtualSize, physicalSize + virtualSize);
            }
        }
    } // ReportUtils
} // RSG.Model.Report.Reports
