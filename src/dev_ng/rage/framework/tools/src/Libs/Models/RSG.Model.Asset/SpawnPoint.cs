﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.Model.Common.Map;
using RSG.Base.Math;
using RSG.SceneXml;
using System.Xml.Linq;
using RSG.Statistics.Common.Dto.GameAssetStats;

namespace RSG.Model.Asset
{
    /// <summary>
    /// A group of spawn points.
    /// </summary>
    public class SpawnPointCluster : ISpawnPointCluster
    {
		#region Properties
        /// <summary>
        /// 
        /// </summary>
        public IList<ISpawnPoint> SpawnPoints
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public string SourceFile
        {
            get;
            private set;
        }
		#endregion

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlElem"></param>
        /// <param name="sourceFile"></param>
        public SpawnPointCluster(XElement xmlElem, string sourceFile, SpawnPointLookupTables tables)
        {
            SpawnPoints = new List<ISpawnPoint>();
            SourceFile = sourceFile;
            foreach (XElement pointElement in xmlElem.Descendants("Points"))
            {
                foreach (XElement loadSavePointElement in pointElement.Descendants("MyPoints"))
                {
                    foreach (XElement itemElement in loadSavePointElement.Descendants("Item"))
                    {
                        SpawnPoints.Add(new SpawnPoint(itemElement, sourceFile, tables));
                    }
                }
            }
        }
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    public class SpawnPoint : AssetBase, ISpawnPoint
    {
        #region Constants
        private const string c_positionElement = "vPositionAndDirection";
        private const string c_oldPositionElement = "offsetPosition";
        private const string c_spawnTypeElement = "spawnType";
        private const string c_spawnGroupElement = "group";
        private const string c_modelSetElement = "pedType";
        private const string c_availabilityElement = "availableInMpSp";
        private const string c_timeUntilPedLeavesElement = "timeTillPedLeaves";
        private const string c_startTimeElement = "start";
        private const string c_endTimeElement = "end";

        private const string c_xAttribute = "x";
        private const string c_yAttribute = "y";
        private const string c_zAttribute = "z";

        #endregion // Constants

        #region Properties
        /// <summary>
        /// Position in world-space.
        /// </summary>
        public Vector3f Position
        {
            get;
            private set;
        }

        /// <summary>
        /// Position in world-space.
        /// </summary>
        public Quaternionf Rotation
        {
            get;
            private set;
        }

        /// <summary>
        /// Position in world-space.
        /// </summary>
        public Matrix34f Transform
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public string SourceFile
        {
            get;
            private set;
        }

        /// <summary>
        /// Type of spawn point this is.
        /// </summary>
        public string SpawnType
        {
            get;
            private set;
        }

        /// <summary>
        /// Group spawn point this in.
        /// </summary>
        public string SpawnGroup
        {
            get;
            private set;
        }

        /// <summary>
        /// Model set override for the scenario point.
        /// </summary>
        public string ModelSet
        {
            get;
            private set;
        }

        /// <summary>
        /// Game modes this spawn point is enabled in.
        /// </summary>
        public SpawnPointAvailableModes? AvailabilityMode
        {
            get;
            private set;
        }

        /// <summary>
        /// Start time active.
        /// </summary>
        public int? StartTime
        {
            get;
            private set;
        }

        /// <summary>
        /// End time active.
        /// </summary>
        public int? EndTime
        {
            get;
            private set;
        }

        /// <summary>
        /// Parent entity.
        /// </summary>
        public ISimpleMapArchetype Archetype
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public SpawnPoint(ObjectDef sceneObject)
            : base(sceneObject.Name)
        {
            if (sceneObject.Parent != null)
                Position = sceneObject.Parent.NodeTransform.Translation - sceneObject.NodeTransform.Translation;
            else
                Position = sceneObject.NodeTransform.Translation;

            SpawnType = sceneObject.GetParameter(ParamNames.TWODFX_SPAWNPOINT_SPAWNTYPE, ParamDefaults.TWODFX_SPAWNPOINT_SPAWNTYPE);
            Archetype = null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sceneObject"></param>
        /// <param name="parentEntity"></param>
        public SpawnPoint(TargetObjectDef sceneObject, ISimpleMapArchetype archetype)
            : base(sceneObject.Name)
        {
            if (sceneObject.Parent != null)
                Position = sceneObject.Parent.NodeTransform.Translation - sceneObject.NodeTransform.Translation;
            else
                Position = sceneObject.NodeTransform.Translation;

            SpawnType = sceneObject.GetParameter(ParamNames.TWODFX_SPAWNPOINT_SPAWNTYPE, ParamDefaults.TWODFX_SPAWNPOINT_SPAWNTYPE);
            Archetype = archetype;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlElem"></param>
        /// <param name="sourceFile"></param>
        /// <param name="tables"></param>
        public SpawnPoint(XElement xmlElem, string sourceFile, SpawnPointLookupTables tables)
        {
            SourceFile = sourceFile;

            //Position Element
            XElement posElem = xmlElem.Element(c_positionElement);
            if (posElem == null)
            {
                posElem = xmlElem.Element(c_oldPositionElement);
            }

            if (posElem != null)
            {
                XAttribute xAtt = posElem.Attribute(c_xAttribute);
                XAttribute yAtt = posElem.Attribute(c_yAttribute);
                XAttribute zAtt = posElem.Attribute(c_zAttribute);

                if (xAtt != null && yAtt != null && zAtt != null)
                {
                    Position = new Vector3f((float)Double.Parse(xAtt.Value), (float)Double.Parse(yAtt.Value), (float)Double.Parse(zAtt.Value));
                }
            }

            //Spawn Type Element
            XElement typeElem = xmlElem.Element(c_spawnTypeElement);
            if (typeElem != null)
            {
                SpawnType = typeElem.Value;
            }
            else
            {
                typeElem = xmlElem.Element("iType");
                if (typeElem != null)
                {
                    XAttribute valueAtt = typeElem.Attribute("value");
                    if (valueAtt != null)
                    {
                        int index = 0;
                        int.TryParse(valueAtt.Value, out index);
                        SpawnType = tables.GetTypeName(index);
                    }
                }                
            }

            //Spawn Group
            XElement groupElem = xmlElem.Element(c_spawnGroupElement);
            if (groupElem != null)
            {
                SpawnGroup = groupElem.Value;
            }
            else
            {
                typeElem = xmlElem.Element("iScenarioGroup");
                if (typeElem != null)
                {
                    XAttribute valueAtt = typeElem.Attribute("value");
                    if (valueAtt != null)
                    {
                        int index = 0;
                        int.TryParse(valueAtt.Value, out index);
                        SpawnGroup = tables.GetGroupName(index);
                    }
                }
            }

			//Model Set
            XElement modelSetElem = xmlElem.Element(c_modelSetElement);
            if (modelSetElem != null)
            {
                ModelSet = modelSetElem.Value;
            }
            else
            {
                typeElem = xmlElem.Element("ModelSetId");
                if (typeElem != null)
                {
                    XAttribute valueAtt = typeElem.Attribute("value");
                    if (valueAtt != null)
                    {
                        int index = 0;
                        int.TryParse(valueAtt.Value, out index);
                        if (SpawnType != null && SpawnType.Contains("VEHICLE"))
                        {
                            ModelSet = tables.GetVehicleModelSetName(index);
                        }
                        else
                        {
                            ModelSet = tables.GetPedModelSetName(index);
                        }
                    }
                }
            }


            //Availability
            XElement availabilityElem = xmlElem.Element(c_availabilityElement);
            SpawnPointAvailableModes mode = SpawnPointAvailableModes.kUnknown;
            AvailabilityMode = SpawnPointAvailableModes.kUnknown;
            if (availabilityElem != null)
            {
                if (Enum.TryParse(availabilityElem.Value, out mode))
                {
                    AvailabilityMode = mode;
                }
                else
                {
                    AvailabilityMode = SpawnPointAvailableModes.kUnknown;
                }
            }
            else
            {
                availabilityElem = xmlElem.Element("uAvailableInMpSp");
                if (availabilityElem != null)
                {
                    XAttribute valueAtt = typeElem.Attribute("value");
                    if (valueAtt != null)
                    {
                        int index = 0;
                        if (int.TryParse(valueAtt.Value, out index))
                        {
                            if (index == 0)
                            {
                                AvailabilityMode = SpawnPointAvailableModes.kBoth;
                            }
                            else if (index == 1)
                            {
                                AvailabilityMode = SpawnPointAvailableModes.kSingleplayer;
                            }
                            else if (index == 2)
                            {
                                AvailabilityMode = SpawnPointAvailableModes.kMultiplayer;
                            }
                        }
                    }
                }
            }
                

            //Start Time
            XElement startTimeElem = xmlElem.Element(c_startTimeElement);
            this.StartTime = null;
            if (startTimeElem == null)
            {
                startTimeElem = xmlElem.Element("iTimeStartOverride");
            }


            if (startTimeElem != null)
            {
                XAttribute startAttribute = startTimeElem.Attribute("value");
                if (startAttribute != null)
                {
                    string startValue = startAttribute.Value;
                    int startTime = -1;
                    if (Int32.TryParse(startValue, out startTime))
                    {
                        this.StartTime = startTime;
                    }
                }
            }

            //End Time
            XElement endTimeElem = xmlElem.Element(c_endTimeElement);
            this.EndTime = null;
            if (endTimeElem == null)
            {
                endTimeElem = xmlElem.Element("iTimeEndOverride");
            }

            if (endTimeElem != null)
            {
                XAttribute endAttribute = endTimeElem.Attribute("value");
                if (endAttribute != null)
                {
                    string endValue = endAttribute.Value;
                    int endTime = -1;
                    if (Int32.TryParse(endValue, out endTime))
                    {
                        this.EndTime = endTime;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public SpawnPoint(String name)
            : base(name)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public SpawnPoint(String name, ISimpleMapArchetype archetype)
            : base(name)
        {
            Archetype = archetype;
        }
        #endregion // Constructor

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public void LoadStatsFromDatabase(SpawnPointStatDto dto)
        {
            Position = dto.Position;
            SourceFile = dto.SourceFile;
            SpawnType = dto.SpawnType;
            SpawnGroup = dto.SpawnGroup;
            ModelSet = dto.ModelSet;
            AvailabilityMode = dto.AvailabilityMode;
            StartTime = dto.StartTime;
            EndTime = dto.EndTime;
        }
        #endregion // Public Methods
    } // SpawnPoint
}
