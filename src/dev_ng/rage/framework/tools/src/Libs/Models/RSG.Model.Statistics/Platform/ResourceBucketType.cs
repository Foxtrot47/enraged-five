﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Model.Statistics.Platform
{
    /// <summary>
    /// Resource bucket type.
    /// </summary>
    [Serializable]
    [DataContract]
    public enum ResourceBucketType
    {
        [EnumMember]
        Physical,

        [EnumMember]
        Virtual,
    }
}
