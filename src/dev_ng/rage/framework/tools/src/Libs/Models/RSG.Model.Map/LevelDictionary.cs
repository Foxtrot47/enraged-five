﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using RSG.Base.ConfigParser;
using RSG.Model.Map.Utilities;
using RSG.Model.Map.Statistics;

namespace RSG.Model.Map
{
    /// <summary>
    /// Read-only dictionary of Levels, indexed by level short-name
    /// (e.g. "gta5", "alpine").
    /// </summary>
    public class LevelDictionary : IEnumerable<Level>
    {
        public static String TextureFolder = String.Empty;

        #region Published Events
        /// <summary>
        /// Event raised whenever the level data is reloaded from disk.
        /// </summary>
        public event EventHandler LevelDataReloaded;
        #endregion // Published Events

        #region Properties and Associated Member ExportData
        /// <summary>
        /// 
        /// </summary>
        public String Game
        {
            get { return m_sGame; }
            private set { m_sGame = value; }
        }
        private String m_sGame;
        
        /// <summary>
        /// Independent build data root directory.
        /// </summary>
        public String IndependentDataPath
        {
            get { return m_sIndependentDataPath; }
            protected set { m_sIndependentDataPath = value; }
        }
        private String m_sIndependentDataPath;

        /// <summary>
        /// Indexer by level name.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public Level this[String levelName]
        {
            get
            {
                Debug.Assert(this.m_Levels.ContainsKey(levelName),
                    "Level does not exist.");
                return (this.m_Levels[levelName]);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Level[] Levels
        {
            get
            {
                List<Level> levels = new List<Level>();
                foreach (Level l in m_Levels.Values)
                    levels.Add(l);
                return (levels.ToArray());
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public String[] LevelNames
        {
            get 
            { 
                String[] levels = new String[this.m_Levels.Keys.Count];
                this.m_Levels.Keys.CopyTo(levels, 0);
                return (levels); 
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool Loaded
        {
            get { return m_bLoaded; }
            private set { m_bLoaded = value; }
        }
        private bool m_bLoaded;
        #endregion // Properties and Associated Member ExportData

        #region Filter Delegates
        /// <summary>
        /// Used to filter levels so as to load only levels that pass this function
        /// </summary>
        /// <param name="levelNode">The map node to test and see if it should get loaded or not</param>
        /// <returns type=Boolean>If this function returns false the level will not get loaded else it will get loaded</returns>
        public delegate Boolean FilterLevelDelegate(ContentNodeLevel levelNode);

        /// <summary>
        /// Used to filter map sections so as to load only map sections that pass this function
        /// </summary>
        /// <param name="mapNode">The map node to test and see if it should get loaded or not</param>
        /// <returns type=Boolean>If this function returns false the map section will not get loaded else it will get loaded</returns>
        public delegate Boolean FilterMapSectionDelegate(ContentNodeMap mapNode);

        /// <summary>
        /// Used to filter map areas.
        /// </summary>
        /// Only map areas that pass this method are loaded.
        /// <param name="mapGroup"></param>
        /// <returns></returns>
        public delegate Boolean FilterMapAreaDelegate(ContentNodeGroup mapGroup);
        #endregion // Filter Delegates

        #region Member ExportData
        /// <summary>
        /// Level dictionary.
        /// </summary>
        protected Dictionary<String, Level> m_Levels;

        #endregion // Member ExportData

        #region Constructor(s)

        /// <summary>
        /// Default constructor 
        /// </summary>
        public LevelDictionary()
        {
            this.Loaded = false;
        }

        #endregion // Constructor(s)

        #region IEnumerable<Level>
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerator<Level> IEnumerable<Level>.GetEnumerator()
        {
            foreach (Level l in this.m_Levels.Values)
                yield return l;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerator)this);
        }
        #endregion // IEnumerable<Level>

        #region Controller Methods

        /// <summary>
        /// Reloads the level data without using any filters so that everything gets loaded.
        /// </summary>
        public virtual void ReloadLevelData()
        {
            this.m_Levels = new Dictionary<String, Level>();

            ConfigGameView gv = new RSG.Base.ConfigParser.ConfigGameView();
            ContentModel cm = gv.Content;

            // Grab all the levels names
            List<ContentNode> levelNodes = cm.Root.FindAll("level");
            foreach (ContentNode node in levelNodes)
            {
                Debug.Assert(node is ContentNodeLevel, "FindAll returned invalid node.");
                if (!(node is ContentNodeLevel))
                    continue;

                ContentNodeLevel levelNode = (node as ContentNodeLevel);

                Level level = new Level(levelNode.Name, cm);
                this.m_Levels.Add(levelNode.Name, level);
            }
            
            this.Loaded = true;
            if (null != LevelDataReloaded)
                LevelDataReloaded(this, EventArgs.Empty);
        }

        /// <summary>
        /// Reloads the level data while using a filter function for both the levels and the map sections
        /// </summary>
        /// <param name="levelFilter">The delegate function used to filter the levels</param>
        /// <param name="areaFilter"></param>
        /// <param name="mapFilter">The delegate function used to filter the map sections</param>
        public virtual void ReloadLevelData(FilterLevelDelegate levelFilter, FilterMapAreaDelegate areaFilter, FilterMapSectionDelegate mapFilter)
        {
            this.m_Levels = new Dictionary<String, Level>();

            ConfigGameView gv = new RSG.Base.ConfigParser.ConfigGameView();
            ContentModel cm = gv.Content;

            // Grab all the levels names
            List<ContentNode> levelNodes = cm.Root.FindAll("level");
            foreach (ContentNode node in levelNodes)
            {
                Debug.Assert(node is ContentNodeLevel, "FindAll returned invalid node.");
                if (!(node is ContentNodeLevel))
                    continue;

                ContentNodeLevel levelNode = (node as ContentNodeLevel);

                Boolean loadLevel = true;
                if (levelFilter != null)
                    loadLevel = levelFilter(levelNode);

                if (loadLevel)
                {
                    Level level = new Level(levelNode.Name, cm, areaFilter, mapFilter);
                    this.m_Levels.Add(levelNode.Name, level);
                }
            }

            this.Loaded = true;
            if (null != LevelDataReloaded)
                LevelDataReloaded(this, EventArgs.Empty);
        }

        /// <summary>
        /// Reloads the level data while using a filter function for the levels
        /// </summary>
        /// <param name="levelFilter">The delegate function used to filter the levels</param>
        public virtual void ReloadLevelData(FilterLevelDelegate levelFilter)
        {
            this.m_Levels = new Dictionary<String, Level>();

            ConfigGameView gv = new RSG.Base.ConfigParser.ConfigGameView();
            ContentModel cm = gv.Content;

            // Grab all the levels names
            List<ContentNode> levelNodes = cm.Root.FindAll("level");
            foreach (ContentNode node in levelNodes)
            {
                Debug.Assert(node is ContentNodeLevel, "FindAll returned invalid node.");
                if (!(node is ContentNodeLevel))
                    continue;

                ContentNodeLevel levelNode = (node as ContentNodeLevel);

                Boolean loadLevel = true;
                if (levelFilter != null)
                    loadLevel = levelFilter(levelNode);

                if (loadLevel)
                {
                    Level level = new Level(levelNode.Name, cm);
                    this.m_Levels.Add(levelNode.Name, level);
                }
            }

            this.Loaded = true;
            if (null != LevelDataReloaded)
                LevelDataReloaded(this, EventArgs.Empty);
        }

        /// <summary>
        /// Reloads the level data while using a filter function for both the levels and the map sections
        /// </summary>
        /// <param name="areaFilter"></param>
        /// <param name="mapFilter">The delegate function used to filter the map sections</param>
        public virtual void ReloadLevelData(FilterMapAreaDelegate areaFilter, FilterMapSectionDelegate mapFilter)
        {
            this.m_Levels = new Dictionary<String, Level>();

            ConfigGameView gv = new RSG.Base.ConfigParser.ConfigGameView();
            ContentModel cm = gv.Content;

            // Grab all the levels names
            List<ContentNode> levelNodes = cm.Root.FindAll("level");
            foreach (ContentNode node in levelNodes)
            {
                Debug.Assert(node is ContentNodeLevel, "FindAll returned invalid node.");
                if (!(node is ContentNodeLevel))
                    continue;

                ContentNodeLevel levelNode = (node as ContentNodeLevel);

                Level level = null;
                if (mapFilter != null)
                    level = new Level(levelNode.Name, cm, areaFilter, mapFilter);
                else
                    level = new Level(levelNode.Name, cm);

                this.m_Levels.Add(levelNode.Name, level);
            }

            this.Loaded = true;
            if (null != LevelDataReloaded)
                LevelDataReloaded(this, EventArgs.Empty);

        }

        ///// <summary>
        ///// Parse Statistics in the scene and create the post process statistics for the props.
        ///// </summary>
        //private void GetStatistics()
        //{
        //    // Do a post process step that creates the prop statistics for each map section
        //    // Walk each map section and do the following:
        //    // If the map section only has ide data (i.e a prop file) get the stats for each prop by name and store size, polycount, and polydensity.
        //    Dictionary<String, SceneXml.Statistics.DrawableStats> propGeometryStatistics = new Dictionary<String, SceneXml.Statistics.DrawableStats>();
        //    Dictionary<String, List<SceneXml.Statistics.DrawableStats>> propCollisionStatistics = new Dictionary<String, List<SceneXml.Statistics.DrawableStats>>();
        //    foreach (Level level in this.m_Levels.Values)
        //    {
        //        foreach (IMapComponent mapArea in level.MapComponents.Values)
        //        {
        //            if (mapArea.Name == "props")
        //            {
        //                foreach (MapSection mapSection in Model.Map.Utilities.StaticMapHelperFunctions.GetMapSections(mapArea, true))
        //                {
        //                    try
        //                    {
        //                        if (mapSection.ExportInstances == false && mapSection.ExportDefinitions == true)
        //                        {
        //                            SceneXml.Scene scene = SceneManager.GetScene(mapSection.RpfFilename);
        //                            if (scene.Statistics != null && scene.Statistics.ValidStatistics == true)
        //                            {
        //                                foreach (KeyValuePair<Guid, SceneXml.Statistics.DrawableStats> stat in scene.Statistics.GeometryStats)
        //                                {
        //                                    if (!propGeometryStatistics.ContainsKey(stat.Value.Name))
        //                                    {
        //                                        propGeometryStatistics.Add(stat.Value.Name, stat.Value);
        //                                    }

        //                                    // Get this props collision stats if they exist.
        //                                    SceneXml.ObjectDef obj = scene.FindObject(stat.Key);
        //                                    foreach (SceneXml.ObjectDef child in obj.Children)
        //                                    {
        //                                        if (child.IsCollision())
        //                                        {
        //                                            SceneXml.Statistics.DrawableStats collisionStats = null;
        //                                            scene.Statistics.CollisionStats.TryGetValue(child.Guid, out collisionStats);
        //                                            if (!propCollisionStatistics.ContainsKey(stat.Value.Name))
        //                                            {
        //                                                propCollisionStatistics.Add(stat.Value.Name, new List<SceneXml.Statistics.DrawableStats>());
        //                                                propCollisionStatistics[stat.Value.Name].Add(collisionStats);
        //                                            }
        //                                            else
        //                                            {
        //                                                propCollisionStatistics[stat.Value.Name].Add(collisionStats);
        //                                            }
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                        }
        //                    }
        //                    catch
        //                    {
        //                        RSG.Base.Logging.Log.Log__Error("Error with map section {0} while getting prop stats.", mapSection.Name);
        //                    }
        //                }
        //            }
        //        }
        //    }

        //    // Get the neighbours of all the map containers
        //    foreach (Level level in this.m_Levels.Values)
        //    {
        //        foreach (MapSection mapSection in Model.Map.Utilities.StaticMapHelperFunctions.GetMapSections(level, true))
        //        {
        //            if (mapSection.ExportInstances == true && mapSection.ExportDefinitions == true && mapSection.Statistics != null)
        //            {
        //                try
        //                {
        //                    mapSection.Statistics.Neighbours.Clear();
        //                    RSG.Base.Math.BoundingBox2f boundingBox = new Base.Math.BoundingBox2f(mapSection.Statistics.BoundingBox);
        //                    // Make the bounding box 10 units bigger on each side.
        //                    boundingBox.Min.X -= 15.0f;
        //                    boundingBox.Min.Y -= 15.0f;
        //                    boundingBox.Max.X += 15.0f;
        //                    boundingBox.Max.Y += 15.0f;
        //                    foreach (MapSection othermapsections in Model.Map.Utilities.StaticMapHelperFunctions.GetMapSections(level, true))
        //                    {
        //                        if (othermapsections.ExportInstances == true && othermapsections.ExportDefinitions == true && othermapsections.Statistics != null && othermapsections != mapSection)
        //                        {
        //                            RSG.Base.Math.BoundingBox2f otherBox = new Base.Math.BoundingBox2f(othermapsections.Statistics.BoundingBox);
        //                            otherBox.Min.X -= 15.0f;
        //                            otherBox.Min.Y -= 15.0f;
        //                            otherBox.Max.X += 15.0f;
        //                            otherBox.Max.Y += 15.0f;

        //                            // Do they overlap
        //                            if (otherBox.Max.Y < boundingBox.Min.Y || otherBox.Min.Y > boundingBox.Max.Y)
        //                            {
        //                                continue;
        //                            }
        //                            if (otherBox.Max.X < boundingBox.Min.X || otherBox.Min.X > boundingBox.Max.X)
        //                            {
        //                                continue;
        //                            }

        //                            mapSection.Statistics.Neighbours.Add(othermapsections);
        //                        }
        //                    }
        //                }
        //                catch
        //                {
        //                    RSG.Base.Logging.Log.Log__Error("Error with map section {0} while getting neighbours.", mapSection.Name);
        //                }
        //            }
        //        }
        //    }

        //    foreach (Level level in this.m_Levels.Values)
        //    {
        //        foreach (MapSection mapSection in Model.Map.Utilities.StaticMapHelperFunctions.GetMapSections(level, true))
        //        {
        //            if (mapSection.ExportInstances == true && mapSection.ExportDefinitions == true && mapSection.Statistics != null)
        //            {
        //                try
        //                {
        //                    // Find the prop file if it exists and collect the props in it and put them into the map section
        //                    String propFilePath = Path.Combine(mapSection.IndependentPath, mapSection.Name + "_props.rpf");
        //                    if (File.Exists(propFilePath))
        //                    {
        //                        //SceneXml.Scene scene = SceneManager.GetScene(propFilePath);
        //                        MapSection propFile = Map.Utilities.StaticMapHelperFunctions.GetMapSection(mapSection.Level, mapSection.Name + "_props", true);
        //                        if (propFile != null && propFile.Statistics != null)
        //                        {
        //                            foreach (KeyValuePair<String, int> uniqueProp in propFile.Statistics.UniqueProps)
        //                            {
        //                                SceneXml.Statistics.DrawableStats drawableStat = null;
        //                                propGeometryStatistics.TryGetValue(uniqueProp.Key, out drawableStat);
        //                                List<SceneXml.Statistics.DrawableStats> collisionStats = null;
        //                                propCollisionStatistics.TryGetValue(uniqueProp.Key, out collisionStats);
        //                                SceneXml.Statistics.DrawableStats collisionStat = collisionStats == null ? null : collisionStats[0];

        //                                mapSection.Statistics.AddPropStatisticToSection(uniqueProp.Key, uniqueProp.Value, drawableStat, collisionStat);
        //                            }
        //                        }
        //                    }

        //                    // Make sure we add the static stats to the static Statistics
        //                    Map.Statistics.MapStatistics.PropCounts.AddStatistics(mapSection.Statistics.PropCount.High, 0, 0);
        //                    Map.Statistics.MapStatistics.UniquePropCounts.AddStatistics(mapSection.Statistics.UniquePropCount.High, 0, 0);
        //                    Map.Statistics.MapStatistics.CollisionPolygonCounts.AddStatistics(mapSection.Statistics.CollisionPolygonCount.High, mapSection.Statistics.CollisionPolygonCount.Lod, 0);
        //                }
        //                catch
        //                {
        //                    RSG.Base.Logging.Log.Log__Error("Error with map section {0} while getting props.", mapSection.Name);
        //                }
        //            }
        //        }
        //    }
        //}

        /// <summary>
        /// The pre load step creates the objects from the content tree but doesn't intialise them (i.e kept really quick).
        /// It returns a list of map sections that will then have to be initialised and also returns a total of all the map sections
        /// found in the content tree.
        /// </summary>
        public int DoPreLoadStep(FilterLevelDelegate levelFilter, FilterMapAreaDelegate areaFilter, FilterMapSectionDelegate mapFilter, ConfigGameView gv,
            ref List<MapSection> mapSections)
        {
            this.m_Levels = new Dictionary<String, Level>();
            int totalMapSections = 0;
            TextureFolder = System.IO.Path.Combine(gv.AssetsDir, "maps", "Textures");

            ContentModel cm = gv.Content;
            List<ContentNode> levelNodes = cm.Root.FindAll("level");

            foreach (ContentNode node in levelNodes)
            {
                Debug.Assert(node is ContentNodeLevel, "FindAll returned invalid node.");
                if (!(node is ContentNodeLevel))
                    continue;

                ContentNodeLevel levelNode = (node as ContentNodeLevel);

                Boolean loadLevel = true;
                if (levelFilter != null)
                    loadLevel = levelFilter(levelNode);

                if (loadLevel)
                {
                    Boolean alreadyAdded = this.m_Levels.ContainsKey(levelNode.Name);
                    if (alreadyAdded == false)
                    {
                        Level newLevel = new Level(levelNode.Name, cm.Root.FindFirst(levelNode.Name, "level") as ContentNodeLevel);
                        this.AddLevel(newLevel);
                        ContentNodeLevel group = cm.Root.FindFirst(levelNode.Name, "level") as ContentNodeLevel;
                        totalMapSections += DoPreLoadStepLevel(newLevel, group, cm, areaFilter, mapFilter, ref mapSections);
                    }
                }
            }

            return totalMapSections;
        }

        /// <summary>
        /// Collects the mapsections that need to have their scenes loaded and adds together a total. This
        /// also creates the mapsections/map areas underneath the given level
        /// </summary>
        private int DoPreLoadStepLevel(Level level, ContentNodeLevel group, ContentModel cm, FilterMapAreaDelegate areaFilter, FilterMapSectionDelegate mapFilter,
            ref List<MapSection> mapSections)
        {
            int totalMapSections = 0;

            foreach (ContentNode node in group.Children)
            {
                // Map Section
                if (node is ContentNodeMapZip)
                {
                    ContentNodeMapZip mapzip = (node as ContentNodeMapZip);
                    if (node.Inputs.Length == 0)
                        continue;

                    ContentNodeMap map = (node.Inputs[0] as ContentNodeMap);
                    if (level.MapComponents.ContainsKey(map.Name.ToLower()))
                        continue;

                    Boolean loadMap = true;
                    if (mapFilter != null)
                        loadMap = mapFilter(map);

                    if (loadMap)
                    {
                        MapSection newSection = new MapSection(level, level, map, mapzip);
                        level.MapComponents.Add(map.Name.ToLower(), newSection);
                        totalMapSections++;
                        mapSections.Add(newSection);
                    }
                }
                // Map Area 
                else if (node is ContentNodeGroup)
                {
                    ContentNodeGroup area = (node as ContentNodeGroup);
                    if (0 == String.Compare(area.RawRelativePath, String.Empty))
                        continue;

                    if (level.MapComponents.ContainsKey(area.RawRelativePath.ToLower()))
                        continue;

                    Boolean loadArea = true;
                    if (null != areaFilter)
                        loadArea = areaFilter(area);

                    if (loadArea)
                    {
                        MapArea newArea = new MapArea(level, area, level);
                        level.MapComponents.Add(area.RawRelativePath.ToLower(), newArea);
                        totalMapSections += DoPreLoadStepArea(newArea, level, node as ContentNodeGroup, areaFilter, mapFilter, ref mapSections);
                    }
                }
            }

            return totalMapSections;
        }

        /// <summary>
        /// Collects the mapsections that need to have their scenes loaded and adds together a total. This
        /// also creates the mapsections/map areas underneath the given container.
        /// </summary>
        private int DoPreLoadStepArea(IMapContainer container, Level level, ContentNodeGroup group, FilterMapAreaDelegate areaFilter, FilterMapSectionDelegate mapFilter,
            ref List<MapSection> mapSections)
        {
            int totalMapSections = 0;

            foreach (ContentNode node in group.Children)
            {
                // Map Section
                if (node is ContentNodeMapZip)
                {
                    ContentNodeMapZip mapzip = (node as ContentNodeMapZip);
                    if (node.Inputs.Length == 0)
                        continue;

                    ContentNodeMap map = (node.Inputs[0] as ContentNodeMap);
                    if (container.MapComponents.ContainsKey(map.Name.ToLower()))
                        continue;

                    Boolean loadMap = true;
                    if (mapFilter != null)
                        loadMap = mapFilter(map);

                    if (loadMap)
                    {
                        MapSection newSection = new MapSection(level, container, map, mapzip);
                        container.MapComponents.Add(map.Name.ToLower(), newSection);
                        totalMapSections++;
                        mapSections.Add(newSection);
                    }
                }
                // Map Area 
                else if (node is ContentNodeGroup)
                {
                    ContentNodeGroup area = (node as ContentNodeGroup);
                    if (0 == String.Compare(area.RawRelativePath, String.Empty))
                        continue;

                    if (container.MapComponents.ContainsKey(area.RawRelativePath.ToLower()))
                        continue;

                    Boolean loadArea = true;
                    if (null != areaFilter)
                        loadArea = areaFilter(area);

                    if (loadArea)
                    {
                        MapArea newArea = new MapArea(level, area, container);
                        container.MapComponents.Add(area.RawRelativePath.ToLower(), newArea);
                        totalMapSections += DoPreLoadStepArea(newArea, level, node as ContentNodeGroup, areaFilter, mapFilter, ref mapSections);
                    }
                }
            }

            return totalMapSections;
        }

        /// <summary>
        /// Adds a level to this level dictionary if it isn't already
        /// </summary>
        /// <param name="level"></param>
        public void AddLevel(Level level)
        {
            if (this.m_Levels == null)
            {
                this.m_Levels = new Dictionary<String, Level>();
            }
            if (!this.m_Levels.ContainsKey(level.Name))
            {
                this.m_Levels.Add(level.Name, level);
            }
        }
        
        /// <summary>
        /// Adds all the props in the given map section to the given list.
        /// </summary>
        public void GetAllReferenceStatistics(MapSection section, ref Dictionary<String, Dictionary<String, SingleReference>> allReferences)
        {
            String sourcePath = new Uri(section.SourceAssetFilename).AbsolutePath;
            if (section.SectionStatistics is PropFile)
            {
                PropFile sectionStatistics = section.SectionStatistics as PropFile;
                foreach (KeyValuePair<String, SingleProp> propStatistic in sectionStatistics.Props)
                {
                    if (!allReferences.ContainsKey(sourcePath))
                    {
                        allReferences.Add(sourcePath, new Dictionary<String, SingleReference>());
                    }
                    if (!allReferences[sourcePath].ContainsKey(propStatistic.Key))
                    {
                        allReferences[sourcePath].Add(propStatistic.Key, null);
                    }
                    allReferences[sourcePath][propStatistic.Key] = propStatistic.Value;
                }
            }
            else if (section.SectionStatistics is InteriorFile)
            {
                InteriorFile sectionStatistics = section.SectionStatistics as InteriorFile;
                SingleInterior interiorStatistic = sectionStatistics.InteriorStatistic;
                if (!String.IsNullOrEmpty(interiorStatistic.ReferenceName))
                {
                    if (!allReferences.ContainsKey(sourcePath))
                    {
                        allReferences.Add(sourcePath, new Dictionary<String, SingleReference>());
                    }
                    if (!allReferences[sourcePath].ContainsKey(interiorStatistic.ReferenceName))
                    {
                        allReferences[sourcePath].Add(interiorStatistic.ReferenceName, null);
                    }
                    allReferences[sourcePath][interiorStatistic.ReferenceName] = interiorStatistic;
                }
            }
        }

        /// <summary>
        /// Adds all the props/interiors in the given map section to the given list.
        /// </summary>
        public void GetAllReferenceStatistics(MapSection section, ref SectionReferences allReferences)
        {
            String sourcePath = new Uri(section.SourceAssetFilename).AbsolutePath;
            if (section.SectionStatistics is PropFile)
            {
                PropFile sectionStatistics = section.SectionStatistics as PropFile;
                Dictionary<SingleProp, uint> uniqueProps = new Dictionary<SingleProp, uint>();
                foreach (KeyValuePair<String, SingleProp> propStatistic in sectionStatistics.Props)
                {
                    Boolean foundProp = false;
                    foreach (SingleProp prop in uniqueProps.Keys)
                    {
                        if (prop.ReferenceName == propStatistic.Key)
                        {
                            uniqueProps[prop]++;
                            break;
                        }
                    }
                    if (foundProp == false)
                    {
                        uniqueProps.Add(propStatistic.Value, 1);
                    }
                }
                foreach (KeyValuePair<SingleProp, uint> prop in uniqueProps)
                {
                    SingleUniqueReference reference = new SingleUniqueReference(prop.Key.ReferenceName, 1);
                    reference.Statistics = prop.Key;
                    allReferences.AddReference(sourcePath, reference);
                }
            }
        }

        /// <summary>
        /// Adds all the props/interiors in the given map section to the given list.
        /// </summary>
        public void GetAllReferenceStatistics(MapSection section, LevelReferences allLevelReferences)
        {
            String sourcePath = new Uri(section.SourceAssetFilename).AbsolutePath;
            if (section.SectionStatistics is PropFile)
            {
                PropFile propStatistics = section.SectionStatistics as PropFile;
                Dictionary<SingleProp, uint> uniqueProps = new Dictionary<SingleProp, uint>();
                foreach (KeyValuePair<String, SingleProp> prop in propStatistics.Props)
                {
                    allLevelReferences.AddReference(sourcePath, prop.Key, prop.Value);
                }
            }
        }

        public void ResolveReferences(MapSection section, Dictionary<String, Dictionary<String, SingleReference>> allReferences)
        {
            if (section.SectionStatistics != null && section.SectionStatistics is IReferences)
            {
                IReferences sectionStatistics = section.SectionStatistics as IReferences;
                if (sectionStatistics.References.UniqueReferenceCount > 0)
                {
                    sectionStatistics.ResolveReferences(allReferences);
                }
            }
        }

        public void AddPropGroupsToSection(MapSection section)
        {
            // First thing to do is make sure all the references are found for the map section
            if (section.SectionStatistics != null && section.SectionStatistics is IReferences)
            {
                IReferences sectionStatistics = section.SectionStatistics as IReferences;

                // See if we have reference files for this map section (needs to be done through a naming convention)
                // DT1_01 - DT1_01_props
                MapSection referenceFile = StaticMapHelperFunctions.GetMapSectionByName(section.Level, section.Name + "_props", true);

                if (referenceFile != null && (referenceFile.SectionStatistics is IReferences == true))
                {
                    IReferences referenceStatistics = referenceFile.SectionStatistics as IReferences;

                    sectionStatistics.References.AppendReferences(referenceStatistics.References);
                    if (sectionStatistics is MapContainer)
                    {
                        (sectionStatistics as MapContainer).HasPropGroup = true;
                        if (referenceStatistics.MaxReferenceDistance > (sectionStatistics as MapContainer).MaxReferenceDistance)
                        {
                            (sectionStatistics as MapContainer).MaxReferenceDistance = referenceStatistics.MaxReferenceDistance;
                        }
                    }
                }
            }
        }

        #endregion // Controller Methods
    } // LevelDictionary
} // RSG.Model.Map namespace
