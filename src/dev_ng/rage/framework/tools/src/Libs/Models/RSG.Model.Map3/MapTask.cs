﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Threading;
using System.Threading;
using RSG.Model.Common;

namespace RSG.Model.Map3
{
    /// <summary>
    /// Special task used by the map stat streaming coordinator
    /// </summary>
    internal class MapTask : NamedTask
    {
        #region Properties
        /// <summary>
        /// Flag indicating whether this particular task can be run
        /// </summary>
        public bool CanRunTask
        {
            get
            {
                return Level.Initialised;
            }
        }

        /// <summary>
        /// Level this task is associated with
        /// </summary>
        private ILevel Level
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="action"></param>
        /// <param name="state"></param>
        /// <param name="cancellationToken"></param>
        public MapTask(ILevel level, string name, Action<object> action, object state, CancellationToken cancellationToken)
            : base(name, action, state, cancellationToken)
        {
            Level = level;
        }
        #endregion // Constructor(s)
    } // MapTask
}
