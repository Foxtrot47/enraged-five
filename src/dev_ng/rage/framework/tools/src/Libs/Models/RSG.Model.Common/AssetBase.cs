﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using System.Text.RegularExpressions;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace RSG.Model.Common
{
    /// <summary>
    /// Base class for all asset types
    /// </summary>
    [DataContract]
    [Serializable]
    public abstract class AssetBase : ModelBase, IAsset
    {
        #region Members

        protected String m_name;
        protected UInt32 m_hash;

        #endregion // Members

        #region Properties

        /// <summary>
        /// The name of the asset
        /// </summary>
        [DataMember]
        public String Name
        {
            get { return m_name; }
            protected set
            {
                SetPropertyValue(ref m_name, value, "Name");
            }
        }

        /// <summary>
        /// Unique hash code which identifies this asset
        /// </summary>
        [DataMember]
        [Browsable(false)]
        public UInt32 Hash
        {
            get { return m_hash; }
            protected set
            {
                SetPropertyValue(ref m_hash, value, "Hash");
            }
        }

        #endregion // Properties

        #region Constructors

        /// <summary>
        /// Default Constructor
        /// </summary>
        public AssetBase()
            : base()
        {
            this.m_name = "Unknown Asset";
            this.m_hash = 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        public AssetBase(String name)
            : base()
        {
            this.m_name = name;
            this.m_hash = RSG.Base.Security.Cryptography.OneAtATime.ComputeHash(m_name);
        }

        #endregion // Constructors

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public virtual List<IAsset> Find(Regex expression)
        {
            if (!string.IsNullOrEmpty(this.Name))
            {
                if (expression.IsMatch(this.Name))
                    return new List<IAsset>() { this };
            }
            return new List<IAsset>();
        }
        #endregion

        #region IComparable<AssetBase> Implementation
        /// <summary>
        /// Compares the current object with another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>A value that indicates the relative order of the objects being compared.
        ///     The return value has the following meanings: Value Meaning Less than zero
        ///     This object is less than the other parameter.Zero This object is equal to
        ///     other. Greater than zero This object is greater than other.</returns>
        public int CompareTo(IAsset other)
        {
            return Name.CompareTo(other.Name);
        }
        #endregion // IComparable<MapDefinition> Implementation

        #region Comparable Implementation
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int CompareTo(object obj)
        {
            if (obj == null) return 1;

            IAsset other = obj as IAsset;
            if (other != null)
                return CompareTo(other);
            else
                throw new ArgumentException("Object is not a IAsset");
        }
        #endregion // Comparable Implementation

        #region Object Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return (String.Format("Asset: [{0}]{1}", this.Name, this.Hash));
        }
        #endregion // Object Overrides
    } // AssetBase
} // RSG.Model.Common
