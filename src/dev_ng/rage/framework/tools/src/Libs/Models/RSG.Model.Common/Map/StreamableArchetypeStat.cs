﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common.Map
{
    /// <summary>
    /// Base class for streamable archetype stat enumerations.
    /// </summary>
    public static class StreamableArchetypeStat
    {
        public const String LodDistanceString = "09CF66AA-4398-490C-9F9C-636717A0FF76";
        public const String SceneLinksString  = "76F2D83B-06C4-40C8-A19A-3175DB718167";

        public static readonly StreamableStat LodDistance = new StreamableStat(LodDistanceString, true);
        public static readonly StreamableStat SceneLinks = new StreamableStat(SceneLinksString, true);
    } // StreamableArchetypeStat
}
