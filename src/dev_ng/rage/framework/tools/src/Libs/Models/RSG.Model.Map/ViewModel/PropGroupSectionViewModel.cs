﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Collections;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using RSG.Model.Map;


namespace RSG.Model.Map.ViewModel
{
    public class PropGroupSectionViewModel :
        RSG.Base.Editor.HierarchicalViewModelBase, IMapViewModelComponent, IMapSectionViewModel
    {
        #region Properties

        /// <summary>
        /// The name of the map section
        /// </summary>
        public String Name
        {
            get { return Model.Name.ToLower(); }
        }

        /// <summary>
        /// The model reference that this view model represents.
        /// </summary>
        public MapSection Model
        {
            get { return m_model; }
            set { m_model = value; }
        }
        private MapSection m_model;

        /// <summary>
        /// The level that this section belongs to.
        /// </summary>
        public LevelViewModel Level
        {
            get;
            private set;
        }

        /// <summary>
        /// Returns true iff this map section represents a non generic map section
        /// that also exports definitions. (i.e exports a ipl file and a ide file).
        /// </summary>
        public Boolean NonGenericWithDefinitions
        {
            get;
            private set;
        }

        /// <summary>
        /// The user data that can be attached to this view model
        /// </summary>
        public UserData ViewModelUserData
        {
            get { return m_viewModelUserData; }
            set { m_viewModelUserData = value; }
        }
        private UserData m_viewModelUserData = new UserData();

        public ObservableCollection<MapInstanceViewModel> Instances
        {
            get { return m_instances; }
            set
            {
                SetPropertyValue(value, () => this.Instances,
                    new PropertySetDelegate(delegate(Object newValue) { m_instances = (ObservableCollection<MapInstanceViewModel>)newValue; }));
            }
        }
        private ObservableCollection<MapInstanceViewModel> m_instances;

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Default Constructor
        /// </summary>
        public PropGroupSectionViewModel(IViewModel parent, MapSection model)
        {
            this.Parent = parent;
            this.Model = model;
        }

        #endregion // Constructors

        #region Override Functions

        protected override void OnFirstExpanded()
        {
            if (this.Model.Instances != null)
            {
                ObservableCollection<MapInstanceViewModel> instances = new ObservableCollection<MapInstanceViewModel>();
                foreach (MapInstance instance in this.Model.Instances)
                {
                    instances.Add(new MapInstanceViewModel(this, instance));
                }
                this.Instances = instances;
            }
        }

        public override IViewModel GetChildWithString(String name)
        {
            foreach (MapInstanceViewModel child in this.Instances)
            {
                if (String.Compare(child.Name, name, true) == 0)
                {
                    return child as IViewModel;
                }
            }
            return null;
        }

        #endregion // Override Functions

        #region Public Methods

        public void UpdateChildrenWithModel()
        {
            ObservableCollection<MapInstanceViewModel> instances = new ObservableCollection<MapInstanceViewModel>();
            instances.Add(new MapInstanceViewModel());
            this.Instances = instances;
        }

        #endregion // Public Methods
    } // PropSectionViewModel
} // Workbench.AddIn.MapBrowser.MapViewModel
