﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common
{

    /// <summary>
    /// IHasLevel interface; defining that an object has an associated ILevel
    /// object.
    /// </summary>
    public interface IHasLevel
    {
        /// <summary>
        /// Associated ILevel object.
        /// </summary>
        ILevel Level { get; }
    }

} // RSG.Model.Common namespace
