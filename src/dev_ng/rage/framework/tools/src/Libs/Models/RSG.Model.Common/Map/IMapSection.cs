﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.ConfigParser;
using RSG.Base.Collections;
using RSG.Base.Math;
using RSG.Platform;

namespace RSG.Model.Common.Map
{
    /// <summary>
    /// A map section.  This is the only leaf node in the map hierarchy.
    /// </summary>
    public interface IMapSection : IMapNode, IHasEntityChildren, IStreamableObject
    {
        #region Properties
        /// <summary>
        /// Type of section this is (e.g. Container, Props, Interiors, etc...).
        /// </summary>
        SectionType Type { get; }

        /// <summary>
        /// Name of the exported zip file.
        /// </summary>
        String ExportZipFilepath { get; }

        /// <summary>
        /// DCC source filename (excluding extension).
        /// </summary>
        String DCCSourceFilename { get; }

        /// <summary>
        /// The points for this section gotten from the levels vector map.
        /// </summary>
        IList<Vector2f> VectorMapPoints { get; set; }

        /// <summary>
        /// Array of lists of neighbours (should contain a list per neighbour mode).
        /// </summary>
        IList<IMapSection>[] Neighbours { get; }

        /// <summary>
        /// The area of the section based on the current vector map points.
        /// </summary>
        float Area { get; }

        /// <summary>
        /// The prop group to this map section if one exists and is applicable.
        /// </summary>
        IMapSection PropGroup { get; }

        /// <summary>
        /// Gets a value indicating whether this map section is considered to be a prop group
        /// for another map section.
        /// </summary>
        bool IsPropGroup { get; set; }

        /// <summary>
        /// Whether the section exports archetypes.
        /// </summary>
        bool ExportArchetypes { get; }

        /// <summary>
        /// Whether the section exports entities.
        /// </summary>
        bool ExportEntities { get; }

        /// <summary>
        /// List of archetypes this map section contains.
        /// </summary>
        ObservableCollection<IMapArchetype> Archetypes { get; }

        /// <summary>
        /// Filters the list of archetypes to return only interiors.
        /// </summary>
        IEnumerable<IInteriorArchetype> InteriorArchetypes { get; }

        /// <summary>
        /// Filters the list of archetypes to return only non-interior archetype.
        /// </summary>
        IEnumerable<IMapArchetype> NonInteriorArchetypes { get; }

        /// <summary>
        /// List of car gens this map section contains.
        /// </summary>
        ObservableCollection<ICarGen> CarGens { get; }

        /// <summary>
        /// Map sections top level collision poly count.
        /// </summary>
        uint CollisionPolygonCount { get; }

        /// <summary>
        /// Collision poly counts by collision type flags.
        /// </summary>
        IList<CollisionInfo> CollisionPolygonCountBreakdown { get; }

        /// <summary>
        /// Last person to export this section.
        /// </summary>
        String LastExportUser { get; }

        /// <summary>
        /// Last time this section was exported.
        /// </summary>
        DateTime LastExportTime { get; }

        /// <summary>
        /// Container attributes...
        /// </summary>
        IDictionary<String, object> ContainerAttributes { get; }

        /// <summary>
        /// Platform stats for this map section.
        /// </summary>
        IDictionary<RSG.Platform.Platform, IMapSectionPlatformStat> PlatformStats { get; }

        /// <summary>
        /// Statistics for this section which have been aggregated from the section's archetypes/entities.
        /// </summary>
        IDictionary<StatKey, IMapSectionStatistic> AggregatedStats { get; }

        /// <summary>
        /// Gets the attribute guid set on the container for this section.
        /// </summary>
        string ContainerAttributeGuid { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Generate the neighbours for this section.
        /// </summary>
        void GenerateNeighbours();

        /// <summary>
        /// Accessor for setting the prop group for this section.
        /// This should really be done in the setter shouldn't it?!?
        /// </summary>
        /// <param name="propSection"></param>
        void SetPropGroup(IMapSection propSection);

        /// <summary>
        /// Set the neighbours for this section.
        /// Which neighbour method is this for?
        /// </summary>
        /// <param name="neighbours"></param>
        void SetNeighbours(IList<IMapSection>[] neighbours);
        #endregion // Methods
    } // IMapSection

    /// <summary>
    /// Interface for map section platform stats.
    /// </summary>
    public interface IMapSectionPlatformStat
    {
        /// <summary>
        /// Platform for this stat.
        /// </summary>
        RSG.Platform.Platform Platform { get; }

        /// <summary>
        /// Physical sizes on a per file type basis.
        /// </summary>
        IDictionary<FileType, IMemoryStat> FileTypeSizes { get; }

        /// <summary>
        /// Total size of the sections.
        /// </summary>
        long TotalPhysicalSize { get; }

        /// <summary>
        /// Total size of the sections.
        /// </summary>
        long TotalVirtualSize { get; }
    } // IMapSectionPlatformStat
}
