﻿using System;
using System.Windows;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using System.Collections.ObjectModel;
using RSG.Base.Collections;
using RSG.Model.Map.TXDHierarchy;
using System.Windows.Media.Imaging;
using System.Drawing;

namespace RSG.Model.Map.TXDHierarchy
{
    public class TXDHierarchy
    {
        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        public static extern bool DeleteObject(IntPtr hObject);

        #region Constants
        #region XPath Compiled Expressions
        private static readonly XPathExpression sC_s_xpath_XPathRootDictionaries =
            XPathExpression.Compile("/Global_Dictionaries/UserDictionary");
        #endregion // XPath Compiled Expressions
        #endregion

        #region Properties and Associated Member Data

        public RSG.Base.Collections.ObservableCollection<UserTXD> Root
        {
            get { return m_Root; }
        }
        private RSG.Base.Collections.ObservableCollection<UserTXD> m_Root;

        public String Name
        {
            get { return m_sName; }
            set { m_sName = value; }
        }
        public String m_sName;

        public String Filename
        {
            get { return m_sFilename; }
            set { m_sFilename = value; }
        }
        public String m_sFilename;

        #endregion

        #region Constructor(s)

        public TXDHierarchy()
        {
            UserTXD newRoot = new UserTXD("Global Texture Dictionaries", null, this);
            m_Root = new RSG.Base.Collections.ObservableCollection<UserTXD>();
            m_Root.Add(newRoot);

            this.Filename = String.Empty;
            this.Name = String.Empty;
        }

        public TXDHierarchy(String filename, String Name)
        {
            // Create the dummy node at the top of the hierarchy
            UserTXD NewRoot = new UserTXD("Global Texture Dictionaries", null, this);
            m_Root = new RSG.Base.Collections.ObservableCollection<UserTXD>();
            m_Root.Add(NewRoot);

            m_sFilename = filename;
            m_sName = "Global Texture Dictionaries";
        }

        public TXDHierarchy(String filename)
        {
            // Create the dummy node at the top of the hierarchy
            UserTXD NewRoot = new UserTXD("Global Texture Dictionaries", null, this);
            m_Root = new RSG.Base.Collections.ObservableCollection<UserTXD>();
            m_Root.Add(NewRoot);

            m_sFilename = filename;
            m_sName = "Global Texture Dictionaries";

            this.LoadHierarchy(filename);
        }

        #endregion

        #region Public Function(s)

        /// <summary>
        /// Clears all of the children from the hierarchy and recreates the dummy node
        /// </summary>
        public void ResetHierarchy()
        {
            m_Root = null;
            m_sFilename = "New Txd Document";

            // Create the dummy node at the top of the hierarchy
            UserTXD NewRoot = new UserTXD("Global Texture Dictionaries", null, this);
            m_Root = new RSG.Base.Collections.ObservableCollection<UserTXD>();
            m_Root.Add(NewRoot);
        }

        /// <summary>
        /// Load a hierarchy from the given filename, this function resets the hierarchy and
        /// changes the filename to the one given.
        /// </summary>
        /// <param name="filename"></param>
        public void LoadHierarchy(String filename)
        {
            m_Root = null;
            m_sFilename = filename;

            // Create the dummy node at the top of the hierarchy
            UserTXD NewRoot = new UserTXD("Global Texture Dictionaries", null, this);
            m_Root = new RSG.Base.Collections.ObservableCollection<UserTXD>();
            m_Root.Add(NewRoot);

            DeserialiseHierarchy();
        }

        /// <summary>
        /// Saves an entire hierarchy to the file "filename member"
        /// </summary>
        public void SaveHierarchy()
        {
            SerialiseHierarchy();
        }

        /// <summary>
        /// Exports the global texture dictionaries into the two files given.
        /// </summary>
        /// <param name="ideFilename"></param>
        /// <param name="rpfFilename"></param>
        public void Export(String ideFilename, String rpfFilename, String projectName)
        {
            if (this.ContainsEmptyDictionaries())
            {
                MessageBox.Show("Unable to export the file " + this.Filename + " due to the fact that it contains empty texture dictionaries", "Export Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            Dictionary<String, String> parents = new Dictionary<String, String>();
            m_Root[0].ResurseCollectParents(parents);

            TextWriter textWriter = new StreamWriter(ideFilename);

            textWriter.WriteLine("txdp");
            foreach (KeyValuePair<String, String> parent in parents)
            {
                if (parent.Value != "Global Texture Dictionaries")
                {
                    textWriter.WriteLine("{0}, {1}", parent.Key, parent.Value);
                }
            }
            textWriter.WriteLine("end");
            textWriter.Close();

            CreateRubyExportScript(rpfFilename, projectName);

            RunRubyExportScript();
        }

        /// <summary>
        /// Determines whether there is a global dictionary name with the given name in this hierarchy
        /// this is used to make sure that any dictionary added will have a unique name.
        /// </summary>
        /// <param name="dictionaryName"></param>
        /// <returns></returns>
        public bool ContainsDictionaryName(String dictionaryName)
        {
            return m_Root[0].RecurseContainsDictionaryName(dictionaryName);
        }

        /// <summary>
        /// The main save function, this creates and file and loops through the whole tree
        /// serialising the objects out to the file.
        /// </summary>
        public void SerialiseHierarchy()
        {
            XmlDocument xmlDoc = new XmlDocument();

            XmlDeclaration xmlDecl = xmlDoc.CreateXmlDeclaration("1.0", "utf-8", String.Empty);
            xmlDoc.AppendChild(xmlDecl);

            // Add the root node
            XmlElement rootNode = xmlDoc.CreateElement("Global_Dictionaries");
            xmlDoc.InsertBefore(xmlDecl, xmlDoc.DocumentElement);

            // Add a new user txd element to the root node for each parent texture dictionary
            foreach (UserTXD txd in m_Root[0].ChildrenUserTXD.Values)
            {
                txd.Serialise(xmlDoc, rootNode);
            }

            xmlDoc.AppendChild(rootNode);
            xmlDoc.Save(m_sFilename);
        }

        /// <summary>
        ///  Determines whether this hierarchy contains the map with the given name,
        ///  and if it does returns the parent of that map txd.
        /// </summary>
        /// <param name="mapTxdName"></param>
        /// <returns></returns>
        public UserTXD HierarachyContainsMapTXD(String mapTxdName)
        {
            return m_Root[0].ResurseContainsMapTXD(mapTxdName);
        }

        /// <summary>
        /// Finds the first occurance to the given obj name
        /// </summary>
        /// <param name="objName"></param>
        /// <param name="parentsToObject">Lists the parents it has to go through to get to that object</param>
        /// <returns>The object it found</returns>
        public Object FindFirst(String objName, List<Object> parentsToObject, Boolean matchWhole, Boolean caseSensitive)
        {
            foreach (UserTXD root in m_Root)
            {
                Object result = root.FindFirst(objName, parentsToObject, matchWhole, caseSensitive);
                if (result != null)
                {
                    return result;
                }
            }

            return null;
        }

        /// <summary>
        /// Finds the last occurance to the given obj name
        /// </summary>
        /// <param name="objName"></param>
        /// <param name="parentsToObject">Lists the parents it has to go through to get to that object</param>
        /// <returns>The object it found</returns>
        public Object FindLast(String objName, List<Object> parentsToObject, Boolean matchWhole, Boolean caseSensitive)
        {
            foreach (UserTXD root in m_Root.Reverse<UserTXD>())
            {
                Object result = root.FindLast(objName, parentsToObject, matchWhole, caseSensitive);
                if (result != null)
                {
                    return result;
                }
            }

            return null;
        }

        /// <summary>
        /// Finds the next occurance to the given obj name starting at the object given in the parentsToLastObject parameter
        /// </summary>
        /// <param name="objName"></param>
        /// <param name="parentsToObject">Lists the parents it has to go through to get to that object</param>
        /// <returns>The object it found</returns>
        public void FindAll(String objName, Collection<List<Object>> parentsToObjects, Collection<Object> results, Boolean matchWhole, Boolean caseSensitive)
        {
            foreach (UserTXD root in m_Root)
            {
                root.FindAll(objName, parentsToObjects, results, matchWhole, caseSensitive);
            }
        }

        /// <summary>
        /// Returns true if any of the dictionaries in this hierarchy are empty (i.e no textures in them)
        /// </summary>
        /// <returns></returns>
        public Boolean ContainsEmptyDictionaries()
        {
            foreach (UserTXD child in m_Root[0].ChildrenUserTXD.Values)
            {
                if (child.ContainsEmptyDictionaries()) return true; 
            }
            return false;
        }

        #endregion

        #region Private Function(s)

        /// <summary>
        /// Deserialise the hierarchy by looping through all the elements in the given xml document.
        /// If the document cannot be found an exception is thrown.
        /// </summary>
        private void DeserialiseHierarchy()
        {
            XmlDocument document = new XmlDocument();
            if (!System.IO.File.Exists(m_sFilename))
            {
                RSG.Base.Logging.Log.Log__Warning("Unable to open the parent texture dictionary file located at " + m_sFilename);
                return;
            }
            try
            {
                document.Load(m_sFilename);
            }
            catch (XmlException ex)
            {
                RSG.Base.Logging.Log.Log__Exception(ex, "Unable to open the parent texture dictionary file located at " + m_sFilename);
                return;
            }
            XPathNavigator navigator = document.CreateNavigator();
            XPathNodeIterator dictionaryIt = navigator.Select(sC_s_xpath_XPathRootDictionaries);

            while (dictionaryIt.MoveNext())
            {
                XPathNavigator dictionaryNavigator = dictionaryIt.Current;
                UserTXD newDictionary = new UserTXD(dictionaryNavigator, m_Root[0], this);

                m_Root[0].AddNewUserTXDChild(newDictionary);
            }
        }

        private const String EXPORT_COMMAND = "%RS_TOOLSROOT%\\bin\\ruby\\bin\\ruby.exe";
        private const String EXPORT_SCRIPT = "%RS_TOOLSROOT%\\script\\misc\\data_mk_txd_rpf.rb";
        private List<String> m_missingTextures = new List<String>();
        private System.Diagnostics.Process m_exportProcess;

        /// <summary>
        /// Creates the ruby script that is executed during the export process.
        /// </summary>
        /// <param name="rpfFilename"></param>
        /// <param name="projectName"></param>
        private void CreateRubyExportScript(String rpfFilename, String projectName)
        {
            String rpfDirectory = System.IO.Path.GetDirectoryName(rpfFilename);
            String filename = System.Environment.ExpandEnvironmentVariables(EXPORT_SCRIPT);
            TextWriter textWriter = new StreamWriter(filename);

            textWriter.WriteLine("require 'pipeline/config/projects'");
            textWriter.WriteLine("require 'pipeline/config/project'");
            textWriter.WriteLine("require 'pipeline/os/file'");
            textWriter.WriteLine("require 'pipeline/os/getopt'");
            textWriter.WriteLine("require 'pipeline/os/path'");
            textWriter.WriteLine("require 'pipeline/util/environment'");
            textWriter.WriteLine("require 'pipeline/util/rage'");
            textWriter.WriteLine("require 'pipeline/projectutil/data_convert'");
            textWriter.WriteLine("require 'pipeline/gui/exception_dialog'");
            textWriter.WriteLine("require 'pipeline/projectutil/data_extract'");
            textWriter.WriteLine("require 'pipeline/scm/perforce'");
            textWriter.WriteLine("include Pipeline");

            textWriter.WriteLine("if ( __FILE__ == $0 ) then");
            textWriter.WriteLine("\tbegin");

            textWriter.WriteLine("\t\t#-----------------------------------------------------------------------------");
            textWriter.WriteLine("\t\t# Global varaibles to create the pack files with.");
            textWriter.WriteLine("\t\t#-----------------------------------------------------------------------------");
            textWriter.WriteLine("\t\tg_Config = Pipeline::Config.instance( )");
            textWriter.WriteLine("\t\tg_Project = g_Config.projects[ '" + projectName + "' ]");
            textWriter.WriteLine("\t\tr = RageUtils.new( g_Project, 'dev' )");

            textWriter.WriteLine("\n\n\t\t#-----------------------------------------------------------------------------");
            textWriter.WriteLine("\t\t# Global hash varaible containing the dictionaries and the textures.");
            textWriter.WriteLine("\t\t#-----------------------------------------------------------------------------");
            textWriter.WriteLine("\t\tTextureDictionaries = Hash.new");

            // First create the has array by exporting the indiviual userTXDs
            UserTXD.ExportIndex = 0;
            m_missingTextures.Clear();
            foreach (UserTXD txd in m_Root[0].ChildrenUserTXD.Values)
            {
                txd.Export(textWriter, m_missingTextures);
            }

            // Now that the has varaible is created loop through it and create a itd file for each texture dictionary
            textWriter.WriteLine("\n\n\t\t#-----------------------------------------------------------------------------");
            textWriter.WriteLine("\t\t# Create the texture dictionaries itd files.");
            textWriter.WriteLine("\t\t#-----------------------------------------------------------------------------");
            textWriter.WriteLine("\t\titd_files = []");
            textWriter.WriteLine("\t\tTextureDictionaries.each { | key, values |");
            textWriter.WriteLine("\t\t\titd_filepath = Pipeline::OS::Path::combine( '" + rpfDirectory + "', \"#{key}.itd\" )");
            textWriter.WriteLine("\t\t\tputs( \"itd_filepath = #{itd_filepath}\" )");
            textWriter.WriteLine("\t\t\tr.pack.start_uncompressed( )");
            textWriter.WriteLine("\t\t\tvalues.each { | value |");
            textWriter.WriteLine("\t\t\t\tfilename = OS::Path::get_filename( value )");
            textWriter.WriteLine("\t\t\t\tputs( \"filename = #{filename}\" ) ");
            textWriter.WriteLine("\t\t\t\tr.pack.add( value, filename )");
            textWriter.WriteLine("\t\t\t}");
            textWriter.WriteLine("\t\t\tr.pack.save( itd_filepath )");
            textWriter.WriteLine("\t\t\tr.pack.close( )");
            textWriter.WriteLine("\t\t\titd_files.push itd_filepath");
            textWriter.WriteLine("\t\t}");

            // Now that we have the indiviual itd files for the texture dictionaries pack them into the main rpf file.
            textWriter.WriteLine("\n\n\t\t#-----------------------------------------------------------------------------");
            textWriter.WriteLine("\t\t# Create the main rpf file x:/gta5/build/dev/independent/data/cdimages/gtxd.rpf.");
            textWriter.WriteLine("\t\t#-----------------------------------------------------------------------------");
            textWriter.WriteLine("\t\tr.pack.start_uncompressed( )");
            textWriter.WriteLine("\t\titd_files.each { | itd_file |");
            textWriter.WriteLine("\t\t\tfilename = OS::Path::get_filename( itd_file )");
            textWriter.WriteLine("\t\t\tr.pack.add( itd_file, filename )");
            textWriter.WriteLine("\t\t}");
            textWriter.WriteLine("\t\tr.pack.save( '" + rpfFilename + "' )");
            textWriter.WriteLine("\t\tr.pack.close( )");

            // Just clean up the created itd files since they are no longer needed and clean up any changelists we have created and left empty.
            textWriter.WriteLine("\n\n\t\t#-----------------------------------------------------------------------------");
            textWriter.WriteLine("\t\t# Delete the created itd files and any changelist that is empty.");
            textWriter.WriteLine("\t\t#-----------------------------------------------------------------------------");
            textWriter.WriteLine("\t\titd_files.each { | itd_file |");
            textWriter.WriteLine("\t\t\tFile.delete( itd_file )");
            textWriter.WriteLine("\t\t}");

            textWriter.WriteLine("\n\t\tProjectUtil::data_convert_file( '" + rpfFilename + "', true )");

            textWriter.WriteLine("\trescue Exception => ex");
            textWriter.WriteLine("\t\tputs 'Unhandled exception: #{ex.message}'");
            textWriter.WriteLine("\t\tPipeline::GUI::ExceptionDialog.show_dialog( ex )");
            textWriter.WriteLine("\t\texit( 1 )");

            textWriter.WriteLine("\n\tend");
            textWriter.WriteLine("end");
            textWriter.Close();
        }

        /// <summary>
        /// Runs the ruby script that was created above.
        /// </summary>
        private void RunRubyExportScript()
        {
            try
            {
                m_exportProcess = new System.Diagnostics.Process();
                m_exportProcess.StartInfo.FileName = Environment.ExpandEnvironmentVariables(EXPORT_COMMAND);
                m_exportProcess.StartInfo.Arguments = Environment.ExpandEnvironmentVariables(EXPORT_SCRIPT);
                m_exportProcess.StartInfo.UseShellExecute = false;
                m_exportProcess.StartInfo.CreateNoWindow = false;
                m_exportProcess.StartInfo.RedirectStandardOutput = true;
                m_exportProcess.StartInfo.RedirectStandardInput = true;
                m_exportProcess.StartInfo.RedirectStandardError = true;

                m_exportProcess.OutputDataReceived += StdoutHandler;
                m_exportProcess.ErrorDataReceived += StderrHandler;
                m_exportProcess.EnableRaisingEvents = true;
                m_exportProcess.Exited += OnExportFinished;

                m_exportProcess.Start();
                m_exportProcess.BeginOutputReadLine();
                m_exportProcess.BeginErrorReadLine();

                m_exportProcess.WaitForExit();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unhandled exception caught in the export process.", "Export Error", MessageBoxButton.OK, MessageBoxImage.Error);
                RSG.Base.Logging.Log.Log__Exception(ex, "Unhandled exception exporting data");
            }
        }

        /// <summary>
        /// The function that determines what happens to the std outputs of the ruby script
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StdoutHandler(Object sender, System.Diagnostics.DataReceivedEventArgs e)
        {
            RSG.Base.Logging.Log.Log__Message(e.Data);
        }

        /// <summary>
        /// The function that determines what happens to the std error output of the ruby script
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StderrHandler(Object sender, System.Diagnostics.DataReceivedEventArgs e)
        {
            RSG.Base.Logging.Log.Log__Error(e.Data);
        }

        /// <summary>
        ///  Determines if the ruby script was succesfull by taking the exit code of the script.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnExportFinished(Object sender, System.EventArgs e)
        {
            // Just display a message box that says that the export has finished successfully.
            if (m_exportProcess.ExitCode != 0)
            {
                // process has finished but there has been an error
                MessageBox.Show("An error has occurred in the export process. The script has returned a non-zero value of " + m_exportProcess.ExitCode.ToString() + ".", "Export Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else if (m_missingTextures.Count > 0)
            {
                String message = "The export process has finished, however some textures couldn't be found in the stream and therefore are missing from the rpf file.\n";

                foreach (String texture in m_missingTextures)
                {
                    message += ("\n" + texture);
                }

                MessageBox.Show(message, "Missing Textures", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                MessageBox.Show("The export process has finished successfully.", "Successful", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        #endregion
    }
}
