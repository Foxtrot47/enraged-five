﻿namespace RSG.Model.Vehicle.Game
{
    using System.Collections.Generic;
    using System.Diagnostics;

    /// <summary>
    /// Represents a single Vehicle that is being mounted by the game through the vehicles.meta
    /// data file.
    /// </summary>
    [DebuggerDisplay("Name = {Name}, Model = {ModelName}")]
    public class GameVehicle
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Name"/> property.
        /// </summary>
        private string _name;

        /// <summary>
        /// The private field used for the <see cref="ModelName"/> property.
        /// </summary>
        private string _modelName;

        /// <summary>
        /// The private field used for the <see cref="TextureDictionary"/> property.
        /// </summary>
        private string _textureDictionary;

        /// <summary>
        /// The private field used for the <see cref="RoofClipDictionary"/> property.
        /// </summary>
        private string _roofClipDictionary;

        /// <summary>
        /// The private field used for the <see cref="Variation"/> property.
        /// </summary>
        private GameVehicleVariation _variation;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="GameVehicle"/> class.
        /// </summary>
        public GameVehicle()
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the name used for this vehicle.
        /// </summary>
        public string Name
        {
            get { return this._name; }
            set { this._name = value; }
        }

        /// <summary>
        /// Gets or sets the name of the model used for this vehicle.
        /// </summary>
        public string ModelName
        {
            get { return this._modelName; }
            set { this._modelName = value; }
        }

        /// <summary>
        /// Gets or sets the name of the texture dictionary containing the textures used on
        /// this vehicle.
        /// </summary>
        public string TextureDictionary
        {
            get { return this._textureDictionary; }
            set { this._textureDictionary = value; }
        }

        /// <summary>
        /// Gets or sets the name of the clip dictionary containing the animation used for the
        /// roof of the vehicle.
        /// </summary>
        public string RoofClipDictionary
        {
            get { return this._roofClipDictionary; }
            set { this._roofClipDictionary = value; }
        }

        /// <summary>
        /// Gets or sets the vehicle variation used for this vehicle.
        /// </summary>
        public GameVehicleVariation Variation
        {
            get
            {
                return this._variation;
            }

            set
            {
                if (this._variation != null)
                {
                    this._variation.ReferenceCount--;
                    foreach (KeyValuePair<string, GameVehicleKit> kit in this._variation.Kits)
                    {
                        if (kit.Value != null)
                        {
                            kit.Value.ReferenceCount--;
                        }
                    }
                }

                this._variation = value;
                if (this._variation != null)
                {
                    this._variation.ReferenceCount++;
                    foreach (KeyValuePair<string, GameVehicleKit> kit in this._variation.Kits)
                    {
                        if (kit.Value != null)
                        {
                            kit.Value.ReferenceCount++;
                        }
                    }
                }
            }
        }
        #endregion Properties
    } // RSG.Model.Vehicle.Game.GameVehicle {Class}
} // RSG.Model.Vehicle.Game {Namespace}
