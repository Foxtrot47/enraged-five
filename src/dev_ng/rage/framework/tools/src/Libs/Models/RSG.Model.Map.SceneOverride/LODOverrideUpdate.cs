﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Map.SceneOverride
{
    /// <summary>
    /// A resolved distance update for a single node.  This is useful for updating source data.
    /// </summary>
    public class LODOverrideUpdate
    {
        public LODOverrideUpdate(string nodeName, float distance)
        {
            NodeName = nodeName;
            Distance = distance;
        }

        public string NodeName { get; internal set; }
        public float Distance { get; internal set; }
    }
}
