﻿namespace RSG.Model.Report2.Reports.Map
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using RSG.Base.ConfigParser;
    using RSG.Base.Tasks;
    using RSG.Model.Common;
    using RSG.Model.Common.Map;
    using RSG.Model.Report;
    using RSG.ManagedRage;
    using RSG.Platform;
    using RSG.Model.Map.SceneOverride;

    /// <summary>
    /// A report that lists the entities with columns that can be used to filter to show
    /// invlaid entities based on a number of attributes.
    /// </summary>
    public class MapEntitiesReport : CSVReport, IDynamicLevelReport
    {
        #region Fields
        /// <summary>
        /// The name of this report.
        /// </summary>
        private const string Name = "Map Entities";

        /// <summary>
        /// The description for this report.
        /// </summary>
        private const string Description =
            "Exports a csv report that lists all of the non-interior entities.";

        /// <summary>
        /// The strings that make up the headers in the csv file.
        /// </summary>
        private string[] headers =
        {
            "Entity Name",
            "Section",
            "Parent Area",
            "Grandparent Area",
            "X",
            "Y",
            "Z",
            "Archetype Name",
            "Has Lod Parent",
            "Is Low Priority",
            "Priority In Max",
            "Priority In Override",
            "Has Scaling",
            "Lod Distance",
            "Lod Level",
            "Lod Siblings Count",
            "Light Instance Count"
        };

        /// <summary>
        /// The private field used for the <see cref="GenerationTask"/> property.
        /// </summary>
        private ITask _generationTask;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="InvalidEntityReport"/> class.
        /// </summary>
        public MapEntitiesReport()
            : base(Name, Description)
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the task that represents the work needing to be done to generate this report.
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (this._generationTask == null)
                {
                    CompositeTask task = new CompositeTask("Generate Report");
                    task.AddSubTask(new ActionTask("Loading data", this.EnsureDataLoaded));
                    task.AddSubTask(new ActionTask("Generating report", this.GenerateReport));
                    this._generationTask = task;
                }

                return this._generationTask;
            }
        }

        /// <summary>
        /// Gets a iterator around the stats that are required to generate this report. Always
        /// returns null.
        /// </summary>
        public IEnumerable<StreamableStat> RequiredStats
        {
            get { return null; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Ensures that the data needed for this report is loaded.
        /// </summary>
        /// <param name="context">
        /// The task context for this action.
        /// </param>
        /// <param name="progress">
        /// The object used to notify a listener of the progress.
        /// </param>
        private void EnsureDataLoaded(ITaskContext context, IProgress<TaskProgress> progress)
        {
            DynamicLevelReportContext reportContext = context as DynamicLevelReportContext;
            Debug.Assert(reportContext != null, "Unable to load date for report");
            if (reportContext == null)
            {
                return;
            }

            IMapHierarchy hierarchy = reportContext.Level.MapHierarchy;
            Debug.Assert(hierarchy != null, "Unable to load date for report");
            if (hierarchy == null || hierarchy.AllSections == null)
            {
                return;
            }

            List<IMapSection> mapSections = hierarchy.AllSections.ToList();
            float increment = 1.0f / mapSections.Count;
            string fmt = "Loading {0}";
            int index = 1;
            foreach (IMapSection section in mapSections)
            {
                Debug.WriteLine("{0} of {1}", index++, mapSections.Count);
                context.Token.ThrowIfCancellationRequested();
                string message = String.Format(fmt, section.Name);
                progress.Report(new TaskProgress(increment, true, message));

                section.LoadStats(new List<StreamableStat>() { StreamableMapSectionStat.Archetypes, StreamableMapSectionStat.Entities });
            }
        }

        /// <summary>
        /// Generates the report and saves the file associated with it.
        /// </summary>
        /// <param name="context">
        /// The task context for this action.
        /// </param>
        /// <param name="progress">
        /// The object used to notify a listener of the progress.
        /// </param>
        private void GenerateReport(ITaskContext context, IProgress<TaskProgress> progress)
        {
            DynamicLevelReportContext reportContext = context as DynamicLevelReportContext;
            Debug.Assert(reportContext != null, "Unable to generate report");
            if (reportContext == null)
            {
                return;
            }

            IMapHierarchy hierarchy = reportContext.Level.MapHierarchy;
            Debug.Assert(hierarchy != null, "Unable to generate report");
            if (hierarchy == null || hierarchy.AllSections == null)
            {
                return;
            }

            Dictionary<IMapArchetype, List<IEntity>> validEntities = new Dictionary<IMapArchetype, List<IEntity>>();

            int mapSectionCount = 0;
            int entityCount = 0;
            List<IInteriorArchetype> interiors = new List<IInteriorArchetype>();

            try
            {
                List<IMapSection> mapSections = hierarchy.AllSections.ToList();
                float increment = 1.0f / mapSections.Count;
                string fmt = "Processing {0}";
                foreach (IMapSection section in mapSections)
                {
                    mapSectionCount++;
                    context.Token.ThrowIfCancellationRequested();
                    string message = String.Format(fmt, section.Name);
                    progress.Report(new TaskProgress(increment, true, message));
                    if (section == null || section.Archetypes == null)
                    {
                        continue;
                    }

                    foreach (IMapArchetype archetype in section.Archetypes)
                    {
                        if (archetype == null)
                        {
                            continue;
                        }

                        if (archetype.Entities == null)
                        {
                            continue;
                        }

                        foreach (IEntity entity in archetype.Entities)
                        {
                            if (entity == null)
                            {
                                continue;
                            }

                            entityCount++;
                            IMapSection entitySection = entity.Parent as IMapSection;
                            if (entitySection == null)
                            {
                                IRoom room = entity.Parent as IRoom;
                                if (room != null)
                                {
                                    IInteriorArchetype interior = room.ParentArchetype;
                                    if (interior != null && !interiors.Contains(interior))
                                    {
                                        interiors.Add(interior);
                                    }
                                }

                                continue;
                            }
                            
                            List<IEntity> entities = null;
                            if (!validEntities.TryGetValue(archetype, out entities))
                            {
                                entities = new List<IEntity>();
                                validEntities.Add(archetype, entities);
                            }

                            entities.Add(entity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.Write("Error occured during data anlysis: " + ex.StackTrace);
            }

            Debug.WriteLine("Sections {0}, Entities: {1}, Interiors: {2}", mapSectionCount, entityCount, interiors.Count);
            this.WriteCsvFile(validEntities, reportContext.GameView);
        }

        /// <summary>
        /// Wtites out the CSV file using the data gathered at the start of the
        /// <see cref="GenerateReport"/> method.
        /// </summary>
        /// <param name="validEntities">
        /// The list containing the valid entities to output to the csv file.
        /// </param>
        private void WriteCsvFile(Dictionary<IMapArchetype, List<IEntity>> validEntities, ConfigGameView gv)
        {
            using (StreamWriter writer = new StreamWriter(this.Filename))
            {
                writer.WriteLine(String.Join(",", headers));
                WriteCsvData(writer, validEntities, gv);
            }
        }

        /// <summary>
        /// Writes the data out to the specified stream.
        /// </summary>
        /// <param name="writer">
        /// The stream to write the comma separated data into.
        /// </param>
        /// <param name="validEntities">
        /// The list containing the valid entities to output to the csv file.
        /// </param>
        private void WriteCsvData(StreamWriter writer, Dictionary<IMapArchetype, List<IEntity>> validEntities, ConfigGameView gv)
        {
            //ISceneOverrideManager sceneOverride = SceneOverrideManagerFactory.CreateXmlSceneOverrideManager(Path.Combine(gv.AssetsDir, "maps", "scene_overrides"), false);
            //foreach (KeyValuePair<IMapArchetype, List<IEntity>> archetype in validEntities)
            //{
            //    if (archetype.Key == null || archetype.Value == null)
            //    {
            //        continue;
            //    }

            //    foreach (IEntity entity in archetype.Value)
            //    {
            //        IMapSection section = entity.Parent as IMapSection;
            //        IMapArea parentArea = (section != null ? section.ParentArea : null);
            //        IMapArea grandParentArea = (parentArea != null ? parentArea.ParentArea : null);

            //        string mapContentName = section.Name;
            //        string containerAttributeGuid = section.ContainerAttributeGuid;
            //        string nodeAttributeGuid = entity.AttributeGuid.ToString();
            //        string archetypeName = entity.Name.ToLower();

            //        RSG.Model.Map.SceneOverride.DCCEntityKey key = new DCCEntityKey(mapContentName, containerAttributeGuid, nodeAttributeGuid, archetypeName);

            //        string csvRecord = String.Format("{0},{1},{2},{3},", entity.Name, section != null ? section.Name : "n/a", parentArea != null ? parentArea.Name : "n/a", grandParentArea != null ? grandParentArea.Name : "n/a");
            //        csvRecord += String.Format("{0},{1},{2},", entity.Position.X, entity.Position.Y, entity.Position.Z);
            //        csvRecord += String.Format("{0},", archetype.Key.Name);

            //        bool hasLodParent = entity.LodParent != null;
            //        bool lowPriority = entity.IsLowPriority;
            //        int priorityInMax = entity.PriorityLevel;
            //        bool scaled = entity.HasScaling;
            //        int priorityInOverride = entity.PriorityLevel;
            //        if (sceneOverride.HasAttributeOverridesForObject(key))
            //        {
            //            priorityInOverride = sceneOverride.GetPriorityAttributeValue(key);
            //        }

            //        int siblingCount = 0;
            //        if (entity.LodParent != null && entity.LodParent.LodChildren != null)
            //        {
            //            siblingCount = Math.Max(0, entity.LodParent.LodChildren.Count - 1);
            //        }

            //        csvRecord += String.Format("{0},{1},{2},{3},{4},", hasLodParent, lowPriority, priorityInMax, priorityInOverride, scaled);
            //        csvRecord += String.Format("{0},{1},{2},", entity.LodDistance, entity.LodLevel.ToString(), siblingCount);
            //        csvRecord += String.Format("{0}", entity.RageLightInstanceCount.ToString());
            //        writer.WriteLine(csvRecord);
            //    }
            //}
        }
        #endregion Methods
    }
}
