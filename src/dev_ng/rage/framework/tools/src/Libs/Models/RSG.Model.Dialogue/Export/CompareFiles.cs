﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace RSG.Model.Dialogue.Export
{
    /// <summary>
    /// Compare two files. Displays the entries from file1 that don't appear in file2.
    /// </summary>
    public class CompareFiles
    {
        #region Public events

        public event KeyExtractorEventHandler KeyNotFound;

        #endregion

        #region Public methods

        /// <summary>
        /// Compare two files. Displays the entries from file1 that don't appear in file2.
        /// </summary>
        /// <param name="file1">File one.</param>
        /// <param name="file2">File two.</param>
        public void Compare(string file1, string file2)
        {
            string[] file1Entries = GetValues(file1);
            string[] file2Entries = GetValues(file2);

            List<string> used = new List<string>(file1Entries.Intersect(file2Entries));

            foreach (string s in file1Entries)
            {
                if (!used.Contains(s))
                {
                    if (KeyNotFound != null)
                    {
                        KeyNotFound(this, new KeyExtractorEventArgs(s));
                    }
                }
            }
        }

        #endregion

        #region Private helper methods

        /// <summary>
        /// Get individual lines from a file and create a string array from them.
        /// </summary>
        /// <param name="filename">File name.</param>
        /// <returns>The contents of the file as an array of strings.</returns>
        private string[] GetValues(string filename)
        {
            List<string> entries = new List<string>();
            using (Stream s = File.Open(filename, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                using (TextReader reader = new StreamReader(s))
                {
                    string line = String.Empty;
                    while ((line = reader.ReadLine()) != null)
                    {
                        if (String.IsNullOrEmpty(line))
                        {
                            continue;
                        }
                        entries.Add(line);
                    }
                }
            }

            return entries.ToArray();
        }

        #endregion
    }
}
