﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Collections;

namespace RSG.Model.Common.Map
{
    /// <summary>
    /// 
    /// </summary>
    public interface IHasEntityChildren : IAsset
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        ObservableCollection<IEntity> ChildEntities { get; }
        #endregion // Properties
    } // IHasEntityChildren
}
