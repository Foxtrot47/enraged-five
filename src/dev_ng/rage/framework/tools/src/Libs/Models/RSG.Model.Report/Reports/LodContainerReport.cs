﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.ConfigParser;
using RSG.Model.Common;
using RSG.Model.Map;
using RSG.Model.Map.Statistics;
using System.Web.UI;

namespace RSG.Model.Report.Reports
{
    /// <summary>
    /// 
    /// </summary>
    public class LodHierarchyAcrossSections
    {
        #region Classes
        /// <summary>
        /// 
        /// </summary>
        public class LodBranch
        {
            #region Properties
            /// <summary>
            /// 
            /// </summary>
            public MapInstance Root
            {
                get;
                set;
            }

            /// <summary>
            /// 
            /// </summary>
            public List<MapInstance> Children
            {
                get;
                set;
            }
            #endregion

            #region Constructors
            /// <summary>
            /// 
            /// </summary>
            public LodBranch(MapInstance root, List<MapInstance> children)
            {
                Children = children;
                Root = root;

            }
            #endregion
        }
        #endregion
        
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public MapSection LodSection
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public List<MapSection> ContainerSections
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public List<LodBranch> SLOD3Branches
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public List<LodBranch>SLOD2Branches
        {
            get;
            set;
        }
        #endregion

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="slod3"></param>
        public LodHierarchyAcrossSections()
        {
            SLOD3Branches = new List<LodBranch>();
            SLOD2Branches = new List<LodBranch>();
        }
        #endregion

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="children"></param>
        public void AddBranch(MapInstance root, List<MapInstance> children)
        {
            if (root.LODLevel == SceneXml.ObjectDef.LodLevel.SLOD3)
            {
                SLOD3Branches.Add(new LodBranch(root, children));
            }
            else if (root.LODLevel == SceneXml.ObjectDef.LodLevel.SLOD2)
            {
                SLOD2Branches.Add(new LodBranch(root, children));
            }
        }
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    public interface ILodHierarchyTest
    {
    }

    /// <summary>
    /// 
    /// </summary>
    public interface ISingleLodHierarchyTest : ILodHierarchyTest
    {
        void TestHierarchy(LodHierarchyAcrossSections hierarchy, ref List<ILodHierarchyFailure> failures);
    }

    /// <summary>
    /// 
    /// </summary>
    public interface IMultipleLodHierarchyTest : ILodHierarchyTest
    {
        void TestHierarchy(List<LodHierarchyAcrossSections> hierarchies, ref List<ILodHierarchyFailure> failures);
    }

    /// <summary>
    /// 
    /// </summary>
    public interface ILodHierarchyFailure
    {
        string ErrorMessage { get; }

        MapSection Section { get; }

        MapInstance Instance { get; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class LodContainerReport : Report, IDynamicMapReport, IReportFileProvider
    {
        #region Constants
        private const String NAME = "Lod Container Validation Report";
        private const String DESCRIPTION = "Exports a html report that notes all lod errors between the lod levels SLOD2 and SLOD1.";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Determines whether the map data should be loaded
        /// </summary>
        public bool LoadMapData
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Determines whether the car generators should be loaded
        /// 
        /// </summary>
        public bool LoadCarGenarators
        {
            get { return false; }
        }

        /// <summary>
        /// Filter used for the save dialog
        /// </summary>
        public string Filter
        {
            get
            {
                return "html|*.html";
            }
        }

        /// <summary>
        /// Extensions that this report support
        /// </summary>
        public string SupportedExtension
        {
            get
            {
                return ".html";
            }
        }

        /// <summary>
        /// Filename specifying where a report should be output
        /// </summary>
        public string Filename
        {
            get;
            set;
        }
        #endregion // Properties

        #region Construtcor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public LodContainerReport()
            : base(NAME, DESCRIPTION)
        {
            
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Generate the report dynamically.
        /// </summary>
        /// <param name="level"></param>
        /// <param name="gv"></param>
        public void Generate(ILevel level, ConfigGameView gv, IPlatformCollection platforms)
        {
            ExportReport(Filename, level, gv);
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="objectFilename"></param>
        /// <param name="level"></param>
        /// <param name="gv"></param>
        public static void ExportReport(String filename, ILevel level, ConfigGameView gv)
        {
            RSG.Model.Map.Util.Creation.InitialiseLevelStatistics(level, gv);

            List<LodHierarchyAcrossSections> hierarchies = GetHierarchies(level);
            List<ISingleLodHierarchyTest> singleTests = GetSingleTests();
            List<IMultipleLodHierarchyTest> mulitpleTests = GetMultipleTests();
            List<ILodHierarchyFailure> failures = new List<ILodHierarchyFailure>();
            foreach (LodHierarchyAcrossSections hierarchy in hierarchies)
            {
                foreach (ISingleLodHierarchyTest singleTest in singleTests)
                {
                    singleTest.TestHierarchy(hierarchy, ref failures);
                }
            }
            foreach (IMultipleLodHierarchyTest mulitpleTest in mulitpleTests)
            {
                mulitpleTest.TestHierarchy(hierarchies, ref failures);
            }

            CreateReport(filename, failures);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private static List<ISingleLodHierarchyTest> GetSingleTests()
        {
            return new List<ISingleLodHierarchyTest>()
            {
                new LodHierarchyDistanceTest()
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private static List<IMultipleLodHierarchyTest> GetMultipleTests()
        {
            return new List<IMultipleLodHierarchyTest>()
            {
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        private static List<LodHierarchyAcrossSections> GetHierarchies(ILevel level)
        {
            List<LodHierarchyAcrossSections> hierarchies = new List<LodHierarchyAcrossSections>();

            List<MapSection> lodSections = new List<MapSection>();
            Dictionary<string, MapSection> allContainerSections = new Dictionary<string, MapSection>();
            foreach (MapSection section in RSG.Model.Map.Util.Enumerations.GetAllMapSections(level, null, true))
            {
                if (!section.ExportInstances || section.AssetChildren == null)
                    continue;

                bool lodContainer = false;
                foreach (MapInstance asset in section.AssetChildren.OfType<MapInstance>().Where(i => i.LODLevel == SceneXml.ObjectDef.LodLevel.SLOD2))
                {
                    lodSections.Add(section);
                    lodContainer = true;
                    break;
                }
                if (!lodContainer)
                    allContainerSections.Add(section.Name.ToLower(), section);
            }

            foreach (MapSection lodSection in lodSections)
            {
                LodHierarchyAcrossSections hierarchy = new LodHierarchyAcrossSections();
                hierarchy.LodSection = lodSection;

                foreach (MapInstance asset in lodSection.AssetChildren.OfType<MapInstance>().Where(i => i.LODLevel == SceneXml.ObjectDef.LodLevel.SLOD3))
                {
                    List<MapInstance> branchChildren = new List<MapInstance>(asset.LodChildren);
                    hierarchy.AddBranch(asset, branchChildren);
                }


                List<MapSection> containerSections = new List<MapSection>();
                foreach (MapInstance asset in lodSection.AssetChildren.OfType<MapInstance>().Where(i => i.LODLevel == SceneXml.ObjectDef.LodLevel.SLOD2))
                {
                    List<MapInstance> branchChildren = new List<MapInstance>();
                    if (!string.IsNullOrWhiteSpace(asset.ContainerLods))
                    {
                        string[] containers = asset.ContainerLods.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string container in containers)
                        {
                            MapSection foundContainer = null;
                            if (allContainerSections.TryGetValue(container.ToLower(), out foundContainer))
                            {
                                containerSections.Add(foundContainer);
                                foreach (MapInstance slod1 in foundContainer.AssetChildren.OfType<MapInstance>().Where(i => i.LODLevel == SceneXml.ObjectDef.LodLevel.SLOD1))
                                {
                                    branchChildren.Add(slod1);
                                }
                            }
                        }
                        hierarchy.AddBranch(asset, branchChildren);
                    }
                }
                hierarchy.ContainerSections = containerSections;
                hierarchies.Add(hierarchy);
            }

            return hierarchies;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="errors"></param>
        private static void CreateReport(string filename, List<ILodHierarchyFailure> failures)
        {
            // Sort the errors by map section and in the order of worst first.
            Dictionary<MapSection, List<ILodHierarchyFailure>> sortedSections = new Dictionary<MapSection, List<ILodHierarchyFailure>>();
            foreach (ILodHierarchyFailure failure in failures)
            {
                if (!sortedSections.ContainsKey(failure.Section))
                    sortedSections.Add(failure.Section, new List<ILodHierarchyFailure>());
                sortedSections[failure.Section].Add(failure);
            }
       
            using (StreamWriter reportFile = new StreamWriter(filename))
            {
                HtmlTextWriter writer = new HtmlTextWriter(reportFile);

                writer.RenderBeginTag(HtmlTextWriterTag.H1);
                writer.Write("Lod Distance Hierarchy Check");
                writer.RenderEndTag();

                foreach (KeyValuePair<MapSection, List<ILodHierarchyFailure>> section in sortedSections)
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.H2);
                    writer.Write(section.Key.Name);
                    writer.RenderEndTag();

                    writer.RenderBeginTag(HtmlTextWriterTag.Table);
                    writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                    writer.RenderBeginTag(HtmlTextWriterTag.Th);
                    writer.Write("Lod Distance Warnings");
                    writer.RenderEndTag();
                    writer.RenderEndTag();

                    foreach (ILodHierarchyFailure failure in section.Value)
                    {
                        writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                        writer.RenderBeginTag(HtmlTextWriterTag.Td);

                        writer.Write(failure.ErrorMessage);
                        
                        writer.RenderEndTag();
                        writer.RenderEndTag();
                    }

                    writer.RenderEndTag();
                }

                writer.Flush();
                writer.Dispose();
                writer.Close();
            }
        }
        #endregion // Private Methods
    } // LodContainerReport

    /// <summary>
    /// 
    /// </summary>
    internal class LodHierarchyDistanceTest : ISingleLodHierarchyTest
    {        
        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="hierarchy"></param>
        public void TestHierarchy(LodHierarchyAcrossSections hierarchy, ref List<ILodHierarchyFailure> failures)
        {
            // Tests to make sure that the lod distance is greater on the SLOD2
            // object than any of the parentied SLOD1 objects.
            foreach (var branch in hierarchy.SLOD2Branches)
            {
                float slod2Distance = branch.Root.LodDistance;
                foreach (var child in branch.Children)
                {
                    if (slod2Distance < child.LodDistance)
                    {
                        failures.Add(new LodHierarchyFailure(hierarchy.LodSection, branch.Root, string.Format("Lod distance for {0}({1}m) is smaller than its child {2}({3}m)", branch.Root.Name, slod2Distance, child.Name, child.LodDistance)));
                        failures.Add(new LodHierarchyFailure(child.Container as MapSection, child, string.Format("Lod distance for {0}({1}m) is greater than its parent {2}({3}m)", child.Name, child.LodDistance, branch.Root.Name, slod2Distance)));
                    }
                }
            }
        }
        #endregion
    } // LodHierarchyDistanceTest

    /// <summary>
    /// 
    /// </summary>
    public class LodHierarchyFailure : ILodHierarchyFailure
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public string ErrorMessage
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public MapSection Section
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public MapInstance Instance
        {
            get;
            set;
        }
        #endregion

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="instance"></param>
        /// <param name="msg"></param>
        public LodHierarchyFailure(MapSection section, MapInstance instance, string msg)
        {
            this.Section = section;
            this.Instance = instance;
            this.ErrorMessage = msg;
        }
        #endregion
    } // LodHierarchyFailure
} // RSG.Model.Report.Reports
