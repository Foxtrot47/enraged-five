﻿//---------------------------------------------------------------------------------------------
// <copyright file="TrackListings.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.Model
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Text;

    /// <summary>
    /// Contains a number of track objects that have been loaded from text files from new york
    /// that can be used to get recording data about a dialogue line.
    /// </summary>
    public class TrackListings
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Tracks"/> property.
        /// </summary>
        private List<Track> _tracks;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="TrackListings"/> class.
        /// </summary>
        public TrackListings()
        {
            this._tracks = new List<Track>();
        }
        #endregion Constructors

        #region Propertes
        /// <summary>
        /// Gets the list of tracks.
        /// </summary>
        public ReadOnlyCollection<Track> Tracks
        {
            get { return this._tracks.AsReadOnly(); }
        }
        #endregion Propertes

        #region Methods
        /// <summary>
        /// Loads the tracks into this objects track list from the specified timing file.
        /// </summary>
        /// <param name="fullPath">
        /// The full path to the file containing the recording data.
        /// </param>
        public void LoadRecordingFile(string fullPath)
        {
            List<string> trackData = new List<string>();
            using (StreamReader reader = new System.IO.StreamReader(fullPath))
            {
                StringBuilder data = null;
                bool inTracks = false;
                string line = null;
                while ((line = reader.ReadLine()) != null)
                {
                    if (String.IsNullOrEmpty(line))
                    {
                        continue;
                    }

                    if (String.Equals(line, "M A R K E R S  L I S T I N G"))
                    {
                        break;
                    }

                    if (String.Equals(line, "T R A C K  L I S T I N G"))
                    {
                        inTracks = true;
                        continue;
                    }

                    if (!inTracks)
                    {
                        continue;
                    }

                    if (line.StartsWith("TRACK NAME:"))
                    {
                        if (data != null)
                        {
                            trackData.Add(data.ToString());
                            data.Clear();
                        }
                    }

                    if (data == null)
                    {
                        data = new StringBuilder();
                    }

                    data.AppendLine(line);
                }

                if (data != null)
                {
                    trackData.Add(data.ToString());
                }
            }

            foreach (string track in trackData)
            {
                this._tracks.Add(new Track(track));
            }
        }
        #endregion Methods
    } // RSG.Text.Model.TrackListings {Class}
} // RSG.Text.Model {Namespace}
