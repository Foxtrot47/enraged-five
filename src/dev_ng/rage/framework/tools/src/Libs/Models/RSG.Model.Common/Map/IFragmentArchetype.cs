﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common.Map
{
    /// <summary>
    /// 
    /// </summary>
    public interface IFragmentArchetype : ISimpleMapArchetype
    {
        #region Properties
        /// <summary>
        /// Number of fragments this is made up of.
        /// </summary>
        uint FragmentCount { get; }

        /// <summary>
        /// Whether this fragment is a piece of cloth.
        /// </summary>
        bool IsCloth { get; }

        /// <summary>
        /// List of drawable archetypes that this fragment is compose of.
        /// </summary>
#warning TODO: Child components of fragments
        //IEnumerable<IDrawableArchetype> Drawables { get; }
        #endregion // Properties
    } // IFragmentArchetype
}
