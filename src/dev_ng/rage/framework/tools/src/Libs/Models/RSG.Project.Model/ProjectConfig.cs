﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProjectConfig.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.Model
{
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Xml;
    using RSG.Editor;

    /// <summary>
    /// Represents a property group defined with-in a project file.
    /// </summary>
    public class ProjectConfig
    {
        #region Fields
        /// <summary>
        /// The private cache of properties defined for this config.
        /// </summary>
        private Dictionary<string, string> _properties;

        /// <summary>
        /// The private field used for the <see cref="Condition"/> property.
        /// </summary>
        private string _condition;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ProjectConfig"/> class.
        /// </summary>
        public ProjectConfig()
        {
            this._properties = new Dictionary<string, string>();
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ProjectConfig"/> class using the
        /// specified xml reader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The xml reader that contains the data used to initialise this instance.
        /// </param>
        internal ProjectConfig(XmlReader reader)
        {
            if (reader == null)
            {
                throw new SmartArgumentNullException(() => reader);
            }

            this.Deserialise(reader);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the condition that has been set on this config group.
        /// </summary>
        public string Condition
        {
            get { return this._condition; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Retrieves the value of the property with the specified name.
        /// </summary>
        /// <param name="name">
        /// The name of the property to retrieve the value of.
        /// </param>
        /// <param name="defaultValue">
        /// The value to return if the property doesn't exist.
        /// </param>
        /// <returns>
        /// The value of the property with the specified name if it exists; otherwise, the
        /// specified fall-back value.
        /// </returns>
        public string GetProperty(string name, string defaultValue)
        {
            if (this._properties == null)
            {
                return defaultValue;
            }

            string value = null;
            if (this._properties.TryGetValue(name, out value))
            {
                return value;
            }

            return defaultValue;
        }

        /// <summary>
        /// Retrieves the value of the property with the specified name.
        /// </summary>
        /// <param name="name">
        /// The name of the property to retrieve the value of.
        /// </param>
        /// <returns>
        /// The value of the property with the specified name if it exists; otherwise, null.
        /// </returns>
        public string GetProperty(string name)
        {
            return this.GetProperty(name, null);
        }

        /// <summary>
        /// Sets the value of the specified property in this property group.
        /// </summary>
        /// <param name="name">
        /// The name of the property whose value is being set.
        /// </param>
        /// <param name="value">
        /// The value to set the property to.
        /// </param>
        public void SetProperty(string name, string value)
        {
            if (this._properties == null)
            {
                this._properties = new Dictionary<string, string>();
            }

            this._properties[name] = value;
        }

        /// <summary>
        /// Serialises the data contained within this instance onto the specified xml writer.
        /// </summary>
        /// <param name="writer">
        /// The xml writer this instance will be serialised to.
        /// </param>
        internal void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteAttributeString("Condition", this._condition);
            if (this._properties == null)
            {
                return;
            }

            foreach (KeyValuePair<string, string> property in this._properties)
            {
                writer.WriteElementString(property.Key, property.Value);
            }
        }

        /// <summary>
        /// Initialises this instance using the data contained within the specified xml reader.
        /// </summary>
        /// <param name="reader">
        /// The xml reader that contains the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            this._condition = reader.GetAttribute("Condition");
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            Dictionary<string, string> properties = new Dictionary<string, string>();
            while (reader.MoveToContent() == XmlNodeType.Element)
            {
                if (!reader.IsEmptyElement)
                {
                    string key = reader.Name;
                    string value = reader.ReadElementContentAsString();
                    if (properties.ContainsKey(key))
                    {
                        Debug.Assert(false, "Property group already contains property key");
                    }

                    properties[key] = value;
                }
                else
                {
                    reader.Skip();
                }
            }

            this._properties = new Dictionary<string, string>(properties);
            if (reader.NodeType == XmlNodeType.EndElement)
            {
                reader.ReadEndElement();
            }
        }
        #endregion Methods
    } // RSG.Project.Model.ProjectConfig {Class}
} // RSG.Project.Model {Namespace}
