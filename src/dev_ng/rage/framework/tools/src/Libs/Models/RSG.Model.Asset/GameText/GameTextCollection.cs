﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using RSG.Base.Configuration;
using RSG.Model.Common.GameText;

namespace RSG.Model.Asset.GameText
{
    /// <summary>
    /// 
    /// </summary>
    public class GameTextCollection : IGameTextCollection
    {
        /// <summary>
        /// Name of the section for strings which don't specify a custom section.
        /// </summary>
        private const String _globalSectionName = "global";

        /// <summary>
        /// Postfix for DLC game text names.
        /// </summary>
        private const String _dlcFilenamePostfix = "DLCNames";

        /// <summary>
        /// Branch that this collection is for.
        /// </summary>
        private IBranch _branch;

        /// <summary>
        /// Mapping of languages to lists of game text sections.
        /// TODO: We could optimise this slightly by using a second dictionary instead of a list.
        /// </summary>
        private IDictionary<Language, IList<IGameTextSection>> _sectionsPerLanguage = new Dictionary<Language, IList<IGameTextSection>>();

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="branch"></param>
        public GameTextCollection(IBranch branch)
        {
            _branch = branch;
        }

        /// <summary>
        /// Retrieves the text associated with a particular text label for the specified platform and optionally
        /// within a particular section.
        /// </summary>
        /// <param name="label"></param>
        /// <param name="language"></param>
        /// <param name="platform"></param>
        /// <param name="sectionName"></param>
        /// <returns></returns>
        public String GetText(String label, Language language, RSG.Platform.Platform platform, String sectionName = null)
        {
            // Attempt to get the list of game text sections for the specified language.
            IList<IGameTextSection> sectionsForLanguage;
            if (!_sectionsPerLanguage.TryGetValue(language, out sectionsForLanguage))
            {
                throw new KeyNotFoundException("The requested language hasn't been loaded.  Make sure you call " +
                    "LoadLanguage() before attempting to retrieve a piece of text.");
            }

            // Set the section name to the global one if none was specified.
            if (sectionName == null)
            {
                sectionName = _globalSectionName;
            }

            // Retrieve the section we are after.
            IGameTextSection section = sectionsForLanguage.FirstOrDefault(item => item.Name == sectionName);
            if (section == null)
            {
                throw new KeyNotFoundException(String.Format("A section with name '{0}' doesn't exist.", sectionName));
            }

            // Query the section for the label/platform combination and return it's result.
            return section.GetText(label, platform);
        }

        /// <summary>
        /// Attempts to retrieve the text for a particular label/language/platform combination.
        /// </summary>
        /// <param name="label"></param>
        /// <param name="language"></param>
        /// <param name="platform"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        public bool TryGetText(String label, Language language, RSG.Platform.Platform platform, out String text)
        {
            return TryGetText(label, language, platform, null, out text);
        }

        /// <summary>
        /// Attempts to retrieve the text for a particular label/language/platform/section combination.
        /// </summary>
        /// <param name="label"></param>
        /// <param name="language"></param>
        /// <param name="platform"></param>
        /// <param name="sectionName"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        public bool TryGetText(String label, Language language, RSG.Platform.Platform platform, String sectionName, out String text)
        {
            try
            {
                text = GetText(label, language, platform, sectionName);
                return true;
            }
            catch (KeyNotFoundException)
            {
                text = null;
                return false;
            }
        }

        /// <summary>
        /// Loads a particular language.
        /// </summary>
        /// <param name="language"></param>
        /// <returns></returns>
        public bool LoadLanguage(Language language)
        {
            // Check whether the language has already been loaded.
            if (_sectionsPerLanguage.ContainsKey(language))
            {
                return true;
            }

            IList<IGameTextSection> sections = new List<IGameTextSection>();
            sections.Add(new GameTextSection(_globalSectionName));

            // Iterate over all the game text files for this branch/language.
            foreach (String filename in GetGameTextFilenames(language))
            {
                if (File.Exists(filename))
                {
                    ParseFile(filename, sections);
                }
            }

            // Add this language to our lookup.
            _sectionsPerLanguage.Add(language, sections);
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="language"></param>
        /// <returns></returns>
        private IEnumerable<String> GetGameTextFilenames(Language language)
        {
            IProject project = _branch.Project;
            if (project.IsDLC)
            {
                // TODO: Confirm this works for all languages.
                yield return Path.Combine(_branch.Text, String.Format("{0}{1}.txt", language, _dlcFilenamePostfix));
            }
            else
            {
                // TODO: Confirm this works for all languages.
                String gameTextPathForLanguage = Path.Combine(_branch.Text, language.ToString());
                if (Directory.Exists(gameTextPathForLanguage))
                {
                    foreach (String filename in Directory.EnumerateFiles(gameTextPathForLanguage, "*.txt"))
                    {
                        yield return filename;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="sections"></param>
        private void ParseFile(String filename, IList<IGameTextSection> sections)
        {
            // Flag to indicate that we are parsing sections.
            bool parsingSections = false;

            // Key(s) for the current text entry.
            String label = null;
            String sectionName = null;
            RSG.Platform.Platform? platform = null;
            String currentText = null;

            // Regex for parsing the labels.
            Regex labelRegex = new Regex(@"^\[(?'label'\w+)(:(?'section'\w+))?(!(?'platform'\w+))?\]");

            // Read through the file line by line extracting all the required information.
            using (StreamReader reader = new StreamReader(filename))
            {
                String line;
                while ((line = reader.ReadLine()) != null)
                {
                    if (line.StartsWith("{") || String.IsNullOrWhiteSpace(line))
                    {
                        continue;
                    }
                    else if (line == "start")
                    {
                        parsingSections = true;
                    }
                    else if (parsingSections)
                    {
                        if (line == "end")
                        {
                            parsingSections = false;
                        }
                        else
                        {
                            // Create a new section (if it doesn't already exist).
                            String name = line.ToLower();

                            IGameTextSection section = sections.FirstOrDefault(item => item.Name == name);
                            if (section == null)
                            {
                                section = new GameTextSection(name);
                                sections.Add(section);
                            }
                        }
                    }
                    else if (line.StartsWith("["))
                    {
                        // Finish off the previous entry.
                        if (label != null && !String.IsNullOrEmpty(currentText))
                        {
                            AddText(label, platform, sectionName, currentText, sections);
                        }

                        // Parse the label, extracting the optional section/platform.
                        Match firstMatch = labelRegex.Match(line);
                        if (firstMatch.Success)
                        {
                            Group labelGroup = firstMatch.Groups["label"];
                            Group sectionGroup = firstMatch.Groups["section"];
                            Group platformGroup = firstMatch.Groups["platform"];

                            label = labelGroup.Value;

                            if (sectionGroup.Success)
                            {
                                sectionName = sectionGroup.Value.ToLower();
                            }
                            else
                            {
                                sectionName = _globalSectionName;
                            }

                            if (platformGroup.Success)
                            {
                                //TODO: Data drive this.
                                switch (platformGroup.Value.ToLower())
                                {
                                    case "ps3":
                                        platform = RSG.Platform.Platform.PS3;
                                        break;
                                    case "360":
                                        platform = RSG.Platform.Platform.Xbox360;
                                        break;
                                    case "pc":
                                        platform = RSG.Platform.Platform.Win64;
                                        break;
                                    case "ps4":
                                        platform = RSG.Platform.Platform.PS4;
                                        break;
                                    case "xb1":
                                        platform = RSG.Platform.Platform.XboxOne;
                                        break;
                                    //case "else":
                                    default:
                                        platform = null;
                                        break;
                                }
                            }
                            else
                            {
                                platform = null;
                            }

                            currentText = "";
                        }
                        else
                        {
                            label = null;
                        }
                    }
                    else if (label != null)
                    {
                        currentText += line;
                    }
                }
            }

            // See if there is a left over label to finish off.
            if (label != null && !String.IsNullOrEmpty(currentText))
            {
                AddText(label, platform, sectionName, currentText, sections);
            }
        }

        private void AddText(String label, RSG.Platform.Platform? platform, String sectionName, String text, IList<IGameTextSection> sections)
        {
            // Parse the text
            IGameTextSection section = sections.FirstOrDefault(item => item.Name == sectionName);
            if (section == null)
            {
                section = new GameTextSection(sectionName);
                sections.Add(section);
            }

            section.AddText(label, platform, text);
        }
    } // GameTextCollection
}
