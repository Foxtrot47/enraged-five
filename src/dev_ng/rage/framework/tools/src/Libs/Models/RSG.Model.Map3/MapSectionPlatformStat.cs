﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common.Map;
using RSG.Platform;
using RSG.Model.Common;

namespace RSG.Model.Map3
{
    /// <summary>
    /// Map section platform stats.
    /// </summary>
    public class MapSectionPlatformStat : IMapSectionPlatformStat
    {
        #region Properties
        /// <summary>
        /// Platform for this stat
        /// </summary>
        public RSG.Platform.Platform Platform { get; set; }

        /// <summary>
        /// Physical sizes on a per file type basis.
        /// </summary>
        public IDictionary<FileType, IMemoryStat> FileTypeSizes { get; private set; }

        /// <summary>
        /// Total physical size of the sections.
        /// </summary>
        public long TotalPhysicalSize
        {
            get
            {
                return FileTypeSizes.Select(item => item.Value).Sum(item => item.PhysicalSize);
            }
        }

        /// <summary>
        /// Total virtual size of the sections.
        /// </summary>
        public long TotalVirtualSize
        {
            get
            {
                return FileTypeSizes.Select(item => item.Value).Sum(item => item.VirtualSize);
            }
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default Constructor
        /// </summary>
        public MapSectionPlatformStat()
        {
            FileTypeSizes = new Dictionary<FileType, IMemoryStat>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="platform"></param>
        public MapSectionPlatformStat(RSG.Platform.Platform platform)
            : this()
        {
            Platform = platform;
        }
        #endregion // Constructor(s)
    } // MapSectionPlatformStat
}
