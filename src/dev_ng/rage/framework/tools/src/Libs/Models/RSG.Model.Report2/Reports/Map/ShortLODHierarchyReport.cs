﻿namespace RSG.Model.Report2.Reports.Map
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using RSG.Base.ConfigParser;
    using RSG.Base.Tasks;
    using Base.Math;
    using RSG.Model.Common;
    using RSG.Model.Common.Map;
    using RSG.Model.Report;
    using RSG.ManagedRage;
    using RSG.Platform;
    using RSG.Model.Map.SceneOverride;

    /// <summary>
    /// A report that lists the entities with columns that can be used to filter to show
    /// invlaid entities based on a number of attributes.
    /// </summary>
    public class ShortLODHierarchyReport : CSVReport, IDynamicLevelReport
    {
        #region Fields
        /// <summary>
        /// The name of this report.
        /// </summary>
        private const string Name = "Short LOD Hierarchy";

        /// <summary>
        /// The description for this report.
        /// </summary>
        private const string Description =
            "Exports a csv report that lists all HD entities with 'short' LOD hierarchies.  " +
            "That is all HD entities that don't have a SLOD2 parent.";

        /// <summary>
        /// The private field used for the <see cref="GenerationTask"/> property.
        /// </summary>
        private ITask _generationTask;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="InvalidEntityReport"/> class.
        /// </summary>
        public ShortLODHierarchyReport()
            : base(Name, Description)
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the task that represents the work needing to be done to generate this report.
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (this._generationTask == null)
                {
                    CompositeTask task = new CompositeTask("Generate Report");
                    task.AddSubTask(new ActionTask("Loading data", this.EnsureDataLoaded));
                    task.AddSubTask(new ActionTask("Generating report", this.GenerateReport));
                    this._generationTask = task;
                }

                return this._generationTask;
            }
        }

        /// <summary>
        /// Gets a iterator around the stats that are required to generate this report. Always
        /// returns null.
        /// </summary>
        public IEnumerable<StreamableStat> RequiredStats
        {
            get { return null; }
        }
        #endregion Properties

        #region Private Classes
        /// <summary>
        /// This class represents an entity with a LOD chain that doesn't go as far a SLOD2.
        /// It's a compound of an IEntity and any additional data as required for the report.
        /// </summary>
        private class ShortLODEntity
        {
            internal ShortLODEntity(IEntity entity, LodLevel highestLODLevel)
            {
                entity_ = entity;

                ArchetypeName = entity.ReferencedArchetype != null ? entity.ReferencedArchetype.Name : String.Empty;
                HighestLODLevel = highestLODLevel;
            }

            internal String Name { get { return entity_.Name; } }
            internal String SectionName { get { return entity_.ContainingSection.Name; } }
            internal String ArchetypeName { get; private set; }
            internal Vector3f Position { get { return entity_.Position; } }
            internal LodLevel HighestLODLevel { get; private set; }

            private IEntity entity_;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Ensures that the data needed for this report is loaded.
        /// </summary>
        /// <param name="context">
        /// The task context for this action.
        /// </param>
        /// <param name="progress">
        /// The object used to notify a listener of the progress.
        /// </param>
        private void EnsureDataLoaded(ITaskContext context, IProgress<TaskProgress> progress)
        {
            DynamicLevelReportContext reportContext = context as DynamicLevelReportContext;
            Debug.Assert(reportContext != null, "Unable to load data for report");
            if (reportContext == null)
            {
                return;
            }

            IMapHierarchy hierarchy = reportContext.Level.MapHierarchy;
            Debug.Assert(hierarchy != null, "Unable to load data for report");
            if (hierarchy == null || hierarchy.AllSections == null)
            {
                return;
            }

            List<IMapSection> mapSections = hierarchy.AllSections.ToList();
            float increment = 1.0f / mapSections.Count;
            string fmt = "Loading {0}";
            int index = 1;
            foreach (IMapSection section in mapSections)
            {
                Debug.WriteLine("{0} of {1}", index++, mapSections.Count);
                context.Token.ThrowIfCancellationRequested();
                string message = String.Format(fmt, section.Name);
                progress.Report(new TaskProgress(increment, true, message));

                section.LoadStats(new List<StreamableStat>() { StreamableMapSectionStat.Archetypes, StreamableMapSectionStat.Entities });
            }
        }

        /// <summary>
        /// Generates the report and saves the file associated with it.
        /// </summary>
        /// <param name="context">
        /// The task context for this action.
        /// </param>
        /// <param name="progress">
        /// The object used to notify a listener of the progress.
        /// </param>
        private void GenerateReport(ITaskContext context, IProgress<TaskProgress> progress)
        {
            DynamicLevelReportContext reportContext = context as DynamicLevelReportContext;
            Debug.Assert(reportContext != null, "Unable to generate report");
            if (reportContext == null)
            {
                return;
            }

            IMapHierarchy hierarchy = reportContext.Level.MapHierarchy;
            Debug.Assert(hierarchy != null, "Unable to generate report");
            if (hierarchy == null || hierarchy.AllSections == null)
            {
                return;
            }

            List<ShortLODEntity> hdEntitiesWithShortLODHierarchy = new List<ShortLODEntity>();
            foreach (IMapSection section in hierarchy.AllSections.Where(sec => sec.ChildEntities != null))
            {
                foreach (IEntity hdEntity in section.ChildEntities.Where(en => en.LodLevel == LodLevel.Hd))
                {
                    LodLevel highestLodLevel = hdEntity.LodLevel;

                    IEntity currentEntity = hdEntity;
                    while (currentEntity.LodParent != null)
                    {
                        currentEntity = currentEntity.LodParent;
                        highestLodLevel = currentEntity.LodLevel;
                    }

                    if (highestLodLevel == LodLevel.Lod || highestLodLevel == LodLevel.SLod1 && !currentEntity.HasSLOD2Link)
                    {
                        hdEntitiesWithShortLODHierarchy.Add(new ShortLODEntity(hdEntity, highestLodLevel));
                    }
                }
            }

            this.WriteCsvFile(hdEntitiesWithShortLODHierarchy);
        }

        private void WriteCsvFile(IEnumerable<ShortLODEntity> shortLodEntities)
        {
            using (StreamWriter writer = new StreamWriter(this.Filename))
            {
                writer.WriteLine(
                    "Section Name," +
                    "Entity Name," +
                    "Archetype Name," +
                    "Highest LOD Level," + 
                    "X," + 
                    "Y," + 
                    "Z");

                WriteCsvData(writer, shortLodEntities);
            }
        }

        private void WriteCsvData(StreamWriter writer, IEnumerable<ShortLODEntity> shortLodEntities)
        {
            foreach (ShortLODEntity shortLodEntity in shortLodEntities)
            {
                writer.WriteLine("{0},{1},{2},{3},{4},{5},{6}",
                    shortLodEntity.SectionName,
                    shortLodEntity.Name,
                    shortLodEntity.ArchetypeName, 
                    shortLodEntity.HighestLODLevel,
                    shortLodEntity.Position.X,
                    shortLodEntity.Position.Y,
                    shortLodEntity.Position.Z);
            }
        }
        #endregion Methods
    }
}
