﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.ConfigParser;
using RSG.Model.Common.Map;

namespace RSG.Model.Common.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    public static class IMapSectionExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="gv"></param>
        /// <returns></returns>
        public static ContentNodeMap GetConfigMapNode(this IMapSection section, ConfigGameView gv)
        {
            return gv.Content.Root.FindAll(section.Name, "map").FirstOrDefault() as ContentNodeMap;
        }
    } // IMapSectionExtensions
}
