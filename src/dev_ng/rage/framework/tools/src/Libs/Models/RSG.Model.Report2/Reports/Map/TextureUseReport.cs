﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.UI;
using RSG.Base.Configuration;
using RSG.Base.Tasks;
using RSG.Model.Common;
using RSG.Model.Common.Map;
using RSG.Model.Report;

namespace RSG.Model.Report2.Reports.Map
{
    /// <summary>
    /// 
    /// </summary>
    public class TextureUseReport : HTMLReport, IDynamicLevelReport
    {
        #region Constants
        private const String c_name = "Texture Use Report";
        private const String c_description = "Generate and display a report of where textures are being used in the map.";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (m_task == null)
                {
                    CompositeTask task = new CompositeTask("Generate Report");
                    task.AddSubTask(new ActionTask("Loading data", (context, progress) => EnsureDataLoaded((DynamicLevelReportContext)context, progress)), 50);
                    task.AddSubTask(new ActionTask("Generating report", (context, progress) => GenerateReport((DynamicLevelReportContext)context, progress)));
                    m_task = task;
                }
                return m_task;
            }
        }
        private ITask m_task;

        /// <summary>
        /// List of stats that are required to generate this report.
        /// </summary>
        public IEnumerable<StreamableStat> RequiredStats
        {
            get { return null; }
        }

        /// <summary>
        /// The file that contains the list of textures to search for.
        /// </summary>
        public String TextureInputFile { get; set; }
        #endregion // Properties

        #region Private Classes
        /// <summary>
        /// Class to store information about a single occurrance of a texture.
        /// </summary>
        private class TextureOccurrence
        {
            public String TextureName { get; set; }
            public IMapSection Section { get; set; }
            public IMapArchetype Archetype { get; set; }
        } // TextureOccurrence
        #endregion // Private Classes

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public TextureUseReport()
            : base(c_name, c_description)
        {
        }
        #endregion // Constructor(s)

        #region Task Methods
        /// <summary>
        /// Task for making sure that all the data required by this report is loaded.
        /// NOTE: eventually this will be handled at a higher level.
        /// </summary>
        private void EnsureDataLoaded(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            IMapHierarchy hierarchy = context.Level.MapHierarchy;

            StreamableStat[] statsToLoad = new StreamableStat[] {
                StreamableMapSectionStat.Archetypes,
                StreamableMapSectionStat.Entities,
                StreamableMapArchetypeStat.Shaders };
            ITask dataLoadTask = hierarchy.CreateStatLoadTask(hierarchy.AllSections, statsToLoad);

            if (dataLoadTask != null)
            {
                (dataLoadTask as CompositeTask).ExecuteInParallel = true;
                dataLoadTask.Execute(context);
            }
        }

        /// <summary>
        /// Task for parsing the texture input file, searching for those textures in the level data and
        /// generating the resulting HTML report stream.
        /// </summary>
        private void GenerateReport(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            IList<String> textureNames = ParseInputFile(Path.GetFullPath(TextureInputFile));
            IList<TextureOccurrence> occurrences = SearchForTextures(context.Level.MapHierarchy, textureNames);
            Stream = WriteReport(occurrences, textureNames);
        }
        #endregion // Task Methods

        #region Private Methods
        /// <summary>
        /// Parses the file containing the list of textures to search for.
        /// </summary>
        private IList<String> ParseInputFile(String filename)
        {
            Debug.Assert(File.Exists(filename), String.Format("Texture use input file doesn't exist at {0}", filename));

            IList<String> textureNames = new List<String>();
            if (File.Exists(filename))
            {
                using (StreamReader reader = new StreamReader(filename)) 
                {
                    String line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        if (!line.StartsWith("#"))
                        {
                            textureNames.Add(Path.GetFileNameWithoutExtension(line));
                        }
                    }
                }
            }
            return textureNames;
        }

        /// <summary>
        /// Searches through the map hierarchy for any archetypes that contain any textures in the texture name list.
        /// </summary>
        private IList<TextureOccurrence> SearchForTextures(IMapHierarchy hierarchy, IList<String> textureNames)
        {
            IList<TextureOccurrence> occurrences = new List<TextureOccurrence>();

            // Iterate over all the sections.
            IEnumerable<IMapSection> sections = hierarchy.AllSections;
            sections.AsParallel().ForAll(section =>
            {
                foreach (IMapArchetype archetype in section.Archetypes)
                {
                    foreach (ITexture texture in archetype.Textures)
                    {
                        foreach (String textureName in textureNames.Where(item => String.Compare(item, texture.Name, true) == 0))
                        {
                            lock (occurrences)
                            {
                                occurrences.Add(new TextureOccurrence { TextureName = textureName, Section = section, Archetype = archetype });
                            }
                        }
                    }
                }
            });

            return occurrences;
        }

        /// <summary>
        /// Creates the HTML stream containing the report data.
        /// </summary>
        private Stream WriteReport(IList<TextureOccurrence> occurrences, IList<String> textureNames)
        {
            // Generate the stream for the report
            MemoryStream stream = new MemoryStream();
            StreamWriter streamWriter = new StreamWriter(stream);

            // Start writing out the report
            HtmlTextWriter writer = new HtmlTextWriter(streamWriter);
            writer.RenderBeginTag(HtmlTextWriterTag.Html);
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Head);
                writer.RenderEndTag();

                writer.RenderBeginTag(HtmlTextWriterTag.Body);
                {
                    WriteHeader(writer, "Texture Use Report");
                    OutputSummary(writer, occurrences, textureNames);

                    if (occurrences.Any())
                    {
                        OutputOccurrencesBySection(writer, occurrences);
                        OutputOccurrencesByTexture(writer, occurrences);
                    }
                }
                writer.RenderEndTag();

            }
            writer.RenderEndTag();
            writer.Flush();

            // Reset the stream before returning it
            stream.Position = 0;
            return stream;
        }

        /// <summary>
        /// Outputs a summary of the information we found
        /// </summary>
        /// <param name="writer"></param>
        private void OutputSummary(HtmlTextWriter writer, IList<TextureOccurrence> occurrences, IList<String> textureNames)
        {
            // Generate the summary text
            if (!occurrences.Any())
            {
                WriteParagraph(writer, "None of the textures in the input file where found in the map data.");
            }
            else
            {
                String summaryText = String.Format("{0} of the textures in the input file are being used by {1} archetypes and {2} sections.",
                                                   textureNames.Count,
                                                   occurrences.GroupBy(item => item.Archetype).Count(),
                                                   occurrences.GroupBy(item => item.Section).Count());
                WriteParagraph(writer, summaryText);

                // Output the table of contents.
                writer.RenderBeginTag(HtmlTextWriterTag.Div);
                writer.AddAttribute(HtmlTextWriterAttribute.Href, "#BySection");
                writer.RenderBeginTag(HtmlTextWriterTag.A);
                writer.Write("Results by Section");
                writer.RenderEndTag();
                writer.RenderEndTag();

                writer.AddAttribute(HtmlTextWriterAttribute.Style, "margin-left: 50px;");
                writer.RenderBeginTag(HtmlTextWriterTag.Div);

                IEnumerable<TextureOccurrence> sortedOccurrences =
                    occurrences.OrderBy(item => item.Section.Name);
                foreach (IGrouping<String, TextureOccurrence> sectionGroup in sortedOccurrences.GroupBy(item => item.Section.Name))
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.Div);
                    writer.AddAttribute(HtmlTextWriterAttribute.Href, String.Format("#{0}", sectionGroup.Key));
                    writer.RenderBeginTag(HtmlTextWriterTag.A);
                    writer.Write(sectionGroup.Key);
                    writer.RenderEndTag();
                    writer.RenderEndTag();
                }
                writer.RenderEndTag();

                writer.RenderBeginTag(HtmlTextWriterTag.Div);
                writer.AddAttribute(HtmlTextWriterAttribute.Href, "#ByTexture");
                writer.RenderBeginTag(HtmlTextWriterTag.A);
                writer.Write("Results by Texture");
                writer.RenderEndTag();
                writer.RenderEndTag();
                /*
                writer.AddAttribute(HtmlTextWriterAttribute.Style, "margin-left: 50px;");
                writer.RenderBeginTag(HtmlTextWriterTag.Div);

                sortedOccurrences =
                    occurrences.OrderBy(item => item.TextureName);
                foreach (IGrouping<String, TextureOccurrence> textureGroup in sortedOccurrences.GroupBy(item => item.Section))
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.Div);
                    writer.AddAttribute(HtmlTextWriterAttribute.Href, String.Format("#{0}", textureGroup.Key));
                    writer.RenderBeginTag(HtmlTextWriterTag.A);
                    writer.Write(textureGroup.Key);
                    writer.RenderEndTag();
                    writer.RenderEndTag();
                }
                writer.RenderEndTag();
                */
            }
        }

        /// <summary>
        /// Outputs the texture information grouped by section then archetype
        /// </summary>
        private void OutputOccurrencesBySection(HtmlTextWriter writer, IList<TextureOccurrence> occurrences)
        {
            // <h2><a name="BySection">By Section</a></h2>
            writer.RenderBeginTag(HtmlTextWriterTag.H2);
            writer.AddAttribute(HtmlTextWriterAttribute.Name, "BySection");
            writer.RenderBeginTag(HtmlTextWriterTag.A);
            writer.Write("Results by Section");
            writer.RenderEndTag();
            writer.RenderEndTag();

            // Group by section
            writer.AddAttribute(HtmlTextWriterAttribute.Style, "margin: 0 auto;");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            IEnumerable<TextureOccurrence> sortedOccurrences =
                occurrences.OrderBy(item => item.Section.Name)
                           .ThenBy(item => item.Archetype.Name)
                           .ThenBy(item => item.TextureName);

            foreach (IGrouping<IMapSection, TextureOccurrence> sectionGroup in sortedOccurrences.GroupBy(item => item.Section))
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Name, sectionGroup.Key.Name);
                writer.RenderBeginTag(HtmlTextWriterTag.A);
                writer.RenderBeginTag(HtmlTextWriterTag.B);
                writer.Write(sectionGroup.Key.Name);
                writer.RenderEndTag();
                writer.RenderEndTag();

                writer.AddAttribute(HtmlTextWriterAttribute.Style, "margin-left: 50px;");
                writer.RenderBeginTag(HtmlTextWriterTag.Div);

                // Group by archetype
                foreach (IGrouping<IMapArchetype, TextureOccurrence> archetypeGroup in sectionGroup.GroupBy(item => item.Archetype))
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.I);
                    writer.Write(archetypeGroup.Key.Name);
                    writer.RenderEndTag();

                    writer.AddAttribute(HtmlTextWriterAttribute.Style, "margin-left: 50px;margin-bottom:10px;");
                    writer.RenderBeginTag(HtmlTextWriterTag.Div);

                    foreach (IGrouping<String, TextureOccurrence> textureGroup in archetypeGroup.GroupBy(item => item.TextureName))
                    {
                        writer.RenderBeginTag(HtmlTextWriterTag.Div);
                        writer.Write(textureGroup.Key);
                        writer.RenderEndTag();
                    }
                    writer.RenderEndTag();
                }
                writer.RenderEndTag();
            }
            writer.RenderEndTag();
        }

        private void OutputOccurrencesByTexture(HtmlTextWriter writer, IList<TextureOccurrence> occurrences)
        {
            // <h2><a name="ByTexture">By Texture</a></h2>
            writer.RenderBeginTag(HtmlTextWriterTag.H2);
            writer.AddAttribute(HtmlTextWriterAttribute.Name, "ByTexture");
            writer.RenderBeginTag(HtmlTextWriterTag.A);
            writer.Write("Results by Texture");
            writer.RenderEndTag();
            writer.RenderEndTag();

            // Group by section
            writer.AddAttribute(HtmlTextWriterAttribute.Style, "margin: 0 auto;");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            IEnumerable<TextureOccurrence> sortedOccurrences =
                occurrences.OrderBy(item => item.TextureName)
                           .ThenBy(item => item.Archetype.Name)
                           .ThenBy(item => item.Section.Name);

            foreach (IGrouping<String, TextureOccurrence> textureGroup in sortedOccurrences.GroupBy(item => item.TextureName))
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Name, textureGroup.Key);
                writer.RenderBeginTag(HtmlTextWriterTag.A);
                writer.RenderBeginTag(HtmlTextWriterTag.B);
                writer.Write(textureGroup.Key);
                writer.RenderEndTag();
                writer.RenderEndTag();

                writer.AddAttribute(HtmlTextWriterAttribute.Style, "margin-left: 50px;");
                writer.RenderBeginTag(HtmlTextWriterTag.Div);

                // Group by archetype
                foreach (IGrouping<IMapSection, TextureOccurrence> sectionGroup in textureGroup.GroupBy(item => item.Section))
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.I);
                    writer.Write(sectionGroup.Key.Name);
                    writer.RenderEndTag();

                    writer.AddAttribute(HtmlTextWriterAttribute.Style, "margin-left: 50px;margin-bottom:10px;");
                    writer.RenderBeginTag(HtmlTextWriterTag.Div);

                    foreach (IGrouping<IMapArchetype, TextureOccurrence> archetypeGroup in sectionGroup.GroupBy(item => item.Archetype))
                    {
                        writer.RenderBeginTag(HtmlTextWriterTag.Div);
                        writer.Write(archetypeGroup.Key.Name);
                        writer.RenderEndTag();
                    }
                    writer.RenderEndTag();
                }
                writer.RenderEndTag();
            }
            writer.RenderEndTag();
        }
        #endregion // Private Methods
    } // TextureUseReport
}
