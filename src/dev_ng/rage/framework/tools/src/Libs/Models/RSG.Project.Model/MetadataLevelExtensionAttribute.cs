﻿//---------------------------------------------------------------------------------------------
// <copyright file="MetadataLevelExtensionAttribute.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.Model
{
    using System;

    /// <summary>
    /// Used to give a metadata level a file extension. This file extension is used to locate
    /// the file from the projects base file that the metadata is loaded from for a specific
    /// metadata level.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
    internal class MetadataLevelExtensionAttribute : Attribute
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Extension"/> property.
        /// </summary>
        private string _extension;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MetadataLevelExtensionAttribute"/>
        /// class with the specified extension.
        /// </summary>
        /// <param name="extension">
        /// The extension level set on this instance.
        /// </param>
        public MetadataLevelExtensionAttribute(string extension)
        {
            this._extension = extension;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the extension value that this attribute has set.
        /// </summary>
        public string Extension
        {
            get { return this._extension; }
        }
        #endregion Properties
    } // RSG.Project.Model.MetadataLevelExtensionAttribute {Class}
} // RSG.Project.Model {Namespace}
