﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Math;

namespace RSG.Model.Common.Map
{
    /// <summary>
    /// An object that can be found on the map.
    /// i.e. one that has a transformation.
    /// </summary>
    public interface IMapObjectWithTransform : IAsset
    {
        /// <summary>
        /// 
        /// </summary>
        Vector3f Position { get; }
        /// <summary>
        /// 
        /// </summary>
        Quaternionf Rotation { get; }
        /// <summary>
        /// 
        /// </summary>
        Matrix34f Transform { get; }
    } // IMapObject
}
