﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common.Map;
using RSG.Base.Tasks;
using System.Threading;

namespace RSG.Model.Report
{
    /// <summary>
    /// A report that generates it's content on the fly based on section data.
    /// </summary>
    public interface IDynamicSectionReport : IDynamicReport
    {
    } // IDynamicSectionReport
} // RSG.Model.Report 
