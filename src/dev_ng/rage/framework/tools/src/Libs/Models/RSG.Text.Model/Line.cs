﻿//---------------------------------------------------------------------------------------------
// <copyright file="Line.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.Model
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Xml;
    using RSG.Base.Extensions;
    using RSG.Editor;
    using RSG.Editor.Model;
    using RSG.Text.Model.Resources;

    /// <summary>
    /// Represents a single line of dialogue within a <see cref="Conversation"/> that is tied
    /// to an audio file.
    /// </summary>
    [DebuggerDisplay("{CharacterName} - {_text}")]
    internal class Line : ModelBase, ILine
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="AlwaysSubtitled"/> property.
        /// </summary>
        private bool _alwaysSubtitled;

        /// <summary>
        /// The private field used for the <see cref="Audibility"/> property.
        /// </summary>
        private Guid _audibility;

        /// <summary>
        /// The private field used for the <see cref="AudioFilepath"/> property.
        /// </summary>
        private string _audioFilepath;

        /// <summary>
        /// The private field used for the <see cref="AudioType"/> property.
        /// </summary>
        private Guid _audioType;

        /// <summary>
        /// The private field used for the <see cref="CharacterId"/> property.
        /// </summary>
        private Guid _characterId;

        /// <summary>
        /// The private field used for the <see cref="Text"/> property.
        /// </summary>
        private string _dialogue;

        /// <summary>
        /// The private field used for the <see cref="DontInterruptForSpecialAbility"/>
        /// property.
        /// </summary>
        private bool _dontInterruptForSpecialAbility;

        /// <summary>
        /// The private field used for the <see cref="DucksRadio"/> property.
        /// </summary>
        private bool _ducksRadio;

        /// <summary>
        /// The private field used for the <see cref="DucksScore"/> property.
        /// </summary>
        private bool _ducksScore;

        /// <summary>
        /// The private field used for the <see cref="HeadsetSubmix"/> property.
        /// </summary>
        private bool _headsetSubmix;

        /// <summary>
        /// The private field used for the <see cref="Interruptible"/> property.
        /// </summary>
        private bool _interruptible;

        /// <summary>
        /// The private field used for the <see cref="LastModifiedTime"/> property.
        /// </summary>
        private DateTime _lastModifiedTime;

        /// <summary>
        /// The private field used for the <see cref="Listener"/> property.
        /// </summary>
        private int _listener;

        /// <summary>
        /// The private field used for the <see cref="ManualFilename"/> property.
        /// </summary>
        private bool _manualFilename;

        /// <summary>
        /// The private field used for the <see cref="PadSpeaker"/> property.
        /// </summary>
        private bool _padSpeaker;

        /// <summary>
        /// The private field used for the <see cref="Recorded"/> property.
        /// </summary>
        private bool _recorded;

        /// <summary>
        /// The private field used for the <see cref="SentForRecording"/> property.
        /// </summary>
        private bool _sentForRecording;

        /// <summary>
        /// The private field used for the <see cref="Speaker"/> property.
        /// </summary>
        private int _speaker;

        /// <summary>
        /// The private field used for the <see cref="Special"/> property.
        /// </summary>
        private Guid _special;

        /// <summary>
        /// The private value indicating whether the update to the modified time has been
        /// suspended.
        /// </summary>
        private int _suspendModifiedTimeUpdaterCount;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="Line"/> class.
        /// </summary>
        /// <param name="conversation">
        /// The conversation that the line will belong to.
        /// </param>
        internal Line(Conversation conversation)
            : base(conversation)
        {
            if (conversation == null)
            {
                throw new SmartArgumentNullException(() => conversation);
            }

            this._alwaysSubtitled = false;
            this._audibility = this.Configurations.DefaultAudibilityId;
            this._audioFilepath = String.Empty;
            this._audioType = this.Configurations.DefaultAudioTypeId;
            this._characterId = Guid.Empty;
            this._dontInterruptForSpecialAbility = false;
            this._ducksRadio = true;
            this._ducksScore = false;
            this._headsetSubmix = false;
            this._interruptible = true;
            this._lastModifiedTime = DateTime.Now.ToUniversalTime();
            this._listener = 9;
            this._manualFilename = false;
            this._padSpeaker = false;
            this._recorded = false;
            this._sentForRecording = false;
            this._speaker = 9;
            this._special = this.Configurations.DefaultSpecialId;
            this._dialogue = String.Empty;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Line"/> class using the specified xml
        /// reader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The xml reader that contains the data used to initialise this instance.
        /// </param>
        /// <param name="conversation">
        /// The conversation that the line will belong to.
        /// </param>
        internal Line(XmlReader reader, Conversation conversation)
            : base(conversation)
        {
            if (conversation == null)
            {
                throw new SmartArgumentNullException(() => conversation);
            }

            if (reader == null)
            {
                throw new SmartArgumentNullException(() => reader);
            }

            this._alwaysSubtitled = false;
            this._audibility = this.Configurations.DefaultAudibilityId;
            this._audioFilepath = String.Empty;
            this._audioType = this.Configurations.DefaultAudioTypeId;
            this._characterId = Guid.Empty;
            this._dontInterruptForSpecialAbility = false;
            this._ducksRadio = true;
            this._ducksScore = false;
            this._headsetSubmix = false;
            this._interruptible = true;
            this._lastModifiedTime = DateTime.Now.ToUniversalTime();
            this._listener = 9;
            this._manualFilename = false;
            this._padSpeaker = false;
            this._recorded = false;
            this._sentForRecording = false;
            this._speaker = 9;
            this._special = this.Configurations.DefaultSpecialId;
            this._dialogue = String.Empty;

            using (new SuspendUndoEngine(this.UndoEngine, SuspendUndoEngineMode.UndoEvents))
            {
                this.Deserialise(reader);
                if (this._audibility == Guid.Empty)
                {
                    this._audibility = this.Configurations.DefaultAudibilityId;
                }

                if (this._audioType == Guid.Empty)
                {
                    this._audioType = this.Configurations.DefaultAudioTypeId;
                }

                if (this._special == Guid.Empty)
                {
                    this._special = this.Configurations.DefaultSpecialId;
                }
            }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Line"/> class as a deep copy of the
        /// specified instance.
        /// </summary>
        /// <param name="other">
        /// The instance to copy.
        /// </param>
        /// <param name="conversation">
        /// The conversation that the line will belong to.
        /// </param>
        internal Line(Line other, Conversation conversation)
            : base(conversation)
        {
            if (conversation == null)
            {
                throw new SmartArgumentNullException(() => conversation);
            }

            if (other == null)
            {
                throw new SmartArgumentNullException(() => other);
            }

            this._alwaysSubtitled = other._alwaysSubtitled;
            this._audibility = other._audibility;
            this._audioFilepath = other._audioFilepath;
            this._audioType = other._audioType;
            this._characterId = other._characterId;
            this._dontInterruptForSpecialAbility = other._dontInterruptForSpecialAbility;
            this._ducksRadio = other._ducksRadio;
            this._ducksScore = other._ducksScore;
            this._headsetSubmix = other._headsetSubmix;
            this._interruptible = other._interruptible;
            this._listener = other._listener;
            this._manualFilename = other._manualFilename;
            this._padSpeaker = other._padSpeaker;
            this._recorded = false;
            this._sentForRecording = false;
            this._speaker = other._speaker;
            this._special = other._special;
            this._dialogue = other._dialogue;
        }
        #endregion Constructors

        #region Events
        /// <summary>
        /// Occurs whenever the character id for this line changes.
        /// </summary>
        public event EventHandler<ValueChangedEventArgs<Guid>> CharacterIdChanged;
        #endregion Events

        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether this line always gets subtitled even when
        /// they are turned off.
        /// </summary>
        public bool AlwaysSubtitled
        {
            get { return this._alwaysSubtitled; }
            set { this.SetProperty(ref this._alwaysSubtitled, value); }
        }

        /// <summary>
        /// Gets or sets the identifier of the audibility value for this line.
        /// </summary>
        public Guid Audibility
        {
            get { return this._audibility; }
            set { this.SetProperty(ref this._audibility, value); }
        }

        /// <summary>
        /// Gets or sets the file path to the audio file containing the data for this line.
        /// </summary>
        public string AudioFilepath
        {
            get { return this._audioFilepath; }
            set { this.SetProperty(ref this._audioFilepath, value ?? String.Empty); }
        }

        /// <summary>
        /// Gets or sets the identifier of the type of audio this line uses.
        /// </summary>
        public Guid AudioType
        {
            get { return this._audioType; }
            set { this.SetProperty(ref this._audioType, value); }
        }

        /// <summary>
        /// Gets or sets the identifier of the character that is speaking this line.
        /// </summary>
        public Guid CharacterId
        {
            get
            {
                return this._characterId;
            }

            set
            {
                using (UndoRedoBatch batch = new UndoRedoBatch(this.UndoEngine))
                {
                    string[] propertyNames = new string[]
                    {
                        "CharacterId",
                        "ManualFilename",
                        "Speaker"
                    };

                    Guid oldValue = this._characterId;
                    if (this.SetProperty(ref this._characterId, value, propertyNames))
                    {
                        if (this.CharacterIdChanged != null)
                        {
                            this.CharacterIdChanged(
                                this,
                                new ValueChangedEventArgs<Guid>(oldValue, this._characterId));
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets the character name that is speaking this line, using the character id and the
        /// current configurations associated with this line.
        /// </summary>
        public string CharacterName
        {
            get { return this.Configurations.GetCharacterName(this.CharacterId); }
        }

        /// <summary>
        /// Gets the dialogue configurations that have been associated with this line.
        /// </summary>
        public DialogueConfigurations Configurations
        {
            get
            {
                if (this.Conversation == null)
                {
                    return new DialogueConfigurations();
                }

                return this.Conversation.Configurations;
            }
        }

        /// <summary>
        /// Gets the conversation this line belongs to.
        /// </summary>
        public Conversation Conversation
        {
            get { return this.Parent as Conversation; }
        }

        /// <summary>
        /// Gets or sets the actual dialogue for this line.
        /// </summary>
        public string Dialogue
        {
            get { return this._dialogue; }
            set { this.SetProperty(ref this._dialogue, value ?? String.Empty); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not the audio for this line can be
        /// interrupted by the special ability.
        /// </summary>
        public bool DontInterruptForSpecialAbility
        {
            get { return this._dontInterruptForSpecialAbility; }
            set { this.SetProperty(ref this._dontInterruptForSpecialAbility, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not this line ducks the radio.
        /// </summary>
        public bool DucksRadio
        {
            get { return this._ducksRadio; }
            set { this.SetProperty(ref this._ducksRadio, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not this line ducks the score.
        /// </summary>
        public bool DucksScore
        {
            get { return this._ducksScore; }
            set { this.SetProperty(ref this._ducksScore, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not this line goes through the headset
        /// sub-mix.
        /// </summary>
        public bool HeadsetSubmix
        {
            get { return this._headsetSubmix; }
            set { this.SetProperty(ref this._headsetSubmix, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the audio for this line can be interrupt.
        /// </summary>
        public bool Interruptible
        {
            get { return this._interruptible; }
            set { this.SetProperty(ref this._interruptible, value); }
        }

        /// <summary>
        /// Gets or sets the date and time this line was last modified.
        /// </summary>
        public DateTime LastModifiedTime
        {
            get
            {
                return this._lastModifiedTime;
            }

            set
            {
                IEnumerable<string> propertyNames = Enumerable.Repeat("LastModifiedTime", 1);
                base.SetProperty(ref this._lastModifiedTime, value, propertyNames);
            }
        }

        /// <summary>
        /// Gets or sets the index value of the listener of this line.
        /// </summary>
        public int Listener
        {
            get { return this._listener; }
            set { this.SetProperty(ref this._listener, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the audio file-path has been entered
        /// manually by the user as suppose to being automatically generated.
        /// </summary>
        public bool ManualFilename
        {
            get
            {
                Guid id = this._characterId;
                DialogueCharacter character = this.Configurations.GetCharacter(id);
                if (character != null && character.UsesManualFilenames)
                {
                    return true;
                }

                return this._manualFilename;
            }

            set
            {
                this.SetProperty(ref this._manualFilename, value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this line should be spoken out of the
        /// pad speaker.
        /// </summary>
        public bool PadSpeaker
        {
            get { return this._padSpeaker; }
            set { this.SetProperty(ref this._padSpeaker, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this line has been recorded.
        /// </summary>
        public bool Recorded
        {
            get { return this._recorded; }
            set { this.SetProperty(ref this._recorded, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this line has been sent to be recorded.
        /// </summary>
        public bool SentForRecording
        {
            get { return this._sentForRecording; }
            set { this.SetProperty(ref this._sentForRecording, value); }
        }

        /// <summary>
        /// Gets the index value of the speaker for this line.
        /// </summary>
        public int Speaker
        {
            get
            {
                CharacterMappings mappings = this.Conversation.Dialogue.CharacterMappings;
                if (mappings == null)
                {
                    return 9;
                }

                if (!mappings.IsCharacterMapped(this._characterId))
                {
                    return this._speaker;
                }

                return mappings.GetSpeakerFromCharacterId(this.CharacterId);
            }
        }

        /// <summary>
        /// Gets or sets the identifier of the special field for this line.
        /// </summary>
        public Guid Special
        {
            get { return this._special; }
            set { this.SetProperty(ref this._special, value); }
        }

        /// <summary>
        /// Gets the voice name that has been assigned to the line. This is the voice that is
        /// used while recording the line.
        /// </summary>
        public string VoiceName
        {
            get
            {
                CharacterMappings mappings = this.Conversation.Dialogue.CharacterMappings;
                if (mappings == null)
                {
                    return null;
                }

                return mappings.GetVoiceNameFromCharacterId(this.CharacterId);
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a deep copy of the current instance.
        /// </summary>
        /// <returns>
        /// A deep copy of the current System.Object.
        /// </returns>
        public override object DeepClone()
        {
            return new Line(this, this.Conversation);
        }

        /// <summary>
        /// Clones this line object and sets up its parent to be the specified conversation.
        /// </summary>
        /// <param name="conversation">
        /// The conversation that is to be the cloned objects parent.
        /// </param>
        /// <returns>
        /// The newly cloned line object.
        /// </returns>
        public ILine DeepClone(Conversation conversation)
        {
            return new Line(this, conversation);
        }

        /// <summary>
        /// Serialises the data contained within this instance onto the specified xml writer.
        /// </summary>
        /// <param name="writer">
        /// The xml writer this instance will be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            writer.WriteStartElement("AlwaysSubtitle");
            writer.WriteAttributeString("value", this._alwaysSubtitled.ToString());
            writer.WriteEndElement();

            writer.WriteElementString("Audibility", this._audibility.ToString("D"));
            writer.WriteElementString("AudioFilepath", this._audioFilepath);
            writer.WriteElementString("AudioType", this._audioType.ToString("D"));
            writer.WriteElementString("CharacterId", this._characterId.ToString("D"));
            writer.WriteElementString("Dialogue", this._dialogue);

            writer.WriteStartElement("DontInterruptForSpecialAbility");
            writer.WriteAttributeString(
                "value", this._dontInterruptForSpecialAbility.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("DucksRadio");
            writer.WriteAttributeString("value", this._ducksRadio.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("DucksScore");
            writer.WriteAttributeString("value", this._ducksScore.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("HeadsetSubmix");
            writer.WriteAttributeString("value", this._headsetSubmix.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("Interruptible");
            writer.WriteAttributeString("value", this._interruptible.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("LastModifiedTime");
            writer.WriteAttributeString("value", this._lastModifiedTime.Ticks.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("Listener");
            writer.WriteAttributeString("value", this._listener.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("ManualFilename");
            writer.WriteAttributeString("value", this._manualFilename.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("PadSpeaker");
            writer.WriteAttributeString("value", this._padSpeaker.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("Recorded");
            writer.WriteAttributeString("value", this._recorded.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("SentForRecording");
            writer.WriteAttributeString("value", this._sentForRecording.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("Speaker");
            writer.WriteAttributeString("value", this._speaker.ToString());
            writer.WriteEndElement();

            writer.WriteElementString("Special", this._special.ToString("D"));
        }

        /// <summary>
        /// Gets a disposable object that is used to suspend the update to the modified time
        /// property due to a property change.
        /// </summary>
        /// <returns>
        /// The disposable object that is used to suspend the update to the modified time.
        /// </returns>
        public DisposableObject SuspendModifiedTimeUpdater()
        {
            return new ModifiedTimeSuspender(this);
        }

        /// <summary>
        /// Validates this entity and creates a validation result object containing the errors
        /// and warnings.
        /// </summary>
        /// <param name="recursive">
        /// A value indicating whether any child models to this one should also be validated.
        /// </param>
        /// <returns>
        /// A validation result object containing all of the errors and warnings associated
        /// with this validation pass.
        /// </returns>
        public override ValidationResult Validate(bool recursive)
        {
            ValidationResult result = new ValidationResult();
            this.ValidateCharacterIdPresence(result);
            this.ValidateAudiopathPresence(result);
            this.ValidateAudiopathUniqueness(result);
            this.ValidateSpeakerMapping(result);
            return result;
        }

        /// <summary>
        /// Sets the given field to the given value making sure the correct property changed
        /// event is fired.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the field that is getting set.
        /// </typeparam>
        /// <param name="field">
        /// A reference to the field that is getting set.
        /// </param>
        /// <param name="value">
        /// The value to set the field to.
        /// </param>
        /// <param name="comparer">
        /// The equality comparer to use to determine whether the value has changed.
        /// </param>
        /// <param name="names">
        /// A iterator around all of property names that are changed due to this one change.
        /// </param>
        /// <returns>
        /// True if the property was changed; otherwise, false.
        /// </returns>
        protected override bool SetProperty<T>(
            ref T field, T value, IEqualityComparer<T> comparer, IEnumerable<string> names)
        {
            UndoEngine undoEngine = this.UndoEngine;
            UndoRedoBatch batch = null;
            if (undoEngine != null && !undoEngine.PerformingEvent)
            {
                batch = new UndoRedoBatch(undoEngine);
            }

            if (!base.SetProperty<T>(ref field, value, comparer, names))
            {
                if (batch != null)
                {
                    batch.Dispose();
                    batch = null;
                }

                return false;
            }

            if (undoEngine == null || !undoEngine.PerformingEvent)
            {
                if (this._suspendModifiedTimeUpdaterCount <= 0)
                {
                    this.LastModifiedTime = DateTime.Now.ToUniversalTime();
                }
            }

            if (batch != null)
            {
                batch.Dispose();
                batch = null;
            }

            return true;
        }

        /// <summary>
        /// Initialises this instance using the data contained within the specified xml reader.
        /// </summary>
        /// <param name="reader">
        /// The xml reader that contains the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() == XmlNodeType.Element)
            {
                if (String.Equals("AlwaysSubtitle", reader.Name))
                {
                    string value = reader.GetAttribute("value");
                    if (!bool.TryParse(value, out this._alwaysSubtitled))
                    {
                        this._alwaysSubtitled = false;
                    }
                }
                else if (String.Equals("Audibility", reader.Name))
                {
                    string input = reader.ReadElementContentAsString();
                    if (!Guid.TryParseExact(input, "D", out this._audibility))
                    {
                        this._audibility = this.Configurations.DefaultAudibilityId;
                    }
                }
                else if (String.Equals("AudioFilepath", reader.Name))
                {
                    this._audioFilepath = reader.ReadElementContentAsString();
                }
                else if (String.Equals("AudioType", reader.Name))
                {
                    string input = reader.ReadElementContentAsString();
                    if (!Guid.TryParseExact(input, "D", out this._audioType))
                    {
                        this._audioType = this.Configurations.DefaultAudioTypeId;
                    }
                }
                else if (String.Equals("CharacterId", reader.Name))
                {
                    string input = reader.ReadElementContentAsString();
                    if (!Guid.TryParseExact(input, "D", out this._characterId))
                    {
                        this._characterId = Guid.Empty;
                    }
                }
                else if (String.Equals("Dialogue", reader.Name))
                {
                    this._dialogue = reader.ReadElementContentAsString();
                }
                else if (String.Equals("DontInterruptForSpecialAbility", reader.Name))
                {
                    string value = reader.GetAttribute("value");
                    if (!bool.TryParse(value, out this._dontInterruptForSpecialAbility))
                    {
                        this._dontInterruptForSpecialAbility = false;
                    }
                }
                else if (String.Equals("DucksRadio", reader.Name))
                {
                    string value = reader.GetAttribute("value");
                    if (!bool.TryParse(value, out this._ducksRadio))
                    {
                        this._ducksRadio = true;
                    }
                }
                else if (String.Equals("DucksScore", reader.Name))
                {
                    string value = reader.GetAttribute("value");
                    if (!bool.TryParse(value, out this._ducksScore))
                    {
                        this._ducksScore = false;
                    }
                }
                else if (String.Equals("HeadsetSubmix", reader.Name))
                {
                    string value = reader.GetAttribute("value");
                    if (!bool.TryParse(value, out this._headsetSubmix))
                    {
                        this._headsetSubmix = false;
                    }
                }
                else if (String.Equals("Interruptible", reader.Name))
                {
                    string value = reader.GetAttribute("value");
                    if (!bool.TryParse(value, out this._interruptible))
                    {
                        this._interruptible = true;
                    }
                }
                else if (String.Equals("LastModifiedTime", reader.Name))
                {
                    string value = reader.GetAttribute("value");
                    long ticks = 0;
                    if (long.TryParse(value, out ticks))
                    {
                        this._lastModifiedTime = new DateTime(ticks, DateTimeKind.Utc);
                    }
                    else
                    {
                        this._lastModifiedTime = DateTime.Now.ToUniversalTime();
                    }
                }
                else if (String.Equals("Listener", reader.Name))
                {
                    string value = reader.GetAttribute("value");
                    if (!int.TryParse(value, out this._listener))
                    {
                        this._listener = 9;
                    }
                }
                else if (String.Equals("ManualFilename", reader.Name))
                {
                    string value = reader.GetAttribute("value");
                    if (!bool.TryParse(value, out this._manualFilename))
                    {
                        this._manualFilename = false;
                    }
                }
                else if (String.Equals("PadSpeaker", reader.Name))
                {
                    string value = reader.GetAttribute("value");
                    if (!bool.TryParse(value, out this._padSpeaker))
                    {
                        this._padSpeaker = false;
                    }
                }
                else if (String.Equals("Recorded", reader.Name))
                {
                    string value = reader.GetAttribute("value");
                    if (!bool.TryParse(value, out this._recorded))
                    {
                        this._recorded = false;
                    }
                }
                else if (String.Equals("SentForRecording", reader.Name))
                {
                    string value = reader.GetAttribute("value");
                    if (!bool.TryParse(value, out this._sentForRecording))
                    {
                        this._sentForRecording = false;
                    }
                }
                else if (String.Equals("Speaker", reader.Name))
                {
                    string value = reader.GetAttribute("value");
                    if (!int.TryParse(value, out this._speaker))
                    {
                        this._speaker = 9;
                    }
                }
                else if (String.Equals("Special", reader.Name))
                {
                    string input = reader.ReadElementContentAsString();
                    if (!Guid.TryParseExact(input, "D", out this._special))
                    {
                        this._special = this.Configurations.DefaultSpecialId;
                    }
                }

                reader.Skip();
            }

            if (reader.NodeType == XmlNodeType.EndElement)
            {
                reader.ReadEndElement();
            }
        }

        /// <summary>
        /// Validates this line to make sure the audio file path is set to a valid value.
        /// </summary>
        /// <param name="result">
        /// The results container to add any errors into.
        /// </param>
        private void ValidateAudiopathPresence(ValidationResult result)
        {
            if (String.IsNullOrWhiteSpace(this.AudioFilepath))
            {
                result.AddError("AudioFilepath", StringTable.MissingFilename);
            }
        }

        /// <summary>
        /// Validates this line to make sure the audio file path is unique among the other
        /// lines in all of the conversations in the top level dialogue object.
        /// </summary>
        /// <param name="result">
        /// The results container to add any errors into.
        /// </param>
        private void ValidateAudiopathUniqueness(ValidationResult result)
        {
            DialogueCharacter character = this.Configurations.GetCharacter(this.CharacterId);
            if (character != null && character.UsesManualFilenames)
            {
                return;
            }

            Conversation parent = this.Conversation;
            if (parent == null)
            {
                return;
            }

            Dialogue dialogue = parent.Dialogue;
            if (dialogue == null)
            {
                return;
            }

            StringComparison comparisonType = StringComparison.OrdinalIgnoreCase;
            foreach (Conversation conversation in dialogue.Conversations)
            {
                foreach (ILine line in conversation.Lines)
                {
                    if (Object.ReferenceEquals(line, this))
                    {
                        continue;
                    }

                    if (String.Equals(line.AudioFilepath, this.AudioFilepath, comparisonType))
                    {
                        string msg =
                            StringTable.FilenameNotUnique.FormatCurrent(this.AudioFilepath);
                        result.AddError("AudioFilepath", msg);
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Validates this line to make sure the character id is set to a valid value.
        /// </summary>
        /// <param name="result">
        /// The results container to add any errors into.
        /// </param>
        private void ValidateCharacterIdPresence(ValidationResult result)
        {
            if (this.CharacterId == Guid.Empty)
            {
                result.AddError("CharacterId", StringTable.MissingCharacter);
            }
        }

        /// <summary>
        /// Validates this line to make sure its speaker is mapped inside the character
        /// mapper for the character id.
        /// </summary>
        /// <param name="result">
        /// The results container to add any errors into.
        /// </param>
        private void ValidateSpeakerMapping(ValidationResult result)
        {
            if (this.CharacterId == Guid.Empty)
            {
                return;
            }

            CharacterMappings mappings = this.Conversation.Dialogue.CharacterMappings;
            if (!mappings.IsCharacterMapped(this.CharacterId))
            {
                result.AddError("Speaker", StringTable.SpeakerNotMapped);
            }
        }
        #endregion Methods

        #region Classes
        /// <summary>
        /// A disposable object used to suspend the update the the modified time property due
        /// to a property change.
        /// </summary>
        private class ModifiedTimeSuspender : DisposableObject
        {
            #region Fields
            /// <summary>
            /// The private reference to the line that is being suspended.
            /// </summary>
            private Line _line;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="ModifiedTimeSuspender"/> class.
            /// </summary>
            /// <param name="line">
            /// The line whose modified time update is suspended.
            /// </param>
            public ModifiedTimeSuspender(Line line)
            {
                this._line = line;
                this._line._suspendModifiedTimeUpdaterCount++;
            }
            #endregion Constructors

            #region Methods
            /// <summary>
            /// Reduces the attached lines suspend modified time updater count.
            /// </summary>
            protected override void DisposeManagedResources()
            {
                this._line._suspendModifiedTimeUpdaterCount--;
            }
            #endregion Methods
        } // Line.ModifiedTimeSuspender {Class}
        #endregion
    } // RSG.Text.Model.Line {Class}
} // RSG.Text.Model {Namespace}
