﻿//---------------------------------------------------------------------------------------------
// <copyright file="Track.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.Model
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    /// <summary>
    /// Contains recording data for a number of dialogue lines. Where the lines can be
    /// identified by the audio filepath.
    /// </summary>
    public class Track
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Name"/> property.
        /// </summary>
        private string _name;

        /// <summary>
        /// The private field used for the <see cref="Comments"/> property.
        /// </summary>
        private string _comments;

        /// <summary>
        /// The private field used for the <see cref="UserDelaySamples"/> property.
        /// </summary>
        private int _userDelaySamples;

        /// <summary>
        /// The private field used for the <see cref="Plugin"/> property.
        /// </summary>
        private string _plugins;

        /// <summary>
        /// The private field used for the <see cref="RecordedData"/> property.
        /// </summary>
        private List<LineRecordedData> _recordedData;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="Track"/> class.
        /// </summary>
        /// <param name="data">
        /// The string line that contains the data for the recording.
        /// </param>
        public Track(string data)
        {
            this._recordedData = new List<LineRecordedData>();
            using (System.IO.StringReader reader = new System.IO.StringReader(data))
            {
                bool inLineData = false;
                string line = null;
                while ((line = reader.ReadLine()) != null)
                {
                    if (line.StartsWith("TRACK NAME:"))
                    {
                        this._name = line.Replace("TRACK NAME:", "").Trim();
                    }
                    else if (line.StartsWith("COMMENTS:"))
                    {
                        this._comments = line.Replace("COMMENTS:", "").Trim();
                    }
                    else if (line.StartsWith("USER DELAY:"))
                    {
                        string text = line.Replace("USER DELAY:", "");
                        text = text.Replace("Samples", "").Trim();
                        int.TryParse(text, out this._userDelaySamples);
                    }
                    else if (line.StartsWith("PLUG-INS:"))
                    {
                        this._plugins = line.Replace("PLUG-INS:", "").Trim();
                    }
                    else if (line.StartsWith("CHANNEL"))
                    {
                        inLineData = true;
                    }
                    else if (inLineData)
                    {
                        this._recordedData.Add(new LineRecordedData(line));
                    }
                }
            }
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the name for this track.
        /// </summary>
        public string Name
        {
            get { return this._name; }
            set { this._name = value; }
        }

        /// <summary>
        /// Gets or sets the comment for this track.
        /// </summary>
        public string Comments
        {
            get { return this._comments; }
            set { this._comments = value; }
        }

        /// <summary>
        /// Gets or sets the user delay samples for this track.
        /// </summary>
        public int UserDelaySamples
        {
            get { return this._userDelaySamples; }
            set { this._userDelaySamples = value; }
        }

        /// <summary>
        /// Gets or sets the plugins for this track.
        /// </summary>
        public string Plugin
        {
            get { return this._plugins; }
            set { this._plugins = value; }
        }

        /// <summary>
        /// Gets the line recording data for this track.
        /// </summary>
        public ReadOnlyCollection<LineRecordedData> RecordedData
        {
            get { return this._recordedData.AsReadOnly(); }
        }
        #endregion Properties
    } // RSG.Text.Model.Track {Class}
} // RSG.Text.Model {Namespace}
