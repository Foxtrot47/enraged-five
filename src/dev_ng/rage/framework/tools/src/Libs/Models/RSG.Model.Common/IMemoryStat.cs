﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common
{
    /// <summary>
    /// A per platform memory statistic.
    /// </summary>
    public interface IMemoryStat
    {
        /// <summary>
        /// 
        /// </summary>
        uint PhysicalSize { get; set; }

        /// <summary>
        /// 
        /// </summary>
        uint VirtualSize { get; set; }
    } // IPlatformMemoryStat
}
