﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.Base.ConfigParser;
using RSG.Base.Configuration;
using RSG.Model.Common.Util;
using RSG.ManagedRage;
using RSG.Model.Asset.Util;

namespace RSG.Model.Weapon
{
    /// <summary>
    /// Collection of vehicles that come from the local export data
    /// </summary>
    public class LocalWeaponCollection : WeaponCollectionBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public ConfigGameView GameView
        {
            get
            {
                if (m_gv == null)
                {
                    m_gv = new ConfigGameView();
                }
                return m_gv;
            }
        }
        private ConfigGameView m_gv;

        /// <summary>
        /// 
        /// </summary>
        public IConfig Config
        {
            get
            {
                if (m_config == null)
                {
                    m_config = ConfigFactory.CreateConfig();
                }
                return m_config;
            }
        }
        private IConfig m_config;

        /// <summary>
        /// The source folder for the weapon data
        /// </summary>
        public string ExportDataPath
        {
            get;
            protected set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="exportDataPath"></param>
        public LocalWeaponCollection(String exportDataPath)
            : base()
        {
            ExportDataPath = exportDataPath;
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="statsToLoad"></param>
        public override void LoadStats(IEnumerable<StreamableStat> statsToLoad)
        {
            // Two different load paths.
            // 1) Loading "basic" stat from scenexml data.
            // 2) Loading platform stats data from rpf's.

            // Basic stats
            IEnumerable<StreamableStat> nonPlatformStats = statsToLoad.Where(item => item != StreamableWeaponStat.PlatformStats);

            // Load each weapon individually.
            foreach (IWeapon weapon in Weapons)
            {
                if (!weapon.AreStatsLoaded(nonPlatformStats))
                {
                    weapon.LoadStats(nonPlatformStats);
                }
            }

            // Platform stats.
            if (statsToLoad.Contains(StreamableWeaponStat.PlatformStats) && !ArePlatformStatsLoaded())
            {
                // Get the path to the independent rpf file
                List<ContentNode> contentNodes = GameView.Content.Root.FindAll("weaponszip");

                String independentPath = null;
                if (contentNodes.Count > 0)
                {
                    independentPath = LocalLoadUtil.GeneratePathForNode(contentNodes[0]);
                }

                // Open up the pack files
                IDictionary<RSG.Platform.Platform, Packfile> packFiles = LocalLoadUtil.OpenPackFiles(independentPath, GameView, Config);

                // Go over all the weapons telling them to load the platform data
                foreach (IWeapon weapon in Weapons)
                {
                    (weapon as LocalWeapon).LoadPlatformStatsFromExportData(packFiles);
                }

                // Close all the pack files we opened
                LocalLoadUtil.ClosePackFiles(packFiles);
            }
        }
        #endregion // Overrides

        #region Private Methods
        /// <summary>
        /// Helper function that checks whether all the weapons in a collection have their platform stats loaded
        /// </summary>
        /// <param name="weapons"></param>
        /// <returns></returns>
        private bool ArePlatformStatsLoaded()
        {
            bool allLoaded = true;

            foreach (IWeapon weapon in Weapons)
            {
                if (!weapon.IsStatLoaded(StreamableWeaponStat.PlatformStats))
                {
                    allLoaded = false;
                    break;
                }
            }

            return allLoaded;
        }
        #endregion // Private Methods

        #region IDisposable Methods
        /// <summary>
        /// 
        /// </summary>
        public override void Dispose()
        {
            base.Dispose();

            if (m_gv != null)
            {
                m_gv.Dispose();
            }
        }
        #endregion // IDisposable Methods
    } // ExportDataWeaponCollection
} // RSG.Model.Weapon
