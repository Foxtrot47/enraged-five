﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Linq;

namespace RSG.Model.Statistics.Platform
{

    /// <summary>
    /// Statistics for a single resource; physical/virtual bucket capacity and
    /// usage sizes.
    /// </summary>
    [Serializable]
    [DataContract]
    public class ResourceStat
    {
        #region Properties
        /// <summary>
        /// Resource platform.
        /// </summary>
        [DataMember]
        public RSG.Platform.Platform Platform { get; set; }

        /// <summary>
        /// Resource basename.
        /// </summary>
        [DataMember]
        public String ResourceName { get; set; }

        /// <summary>
        /// Resource source filename.
        /// </summary>
        [DataMember]
        public String SourceFilename { get; set; }

        /// <summary>
        /// Resource destination filename.
        /// </summary>
        [DataMember]
        public String DestinationFilename { get; set; }

        /// <summary>
        /// Resource filetype.
        /// </summary>
        [DataMember]
        public RSG.Platform.FileType FileType { get; set; }

        /// <summary>
        /// Summary info : The virtual size
        /// </summary>
        [DataMember]
        public long VirtualSize
        {
            get { return CalcVirtualSize(); }
            private set { }
        }

        /// <summary>
        /// Summary info : The virtual capacity
        /// </summary>        
        [DataMember]
        public long VirtualCapacity
        {
            get { return CalcVirtualCapacity(); }
            private set { }
        }

        /// <summary>
        /// Summary info : The Physical size
        /// </summary>
        [DataMember]
        public long PhysicalSize
        {
            get { return CalcPhysicalSize(); }
            private set { }
        }

        /// <summary>
        /// Summary info : The Physical capacity
        /// </summary>        
        [DataMember]
        public long PhysicalCapacity
        {
            get { return CalcPhysicalCapacity(); }
            private set { }
        }

        /// <summary>
        /// Resource buckets.
        /// </summary>
        [DataMember]
        public IDictionary<ResourceBucketType, ICollection<ResourceBucket>> Buckets { get; set; }

        /// <summary>
        /// All physcial buckets
        /// - NOT for serialisation
        /// </summary>
        public ICollection<ResourceBucket> PhysicalBuckets
        {
            get { return this.Buckets[ResourceBucketType.Physical]; }
        }

        /// <summary>
        /// All virtual buckets
        /// - NOT for serialisation
        /// </summary>
        public ICollection<ResourceBucket> VirtualBuckets
        {
            get { return this.Buckets[ResourceBucketType.Virtual]; }
        }

        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="resourceName"></param>
        /// <param name="sourceFilename"></param>
        /// <param name="destFilename"></param>
        /// <param name="filetype"></param>
        /// <param name="platform"></param>
        public ResourceStat(String resourceName, String sourceFilename, String destFilename,
            RSG.Platform.FileType filetype, RSG.Platform.Platform platform)
        {
            this.ResourceName = resourceName;
            this.SourceFilename = sourceFilename;
            this.DestinationFilename = destFilename;
            this.FileType = filetype;
            this.Platform = platform;
            this.Buckets = new Dictionary<ResourceBucketType, ICollection<ResourceBucket>>();

            // For all possible bucketTypes create list
            foreach (ResourceBucketType bucketType in Enum.GetValues(typeof(ResourceBucketType)))
            {
                this.Buckets[bucketType] = new List<ResourceBucket>();
            }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public ResourceStat(String resourceName, String sourceFilename, String destFilename, 
            RSG.Platform.FileType filetype, RSG.Platform.Platform platform, ICollection<ResourceBucket> buckets)
            : this(resourceName, sourceFilename, destFilename, filetype, platform)
        {
            // Now populate appropriate bucket dictionary
            foreach (ResourceBucket bucket in buckets)
            {
                this.Buckets[bucket.BucketType].Add(bucket);
            }    
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Serialise resource stat to XML element.
        /// </summary>
        /// <returns></returns>
        public XElement Serialise()
        {
            XElement xmlResource = new XElement("Resource",
                new XElement("Name", this.ResourceName),
                new XElement("SourceFilename", this.SourceFilename),
                new XElement("DestinationFilename", this.DestinationFilename),
                new XElement("FileType", this.FileType.ToString()),
                new XElement("Platform", this.Platform.ToString()),
                new XElement("VirtualSize", this.VirtualSize),
                new XElement("VirtualCapacity", this.VirtualCapacity),
                new XElement("PhysicalSize", this.PhysicalSize),
                new XElement("PhysicalCapacity", this.PhysicalCapacity),
                new XElement("PhysicalBuckets", this.PhysicalBuckets.Select(bucket => bucket.Serialise())),
                new XElement("VirtualBuckets", this.VirtualBuckets.Select(bucket => bucket.Serialise())));
            return (xmlResource);
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Physical size in bytes across all buckets.
        /// </summary>
        private long CalcPhysicalSize()
        {
            return (PhysicalBuckets.Select(stat => stat.Size).Sum(s => s));
        }

        /// <summary>
        /// Physical capacity in bytes across all buckets.
        /// </summary>
        private long CalcPhysicalCapacity()
        {
            return (PhysicalBuckets.Select(stat => stat.Capacity).Sum(s => s));
        }

        /// <summary>
        /// Physical mean loss across all buckets.
        /// </summary>
        private float CalcPhysicalMeanLoss()
        {
            return MeanLoss(CalcPhysicalSize(), CalcPhysicalCapacity());
        }

        /// <summary>
        /// Virtual size in bytes across all buckets.
        /// </summary>
        private long CalcVirtualSize()
        {
            return (VirtualBuckets.Select(stat => stat.Size).Sum(s => s));
        }

        /// <summary>
        /// Virtual capacity in bytes across all buckets.
        /// </summary>
        private long CalcVirtualCapacity()
        {
            return (VirtualBuckets.Select(stat => stat.Capacity).Sum(s => s));
        }

        /// <summary>
        /// Virtual mean loss across all buckets.
        /// </summary>
        private float CalcVirtualMeanLoss()
        {
            return MeanLoss(CalcVirtualSize(), CalcVirtualCapacity());
        }

        /// <summary>
        /// Helper: the loss expressed fractionaly over the capacity
        /// </summary>
        /// <param name="size"></param>
        /// <param name="capacity"></param>
        /// <returns></returns>
        private static float MeanLoss(long size, long capacity)
        {
            long loss = capacity - size;
            float meanLoss = ((float)loss / (float)capacity);
            return meanLoss;
        }

        #endregion // Private Methods
    }

} // RSG.Model.Statistics.Platform namespace
