﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Runtime.Serialization;

namespace RSG.Model.Statistics.Captures
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class ScriptMemResult
    {
        #region Properties
        /// <summary>
        /// Virtual memory stats.
        /// </summary>
        [DataMember]
        public MemoryStat Virtual { get; set; }

        /// <summary>
        /// Physical memory stats.
        /// </summary>
        [DataMember]
        public MemoryStat Physical { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ScriptMemResult()
        {
        }
        
        /// <summary>
        /// 
        /// </summary>
        public ScriptMemResult(XElement element)
        {
            // Extract the various stats
            XElement virtualElement = element.Element("virtualMem");
            if (virtualElement != null)
            {
                Virtual = GetMemoryStat(virtualElement);
            }

            XElement physicalElement = element.Element("physicalMem");
            if (physicalElement != null)
            {
                Physical = GetMemoryStat(physicalElement);
            }
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        private MemoryStat GetMemoryStat(XElement element)
        {
            uint min = 0;
            uint max = 0;
            uint average = 0;
            float std = 0;

            XElement minElement = element.Element("min");
            if (minElement != null && minElement.Attribute("value") != null)
            {
                min = UInt32.Parse(minElement.Attribute("value").Value);
            }

            XElement maxElement = element.Element("max");
            if (maxElement != null && maxElement.Attribute("value") != null)
            {
                max = UInt32.Parse(maxElement.Attribute("value").Value);
            }

            XElement avgElement = element.Element("average");
            if (avgElement != null && avgElement.Attribute("value") != null)
            {
                average = UInt32.Parse(avgElement.Attribute("value").Value);
            }

            XElement stdElement = element.Element("std");
            if (stdElement != null && stdElement.Attribute("value") != null)
            {
                std = Single.Parse(stdElement.Attribute("value").Value);
            }

            return new MemoryStat(min, max, average, std);
        }
        #endregion // Private Methods
    } // ScriptMemResult
}
