﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Math;

namespace RSG.Model.Common.Map
{
    public interface IChainingGraphNode
    {
        #region Properties

        /// <summary>
        /// 
        /// </summary>
        Vector3f Position { get; }

        /// <summary>
        /// 
        /// </summary>
        string AttachedEntity
        {
            get;
        }

        /// <summary>
        /// 
        /// </summary>
        string ScenarioType
        {
            get;
        }

        /// <summary>
        /// 
        /// </summary>
        bool HasIncomingEdges
        {
            get;
        }

        /// <summary>
        /// 
        /// </summary>
        bool HasOutgoingEdges
        {
            get;
        }
        #endregion // Properties
    }

    public interface IChainingGraphEdge
    {
        #region Properties
		/// <summary>
        /// Index of the 'from' node.
        /// </summary>
        int NodeIndexFrom
        {
            get;
        }

		/// <summary>
        /// Index of the 'to' node.
        /// </summary>
        int NodeIndexTo
        {
            get;
        }

		/// <summary>
        /// 
        /// </summary>
        string NavMode
        {
            get;
        }

		/// <summary>
        /// 
        /// </summary>
        string NavSpeed
        {
            get;
        }
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    public interface IChainingGraph
    {
        /// <summary>
        /// 
        /// </summary>
        string SourceFile { get; }

		/// <summary>
        /// Raw list of chained nodes.
        /// </summary>
        List<IChainingGraphNode> ChainedNodes
        {
            get;
        }

		/// <summary>
        /// Raw list of chained edges.
        /// </summary>
        List<IChainingGraphEdge> ChainedEdges
        {
            get;
        }

		/// <summary>
        /// Linked lists of all paths based on the node and edge data in the file.
        /// </summary>
        List<LinkedList<IChainingGraphNode>> ChainedPaths
        {
            get;
        }

    } // ISpawnPoint
}
