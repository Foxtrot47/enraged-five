﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Report;
using RSG.Base.Tasks;
using RSG.Model.Common;
using RSG.Base.Attributes;
using RSG.Model.Common.Map;
using System.IO;
using RSG.Base.ConfigParser;
using RSG.Model.Common.Extensions;
using RSG.Base.Configuration;
using RSG.Pipeline.Services.Platform;
using RSG.ManagedRage;

namespace RSG.Model.Report.Reports.Map
{
    /// <summary>
    /// Report that outputs HD split counts for all map sections.
    /// </summary>
    public class HdAssetSplitCountReport : CSVReport, IDynamicLevelReport
    {
        #region Constants
        private const String c_name = "HD Asset Split Count Report";
        private const String c_description = "Report that outputs HD split counts for all map sections.";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (m_task == null)
                {
                    CompositeTask task = new CompositeTask("Generate Report");
                    task.AddSubTask(new ActionTask("Gathering data", (context, progress) => GatherData((DynamicLevelReportContext)context, progress)), 50);
                    task.AddSubTask(new ActionTask("Writing report", (context, progress) => OutputReport((DynamicLevelReportContext)context, progress)));
                    m_task = task;
                }
                return m_task;
            }
        }
        private ITask m_task;

        /// <summary>
        /// List of stats that are required to generate this report.
        /// </summary>
        public IEnumerable<StreamableStat> RequiredStats
        {
            get { return null; }
        }

        /// <summary>
        /// List of the csv data to output for this report.
        /// </summary>
        private List<SectionCsvData> AllCsvData { get; set; }
        #endregion // Properties

        #region Private Class
        /// <summary>
        /// Helper class that contains information for a single row for the users summary csv file.
        /// </summary>
        private class SectionCsvData
        {
            [FriendlyName("Name")]
            public String Name { get; set; }

            [FriendlyName("Parent Area")]
            public String ParentArea { get; set; }

            [FriendlyName("Grandparent Area")]
            public String GrandParentArea { get; set; }

            public IDictionary<RSG.Platform.Platform, PlatformCsvData> PerPlatformData { get; set; }
        } // SectionCsvData

        /// <summary>
        /// Per platform csv data.
        /// </summary>
        private class PlatformCsvData
        {
            [FriendlyName("Hi Drawable Count")]
            public uint HiDrawableCount { get; set; }

            [FriendlyName("Hi Fragment Count")]
            public uint HiFragmentCount { get; set; }

            [FriendlyName("Hi TXD Count")]
            public uint HiTxdCount { get; set; }
        } // PlatformCsvData
        #endregion // Private Class
        
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public HdAssetSplitCountReport()
            : base(c_name, c_description)
        {
        }

        #endregion // Constructor(s)

        #region Task Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="progress"></param>
        private void GatherData(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            AllCsvData = new List<SectionCsvData>();

            IMapHierarchy hierarchy = context.Level.MapHierarchy;

            if (hierarchy != null)
            {
                // Gather the distinct list of processed file paths
                IDictionary<IMapSection, String> processedZipFilepaths = new Dictionary<IMapSection, String>();

                foreach (IMapSection section in hierarchy.AllSections)
                {
                    String processedZipPath = GetProcessedZipPaths(section, context.GameView);
                    if (!String.IsNullOrEmpty(processedZipPath))
                    {
                        processedZipFilepaths.Add(section, processedZipPath);
                    }
                }


                float increment = 1.0f / processedZipFilepaths.Count();

                foreach (KeyValuePair<IMapSection, String> pair in processedZipFilepaths)
                {
                    // Report progress
                    string message = String.Format("Processing {0}", pair.Key.Name);
                    progress.Report(new TaskProgress(increment, true, message));

                    // Generate the csv data for this section.
                    IMapArea parentArea = pair.Key.ParentArea;
                    IMapArea grandParentArea = (parentArea != null ? parentArea.ParentArea : null);

                    SectionCsvData csvData = new SectionCsvData();
                    csvData.Name = pair.Key.Name;
                    csvData.ParentArea = (parentArea != null ? parentArea.Name : "n/a");
                    csvData.GrandParentArea = (grandParentArea != null ? grandParentArea.Name : "n/a");
                    csvData.PerPlatformData = new Dictionary<RSG.Platform.Platform, PlatformCsvData>();

                    // Gather the rpf data
                    IDictionary<RSG.Platform.Platform, String> rpfPaths = GetRpfFilePathsForProcessedZipPath(pair.Value, context.Platforms);

                    foreach (KeyValuePair<RSG.Platform.Platform, String> rpfPair in rpfPaths)
                    {
                        Packfile rpfFile = new Packfile();
                        rpfFile.Load(rpfPair.Value);

                        csvData.PerPlatformData.Add(rpfPair.Key, GatherPlatformCsvData(rpfFile));

                        rpfFile.Close();
                    }

                    AllCsvData.Add(csvData);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        private String GetProcessedZipPaths(IMapSection section, ConfigGameView gv)
        {
            String processedZipPath = null;

            // Get the node that we actually want to process
            ContentNodeMap node = section.GetConfigMapNode(gv);

            foreach (ContentNode outputNode in node.Outputs)
            {
                if (!outputNode.Outputs.Any())
                {
                    ContentNodeMapZip nodeZip = outputNode as ContentNodeMapZip;
                    if (nodeZip != null)
                    {
                        processedZipPath = Path.GetFullPath(nodeZip.Filename);
                    }
                }
                else
                {
                    // Make sure this is the first input to the procesed zip.
                    ContentNodeMapProcessedZip processedZip = outputNode.Outputs.First() as ContentNodeMapProcessedZip;
                    if (processedZip != null && processedZip.Inputs.First() == outputNode)
                    {
                        processedZipPath = Path.GetFullPath(processedZip.Filename);
                    }
                }
            }

            return processedZipPath;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        private IDictionary<RSG.Platform.Platform, String> GetRpfFilePathsForProcessedZipPath(String processedZipPath, IPlatformCollection platforms)
        {
            // Generate the results.
            IDictionary<RSG.Platform.Platform, String> platformFilepaths = new Dictionary<RSG.Platform.Platform, String>();

            if (processedZipPath != null)
            {
                // Convert the independent file paths to rpf paths
                IConfig config = ConfigFactory.CreateConfig();

                foreach (RSG.Platform.Platform platform in platforms)
                {
                    ITarget target = config.Project.DefaultBranch.Targets[platform];
                    String platformFilepath = PlatformPathConversion.ConvertAndRemapFilenameToPlatform(target, processedZipPath);

                    if (platformFilepath != null)
                    {
                        platformFilepaths[platform] = platformFilepath;
                    }
                }
            }

            return platformFilepaths;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rpfFile"></param>
        private PlatformCsvData GatherPlatformCsvData(Packfile rpfFile)
        {
            PlatformCsvData csvData = new PlatformCsvData();

            foreach (PackEntry entry in rpfFile.Entries)
            {
                String fileNameNoExtension = Path.GetFileNameWithoutExtension(entry.Name);

                if (fileNameNoExtension.EndsWith("+hidr", StringComparison.CurrentCultureIgnoreCase))
                {
                    csvData.HiDrawableCount++;
                }
                else if (fileNameNoExtension.EndsWith("+hifr", StringComparison.CurrentCultureIgnoreCase))
                {
                    csvData.HiFragmentCount++;
                }
                else if (fileNameNoExtension.EndsWith("+hi", StringComparison.CurrentCultureIgnoreCase))
                {
                    csvData.HiTxdCount++;
                }
            }

            return csvData;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void OutputReport(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            using (StreamWriter writer = new StreamWriter(Filename))
            {
                WriteCsvHeader(writer, context.Platforms);

                foreach (SectionCsvData csvData in AllCsvData)
                {
                    WriteCsvLine(writer, csvData, context.Platforms);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="platforms"></param>
        private void WriteCsvHeader(StreamWriter writer, IPlatformCollection platforms)
        {
            String header = "Map Section,Parent Area,Grandparent Area";

            foreach (RSG.Platform.Platform platform in platforms)
            {
                header += String.Format(",{0} Hi Drawable Count,{0} Hi Fragment Count,{0} Hi TXD Count", platform);
            }

            writer.WriteLine(header);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="csvData"></param>
        /// <param name="platforms"></param>
        private void WriteCsvLine(StreamWriter writer, SectionCsvData csvData, IPlatformCollection platforms)
        {
            String line = String.Format("{0},{1},{2}", csvData.Name, csvData.ParentArea, csvData.GrandParentArea);

            foreach (RSG.Platform.Platform platform in platforms)
            {
                if (csvData.PerPlatformData.ContainsKey(platform))
                {
                    PlatformCsvData platformCsvData = csvData.PerPlatformData[platform];
                    line += String.Format(",{0},{1},{2}", platformCsvData.HiDrawableCount, platformCsvData.HiFragmentCount, platformCsvData.HiTxdCount);
                }
                else
                {
                    line += ",n/a,n/a,n/a";
                }
            }

            writer.WriteLine(line);
        }
        #endregion // Task Methods
    } // HdAssetSplitCountReport
}
