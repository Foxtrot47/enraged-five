﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.ConfigParser;
using RSG.Base.Tasks;

namespace RSG.Model.Report
{
    /// <summary>
    /// A report that generates it's content on the fly.
    /// </summary>
    public interface IDynamicReport : IReport
    {
        #region Properties
        /// <summary>
        /// Task to run for generating this report.
        /// </summary>
        ITask GenerationTask { get; }
        #endregion // Properties
    } // IDynamicReport
} // RSG.Model.Report namespace
