﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.Model.Report.Reports.GameStatsReports.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class CpuResult
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public string Name
        {
            get;
            private set;
        }

        public string Set
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public float Min
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public float Max
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public float Average
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public CpuResult(XElement element)
        {
            // Extract the various stats
            XElement nameElement = element.Element("name");
            if (nameElement != null)
            {
                Name = nameElement.Value;
            }

            XElement setElement = element.Element("set");
            if (setElement != null)
            {
                Set = setElement.Value;
            }

            XElement minElement = element.Element("min");
            if (minElement != null && minElement.Attribute("value") != null)
            {
                Min = Single.Parse(minElement.Attribute("value").Value);
            }

            XElement maxElement = element.Element("max");
            if (maxElement != null && maxElement.Attribute("value") != null)
            {
                Max = Single.Parse(maxElement.Attribute("value").Value);
            }

            XElement avgElement = element.Element("average");
            if (avgElement != null && avgElement.Attribute("value") != null)
            {
                Average = Single.Parse(avgElement.Attribute("value").Value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="average"></param>
        public CpuResult(string name, float average)
        {
            Name = name;
            Average = average;
        }
        #endregion // Constructor(s)
    } // CpuResult
}
