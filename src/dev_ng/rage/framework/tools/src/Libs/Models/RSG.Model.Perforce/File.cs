﻿using System;
using System.Collections.Generic;
using RSG.Model.Common;
using RSG.SourceControl.Perforce;

namespace RSG.Model.Perforce
{

    /// <summary>
    /// Perforce file object.
    /// </summary>
    public class File : FileAssetBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public Revision[] Revisions
        {
            get;
            protected set;
        }
        #endregion // Properties
    }

} // RSG.Model.Perforce namespace
