﻿using System;
using System.Collections.Generic;
using RSG.Model.Common.Weapon;

namespace RSG.Model.Common
{
    /// <summary>
    /// Interface for weapon assets
    /// </summary>
    public interface IWeapon : IAsset, IHasAssetChildren, IComparable<IWeapon>, IEquatable<IWeapon>, IStreamableObject
    {
        #region Properties
        /// <summary>
        /// Friendly name for the weapon (extracted from a localisation file).
        /// </summary>
        String FriendlyName { get; }

        /// <summary>
        /// Name of the model associated with this weapon.
        /// </summary>
        String ModelName { get; }

        /// <summary>
        /// Profile stat name associated with this weapon.
        /// </summary>
        String StatName { get; }

        /// <summary>
        /// Weapon category classification.
        /// </summary>
        WeaponCategory Category { get; }

        /// <summary>
        /// The collection this weapon is a part of
        /// </summary>
        IWeaponCollection ParentCollection { get; }

        /// <summary>
        /// Array of textures the weapon uses
        /// </summary>
        ITexture[] Textures { get; }

        /// <summary>
        /// Array of weapon shaders.
        /// </summary>
        IShader[] Shaders { get; }

        /// <summary>
        /// 
        /// </summary>
        int PolyCount { get; }

        /// <summary>
        /// 
        /// </summary>
        int CollisionCount { get; }

        /// <summary>
        /// 
        /// </summary>
        int BoneCount { get; }

        /// <summary>
        /// 
        /// </summary>
        bool HasLod { get; }

        /// <summary>
        /// 
        /// </summary>
        int LodPolyCount { get; }

        /// <summary>
        /// 
        /// </summary>
        int GripCount { get; }

        /// <summary>
        /// 
        /// </summary>
        int MagazineCount { get; }

        /// <summary>
        /// 
        /// </summary>
        int AttachmentCount { get; }

        /// <summary>
        /// 
        /// </summary>
        IDictionary<RSG.Platform.Platform, IWeaponPlatformStat> PlatformStats { get; }
        #endregion // Properties
    } // IWeapon

    /// <summary>
    /// Interface for weapon based platform stats
    /// </summary>
    public interface IWeaponPlatformStat
    {
        /// <summary>
        /// Platform for this stat
        /// </summary>
        RSG.Platform.Platform Platform { get; }

        /// <summary>
        /// PLysical size of the weapon
        /// </summary>
        uint PhysicalSize { get; }

        /// <summary>
        /// Virtual size of the weapon
        /// </summary>
        uint VirtualSize { get; }
    } // IWeaponPlatformStat
} // RSG.Model.Common
