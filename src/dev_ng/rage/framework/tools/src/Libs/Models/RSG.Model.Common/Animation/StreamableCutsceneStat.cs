﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common.Animation
{
    /// <summary>
    /// Enumeration of all statistics that a cutscene can have associated with it.
    /// </summary>
    public static class StreamableCutsceneStat
    {
        public const String ExportZipFilepathString = "194C248E-50B6-4BCF-980A-AD92B9FD7BE4";
        public const String IsConcatString = "DEFD7039-46CF-427A-A0D7-0C28656FE077";
        public const String HasBranchString = "2EEF6514-C227-498D-B37E-CB0C79E5EBB2";
        public const String DurationString = "A592C8F2-10E5-4A2C-9A8D-3BFB51A1DC7C";
        public const String PlatformStatsString = "C1B5D740-C1F5-444B-A59A-D4B316A98A97";

        public static readonly StreamableStat ExportZipFilepath = new StreamableStat(ExportZipFilepathString, false);
        public static readonly StreamableStat IsConcat = new StreamableStat(IsConcatString, false);
        public static readonly StreamableStat HasBranch = new StreamableStat(HasBranchString, false);
        public static readonly StreamableStat Duration = new StreamableStat(DurationString, false);
        public static readonly StreamableStat PlatformStats = new StreamableStat(PlatformStatsString, false);
    } // StreamableCutsceneStat
}
