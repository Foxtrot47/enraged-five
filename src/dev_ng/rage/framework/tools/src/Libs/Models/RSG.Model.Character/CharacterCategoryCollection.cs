﻿using System;
using RSG.Model.Common;
using RSG.Base.Collections;
using System.Collections.Generic;
using System.Collections;

namespace RSG.Model.Character
{
    /// <summary>
    /// Character category (e.g. component, player).
    /// </summary>
    public class CharacterCategoryCollection : AssetBase, ICharacterCategoryCollection
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<ICharacter> Characters
        {
            get;
            protected set;
        }

        /// <summary>
        /// 
        /// </summary>
        public CharacterCategory Category
        {
            get;
            protected set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="name"></param>
        public CharacterCategoryCollection(CharacterCategory category)
            : base(category.ToString())
        {
            Characters = new ObservableCollection<ICharacter>();
        }
        #endregion // Constructor(s)

        #region IEnumerable<ICharacter> Implementation
        /// <summary>
        /// Loops through the characters and returns each one in turn
        /// </summary>
        IEnumerator<ICharacter> IEnumerable<ICharacter>.GetEnumerator()
        {
            foreach (ICharacter character in Characters)
            {
                yield return character;
            }
        }

        /// <summary>
        /// Returns the Enumerator for this object
        /// </summary>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerator)this);
        }
        #endregion // IEnumerable<ICharacter> Implementation

        #region ICollection<ICharacter> Implementation
        #region ICollection<ICharacter> Properties
        /// <summary>
        /// Returns whether the collection is read-only
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Return number of items in the set.
        /// </summary>
        public int Count
        {
            get
            {
                return Characters.Count;
            }
        }
        #endregion // ICollection<ICharacter> Properties

        #region ICollection<ICharacter> Methods
        /// <summary>
        /// Add a character to the collection.
        /// </summary>
        /// <param name="item"></param>
        public void Add(ICharacter item)
        {
            Characters.Add(item);
        }

        /// <summary>
        /// Removes all characters from the collection.
        /// </summary>
        public void Clear()
        {
            Characters.Clear();
        }

        /// <summary>
        /// Determine whether the collection contains a specific character
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Contains(ICharacter item)
        {
            return Characters.Contains(item);
        }

        /// <summary>
        /// Copy items in the collection to an array, starting at a particular index.
        /// </summary>
        /// <param name="output"></param>
        /// <param name="index"></param>
        public void CopyTo(ICharacter[] output, int index)
        {
            Characters.CopyTo(output, index);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        public bool Remove(ICharacter item)
        {
            return Characters.Remove(item);
        }
        #endregion // ICollection<ICharacter> Methods
        #endregion // ICollection<ICharacter> Implementation
    }
} // RSG.Model.Character
