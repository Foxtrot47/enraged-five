﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.Base.Collections;
using System.Collections;

namespace RSG.Model.Asset.Level
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class LevelCollectionBase : AssetContainerBase, ILevelCollection
    {
        #region Properties
        /// <summary>
        /// The collection of levels that are currently
        /// loaded in this dictionary
        /// </summary>
        public ObservableCollection<ILevel> Levels
        {
            get;
            protected set;
        }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public LevelCollectionBase()
            : base("Levels")
        {
            Levels = new ObservableCollection<ILevel>();
        }
        #endregion // Constructor(s)

        #region ILevelCollection Methods
        /// <summary>
        /// 
        /// </summary>
        public void LoadAllStats()
        {
            LoadStats(StreamableStatUtils.GetValuesAssignedToProperties(typeof(Level)));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="statsToLoad"></param>
        public abstract void LoadStats(IEnumerable<StreamableStat> statsToLoad);
        #endregion // IWeaponCollection Methods

        #region IEnumerable<ILevel> Implementation
        /// <summary>
        /// Loops through the levels and returns each one in turn
        /// </summary>
        IEnumerator<ILevel> IEnumerable<ILevel>.GetEnumerator()
        {
            foreach (ILevel level in this.Levels)
            {
                yield return level;
            }
        }

        /// <summary>
        /// Returns the Enumerator for this object
        /// </summary>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerator)this);
        }
        #endregion // IEnumerable<ILevel> Implementation

        #region ICollection<ILevel> Implementation
        #region ICollection<ILevel> Properties
        /// <summary>
        /// Returns whether the collection is read-only
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Return number of items in the set.
        /// </summary>
        public int Count
        {
            get
            {
                return Levels.Count;
            }
        }
        #endregion // ICollection<ILevel> Properties

        #region ICollection<ILevel> Methods
        /// <summary>
        /// Add a level to the collection.
        /// </summary>
        /// <param name="item"></param>
        public void Add(ILevel item)
        {
            Levels.Add(item);
        }

        /// <summary>
        /// Removes all levels from the collection.
        /// </summary>
        public void Clear()
        {
            Levels.Clear();
        }

        /// <summary>
        /// Determine whether the collection contains a specific level
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Contains(ILevel item)
        {
            return Levels.Contains(item);
        }

        /// <summary>
        /// Copy items in the collection to an array, starting at a particular index.
        /// </summary>
        /// <param name="output"></param>
        /// <param name="index"></param>
        public void CopyTo(ILevel[] output, int index)
        {
            Levels.CopyTo(output, index);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        public bool Remove(ILevel item)
        {
            return Levels.Remove(item);
        }
        #endregion // ICollection<ILevel> Methods
        #endregion // ICollection<ILevel> Implementation

        #region IDisposable Implementation
        /// <summary>
        /// 
        /// </summary>
        public virtual void Dispose()
        {
            foreach (ILevel level in Levels)
            {
                level.Dispose();
            }
        }
        #endregion // IDisposable Implementation
    } // LevelCollection
}
