﻿using System;
using RSG.Base.Editor;

namespace RSG.Model.Map.ViewModel
{
    /// <summary>
    /// The interface that all map section view model
    /// objects implement so that a factory can be used to create
    /// them
    /// </summary>
    public interface IMapSectionViewModel : IViewModel
    {
        String Name { get; }
        MapSection Model { get; }
    }

} // RSG.Model.Map.ViewModel namespace
