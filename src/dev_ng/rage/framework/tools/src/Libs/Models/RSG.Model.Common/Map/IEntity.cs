﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Math;
using RSG.SceneXml;

namespace RSG.Model.Common.Map
{
    /// <summary>
    /// 
    /// </summary>
    public interface IEntity : IMapObjectWithTransform, IStreamableObject
    {
        #region Properties
        /// <summary>
        /// Parent object (a section, interior or room).
        /// </summary>
        IHasEntityChildren Parent { get; }

        /// <summary>
        /// Referenced archetype for this entity (can be null).
        /// </summary>
        IArchetype ReferencedArchetype { get; }

        /// <summary>
        /// Section that this entity appears in.
        /// </summary>
        IMapSection ContainingSection { get; }

        /// <summary>
        /// World space bounding box.
        /// </summary>
        BoundingBox3f BoundingBox { get; }

        /// <summary>
        /// 
        /// </summary>
        int PriorityLevel { get; }

        /// <summary>
        /// 
        /// </summary>
        bool IsLowPriority { get; }

        /// <summary>
        /// 
        /// </summary>
        bool IsReference { get; }

        /// <summary>
        /// Flag indicating whether this is an interior reference.
        /// </summary>
        bool IsInteriorReference { get; }

        /// <summary>
        /// Lod level that this entity is at.
        /// </summary>
        LodLevel LodLevel { get; }

        /// <summary>
        /// Lod distance (returns either the overridden distance or the referenced archetypes lod distance).
        /// </summary>
        float LodDistance { get; }

        /// <summary>
        /// Lod distance override for this entity.
        /// </summary>
        float? LodDistanceOverride { get; }

        /// <summary>
        /// Lod distance automatically assigned to all of its children.
        /// </summary>
        float? LodDistanceForChildren { get; }

        /// <summary>
        /// Parent entity in the lod hierarchy.
        /// </summary>
        IEntity LodParent { get; set; }

        /// <summary>
        /// Child entities in the lod hierarchy.
        /// </summary>
        ICollection<IEntity> LodChildren { get; }

        /// <summary>
        /// AttrNames.OBJ_FORCE_BAKE_COLLISION attribute
        /// </summary>
        bool ForceBakeCollision { get; }

        /// <summary>
        /// List of spawn points associated with this entity.
        /// </summary>
        ICollection<ISpawnPoint> SpawnPoints { get; }

        /// <summary>
        /// Gets the guid set on this entities attributes.
        /// </summary>
        Guid AttributeGuid { get; }

        /// <summary>
        /// Gets a value indicating whether this entity has had scaling applied to it.
        /// </summary>
        bool HasScaling { get; }

        /// <summary>
        /// Gets a value indicating whether this entity has a link (via proxy) to SLOD2.
        /// </summary>
        bool HasSLOD2Link { get; set;  }

        /// <summary>
        /// Gets the number of Rage light instances this entity has underneath it.
        /// </summary>
        int RageLightInstanceCount { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Return the streaming extents of this Entity object.
        /// The streaming extents is defined by the LOD distance of this 
        /// instance; and depends on whether its in a LOD hierarchy or not.
        /// If we have a LOD parent then its relative to the parent's pivot.
        /// </summary>
        /// <returns></returns>
        BoundingBox3f GetStreamingExtents();
        #endregion // Methods
    } // IEntity
}
