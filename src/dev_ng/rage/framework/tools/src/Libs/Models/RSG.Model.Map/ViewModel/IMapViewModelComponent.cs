﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Collections;

namespace RSG.Model.Map.ViewModel
{
    public interface IMapViewModelComponent
    {
        String Name { get; }

        /// <summary>
        /// The user data that can be attached to this view model
        /// </summary>
        UserData ViewModelUserData { get; set; }

    }
}
