﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.IO;
using RSG.Base.Collections;
using RSG.Model.Common.Map;
using RSG.Model.Common;

namespace RSG.Model.Map3
{

    /// <summary>
    /// 
    /// </summary>
    public class MapArea : MapNode, IMapArea
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public ObservableCollection<IMapNode> ChildNodes
        {
            get;
            protected set;
        }

        /// <summary>
        /// Returns all sections in this map hierarchy
        /// </summary>
        [Browsable(false)]
        public IEnumerable<IMapSection> AllDescendentSections
        {
            get
            {
                return GetSectionEnumerator(ChildNodes);
            }
        }

        /// <summary>
        /// Map area export data path.
        /// </summary>
        public String ExportDataPath
        {
            get;
            protected set;
        }

        /// <summary>
        /// Map area processed data path.
        /// </summary>
        public String ProcessedDataPath
        {
            get;
            protected set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor (root map area).
        /// </summary>
        public MapArea(string name, IMapHierarchy hierarchy)
            : base(name, hierarchy)
        {
            ChildNodes = new ObservableCollection<IMapNode>();
            this.ExportDataPath = Path.Combine(this.Level.ExportDataPath, this.Name);
            this.ProcessedDataPath = Path.Combine(this.Level.ProcessedDataPath, this.Name);
        }

        /// <summary>
        /// Constructor (map area with parent).
        /// </summary>
        public MapArea(string name, IMapArea parent)
            : base(name, parent)
        {
            ChildNodes = new ObservableCollection<IMapNode>();
            this.ExportDataPath = Path.Combine(parent.ExportDataPath, this.Name);
            this.ProcessedDataPath = Path.Combine(parent.ProcessedDataPath, this.Name);
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// Recursively traverses the tree hierarchy to retrieve all the sections
        /// </summary>
        /// <returns></returns>
        private IEnumerable<IMapSection> GetSectionEnumerator(IEnumerable<IMapNode> nodes)
        {
            foreach (IMapNode node in nodes)
            {
                if (node is IMapSection)
                {
                    yield return (IMapSection)node;
                }
                else if (node is IMapArea)
                {
                    IMapArea area = node as IMapArea;
                    foreach (IMapSection childSection in GetSectionEnumerator(area.ChildNodes))
                    {
                        yield return childSection;
                    }
                }
            }
        }
        #endregion // Private Methods
    } // MapArea

} // RSG.Model.Map3 namespace
