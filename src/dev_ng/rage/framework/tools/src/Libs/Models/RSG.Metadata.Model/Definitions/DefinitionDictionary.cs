﻿//---------------------------------------------------------------------------------------------
// <copyright file="DefinitionDictionary.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Threading.Tasks;
    using System.Xml;
    using RSG.Base;
    using RSG.Base.Extensions;
    using RSG.Base.Logging;
    using RSG.Editor;
    using RSG.Editor.Model;
    using RSG.Metadata.Model.Converters;
    using RSG.Metadata.Model.Helpers;
    using RSG.Metadata.Model.Resources;

    /// <summary>
    /// Represents a dictionary of parCodeGen definitions that can be used to open and
    /// manipulate metadata files. This class cannot be inherited.
    /// </summary>
    public sealed class DefinitionDictionary : IDefinitionDictionary
    {
        #region Fields
        /// <summary>
        /// The name of the xml attribute that contains the data for a constant name.
        /// </summary>
        private const string XmlConstNameAttr = "name";

        /// <summary>
        /// The name of the xml node containing the data used to initialise a constant.
        /// </summary>
        private const string XmlConstNodeName = "const";

        /// <summary>
        /// The name of the xml attribute that contains the data for a constant value.
        /// </summary>
        private const string XmlConstValueAttr = "value";

        /// <summary>
        /// The name of the xml node containing the data used to initialise a enumeration
        /// definition.
        /// </summary>
        private const string XmlEnumerationNodeName = "enumdef";

        /// <summary>
        /// The name of the xml node containing the data used to initialise a structure
        /// definition.
        /// </summary>
        private const string XmlStructureNodeName = "structdef";

        /// <summary>
        /// The private field used for the <see cref="Constants"/> property.
        /// </summary>
        private ModelCollection<IConstant> _constants;

        /// <summary>
        /// The private field used for the <see cref="Converters"/> property.
        /// </summary>
        private ConverterCollection _converters;

        /// <summary>
        /// The private lookup dictionary to quickly lookup a enumeration by the name.
        /// </summary>
        private Dictionary<string, IEnumeration> _enumerationLookup;

        /// <summary>
        /// The private field used for the <see cref="Enumerations"/> property.
        /// </summary>
        private ModelCollection<IEnumeration> _enumerations;

        /// <summary>
        /// The private field used for the <see cref="Constants"/> property.
        /// </summary>
        private ReadOnlyModelCollection<IConstant> _readOnlyConstants;

        /// <summary>
        /// The private field used for the <see cref="Enumerations"/> property.
        /// </summary>
        private ReadOnlyModelCollection<IEnumeration> _readOnlyEnumerations;

        /// <summary>
        /// The private field used for the <see cref="Structures"/> property.
        /// </summary>
        private ReadOnlyModelCollection<IStructure> _readOnlyStructures;

        /// <summary>
        /// The private lookup dictionary to quickly lookup a structure by the name.
        /// </summary>
        private Dictionary<string, IStructure> _structureLookup;

        /// <summary>
        /// The private field used for the <see cref="Structures"/> property.
        /// </summary>
        private ModelCollection<IStructure> _structures;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DefinitionDictionary"/> class.
        /// </summary>
        public DefinitionDictionary()
        {
            this._structureLookup = new Dictionary<string, IStructure>();
            this._enumerationLookup = new Dictionary<string, IEnumeration>();
            this._structures = new ModelCollection<IStructure>();
            this._enumerations = new ModelCollection<IEnumeration>();
            this._constants = new ModelCollection<IConstant>();
            this._converters = new ConverterCollection();

            this._readOnlyEnumerations = this._enumerations.ToReadOnly();
            this._readOnlyStructures = this._structures.ToReadOnly();
            this._readOnlyConstants = this._constants.ToReadOnly();
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the read-only collection of loaded constants for this dictionary.
        /// </summary>
        public ReadOnlyModelCollection<IConstant> Constants
        {
            get { return this._readOnlyConstants; }
        }

        /// <summary>
        /// Gets the read-only collection of loaded enumerations for this dictionary.
        /// </summary>
        public ReadOnlyModelCollection<IEnumeration> Enumerations
        {
            get { return this._readOnlyEnumerations; }
        }

        /// <summary>
        /// Gets the read-only collection of loaded structures for this dictionary.
        /// </summary>
        public ReadOnlyModelCollection<IStructure> Structures
        {
            get { return this._readOnlyStructures; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Removes all structures and enumerations from this dictionary.
        /// </summary>
        public void Clear()
        {
            this._structures.Clear();
            this._enumerations.Clear();
            this._constants.Clear();
            this._converters.Constants.Clear();
        }

        /// <summary>
        /// Determines whether this dictionary contains a constant definition with the
        /// specified name.
        /// </summary>
        /// <param name="name">
        /// The name of the constant to locate in the dictionary.
        /// </param>
        /// <returns>
        /// True if this dictionary contains a constant with the specified name; otherwise,
        /// false.
        /// </returns>
        public bool ContainsConstant(string name)
        {
            foreach (IConstant c in this._constants)
            {
                if (String.Equals(name, c.Name))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Determines whether this dictionary contains a enumeration definition with the
        /// specified data type.
        /// </summary>
        /// <param name="type">
        /// The data type of the enumeration to locate in the dictionary.
        /// </param>
        /// <returns>
        /// True if this dictionary contains an enumeration with the specified data type;
        /// otherwise, false.
        /// </returns>
        public bool ContainsEnumeration(string type)
        {
            foreach (IEnumeration e in this._enumerations)
            {
                if (e.IsReferencedBy(type))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Determines whether this dictionary contains a structure definition with the
        /// specified data type.
        /// </summary>
        /// <param name="name">
        /// The data type of the structure to locate in the dictionary.
        /// </param>
        /// <returns>
        /// True if this dictionary contains an structure with the specified data type;
        /// otherwise, false.
        /// </returns>
        public bool ContainsStructure(string name)
        {
            foreach (IStructure s in this._structures)
            {
                if (s.IsReferencedBy(name))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Create a new constant and adds it to this dictionary before returning a reference
        /// to it.
        /// </summary>
        /// <param name="name">
        /// The name of the new constant.
        /// </param>
        /// <param name="value">
        /// The value of the new constant.
        /// </param>
        /// <returns>
        /// A reference to the newly created constant.
        /// </returns>
        public IConstant CreateAndAddConstant(string name, string value)
        {
            IConstant newConstant = new Constant(name, value, this);
            this._constants.Add(newConstant);
            this._converters.Constants.Add(name, value, NumericalConstantType.Double);
            return newConstant;
        }

        /// <summary>
        /// Create a new enumeration and adds it to this dictionary before returning a
        /// reference to it.
        /// </summary>
        /// <param name="dataType">
        /// The full data type of the new enumeration.
        /// </param>
        /// <returns>
        /// A reference to the newly created enumeration.
        /// </returns>
        public IEnumeration CreateAndAddEnumeration(string dataType)
        {
            IEnumeration newEnumeration = new Enumeration(dataType, this);
            this._enumerations.Add(newEnumeration);
            return newEnumeration;
        }

        /// <summary>
        /// Create a new structure and adds it to this dictionary before returning a reference
        /// to it.
        /// </summary>
        /// <param name="dataType">
        /// The full data type of the new structure.
        /// </param>
        /// <returns>
        /// A reference to the newly created structure.
        /// </returns>
        public IStructure CreateAndAddStructure(string dataType)
        {
            IStructure newStructure = new Structure(dataType, this);
            this._structures.Add(newStructure);
            return newStructure;
        }

        /// <summary>
        /// Gets the first occurrence of a loaded constant with the specified name if found;
        /// otherwise null.
        /// </summary>
        /// <param name="name">
        /// The name of the constant to locate.
        /// </param>
        /// <returns>
        /// The first occurrence of a loaded constant with the specified name if found;
        /// otherwise null.
        /// </returns>
        public IConstant GetConstant(string name)
        {
            foreach (IConstant c in this._constants)
            {
                if (String.Equals(name, c.Name))
                {
                    return c;
                }
            }

            return null;
        }

        /// <summary>
        /// Attempts to get the constant with the specified name and returns a value indicating
        /// whether or not it was successful.
        /// </summary>
        /// <param name="name">
        /// The name of the constant to get.
        /// </param>
        /// <param name="constant">
        /// When this method returns, contains the constant with the specified name, if the
        /// constant is found; otherwise, null.
        /// </param>
        /// <returns>
        /// True if the dictionary contains a constant with the specified name; otherwise,
        /// false.
        /// </returns>
        public bool GetConstant(string name, out IConstant constant)
        {
            foreach (IConstant c in this._constants)
            {
                if (String.Equals(name, c.Name))
                {
                    constant = c;
                    return true;
                }
            }

            constant = null;
            return false;
        }

        /// <summary>
        /// Gets all of the constants that have been loaded from the file with the specified
        /// full path.
        /// </summary>
        /// <param name="fullPath">
        /// The full path of the file whose constants should be retrieved.
        /// </param>
        /// <returns>
        /// A read-only list of constants that have been loaded from the file with the
        /// specified full path.
        /// </returns>
        public IReadOnlyList<IConstant> GetConstantsFromFile(string fullPath)
        {
            lock (this._constants)
            {
                List<IConstant> constants = new List<IConstant>();
                foreach (IConstant c in this._constants)
                {
                    if (String.Equals(c.Filename, fullPath))
                    {
                        constants.Add(c);
                    }
                }

                return constants;
            }
        }

        /// <summary>
        /// Gets the first occurrence of a loaded enumeration with the specified data type if
        /// found; otherwise null.
        /// </summary>
        /// <param name="type">
        /// The data type of the enumeration to locate.
        /// </param>
        /// <returns>
        /// The first occurrence of a loaded enumeration with the specified data type if
        /// found; otherwise null.
        /// </returns>
        public IEnumeration GetEnumeration(string type)
        {
            string lookupName = type.TrimStart(':');
            IEnumeration enumeration = null;
            if (this._enumerationLookup.TryGetValue(lookupName, out enumeration))
            {
                return enumeration;
            }

            return null;
        }

        /// <summary>
        /// Attempts to get the enumeration with the specified data type and returns a value
        /// indicating whether or not it was successful.
        /// </summary>
        /// <param name="type">
        /// The data type of the enumeration to get.
        /// </param>
        /// <param name="enumeration">
        /// When this method returns, contains the enumeration with the specified data type,
        /// if the enumeration is found; otherwise, null.
        /// </param>
        /// <returns>
        /// True if the dictionary contains a enumeration with the specified data type;
        /// otherwise, false.
        /// </returns>
        public bool GetEnumeration(string type, out IEnumeration enumeration)
        {
            string lookupName = type.TrimStart(':');
            if (this._enumerationLookup.TryGetValue(lookupName, out enumeration))
            {
                return true;
            }

            enumeration = null;
            return false;
        }

        /// <summary>
        /// Gets all of the enumerations that have been loaded from the file with the specified
        /// full path.
        /// </summary>
        /// <param name="fullPath">
        /// The full path of the file whose enumerations should be retrieved.
        /// </param>
        /// <returns>
        /// A read-only list of enumerations that have been loaded from the file with the
        /// specified full path.
        /// </returns>
        public IReadOnlyList<IEnumeration> GetEnumerationsFromFile(string fullPath)
        {
            lock (this._enumerations)
            {
                List<IEnumeration> enumerations = new List<IEnumeration>();
                foreach (IEnumeration e in this._enumerations)
                {
                    if (String.Equals(e.Filename, fullPath))
                    {
                        enumerations.Add(e);
                    }
                }

                return enumerations;
            }
        }

        /// <summary>
        /// Gets all of the enumerations that have been loaded that are located in the
        /// specified scope.
        /// </summary>
        /// <param name="scope">
        /// The scope whose enumerations should be retrieved.
        /// </param>
        /// <returns>
        /// A read-only list of enumerations that have been loaded that are located in the
        /// specified scope.
        /// </returns>
        public IReadOnlyList<IEnumeration> GetEnumerationsFromScope(string scope)
        {
            List<IEnumeration> enumerations = new List<IEnumeration>();
            if (String.IsNullOrWhiteSpace(scope))
            {
                scope = String.Empty;
            }

            lock (this._structures)
            {
                foreach (IEnumeration enumeration in this._enumerations)
                {
                    string enumerationScope = enumeration.Scope;
                    if (!String.Equals(enumerationScope, scope))
                    {
                        continue;
                    }

                    enumerations.Add(enumeration);
                }
            }

            return enumerations;
        }

        /// <summary>
        /// Gets all of the valid namespaces for the loaded definitions.
        /// </summary>
        /// <returns>
        /// A read-only list of namespaces for the loaded definitions.
        /// </returns>
        public IReadOnlyList<string> GetNamespaces()
        {
            HashSet<string> namespaces = new HashSet<string>();
            lock (this._structures)
            {
                foreach (IStructure structure in this._structures)
                {
                    string structureScope = structure.Scope;
                    if (String.IsNullOrWhiteSpace(structureScope))
                    {
                        continue;
                    }

                    namespaces.Add(structureScope);
                }
            }

            lock (this._enumerations)
            {
                foreach (IEnumeration enumeration in this._enumerations)
                {
                    string enumerationScope = enumeration.Scope;
                    if (String.IsNullOrWhiteSpace(enumerationScope))
                    {
                        continue;
                    }

                    namespaces.Add(enumerationScope);
                }
            }

            return new List<string>(namespaces);
        }

        /// <summary>
        /// Gets the first occurrence of a loaded structure with the specified name if found;
        /// otherwise null.
        /// </summary>
        /// <param name="name">
        /// The name of the structure to locate.
        /// </param>
        /// <returns>
        /// The first occurrence of a loaded structure with the specified name if found;
        /// otherwise null.
        /// </returns>
        public IStructure GetStructure(string name)
        {
            string lookupName = name.TrimStart(':');
            IStructure structure = null;
            if (this._structureLookup.TryGetValue(lookupName, out structure))
            {
                return structure;
            }

            return null;
        }

        /// <summary>
        /// Attempts to get the structure with the specified data type and returns a value
        /// indicating whether or not it was successful.
        /// </summary>
        /// <param name="name">
        /// The data type of the structure to get.
        /// </param>
        /// <param name="structure">
        /// When this method returns, contains the structure with the specified data type, if
        /// the structure is found; otherwise, null.
        /// </param>
        /// <returns>
        /// True if the dictionary contains a structure with the specified data type;
        /// otherwise, false.
        /// </returns>
        public bool GetStructure(string name, out IStructure structure)
        {
            string lookupName = name.TrimStart(':');
            if (this._structureLookup.TryGetValue(lookupName, out structure))
            {
                return true;
            }

            structure = null;
            return false;
        }

        /// <summary>
        /// Gets all of the structures that have been loaded from the file with the specified
        /// full path.
        /// </summary>
        /// <param name="fullPath">
        /// The full path of the file whose structures should be retrieved.
        /// </param>
        /// <returns>
        /// A read-only list of structures that have been loaded from the file with the
        /// specified full path.
        /// </returns>
        public IReadOnlyList<IStructure> GetStructuresFromFile(string fullPath)
        {
            lock (this._structures)
            {
                List<IStructure> structures = new List<IStructure>();
                foreach (IStructure s in this._structures)
                {
                    if (String.Equals(s.Filename, fullPath))
                    {
                        structures.Add(s);
                    }
                }

                return structures;
            }
        }

        /// <summary>
        /// Gets all of the structures that have been loaded that are located in the specified
        /// scope.
        /// </summary>
        /// <param name="scope">
        /// The scope whose structures should be retrieved.
        /// </param>
        /// <returns>
        /// A read-only list of structures that have been loaded that are located in the
        /// specified scope.
        /// </returns>
        public IReadOnlyList<IStructure> GetStructuresFromScope(string scope)
        {
            List<IStructure> structures = new List<IStructure>();
            if (String.IsNullOrWhiteSpace(scope))
            {
                scope = String.Empty;
            }

            lock (this._structures)
            {
                foreach (IStructure structure in this._structures)
                {
                    string structureScope = structure.Scope;
                    if (!String.Equals(structureScope, scope))
                    {
                        continue;
                    }

                    structures.Add(structure);
                }
            }

            return structures;
        }

        /// <summary>
        /// Determines whether the specified structure can be instanced inside a metadata file
        /// without any errors being produced.
        /// </summary>
        /// <param name="structure">
        /// The structure to test.
        /// </param>
        /// <param name="log">
        /// The log object that will receives any errors or warning found while validating.
        /// </param>
        /// <returns>
        /// True if the specified structure can be instanced; otherwise false. If false the
        /// specified log object will have received the reasons why not.
        /// </returns>
        public bool IsStructureValidForInstancing(IStructure structure, ILog log)
        {
            if (structure == null)
            {
                return false;
            }

            int count = 0;
            foreach (IStructure s in this._structures)
            {
                if (!String.Equals(s.Name, structure.Name))
                {
                    continue;
                }

                count++;
                if (count > 1)
                {
                    return false;
                }
            }

            if (count == 0)
            {
                return false;
            }

            return structure.IsValidForInstancing(log);
        }

        /// <summary>
        /// Determines whether the specified data type can be instanced inside a metadata file
        /// without any errors being produced.
        /// </summary>
        /// <param name="structureType">
        /// The data type of the structure to test.
        /// </param>
        /// <param name="log">
        /// The log object that will receives any errors or warning found while validating.
        /// </param>
        /// <returns>
        /// True if the specified data type can be instanced; otherwise false. If false the
        /// specified log object will have received the reasons why not.
        /// </returns>
        public bool IsStructureValidForInstancing(string structureType, ILog log)
        {
            foreach (IStructure structure in this._structures)
            {
                if (!String.Equals(structure.Name, structureType))
                {
                    continue;
                }

                return this.IsStructureValidForInstancing(structure, log);
            }

            return false;
        }

        /// <summary>
        /// Loads the given file.
        /// </summary>
        /// <param name="filename">
        /// The path to the file to load.
        /// </param>
        public void LoadFile(string filename)
        {
            try
            {
                XmlReaderSettings settings = new XmlReaderSettings();
                settings.IgnoreComments = false;
                using (XmlReader reader = XmlReader.Create(filename, settings))
                {
                    while (reader.MoveToContent() == XmlNodeType.Element)
                    {
                        if (String.Equals(reader.Name, "ParserSchema"))
                        {
                            this.CreateDefinitions(reader);
                        }
                        else
                        {
                            reader.Skip();
                        }
                    }
                }
            }
            catch
            {
            }
        }

        /// <summary>
        /// Gets a task that represents the work needing to be done to load the specified file
        /// into this dictionary.
        /// </summary>
        /// <param name="filename">
        /// The path to the file to load.
        /// </param>
        /// <returns>
        /// A task that represents the work needing to be done to load the specified file.
        /// </returns>
        public Task LoadFileAsync(string filename)
        {
            return Task.Factory.StartNew(delegate { this.LoadFile(filename); });
        }

        /// <summary>
        /// Removes the first occurrence of the specified constant from this dictionary.
        /// </summary>
        /// <param name="constant">
        /// The constant to remove.
        /// </param>
        public void RemoveConstant(IConstant constant)
        {
            this._constants.Remove(constant);
            this._converters.Constants.Remove(constant.Name, NumericalConstantType.Double);
        }

        /// <summary>
        /// Removes the first occurrence of the specified enumeration from this dictionary.
        /// </summary>
        /// <param name="enumeration">
        /// The enumeration to remove.
        /// </param>
        public void RemoveEnumeration(IEnumeration enumeration)
        {
            this._enumerations.Remove(enumeration);

            string basename = enumeration.DataType;
            string countName = basename + "_NUM_ENUMS";
            string minName = basename + "_MAX_VALUE";
            string maxName = basename + "_MIN_VALUE";

            this._converters.Constants.Remove(countName, NumericalConstantType.Double);
            this._converters.Constants.Remove(minName, NumericalConstantType.Double);
            this._converters.Constants.Remove(maxName, NumericalConstantType.Double);
        }

        /// <summary>
        /// Removes the first occurrence of the specified structure from this dictionary.
        /// </summary>
        /// <param name="structure">
        /// The structure to remove.
        /// </param>
        public void RemoveStructure(IStructure structure)
        {
            this._structures.Remove(structure);
        }

        /// <summary>
        /// Converts the specified <paramref name="s"/> parameter to an instance of the type
        /// specified by the type parameter.
        /// </summary>
        /// <typeparam name="T">
        /// The type that the specified string object will be converted to.
        /// </typeparam>
        /// <param name="s">
        /// The string instance to convert.
        /// </param>
        /// <param name="fallback">
        /// The value that will be returned if the conversion fails.
        /// </param>
        /// <returns>
        /// The converted value if the conversion is successful; otherwise, the specified
        /// <paramref name="fallback"/> value.
        /// </returns>
        public T To<T>(string s, T fallback)
        {
            return s.To<T>(fallback, this._converters);
        }

        /// <summary>
        /// Converts the specified <paramref name="s"/> parameter to an array of values of the
        /// specified type.
        /// </summary>
        /// <typeparam name="T">
        /// The type that the specified string object will be converted to.
        /// </typeparam>
        /// <param name="s">
        /// The string instance to convert.
        /// </param>
        /// <param name="count">
        /// The number of values the <paramref name="s"/> parameter should be converted to.
        /// </param>
        /// <param name="fallback">
        /// The value that will be returned if the conversion fails.
        /// </param>
        /// <returns>
        /// The converted value if the conversion is successful; otherwise, the specified
        /// <paramref name="fallback"/> value.
        /// </returns>
        public T[] To<T>(string s, int count, T fallback)
        {
            return s.To<T>(count, fallback, this._converters);
        }

        /// <summary>
        /// Attempts to convert the specified <paramref name="s"/> parameter to an instance of
        /// the type specified by the type parameter.
        /// </summary>
        /// <typeparam name="T">
        /// The type that the specified string object will be converted to.
        /// </typeparam>
        /// <param name="s">
        /// The string instance to convert.
        /// </param>
        /// <param name="fallback">
        /// The value that will be returned if the conversion fails.
        /// </param>
        /// <returns>
        /// A structure containing the converted value and a value indicating whether the
        /// conversion was successful.
        /// </returns>
        public TryResult<T> TryTo<T>(string s, T fallback)
        {
            return s.TryTo<T>(fallback, this._converters);
        }

        /// <summary>
        /// Attempts to convert the specified <paramref name="s"/> parameter to an array of
        /// values of the specified type.
        /// </summary>
        /// <typeparam name="T">
        /// The type that the specified string object will be converted to.
        /// </typeparam>
        /// <param name="s">
        /// The string instance to convert.
        /// </param>
        /// <param name="count">
        /// The number of values the <paramref name="s"/> parameter should be converted to.
        /// </param>
        /// <param name="fallback">
        /// The value that will be returned if the conversion fails.
        /// </param>
        /// <returns>
        /// A structure containing the converted value and a value indicating whether the
        /// conversion was successful.
        /// </returns>
        public TryResult<T[]> TryTo<T>(string s, int count, T fallback)
        {
            return s.TryTo<T>(count, fallback, this._converters);
        }

        /// <summary>
        /// Validates this entity and creates a validation result object containing the errors
        /// and warnings.
        /// </summary>
        /// <param name="recursive">
        /// A value indicating whether any child models to this one should also be validated.
        /// </param>
        /// <returns>
        /// A validation result object containing all of the errors and warnings associated
        /// with this validation pass.
        /// </returns>
        public ValidationResult Validate(bool recursive)
        {
            ValidationResult result = new ValidationResult();
            HashSet<string> structureDataTypes = new HashSet<string>();
            foreach (IStructure structure in this._structures)
            {
                if (structure == null)
                {
                    continue;
                }

                string dataType = structure.DataType;
                if (dataType == null)
                {
                    continue;
                }

                if (!structureDataTypes.Contains(dataType))
                {
                    structureDataTypes.Add(dataType);
                    continue;
                }

                string structureNamespace = structure.Namespace;
                if (String.IsNullOrWhiteSpace(structureNamespace))
                {
                    structureNamespace = StringTable.GlobalNamespace;
                }

                string msg = StringTable.DuplicateStructureError;
                msg = msg.FormatCurrent(structureNamespace, structure.ShortDataType);
                result.AddError(msg, structure.Location);
            }

            HashSet<string> enumerationDataTypes = new HashSet<string>();
            foreach (IEnumeration enumeration in this._enumerations)
            {
                if (enumeration == null)
                {
                    continue;
                }

                string dataType = enumeration.DataType;
                if (dataType == null)
                {
                    continue;
                }

                if (!enumerationDataTypes.Contains(dataType))
                {
                    enumerationDataTypes.Add(dataType);
                    continue;
                }

                string enumerationNamespace = enumeration.Namespace;
                if (String.IsNullOrWhiteSpace(enumerationNamespace))
                {
                    enumerationNamespace = StringTable.GlobalNamespace;
                }

                string msg = StringTable.DuplicateEnumerationError;
                msg = msg.FormatCurrent(enumerationNamespace, enumeration.ShortDataType);
                result.AddError(msg, enumeration.Location);
            }

            HashSet<string> constantNames = new HashSet<string>();
            foreach (IConstant constant in this._constants)
            {
                if (constant == null)
                {
                    continue;
                }

                string name = constant.Name;
                if (name == null)
                {
                    continue;
                }

                if (!constantNames.Contains(name))
                {
                    constantNames.Add(name);
                    continue;
                }

                string msg = StringTable.DuplicateConstantError;
                msg = msg.FormatCurrent(name);
                result.AddError(msg, constant.Location);
            }

            if (recursive)
            {
                foreach (IStructure structure in this._structures)
                {
                    result.AddChildResults(structure.Validate(true));
                }

                foreach (IEnumeration enumeration in this._enumerations)
                {
                    result.AddChildResults(enumeration.Validate(true));
                }

                foreach (IConstant constant in this._constants)
                {
                    result.AddChildResults(constant.Validate(true));
                }
            }

            return result;
        }

        /// <summary>
        /// Validates this entity and adds the errors and warnings to the specified log object.
        /// </summary>
        /// <param name="log">
        /// The log object the errors and warnings for this entity should be added.
        /// </param>
        /// <param name="recursive">
        /// A value indicating whether any child models to this one should also be validated.
        /// </param>
        public void Validate(ILog log, bool recursive)
        {
            if (log == null)
            {
                Debug.Assert(log != null, "Unable to validate into a null log");
                return;
            }

            ValidationResult result = this.Validate(recursive);
            if (result == null)
            {
                Debug.Assert(result != null, "Unable to validate from a null result object.");
                return;
            }

            this.AddValidationResultToLog(result, log);
        }

        /// <summary>
        /// Determines whether the specified string object can be successfully converted to a
        /// instance of the specified type.
        /// </summary>
        /// <typeparam name="T">
        /// The type that this string will be tested against.
        /// </typeparam>
        /// <param name="s">
        /// The string instance to test.
        /// </param>
        /// <param name="treatNullAsValid">
        /// A value indicating whether a null value is handled as a success.
        /// </param>
        /// <returns>
        /// True if the specified string can be successfully converted to the specified type;
        /// otherwise, false.
        /// </returns>
        public bool Validate<T>(string s, bool treatNullAsValid)
        {
            return s.Validate<T>(treatNullAsValid, this._converters);
        }

        /// <summary>
        /// Determines whether this string object can be successfully converted to an array of
        /// of the specified type of the specified count.
        /// </summary>
        /// <typeparam name="T">
        /// The type that this string will be tested against.
        /// </typeparam>
        /// <param name="s">
        /// The string instance to test.
        /// </param>
        /// <param name="count">
        /// The number of instances this string should represent.
        /// </param>
        /// <param name="treatNullAsValid">
        /// A value indicating whether a null value is handled as a success.
        /// </param>
        /// <returns>
        /// True if this string can be successfully converted to the specified number of the
        /// specified type; otherwise, false.
        /// </returns>
        public bool Validate<T>(string s, int count, bool treatNullAsValid)
        {
            return s.Validate<T>(count, treatNullAsValid, this._converters);
        }

        /// <summary>
        /// Adds the errors and warnings that are currently inside the specified results object
        /// into the specified log object.
        /// </summary>
        /// <param name="result">
        /// The validation results object containing all of the errors and warnings.
        /// </param>
        /// <param name="log">
        /// The log object to add the errors and warning to.
        /// </param>
        private void AddValidationResultToLog(ValidationResult result, ILog log)
        {
            string format = "{0} {1}({2},{3})";
            foreach (IValidationError error in result.Errors)
            {
                string msg = null;
                if (error.HasLocationInformation)
                {
                    FileLocation location = error.Location;
                    msg = format.FormatCurrent(
                        error.Message, location.FullPath, location.Line, location.Column);
                }
                else
                {
                    msg = error.Message;
                }

                log.Error(msg);
            }

            foreach (IValidationWarning warning in result.Warnings)
            {
                string msg = null;
                if (warning.HasLocationInformation)
                {
                    FileLocation location = warning.Location;
                    msg = format.FormatCurrent(
                        warning.Message, location.FullPath, location.Line, location.Column);
                }
                else
                {
                    msg = warning.Message;
                }

                log.Warning(msg);
            }

            foreach (ValidationResult childResult in result.ChildResults)
            {
                this.AddValidationResultToLog(childResult, log);
            }
        }

        /// <summary>
        /// Creates definitions using the data provided by the specified System.Xml.XmlReader.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to load a set
        /// of definitions.
        /// </param>
        private void CreateDefinitions(XmlReader reader)
        {
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            NumericalConstantType type = NumericalConstantType.Double;
            reader.ReadStartElement();
            while (reader.MoveToElementOrComment())
            {
                if (reader.NodeType == XmlNodeType.Comment)
                {
                    reader.Skip();
                    continue;
                }

                if (String.Equals(reader.Name, XmlStructureNodeName))
                {
                    IStructure structure = new Structure(reader, this);
                    lock (this._structures)
                    {
                        this._structures.Add(structure);
                        string name = structure.Name;
                        if (!this._structureLookup.ContainsKey(name))
                        {
                            this._structureLookup.Add(name, structure);
                        }

                        string dataType = structure.DataType;
                        if (!String.Equals(dataType, name))
                        {
                            if (!this._structureLookup.ContainsKey(dataType))
                            {
                                this._structureLookup.Add(dataType, structure);
                            }
                        }
                    }
                }
                else if (String.Equals(reader.Name, XmlEnumerationNodeName))
                {
                    IEnumeration enumeration = new Enumeration(reader, this);
                    lock (this._enumerations)
                    {
                        this._enumerations.Add(enumeration);
                        string dataType = enumeration.DataType;
                        if (!this._enumerationLookup.ContainsKey(dataType))
                        {
                            this._enumerationLookup.Add(dataType, enumeration);
                        }
                    }

                    lock (this._constants)
                    {
                        string basename = enumeration.DataType;
                        string count = enumeration.EnumConstants.Count.ToStringInvariant();
                        long min = long.MaxValue;
                        long max = long.MinValue;

                        foreach (IEnumConstant constant in enumeration.EnumConstants)
                        {
                            long value = constant.Value;
                            min = Math.Min(min, value);
                            max = Math.Max(max, value);
                        }

                        string countName = basename + "_NUM_ENUMS";
                        string minName = basename + "_MAX_VALUE";
                        string maxName = basename + "_MIN_VALUE";

                        this._converters.Constants.Add(countName, count, type);
                        this._converters.Constants.Add(minName, min.ToStringInvariant(), type);
                        this._converters.Constants.Add(maxName, max.ToStringInvariant(), type);
                    }
                }
                else if (String.Equals(reader.Name, XmlConstNodeName))
                {
                    IConstant constant = new Constant(reader, this);
                    lock (this._constants)
                    {
                        this._constants.Add(constant);
                        string name = constant.Name;
                        string value = constant.Value;
                        this._converters.Constants.Add(name, value, type);
                    }
                }
                else
                {
                    reader.Skip();
                }
            }

            if (reader.NodeType == XmlNodeType.EndElement)
            {
                reader.ReadEndElement();
            }
        }
        #endregion Methods
    } // RSG.Metadata.Model.Definitions.DefinitionDictionary {Class}
} // RSG.Metadata.Model.Definitions {Namespace}
