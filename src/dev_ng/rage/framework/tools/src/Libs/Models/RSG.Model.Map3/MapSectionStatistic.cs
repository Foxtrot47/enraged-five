﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common.Map;
using RSG.Statistics.Common.Dto.GameAssetStats;
using RSG.Statistics.Common.Dto.GameAssetStats.Bundles;
using RSG.Bounds;
using RSG.Model.Common;

namespace RSG.Model.Map3
{
    /// <summary>
    /// 
    /// </summary>
    public class MapSectionStatistic : IMapSectionStatistic
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public uint TotalCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public uint UniqueCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public uint DrawableCost { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public uint TXDCost { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public uint TXDCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public uint ShaderCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public uint TextureCount { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public uint PolygonCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public uint CollisionCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IDictionary<CollisionType, uint> CollisionTypePolygonCounts { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public float LodDistance { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public MapSectionStatistic()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="group"></param>
        /// <param name="lodLevel"></param>
        /// <param name="entityType"></param>
        public MapSectionStatistic(IMapSection section, StatGroup group, StatLodLevel lodLevel, StatEntityType entityType)
        {
            CollisionTypePolygonCounts = new Dictionary<CollisionType, uint>();

            if (group == StatGroup.Archetypes && section.ExportArchetypes == true)
            {
                GatherArchetypeStatistics(new List<IMapSection> { section }, entityType);
            }
            else if (section.ExportEntities == true)
            {
                GatherEntityStatistics(new List<IMapSection> { section }, group, lodLevel, entityType);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="group"></param>
        /// <param name="lodLevel"></param>
        /// <param name="entityType"></param>
        public MapSectionStatistic(IList<IMapSection> sections, StatGroup group, StatLodLevel lodLevel, StatEntityType entityType)
        {
            CollisionTypePolygonCounts = new Dictionary<CollisionType, uint>();

            if (group == StatGroup.Archetypes)
            {
                GatherArchetypeStatistics(sections, entityType);
            }
            else
            {
                GatherEntityStatistics(sections, group, lodLevel, entityType);
            }
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// Returns the combined polygon count for a particular collision type.
        /// E.g. If Mover is passed in this would return things like Mover, Mover+Weapon, Mover+etc...
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public uint GetPolyCountForCollisionType(CollisionType type)
        {
            uint value = 0;
            foreach (KeyValuePair<CollisionType, uint> pair in CollisionTypePolygonCounts)
            {
                if ((pair.Key & type) != 0)
                {
                    value += pair.Value;
                }
            }
            return value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public void LoadStatsFromDatabase(MapSectionAggregateStatDto dto)
        {
            TotalCount = dto.TotalCount;
            UniqueCount = dto.UniqueCount;
            DrawableCost = dto.DrawableCost;
            TXDCost = dto.TXDCost;
            TXDCount = dto.TXDCount;
            ShaderCount = dto.ShaderCount;
            TextureCount = dto.TextureCount;
            PolygonCount = dto.PolygonCount;
            CollisionCount = dto.CollisionCount;
            LodDistance = dto.LodDistance;

            CollisionTypePolygonCounts = new Dictionary<CollisionType, uint>();
            foreach (CollisionPolyStatDto collisionDto in dto.CollisionTypePolygonCounts)
            {
                CollisionTypePolygonCounts.Add((CollisionType)collisionDto.CollisionFlags, collisionDto.PolygonCount);
            }
        }
        #endregion // Public Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="entityType"></param>
        /// <returns></returns>
        private void GatherArchetypeStatistics(IList<IMapSection> sections, StatEntityType entityType)
        {
            GatherEntityTypeStatistics(sections.SelectMany(item => item.Archetypes), entityType);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="group"></param>
        /// <param name="lodLevel"></param>
        /// <param name="entityType"></param>
        /// <returns></returns>
        private void GatherEntityStatistics(IList<IMapSection> sections, StatGroup group, StatLodLevel lodLevel, StatEntityType entityType)
        {
            IEnumerable<IEntity> entities = null;

            if (group == StatGroup.InteriorEntities)
            {
                entities = sections.SelectMany(item => item.ChildEntities).Where(ent => ent.ReferencedArchetype is IInteriorArchetype);
            }
            else
            {
                IEnumerable<IArchetype> allArchetypes = sections.SelectMany(item => item.Archetypes);

                // Restrict the entities to non interior ones
                IEnumerable<IEntity> nonInteriorEntities = sections.SelectMany(item => item.ChildEntities).Where(ent => ent.ReferencedArchetype != null && !(ent.ReferencedArchetype is IInteriorArchetype));
                IEnumerable<IEntity> allLodLevelEntities = null;

                // Restrict the entities further by taking the type into account
                if (group == StatGroup.DrawableEntities)
                {
                    allLodLevelEntities = nonInteriorEntities.Where(ent => allArchetypes.Contains(ent.ReferencedArchetype));
                }
                else if (group == StatGroup.NonDrawableEntities)
                {
                    allLodLevelEntities = nonInteriorEntities.Where(ent => !allArchetypes.Contains(ent.ReferencedArchetype));
                }
                else // if (group == StatGroup.AllNonInteriorEntities)
                {
                    allLodLevelEntities = nonInteriorEntities;
                }

                // And take the lod level into account
                switch (lodLevel)
                {
                    case StatLodLevel.Hd:
                        entities = allLodLevelEntities.Where(ent => ent.LodLevel == LodLevel.Hd || ent.LodLevel == LodLevel.OrphanHd);
                        break;

                    case StatLodLevel.Lod:
                        entities = allLodLevelEntities.Where(ent => ent.LodLevel == LodLevel.Lod || ent.LodLevel == LodLevel.SLod2);
                        break;

                    case StatLodLevel.SLod1:
                        entities = allLodLevelEntities.Where(ent => ent.LodLevel == LodLevel.SLod1 || ent.LodLevel == LodLevel.SLod3);
                        break;

                    case StatLodLevel.Total:
                        entities = allLodLevelEntities;
                        break;

                    default:
                        throw new ArgumentException(String.Format("Stats requested for an unsupported lod level '{0}'", lodLevel));
                }
            }

            // Finally gather the stats for all the referenced archetypes
            IEnumerable<IMapArchetype> archetypes = entities.Select(ent => ent.ReferencedArchetype as IMapArchetype);
            GatherEntityTypeStatistics(archetypes, entityType);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entityType"></param>
        /// <returns></returns>
        private void GatherEntityTypeStatistics(IEnumerable<IMapArchetype> allArchetypes, StatEntityType entityType)
        {
            // Get the list of archetypes we should be working with
            IEnumerable<IMapArchetype> archetypes = null;

            switch (entityType)
            {
                case StatEntityType.Dynamic:
                    archetypes = allArchetypes.Where(item => (item is ISimpleMapArchetype) && ((ISimpleMapArchetype)item).IsDynamic == true);
                    break; ;
                case StatEntityType.NonDynamic:
                    archetypes = allArchetypes.Where(item => !(item is ISimpleMapArchetype) || ((ISimpleMapArchetype)item).IsDynamic == false);
                    break;
                case StatEntityType.Total:
                    archetypes = allArchetypes;
                    break;
                default:
                    throw new ArgumentException(String.Format("Stats requested for an unsupported entity type '{0}'", entityType));
            }

            // Get the distinct list of them too
            IEnumerable<IMapArchetype> uniqueArchetypes = archetypes.Distinct();

            // Start compiling the stats
            TotalCount = (uint)archetypes.Count();
            UniqueCount = (uint)uniqueArchetypes.Count();

            // Get the list of unique txds
            IEnumerable<string> uniqueTxds = uniqueArchetypes.SelectMany(arch => arch.TxdExportSizes.Keys).Distinct(new TXDNameComparer());
            TXDCount = (uint)uniqueTxds.Count();

            // Shader and texture counts next
            IEnumerable<IShader> uniqueShaders = uniqueArchetypes.SelectMany(arch => arch.Shaders).Distinct();
            IEnumerable<ITexture> uniqueTextures = uniqueShaders.SelectMany(shader => shader.Textures).Distinct();

            ShaderCount = (uint)uniqueShaders.Count();
            TextureCount = (uint)uniqueTextures.Count();

            // Various other counts
            foreach (IMapArchetype archetype in archetypes)
            {
                PolygonCount += (uint)archetype.PolygonCount;
                CollisionCount += (uint)archetype.CollisionPolygonCount;

                // Collision type flags
                foreach (KeyValuePair<CollisionType, uint> pair in archetype.CollisionTypePolygonCounts)
                {
                    if (!CollisionTypePolygonCounts.ContainsKey(pair.Key))
                    {
                        CollisionTypePolygonCounts.Add(pair.Key, 0);
                    }
                    CollisionTypePolygonCounts[pair.Key] += pair.Value;
                }
            }

            // TXD and drawable costs are calculated per unique archetypes
            ISet<string> txdsEncountered = new HashSet<string>();
            foreach (IMapArchetype archetype in uniqueArchetypes)
            {
                DrawableCost += (uint)archetype.ExportGeometrySize;

                foreach (KeyValuePair<string, uint> txdPair in archetype.TxdExportSizes)
                {
                    if (!txdsEncountered.Contains(txdPair.Key))
                    {
                        TXDCost += txdPair.Value;
                        txdsEncountered.Add(txdPair.Key);
                    }
                }
            }

            // And finally the LOD distance
            if (archetypes.Count() > 0)
            {
                LodDistance = archetypes.Select(arch => (float)arch.LodDistance).Max();
            }
            else
            {
                LodDistance = 0;
            }
        }
        #endregion // Private Methods
    } // MapSectionStatistic



    /// <summary>
    /// Helper class for comparing txd names in a case insensitive manner
    /// </summary>
    internal class TXDNameComparer : IEqualityComparer<string>
    {
        // Summary:
        //     Determines whether the specified objects are equal.
        //
        // Parameters:
        //   x:
        //     The first object of type T to compare.
        //
        //   y:
        //     The second object of type T to compare.
        //
        // Returns:
        //     true if the specified objects are equal; otherwise, false.
        public bool Equals(string x, string y)
        {
            if (string.Compare(x.ToLower(), y.ToLower()) == 0)
                return true;
            return false;
        }

        //
        // Summary:
        //     Returns a hash code for the specified object.
        //
        // Parameters:
        //   obj:
        //     The System.Object for which a hash code is to be returned.
        //
        // Returns:
        //     A hash code for the specified object.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     The type of obj is a reference type and obj is null.
        public int GetHashCode(string obj)
        {
            return obj.ToLower().GetHashCode();
        }
    } // TXDNameComparer
}
