﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Map.SceneOverride
{
    public class DCCEntityKey
    {
        public DCCEntityKey(string mapContentName, string containerAttributeGuid, string nodeAttributeGuid, string archetypeName)
        {
            MapContentName = mapContentName;
            ContainerAttributeGuid = containerAttributeGuid;
            NodeAttributeGuid = nodeAttributeGuid;
            ArchetypeName = archetypeName;
        }

        public string MapContentName { get; private set; }
        public string ContainerAttributeGuid { get; private set; }
        public string NodeAttributeGuid { get; private set; }
        public string ArchetypeName { get; private set; }
    }
}
