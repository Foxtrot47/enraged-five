﻿//---------------------------------------------------------------------------------------------
// <copyright file="IConverter{T}.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Converters
{
    using RSG.Base;

    /// <summary>
    /// When implemented represents a converter that converts a string instance to the type
    /// specified by the type parameter.
    /// </summary>
    /// <typeparam name="T">
    /// The type that this converter can convert a string instance to.
    /// </typeparam>
    public interface IConverter<T>
    {
        #region Methods
        /// <summary>
        /// Converts the specified <paramref name="value"/> parameter to an instance of the
        /// type specified by the type parameter.
        /// </summary>
        /// <param name="value">
        /// The string instance to convert.
        /// </param>
        /// <param name="fallback">
        /// The value to return if an error occurs during the conversion or if the conversion
        /// isn't possible.
        /// </param>
        /// <param name="methods">
        /// The conversion methods that can be used during this conversion.
        /// </param>
        /// <returns>
        /// The converted value if the conversion is successful; otherwise, the specified
        /// <paramref name="fallback"/> value.
        /// </returns>
        T Convert(string value, T fallback, Methods methods);

        /// <summary>
        /// Converts the specified <paramref name="value"/> parameter to an array of instances
        /// of the type specified by the type parameter.
        /// </summary>
        /// <param name="value">
        /// The string instance to convert.
        /// </param>
        /// <param name="count">
        /// The number of values the specified <paramref name="value"/> should be converted
        /// into.
        /// </param>
        /// <param name="fallback">
        /// The value that should be inserted into the returned array if the conversion fails.
        /// </param>
        /// <param name="methods">
        /// The conversion methods that can be used during this conversion.
        /// </param>
        /// <returns>
        /// The converted array of values if the conversation is successful; otherwise, an
        /// array of the specified count with the specified <paramref name="fallback"/> value
        /// in place of any unconverted values.
        /// </returns>
        T[] Convert(string value, int count, T fallback, Methods methods);

        /// <summary>
        /// Attempts to convert the specified <paramref name="value"/> parameter to an instance
        /// of the type specified by the type parameter.
        /// </summary>
        /// <param name="value">
        /// The string instance to convert.
        /// </param>
        /// <param name="fallback">
        /// The value to return if an error occurs during the conversion or if the conversion
        /// isn't possible.
        /// </param>
        /// <param name="methods">
        /// The conversion methods that can be used during this conversion.
        /// </param>
        /// <returns>
        /// A structure containing the converted value and a boolean value indicating whether
        /// the conversion was successful or not.
        /// </returns>
        TryResult<T> TryConvert(string value, T fallback, Methods methods);

        /// <summary>
        /// Attempts to convert the specified <paramref name="value"/> parameter to an array
        /// of instances of the type specified by the type parameter.
        /// </summary>
        /// <param name="value">
        /// The string instance to convert.
        /// </param>
        /// <param name="count">
        /// The number of values the specified <paramref name="value"/> should be converted
        /// into.
        /// </param>
        /// <param name="fallback">
        /// The value that should be inserted into the returned array if the conversion fails.
        /// </param>
        /// <param name="methods">
        /// The conversion methods that can be used during this conversion.
        /// </param>
        /// <returns>
        /// A structure containing the converted value and a boolean value indicating whether
        /// the conversion was successful or not.
        /// </returns>
        TryResult<T[]> TryConvert(string value, int count, T fallback, Methods methods);
        #endregion Methods
    } // RSG.Metadata.Model.Converters.IConverter{T} {Interface}
} // RSG.Metadata.Model.Converters {Namespace}
