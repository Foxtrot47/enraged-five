﻿using System;
using System.Collections.Generic;
using RSG.Base.Collections;
using RSG.Model.Common;

namespace RSG.Model.Report
{
    /// <summary>
    /// Report Category Model Interface
    /// </summary>
    public interface IReportCategory :
        IReport,
        IHasAssetChildren
    {
        /// <summary>
        /// List of reports that make up this category
        /// </summary>
        IEnumerable<IReport> Reports { get; }
    } // IReportCategory
} // RSG.Model.Report namespace
