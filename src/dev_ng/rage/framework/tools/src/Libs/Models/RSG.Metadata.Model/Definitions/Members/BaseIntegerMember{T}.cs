﻿// --------------------------------------------------------------------------------------------
// <copyright file="BaseIntegerMember{T}.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions.Members
{
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using RSG.Base;
    using RSG.Base.Extensions;
    using RSG.Editor;
    using RSG.Editor.Model;
    using RSG.Metadata.Model.Resources;
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// Provides a base class to a integer member node in the parCodeGen system that can be
    /// instanced in a metadata file.
    /// </summary>
    /// <typeparam name="T">
    /// The value type for the integer value.
    /// </typeparam>
    public abstract class BaseIntegerMember<T> :
        MemberBase,
        IHasInitialValue<T>,
        IHasRange<T>,
        IHasStep<T>,
        IScalarMember where T : IComparable
    {
        #region Fields
        /// <summary>
        /// The string representation of the <see cref="IntegerType.Colour"/> constant.
        /// </summary>
        private const string IntegerTypeColour = "color";

        /// <summary>
        /// The name of the xml attribute containing the data for the
        /// <see cref="InitialValue"/> property.
        /// </summary>
        private const string XmlInitialAttr = "init";

        /// <summary>
        /// The name of the xml attribute containing the data for the <see cref="Maximum"/>
        /// property.
        /// </summary>
        private const string XmlMaxAttr = "max";

        /// <summary>
        /// The name of the xml attribute containing the data for the <see cref="Minimum"/>
        /// property.
        /// </summary>
        private const string XmlMinAttr = "min";

        /// <summary>
        /// The name of the xml attribute containing the data for the <see cref="Step"/>
        /// property.
        /// </summary>
        private const string XmlStepAttr = "step";

        /// <summary>
        /// The name of the xml attribute containing the data for the <see cref="Type"/>
        /// property.
        /// </summary>
        private const string XmlTypeAttr = "type";

        /// <summary>
        /// The private field used for the <see cref="InitialValue"/> property.
        /// </summary>
        private string _initialValue;

        /// <summary>
        /// The private field used for the <see cref="Maximum"/> property.
        /// </summary>
        private string _maximum;

        /// <summary>
        /// The private field used for the <see cref="Minimum"/> property.
        /// </summary>
        private string _minimum;

        /// <summary>
        /// The private field used for the <see cref="Step"/> property.
        /// </summary>
        private string _step;

        /// <summary>
        /// The private field used for the <see cref="Type"/> property.
        /// </summary>
        private string _type;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="BaseIntegerMember{T}"/> class to be
        /// one of the members of the specified structure.
        /// </summary>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        public BaseIntegerMember(IStructure structure)
            : base(structure)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="BaseIntegerMember{T}"/> class as a
        /// copy of the specified instance.
        /// </summary>
        /// <param name="other">
        /// The instance to copy.
        /// </param>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        public BaseIntegerMember(BaseIntegerMember<T> other, IStructure structure)
            : base(other, structure)
        {
            this._initialValue = other._initialValue;
            this._minimum = other._minimum;
            this._maximum = other._maximum;
            this._step = other._step;
            this._type = other._type;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="BaseIntegerMember{T}"/> class.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        public BaseIntegerMember(XmlReader reader, IStructure structure)
            : base(reader, structure)
        {
            this.Deserialise(reader);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the initial value that any instance of this member is set to.
        /// </summary>
        public T InitialValue
        {
            get { return this.Dictionary.To<T>(this._initialValue, this.DefaultInitialValue); }
            set { this.SetProperty(ref this._initialValue, value.ToString()); }
        }

        /// <summary>
        /// Gets or sets the maximum value that a instance of this member can be set to.
        /// </summary>
        public T Maximum
        {
            get { return this.Dictionary.To<T>(this._maximum, this.DefaultMaximumValue); }
            set { this.SetProperty(ref this._maximum, value.ToString()); }
        }

        /// <summary>
        /// Gets or sets the minimum value that a instance of this member can be set to.
        /// </summary>
        public T Minimum
        {
            get { return this.Dictionary.To<T>(this._minimum, this.DefaultMinimumValue); }
            set { this.SetProperty(ref this._minimum, value.ToString()); }
        }

        /// <summary>
        /// Gets a value indicating whether this integer member represents a colour value.
        /// </summary>
        public virtual bool RepresentsColourValue
        {
            get { return false; }
        }

        /// <summary>
        /// Gets the number of string components separated by whitespaces the scalar value
        /// contains.
        /// </summary>
        public int ScalarComponentCount
        {
            get { return 1; }
        }

        /// <summary>
        /// Gets a string that represents the content type of this scalar member.
        /// </summary>
        public abstract string ScalarContent { get; }

        /// <summary>
        /// Gets or sets the increase and decrease amount for a single step.
        /// </summary>
        public T Step
        {
            get { return this.Dictionary.To<T>(this._step, this.DefaultStepValue); }
            set { this.SetProperty(ref this._step, value.ToString()); }
        }

        /// <summary>
        /// Gets or sets the type of integer this member represents in the c++ code.
        /// </summary>
        public IntegerType Type
        {
            get { return this.GetTypeFromString(this._type); }
            set { this.SetProperty(ref this._type, this.GetStringFromType(value)); }
        }

        /// <summary>
        /// Gets the type name for this member.
        /// </summary>
        public override string TypeName
        {
            get { return "integer"; }
        }

        /// <summary>
        /// Gets the default initial value for the integer type.
        /// </summary>
        protected abstract T DefaultInitialValue { get; }

        /// <summary>
        /// Gets the default maximum value for the integer type.
        /// </summary>
        protected abstract T DefaultMaximumValue { get; }

        /// <summary>
        /// Gets the default minimum value for the integer type.
        /// </summary>
        protected abstract T DefaultMinimumValue { get; }

        /// <summary>
        /// Gets the default step value for the integer type.
        /// </summary>
        protected abstract T DefaultStepValue { get; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a <see cref="ITunable"/> object that represents a instance of this member
        /// using the specified string components to initialise it.
        /// </summary>
        /// <param name="components">
        /// The string components that provides the data used to initialise the new tunable.
        /// </param>
        /// <param name="parent">
        /// The tunable parent the new tunable will belong to.
        /// </param>
        /// <returns>
        /// A parCodeGen tunable that represents a instance of this member.
        /// </returns>
        public virtual ITunable CreateTunable(List<string> components, ITunableParent parent)
        {
            return null;
        }

        /// <summary>
        /// Writes the xml representation of this instance onto the specified
        /// System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that the xml representation of this instance should be
        /// written to.
        /// </param>
        public override void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                throw new SmartArgumentNullException(() => writer);
            }

            this.SerialiseBaseAttributes(writer);
            if (this._initialValue != null)
            {
                writer.WriteAttributeString(XmlInitialAttr, this._initialValue);
            }

            if (this._minimum != null)
            {
                writer.WriteAttributeString(XmlMinAttr, this._minimum);
            }

            if (this._maximum != null)
            {
                writer.WriteAttributeString(XmlMaxAttr, this._maximum);
            }

            if (this._step != null)
            {
                writer.WriteAttributeString(XmlStepAttr, this._step);
            }

            if (this._type != null)
            {
                writer.WriteAttributeString(XmlTypeAttr, this._type);
            }
        }

        /// <summary>
        /// Validates this entity and creates a validation result object containing the errors
        /// and warnings.
        /// </summary>
        /// <param name="recursive">
        /// A value indicating whether any child models to this one should also be validated.
        /// </param>
        /// <returns>
        /// A validation result object containing all of the errors and warnings associated
        /// with this validation pass.
        /// </returns>
        public override ValidationResult Validate(bool recursive)
        {
            ValidationResult result = new ValidationResult();
            this.ValidateBaseProperties(result);

            if (!this.Dictionary.Validate<T>(this._minimum, true))
            {
                string msg = StringTable.FormatAttributeWarning;
                msg = msg.FormatCurrent(XmlMinAttr, this._minimum, this.TypeName);
                result.AddWarning(msg, this.Location);
            }

            if (!this.Dictionary.Validate<T>(this._maximum, true))
            {
                string msg = StringTable.FormatAttributeWarning;
                msg = msg.FormatCurrent(XmlMaxAttr, this._maximum, this.TypeName);
                result.AddWarning(msg, this.Location);
            }

            if (!this.Dictionary.Validate<T>(this._step, true))
            {
                string msg = StringTable.FormatAttributeWarning;
                msg = msg.FormatCurrent(XmlStepAttr, this._step, this.TypeName);
                result.AddWarning(msg, this.Location);
            }

            if (!this.Dictionary.Validate<T>(this._initialValue, true))
            {
                string msg = StringTable.FormatAttributeWarning;
                msg = msg.FormatCurrent(XmlInitialAttr, this._initialValue, this.TypeName);
                result.AddWarning(msg, this.Location);
            }

            T min = this.Minimum;
            T max = this.Maximum;
            T step = this.Step;
            T init = this.InitialValue;
            if (min.CompareTo(max) > 0)
            {
                string msg = StringTable.MinGreaterThanMaxWarning;
                msg = msg.FormatCurrent(min.ToString(), max.ToString());
                result.AddWarning(msg, this.Location);
            }

            if (init.CompareTo(max) > 0)
            {
                string msg = StringTable.InitialOutsideOfRangeWarning;
                msg = msg.FormatCurrent(init.ToString(), min.ToString(), max.ToString());
                result.AddWarning(msg, this.Location);
            }

            if (this.Type == IntegerType.Unrecognised)
            {
                string msg = StringTable.UnrecognisedIntTypeWarning;
                msg = msg.FormatCurrent(this._type);
                result.AddWarning(msg, this.Location);
            }

            if (this.Type == IntegerType.Colour && !this.RepresentsColourValue)
            {
                string msg = StringTable.NotSupportedColourIntTypeWarning;
                result.AddWarning(msg, this.Location);
            }

            return result;
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            this._initialValue = reader.GetAttribute(XmlInitialAttr);
            this._minimum = reader.GetAttribute(XmlMinAttr);
            this._maximum = reader.GetAttribute(XmlMaxAttr);
            this._step = reader.GetAttribute(XmlStepAttr);
            this._type = reader.GetAttribute(XmlTypeAttr);
            reader.Skip();
        }

        /// <summary>
        /// Gets the string value that is equivalent to the specified type.
        /// </summary>
        /// <param name="type">
        /// The type that the returned string will be equivalent to.
        /// </param>
        /// <returns>
        /// The string that is equivalent to the specified type.
        /// </returns>
        private string GetStringFromType(IntegerType type)
        {
            switch (type)
            {
                case IntegerType.Colour:
                    return IntegerTypeColour;
                case IntegerType.Standard:
                case IntegerType.Unrecognised:
                default:
                    return null;
            }
        }

        /// <summary>
        /// Gets the integer type that is equivalent to the specified string.
        /// </summary>
        /// <param name="type">
        /// The string to determine the type to return.
        /// </param>
        /// <returns>
        /// The type that is equivalent to the specified string.
        /// </returns>
        private IntegerType GetTypeFromString(string type)
        {
            if (type == null)
            {
                return IntegerType.Standard;
            }
            else if (String.Equals(type, IntegerTypeColour))
            {
                return IntegerType.Colour;
            }
            else
            {
                return IntegerType.Unrecognised;
            }
        }
        #endregion Methods
    } // RSG.Metadata.Model.Definitions.Members.BaseIntegerMember{T} {Class}
} // RSG.Metadata.Model.Definitions.Members {Namespace}
