﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Model.Common.Mission
{
    /// <summary>
    /// Mission assets.
    /// </summary>
    [DataContract]
    [KnownType(typeof(MissionCheckpoint))]
    public class Mission : AssetBase, IMission
    {
        #region Properties
        /// <summary>
        /// Description of the mission
        /// </summary>
        [DataMember]
        public String Description { get; set; }

        /// <summary>
        /// Mission Id for the mission
        /// </summary>
        [DataMember]
        public String MissionId { get; set; }

        /// <summary>
        /// Script for the mission
        /// </summary>
        [DataMember]
        public String ScriptName { get; set; }

        /// <summary>
        /// Number of attempts we expect this mission to require.
        /// </summary>
        [DataMember]
        public float ProjectedAttempts { get; set; }

        /// <summary>
        /// Minimum number of attempts we expect this mission to require.
        /// </summary>
        [DataMember]
        public float ProjectedAttemptsMin { get; set; }

        /// <summary>
        /// Maximum number of attempts we expect this mission to require.
        /// </summary>
        [DataMember]
        public float ProjectedAttemptsMax { get; set; }

        /// <summary>
        /// Whether this is a singleplayer mission or not.
        /// </summary>
        [DataMember]
        public bool Singleplayer { get; set; }

        /// <summary>
        /// Whether this mission is active or not.
        /// </summary>
        [DataMember]
        public bool Active { get; set; }

        /// <summary>
        /// Whether bugstar knows about mission.
        /// </summary>
        [DataMember]
        public bool InBugstar { get; set; }

        /// <summary>
        /// List of checkpoints this mission has (if any).
        /// </summary>
        [DataMember]
        public IList<IMissionCheckpoint> Checkpoints { get; set; }

        /// <summary>
        /// Category this mission falls under.
        /// </summary>
        [DataMember]
        public MissionCategory Category { get; set; }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// For serialisation purposes.
        /// </summary>
        private Mission()
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public Mission(String name)
            : base(name)
        {
            Checkpoints = new List<IMissionCheckpoint>();
            Category = MissionCategory.Other;
        }
        #endregion // Constructor(s)
    } // Mission
}
