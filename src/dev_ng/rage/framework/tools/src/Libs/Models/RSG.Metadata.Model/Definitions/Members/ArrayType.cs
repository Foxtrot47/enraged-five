﻿// --------------------------------------------------------------------------------------------
// <copyright file="ArrayType.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions.Members
{
    /// <summary>
    /// Specifies the different types of array members the parCodeGen system supports.
    /// </summary>
    public enum ArrayType
    {
        /// <summary>
        /// The array type is unknown.
        /// </summary>
        Unknown,

        /// <summary>
        /// The array type is not recognised as valid, though it is defined as something.
        /// </summary>
        Unrecognised,

        /// <summary>
        /// The array member has a type of AtArray.
        /// </summary>
        AtArray,

        /// <summary>
        /// The array member has a fixed length.
        /// </summary>
        Fixed,

        /// <summary>
        /// The array member has a type of Range.
        /// </summary>
        Range,

        /// <summary>
        /// The array member has a type of Pointer and can be variable length or null.
        /// </summary>
        Pointer,

        /// <summary>
        /// The array member has a type of member and is a fixed length.
        /// </summary>
        Member,
    } // RSG.Metadata.Model.Definitions.Members.ArrayType {Enum}
} // RSG.Metadata.Model.Definitions.Members {Namespace}
