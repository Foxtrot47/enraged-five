﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Tasks;
using RSG.Model.Common;
using RSG.Base.ConfigParser;
using System.Threading;
using RSG.Base.Configuration;

namespace RSG.Model.Report
{
    /// <summary>
    /// Context used when creating a dynamic map report task.
    /// </summary>
    public class DynamicLevelReportContext : DynamicReportContext
    {
        #region Properties
        /// <summary>
        /// Level the report should be using.
        /// </summary>
        public ILevel Level { get; private set; }

        /// <summary>
        /// Platforms that are available.
        /// </summary>
        public IPlatformCollection Platforms { get; private set; }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public DynamicLevelReportContext(ConfigGameView gv, IConfig config, ILevel level, IPlatformCollection platforms)
            : this(CancellationToken.None, gv, config, level, platforms)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="token"></param>
        public DynamicLevelReportContext(CancellationToken token, ConfigGameView gv, IConfig config, ILevel level, IPlatformCollection platforms)
            : base(token, gv, config)
        {
            Level = level;
            Platforms = platforms;
        }
        #endregion // Constructor(s)
    } // DynamicLevelReportContext
}
