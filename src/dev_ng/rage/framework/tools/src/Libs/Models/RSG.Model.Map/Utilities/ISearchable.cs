﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Map.Utilities
{

    /// <summary>
    /// Search option flags
    /// </summary>
    [Flags] public enum SearchOptions
    {
        None,
        MatchWholeWord,
        CaseSensitive
    }

    /// <summary>
    /// 
    /// </summary>
    public interface ISearchable
    {
        #region Properties

        /// <summary>
        /// The name of the object and also the string that is
        /// checked during a simple search operation.
        /// </summary>
        String Name { get; }

        /// <summary>
        /// The parent of this searchable object
        /// </summary>
        ISearchable SearchableParent { get; }

        #endregion // Properties

        #region Walk Properties

        /// <summary>
        /// Walks through the children of ISearchables, going
        /// depth first
        /// </summary>
        IEnumerable<ISearchable> WalkSearchDepth { get; }
        
        #endregion // Walk Properties
    }

} // RSG.Model.Map.Utilities namespace
