﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.ConfigParser;
using RSG.Model.Common;
using RSG.Model.Common.Map;
using RSG.Base.Extensions;
using RSG.Base.Threading;
using System.Threading;
using RSG.Base.Tasks;
using RSG.Bounds;
using RSG.Base.Attributes;

namespace RSG.Model.Report.Reports.Map
{
    /// <summary>
    /// 
    /// </summary>
    public class SectionReport : CSVReport, IDynamicLevelReport
    {
        #region Constants
        private const String c_name = "Section Report";
        private const String c_description = "Exports the selected levels map sections into a .csv file";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (m_task == null)
                {
                    CompositeTask task = new CompositeTask("Generate Report");
                    task.AddSubTask(new ActionTask("Loading data", (context, progress) => EnsureDataLoaded((DynamicLevelReportContext)context, progress)), 50);
                    task.AddSubTask(new ActionTask("Generating report", (context, progress) => GenerateReport((DynamicLevelReportContext)context, progress)));
                    m_task = task;
                }
                return m_task;
            }
        }
        private ITask m_task;

        /// <summary>
        /// List of stats that are required to generate this report.
        /// </summary>
        public IEnumerable<StreamableStat> RequiredStats
        {
            get { return null; }
        }
        #endregion // Properties

        #region Private Class
        /// <summary>
        /// 
        /// </summary>
        private class CachedSectionStats
        {
            public IMapSectionStatistic ArchetypeStats { get; private set; }
            public IMapSectionStatistic EntityStats { get; private set; }
            public IMapSectionStatistic InteriorEntityStats { get; private set; }
            public IMapSectionStatistic DrawableEntityStats { get; private set; }
            public IMapSectionStatistic NonDrawableEntityStats { get; private set; }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="section"></param>
            public CachedSectionStats(IMapSection section)
            {
                ArchetypeStats = section.AggregatedStats[new StatKey(StatGroup.Archetypes, StatLodLevel.Total, StatEntityType.Total)];
                EntityStats = section.AggregatedStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Total, StatEntityType.Total)];
                InteriorEntityStats = section.AggregatedStats[new StatKey(StatGroup.InteriorEntities, StatLodLevel.Total, StatEntityType.Total)];
                DrawableEntityStats = section.AggregatedStats[new StatKey(StatGroup.DrawableEntities, StatLodLevel.Total, StatEntityType.Total)];
                NonDrawableEntityStats = section.AggregatedStats[new StatKey(StatGroup.NonDrawableEntities, StatLodLevel.Total, StatEntityType.Total)];
            }
        }

        /// <summary>
        /// Helper class that contains information for a single row for the users summary csv file.
        /// </summary>
        private class SectionCsvData
        {
            [FriendlyName("Name")]
            public String Name { get; set; }

            [FriendlyName("Parent Area")]
            public String ParentArea { get; set; }

            [FriendlyName("Grandparent Area")]
            public String GrandParentArea { get; set; }

            [FriendlyName("Drawable Entity Count")]
            public uint DrawableEntityCount { get; set; }

            [FriendlyName("Non-Drawable Entity Count")]
            public uint NonDrawableEntityCount { get; set; }

            [FriendlyName("Interior Instance Count")]
            public uint InteriorEntityCount { get; set; }

            [FriendlyName("Polygon Count")]
            public uint PolygonCount { get; set; }

            [FriendlyName("Texture Count")]
            public uint TextureCount { get; set; }

            [FriendlyName("Shader Count")]
            public uint ShaderCount { get; set; }

            [FriendlyName("Collision Group Count")]
            public uint CollisionGroupCount { get; set; }

            [FriendlyName("Light Instance Count")]
            public uint LightInstanceCount { get; set; }

            [FriendlyName("Texture Density")]
            public float TextureDensity { get; set; }

            [FriendlyName("Shader Density")]
            public float ShaderDensity { get; set; }

            [FriendlyName("Drawable Entity Density")]
            public float DrawableEntityDensity { get; set; }

            [FriendlyName("Non-Drawable Entity Density")]
            public float NonDrawableEntityDensity { get; set; }

            [FriendlyName("Polygon Density")]
            public float PolygonDensity { get; set; }

            [FriendlyName("Weapon Collision")]
            public ulong WeaponCollision { get; set; }

            [FriendlyName("Weapon Mover Collision")]
            public ulong WeaponMoverCollision { get; set; }

            [FriendlyName("Mover Collision")]
            public ulong MoverCollision { get; set; }

            [FriendlyName("Horse Collision")]
            public ulong HorseCollision { get; set; }

            [FriendlyName("Horse Vehicle Collision")]
            public ulong HorseVehicleCollision { get; set; }

            [FriendlyName("Vehicle Collision")]
            public ulong VehicleCollision { get; set; }

            [FriendlyName("Cover Collision")]
            public ulong CoverCollision { get; set; }

            [FriendlyName("Foliage Collision")]
            public ulong FoliageCollision { get; set; }

            [FriendlyName("River Collision")]
            public ulong RiverCollision { get; set; }

            [FriendlyName("Stair Slope Collision")]
            public ulong StairSlopeCollision { get; set; }

            [FriendlyName("Other Collision")]
            public ulong OtherCollision { get; set; }
        } // SectionCsvData
        #endregion // Private Class

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public SectionReport()
            : base(c_name, c_description)
        {
        }
        #endregion // Constructor(s)

        #region Task Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void EnsureDataLoaded(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            IMapHierarchy hierarchy = context.Level.MapHierarchy;

            if (hierarchy != null)
            {
                float increment = 1.0f / hierarchy.AllSections.Count();

                // Load all the map section data
                foreach (IMapSection section in hierarchy.AllSections)
                {
                    context.Token.ThrowIfCancellationRequested();

                    // Update the progress information
                    string message = String.Format("Loading {0}", section.Name);
                    progress.Report(new TaskProgress(increment, true, message));

                    section.LoadStats(new List<StreamableStat>() { StreamableMapSectionStat.Archetypes, StreamableMapSectionStat.Entities, StreamableMapSectionStat.AggregatedStats });
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void GenerateReport(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            string directory = System.IO.Path.GetDirectoryName(Filename);
            string filename = System.IO.Path.GetFileNameWithoutExtension(Filename);
            string genericFilename = System.IO.Path.Combine(directory, filename + "_generic.csv");
            string nongenericFilename = System.IO.Path.Combine(directory, filename + "_nongeneric.csv");
            ExportSectionCSVReport(genericFilename, nongenericFilename, context.Level, context.GameView);
        }
        #endregion // Task Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="objectFilename"></param>
        /// <param name="level"></param>
        /// <param name="gv"></param>
        public void ExportSectionCSVReport(String genericFilename, String nonGenericFilename, ILevel level, ConfigGameView gv)
        {
            // Attempt the get the map hierarchy from the level
            IMapHierarchy hierarchy = level.MapHierarchy;

            if (hierarchy != null)
            {
                // Gather the stats for all the sections and store them in a local cache.
                Dictionary<IMapSection, CachedSectionStats> sectionStats = new Dictionary<IMapSection, CachedSectionStats>();
                foreach (IMapSection section in hierarchy.AllSections)
                {
                    sectionStats.Add(section, new CachedSectionStats(section));
                }

                // Generate the report.
                IList<SectionCsvData> genericData = new List<SectionCsvData>();
                IList<SectionCsvData> nonGenericData = new List<SectionCsvData>();

                foreach (IMapSection section in hierarchy.AllSections)
                {
                    IMapArea parentArea = section.ParentArea;
                    IMapArea grandParentArea = (parentArea != null ? parentArea.ParentArea : null);

                    uint drawableInstanceCount = 0;
                    uint nonDrawableInstanceCount = 0;
                    uint interiorCount = 0;
                    int lightInstances = 0;

                    uint polycount = 0;
                    uint shaderCount = 0;
                    uint textureCount = 0;

                    // Get some data based on type of section this is
                    CachedSectionStats stats = sectionStats[section];

                    if (section.ExportEntities)
                    {
                        drawableInstanceCount = stats.DrawableEntityStats.TotalCount;
                        nonDrawableInstanceCount = stats.NonDrawableEntityStats.TotalCount;
                        interiorCount = stats.InteriorEntityStats.TotalCount;

                        polycount = stats.EntityStats.PolygonCount;
                        shaderCount = stats.EntityStats.ShaderCount;
                        textureCount = stats.EntityStats.TextureCount;
                        foreach (IEntity entity in section.ChildEntities)
                        {
                            if (entity == null)
                            {
                                continue;
                            }

                            lightInstances += entity.RageLightInstanceCount;
                        }
                    }
                    else
                    {
                        drawableInstanceCount = stats.ArchetypeStats.TotalCount;
                        nonDrawableInstanceCount = stats.ArchetypeStats.TotalCount;
                        interiorCount = stats.ArchetypeStats.TotalCount;

                        polycount = stats.ArchetypeStats.PolygonCount;
                        shaderCount = stats.ArchetypeStats.ShaderCount;
                        textureCount = stats.ArchetypeStats.TextureCount;
                        lightInstances = 0;
                    }

                    // Create the csv data object.
                    SectionCsvData data =
                        new SectionCsvData
                        {
                            Name = section.Name,
                            ParentArea = (parentArea != null ? parentArea.Name : "n/a"),
                            GrandParentArea = (grandParentArea != null ? grandParentArea.Name : "n/a"),
                            DrawableEntityCount = drawableInstanceCount,
                            NonDrawableEntityCount = nonDrawableInstanceCount,
                            InteriorEntityCount = interiorCount,
                            PolygonCount = polycount,
                            TextureCount = shaderCount,
                            ShaderCount = textureCount,
                            CollisionGroupCount = (uint)GetCollisionGroups(section).Count(),
                            LightInstanceCount = (uint)lightInstances
                        };

                    // Gather the collision data
                    GatherCollisionData(section, ref data);

                    // Only set density properties if the area isn't 0.
                    if (section.Area != 0.0f)
                    {
                        data.TextureDensity = (textureCount / section.Area);
                        data.ShaderDensity = (shaderCount / section.Area);
                        data.DrawableEntityDensity = (drawableInstanceCount / section.Area);
                        data.NonDrawableEntityDensity = (nonDrawableInstanceCount / section.Area);
                        data.PolygonDensity = (polycount / section.Area);
                    }

                    // Determine which report we should output this piece of data to.
                    if (section.ExportEntities)
                    {
                        nonGenericData.Add(data);
                    }
                    else
                    {
                        genericData.Add(data);
                    }
                }

                // Convert the data to a csv stream and save it out.
                Stream genericStream = GenerateCSVStream(genericData);
                SaveStreamToFile(genericStream, genericFilename);

                Stream nonGenericStream = GenerateCSVStream(nonGenericData);
                SaveStreamToFile(nonGenericStream, nonGenericFilename);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        private IEnumerable<string> GetCollisionGroups(IMapSection section)
        {
            ISet<string> collisionGroups = new HashSet<string>();
            foreach (IArchetype archetype in section.Archetypes)
            {
                ISimpleMapArchetype simpleArchetype = archetype as ISimpleMapArchetype;
                if (simpleArchetype != null && !simpleArchetype.HasDefaultCollisionGroup)
                {
                    collisionGroups.Add(simpleArchetype.CollisionGroup);
                }
            }

            return collisionGroups;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="data"></param>
        private void GatherCollisionData(IMapSection section, ref SectionCsvData data)
        {
            foreach (IGrouping<BNDFile.CollisionType, CollisionInfo> group in section.CollisionPolygonCountBreakdown.GroupBy(item => item.CollisionFlags))
            {
                long count = group.Sum(item => item.Count);

                if (group.Key.HasFlag(BNDFile.CollisionType.Mover))
                {
                    if (group.Key.HasFlag(BNDFile.CollisionType.Weapons))
                    {
                        data.WeaponMoverCollision += (ulong)count;
                    }
                    else
                    {
                        data.MoverCollision += (ulong)count;
                    }
                }
                else if (group.Key == (BNDFile.CollisionType.Horse | BNDFile.CollisionType.Vehicle))
                {
                    data.HorseVehicleCollision += (ulong)count;
                }
                else if (group.Key == BNDFile.CollisionType.Weapons)
                {
                    data.WeaponCollision += (ulong)count;
                }
                else if (group.Key == BNDFile.CollisionType.Horse)
                {
                    data.HorseCollision += (ulong)count;
                }
                else if (group.Key == BNDFile.CollisionType.Vehicle)
                {
                    data.VehicleCollision += (ulong)count;
                }
                else if (group.Key == BNDFile.CollisionType.Cover)
                {
                    data.CoverCollision += (ulong)count;
                }
                else if (group.Key == BNDFile.CollisionType.Foliage)
                {
                    data.FoliageCollision += (ulong)count;
                }
                else if (group.Key == BNDFile.CollisionType.River)
                {
                    data.RiverCollision += (ulong)count;
                }
                else if (group.Key == BNDFile.CollisionType.StairSlope)
                {
                    data.StairSlopeCollision += (ulong)count;
                }
                else
                {
                    data.OtherCollision += (ulong)count;
                }
            }
        }
        #endregion // Private Methods
    } // SectionReport
}
