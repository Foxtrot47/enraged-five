﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Collections;
using RSG.Model.Map.Utilities;

namespace RSG.Model.Map
{
    /// <summary>
    /// The map model for a single texture dictionary
    /// </summary>
    public class TextureDictionary : IComparable<TextureDictionary>, IEnumerable<Texture>, ISearchable
    {
        #region Properties

        /// <summary>
        /// The name of this texture dictionary
        /// </summary>
        public String Name
        {
            get;
            private set;
        }

        /// <summary>
        /// The collection of textures that are inside this dictionary, indexed
        /// by the textures filepath
        /// </summary>
        private Dictionary<String, Texture> TextureSet
        {
            get;
            set;
        }

        /// <summary>
        /// The parent of this searchable object
        /// </summary>
        public ISearchable SearchableParent
        {
            get;
            set;
        }

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// Default constructor, takes the name of the texture dictionary
        /// </summary>
        /// <param name="name"></param>
        public TextureDictionary(String name)
        {
            this.TextureSet = new Dictionary<String, Texture>();
            this.Name = name == null ? String.Empty : name;
        }

        /// <summary>
        /// Creates a texture dictionary with a given name a a given texture list
        /// </summary>
        /// <param name="name"></param>
        /// <param name="textures"></param>
        public TextureDictionary(String name, IList<Texture> textures)
        {
            this.TextureSet = new Dictionary<String, Texture>();
            this.Name = name == null ? String.Empty : name;
            foreach (Texture texture in textures)
            {
                this.AddTexture(texture);
            }
        }

        #endregion //Constructor(s)

        #region IComparable

        /// <summary>
        /// Compares two texture dictionaries and returns 0 if they are the same.
        /// Two texture dictionaries are the same if they have the same name (ignore case)
        /// </summary>
        public int CompareTo(TextureDictionary dictionary)
        {
            return String.Compare(this.Name, dictionary.Name, true);
        }

        #endregion // IComparable

        #region Dictionary Manipulation

        /// <summary>
        /// Adds a texture to this set iff it isn't already and it has a stream filepath
        /// </summary>
        /// <param name="textureDictionary">The texture to add</param>
        public void AddTexture(Texture texture)
        {
            if (!String.IsNullOrEmpty(texture.StreamName))
            {
                if (!this.TextureSet.ContainsKey(texture.StreamName))
                {
                    this.TextureSet.Add(texture.StreamName, texture);
                }
            }
        }

        /// <summary>
        /// Adds the given textures to this dictionary first making sure
        /// that they are not in already
        /// </summary>
        /// <param name="textures">The set of textures to add</param>
        public void AddTextures(IList<Texture> textures)
        {
            foreach (Texture texture in textures)
            {
                this.AddTexture(texture);
            }
        }

        /// <summary>
        /// Removes a texture dictionary from this set iff it is currently a member of this set
        /// </summary>
        /// <param name="textureDictionary">The texture dictionary to remove</param>
        public void RemoveTexture(Texture texture)
        {
            if (this.TextureSet.ContainsKey(texture.StreamName))
            {
                this.TextureSet.Remove(texture.StreamName);
            }
        }

        /// <summary>
        /// Clears the textures making this set empty
        /// </summary>
        public void ClearTextures()
        {
            this.TextureSet.Clear();
        }

        #endregion // Dictionary Manipulation

        #region IEnumerable<TextureDictionary>

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerator<Texture> IEnumerable<Texture>.GetEnumerator()
        {
            foreach (Texture t in this.TextureSet.Values)
            {
                yield return t;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerator)this);
        }

        #endregion // IEnumerable<TextureDictionary>

        #region Walk Properties

        /// <summary>
        /// Walks through the children of ISearchables, going
        /// depth first
        /// </summary>
        public IEnumerable<ISearchable> WalkSearchDepth
        {
            get
            {
                yield return this;
                if (this.TextureSet != null)
                {
                    List<Texture> textures = new List<Texture>(this.TextureSet.Values);
                    foreach (ISearchable searchable in textures.Where(t => t is ISearchable == true))
                    {
                        foreach (ISearchable searchableChld in searchable.WalkSearchDepth)
                        {
                            yield return searchableChld;
                        }
                    }
                }
            }
        }

        #endregion // Walk Properties

    } // TextureDictionary
} // RSG.Model.Map
