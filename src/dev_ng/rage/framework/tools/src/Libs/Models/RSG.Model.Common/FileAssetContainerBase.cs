﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace RSG.Model.Common
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class FileAssetContainerBase : FileAssetBase, IHasAssetChildren
    {
        #region Members

        protected RSG.Base.Collections.ObservableCollection<IAsset> m_assetChildren;

        #endregion // Members

        #region Properties

        /// <summary>
        /// The collection of assets belonging to this container
        /// </summary>
        public RSG.Base.Collections.ObservableCollection<IAsset> AssetChildren
        {
            get { return m_assetChildren; }
            set
            {
                SetPropertyValue(ref m_assetChildren, value, "AssetChildren");
            }
        }

        #endregion // Properties

        #region Constructors

        /// <summary>
        /// Default Constructor
        /// </summary>
        public FileAssetContainerBase()
            : base()
        {
            this.m_assetChildren = new RSG.Base.Collections.ObservableCollection<IAsset>();
        }

        /// <summary>
        /// Default Constructor
        /// </summary>
        public FileAssetContainerBase(String name, String filename)
            : base(name, filename)
        {
            this.m_assetChildren = new RSG.Base.Collections.ObservableCollection<IAsset>();
        }

        #endregion // Constructors

        #region Public Methods
        /// <summary>
        /// Returns true if this container contains a asset in it
        /// with the given name
        /// </summary>
        public Boolean ContainsAsset(String name)
        {
            foreach (IAsset asset in this.AssetChildren)
            {
                if (String.Compare(asset.Name, name) == 0)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public override List<IAsset> Find(Regex expression)
        {
            var results = new List<IAsset>();

            if (!string.IsNullOrEmpty(this.Name))
            {
                if (expression.IsMatch(this.Name))
                    results.Add(this);
            }
            if (this.AssetChildren != null)
            {
                foreach (var child in this.AssetChildren)
                {
                    results.AddRange(child.Find(expression));
                }
            }

            return results;
        }
        #endregion // Public Methods
    } // FileAssetContainerBase
} // RSG.Model.Common
