﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Logging;
using RSG.Model.Common;
using RSG.Model.Common.Animation;
using System.Diagnostics;
using RSG.Base.Configuration;
using System.Runtime.Serialization;

namespace RSG.Model.Animation
{
    /// <summary>
    /// Cutscene asset.
    /// </summary>
    [DataContract]
    [Serializable]
    [KnownType(typeof(CutscenePart))]
    [KnownType(typeof(CutscenePlatformStat))]
    public class Cutscene : StreamableAssetBase, ICutscene
    {
        #region Properties
        /// <summary>
        /// Friendly name for the cutscene (extracted from the cutscene data file).
        /// </summary>
        [DataMember]
        public String FriendlyName { get; protected set; }

        /// <summary>
        /// Mission this cutscene is associated with (extracted from the cutscene data file).
        /// </summary>
        [DataMember]
        public String MissionId { get; protected set; }

        /// <summary>
        /// Flag indicating whether this is a cutscene concat.
        /// </summary>
        [StreamableStat(StreamableCutsceneStat.ExportZipFilepathString)]
        public String ExportZipFilepath
        {
            get
            {
                return m_exportZipFilepath;
            }
            protected set
            {
                SetPropertyValue(ref m_exportZipFilepath, value, "ExportZipFilepath");
                SetStatLodaded(StreamableCutsceneStat.ExportZipFilepath, true);
            }
        }
        [DataMember]
        private String m_exportZipFilepath;

        /// <summary>
        /// Flag indicating whether this is a cutscene concat.
        /// </summary>
        [StreamableStat(StreamableCutsceneStat.IsConcatString)]
        public bool IsConcat
        {
            get
            {
                return m_isConcat;
            }
            protected set
            {
                SetPropertyValue(ref m_isConcat, value, "IsConcat");
                SetStatLodaded(StreamableCutsceneStat.IsConcat, true);
            }
        }
        [DataMember]
        private bool m_isConcat;

        /// <summary>
        /// Flag indicating whether this cutscene contains branches.
        /// </summary>
        [StreamableStat(StreamableCutsceneStat.HasBranchString)]
        public bool HasBranch
        {
            get
            {
                return m_hasBranch;
            }
            protected set
            {
                SetPropertyValue(ref m_hasBranch, value, "HasBranch");
                SetStatLodaded(StreamableCutsceneStat.HasBranch, true);
            }
        }
        [DataMember]
        private bool m_hasBranch;

        /// <summary>
        /// The sub parts that make up this cutscene.
        /// </summary>
        [DataMember]
        [Browsable(false)]
        public IList<ICutscenePart> Parts { get; protected set; }

        /// <summary>
        /// How long this entire cutscene is (in seconds).
        /// Based of the duration of the individual parts.
        /// </summary>
        [StreamableStat(StreamableCutsceneStat.DurationString)]
        public float TotalDuration
        {
            get
            {
                return Parts.Sum(item => item.Duration);
            }
        }

        /// <summary>
        /// Platform size statistics.
        /// </summary>
        [Browsable(false)]
        [StreamableStat(StreamableCutsceneStat.PlatformStatsString)]
        public IDictionary<RSG.Platform.Platform, ICutscenePlatformStat> PlatformStats
        {
            get
            {
                return m_platformStats;
            }
            protected set
            {
                SetPropertyValue(ref m_platformStats, value, "PlatformStats");
                SetStatLodaded(StreamableCutsceneStat.PlatformStats, true);
            }
        }
        [DataMember]
        private IDictionary<RSG.Platform.Platform, ICutscenePlatformStat> m_platformStats;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public Cutscene(String name, String friendlyName, String missionId)
            : base(name)
        {
            Parts = new List<ICutscenePart>();
            FriendlyName = friendlyName;
            MissionId = missionId;
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public Cutscene(String name, String friendlyName, String missionId, bool isConcat, bool hasBranch, String exportZipFilepath)
            : this(name, friendlyName, missionId)
        {
            IsConcat = isConcat;
            HasBranch = hasBranch;
            ExportZipFilepath = exportZipFilepath;
        }
        #endregion // Constructor(s)

        #region IComparable<ICutscene> Interface
        /// <summary>
        /// Compare this cutscene to another.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(ICutscene other)
        {
            if (other == null)
            {
                return 1;
            }

            return (Name.CompareTo(other.Name));
        }
        #endregion // IComparable<ICutscene> Interface

        #region IEquatable<ICutscene> Interface
        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>true if the current object is equal to the other parameter; otherwise, false.</returns>
        public bool Equals(ICutscene other)
        {
            if (other == null)
            {
                return false;
            }

            return (Name == other.Name);
        }
        #endregion // IEquatable<IWeapon> Interface
    } // Cutscene
}
