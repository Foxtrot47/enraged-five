﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace RSG.Model.Statistics.Captures
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class StreamingMemoryStat
    {
        #region Properties
        /// <summary>
        /// Name of the category of this streaming memory stat.
        /// </summary>
        [DataMember]
        public String CategoryName { get; private set; }

        /// <summary>
        /// Virtual memory usage.
        /// </summary>
        [DataMember]
        public uint Virtual { get; private set; }

        /// <summary>
        /// Physical memory usage.
        /// </summary>
        [DataMember]
        public uint Physical { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="virt"></param>
        /// <param name="physical"></param>
        public StreamingMemoryStat(String categoryName, uint virt, uint physical)
        {
            CategoryName = categoryName;
            Virtual = virt;
            Physical = physical;
        }
        #endregion // Constructor(s)
    } // StreamingMemoryStat
}
