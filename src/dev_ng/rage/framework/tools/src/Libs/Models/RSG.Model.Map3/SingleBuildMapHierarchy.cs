﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.Statistics.Common.Dto.GameAssets;

namespace RSG.Model.Map3
{
    /// <summary>
    /// Map hierarchy that comes from a single build in the database
    /// </summary>
    public class SingleBuildMapHierarchy : MapHierarchyBase
    {
        #region Properties
        /// <summary>
        /// The build this vehicle collection is for
        /// </summary>
        public String BuildIdentifier { get; protected set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public SingleBuildMapHierarchy(ILevel level, MapStatStreamingCoordinator streamingCoordinator, String buildIdentifier)
            : base(level, streamingCoordinator)
        {
            BuildIdentifier = buildIdentifier;
        }
        #endregion // Constructor(s)
    } // SingleBuildMapHierarchy
}
