﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using RSG.Base.Logging;
using RSG.Model.Common;

namespace RSG.Model.Map3
{
    internal class GeometryStatsCollection : IGeometryStatsCollection
    {
        internal GeometryStatsCollection(string pathname)
        {
            data_ = new SortedDictionary<string, GeometryStats>();

            if (File.Exists(pathname))
            {
                try
                {
                    XDocument document = XDocument.Load(pathname);
                    foreach (XElement sectionElement in document.Root.Element("Sections").Elements("Item"))
                    {
                        foreach (XElement archetypeElement in sectionElement.Element("Archetypes").Elements("Item"))
                        {
                            string archetypeName = archetypeElement.Element("Name").Value;
                            float archetypeHeuristic = Single.Parse(archetypeElement.Element("Heuristic").Attribute("value").Value);
                            if (!data_.ContainsKey(archetypeName))
                            {
                                data_.Add(archetypeName, new GeometryStats(archetypeName, archetypeHeuristic));
                            }
                            else
                            {
                                // TODO - log the duplicate archetype issue - skipping over the duplicate is better than failure
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    // A broken file just means no data but we should log the exception to help diagnose the issue
                    Log.Log__Exception(e, "Unexpected exception when trying to read data from the geometry stats file.");
                }
            }
            else
            {
                Log.Log__Message("Geometry stats file doesn't exist at '{0}'.", pathname);
            }
        }

        public bool HasStatsForArchetype(string archetypeName)
        {
            return data_.ContainsKey(archetypeName);
        }

        public IGeometryStats GetStatsForArchetype(string archetypeName)
        {
            return data_[archetypeName];
        }

        private SortedDictionary<string, GeometryStats> data_;
    }
}
