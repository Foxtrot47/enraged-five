﻿//---------------------------------------------------------------------------------------------
// <copyright file="Methods.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Converters
{
    using System;

    /// <summary>
    /// Defines the different methods available for the converters to use.
    /// </summary>
    [Flags]
    public enum Methods
    {
        /// <summary>
        /// The string is converted to a number with the assumption that it is in the string
        /// representation of that number.
        /// </summary>
        Number = 1 << 0,

        /// <summary>
        /// The string is converted to a number with the assumption that it is in the
        /// hexadecimal representation of that number.
        /// </summary>
        Hex = 1 << 1,

        /// <summary>
        /// The string is converted to a number with the assumption that it is equal to a
        /// constant identifier that identifies a number.
        /// </summary>
        Constant = 1 << 2,

        /// <summary>
        /// The string is converted to a number with the assumption that it is a mathematical
        /// expression consisting of any number of values, operators and brackets.
        /// </summary>
        Expression = 1 << 3,

        /// <summary>
        /// The string is converted to a number with no assumptions about formatting.
        /// </summary>
        Any = Methods.Number | Methods.Hex | Methods.Constant | Methods.Expression,

        /// <summary>
        /// The string is converted to a number with the assumption that it is represented in
        /// a mathematical way, either the value or a expression to the value.
        /// </summary>
        Mathematical = Methods.Number | Methods.Hex | Methods.Expression,
    } // RSG.Metadata.Model.Converters.Methods {Enum}
} // RSG.Metadata.Model.Converters {Namespace}
