﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Math;
using RSG.SceneXml;
using RSG.Base.Collections;

namespace RSG.Model.Common.Map
{
    /// <summary>
    /// 
    /// </summary>
    public interface IRoom : IAsset, IHasEntityChildren, IStreamableObject
    {
        #region Properties
        /// <summary>
        /// The interior archetype that "owns" this room
        /// </summary>
        IInteriorArchetype ParentArchetype { get; }

        /// <summary>
        /// 
        /// </summary>
        BoundingBox3f BoundingBox { get; }

        /// <summary>
        /// 
        /// </summary>
        IShader[] Shaders { get; }

        /// <summary>
        /// 
        /// </summary>
        ITexture[] Textures { get; }

        /// <summary>
        /// Map of texture dictionary names to their export size (used for section stat calculations)
        /// </summary>
        IDictionary<String, uint> TxdExportSizes { get; }

        /// <summary>
        /// 
        /// </summary>
        uint ExportGeometrySize { get; }

        /// <summary>
        /// 
        /// </summary>
        uint PolygonCount { get; }

        /// <summary>
        /// 
        /// </summary>
        uint CollisionPolygonCount { get; }

        /// <summary>
        /// Map of collision type flags to poly counts
        /// </summary>
        IDictionary<CollisionType, uint> CollisionTypePolygonCounts { get; }
        #endregion // Properties
    } // IRoom
}
