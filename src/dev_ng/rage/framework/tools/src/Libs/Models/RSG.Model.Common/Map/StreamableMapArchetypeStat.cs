﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common.Map
{
    /// <summary>
    /// Base class for streamable map archetype stat enumerations.
    /// </summary>
    [DependentStat(StreamableMapSectionStat.ArchetypesString)]
    public static class StreamableMapArchetypeStat
    {
        public const String BoundingBoxString = "EBF51B22-A695-481D-AE09-8E2503687EE7";
        public const String ShadersString = "BC6DC93F-501A-4EE6-A9A7-060C97D63CBC";
        public const String ExportGeometrySizeString = "04E3DE0E-ADE0-409F-B494-286CBB62DDF3";
        public const String TxdExportSizesString = "A8394DA8-276E-4257-A78A-B04E02AD4F93";
        public const String PolygonCountString = "B65E4E64-3C09-433E-9C87-BC5B5579524D";
        public const String CollisionPolygonCountString = "A391A1D6-C1F4-4671-8104-AFDAD877A8A7";
        public const String CollisionTypePolygonCountsString = "AE9A9646-827C-457A-AA79-E3D27DCE5E2D";
        public const String HasAttachedLightString = "52E6C7A7-4FE6-4245-B3F9-5BAE4FFA10A0";
        public const String HasExplosiveEffectString = "7261A853-3FC9-488D-B7C9-CE49733E3ACC";
        public const String DontAddToIplString = "DE7E7F0E-D051-4AB7-BDAD-6D69C3DE3C6F";
        public const String IsPermanentArchetypeString = "E94201A1-BCB0-4A60-A84E-EC5FC6591D00";

        public static readonly StreamableStat BoundingBox = new StreamableStat(BoundingBoxString, true, true, false);
        public static readonly StreamableStat Shaders = new StreamableStat(ShadersString, true, true);
        public static readonly StreamableStat ExportGeometrySize = new StreamableStat(ExportGeometrySizeString, true);
        public static readonly StreamableStat TxdExportSizes = new StreamableStat(TxdExportSizesString, true, true);
        public static readonly StreamableStat PolygonCount = new StreamableStat(PolygonCountString, true);
        public static readonly StreamableStat CollisionPolygonCount = new StreamableStat(CollisionPolygonCountString, true);
        public static readonly StreamableStat CollisionTypePolygonCounts = new StreamableStat(CollisionTypePolygonCountsString, true, true);
        public static readonly StreamableStat HasAttachedLight = new StreamableStat(HasAttachedLightString, true);
        public static readonly StreamableStat HasExplosiveEffect = new StreamableStat(HasExplosiveEffectString, true);
        public static readonly StreamableStat DontAddToIpl = new StreamableStat(DontAddToIplString, false);
        public static readonly StreamableStat IsPermanentArchetype = new StreamableStat(IsPermanentArchetypeString, false);
        
    } // StreamableMapArchetypeStat
}
