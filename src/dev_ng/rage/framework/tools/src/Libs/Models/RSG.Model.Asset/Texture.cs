﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.Base.Editor.Command;
using RSG.SceneXml.Material;
using System.IO;
using RSG.Statistics.Common.Dto.GameAssets.Bundles;

namespace RSG.Model.Asset
{
    /// <summary>
    /// 
    /// </summary>
    public class Texture : FileAssetBase, ITexture
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// The parent asset container that 
        /// </summary>
        public IHasTextureChildren Parent
        {
            get { return m_parent; }
            set
            {
                SetPropertyValue(ref m_parent, value, "Parent");
            }
        }
        private IHasTextureChildren m_parent;

        /// <summary>
        /// The path to the alpha texture that is associated with
        /// this texture (this can be null when no alpha texture is associated).
        /// </summary>
        public String AlphaFilename
        {
            get { return m_alphaFilename; }
            set
            {
                SetPropertyValue(ref m_alphaFilename, value, "AlphaFilename");
            }
        }
        private String m_alphaFilename;

        /// <summary>
        /// The path to the alpha texture that is associated with
        /// this texture (this can be null when no alpha texture is associated).
        /// </summary>
        public TextureTypes TextureType
        {
            get { return m_textureType; }
            set
            {
                SetPropertyValue(ref m_textureType, value, "TextureType");
            }
        }
        private TextureTypes m_textureType;

        /// <summary>
        /// Gets a iterator around the source filenames that have gone into creating
        /// this texture.
        /// </summary>
        public IEnumerable<string> SourceFilenames
        {
            get
            {
                if (this.sourceFilenames == null)
                {
                    yield break;
                }

                foreach (string sourceFilename in this.sourceFilenames)
                {
                    yield return sourceFilename;
                }
            }
        }

        /// <summary>
        /// The private field used for the <see cref="SourceFilenames"/> property.
        /// </summary>
        private List<string> sourceFilenames;
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Texture asset constructor.
        /// </summary>
        public Texture(String filename)
            : base(Path.GetFileNameWithoutExtension(filename), filename)
        {
            this.sourceFilenames = new List<string>() { filename };
        }

        /// <summary>
        /// Texture asset constructor.
        /// </summary>
        public Texture(String filename, String alphaFilename, TextureTypes type, IHasTextureChildren parent)
            : base(Path.GetFileNameWithoutExtension(filename).ToLower(), filename)
        {
            this.sourceFilenames = new List<string>() { filename };
            this.Parent = parent;
            this.AlphaFilename = alphaFilename;

            if (!String.IsNullOrEmpty(this.AlphaFilename))
            {
                this.sourceFilenames.Add(alphaFilename);
                this.Name += Path.GetFileNameWithoutExtension(alphaFilename).ToLower();
            }

            // Hash is created out of the filenames as the name may not be unique
            this.Hash = RSG.ManagedRage.StringHashUtil.atStringHash(filename + alphaFilename, 0);
        }

        /// <summary>
        /// Internal constructor for creating a texture from the stats server
        /// </summary>
        /// <param name="shader"></param>
        public Texture(TextureBundleDto bundle)
            : base(bundle.Name, bundle.Filename)
        {
            this.sourceFilenames = new List<string>();
            this.AlphaFilename = bundle.AlphaFilename;
            this.TextureType = (RSG.SceneXml.Material.TextureTypes)Enum.Parse(typeof(RSG.SceneXml.Material.TextureTypes), bundle.TextureType);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public Texture(RSG.Statistics.Common.Dto.GameAssetStats.TextureStatDto dto)
            : base(dto.Name, dto.Filename)
        {
            sourceFilenames = new List<String>();
            AlphaFilename = dto.AlphaFilename;
            TextureType = (RSG.SceneXml.Material.TextureTypes)Enum.Parse(typeof(RSG.SceneXml.Material.TextureTypes), dto.TextureType);
        }
        #endregion // Constructor(s)

        #region IEquatable<ITexture> Implementation
        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>true if the current object is equal to the other parameter; otherwise, false.</returns>
        public bool Equals(ITexture other)
        {
            if (other == null)
            {
                return false;
            }

            return (Hash == other.Hash);
        }
        #endregion // IEquatable<Texture> Implementation

        #region IComparable<ITexture> Implementation
        /// <summary>
        /// Compares the current object with another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns></returns>
        public int CompareTo(ITexture other)
        {
            return Name.CompareTo(other.Name);
        }
        #endregion // IComparable<ITexture> Implementation

        #region Object Overrides
        /// <summary>
        /// Compare
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(Object obj)
        {
            if (obj == null)
            {
                return base.Equals(obj);
            }

            return ((obj is ITexture) && Equals(obj as ITexture));
        }

        /// <summary>
        /// Return String representation of the Vehicle.
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return Name;
        }

        /// <summary>
        /// Return hash code of Vehicle (based on Name).
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }
        #endregion // Object Overrides
    } // Texture
} // RSG.Model.Asset
