﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Linq;
using System.Xml.XPath;

namespace RSG.Model.Statistics.Captures
{
    /// <summary>
    /// Class that contains all information extract from a single modifications file.
    /// </summary>
    public class Modifications
    {
        #region Properties
        /// <summary>
        /// List of modifications (unsorted).
        /// </summary>
        public IList<Modification> AllModifications { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public Modifications(String filename)
        {
            AllModifications = new List<Modification>();
            ParseModificationsFile(filename);
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        private void ParseModificationsFile(String filename)
        {
            if (File.Exists(filename))
            {
                IList<ModificationInfo> modificationInfos = new List<ModificationInfo>();

                using (Stream stream = File.OpenRead(filename))
                {
                    XDocument doc = XDocument.Load(stream);

                    foreach (XElement elem in doc.XPathSelectElements("//ArrayOfModification/Modification"))
                    {
                        modificationInfos.Add(new ModificationInfo(elem));
                    }
                }

                // Group the infos into modifications so that the files are grouped together.
                foreach (IGrouping<uint, ModificationInfo> groupedInfos in modificationInfos.GroupBy(item => item.Changelist))
                {
                    AllModifications.Add(new Modification(groupedInfos));
                }
            }
        }
        #endregion // Private Methods
    } // Modifications
}
