﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProjectItemComparer.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.Model
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Supports the comparison between object of the type <see cref="ProjectItem"/>.
    /// </summary>
    public class ProjectItemComparer : IEqualityComparer<ProjectItem>
    {
        #region Methods
        /// <summary>
        /// Determines whether the specified objects are equal.
        /// </summary>
        /// <param name="x">
        /// The first object of type T to compare.
        /// </param>
        /// <param name="y">
        /// The second object of type T to compare.
        /// </param>
        /// <returns>
        /// True if the specified objects are equal; otherwise, false.
        /// </returns>
        public bool Equals(ProjectItem x, ProjectItem y)
        {
            if (x == null)
            {
                return y == null;
            }
            else
            {
                if (y == null)
                {
                    return false;
                }
            }

            return String.Equals(x.Include, y.Include, StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Returns a hash code for the specified object.
        /// </summary>
        /// <param name="obj">
        /// The System.Object for which a hash code is to be returned.
        /// </param>
        /// <returns>
        /// A hash code for the specified object.
        /// </returns>
        public int GetHashCode(ProjectItem obj)
        {
            return obj == null ? -1 : obj.Include.ToLowerInvariant().GetHashCode();
        }
        #endregion Methods
    } // RSG.Project.Model.ProjectItemComparer {Class}
} // RSG.Project.Model {Namespace}
