﻿// --------------------------------------------------------------------------------------------
// <copyright file="ArrayMember.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions.Members
{
    using System;
    using System.Xml;
    using RSG.Base;
    using RSG.Base.Extensions;
    using RSG.Base.Logging;
    using RSG.Editor;
    using RSG.Editor.Model;
    using RSG.Metadata.Model.Resources;
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// Represents the &lt;array&gt; member node in the parCodeGen system that can be instanced
    /// in a metadata file.
    /// </summary>
    public class ArrayMember : MemberBase, IEquatable<ArrayMember>
    {
        #region Fields
        /// <summary>
        /// The string representation of the <see cref="ArrayType.AtArray"/> constant.
        /// </summary>
        private const string ArrayTypeAtArray = "atArray";

        /// <summary>
        /// The string representation of the <see cref="ArrayType.Fixed"/> constant.
        /// </summary>
        private const string ArrayTypeFixed = "atFixedArray";

        /// <summary>
        /// The string representation of the <see cref="ArrayType.Member"/> constant.
        /// </summary>
        private const string ArrayTypeMember = "member";

        /// <summary>
        /// The string representation of the <see cref="ArrayType.Pointer"/> constant.
        /// </summary>
        private const string ArrayTypePointer = "pointer";

        /// <summary>
        /// The string representation of the <see cref="ArrayType.Range"/> constant.
        /// </summary>
        private const string ArrayTypeRanged = "atRangeArray";

        /// <summary>
        /// The name of the xml attribute containing the data for the
        /// <see cref="AddGroupWidgets"/> property.
        /// </summary>
        private const string XmlGroupWidgetsAttr = "addGroupWidget ";

        /// <summary>
        /// The name of the xml attribute containing the data for the
        /// <see cref="IndexBits"/> property.
        /// </summary>
        private const string XmlIndexBitsAttr = "indexBits";

        /// <summary>
        /// The name of the xml attribute containing the data for the
        /// <see cref="Size"/> property.
        /// </summary>
        private const string XmlSizeAttr = "size";

        /// <summary>
        /// The name of the xml attribute containing the data for the
        /// <see cref="SizeVariable"/> property.
        /// </summary>
        private const string XmlSizeVariableAttr = "sizeVar";

        /// <summary>
        /// The name of the xml attribute containing the data for the
        /// <see cref="Type"/> property.
        /// </summary>
        private const string XmlTypeAttr = "type";

        /// <summary>
        /// The name of the xml attribute containing the data for the
        /// <see cref="ValuesPerLine"/> property.
        /// </summary>
        private const string XmlValuesPerLineAttr = "xValuesPerLine";

        /// <summary>
        /// The private field used for the <see cref="AddGroupWidgets"/> property.
        /// </summary>
        private string _addGroupWidgets;

        /// <summary>
        /// The private field used for the <see cref="ElementType"/> property.
        /// </summary>
        private IMember _elementType;

        /// <summary>
        /// The private field used for the <see cref="IndexBits"/> property.
        /// </summary>
        private string _indexBits;

        /// <summary>
        /// The private field used for the <see cref="Size"/> property.
        /// </summary>
        private string _size;

        /// <summary>
        /// The private field used for the <see cref="SizeVariable"/> property.
        /// </summary>
        private string _sizeVariable;

        /// <summary>
        /// The private field used for the <see cref="Type"/> property.
        /// </summary>
        private string _type;

        /// <summary>
        /// The private field used for the <see cref="ValuesPerLine"/> property.
        /// </summary>
        private string _valuesPerLine;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ArrayMember"/> class to be one of the
        /// members of the specified structure.
        /// </summary>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        public ArrayMember(IStructure structure)
            : base(structure)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ArrayMember"/> class as a copy of the
        /// specified instance.
        /// </summary>
        /// <param name="other">
        /// The instance to copy.
        /// </param>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        public ArrayMember(ArrayMember other, IStructure structure)
            : base(other, structure)
        {
            this._size = other._size;
            this._addGroupWidgets = other._addGroupWidgets;
            this._type = other._type;
            this._indexBits = other._indexBits;
            this._valuesPerLine = other._valuesPerLine;
            this._sizeVariable = other._sizeVariable;
            this._elementType = other._elementType.Clone();
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ArrayMember"/> class.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        public ArrayMember(XmlReader reader, IStructure structure)
            : base(reader, structure)
        {
            this.Deserialise(reader);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether the child widgets should be displayed.
        /// </summary>
        public bool AddGroupWidgets
        {
            get { return this.Dictionary.To<bool>(this._addGroupWidgets, true); }
            set { this.SetProperty(ref this._addGroupWidgets, value.ToString()); }
        }

        /// <summary>
        /// Gets a value indicating whether a instance of this array can be set to NULL.
        /// </summary>
        public bool CanBeNull
        {
            get
            {
                if (this.Type == ArrayType.Pointer)
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// Gets or sets the member that is used as the item to this array.
        /// </summary>
        public IMember ElementType
        {
            get { return this._elementType; }
            set { this.SetProperty(ref this._elementType, value); }
        }

        /// <summary>
        /// Gets a value indicating whether a instance of this array is fixed to a specific
        /// number of items or not.
        /// </summary>
        public bool HasFixedLength
        {
            get
            {
                if (this.Size > 0)
                {
                    return true;
                }

                ArrayType type = this.Type;
                if (type == ArrayType.AtArray)
                {
                    return false;
                }

                if (type == ArrayType.Pointer)
                {
                    return false;
                }

                return true;
            }
        }

        /// <summary>
        /// Gets or sets the number of bits that can be used to store the size of this array.
        /// This can be seen as the size limit of any instance of this array.
        /// </summary>
        public int IndexBits
        {
            get { return this.Dictionary.To<int>(this._indexBits, 16); }
            set { this.SetProperty(ref this._indexBits, value.ToString()); }
        }

        /// <summary>
        /// Gets or sets the length this array has to have. A value of 0 for a dynamic limit.
        /// </summary>
        public ushort Size
        {
            get { return this.Dictionary.To<ushort>(this._size, 0); }
            set { this.SetProperty(ref this._size, value.ToString()); }
        }

        /// <summary>
        /// Gets or sets the name of the sibling member that should be used to store the size
        /// of this array in.
        /// </summary>
        public string SizeVariable
        {
            get { return this._sizeVariable; }
            set { this.SetProperty(ref this._sizeVariable, value); }
        }

        /// <summary>
        /// Gets or sets the type of array this array member represents in the c++ code.
        /// </summary>
        public ArrayType Type
        {
            get { return this.GetTypeFromString(this._type); }
            set { this.SetProperty(ref this._type, this.GetStringFromType(value)); }
        }

        /// <summary>
        /// Gets the type name for this member.
        /// </summary>
        public override string TypeName
        {
            get { return "array"; }
        }

        /// <summary>
        /// Gets or sets the number of scalar values that should be serialised per line when
        /// any instance of this array gets serialised to a metadata file.
        /// </summary>
        public int ValuesPerLine
        {
            get { return this.Dictionary.To<ushort>(this._valuesPerLine, 1); }
            set { this.SetProperty(ref this._valuesPerLine, value.ToString()); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new <see cref="ArrayMember"/> that is a copy of this instance.
        /// </summary>
        /// <returns>
        /// A new <see cref="ArrayMember"/> that is a copy of this instance.
        /// </returns>
        public new ArrayMember Clone()
        {
            return new ArrayMember(this, this.Structure);
        }

        /// <summary>
        /// Creates a <see cref="ITunable"/> object that represents a instance of this member
        /// with it's default value set.
        /// </summary>
        /// <param name="parent">
        /// The tunable parent the new tunable will belong to.
        /// </param>
        /// <returns>
        /// A parCodeGen tunable that represents a instance of this member.
        /// </returns>
        public override ITunable CreateTunable(ITunableParent parent)
        {
            return new ArrayTunable(this, parent);
        }

        /// <summary>
        /// Creates a <see cref="ITunable"/> object that represents a instance of this member
        /// using the specified Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The Xml.XmlReader that provides the data used to initialise the new tunable.
        /// </param>
        /// <param name="parent">
        /// The tunable parent the new tunable will belong to.
        /// </param>
        /// <param name="log">
        /// A object that is used to log any warnings or errors produced during the parsing of
        /// the specified reader.
        /// </param>
        /// <returns>
        /// A parCodeGen tunable that represents a instance of this member.
        /// </returns>
        public override ITunable CreateTunable(
            XmlReader reader, ITunableParent parent, ILog log)
        {
            return new ArrayTunable(reader, this, parent, log);
        }

        /// <summary>
        /// Creates a deep copy of the current instance.
        /// </summary>
        /// <returns>
        /// A deep copy of the current System.Object.
        /// </returns>
        public override object DeepClone()
        {
            return this.Clone();
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified
        /// <see cref="ArrayMember"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public bool Equals(ArrayMember other)
        {
            if (other == null)
            {
                return false;
            }

            return object.ReferenceEquals(other, this);
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="IMember"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public override bool Equals(IMember other)
        {
            return this.Equals(other as ArrayMember);
        }

        /// <summary>
        /// Writes the xml representation of this instance onto the specified
        /// System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that the xml representation of this instance should be
        /// written to.
        /// </param>
        public override void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                throw new SmartArgumentNullException(() => writer);
            }

            this.SerialiseBaseAttributes(writer);
            if (this._type != null)
            {
                writer.WriteAttributeString(XmlTypeAttr, this._type);
            }

            if (this._size != null)
            {
                writer.WriteAttributeString(XmlSizeAttr, this._size);
            }

            if (this._addGroupWidgets != null)
            {
                writer.WriteAttributeString(XmlGroupWidgetsAttr, this._addGroupWidgets);
            }

            if (this._indexBits != null)
            {
                writer.WriteAttributeString(XmlIndexBitsAttr, this._indexBits);
            }

            if (this._sizeVariable != null)
            {
                writer.WriteAttributeString(XmlSizeVariableAttr, this._sizeVariable);
            }

            if (this._valuesPerLine != null)
            {
                writer.WriteAttributeString(XmlValuesPerLineAttr, this._valuesPerLine);
            }

            if (this.ElementType == null)
            {
                return;
            }

            writer.WriteStartAttribute(MemberFactory.GetNodeNameForMember(this.ElementType));
            this.ElementType.Serialise(writer);
            writer.WriteEndElement();
        }

        /// <summary>
        /// Validates this entity and creates a validation result object containing the errors
        /// and warnings.
        /// </summary>
        /// <param name="recursive">
        /// A value indicating whether any child models to this one should also be validated.
        /// </param>
        /// <returns>
        /// A validation result object containing all of the errors and warnings associated
        /// with this validation pass.
        /// </returns>
        public override ValidationResult Validate(bool recursive)
        {
            ValidationResult result = new ValidationResult();
            this.ValidateBaseProperties(result);

            if (this._type == null)
            {
                string msg = StringTable.MissingRequiredAttributeError;
                msg = msg.FormatCurrent("type", "array");
                result.AddError(msg, this.Location);
            }
            else if (String.IsNullOrWhiteSpace(this._type))
            {
                string msg = StringTable.EmptyRequiredAttributeError;
                msg = msg.FormatCurrent("type", "array");
                result.AddError(msg, this.Location);
            }
            else if (this.Type == ArrayType.Unrecognised)
            {
                string msg = StringTable.UnrecognisedRequiredAttributeError;
                msg = msg.FormatCurrent("type", "array", this._type);
                result.AddError(msg, this.Location);
            }

            TryResult<ushort> sizeTry = this.Dictionary.TryTo<ushort>(this._size, 0);
            if (this.Type == ArrayType.Fixed)
            {
                if (this._size == null || !sizeTry.Success)
                {
                    string msg = StringTable.FormatAttributeWarning;
                    msg = msg.FormatCurrent(XmlSizeAttr, this._size, "u16");
                    result.AddError(msg, this.Location);
                }
            }
            else if (this.Type == ArrayType.Range)
            {
                if (this._size == null || !sizeTry.Success)
                {
                    string msg = StringTable.FormatAttributeWarning;
                    msg = msg.FormatCurrent(XmlSizeAttr, this._size, "u16");
                    result.AddError(msg, this.Location);
                }
            }
            else if (this.Type == ArrayType.Member)
            {
                if (this._size == null || !sizeTry.Success)
                {
                    string msg = StringTable.FormatAttributeWarning;
                    msg = msg.FormatCurrent(XmlSizeAttr, this._size, "u16");
                    result.AddError(msg, this.Location);
                }
            }
            else if (this.Type == ArrayType.Pointer)
            {
                if (this._size == null && this._sizeVariable == null)
                {
                    string msg = StringTable.ArraySizeInformationMissing;
                    result.AddError(msg, this.Location);
                }
            }
            else
            {
                if (this._size != null && !sizeTry.Success)
                {
                    string msg = StringTable.FormatAttributeWarning;
                    msg = msg.FormatCurrent(XmlSizeAttr, this._size, "u16");
                    result.AddWarning(msg, this.Location);
                }
            }

            if (!this.Dictionary.Validate<bool>(this._addGroupWidgets, true))
            {
                string msg = StringTable.FormatAttributeWarning;
                msg = msg.FormatCurrent(XmlGroupWidgetsAttr, this._addGroupWidgets, "boolean");
                result.AddWarning(msg, this.Location);
            }

            if (this.ElementType == null)
            {
                string msg = StringTable.MissingElementType;
                msg = msg.FormatCurrent("array");
                result.AddError(msg, this.Location);
            }
            else
            {
                result.AddChildResults(this.ElementType.Validate(recursive));
            }

            return result;
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            this._size = reader.GetAttribute(XmlSizeAttr);
            this._addGroupWidgets = reader.GetAttribute(XmlGroupWidgetsAttr);
            this._type = reader.GetAttribute(XmlTypeAttr);
            this._indexBits = reader.GetAttribute(XmlIndexBitsAttr);
            this._valuesPerLine = reader.GetAttribute(XmlValuesPerLineAttr);
            this._sizeVariable = reader.GetAttribute(XmlSizeVariableAttr);
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToElementOrComment())
            {
                if (reader.NodeType == XmlNodeType.Comment)
                {
                    reader.Skip();
                    continue;
                }

                if (this.ElementType == null)
                {
                    this.ElementType = MemberFactory.Create(reader, this.Structure);
                    if (this.ElementType == null)
                    {
                        reader.Skip();
                    }
                    else
                    {
                        this.ElementType.IsSubMember = true;
                    }

                    continue;
                }
                else
                {
                    reader.Skip();
                }
            }

            if (reader.NodeType == XmlNodeType.EndElement)
            {
                reader.ReadEndElement();
            }
        }

        /// <summary>
        /// Gets the string value that is equivalent to the specified type.
        /// </summary>
        /// <param name="type">
        /// The type that the returned string will be equivalent to.
        /// </param>
        /// <returns>
        /// The string that is equivalent to the specified type.
        /// </returns>
        private string GetStringFromType(ArrayType type)
        {
            switch (type)
            {
                case ArrayType.AtArray:
                    return ArrayTypeAtArray;
                case ArrayType.Fixed:
                    return ArrayTypeFixed;
                case ArrayType.Range:
                    return ArrayTypeRanged;
                case ArrayType.Pointer:
                    return ArrayTypePointer;
                case ArrayType.Member:
                    return ArrayTypeMember;
                case ArrayType.Unknown:
                case ArrayType.Unrecognised:
                default:
                    return null;
            }
        }

        /// <summary>
        /// Gets the array type that is equivalent to the specified String.
        /// </summary>
        /// <param name="type">
        /// The string to determine the type to return.
        /// </param>
        /// <returns>
        /// The type that is equivalent to the specified string.
        /// </returns>
        private ArrayType GetTypeFromString(string type)
        {
            if (type == null)
            {
                return ArrayType.Unknown;
            }
            else if (String.Equals(type, ArrayTypeAtArray))
            {
                return ArrayType.AtArray;
            }
            else if (String.Equals(type, ArrayTypeFixed))
            {
                return ArrayType.Fixed;
            }
            else if (String.Equals(type, ArrayTypeRanged))
            {
                return ArrayType.Range;
            }
            else if (String.Equals(type, ArrayTypePointer))
            {
                return ArrayType.Pointer;
            }
            else if (String.Equals(type, ArrayTypeMember))
            {
                return ArrayType.Member;
            }
            else
            {
                return ArrayType.Unrecognised;
            }
        }
        #endregion Methods
    } // RSG.Metadata.Model.Definitions.Members.ArrayMember {Class}
} // RSG.Metadata.Model.Definitions.Members {Namespace}
