﻿using System;

namespace RSG.Model.Map.Filters
{

    /// <summary>
    /// Common level filter methods.
    /// </summary>
    public static class LevelFilters
    {
        /// <summary>
        /// Returns true for all levels so that everything gets loaded
        /// </summary>
        /// <param name="mapNode"></param>
        /// <returns></returns>
        public static Boolean All(RSG.Base.ConfigParser.ContentNodeLevel levelNode)
        {
            return true;
        }

        /// <summary>
        /// Returns true for the level named "gta5"
        /// </summary>
        /// <param name="mapNode"></param>
        /// <returns></returns>
        public static Boolean Gta5(RSG.Base.ConfigParser.ContentNodeLevel levelNode)
        {
            if (levelNode.Name == "gta5") return true;

            return false;
        }
    }

} // RSG.Model.Map.Filters namespace
