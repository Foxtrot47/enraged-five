﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using RSG.Base.Extensions;
using System.Runtime.Serialization;

namespace RSG.Model.Common
{
    /// <summary>
    /// Base class for assets that are also streamable.
    /// </summary>
    [DataContract]
    [Serializable]
    [KnownType(typeof(HashSet<StreamableStat>))]
    public abstract class StreamableAssetBase : AssetBase, IStreamableObject
    {
        #region Static Data
        /// <summary>
        /// List of stats that are valid for this streamable object.
        /// </summary>
        private static readonly IDictionary<Type, ISet<StreamableStat>> s_validStatsPerType;
        #endregion // Static Data

        #region Member Data
        /// <summary>
        /// List of stats that have been loaded for this streamable object.
        /// </summary>
        [DataMember]
        private ISet<StreamableStat> LoadedStats { get; set; }
        #endregion // MemberData

        #region Constructors
        /// <summary>
        /// Default Constructor
        /// </summary>
        public StreamableAssetBase(String name)
            : base(name)
        {
            LoadedStats = new HashSet<StreamableStat>();
        }

        /// <summary>
        /// Static constructor.
        /// </summary>
        static StreamableAssetBase()
        {
            s_validStatsPerType = new Dictionary<Type, ISet<StreamableStat>>();

            // Add an entry into the dictionary for each streamable stat type.
            Type streamableType = typeof(IStreamableObject);
            foreach (Type type in streamableType.GetDerivedTypesInLoadedAssemblies())
            {
                s_validStatsPerType[type] = new HashSet<StreamableStat>();
                s_validStatsPerType[type].AddRange(StreamableStatUtils.GetValuesAssignedToProperties(type));
            }

            // Make sure we catch any additional assemblies being loaded.
            AppDomain.CurrentDomain.AssemblyLoad += CurrentDomain_AssemblyLoad;
        }
        #endregion // Constructors

        #region IStreamableObject Implementation
        /// <summary>
        /// 
        /// </summary>
        public void LoadAllStats()
        {
            LoadStats(GetValidStreamableStats());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="statsToLoad"></param>
        public void LoadStats(IEnumerable<StreamableStat> statsToLoad)
        {
            // Make sure that the requested stats are for this type.
            ISet<StreamableStat> validStats = GetValidStreamableStats();
            foreach (StreamableStat stat in statsToLoad)
            {
                if (!validStats.Contains(stat))
                {
                    String message = String.Format("Requested an invalid streamable stat '{0}' for this type of object.", stat);
                    Debug.Assert(false, message);
                    throw new ArgumentException(message);
                }
            }

            // Remove any stats from the list that we've already loaded
            LoadStatsInternal(statsToLoad.Except(LoadedStats));
        }

        /// <summary>
        /// Request all the statistics for this object.
        /// </summary>
        /// <param name="async"></param>
        public virtual void RequestAllStatistics(bool async)
        {
            RequestStatistics(StreamableStatUtils.GetValuesAssignedToProperties(GetType()), async);
        }

        /// <summary>
        /// Request specific stats for this object.
        /// </summary>
        /// <param name="statsToLoad"></param>
        /// <param name="async"></param>
        public virtual void RequestStatistics(IEnumerable<StreamableStat> statsToLoad, bool async)
        {
            throw new NotSupportedException("This load path has been deprecated.  Should be overridden in derived classes that still rely on this.");
        }

        /// <summary>
        /// Returns whether or not a particular stat has been loaded for this object.
        /// </summary>
        /// <param name="stat"></param>
        /// <returns></returns>
        public bool IsStatLoaded(StreamableStat stat)
        {
            return LoadedStats.Contains(stat);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestedStats"></param>
        /// <returns></returns>
        public bool AreStatsLoaded(IEnumerable<StreamableStat> requestedStats)
        {
            return !StatsRequiredForObject(requestedStats).Any();
        }
        #endregion // IStreamableObject<T> Implementation

        #region Protected Methods
        /// <summary>
        /// TODO: Make this abstract.  They are virtual for now for backwards compatibility.
        /// </summary>
        /// <param name="statsToLoad"></param>
        protected virtual void LoadStatsInternal(IEnumerable<StreamableStat> statsToLoad)
        {
            // Code provided for backwards compatibility only.
            RequestStatistics(statsToLoad, false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="requestedStats"></param>
        /// <returns></returns>
        protected IEnumerable<StreamableStat> StatsRequiredForObject(IEnumerable<StreamableStat> requestedStats)
        {
            foreach (StreamableStat stat in requestedStats)
            {
                if (!IsStatLoaded(stat))
                {
                    yield return stat;
                }
            }
        }

        /// <summary>
        /// Checks whether a particular stat is loaded.  Only active in debug.
        /// </summary>
        /// <param name="stat"></param>
        [Conditional("DEBUG")]
        protected void CheckStatLoaded(StreamableStat stat)
        {
#warning Readd once view model have been sorted out.
            //Debug.Assert(IsStatLoaded(stat), "Attempting to access a stat that hasn't been loaded.");
        }

        /// <summary>
        /// Sets the flag indicating that a particular stat has been loaded.
        /// </summary>
        /// <param name="stat"></param>
        /// <param name="loaded"></param>
        protected void SetStatLodaded(StreamableStat stat, bool loaded)
        {
            if (loaded)
            {
                LoadedStats.Add(stat);
            }
            else
            {
                LoadedStats.Remove(stat);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected ISet<StreamableStat> GetValidStreamableStats()
        {
            Debug.Assert(s_validStatsPerType.ContainsKey(GetType()), "This type of streamable stat isn't in the lookup.");
            return s_validStatsPerType[GetType()];
        }
        #endregion // Protected Methods

        #region Static Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private static void CurrentDomain_AssemblyLoad(object sender, AssemblyLoadEventArgs args)
        {
            Type streamableType = typeof(IStreamableObject);
            foreach (Type type in streamableType.GetDerivedTypeInAssembly(args.LoadedAssembly))
            {
                s_validStatsPerType[type] = new HashSet<StreamableStat>();
                s_validStatsPerType[type].AddRange(StreamableStatUtils.GetValuesAssignedToProperties(type));
            }
        }
        #endregion // Static Event Handlers
    } // StreamableAssetBase
}
