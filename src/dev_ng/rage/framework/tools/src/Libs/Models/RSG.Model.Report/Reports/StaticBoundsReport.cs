﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using RSG.Base.ConfigParser;
using RSG.Model.Common;
using RSG.Model.Map;
using RSG.Model.Map.Statistics;

namespace RSG.Model.Report.Reports
{
    /// <summary>
    /// 
    /// </summary>
    public class StaticBoundsReport : HTMLReport, IDynamicMapReport
    {
        #region Constants
        private const String NAME = "Static Bounds Report";
        private const String DESCRIPTION = "Generate and display a static bounds report.";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Determines whether the map data should be loaded
        /// </summary>
        public bool LoadMapData
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Determines whether the car generators should be loaded
        /// 
        /// </summary>
        public bool LoadCarGenarators
        {
            get { return false; }
        }
        #endregion

        #region Construtcor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public StaticBoundsReport()
            : base(NAME, DESCRIPTION)
        {
        }
        #endregion // Constructor(s)

        /// <summary>
        /// Represents the data to be gathered for each processed map section
        /// </summary>
        internal class ProcessedMapSectionReportData
        {
            internal ProcessedMapSectionReportData()
            {
                NumInputs = 0;
                IsBoundsProcessorOn = false;
                HasBoundsProcessorData = HasBPData.No;
            }

            internal enum HasBPData
            {
                Yes,
                No,
                UnableToDetermine
            }

            internal int NumInputs { get; set; }
            internal bool IsBoundsProcessorOn { get; set; }
            internal HasBPData HasBoundsProcessorData { get; set; }
        }

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        /// <param name="gv"></param>
        public void Generate(ILevel level, ConfigGameView gv, IPlatformCollection platforms)
        {
            // Process data
            List<MapSection> mapSections = RSG.Model.Map.Util.Enumerations.GetAllMapSections(level, null, true);

            // Build the data for our report
            var reportDataMap = new Dictionary<string, ProcessedMapSectionReportData>();
            foreach (MapSection mapSection in mapSections)
            {
                if (mapSection.SectionNode.Outputs.Count() == 1 && mapSection.SectionNode.Outputs.ElementAt(0) is ContentNodeMapProcessedZip)
                {
                    ContentNodeMapProcessedZip processedMapNode = (ContentNodeMapProcessedZip)mapSection.SectionNode.Outputs.ElementAt(0);
                    if (!reportDataMap.ContainsKey(processedMapNode.Name))
                    {
                        reportDataMap.Add(processedMapNode.Name, new ProcessedMapSectionReportData());
                    }

                    ++reportDataMap[processedMapNode.Name].NumInputs;

                    if (mapSection.SectionNode.ViaBoundsProcessor)
                    {
                        reportDataMap[processedMapNode.Name].IsBoundsProcessorOn = true;
                        try
                        {
                            using (Ionic.Zip.ZipFile mapzip = Ionic.Zip.ZipFile.Read(mapSection.ZipFilename))
                            {
                                string staticBoundsEntryName = mapSection.Name + "_static_bounds.ibn.zip";
                                if (mapzip.ContainsEntry(staticBoundsEntryName))
                                {
                                    reportDataMap[processedMapNode.Name].HasBoundsProcessorData = ProcessedMapSectionReportData.HasBPData.Yes;
                                }
                            }
                        }
                        catch (System.IO.FileNotFoundException)
                        {
                            reportDataMap[processedMapNode.Name].HasBoundsProcessorData = ProcessedMapSectionReportData.HasBPData.UnableToDetermine;
                        }
                    }
                    else
                    {
                        reportDataMap[processedMapNode.Name].IsBoundsProcessorOn = false;
                        reportDataMap[processedMapNode.Name].HasBoundsProcessorData = ProcessedMapSectionReportData.HasBPData.No;
                    }
                }
            }

            // Write report
            var memoryStream = new MemoryStream();
            var streamWriter = new StreamWriter(memoryStream);
            var htmlWriter = new HtmlTextWriter(streamWriter);
            
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.Html);
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.Head);
            htmlWriter.RenderEndTag();
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.Body);

            htmlWriter.RenderBeginTag(HtmlTextWriterTag.H1);
            htmlWriter.Write("Static Bounds Report");
            htmlWriter.RenderEndTag();

            int numberOfProcessedMaps = reportDataMap.Count;
            int numberOfProcessedMapsWithBPOn = reportDataMap.Values.Where(promap => promap.IsBoundsProcessorOn).Count();
            int numberOfProcessedMapsWithBPActive = reportDataMap.Values.Where(promap => promap.HasBoundsProcessorData == ProcessedMapSectionReportData.HasBPData.Yes).Count();
            decimal bpOnPercentage = (decimal)numberOfProcessedMapsWithBPOn / (decimal)numberOfProcessedMaps * 100.0m;
            decimal bpActivePercentage = (decimal)numberOfProcessedMapsWithBPActive / (decimal)numberOfProcessedMapsWithBPOn * 100.0m;
            htmlWriter.WriteLine("{0}% of the processed maps have the bounds processor turned on ({1} of {2}).", 
                bpOnPercentage.ToString("0.00"), numberOfProcessedMapsWithBPOn, numberOfProcessedMaps);
            htmlWriter.WriteLine("{0}% of the processed maps enabled have active bounds processor data ({1} of {2}).",
                bpActivePercentage.ToString("0.00"), numberOfProcessedMapsWithBPActive, numberOfProcessedMapsWithBPOn);

            htmlWriter.AddAttribute(HtmlTextWriterAttribute.Border, "1");
            htmlWriter.AddAttribute(HtmlTextWriterAttribute.Width, "40%");
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.Table);
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.Tr);
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.Th);
            htmlWriter.Write("Processed Section");
            htmlWriter.RenderEndTag();
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.Th);
            htmlWriter.Write("Num Inputs");
            htmlWriter.RenderEndTag();
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.Th);
            htmlWriter.Write("BP On");
            htmlWriter.RenderEndTag();
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.Th);
            htmlWriter.Write("BP Active");
            htmlWriter.RenderEndTag();
            htmlWriter.RenderEndTag();

            foreach (KeyValuePair<string, ProcessedMapSectionReportData> reportDataPair in reportDataMap)
            {
                htmlWriter.RenderBeginTag(HtmlTextWriterTag.Tr);

                htmlWriter.RenderBeginTag(HtmlTextWriterTag.Td);
                htmlWriter.Write(String.Format("{0}    ", reportDataPair.Key));
                htmlWriter.RenderEndTag();

                htmlWriter.RenderBeginTag(HtmlTextWriterTag.Td);
                htmlWriter.Write(reportDataPair.Value.NumInputs.ToString());
                htmlWriter.RenderEndTag();

                htmlWriter.RenderBeginTag(HtmlTextWriterTag.Td);
                htmlWriter.Write(String.Format("{0}", reportDataPair.Value.IsBoundsProcessorOn ? "Yes" : "No"));
                htmlWriter.RenderEndTag();

                htmlWriter.RenderBeginTag(HtmlTextWriterTag.Td);
                htmlWriter.Write(reportDataPair.Value.HasBoundsProcessorData.ToString());
                htmlWriter.RenderEndTag();

                htmlWriter.RenderEndTag();
            }

            htmlWriter.RenderEndTag();// table end

            htmlWriter.RenderEndTag();
            htmlWriter.RenderEndTag();
            htmlWriter.Flush();

            memoryStream.Position = 0;
            Stream = memoryStream;
        }
        #endregion // Controller Methods
    } // StaticBoundsReport
}
