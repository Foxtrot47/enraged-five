﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.Base.ConfigParser;
using System.IO;
using RSG.Statistics.Common.Dto.GameAssets;
using RSG.Statistics.Client.ServiceClients;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Diagnostics;
using RSG.Base.Logging;
using RSG.Base.Configuration;
using RSG.Model.Asset.GameText;
using RSG.Model.Common.GameText;
using RSG.Model.Asset.Util;

namespace RSG.Model.Weapon
{
    /// <summary>
    /// 
    /// </summary>
    public class WeaponFactory : IWeaponFactory
    {
        #region Constants
        /// <summary>
        /// Location of the weapons.meta file from which to source the local data weapon collection list
        /// </summary>
        private const String _weaponsMetaFile = @"$(common)\data\ai\weapons.meta";
        #endregion // Constants

        #region Member Data
        /// <summary>
        /// Branch we are working off of.
        /// </summary>
        private IBranch _branch;

        /// <summary>
        /// Weapon factory log.
        /// </summary>
        private static ILog _log = LogFactory.CreateUniversalLog("WeaponFactory");
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        [Obsolete("Use the IBranch version of the constructor.")]
        public WeaponFactory()
            : this(ConfigFactory.CreateConfig())
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="config"></param>
        [Obsolete("Use the IBranch version of the constructor.")]
        public WeaponFactory(IConfig config)
            : this(config.CoreProject.DefaultBranch)
        {
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="branch"></param>
        [Obsolete("Use the static controller method instead.")]
        public WeaponFactory(IBranch branch)
        {
            _branch = branch;
        }
        #endregion // Constructor(s)

        #region IWeaponFactory Implementation
        /// <summary>
        /// Creates a new weapon collection from the specified source.
        /// </summary>
        /// <param name="source">Source to use for creating the collection.</param>
        /// <param name="buildIdentifier">Optional build identifier for when the source is Database.</param>
        /// <returns></returns>
        [Obsolete("Use the static method instead.")]
        public IWeaponCollection CreateCollection(DataSource source, string buildIdentifier = null)
        {
            IWeaponCollection collection = null;

            if (source == DataSource.SceneXml)
            {
                _log.Profile("WeaponFactory:CreateCollection:SceneXml");
                collection = CreateCollectionFromExportData(_branch);
                _log.ProfileEnd();
            }
            else if (source == DataSource.Database)
            {
                _log.Profile("WeaponFactory:CreateCollection:Database");
                collection = CreateCollectionFromDatabase(buildIdentifier);
                _log.ProfileEnd();
            }
            else
            {
                Debug.Assert(false, String.Format("Unknown data source '{0}' specified while creating a weapon collection.", source.ToString()));
                throw new ArgumentException(String.Format("Unknown data source '{0}' specified while creating a weapon collection.", source.ToString()));
            }

            return collection;
        }
        #endregion // IWeaponFactory Implementation

        #region Static Controller Methods
        /// <summary>
        /// Creates a new weapon collection from the specified source.
        /// </summary>
        /// <param name="source">Source to use for creating the collection.</param>
        /// <param name="buildIdentifier">Optional build identifier for when the source is Database.</param>
        /// <returns></returns>
        public static IWeaponCollection CreateCollection(DataSource source, IBranch branch = null, String buildIdentifier = null)
        {
            IWeaponCollection collection = null;

            if (source == DataSource.SceneXml)
            {
                Debug.Assert(branch != null, "Branch must be supplied when creating a weapon collection from SceneXml.");
                if (branch == null)
                {
                    throw new ArgumentNullException("branch", "Branch must be supplied when creating a weapon collection from SceneXml.");
                }

                using (ProfileContext ctx = new ProfileContext(_log, "WeaponFactory:CreateCollection:SceneXml"))
                {
                    collection = CreateCollectionFromExportData(branch);
                }
            }
            else if (source == DataSource.Database)
            {
                Debug.Assert(String.IsNullOrEmpty(buildIdentifier), "Build identifier must be supplied when creating a weapon collection from SceneXml.");
                if (branch == null)
                {
                    throw new ArgumentNullException("buildIdentifier", "Build identifier must be supplied when creating a weapon collection from SceneXml.");
                }

                using (ProfileContext ctx = new ProfileContext(_log, "WeaponFactory:CreateCollection:Database"))
                {
                    collection = CreateCollectionFromDatabase(buildIdentifier);
                }
            }
            else
            {
                Debug.Fail(String.Format("Unknown data source '{0}' specified while creating a weapon collection.", source.ToString()));
                throw new ArgumentException(String.Format("Unknown data source '{0}' specified while creating a weapon collection.", source.ToString()));
            }

            return collection;
        }
        #endregion // Static Controller Methods

        #region Private Methods
        /// <summary>
        /// Creates a new weapon collection based off of the weapons in the weapons.meta file.
        /// </summary>
        private static IWeaponCollection CreateCollectionFromExportData(IBranch branch)
        {
            String exportPath = Path.Combine(branch.Assets, "weapons");
            LocalWeaponCollection weapons = new LocalWeaponCollection(exportPath);

            GameTextCollection gameText = new GameTextCollection(branch);
            gameText.LoadLanguage(Language.American);

            // Loop over the list of files that contain weapon information.
            foreach (String weaponInfoFile in GetWeaponInfoFilenames(branch))
            {
                if (File.Exists(weaponInfoFile))
                {
                    try
                    {
                        // Parse the file.
                        XDocument xmlDoc = XDocument.Load(weaponInfoFile);
                        foreach (XElement itemElem in xmlDoc.XPathSelectElements("//Item[@type='CWeaponInfo']"))
                        {
                            // Try and get the weapon name from the localisation files if it is present otherwise from the game name.
                            WeaponMetadataInfo metadataInfo = LocalWeapon.ExtractDataFromMetadataElement(itemElem);

                            if (metadataInfo.GameName != null)
                            {
                                String key = metadataInfo.HumanNameHash.ToLower();

                                // WT_INVALID check is a bit of a hack so that we don't display Invalid as a friendly name for some items.
                                String friendlyName = null;
                                if (key != "wt_invalid")
                                {
                                    gameText.TryGetText(key, Language.American, RSG.Platform.Platform.PS3, out friendlyName);
                                }

                                IWeapon weapon = new LocalWeapon(metadataInfo.GameName, friendlyName,
                                    metadataInfo.ModelName, metadataInfo.StatName, metadataInfo.Category, weapons);
                                weapons.Add(weapon);
                            }
                        }
                    }
                    catch (System.Exception ex)
                    {
                        _log.ToolException(ex, "Unhandled exception occurred while creating the weapons collection for branch {0}.", branch.Name);
                    }
                }
                else
                {
                    _log.Warning("{0} file doesn't exist.", weaponInfoFile);
                }
            }

            return weapons;
        }

        /// <summary>
        /// Get the list of files we need to parse that contain weapon information.
        /// </summary>
        /// <param name="branch"></param>
        /// <returns></returns>
        public static IEnumerable<String> GetWeaponInfoFilenames(IBranch branch)
        {
            // Check what kind of project we are in to determine the list of files to parse.
            if (branch.Project.IsDLC)
            {
                foreach (String weaponInfoFile in DLCContentUtil.GetFilenames(branch, "WEAPONINFO_FILE"))
                {
                    yield return weaponInfoFile;
                }
            }
            else
            {
                yield return branch.Environment.Subst(_weaponsMetaFile);
            }
        }

        /// <summary>
        /// Creates a new weapon collection based off the information in the database for the specified build.
        /// </summary>
        private static IWeaponCollection CreateCollectionFromDatabase(String buildIdentifier)
        {
            IWeaponCollection weapons = null;

            try
            {
                using (BuildClient client = new BuildClient())
                {
                    // If no build identifier was supplied, use the latest build.
                    if (buildIdentifier == null)
                    {
                        BuildDto dto = client.GetLatest();
                        if (dto != null)
                        {
                            buildIdentifier = dto.Identifier;
                        }
                    }

                    // Check that the identifier is set.
                    if (buildIdentifier != null)
                    {
                        weapons = new SingleBuildWeaponCollection(buildIdentifier);

                        foreach (WeaponDto dto in client.GetAllWeaponsForBuild(buildIdentifier).Items)
                        {
                            Weapon weapon = new DbWeapon(dto, weapons);
                            weapons.Add(weapon);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.ToolException(ex, "Unhandled exception occurred while creating a weapon collection from the database.");
            }

            return weapons;
        }
        #endregion // Private Methods
    } // WeaponFactory
} // RSG.Model.Weapon
