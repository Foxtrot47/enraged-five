﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Map.SceneOverride
{
    /// <summary>
    /// A class representing a entity delete item as serialised by the game
    /// </summary>
    public class RuntimeEntityDeleteItem
    {
        public RuntimeEntityDeleteItem(uint hash, uint guid, float posX, float posY, float posZ, String imapName, String modelName)
        {
            Guid = guid;
            Hash = hash;
            PosX = posX;
            PosY = posY;
            PosZ = posZ;
            IMAPName = imapName;
            ModelName = modelName;
        }

        public uint Guid { get; private set; }
        public uint Hash { get; private set; }
        public float PosX { get; private set; }
        public float PosY { get; private set; }
        public float PosZ { get; private set; }
        public String IMAPName { get; private set; }
        public String ModelName { get; private set; }
    }
}
