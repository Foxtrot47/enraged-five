﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Dialogue.Export
{
    /// <summary>
    /// Key extractor event handler delegate.
    /// </summary>
    /// <param name="sender">Sender.</param>
    /// <param name="e">Key extractor event arguments.</param>
    public delegate void  KeyExtractorEventHandler(object sender, KeyExtractorEventArgs e);
}
