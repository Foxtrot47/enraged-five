﻿using System;
using RSG.Base.Editor;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace RSG.Model.Common
{
    /// <summary>
    /// Asset interface.
    /// </summary>
    public interface IAsset : IModel, IComparable<IAsset>, IComparable
    {
        #region Properties
        /// <summary>
        /// The name of the asset
        /// </summary>
        String Name { get; }

        /// <summary>
        /// Unique hash for this asset
        /// </summary>
        UInt32 Hash { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        List<IAsset> Find(Regex expression);
        #endregion
    } // IAsset
} // RSG.Model.Common namespace
