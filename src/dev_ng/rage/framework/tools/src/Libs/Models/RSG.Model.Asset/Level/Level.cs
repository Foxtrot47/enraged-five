﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.IO;
using System.Windows.Media.Imaging;
using System.Xml;
using System.Xml.XPath;
using RSG.Base.Math;
using RSG.Base.ConfigParser;
using RSG.Base.Logging;
using RSG.Base.Collections;
using RSG.Interop.Bugstar;
using RSG.Interop.Bugstar.Organisation;
using RSG.Interop.Bugstar.Game;
using RSG.Model.Common;
using RSG.Model.Common.Extensions;
using RSG.Model.Common.Map;
using RSG.Statistics.Common.Dto.GameAssetStats;
using System.Reflection;

namespace RSG.Model.Asset.Level
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class Level : StreamableAssetBase, ILevel
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// The collection this level is a part of
        /// </summary>
        [Browsable(false)]
        public ILevelCollection ParentCollection
        {
            get;
            protected set;
        }

        /// <summary>
        /// Represents whether this level has had it's map hierarchy
        /// added to it through the use of the config parser.
        /// </summary>
        public bool Initialised
        {
            get { return m_initialised; }
            set
            {
                SetPropertyValue(value, () => Initialised,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_initialised = (Boolean)newValue;
                        }
                ));
            }
        }
        private bool m_initialised;

        /// <summary>
        /// Level map image
        /// </summary>
        [StreamableStat(StreamableLevelStat.BackgroundImageString)]
        public BitmapImage Image
        {
            get
            {
                CheckStatLoaded(StreamableLevelStat.BackgroundImage);
                return m_image;
            }
            protected set
            {
                SetPropertyValue(value, () => Image,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_image = (BitmapImage)newValue;
                        }
                ));
                SetStatLodaded(StreamableLevelStat.BackgroundImage, true);
            }
        }
        private BitmapImage m_image;

        /// <summary>
        /// Level map image bounding box, basically the
        /// game world-space bounding box
        /// </summary>
        [StreamableStat(StreamableLevelStat.ImageBoundsString)]
        public BoundingBox2f ImageBounds
        {
            get
            {
                CheckStatLoaded(StreamableLevelStat.ImageBounds);
                return m_imageBounds;
            }
            protected set
            {
                SetPropertyValue(value, () => ImageBounds,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_imageBounds = (BoundingBox2f)newValue;
                            SetStatLodaded(StreamableLevelStat.ImageBounds, newValue != null);
                        }
                ));
            }
        }
        private BoundingBox2f m_imageBounds;

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        [StreamableStat(StreamableLevelStat.SpawnPointsString)]
        public IList<ISpawnPoint> SpawnPoints
        {
            get
            {
                CheckStatLoaded(StreamableLevelStat.SpawnPoints);
                return m_spawnPoints;
            }
            protected set
            {
                SetPropertyValue(value, m_spawnPoints, () => SpawnPoints,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_spawnPoints = (IList<ISpawnPoint>)newValue;
                            SetStatLodaded(StreamableLevelStat.SpawnPoints, newValue != null);
                        }
                ));
            }
        }
        private IList<ISpawnPoint> m_spawnPoints;

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        [StreamableStat(StreamableLevelStat.SpawnPointClustersString)]
        public IList<ISpawnPointCluster> SpawnPointClusters
        {
            get
            {
                CheckStatLoaded(StreamableLevelStat.SpawnPointClusters);
                return m_spawnPointClusters;
            }
            protected set
            {
                SetPropertyValue(value, m_spawnPointClusters, () => SpawnPointClusters,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_spawnPointClusters = (IList<ISpawnPointCluster>)newValue;
                            SetStatLodaded(StreamableLevelStat.SpawnPointClusters, newValue != null);
                        }
                ));
            }
        }
        private IList<ISpawnPointCluster> m_spawnPointClusters;

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        [StreamableStat(StreamableLevelStat.ChainingGraphsString)]
        public IList<IChainingGraph> ChainingGraphs
        {
            get
            {
                CheckStatLoaded(StreamableLevelStat.ChainingGraphs);
                return m_chainingGraphs;
            }
            protected set
            {
                SetPropertyValue(value, m_chainingGraphs, () => ChainingGraphs,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_chainingGraphs = (IList<IChainingGraph>)newValue;
                            SetStatLodaded(StreamableLevelStat.ChainingGraphs, newValue != null);
                        }
                ));
            }
        }
        private IList<IChainingGraph> m_chainingGraphs;

        /// <summary>
        /// Level export data path.
        /// </summary>
        [StreamableStat(StreamableLevelStat.ExportDataPathString)]
        public String ExportDataPath
        {
            get
            {
                CheckStatLoaded(StreamableLevelStat.ExportDataPath);
                return m_exportDataPath;
            }
            protected set
            {
                SetPropertyValue(value, m_exportDataPath, () => ExportDataPath,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_exportDataPath = (String)newValue;
                        }
                ));
                SetStatLodaded(StreamableLevelStat.ExportDataPath, true);
            }
        }
        private String m_exportDataPath;

        /// <summary>
        /// Level processed data path.
        /// </summary>
        [StreamableStat(StreamableLevelStat.ProcessedDataPathString)]
        public String ProcessedDataPath
        {
            get
            {
                CheckStatLoaded(StreamableLevelStat.ProcessedDataPath);
                return m_processedDataPath;
            }
            protected set
            {
                SetPropertyValue(value, m_processedDataPath, () => ProcessedDataPath,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_processedDataPath = (String)newValue;
                        }
                ));
                SetStatLodaded(StreamableLevelStat.ProcessedDataPath, true);
            }
        }
        private String m_processedDataPath;
        #endregion // Properties and Associated Member Data

        #region Level Components
        /// <summary>
        /// Map hierarchy data (i.e. map areas/map sections).
        /// </summary>
        [StreamableStat(StreamableLevelStat.MapHierarchyString)]
        public IMapHierarchy MapHierarchy
        {
            get { return m_mapHierarchy; }
            set
            {
                SetPropertyValue(ref m_mapHierarchy, value, "MapHierarchy");
                SetStatLodaded(StreamableLevelStat.MapHierarchy, true);
            }
        }
        private IMapHierarchy m_mapHierarchy;

        /// <summary>
        /// Collection of characters.
        /// </summary>
        [StreamableStat(StreamableLevelStat.MapHierarchyString)]
        public ICharacterCollection Characters
        {
            get { return m_characters; }
            set
            {
                SetPropertyValue(ref m_characters, value, "Characters");
                SetStatLodaded(StreamableLevelStat.Characters, true);
            }
        }
        private ICharacterCollection m_characters;

        /// <summary>
        /// Collection of vehicles.
        /// </summary>
        [StreamableStat(StreamableLevelStat.VehiclesString)]
        public IVehicleCollection Vehicles
        {
            get { return m_vehicles; }
            set
            {
                SetPropertyValue(ref m_vehicles, value, "Vehicles");
                SetStatLodaded(StreamableLevelStat.Vehicles, true);
            }
        }
        private IVehicleCollection m_vehicles;

        /// <summary>
        /// Collection of weapons.
        /// </summary>
        [StreamableStat(StreamableLevelStat.WeaponsString)]
        public IWeaponCollection Weapons
        {
            get { return m_weapons; }
            set
            {
                SetPropertyValue(ref m_weapons, value, "Weapons");
                SetStatLodaded(StreamableLevelStat.Weapons, true);
            }
        }
        private IWeaponCollection m_weapons;

        /// <summary>
        /// Collection of geometry stats.
        /// </summary>
        public IGeometryStatsCollection GeometryStats
        {
            get { return m_geometryStatsCollection; }
            set
            {
                SetPropertyValue(ref m_geometryStatsCollection, value, "GeometryStats");
            }
        }
        private IGeometryStatsCollection m_geometryStatsCollection;
        #endregion // Level Components

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        protected Level(String name, ILevelCollection parentCollection)
            : base(name)
        {
            ParentCollection = parentCollection;
        }
        #endregion // Constructor(s)

        #region IComparable<ILevel> Interface
        /// <summary>
        /// Compare this level to another.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(ILevel other)
        {
            if (other == null)
            {
                return 1;
            }

            return (Name.CompareTo(other.Name));
        }
        #endregion // IComparable<ILevel> Interface

        #region IEquatable<ILevel> Interface
        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>true if the current object is equal to the other parameter; otherwise, false.</returns>
        public bool Equals(ILevel other)
        {
            if (other == null)
            {
                return false;
            }

            return (Name == other.Name);
        }
        #endregion // IEquatable<ILevel> Interface

        #region IDisposable Implementation
        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            if (Characters != null)
            {
                Characters.Dispose();
            }

            if (Vehicles != null)
            {
                Vehicles.Dispose();
            }

            if (Weapons != null)
            {
                Weapons.Dispose();
            }
        }
        #endregion // IDisposable Implementation

        #region Object Overrides
        /// <summary>
        /// Compare
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(Object obj)
        {
            if (obj == null)
            {
                return base.Equals(obj);
            }

            return ((obj is ILevel) && Equals(obj as ILevel));
        }

        /// <summary>
        /// Return String representation of the level.
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return Name;
        }

        /// <summary>
        /// Return hash code of level (based on Name).
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return (this.Name.GetHashCode());
        }
        #endregion // Object Overrides
    } // Level
} // RSG.Model.Asset
