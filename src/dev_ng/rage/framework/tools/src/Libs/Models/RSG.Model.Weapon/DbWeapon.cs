﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using RSG.Base.Logging;
using RSG.Model.Asset;
using RSG.Model.Common;
using RSG.Model.Common.Weapon;
using RSG.Statistics.Client.ServiceClients;
using RSG.Statistics.Common.Dto.GameAssets;
using RSG.Statistics.Common.Dto.GameAssets.Bundles;

namespace RSG.Model.Weapon
{
    /// <summary>
    /// 
    /// </summary>
    public class DbWeapon : Weapon
    {
        #region Constructor(s)
        /// <summary>
        /// Constructor from a SceneXml file.
        /// </summary>
        public DbWeapon(WeaponDto dto, IWeaponCollection parentCollection)
            : base(dto.Name, dto.FriendlyName, dto.ModelName, dto.StatName, (WeaponCategory)Enum.Parse(typeof(WeaponCategory), dto.Category), parentCollection)
        {
        }
        #endregion // Constructor(s)

        #region StreamableAssetContainerBase Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="statsToLoad"></param>
        protected override void LoadStatsInternal(IEnumerable<StreamableStat> statsToLoad)
        {
            Debug.Assert(ParentCollection is SingleBuildWeaponCollection, "Parent collection isn't an SingleBuildWeaponCollection.");
            SingleBuildWeaponCollection dbCollection = (SingleBuildWeaponCollection)ParentCollection;

            if (!AreStatsLoaded(statsToLoad))
            {
                try
                {
                    using (BuildClient client = new BuildClient())
                    {
                        WeaponStatBundleDto bundle = client.GetWeaponStat(dbCollection.BuildIdentifier, Hash.ToString());
                        LoadStatsFromDatabase(bundle);
                    }
                }
                catch (Exception e)
                {
                    Log.Log__Exception(e, "Unhandled exception while retrieving weapon stats.");
                }
            }
        }
        #endregion // StreamableAssetContainerBase Overrides

        #region Internal Methods
        /// <summary>
        /// Load all the statistic information from a database object
        /// </summary>
        /// <param name="stats"></param>
        internal void LoadStatsFromDatabase(WeaponStatBundleDto bundle)
        {
            // Load in the basic data
            PolyCount = bundle.PolyCount;
            CollisionCount = bundle.CollisionCount;
            BoneCount = bundle.BoneCount;
            HasLod = bundle.HasLod;
            LodPolyCount = bundle.LodPolyCount;
            GripCount = bundle.GripCount;
            MagazineCount = bundle.MagazineCount;
            AttachmentCount = bundle.AttachmentCount;

            // Load in the shaders/textures
            List<IShader> shaders = new List<IShader>();

            foreach (ShaderStatBundleDto shaderBundle in bundle.ShaderStats)
            {
                shaders.Add(new Shader(shaderBundle));
            }

            shaders.Sort();
            this.Shaders = shaders.ToArray();

            this.AssetChildren.Clear();
            this.AssetChildren.AddRange(Textures);

            // Next load in the platform stats
            PlatformStats = new Dictionary<RSG.Platform.Platform, IWeaponPlatformStat>();

            foreach (WeaponPlatformStatBundleDto platformStatDto in bundle.WeaponPlatformStats)
            {
                RSG.Platform.Platform platform = platformStatDto.Platform.Value;
                IWeaponPlatformStat platformStat = new WeaponPlatformStat(platform, platformStatDto.PhysicalSize, platformStatDto.VirtualSize);
                PlatformStats.Add(platform, platformStat);
            }
        }
        #endregion // Internal Methods
    } // DbWeapon
}
