﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace RSG.Model.Statistics.Captures.Historical
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class HistoricalScriptMemoryResults
    {
        #region Properties
        /// <summary>
        /// Name of the zone these stats are for.
        /// </summary>
        [DataMember]
        public String ZoneName { get; private set; }

        /// <summary>
        /// Mapping of changelist numbers ( or other ) to a dictionary of cpu result keys to cpu results.
        /// </summary>
        [DataMember]
        public IDictionary<Object, ScriptMemResult> Results { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public HistoricalScriptMemoryResults(String zone)
        {
            ZoneName = zone;
            Results = new SortedDictionary<Object, ScriptMemResult>();
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="result"></param>
        public void AddResult(Object context, ScriptMemResult result)
        {
            Results.Add(context, result);
        }
        #endregion // Public Methods
    } // HistoricalScriptMemoryResults
}
