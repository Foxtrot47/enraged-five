﻿//---------------------------------------------------------------------------------------------
// <copyright file="TunableBase.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Tunables
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Linq;
    using System.Xml;
    using RSG.Base.Extensions;
    using RSG.Editor;
    using RSG.Editor.Model;
    using RSG.Metadata.Model.Definitions;
    using RSG.Metadata.Model.Definitions.Members;

    /// <summary>
    /// Serves as the base class for all the type tunables supported by the parCodeGen system.
    /// </summary>
    [DebuggerDisplay("{Member.TypeName} - {Member.Name}")]
    public abstract class TunableBase : ModelBase, ITunable
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="HasDefaultValue"/> property.
        /// </summary>
        private bool _hasDefaultValue;

        /// <summary>
        /// The private field used for the <see cref="InheritanceParent"/> property.
        /// </summary>
        private ITunable _inheritanceParent;

        /// <summary>
        /// The private field used for the <see cref="IsLocked"/> property.
        /// </summary>
        private bool _isLocked;

        /// <summary>
        /// The private field used for the <see cref="LineNumber"/> property.
        /// </summary>
        private int _lineNumber;

        /// <summary>
        /// The private field used for the <see cref="LinePosition"/> property.
        /// </summary>
        private int _linePosition;

        /// <summary>
        /// The private field used for the <see cref="Member"/> property.
        /// </summary>
        private IMember _member;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="TunableBase"/> class to be as instance
        /// of the specified member.
        /// </summary>
        /// <param name="member">
        /// The member that this tunable will be instancing.
        /// </param>
        /// <param name="parent">
        /// The tunable structure this tunable will belong to.
        /// </param>
        protected TunableBase(IMember member, ITunableParent parent)
            : base(parent)
        {
            if (member == null)
            {
                throw new SmartArgumentNullException(() => member);
            }

            this._member = member;
            if (parent is ArrayTunable || parent is MapTunable)
            {
                this._hasDefaultValue = false;
            }
            else
            {
                this._hasDefaultValue = true;
            }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="TunableBase"/> class as a copy of the
        /// specified instance.
        /// </summary>
        /// <param name="other">
        /// The instance to copy.
        /// </param>
        /// <param name="parent">
        /// The tunable structure this tunable will belong to.
        /// </param>
        protected TunableBase(TunableBase other, ITunableParent parent)
            : base(parent)
        {
            if (other == null)
            {
                throw new SmartArgumentNullException(() => other);
            }

            this._member = other._member;
            this._lineNumber = other._lineNumber;
            this._linePosition = other._linePosition;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="TunableBase"/> class using the
        /// specified System.Xml.XmlReader as a initialising data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <param name="member">
        /// The member that this tunable will be instancing.
        /// </param>
        /// <param name="parent">
        /// The tunable structure this tunable will belong to.
        /// </param>
        protected TunableBase(XmlReader reader, IMember member, ITunableParent parent)
            : base(parent)
        {
            if (member == null)
            {
                throw new SmartArgumentNullException(() => member);
            }

            if (reader == null)
            {
                throw new SmartArgumentNullException(() => reader);
            }

            this._member = member;
            IXmlLineInfo info = reader as IXmlLineInfo;
            if (info != null)
            {
                this._lineNumber = info.LineNumber;
                this._linePosition = info.LinePosition;
            }
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the path for the file this instance is defined in.
        /// </summary>
        public string BasePath
        {
            get
            {
                ITunableParent parent = this.Parent as ITunableParent;
                if (parent != null)
                {
                    return parent.BasePath;
                }

                return null;
            }
        }

        /// <summary>
        /// Gets a value indicating whether or not the value for this tunable can be inherited.
        /// </summary>
        public bool CanInheritValue
        {
            get
            {
                if (this.Member == null || this.Member.Structure == null)
                {
                    return false;
                }

                if (this.Member.Structure.UsesDataInheritance)
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this tunable currently has it's default value. This
        /// is the equivalent of having no source data present in the metadata file.
        /// </summary>
        public virtual bool HasDefaultValue
        {
            get
            {
                return this._hasDefaultValue;
            }

            set
            {
                if (this.SetProperty(ref this._hasDefaultValue, value))
                {
                    this.NotifyPropertyChanged("ValueSourceInheritanceLevel");
                    this.Parent.NotifyPropertyChanged("ValueSourceInheritanceLevel");
                }
            }
        }

        /// <summary>
        /// Gets the depth of the inheritance hierarchy this tunable is currently inside.
        /// </summary>
        public int InheritanceHierarchyDepth
        {
            get { return this.TunableStructure.InheritanceHierarchyDepth; }
        }

        /// <summary>
        /// Gets the inheritance level this tunable current resides in. This level represents
        /// the number of inheritance levels above this tunable that currently exist.
        /// </summary>
        public int InheritanceLevel
        {
            get { return this.TunableStructure.InheritanceLevel; }
        }

        /// <summary>
        /// Gets the parent tunable that has been set as the value source of this tunables
        /// value when the value is being inherited.
        /// </summary>
        public ITunable InheritanceParent
        {
            get { return this._inheritanceParent; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this tunable is currently locked from being
        /// edited.
        /// </summary>
        public bool IsLocked
        {
            get
            {
                if (this._isLocked || this.Member.IsLocked)
                {
                    return true;
                }
                
                ITunable parent = this.Parent as ITunable;
                if (parent != null)
                {
                    return parent.IsLocked;
                }

                return false;
            }

            set
            {
                this.SetProperty(ref this._isLocked, value);
            }
        }

        /// <summary>
        /// Gets or sets the line number this instance is defined on or 0 if none is available.
        /// </summary>
        public int LineNumber
        {
            get { return this._lineNumber; }
            set { this.SetProperty(ref this._lineNumber, value); }
        }

        /// <summary>
        /// Gets or sets the line position this instance is defined at or 0 if none is
        /// available.
        /// </summary>
        public int LinePosition
        {
            get { return this._linePosition; }
            set { this.SetProperty(ref this._linePosition, value); }
        }

        /// <summary>
        /// Gets the member that this tunable is currently instancing.
        /// </summary>
        public IMember Member
        {
            get { return this._member; }
        }

        /// <summary>
        /// Gets the tunable structure this instance belongs to.
        /// </summary>
        public TunableStructure TunableStructure
        {
            get
            {
                ITunableParent parent = this.Parent as ITunableParent;
                if (parent != null)
                {
                    return parent.TunableStructure;
                }

                return null;
            }
        }

        /// <summary>
        /// Gets the inheritance level the value for this tunable is currently coming from.
        /// This value goes from the <see cref="InheritanceLevel"/> (no inheritance) to n (the
        /// maximum level). If the value is currently coming from the psc file this property
        /// returns -1.
        /// </summary>
        public int ValueSourceInheritanceLevel
        {
            get
            {
                if (this.HasDefaultValue)
                {
                    if (this._inheritanceParent == null)
                    {
                        return -1;
                    }
                    else
                    {
                        return this._inheritanceParent.ValueSourceInheritanceLevel;
                    }
                }

                return this.InheritanceLevel;
            }
        }

        /// <summary>
        /// Gets the dictionary this member is associated with.
        /// </summary>
        protected IDefinitionDictionary Dictionary
        {
            get { return this.Member != null ? this.Member.Dictionary : null; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new <see cref="ITunable"/> that is a copy of this instance.
        /// </summary>
        /// <returns>
        /// A new <see cref="ITunable"/> that is a copy of this instance.
        /// </returns>
        public new ITunable Clone()
        {
            return base.Clone() as ITunable;
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="ITunable"/> by
        /// looking at their values.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance by their values;
        /// otherwise, false.
        /// </returns>
        public abstract bool EqualByValue(ITunable other);

        /// <summary>
        /// Indicates whether this instance is equal to the specified object.
        /// </summary>
        /// <param name="obj">
        /// The object to compare with this instance.
        /// </param>
        /// <returns>
        /// True if the specified object is equal to this instance; otherwise, false.
        /// </returns>
        public override bool Equals(object obj)
        {
            return this.Equals(obj as ITunable);
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="ITunable"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public abstract bool Equals(ITunable other);

        /// <summary>
        /// Serves as a hash function for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for the current instance.
        /// </returns>
        public override int GetHashCode()
        {
            string toString = this.ToString();
            if (toString == null)
            {
                return base.GetHashCode();
            }

            return toString.GetHashCode();
        }

        /// <summary>
        /// Resets this tunable to have its default value. (i.e no source data).
        /// </summary>
        public void ResetValueToDefault()
        {
            if (this.HasDefaultValue)
            {
                return;
            }

            using (new UndoRedoBatch(this.UndoEngine))
            {
                this.ResetValueToDefaultCore();
                this.HasDefaultValue = true;
            }
        }

        /// <summary>
        /// Resets this tunable to have its default value. (i.e no source data with its initial
        /// values).
        /// </summary>
        public abstract void ResetValueToDefaultCore();

        /// <summary>
        /// Writes the xml representation of this instance onto the specified
        /// System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that the xml representation of this instance should be
        /// written to.
        /// </param>
        /// <param name="serialiseDefaultTunables">
        /// A value indicating whether the tunables that have a default value should be written
        /// to the specified writer or not.
        /// </param>
        public abstract void Serialise(XmlWriter writer, bool serialiseDefaultTunables);

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A string that represents this instance.
        /// </returns>
        public override string ToString()
        {
            if (this.Member != null)
            {
                return "{0} - {1}".FormatInvariant(this.Member.TypeName, this.Member.Name);
            }

            return base.ToString();
        }

        /// <summary>
        /// Sets the source of this tunables value when the value is being inherited.
        /// </summary>
        /// <param name="tunable">
        /// The tunable whose value will be used when this tunable is being inherited.
        /// </param>
        internal void SetInheritanceParent(ITunable tunable)
        {
            if (Object.ReferenceEquals(tunable, this._inheritanceParent))
            {
                return;
            }

            if (Object.ReferenceEquals(tunable, this))
            {
                throw new NotSupportedException("Cannot set a tunable to parent itself.");
            }

            ITunable oldValue = this._inheritanceParent;
            this._inheritanceParent = tunable;
            this.InheritanceParentChanged(oldValue, tunable);

            if (oldValue != null)
            {
                PropertyChangedEventManager.RemoveHandler(
                    oldValue, this.OnInheritLevelChanged, "ValueSourceInheritanceLevel");
                PropertyChangedEventManager.RemoveHandler(
                    oldValue, this.OnInheritLevelChanged, "InheritanceLevel");
            }

            if (tunable != null)
            {
                PropertyChangedEventManager.AddHandler(
                    tunable, this.OnInheritLevelChanged, "ValueSourceInheritanceLevel");
                PropertyChangedEventManager.AddHandler(
                    tunable, this.OnInheritLevelChanged, "InheritanceLevel");
            }

            this.NotifyPropertyChanged("InheritanceLevel");
            this.NotifyPropertyChanged("ValueSourceInheritanceLevel");
        }

        /// <summary>
        /// Called whenever the tunable whose value will be used when this tunable is being
        /// inherited changes.
        /// </summary>
        protected virtual void InheritanceParentChanged(ITunable oldValue, ITunable newValue)
        {
        }

        /// <summary>
        /// Sets the given field to the given value making sure the correct property changed
        /// event is fired.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the field that is getting set.
        /// </typeparam>
        /// <param name="field">
        /// A reference to the field that is getting set.
        /// </param>
        /// <param name="value">
        /// The value to set the field to.
        /// </param>
        /// <param name="comparer">
        /// The equality comparer to use to determine whether the value has changed.
        /// </param>
        /// <param name="names">
        /// A iterator around all of property names that are changed due to this one change.
        /// </param>
        /// <returns>
        /// True if the property was changed; otherwise, false.
        /// </returns>
        protected override bool SetProperty<T>(
            ref T field, T value, IEqualityComparer<T> comparer, IEnumerable<string> names)
        {
            using (new UndoRedoBatch(this.UndoEngine))
            {
                if (!names.Contains("HasDefaultValue"))
                {
                    this.HasDefaultValue = false;
                    if (this.Parent is StructTunable)
                    {
                        ((StructTunable)this.Parent).HasDefaultValue = false;
                    }
                }

                return base.SetProperty<T>(ref field, value, comparer, names);
            }
        }

        /// <summary>
        /// Called if the inherit level or the value source inheritance level change on the
        /// parent.
        /// </summary>
        /// <param name="sender">
        /// The inheritance parent whose values changed.
        /// </param>
        /// <param name="e">
        /// The event data.
        /// </param>
        private void OnInheritLevelChanged(object sender, PropertyChangedEventArgs e)
        {
            this.NotifyPropertyChanged("InheritanceLevel");
            this.NotifyPropertyChanged("ValueSourceInheritanceLevel");
            this.Parent.NotifyPropertyChanged("ValueSourceInheritanceLevel");
        }
        #endregion Methods
    } // RSG.Metadata.Model.Tunables.TunableBase {Class}
} // RSG.Metadata.Model.Tunables {Namespace}
