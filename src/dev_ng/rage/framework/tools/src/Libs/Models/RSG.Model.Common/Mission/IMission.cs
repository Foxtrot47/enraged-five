﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common.Mission
{
    /// <summary>
    /// Interface for mission related objects.
    /// </summary>
    public interface IMission : IAsset
    {
        /// <summary>
        /// Description of the mission
        /// </summary>
        String Description { get; set; }

        /// <summary>
        /// Identifier for the mission.
        /// </summary>
        String MissionId { get; set; }

        /// <summary>
        /// Script for the mission.
        /// </summary>
        String ScriptName { get; set; }

        /// <summary>
        /// Number of attempts we expect this mission to require.
        /// </summary>
        float ProjectedAttempts { get; set; }

        /// <summary>
        /// Minimum number of attempts we expect this mission to require.
        /// </summary>
        float ProjectedAttemptsMin { get; set; }

        /// <summary>
        /// Maximum number of attempts we expect this mission to require.
        /// </summary>
        float ProjectedAttemptsMax { get; set; }

        /// <summary>
        /// Whether this is a singleplayer mission or not.
        /// </summary>
        bool Singleplayer { get; set; }

        /// <summary>
        /// Whether this mission is active or not.
        /// </summary>
        bool Active { get; set; }

        /// <summary>
        /// Whether bugstar knows about mission.
        /// </summary>
        bool InBugstar { get; set; }

        /// <summary>
        /// List of checkpoints this mission has (if any).
        /// </summary>
        IList<IMissionCheckpoint> Checkpoints { get; set; }

        /// <summary>
        /// Category this mission falls under.
        /// </summary>
        MissionCategory Category { get; set; }
    } // IMission
}
