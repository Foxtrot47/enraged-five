﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.IO;
using System.Collections.ObjectModel;

namespace RSG.Model.Report.Reports.GameStatsReports.Model
{
    public class TestSession
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public string Platform
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public ICollection<TestResults> TestResults
        {
            get;
            private set;
        }

        /// <summary>
        /// Time when this test session ran
        /// </summary>
        public DateTime Timestamp
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="timestamp"></param>
        public TestSession(String filename, DateTime timestamp)
        {
            TestResults = new Collection<TestResults>();
            Timestamp = timestamp;

            using (Stream stream = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                Parse(stream);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="timestamp"></param>
        public TestSession(Stream stream, DateTime timestamp)
        {
            TestResults = new Collection<TestResults>();
            Timestamp = timestamp;
            Parse(stream);
        }
        #endregion // TestSession

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="stream"></param>
        private void Parse(Stream stream)
        {
            // Open up the document and start reading in the data it contains
            XDocument doc = XDocument.Load(stream);

            // Extract the platform
            XElement platformElement = doc.Root.Element("platform");
            if (platformElement != null)
            {
                Platform = platformElement.Value;
            }

            // Extract the individual tests
            XElement resultsElement = doc.Root.Element("results");
            if (resultsElement != null)
            {
                foreach (XElement testElement in resultsElement.Elements("Item"))
                {
                    TestResults test = new TestResults(testElement);
                    TestResults.Add(test);
                }
            }
        }
        #endregion // Private Methods
    } // TestSession
}
