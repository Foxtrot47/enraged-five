﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Math;
using RSG.SceneXml;
using RSG.Base.Collections;

namespace RSG.Model.Common.Map
{
    /// <summary>
    /// Interface for interior archetypes.  Note that an interior is solely made up of rooms and portals.
    /// All stats that are stored in an interior are cached values computed from the interiors rooms.
    /// </summary>
    public interface IInteriorArchetype : IMapArchetype, IHasEntityChildren
    {
        #region Properties
        /// <summary>
        /// List of rooms this interior has
        /// </summary>
        ObservableCollection<IRoom> Rooms { get; }

        /// <summary>
        /// List of portals this interior has
        /// </summary>
        ObservableCollection<IPortal> Portals { get; }
        #endregion // Properties
    } // IInteriorArchetype
}
