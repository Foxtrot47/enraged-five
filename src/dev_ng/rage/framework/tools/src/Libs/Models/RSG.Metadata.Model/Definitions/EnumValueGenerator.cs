﻿// --------------------------------------------------------------------------------------------
// <copyright file="EnumValueGenerator.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions
{
    /// <summary>
    /// The different methods used to give enumeration constants a value.
    /// </summary>
    public enum EnumValueGenerator
    {
        /// <summary>
        /// The value generator is not recognised as a valid generator.
        /// </summary>
        Unrecognised,

        /// <summary>
        /// Will set the value to the previous value plus one (or 0 for the first constant).
        /// </summary>
        Increment,

        /// <summary>
        /// Will set the value using the atStringHash function with constant name.
        /// </summary>
        Hash,

        /// <summary>
        /// Will set the value to the old signed 16 bit hash value of the constant name.
        /// </summary>
        Hash16,

        /// <summary>
        /// Will set the value to the old unsigned 16 bit hash value of the constant name.
        /// </summary>
        Hash16u,

        /// <summary>
        /// Will set the value using the atLiteralStringHash function with constant name.
        /// </summary>
        LiteralHash,
    } // RSG.Metadata.Model.Definitions.EnumValueGenerator {Enum}
} // RSG.Metadata.Model.Definitions {Namespace}
