﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common.Map
{
    /// <summary>
    /// Enumeration of all statistics that a room can have associated with it
    /// </summary>
    [DependentStat(StreamableInteriorArchetypeStat.RoomsString)]
    public static class StreamableRoomStat
    {
        public const String ShadersString = "EF28D291-09B6-4C14-B124-4DC6230ABE21";
        public const String BoundingBoxString = "2AACA486-66F6-4549-A0A4-3DF6D2336947";
        public const String ExportGeometrySizeString = "0BC59BCE-18E6-4791-9BEF-4E253F93EA4A";
        public const String TxdExportSizesString = "12CB557D-2658-43E2-91F7-A9AC512BEB01";
        public const String PolygonCountString = "1160AAD0-53EE-451C-9B48-51DFE0216C40";
        public const String CollisionPolygonCountString = "5CBF5A4E-3E49-4CFE-9718-2FA4B1B435B2";
        public const String CollisionTypePolygonCountsString = "5EF9A177-B413-4E10-8A68-6A67EF8FDFF5";
        public const String EntitiesString = "90E1AFBA-7484-421A-868F-3584C71B3D64";

        public static readonly StreamableStat Shaders = new StreamableStat(ShadersString, true, true);
        public static readonly StreamableStat BoundingBox = new StreamableStat(BoundingBoxString, true, true, false);
        public static readonly StreamableStat ExportGeometrySize = new StreamableStat(ExportGeometrySizeString, true);
        public static readonly StreamableStat TxdExportSizes = new StreamableStat(TxdExportSizesString, true, true);
        public static readonly StreamableStat PolygonCount = new StreamableStat(PolygonCountString, true);
        public static readonly StreamableStat CollisionPolygonCount = new StreamableStat(CollisionPolygonCountString, true);
        public static readonly StreamableStat CollisionTypePolygonCounts = new StreamableStat(CollisionTypePolygonCountsString, true, true);
        public static readonly StreamableStat Entities = new StreamableStat(EntitiesString, true, true);
    } // StreamableRoomStat
}
