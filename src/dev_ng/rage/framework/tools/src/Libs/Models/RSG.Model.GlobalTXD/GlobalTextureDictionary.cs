﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using RSG.Base.Collections;
using RSG.Model.Asset;

namespace RSG.Model.GlobalTXD
{
    /// <summary>
    /// A global texture dictionary is a texture dictionary that
    /// has special demands put on it and special situations that
    /// allow textures to be promoted to it and from it
    /// </summary>
    public class GlobalTextureDictionary : ModelBase,
        IDictionaryContainer, IDictionaryChild, ITextureContainer
    {
        #region Constants
        
        private static readonly String NameAttribute = "name";
        private static readonly String GlobalCountAttribute = "globalCount";
        private static readonly String SourceCountAttribute = "sourceCount";
        private static readonly String TextureCountAttribute = "textureCount";

        #region XPath Compiled Expressions

        private static readonly XPathExpression sC_s_xpath_XPathAttributes = XPathExpression.Compile("@*");
        private static readonly XPathExpression sC_s_xpath_XPathGlobalDictionaries = XPathExpression.Compile("globalDictionaries/globalDictionary");
        private static readonly XPathExpression sC_s_xpath_XPathGlobalDictionariesOld = XPathExpression.Compile("UserChildren/UserDictionary");

        private static readonly XPathExpression sC_s_xpath_XPathSourceDictionaries = XPathExpression.Compile("sourceDictionaries/sourceDictionary");
        private static readonly XPathExpression sC_s_xpath_XPathSourceDictionariesOld = XPathExpression.Compile("MapChildren/MapDictionary");

        private static readonly XPathExpression sC_s_xpath_XPathTextures = XPathExpression.Compile("globalTextures/globalTexture");
        private static readonly XPathExpression sC_s_xpath_XPathTexturesOld = XPathExpression.Compile("Textures/Texture");
        
        #endregion // XPath Compiled Expressions

        #endregion // Constants

        #region Fields

        private String m_name;
        private Boolean m_canAcceptTextures;
        private Boolean m_canAcceptSourceDictionaries;
        private Boolean m_canAcceptGlobalDictionaries;
        private IDictionaryContainer m_parent;
        private GlobalRoot m_root;
        private ObservableDictionary<String, GlobalTextureDictionary> m_globalTextureDictionaries;
        private ObservableDictionary<String, SourceTextureDictionary> m_sourceTextureDictionaries;
        private ObservableDictionary<String, ITextureChild> m_textures;
        private int m_currentSize;
        private bool m_wipFlag;
        #endregion // Fields

        #region Properties

        /// <summary>
        /// If this is true it means that source textures can be added to
        /// this container as children
        /// </summary>
        public Boolean CanAcceptTextures
        {
            get { return m_canAcceptTextures; }
            private set
            {
                this.SetPropertyValue(
                    ref this.m_canAcceptTextures, value, "CanAcceptTextures");
            }
        }

        /// <summary>
        /// If this is true it means that source texture dictionaries can be added to
        /// this container as children
        /// </summary>
        public Boolean CanAcceptSourceDictionaries
        {
            get { return this.GlobalTextureDictionaries.Count == 0; }
        }

        /// <summary>
        /// If this is true it means that global texture dictionaries can be added to
        /// this container as children
        /// </summary>
        public Boolean CanAcceptGlobalDictionaries
        {
            get { return this.SourceTextureDictionaries.Count == 0; }
        }

        /// <summary>
        /// The name of the texture dictionary, this name will be exported and used at runtime,
        /// which means that this needs to be unique.
        /// </summary>
        public String Name
        {
            get { return m_name; }
            set
            {
                this.SetPropertyValue(ref this.m_name, value, "Name");
            }
        }

        /// <summary>
        /// The parent global dictionary if it has one
        /// </summary>
        public IDictionaryContainer Parent
        {
            get { return m_parent; }
            set
            {
                this.SetPropertyValue(ref this.m_parent, value, "Parent");
            }
        }

        /// <summary>
        /// The global root object that this dictionary belongs to
        /// </summary>
        public GlobalRoot Root
        {
            get { return m_root; }
            set
            {
                this.SetPropertyValue(ref this.m_root, value, "Root");
            }
        }

        /// <summary>
        /// A dictionary of uniquely named texture dictionaries that are children of this dictionary
        /// The dictionaries are indexed by names and these names need to be unique.
        /// </summary>
        public ObservableDictionary<String, GlobalTextureDictionary> GlobalTextureDictionaries
        {
            get { return m_globalTextureDictionaries; }
            private set
            {
                this.SetPropertyValue(
                    ref this.m_globalTextureDictionaries, value, "GlobalTextureDictionaries");
            }
        }

        /// <summary>
        /// A dictionary of uniquely named source texture dictionaries that are children of this dictionary
        /// The dictionaries are indexed by names and these names need to be unique.
        /// </summary>
        public ObservableDictionary<String, SourceTextureDictionary> SourceTextureDictionaries
        {
            get { return m_sourceTextureDictionaries; }
            private set
            {
                this.SetPropertyValue(
                    ref this.m_sourceTextureDictionaries, value, "SourceTextureDictionaries");
            }
        }

        /// <summary>
        /// A dictionary of uniquely named textures that are children of this texture container
        /// The textures are indexed by stream names and these names need to be unique.
        /// </summary>
        public ObservableDictionary<String, ITextureChild> Textures 
        {
            get { return m_textures; }
            private set
            {
                this.SetPropertyValue(ref this.m_textures, value, "Textures");
            }
        }

        /// <summary>
        /// The current size of the container based on the sum of the texture
        /// sizes inside it
        /// </summary>
        public int CurrentSize
        {
            get { return m_currentSize; }
            private set
            {
                this.SetPropertyValue(ref this.m_currentSize, value, "CurrentSize");

                if (this.SourceTextureDictionaries != null)
                {
                    foreach (var source in this.SourceTextureDictionaries.Values)
                    {
                        source.IsSavingLessThanParent = source.SavingSize < this.CurrentSize;
                    }
                }
            }
        }

        public GlobalTextureDictionary this[String name]
        {
            get
            {
                GlobalTextureDictionary dictionary = null;
                this.GlobalTextureDictionaries.TryGetValue(name, out dictionary);
                return dictionary;
            }
        }

        public GlobalTextureDictionary GetGlobalDictionaryByName(String name)
        {
            GlobalTextureDictionary dictionary = null;
            this.GlobalTextureDictionaries.TryGetValue(name, out dictionary);
            return dictionary;
        }

        public SourceTextureDictionary GetSourceDictionaryByName(String name)
        {
            SourceTextureDictionary dictionary = null;
            this.SourceTextureDictionaries.TryGetValue(name, out dictionary);
            return dictionary;
        }

        public GlobalTexture GetTextureByName(String name)
        {
            ITextureChild texture = null;
            this.Textures.TryGetValue(name, out texture);
            return texture as GlobalTexture;
        }

        /// <summary>
        /// Gets whether or not this global dictionary is set to 
        /// work in progress.
        /// </summary>
        public bool WorkInProgress
        {
            get { return m_wipFlag; }
            set
            {
                this.SetPropertyValue(ref this.m_wipFlag, value, "WorkInProgress");
            }
        }
        #endregion // Properties

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        public GlobalTextureDictionary(String name, GlobalRoot root)
        {
            this.Name = name;
            this.Root = root;
            this.Parent = null;
            this.GlobalTextureDictionaries = new ObservableDictionary<String, GlobalTextureDictionary>();
            this.SourceTextureDictionaries = new ObservableDictionary<String, SourceTextureDictionary>();
            this.Textures = new ObservableDictionary<String, ITextureChild>();
            this.CanAcceptTextures = true;
            this.CurrentSize = 0;
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public GlobalTextureDictionary(String name, GlobalTextureDictionary parent)
        {
            this.Name = name;
            this.Root = parent.Root;
            this.Parent = parent;
            this.GlobalTextureDictionaries = new ObservableDictionary<String, GlobalTextureDictionary>();
            this.SourceTextureDictionaries = new ObservableDictionary<String, SourceTextureDictionary>();
            this.Textures = new ObservableDictionary<String, ITextureChild>();
            this.CanAcceptTextures = true;
            this.CurrentSize = 0;
        }

        /// <summary>
        /// 
        /// </summary>
        public GlobalTextureDictionary(XmlReader reader, GlobalRoot root)
        {
            this.Root = root;
            this.Parent = null;
            this.GlobalTextureDictionaries = new ObservableDictionary<String, GlobalTextureDictionary>();
            this.SourceTextureDictionaries = new ObservableDictionary<String, SourceTextureDictionary>();
            this.Textures = new ObservableDictionary<String, ITextureChild>();
            this.CanAcceptTextures = true;
            this.CurrentSize = 0;

            Deserialise(reader);
        }

        /// <summary>
        /// 
        /// </summary>
        public GlobalTextureDictionary(XmlReader reader, GlobalTextureDictionary parent)
            : this(reader, parent.Root)
        {
            this.Parent = parent;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="clone"></param>
        public GlobalTextureDictionary(GlobalTextureDictionary clone, GlobalTextureDictionary parent)
        {
            this.Name = clone.Name;
            this.Root = clone.Root;
            this.Parent = parent;
            this.GlobalTextureDictionaries = new ObservableDictionary<String, GlobalTextureDictionary>();
            this.SourceTextureDictionaries = new ObservableDictionary<String, SourceTextureDictionary>();
            this.Textures = new ObservableDictionary<String, ITextureChild>();
            this.CanAcceptTextures = clone.CanAcceptTextures;
            this.CurrentSize = clone.CurrentSize;

            foreach (SourceTextureDictionary sd in clone.SourceTextureDictionaries.Values)
            {
                this.SourceTextureDictionaries.Add(sd.Name, new SourceTextureDictionary(sd));
            }
            foreach (GlobalTextureDictionary gd in clone.GlobalTextureDictionaries.Values)
            {
                this.GlobalTextureDictionaries.Add(gd.Name, new GlobalTextureDictionary(gd, this));
            }
            foreach (GlobalTexture t in clone.Textures.Values)
            {
                this.Textures.Add(t.StreamName, new GlobalTexture(t, this));
            }
        }
        #endregion // Constructors

        #region IDictionaryContainer Implementation

        /// <summary>
        /// Returns true iff this dictionary has a texture dictionary in it with
        /// the given name. If recursive is true it will recursive through all the texture
        /// dictionaries as well
        /// </summary>
        public Boolean ContainsTextureDictionary(String name, Boolean recursive)
        {
            foreach (GlobalTextureDictionary dictionary in GlobalTextureDictionaries.Values)
            {
                if (String.Compare(name, dictionary.Name, true) == 0)
                {
                    return true;
                }
                if (recursive == true)
                {
                    if (dictionary.ContainsTextureDictionary(name, true))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Returns true iff this dictionary has a texture dictionary in it with
        /// the given name. If recursive is true it will recursive through all the texture
        /// dictionaries as well
        /// </summary>
        public Boolean ContainsSourceTextureDictionary(String name, Boolean recursive)
        {
            foreach (SourceTextureDictionary dictionary in SourceTextureDictionaries.Values)
            {
                if (String.Compare(name, dictionary.Name, true) == 0)
                {
                    return true;
                }
            }

            if (recursive == true)
            {
                foreach (GlobalTextureDictionary dictionary in GlobalTextureDictionaries.Values)
                {
                    if (dictionary.ContainsSourceTextureDictionary(name, true))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Returns true if the texture with the given stream name can
        /// be promoted here from a source dictionary.
        /// </summary>
        /// <param name="streamName"></param>
        /// <returns></returns>
        public Boolean CanPromoteSourceTexture(String streamName)
        {
            foreach (GlobalTextureDictionary dictionary in GlobalTextureDictionaries.Values)
            {
                if (dictionary.CanPromoteSourceTexture(streamName))
                {
                    return true;
                }
            }
            foreach (SourceTextureDictionary dictionary in SourceTextureDictionaries.Values)
            {
                if (dictionary.ContainsTexture(streamName))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Returns true if the texture with the given stream name can
        /// be promoted here from a source dictionary.
        /// </summary>
        /// <param name="streamName"></param>
        /// <returns></returns>
        public Boolean CanPromoteGlobalTexture(String streamName, ValidationDirection direction)
        {
            if (this.ContainsTexture(streamName))
                return true;

            if (direction == ValidationDirection.Up || direction == ValidationDirection.Both)
            {
                if (this.Parent != null && this.Parent is GlobalTextureDictionary)
                {
                    if ((this.Parent as GlobalTextureDictionary).CanPromoteGlobalTexture(streamName, ValidationDirection.Up))
                        return true;
                }
            }
            if (direction == ValidationDirection.Down || direction == ValidationDirection.Both)
            {
                foreach (GlobalTextureDictionary dictionary in GlobalTextureDictionaries.Values)
                {
                    if (dictionary.CanPromoteGlobalTexture(streamName, ValidationDirection.Down))
                        return true;
                }
            }

            return false;
        }

        public Boolean ContainsEmptyTextureDictionary(Boolean recursive)
        {
            foreach (GlobalTextureDictionary dictionary in this.GlobalTextureDictionaries.Values)
            {
                if (dictionary.Textures.Count == 0)
                {
                    return true;
                }
                if (recursive == true)
                {
                    if (dictionary.ContainsEmptyTextureDictionary(true))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Adds the given dictionary into the children texture dictionaries iff the name
        /// is unique.
        /// </summary>
        public Boolean AddExistingGlobalDictionary(GlobalTextureDictionary dictionary)
        {
            if (this.GlobalTextureDictionaries.ContainsKey(dictionary.Name))
                return false;

            this.GlobalTextureDictionaries.Add(dictionary.Name, dictionary);
            return true;
        }

        /// <summary>
        /// Creates a new dictionary with the given name and adds it to this dictionary 
        /// iff the name is unique
        /// </summary>
        public Boolean AddNewGlobalDictionary(String name)
        {
            if (this.GlobalTextureDictionaries.ContainsKey(name))
                return false;

            GlobalTextureDictionary newDictionary = new GlobalTextureDictionary(name, this);
            this.GlobalTextureDictionaries.Add(name, newDictionary);

            if (this.SourceTextureDictionaries != null)
            {
                while (this.SourceTextureDictionaries.Count > 0)
                {
                    SourceTextureDictionary sd = this.SourceTextureDictionaries.FirstOrDefault().Value;
                    sd.Parent = newDictionary;
                    newDictionary.SourceTextureDictionaries.Add(sd.Name, sd);
                    this.SourceTextureDictionaries.Remove(sd.Name);
                }
            }
            return true;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dictionary"></param>
        /// <returns></returns>
        public bool MoveGlobalDictionaryHere(GlobalTextureDictionary dictionary)
        {
            IDictionaryContainer oldParent = dictionary.Parent;
            if (oldParent == null)
                oldParent = this.Root;
            if (this.GlobalTextureDictionaries.ContainsKey(dictionary.Name))
                return false;

            dictionary.Parent = this;
            this.GlobalTextureDictionaries.Add(dictionary.Name, dictionary);
            oldParent.GlobalTextureDictionaries.Remove(dictionary.Name);

            GlobalRoot.CheckPromotedTexturesAreValid(oldParent, ValidationDirection.Both);
            GlobalRoot.CheckTexturesPromotionState(this);

            return true;
        }

        /// <summary>
        /// Adds the given dictionary into the children texture dictionaries iff the name
        /// is unique.
        /// </summary>
        public Boolean AddExistingSourceDictionary(SourceTextureDictionary dictionary)
        {
            if (this.SourceTextureDictionaries.ContainsKey(dictionary.Name))
                return false;

            this.SourceTextureDictionaries.Add(dictionary.Name, dictionary);

            // Calculate the texture instance count and set promoted state
            IList<String> promotedTextures = new List<String>();
            GlobalRoot.GetAllPromotedTexturesFromBase(this, ref promotedTextures);
            foreach (SourceTexture texture in this.GetSourceDictionaryByName(dictionary.Name))
            {
                GlobalRoot.SetTextureInstanceCount(texture.StreamName, this);
                if (promotedTextures.Contains(texture.StreamName))
                    texture.Promoted = true;

            }
            dictionary.Parent = this;
            return true;
        }

        /// <summary>
        /// Creates a new source dictionary using the given texture dictionary as a 
        /// source guide.
        /// </summary>
        public Boolean AddNewSourceDictionary(TextureDictionary dictionary)
        {
            if (this.SourceTextureDictionaries.ContainsKey(dictionary.Name.ToLower()))
                return false;

            SourceTextureDictionary newDictionary = new SourceTextureDictionary(dictionary, this);
            this.SourceTextureDictionaries.Add(dictionary.Name.ToLower(), newDictionary);

            // Calculate the texture instance count and set promoted state
            IList<String> promotedTextures = new List<String>();
            GlobalRoot.GetAllPromotedTexturesFromBase(this, ref promotedTextures);
            foreach (SourceTexture texture in newDictionary)
            {
                GlobalRoot.SetTextureInstanceCount(texture.StreamName, this);
                if (promotedTextures.Contains(texture.StreamName))
                    texture.Promoted = true;

            }
            return true;
        }

        /// <summary>
        /// Removes the texture dictionary with the given name from the list iff
        /// it exists.
        /// </summary>
        public Boolean RemoveGlobalTextureDictionary(String name)
        {
            if (!this.GlobalTextureDictionaries.ContainsKey(name))
                return false;

            GlobalTextureDictionary dictionary = this.GlobalTextureDictionaries[name];
            return RemoveGlobalTextureDictionary(dictionary);
        }

        /// <summary>
        /// Removes the given texture dictionary from the list iff
        /// it exists.
        /// </summary>
        public Boolean RemoveGlobalTextureDictionary(GlobalTextureDictionary dictionary)
        {
            if (!this.GlobalTextureDictionaries.ContainsKey(dictionary.Name))
                return false;

            this.GlobalTextureDictionaries.Remove(dictionary.Name);
            GlobalRoot.CheckPromotedTexturesAreValid(this, ValidationDirection.Up);
            GlobalRoot.CheckTexturesPromotionState(this);
            return true;
        }
        
        /// <summary>
        /// Removes the parent dictionary put keeps this and all siblings.
        /// </summary>
        public Boolean RemoveParentGlobalDictionary()
        {
            if (this.Parent == null)
                return false;
            
            IDictionaryContainer newParent = this.Parent.Parent ?? this.Root;
            newParent.GlobalTextureDictionaries.Remove(this.Parent.Name);
            GlobalTextureDictionary oldParent = (this.Parent as GlobalTextureDictionary);
            foreach (GlobalTextureDictionary child in oldParent.GlobalTextureDictionaries.Values)
            {
                child.Parent = newParent;
                newParent.GlobalTextureDictionaries.Add(child.Name, child);
            }

            GlobalRoot.CheckPromotedTexturesAreValid(newParent, ValidationDirection.Up);
            GlobalRoot.CheckTexturesPromotionState(newParent);

            return true;
        }

        /// <summary>
        /// Removes the parent dictionary put keeps this and all siblings.
        /// </summary>
        public Boolean RemoveButKeepChildren()
        {
            IDictionaryContainer newParent = this.Parent ?? this.Root;
            newParent.GlobalTextureDictionaries.Remove(this.Name);
            GlobalTextureDictionary oldParent = this;
            foreach (GlobalTextureDictionary child in oldParent.GlobalTextureDictionaries.Values)
            {
                child.Parent = newParent;
                newParent.GlobalTextureDictionaries.Add(child.Name, child);
            }
            foreach (SourceTextureDictionary child in oldParent.SourceTextureDictionaries.Values)
            {
                child.Parent = newParent;
                newParent.SourceTextureDictionaries.Add(child.Name, child);
            }

            GlobalRoot.CheckPromotedTexturesAreValid(newParent, ValidationDirection.Up);
            GlobalRoot.CheckTexturesPromotionState(newParent);

            return true;
        }

        /// <summary>
        /// Removes the given texture dictionary from the list iff
        /// it exists.
        /// </summary>
        public Boolean RemoveSourceTextureDictionary(SourceTextureDictionary dictionary)
        {
            if (!this.SourceTextureDictionaries.ContainsKey(dictionary.Name))
                return false;

            this.SourceTextureDictionaries.Remove(dictionary.Name);
            GlobalRoot.CheckPromotedTexturesAreValid(this, ValidationDirection.Up);

            IList<String> promotedTextures = new List<String>();
            GlobalRoot.GetAllPromotedTexturesFromBase(this, ref promotedTextures);
            foreach (SourceTextureDictionary sd in this.SourceTextureDictionaries.Values)
            {
                foreach (SourceTexture texture in sd)
                {
                    GlobalRoot.SetTextureInstanceCount(texture.StreamName, this);
                    if (promotedTextures.Contains(texture.StreamName))
                        texture.Promoted = true;
                    else
                        texture.Promoted = false;

                }
            }
            return true;
        }

        /// <summary>
        /// Removes the given texture dictionary from the list iff
        /// it exists.
        /// </summary>
        public Boolean MoveSourceDictionaryHere(SourceTextureDictionary dictionary)
        {
            IDictionaryContainer oldParent = dictionary.Parent;
            if (this.SourceTextureDictionaries.ContainsKey(dictionary.Name))
                return false;

            dictionary.Parent = this;
            this.SourceTextureDictionaries.Add(dictionary.Name, dictionary);
            oldParent.RemoveSourceTextureDictionary(dictionary);

            IList<String> promotedTextures = new List<String>();
            GlobalRoot.GetAllPromotedTexturesFromBase(this, ref promotedTextures);
            foreach (SourceTexture texture in dictionary)
            {
                GlobalRoot.SetTextureInstanceCount(texture.StreamName, this);
                if (promotedTextures.Contains(texture.StreamName))
                    texture.Promoted = true;
                else
                    texture.Promoted = false;

            }
            return true;
        }

        #endregion // IDictionaryContainer Implementation

        #region ITextureContainer Implementation

        /// <summary>
        /// Returns true iff the container has the texture with the given stream name in it
        /// </summary>
        public Boolean ContainsTexture(String streamName)
        {
            return this.Textures.ContainsKey(streamName);
        }

        /// <summary>
        /// Promotes the given texture (source or global) to here and makes sure that
        /// validation is done so that the texture is removed elsewhere
        /// </summary>
        public Boolean PromoteTextureHere(SourceTexture texture)
        {
            if (this.Textures.ContainsKey(texture.StreamName))
                return false;

            this.Textures.Add(texture.StreamName, new GlobalTexture(texture as SourceTexture, this));
            GlobalRoot.CheckTexturesPromotionState(this);
            GlobalRoot.CheckSingleGlobalTexture(this, texture.StreamName, ValidationDirection.Down);
            SetCurrentSize();

            GlobalRoot.SetTextureInstanceCount(texture.StreamName, this.Parent);
            return true;
        }

        /// <summary>
        /// Promotes the given texture (source or global) to here and makes sure that
        /// validation is done so that the texture is removed elsewhere
        /// </summary>
        public Boolean PromoteTextureHere(GlobalTexture texture)
        {
            if (this.Textures.ContainsKey(texture.StreamName))
                return false;

            this.Textures.Add(texture.StreamName, texture);
            GlobalRoot.TexturePromotionPostProcess(texture.StreamName, this);
            SetCurrentSize();

            GlobalRoot.SetTextureInstanceCount(texture.StreamName, this.Parent);
            return true;
        }

        /// <summary>
        /// Unpromotes the given texture from this dictionary and makes sure that all instances
        /// of it inside source dictionaries have their promotion state reset.
        /// </summary>
        public Boolean FullyUnpromoteTexture(GlobalTexture texture)
        {
            if (!this.Textures.ContainsKey(texture.StreamName))
                return false;

            this.Textures.Remove(texture.StreamName);
            GlobalRoot.CheckTexturesPromotionState(this);

            SetCurrentSize();
            return true;
        }

        /// <summary>
        /// Moves the given global texture from its previous global dictionary to this 
        /// global dictionary
        /// </summary>
        public Boolean MoveTextureHere(GlobalTexture texture)
        {
            if (this.Textures.ContainsKey(texture.StreamName))
                return false;

            GlobalTextureDictionary textureParent = texture.Parent as GlobalTextureDictionary;
            GlobalTextureDictionary theParent = GlobalRoot.GetParent(this, textureParent);


            texture.Parent = this;
            texture.Root = this.Root;
            this.Textures.Add(texture.StreamName, texture);

            GlobalRoot.CheckSingleGlobalTexture(this, texture.StreamName, ValidationDirection.Both);
            if (theParent == null)
            {
                GlobalRoot.CheckTexturesPromotionState(this);
            }
            else
            {
                GlobalRoot.CheckTexturesPromotionState(theParent);
            }
            GlobalRoot.SetTextureInstanceCount(texture.StreamName, this.Parent);
            GlobalRoot.CheckTextureDictionarySizes(this, ValidationDirection.Both);

            textureParent.SetCurrentSize();
            this.SetCurrentSize();
            return true;
        }

        /// <summary>
        /// Moves the given global texture from this dictionary before it gets
        /// added to another dictionary
        /// </summary>
        public Boolean MoveTextureFromHere(GlobalTexture texture)
        {
            if (!this.Textures.ContainsKey(texture.StreamName))
                return false;

            this.Textures.Remove(texture.StreamName);
            SetCurrentSize();
            return true;
        }

        /// <summary>
        /// Moves the given global texture from this dictionary before it gets
        /// added to another dictionary
        /// </summary>
        public Boolean MoveTextureFromHere(String streamName)
        {
            if (!this.Textures.ContainsKey(streamName))
                return false;

            this.Textures.Remove(streamName);
            GlobalRoot.SetTextureInstanceCount(streamName, this.Parent);
            SetCurrentSize();
            return true;
        }

        /// <summary>
        /// Moves the given global textures from this dictionary before it gets
        /// added to another dictionary
        /// </summary>
        public void MoveTexturesFromHere(IList<String> streamNames)
        {
            foreach (String streamName in streamNames)
            {
                if (!this.Textures.ContainsKey(streamName))
                    continue;

                this.Textures.Remove(streamName);
                GlobalRoot.SetTextureInstanceCount(streamName, this.Parent);
            }

            SetCurrentSize();
        }

        /// <summary>
        /// Gets called whenever a texture inside of this container has their promotion
        /// state changed
        /// </summary>
        public void OnPromotionChanged(ITextureChild sender, Boolean promoted)
        {
            SetCurrentSize();
        }

        /// <summary>
        /// Gets called whenever a texture inside of this container has their stream 
        /// size changed
        /// </summary>
        public void OnSizeChanged(ITextureChild sender, int size)
        {
            SetCurrentSize();
        }

        #endregion // ITextureContainer Implementation

        #region Public Functions
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        public bool AddNewParent(string name)
        {
            if (!this.Parent.AddNewGlobalDictionary(name))
                return false;

            GlobalTextureDictionary newDictionary = this.Parent.GlobalTextureDictionaries[name];
            newDictionary.MoveGlobalDictionaryHere(this);
            return true;
        }
        #endregion // public functions

        #region Private Functions

        /// <summary>
        /// 
        /// </summary>
        internal void SetCurrentSize()
        {
            if (this.Textures.Count == 0)
            {
                this.CurrentSize = 0;
                return;
            }

            int size = 0;
            foreach (GlobalTexture texture in this.Textures.Values)
            {
                size += texture.StreamSize;
            }
            this.CurrentSize = size;
        }

        #endregion // Private Functions

        #region Serialisation / Deserialisation Methods

        /// <summary>
        /// 
        /// </summary>
        public void Serialise(XmlDocument xmlDoc, XmlElement parentNode)
        {
            XmlElement dictionaryNode = xmlDoc.CreateElement("globalDictionary");

            dictionaryNode.SetAttribute(NameAttribute, this.Name);
            dictionaryNode.SetAttribute(GlobalCountAttribute, this.GlobalTextureDictionaries.Count.ToString());
            dictionaryNode.SetAttribute(SourceCountAttribute, this.SourceTextureDictionaries.Count.ToString());
            dictionaryNode.SetAttribute(TextureCountAttribute, this.Textures.Count.ToString());
            if (this.Parent == null)
            {
                dictionaryNode.SetAttribute("wip", this.WorkInProgress ? "true" : "false");
            }

            XmlElement GlobalDicitonariesRootNode = xmlDoc.CreateElement("globalDictionaries");
            foreach (GlobalTextureDictionary dictionary in this.GlobalTextureDictionaries.Values)
            {
                dictionary.Serialise(xmlDoc, GlobalDicitonariesRootNode);
            }
            dictionaryNode.AppendChild(GlobalDicitonariesRootNode);


            XmlElement SourceDicitonariesRootNode = xmlDoc.CreateElement("sourceDictionaries");
            foreach (SourceTextureDictionary dictionary in this.SourceTextureDictionaries.Values)
            {
                dictionary.Serialise(xmlDoc, SourceDicitonariesRootNode);
            }
            dictionaryNode.AppendChild(SourceDicitonariesRootNode);


            XmlElement GlobalTexturesRootNode = xmlDoc.CreateElement("globalTextures");
            foreach (GlobalTexture texture in this.Textures.Values)
            {
                texture.Serialise(xmlDoc, GlobalTexturesRootNode);
            }
            dictionaryNode.AppendChild(GlobalTexturesRootNode);


            parentNode.AppendChild(dictionaryNode);
        }

        /// <summary>
        /// 
        /// </summary>
        public void Deserialise(XmlReader reader)
        {
            this.Name = reader.GetAttribute("name");
            bool wip;
            bool.TryParse(reader.GetAttribute("wip"), out wip);
            this.WorkInProgress = wip;
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.Read();
            while (reader.MoveToContent() != XmlNodeType.EndElement)
            {
                if (reader.IsStartElement())
                {
                    if (string.Compare(reader.Name, "globalDictionaries") == 0)
                    {
                        this.LoadGlobalDictionaries(reader);
                    }
                    else if (string.Compare(reader.Name, "sourceDictionaries") == 0)
                    {
                        this.LoadSourceDictionaries(reader);
                    }
                    else if (string.Compare(reader.Name, "globalTextures") == 0)
                    {
                        this.LoadGlobalTextures(reader);
                    }
                }

                reader.Read();
            }

            reader.Read();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        private void LoadGlobalDictionaries(XmlReader reader)
        {
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.Read();
            while (reader.MoveToContent() != XmlNodeType.EndElement)
            {
                if (reader.IsStartElement())
                {
                    if (string.Compare(reader.Name, "globalDictionary") == 0)
                    {
                        GlobalTextureDictionary newDictionary = new GlobalTextureDictionary(reader, this);
                        this.GlobalTextureDictionaries.Add(newDictionary.Name, newDictionary);
                        continue;
                    }
                }

                reader.Read();
            }

            reader.Read();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        private void LoadSourceDictionaries(XmlReader reader)
        {
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.Read();
            while (reader.MoveToContent() != XmlNodeType.EndElement)
            {
                if (reader.IsStartElement())
                {
                    if (string.Compare(reader.Name, "sourceDictionary") == 0)
                    {
                        SourceTextureDictionary newDictionary = new SourceTextureDictionary(reader, this);
                        this.SourceTextureDictionaries.Add(newDictionary.Name, newDictionary);
                        continue;
                    }
                }

                reader.Read();
            }

            reader.Read();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        private void LoadGlobalTextures(XmlReader reader)
        {
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.Read();
            while (reader.MoveToContent() != XmlNodeType.EndElement)
            {
                if (reader.IsStartElement())
                {
                    if (string.Compare(reader.Name, "globalTexture") == 0)
                    {
                        GlobalTexture newTexture = new GlobalTexture(reader, this);
                        this.Textures.Add(newTexture.StreamName, newTexture);
                        continue;
                    }
                }

                reader.Read();
            }

            reader.Read();
        }
        #endregion // Serialisation / Deserialisation Methods
    } // GlobalTextureDictionary
}
