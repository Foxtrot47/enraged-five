﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using RSG.Base.Extensions;
using RSG.Model.Report.Reports.GameStatsReports.Model;
using System.IO;
using System.Windows.Controls.DataVisualization.Charting;
using System.Windows.Media;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using System.Windows;

namespace RSG.Model.Report.Reports.GameStatsReports.Tests
{
    /// <summary>
    /// 
    /// </summary>
    public class DrawListSmokeTest : SmokeTestBase
    {
        #region Constants
        private const string NAME = "Draw List";
        #endregion // Constants

        #region Private Classes
        /// <summary>
        /// Contains information about a single cpu stat
        /// </summary>
        private class DrawListInfo
        {
            public DrawListResult Result { get; private set; }
            public DateTime Timestamp { get; private set; }

            public DrawListInfo(DrawListResult result, DateTime time)
            {
                Result = result;
                Timestamp = time;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private class DrawListNameKey : IEquatable<DrawListNameKey>
        {
            public string TestName { get; private set; }
            public string DrawListName { get; private set; }

            public DrawListNameKey(string testName, string drawListName)
            {
                TestName = testName;
                DrawListName = drawListName;
            }

            public override string ToString()
            {
                return String.Format("{0} {1}", TestName, DrawListName);
            }

            public override bool Equals(object obj)
            {
                if (obj == null)
                {
                    return base.Equals(obj);
                }

                return ((obj is DrawListNameKey) && Equals(obj as DrawListNameKey));
            }

            public override int GetHashCode()
            {
                return (TestName.GetHashCode() ^ DrawListName.GetHashCode());
            }

            public bool Equals(DrawListNameKey other)
            {
                if (other == null)
                {
                    return false;
                }

                return (TestName == other.TestName && DrawListName == other.DrawListName);
            }
        }
        #endregion // Private Methods

        #region Properties
        /// <summary>
        /// Test name -> list of memory results for the latest build
        /// </summary>
        private IDictionary<string, IList<DrawListResult>> LatestStats
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        private IDictionary<DrawListNameKey, IList<DrawListInfo>> HistoricalStats
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public DrawListSmokeTest()
            : base(NAME)
        {
        }
        #endregion // Constructor(s)

        #region SmokeTestBase Methods
        /// <summary>
        /// Executes the particular test
        /// </summary>
        /// <param name="testSessions">List of test sessions</param>
        public override void RunTest(IList<TestSession> testSessions)
        {
            base.RunTest(testSessions);

            LatestStats = GatherLatestBuildStats(testSessions.FirstOrDefault());
            HistoricalStats = GatherHistoricalStats(testSessions);
        }

        /// <summary>
        /// Generates test specific graphs for this smoke test
        /// </summary>
        /// <param name="test"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns>Null if the smoke test doesn't require a graph</returns>
        public override IList<GraphImage> GenerateGraphs(DateTime start, DateTime end)
        {
            IList<GraphImage> allGraphs = new List<GraphImage>();

            // Generate a couple of graphs for each  test
            foreach (KeyValuePair<string, IList<DrawListResult>> pair in LatestStats)
            {
                string testName = pair.Key;

                // Create the pie graph
                Stream imageStream = GenerateColumnGraph(testName, pair.Value);
                string graphName = String.Format("drawlist_column_{0}", testName.Replace("(", "").Replace(")", "").Replace(" ", ""));
                GraphImage image = new GraphImage(graphName, imageStream);

                if (!PerTestGraphs.ContainsKey(testName))
                {
                    PerTestGraphs.Add(testName, new List<GraphImage>());
                }

                PerTestGraphs[testName].Add(image);
                allGraphs.Add(image);
            }

            return allGraphs;
        }

        /// <summary>
        /// Write this test's portion of the report
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="test">The name of the test for which we wish to write the report</param>
        /// <param name="includeHeader">Whether we should output a header for this report</param>
        /// <param name="tableResults"></param>
        public override void WriteReport(HtmlTextWriter writer, string test, bool includeHeader, uint tableResults, bool email)
        {
            base.WriteReport(writer, test, includeHeader, tableResults, email);
        }
        #endregion // SmokeTestBase Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        private IDictionary<string, IList<DrawListResult>> GatherLatestBuildStats(TestSession session)
        {
            IDictionary<string, IList<DrawListResult>> latestStats = new Dictionary<string, IList<DrawListResult>>();

            if (session != null)
            {
                foreach (TestResults test in session.TestResults)
                {
                    if (!latestStats.ContainsKey(test.Name))
                    {
                        latestStats.Add(test.Name, new List<DrawListResult>());
                    }
                    latestStats[test.Name].AddRange(test.DrawListResults.Where(result => (result.Average + result.Min + result.Max) != 0));
                }
            }

            return latestStats;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="testSessions"></param>
        /// <returns></returns>
        private IDictionary<DrawListNameKey, IList<DrawListInfo>> GatherHistoricalStats(IList<TestSession> testSessions)
        {
            IDictionary<DrawListNameKey, IList<DrawListInfo>> historicalStats = new Dictionary<DrawListNameKey, IList<DrawListInfo>>();

            foreach (TestSession session in testSessions)
            {
                foreach (TestResults test in session.TestResults)
                {
                    foreach (DrawListResult result in test.DrawListResults)
                    {
                        DrawListNameKey key = new DrawListNameKey(test.Name, result.Name);

                        if (!historicalStats.ContainsKey(key))
                        {
                            historicalStats.Add(key, new List<DrawListInfo>());
                        }
                        historicalStats[key].Add(new DrawListInfo(result, session.Timestamp));
                    }
                }
            }

            return historicalStats;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="testName"></param>
        /// <param name="setName"></param>
        /// <param name="stats"></param>
        /// <returns></returns>
        private Stream GenerateColumnGraph(string testName, IList<DrawListResult> stats)
        {
            Chart chart = new Chart();
            chart.BeginInit();
            chart.Width = 1280;
            chart.Height = 600;
            chart.Title = String.Format("Draw List Breakdown for '{0}'", testName);
            chart.LegendTitle = "Metrics";
            chart.EndInit();

            RenderTargetBitmap renderTarget = new RenderTargetBitmap((int)chart.Width, (int)chart.Height, 96d, 96d, PixelFormats.Default);

            ColumnSeries minSeries = new ColumnSeries
            {
                Title = "Minimum",
                ItemsSource = stats,
                IndependentValueBinding = new Binding("Name"),
                DependentValueBinding = new Binding("Min")
            };
            chart.Series.Add(minSeries);

            ColumnSeries avgSeries = new ColumnSeries
            {
                Title = "Average",
                ItemsSource = stats,
                IndependentValueBinding = new Binding("Name"),
                DependentValueBinding = new Binding("Average")
            };
            chart.Series.Add(avgSeries);

            ColumnSeries maxSeries = new ColumnSeries
            {
                Title = "Maximum",
                ItemsSource = stats,
                IndependentValueBinding = new Binding("Name"),
                DependentValueBinding = new Binding("Max")
            };
            chart.Series.Add(maxSeries);
            
            CategoryAxis xAxis = new CategoryAxis
            {
                Orientation = AxisOrientation.X,
                Title = "Draw List Name"
            };
            chart.Axes.Add(xAxis);

            LinearAxis yAxis = new LinearAxis
            {
                Orientation = AxisOrientation.Y,
                ShowGridLines = true
            };
            chart.Axes.Add(yAxis);
            
            chart.Measure(new Size(chart.Width, chart.Height));
            chart.Arrange(new Rect(new Size(chart.Width, chart.Height)));
            chart.UpdateLayout();

            renderTarget.Render(chart);

            // Save out the chart to a png file
            PngBitmapEncoder png = new PngBitmapEncoder();
            png.Frames.Add(BitmapFrame.Create(renderTarget));
            MemoryStream stream = new MemoryStream();
            png.Save(stream);
            stream.Position = 0;
            return stream;
        }
        #endregion // Private Methods
    } // DrawlistSmokeTest
}
