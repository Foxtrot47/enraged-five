﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using RSG.Base.Logging;
using System.IO;
using System.Web.UI;
using RSG.Model.Common;
using RSG.Model.Asset;
using RSG.Base.Tasks;
using System.Xml;
using RSG.Base.ConfigParser;

namespace RSG.Model.Report.Reports.Cutscene
{
    /// <summary>
    /// 
    /// </summary>
    public class CutsceneReport : HTMLReport, IDynamicLevelReport
    {
        #region Fields
        /// <summary>
        /// The name of this report.
        /// </summary>
        private const string ReportName = "Cutscene Ped Asset Usage";

        /// <summary>
        /// The description for this report.
        /// </summary>
        private const string ReportDescription = "Provides lists detailing the ped asset usage in the cutscenes.";

        /// <summary>
        /// The private field used for the <see cref="GenerationTask"/> property.
        /// </summary>
        private ITask _generationTask;

        /// <summary>
        /// List of model names
        /// </summary>
        private HashSet<String> modelNames;

        /// <summary>
        /// List of prop names
        /// </summary>
        private HashSet<String> propNames;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        public CutsceneReport()
        {
            this.Name = ReportName;
            this.Description = ReportDescription;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the task to run for generating this report.
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (this._generationTask == null)
                {
                    this._generationTask = new ActionTask(
                        "Generating report", this.GenerateReport);
                }

                return this._generationTask;
            }
        }

        public IEnumerable<StreamableStat> RequiredStats
        {
            get { return Enumerable.Empty<StreamableStat>(); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Generates the report dynamically.
        /// </summary>
        /// <param name="context">
        /// The context for this asynchronous task which includes the cancellation token.
        /// </param>
        /// <param name="progress">
        /// The progress object to use to report the progress of the generation.
        /// </param>
        private void GenerateReport(ITaskContext context, IProgress<TaskProgress> progress)
        {
            DynamicLevelReportContext reportContext = context as DynamicLevelReportContext;
            Debug.Assert(reportContext != null, "Unable to generate report");
            if (reportContext == null)
            {
                return;
            }

            Stopwatch sw = new Stopwatch();
            sw.Start();
            Log.Log__Message("Generating Cutscene Report");
            GenerateCutsceneData(reportContext.GameView);

            MemoryStream stream = new MemoryStream();
            StreamWriter reportFile = new StreamWriter(stream);

            HtmlTextWriter writer = new HtmlTextWriter(reportFile);
            writer.RenderBeginTag(HtmlTextWriterTag.Html);
            writer.RenderBeginTag(HtmlTextWriterTag.Head);
            writer.RenderEndTag();
            writer.RenderBeginTag(HtmlTextWriterTag.Body);
            writer.RenderBeginTag(HtmlTextWriterTag.H1);
            writer.Write("Cutscene Report");
            writer.RenderEndTag();

            writer.RenderBeginTag(HtmlTextWriterTag.H1);
            writer.Write("Summary");
            writer.RenderEndTag();

            writer.Write("{0} cutscene ped models currently used", this.modelNames.Count);
            writer.Write("<br />");
            writer.Write("{0} cutscene prop models currently used", this.propNames.Count);
            writer.Write("<br />");

            writer.RenderBeginTag(HtmlTextWriterTag.H2);
            writer.Write("Ped Models");
            writer.RenderEndTag();
            RenderPedDetails(writer);

            writer.RenderBeginTag(HtmlTextWriterTag.H2);
            writer.Write("Prop Models");
            writer.RenderEndTag();
            RenderPropDetails(writer);

            writer.RenderEndTag();
            writer.RenderEndTag();
            writer.Flush();

            stream.Position = 0;
            Stream = stream;

            sw.Stop();
            Log.Log__Message("Cutscene Report took {0}ms.", sw.ElapsedMilliseconds);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gameView">
        /// 
        /// </param>
        private void GenerateCutsceneData(ConfigGameView gameView)
        {
            modelNames = new HashSet<string>();
            propNames = new HashSet<string>();

            string[] dirs = Directory.GetDirectories(Path.Combine(gameView.AssetsDir, "cuts"));

            for (int i = 0; i < dirs.Length; ++i)
            {
                string strCutfile = Path.Combine(dirs[i], "data.cutxml");

                if (!File.Exists(strCutfile)) continue;

                var xmlDoc = new XmlDocument();
                xmlDoc.Load(strCutfile);

                XmlNodeList pedNodes = xmlDoc.SelectNodes("/rage__cutfCutsceneFile2/pCutsceneObjects/Item[@type='rage__cutfPedModelObject']");

                foreach (XmlNode node in pedNodes)
                {
                    string objectName = node["cName"].InnerText;

                    if (objectName.Contains("^")) continue;
                    modelNames.Add(objectName.ToLower());
                }

                XmlNodeList propNodes = xmlDoc.SelectNodes("/rage__cutfCutsceneFile2/pCutsceneObjects/Item[@type='rage__cutfPropModelObject']");

                foreach (XmlNode node in propNodes)
                {
                    string objectName = node["cName"].InnerText;

                    if (objectName.Contains("^")) continue;
                    propNames.Add(objectName.ToLower());
                }
            }
        }

        /// <summary>
        /// Render HTML table to display ped info.
        /// </summary>
        /// <param name="writer"></param>
        private void RenderPedDetails(HtmlTextWriter writer)
        {
            foreach (string ped in modelNames)
            {
                writer.Write(ped);
                writer.Write("<br />");
            }
        }

        /// <summary>
        /// Render HTML table to display prop info.
        /// </summary>
        /// <param name="writer"></param>
        private void RenderPropDetails(HtmlTextWriter writer)
        {
            foreach (string prop in propNames)
            {
                writer.Write(prop);
                writer.Write("<br />");
            }
        }
        #endregion Methods
    } // RSG.Model.Report.Reports.Cutscene.CutsceneReport {Class}
} // RSG.Model.Report.Reports.Cutscene {Namespace}
