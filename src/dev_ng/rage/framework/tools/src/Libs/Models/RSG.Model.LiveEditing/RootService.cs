﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.ConfigParser;
using System.Net;
using RSG.Base.Logging;
using System.Xml.Linq;
using System.IO;

namespace RSG.Model.LiveEditing
{
    /// <summary>
    /// The root service for live editing
    /// </summary>
    public class RootService : DirectoryService
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        public RootService(Uri url)
            : base("root", url)
        {
        }
        #endregion // Constructor(s)

        #region Static Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <param name="config"></param>
        public static RootService AcquireRootService(Uri url, ConfigGameView gv)
        {
            RootService root = new RootService(url);
            PopulateSubServices(root, gv);
            return root;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <param name="gv"></param>
        private static void PopulateSubServices(DirectoryService service, ConfigGameView gv)
        {
            try
            {
                using (WebClient client = new WebClient())
                {
                    // Get and attempt to parse the response
                    string response = client.DownloadString(service.Url);
                    if (!String.IsNullOrEmpty(response))
                    {
                        XElement xmlElem = XElement.Parse(response);
                        if (xmlElem.Name == "restServiceDirectory")
                        {
                            // Get the list of sub-services
                            var subServices =
                                from item in xmlElem.Descendants("Item")
                                select new
                                {
                                    RelativeAddress = (string)item.Attribute("href"),
                                    Name = ((string)item.Attribute("href")).Replace("./", ""),
                                    ServiceType = (string)item.Attribute("serviceType"),
                                    SourceFile = (string)item.Attribute("sourceFile"),
                                };

                            // Process the sub-services
                            foreach (var item in subServices)
                            {
                                if (item.RelativeAddress != "./")
                                {
                                    Uri url = new Uri(String.Format("{0}/{1}", service.Url.ToString().TrimEnd(new char[] { '/' }), item.RelativeAddress.Replace("./", "")));

                                    if (item.ServiceType == "ParsableObject")
                                    {
                                        string resolvedPath = ResolvePath(item.SourceFile, gv);
                                        if (resolvedPath != null)
                                        {
                                            ObjectService objService = new ObjectService(item.Name, url, resolvedPath);
                                            service.AssetChildren.Add(objService);
                                        }
                                    }
                                    else if (item.ServiceType == "ServiceDirectory")
                                    {
                                        DirectoryService dirService = new DirectoryService(item.Name, url);
                                        PopulateSubServices(dirService, gv);
                                        service.AssetChildren.Add(dirService);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        Log.Log__Error("Response was empty from {0}", service.Url);
                    }
                }
            }
            catch (WebException ex)
            {
                Log.Log__Exception(ex, "Failed downloading service information from {0}", service.Url);
            }

        }

        /// <summary>
        /// Converts a path that starts with either common: or platform: to a fully qualified path
        /// </summary>
        /// <param name="path"></param>
        /// <param name="gv"></param>
        /// <returns></returns>
        private static string ResolvePath(string path, ConfigGameView gv)
        {
            String resolvedPath = path;

            if (!String.IsNullOrEmpty(resolvedPath))
            {
                if (resolvedPath.Contains("common:"))
                {
                    resolvedPath = resolvedPath.Replace("common:", gv.CommonDir);
                }
                if (resolvedPath.Contains("platform:"))
                {
                    resolvedPath = resolvedPath.Replace("platform:", gv.ExportDir);
                }

                try
                {
                    resolvedPath = Path.GetFullPath(resolvedPath);
                }
                catch (Exception ex)
                {
                    Log.Log__Exception(ex, "Unable to get the full path for the resolved path {0}.", resolvedPath);
                    return null;
                }
            }

            return resolvedPath;
        }
        #endregion // Static Controller Methods
    } // RootService
}
