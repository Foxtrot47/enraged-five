﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProjectCollection.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.Model
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Xml;
    using RSG.Base.Extensions;
    using RSG.Editor;
    using RSG.Editor.Model;
    using RSG.Interop.Microsoft.Windows;

    /// <summary>
    /// Represents a collection of projects.
    /// </summary>
    public class ProjectCollection : ModelBase, IProjectItemScope
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="DirectoryPath"/> property.
        /// </summary>
        private string _directoryPath;

        /// <summary>
        /// The private field used for the <see cref="FullPath"/> property.
        /// </summary>
        private string _fullPath;

        /// <summary>
        /// The private map containing the project items indexed by their type.
        /// </summary>
        private Dictionary<string, HashSet<ProjectItem>> _items;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ProjectCollection"/> class.
        /// </summary>
        public ProjectCollection()
        {
            this._items = new Dictionary<string, HashSet<ProjectItem>>();
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the root directory for this scope. The root directory is never null; in-memory
        /// loads use the current directory at the time of the load.
        /// </summary>
        public string DirectoryPath
        {
            get { return this._directoryPath ?? String.Empty; }
        }

        /// <summary>
        /// Gets or sets the full path to the collections source file. Returns an empty string
        /// if the project was not loaded from disk.
        /// </summary>
        public string FullPath
        {
            get
            {
                return this._fullPath;
            }

            set
            {
                if (String.Equals(this._fullPath, value))
                {
                    return;
                }

                string oldValue = this._fullPath;
                this._fullPath = value;
                if (value == null)
                {
                    this._directoryPath = null;
                }
                else
                {
                    this._directoryPath = Path.GetDirectoryName(this._fullPath);
                }

                foreach (KeyValuePair<string, HashSet<ProjectItem>> itemGroup in this._items)
                {
                    foreach (ProjectItem item in itemGroup.Value)
                    {
                        item.UpdateIncludeValueFromScopePathChange(oldValue);
                    }
                }
            }
        }

        /// <summary>
        /// Gets the name for this project collection using the filename from the full path.
        /// </summary>
        public string Name
        {
            get { return Path.GetFileNameWithoutExtension(this.FullPath); }
        }

        /// <summary>
        /// Retrieves a iterator around the project items that have the specified type. This
        /// will return a empty enumerable instead of null.
        /// </summary>
        /// <param name="itemType">
        /// The type of items that should be retrieved.
        /// </param>
        /// <returns>
        /// A iterator around the project items defined for this project with the specified
        /// item type.
        /// </returns>
        public IEnumerable<ProjectItem> this[string itemType]
        {
            get
            {
                HashSet<ProjectItem> items;
                if (!this._items.TryGetValue(itemType, out items))
                {
                    return Enumerable.Empty<ProjectItem>();
                }

                return items.AsEnumerable();
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Adds a new project item to this scope with the specified type and include value.
        /// </summary>
        /// <param name="type">
        /// The type of the new item.
        /// </param>
        /// <param name="include">
        /// The include value for the new item.
        /// </param>
        /// <returns>
        /// The new instance that has been created and added to this scope.
        /// </returns>
        public ProjectItem AddNewProjectItem(string type, string include)
        {
            ProjectItem existing = this.Find(include);
            if (existing != null)
            {
                return existing;
            }

            ProjectItem newItem = new ProjectItem(type, include, this);
            HashSet<ProjectItem> items;
            if (!this._items.TryGetValue(type, out items))
            {
                items = new HashSet<ProjectItem>(new ProjectItemComparer());
                this._items.Add(type, items);
            }

            items.Add(newItem);
            return newItem;
        }

        /// <summary>
        /// Adds the specified project item to this scope.
        /// </summary>
        /// <param name="item">
        /// The item to add to this scope.
        /// </param>
        public void AddProjectItem(ProjectItem item)
        {
            HashSet<ProjectItem> items;
            if (!this._items.TryGetValue(item.ItemType, out items))
            {
                items = new HashSet<ProjectItem>(new ProjectItemComparer());
                this._items.Add(item.ItemType, items);
            }

            items.Add(item);
        }

        /// <summary>
        /// Creates a deep copy of the current instance.
        /// </summary>
        /// <returns>
        /// A deep copy of the current System.Object.
        /// </returns>
        public override object DeepClone()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Attempts to find the project item contained within this scope that has the
        /// specified include value.
        /// </summary>
        /// <param name="include">
        /// The include value to search for.
        /// </param>
        /// <returns>
        /// The project item contained within this scope that has the specified include value
        /// if found; otherwise, null.
        /// </returns>
        public ProjectItem Find(string include)
        {
            ItemIncludeComparer comparer = new ItemIncludeComparer();
            foreach (HashSet<ProjectItem> set in this._items.Values)
            {
                foreach (ProjectItem item in set)
                {
                    if (comparer.Equals(item, include))
                    {
                        return item;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Attempts to find the project item contained within this scope that has the
        /// specified filename value.
        /// </summary>
        /// <param name="filename">
        /// The filename value to search for.
        /// </param>
        /// <returns>
        /// The project item contained within this scope that has the specified filename value
        /// if found; otherwise, null.
        /// </returns>
        public ProjectItem FindFilename(string filename)
        {
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;
            foreach (KeyValuePair<string, HashSet<ProjectItem>> set in this._items)
            {
                if (String.Equals("Folder", set.Key))
                {
                    continue;
                }

                foreach (ProjectItem item in set.Value)
                {
                    string itemFilename = Path.GetFileName(item.Include);
                    if (comparer.Equals(itemFilename, filename))
                    {
                        return item;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Retrieves a unique version of the specified string based on the current contents
        /// of this scope. The string will become unique by adding a numerical value to the
        /// end and incrementing it until uniqueness is found.
        /// </summary>
        /// <param name="baseFilename">
        /// The base filename that needs to be unique.
        /// </param>
        /// <returns>
        /// A unique version of the specified filename.
        /// </returns>
        public string GetUniqueFilename(string baseFilename)
        {
            int index = 1;
            string name = Path.GetFileNameWithoutExtension(baseFilename);
            while (name.Contains('.'))
            {
                name = Path.GetFileNameWithoutExtension(name);
            }

            string extension = baseFilename.Replace(name, String.Empty);
            string format = name + "{0}" + extension;
            string filename = format.FormatInvariant(index.ToString());
            while (this.FindFilename(filename) != null)
            {
                index++;
                filename = format.FormatInvariant(index.ToString());
            }

            return filename;
        }

        /// <summary>
        /// Retrieves a unique version of the specified string based on the current contents
        /// of this scope. The string will become unique by adding a numerical value to the
        /// end and incrementing it until uniqueness is found.
        /// </summary>
        /// <param name="baseInclude">
        /// The base include path that needs to be unique.
        /// </param>
        /// <returns>
        /// A unique version of the specified string include.
        /// </returns>
        public string GetUniqueInclude(string baseInclude)
        {
            int index = 1;
            string format = baseInclude + "{0}";
            string include = format.FormatInvariant(index.ToString());

            while (this.Find(include) != null)
            {
                index++;
                include = format.FormatInvariant(index.ToString());
            }

            return include;
        }

        /// <summary>
        /// Loads the data contained within the file located at the specified path.
        /// </summary>
        /// <param name="fullPath">
        /// The path to the file that contains the data to load.
        /// </param>
        public void Load(string fullPath)
        {
            try
            {
                this._items = new Dictionary<string, HashSet<ProjectItem>>();
                foreach (MetadataLevel level in Enum.GetValues(typeof(MetadataLevel)))
                {
                    string extension = MetadataLevelUtil.GetExtensionAttribute(level);
                    if (extension == null)
                    {
                        continue;
                    }

                    string filename = fullPath + extension;
                    if (File.Exists(filename))
                    {
                        using (XmlReader reader = new XmlTextReader(fullPath + extension))
                        {
                            reader.MoveToContent();
                            this.Deserialise(reader, level);
                        }
                    }
                }
            }
            catch (XmlException ex)
            {
                throw ProjectLoadException.XmlException(fullPath, ex);
            }
            catch (ProjectLoadException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw ProjectLoadException.GenericException(fullPath, ex);
            }

            this._fullPath = fullPath;
            this._directoryPath = Path.GetDirectoryName(fullPath);
        }

        /// <summary>
        /// Removes the first occurrence of the specified item from this scope.
        /// </summary>
        /// <param name="item">
        /// The item to remove.
        /// </param>
        public void RemoveProjectItem(ProjectItem item)
        {
            foreach (HashSet<ProjectItem> itemSet in this._items.Values)
            {
                itemSet.Remove(item);
                string itemInclude = item.Include;
                List<ProjectItem> itemsToRemove = new List<ProjectItem>();
                foreach (ProjectItem setItem in itemSet)
                {
                    if (setItem.Include.StartsWith(itemInclude + "\\"))
                    {
                        itemsToRemove.Add(setItem);
                    }
                    else
                    {
                        string filter = setItem.GetMetadata("Filter");
                        if (filter != null && String.Equals(filter, itemInclude))
                        {
                            itemsToRemove.Add(setItem);
                        }
                    }
                }

                foreach (ProjectItem itemToRemove in itemsToRemove)
                {
                    itemSet.Remove(itemToRemove);
                }
            }
        }

        /// <summary>
        /// Saves the current state of this instances specified metadata level to the specified
        /// stream.
        /// </summary>
        /// <param name="stream">
        /// The stream to write this instances data to.
        /// </param>
        /// <param name="metadataLevel">
        /// The metadata level that needs to be serialised into the specified stream.
        /// </param>
        /// <returns>
        /// True if the save finished successfully; otherwise, false.
        /// </returns>
        public bool Save(Stream stream, MetadataLevel metadataLevel)
        {
            try
            {
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Indent = true;
                using (XmlWriter writer = XmlWriter.Create(stream, settings))
                {
                    writer.WriteStartDocument();
                    writer.WriteStartElement("Project");

                    this.SerialiseItemGroups(writer, metadataLevel);

                    writer.WriteEndElement();
                    writer.WriteEndDocument();
                }
            }
            catch
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Unloads the currently loaded collection, resetting all of the properties back to
        /// default.
        /// </summary>
        public void Unload()
        {
            this._items = new Dictionary<string, HashSet<ProjectItem>>();

            this._fullPath = null;
            this._directoryPath = null;
        }

        /// <summary>
        /// Determines whether any data exists for a specific metadata level on any of the
        /// project items.
        /// </summary>
        /// <param name="level">
        /// The level to test.
        /// </param>
        /// <returns>
        /// True if any of the project items for this collection contain metadata for the
        /// specified level; otherwise, false.
        /// </returns>
        private bool ContainsData(MetadataLevel level)
        {
            if (level == MetadataLevel.Core)
            {
                return true;
            }

            foreach (HashSet<ProjectItem> itemGroup in this._items.Values)
            {
                foreach (ProjectItem item in itemGroup)
                {
                    if (item.ContainsData(level))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Initialises this instance using the data contained within the specified xml reader.
        /// </summary>
        /// <param name="reader">
        /// The xml reader that contains the data used to initialise this instance.
        /// </param>
        /// <param name="level">
        /// The metadata level that is currently being deserialised.
        /// </param>
        private void Deserialise(XmlReader reader, MetadataLevel level)
        {
            reader.ValidateStartPosition("Project");
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() == XmlNodeType.Element)
            {
                if (String.Equals("ItemGroup", reader.Name))
                {
                    this.DeserialiseItemGroup(reader, level);
                }
                else
                {
                    reader.Skip();
                }
            }

            if (reader.NodeType == XmlNodeType.EndElement)
            {
                reader.ReadEndElement();
            }
        }

        /// <summary>
        /// Creates a set of project items from a single item group.
        /// </summary>
        /// <param name="reader">
        /// The xml reader that contains the data for the item group.
        /// </param>
        /// <param name="level">
        /// The metadata level that is currently being deserialised.
        /// </param>
        private void DeserialiseItemGroup(XmlReader reader, MetadataLevel level)
        {
            reader.ValidateStartPosition("ItemGroup");
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() == XmlNodeType.Element)
            {
                string type = reader.Name;
                string include = reader.GetAttribute("Include");
                if (String.IsNullOrWhiteSpace(include))
                {
                    reader.Skip();
                    continue;
                }

                ProjectItem newItem = this.AddNewProjectItem(type, include);
                newItem.DeserialiseMetadata(reader, level);
            }

            if (reader.NodeType == XmlNodeType.EndElement)
            {
                reader.ReadEndElement();
            }
        }

        /// <summary>
        /// Serialises all of the item groups to the specified writer.
        /// </summary>
        /// <param name="writer">
        /// The writer that the item groups should be written out to.
        /// </param>
        /// <param name="level">
        /// The metadata level that should be serialised.
        /// </param>
        private void SerialiseItemGroups(XmlWriter writer, MetadataLevel level)
        {
            foreach (KeyValuePair<string, HashSet<ProjectItem>> itemGroup in this._items)
            {
                int validCount = 0;
                foreach (ProjectItem projectItem in itemGroup.Value)
                {
                    if (!projectItem.ContainsData(level) && level != MetadataLevel.Core)
                    {
                        continue;
                    }

                    validCount++;
                }

                if (validCount == 0)
                {
                    continue;
                }

                writer.WriteStartElement("ItemGroup");
                foreach (ProjectItem projectItem in itemGroup.Value)
                {
                    writer.WriteStartElement(projectItem.ItemType);
                    projectItem.Serialise(writer, level);
                    writer.WriteEndElement();
                }

                writer.WriteEndElement();
            }
        }
        #endregion Methods
    } // RSG.Project.Model.ProjectCollection {Class}
} // RSG.Project.Model {Namespace}
