﻿//---------------------------------------------------------------------------------------------
// <copyright file="Extensions.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Helpers
{
    using RSG.Base;
    using RSG.Metadata.Model.Converters;

    /// <summary>
    /// Provides extension methods on a string that adds conversion operations.
    /// </summary>
    public static class Extensions
    {
        #region Methods
        /// <summary>
        /// Converts the specified <paramref name="s"/> parameter to an instance of the type
        /// specified by the type parameter.
        /// </summary>
        /// <typeparam name="T">
        /// The type that the specified string object will be converted to.
        /// </typeparam>
        /// <param name="s">
        /// The string instance to convert.
        /// </param>
        /// <param name="fallback">
        /// The value that will be returned if the conversion fails.
        /// </param>
        /// <param name="converters">
        /// The collection containing the converter objects to use.
        /// </param>
        /// <returns>
        /// The converted value if the conversion is successful; otherwise, the specified
        /// <paramref name="fallback"/> value.
        /// </returns>
        public static T To<T>(this string s, T fallback, ConverterCollection converters)
        {
            IConverter<T> converter = converters.GetConverter<T>();
            return converter.Convert(s, fallback, Methods.Any);
        }

        /// <summary>
        /// Converts the specified <paramref name="s"/> parameter to an array of values of the
        /// specified type.
        /// </summary>
        /// <typeparam name="T">
        /// The type that the specified string object will be converted to.
        /// </typeparam>
        /// <param name="s">
        /// The string instance to convert.
        /// </param>
        /// <param name="count">
        /// The number of values the <paramref name="s"/> parameter should be converted to.
        /// </param>
        /// <param name="fallback">
        /// The value that will be returned if the conversion fails.
        /// </param>
        /// <param name="converters">
        /// The collection containing the converter objects to use.
        /// </param>
        /// <returns>
        /// The converted value if the conversion is successful; otherwise, the specified
        /// <paramref name="fallback"/> value.
        /// </returns>
        public static T[] To<T>(
            this string s, int count, T fallback, ConverterCollection converters)
        {
            IConverter<T> converter = converters.GetConverter<T>();
            return converter.Convert(s, count, fallback, Methods.Any);
        }

        /// <summary>
        /// Attempts to convert the specified <paramref name="s"/> parameter to an instance of
        /// the type specified by the type parameter.
        /// </summary>
        /// <typeparam name="T">
        /// The type that the specified string object will be converted to.
        /// </typeparam>
        /// <param name="s">
        /// The string instance to convert.
        /// </param>
        /// <param name="fallback">
        /// The value that will be returned if the conversion fails.
        /// </param>
        /// <param name="converters">
        /// The collection containing the converter objects to use.
        /// </param>
        /// <returns>
        /// A structure containing the converted value and a value indicating whether the
        /// conversion was successful.
        /// </returns>
        public static TryResult<T> TryTo<T>(
            this string s, T fallback, ConverterCollection converters)
        {
            IConverter<T> converter = converters.GetConverter<T>();
            return converter.TryConvert(s, fallback, Methods.Any);
        }

        /// <summary>
        /// Attempts to convert the specified <paramref name="s"/> parameter to an array of
        /// values of the specified type.
        /// </summary>
        /// <typeparam name="T">
        /// The type that the specified string object will be converted to.
        /// </typeparam>
        /// <param name="s">
        /// The string instance to convert.
        /// </param>
        /// <param name="count">
        /// The number of values the <paramref name="s"/> parameter should be converted to.
        /// </param>
        /// <param name="fallback">
        /// The value that will be returned if the conversion fails.
        /// </param>
        /// <param name="converters">
        /// The collection containing the converter objects to use.
        /// </param>
        /// <returns>
        /// A structure containing the converted value and a value indicating whether the
        /// conversion was successful.
        /// </returns>
        public static TryResult<T[]> TryTo<T>(
            this string s, int count, T fallback, ConverterCollection converters)
        {
            IConverter<T> converter = converters.GetConverter<T>();
            return converter.TryConvert(s, count, fallback, Methods.Any);
        }

        /// <summary>
        /// Determines whether this string object can be successfully converted to a instance
        /// of the specified type.
        /// </summary>
        /// <typeparam name="T">
        /// The type that this string will be tested against.
        /// </typeparam>
        /// <param name="s">
        /// The string instance to test.
        /// </param>
        /// <param name="treatNullAsValid">
        /// A value indicating whether a null value is handled as a success.
        /// </param>
        /// <param name="converters">
        /// The collection containing the converter objects to use.
        /// </param>
        /// <returns>
        /// True if this string can be successfully converted to the specified type; otherwise,
        /// false.
        /// </returns>
        public static bool Validate<T>(
            this string s, bool treatNullAsValid, ConverterCollection converters)
        {
            if (treatNullAsValid && s == null)
            {
                return true;
            }

            return s.TryTo<T>(default(T), converters).Success;
        }

        /// <summary>
        /// Determines whether this string object can be successfully converted to an array of
        /// of the specified type of the specified count.
        /// </summary>
        /// <typeparam name="T">
        /// The type that this string will be tested against.
        /// </typeparam>
        /// <param name="s">
        /// The string instance to test.
        /// </param>
        /// <param name="count">
        /// The number of instances this string should represent.
        /// </param>
        /// <param name="treatNullAsValid">
        /// A value indicating whether a null value is handled as a success.
        /// </param>
        /// <param name="converters">
        /// The collection containing the converter objects to use.
        /// </param>
        /// <returns>
        /// True if this string can be successfully converted to the specified number of the
        /// specified type; otherwise, false.
        /// </returns>
        public static bool Validate<T>(
            this string s, int count, bool treatNullAsValid, ConverterCollection converters)
        {
            if (treatNullAsValid && s == null)
            {
                return true;
            }

            return s.TryTo<T>(count, default(T), converters).Success;
        }
        #endregion Methods
    } // RSG.Metadata.Model.Helpers.Extensions {Class}
} // RSG.Metadata.Model.Helpers {Namespace}
