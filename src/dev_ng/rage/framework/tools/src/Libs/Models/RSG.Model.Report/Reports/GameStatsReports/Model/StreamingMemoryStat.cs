﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Report.Reports.GameStatsReports.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class StreamingMemoryStat
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public string CategoryName
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public uint Virtual
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public uint Physical
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="virt"></param>
        /// <param name="physical"></param>
        public StreamingMemoryStat(string categoryName, uint virt, uint physical)
        {
            CategoryName = categoryName;
            Virtual = virt;
            Physical = physical;
        }
        #endregion // Constructor(s)
    } // StreamingMemoryStat
}
