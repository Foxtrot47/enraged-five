﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common.Map;
using RSG.SceneXml;
using RSG.Statistics.Common.Dto.GameAssetStats.Bundles;

namespace RSG.Model.Map3
{
    public class MapDrawableLOD : IMapDrawableLOD
    {
        #region properties 

        /// <summary>
        /// 
        /// </summary>
        public string Name
        {
            get;
            protected set;
        }

        public float LodDistance
        {
            get;
            protected set;
        }

        public uint PolygonCount
        {
            get;
            protected set;
        }

        public MapDrawableLODLevel LODLevel
        {
            get;
            protected set;
        }
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public MapDrawableLOD(TargetObjectDef sceneObject, MapDrawableLODLevel lodLvl)
        {
            Name = sceneObject.Name;
            LodDistance = sceneObject.GetLODDistance();
            PolygonCount = (uint)sceneObject.PolyCount;            
            LODLevel = lodLvl;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public MapDrawableLOD(DrawableLodStatDto dto)
        {
            Name = dto.Name;
            LodDistance = dto.LodDistance;
            PolygonCount = dto.PolygonCount;
            LODLevel = dto.LodLevel;
        }
        #endregion

    } // MapDrawableLOD
}
