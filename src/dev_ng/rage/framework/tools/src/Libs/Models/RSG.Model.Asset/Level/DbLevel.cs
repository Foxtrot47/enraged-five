﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Media.Imaging;
using RSG.Base.Logging;
using RSG.Model.Common;
using RSG.Model.Common.Map;
using RSG.Statistics.Client.ServiceClients;
using RSG.Statistics.Common.Dto.GameAssetStats;

namespace RSG.Model.Asset.Level
{
    /// <summary>
    /// 
    /// </summary>
    public class DbLevel : Level
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public DbLevel(String name, ILevelCollection parentCollection)
            : base(name, parentCollection)
        {
        }
        #endregion // Constructor(s)

        #region StreamableAssetContainerBase Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="statsToLoad"></param>
        protected override void LoadStatsInternal(IEnumerable<StreamableStat> statsToLoad)
        {
            Debug.Assert(ParentCollection is SingleBuildLevelCollection, "Parent collection isn't an SingleBuildLevelCollection.");
            SingleBuildLevelCollection dbCollection = (SingleBuildLevelCollection)ParentCollection;

            if (!AreStatsLoaded(statsToLoad))
            {
                try
                {
                    using (BuildClient client = new BuildClient())
                    {
                        // Construct the request object and query the database for the data.
                        List<uint> identifiers = new List<uint>();
                        identifiers.Add(Hash);

                        StreamableStatFetchDto fetchDto = new StreamableStatFetchDto(identifiers, statsToLoad.ToList());
                        IList<LevelStatDto> levelDtos = client.GetMultipleLevelStats(dbCollection.BuildIdentifier, fetchDto);

                        foreach (LevelStatDto levelDto in levelDtos)
                        {
                            if (levelDto.Identifier == Hash)
                            {
                                LoadStatsFromDatabase(levelDto, statsToLoad);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    Log.Log__Exception(e, "Unhandled exception while retrieving weapon stats.");
                }
            }
        }
        #endregion // StreamableAssetContainerBase Overrides

        #region Internal Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="bundle"></param>
        internal void LoadStatsFromDatabase(LevelStatDto dto, IEnumerable<StreamableStat> requestedStats)
        {
            IDictionary<StreamableStat, PropertyInfo> modelLookup = StreamableStatUtils.GetPropertyInfoLookup(GetType());
            IDictionary<StreamableStat, PropertyInfo> dtoLookup = StreamableStatUtils.GetPropertyInfoLookup(dto.GetType());

            // Map the properties we are after.
            foreach (StreamableStat stat in requestedStats.Where(item => !IsStatLoaded(item)))
            {
                if (dtoLookup.ContainsKey(stat) && modelLookup.ContainsKey(stat))
                {
                    // Is this a complex item?
                    if (stat.DtoToModelRequiresProcessing)
                    {
                        ProcessComplexLevelStat(dto, stat);
                    }
                    else
                    {
                        object value = dtoLookup[stat].GetValue(dto, null);
                        modelLookup[stat].SetValue(this, value, null);
                    }
                }
                else
                {
                    Log.Log__Error(String.Format("One of the MapSectionStat dto class or the model class doesn't have the '{0}' stat marked up.", stat));
                }
            }
        }
        #endregion // Internal Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="stat"></param>
        private void ProcessComplexLevelStat(LevelStatDto dto, StreamableStat stat)
        {
            if (stat == StreamableLevelStat.BackgroundImage)
            {
                ProcessBackgroundImageStat(dto.ImageData);
            }
            else if (stat == StreamableLevelStat.SpawnPoints)
            {
                ProcessSpawnPointStats(dto.SpawnPoints);
            }
            else if (stat == StreamableLevelStat.SpawnPointClusters)
            {
                //ProcessArchetypeStats(dto.Archetypes);
                SpawnPointClusters = new List<ISpawnPointCluster>();
            }
            else if (stat == StreamableLevelStat.ChainingGraphs)
            {
                //ProcessArchetypeStats(dto.Archetypes);
                ChainingGraphs = new List<IChainingGraph>();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="imageData"></param>
        private void ProcessBackgroundImageStat(byte[] imageData)
        {
            if (imageData != null)
            {
                BitmapImage bImg = new BitmapImage();
                bImg.BeginInit();
                bImg.StreamSource = new MemoryStream(imageData);
                bImg.EndInit();

                Image = bImg;
                Image.Freeze();
            }
            else
            {
                SetStatLodaded(StreamableLevelStat.BackgroundImage, true);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dtos"></param>
        private void ProcessSpawnPointStats(List<SpawnPointStatDto> dtos)
        {
            SpawnPoints = new List<ISpawnPoint>();

            foreach (SpawnPointStatDto dto in dtos)
            {
                SpawnPoint spawnPoint = new SpawnPoint(dto.Name);
                spawnPoint.LoadStatsFromDatabase(dto);
                SpawnPoints.Add(spawnPoint);
            }
        }
        #endregion // Private Methods
    } // DbLevel
}
