﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Report
{
    /// <summary>
    /// 
    /// </summary>
    public interface IReportFileProvider
    {
        /// <summary>
        /// Filter used for the save dialog
        /// </summary>
        string Filter
        {
            get;
        }

        /// <summary>
        /// Extensions that this report support
        /// </summary>
        string SupportedExtension
        {
            get;
        }

        /// <summary>
        /// Filename specifying where a report should be output
        /// </summary>
        string Filename
        {
            get;
            set;
        }
    }
} // RSG.Model.Report
