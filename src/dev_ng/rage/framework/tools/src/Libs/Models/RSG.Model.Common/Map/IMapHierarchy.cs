﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Collections;
using RSG.Base.Tasks;

namespace RSG.Model.Common.Map
{
    /// <summary>
    /// Root of the map hierarchy
    /// </summary>
    public interface IMapHierarchy : IAsset
    {
        #region Properties
        /// <summary>
        /// Reference to the owning level
        /// </summary>
        ILevel OwningLevel { get; }

        /// <summary>
        /// Collection of child map nodes
        /// </summary>
        ObservableCollection<IMapNode> ChildNodes { get; }

        /// <summary>
        /// Returns all areas in this map hierarchy
        /// </summary>
        IEnumerable<IMapArea> AllAreas { get; }

        /// <summary>
        /// Returns all sections in this map hierarchy
        /// </summary>
        IEnumerable<IMapSection> AllSections { get; }

        /// <summary>
        /// A map of archetype names to archetypes to be used for optimising when we
        /// patch up archetype <-> entity references
        /// </summary>
        IDictionary<String, IMapArchetype> ArchetypeLookup { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Helper function for adding an archetype to our lookup.
        /// </summary>
        void AddArchetypeToLookup(IMapArchetype archetype);

        /// <summary>
        /// Load specific statistics for a single section.
        /// </summary>
        /// <param name="section"></param>
        /// <param name="statsToLoad"></param>
        /// <param name="async"></param>
        void RequestStatisticsForSection(IMapSection section, IEnumerable<StreamableStat> statsToLoad, bool async);

        /// <summary>
        /// Load specific statistics for a single section.
        /// </summary>
        /// <param name="section"></param>
        /// <param name="statsToLoad"></param>
        /// <param name="async"></param>
        void RequestStatisticsForSections(IEnumerable<IMapSection> sections, IEnumerable<StreamableStat> statsToLoad, bool async);

        /// <summary>
        /// Load specific statistics for a list of map archetypes.
        /// </summary>
        /// <param name="fragment"></param>
        /// <param name="statsToLoad"></param>
        /// <param name="async"></param>
        void RequestStatisticsForMapArchetypes(IEnumerable<IMapArchetype> archetypes, IEnumerable<StreamableStat> statsToLoad, bool async);

        /// <summary>
        /// Load specific statistics for a drawable archetype.
        /// </summary>
        /// <param name="fragment"></param>
        /// <param name="statsToLoad"></param>
        /// <param name="async"></param>
        void RequestStatisticsForDrawable(IDrawableArchetype drawable, IEnumerable<StreamableStat> statsToLoad, bool async);

        /// <summary>
        /// Load specific statistics for a fragment archetype.
        /// </summary>
        /// <param name="fragment"></param>
        /// <param name="statsToLoad"></param>
        /// <param name="async"></param>
        void RequestStatisticsForFragment(IFragmentArchetype fragment, IEnumerable<StreamableStat> statsToLoad, bool async);

        /// <summary>
        /// Load specific statistics for an interior archetype.
        /// </summary>
        /// <param name="fragment"></param>
        /// <param name="statsToLoad"></param>
        /// <param name="async"></param>
        void RequestStatisticsForInterior(IInteriorArchetype interior, IEnumerable<StreamableStat> statsToLoad, bool async);

        /// <summary>
        /// Load specific statistics for a stated anim archetype.
        /// </summary>
        /// <param name="fragment"></param>
        /// <param name="statsToLoad"></param>
        /// <param name="async"></param>
        void RequestStatisticsForStatedAnim(IStatedAnimArchetype statedAnim, IEnumerable<StreamableStat> statsToLoad, bool async);

        /// <summary>
        /// Load specific statistics for a room.
        /// </summary>
        /// <param name="fragment"></param>
        /// <param name="statsToLoad"></param>
        /// <param name="async"></param>
        void RequestStatisticsForRoom(IRoom room, IEnumerable<StreamableStat> statsToLoad, bool async);

        /// <summary>
        /// Load specific statistics for an entity.
        /// </summary>
        /// <param name="fragment"></param>
        /// <param name="statsToLoad"></param>
        /// <param name="async"></param>
        void RequestStatisticsForEntity(IEntity entity, IEnumerable<StreamableStat> statsToLoad, bool async);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="statsToLoad"></param>
        /// <param name="async"></param>
        void RequestStatisticsForEntities(IEnumerable<IEntity> entities, IEnumerable<StreamableStat> statsToLoad, bool async);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sections"></param>
        /// <param name="statsToLoad"></param>
        /// <returns></returns>
        ITask CreateStatLoadTask(IEnumerable<IMapSection> sections, IEnumerable<StreamableStat> statsToLoad);
        #endregion // Methods
    } // IMapHierarchy
}
