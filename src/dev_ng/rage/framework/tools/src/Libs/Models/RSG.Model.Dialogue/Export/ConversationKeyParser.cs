﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;

namespace RSG.Model.Dialogue.Export
{
    /// <summary>
    /// Match keys from a given file to their parent .dstar files.
    /// </summary>
    class ConversationKeyParser
    {
        #region Private member fields

        private string m_keyFile;
        private string m_dstarFolder;

        private List<string> m_keys;
        private Dictionary<string, List<string>> m_keyMappings;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="keyFile">File containing conversation keys.</param>
        /// <param name="dstarFolder">Folder containing .dstar files.</param>
        public ConversationKeyParser(string keyFile, string dstarFolder)
        {
            m_keyMappings = new Dictionary<string, List<string>>();
            m_keyFile = keyFile;
            m_dstarFolder = dstarFolder;
            m_keys = new List<string>();
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Parse the files in the folder and match the keys to the files.
        /// </summary>
        public void Parse()
        {
            GetKeys();
            ParseFolder();
        }

        #endregion

        #region Private helper methods

        ///// <summary>
        ///// Display the results as a file \ key hierarchy.
        ///// </summary>
        //private void DisplayResults()
        //{
        //    foreach (string file in m_keyMappings.Keys)
        //    {
        //        Console.WriteLine(file);

        //        foreach (string key in m_keyMappings[file])
        //        {
        //            Console.WriteLine("\t{0}", key);
        //        }
        //    }
        //}

        /// <summary>
        /// Parse the folder.
        /// </summary>
        private void ParseFolder()
        {
            string[] files = Directory.GetFiles(m_dstarFolder, "*.dstar");
            foreach (string file in files)
            {
                //Console.Error.Write("{0}\r", file);
                ParseFile(file);
                //Console.Error.Write(" ".PadRight(file.Length) + "\r");
            }
        }

        /// <summary>
        /// Parse a single file.
        /// </summary>
        /// <param name="file">File.</param>
        private void ParseFile(string file)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(file);

            foreach (string key in m_keys)
            {
                XmlNodeList nodes = doc.SelectNodes("//Conversation[@root='" + key + "']");
                if (nodes.Count > 0)
                {
                    if (!m_keyMappings.ContainsKey(file))
                    {
                        m_keyMappings.Add(file, new List<string>());
                    }

                    m_keyMappings[file].Add(key);
                }
            }
        }

        /// <summary>
        /// Get the collection of keys from the key file.
        /// </summary>
        private void GetKeys()
        {
            using (Stream s = File.Open(m_keyFile, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                using (TextReader reader = new StreamReader(s))
                {
                    string line = String.Empty;
                    while ((line = reader.ReadLine()) != null)
                    {
                        if (!String.IsNullOrEmpty(line.Trim()))
                        {
                            m_keys.Add(line);
                        }
                    }
                }
            }
        }

        #endregion
    }
}
