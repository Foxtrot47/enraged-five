﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using RSG.Model.Common;
using RSG.Base.Configuration.Reports;

namespace RSG.Model.Report
{
    /// <summary>
    /// ReportUri - A report that has a Uri - file or URL to display.
    /// </summary>
    public abstract class Report : AssetBase, IReport
    {
        #region Properties
        /// <summary>
        /// Report description string.
        /// </summary>
        public String Description
        {
            get { return m_sDescription; }
            set
            {
                SetPropertyValue(value, () => this.Description,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_sDescription = (String)newValue;
                        }
                ));
            }
        }
        private String m_sDescription;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="desc"></param>
        public Report(string name, string desc)
            : base(name)
        {
            Description = desc;
        }
        #endregion // Constructor(s)
    } // Report
} // RSG.Model.Report
