﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using RSG.Base.Extensions;

namespace RSG.Model.Statistics.Captures.Historical
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    public class HistoricalResults
    {
        #region Constants
        /// <summary>
        /// Epsilon value to use when determining whether a reading is close enough to zero to be zero.
        /// </summary>
        private const float c_epsilon = 0.0001f;
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Mapping of changelist numbers to values.
        /// </summary>
        [DataMember]
        public IDictionary<Object, float> Values { get; private set; }

        /// <summary>
        /// Tracks the standard deviations to calculate the overall standard deviation for these results.
        /// </summary>
        private IList<double> StandardDeviations { get; set; }

        /// <summary>
        /// Standard deviation of the results.
        /// </summary>
        [DataMember]
        public double StandardDeviation { get; set; }

        /// <summary>
        /// Retrieves the most recent value.
        /// </summary>
        public float MostRecentValue
        {
            get
            {
                return Values.Values.LastOrDefault();
            }
        }

        /// <summary>
        /// Retrieves the previous value.
        /// </summary>
        public float PreviousValue
        {
            get
            {
                if (Values.Count > 1)
                {
                    return Values.Values.ElementAt(Values.Count - 2);
                }
                else
                {
                    return 0.0f;
                }
            }
        }

        /// <summary>
        /// Returns the difference of the most recent two values.
        /// </summary>
        public float LatestDifference
        {
            get
            {
                float[] values = Values.Values.ToArray();
                if (values.Length > 1)
                {
                    return (MostRecentValue - PreviousValue);
                }
                else
                {
                    return 0.0f;
                }
            }
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public HistoricalResults()
        {
            Values = new SortedDictionary<Object, float>();
            StandardDeviations = new List<double>();
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="value"></param>
        /// <param name="std"></param>
        public void AddResult(Object context, float value, double std)
        {
            if (!Values.ContainsKey(context))
            {
                if (AlmostZero(value))
                {
                    value = 0.0f;
                }
                Values.Add(context, value);
                StandardDeviations.Add(std);
                RecalculateStandardDeviation();
            }
        }
        #endregion // Public Methods

        #region Private Methods
        /// <summary>
        /// Checks whether a value is almost zero based on a threshold.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private bool AlmostZero(float value)
        {
            return (Math.Abs(value) < c_epsilon);
        }

        /// <summary>
        /// Recomputes the standard deviation based on the values stored in the historical std array.
        /// </summary>
        private void RecalculateStandardDeviation()
        {
            if (StandardDeviations.Any())
            {
                double sumOfSquares = StandardDeviations.Sum(item => item * item);
                double count = StandardDeviations.Count;
                double stdSquared = sumOfSquares / count;
                StandardDeviation = Math.Sqrt(stdSquared);
            }
            else
            {
                StandardDeviation = 0.0;
            }
        }
        #endregion // Private Methods
    } // HistoricalResults
}
