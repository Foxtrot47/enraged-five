﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using RSG.Base;
using RSG.Base.IO;
using RSG.Base.Logging;
using RSG.SceneXml;
using RSG.ManagedRage.file;

namespace RSG.Model.Map
{

    /// <summary>
    /// 
    /// </summary>
    public class PropUserData
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Associated MapSection object.
        /// </summary>
        public MapSection MapSection
        {
            get { return m_MapSection; }
            private set { m_MapSection = value; }
        }
        private MapSection m_MapSection;

        /// <summary>
        /// 
        /// </summary>
        public Scene SceneXml
        {
            get { return m_SceneXml; }
            private set { m_SceneXml = value; }
        }
        private Scene m_SceneXml;

        /// <summary>
        /// Map Section list of Prop objects.
        /// </summary>
        public Dictionary<String, Prop> Props
        {
            get { return m_Props; }
            private set { m_Props = value; }
        }
        private Dictionary<String, Prop> m_Props;

        /// <summary>
        /// 
        /// </summary>
        public String MaxFilename
        {
            get { return m_sMaxFilename; }
            private set { m_sMaxFilename = value; }
        }
        private String m_sMaxFilename;
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public PropUserData(MapSection parent, String thumbPrefixFolder)
        {
            this.MapSection = parent;
            this.Props = new Dictionary<String, Prop>();
            ParseRpfFile();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Find props with a particular name (case-insensetive).
        /// </summary>
        /// <param name="propName">Prop name to search for.</param>
        /// <param name="prop">Array of Model.Prop found.</param>
        /// <returns>true iff props are found in this level, false otherwise.</returns>
        public bool FindPropsByName(String propName, out Prop[] props)
        {
            String propNameUp = propName.ToUpper();
            Wildcard wildcard = new Wildcard(propName, RegexOptions.IgnoreCase);
            List<Prop> propList = new List<Prop>();
            foreach (Prop prop in this.Props.Values)
            {
                String testPropUp = prop.Name.ToUpper();
                if (testPropUp.Contains(propNameUp) || wildcard.IsMatch(prop.Name))
                    propList.Add(prop);
            }
            props = propList.ToArray();
            return (propList.Count > 0);
        }

        /// <summary>
        /// Find props with particular flag attributes.
        /// </summary>
        /// <param name="filter">Filter flags.</param>
        /// <param name="props">Array of Model.Prop found.</param>
        /// <returns>true iff props are found in this level, false otherwise.</returns>
        public bool FindPropsByFilter(PropFilter filter, out Prop[] props)
        {
#warning DHM FIX ME
#if false
            List<Prop> propList = new List<Prop>();
            foreach (Prop prop in this.Props.Values)
            {
                PropFilter thisPropFilter = prop.CurrentFilter;

                if (filter == PropFilter.None)
                    propList.Add(prop);
                else if ((filter & thisPropFilter) == filter)
                    propList.Add(prop);
            }
            props = propList.ToArray();
            return (propList.Count > 0);
#endif
            props = null;
            return (false);
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private void ParseRpfFile()
        {
            cPackFileBuilder packFile = new cPackFileBuilder();
            if (!packFile.Init(this.MapSection.RpfFilename, true))
            {
                Log.Log__Error("RPF file does not exist or is unreadable: {0}.", this.MapSection.RpfFilename);
                return;
            }

            String sceneXmlFilename = Path.GetFileName(Path.ChangeExtension(this.MapSection.RpfFilename, "xml"));
            cRPF_Node node = null;
            try
            {
                node = packFile.FindEntry(sceneXmlFilename);
            }
            catch (NullReferenceException ex)
            {
                Log.Log__Exception(ex, "No SceneXml name found ({0}).", sceneXmlFilename);
                return;
            }
            if (!(node is cRPF_FileNode))
            {
                Log.Log__Error("Found a SceneXml name but it is not a file!");
                return;
            }

            cRPF_FileNode fNode = (node as cRPF_FileNode);
            tHandle hHandle = packFile.Open(fNode.RetFullPath());
            if (hHandle.IsValid())
            {
                Byte[] data = new Byte[fNode.RetUncompressedSize()];
                packFile.Read(hHandle, data, (int)fNode.RetUncompressedSize());

                MemoryStream stream = new MemoryStream(data);
                StreamReader reader = new StreamReader(stream);
                this.SceneXml = new Scene(reader);
                ParseScene();
            }
            else
            {
                Log.Log__Error("Failed to open SceneXml file: {0}.", fNode.RetFullPath());
            }
        }

        /// <summary>
        /// Parse the SceneXml Scene object for props.
        /// </summary>
        private void ParseScene()
        {
            this.MaxFilename = this.SceneXml.ExportFilename;
            this.MaxFilename = this.MaxFilename.Replace("\\", "/");

            foreach (ObjectDef sceneObject in this.SceneXml.Walk(Scene.WalkMode.BreadthFirst))
            {
                if (sceneObject.IsXRef())
                    continue;
                if (sceneObject.IsInternalRef())
                    continue;
                if (sceneObject.HasParent())
                    continue;
                if (!sceneObject.IsObject())
                    continue;
                if (sceneObject.DontExport())
                    continue;

                String name = sceneObject.GetObjectName();
                Debug.Assert(!this.Props.ContainsKey(name), "Duplicate prop detected.");
                if (this.Props.ContainsKey(name))
                {
                    Log.Log__Error("Duplicate prop detected in map section {0}: {1}.",
                        Path.GetFileNameWithoutExtension(this.MapSection.RpfFilename), name);
                    continue;
                }
                this.Props.Add(name, new Prop(sceneObject, this));
            }
        }
        #endregion // Private Methods
    }

} // RSG.Model.Map namespace
