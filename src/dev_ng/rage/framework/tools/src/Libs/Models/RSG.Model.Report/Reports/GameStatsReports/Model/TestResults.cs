﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Collections.ObjectModel;
using RSG.Base.Logging;
using System.Reflection;

namespace RSG.Model.Report.Reports.GameStatsReports.Model
{
    public class TestResults
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public string Name
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public FpsResult FpsResult
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public ICollection<GpuResult> GpuResults
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public ICollection<CpuResult> CpuResults
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public ICollection<MemoryResult> MemoryResults
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public ICollection<StreamingMemoryResult> StreamingMemoryResults
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public ICollection<DrawListResult> DrawListResults
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public TestResults(XElement element)
        {
            GpuResults = new Collection<GpuResult>();
            CpuResults = new Collection<CpuResult>();
            MemoryResults = new Collection<MemoryResult>();
            StreamingMemoryResults = new Collection<StreamingMemoryResult>();
            DrawListResults = new Collection<DrawListResult>();

            Parse(element);
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="element"></param>
        private void Parse(XElement testElement)
        {
            // Extract the name
            XElement nameElement = testElement.Element("name");
            if (nameElement != null)
            {
                Name = nameElement.Value;
            }

            XElement fpsElement = testElement.Element("fpsResult");
            if (fpsElement != null)
            {
                FpsResult = new FpsResult(fpsElement);
            }

            XElement gpuElement = testElement.Element("gpuResults");
            if (gpuElement != null)
            {
                foreach (XElement item in gpuElement.Elements("Item"))
                {
                    GpuResults.Add(new GpuResult(item));
                }
            }

            XElement cpuElement = testElement.Element("cpuResults");
            if (cpuElement != null)
            {
                foreach (XElement item in cpuElement.Elements("Item"))
                {
                    CpuResults.Add(new CpuResult(item));
                }
            }

            XElement memoryElement = testElement.Element("memoryResults");
            if (memoryElement != null)
            {
                foreach (XElement item in memoryElement.Elements("Item"))
                {
                    MemoryResults.Add(new MemoryResult(item));
                }
            }

            XElement streamingMemoryElement = testElement.Element("streamingMemoryResults");
            if (streamingMemoryElement != null)
            {
                foreach (XElement item in streamingMemoryElement.Elements("Item"))
                {
                    StreamingMemoryResults.Add(new StreamingMemoryResult(item));
                }
            }

            XElement drawListElement = testElement.Element("drawListResults");
            if (drawListElement != null)
            {
                foreach (XElement item in drawListElement.Elements("Item"))
                {
                    DrawListResults.Add(new DrawListResult(item));
                }
            }
        }
        #endregion // Private Methods
    } // Test
}
