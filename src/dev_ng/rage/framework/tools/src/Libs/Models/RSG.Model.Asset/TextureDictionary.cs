﻿using System;
using System.Linq;
using System.Collections.Generic;
using RSG.Base.Collections;
using RSG.Model.Common;

namespace RSG.Model.Asset
{
    /// <summary>
    /// A texture dictionary represents a itd file that contains
    /// 'promoted textures' from a definition or container.
    /// </summary>
    public class TextureDictionary : FileAssetContainerBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public string SourceSectionName
        {
            get;
            set;
        }
        #endregion

        #region Constructors

        /// <summary>
        /// Default Constructor
        /// </summary>
        public TextureDictionary()
        {
        }

        /// <summary>
        /// Default Constructor
        /// </summary>
        public TextureDictionary(String name, String filename, String sourceSectionName)
            : base(name, filename)
        {
            this.SourceSectionName = sourceSectionName;
        }

        /// <summary>
        /// Default Constructor
        /// </summary>
        public TextureDictionary(String name, String filename, IEnumerable<IAsset> assets, string sourceSectionName)
            : base(name, filename)
        {
            this.SourceSectionName = sourceSectionName;

            AssetChildren.BeginUpdate();
            AssetChildren.AddRange(GetUniqueTextures(assets));
            AssetChildren.EndUpdate();
        }

        #endregion // Constructors

        #region Public Functions

        /// <summary>
        /// 
        /// </summary>
        public void AddAssetChildren(IEnumerable<IAsset> assets)
        {
            AssetChildren.BeginUpdate();
            AssetChildren.AddRange(GetUniqueTextures(assets));
            AssetChildren.EndUpdate();
        }

        /// <summary>
        /// Gets the unique textures in the input assets
        /// </summary>
        /// <param name="assets">input assets</param>
        /// <returns>unique textures per name</returns>
        private IList<ITexture> GetUniqueTextures(IEnumerable<IAsset> assets)
        {
            IList<ITexture> range = new List<ITexture>();
            foreach (var group in assets.OfType<ITexture>().ToLookup(a => a.Name))
            {
                ITexture texture = group.FirstOrDefault();
                if (texture != null && !ContainsAsset(texture.Name))
                {
                    range.Add(texture);
                }
            }

            return range;
        }


        #endregion // Public Functions
    } // TextureDictionary
} // RSG.Model.Asset namespace
