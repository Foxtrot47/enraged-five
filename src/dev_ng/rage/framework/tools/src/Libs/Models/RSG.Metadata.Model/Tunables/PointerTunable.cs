﻿//---------------------------------------------------------------------------------------------
// <copyright file="PointerTunable.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Tunables
{
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using RSG.Base.Extensions;
    using RSG.Base.Logging;
    using RSG.Editor;
    using RSG.Editor.Model;
    using RSG.Metadata.Model.Definitions;
    using RSG.Metadata.Model.Definitions.Members;
    using RSG.Metadata.Model.Resources;

    /// <summary>
    /// Represents a tunable used by the parCodeGen system that is instancing a
    /// <see cref="PointerMember"/> object.
    /// </summary>
    public class PointerTunable : TunableBase, ITunableParent
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="OwnedStructure"/> property.
        /// </summary>
        private IStructure _ownedStructure;

        /// <summary>
        /// The private field used for the <see cref="Tunables"/> property.
        /// </summary>
        private ReadOnlyModelCollection<ITunable> _readOnlyTunables;

        /// <summary>
        /// The private field used for the <see cref="Reference"/> property.
        /// </summary>
        private string _reference;

        /// <summary>
        /// The private field for the <see cref="Tunables"/> property.
        /// </summary>
        private ModelCollection<ITunable> _tunables;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="PointerTunable"/> class to be as
        /// instance of the specified member.
        /// </summary>
        /// <param name="member">
        /// The member that this tunable will be instancing.
        /// </param>
        /// <param name="parent">
        /// The tunable parent this tunable will belong to.
        /// </param>
        public PointerTunable(PointerMember member, ITunableParent parent)
            : base(member, parent)
        {
            this._tunables = new ModelCollection<ITunable>(this);
            if (member.OwnsObject && !member.CanBeDerivedType)
            {
                IStructure referenced = member.ReferencedStructure;
                this._ownedStructure = referenced;
                this.CreateTunables();
            }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="PointerTunable"/> class as a copy of
        /// the specified instance.
        /// </summary>
        /// <param name="other">
        /// The instance to copy.
        /// </param>
        /// <param name="parent">
        /// The tunable parent this tunable will belong to.
        /// </param>
        public PointerTunable(PointerTunable other, ITunableParent parent)
            : base(other, parent)
        {
            List<ITunable> tunables = new List<ITunable>();
            foreach (ITunable tunable in other.Tunables)
            {
                tunables.Add(tunable.Clone());
            }

            this._tunables = new ModelCollection<ITunable>(this, tunables);
            this._reference = other._reference;
            this._ownedStructure = other._ownedStructure;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="PointerTunable"/> class using the
        /// specified System.Xml.XmlReader as a initialising data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <param name="member">
        /// The member that this tunable will be instancing.
        /// </param>
        /// <param name="parent">
        /// The tunable parent this tunable will belong to.
        /// </param>
        /// <param name="log">
        /// A object that is used to log any warnings or errors produced during the parsing of
        /// the specified reader.
        /// </param>
        public PointerTunable(
            XmlReader reader, PointerMember member, ITunableParent parent, ILog log)
            : base(reader, member, parent)
        {
            this._tunables = new ModelCollection<ITunable>(this);
            this.Deserialise(reader, log);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the structure definition of the object that this pointer is pointing
        /// to.
        /// </summary>
        public IStructure OwnedStructure
        {
            get { return this._ownedStructure; }
            set { this.SetProperty(ref this._ownedStructure, value); }
        }

        /// <summary>
        /// Gets the pointer member that this tunable is instancing.
        /// </summary>
        public PointerMember PointerMember
        {
            get
            {
                PointerMember member = this.Member as PointerMember;
                if (member != null)
                {
                    return member;
                }

                return new PointerMember(this.TunableStructure.Definition);
            }
        }

        /// <summary>
        /// Gets or sets the named reference for this pointer.
        /// </summary>
        public string Reference
        {
            get { return this._reference ?? "NULL"; }
            set { this.SetProperty(ref this._reference, value); }
        }

        /// <summary>
        /// Gets a collection of the tunables that are defined for this pointer.
        /// </summary>
        public ReadOnlyModelCollection<ITunable> Tunables
        {
            get
            {
                if (this._readOnlyTunables == null)
                {
                    this._readOnlyTunables = this._tunables.ToReadOnly();
                }

                return this._readOnlyTunables;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Changes the object type that this tunable points to. This changes the tunable
        /// children when appropriate. This is only supported on owner and simple owner
        /// pointers.
        /// </summary>
        /// <param name="structure">
        /// The new owned structure for the pointer.
        /// </param>
        public void ChangePointerType(IStructure structure)
        {
            if (!this.PointerMember.OwnsObject)
            {
                return;
            }

            this.OwnedStructure = structure;
            if (structure == null)
            {
                this._tunables.Clear();
                return;
            }

            if (!this.PointerMember.ReferencedStructure.IsRelatedTo(structure))
            {
                throw new NotSupportedException(
                    "Changed pointer type needs to be relative to defined type.");
            }

            MemberInfo members = this.OwnedStructure.GetMemberInformation();
            ITunable[] tunables = new ITunable[members.Count];
            TunableFactory factory = new TunableFactory(this);
            foreach (MemberOrderInfo memberInfo in members.Values)
            {
                if (!memberInfo.Member.CanBeInstanced)
                {
                    continue;
                }

                ITunable newTunable = null;
                foreach (ITunable tunable in this.Tunables)
                {
                    if (Object.ReferenceEquals(tunable.Member, memberInfo.Member))
                    {
                        newTunable = tunable;
                        break;
                    }
                }

                if (newTunable == null)
                {
                    newTunable = factory.Create(memberInfo.Member);
                    if (newTunable == null)
                    {
                        throw new MetadataException(
                            "Unable to create a tunable for the member {0}.",
                            memberInfo.Member.Name);
                    }
                }

                tunables[memberInfo.Position] = newTunable;
            }

            this._tunables.Clear();
            this._tunables.AddRange(tunables);
        }

        /// <summary>
        /// Creates a new <see cref="PointerTunable"/> that is a copy of this instance.
        /// </summary>
        /// <returns>
        /// A new <see cref="PointerTunable"/> that is a copy of this instance.
        /// </returns>
        public new PointerTunable Clone()
        {
            return new PointerTunable(this, this.TunableStructure);
        }

        /// <summary>
        /// Creates a deep copy of the current instance.
        /// </summary>
        /// <returns>
        /// A deep copy of the current System.Object.
        /// </returns>
        public override object DeepClone()
        {
            return this.Clone();
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="ITunable"/> by
        /// looking at their values.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance by their values;
        /// otherwise, false.
        /// </returns>
        public override bool EqualByValue(ITunable other)
        {
            return this.EqualByValue(other as PointerTunable);
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="ITunable"/> by
        /// looking at their values.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance by their values;
        /// otherwise, false.
        /// </returns>
        public bool EqualByValue(PointerTunable other)
        {
            if (other == null)
            {
                return false;
            }

            if (this.PointerMember.OwnsObject && other.PointerMember.OwnsObject)
            {
                if (!Object.ReferenceEquals(this.OwnedStructure, other.OwnedStructure))
                {
                    return false;
                }

                if (this.Tunables.Count != other.Tunables.Count)
                {
                    return false;
                }

                for (int i = 0; i < this.Tunables.Count; i++)
                {
                    if (!this.Tunables[i].EqualByValue(other.Tunables[i]))
                    {
                        return false;
                    }
                }

                return true;
            }
            else if (!this.PointerMember.OwnsObject && !other.PointerMember.OwnsObject)
            {
                return this.Reference == other.Reference;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified
        /// <see cref="PointerTunable"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public bool Equals(PointerTunable other)
        {
            if (other == null)
            {
                return false;
            }

            return Object.ReferenceEquals(other, this);
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="ITunable"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public override bool Equals(ITunable other)
        {
            return this.Equals(other as PointerTunable);
        }

        /// <summary>
        /// Resets this tunable to have its default value. (i.e no source data with its initial
        /// values).
        /// </summary>
        public override void ResetValueToDefaultCore()
        {
            if (this.PointerMember.OwnsObject)
            {
                this.ChangePointerType(null);
            }
            else
            {
                this.Reference = null;
            }
        }

        /// <summary>
        /// Writes the xml representation of this instance onto the specified
        /// System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that the xml representation of this instance should be
        /// written to.
        /// </param>
        /// <param name="serialiseDefaultTunables">
        /// A value indicating whether the tunables that have a default value should be written
        /// to the specified writer or not.
        /// </param>
        public override void Serialise(XmlWriter writer, bool serialiseDefaultTunables)
        {
            if (writer == null)
            {
                throw new SmartArgumentNullException(() => writer);
            }

            if (this.PointerMember.OwnsObject)
            {
                if (this.PointerMember.CanBeDerivedType)
                {
                    if (this.OwnedStructure != null)
                    {
                        writer.WriteAttributeString("type", this.OwnedStructure.MetadataName);
                    }
                    else
                    {
                        writer.WriteAttributeString("type", "NULL");
                    }
                }

                foreach (ITunable tunable in this._tunables)
                {
                    if (!serialiseDefaultTunables && tunable.HasDefaultValue)
                    {
                        continue;
                    }

                    writer.WriteStartElement(tunable.Member.MetadataName);
                    tunable.Serialise(writer, serialiseDefaultTunables);
                    writer.WriteEndElement();
                }
            }
            else
            {
                writer.WriteAttributeString("ref", this.Reference ?? String.Empty);
            }
        }

        /// <summary>
        /// Updates this tunables HasDefaultValue property.
        /// </summary>
        public void UpdateHasDefaultValue()
        {
        }

        /// <summary>
        /// Creates the tunables that represent the associated definition members.
        /// </summary>
        private void CreateTunables()
        {
            if (this.OwnedStructure == null || !this.PointerMember.OwnsObject)
            {
                return;
            }

            MemberInfo members = this.OwnedStructure.GetMemberInformation();
            ITunable[] tunables = new ITunable[members.Count];
            TunableFactory factory = new TunableFactory(this);
            foreach (MemberOrderInfo memberInfo in members.Values)
            {
                if (!memberInfo.Member.CanBeInstanced)
                {
                    continue;
                }

                ITunable tunable = factory.Create(memberInfo.Member);
                if (tunable == null)
                {
                    throw new MetadataException(
                        "Unable to create a tunable for the member {0}.",
                        memberInfo.Member.Name);
                }

                tunables[memberInfo.Position] = tunable;
            }

            this._tunables.AddRange(tunables);
        }

        /// <summary>
        /// Creates the tunables that represent the specified definition members using the
        /// specified System.Xml.XmlReader as a initialising data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// the tunables from.
        /// </param>
        /// <param name="log">
        /// A object that is used to log any warnings or errors produced during the parsing of
        /// the specified reader.
        /// </param>
        /// <returns>
        /// A array containing the tunables that have been created.
        /// </returns>
        private ITunable[] CreateTunables(XmlReader reader, ILog log)
        {
            MemberInfo members = this.OwnedStructure.GetMemberInformation();
            ITunable[] tunables = new ITunable[members.Count];
            TunableFactory factory = new TunableFactory(this);

            IXmlLineInfo lineInfo = reader as IXmlLineInfo;
            string line = lineInfo.LineNumber.ToStringInvariant();
            string pos = lineInfo.LinePosition.ToStringInvariant();

            if (!reader.IsEmptyElement)
            {
                reader.ReadStartElement();
                while (reader.MoveToElementOrComment())
                {
                    if (reader.NodeType == XmlNodeType.Comment)
                    {
                        reader.Skip();
                        continue;
                    }

                    MemberOrderInfo orderInfo;
                    if (!members.TryGetValue(reader.Name, out orderInfo))
                    {
                        reader.Skip();
                        continue;
                    }

                    ITunable tunable = factory.Create(reader, orderInfo.Member, log);
                    if (tunable == null)
                    {
                        reader.Skip();
                        continue;
                    }

                    tunables[orderInfo.Position] = tunable;
                }
            }

            foreach (MemberOrderInfo memberInfo in members.Values)
            {
                int index = memberInfo.Position;
                if (tunables[index] == null)
                {
                    ITunable tunable = factory.Create(memberInfo.Member);
                    if (tunable == null)
                    {
                        if (memberInfo.Member is UnknownMember)
                        {
                            throw new MetadataException(
                                StringTable.PointerMemberUnknownError,
                                memberInfo.Member.Name,
                                this.OwnedStructure.DataType,
                                ((UnknownMember)memberInfo.Member).XmlNodeName,
                                line,
                                pos);
                        }
                        else
                        {
                            throw new MetadataException(
                                StringTable.PointerMemberFailedCreationError,
                                memberInfo.Member.Name,
                                this.OwnedStructure.DataType,
                                memberInfo.Member,
                                line,
                                pos);
                        }
                    }

                    tunables[index] = tunable;
                }
            }

            return tunables;
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        /// <param name="log">
        /// A object that is used to log any warnings or errors produced during the parsing of
        /// the specified reader.
        /// </param>
        private void Deserialise(XmlReader reader, ILog log)
        {
            if (reader == null)
            {
                return;
            }

            IXmlLineInfo lineInfo = reader as IXmlLineInfo;
            string line = lineInfo.LineNumber.ToStringInvariant();
            string pos = lineInfo.LinePosition.ToStringInvariant();

            try
            {
                this.DeserialiseInternal(reader, line, pos, log);
            }
            catch (MetadataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                string msg = StringTable.PointerTunableDeserialiseError;
                msg = msg.FormatInvariant(line, pos, ex.Message);
                throw new MetadataException(msg);
            }

            reader.Skip();
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        /// <param name="line">
        /// The reader line number to add to any thrown exceptions.
        /// </param>
        /// <param name="pos">
        /// The reader position number to add to any thrown exceptions.
        /// </param>
        /// <param name="log">
        /// A object that is used to log any warnings or errors produced during the parsing of
        /// the specified reader.
        /// </param>
        private void DeserialiseInternal(XmlReader reader, string line, string pos, ILog log)
        {
            PointerMember member = this.PointerMember;
            if (member.OwnsObject)
            {
                if (member.CanBeDerivedType)
                {
                    string type = reader.GetAttribute("type");
                    if (type == null)
                    {
                        string msg = StringTable.PointerTypeMissingError;
                        throw new MetadataException(msg, line, pos);
                    }

                    if (type == "NULL")
                    {
                        if (reader.IsEmptyElement)
                        {
                            reader.Skip();
                            return;
                        }
                        else
                        {
                            string msg = StringTable.PointerRedundantXmlError;
                            throw new MetadataException(msg, line, pos);
                        }
                    }

                    string name = type.Replace("__", "::");
                    IStructure referenced = member.Structure.Dictionary.GetStructure(name);
                    if (referenced == null)
                    {
                        string msg = StringTable.PointerTypeNotFoundError;
                        throw new MetadataException(msg, type, line, pos);
                    }

                    this._ownedStructure = referenced;
                    this._tunables.AddRange(this.CreateTunables(reader, log));
                }
                else
                {
                    IStructure referenced = member.ReferencedStructure;
                    if (referenced == null)
                    {
                        string msg = StringTable.PointerTypeNotFoundError;
                        throw new MetadataException(msg, member.Type, line, pos);
                    }

                    this._ownedStructure = referenced;
                    this._tunables.AddRange(this.CreateTunables(reader, log));
                }
            }
            else
            {
                this._reference = reader.GetAttribute("ref");
                if (this._reference == null)
                {
                    string msg = StringTable.PointerRefMissingError;
                    log.Warning(msg, line, pos);
                    this._reference = "NULL";
                }

                reader.Skip();
            }
        }
        #endregion Methods
    } // RSG.Metadata.Model.Tunables.PointerTunable {Class}
} // RSG.Metadata.Model.Tunables {Namespace}
