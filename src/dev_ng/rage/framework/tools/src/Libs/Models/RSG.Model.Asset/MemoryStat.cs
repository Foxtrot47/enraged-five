﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;

namespace RSG.Model.Asset
{
    /// <summary>
    /// 
    /// </summary>
    public class MemoryStat : IMemoryStat
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public uint PhysicalSize { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public uint VirtualSize { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public MemoryStat()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="physicalSize"></param>
        /// <param name="virtualSize"></param>
        public MemoryStat(uint physicalSize, uint virtualSize)
        {
            PhysicalSize = physicalSize;
            VirtualSize = virtualSize;
        }
        #endregion // Constructor(s)
    } // MemoryStat
}
