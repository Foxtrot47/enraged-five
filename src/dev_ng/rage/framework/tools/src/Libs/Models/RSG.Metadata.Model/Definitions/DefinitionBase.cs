﻿//---------------------------------------------------------------------------------------------
// <copyright file="DefinitionBase.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Xml;
    using RSG.Editor;
    using RSG.Editor.Model;

    /// <summary>
    /// Provides a base class to all of the definitions used in the parCodeGen system.
    /// </summary>
    [DebuggerDisplay("{_dataType}")]
    public abstract class DefinitionBase : ModelBase, IDefinition
    {
        #region Fields
        /// <summary>
        /// The name of the xml attribute used for the <see cref="_filename"/> field.
        /// </summary>
        protected const string XmlFileAttr = "file";

        /// <summary>
        /// The name of the xml attribute used for the <see cref="_lineNumber"/> field.
        /// </summary>
        protected const string XmlLineNumberAttr = "line";

        /// <summary>
        /// The name of the xml attribute used for the <see cref="_linePosition"/> field.
        /// </summary>
        protected const string XmlLinePositionAttr = "pos";

        /// <summary>
        /// The name of the xml attribute used for the <see cref="DataType"/> property.
        /// </summary>
        protected const string XmlTypeAttr = "type";

        /// <summary>
        /// The private field used for the <see cref="DataType"/> property.
        /// </summary>
        private string _dataType;

        /// <summary>
        /// The private field used for the <see cref="Dictionary"/> property.
        /// </summary>
        private IDefinitionDictionary _dictionary;

        /// <summary>
        /// The private field used for the <see cref="Filename"/> property.
        /// </summary>
        private string _filename;

        /// <summary>
        /// The private field used for the <see cref="LineNumber"/> property.
        /// </summary>
        private int _lineNumber;

        /// <summary>
        /// The private field used for the <see cref="LinePosition"/> property.
        /// </summary>
        private int _linePosition;

        /// <summary>
        /// The private field used for the <see cref="DataType"/> property.
        /// </summary>
        private string _trimmedDataType;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DefinitionBase"/> class.
        /// </summary>
        /// <param name="dictionary">
        /// The dictionary that this definition will be associated with.
        /// </param>
        protected DefinitionBase(IDefinitionDictionary dictionary)
        {
            if (dictionary == null)
            {
                throw new SmartArgumentNullException(() => dictionary);
            }

            this._dictionary = dictionary;
            this._lineNumber = -1;
            this._linePosition = -1;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="DefinitionBase"/> class.
        /// </summary>
        /// <param name="dataType">
        /// The full data type this definition will represent.
        /// </param>
        /// <param name="dictionary">
        /// The dictionary that this definition will be associated with.
        /// </param>
        protected DefinitionBase(string dataType, IDefinitionDictionary dictionary)
        {
            if (dictionary == null)
            {
                throw new SmartArgumentNullException(() => dictionary);
            }

            this._dictionary = dictionary;
            this._dataType = dataType;
            this._trimmedDataType = dataType != null ? dataType.TrimStart(':') : null;
            this._lineNumber = -1;
            this._linePosition = -1;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="DefinitionBase"/> class as a copy of
        /// the specified instance.
        /// </summary>
        /// <param name="other">
        /// The instance to copy.
        /// </param>
        /// <param name="dictionary">
        /// The dictionary that this definition will be associated with.
        /// </param>
        protected DefinitionBase(DefinitionBase other, IDefinitionDictionary dictionary)
        {
            if (dictionary == null)
            {
                throw new SmartArgumentNullException(() => dictionary);
            }

            if (other == null)
            {
                throw new SmartArgumentNullException(() => other);
            }

            this._dictionary = dictionary;
            this._dataType = other._dataType;
            this._trimmedDataType = other._trimmedDataType;
            this._filename = other._filename;
            this._lineNumber = other._lineNumber;
            this._linePosition = other._linePosition;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="DefinitionBase"/>
        /// class using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        /// <param name="dictionary">
        /// The dictionary that this definition will be associated with.
        /// </param>
        protected DefinitionBase(XmlReader reader, IDefinitionDictionary dictionary)
        {
            if (dictionary == null)
            {
                throw new SmartArgumentNullException(() => dictionary);
            }

            if (reader == null)
            {
                throw new SmartArgumentNullException(() => reader);
            }

            this._dictionary = dictionary;
            IXmlLineInfo info = reader as IXmlLineInfo;
            if (info != null)
            {
                this._lineNumber = info.LineNumber;
                this._linePosition = info.LinePosition;
            }
            else
            {
                this._lineNumber = -1;
                this._linePosition = -1;
            }

            Uri uri = new Uri(reader.BaseURI);
            this._filename = Path.GetFullPath(uri.LocalPath);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the full data type for this definition.
        /// </summary>
        public virtual string DataType
        {
            get
            {
                return this._trimmedDataType;
            }

            set
            {
                string[] changedProperties = new string[]
                        {
                            "DataType",
                            "ShortDataType",
                            "Scope",
                            "Namespaces"
                        };

                this._trimmedDataType = value != null ? value.TrimStart(':') : null;
                this.SetProperty(ref this._dataType, value, changedProperties);
            }
        }

        /// <summary>
        /// Gets the dictionary this definition is defined inside.
        /// </summary>
        public IDefinitionDictionary Dictionary
        {
            get { return this._dictionary; }
        }

        /// <summary>
        /// Gets or sets the filename this instance is defined in or null if not applicable.
        /// </summary>
        public string Filename
        {
            get
            {
                return this._filename;
            }

            set
            {
                string[] changedProperties = new string[]
                        {
                            "Filename",
                            "Location"
                        };

                this.SetProperty(ref this._filename, value, changedProperties);
            }
        }

        /// <summary>
        /// Gets or sets the line number this instance is defined on or -1 if not available.
        /// </summary>
        public int LineNumber
        {
            get
            {
                return this._lineNumber;
            }

            set
            {
                string[] changedProperties = new string[]
                        {
                            "LineNumber",
                            "Location"
                        };

                this.SetProperty(ref this._lineNumber, value, changedProperties);
            }
        }

        /// <summary>
        /// Gets or sets the line position this instance is defined at or -1 if not available.
        /// </summary>
        public int LinePosition
        {
            get
            {
                return this._linePosition;
            }

            set
            {
                string[] changedProperties = new string[]
                        {
                            "LinePosition",
                            "Location"
                        };

                this.SetProperty(ref this._linePosition, value, changedProperties);
            }
        }

        /// <summary>
        /// Gets the location of this definition inside the parCodeGen definition data sat on
        /// the local disk.
        /// </summary>
        public FileLocation Location
        {
            get { return new FileLocation(this.LineNumber, this.LinePosition, this.Filename); }
        }

        /// <summary>
        /// Gets the namespace this definition is currently inside by looking at the data type.
        /// </summary>
        public string Namespace
        {
            get { return this.GetNamespaceFromDataType(this.RawDataType); }
        }

        /// <summary>
        /// Gets an iterator over the namespace hierarchy starting at the root.
        /// </summary>
        public IEnumerable<string> Namespaces
        {
            get { return this.GetNamespaceHierarchy(this._dataType); }
        }

        /// <summary>
        /// Gets the scope for this definition.
        /// </summary>
        public string Scope
        {
            get { return this.GetScopeFromDataType(this._dataType); }
        }

        /// <summary>
        /// Gets the short data type for this definition.
        /// </summary>
        public string ShortDataType
        {
            get { return this.GetShortNameFromDataType(this._dataType); }
        }

        /// <summary>
        /// Gets the raw data type without any modifications.
        /// </summary>
        protected string RawDataType
        {
            get { return this._dataType; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Indicates whether this instance is equal to the specified
        /// <see cref="IDefinition"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public abstract bool Equals(IDefinition other);

        /// <summary>
        /// Serves as a hash function for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for the current instance.
        /// </returns>
        public override int GetHashCode()
        {
            if (this.DataType == null)
            {
                return base.GetHashCode();
            }

            return this.DataType.GetHashCode();
        }

        /// <summary>
        /// Determine whether the specified string is referencing this definition.
        /// </summary>
        /// <param name="type">
        /// The string that could be referencing this definition.
        /// </param>
        /// <returns>
        /// True if the specified string is referencing this definition; otherwise, false.
        /// </returns>
        public virtual bool IsReferencedBy(string type)
        {
            if (String.IsNullOrWhiteSpace(this.DataType))
            {
                return false;
            }

            if (String.Equals(this.DataType, type, StringComparison.Ordinal))
            {
                return true;
            }

            type = type.TrimStart(':');
            if (String.Equals(this.DataType, type, StringComparison.Ordinal))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A string that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return this.DataType ?? base.ToString();
        }

        /// <summary>
        /// Sets the raw data type value to the specified value making sure the correct
        /// property changed events are fired.
        /// </summary>
        /// <param name="value">
        /// The value to set the raw data type property to.
        /// </param>
        /// <param name="names">
        /// A array of property names that are changed due to this one change.
        /// </param>
        protected void SetRawDataType(string value, params string[] names)
        {
            this._trimmedDataType = value != null ? value.TrimStart(':') : null;
            this.SetProperty(ref this._dataType, value, names);
        }

        /// <summary>
        /// Gets the namespace for this definition from the specified data type.
        /// </summary>
        /// <param name="dataType">
        /// The data type to use to get the namespace from.
        /// </param>
        /// <returns>
        /// The namespace for this definition based on the specified data type.
        /// </returns>
        private string GetNamespaceFromDataType(string dataType)
        {
            if (String.IsNullOrWhiteSpace(dataType))
            {
                return String.Empty;
            }

            string fullDataType = dataType.TrimStart(':');
            fullDataType = fullDataType.TrimEnd(':');

            int index = fullDataType.LastIndexOf(':');
            if (index == -1)
            {
                return String.Empty;
            }

            string structureNamespace = fullDataType.Substring(0, index);
            return structureNamespace.TrimEnd(':');
        }

        /// <summary>
        /// Gets the namespace hierarchy for this definition based on the specified data type.
        /// </summary>
        /// <param name="dataType">
        /// The data type to use to get the namespace hierarchy from.
        /// </param>
        /// <returns>
        /// The namespace hierarchy for this definition based on the specified data type.
        /// </returns>
        private IEnumerable<string> GetNamespaceHierarchy(string dataType)
        {
            if (String.IsNullOrWhiteSpace(dataType))
            {
                yield break;
            }

            char[] separator = new char[] { ':' };
            string[] parts = dataType.Split(separator, StringSplitOptions.RemoveEmptyEntries);
            yield return "<global namespace>";
            for (int i = 0; i < parts.Length - 1; i++)
            {
                yield return parts[i];
            }
        }

        /// <summary>
        /// Gets the scope for this definition based on the specified data type.
        /// </summary>
        /// <param name="dataType">
        /// The data type to use to get the scope from.
        /// </param>
        /// <returns>
        /// The scope for this definition based on the specified data type.
        /// </returns>
        private string GetScopeFromDataType(string dataType)
        {
            if (String.IsNullOrWhiteSpace(dataType))
            {
                return String.Empty;
            }

            dataType = dataType.TrimEnd(':');
            int index = dataType.LastIndexOf(':');
            if (index != -1)
            {
                return dataType.Substring(0, index).Trim(':');
            }

            return String.Empty;
        }

        /// <summary>
        /// Gets the short data type name for this definition from the specified data type.
        /// </summary>
        /// <param name="dataType">
        /// The data type to use to get the short name from.
        /// </param>
        /// <returns>
        /// The short data type name for this definition based on the specified data type.
        /// </returns>
        private string GetShortNameFromDataType(string dataType)
        {
            if (String.IsNullOrWhiteSpace(dataType))
            {
                return String.Empty;
            }

            dataType = dataType.TrimEnd(':');
            int index = dataType.LastIndexOf(':');
            if (index != -1)
            {
                return dataType.Substring(index + 1);
            }

            return dataType;
        }
        #endregion Methods
    } // RSG.Metadata.Model.Definitions.DefinitionBase {Class}
} // RSG.Metadata.Model.Definitions {Namespace}
