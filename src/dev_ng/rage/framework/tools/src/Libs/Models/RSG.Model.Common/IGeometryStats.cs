﻿using System;

namespace RSG.Model.Common
{
    public interface IGeometryStats
    {
        string ArchetypeName { get; }
        float Heuristic { get; }
    }
}
