﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.Model.Report.Reports.GameStatsReports.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class StreamingMemoryResult
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public string ModuleName
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public IDictionary<string, StreamingMemoryStat> Categories
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public StreamingMemoryResult(XElement element)
        {
            // Extract the various stats
            XElement nameElement = element.Element("moduleName");
            if (nameElement != null)
            {
                ModuleName = nameElement.Value;
            }

            // Extract all the categorised stats
            Categories = new Dictionary<string, StreamingMemoryStat>();

            if (element.Element("categories") != null)
            {
                foreach (XElement item in element.Element("categories").Elements("Item"))
                {
                    nameElement = item.Element("categoryName");
                    if (nameElement != null)
                    {
                        string categoryName = nameElement.Value;
                        uint virtualMemory = 0;
                        uint physicalMemory = 0;

                        XElement virtualElement = item.Element("virtualMemory");
                        if (virtualElement != null && virtualElement.Attribute("value") != null)
                        {
                            virtualMemory = UInt32.Parse(virtualElement.Attribute("value").Value);
                        }

                        XElement physicalElement = item.Element("physicalMemory");
                        if (physicalElement != null && physicalElement.Attribute("value") != null)
                        {
                            physicalMemory = UInt32.Parse(physicalElement.Attribute("value").Value);
                        }

                        Categories.Add(categoryName, new StreamingMemoryStat(categoryName, virtualMemory, physicalMemory));
                    }
                }
            }
        }
        #endregion // Constructor(s)
    } // StreamingMemoryResult
}
