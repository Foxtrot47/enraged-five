﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace RSG.Model.Dialogue.Export
{
    public class UnusedConversationFinder
    {
        TextWriter m_writer;
        Stream m_stream;

        private string m_dstarFolder;
        private string m_rootKeyFile;
        private string m_scriptLiteralFile;

        public UnusedConversationFinder(string dstarFolder, string rootKeyFile, string scriptLiteralFile)
        {
            m_dstarFolder = dstarFolder;
            m_rootKeyFile = rootKeyFile;
            m_scriptLiteralFile = scriptLiteralFile;
        }

        /// <summary>
        /// Writes out all the conversation keys found in the dstarFolder to a file (rootKeyFile).
        /// </summary>
        public void WriteConversationKeys()
        {
            KeyExtractor extractor = new KeyExtractor(m_rootKeyFile, m_dstarFolder);
            extractor.KeyFound += new KeyExtractorEventHandler(Extractor_KeyFound);
            using (m_stream = File.Open(m_rootKeyFile, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Read))
            {
                using (m_writer = new StreamWriter(m_stream))
                {
                    extractor.Run();
                }
            }
            
            extractor.KeyFound -= Extractor_KeyFound;
        }

        #region Event handlers

        /// <summary>
        /// Write out the value to the file.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private void Extractor_KeyFound(object sender, KeyExtractorEventArgs e)
        {
            m_writer.WriteLine(e.ConversationKey);
        }

        #endregion
    }
}
