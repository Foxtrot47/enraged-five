﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Model.Animation
{
    /// <summary>
    /// Database cutscene collection.
    /// </summary>
    [DataContract]
    [KnownType(typeof(DbCutscene))]
    [ReportType]
    [Serializable]
    public class DbCutsceneCollection : CutsceneCollectionBase
    {
        #region Properties
        /// <summary>
        /// Build that this collection is for.
        /// </summary>
        [DataMember]
        public String BuildIdentifier { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// For serialisation purposes.
        /// </summary>
        private DbCutsceneCollection()
            : base()
        {
        }

        /// <summary>
        /// Main constructor.
        /// </summary>
        public DbCutsceneCollection(String buildIdentifier)
            : base()
        {
            BuildIdentifier = buildIdentifier;
        }
        #endregion // Constructor(s)
    } // DbCutsceneCollection
}
