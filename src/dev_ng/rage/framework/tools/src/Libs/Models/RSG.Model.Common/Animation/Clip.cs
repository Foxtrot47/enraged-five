﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using System.ComponentModel;
using RSG.Base.ConfigParser;
using System.IO;
using System.Xml.Linq;
using RSG.Base.Logging;
using System.Xml.XPath;
using System.Runtime.Serialization;

namespace RSG.Model.Common.Animation
{
    /// <summary>
    /// Clip asset
    /// </summary>
    [DataContract]
    [KnownType(typeof(Animation))]
    public class Clip : AssetBase, IClip
    {
        #region Constants
        #endregion // Constants

        #region Properties

        /// <summary>
        /// The animations this clip references
        /// </summary>
        [DataMember]
        public IEnumerable<IAnimation> Animations
        {
            get
            {
                return m_animations;
            }
            private set
            {
                SetPropertyValue(value, () => Animations,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_animations = (IEnumerable<IAnimation>)newValue;
                        }
                ));
            }
        }
        private IEnumerable<IAnimation> m_animations;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public Clip(String name)
            : base(name)
        {
            // DW: for now (and for RSEXPORT zip extraction speed concerns) we simply assume there is a 1 to 1 animation relationship with the Clip.         
            Animations = new List<IAnimation>() { new Animation(name) };
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        public void LoadFromAssetData()
        {
            // Load the Clip file, search for animation references.
        }
        #endregion // Public Methods

        #region IComparable<Clip> Interface
        /// <summary>
        /// Compare this clip to another.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(IClip other)
        {
            if (other == null)
            {
                return 1;
            }

            return (Name.CompareTo(other.Name));
        }
        #endregion // IComparable<IClip> Interface

        #region IEquatable<IClip> Interface
        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>true if the current object is equal to the other parameter; otherwise, false.</returns>
        public bool Equals(IClip other)
        {
            if (other == null)
            {
                return false;
            }

            return (Name == other.Name);
        }
        #endregion // IEquatable<IClip> Interface

    } // class Clip
} // namespace RSG.Model.Animation
