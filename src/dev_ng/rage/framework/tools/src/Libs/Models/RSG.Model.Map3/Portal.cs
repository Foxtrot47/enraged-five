﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common.Map;
using RSG.Model.Common;
using System.ComponentModel;

namespace RSG.Model.Map3
{
    /// <summary>
    /// 
    /// </summary>
    public class Portal : AssetBase, IPortal
    {
        #region Propterties
        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public IInteriorArchetype ParentArchetype
        {
            get;
            protected set;
        }

        /// <summary>
        /// 
        /// </summary>
        public IRoom RoomA
        {
            get;
            protected set;
        }

        /// <summary>
        /// 
        /// </summary>
        public IRoom RoomB
        {
            get;
            protected set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        public Portal(string name, IInteriorArchetype parent, IRoom a, IRoom b)
            : base(name)
        {
            ParentArchetype = parent;
            RoomA = a;
            RoomB = b;
        }
        #endregion // Constructor(s)
    } // Portal
}
