﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using System.Collections;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using RSG.Base.Collections;

namespace RSG.Model.GlobalTXD
{
    public class GlobalTexture : ModelBase, ITextureChild
    {
        #region Constants

        private const String FilenameAttribute = "filename";
        private const String AlphaAttribute = "alpha";

        #region XPath Compiled Expressions

        private static readonly XPathExpression sC_s_xpath_XPathAttributes = XPathExpression.Compile("@*");

        #endregion // XPath Compiled Expressions

        #endregion // Constants

        #region Properties

        /// <summary>
        /// The stream name for this texture
        /// </summary>
        public String StreamName
        {
            get { return m_sStreamName; }
            set { m_sStreamName = value; }
        }
        private String m_sStreamName;

        /// <summary>
        /// 
        /// </summary>
        public String Filename
        {
            get;
            set;
        }

        /// <summary>
        /// The parent global dictionary if it has one
        /// </summary>
        public ITextureContainer Parent
        {
            get { return m_parent; }
            set
            {
                this.SetPropertyValue(ref this.m_parent, value, "Parent");
            }
        }
        private ITextureContainer m_parent;

        /// <summary>
        /// The global root object that this dictionary belongs to
        /// </summary>
        public GlobalRoot Root
        {
            get { return m_root; }
            set
            {
                this.SetPropertyValue(ref this.m_root, value, "Root");
            }
        }
        private GlobalRoot m_root;

        public Boolean HasAlphaTexture
        {
            get { return m_sAlphaFilepath != null && m_sAlphaFilepath != String.Empty; }
        }
        public String AlphaFilepath
        {
            get { return m_sAlphaFilepath; }
            set { m_sAlphaFilepath = value; }
        }
        public String AlphaTextureName
        {
            get { return HasAlphaTexture ? System.IO.Path.GetFileNameWithoutExtension(m_sAlphaFilepath) : String.Empty; }
        }
        private String m_sAlphaFilepath;

        public int StreamSize
        {
            get
            {
                if (m_streamSize == -1)
                {
                    DetermineStreamProperties();
                }
                return m_streamSize;
            }
            private set
            {
                m_streamSize = value;
            }
        }
        private int m_streamSize = -1;

        public String StreamFilename
        {
            get
            {
                if (m_streamFilename == null)
                {
                    this.DetermineStreamProperties();
                }
                return m_streamFilename;
            }
            private set { m_streamFilename = value; }
        }
        private String m_streamFilename = null;

        /// <summary>
        /// Represents the number of times the texture appears in the sibling
        /// texture containers
        /// </summary>
        public int InstanceCount
        {
            get
            {
                return m_instanceCount;
            }
            set
            {
                this.SetPropertyValue(ref this.m_instanceCount, value, "InstanceCount");
                if (this.Parent is GlobalTextureDictionary)
                {
                    GlobalTextureDictionary parent = this.Parent as GlobalTextureDictionary;
                    if (parent.Parent is GlobalTextureDictionary)
                    {
                        InstanceCountAsPercentage = (float)value / (float)((parent.Parent as GlobalTextureDictionary).GlobalTextureDictionaries.Count);
                    }
                }
            }
        }
        private int m_instanceCount = 1;

        /// <summary>
        /// Represents the number of times the texture appears in the sibling
        /// texture containers
        /// </summary>
        public float InstanceCountAsPercentage
        {
            get
            {
                return m_instanceCountAsPercentage;
            }
            set
            {
                this.SetPropertyValue(
                    ref this.m_instanceCountAsPercentage, value, "InstanceCountAsPercentage");
            }
        }
        private float m_instanceCountAsPercentage;
 
        /// <summary>
        /// Represents the number of times the texture appears in the sibling
        /// texture containers
        /// </summary>
        public int InstanceCountIfPromoted
        {
            get
            {
                return m_instanceCountIfPromoted;
            }
            set
            {
                this.SetPropertyValue(
                    ref this.m_instanceCountIfPromoted, value, "InstanceCountIfPromoted");

                if (this.Parent is GlobalTextureDictionary)
                {
                    GlobalTextureDictionary parent = this.Parent as GlobalTextureDictionary;
                    if (parent.Parent is GlobalTextureDictionary && (parent.Parent as GlobalTextureDictionary).Parent is GlobalTextureDictionary)
                    {
                        InstanceCountIfPromotedAsPercentage = (float)value / (float)((parent.Parent as GlobalTextureDictionary).Parent.GlobalTextureDictionaries.Count);
                    }
                }
            }
        }
        private int m_instanceCountIfPromoted = 1;

        /// <summary>
        /// Represents the number of times the texture appears in the sibling
        /// texture containers
        /// </summary>
        public float InstanceCountIfPromotedAsPercentage
        {
            get
            {
                return m_instanceCountIfPromotedAsPercentage;
            }
            set
            {
                this.SetPropertyValue(
                    ref this.m_instanceCountIfPromotedAsPercentage,
                    value,
                    "InstanceCountIfPromotedAsPercentage");
            }
        }
        private float m_instanceCountIfPromotedAsPercentage;
        #endregion // Properties

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        public GlobalTexture(SourceTexture texture, GlobalTextureDictionary parent)
        {
            this.StreamName = texture.StreamName;
            this.Filename = texture.Filename;
            this.AlphaFilepath = texture.AlphaFilepath;
            this.Root = parent.Root;
            this.Parent = parent;

            this.StreamFilename = texture.StreamFilename;
            this.StreamSize = texture.StreamSize;
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public GlobalTexture(GlobalTexture texture, GlobalTextureDictionary parent)
        {
            this.StreamName = texture.StreamName;
            this.Filename = texture.Filename;
            this.AlphaFilepath = texture.AlphaFilepath;
            this.Root = parent.Root;
            this.Parent = parent;

            this.StreamFilename = texture.StreamFilename;
            this.StreamSize = texture.StreamSize;
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public GlobalTexture(XmlReader reader, GlobalTextureDictionary parent)
        {
            this.Root = parent.Root;
            this.Parent = parent;

            this.Deserialise(reader);
        }

        #endregion // Constructors

        #region Private Function(s)

        private void DetermineStreamProperties()
        {
            this.StreamSize = 0;
            this.StreamFilename = string.Empty;

            bool fileFound = false;
            string location = System.IO.Path.Combine(this.Root.TextureLocation, this.StreamName);
            this.StreamFilename = location + ".tif";
            if (System.IO.File.Exists(this.StreamFilename))
            {
                fileFound = true;
            }

            if (fileFound)
            {
                System.IO.FileInfo fileInfo = new System.IO.FileInfo(this.StreamFilename);
                this.StreamSize = (int)fileInfo.Length;
            }
        }

        #endregion

        #region Serialisation / Deserialisation Methods

        public void Serialise(XmlDocument xmlDoc, XmlElement parentNode)
        {
            XmlElement textureNode = xmlDoc.CreateElement("globalTexture");

            textureNode.SetAttribute(FilenameAttribute, this.Filename);
            textureNode.SetAttribute(AlphaAttribute, this.AlphaFilepath);

            parentNode.AppendChild(textureNode);
        }

        /// <summary>
        /// 
        /// </summary>
        public void Deserialise(XmlReader reader)
        {
            this.Filename = reader.GetAttribute(FilenameAttribute);
            this.AlphaFilepath = reader.GetAttribute(AlphaAttribute);
            this.StreamName = System.IO.Path.GetFileNameWithoutExtension(this.Filename) + System.IO.Path.GetFileNameWithoutExtension(this.AlphaFilepath);

            reader.Skip();
        }

        #endregion // Serialisation / Deserialisation Methods
    } // GlobalTexture
} // RSG.Model.GlobalTXD
