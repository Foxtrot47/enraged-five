﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Collections;
using RSG.Model.Asset;

namespace RSG.Model.GlobalTXD
{
    /// <summary>
    /// This interface represents a object that can contain a set of
    /// global texture dictionaries, texture dictionaries, and global textures
    /// </summary>
    public interface IDictionaryContainer
    {
        /// <summary>
        /// If this is true it means that source texture dictionaries can be added to
        /// this container as children
        /// </summary>
        Boolean CanAcceptSourceDictionaries { get; }

        /// <summary>
        /// If this is true it means that global texture dictionaries can be added to
        /// this container as children
        /// </summary>
        Boolean CanAcceptGlobalDictionaries { get; }

        /// <summary>
        /// The name given to the dictionary container, has to be unique for a global texture
        /// dictionary as these names are used by runtime
        /// </summary>
        String Name { get; }

        /// <summary>
        /// The parent global dictionary if it has one
        /// </summary>
        IDictionaryContainer Parent { get; }

        /// <summary>
        /// A dictionary of uniquely named texture dictionaries that are children of this global root
        /// The dictionaries are indexed by names and these names need to be unique.
        /// </summary>
        ObservableDictionary<String, GlobalTextureDictionary> GlobalTextureDictionaries { get; }

        /// <summary>
        /// A dictionary of uniquely named source texture dictionaries that are children of this global root
        /// The dictionaries are indexed by names and these names need to be unique.
        /// </summary>
        ObservableDictionary<String, SourceTextureDictionary> SourceTextureDictionaries { get; }

        /// <summary>
        /// Returns true iff this dictionary has a texture dictionary in it with
        /// the given name. If recursive is true it will recursive through all the texture
        /// dictionaries as well
        /// </summary>
        Boolean ContainsTextureDictionary(String name, Boolean recursive);

        /// <summary>
        /// Adds the given dictionary into the children texture dictionaries iff the name
        /// is unique.
        /// </summary>
        Boolean AddExistingGlobalDictionary(GlobalTextureDictionary dictionary);

        /// <summary>
        /// Creates a new dictionary with the given name and adds it to this dictionary 
        /// iff the name is unique
        /// </summary>
        Boolean AddNewGlobalDictionary(String name);

        /// <summary>
        /// Creates a new source dictionary using the given texture dictionary as a 
        /// source guide.
        /// </summary>
        Boolean AddNewSourceDictionary(TextureDictionary dictionary);

        /// <summary>
        /// Removes the texture dictionary with the given name from the list iff
        /// it exists.
        /// </summary>
        Boolean RemoveGlobalTextureDictionary(String name);

        /// <summary>
        /// Removes the given texture dictionary from the list iff
        /// it exists.
        /// </summary>
        Boolean RemoveGlobalTextureDictionary(GlobalTextureDictionary dictionary);

        /// <summary>
        /// Removes the given texture dictionary from the list iff
        /// it exists.
        /// </summary>
        Boolean RemoveSourceTextureDictionary(SourceTextureDictionary dictionary);

    }
}
