﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Configuration.Reports;
using RSG.Base.Configuration;
using RSG.SourceControl.Perforce;
using RSG.Base.ConfigParser;
using RSG.Statistics.Client.ServiceClients;
using RSG.Statistics.Common.Dto;
using RSG.Model.Statistics.Captures.Historical;
using System.Net.Mail;
using System.Web.UI;
using RSG.Model.Statistics.Captures;
using System.IO;
using System.ServiceModel;
using RSG.Statistics.Common.Config;
using System.ServiceModel.Description;
using RSG.Base.Tasks;
using System.Windows.Media;

namespace RSG.Model.Report.Reports.Captures
{
    /// <summary>
    /// Report that is emailed to users
    /// </summary>
    public class PerChangelistReport : Report, IDynamicReport, IReportMailProvider, IDisposable
    {
        #region Constants
        /// <summary>
        /// Default sender of the automated stats report mail.
        /// </summary>
        private const String c_defaultEmailSender = "workbench@rockstarnorth.com";

        // Constants for use in the html reports
        protected const String c_headerBackgroundColor = "#999";
        protected const String c_headerForegroundColor = "#FFF";
        protected readonly Color c_valueIncreaseColor = Colors.Red;
        protected readonly Color c_valueDecreaseColor = Colors.LightGreen;
        protected const float c_changePercentageClamp = 1.0f;

        private const String c_name = "Per Changelist Automated Capture Report";
        private const String c_description = "Report that shows information regarding various stats from the per changelist capture stats.";
        #endregion // Constants

        #region Private Classes
        /// <summary>
        /// 
        /// </summary>
        private abstract class ValueChangeBase
        {
            public abstract String TypeName { get; }
            public abstract String Unit { get; }

            public String ZoneName { get; private set; }
            public String ItemName { get; private set; }
            public float CurrentValue { get; private set; }
            public float PreviousValue { get; private set; }
            public double StandardDeviation { get; private set; }
            public float Difference { get { return CurrentValue - PreviousValue; } }
            public double StdPercent { get { return (StandardDeviation != 0.0 ? Difference / StandardDeviation : 0.0); } }

            protected ValueChangeBase(String zoneName, String itemName, HistoricalResults result)
            {
                ZoneName = zoneName;
                ItemName = itemName;
                CurrentValue = result.MostRecentValue;
                PreviousValue = result.PreviousValue;
                StandardDeviation = result.StandardDeviation;
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        private class FpsValueChange : ValueChangeBase
        {
            public override String TypeName { get { return "Fps"; } }
            public override String Unit { get { return "fps"; } }

            public FpsValueChange(String zoneName, HistoricalResults results)
                : base(zoneName, null, results)
            {
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private class FrametimeValueChange : ValueChangeBase
        {
            public override String TypeName { get { return "Frametime"; } }
            public override String Unit { get { return "ms"; } }

            public FrametimeValueChange(String zoneName, HistoricalResults results)
                : base(zoneName, null, results)
            {
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        private class CpuValueChange : ValueChangeBase
        {
            public override String TypeName { get { return "Timebars"; } }
            public override String Unit { get { return "ms"; } }
            public String SetName { get; private set; }

            public CpuValueChange(String zoneName, String setName, String metricName, HistoricalResults results)
                : base(zoneName, String.Format("{0}/{1}", setName, metricName), results)
            {
                SetName = setName;
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        private class ThreadValueChange : ValueChangeBase
        {
            public override String TypeName { get { return "Thread"; } }
            public override String Unit { get { return "ms"; } }

            public ThreadValueChange(String zoneName, String threadName, HistoricalResults results)
                : base(zoneName, threadName, results)
            {
            }
        }
        #endregion // Private Classes

        #region Properties
        /// <summary>
        /// Task to run for generating this report.
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (m_task == null)
                {
                    m_task = new ActionTask("Generating report", (context, progress) => GenerateReport((DynamicReportContext)context, progress));
                }
                return m_task;
            }
        }
        private ITask m_task;

        /// <summary>
        /// Mail message containing the report data
        /// </summary>
        public MailMessage MailMessage { get; private set; }

        /// <summary>
        /// Email address for the sender of the email.
        /// </summary>
        public string EmailSender { get; internal set; }

        /// <summary>
        /// Comma separated list of the default people to email
        /// </summary>
        public string DefaultEmailList { get; internal set; }

        /// <summary>
        /// Whether the people named in the modifications list should be emailed
        /// </summary>
        public bool EmailModifiers { get; internal set; }

        /// <summary>
        /// Number of changelists to use for returning results.
        /// </summary>
        public uint NumChangelists { get; internal set; }

        /// <summary>
        /// Ignore readings that are below a particular size.
        /// </summary>
        public float IgnoreReadingsBelow { get; internal set; }

        /// <summary>
        /// Ignore readings that haven't changed much.
        /// </summary>
        public float IgnoreDifferencesBelow { get; internal set; }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        private IConfig Config { get; set; }

        /// <summary>
        /// Report config object (for getting the modifications file).
        /// </summary>
        private IReportsConfig ReportsConfig { get; set; }

        /// <summary>
        /// The perforce connection object to use for p4 queries
        /// </summary>
        private P4 PerforceConnection
        {
            get
            {
                if (m_perforceConnection == null)
                {
                    m_perforceConnection = new P4();
                    m_perforceConnection.Connect();
                    m_createdConnection = true;
                }
                return m_perforceConnection;
            }
            set
            {
                m_perforceConnection = value;
            }
        }
        private P4 m_perforceConnection;
        private bool m_createdConnection = false;
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="desc"></param>
        public PerChangelistReport()
            : base(c_name, c_description)
        {
            Config = ConfigFactory.CreateConfig();
            ReportsConfig = ConfigFactory.CreateReportConfig(Config.Project);

            EmailSender = c_defaultEmailSender;
            EmailModifiers = false;
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// Generate the report dynamically.
        /// </summary>
        private void GenerateReport(DynamicReportContext context, IProgress<TaskProgress> progress)
        {
            // Gather the data from the server
            uint changelist;
            IDictionary<String, HistoricalZoneResults> perZoneData = GatherPerZoneData(out changelist);

            // Creates some summary information about all the values that have changed.
            IList<ValueChangeBase> valueChanges = GatherValueChanges(perZoneData);

            // Extract the modifications information from the modifications file.
            Modifications mods = new Modifications(ReportsConfig.AutomatedReports.PerChangelistModificationsFile);

            // Generate the report contents and create the email.
            Stream contentStream = GenerateContentStream(mods, valueChanges, changelist);
            String subject = String.Format("{0} (CL: {1})", Name, changelist);

            MailMessage = GenerateEmail(subject, contentStream, GetEmailAddresses(mods));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private IDictionary<String, HistoricalZoneResults> GatherPerZoneData(out uint changelist)
        {
            IDictionary<String, HistoricalZoneResults> perZoneData = new Dictionary<String, HistoricalZoneResults>();

            // Create the statistics endpoint for retrieving the relevant data.
            IStatisticsConfig statsConfig = new StatisticsConfig();
            EndpointAddress address = new EndpointAddress(String.Format("http://{0}:{1}/CaptureStats", statsConfig.DefaultServer.Address, statsConfig.DefaultServer.HttpPort));
            WebHttpBinding binding = new WebHttpBinding(WebHttpSecurityMode.None);
            binding.MaxReceivedMessageSize = 4194304;
            CaptureClient client = new CaptureClient(binding, address);
            client.Endpoint.Behaviors.Add(new WebHttpBehavior());

            // What changelist are we creating the stats for?
            changelist = client.RetrieveLatestChangelistNumber();

            // Get the list of zones that were a part of that changelist
            NameHashDtos zones = client.RetrieveChangelistZones(changelist.ToString());

            // Gather the data associated with each zone.
            foreach (NameHashDto zone in zones.Items)
            {
                HistoricalZoneResults zoneData = client.RetrieveHistoricalResultsForZone(changelist.ToString(), zone.Hash.ToString(), NumChangelists);
                perZoneData.Add(zone.Name, zoneData);
            }

            client.Close();

            return perZoneData;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="perZoneData"></param>
        /// <returns></returns>
        private IList<ValueChangeBase> GatherValueChanges(IDictionary<String, HistoricalZoneResults> perZoneData)
        {
            IList<ValueChangeBase> valueChanges = new List<ValueChangeBase>();
            foreach (HistoricalZoneResults zoneData in perZoneData.Values)
            {
                String zoneName = zoneData.ZoneName;

                if (zoneData.FpsResults.Values.Count > 1)
                {
                    /*
                     * This didn't work as we can't conver thte standard deviation from fps to ms.
                    HistoricalResults fpsResults = new HistoricalResults();

                    // Convert the fps to milliseconds
                    foreach (KeyValuePair<uint, float> pair in zoneData.FpsResults.ValuePerChangelist)
                    {
                        if (pair.Value != 0.0f)
                        {
                            fpsResults.ValuePerChangelist[pair.Key] = (1.0f / pair.Value) * 1000.0f;
                        }
                    }
                    fpsResults.StandardDeviation = 1.0 / (zoneData.FpsResults.StandardDeviation * 1000.0);
                    */
                    valueChanges.Add(new FpsValueChange(zoneName, zoneData.FpsResults));
                }

                if (zoneData.FrametimeResults.Values.Count > 1)
                {
                    valueChanges.Add(new FrametimeValueChange(zoneName, zoneData.FrametimeResults));
                }

                foreach (KeyValuePair<String, HistoricalCpuSetResult> setPair in zoneData.CpuResults.PerSetResults)
                {
                    String setName = setPair.Key;

                    foreach (KeyValuePair<String, HistoricalResults> metricPair in setPair.Value.PerMetricResults)
                    {
                        String metricName = metricPair.Key;

                        if (ShouldTrackChange(metricPair.Value))
                        {
                            valueChanges.Add(new CpuValueChange(zoneName, setName, metricName, metricPair.Value));
                        }
                    }
                }

                foreach (KeyValuePair<String, HistoricalResults> threadPair in zoneData.ThreadResults.Results)
                {
                    String threadName = threadPair.Key;

                    if (ShouldTrackChange(threadPair.Value))
                    {
                        valueChanges.Add(new ThreadValueChange(zoneName, threadName, threadPair.Value));
                    }
                }
            }

            return valueChanges;
        }

        /// <summary>
        /// Helper function that returns whether we should track a particular change.
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        private bool ShouldTrackChange(HistoricalResults result)
        {
            // Track this if there is more than one changelist it has appeared in, the readings are above the minimum cutoff
            // and the difference is above the minimum cutoff.
            return (result.Values.Count > 1) &&
                    (result.MostRecentValue > IgnoreReadingsBelow || result.PreviousValue > IgnoreReadingsBelow) &&
                    (Math.Abs(result.LatestDifference) > IgnoreDifferencesBelow);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mods"></param>
        /// <param name="perZoneData"></param>
        private Stream GenerateContentStream(Modifications mods, IList<ValueChangeBase> valueChanges, uint changelist)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter sw = new StreamWriter(stream);
            HtmlTextWriter writer = new HtmlTextWriter(sw);

            writer.RenderBeginTag(HtmlTextWriterTag.Html);
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Body);
                {
                    OutputHeader(writer, changelist);
                    writer.RenderBeginTag(HtmlTextWriterTag.Br);
                    writer.RenderEndTag();

                    OutputModifications(writer, mods);
                    writer.RenderBeginTag(HtmlTextWriterTag.Br);
                    writer.RenderEndTag();

                    OutputTopTen(writer, valueChanges);
                    writer.RenderBeginTag(HtmlTextWriterTag.Br);
                    writer.RenderEndTag();

                    OutputHistory(writer, valueChanges);
                }
                writer.RenderEndTag();
            }
            writer.RenderEndTag();

            writer.Flush();

            // Reset the local stream and set the property
            stream.Position = 0;
            return stream;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="mods"></param>
        private void OutputHeader(HtmlTextWriter writer, uint changelist)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Border, "1");
            writer.AddAttribute(HtmlTextWriterAttribute.Width, "100%");
            writer.RenderBeginTag(HtmlTextWriterTag.Table);
            {
                // Header row
                writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                writer.AddAttribute(HtmlTextWriterAttribute.Align, "center");
                writer.AddAttribute(HtmlTextWriterAttribute.Style, String.Format("background-color: {0}; color: {1}; font-size: 16pt", c_headerBackgroundColor, c_headerForegroundColor));
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write(String.Format("{0} ({1}, CL: {2})", Name, Config.Project.Name, changelist));
                writer.RenderEndTag();
                writer.RenderEndTag();
            }
            writer.RenderEndTag();

            // Create a link to the webpage containing the full details.
            // e.g. http://rsgedista2:8080/capture_stats/report.html?Changelist=3636446

            writer.Write(String.Format("Full Details: "));
            String url = String.Format("http://rsgedista2:8080/capture_stats/report.html?Changelist={0}", changelist);
            writer.AddAttribute(HtmlTextWriterAttribute.Href, url);
            writer.RenderBeginTag(HtmlTextWriterTag.A);
            writer.Write(url);
            writer.RenderEndTag();

            // TODO: Add link to wiki containing information about the report.
        }

        /// <summary>
        /// Outputs a table containing the modification history.
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="mods"></param>
        private void OutputModifications(HtmlTextWriter writer, Modifications mods)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Border, "1");
            writer.AddAttribute(HtmlTextWriterAttribute.Width, "100%");
            writer.RenderBeginTag(HtmlTextWriterTag.Table);
            {
                // Header row
                writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                writer.AddAttribute(HtmlTextWriterAttribute.Colspan, "4");
                writer.AddAttribute(HtmlTextWriterAttribute.Align, "center");
                writer.AddAttribute(HtmlTextWriterAttribute.Style, String.Format("background-color: {0}; color: {1}; font-size: 16pt", c_headerBackgroundColor, c_headerForegroundColor));
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write("Modifications");
                writer.RenderEndTag();
                writer.RenderEndTag();

                writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                writer.AddAttribute(HtmlTextWriterAttribute.Align, "center");
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write("Changelist");
                writer.RenderEndTag();

                writer.AddAttribute(HtmlTextWriterAttribute.Align, "center");
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write("Committer");
                writer.RenderEndTag();

                writer.AddAttribute(HtmlTextWriterAttribute.Align, "center");
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write("Description");
                writer.RenderEndTag();

                writer.AddAttribute(HtmlTextWriterAttribute.Align, "center");
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write("Files");
                writer.RenderEndTag();
                writer.RenderEndTag();

                // Output a list of the modifications
                foreach (Modification mod in mods.AllModifications.Where(item => item.Username != "buildernorth"))
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                    writer.RenderBeginTag(HtmlTextWriterTag.Td);

                    // Create a link to the changelist.
                    // e.g. http://rsgedip4s1:8080/@md=d&cd=@/3636446?ac=10
                    writer.AddAttribute(HtmlTextWriterAttribute.Href, String.Format("http://rsgedip4s1:8080/@md=d&cd=@/{0}?ac=10", mod.Changelist));
                    writer.RenderBeginTag(HtmlTextWriterTag.A);
                    writer.Write(mod.Changelist);
                    writer.RenderEndTag();

                    writer.RenderEndTag();
                    writer.RenderBeginTag(HtmlTextWriterTag.Td);
                    writer.Write(mod.Username);
                    writer.RenderEndTag();
                    writer.RenderBeginTag(HtmlTextWriterTag.Td);
                    writer.Write(mod.Comment);
                    writer.RenderEndTag();
                    writer.RenderBeginTag(HtmlTextWriterTag.Td);
                    writer.Write(mod.Files.Count);
                    writer.RenderEndTag();
                    writer.RenderEndTag();
                }
            }
            writer.RenderEndTag();
        }

        /// <summary>
        /// Outputs the top ten differences in a table.
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="perZoneData"></param>
        private void OutputTopTen(HtmlTextWriter writer, IList<ValueChangeBase> valueChanges)
        {
            // Output them to the message.
            writer.AddAttribute(HtmlTextWriterAttribute.Border, "1");
            writer.AddAttribute(HtmlTextWriterAttribute.Width, "100%");
            writer.RenderBeginTag(HtmlTextWriterTag.Table);
            {
                // Header row
                writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                writer.AddAttribute(HtmlTextWriterAttribute.Colspan, "5");
                writer.AddAttribute(HtmlTextWriterAttribute.Align, "center");
                writer.AddAttribute(HtmlTextWriterAttribute.Style, String.Format("background-color: {0}; color: {1}; font-size: 16pt", c_headerBackgroundColor, c_headerForegroundColor));
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write("Top Ten Changes");
                writer.RenderEndTag();
                writer.RenderEndTag();

                OutputHeaderRow(writer, true);

                // Output the top ten changes
                foreach (ValueChangeBase change in valueChanges.OrderByDescending(item => item.StdPercent).Take(10))
                {
                    OutputValueChangeTableRow(writer, change, true, true);
                }
            }
            writer.RenderEndTag();
        }

        /// <summary>
        /// Outputs the historical information.
        /// </summary>
        /// <param name="writer"></param>
        private void OutputHistory(HtmlTextWriter writer, IList<ValueChangeBase> valueChanges)
        {
            // Output them to the message.
            writer.AddAttribute(HtmlTextWriterAttribute.Border, "1");
            writer.AddAttribute(HtmlTextWriterAttribute.Width, "100%");
            writer.RenderBeginTag(HtmlTextWriterTag.Table);
            {
                // Header row
                writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                writer.AddAttribute(HtmlTextWriterAttribute.Align, "center");
                writer.AddAttribute(HtmlTextWriterAttribute.Style, String.Format("background-color: {0}; color: {1}; font-size: 16pt;", c_headerBackgroundColor, c_headerForegroundColor));
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write("History");
                writer.RenderEndTag();
                writer.RenderEndTag();
            }
            writer.RenderEndTag();

            // Output information per zone
            foreach (IGrouping<String, ValueChangeBase> zoneGroup in valueChanges.GroupBy(item => item.ZoneName))
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Br);
                writer.RenderEndTag();

                writer.AddAttribute(HtmlTextWriterAttribute.Border, "1");
                writer.AddAttribute(HtmlTextWriterAttribute.Width, "100%");
                writer.RenderBeginTag(HtmlTextWriterTag.Table);
                {
                    // Header row
                    writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                    writer.AddAttribute(HtmlTextWriterAttribute.Style, String.Format("background-color: {0}; color: {1}; font-size: 14pt;", c_headerBackgroundColor, c_headerForegroundColor));
                    writer.RenderBeginTag(HtmlTextWriterTag.Td);
                    writer.Write(zoneGroup.Key);
                    writer.RenderEndTag();
                    writer.RenderEndTag();
                }
                writer.RenderEndTag();


                // Fps Information
                IEnumerable<ValueChangeBase> fpsChanges = zoneGroup.OfType<FpsValueChange>();

                if (fpsChanges.Any())
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.H3);
                    writer.Write("Fps");
                    writer.RenderEndTag();

                    writer.AddAttribute(HtmlTextWriterAttribute.Border, "1");
                    writer.AddAttribute(HtmlTextWriterAttribute.Width, "100%");
                    writer.RenderBeginTag(HtmlTextWriterTag.Table);
                    {
                        OutputHeaderRow(writer, false, false);
                        foreach (ValueChangeBase change in fpsChanges.OrderBy(item => item.ItemName))
                        {
                            OutputValueChangeTableRow(writer, change, false, false, false);
                        }
                    }
                    writer.RenderEndTag();
                }

                // Frametime Information
                IEnumerable<ValueChangeBase> ftChanges = zoneGroup.OfType<FrametimeValueChange>();

                if (ftChanges.Any())
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.H3);
                    writer.Write("Frametime");
                    writer.RenderEndTag();

                    writer.AddAttribute(HtmlTextWriterAttribute.Border, "1");
                    writer.AddAttribute(HtmlTextWriterAttribute.Width, "100%");
                    writer.RenderBeginTag(HtmlTextWriterTag.Table);
                    {
                        OutputHeaderRow(writer, false, false);
                        foreach (ValueChangeBase change in ftChanges.OrderBy(item => item.ItemName))
                        {
                            OutputValueChangeTableRow(writer, change, false, false, false);
                        }
                    }
                    writer.RenderEndTag();
                }

                // Thread information
                IEnumerable<ValueChangeBase> threadChanges = zoneGroup.OfType<ThreadValueChange>();

                if (threadChanges.Any())
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.H3);
                    writer.Write("Threads");
                    writer.RenderEndTag();

                    writer.AddAttribute(HtmlTextWriterAttribute.Border, "1");
                    writer.AddAttribute(HtmlTextWriterAttribute.Width, "100%");
                    writer.RenderBeginTag(HtmlTextWriterTag.Table);
                    {
                        OutputHeaderRow(writer);

                        foreach (ValueChangeBase change in threadChanges.OrderBy(item => item.ItemName))
                        {
                            OutputValueChangeTableRow(writer, change);
                        }
                    }
                    writer.RenderEndTag();
                }

                // Cpu information
                IEnumerable<ValueChangeBase> cpuChanges = zoneGroup.OfType<CpuValueChange>();

                if (cpuChanges.Any())
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.H3);
                    writer.Write("Timebars");
                    writer.RenderEndTag();

                    foreach (IGrouping<String, ValueChangeBase> setGroupedChanges in cpuChanges.GroupBy(item => (item as CpuValueChange).SetName))
                    {
                        writer.RenderBeginTag(HtmlTextWriterTag.H4);
                        writer.Write(setGroupedChanges.Key);
                        writer.RenderEndTag();

                        writer.AddAttribute(HtmlTextWriterAttribute.Border, "1");
                        writer.AddAttribute(HtmlTextWriterAttribute.Width, "100%");
                        writer.RenderBeginTag(HtmlTextWriterTag.Table);
                        {
                            OutputHeaderRow(writer);
                            foreach (ValueChangeBase change in setGroupedChanges.OrderBy(item => (item as CpuValueChange).ItemName))
                            {
                                OutputValueChangeTableRow(writer, change);
                            }
                        }
                        writer.RenderEndTag();
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="includeTestName"></param>
        private void OutputHeaderRow(HtmlTextWriter writer, bool includeTestName = false, bool includeItemName = true)
        {
            // Have the same column widths based on what is output.
            int numParts = 1 + 2 + 2 + (includeTestName ? 3 : 0) + (includeItemName ? 5 : 0);
            float widthPerPart = 100 / numParts;

            writer.RenderBeginTag(HtmlTextWriterTag.Tr);
            if (includeItemName)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Align, "center");
                writer.AddAttribute(HtmlTextWriterAttribute.Width, String.Format("{0}%", (int)(widthPerPart * 5)));
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write("Item");
                writer.RenderEndTag();
            }

            writer.AddAttribute(HtmlTextWriterAttribute.Align, "center");
            writer.AddAttribute(HtmlTextWriterAttribute.Width, String.Format("{0}%", (int)(widthPerPart * 1)));
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.Write("Current");
            writer.RenderEndTag();

            writer.AddAttribute(HtmlTextWriterAttribute.Align, "center");
            writer.AddAttribute(HtmlTextWriterAttribute.Width, String.Format("{0}%", (int)(widthPerPart * 2)));
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.Write("Previous (std)");
            writer.RenderEndTag();

            writer.AddAttribute(HtmlTextWriterAttribute.Align, "center");
            writer.AddAttribute(HtmlTextWriterAttribute.Width, String.Format("{0}%", (int)(widthPerPart * 2)));
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.Write("Diff (Std %)");
            writer.RenderEndTag();

            if (includeTestName)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Align, "center");
                writer.AddAttribute(HtmlTextWriterAttribute.Width, String.Format("{0}%", (int)(widthPerPart * 3)));
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write("Test Name");
                writer.RenderEndTag();
            }
            writer.RenderEndTag();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="change"></param>
        private void OutputValueChangeTableRow(HtmlTextWriter writer, ValueChangeBase change, bool useFullName = false, bool includeTestName = false, bool includeItemName = true)
        {
            // Ignore changes that aren't of interest to use (those that haven't changed enough to warrant displaying).
            writer.RenderBeginTag(HtmlTextWriterTag.Tr);
            if (includeItemName)
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                if (useFullName)
                {
                    if (change.ItemName != null)
                    {
                        writer.Write(String.Format("{0}/{1}", change.TypeName, change.ItemName));
                    }
                    else
                    {
                        writer.Write(change.TypeName);
                    }
                }
                else
                {
                    writer.Write(change.ItemName);
                }
                writer.RenderEndTag();
            }
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.Write(OutputValue(change.CurrentValue, change.Unit));
            writer.RenderEndTag();
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.Write(String.Format("{0} ({1})", OutputValue(change.PreviousValue, change.Unit), OutputValue(change.StandardDeviation, change.Unit)));
            writer.RenderEndTag();
            writer.AddAttribute(HtmlTextWriterAttribute.Style, CreateStyleForValueChange(change));
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.Write(String.Format("{0} ({1:P1})", OutputValue(change.Difference, change.Unit), change.StdPercent));
            writer.RenderEndTag();
            if (includeTestName)
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write(change.ZoneName);
                writer.RenderEndTag();
            }
            writer.RenderEndTag();
        }

        /// <summary>
        /// Helper function for outputting values.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private String OutputValue(double value, String unit)
        {
            if (Math.Truncate(value) > 0)
            {
                return String.Format("{0:0.###} {1}", value, unit);
            }
            else
            {
                return String.Format("{0:0.#######} {1}", RoundToSignificantDigits(value, 3), unit);
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="change"></param>
        /// <returns></returns>
        private String CreateStyleForValueChange(ValueChangeBase change)
        {
            // Fade the color to white based on the change percentage, clamped to c_changePercentageClamp
            double diff = change.StdPercent;
            if (diff > c_changePercentageClamp)
            {
                diff = c_changePercentageClamp;
            }
            else if (diff < -c_changePercentageClamp)
            {
                diff = -c_changePercentageClamp;
            }

            Color bgColor = (diff > 0.0f ? c_valueIncreaseColor : c_valueDecreaseColor);
            bgColor.R = (byte)(bgColor.R + (255 - bgColor.R) * (1.0 - Math.Abs(diff)));
            bgColor.G = (byte)(bgColor.G + (255 - bgColor.G) * (1.0 - Math.Abs(diff)));
            bgColor.B = (byte)(bgColor.B + (255 - bgColor.B) * (1.0 - Math.Abs(diff)));

            return String.Format("background-color: #{0}{1}{2};", bgColor.R.ToString("X2"), bgColor.G.ToString("X2"), bgColor.B.ToString("X2"));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="d"></param>
        /// <param name="digits"></param>
        /// <returns></returns>
        private double RoundToSignificantDigits(double d, int digits)
        {
            double negate = 1;
            if (d == 0)
            {
                return 0;
            }
            if (d < 0)
            {
                negate = -1;
                d = Math.Abs(d);
            }

            double scale = Math.Pow(10, Math.Floor(Math.Log10(d)) + 1);
            return scale * Math.Round(d / scale, digits) * negate;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="d"></param>
        /// <param name="digits"></param>
        /// <returns></returns>
        private double TruncateToSignificantDigits(double d, int digits)
        {
            double negate = 1;
            if (d == 0)
            {
                return 0;
            }
            if (d < 0)
            {
                negate = -1;
                d = Math.Abs(d);
            }

            double scale = Math.Pow(10, Math.Floor(Math.Log10(d)) + 1 - digits);
            return scale * Math.Truncate(d / scale) * negate;
        }

        /// <summary>
        /// Get the list of people to email.
        /// </summary>
        /// <param name="modifications"></param>
        /// <returns></returns>
        private IEnumerable<string> GetEmailAddresses(Modifications mods)
        {
            // Add any default recipients to the list
            if (DefaultEmailList != null)
            {
                foreach (string email in DefaultEmailList.Split(new char[] { ';' }))
                {
                    yield return email;
                }
            }

            // Do we want to email all the modifiers as well?
            if (EmailModifiers)
            {
                foreach (Modification mod in mods.AllModifications)
                {
                    P4API.P4RecordSet userRecordSet = PerforceConnection.Run("users", mod.Username);

                    if (userRecordSet.Records.Length > 0)
                    {
                        P4API.P4Record userRecord = userRecordSet.Records[0];
                        if (userRecord.Fields.ContainsKey("Email"))
                        {
                            yield return userRecord["Email"];
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Generate the mail message.
        /// </summary>
        /// <param name="content"></param>
        /// <param name="emailList"></param>
        /// <returns></returns>
        private MailMessage GenerateEmail(String subject, Stream content, IEnumerable<string> emailList)
        {
            MailMessage mailMessage = new MailMessage();

            // Add basic information.
            mailMessage.From = new MailAddress(EmailSender);
            mailMessage.Subject = subject;

            foreach (string email in emailList.Distinct())
            {
                mailMessage.To.Add(email);
            }

            // Create the HTML view of the message and add it to the mail
            AlternateView htmlView = null;

            content.Position = 0;
            using (StreamReader reader = new StreamReader(content))
            {
                String mailContent = reader.ReadToEnd();
                htmlView = AlternateView.CreateAlternateViewFromString(mailContent, null, "text/html");
                mailMessage.AlternateViews.Add(htmlView);
            }

            return mailMessage;
        }
        #endregion // Private Methods

        #region IDisposable Implementation
        /// <summary>
        /// Perform cleanup
        /// </summary>
        public virtual void Dispose()
        {
            // Disconnect from perforce (if we created the p4 object)
            if (m_createdConnection)
            {
                PerforceConnection.Disconnect();
            }
        }
        #endregion // IDisposable Implementation
    } // PerChangelistReport
}
