﻿//---------------------------------------------------------------------------------------------
// <copyright file="CharacterMappings.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.Model
{
    using System;
    using System.Xml;
    using RSG.Editor.Model;

    /// <summary>
    /// Contains all of the character mappings for a specific dialogue object.
    /// </summary>
    public class CharacterMappings : ModelBase
    {
        #region Fields
        /// <summary>
        /// The internal collection of mappings.
        /// </summary>
        private ModelCollection<CharacterMapping> _internal;

        /// <summary>
        /// The private field used for the <see cref="Mappings"/> property.
        /// </summary>
        private ReadOnlyModelCollection<CharacterMapping> _mappings;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CharacterMappings"/> class.
        /// </summary>
        /// <param name="dialogue">
        /// The dialogue object this mappings class is associated with.
        /// </param>
        public CharacterMappings(Dialogue dialogue)
            : base(dialogue)
        {
            this._internal = new ModelCollection<CharacterMapping>();
            this._mappings = new ReadOnlyModelCollection<CharacterMapping>(this._internal);

            using (new SuspendUndoEngine(this.UndoEngine, SuspendUndoEngineMode.Both))
            {
                for (int i = 0; i < 36; i++)
                {
                    if (i == 9)
                    {
                        continue;
                    }

                    this._internal.Add(new CharacterMapping(this) { SpeakerId = i });
                }
            }

            this._internal.SetUndoEngineProvider(this);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="CharacterMappings"/> class using the
        /// specified xml reader as a data provider.
        /// </summary>
        /// <param name="dialogue">
        /// The dialogue object this mappings class is associated with.
        /// </param>
        /// <param name="reader">
        /// The xml reader that contains the data used to initialise this instance.
        /// </param>
        public CharacterMappings(Dialogue dialogue, XmlReader reader)
            : base(dialogue)
        {
            this._internal = new ModelCollection<CharacterMapping>();
            this._mappings = new ReadOnlyModelCollection<CharacterMapping>(this._internal);

            using (new SuspendUndoEngine(this.UndoEngine, SuspendUndoEngineMode.Both))
            {
                for (int i = 0; i < 36; i++)
                {
                    if (i == 9)
                    {
                        continue;
                    }

                    this._internal.Add(new CharacterMapping(this) { SpeakerId = i });
                }
            }

            this.Deserialise(reader);
            this._internal.SetUndoEngineProvider(this);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the dialogue configurations that have been associated with this mapping.
        /// </summary>
        public DialogueConfigurations Configurations
        {
            get
            {
                if (this.Dialogue == null)
                {
                    return new DialogueConfigurations();
                }

                return this.Dialogue.Configurations;
            }
        }

        /// <summary>
        /// Gets the dialogue this mapping is associated with.
        /// </summary>
        public Dialogue Dialogue
        {
            get { return this.Parent as Dialogue; }
        }

        /// <summary>
        /// Gets the read-only collection of all of the mappings.
        /// </summary>
        public ReadOnlyModelCollection<CharacterMapping> Mappings
        {
            get { return this._mappings; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Adds a new mapping or modifies the current mapping for the specified character id.
        /// </summary>
        /// <param name="characterId">
        /// The character id to map.
        /// </param>
        /// <param name="speakerId">
        /// The speaker id for the character.
        /// </param>
        /// <param name="voiceName">
        /// The voice name for the specified character.
        /// </param>
        public void AddOrModifyMapping(Guid characterId, int speakerId, string voiceName)
        {
            foreach (CharacterMapping mapping in this._mappings)
            {
                if (int.Equals(speakerId, mapping.SpeakerId))
                {
                    mapping.CharacterId = characterId;
                    mapping.SpeakerId = speakerId;
                    mapping.VoiceName = voiceName;
                    return;
                }
            }

            this._internal.Add(new CharacterMapping(this)
            {
                CharacterId = characterId,
                VoiceName = voiceName,
                SpeakerId = speakerId
            });
        }

        /// <summary>
        /// Gets the speaker id value for the mapped character id. If the specified character
        /// is not mapped then 9 is returned.
        /// </summary>
        /// <param name="characterId">
        /// The character id whose mapped speaker id with be returned.
        /// </param>
        /// <returns>
        /// The mapped speaker id for the specified character id.
        /// </returns>
        public int GetSpeakerFromCharacterId(Guid characterId)
        {
            foreach (CharacterMapping mapping in this._mappings)
            {
                if (Guid.Equals(mapping.CharacterId, characterId))
                {
                    return mapping.SpeakerId;
                }
            }

            return 9;
        }

        /// <summary>
        /// Gets the voice name for the mapped character id. If the specified character is not
        /// mapped then null is returned.
        /// </summary>
        /// <param name="characterId">
        /// The character id whose mapped voice name with be returned.
        /// </param>
        /// <returns>
        /// The mapped voice name for the specified character id.
        /// </returns>
        public string GetVoiceNameFromCharacterId(Guid characterId)
        {
            foreach (CharacterMapping mapping in this._mappings)
            {
                if (Guid.Equals(mapping.CharacterId, characterId))
                {
                    return mapping.VoiceName;
                }
            }

            return null;
        }

        /// <summary>
        /// Determines whether the character with the specified id is currently mapped.
        /// </summary>
        /// <param name="characterId">
        /// The character id to test.
        /// </param>
        /// <returns>
        /// True if the character is mapped; otherwise, false.
        /// </returns>
        public bool IsCharacterMapped(Guid characterId)
        {
            if (characterId == Guid.Empty)
            {
                return false;
            }

            foreach (CharacterMapping mapping in this._mappings)
            {
                if (Guid.Equals(mapping.CharacterId, characterId))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Initialises this instance using the data contained within the specified xml reader.
        /// </summary>
        /// <param name="reader">
        /// The xml reader that contains the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() == XmlNodeType.Element)
            {
                if (String.Equals("Item", reader.Name))
                {
                    CharacterMapping mapping = new CharacterMapping(this, reader);
                    this.AddOrModifyMapping(mapping.CharacterId, mapping.SpeakerId, null);
                }
                else
                {
                    reader.Skip();
                }
            }

            if (reader.NodeType == XmlNodeType.EndElement)
            {
                reader.ReadEndElement();
            }
        }
        #endregion Methods
    } // RSG.Text.Model.CharacterMappings {Class}
} // RSG.Text.Model {Namespace}
