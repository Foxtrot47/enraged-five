﻿// --------------------------------------------------------------------------------------------
// <copyright file="StringType.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions.Members
{
    /// <summary>
    /// Defines the different types of string members the parCodeGen system supports.
    /// </summary>
    public enum StringType
    {
        /// <summary>
        /// The string type is unknown.
        /// </summary>
        Unknown,

        /// <summary>
        /// The string type is not recognised as valid, though it is defined as something.
        /// </summary>
        Unrecognised,

        /// <summary>
        /// The string member has a type of pointer.
        /// </summary>
        Pointer,

        /// <summary>
        /// The string member has a type of member and needs to have a limited size.
        /// </summary>
        Member,

        /// <summary>
        /// The string member has a type of ConstString.
        /// </summary>
        Const,

        /// <summary>
        /// The string member has a type of AtString.
        /// </summary>
        AtString,

        /// <summary>
        /// The string member has a type of wide_pointer.
        /// </summary>
        WidePointer,

        /// <summary>
        /// The string member has a type of wide_member and needs to have a limited size.
        /// </summary>
        WideMember,

        /// <summary>
        /// The string member has a type of atWideString.
        /// </summary>
        WideString,

        /// <summary>
        /// The string member has a type of atHashString.
        /// </summary>
        Hash,

        /// <summary>
        /// The string member has a type of atFinalHashString.
        /// </summary>
        FinalHash,

        /// <summary>
        /// The string member has a type of atHashValue.
        /// </summary>
        HashValue,

        /// <summary>
        /// The string member has a type of atHashWithStringDev.
        /// </summary>
        HashDev,

        /// <summary>
        /// The string member has a type of atHashWithStringBank.
        /// </summary>
        HashBank,

        /// <summary>
        /// The string member has a type of atHashWithStringNoFinal.
        /// </summary>
        HashNotFinal,
    } // RSG.Metadata.Model.Definitions.Members.StringType {Enum}
} // RSG.Metadata.Model.Definitions.Members {Namespace}
