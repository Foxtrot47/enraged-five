﻿// --------------------------------------------------------------------------------------------
// <copyright file="IEnumeration.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions
{
    using System;
    using System.Xml;
    using RSG.Editor.Model;

    /// <summary>
    /// When implemented defines a parsable enumeration that the parCodeGen system can use.
    /// </summary>
    public interface IEnumeration : IDefinition, IEquatable<IEnumeration>
    {
        #region Properties
        /// <summary>
        /// Gets or sets the code generation option.
        /// </summary>
        string CodeGeneration { get; set; }

        /// <summary>
        /// Gets a read-only collection containing the constants that are defined for this
        /// instance.
        /// </summary>
        ReadOnlyModelCollection<IEnumConstant> EnumConstants { get; }

        /// <summary>
        /// Gets or sets the sorting method that should be applied to the items when displayed.
        /// </summary>
        EnumerationSortMethod SortMethod { get; set; }

        /// <summary>
        /// Gets or sets the generator that is used to create the constant values.
        /// </summary>
        EnumValueGenerator ValueGenerator { get; set; }

        /// <summary>
        /// Gets the constant at the specified index.
        /// </summary>
        /// <param name="index">
        /// The zero-based index of the constant to get.
        /// </param>
        /// <returns>
        /// The constant at the specified index.
        /// </returns>
        IEnumConstant this[int index] { get; }

        /// <summary>
        /// Gets the constant that has the specified name if it exists; otherwise, null.
        /// </summary>
        /// <param name="name">
        /// The name of the constant to get.
        /// </param>
        /// <returns>
        /// The constant with the specified name or null if it doesn't exist.
        /// </returns>
        IEnumConstant this[string name] { get; }

        /// <summary>
        /// Gets the constant that has the specified value if it exists; otherwise, null.
        /// </summary>
        /// <param name="value">
        /// The value of the constant to get.
        /// </param>
        /// <returns>
        /// The constant with the specified value or null if it doesn't exist.
        /// </returns>
        IEnumConstant this[long value] { get; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new <see cref="IEnumeration"/> that is a copy of this instance.
        /// </summary>
        /// <returns>
        /// A new <see cref="IEnumeration"/> that is a copy of this instance.
        /// </returns>
        new IEnumeration Clone();

        /// <summary>
        /// Searches for a constant with the specified name and returns the zero-based index of
        /// the first occurrence within the enumeration.
        /// </summary>
        /// <param name="name">
        /// The name of the constant you wish to get the index of.
        /// </param>
        /// <returns>
        /// The zero-based index of the first occurrence of the specified name within the
        /// enumeration, if found; otherwise, –1.
        /// </returns>
        int GetIndexOf(string name);

        /// <summary>
        /// Writes the xml representation of this instance onto the specified
        /// System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that the xml representation of this instance should be
        /// written to.
        /// </param>
        /// <param name="saveFileInfo">
        /// If true the file, line number and line position will be written out as attributes.
        /// </param>
        void Serialise(XmlWriter writer, bool saveFileInfo);
        #endregion Methods
    } // RSG.Metadata.Model.Definitions.IEnumeration {Interface}
} // RSG.Metadata.Model.Definitions {Namespace}
