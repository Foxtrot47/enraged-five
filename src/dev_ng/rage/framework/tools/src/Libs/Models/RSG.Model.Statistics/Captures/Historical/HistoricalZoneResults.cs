﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using RSG.Model.Common.Report;

namespace RSG.Model.Statistics.Captures.Historical
{
    /// <summary>
    /// Encapsulates all historical results for a particular zone.
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class HistoricalZoneResults
    {
        #region Properties
        /// <summary>
        /// Name of the zone these results are for.
        /// </summary>
        [DataMember]
        public String ZoneName { get; set; }

        /// <summary>
        /// Historical cpu results.
        /// </summary>
        [DataMember]
        public HistoricalCpuResults CpuResults { get; set; }

        /// <summary>
        /// Historical fps results.
        /// </summary>
        [DataMember]
        public HistoricalResults FpsResults { get; set; }

        /// <summary>
        /// Historical fps results.
        /// </summary>
        [DataMember]
        public HistoricalResults FrametimeResults { get; set; }

        /// <summary>
        /// Historical gpu results.
        /// </summary>
        [DataMember]
        public HistoricalGpuResults GpuResults { get; set; }

        /// <summary>
        /// Historical thread results.
        /// </summary>
        [DataMember]
        public HistoricalThreadResults ThreadResults { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Main constructor.
        /// </summary>
        public HistoricalZoneResults(String zone)
        {
            ZoneName = zone;
            CpuResults = new HistoricalCpuResults();
            FpsResults = new HistoricalResults();
            FrametimeResults = new HistoricalResults();
            GpuResults = new HistoricalGpuResults();
            ThreadResults = new HistoricalThreadResults();
        }
        #endregion // Constructor(s)
    } // HistoricalZoneResults
}
