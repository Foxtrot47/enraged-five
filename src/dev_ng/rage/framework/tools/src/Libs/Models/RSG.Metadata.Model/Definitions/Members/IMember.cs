﻿// --------------------------------------------------------------------------------------------
// <copyright file="IMember.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions.Members
{
    using System;
    using System.Xml;
    using RSG.Base.Logging;
    using RSG.Editor.Model;
using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// When implemented represents a member of a parsable structure definition that the
    /// parCodeGen system can use.
    /// </summary>
    public interface IMember : IModel, IEquatable<IMember>
    {
        #region Properties
        /// <summary>
        /// Gets a value indicating whether or not a tunable can be created for this member.
        /// </summary>
        bool CanBeInstanced { get; }

        /// <summary>
        /// Gets or sets the description for this member.
        /// </summary>
        string Description { get; set; }

        /// <summary>
        /// Gets the dictionary this member is associated with.
        /// </summary>
        IDefinitionDictionary Dictionary { get; }

        /// <summary>
        /// Gets or sets an array of runtime visitors which cannot see this member.
        /// </summary>
        string[] HideFrom { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether widgets are created for this member.
        /// </summary>
        bool HideWidgets { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether all instances of this member should be
        /// locked from being edited.
        /// </summary>
        bool IsLocked { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this member is a key to its
        /// structure definition.
        /// </summary>
        bool IsStructureKey { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this member is to be used as a sub item
        /// member to another member that supports children.
        /// </summary>
        bool IsSubMember { get; set; }

        /// <summary>
        /// Gets the location of this member inside the parCodeGen definition data sat on the
        /// local disk.
        /// </summary>
        FileLocation Location { get; }

        /// <summary>
        /// Gets the metadata name for this member. This is the name but without the m_ at the
        /// front of it. This is also the name that this member gets serialised with into a
        /// metadata file.
        /// </summary>
        string MetadataName { get; }

        /// <summary>
        /// Gets or sets the name for this member.
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether PARSER.InitObject() will modify this
        /// member or not.
        /// </summary>
        bool NoInit { get; set; }

        /// <summary>
        /// Gets or sets the priority this member has been given inside its owning structure.
        /// </summary>
        int Priority { get; set; }

        /// <summary>
        /// Gets or sets the structure that this member is associated with.
        /// </summary>
        IStructure Structure { get; set; }

        /// <summary>
        /// Gets the type name for this member.
        /// </summary>
        string TypeName { get; }

        /// <summary>
        /// Gets or sets the call back function used when the widget is changed.
        /// </summary>
        string WidgetChangedCallback { get; set; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new <see cref="IMember"/> that is a copy of this instance.
        /// </summary>
        /// <returns>
        /// A new <see cref="IMember"/> that is a copy of this instance.
        /// </returns>
        new IMember Clone();

        /// <summary>
        /// Creates a <see cref="ITunable"/> object that represents a instance of this member
        /// with it's default value set.
        /// </summary>
        /// <param name="tunableParent">
        /// The tunable parent the new tunable will belong to.
        /// </param>
        /// <returns>
        /// A parCodeGen tunable that represents a instance of this member.
        /// </returns>
        ITunable CreateTunable(ITunableParent tunableParent);

        /// <summary>
        /// Creates a <see cref="ITunable"/> object that represents a instance of this member
        /// using the specified Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The Xml.XmlReader that provides the data used to initialise the new tunable.
        /// </param>
        /// <param name="tunableParent">
        /// The tunable parent the new tunable will belong to.
        /// </param>
        /// <param name="log">
        /// A object that is used to log any warnings or errors produced during the parsing of
        /// the specified reader.
        /// </param>
        /// <returns>
        /// A parCodeGen tunable that represents a instance of this member.
        /// </returns>
        ITunable CreateTunable(XmlReader reader, ITunableParent tunableParent, ILog log);

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="IMember"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        new bool Equals(IMember other);

        /// <summary>
        /// Determines whether this structure can be instanced inside a metadata file depending
        /// on any critical errors found.
        /// </summary>
        /// <param name="log">
        /// The log object that will receives any errors or warning found while validating.
        /// </param>
        /// <returns>
        /// True if this structure can be instanced inside a metadata file; otherwise false. If
        /// false the specified log object will have received the reasons why not.
        /// </returns>
        bool IsValidForInstancing(ILog log);

        /// <summary>
        /// Writes the xml representation of this instance onto the specified
        /// System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that the xml representation of this instance should be
        /// written to.
        /// </param>
        void Serialise(XmlWriter writer);
        #endregion Methods
    } // RSG.Metadata.Model.Definitions.Members.IMember {Interface}
} // RSG.Metadata.Model.Definitions.Members {Namespace}
