﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Dialogue.UndoInterface
{
    /// <summary>
    /// Interface that defines all the undo redo stuff for an object to be able to undo/redo
    /// property changes
    /// </summary>
    public interface IUndoRedo : INotifyPropertyChanging, INotifyPropertyChanged 
    {



    }
}
