﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using RSG.Base.Math;
using RSG.Base.ConfigParser;
using RSG.SceneXml;
using RSG.SceneXml.Material;
using RSG.SceneXml.Statistics;
using RSG.Model.Map.Statistics;
using RSG.Model.Map.Utilities;

namespace RSG.Model.Map
{
    using AttributeContainer = Dictionary<String, Object>;
    using TextureContainer = Dictionary<String, Texture>;
    using IndexedDefinition = Dictionary<String, IMapDefinition>;
    using DefinitionContainer = Dictionary<String, Dictionary<String, IMapDefinition>>;

    /// <summary>
    /// A static utility class used for the definitions
    /// </summary>
    public static class DefinitionLODHierarchy
    {
        #region Static Methods

        /// <summary>
        /// Using the given ObjectDef calculates the lod hierarchy (if there is one) and
        /// adds all the ObjectDefs that delong to the hierarchy into the given list
        /// </summary>
        public static void GetFullDefinitionLodHierarchy(TargetObjectDef sceneObject, ref List<TargetObjectDef> hierarchy)
        {
            if (sceneObject.DrawableLOD != null)
            {
                // Find the top level parent (the very low detail)
                TargetObjectDef lodParent = sceneObject.DrawableLOD.Parent;
                if (lodParent == null)
                {
                    hierarchy.Add(sceneObject);
                }
                else
                {
                    TargetObjectDef topParent = lodParent;
                    while (lodParent != null)
                    {
                        topParent = lodParent;
                        lodParent = lodParent.DrawableLOD.Parent;
                    }
                    hierarchy.Add(topParent);
                }

                // Now go through the children of the top level parent
                TargetObjectDef lodChild = null;
                if (hierarchy[0].DrawableLOD.Children.Length >= 1)
                    lodChild = hierarchy[0].DrawableLOD.Children[0];

                while (lodChild != null)
                {
                    hierarchy.Add(lodChild);
                    if (lodChild.DrawableLOD.Children.Length >= 1)
                        lodChild = lodChild.DrawableLOD.Children[0];
                    else
                        lodChild = null;
                }
            }
            else
            {
                hierarchy.Add(sceneObject);
            }
        }

        #endregion // Static Methods
    }

    /// <summary>
    /// A base interface for all components that come underneath a
    /// map section in the map data
    /// </summary>
    public interface IMapSectionComponent
    {
    }

    /// <summary>
    /// Base interface for a map definition.
    /// </summary>
    public interface IMapDefinition : IMapSectionComponent
    {
        #region Properties

        /// <summary>
        /// The name of the definition, used to
        /// instance it elsewhere in the map data.
        /// </summary>
        String Name { get; }

        /// <summary>
        /// A dictionary of attributes used on the definition.
        /// This is a cc of the attributes on a ObjectDef. This
        /// dictionary is indexed by attributes names found in
        /// RSG.SceneXml.AttrNames
        /// </summary>
        AttributeContainer Attributes { get; }

        /// <summary>
        /// The local bounding box for the definition,
        /// used to create areas that is used for statistics
        /// etc. This is a cc of the bounding box on a ObjectDef
        /// </summary>
        BoundingBox3f LocalBoundingBox { get; }

        /// <summary>
        /// A object that contains all of the statistics
        /// for a single definition
        /// </summary>
        DefinitionStatistics Statistics { get; }

        /// <summary>
        /// The collasped list of textures that
        /// are attached to this definition.
        /// </summary>
        TextureContainer Textures { get; }

        #endregion // Properties
    }

    /// <summary>
    /// A high level object that represents a map definition. A map definition
    /// is defined as a object that gets serialised out to the IDE file for the map
    /// section and is used by the instance data.
    /// </summary>
    public class MapDefinition : IMapDefinition, ISearchable
    {
        #region Constants

        #endregion // Constants

        #region Properties

        /// <summary>
        /// The name of the definition, used to
        /// instance it elsewhere in the map data.
        /// </summary>
        public String Name
        {
            get;
            private set;
        }

        /// <summary>
        /// A dictionary of attributes used on the definition.
        /// This is a cc of the attributes on a ObjectDef. This
        /// dictionary is indexed by attributes names found in
        /// RSG.SceneXml.AttrNames
        /// </summary>
        public AttributeContainer Attributes
        {
            get;
            private set;
        }

        /// <summary>
        /// The local bounding box for the definition,
        /// used to create areas that is used for statistics
        /// etc. This is a cc of the bounding box on a ObjectDef
        /// </summary>
        public BoundingBox3f LocalBoundingBox
        {
            get;
            private set;
        }

        /// <summary>
        /// A object that contains all of the statistics
        /// for a single definition
        /// </summary>
        public DefinitionStatistics Statistics
        {
            get;
            private set;
        }

        /// <summary>
        /// The collasped list of textures that
        /// are attached to this definition.
        /// </summary>
        public TextureContainer Textures
        {
            get;
            private set;
        }

        /// <summary>
        /// When true this represents that this definition is apart
        /// of a drawable lod hierarchy. The name if got from the high
        /// detail definition as are the stats while the lod
        /// distance is from the very low detailed definition
        /// </summary>
        public Boolean HasDrawableLODs
        {
            get;
            private set;
        }

        /// <summary>
        /// The parent of this searchable object
        /// </summary>
        public ISearchable SearchableParent
        {
            get;
            set;
        }

        #endregion // Properties

        #region Constructors

        /// <summary>
        /// Creates a new instance of the map definition
        /// object using the given object def as a base
        /// and the scene to get the other statistics
        /// needed
        /// </summary>
        public MapDefinition(TargetObjectDef sceneObject, Scene scene)
        {
            this.GetNameFromObject(sceneObject);
            this.GetAttributesFromObject(sceneObject);
            float inverseArea = this.GetBoundingBoxFromObject(sceneObject);
            this.GetTexturesFromObject(sceneObject, scene);
            this.GetStatisticsFromObject(sceneObject, scene, inverseArea);
#if false
            // DHM 2011/10/11
            // Remove this error; seems redundant for what we're using this model for now.
            // Eventually we'll move the serialiser to RSG.Model.Map2.
            if (sceneObject.DrawableLOD != null)
            {
                RSG.Base.Logging.Log.Log__Error("Error creating map definition with name {0} from the scene {1}." +
                                                " This has been created without a lod hierarchy in mind but it does have one!", this.Name, scene.Filename);
                this.HasDrawableLODs = true;
            }
            
#endif
        }

        /// <summary>
        /// Creates a new instance of the map definition
        /// object using the given object def lod hierary as a base
        /// and the scene to get the other statistics needed. The
        /// given hierarchy needs to be at least one long and
        /// the very low detail object in index [0]
        /// </summary>
        public MapDefinition(List<TargetObjectDef> lodHierarchy, Scene scene)
        {
            try
            {
                this.HasDrawableLODs = true;
                TargetObjectDef highDefintionObject = null;
                if (lodHierarchy.Count == 1)
                {
                    highDefintionObject = lodHierarchy[0];
                    if (highDefintionObject.DrawableLOD == null)
                        this.HasDrawableLODs = false;
                }
                else
                {
                    highDefintionObject = lodHierarchy[lodHierarchy.Count - 1];
                }
                this.GetNameFromObject(highDefintionObject);
                this.GetAttributesFromObject(highDefintionObject);
                float inverseArea = this.GetBoundingBoxFromObject(highDefintionObject);
                this.GetTexturesFromObject(highDefintionObject, scene);
                this.GetStatisticsFromObject(highDefintionObject, scene, inverseArea);

                if (lodHierarchy[0].Attributes.ContainsKey(AttrNames.OBJ_LOD_DISTANCE))
                {
                    if (this.Attributes.ContainsKey(AttrNames.OBJ_LOD_DISTANCE))
                    {
                        this.Attributes[AttrNames.OBJ_LOD_DISTANCE] = lodHierarchy[0].Attributes[AttrNames.OBJ_LOD_DISTANCE];
                    }
                    else
                    {
                        this.Attributes.Add(AttrNames.OBJ_LOD_DISTANCE, lodHierarchy[0].Attributes[AttrNames.OBJ_LOD_DISTANCE]);
                    }
                }
            }
            catch
            {
                RSG.Base.Logging.Log.Log__Error("Error creating map definition with name {0} and lod hierarchy count {1}", this.Name, lodHierarchy.Count);
            }
        }

        /// <summary>
        /// Creates a definition from a animation proxy and a list of animation
        /// objects
        /// </summary>
        public MapDefinition(TargetObjectDef startAnim, TargetObjectDef endAnim, List<TargetObjectDef> animationObjects, TargetObjectDef proxyAnim, Scene scene)
        {
            this.GetNameFromObject(proxyAnim);
            this.GetAttributesFromObjects(animationObjects, proxyAnim);
            float inverseArea = this.GetBoundingBoxFromObjects(startAnim, endAnim);
            this.GetTexturesFromObject(startAnim, scene);
            this.AddTexturesFromObject(endAnim, scene);
            foreach (TargetObjectDef anim in animationObjects)
            {
                this.AddTexturesFromObject(anim, scene);
            }
            this.GetStatisticsFromObject(startAnim, scene, inverseArea);
            this.AddStatisticsFromObject(endAnim, scene, inverseArea);
            foreach (TargetObjectDef anim in animationObjects)
            {
                this.AddStatisticsFromObject(endAnim, scene, inverseArea);
            }
        }

        public MapDefinition(TargetObjectDef fragmentHead, TargetObjectDef fragmentProxy, Scene scene)
        {
            this.GetNameFromObject(fragmentProxy);
            this.GetAttributesFromObject(fragmentHead);
            float inverseArea = this.GetBoundingBoxFromObject(fragmentHead);
            this.GetTexturesFromObject(fragmentHead, scene);
            this.GetStatisticsFromObject(fragmentHead, scene, inverseArea);
        }

        #endregion // Constructors

        #region Public Methods
        /// <summary>
        /// Read an attribute value, if not present return default value.
        /// </summary>
        /// <param name="name">String attribute name</param>
        /// <param name="defValue">Default value</param>
        /// <returns>Attribute value or default value</returns>
        public T GetAttribute<T>(String name, T defValue)
        {
            if (!this.Attributes.ContainsKey(name.ToLower()))
                return (defValue);

            return ((T)this.Attributes[name.ToLower()]);
        }
        #endregion // Public Methods

        #region Private Methods

        /// <summary>
        /// Gets the name from the given object
        /// </summary>
        private void GetNameFromObject(TargetObjectDef sceneObject)
        {
            this.Name = String.Copy(sceneObject.Name);
        }

        /// <summary>
        /// Gets the attributes from the given object
        /// </summary>
        private void GetAttributesFromObject(TargetObjectDef sceneObject)
        {
            this.Attributes = new AttributeContainer();
            foreach (var attribute in sceneObject.Attributes)
            {
                String attributeName = String.Copy(attribute.Key);
                Object attributeValue = attribute.Value;
                this.Attributes.Add(attributeName, attributeValue);
            }
        }

        /// <summary>
        /// Gets the attributes from the given list of object
        /// </summary>
        private void GetAttributesFromObjects(List<TargetObjectDef> sceneObjects, TargetObjectDef startAnim)
        {
            this.Attributes = new AttributeContainer();
            foreach (var attribute in startAnim.Attributes)
            {
                String attributeName = String.Copy(attribute.Key);
                Object attributeValue = attribute.Value;
                this.Attributes.Add(attributeName, attributeValue);
            }
            float lodDistance = this.Attributes.ContainsKey(AttrNames.OBJ_LOD_DISTANCE) ? (float)this.Attributes[AttrNames.OBJ_LOD_DISTANCE] : float.MinValue;
            foreach (TargetObjectDef anim in sceneObjects)
            {
                float animLodDistance = anim.Attributes.ContainsKey(AttrNames.OBJ_LOD_DISTANCE) ? (float)anim.Attributes[AttrNames.OBJ_LOD_DISTANCE] : float.MinValue;
                lodDistance = animLodDistance > lodDistance ? animLodDistance : lodDistance;
            }
            if (lodDistance != float.MinValue)
            {
                this.Attributes[AttrNames.OBJ_LOD_DISTANCE] = lodDistance;
            }
        }

        /// <summary>
        /// Gets the local bounding box for the given scene object and
        /// returns the inverse area for it. (Default return is 0.0f)
        /// </summary>
        private float GetBoundingBoxFromObject(TargetObjectDef sceneObject)
        {
            if (sceneObject.LocalBoundingBox != null)
            {
                this.LocalBoundingBox = new BoundingBox3f(sceneObject.LocalBoundingBox);
                float sectionWidth = this.LocalBoundingBox.Max.X - this.LocalBoundingBox.Min.X;
                float sectionLength = this.LocalBoundingBox.Max.Y - this.LocalBoundingBox.Min.Y;
                float sectionHeight = this.LocalBoundingBox.Max.Z - this.LocalBoundingBox.Min.Z;
                if (sectionWidth > 0.1f && sectionLength > 0.1f && sectionHeight > 0.1f)
                {
                    return 1.0f / (sectionWidth * sectionLength * sectionHeight);
                }
            }

            return 0.0f;
        }

        /// <summary>
        /// Gets the local bounding box for the given scene object and
        /// returns the inverse area for it. (Default return is 0.0f)
        /// </summary>
        private float GetBoundingBoxFromObjects(TargetObjectDef start, TargetObjectDef end)
        {
            if (start.LocalBoundingBox != null || end.LocalBoundingBox != null)
            {
                if (start.LocalBoundingBox != null)
                {
                    if (this.LocalBoundingBox == null)
                        this.LocalBoundingBox = new BoundingBox3f(start.LocalBoundingBox);
                    else
                        this.LocalBoundingBox.Expand(start.LocalBoundingBox);
                }
                if (end.LocalBoundingBox != null)
                {
                    if (this.LocalBoundingBox == null)
                        this.LocalBoundingBox = new BoundingBox3f(end.LocalBoundingBox);
                    else
                        this.LocalBoundingBox.Expand(end.LocalBoundingBox);
                }
                float sectionWidth = this.LocalBoundingBox.Max.X - this.LocalBoundingBox.Min.X;
                float sectionLength = this.LocalBoundingBox.Max.Y - this.LocalBoundingBox.Min.Y;
                float sectionHeight = this.LocalBoundingBox.Max.Z - this.LocalBoundingBox.Min.Z;
                if (sectionWidth > 0.1f && sectionLength > 0.1f && sectionHeight > 0.1f)
                {
                    return 1.0f / (sectionWidth * sectionLength * sectionHeight);
                }
            }

            return 0.0f;
        }

        /// <summary>
        /// Gets all the textures the are attached to the given object in
        /// all its materials
        /// </summary>
        private void GetTexturesFromObject(TargetObjectDef sceneObject, Scene scene)
        {
            this.Textures = new TextureContainer();
            MaterialDef material = null;
            if (scene.MaterialLookup.TryGetValue(sceneObject.Material, out material))
            {
                this.GetTexturesFromMaterial(material, this.Textures, true);
            }
        }

        /// <summary>
        /// Gets all the textures the are attached to the given object in
        /// all its materials and add them to the already initialised
        /// texture list
        /// </summary>
        private void AddTexturesFromObject(TargetObjectDef sceneObject, Scene scene)
        {
            MaterialDef material = null;
            if (scene.MaterialLookup.TryGetValue(sceneObject.Material, out material))
            {
                this.GetTexturesFromMaterial(material, this.Textures, true);
            }
        }

        /// <summary>
        /// Goes through the material def and adds all the textures into the referenced textures list, if the
        /// parameter recursive is true it will also go through all the sub materials if any exist
        /// </summary>
        private void GetTexturesFromMaterial(MaterialDef material, Dictionary<String, Texture> textures, Boolean recursive)
        {
            if (material.HasTextures)
            {
                for (int i = 0; i < material.Textures.Length; i++)
                {
                    TextureDef texture = material.Textures[i];

                    String filename = texture.FilePath;
                    String name = System.IO.Path.GetFileNameWithoutExtension(texture.FilePath);
                    TextureTypes type = texture.Type;

                    if (type != TextureTypes.None)
                    {
                        Texture newTexture = null;
                        if (type == TextureTypes.DiffuseMap || type == TextureTypes.SpecularMap || type == TextureTypes.BumpMap)
                        {
                            // We need to determine if the next texture is an appropriate alpha texture, if a next texture exists
                            int nextIndex = i + 1;
                            if (nextIndex < material.Textures.Length)
                            {
                                TextureDef nextTexture = material.Textures[i + 1];
                                if (type == TextureTypes.DiffuseMap && nextTexture.Type == TextureTypes.DiffuseAlpha)
                                {
                                    newTexture = new Texture(filename, nextTexture.FilePath, type);
                                    newTexture.SearchableParent = this;
                                    if (!textures.ContainsKey(newTexture.StreamName))
                                        textures.Add(newTexture.StreamName, newTexture);
                                    i++;
                                    continue;
                                }
                                else if (type == TextureTypes.SpecularMap && nextTexture.Type == TextureTypes.SpecularAlpha)
                                {
                                    newTexture = new Texture(filename, nextTexture.FilePath, type);
                                    newTexture.SearchableParent = this;
                                    if (!textures.ContainsKey(newTexture.StreamName))
                                        textures.Add(newTexture.StreamName, newTexture);
                                    i++;
                                    continue;
                                }
                                else if (type == TextureTypes.BumpMap && nextTexture.Type == TextureTypes.BumpAlpha)
                                {
                                    newTexture = new Texture(filename, nextTexture.FilePath, type);
                                    newTexture.SearchableParent = this;
                                    if (!textures.ContainsKey(newTexture.StreamName))
                                        textures.Add(newTexture.StreamName, newTexture);
                                    i++;
                                    continue;
                                }
                            }
                        }

                        // Shouldn't add any alpha textures as a indiviual texture, alpha textures need to go in pairs with another texture
                        if (type == TextureTypes.DiffuseAlpha || type == TextureTypes.BumpAlpha || type == TextureTypes.SpecularAlpha)
                        {
                            continue;
                        }

                        newTexture = new Texture(filename, type);
                        newTexture.SearchableParent = this;
                        if (!textures.ContainsKey(newTexture.StreamName))
                            textures.Add(newTexture.StreamName, newTexture);
                    }
                }
            }

            if (material.HasSubMaterials)
            {
                foreach (MaterialDef child in material.SubMaterials)
                {
                    GetTexturesFromMaterial(child, textures, true);
                }
            }
        }

        /// <summary>
        /// Gets all the statistics for this given object and creates
        /// a dictionary for them
        /// </summary>
        private void GetStatisticsFromObject(TargetObjectDef sceneObject, Scene scene, float inverseArea)
        {
            this.Statistics = new DefinitionStatistics();

            this.GetGeometryStatistics(sceneObject, scene, inverseArea);
            this.GetCollisionStatistics(sceneObject, scene, inverseArea);

            foreach (TargetObjectDef child in sceneObject.Children)
            {
                if (child.IsObject() && child.DontExport() == false)
                {
                    AddStatisticsFromObject(child, scene, inverseArea);
                }
            }
        }

        /// <summary>
        /// Gets all the statistics for this given object and creates
        /// a dictionary for them
        /// </summary>
        private void AddStatisticsFromObject(TargetObjectDef sceneObject, Scene scene, float inverseArea)
        {
            this.GetGeometryStatistics(sceneObject, scene, inverseArea);
            this.GetCollisionStatistics(sceneObject, scene, inverseArea);

            foreach (TargetObjectDef child in sceneObject.Children)
            {
                if (child.IsObject() && child.DontExport() == false)
                {
                    AddStatisticsFromObject(child, scene, inverseArea);
                }
            }
        }
        
        /// <summary>
        /// Gets all the geometry statistics for a scene object
        /// </summary>
        private void GetGeometryStatistics(TargetObjectDef sceneObject, Scene scene, float inverseArea)
        {
            int polycount = sceneObject.PolyCount;
            DrawableStats geometryStat = null;
            scene.GeometryStatsLookup.TryGetValue(sceneObject.Guid, out geometryStat);
            if (geometryStat != null)
            {
                this.Statistics.AddGeometryStatistics((int)geometryStat.Size, polycount, inverseArea);
            }
            else
            {
                this.Statistics.AddGeometryStatistics(0, polycount, inverseArea);
            }

            TxdStats txdStat = null;
            if (sceneObject.Attributes.ContainsKey(AttrNames.OBJ_TXD))
            {
                if (scene.TxdStatsLookup.ContainsKey(sceneObject.Attributes[AttrNames.OBJ_TXD] as String))
                {
                    scene.TxdStatsLookup.TryGetValue(sceneObject.Attributes[AttrNames.OBJ_TXD] as String, out txdStat);
                    if (txdStat != null)
                    {
                        this.Statistics.AddTXDStatistics((int)txdStat.Size);
                    }
                    else
                    {
                        this.Statistics.AddTXDStatistics(0);
                    }
                }
            }
            else
            {
                this.Statistics.AddTXDStatistics(0);
            }
        }

        /// <summary>
        /// Gets all the collision statistics for a scene object by going through its children
        /// and looking at the onces that are collision
        /// </summary>
        private void GetCollisionStatistics(TargetObjectDef sceneObject, Scene scene, float inverseArea)
        {
            int count = 0;
            foreach (TargetObjectDef child in sceneObject.Children)
            {
                if (child.DontExport())
                    continue;
                if (child.IsDummy())
                    continue;

                if (child.IsCollision())
                {
                    count += child.PolyCount;
                }
            }
            this.Statistics.AddCollisionStatistics(count, inverseArea);
        }

        #endregion // Private Methods

        #region Static Functions

        public static MapDefinition CreateFragmentDefinition(TargetObjectDef fragment, Scene scene)
        {
            MapDefinition newDefinition = new MapDefinition(fragment, scene);
            newDefinition.Name += "_frag_";
            return newDefinition;
        }

        #endregion // Static Functions

        #region Walk Properties

        /// <summary>
        /// Walks through the children of ISearchables, going
        /// depth first
        /// </summary>
        public IEnumerable<ISearchable> WalkSearchDepth
        {
            get
            {
                yield return this;
                if (this.Textures != null)
                {
                    List<Texture> textures = new List<Texture>(this.Textures.Values);
                    foreach (ISearchable searchable in textures.Where(t => t is ISearchable == true))
                    {
                        foreach (ISearchable searchableChld in searchable.WalkSearchDepth)
                        {
                            yield return searchableChld;
                        }
                    }
                }
            }
        }

        #endregion // Walk Properties

    } // MapDefinition

    /// <summary>
    /// A wrapper class around a collection of definitions
    /// first indexed by filename and then by definition name.
    /// </summary>
    public class DefinitionList
    {
        #region Properties

        /// <summary>
        /// The main container that sorts the definitions indexed
        /// by filename first and then name, kept private so that
        /// other objects cannot directly manipulate it.
        /// </summary>
        private DefinitionContainer Definitions
        {
            get;
            set;
        }

        /// <summary>
        /// A simple list that holds the names of all the definitions
        /// in the map data. This list is used to make sure that
        /// no definitions are in the data twice.
        /// </summary>
        public Dictionary<String, List<String>> DefinitionNames
        {
            get;
            set;
        }

        #endregion // Properties

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        public DefinitionList()
        {
            this.Definitions = new DefinitionContainer();
            this.DefinitionNames = new Dictionary<String, List<String>>();
        }

        #endregion // Constructors

        #region Public Methods

        /// <summary>
        /// Adds a new definition to the list, this makes sure that the indexing stays
        /// unique
        /// </summary>
        public void AddDefinition(String filename, String name, IMapDefinition definition)
        {
            String path = System.IO.Path.GetFullPath(filename);
            if (!this.Definitions.ContainsKey(path))
            {
                this.Definitions.Add(path, new IndexedDefinition());
            }
            if (!this.Definitions[path].ContainsKey(name.ToLower()))
            {
                this.Definitions[path].Add(name.ToLower(), definition);
            }

            if (!this.DefinitionNames.ContainsKey(name.ToLower()))
            {
                this.DefinitionNames.Add(name.ToLower(), new List<String>());
            }
            this.DefinitionNames[name.ToLower()].Add(System.IO.Path.GetFileNameWithoutExtension(path));
        }

        public Dictionary<String, KeyValuePair<String, List<String>>> Missing = new Dictionary<String, KeyValuePair<String, List<String>>>();

        /// <summary>
        /// Gets the definition that has the given indexers. If the definition
        /// is thought to not exist this returns null.
        /// </summary>
        public IMapDefinition GetDefinition(String filename, String name, String objectName, String sceneFilename)
        {
            if (filename != null && name != null)
            {
                String path = System.IO.Path.GetFullPath(filename);
                if (this.Definitions.ContainsKey(path))
                {
                    if (this.Definitions[path].ContainsKey(name.ToLower()))
                    {
                        return this.Definitions[path][name.ToLower()];
                    }
                }
            }
            else
            {
                if (name != null)
                {
                    foreach (IndexedDefinition definitions in this.Definitions.Values)
                    {
                        foreach (KeyValuePair<String, IMapDefinition> definition in definitions)
                        {
                            if (String.Compare(definition.Key, name, true) == 0)
                            {
                                RSG.Base.Logging.Log.Log__Warning("The object {0} in {1} has incomplete reference information but the definition was found", objectName, sceneFilename);
                                return definition.Value;
                            }
                        }
                    }

                    if (!sceneFilename.Contains("testbed_int01") && !sceneFilename.Contains("test_02"))
                    {
                        if (!Missing.ContainsKey(name.ToLower()))
                            Missing.Add(name.ToLower(), new KeyValuePair<String, List<String>>(filename, new List<String>()));

                        Missing[name.ToLower()].Value.Add(System.IO.Path.GetFileNameWithoutExtension(sceneFilename));
                    }
                    
                    return null;
                }
            }
            if (!sceneFilename.Contains("testbed_int01") && !sceneFilename.Contains("test_02"))
            {
                if (!Missing.ContainsKey(name.ToLower()))
                    Missing.Add(name.ToLower(), new KeyValuePair<String, List<String>>(filename, new List<String>()));
            }

            if (Missing.ContainsKey(name.ToLower()))
                Missing[name.ToLower()].Value.Add(System.IO.Path.GetFileNameWithoutExtension(sceneFilename));
            return null;
        }

        #endregion // Public Methods
    } // DefinitionList

} // RSG.Model.Map
