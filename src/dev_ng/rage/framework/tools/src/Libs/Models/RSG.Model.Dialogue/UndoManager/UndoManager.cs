﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using RSG.Model.Dialogue.UndoInterface;
using RSG.Base.Collections;

namespace RSG.Model.Dialogue.UndoManager
{
    public class UndoManagerObject
    {
        #region Properties

        /// <summary>
        /// The actual stack with the undo objects in it.
        /// </summary>
        public ObservableStack<UndoStackObject> UndoStack
        {
            get { return m_undoStack; }
            set { m_undoStack = value; }
        }
        private ObservableStack<UndoStackObject> m_undoStack;

        /// <summary>
        /// The actual stack with the redo objects in it.
        /// </summary>
        public ObservableStack<UndoStackObject> RedoStack
        {
            get { return m_redoStack; }
            set { m_redoStack = value; }
        }
        private ObservableStack<UndoStackObject> m_redoStack;

        /// <summary>
        /// The temp stack with the undo objects in it that are
        /// to go into a composite undo object as the children.
        /// </summary>
        private Stack<SingleUndoStackObject> m_tempUndoStack;

        /// <summary>
        /// A collection of UndoRedoBase objects that this undo manager is listening to.
        /// </summary>
        private Collection<UndoRedoBase> m_registeredViewModels;

        /// <summary>
        /// If this is set to true it means that any undos created will
        /// need to go into one composite one so that only one undo action undos
        /// them all.
        /// </summary>
        private Boolean m_doingCompositeUndo;

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// Default constructor, just initialises the properties.
        /// </summary>
        public UndoManagerObject()
        {
            this.m_undoStack = new ObservableStack<UndoStackObject>();
            this.m_redoStack = new ObservableStack<UndoStackObject>();
            this.m_tempUndoStack = new Stack<SingleUndoStackObject>();
            this.m_registeredViewModels = new Collection<UndoRedoBase>();
            this.m_doingCompositeUndo = false;
        }

        #endregion // Constructor(s)

        #region Public Function(s)

        /// <summary>
        /// Starts a composite undo sequence. This means that any undo object created between now
        /// and the end composite function being called will be put in a single stack object
        /// </summary>
        public void StartCompositeUndo()
        {
            m_doingCompositeUndo = true;
            this.m_tempUndoStack.Clear();
        }

        /// <summary>
        /// Ends the composite undo phase and creates the single stack object with the 
        /// temp single stack objects
        /// </summary>
        public void EndCompositeUndo()
        {
            m_doingCompositeUndo = false;

            // Create the composite object based on the temp undo stack objects
            CompositeUndoStackObject newCompositeStackObject = new CompositeUndoStackObject();

            foreach (SingleUndoStackObject obj in this.m_tempUndoStack.Reverse())
            {
                newCompositeStackObject.Children.Push(obj);
            }

            m_undoStack.Push(newCompositeStackObject);
            this.m_tempUndoStack.Clear();
        }

        #endregion // Public Function(s)

        #region Private Function(s)

        /// <summary>
        /// Register a view model with this undo manager, meaning that 
        /// any undo property change will be managed here.
        /// </summary>
        /// <param name="viewModel"></param>
        public void RegisterViewModel(Object viewModel)
        {
            if (viewModel is UndoRedoBase)
            {
                m_registeredViewModels.Add((UndoRedoBase)viewModel);
                (viewModel as UndoRedoBase).UndoCreating += new UndoEventHandler(UndoManagerObject_UndoCreating);
                (viewModel as UndoRedoBase).PerformUndoAction += new UndoEventHandler(UndoManagerObject_PerformUndoAction);
                (viewModel as UndoRedoBase).PerformRedoAction += new UndoEventHandler(UndoManagerObject_PerformRedoAction);
            }
        }

        /// <summary>
        /// Unregisters the given view model with this undo manager so that 
        /// the undo events of the view model will no longer be managed here.
        /// </summary>
        /// <param name="viewModel"></param>
        public void UnRegisterViewModel(Object viewModel)
        {
            if (viewModel is UndoRedoBase)
            {
                m_registeredViewModels.Remove((UndoRedoBase)viewModel);
                (viewModel as UndoRedoBase).UndoCreating -= this.UndoManagerObject_UndoCreating;
                (viewModel as UndoRedoBase).PerformUndoAction -= UndoManagerObject_PerformUndoAction;
                (viewModel as UndoRedoBase).PerformRedoAction -= this.UndoManagerObject_PerformRedoAction;
            }
        }

        /// <summary>
        /// Creates an undo object based on the given arguments and puts it on the stack.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void UndoManagerObject_UndoCreating(Object sender, UndoEventArgs args)
        {
            if (args.Collapsable && m_undoStack.Count > 0)
            {
                // See whether the last change is valid to collapse this new change into it,
                // it needs to be the same property from the same object
                UndoStackObject lastChange = m_undoStack.Peek();
                if (lastChange is SingleUndoStackObject)
                {
                    SingleUndoStackObject lastSingleChange = (lastChange as SingleUndoStackObject);

                    var lastMethod = lastSingleChange.CallingMethod;
                    var lastPropertyInfo = lastMethod.Member as PropertyInfo;

                    var newMethod = args.CallingMethod;
                    var newPropertyInfo = newMethod.Member as PropertyInfo;

                    if (lastPropertyInfo == newPropertyInfo && sender == lastSingleChange.Sender)
                    {
                        lastSingleChange.NewValue = args.NewValue;
                    }
                    else
                    {
                        if (this.m_doingCompositeUndo == true)
                        {
                            SingleUndoStackObject newUndoObject = new SingleUndoStackObject(sender, args.CallingMethod, args.Setter, args.OldValue, args.NewValue);
                            this.m_tempUndoStack.Push(newUndoObject);
                        }
                        else
                        {
                            SingleUndoStackObject newUndoObject = new SingleUndoStackObject(sender, args.CallingMethod, args.Setter, args.OldValue, args.NewValue);
                            m_undoStack.Push(newUndoObject);
                        } 
                    }
                }
            }
            else
            {
                if (this.m_doingCompositeUndo == true)
                {
                    SingleUndoStackObject newUndoObject = new SingleUndoStackObject(sender, args.CallingMethod, args.Setter, args.OldValue, args.NewValue);
                    this.m_tempUndoStack.Push(newUndoObject);
                }
                else
                {
                    SingleUndoStackObject newUndoObject = new SingleUndoStackObject(sender, args.CallingMethod, args.Setter, args.OldValue, args.NewValue);
                    m_undoStack.Push(newUndoObject);
                }
            }
        }

        /// <summary>
        /// Performs a single undo action by popping the last change from the undo stack and
        /// calling the setter function on it.It also makes sure that the correct property
        /// change events are performed by using the sender peroperty.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void UndoManagerObject_PerformUndoAction(Object sender, UndoEventArgs args)
        {
            if (this.UndoStack.Count > 0 && sender is UndoRedoBase)
            {
                UndoStackObject lastChange = this.UndoStack.Pop();

                if (lastChange is SingleUndoStackObject)
                {
                    SingleUndoStackObject lastSingleChange = (lastChange as SingleUndoStackObject);

                    // Need to make sure that the correct events are fired for this change
                    var method = lastSingleChange.CallingMethod;
                    var propertyInfo = method.Member as PropertyInfo;
                    String propertyName = propertyInfo.Name;

                    (lastSingleChange.Sender as UndoRedoBase).OnPropertyChanging(propertyName);
                    lastSingleChange.Setter(lastSingleChange.OldValue);
                    (lastSingleChange.Sender as UndoRedoBase).OnPropertyChanged(propertyName);

                    // Need to create the redo stack object
                    Object temp = lastSingleChange.NewValue;
                    lastSingleChange.NewValue = lastSingleChange.OldValue;
                    lastSingleChange.OldValue = temp;

                    this.m_redoStack.Push(lastSingleChange);
                }
                else if (lastChange is CompositeUndoStackObject)
                {
                    this.m_tempUndoStack.Clear();
                    // Since this is a composite we have to undo all of the children in this object
                    foreach (SingleUndoStackObject undoObject in (lastChange as CompositeUndoStackObject).Children)
                    {
                        SingleUndoStackObject currentChange = (undoObject as SingleUndoStackObject);

                        // Need to make sure that the correct events are fired for this change
                        var method = currentChange.CallingMethod;
                        var propertyInfo = method.Member as PropertyInfo;
                        String propertyName = propertyInfo.Name;

                        (currentChange.Sender as UndoRedoBase).OnPropertyChanging(propertyName);
                        currentChange.Setter(currentChange.OldValue);
                        (currentChange.Sender as UndoRedoBase).OnPropertyChanged(propertyName);

                        // Need to create the redo stack object
                        Object temp = currentChange.NewValue;
                        currentChange.NewValue = currentChange.OldValue;
                        currentChange.OldValue = temp;

                        this.m_tempUndoStack.Push(currentChange);
                    }

                    CompositeUndoStackObject newCompositeStackObject = new CompositeUndoStackObject();
                    foreach (SingleUndoStackObject obj in this.m_tempUndoStack.Reverse())
                    {
                        newCompositeStackObject.Children.Push(obj);
                    }
                    m_redoStack.Push(newCompositeStackObject);
                }
            }
        }

        /// <summary>
        /// Performs a single redo action by popping the last change from the redo stack and
        /// calling the setter function on it.It also makes sure that the correct property
        /// change events are performed by using the sender peroperty.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void UndoManagerObject_PerformRedoAction(Object sender, UndoEventArgs args)
        {
            if (this.RedoStack.Count > 0 && sender is UndoRedoBase)
            {
                UndoStackObject lastChange = this.RedoStack.Pop();

                if (lastChange is SingleUndoStackObject)
                {
                    SingleUndoStackObject lastSingleChange = (lastChange as SingleUndoStackObject);

                    // Need to make sure that the correct events are fired for this change
                    var method = lastSingleChange.CallingMethod;
                    var propertyInfo = method.Member as PropertyInfo;
                    String propertyName = propertyInfo.Name;

                    (lastSingleChange.Sender as UndoRedoBase).OnPropertyChanging(propertyName);
                    lastSingleChange.Setter(lastSingleChange.OldValue);
                    (lastSingleChange.Sender as UndoRedoBase).OnPropertyChanged(propertyName);

                    // Need to create the undo stack object
                    Object temp = lastSingleChange.NewValue;
                    lastSingleChange.NewValue = lastSingleChange.OldValue;
                    lastSingleChange.OldValue = temp;

                    this.m_undoStack.Push(lastSingleChange);
                }
                else if (lastChange is CompositeUndoStackObject)
                {
                    this.m_tempUndoStack.Clear();

                    // Since this is a composite we have to undo all of the children in this object
                    foreach (SingleUndoStackObject undoObject in (lastChange as CompositeUndoStackObject).Children)
                    {
                        SingleUndoStackObject currentChange = (undoObject as SingleUndoStackObject);

                        // Need to make sure that the correct events are fired for this change
                        var method = currentChange.CallingMethod;
                        var propertyInfo = method.Member as PropertyInfo;
                        String propertyName = propertyInfo.Name;

                        (currentChange.Sender as UndoRedoBase).OnPropertyChanging(propertyName);
                        currentChange.Setter(currentChange.OldValue);
                        (currentChange.Sender as UndoRedoBase).OnPropertyChanged(propertyName);

                        // Need to create the redo stack object
                        Object temp = currentChange.NewValue;
                        currentChange.NewValue = currentChange.OldValue;
                        currentChange.OldValue = temp;

                        this.m_tempUndoStack.Push(currentChange);
                    }

                    CompositeUndoStackObject newCompositeStackObject = new CompositeUndoStackObject();
                    foreach (SingleUndoStackObject obj in this.m_tempUndoStack.Reverse())
                    {
                        newCompositeStackObject.Children.Push(obj);
                    }
                    m_undoStack.Push(newCompositeStackObject);
                }
            }
        }

        #endregion // Private Function(s)

    } // UndoManager
} // RSG.Model.Dialogue.UndoManager
