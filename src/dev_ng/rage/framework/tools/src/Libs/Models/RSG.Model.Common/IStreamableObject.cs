﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common
{
    /// <summary>
    /// Interface for all streamable stat objects.
    /// </summary>
    /// <typeparam name="TEnum">Enum type</typeparam>
    public interface IStreamableObject
    {
        #region Methods
        /// <summary>
        /// 
        /// </summary>
        void LoadAllStats();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="statsToLoad"></param>
        void LoadStats(IEnumerable<StreamableStat> statsToLoad);

        /// <summary>
        /// Request all the statistics for this object.
        /// </summary>
        /// <param name="async"></param>
        [Obsolete("Use the LoadAllStats() method instead, optionally wrapping it in a ITask.")]
        void RequestAllStatistics(bool async);

        /// <summary>
        /// Request specific stats for this object.
        /// </summary>
        /// <param name="statsToLoad"></param>
        /// <param name="async"></param>
        [Obsolete("Use the LoadStats() method instead, optionally wrapping it in a ITask.")]
        void RequestStatistics(IEnumerable<StreamableStat> statsToLoad, bool async);

        /// <summary>
        /// Returns whether or not a particular stat has been loaded for this object.
        /// </summary>
        /// <param name="stat"></param>
        /// <returns></returns>
        bool IsStatLoaded(StreamableStat stat);

        /// <summary>
        /// Returns whether a list of stats are loaded for this object.
        /// </summary>
        /// <param name="requestedStats"></param>
        /// <returns></returns>
        bool AreStatsLoaded(IEnumerable<StreamableStat> requestedStats);
        #endregion // Methods
    } // IStreamableObject<T>
}
