﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Map.SceneOverride
{
    /// <summary>
    /// A full attribute override record with foreign keys to the processed content and user 'tables'
    /// </summary>
    internal class AttributeOverrideRecord
    {
        internal AttributeOverrideRecord(int contentId, uint hash, uint guid,
                                         String modelName, float posX, float posY, float posZ, 
                                         int userId, 
                                         bool dontCastShadows, bool dontRenderInShadows,
                                         bool dontRenderInReflections, bool onlyRenderInReflections,
                                         bool dontRenderInWaterReflections, bool onlyRenderInWaterReflections,
                                         bool dontRenderInMirrorReflections, bool onlyRenderInMirrorReflections, 
                                         bool streamingPriorityLow, int priority, 
                                         long utcFileTimeSubmitted)
        {
            ContentId = contentId;
            Hash = hash;
            Guid = guid;
            ModelName = modelName;
            PosX = posX;
            PosY = posY;
            PosZ = posZ;
            DontCastShadows = dontCastShadows;
            DontRenderInShadows = dontRenderInShadows;
            DontRenderInReflections = dontRenderInReflections;
            OnlyRenderInReflections = onlyRenderInReflections;
            DontRenderInWaterReflections = dontRenderInWaterReflections;
            OnlyRenderInWaterReflections = onlyRenderInWaterReflections;
            DontRenderInMirrorReflections = dontRenderInMirrorReflections;
            OnlyRenderInMirrorReflections = onlyRenderInMirrorReflections;
            StreamingPriorityLow = streamingPriorityLow;
            Priority = priority;
            UserId = userId;
            UtcFileTimeSubmitted = utcFileTimeSubmitted;
        }

        internal int ContentId { get; private set; }
        internal uint Hash { get; private set; }
        internal uint Guid { get; private set; }
        internal String ModelName { get; private set; }
        internal float PosX { get; set; }
        internal float PosY { get; set; }
        internal float PosZ { get; set; }
        internal bool DontCastShadows { get; private set; }
        internal bool DontRenderInShadows { get; private set; }
        internal bool DontRenderInReflections { get; private set; }
        internal bool OnlyRenderInReflections { get; private set; }
        internal bool DontRenderInWaterReflections { get; private set; }
        internal bool OnlyRenderInWaterReflections { get; private set; }
        internal bool DontRenderInMirrorReflections { get; private set; }
        internal bool OnlyRenderInMirrorReflections { get; private set; }
        internal bool StreamingPriorityLow { get; private set; }
        internal int Priority { get; private set; }
        internal int UserId { get; set; }
        internal long UtcFileTimeSubmitted { get; set; }
    }
}
