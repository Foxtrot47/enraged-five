﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using RSG.Model.Common;
using RSG.Base.Math;
using RSG.Base.Tasks;
using RSG.Base.Configuration;
using RSG.SourceControl.Perforce;
using RSG.Pipeline.Services.InstancePlacement;
using P4API;

namespace RSG.Model.Report.Reports.Pipeline
{
    public class InstancePlacementReport : CSVReport, IDynamicReport, IReportFileProvider
    {
        #region Constants
        private const String c_name = "Instance Placement Processor Report";
        private const String c_description = "Gathers all the exported stats from the instance placement processor and breaks down the stats for the entire map.";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Task to run for generating this report.
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (m_task == null)
                {
                    CompositeTask task = new CompositeTask("Generate Report");
                    task.AddSubTask(new ActionTask("Syncing data", (context, progress) => SynchronizeData((DynamicReportContext)context, progress)));
                    task.AddSubTask(new ActionTask("Loading data", (context, progress) => EnsureDataLoaded((DynamicReportContext)context, progress)));
                    task.AddSubTask(new ActionTask("Generating report", (context, progress) => GenerateReport((DynamicReportContext)context, progress)));
                    m_task = task;
                }
                return m_task;
            }
        }
        private ITask m_task;

        public bool GenerateWorldStats
        {
            get;
            set;
        }
        #endregion

        #region Member Data
        private IConfig Config;
        private Dictionary<String, List<InstancePlacementContainerStat>> PlacementStatData;
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public InstancePlacementReport()
            : base(c_name, c_description)
        {
            Config = ConfigFactory.CreateConfig();
            PlacementStatData = new Dictionary<string, List<InstancePlacementContainerStat>>();
        }
        #endregion // Constructor(s)

        #region Task Methods
        /// <summary>
        /// Sync all the current single reports that are checked into perforce.
        /// Each container has its own stats so make sure we're up to date on all of them.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="progress"></param>
        private void SynchronizeData(ITaskContext context, IProgress<TaskProgress> progress)
        {
            List<String> syncArguments = new List<String>();
            syncArguments.Add(Path.Combine(Config.Project.DefaultBranch.Assets, "reports/InstancePlacement", "..."));

            // Sync up to the most recent stats that are checked in.
            string message = String.Format("Syncing updated instance placement reports ...");
            progress.Report(new TaskProgress(0.0f, true, message));

            using (P4 perforceConnection = new P4())
            {
                perforceConnection.Connect();
                perforceConnection.Run("sync", syncArguments.ToArray());
            }
        }

        /// <summary>
        /// Go through and load every stat file for the entire map and store the data to generate our report from.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="progress"></param>
        private void EnsureDataLoaded(ITaskContext context, IProgress<TaskProgress> progress)
        {
            string reportDirectory = Config.CoreProject.DefaultBranch.Environment.Subst("$(assets)/reports/InstancePlacement");
            string[] regionDirectories = Directory.GetDirectories(reportDirectory);

            Dictionary<string, List<string>> allStatsFiles = new Dictionary<string, List<string>>();

            int statFileCount = 0;

            // Cache the stats filenames by region and count them for progress count.
            foreach (string regionDir in regionDirectories)
            {
                string[] regionSplit = regionDir.Split(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                string region = regionSplit[regionSplit.Length - 1];

                string[] statsFileEntries = Directory.GetFiles(regionDir, "*_stats.xml");
                allStatsFiles.Add(region, new List<string>());

                foreach (string statsFile in statsFileEntries)
                {
                    allStatsFiles[region].Add(statsFile);
                    statFileCount++;
                }
            }

            // Load the actual stats files.
            foreach (KeyValuePair<string, List<string>> kvp in allStatsFiles)
            {
                foreach(string statFilename in kvp.Value)
                {
                    float increment = 1.0f / statFileCount;
                    string message = String.Format("Gathering stats for: {0}", statFilename);
                    progress.Report(new TaskProgress(increment, true, message));

                    LoadIPStatsFile(kvp.Key, statFilename);
                }
            }
        }

        /// <summary>
        /// This will load a specific instance placement stat file and cache all the relevant data.
        /// </summary>
        /// <param name="region"></param>
        /// <param name="container"></param>
        /// <param name="filename"></param>
        private void LoadIPStatsFile(string region, string filename)
        {
            InstancePlacementContainerStat containerStat = InstancePlacementStatsUtil.LoadContainerStatsFile(filename);

            if (!PlacementStatData.ContainsKey(region))
                PlacementStatData.Add(region, new List<InstancePlacementContainerStat>());

            PlacementStatData[region].Add(containerStat);
        }

        /// <summary>
        /// Generate the actual report.
        /// </summary>
        /// <param name="context"></param>
        private void GenerateReport(ITaskContext context, IProgress<TaskProgress> progress)
        {
            // Write out the specific report we want.
            if (GenerateWorldStats)
                WriteWorldBasedReport();
            else
                WriteRegionBasedReport();
        }

        /// <summary>
        /// The regular ToString() built into the Vector3f class add comma's which screw up the formatting.
        /// Rework it slightly so we have different formatting.
        /// </summary>
        /// <param name="vector"></param>
        /// <returns></returns>
        private string GetVectorString(Vector3f vector)
        {
            return String.Format("({0} {1} {2})", vector.X, vector.Y, vector.Z);
        }

        /// <summary>
        /// Write the region based report which will break down the world by region/continer
        /// and output all the stats for each area.
        /// </summary>
        private void WriteRegionBasedReport()
        {
            using (StreamWriter writer = new StreamWriter(this.Filename))
            {
                writer.Write("Archetype, Batch Count, Total Instance Count, Min Instance Count, Max Instance Count, Average Instance Count, ");
                writer.Write("Min Occupancy, Max Occupancy, Average Occupancy, Min Volume (m3), Max Volume (m3), Average Volume (m3), ");
                writer.Write("Min Bound Size, Max Bound Size, Average Bound Size\n");

                // Working at a region level.
                foreach (KeyValuePair<String, List<InstancePlacementContainerStat>> kvp in PlacementStatData)
                {
                    string region = kvp.Key;
                    List<InstancePlacementContainerStat> containerList = kvp.Value;

                    writer.WriteLine("\n" + region);

                    // Working at a container level.
                    foreach (InstancePlacementContainerStat containerStat in containerList)
                    {
                        writer.WriteLine("\n" + containerStat.ContainerName + "\n");

                        // Working at an archetype level.
                        foreach (KeyValuePair<String, List<InstancePlacementArchetypeStat>> containerKvp in containerStat.ContainerStats)
                        {
                            string archetype = containerKvp.Key;

                            // Create our single archetype stat from the various batches.
                            InstancePlacementArchetypeStat archetypeStat = new InstancePlacementArchetypeStat(archetype, containerKvp.Value);

                            writer.WriteLine(String.Format("{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}", archetype, archetypeStat.TotalBatchCount,
                                archetypeStat.TotalInstanceCount, archetypeStat.MinInstances, archetypeStat.MaxInstances, archetypeStat.AverageInstances,
                                archetypeStat.MinOccupancy, archetypeStat.MaxOccupancy, archetypeStat.AverageOccupancy,
                                archetypeStat.MinVolume, archetypeStat.MaxVolume, archetypeStat.AverageVolume,
                                GetVectorString(archetypeStat.MinBoundSize), GetVectorString(archetypeStat.MaxBoundSize), GetVectorString(archetypeStat.AverageBoundSize)));
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Write out a full world report which is basically a listing of each archetype for the entire
        /// world and how many times it is used along with min/max/average.
        /// </summary>
        private void WriteWorldBasedReport()
        {
            using (StreamWriter writer = new StreamWriter(this.Filename))
            {
                writer.Write("Archetype, Batch Count, Total Instance Count, Min Instance Count, Max Instance Count, Average Instance Count, ");
                writer.Write("Min Occupancy, Max Occupancy, Average Occupancy, Min Volume (m3), Max Volume (m3), Average Volume (m3), ");
                writer.Write("Min Bound Size, Max Bound Size, Average Bound Size\n");

                Dictionary<String, InstancePlacementArchetypeStat> worldStats = new Dictionary<string, InstancePlacementArchetypeStat>();

                // Gather all the region/container data and break it down into an archetype level.
                foreach (KeyValuePair<String, List<InstancePlacementContainerStat>> kvp in PlacementStatData)
                {
                    List<InstancePlacementContainerStat> containerList = kvp.Value;

                    foreach (InstancePlacementContainerStat containerStat in containerList)
                    {
                        foreach (KeyValuePair<String, List<InstancePlacementArchetypeStat>> containerKvp in containerStat.ContainerStats)
                        {
                            string archetype = containerKvp.Key;

                            if (!worldStats.ContainsKey(archetype))
                                worldStats.Add(archetype, new InstancePlacementArchetypeStat(archetype));

                            worldStats[archetype].AddInstanceBatches(containerKvp.Value);
                        }
                    }
                }

                // Write out the stats per archetype.
                foreach (KeyValuePair<String, InstancePlacementArchetypeStat> kvp in worldStats)
                {
                    string archetypeName = kvp.Key;
                    InstancePlacementArchetypeStat stat = kvp.Value;

                    writer.WriteLine(String.Format("{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}", archetypeName, stat.TotalBatchCount,
                                stat.TotalInstanceCount, stat.MinInstances, stat.MaxInstances, stat.AverageInstances,
                                stat.MinOccupancy, stat.MaxOccupancy, stat.AverageOccupancy,
                                stat.MinVolume, stat.MaxVolume, stat.AverageVolume,
                                GetVectorString(stat.MinBoundSize), GetVectorString(stat.MaxBoundSize), GetVectorString(stat.AverageBoundSize)));
                }
            }
        }
        #endregion // Task Methods
    }
}
