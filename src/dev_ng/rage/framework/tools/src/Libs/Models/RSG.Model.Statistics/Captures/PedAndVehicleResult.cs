﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Xml.Linq;

namespace RSG.Model.Statistics.Captures
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class PedAndVehicleResult
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public PedAndVehicleStat NumPeds { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public PedAndVehicleStat NumActivePeds { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public PedAndVehicleStat NumInactivePeds { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public PedAndVehicleStat NumEventScanHiPeds { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public PedAndVehicleStat NumEventScanLoPeds { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public PedAndVehicleStat NumMotionTaskHiPeds { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public PedAndVehicleStat NumMotionTaskLoPeds { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public PedAndVehicleStat NumPhysicsHiPeds { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public PedAndVehicleStat NumPhysicsLoPeds { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public PedAndVehicleStat NumEntityScanHiPeds { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public PedAndVehicleStat NumEntityScanLoPeds { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public PedAndVehicleStat NumVehicles { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public PedAndVehicleStat NumActiveVehicles { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public PedAndVehicleStat NumInactiveVehicles { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public PedAndVehicleStat NumRealVehicles { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public PedAndVehicleStat NumDummyVehicles { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public PedAndVehicleStat NumSuperDummyVehicles { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public PedAndVehicleStat NumNetworkDummyVehicles { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public PedAndVehicleResult()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public PedAndVehicleResult(XElement element)
        {
            NumPeds = ProcessElement(element, "numPeds");
            NumActivePeds = ProcessElement(element, "numActivePeds");
            NumInactivePeds = ProcessElement(element, "numInactivePeds");
            NumEventScanHiPeds = ProcessElement(element, "numEventScanHiPeds");
            NumEventScanLoPeds = ProcessElement(element, "numEventScanLoPeds");
            NumMotionTaskHiPeds = ProcessElement(element, "numMotionTaskHiPeds");
            NumMotionTaskLoPeds = ProcessElement(element, "numMotionTaskLoPeds");
            NumPhysicsHiPeds = ProcessElement(element, "numPhysicsHiPeds");
            NumPhysicsLoPeds = ProcessElement(element, "numPhysicsLoPeds");
            NumEntityScanHiPeds = ProcessElement(element, "numEntityScanHiPeds");
            NumEntityScanLoPeds = ProcessElement(element, "numEntityScanLoPeds");
            NumVehicles = ProcessElement(element, "numVehicles");
            NumActiveVehicles = ProcessElement(element, "numActiveVehicles");
            NumInactiveVehicles = ProcessElement(element, "numInactiveVehicles");
            NumRealVehicles = ProcessElement(element, "numRealVehicles");
            NumDummyVehicles = ProcessElement(element, "numDummyVehicles");
            NumSuperDummyVehicles = ProcessElement(element, "numSuperDummyVehicles");
            NumNetworkDummyVehicles = ProcessElement(element, "numNetworkDummyVehicles");
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="element"></param>
        /// <param name="subElementName"></param>
        /// <returns></returns>
        private PedAndVehicleStat ProcessElement(XElement element, String subElementName)
        {
            XElement subElement = element.Element(subElementName);
            return (subElement != null ? new PedAndVehicleStat(subElement) : null);
        }
        #endregion // Private Methods
    } // PedAndVehicleResult

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class PedAndVehicleStat
    {
        #region Properties
        /// <summary>
        /// Minimum value recorded for this memory result.
        /// </summary>
        [DataMember]
        public uint Min { get; private set; }

        /// <summary>
        /// Maximum value recorded for this memory result.
        /// </summary>
        [DataMember]
        public uint Max { get; private set; }

        /// <summary>
        /// Average value recorded for this memory result.
        /// </summary>
        [DataMember]
        public uint Average { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        internal PedAndVehicleStat()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <param name="average"></param>
        public PedAndVehicleStat(uint min, uint max, uint average)
        {
            Min = min;
            Max = max;
            Average = average;
        }

        /// <summary>
        /// 
        /// </summary>
        public PedAndVehicleStat(XElement element)
        {
            // Extract the various stats
            XElement minElement = element.Element("min");
            if (minElement != null && minElement.Attribute("value") != null)
            {
                Min = UInt32.Parse(minElement.Attribute("value").Value);
            }

            XElement maxElement = element.Element("max");
            if (maxElement != null && maxElement.Attribute("value") != null)
            {
                Max = UInt32.Parse(maxElement.Attribute("value").Value);
            }

            XElement avgElement = element.Element("average");
            if (avgElement != null && avgElement.Attribute("value") != null)
            {
                Average = UInt32.Parse(avgElement.Attribute("value").Value);
            }
        }
        #endregion // Constructor(s)
    } // PedAndVehicleStat
}
