﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RSG.Model.Map.Utilities
{
    /// <summary>
    /// A static class that contains search functions to use on the model.map data.
    /// </summary>
    public static class Search
    {
        static SortedDictionary<char, Dictionary<String, List<ISearchable>>> NewSearchIndex = null;

        /// <summary>
        /// Creates the search indexer used to make searches super fast, this is only called once
        /// </summary>
        /// <param name="headSearchable"></param>
        private static void CreateIndex(ISearchable headSearchable)
        {
            NewSearchIndex = new SortedDictionary<char, Dictionary<String, List<ISearchable>>>();
            foreach (ISearchable searchable in headSearchable.WalkSearchDepth)
            {
                if (!String.IsNullOrEmpty(searchable.Name))
                {
                    if (!NewSearchIndex.ContainsKey(searchable.Name.ToLower()[0]))
                        NewSearchIndex.Add(searchable.Name.ToLower()[0], new Dictionary<String,List<ISearchable>>());
                    if (!NewSearchIndex[searchable.Name.ToLower()[0]].ContainsKey(searchable.Name.ToLower()))
                        NewSearchIndex[searchable.Name.ToLower()[0]].Add(searchable.Name.ToLower(), new List<ISearchable>());

                    NewSearchIndex[searchable.Name.ToLower()[0]][searchable.Name.ToLower()].Add(searchable);
                }
            }
        }

        /// <summary>
        /// Goes through all of the ISearchable objects starting with the given root
        /// and returns a list of objects that match the search parameter
        /// </summary>
        public static List<ISearchable> SimpleSearchFirst(ISearchable searchRoot, String searchParameter)
        {
            if (NewSearchIndex == null)
                CreateIndex(searchRoot);

            List<ISearchable> result = new List<ISearchable>();
            String searchString = searchParameter.ToLower();

            if (searchParameter.Length > 0 && NewSearchIndex.ContainsKey(searchString[0]))
            {
                Dictionary<String, List<String>> validMatches = new Dictionary<String, List<String>>();
                foreach (KeyValuePair<String, List<ISearchable>> searchable in NewSearchIndex[searchString[0]])
                {
                    if (searchable.Key.Length >= searchParameter.Length)
                    {
                        if (!validMatches.ContainsKey(searchable.Key.Substring(0, searchParameter.Length)))
                        {
                            validMatches.Add(searchable.Key.Substring(0, searchParameter.Length), new List<String>());
                        }
                        validMatches[searchable.Key.Substring(0, searchParameter.Length)].Add(searchable.Key);
                    }
                }
                foreach (KeyValuePair<String, List<String>> validMatch in validMatches)
                {
                    if (validMatch.Key == searchString)
                    {
                        ISearchable finalResult = NewSearchIndex[searchString[0]][validMatch.Value[0]][0];
                        ISearchable parent = finalResult;
                        while (parent != null)
                        {
                            result.Add(parent);
                            ISearchable newParent = parent.SearchableParent;
                            if (newParent == null)
                                break;

                            parent = newParent;
                        }
                        result.Reverse();
                        foreach (ISearchable TEST in result)
                        {
                            System.Diagnostics.Debug.Print(TEST.Name);
                        }
                        break;
                    }
                }
                validMatches = null;
            }

            return result;
        }

        /// <summary>
        /// Goes through all of the ISearchable objects starting with the given root
        /// and returns a list of objects that match the search parameter
        /// </summary>
        public static ISearchable SimpleSearchNext(ISearchable searchHead, ISearchable lastSearch, String searchParameter)
        {
            ISearchable result = null;
            //String searchString = searchParameter.ToLower();

            //if (searchParameter.Length > 0 && NewSearchIndex.ContainsKey(searchString[0]))
            //{
            //    Dictionary<String, String> validMatches = new Dictionary<String, String>();
            //    foreach (KeyValuePair<String, List<ISearchable>> searchable in NewSearchIndex[searchString[0]])
            //    {
            //        if (searchable.Key.Length >= searchParameter.Length)
            //        {
            //            if (!validMatches.ContainsKey(searchable.Key.Substring(0, searchParameter.Length)))
            //            {
            //                validMatches.Add(searchable.Key.Substring(0, searchParameter.Length), searchable.Key);
            //            }
            //        }
            //    }

            //    //Boolean foundCurrent = false;
            //    //foreach (KeyValuePair<String, String> validMatch in validMatches)
            //    //{
            //    //    if (validMatch.Key == searchString)
            //    //    {
            //    //        result = NewSearchIndex[searchString[0]][validMatch.Value][0];
            //    //        break;
            //    //    }
            //    //}
            //}
            //foreach (ISearchable searchable in searchHead.WalkSearchDepth)
            //{
        //        if (searchable == lastSearch)
        //        {
        //            foundLast = true;
        //            continue;
        //        }

        //        if (foundLast == true)
        //        {
        //            if (searchable.Name.StartsWith(searchParameter, true, System.Globalization.CultureInfo.CreateSpecificCulture("en-GB")))
        //            {
        //                result = searchable;
        //                break;
        //            }
        //        }
         //   }

            return result;
        }

        /// <summary>
        /// Goes through all of the ISearchable objects starting with the given root
        /// and returns a list of objects that match the search parameter
        /// </summary>
        public static ISearchable SimpleSearchPrevious(ISearchable searchHead, ISearchable currentSearchable, String searchParameter)
        {
            ISearchable result = null;

        //    foreach (ISearchable searchable in searchHead.WalkSearchDepth)
        //    {
        //        if (searchable == lastSearch)
        //            break;

        //        if (searchable.Name.StartsWith(searchParameter, true, System.Globalization.CultureInfo.CreateSpecificCulture("en-GB")))
        //        {
        //            result = searchable;
        //            break;
        //        }
        //    }

            return result;
        }

    } // Search

} // RSG.Model.Map.Utilities namespace
