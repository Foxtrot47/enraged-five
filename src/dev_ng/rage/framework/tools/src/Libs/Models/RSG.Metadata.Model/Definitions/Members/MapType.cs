﻿// --------------------------------------------------------------------------------------------
// <copyright file="MapType.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions.Members
{
    /// <summary>
    /// Specifies the different types of map members the parCodeGen system supports.
    /// </summary>
    public enum MapType
    {
        /// <summary>
        /// The map type is unknown.
        /// </summary>
        Unknown,

        /// <summary>
        /// The map type is not recognised as valid, though it is defined as something.
        /// </summary>
        Unrecognised,

        /// <summary>
        /// The map member has a type of AtMap.
        /// </summary>
        AtMap,

        /// <summary>
        /// The array member has a type of AtBinaryMap.
        /// </summary>
        AtBinary,
    } // RSG.Metadata.Model.Definitions.Members.MapType {Enum}
} // RSG.Metadata.Model.Definitions.Members {Namespace}
