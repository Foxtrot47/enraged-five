﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.Model.Map;
using RSG.ManagedRage;
using RSG.Platform;
using RSG.Base.Extensions;
using RSG.Base.ConfigParser;
using RSG.Base.Logging;

namespace RSG.Model.Report.Reports
{
    /// <summary>
    /// 
    /// </summary>
    public class MapAssetCSVReport : CSVReport, IDynamicMapReport
    {
        #region Constants
        private const String NAME = "Export Map Asset CSV Report";
        private const String DESCRIPTION = "Exports the selected level assets into a .csv file";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Determines whether the map data should be loaded
        /// </summary>
        public bool LoadMapData
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Determines whether the car generators should be loaded
        /// 
        /// </summary>
        public bool LoadCarGenarators
        {
            get { return false; }
        }
        #endregion // Properties

        #region Construtcor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public MapAssetCSVReport()
            : base(NAME, DESCRIPTION)
        {
        }

        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Generate the report dynamically.
        /// </summary>
        /// <param name="level"></param>
        /// <param name="gv"></param>
        public virtual void Generate(ILevel level, RSG.Base.ConfigParser.ConfigGameView gv, IPlatformCollection platforms)
        {
            DateTime start = DateTime.Now;
            ExportMapAssetsCSVReport(level, gv);
            Log.Log__Message("Map asset CSV export complete. (report took {0} milliseconds to create.)", (DateTime.Now - start).TotalMilliseconds);
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Generates the vehicles CSV report
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="level"></param>
        /// <param name="gv"></param>
        protected void ExportMapAssetsCSVReport(ILevel level, RSG.Base.ConfigParser.ConfigGameView gv)
        {
            using (StreamWriter sw = new StreamWriter(Filename))
            {
                WriteCsvHeader(sw);

                foreach (MapSection section in RSG.Model.Map.Util.Enumerations.GetAllMapSections(level, null, true))
                {
                    // Get the node that we actually want to process
                    ContentNodeMap node = section.MapNode;

                    foreach (ContentNode outputNode in node.Outputs)
                    {
                        if (outputNode.Outputs.Count() == 0)
                        {
                            string independentFilePath = ReportUtils.GeneratePathForNode(outputNode);
                            ProcessIndependentFilePath(independentFilePath, section, gv, sw);
                        }
                        else
                        {
                            foreach (ContentNode processedNode in outputNode.Outputs)
                            {
                                string independentFilePath = ReportUtils.GeneratePathForNode(processedNode);
                                ProcessIndependentFilePath(independentFilePath, section, gv, sw);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sw"></param>
        /// <param name="milo"></param>
        protected void WriteCsvHeader(StreamWriter sw)
        {
            string header = "Asset Name, Type, Map Section, Parent Area, Grandparent Area, PS3 Physical Size, PS3 Virtual Size, PS3 Total Size, X360 Physical Size, X360 Virtual Size, X360 Total Size, Win Physical Size, Win Virtual Size, Win Total Size";
            sw.WriteLine(header);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="section"></param>
        /// <param name="sw"></param>
        protected void ProcessIndependentFilePath(string filePath, MapSection section, ConfigGameView gv, StreamWriter sw)
        {
            // Open up the packfiles we wish to process
            Dictionary<BuildPlatform, Packfile> packFiles = ReportUtils.OpenPackFiles(filePath, gv);

            // Extract the list of entries in the pack file
            Dictionary<Tuple<string, FileType>, Dictionary<BuildPlatform, PackEntry>> assetPackEntryMap = new Dictionary<Tuple<string, FileType>, Dictionary<BuildPlatform, PackEntry>>();

            foreach (KeyValuePair<BuildPlatform, Packfile> pair in packFiles)
            {
                ExtractEntries(pair.Value, pair.Key, ref assetPackEntryMap);
            }

            // Output details for each of the entries in our map
            MapArea parent = section.Container as MapArea;
            MapArea grandParent = (parent != null ? parent.Container as MapArea : null);

            foreach (KeyValuePair<Tuple<string, FileType>, Dictionary<BuildPlatform, PackEntry>> pair in assetPackEntryMap)
            {
                string name = pair.Key.Item1;
                FileType fileType = pair.Key.Item2;

                string scvRecord = string.Format("{0}, {1}, {2}, {3}, {4}, {5}",
                            name,
                            fileType.ToString(),
                            section.Name, (parent != null ? parent.Name : "n/a"), (grandParent != null ? grandParent.Name : "n/a"),
                            GenerateMemoryDetails(pair.Value));
                sw.WriteLine(scvRecord);
            }

            // Close all the pack files we opened
            foreach (KeyValuePair<BuildPlatform, Packfile> pair in packFiles)
            {
                pair.Value.Close();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="entryMap"></param>
        private void ExtractEntries(Packfile pack, BuildPlatform platform, ref Dictionary<Tuple<string, FileType>, Dictionary<BuildPlatform, PackEntry>> entryMap)
        {
            if (pack == null || pack.Entries == null)
            {
                return;
            }

            foreach (PackEntry entry in pack.Entries)
            {
                // Ignore non-resource entries
                if (entry.IsResource)
                {
                    // Generate the name to use for this entry
                    string extension = Path.GetExtension(entry.Name);
                    FileType fileType = FileTypeUtils.ConvertExtensionToFileType(extension);

                    string name = Path.GetFileNameWithoutExtension(entry.Name);
                    if (!String.IsNullOrEmpty(entry.Path))
                    {
                        name = entry.Path + "\\" + name;
                    }
                    name.ToLower();

                    Tuple<string, FileType> key = new Tuple<string, FileType>(name, fileType);

                    if (!entryMap.ContainsKey(key))
                    {
                        entryMap[key] = new Dictionary<BuildPlatform, PackEntry>();
                    }

                    entryMap[key][platform] = entry;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entries"></param>
        /// <returns></returns>
        private string GenerateMemoryDetails(Dictionary<BuildPlatform, PackEntry> entries)
        {
            string ps3 = "n/a, n/a, n/a";
            if (entries.ContainsKey(BuildPlatform.PS3))
            {
                ps3 = GenerateMemoryDetails(entries[BuildPlatform.PS3]);
            }

            string xbox = "n/a, n/a, n/a";
            if (entries.ContainsKey(BuildPlatform.Xbox360))
            {
                xbox = GenerateMemoryDetails(entries[BuildPlatform.Xbox360]);
            }

            string pc = "n/a, n/a, n/a";
            if (entries.ContainsKey(BuildPlatform.PS3))
            {
                pc = GenerateMemoryDetails(entries[BuildPlatform.PS3]);
            }

            return String.Format("{0}, {1}, {2}", ps3, xbox, pc);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entry"></param>
        /// <returns></returns>
        private string GenerateMemoryDetails(PackEntry entry)
        {
            return String.Format("{0}, {1}, {2}", entry.PhysicalSize, entry.VirtualSize, entry.PhysicalSize + entry.VirtualSize);
        }
        #endregion // Private Methods
    } // VehicleCSVReport
} // RSG.Model.Report.Reports
