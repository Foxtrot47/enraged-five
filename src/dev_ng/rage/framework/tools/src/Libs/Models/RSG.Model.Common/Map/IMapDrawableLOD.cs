﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common.Map
{
    /// <summary>
    /// Interface for Drawable LOD
    /// </summary>
    public interface IMapDrawableLOD
    {
        #region Properties
        string Name { get; }

        float LodDistance { get; }

        uint PolygonCount { get; }

        MapDrawableLODLevel LODLevel { get; }
        #endregion  
    } // IMapDrawableLOD
}
