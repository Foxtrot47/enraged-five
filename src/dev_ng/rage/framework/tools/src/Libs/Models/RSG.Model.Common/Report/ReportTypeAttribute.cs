﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common.Report
{
    /// <summary>
    /// Attribute used for marking up types that are to be returned from reports.
    /// </summary>
    public class ReportTypeAttribute : Attribute
    {
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ReportTypeAttribute()
            : base()
        {
        }
    } // ReportTypeAttribute
}
