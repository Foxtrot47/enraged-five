﻿// --------------------------------------------------------------------------------------------
// <copyright file="StructMember.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions.Members
{
    using System;
    using System.Xml;
    using RSG.Base.Extensions;
    using RSG.Base.Logging;
    using RSG.Editor;
    using RSG.Editor.Model;
    using RSG.Metadata.Model.Resources;
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// Represents the &lt;struct&gt; member node in the parCodeGen system that can be
    /// instanced in a metadata file.
    /// </summary>
    public class StructMember : MemberBase, IEquatable<StructMember>
    {
        #region Fields
        /// <summary>
        /// The name of the xml attribute containing the data for the
        /// <see cref="AddGroupWidgets"/> property.
        /// </summary>
        private const string XmlGroupWidgetsAttr = "addGroupWidget";

        /// <summary>
        /// The name of the xml attribute containing the data for the
        /// <see cref="Type"/> property.
        /// </summary>
        private const string XmlTypeAttr = "type";

        /// <summary>
        /// The private field used for the <see cref="AddGroupWidgets"/> property.
        /// </summary>
        private string _addGroupWidgets;

        /// <summary>
        /// The private field used for the <see cref="Type"/> property.
        /// </summary>
        private string _type;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="StructMember"/> class to be one of the
        /// members of the specified structure.
        /// </summary>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        public StructMember(IStructure structure)
            : base(structure)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="StructMember"/> class as a copy of the
        /// specified instance.
        /// </summary>
        /// <param name="other">
        /// The instance to copy.
        /// </param>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        public StructMember(StructMember other, IStructure structure)
            : base(other, structure)
        {
            this._type = other._type;
            this._addGroupWidgets = other._addGroupWidgets;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="StructMember"/> class.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        public StructMember(XmlReader reader, IStructure structure)
            : base(reader, structure)
        {
            this.Deserialise(reader);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether the child widgets should be displayed.
        /// </summary>
        public bool AddGroupWidgets
        {
            get { return this.Dictionary.To<bool>(this._addGroupWidgets, true); }
            set { this.SetProperty(ref this._addGroupWidgets, value.ToString()); }
        }

        /// <summary>
        /// Gets the structure definition that this member represents.
        /// </summary>
        public IStructure ReferencedStructure
        {
            get
            {
                IDefinitionDictionary scope = null;
                if (this.Structure != null)
                {
                    scope = this.Structure.Dictionary;
                }

                return this.GetStructureType(scope);
            }
        }

        /// <summary>
        /// Gets or sets the data type for the structure that this member represents.
        /// </summary>
        public string Type
        {
            get
            {
                return this._type;
            }

            set
            {
                string[] properties =
                {
                    "Type",
                    "ReferencedStructure"
                };

                this.SetProperty(ref this._type, value.ToString(), properties);
            }
        }

        /// <summary>
        /// Gets the type name for this member.
        /// </summary>
        public override string TypeName
        {
            get { return "struct"; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new <see cref="StructMember"/> that is a copy of this instance.
        /// </summary>
        /// <returns>
        /// A new <see cref="StructMember"/> that is a copy of this instance.
        /// </returns>
        public new StructMember Clone()
        {
            return new StructMember(this, this.Structure);
        }

        /// <summary>
        /// Creates a <see cref="ITunable"/> object that represents a instance of this member
        /// with it's default value set.
        /// </summary>
        /// <param name="parent">
        /// The tunable parent the new tunable will belong to.
        /// </param>
        /// <returns>
        /// A parCodeGen tunable that represents a instance of this member.
        /// </returns>
        public override ITunable CreateTunable(ITunableParent parent)
        {
            return new StructTunable(this, parent);
        }

        /// <summary>
        /// Creates a <see cref="ITunable"/> object that represents a instance of this member
        /// using the specified Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The Xml.XmlReader that provides the data used to initialise the new tunable.
        /// </param>
        /// <param name="parent">
        /// The tunable parent the new tunable will belong to.
        /// </param>
        /// <param name="log">
        /// A object that is used to log any warnings or errors produced during the parsing of
        /// the specified reader.
        /// </param>
        /// <returns>
        /// A parCodeGen tunable that represents a instance of this member.
        /// </returns>
        public override ITunable CreateTunable(
            XmlReader reader, ITunableParent parent, ILog log)
        {
            return new StructTunable(reader, this, parent, log);
        }

        /// <summary>
        /// Creates a deep copy of the current instance.
        /// </summary>
        /// <returns>
        /// A deep copy of the current System.Object.
        /// </returns>
        public override object DeepClone()
        {
            return this.Clone();
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified
        /// <see cref="StructMember"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public bool Equals(StructMember other)
        {
            if (other == null)
            {
                return false;
            }

            return object.ReferenceEquals(other, this);
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="IMember"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public override bool Equals(IMember other)
        {
            return this.Equals(other as StructMember);
        }

        /// <summary>
        /// Writes the xml representation of this instance onto the specified
        /// System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that the xml representation of this instance should be
        /// written to.
        /// </param>
        public override void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                throw new SmartArgumentNullException(() => writer);
            }

            this.SerialiseBaseAttributes(writer);
            if (this._type != null)
            {
                writer.WriteAttributeString(XmlTypeAttr, this._type);
            }

            if (this._addGroupWidgets != null)
            {
                writer.WriteAttributeString(XmlGroupWidgetsAttr, this._addGroupWidgets);
            }
        }

        /// <summary>
        /// Validates this entity and creates a validation result object containing the errors
        /// and warnings.
        /// </summary>
        /// <param name="recursive">
        /// A value indicating whether any child models to this one should also be validated.
        /// </param>
        /// <returns>
        /// A validation result object containing all of the errors and warnings associated
        /// with this validation pass.
        /// </returns>
        public override ValidationResult Validate(bool recursive)
        {
            ValidationResult result = new ValidationResult();
            this.ValidateBaseProperties(result);

            if (this._type == null)
            {
                string msg = StringTable.MissingRequiredAttributeError;
                msg = msg.FormatCurrent("type", "struct");
                result.AddError(msg, this.Location);
            }
            else if (String.IsNullOrWhiteSpace(this._type))
            {
                string msg = StringTable.EmptyRequiredAttributeError;
                msg = msg.FormatCurrent("type", "struct");
                result.AddError(msg, this.Location);
            }

            if (this.ReferencedStructure == null)
            {
                string msg = StringTable.UnresolvedStructureError;
                msg = msg.FormatCurrent(this._type, "struct");
                result.AddError(msg, this.Location);
            }

            return result;
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            this._type = reader.GetAttribute(XmlTypeAttr);
            this._addGroupWidgets = reader.GetAttribute(XmlGroupWidgetsAttr);
            reader.Skip();
        }

        /// <summary>
        /// Looks through the specified scope and returns the structure that this struct
        /// member represents.
        /// </summary>
        /// <param name="scope">
        /// A definition dictionary containing all the other types in the search scope.
        /// </param>
        /// <returns>
        /// This struct members represented structure.
        /// </returns>
        private IStructure GetStructureType(IDefinitionDictionary scope)
        {
            if (this._type == null || scope == null)
            {
                return null;
            }

            return scope.GetStructure(this._type);
        }
        #endregion Methods
    } // RSG.Metadata.Model.Definitions.Members.StructMember {Class}
} // RSG.Metadata.Model.Definitions.Members {Namespace}
