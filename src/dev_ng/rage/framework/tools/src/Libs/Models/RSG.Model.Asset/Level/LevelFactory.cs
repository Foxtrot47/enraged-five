﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.Base.ConfigParser;
using RSG.Interop.Bugstar;
using RSG.Interop.Bugstar.Game;
using RSG.Interop.Bugstar.Organisation;
using RSG.Statistics.Client.ServiceClients;
using RSG.Statistics.Common.Dto.GameAssets;
using RSG.Base.Logging;
using System.Diagnostics;
using RSG.Base.Configuration;
using RSG.Base.Configuration.Bugstar;

namespace RSG.Model.Asset.Level
{
    /// <summary>
    /// 
    /// </summary>
    public class LevelFactory : ILevelFactory
    {
        #region Properties
        /// <summary>
        /// Personal game view (to prevent any threading issues).
        /// </summary>
        private ConfigGameView _gameView;

        /// <summary>
        /// Tools configuration object.
        /// </summary>
        private IConfig _config;

        /// <summary>
        /// Bugstar configuration object.
        /// </summary>
        private IBugstarConfig _bugstarConfig;
        #endregion
        
        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public LevelFactory(IConfig config, IBugstarConfig bugstarConfig)
        {
            _gameView = new ConfigGameView();
            _config = config;
            _bugstarConfig = bugstarConfig;
        }
        #endregion // Constructor(s)

        #region ILevelFactory Implementation
        /// <summary>
        /// Creates a new level collection from the specified source.
        /// </summary>
        /// <param name="source">Source to use for creating the collection.</param>
        /// <param name="buildIdentifier">Optional build identifier for when the source is Database.</param>
        /// <returns></returns>
        public ILevelCollection CreateCollection(DataSource source, string buildIdentifier = null)
        {
            ILevelCollection collection = null;

            if (source == DataSource.SceneXml)
            {
                Log.Log__Profile("Level collection creation - export data.");
                collection = CreateCollectionFromExportData();
                Log.Log__ProfileEnd();
            }
            else if (source == DataSource.Database)
            {
                Log.Log__Profile("Level collection creation - database.");
                collection = CreateCollectionFromDatabase(buildIdentifier);
                Log.Log__ProfileEnd();
            }
            else
            {
                Debug.Assert(false, String.Format("Unknown data source '{0}' specified while creating a level collection.", source.ToString()));
                throw new ArgumentException(String.Format("Unknown data source '{0}' specified while creating a level collection.", source.ToString()));
            }

            return collection;
        }
        #endregion // ILevelFactory Implementation

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="gv"></param>
        /// <returns></returns>
        private ILevelCollection CreateCollectionFromExportData()
        {
            ILevelCollection levels = new LocalLevelCollection(_gameView, _config, _bugstarConfig);
            levels.Add(new LocalLevel("None", levels));

            foreach (ContentNodeLevel levelNode in _gameView.Content.Root.FindAll("level"))
            {
                ILevel level = new LocalLevel(levelNode.Name, levels);

                if (!levels.Contains(level))
                {
                    levels.Add(level);
                }
            }

            return levels;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <returns></returns>
        private ILevelCollection CreateCollectionFromDatabase(string buildIdentifier)
        {
            ILevelCollection levels = new SingleBuildLevelCollection(buildIdentifier);
            levels.Add(new DbLevel("None", levels));
            
            try
            {
                using (BuildClient client = new BuildClient())
                {
                    // If no build identifier was supplied, use the latest build.
                    if (buildIdentifier == null)
                    {
                        BuildDto dto = client.GetLatest();
                        if (dto != null)
                        {
                            buildIdentifier = dto.Identifier;
                        }
                    }

                    // Check that the identifier is set.
                    if (buildIdentifier != null)
                    {
                        foreach (LevelDto dto in client.GetAllLevelsForBuild(buildIdentifier).Items)
                        {
                            Level level = new DbLevel(dto.Name, levels);
                            levels.Add(level);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                Log.Log__Exception(ex, "Unhandled exception occurred while creating a level collection from the database.");
            }

            return levels;
        }
        #endregion // Private Methods

        #region IDisposable Implementation
        /// <summary>
        /// Dispose of the weapon streaming coordinator
        /// </summary>
        public void Dispose()
        {
            _gameView.Dispose();
        }
        #endregion // IDisposable Implementation
    } // LevelFactory
}
