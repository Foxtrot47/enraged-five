﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.ManagedRage;
using RSG.Platform;
using RSG.Base.Extensions;
using RSG.Base.ConfigParser;
using RSG.Base.Logging;
using RSG.Base.Tasks;
using RSG.Model.Common.Map;
using RSG.Model.Common.Extensions;

namespace RSG.Model.Report.Reports.Map
{
    /// <summary>
    /// 
    /// </summary>
    public class MapAssetReport : CSVReport, IDynamicLevelReport
    {
        #region Constants
        private const String c_name = "Map Asset Report";
        private const String c_description = "Exports the selected level assets into a .csv file";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (m_task == null)
                {
                    m_task = new ActionTask("Generating report", (context, progress) => GenerateReport((DynamicLevelReportContext)context, progress));
                }
                return m_task;
            }
        }
        private ITask m_task;

        /// <summary>
        /// List of stats that are required to generate this report.
        /// </summary>
        public IEnumerable<StreamableStat> RequiredStats
        {
            get { return null; }
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public MapAssetReport()
            : base(c_name, c_description)
        {
        }

        #endregion // Constructor(s)

        #region Task Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void GenerateReport(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            IMapHierarchy hierarchy = context.Level.MapHierarchy;

            if (hierarchy != null)
            {
                float increment = 1.0f / hierarchy.AllSections.Count();

                using (StreamWriter writer = new StreamWriter(Filename))
                {
                    WriteCsvHeader(writer, context.Platforms);

                    foreach (IMapSection section in hierarchy.AllSections)
                    {
                        string message = String.Format("Processing {0}", section.Name);
                        progress.Report(new TaskProgress(increment, true, message));

                        // Get the node that we actually want to process
                        ContentNodeMap node = section.GetConfigMapNode(context.GameView);

                        foreach (ContentNode outputNode in node.Outputs)
                        {
                            if (outputNode.Outputs.Count() == 0)
                            {
                                ContentNodeMapZip nodeZip = outputNode as ContentNodeMapZip;
                                if (nodeZip != null)
                                {
                                    string independentFilePath = Path.GetFullPath(nodeZip.Filename);
                                    ProcessIndependentFilePath(writer, independentFilePath, section, context.GameView, context.Platforms);
                                }
                            }
                            else
                            {
                                foreach (ContentNode processedNode in outputNode.Outputs)
                                {
                                    ContentNodeMapProcessedZip processedZip = processedNode as ContentNodeMapProcessedZip;
                                    if (processedZip != null)
                                    {
                                        string independentFilePath = Path.GetFullPath(processedZip.Filename);
                                        ProcessIndependentFilePath(writer, independentFilePath, section, context.GameView, context.Platforms);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        #endregion // Task Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sw"></param>
        /// <param name="milo"></param>
        protected void WriteCsvHeader(StreamWriter writer, IPlatformCollection platforms)
        {
            string header = "Asset Name,Type,Map Section,Parent Area,Grandparent Area";

            foreach (RSG.Platform.Platform platform in platforms)
            {
                header += String.Format(",{0} Physical Size,{0} Virtual Size,{0} Total Size", platform);
            }

            writer.WriteLine(header);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="section"></param>
        /// <param name="sw"></param>
        protected void ProcessIndependentFilePath(StreamWriter sw, string filepath, IMapSection section, ConfigGameView gv, IPlatformCollection platforms)
        {
            // Open up the packfiles we wish to process
            IDictionary<RSG.Platform.Platform, Packfile> packFiles = OpenPackFiles(filepath, gv, platforms);

            // Extract the list of entries in the pack file
            IDictionary<Tuple<string, FileType>, IDictionary<RSG.Platform.Platform, PackEntry>> assetPackEntryMap = new Dictionary<Tuple<string, FileType>, IDictionary<RSG.Platform.Platform, PackEntry>>();

            foreach (KeyValuePair<RSG.Platform.Platform, Packfile> pair in packFiles)
            {
                ExtractEntries(pair.Value, pair.Key, ref assetPackEntryMap);
            }

            // Output details for each of the entries in our map
            IMapArea parentArea = section.ParentArea;
            IMapArea grandParentArea = (parentArea != null ? parentArea.ParentArea : null);

            foreach (KeyValuePair<Tuple<string, FileType>, IDictionary<RSG.Platform.Platform, PackEntry>> pair in assetPackEntryMap)
            {
                string name = pair.Key.Item1;
                FileType fileType = pair.Key.Item2;

                string csvRecord = string.Format("{0},{1},{2},{3},{4}",
                            name,
                            fileType.ToString(),
                            section.Name, (parentArea != null ? parentArea.Name : "n/a"), (grandParentArea != null ? grandParentArea.Name : "n/a"));
                csvRecord += GenerateMemoryDetails(pair.Value, platforms);
                sw.WriteLine(csvRecord);
            }

            // Close all the pack files we opened
            foreach (KeyValuePair<RSG.Platform.Platform, Packfile> pair in packFiles)
            {
                pair.Value.Close();
            }
        }

        /// <summary>
        /// Uses the config's target info to convert the independent file path to per platform files and loads the resulting pack files.
        /// </summary>
        /// <param name="independentFilePath"></param>
        /// <param name="gv"></param>
        /// <returns>Dictionary mapping platform -> packfile containing all the loaded files</returns>
        private IDictionary<RSG.Platform.Platform, Packfile> OpenPackFiles(string independentFilePath, ConfigGameView gv, IPlatformCollection platforms)
        {
            IDictionary<RSG.Platform.Platform, Packfile> packFiles = new Dictionary<RSG.Platform.Platform, Packfile>();

            foreach (RSG.Platform.Platform platform in platforms)
            {
                string platformFilepath = ConvertIndependentPathToPlatformPath(independentFilePath, gv, platform);

                if (platformFilepath != null)
                {
                    // Load the pack file
                    packFiles[platform] = new Packfile();
                    packFiles[platform].Load(platformFilepath);
                }
            }

            return packFiles;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filepath"></param>
        /// <param name="gv"></param>
        /// <returns></returns>
        private string ConvertIndependentPathToPlatformPath(string filepath, ConfigGameView gv, RSG.Platform.Platform platform)
        {
            var config = RSG.Base.Configuration.ConfigFactory.CreateConfig();
            var target = config.Project.DefaultBranch.Targets[platform];

            string result = RSG.Pipeline.Services.Platform.PlatformPathConversion.ConvertAndRemapFilenameToPlatform(target, filepath);
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="entryMap"></param>
        private void ExtractEntries(Packfile pack, RSG.Platform.Platform platform, ref IDictionary<Tuple<string, FileType>, IDictionary<RSG.Platform.Platform, PackEntry>> entryMap)
        {
            if (pack == null || pack.Entries == null)
            {
                return;
            }

            foreach (PackEntry entry in pack.Entries)
            {
                // Ignore non-resource entries
                if (entry.IsResource)
                {
                    // Generate the name to use for this entry
                    string extension = Path.GetExtension(entry.Name);
                    FileType fileType = FileTypeUtils.ConvertExtensionToFileType(extension);

                    string name = Path.GetFileNameWithoutExtension(entry.Name);
                    if (!String.IsNullOrEmpty(entry.Path))
                    {
                        name = entry.Path + "\\" + name;
                    }
                    name.ToLower();

                    Tuple<string, FileType> key = new Tuple<string, FileType>(name, fileType);

                    if (!entryMap.ContainsKey(key))
                    {
                        entryMap[key] = new Dictionary<RSG.Platform.Platform, PackEntry>();
                    }

                    entryMap[key][platform] = entry;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entries"></param>
        /// <returns></returns>
        private string GenerateMemoryDetails(IDictionary<RSG.Platform.Platform, PackEntry> entries, IPlatformCollection platforms)
        {
            string csvData = "";
            foreach (RSG.Platform.Platform platform in platforms)
            {
                string platformData = ",n/a,n/a,n/a";
                if (entries.ContainsKey(platform))
                {
                    platformData = String.Format(",{0},{1},{2}", entries[platform].PhysicalSize, entries[platform].VirtualSize, entries[platform].PhysicalSize + entries[platform].VirtualSize);
                }
                csvData += platformData;
            }
            return csvData;
        }
        #endregion // Private Methods
    } // MapAssetReport
} // RSG.Model.Report.Reports
