﻿using System;
using System.Collections.Generic;
using RSG.Base.Logging;
using RSG.Model.Common;
using RSG.Statistics.Client.ServiceClients;
using RSG.Statistics.Common.Dto.GameAssets.Bundles;

namespace RSG.Model.Character
{
    /// <summary>
    /// Collection of characters that come from a single build
    /// </summary>
    public class SingleBuildCharacterCollection : CharacterCollectionBase
    {
        #region Properties
        /// <summary>
        /// The build this character collection is for.
        /// </summary>
        public string BuildIdentifier { get; protected set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public SingleBuildCharacterCollection(string buildIdentifier)
            : base()
        {
            BuildIdentifier = buildIdentifier;
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="statsToLoad"></param>
        public override void LoadStats(IEnumerable<StreamableStat> statsToLoad)
        {
            try
            {
                using (BuildClient client = new BuildClient())
                {
                    foreach (ICharacter character in AllCharacters)
                    {
                        // Check whether the stats are already loaded
                        if (!character.AreStatsLoaded(statsToLoad))
                        {
                            CharacterStatBundleDto bundle = client.GetCharacterStat(BuildIdentifier, character.Hash.ToString());
                            (character as DbCharacter).LoadStatsFromDatabase(bundle);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Log.Log__Exception(e, "Unhandled exception while retrieving stats from the database.");
            }
        }
        #endregion // Overrides
    } // SingleBuildCharacterCollection
}
