﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common.Map;
using RSG.Model.Common;
using RSG.Base.ConfigParser;
using RSG.SceneXml;
using System.ComponentModel;
using System.Collections.ObjectModel;
using RSG.Statistics.Common.Dto.GameAssetStats;
using System.Reflection;
using RSG.Base.Logging;

namespace RSG.Model.Map3
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class ArchetypeBase : StreamableAssetBase, IArchetype
    {
        #region Properties
        /// <summary>
        /// List of entities that reference this archetype
        /// </summary>
        [Browsable(false)]
        public ICollection<IEntity> Entities
        {
            get
            {
                return m_entities;
            }
            private set
            {
                SetPropertyValue(ref m_entities, value, "Entities");
            }
        }
        private ICollection<IEntity> m_entities;

        /// <summary>
        /// 
        /// </summary>
        [StreamableStatAttribute(StreamableArchetypeStat.LodDistanceString)]
        public float LodDistance
        {
            get
            {
                CheckStatLoaded(StreamableArchetypeStat.LodDistance);
                return m_lodDistance;
            }
            protected set
            {
                SetPropertyValue(ref m_lodDistance, value, "LodDistance");
                SetStatLodaded(StreamableArchetypeStat.LodDistance, true);
            }
        }
        private float m_lodDistance;

        /// <summary>
        /// Holds our custom linking information
        /// </summary>
        [StreamableStatAttribute(StreamableArchetypeStat.SceneLinksString)]
        public Dictionary<String, LODHierarchyDef> SceneLinks
        {
            get
            {
                CheckStatLoaded(StreamableArchetypeStat.SceneLinks);
                return m_SceneLinks;
            }
            protected set
            {
                SetPropertyValue(ref m_SceneLinks, value, "SceneLinks");
                SetStatLodaded(StreamableArchetypeStat.SceneLinks, true);
            }
        }
        protected Dictionary<String, LODHierarchyDef> m_SceneLinks;
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="name"></param>
        /// <param name="parent"></param>
        public ArchetypeBase(string name)
            : base(name)
        {
            Entities = new Collection<IEntity>();
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sceneObject"></param>
        /// <param name="scene"></param>
        public virtual void LoadStatsFromExportData(TargetObjectDef sceneObject, Scene scene)
        {
            LodDistance = sceneObject.GetLODDistance();
            // JWR - this prevents an enormous number of objects from being collected
            //m_SceneLinks = sceneObject.SceneLinks;
            m_SceneLinks = new Dictionary<string, LODHierarchyDef>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="requestedStats"></param>
        public void LoadStatsFromDatabase(ArchetypeStatDto dto, IList<StreamableStat> requestedStats)
        {
            IDictionary<StreamableStat, PropertyInfo> modelLookup = StreamableStatUtils.GetPropertyInfoLookup(GetType());
            IDictionary<StreamableStat, PropertyInfo> dtoLookup = StreamableStatUtils.GetPropertyInfoLookup(dto.GetType());

            // Map the properties we are after.
            foreach (StreamableStat stat in requestedStats.Where(item => !IsStatLoaded(item)))
            {
                if (dtoLookup.ContainsKey(stat) && modelLookup.ContainsKey(stat))
                {
                    // Is this a complex item?
                    if (stat.DtoToModelRequiresProcessing)
                    {
                        ProcessComplexArchetypeStat(dto, stat);
                    }
                    else
                    {
                        object value = dtoLookup[stat].GetValue(dto, null);
                        modelLookup[stat].SetValue(this, value, null);
                    }
                }
                else
                {
#warning Readd assert once everything is marked up.
                    //Debug.Assert(false, String.Format("One of the ArchetypeStat dto class or the model class doesn't have the '{0}' stat marked up.", stat));
                    Log.Log__Error(String.Format("One of the ArchetypeStat dto class or the model class doesn't have the '{0}' stat marked up.", stat));
                }
            }
        }
        #endregion // Public Methods

        #region Virtual Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="stat"></param>
        protected virtual void ProcessComplexArchetypeStat(ArchetypeStatDto dto, StreamableStat stat)
        {
        }
        #endregion // Virtual Methods
    } // ArchetypeBase
}
