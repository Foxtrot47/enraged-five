﻿//---------------------------------------------------------------------------------------------
// <copyright file="DialogueConfigurations.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.Model
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Xml;
    using RSG.Editor.Model;

    /// <summary>
    /// A class object containing the configurations used for the dialogue including the
    /// character names, and different audio types that can be applied.
    /// </summary>
    public sealed class DialogueConfigurations : ModelBase
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Audibilities"/> property.
        /// </summary>
        private ModelCollection<DialogueAudibility> _audibilities;

        /// <summary>
        /// The private field used for the <see cref="AudioTypes"/> property.
        /// </summary>
        private ModelCollection<DialogueAudioType> _audioTypes;

        /// <summary>
        /// The private field used for the <see cref="Characters"/> property.
        /// </summary>
        private Dictionary<Guid, DialogueCharacter> _characterLookup;

        /// <summary>
        /// The private field used for the <see cref="Characters"/> property.
        /// </summary>
        private ModelCollection<DialogueCharacter> _characters;

        /// <summary>
        /// The private field used for the <see cref="DefaultAudibilityId"/> property.
        /// </summary>
        private DialogueAudibility _defaultAudibility;

        /// <summary>
        /// The private field used for the <see cref="DefaultAudioTypeId"/> property.
        /// </summary>
        private DialogueAudioType _defaultAudioType;

        /// <summary>
        /// The private field used for the <see cref="DefaultSpecialId"/> property.
        /// </summary>
        private DialogueSpecial _defaultSpecial;

        /// <summary>
        /// The private field used for the <see cref="DefaultSubtitleTypeId"/> property.
        /// </summary>
        private DialogueSubtitleType _defaultSubtitleType;

        /// <summary>
        /// The private field used for the <see cref="Specials"/> property.
        /// </summary>
        private ModelCollection<DialogueSpecial> _specials;

        /// <summary>
        /// The private field used for the <see cref="SubtitleTypes"/> property.
        /// </summary>
        private ModelCollection<DialogueSubtitleType> _subtitleTypes;

        /// <summary>
        /// The private field used for the <see cref="Voices"/> property.
        /// </summary>
        private ModelCollection<DialogueVoice> _voices;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DialogueConfigurations"/> class.
        /// </summary>
        public DialogueConfigurations()
        {
            this._audibilities = new ModelCollection<DialogueAudibility>(this);
            this._audioTypes = new ModelCollection<DialogueAudioType>(this);
            this._characters = new ModelCollection<DialogueCharacter>(this);
            this._characterLookup = new Dictionary<Guid, DialogueCharacter>();
            this._characters.CollectionChanged += this.CharactersChanged;
            this._specials = new ModelCollection<DialogueSpecial>(this);
            this._subtitleTypes = new ModelCollection<DialogueSubtitleType>();
            this._voices = new ModelCollection<DialogueVoice>(this);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the collection of audibility fields that have been defined for this dialogue
        /// configuration.
        /// </summary>
        public ModelCollection<DialogueAudibility> Audibilities
        {
            get { return this._audibilities; }
        }

        /// <summary>
        /// Gets the collection of audio types that have been defined for this dialogue
        /// configuration.
        /// </summary>
        public ModelCollection<DialogueAudioType> AudioTypes
        {
            get { return this._audioTypes; }
        }

        /// <summary>
        /// Gets the collection of dialogue characters that have been defined for this dialogue
        /// configurations.
        /// </summary>
        public ModelCollection<DialogueCharacter> Characters
        {
            get { return this._characters; }
        }

        /// <summary>
        /// Gets the audibility id that has been setup as the default.
        /// </summary>
        public Guid DefaultAudibilityId
        {
            get { return _defaultAudibility != null ? _defaultAudibility.Id : Guid.Empty; }
        }

        /// <summary>
        /// Gets the audio type id that has been setup as the default.
        /// </summary>
        public Guid DefaultAudioTypeId
        {
            get { return _defaultAudioType != null ? _defaultAudioType.Id : Guid.Empty; }
        }

        /// <summary>
        /// Gets the special id that has been setup as the default.
        /// </summary>
        public Guid DefaultSpecialId
        {
            get { return _defaultSpecial != null ? _defaultSpecial.Id : Guid.Empty; }
        }

        /// <summary>
        /// Gets the subtitle type id that has been setup as the default.
        /// </summary>
        public Guid DefaultSubtitleTypeId
        {
            get { return _defaultSubtitleType != null ? _defaultSubtitleType.Id : Guid.Empty; }
        }

        /// <summary>
        /// Gets the collection of special fields that have been defined for this dialogue
        /// configuration.
        /// </summary>
        public ModelCollection<DialogueSpecial> Specials
        {
            get { return this._specials; }
        }

        /// <summary>
        /// Gets the collection of subtitle types that have been defined for this dialogue
        /// configuration.
        /// </summary>
        public ModelCollection<DialogueSubtitleType> SubtitleTypes
        {
            get { return this._subtitleTypes; }
        }

        /// <summary>
        /// Gets the collection of voices that have been defined for this dialogue
        /// configuration.
        /// </summary>
        public ModelCollection<DialogueVoice> Voices
        {
            get { return this._voices; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a deep copy of the current instance.
        /// </summary>
        /// <returns>
        /// A deep copy of the current System.Object.
        /// </returns>
        public override object DeepClone()
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Retrieves the audibility whose identifier is equal to the specified id if found;
        /// otherwise, null.
        /// </summary>
        /// <param name="id">
        /// The identifier of the audibility to retrieve.
        /// </param>
        /// <returns>
        /// The dialogue audibility whose identifier is equal to the specified id.
        /// </returns>
        public DialogueAudibility GetAudibility(Guid id)
        {
            foreach (DialogueAudibility audibility in this._audibilities)
            {
                if (audibility.Id != id)
                {
                    continue;
                }

                return audibility;
            }

            return null;
        }

        /// <summary>
        /// Gets the export string of the audibility whose id equals the specified id.
        /// </summary>
        /// <param name="id">
        /// The identifier of the audibility whose export string needs to be retrieved.
        /// </param>
        /// <returns>
        /// The export string of the audibility whose id equals the specified id.
        /// </returns>
        public string GetAudibilityExportString(Guid id)
        {
            foreach (DialogueAudibility audibility in this._audibilities)
            {
                if (Guid.Equals(audibility.Id, id))
                {
                    return audibility.ExportString;
                }
            }

            return String.Empty;
        }

        /// <summary>
        /// Gets the export index of the audibility whose id equals the specified id.
        /// </summary>
        /// <param name="id">
        /// The identifier of the audibility whose export index needs to be retrieved.
        /// </param>
        /// <returns>
        /// The export index of the audibility whose id equals the specified id.
        /// </returns>
        public int GetAudibilityIndex(Guid id)
        {
            foreach (DialogueAudibility audibility in this._audibilities)
            {
                if (Guid.Equals(audibility.Id, id))
                {
                    return audibility.ExportIndex;
                }
            }

            return -1;
        }

        /// <summary>
        /// Retrieves the name of the audibility whose identifier is equal to the specified id
        /// if found; otherwise, null.
        /// </summary>
        /// <param name="id">
        /// The identifier of the audibility whose name is to be retrieved.
        /// </param>
        /// <returns>
        /// The dialogue audibility name whose identifier is equal to the specified id.
        /// </returns>
        public string GetAudibilityName(Guid id)
        {
            foreach (DialogueAudibility audibility in this._audibilities)
            {
                if (Guid.Equals(audibility.Id, id))
                {
                    return audibility.Name;
                }
            }

            return String.Empty;
        }

        /// <summary>
        /// Retrieves the audio type whose identifier is equal to the specified id if found;
        /// otherwise, null.
        /// </summary>
        /// <param name="id">
        /// The identifier of the audio type to retrieve.
        /// </param>
        /// <returns>
        /// The dialogue audio type whose identifier is equal to the specified id.
        /// </returns>
        public DialogueAudioType GetAudioType(Guid id)
        {
            foreach (DialogueAudioType audioType in this._audioTypes)
            {
                if (audioType.Id != id)
                {
                    continue;
                }

                return audioType;
            }

            return null;
        }

        /// <summary>
        /// Gets the export index of the audio type whose id equals the specified id.
        /// </summary>
        /// <param name="id">
        /// The identifier of the audio type whose export index needs to be retrieved.
        /// </param>
        /// <returns>
        /// The export index of the audio type whose id equals the specified id.
        /// </returns>
        public string GetAudioTypeExportString(Guid id)
        {
            foreach (DialogueAudioType audioType in this._audioTypes)
            {
                if (Guid.Equals(audioType.Id, id))
                {
                    return audioType.ExportString;
                }
            }

            return String.Empty;
        }

        /// <summary>
        /// Gets the export index of the audio type whose id equals the specified id.
        /// </summary>
        /// <param name="id">
        /// The identifier of the audio type whose export index needs to be retrieved.
        /// </param>
        /// <returns>
        /// The export index of the audio type whose id equals the specified id.
        /// </returns>
        public int GetAudioTypeIndex(Guid id)
        {
            foreach (DialogueAudioType audioType in this._audioTypes)
            {
                if (Guid.Equals(audioType.Id, id))
                {
                    return audioType.ExportIndex;
                }
            }

            return -1;
        }

        /// <summary>
        /// Retrieves the name of the audio type whose identifier is equal to the specified id
        /// if found; otherwise, null.
        /// </summary>
        /// <param name="id">
        /// The identifier of the audio type whose name is to be retrieved.
        /// </param>
        /// <returns>
        /// The dialogue audio types name whose identifier is equal to the specified id.
        /// </returns>
        public string GetAudioTypeName(Guid id)
        {
            foreach (DialogueAudioType audioType in this._audioTypes)
            {
                if (Guid.Equals(audioType.Id, id))
                {
                    return audioType.Name;
                }
            }

            return String.Empty;
        }

        /// <summary>
        /// Retrieves the dialogue character whose identifier is equal to the specified id if
        /// found; otherwise, null.
        /// </summary>
        /// <param name="id">
        /// The identifier of the character to retrieve.
        /// </param>
        /// <returns>
        /// The dialogue character whose identifier is equal to the specified id.
        /// </returns>
        public DialogueCharacter GetCharacter(Guid id)
        {
            DialogueCharacter result;
            if (this._characterLookup.TryGetValue(id, out result))
            {
                return result;
            }

            foreach (DialogueCharacter character in this._characters)
            {
                if (character.Id != id)
                {
                    continue;
                }

                return character;
            }

            return null;
        }

        /// <summary>
        /// Retrieves the dialogue characters id whose name is equal to the specified name if
        /// found; otherwise, the empty id.
        /// </summary>
        /// <param name="name">
        /// The name of the character whose id is to be retrieved.
        /// </param>
        /// <returns>
        /// The dialogue characters id whose name is equal to the specified name.
        /// </returns>
        public Guid GetCharacterId(string name)
        {
            foreach (DialogueCharacter character in this._characters)
            {
                if (!String.Equals(character.Name, name))
                {
                    continue;
                }

                return character.Id;
            }

            return Guid.Empty;
        }

        /// <summary>
        /// Retrieves the name of the character whose identifier is equal to the specified id
        /// if found; otherwise, null.
        /// </summary>
        /// <param name="id">
        /// The identifier of the character whose name is to be retrieved.
        /// </param>
        /// <returns>
        /// The dialogue characters name whose identifier is equal to the specified id.
        /// </returns>
        public string GetCharacterName(Guid id)
        {
            DialogueCharacter result;
            if (this._characterLookup.TryGetValue(id, out result))
            {
                return result.Name;
            }

            foreach (DialogueCharacter character in this._characters)
            {
                if (character.Id != id)
                {
                    continue;
                }

                return character.Name;
            }

            return null;
        }

        /// <summary>
        /// Retrieves the special whose identifier is equal to the specified id if found;
        /// otherwise, null.
        /// </summary>
        /// <param name="id">
        /// The identifier of the special to retrieve.
        /// </param>
        /// <returns>
        /// The dialogue special whose identifier is equal to the specified id.
        /// </returns>
        public DialogueSpecial GetSpecial(Guid id)
        {
            foreach (DialogueSpecial special in this._specials)
            {
                if (special.Id != id)
                {
                    continue;
                }

                return special;
            }

            return null;
        }

        /// <summary>
        /// Retrieves the name of the special whose identifier is equal to the specified id
        /// if found; otherwise, null.
        /// </summary>
        /// <param name="id">
        /// The identifier of the special whose name is to be retrieved.
        /// </param>
        /// <returns>
        /// The dialogue specials name whose identifier is equal to the specified id.
        /// </returns>
        public string GetSpecialName(Guid id)
        {
            foreach (DialogueSpecial special in this._specials)
            {
                if (special.Id != id)
                {
                    continue;
                }

                return special.Name;
            }

            return String.Empty;
        }

        /// <summary>
        /// Retrieves the subtitle type whose identifier is equal to the specified id if found;
        /// otherwise, null.
        /// </summary>
        /// <param name="id">
        /// The identifier of the subtitle type to retrieve.
        /// </param>
        /// <returns>
        /// The dialogue subtitle type whose identifier is equal to the specified id.
        /// </returns>
        public DialogueSubtitleType GetSubtitleType(Guid id)
        {
            foreach (DialogueSubtitleType subtitleType in this._subtitleTypes)
            {
                if (subtitleType.Id != id)
                {
                    continue;
                }

                return subtitleType;
            }

            return null;
        }

        /// <summary>
        /// Gets the export index of the subtitle type whose id equals the specified id.
        /// </summary>
        /// <param name="id">
        /// The identifier of the subtitle type whose export index needs to be retrieved.
        /// </param>
        /// <returns>
        /// The export index of the subtitle type whose id equals the specified id.
        /// </returns>
        public string GetSubtitleTypeExportString(Guid id)
        {
            foreach (DialogueSubtitleType subtitleType in this._subtitleTypes)
            {
                if (Guid.Equals(subtitleType.Id, id))
                {
                    return subtitleType.ExportString;
                }
            }

            return String.Empty;
        }

        /// <summary>
        /// Loads the characters using the xml data contained in the specified path.
        /// </summary>
        /// <param name="filepath">
        /// The full file path to the file to load the configurations from.
        /// </param>
        public void LoadConfigurations(string filepath)
        {
            this._characters.CollectionChanged -= this.CharactersChanged;
            this._characters.Clear();
            this._audibilities.Clear();
            this._audioTypes.Clear();
            this._characters.Clear();
            this._specials.Clear();
            this._voices.Clear();

            if (!File.Exists(filepath))
            {
                return;
            }

            Guid defaultAudibilityId = Guid.Empty;
            Guid defaultAudioTypeId = Guid.Empty;
            Guid defaultSpecialId = Guid.Empty;
            Guid defaultSubtitleTypeId = Guid.Empty;
            using (XmlReader reader = XmlReader.Create(filepath))
            {
                reader.MoveToContent();
                while (reader.MoveToContent() == XmlNodeType.Element)
                {
                    if (String.Equals(reader.Name, "Configurations"))
                    {
                        reader.Read();
                    }
                    else if (String.Equals(reader.Name, "Characters"))
                    {
                        this.DeserialiseCharacters(reader);
                    }
                    else if (String.Equals(reader.Name, "Voices"))
                    {
                        this.DeserialiseVoices(reader);
                    }
                    else if (String.Equals(reader.Name, "Specials"))
                    {
                        this.DeserialiseSpecials(reader);
                    }
                    else if (String.Equals(reader.Name, "AudioTypes"))
                    {
                        this.DeserialiseAudioTypes(reader);
                    }
                    else if (String.Equals(reader.Name, "Audibilities"))
                    {
                        this.DeserialiseAudibilities(reader);
                    }
                    else if (String.Equals(reader.Name, "SubtitleTypes"))
                    {
                        this.DeserialiseSubtitleTypes(reader);
                    }
                    else if (String.Equals(reader.Name, "DefaultAudibility"))
                    {
                        string input = reader.ReadElementContentAsString();
                        Guid.TryParseExact(input, "D", out defaultAudibilityId);
                    }
                    else if (String.Equals(reader.Name, "DefaultAudioType"))
                    {
                        string input = reader.ReadElementContentAsString();
                        Guid.TryParseExact(input, "D", out defaultAudioTypeId);
                    }
                    else if (String.Equals(reader.Name, "DefaultSpecial"))
                    {
                        string input = reader.ReadElementContentAsString();
                        Guid.TryParseExact(input, "D", out defaultSpecialId);
                    }
                    else if (String.Equals(reader.Name, "DefaultSubtitleType"))
                    {
                        string input = reader.ReadElementContentAsString();
                        Guid.TryParseExact(input, "D", out defaultSubtitleTypeId);
                    }

                    reader.Skip();
                }
            }

            foreach (DialogueAudibility audibility in this._audibilities)
            {
                if (audibility.Id == defaultAudibilityId)
                {
                    this._defaultAudibility = audibility;
                    break;
                }
            }

            foreach (DialogueAudioType audioType in this._audioTypes)
            {
                if (audioType.Id == defaultAudioTypeId)
                {
                    this._defaultAudioType = audioType;
                    break;
                }
            }

            foreach (DialogueSpecial special in this._specials)
            {
                if (special.Id == defaultSpecialId)
                {
                    this._defaultSpecial = special;
                    break;
                }
            }

            foreach (DialogueSubtitleType subtitleType in this._subtitleTypes)
            {
                if (subtitleType.Id == defaultSubtitleTypeId)
                {
                    this._defaultSubtitleType = subtitleType;
                    break;
                }
            }

            this._defaultAudibility =
                this._defaultAudibility ?? this._audibilities.FirstOrDefault();
            this._defaultAudioType =
                this._defaultAudioType ?? this._audioTypes.FirstOrDefault();
            this._defaultSpecial =
                this._defaultSpecial ?? this._specials.FirstOrDefault();
            this._defaultSubtitleType =
                this._defaultSubtitleType ?? this._subtitleTypes.FirstOrDefault();

            this._characters.CollectionChanged += this.CharactersChanged;
            this.UpdateCharacterLookup();
        }

        /// <summary>
        /// Serialises the data contained within this instance onto the specified xml writer.
        /// </summary>
        /// <param name="writer">
        /// The xml writer this instance will be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                Debug.Assert(writer != null, "Unable to serialise into a null writer.");
                return;
            }

            writer.WriteStartElement("Characters");
            foreach (DialogueCharacter character in this._characters)
            {
                writer.WriteStartElement("Item");
                character.Serialise(writer);
                writer.WriteEndElement();
            }

            writer.WriteEndElement();

            writer.WriteStartElement("Voices");
            foreach (DialogueVoice voice in this._voices)
            {
                writer.WriteStartElement("Item");
                voice.Serialise(writer);
                writer.WriteEndElement();
            }

            writer.WriteEndElement();

            writer.WriteStartElement("Specials");
            foreach (DialogueSpecial special in this._specials)
            {
                writer.WriteStartElement("Item");
                special.Serialise(writer);
                writer.WriteEndElement();
            }

            writer.WriteEndElement();

            writer.WriteStartElement("AudioTypes");
            foreach (DialogueAudioType audioType in this._audioTypes)
            {
                writer.WriteStartElement("Item");
                audioType.Serialise(writer);
                writer.WriteEndElement();
            }

            writer.WriteEndElement();

            writer.WriteStartElement("Audibilities");
            foreach (DialogueAudibility audibility in this._audibilities)
            {
                writer.WriteStartElement("Item");
                audibility.Serialise(writer);
                writer.WriteEndElement();
            }

            writer.WriteEndElement();

            writer.WriteStartElement("SubtitleTypes");
            foreach (DialogueSubtitleType subtitleType in this._subtitleTypes)
            {
                writer.WriteStartElement("Item");
                subtitleType.Serialise(writer);
                writer.WriteEndElement();
            }

            writer.WriteEndElement();

            if (this._defaultSpecial != null)
            {
                writer.WriteElementString(
                    "DefaultSpecial", this._defaultSpecial.Id.ToString("D"));
            }

            if (this._defaultAudioType != null)
            {
                writer.WriteElementString(
                    "DefaultAudioType", this._defaultAudioType.Id.ToString("D"));
            }

            if (this._defaultAudibility != null)
            {
                writer.WriteElementString(
                    "DefaultAudibility", this._defaultAudibility.Id.ToString("D"));
            }

            if (this._defaultSubtitleType != null)
            {
                writer.WriteElementString(
                    "DefaultSubtitleType", this._defaultSubtitleType.Id.ToString("D"));
            }
        }

        /// <summary>
        /// Called whenever the collection for the characters has a item added or removed from
        /// it so that a master character lookup can be created.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Collections.Specialized.NotifyCollectionChangedEventArgs containing the
        /// event data.
        /// </param>
        private void CharactersChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            this.UpdateCharacterLookup();
        }

        /// <summary>
        /// Creates the audibility fields for this configuration using the data contained
        /// within the specified xml reader.
        /// </summary>
        /// <param name="reader">
        /// The xml reader that contains the data used to initialise the audibility fields for
        /// this configuration.
        /// </param>
        private void DeserialiseAudibilities(XmlReader reader)
        {
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            int previousEportIndex = -1;
            reader.ReadStartElement();
            while (reader.MoveToContent() == XmlNodeType.Element)
            {
                if (String.Equals("Item", reader.Name))
                {
                    DialogueAudibility item = new DialogueAudibility(reader);
                    if (item.ExportIndex == -1)
                    {
                        item.ExportIndex = previousEportIndex + 1;
                    }

                    this._audibilities.Add(item);
                    previousEportIndex = item.ExportIndex;
                }
            }

            if (reader.NodeType == XmlNodeType.EndElement)
            {
                reader.ReadEndElement();
            }
        }

        /// <summary>
        /// Creates the audio types for this configuration using the data contained within the
        /// specified xml reader.
        /// </summary>
        /// <param name="reader">
        /// The xml reader that contains the data used to initialise the audio types for this
        /// configuration.
        /// </param>
        private void DeserialiseAudioTypes(XmlReader reader)
        {
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            int previousEportIndex = -1;
            reader.ReadStartElement();
            while (reader.MoveToContent() == XmlNodeType.Element)
            {
                if (String.Equals("Item", reader.Name))
                {
                    DialogueAudioType item = new DialogueAudioType(reader);
                    if (item.ExportIndex == -1)
                    {
                        item.ExportIndex = previousEportIndex + 1;
                    }

                    this._audioTypes.Add(item);
                    previousEportIndex = item.ExportIndex;
                }
            }

            if (reader.NodeType == XmlNodeType.EndElement)
            {
                reader.ReadEndElement();
            }
        }

        /// <summary>
        /// Creates the characters for this configuration using the data contained within the
        /// specified xml reader.
        /// </summary>
        /// <param name="reader">
        /// The xml reader that contains the data used to initialise the characters for this
        /// configuration.
        /// </param>
        private void DeserialiseCharacters(XmlReader reader)
        {
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() == XmlNodeType.Element)
            {
                if (String.Equals("Item", reader.Name))
                {
                    this._characters.Add(new DialogueCharacter(reader));
                }
            }

            if (reader.NodeType == XmlNodeType.EndElement)
            {
                reader.ReadEndElement();
            }
        }

        /// <summary>
        /// Creates the special fields for this configuration using the data contained within
        /// the specified xml reader.
        /// </summary>
        /// <param name="reader">
        /// The xml reader that contains the data used to initialise the special fields for
        /// this configuration.
        /// </param>
        private void DeserialiseSpecials(XmlReader reader)
        {
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() == XmlNodeType.Element)
            {
                if (String.Equals("Item", reader.Name))
                {
                    this._specials.Add(new DialogueSpecial(reader));
                }
            }

            if (reader.NodeType == XmlNodeType.EndElement)
            {
                reader.ReadEndElement();
            }
        }

        /// <summary>
        /// Creates the subtitle types for this configuration using the data contained within
        /// the specified xml reader.
        /// </summary>
        /// <param name="reader">
        /// The xml reader that contains the data used to initialise the subtitle types for
        /// this configuration.
        /// </param>
        private void DeserialiseSubtitleTypes(XmlReader reader)
        {
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() == XmlNodeType.Element)
            {
                if (String.Equals("Item", reader.Name))
                {
                    this._subtitleTypes.Add(new DialogueSubtitleType(reader));
                }
            }

            if (reader.NodeType == XmlNodeType.EndElement)
            {
                reader.ReadEndElement();
            }
        }

        /// <summary>
        /// Creates the voices for this configuration using the data contained within the
        /// specified xml reader.
        /// </summary>
        /// <param name="reader">
        /// The xml reader that contains the data used to initialise the voices for this
        /// configuration.
        /// </param>
        private void DeserialiseVoices(XmlReader reader)
        {
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() == XmlNodeType.Element)
            {
                if (String.Equals("Item", reader.Name))
                {
                    this._voices.Add(new DialogueVoice(reader));
                }
            }

            if (reader.NodeType == XmlNodeType.EndElement)
            {
                reader.ReadEndElement();
            }
        }

        /// <summary>
        /// Updates the character lookup table by first clearing it.
        /// </summary>
        private void UpdateCharacterLookup()
        {
            this._characterLookup.Clear();
            foreach (DialogueCharacter character in this._characters)
            {
                if (this._characterLookup.ContainsKey(character.Id))
                {
                    Debug.Assert(false, "Two or more characters have the same id.");
                    continue;
                }

                this._characterLookup.Add(character.Id, character);
            }
        }
        #endregion Methods
    } // RSG.Text.Model.DialogueConfigurations {Class}
} // RSG.Text.Model {Namespace}
