﻿//---------------------------------------------------------------------------------------------
// <copyright file="IDynamicTunableParent.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Tunables
{
    using System;
    using System.Xml;
    using RSG.Base.Logging;
    using RSG.Editor.Model;
    using RSG.Metadata.Model.Definitions.Members;

    /// <summary>
    /// When implemented represents a instance of a specific parCodeGen member object that
    /// can contain additional instances whose children can be dynamically added and removed.
    /// </summary>
    public interface IDynamicTunableParent : ITunableParent
    {
        #region Properties
        /// <summary>
        /// Gets the element type that this dynamic tunables children are instancing.
        /// </summary>
        IMember ElementType { get; }

        /// <summary>
        /// Gets the number of items currently contained inside this tunables child collection.
        /// </summary>
        int ItemCount { get; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Adds a new item to this tunable and returns it. The item added is a default
        /// instance of the child element type.
        /// </summary>
        /// <returns>
        /// The item that has been added.
        /// </returns>
        ITunable AddNewItem();

        /// <summary>
        /// Gets the zero-based index for the specified tunable within this tunables items
        /// collection.
        /// </summary>
        /// <param name="tunable">
        /// The tunable whose index will be retrieved.
        /// </param>
        /// <returns>
        /// The index of the specified tunable within this tunables items collection if found;
        /// otherwise, -1.
        /// </returns>
        int IndexOf(ITunable tunable);

        /// <summary>
        /// Adds a new item to this tunable and returns it. The item added is a default
        /// instance of the child element type.
        /// </summary>
        /// <param name="index">
        /// The zero-based index at which the item should be inserted.
        /// </param>
        /// <returns>
        /// The item that has been added.
        /// </returns>
        ITunable InsertNewItem(int index);

        /// <summary>
        /// Adds a new item to this tunable and returns it. The item added is initialised with
        /// the data in the specified xml reader.
        /// </summary>
        /// <param name="index">
        /// The zero-based index at which the item should be inserted.
        /// </param>
        /// <param name="reader">
        /// The reader that contains the data to initialise the new item.
        /// </param>
        /// <param name="log">
        /// A object that is used to log any warnings or errors produced during the parsing of
        /// the specified reader.
        /// </param>
        /// <returns>
        /// The item that has been added.
        /// </returns>
        ITunable InsertNewItem(int index, XmlReader reader, ILog log);

        /// <summary>
        /// Moves the item at the specified index to a new location in the collection.
        /// </summary>
        /// <param name="oldIndex">
        /// The zero-based index specifying the location of the item to be moved.
        /// </param>
        /// <param name="newIndex">
        /// The zero-based index specifying the new location of the item.
        /// </param>
        void MoveItem(int oldIndex, int newIndex);

        /// <summary>
        /// Removes the first occurrence of the specified object from this tunables child
        /// collection.
        /// </summary>
        /// <param name="item">
        /// The tunable to remove from this tunables child collection.
        /// </param>
        void RemoveItem(ITunable item);

        /// <summary>
        /// Removes the child tunable at the specified index.
        /// </summary>
        /// <param name="index">
        /// The zero-based index of the tunable to remove.
        /// </param>
        void RemoveItemAt(int index);
        #endregion Methods
    } // RSG.Metadata.Model.Tunables.IDynamicTunableParent {Interface}
} // RSG.Metadata.Model.Tunables {Namespace}
