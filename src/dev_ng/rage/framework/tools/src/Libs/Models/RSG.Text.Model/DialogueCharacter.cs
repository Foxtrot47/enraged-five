﻿//---------------------------------------------------------------------------------------------
// <copyright file="DialogueCharacter.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.Model
{
    using System;
    using System.Diagnostics;
    using System.Xml;
    using RSG.Editor;
    using RSG.Editor.Model;

    /// <summary>
    /// Represents a single character within the dialogue configurations.
    /// </summary>
    [DebuggerDisplay("{_name}")]
    public sealed class DialogueCharacter : ModelBase
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="DontExport"/> property.
        /// </summary>
        private bool _dontExport;

        /// <summary>
        /// The private field used for the <see cref="Id"/> property.
        /// </summary>
        private Guid _id;

        /// <summary>
        /// The private field used for the <see cref="Name"/> property.
        /// </summary>
        private string _name;

        /// <summary>
        /// The private field used for the <see cref="UsesManualFilenames"/> property.
        /// </summary>
        private bool _usesManualFilenames;

        /// <summary>
        /// The private field used for the <see cref="Voice"/> property.
        /// </summary>
        private Guid _voice;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DialogueCharacter"/> class.
        /// </summary>
        /// <param name="id">
        /// The static id for this character that cannot be changed.
        /// </param>
        /// <param name="name">
        /// The name for this character.
        /// </param>
        public DialogueCharacter(Guid id, string name)
        {
            this._id = id;
            this._name = name;
            this._voice = Guid.Empty;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="DialogueCharacter"/> class.
        /// </summary>
        /// <param name="id">
        /// The static id for this character that cannot be changed.
        /// </param>
        /// <param name="name">
        /// The name for this character.
        /// </param>
        /// <param name="voice">
        /// The identifier to the voice that is used to synthesize audio lines spoken by this
        /// character.
        /// </param>
        public DialogueCharacter(Guid id, string name, Guid voice)
        {
            this._id = id;
            this._name = name;
            this._voice = voice;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="DialogueCharacter"/> class.
        /// </summary>
        /// <param name="reader">
        /// Contains data used to initialise this instance.
        /// </param>
        public DialogueCharacter(XmlReader reader)
        {
            if (reader == null)
            {
                throw new SmartArgumentNullException(() => reader);
            }

            this.Deserialise(reader);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether a line using this character should be
        /// exported or ignored.
        /// </summary>
        public bool DontExport
        {
            get { return this._dontExport; }
            set { this.SetProperty(ref this._dontExport, value); }
        }

        /// <summary>
        /// Gets the id used by this character. This cannot be changed once setup and is how
        /// the character can be renamed without any data being lost or manipulated.
        /// </summary>
        public Guid Id
        {
            get { return this._id; }
        }

        /// <summary>
        /// Gets or sets the name for this character.
        /// </summary>
        public string Name
        {
            get { return this._name; }
            set { this.SetProperty(ref this._name, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this character uses manually entered
        /// filenames or not.
        /// </summary>
        public bool UsesManualFilenames
        {
            get { return this._usesManualFilenames; }
            set { this.SetProperty(ref this._usesManualFilenames, value); }
        }

        /// <summary>
        /// Gets or sets the identifier to the voice used for this character. This is the voice
        /// that is used to synthesize the line using built in audio voices.
        /// </summary>
        public Guid Voice
        {
            get { return this._voice; }
            set { this.SetProperty(ref this._voice, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a deep copy of the current instance.
        /// </summary>
        /// <returns>
        /// A deep copy of the current System.Object.
        /// </returns>
        public override object DeepClone()
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Serialises the data contained within this instance onto the specified xml writer.
        /// </summary>
        /// <param name="writer">
        /// The xml writer this instance will be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                Debug.Assert(writer != null, "Unable to serialise into a null writer.");
                return;
            }

            writer.WriteElementString("Name", this._name);
            writer.WriteElementString("Id", this._id.ToString("D"));

            if (this._voice != Guid.Empty)
            {
                writer.WriteElementString("Voice", this._voice.ToString("D"));
            }

            writer.WriteStartElement("DontExport");
            writer.WriteAttributeString("value", this._dontExport.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("UsesManualFilenames");
            writer.WriteAttributeString("value", this._usesManualFilenames.ToString());
            writer.WriteEndElement();
        }

        /// <summary>
        /// Initialises this instance using the data contained within the specified xml reader.
        /// </summary>
        /// <param name="reader">
        /// The xml reader that contains the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() == XmlNodeType.Element)
            {
                if (String.Equals("Name", reader.Name))
                {
                    this._name = reader.ReadElementContentAsString();
                }
                else if (String.Equals("Id", reader.Name))
                {
                    string value = reader.ReadElementContentAsString();
                    if (!Guid.TryParseExact(value, "D", out this._id))
                    {
                        this._id = Guid.Empty;
                    }
                }
                else if (String.Equals("Voice", reader.Name))
                {
                    string value = reader.ReadElementContentAsString();
                    if (!Guid.TryParseExact(value, "D", out this._voice))
                    {
                        this._voice = Guid.Empty;
                    }
                }
                else if (String.Equals("DontExport", reader.Name))
                {
                    string value = reader.GetAttribute("value");
                    if (!bool.TryParse(value, out this._dontExport))
                    {
                        this._dontExport = false;
                    }
                }
                else if (String.Equals("UsesManualFilenames", reader.Name))
                {
                    string value = reader.GetAttribute("value");
                    if (!bool.TryParse(value, out this._usesManualFilenames))
                    {
                        this._usesManualFilenames = false;
                    }
                }

                reader.Skip();
            }

            if (reader.NodeType == XmlNodeType.EndElement)
            {
                reader.ReadEndElement();
            }
        }
        #endregion Methods
    } // RSG.Text.Model.DialogueCharacter {Class}
} // RSG.Text.Model {Namespace}
