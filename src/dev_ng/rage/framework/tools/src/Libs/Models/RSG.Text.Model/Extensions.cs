﻿//---------------------------------------------------------------------------------------------
// <copyright file="Extensions.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.Model
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Xml;
    using RSG.Base.Extensions;
    using RSG.Editor.Model;

    /// <summary>
    /// A extensions class for types inside the RSG.Text.Model assembly.
    /// </summary>
    public static class Extensions
    {
        #region Methods
        /// <summary>
        /// Loads the data inside the file with the specified full path assuming it is in the
        /// old .xml format.
        /// </summary>
        /// <param name="config">
        /// The configuration object to add the loaded items to.
        /// </param>
        /// <param name="fullPath">
        /// The full path to the file to load.
        /// </param>
        public static void LoadOldFormat(this DialogueConfigurations config, string fullPath)
        {
            config.Audibilities.Clear();
            config.AudioTypes.Clear();
            config.Characters.Clear();
            config.Specials.Clear();
            config.Voices.Clear();

            config.AudioTypes.Add(
                new DialogueAudioType(Guid.NewGuid(), "Normal", 0, "AUD_SPEECH_NORMAL"));
            config.AudioTypes.Add(
                new DialogueAudioType(Guid.NewGuid(), "Shouted", 1, "AUD_SPEECH_SHOUT"));
            config.AudioTypes.Add(
                new DialogueAudioType(Guid.NewGuid(), "FrontEnd", 2, "AUD_SPEECH_FRONTEND"));
            config.AudioTypes.Add(
                new DialogueAudioType(Guid.NewGuid(), "MegaPhone", 3, "AUD_SPEECH_MEGAPHONE"));

            config.Audibilities.Add(
                new DialogueAudibility(Guid.NewGuid(), "Normal", 0, "AUD_AUDIBILITY_NORMAL"));
            config.Audibilities.Add(
                new DialogueAudibility(Guid.NewGuid(), "Clear", 1, "AUD_AUDIBILITY_CLEAR"));
            config.Audibilities.Add(
                new DialogueAudibility(
                    Guid.NewGuid(), "Critical", 2, "AUD_AUDIBILITY_CRITICAL"));
            config.Audibilities.Add(
                new DialogueAudibility(Guid.NewGuid(), "LeadIn", 3, "AUD_AUDIBILITY_LEAD_IN"));

            Dictionary<Guid, string> characterVoices = new Dictionary<Guid, string>();
            using (XmlReader reader = XmlReader.Create(fullPath))
            {
                while (reader.Read())
                {
                    if (reader.IsStartElement("Character"))
                    {
                        string value = reader.GetAttribute("guid");
                        string name = reader.GetAttribute("name");
                        string voice = reader.GetAttribute("voice");
                        System.Guid id;
                        if (!System.Guid.TryParse(value, out id))
                        {
                            continue;
                        }

                        DialogueCharacter character = new DialogueCharacter(id, name);
                        if (name == "SFX")
                        {
                            character.DontExport = true;
                            character.UsesManualFilenames = true;
                        }

                        config.Characters.Add(character);
                        characterVoices.Add(id, voice);
                    }
                    else if (reader.IsStartElement("Voice"))
                    {
                        string name = reader.ReadElementContentAsString();
                        config.Voices.Add(new DialogueVoice(Guid.NewGuid(), name));
                    }
                    else if (reader.IsStartElement("Special"))
                    {
                        string name = reader.ReadElementContentAsString();
                        DialogueSpecial special = new DialogueSpecial(Guid.NewGuid(), name);
                        if (String.Equals(name, "Headset"))
                        {
                            special.Flags = DialogueSpecialFlags.Headset;
                        }
                        else if (String.Equals(name, "Pad_Speaker"))
                        {
                            special.Flags = DialogueSpecialFlags.PadSpeaker;
                        }

                        config.Specials.Add(new DialogueSpecial(Guid.NewGuid(), name));
                    }
                }
            }

            foreach (DialogueCharacter character in config.Characters)
            {
                string voiceName = null;
                if (characterVoices.TryGetValue(character.Id, out voiceName))
                {
                    foreach (DialogueVoice voice in config.Voices)
                    {
                        if (!String.Equals(voiceName, voice.Name))
                        {
                            continue;
                        }

                        character.Voice = voice.Id;
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Loads the data inside the stream assuming it is in the old dialogue star format.
        /// </summary>
        /// <param name="dialogue">
        /// The dialogue object to add the loaded items to.
        /// </param>
        /// <param name="reader">
        /// The reader containing the data to load.
        /// </param>
        public static void LoadOldFormat(this Dialogue dialogue, XmlReader reader)
        {
            while (reader.Read())
            {
                if (reader.IsStartElement("MissionDialogue"))
                {
                    string multiplayerValue = reader.GetAttribute("multiplayer");
                    bool multiplayer = false;
                    if (!bool.TryParse(multiplayerValue, out multiplayer))
                    {
                        multiplayer = false;
                    }

                    string ambientValue = reader.GetAttribute("ambient");
                    bool ambient = false;
                    if (!bool.TryParse(multiplayerValue, out ambient))
                    {
                        ambient = false;
                    }

                    dialogue.Id = reader.GetAttribute("id");
                    dialogue.Name = reader.GetAttribute("name");
                    dialogue.SubtitleId = reader.GetAttribute("subtitle");
                    dialogue.Type = reader.GetAttribute("type");
                    dialogue.AmbientFile = ambient;
                    dialogue.Multiplayer = multiplayer;
                    while (reader.Read())
                    {
                        if (reader.IsStartElement("Conversation"))
                        {
                            Conversation conversation = dialogue.AddNewConversation();
                            conversation.LoadOldFormat(reader);
                        }
                        else if (reader.IsStartElement("Filename"))
                        {
                            string filename = reader.ReadElementContentAsString();
                            dialogue.DeletedFilenames.Add(filename);
                        }
                    }
                }
            }

            dialogue.DetermineConversationIndices();
        }

        /// <summary>
        /// Serialises the data in the model onto the specified xml writer in the old dialogue
        /// star file format.
        /// </summary>
        /// <param name="dialogue">
        /// The dialogue object to serialise.
        /// </param>
        /// <param name="writer">
        /// The xml writer the specified dialogue object should be serialised to.
        /// </param>
        public static void SaveOldFormat(this Dialogue dialogue, XmlWriter writer)
        {
            writer.WriteStartElement("MissionDialogue");
            writer.WriteAttributeString("version", "2.0"); //// Hardcode last supported version
            writer.WriteAttributeString("id", dialogue.Id.ToString());
            writer.WriteAttributeString("name", dialogue.Name.ToString());
            writer.WriteAttributeString("subtitle", dialogue.SubtitleId.ToString());
            writer.WriteAttributeString("type", dialogue.Type.ToString());
            writer.WriteAttributeString("multiplayer", dialogue.Multiplayer.ToString());
            writer.WriteAttributeString("ambient", dialogue.AmbientFile.ToString());

            writer.WriteStartElement("Conversations");
            foreach (Conversation conversation in dialogue.Conversations)
            {
                conversation.SaveOldFormat(writer);
            }

            writer.WriteEndElement(); //// Conversations

            writer.WriteStartElement("DeletedFilenames");
            foreach (String deletedFilename in dialogue.DeletedFilenames)
            {
                writer.WriteElementString("Filename", deletedFilename);
            }

            writer.WriteEndElement(); //// DeletedFilenames
            writer.WriteEndElement(); //// MissionDialogue
        }

        /// <summary>
        /// Determines the conversations index for all of the conversations inside the given
        /// dialogue object.
        /// </summary>
        /// <param name="dialogue">
        /// The dialogue object whose conversations should have their conversation index
        /// determined.
        /// </param>
        private static void DetermineConversationIndices(this Dialogue dialogue)
        {
            Regex regex = new Regex("\\A" + dialogue.Id + "_[a-zA-Z]{4}\\Z");

            List<Conversation> invalid = new List<Conversation>();
            List<int> indices = new List<int>();
            foreach (Conversation conversation in dialogue.Conversations)
            {
                if (conversation == null)
                {
                    continue;
                }

                int index = conversation.DetermineIndex(regex);
                if (index == -1)
                {
                    invalid.Add(conversation);
                }
                else
                {
                    if (!indices.Contains(index))
                    {
                        conversation.AudioFilepathIndex = index;
                        indices.Add(index);
                    }
                    else
                    {
                        invalid.Add(conversation);
                    }
                }
            }

            foreach (string filename in dialogue.DeletedFilenames)
            {
                string substring = null;
                if (regex.IsMatch(filename))
                {
                    substring = filename.Substring(filename.IndexOf('_') + 1, 2);
                }
                else
                {
                    continue;
                }

                int index = GetIndexFromFilenameString(substring);
                if (!indices.Contains(index))
                {
                    indices.Add(index);
                }
            }

            indices.Sort();
            foreach (Conversation conversation in invalid)
            {
                int index = 0;
                while (indices.Contains(index))
                {
                    index++;
                }

                conversation.AudioFilepathIndex = index;
                indices.Add(index);
            }
        }

        /// <summary>
        /// Determines the index for the specified conversation object.
        /// </summary>
        /// <param name="conversation">
        /// The conversations whose index should be determined.
        /// </param>
        /// <param name="regex">
        /// The regular expression the audio file paths are checked against.
        /// </param>
        /// <returns>
        /// The conversation index for the specified conversation.
        /// </returns>
        private static int DetermineIndex(this Conversation conversation, Regex regex)
        {
            if (conversation.IsRandom)
            {
                ILine line = conversation.Lines.FirstOrDefault();
                if (line == null)
                {
                    return -1;
                }

                if (regex.IsMatch(line.AudioFilepath))
                {
                    string audioPath = line.AudioFilepath;
                    string substring = audioPath.Substring(audioPath.IndexOf('_') + 1, 2);
                    int index = GetIndexFromFilenameString(substring);
                    return index;
                }
                else
                {
                    return -1;
                }
            }
            else
            {
                int index = -1;
                foreach (Line line in conversation.Lines)
                {
                    string audioPath = line.AudioFilepath;
                    if (line == null)
                    {
                        continue;
                    }

                    if (regex.IsMatch(audioPath))
                    {
                        string substring = audioPath.Substring(audioPath.IndexOf('_') + 1, 2);
                        if (index == -1)
                        {
                            index = GetIndexFromFilenameString(substring);
                        }
                        else
                        {
                            int newIndex = GetIndexFromFilenameString(substring);
                            if (newIndex != index)
                            {
                                return -1;
                            }
                        }
                    }
                }

                return index;
            }
        }

        /// <summary>
        /// Gets the index from the specified string. This starts with AA - 0 and ends with
        /// ZZ - 675.
        /// </summary>
        /// <param name="value">
        /// The string value whose index should be returned.
        /// </param>
        /// <returns>
        /// The index equivalent of the specified string.
        /// </returns>
        private static int GetIndexFromFilenameString(string value)
        {
            int mod = (int)value[0];
            int remainder = (int)value[1];
            mod -= 65;
            remainder -= 65;

            return (26 * mod) + remainder;
        }

        /// <summary>
        /// Initialises the properties inside the specified conversation object using the data
        /// in the specified xml reader assuming the old dialogue star format.
        /// </summary>
        /// <param name="conversation">
        /// The conversation to initialise.
        /// </param>
        /// <param name="reader">
        /// The xml reader containing the data.
        /// </param>
        private static void LoadOldFormat(this Conversation conversation, XmlReader reader)
        {
            string placeholderValue = reader.GetAttribute("placeholder");
            bool placeholder = false;
            if (!bool.TryParse(placeholderValue, out placeholder))
            {
                placeholder = false;
            }

            string randomValue = reader.GetAttribute("random");
            bool random = false;
            if (!bool.TryParse(randomValue, out random))
            {
                random = false;
            }

            string lockedValue = reader.GetAttribute("locked");
            bool locked = false;
            if (!bool.TryParse(lockedValue, out locked))
            {
                locked = false;
            }

            string interruptibleValue = reader.GetAttribute("interruptible");
            bool interruptible = false;
            if (!bool.TryParse(interruptibleValue, out interruptible))
            {
                interruptible = true;
            }

            string animValue = reader.GetAttribute("anim");
            bool anim = false;
            if (!bool.TryParse(animValue, out anim))
            {
                anim = false;
            }

            string cutsceneValue = reader.GetAttribute("cutscene");
            bool cutscene = false;
            if (!bool.TryParse(cutsceneValue, out cutscene))
            {
                cutscene = false;
            }

            conversation.Category = reader.GetAttribute("category");
            conversation.CutsceneSubtitles = cutscene;
            conversation.Description = reader.GetAttribute("description");
            conversation.Interruptible = interruptible;
            conversation.IsPlaceholder = placeholder;
            conversation.IsRandom = random;
            conversation.IsTriggeredByAnimation = anim;
            conversation.LockedFilenames = locked;
            conversation.Root = reader.GetAttribute("root");
            while (reader.Read())
            {
                if (reader.IsStartElement("Line"))
                {
                    ILine line = conversation.AddNewLine();
                    using (line.SuspendModifiedTimeUpdater())
                    {
                        line.LoadOldFormat(reader);
                    }
                }
                else if (reader.NodeType == XmlNodeType.EndElement)
                {
                    break;
                }
            }

            Regex endRegex = new Regex("_[0-9]{2}\\Z");
            Regex baseRegex = new Regex("\\A[a-zA-Z0-9]+_[a-zA-Z]{4}\\Z");
            Regex regex = new Regex("\\A[a-zA-Z0-9]+_[a-zA-Z]{4}_[0-9]{2}\\Z");
            if (conversation.IsRandom)
            {
                List<ILine> needsupdating = new List<ILine>();
                List<int> takenIndicies = new List<int>();
                foreach (ILine line in conversation.Lines)
                {
                    if (line.ManualFilename)
                    {
                        continue;
                    }

                    if (!endRegex.IsMatch(line.AudioFilepath))
                    {
                        needsupdating.Add(line);
                    }
                    else
                    {
                        int startIndex = line.AudioFilepath.LastIndexOf('_') + 1;
                        takenIndicies.Add(int.Parse(line.AudioFilepath.Substring(startIndex)));
                    }
                }

                int lineIndex = 0;
                foreach (ILine line in needsupdating)
                {
                    do
                    {
                        lineIndex++;
                    }
                    while (takenIndicies.Contains(lineIndex));
                    using (line.SuspendModifiedTimeUpdater())
                    {
                        line.AudioFilepath += "_" + lineIndex.ToStringInvariant("D2");
                    }
                }
            }
            else
            {
                List<ILine> needsupdating = new List<ILine>();
                List<int> takenIndicies = new List<int>();
                foreach (ILine line in conversation.Lines)
                {
                    if (line.ManualFilename)
                    {
                        if (baseRegex.IsMatch(line.AudioFilepath))
                        {
                            using (line.SuspendModifiedTimeUpdater())
                            {
                                line.ManualFilename = false;
                            }
                        }
                        else if (regex.IsMatch(line.AudioFilepath))
                        {
                            using (line.SuspendModifiedTimeUpdater())
                            {
                                line.ManualFilename = false;
                            }
                        }
                        else
                        {
                            continue;
                        }
                    }

                    if (!endRegex.IsMatch(line.AudioFilepath))
                    {
                        using (line.SuspendModifiedTimeUpdater())
                        {
                            line.AudioFilepath += "_01";
                        }
                    }
                }
            }

            foreach (ILine line in conversation.Lines)
            {
                if (regex.IsMatch(line.AudioFilepath))
                {
                    using (line.SuspendModifiedTimeUpdater())
                    {
                        line.ManualFilename = false;
                    }
                }
            }
        }

        /// <summary>
        /// Initialises the properties inside the specified line object using the data in the
        /// specified xml reader assuming the old dialogue star format.
        /// </summary>
        /// <param name="line">
        /// The line to initialise.
        /// </param>
        /// <param name="reader">
        /// The xml reader containing the data.
        /// </param>
        private static void LoadOldFormat(this ILine line, XmlReader reader)
        {
            string guidValue = reader.GetAttribute("guid");
            Guid guid = Guid.Empty;
            if (!Guid.TryParse(guidValue, out guid))
            {
                guid = Guid.Empty;
            }

            string listenerValue = reader.GetAttribute("listener");
            int listener = 0;
            if (!int.TryParse(listenerValue, out listener))
            {
                if (listenerValue.Length > 0)
                {
                    int characterValue = ((int)Char.ToUpper(listenerValue[0])) - 65;
                    if (characterValue >= 0 && characterValue <= 25)
                    {
                        listener = characterValue + 10;
                    }
                    else
                    {
                        listener = 0;
                    }
                }
                else
                {
                    listener = 0;
                }
            }

            string manualValue = reader.GetAttribute("manual");
            bool manual = false;
            if (!bool.TryParse(manualValue, out manual))
            {
                manual = false;
            }

            string recordedValue = reader.GetAttribute("recorded");
            bool recorded = false;
            if (!bool.TryParse(recordedValue, out recorded))
            {
                recorded = false;
            }

            string sentValue = reader.GetAttribute("sent");
            bool sent = false;
            if (!bool.TryParse(sentValue, out sent))
            {
                sent = false;
            }

            string subtitledValue = reader.GetAttribute("subtitled");
            bool subtitled = false;
            if (!bool.TryParse(subtitledValue, out subtitled))
            {
                subtitled = false;
            }

            string interruptibleValue = reader.GetAttribute("interruptible");
            bool interruptible = false;
            if (!bool.TryParse(interruptibleValue, out interruptible))
            {
                interruptible = true;
            }

            string dontInterruptValue = reader.GetAttribute("dontInterruptForSpecialAbility");
            bool dontInterruptForSpecialAbility = false;
            if (!bool.TryParse(dontInterruptValue, out dontInterruptForSpecialAbility))
            {
                dontInterruptForSpecialAbility = false;
            }

            string ducksScoreValue = reader.GetAttribute("ducks_score");
            bool ducksScore = false;
            if (!bool.TryParse(ducksScoreValue, out ducksScore))
            {
                ducksScore = false;
            }

            string ducksRadioValue = reader.GetAttribute("ducks_radio");
            bool ducksRadio = false;
            if (!bool.TryParse(ducksRadioValue, out ducksRadio))
            {
                ducksRadio = true;
            }

            string headsetSubmixValue = reader.GetAttribute("headset_submix");
            bool headsetSubmix = false;
            if (!bool.TryParse(headsetSubmixValue, out headsetSubmix))
            {
                headsetSubmix = false;
            }

            string audio = reader.GetAttribute("audio");
            string special = reader.GetAttribute("special");
            string audibility = reader.GetAttribute("audibility");

            string timestampValue = reader.GetAttribute("timestamp");
            DateTime timestamp = DateTime.Now;
            DateTime time = DateTime.Now;
            CultureInfo info = CultureInfo.CreateSpecificCulture("en-GB");
            if (DateTime.TryParse(timestampValue, info, DateTimeStyles.None, out time))
            {
                timestamp = time;
            }
            else
            {
                info = CultureInfo.CreateSpecificCulture("en-US");
                if (DateTime.TryParse(timestampValue, info, DateTimeStyles.None, out time))
                {
                    timestamp = time;
                }
            }

            line.CharacterId = guid;
            line.Dialogue = reader.GetAttribute("dialogue");
            line.Listener = listener;
            line.AudioFilepath = reader.GetAttribute("filename");
            line.Recorded = recorded;
            line.SentForRecording = sent;
            line.AlwaysSubtitled = subtitled;
            line.Interruptible = interruptible;
            line.DontInterruptForSpecialAbility = dontInterruptForSpecialAbility;
            line.DucksScore = ducksScore;
            line.DucksRadio = ducksRadio;
            line.HeadsetSubmix = headsetSubmix;

            DialogueConfigurations config = line.Configurations;
            DialogueCharacter character = config.GetCharacter(guid);
            if (character != null && character.UsesManualFilenames)
            {
                line.ManualFilename = false;
            }
            else
            {
                line.ManualFilename = manual;
            }

            if (audio == null)
            {
                line.AudioType = config.AudioTypes.First().Id;
            }
            else
            {
                foreach (DialogueAudioType dialogueAudioType in config.AudioTypes)
                {
                    if (!String.Equals(audio, dialogueAudioType.Name))
                    {
                        continue;
                    }

                    line.AudioType = dialogueAudioType.Id;
                    break;
                }
            }

            if (special == null)
            {
                line.Special = config.Specials.First().Id;
            }
            else
            {
                foreach (DialogueSpecial dialogueSpecial in config.Specials)
                {
                    if (!String.Equals(special, dialogueSpecial.Name))
                    {
                        continue;
                    }

                    line.Special = dialogueSpecial.Id;
                    break;
                }
            }

            if (audibility == null)
            {
                line.Audibility = config.Audibilities.First().Id;
            }
            else
            {
                int audibiltyIndex = 0;
                if (!int.TryParse(audibility, out audibiltyIndex))
                {
                    audibiltyIndex = 0;
                }

                line.Audibility = config.Audibilities[audibiltyIndex].Id;
            }

            line.LastModifiedTime = timestamp;
        }

        /// <summary>
        /// Serialises the data in the model onto the specified xml writer in the old dialogue
        /// star file format.
        /// </summary>
        /// <param name="conversation">
        /// The conversation object to serialise.
        /// </param>
        /// <param name="writer">
        /// The xml writer the specified conversation object should be serialised to.
        /// </param>
        private static void SaveOldFormat(this Conversation conversation, XmlWriter writer)
        {
            if (writer == null)
            {
                Debug.Assert(writer != null, "Unable to serialise into a null writer.");
                return;
            }

            writer.WriteStartElement("Conversation");
            writer.WriteAttributeString("placeholder", conversation.IsPlaceholder.ToString());
            writer.WriteAttributeString("root", conversation.Root.ToString());
            writer.WriteAttributeString("description", conversation.Description.ToString());
            writer.WriteAttributeString("random", conversation.IsRandom.ToString());
            writer.WriteAttributeString("category", conversation.Category.ToString());
            writer.WriteAttributeString("locked", conversation.LockedFilenames.ToString());
            //// Role is no longer stored in the new model
            writer.WriteAttributeString("role", String.Empty);
            //// Model is no longer stored in the new model
            writer.WriteAttributeString("model", String.Empty);
            //// Voice is no longer stored in the new model
            writer.WriteAttributeString("voice", String.Empty);
            //// UseVoice is no longer stored in the new model
            writer.WriteAttributeString("usevoice", "False");
            writer.WriteAttributeString(
                "interruptible", conversation.Interruptible.ToString());
            writer.WriteAttributeString(
                "anim", conversation.IsTriggeredByAnimation.ToString());
            writer.WriteAttributeString(
                "cutscene", conversation.CutsceneSubtitles.ToString());

            writer.WriteStartElement("Lines");
            foreach (ILine line in conversation.Lines)
            {
                line.SaveOldFormat(writer);
            }

            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        /// <summary>
        /// Serialises the data in the model onto the specified xml writer in the old dialogue
        /// star file format.
        /// </summary>
        /// <param name="line">
        /// The line object to serialise.
        /// </param>
        /// <param name="writer">
        /// The xml writer the specified line object should be serialised to.
        /// </param>
        private static void SaveOldFormat(this ILine line, XmlWriter writer)
        {
            DialogueConfigurations config = line.Configurations;

            //// Remove last _01 from AudioFilepath to serialise old filename that doesn't
            //// support random audio clips.
            String[] filenamePieces = line.AudioFilepath.Split('_');
            List<String> baseParts = new List<String>();
            for (int index = 0; index < filenamePieces.Count() - 1; index++)
            {
                baseParts.Add(filenamePieces[index]);
            }

            writer.WriteStartElement("Line");
            writer.WriteAttributeString("guid", line.CharacterId.ToString());
            writer.WriteAttributeString("dialogue", line.Dialogue.ToString());
            //// Volume isn't stored in the new model
            writer.WriteAttributeString("volume", "Normal");
            writer.WriteAttributeString("speaker", line.Speaker.ToString());
            writer.WriteAttributeString("listener", line.Listener.ToString());
            //// Audio isn't stored in the new model
            writer.WriteAttributeString("audio", "Normal");
            writer.WriteAttributeString("filename", String.Join("_", baseParts));
            writer.WriteAttributeString("manual", line.ManualFilename.ToString());
            writer.WriteAttributeString("special", config.GetSpecial(line.Special).Name);
            writer.WriteAttributeString("recorded", line.Recorded.ToString());
            writer.WriteAttributeString("sent", line.SentForRecording.ToString());
            writer.WriteAttributeString("subtitled", line.AlwaysSubtitled.ToString());
            writer.WriteAttributeString("timestamp", DateTime.UtcNow.ToString());
            writer.WriteAttributeString("interruptible", line.Interruptible.ToString());
            writer.WriteAttributeString(
                "dontInterruptForSpecialAbility",
                line.DontInterruptForSpecialAbility.ToString());
            writer.WriteAttributeString("ducks_score", line.DucksScore.ToString());
            writer.WriteAttributeString("ducks_radio", line.DucksRadio.ToString());
            writer.WriteAttributeString(
                "audibility", config.GetAudibilityIndex(line.Audibility).ToString());
            writer.WriteAttributeString("headset_submix", line.HeadsetSubmix.ToString());
            writer.WriteEndElement();
        }
        #endregion Methods
    } // RSG.Text.Model.Extensions {Class}
} // RSG.Text.Model {Namespace}
