﻿//---------------------------------------------------------------------------------------------
// <copyright file="Enumeration.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions
{
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using RSG.Base.Extensions;
    using RSG.Editor;
    using RSG.Editor.Model;
    using RSG.Metadata.Model.Resources;

    /// <summary>
    /// Defines a parsable class or structure that can be used by the parCodeGen system. This
    /// class cannot be inherited.
    /// </summary>
    internal sealed class Enumeration : DefinitionBase, IEnumeration
    {
        #region Fields
        /// <summary>
        /// The string representation of the <see cref="EnumValueGenerator.Hash"/> constant.
        /// </summary>
        private const string ValueGeneratorHash = "hash";

        /// <summary>
        /// The string representation of the <see cref="EnumValueGenerator.Hash16"/>
        /// constant.
        /// </summary>
        private const string ValueGeneratorHash16 = "atHash16";

        /// <summary>
        /// The string representation of the <see cref="EnumValueGenerator.Hash16u"/>
        /// constant.
        /// </summary>
        private const string ValueGeneratorHash16u = "atHash16U";

        /// <summary>
        /// The string representation of the <see cref="EnumValueGenerator.Increment"/>
        /// constant.
        /// </summary>
        private const string ValueGeneratorIncrement = "incr";

        /// <summary>
        /// The string representation of the <see cref="EnumValueGenerator.LiteralHash"/>
        /// constant.
        /// </summary>
        private const string ValueGeneratorLiteralHash = "literalhash";

        /// <summary>
        /// The name of the xml node that contains the data for a enumeration constant.
        /// </summary>
        private const string XmlConstantNodeName = "enumval";

        /// <summary>
        /// The name of the xml attribute used for the <see cref="CodeGeneration"/> property.
        /// </summary>
        private const string XmlGenerateAttr = "generate";

        /// <summary>
        /// The name of the xml attribute used for the <see cref="SortMethod"/> property.
        /// </summary>
        private const string XmlSortAttr = "ui_sort";

        /// <summary>
        /// The name of the xml attribute used for the <see cref="ValueGenerator"/> property.
        /// </summary>
        private const string XmlGeneratorAttr = "values";

        /// <summary>
        /// The private field used for the <see cref="CodeGeneration"/> property.
        /// </summary>
        private string _codeGeneration;

        /// <summary>
        /// The private field used for the <see cref="EnumConstants"/> property.
        /// </summary>
        private ModelCollection<IEnumConstant> _enumConstants;

        /// <summary>
        /// The private field used for the <see cref="EnumConstants"/> property.
        /// </summary>
        private ReadOnlyModelCollection<IEnumConstant> _readOnlyEnumConstants;

        /// <summary>
        /// The private field used for the <see cref="SortMethod"/> property.
        /// </summary>
        private EnumerationSortMethod _sortMethod;

        /// <summary>
        /// The private collection of attributes that have been found on the xml data that
        /// weren't recognised as valid attributes.
        /// </summary>
        private HashSet<string> _unrecognisedAttributes;

        /// <summary>
        /// The private field used for the <see cref="ValueGenerator"/> property.
        /// </summary>
        private string _valueGenerator;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="Enumeration"/> class.
        /// </summary>
        /// <param name="dictionary">
        /// The dictionary that this enumeration will be associated with.
        /// </param>
        public Enumeration(IDefinitionDictionary dictionary)
            : base(dictionary)
        {
            this._sortMethod = EnumerationSortMethod.Default;
            this._enumConstants = new ModelCollection<IEnumConstant>(this);
            this._readOnlyEnumConstants = this._enumConstants.ToReadOnly();
            this._unrecognisedAttributes = new HashSet<string>();
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Enumeration"/> class with the
        /// specified data type.
        /// </summary>
        /// <param name="dataType">
        /// The full data type this enumeration will represent.
        /// </param>
        /// <param name="dictionary">
        /// The dictionary that this enumeration will be associated with.
        /// </param>
        public Enumeration(string dataType, IDefinitionDictionary dictionary)
            : base(dataType, dictionary)
        {
            this._sortMethod = EnumerationSortMethod.Default;
            this._enumConstants = new ModelCollection<IEnumConstant>(this);
            this._readOnlyEnumConstants = this._enumConstants.ToReadOnly();
            this._unrecognisedAttributes = new HashSet<string>();
        }

        /// <summary>
        /// Initialises a new instance of the  <see cref="Enumeration"/> class as a copy of the
        /// specified instance.
        /// </summary>
        /// <param name="other">
        /// The instance to copy.
        /// </param>
        /// <param name="dictionary">
        /// The dictionary that this enumeration will be associated with.
        /// </param>
        public Enumeration(Enumeration other, IDefinitionDictionary dictionary)
            : base(other, dictionary)
        {
            this._enumConstants = new ModelCollection<IEnumConstant>(this);
            this._readOnlyEnumConstants = this._enumConstants.ToReadOnly();
            this._unrecognisedAttributes = new HashSet<string>();
            this._valueGenerator = other._valueGenerator;
            this._codeGeneration = other._codeGeneration;
            this._sortMethod = other._sortMethod;

            foreach (IEnumConstant constant in other._enumConstants)
            {
                IEnumConstant newConstant = null;
                if (constant != null)
                {
                    newConstant = constant.Clone();
                }

                this._enumConstants.Add(newConstant);
            }
        }

        /// <summary>
        /// Initialises a new instance of the  <see cref="Enumeration"/> class using the
        /// specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        /// <param name="dictionary">
        /// The dictionary that this enumeration will be associated with.
        /// </param>
        public Enumeration(XmlReader reader, IDefinitionDictionary dictionary)
            : base(reader, dictionary)
        {
            this._sortMethod = EnumerationSortMethod.Default;
            this._enumConstants = new ModelCollection<IEnumConstant>(this);
            this._readOnlyEnumConstants = this._enumConstants.ToReadOnly();
            this._unrecognisedAttributes = new HashSet<string>();
            this.Deserialise(reader);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the code generation option.
        /// </summary>
        public string CodeGeneration
        {
            get { return this._codeGeneration; }
            set { this.SetProperty(ref this._codeGeneration, value); }
        }

        /// <summary>
        /// Gets a read-only collection containing the constants that are defined for this
        /// instance.
        /// </summary>
        public ReadOnlyModelCollection<IEnumConstant> EnumConstants
        {
            get { return this._readOnlyEnumConstants; }
        }

        /// <summary>
        /// Gets or sets the sorting method that should be applied to the items when displayed.
        /// </summary>
        public EnumerationSortMethod SortMethod
        {
            get { return this._sortMethod; }
            set { this.SetProperty(ref this._sortMethod, value); }
        }

        /// <summary>
        /// Gets or sets the generator that is used to create the constant values.
        /// </summary>
        public EnumValueGenerator ValueGenerator
        {
            get
            {
                return this.GetGeneratorFromString(this._valueGenerator);
            }

            set
            {
                string generator = this.GetStringFromGenerator(value);
                this.SetProperty(ref this._valueGenerator, generator);
            }
        }

        /// <summary>
        /// Gets the constant at the specified index.
        /// </summary>
        /// <param name="index">
        /// The zero-based index of the constant to get.
        /// </param>
        /// <returns>
        /// The constant at the specified index.
        /// </returns>
        public IEnumConstant this[int index]
        {
            get { return this._enumConstants != null ? this._enumConstants[index] : null; }
        }

        /// <summary>
        /// Gets the constant that has the specified name if it exists; otherwise, null.
        /// </summary>
        /// <param name="name">
        /// The name of the constant to get.
        /// </param>
        /// <returns>
        /// The constant with the specified name or null if it doesn't exist.
        /// </returns>
        public IEnumConstant this[string name]
        {
            get
            {
                foreach (IEnumConstant constant in this._enumConstants)
                {
                    if (String.Equals(constant.Name, name))
                    {
                        return constant;
                    }
                }

                return null;
            }
        }

        /// <summary>
        /// Gets the constant that has the specified value if it exists; otherwise, null.
        /// </summary>
        /// <param name="value">
        /// The value of the constant to get.
        /// </param>
        /// <returns>
        /// The constant with the specified value or null if it doesn't exist.
        /// </returns>
        public IEnumConstant this[long value]
        {
            get
            {
                foreach (IEnumConstant constant in this._enumConstants)
                {
                    if (String.Equals(constant.Value, value))
                    {
                        return constant;
                    }
                }

                return null;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new <see cref="Enumeration"/> that is a copy of this instance.
        /// </summary>
        /// <returns>
        /// A new <see cref="Enumeration"/> that is a copy of this instance.
        /// </returns>
        public new Enumeration Clone()
        {
            return new Enumeration(this, this.Dictionary);
        }

        /// <summary>
        /// Creates a deep copy of the current instance.
        /// </summary>
        /// <returns>
        /// A deep copy of the current System.Object.
        /// </returns>
        public override object DeepClone()
        {
            return this.Clone();
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified
        /// <see cref="Enumeration"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public bool Equals(Enumeration other)
        {
            if (other == null)
            {
                return false;
            }

            if (!String.Equals(
                this.RawDataType, other.DataType, StringComparison.OrdinalIgnoreCase))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified
        /// <see cref="IEnumeration"/>.
        /// </summary>
        /// <param name="other">
        /// A <see cref="IEnumeration"/> to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public bool Equals(IEnumeration other)
        {
            return this.Equals(other as Enumeration);
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified
        /// <see cref="IDefinition"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public override bool Equals(IDefinition other)
        {
            return this.Equals(other as Enumeration);
        }

        /// <summary>
        /// Searches for a constant with the specified name and returns the zero-based index of
        /// the first occurrence within the enumeration.
        /// </summary>
        /// <param name="name">
        /// The name of the constant you wish to get the index of.
        /// </param>
        /// <returns>
        /// The zero-based index of the first occurrence of the specified name within the
        /// enumeration, if found; otherwise, –1.
        /// </returns>
        public int GetIndexOf(string name)
        {
            if (this._enumConstants == null)
            {
                return -1;
            }

            for (int i = 0; i < this._enumConstants.Count; i++)
            {
                IEnumConstant constant = this._enumConstants[i];
                if (constant == null)
                {
                    continue;
                }

                if (String.Equals(constant.Name, name))
                {
                    return i;
                }
            }

            return -1;
        }

        /// <summary>
        /// Creates a new <see cref="IEnumeration"/> that is a copy of this instance.
        /// </summary>
        /// <returns>
        /// A new <see cref="IEnumeration"/> that is a copy of this instance.
        /// </returns>
        IEnumeration IEnumeration.Clone()
        {
            return this.Clone();
        }

        /// <summary>
        /// Writes the xml representation of this instance onto the specified
        /// System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that the xml representation of this instance should be
        /// written to.
        /// </param>
        /// <param name="saveFileInfo">
        /// If true the file, line number and line position will be written out as attributes.
        /// </param>
        public void Serialise(XmlWriter writer, bool saveFileInfo)
        {
            if (writer == null)
            {
                throw new SmartArgumentNullException(() => writer);
            }

            if (this.RawDataType != null)
            {
                writer.WriteAttributeString(DefinitionBase.XmlTypeAttr, this.RawDataType);
            }

            if (this._codeGeneration != null)
            {
                writer.WriteAttributeString(XmlGenerateAttr, this._codeGeneration);
            }

            if (this._valueGenerator != null)
            {
                writer.WriteAttributeString(XmlGeneratorAttr, this._valueGenerator);
            }

            if (saveFileInfo)
            {
                writer.WriteAttributeString(DefinitionBase.XmlFileAttr, this.Filename);
                writer.WriteAttributeString(
                    DefinitionBase.XmlLineNumberAttr, this.LineNumber.ToString());
                writer.WriteAttributeString(
                    DefinitionBase.XmlLinePositionAttr, this.LinePosition.ToString());
            }

            if (this._enumConstants != null)
            {
                foreach (IEnumConstant constant in this._enumConstants)
                {
                    writer.WriteStartElement(XmlConstantNodeName);
                    constant.Serialise(writer);
                    writer.WriteEndElement();
                }
            }
        }

        /// <summary>
        /// Validates this entity and creates a validation result object containing the errors
        /// and warnings.
        /// </summary>
        /// <param name="recursive">
        /// A value indicating whether any child models to this one should also be validated.
        /// </param>
        /// <returns>
        /// A validation result object containing all of the errors and warnings associated
        /// with this validation pass.
        /// </returns>
        public override ValidationResult Validate(bool recursive)
        {
            ValidationResult result = new ValidationResult();
            foreach (string attribute in this._unrecognisedAttributes)
            {
                string msg = StringTable.UnrecognisedEnumerationAttribute;
                msg = msg.FormatCurrent(attribute);
                result.AddWarning(msg, this.Location);
            }

            if (this.RawDataType == null)
            {
                string msg = StringTable.MissingTypeAttributeError;
                result.AddError(msg, this.Location);
            }
            else if (String.IsNullOrWhiteSpace(this.RawDataType))
            {
                string msg = StringTable.EmptyTypeAttributeError;
                result.AddError(msg, this.Location);
            }

            if (this.ValueGenerator == EnumValueGenerator.Unrecognised)
            {
                string msg = StringTable.UnrecognisedValueGeneratorWarning;
                msg = msg.FormatCurrent(this.DataType, this._valueGenerator);
                result.AddError(msg, this.Location);
            }

            if (this._codeGeneration != null && this._codeGeneration != "bitset")
            {
                string msg = StringTable.UnrecognisedCodeGenerationWarning;
                msg = msg.FormatCurrent(this.DataType, this._codeGeneration);
                result.AddError(msg, this.Location);
            }

            HashSet<string> constantNames = new HashSet<string>();
            foreach (IEnumConstant constant in this._enumConstants)
            {
                if (constant == null)
                {
                    continue;
                }

                string name = constant.Name;
                if (name != null)
                {
                    if (!constantNames.Contains(name))
                    {
                        constantNames.Add(name);
                        continue;
                    }

                    string msg = StringTable.DuplicateConstantNameError;
                    msg = msg.FormatCurrent(this.DataType, name);
                    result.AddError(msg, constant.Location);
                }
            }

            if (recursive)
            {
                foreach (IEnumConstant constant in this._enumConstants)
                {
                    result.AddChildResults(constant.Validate(true));
                }
            }

            return result;
        }

        /// <summary>
        /// Initialises this instance using the specified System.Xml.XmlReader as a
        /// data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader used as the data provider to initialise this
        /// instance with.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            bool isEmptyElement = reader.IsEmptyElement;
            for (int i = 0; i < reader.AttributeCount; i++)
            {
                reader.MoveToAttribute(i);
                string name = reader.LocalName;
                string value = reader.Value;

                if (String.Equals(name, DefinitionBase.XmlTypeAttr))
                {
                    this.SetRawDataType(value);
                }
                else if (String.Equals(name, XmlGenerateAttr))
                {
                    this._codeGeneration = value;
                }
                else if (String.Equals(name, XmlGeneratorAttr))
                {
                    this._valueGenerator = value;
                }
                else if (String.Equals(name, XmlSortAttr))
                {
                    if (!Enum.TryParse<EnumerationSortMethod>(value, out this._sortMethod))
                    {
                        this._sortMethod = EnumerationSortMethod.Default;
                    }
                }
                else
                {
                    this._unrecognisedAttributes.Add(name);
                }
            }

            if (isEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToElementOrComment())
            {
                if (reader.NodeType == XmlNodeType.Comment)
                {
                    reader.Skip();
                    continue;
                }

                if (String.Equals(reader.Name, XmlConstantNodeName))
                {
                    IEnumConstant constant = new EnumConstant(reader, this);
                    if (constant == null)
                    {
                        reader.Skip();
                        continue;
                    }

                    this._enumConstants.Add(constant);
                }
            }

            if (reader.NodeType == XmlNodeType.EndElement)
            {
                reader.ReadEndElement();
            }
        }

        /// <summary>
        /// Gets the value generator that is equivalent to the specified String.
        /// </summary>
        /// <param name="generator">
        /// The string to determine the value generator to return.
        /// </param>
        /// <returns>
        /// The value generator that is equivalent to the specified String.
        /// </returns>
        private EnumValueGenerator GetGeneratorFromString(string generator)
        {
            if (generator == null)
            {
                return EnumValueGenerator.Increment;
            }
            else if (String.Equals(generator, ValueGeneratorHash))
            {
                return EnumValueGenerator.Hash;
            }
            else if (String.Equals(generator, ValueGeneratorLiteralHash))
            {
                return EnumValueGenerator.LiteralHash;
            }
            else if (String.Equals(generator, ValueGeneratorHash16))
            {
                return EnumValueGenerator.Hash16;
            }
            else if (String.Equals(generator, ValueGeneratorHash16u))
            {
                return EnumValueGenerator.Hash16u;
            }
            else if (String.Equals(generator, ValueGeneratorIncrement))
            {
                return EnumValueGenerator.Increment;
            }
            else
            {
                return EnumValueGenerator.Unrecognised;
            }
        }

        /// <summary>
        /// Gets the string value that is equivalent to the specified generator.
        /// </summary>
        /// <param name="generator">
        /// The generator of the representing string that will be returned.
        /// </param>
        /// <returns>
        /// The string that is equivalent to the specified generator.
        /// </returns>
        private string GetStringFromGenerator(EnumValueGenerator generator)
        {
            switch (generator)
            {
                case EnumValueGenerator.Hash:
                    return ValueGeneratorHash;
                case EnumValueGenerator.LiteralHash:
                    return ValueGeneratorLiteralHash;
                case EnumValueGenerator.Hash16:
                    return ValueGeneratorHash16;
                case EnumValueGenerator.Hash16u:
                    return ValueGeneratorHash16u;
                case EnumValueGenerator.Increment:
                    return ValueGeneratorIncrement;
                case EnumValueGenerator.Unrecognised:
                default:
                    return null;
            }
        }
        #endregion Methods
    } // RSG.Metadata.Model.Definitions.Structure {Class}
} // RSG.Metadata.Model.Definitions {Namespace}
