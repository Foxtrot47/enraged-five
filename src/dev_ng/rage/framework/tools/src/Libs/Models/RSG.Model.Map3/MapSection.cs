﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using RSG.Base.Collections;
using RSG.Base.ConfigParser;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Base.Math;
using RSG.ManagedRage;
using RSG.Model.Asset;
using RSG.Model.Common;
using RSG.Model.Common.Map;
using RSG.Model.Map3.Util;
using RSG.Platform;
using RSG.SceneXml;
using RSG.Statistics.Common.Dto.GameAssets;
using RSG.Statistics.Common.Dto.GameAssetStats;
using System.Reflection;
using Ionic.Zip;
using RSG.Bounds;
using RSG.Statistics.Common.Dto.GameAssetStats.Bundles;
using RSG.Base.Configuration;

namespace RSG.Model.Map3
{
    /// <summary>
    /// 
    /// </summary>
    public class MapSection : MapNode, IMapSection
    {
        #region Properties
        /// <summary>
        /// Type of section this is (e.g. Container, Props, Interiors, etc...)
        /// </summary>
        public SectionType Type
        {
            get;
            protected set;
        }

        /// <summary>
        /// The points for this section gotten from the levels vector map
        /// </summary>
        public IList<Vector2f> VectorMapPoints
        {
            get { return m_vectorMapPoints; }
            set
            {
                if (value != m_vectorMapPoints)
                {
                    m_vectorMapPoints = value;
                    Area = CalculateSectionArea();
                }
            }
        }
        private IList<Vector2f> m_vectorMapPoints;

        /// <summary>
        /// Bounding box for this section.
        /// </summary>
        public BoundingBox2f BoundingBox
        {
            get
            {
                if (m_boundingBox == null)
                {
                    m_boundingBox = new BoundingBox2f();
                    if (VectorMapPoints != null)
                    {
                        foreach (Vector2f point in VectorMapPoints)
                        {
                            m_boundingBox.Expand(point);
                        }
                    }
                }
                return m_boundingBox;
            }
        }
        private BoundingBox2f m_boundingBox;

        /// <summary>
        /// The area of the section based on the current vector map points
        /// </summary>
        public float Area
        {
            get;
            set;
        }

        /// <summary>
        /// The prop group to this map section if one exists and is applicable
        /// </summary>
        public IMapSection PropGroup
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets a value indicating whether this map section is considered to be a prop group
        /// for another map section.
        /// </summary>
        public bool IsPropGroup
        {
            get;
            set;
        }

        /// <summary>
        /// Array of lists of neighbours (should contain a list per neighbour mode)
        /// </summary>
        public IList<IMapSection>[] Neighbours
        {
            get;
            protected set;
        }

        /// <summary>
        /// Whether the section exports archetypes
        /// </summary>
        [StreamableStat(StreamableMapSectionStat.ExportArchetypesString)]
        public bool ExportArchetypes
        {
            get
            {
                CheckStatLoaded(StreamableMapSectionStat.ExportArchetypes);
                return m_exportArchetypes;
            }
            private set
            {
                SetPropertyValue(ref m_exportArchetypes, value, "ExportArchetypes");
                SetStatLodaded(StreamableMapSectionStat.ExportArchetypes, true);
            }
        }
        private bool m_exportArchetypes;

        /// <summary>
        /// Whether the section exports entities
        /// </summary>
        [StreamableStat(StreamableMapSectionStat.ExportEntitiesString)]
        public bool ExportEntities
        {
            get
            {
                CheckStatLoaded(StreamableMapSectionStat.ExportEntities);
                return m_exportEntities;
            }
            private set
            {
                SetPropertyValue(ref m_exportEntities, value, "ExportEntities");
                SetStatLodaded(StreamableMapSectionStat.ExportEntities, true);
            }
        }
        private bool m_exportEntities;

        /// <summary>
        /// List of archetypes this map section contains
        /// </summary>
        [StreamableStatAttribute(StreamableMapSectionStat.ArchetypesString)]
        public ObservableCollection<IMapArchetype> Archetypes
        {
            get
            {
                CheckStatLoaded(StreamableMapSectionStat.Archetypes);
                return m_archetypes;
            }
            private set
            {
                SetPropertyValue(ref m_archetypes, value, "Archetypes");
                SetStatLodaded(StreamableMapSectionStat.Archetypes, true);
            }
        }
        private ObservableCollection<IMapArchetype> m_archetypes;

        /// <summary>
        /// Filters the list of archetypes to return only interior archetypes.
        /// </summary>
        [Browsable(false)]
        public IEnumerable<IInteriorArchetype> InteriorArchetypes
        {
            get
            {
                CheckStatLoaded(StreamableMapSectionStat.Archetypes);
                return Archetypes.Where(item => (item is IInteriorArchetype)).Select(item => item as IInteriorArchetype);
            }
        }

        /// <summary>
        /// Filters the list of archetypes to return only non-interior archetype.
        /// </summary>
        [Browsable(false)]
        public IEnumerable<IMapArchetype> NonInteriorArchetypes
        {
            get
            {
                CheckStatLoaded(StreamableMapSectionStat.Archetypes);
                return Archetypes.Where(item => !(item is IInteriorArchetype));
            }
        }

        /// <summary>
        /// List of entities this map section contains
        /// </summary>
        [StreamableStatAttribute(StreamableMapSectionStat.EntitiesString)]
        public ObservableCollection<IEntity> ChildEntities
        {
            get
            {
                CheckStatLoaded(StreamableMapSectionStat.Entities);
                return m_childEntities;
            }
            private set
            {
                SetPropertyValue(ref m_childEntities, value, "ChildEntities");
                SetStatLodaded(StreamableMapSectionStat.Entities, true);
            }
        }
        private ObservableCollection<IEntity> m_childEntities;

        /// <summary>
        /// List of car gens this map section contains
        /// </summary>
        [StreamableStatAttribute(StreamableMapSectionStat.CarGensString)]
        public ObservableCollection<ICarGen> CarGens
        {
            get
            {
                CheckStatLoaded(StreamableMapSectionStat.CarGens);
                return m_carGens;
            }
            private set
            {
                SetPropertyValue(ref m_carGens, value, "CarGens");
                SetStatLodaded(StreamableMapSectionStat.CarGens, true);
            }
        }
        private ObservableCollection<ICarGen> m_carGens;

        /// <summary>
        /// Map sections top level collision poly count
        /// </summary>
        [StreamableStatAttribute(StreamableMapSectionStat.CollisionPolygonCountString)]
        public uint CollisionPolygonCount
        {
            get
            {
                CheckStatLoaded(StreamableMapSectionStat.CollisionPolygonCount);
                return m_collisionPolygonCount;
            }
            private set
            {
                SetPropertyValue(ref m_collisionPolygonCount, value, "CollisionPolygonCount");
                SetStatLodaded(StreamableMapSectionStat.CollisionPolygonCount, true);
            }
        }
        private uint m_collisionPolygonCount;

        /// <summary>
        /// Collision poly counts by collision type flags
        /// </summary>
        [Browsable(false)]
        [StreamableStatAttribute(StreamableMapSectionStat.CollisionPolygonCountBreakdownString)]
        public IList<CollisionInfo> CollisionPolygonCountBreakdown
        {
            get
            {
                CheckStatLoaded(StreamableMapSectionStat.CollisionPolygonCountBreakdown);
                return m_collisionPolygonCountBreakdown;
            }
            private set
            {
                SetPropertyValue(ref m_collisionPolygonCountBreakdown, value, "CollisionPolygonCountBreakdown");
                SetStatLodaded(StreamableMapSectionStat.CollisionPolygonCountBreakdown, true);
            }
        }
        private IList<CollisionInfo> m_collisionPolygonCountBreakdown;

        /// <summary>
        /// Last person to export this section.
        /// </summary>
        [StreamableStatAttribute(StreamableMapSectionStat.LastExportUserString)]
        public String LastExportUser
        {
            get
            {
                CheckStatLoaded(StreamableMapSectionStat.LastExportUser);
                return m_lastExportUser;
            }
            private set
            {
                SetPropertyValue(ref m_lastExportUser, value, "LastExportUser");
                SetStatLodaded(StreamableMapSectionStat.LastExportUser, true);
            }
        }
        private String m_lastExportUser;

        /// <summary>
        /// Last time this section was exported.
        /// </summary>
        [StreamableStatAttribute(StreamableMapSectionStat.LastExportTimeString)]
        public DateTime LastExportTime
        {
            get
            {
                CheckStatLoaded(StreamableMapSectionStat.LastExportTime);
                return m_lastExportTime;
            }
            private set
            {
                SetPropertyValue(ref m_lastExportTime, value, "LastExportTime");
                SetStatLodaded(StreamableMapSectionStat.LastExportTime, true);
            }
        }
        private DateTime m_lastExportTime;

        /// <summary>
        /// Name of the exported zip file
        /// </summary>
        [StreamableStatAttribute(StreamableMapSectionStat.ExportZipFilepathString)]
        public String ExportZipFilepath
        {
            get
            {
                CheckStatLoaded(StreamableMapSectionStat.ExportZipFilepath);
                return m_exportZipFilepath;
            }
            private set
            {
                SetPropertyValue(ref m_exportZipFilepath, value, "ExportZipFilepath");
                SetStatLodaded(StreamableMapSectionStat.ExportZipFilepath, true);
            }
        }
        private String m_exportZipFilepath;

        /// <summary>
        /// DCC source filename (excluding extension).
        /// </summary>
        [StreamableStatAttribute(StreamableMapSectionStat.DCCSourceFilenameString)]
        public String DCCSourceFilename
        {
            get
            {
                CheckStatLoaded(StreamableMapSectionStat.DCCSourceFilename);
                return m_DCCSourceFilename;
            }
            private set
            {
                SetPropertyValue(ref m_DCCSourceFilename, value, "DCCSourceFilename");
                SetStatLodaded(StreamableMapSectionStat.DCCSourceFilename, true);
            }
        }
        private String m_DCCSourceFilename;

        /// <summary>
        /// Container attributes...
        /// </summary>
        [StreamableStatAttribute(StreamableMapSectionStat.ContainerAttributesString)]
        public IDictionary<String, object> ContainerAttributes
        {
            get
            {
                CheckStatLoaded(StreamableMapSectionStat.ContainerAttributes);
                return m_containerAttributes;
            }
            private set
            {
                SetPropertyValue(ref m_containerAttributes, value, "ContainerAttributes");
                SetStatLodaded(StreamableMapSectionStat.ContainerAttributes, true);
            }
        }
        private IDictionary<String, object> m_containerAttributes;

        /// <summary>
        /// Dictionary mapping platforms to platform stats.
        /// </summary>
        [Browsable(false)]
        [StreamableStatAttribute(StreamableMapSectionStat.PlatformStatsString)]
        public IDictionary<RSG.Platform.Platform, IMapSectionPlatformStat> PlatformStats
        {
            get
            {
                CheckStatLoaded(StreamableMapSectionStat.PlatformStats);
                return m_platformStats;
            }
            private set
            {
                SetPropertyValue(ref m_platformStats, value, "PlatformStats");
                SetStatLodaded(StreamableMapSectionStat.PlatformStats, true);
            }
        }
        private IDictionary<RSG.Platform.Platform, IMapSectionPlatformStat> m_platformStats;


        /// <summary>
        /// Dictionary mapping platforms to platform stats.
        /// </summary>
        [Browsable(false)]
        [StreamableStatAttribute(StreamableMapSectionStat.AggregatedStatsString)]
        public IDictionary<StatKey, IMapSectionStatistic> AggregatedStats
        {
            get
            {
                CheckStatLoaded(StreamableMapSectionStat.AggregatedStats);
                return m_aggregatedStats;
            }
            private set
            {
                SetPropertyValue(ref m_aggregatedStats, value, "AggregatedStats");
                SetStatLodaded(StreamableMapSectionStat.AggregatedStats, true);
            }
        }
        private IDictionary<StatKey, IMapSectionStatistic> m_aggregatedStats;

        /// <summary>
        /// Gets the attribute guid set on the container for this section.
        /// </summary>
        public string ContainerAttributeGuid
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// List of stats that have been loaded for this streamable object.
        /// </summary>
        private ISet<StreamableStat> LoadedStats { get; set; }
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public MapSection(string name, SectionType type, IMapHierarchy hierarchy)
            : base(name, hierarchy)
        {
            Type = type;
            LoadedStats = new HashSet<StreamableStat>();
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public MapSection(string name, SectionType type, IMapArea parent)
            : base(name, parent)
        {
            Type = type;
            LoadedStats = new HashSet<StreamableStat>();
        }
        #endregion // Constructor(s)

        #region IMapSection Implementation
        /// <summary>
        /// 
        /// </summary>
        public void LoadAllStats()
        {
            LoadStats(StreamableStatUtils.GetValuesAssignedToProperties(GetType()));
        }

        /// <summary>
        /// TODO: Make this abstract.  They are virtual for now for backwards compatibility.
        /// </summary>
        /// <param name="statsToLoad"></param>
        public virtual void LoadStats(IEnumerable<StreamableStat> statsToLoad)
        {
            RequestStatistics(statsToLoad, false);
        }

        /// <summary>
        /// Request all the statistics for this object.
        /// </summary>
        /// <param name="async"></param>
        public void RequestAllStatistics(bool async)
        {
            RequestStatistics(StreamableStatUtils.GetValuesAssignedToProperties(GetType()), async);
        }

        /// <summary>
        /// Load specific statistics for this map section
        /// </summary>
        /// <param name="statsToLoad"></param>
        /// <param name="async"></param>
        public void RequestStatistics(IEnumerable<StreamableStat> statsToLoad, bool async)
        {
            MapHierarchy.RequestStatisticsForSection(this, statsToLoad, async);
        }

        /// <summary>
        /// Returns whether or not a particular stat has been loaded for this object.
        /// </summary>
        /// <param name="stat"></param>
        /// <returns></returns>
        public bool IsStatLoaded(StreamableStat stat)
        {
            return LoadedStats.Contains(stat);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestedStats"></param>
        /// <returns></returns>
        public bool AreStatsLoaded(IEnumerable<StreamableStat> requestedStats)
        {
            return !StatsRequiredForObject(requestedStats).Any();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="requestedStats"></param>
        /// <returns></returns>
        protected IEnumerable<StreamableStat> StatsRequiredForObject(IEnumerable<StreamableStat> requestedStats)
        {
            foreach (StreamableStat stat in requestedStats)
            {
                if (!IsStatLoaded(stat))
                {
                    yield return stat;
                }
            }
        }

        /// <summary>
        /// Load basic stats based on the content tree nodes
        /// </summary>
        /// <param name="gv"></param>
        internal void LoadBasicStatsFromExportData(ConfigGameView gv)
        {
            // Get the content node for this section
            ContentNodeMap contentNode = (ContentNodeMap)gv.Content.Root.FindAll(Name, "map").FirstOrDefault();

            if (contentNode != null)
            {
                DCCSourceFilename = Path.Combine(contentNode.Path, contentNode.Name);
                ExportArchetypes = contentNode.ExportDefinitions;
                ExportEntities = contentNode.ExportInstances;

                // Get the map zip node for the section
                ContentNodeMapZip exportNode = contentNode.Outputs.FirstOrDefault() as ContentNodeMapZip;

                if (exportNode != null)
                {
                    ExportZipFilepath = exportNode.Filename;
                }
            }
        }

        /// <summary>
        /// Load basic stats from the export data scene xml file
        /// </summary>
        /// <param name="source"></param>
        internal void LoadStatsFromExportData(ConfigGameView gv)
        {
            // Get the content node for this section
            ContentNodeMap contentNode = (ContentNodeMap)gv.Content.Root.FindAll(Name, "map").FirstOrDefault();

            if (contentNode != null)
            {
                // Get the map zip node for the section
                ContentNodeMapZip exportNode = contentNode.Outputs.FirstOrDefault() as ContentNodeMapZip;

                if (exportNode != null)
                {
                    // First off, parse the scene xml file
                    string sceneXmlFileName = Path.ChangeExtension(exportNode.Filename, "xml");
                    ParseInternal(sceneXmlFileName, contentNode);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gv"></param>
        internal void LoadCollisionStatsFromProcessedData(ConfigGameView gv)
        {
            CollisionPolygonCount = 0;
            CollisionPolygonCountBreakdown = new List<CollisionInfo>();

            ContentNodeMapProcessedZip processedNode = GetProcesedZipNode(gv);

            if (processedNode != null && File.Exists(processedNode.Filename))
            {
                using (ZipFile zipFile = ZipFile.Read(processedNode.Filename))
                {
                    IEnumerable<ZipEntry> ibnZipEntries = zipFile.Where(item => item.FileName.EndsWith(".ibn.zip"));
                    foreach (ZipEntry entry in ibnZipEntries)
                    {
                        using (MemoryStream ibnZipStream = new MemoryStream())
                        {
                            entry.Extract(ibnZipStream);
                            ibnZipStream.Position = 0;
                            ExtractCollisionDataFromIbnZipFile(ibnZipStream);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Load per platform memory usage statistics.
        /// </summary>
        /// <param name="gv"></param>
        internal void LoadPlatformStatsFromExportData(IConfig config)
        {
            // Load up the pack files for the various platforms.
            IDictionary<RSG.Platform.Platform, Packfile> packfiles = OpenPerPlatformPackfiles(config);

            // Extract the information we are after from the pack files and keep track of it in the model.
            PlatformStats = new Dictionary<RSG.Platform.Platform, IMapSectionPlatformStat>();

            foreach (KeyValuePair<RSG.Platform.Platform, Packfile> pair in packfiles)
            {
                MapSectionPlatformStat platformStat = new MapSectionPlatformStat(pair.Key);

                if (pair.Value.Entries != null)
                {
                    foreach (PackEntry entry in pair.Value.Entries)
                    {
                        // Determine the type of file this entry is to add the physical/virtual sizes to the appropriate property.
                        FileType[] fileTypes = Filename.GetFileTypes(entry.Name);
                        if (fileTypes.Length > 0)
                        {
                            FileType fileType = fileTypes[0];

                            if (fileType == FileType.Drawable ||
                                fileType == FileType.DrawableDictionary ||
                                fileType == FileType.Fragment ||
                                fileType == FileType.TextureDictionary ||
                                fileType == FileType.BoundsFile ||
                                fileType == FileType.BoundsDictionary ||
                                fileType == FileType.ClipDictionary)
                            {
                                if (!platformStat.FileTypeSizes.ContainsKey(fileType))
                                {
                                    platformStat.FileTypeSizes.Add(fileType, new MemoryStat());
                                }

                                platformStat.FileTypeSizes[fileType].PhysicalSize += (uint)entry.PhysicalSize;
                                platformStat.FileTypeSizes[fileType].VirtualSize += (uint)entry.VirtualSize;
                            }
                        }
                    }
                }

                // Add the new platform stat to our dictionary.
                PlatformStats.Add(pair.Key, platformStat);
            }

            // Close any pack files we opened.
            ClosePerPlaformPackfiles(packfiles);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        internal void GatherAggregatedStatistics()
        {
            AggregatedStats = new Dictionary<StatKey, IMapSectionStatistic>();

            // Force population of all the statistics in the cache for this section.
            foreach (StatGroup group in Enum.GetValues(typeof(StatGroup)))
            {
                foreach (StatLodLevel lodLevel in Enum.GetValues(typeof(StatLodLevel)))
                {
                    foreach (StatEntityType entityType in Enum.GetValues(typeof(StatEntityType)))
                    {
                        StatKey key = new StatKey(group, lodLevel, entityType);
                        AggregatedStats[key] = new MapSectionStatistic(this, group, lodLevel, entityType);

                        if (PropGroup != null)
                        {
                            key = new StatKey(group, lodLevel, entityType, true);
                            AggregatedStats[key] = new MapSectionStatistic(new IMapSection[] { this, PropGroup }, group, lodLevel, entityType);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void GenerateNeighbours()
        {
            GenerateNeighboursInternal();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="propSection"></param>
        public void SetPropGroup(IMapSection propSection)
        {
            PropGroup = propSection;

            if (PropGroup != null)
            {
                PropGroup.IsPropGroup = true;
                PropGroup.SetNeighbours(Neighbours);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="neighbours"></param>
        public void SetNeighbours(IList<IMapSection>[] neighbours)
        {
            Neighbours = neighbours;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="requestedStats"></param>
        internal void LoadStatsFromDatabase(MapSectionStatDto dto, IList<StreamableStat> requestedStats)
        {
            IDictionary<StreamableStat, PropertyInfo> modelLookup = StreamableStatUtils.GetPropertyInfoLookup(GetType());
            IDictionary<StreamableStat, PropertyInfo> dtoLookup = StreamableStatUtils.GetPropertyInfoLookup(dto.GetType());

            // Map the properties we are after.
            foreach (StreamableStat stat in requestedStats.Where(item => !IsStatLoaded(item)))
            {
                if (dtoLookup.ContainsKey(stat) && modelLookup.ContainsKey(stat))
                {
                    // Is this a complex item?
                    if (stat.DtoToModelRequiresProcessing)
                    {
                        ProcessComplexMapSectionStat(dto, stat);
                    }
                    else
                    {
                        object value = dtoLookup[stat].GetValue(dto, null);
                        modelLookup[stat].SetValue(this, value, null);
                    }
                }
                else
                {
#warning Readd assert once everything is marked up.
                    //Debug.Assert(false, String.Format("One of the MapSectionStat dto class or the model class doesn't have the '{0}' stat marked up.", stat));
                    Log.Log__Error(String.Format("One of the MapSectionStat dto class or the model class doesn't have the '{0}' stat marked up.", stat));
                }
            }
        }
        #endregion // IMapSection Implementation

        #region Private Methods
        /// <summary>
        /// Checks whether a particular stat is loaded.  Only active in debug.
        /// </summary>
        /// <param name="stat"></param>
        [Conditional("DEBUG")]
        protected void CheckStatLoaded(Guid stat)
        {
#warning Readd this assert once the section view model has been sorted out.
            //Debug.Assert(IsStatLoaded(stat), "Attempting to access a stat that hasn't been loaded.");
        }

        /// <summary>
        /// Sets the flag indicating that a particular stat has been loaded.
        /// </summary>
        /// <param name="stat"></param>
        /// <param name="loaded"></param>
        protected void SetStatLodaded(StreamableStat stat, bool loaded)
        {
            if (loaded)
            {
                LoadedStats.Add(stat);
            }
            else
            {
                LoadedStats.Remove(stat);
            }
        }

        #region SceneXml Parsing
        /// <summary>
        /// Parse section scene xml (internal)
        /// </summary>
        /// <param name="sceneXmlFilename"></param>
        /// <param name="contentNode"></param>
        private void ParseInternal(String sceneXmlFilename, ContentNodeMap contentNode)
        {
            Scene scene = new Scene(sceneXmlFilename, contentNode, LoadOptions.All, true);

            if (scene != null && contentNode != null)
            {
                if (scene.Objects.Length > 0 && scene.Objects[0].IsContainer() && Guid.Empty != scene.Objects[0].AttributeGuid)
                {
                    // Map container; set hashes.
                    this.ContainerAttributeGuid = scene.Objects[0].AttributeGuid.ToString();
                }
                else
                {
                    this.ContainerAttributeGuid = scene.Filename;
                }

                LastExportUser = scene.ExportUser;
                LastExportTime = scene.ExportTimestamp;
                ProcessContainerAttributes(scene);

                // Process the scene categorising the objects we found
                CategorisedObjects categorisedObjects = CategoriseScene(scene, contentNode);

                ProcessArchetypes(scene, contentNode, categorisedObjects);
                ProcessEntities(scene, contentNode, categorisedObjects.Entities);
                ProcessCarGens(scene);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scene"></param>
        private void ProcessContainerAttributes(Scene scene)
        {
            IDictionary<string, object> attributes = new Dictionary<string, object>();

            TargetObjectDef containerObject = scene.Objects.Where(item => item.Name.ToLower() == Name && item.Class == "container").FirstOrDefault();
            if (containerObject != null)
            {
                attributes.AddRange(containerObject.Attributes);
            }

            ContainerAttributes = attributes;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scene"></param>
        /// <param name="contentNode"></param>
        /// <returns></returns>
        private CategorisedObjects CategoriseScene(Scene scene, ContentNodeMap contentNode)
        {
            CategorisedObjects objects = new CategorisedObjects();

            foreach (TargetObjectDef sceneObject in scene.Objects.Where(item => IsSceneObjectValidToShow(item)))
            {
                CategoriseSceneObject(sceneObject, contentNode, ref objects);

                if (sceneObject.IsContainer())
                {
                    foreach (TargetObjectDef childObject in sceneObject.Children.Where(item => IsSceneObjectValidToShow(item)))
                    {
                        CategoriseSceneObject(childObject, contentNode, ref objects);
                    }
                }
            }

            foreach (TargetObjectDef sceneObject in objects.Fragments)
            {
                bool found = false;
                if (sceneObject.SkeletonRoot != null)
                {
                    found = true;
                    objects.FragmentsWithRoots.Add(sceneObject, sceneObject.SkeletonRoot);
                }
                else
                {
                    foreach (TargetObjectDef root in objects.FragmentRoots)
                    {
                        if (root.Name == sceneObject.Name + "_frag_")
                        {
                            found = true;
                            objects.FragmentsWithRoots.Add(sceneObject, root);
                            break;
                        }
                    }
                }

                if (!found)
                {
                    objects.FragmentsWithRoots.Add(sceneObject, sceneObject);
                }
            }

            return objects;
        }

        /// <summary>
        /// Check for whether an item is valid for showing in the workbench
        /// </summary>
        /// <param name="sceneObject"></param>
        /// <returns></returns>
        private bool IsSceneObjectValidToShow(TargetObjectDef sceneObject)
        {
            return (!sceneObject.DontExport() && !sceneObject.DontExportIDE());
        }

        /// <summary>
        /// For the given scene object, determines if it is an instance, definition, fragdef, animdef, milodef, or a combination
        /// </summary>
        private static void CategoriseSceneObject(TargetObjectDef sceneObject, ContentNodeMap contentNode, ref CategorisedObjects categorisedObjects)
        {
            // FragmentRoots (Potential)
            if (sceneObject.Name.EndsWith("_frag_"))
            {
                categorisedObjects.FragmentRoots.Add(sceneObject);
            }

            // Dummies
            if (sceneObject.IsDummy())
            {
                return;
            }

            // Archetypes
            if (contentNode.ExportDefinitions)
            {
                if (sceneObject.IsCloth())
                {
                    if (sceneObject.DrawableLOD != null && (sceneObject.DrawableLOD.Children != null && sceneObject.DrawableLOD.Children.Count() != 0))
                    {
                        return;
                    }

                    categorisedObjects.Drawables.Add(sceneObject);
                }
                else if (sceneObject.IsFragment() && !sceneObject.IsXRef() && !sceneObject.IsRefObject() && !sceneObject.IsInternalRef() && !sceneObject.IsRefInternalObject())
                {
                    if (sceneObject.DrawableLOD != null && (sceneObject.DrawableLOD.Children != null && sceneObject.DrawableLOD.Children.Count() != 0))
                    {
                        return;
                    }

                    categorisedObjects.Fragments.Add(sceneObject);
                }
                else if (sceneObject.IsMilo())
                {
                    categorisedObjects.Interiors.Add(sceneObject);
                }
                else if (sceneObject.IsStatedAnim())
                {
                    if (sceneObject.Attributes.ContainsKey(AttrNames.OBJ_GROUP_NAME))
                    {
                        String animGroup = sceneObject.Attributes[AttrNames.OBJ_GROUP_NAME] as String;
                        if (animGroup != null)
                        {
                            if (!categorisedObjects.AnimGroups.ContainsKey(animGroup.ToLower()))
                            {
                                categorisedObjects.AnimGroups.Add(animGroup.ToLower(), new List<TargetObjectDef>());
                            }

                            categorisedObjects.AnimGroups[animGroup.ToLower()].Add(sceneObject);
                        }
                    }
                }
                else if (sceneObject.IsObject() && !sceneObject.IsXRef() && !sceneObject.IsRefObject() && !sceneObject.IsInternalRef() && !sceneObject.IsRefInternalObject()) // A definition and posible instance
                {
                    if (sceneObject.DrawableLOD != null && (sceneObject.DrawableLOD.Children != null && sceneObject.DrawableLOD.Children.Count() != 0))
                    {
                        return;
                    }

                    categorisedObjects.Drawables.Add(sceneObject);
                }
            }

            // Entities
            if (contentNode.ExportInstances && !sceneObject.DontExportIPL())
            {
                if (sceneObject.IsXRef() || sceneObject.IsRefObject() || sceneObject.IsInternalRef() || sceneObject.IsRefInternalObject()) // A instance
                {
                    if (sceneObject.LOD != null && (sceneObject.LOD.Parent != null && !sceneObject.LOD.Parent.IsContainerLODRefObject()))
                    {
                        return;
                    }

                    categorisedObjects.Entities.Add(sceneObject);
                }
                else if (sceneObject.IsObject()) // An instance and possible definition
                {
                    if (sceneObject.LOD != null && (sceneObject.LOD.Parent != null && !sceneObject.LOD.Parent.IsContainerLODRefObject()))
                    {
                        return;
                    }

                    categorisedObjects.Entities.Add(sceneObject);
                }
                else if (sceneObject.IsMilo())
                {
                    categorisedObjects.Entities.Add(sceneObject);
                }
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="scene"></param>
        /// <param name="section"></param>
        /// <param name="categorisedObjects"></param>
        private void ProcessArchetypes(Scene scene, ContentNodeMap contentNode, CategorisedObjects categorisedObjects)
        {
            Archetypes = new ObservableCollection<IMapArchetype>();

            // Process drawables (keeping track of 0 polycount objects)
            List<TargetObjectDef> polycountZero = new List<TargetObjectDef>();
            foreach (TargetObjectDef sceneObject in categorisedObjects.Drawables)
            {
                if (sceneObject.PolyCount == 0)
                {
                    polycountZero.Add(sceneObject);
                }
                
                IDrawableArchetype drawable = new DrawableArchetype(sceneObject.Name, this);
                (drawable as DrawableArchetype).LoadStatsFromExportData(sceneObject, scene);
                Archetypes.Add(drawable);
            }
            if (polycountZero.Count > 0)
            {
                Log.Log__Error(String.Format("The container {0} has the following objects in it that appear to be made up of zero polygons: {1}.  " +
                                             "Re-exporting this container will most likely fix this issue.",
                                             Name,
                                             String.Join(", ", polycountZero.Select(item => item.Name))));
            }

            // Process fragments
            foreach (KeyValuePair<TargetObjectDef, TargetObjectDef> kvp in categorisedObjects.FragmentsWithRoots)
            {
                IFragmentArchetype fragment = new FragmentArchetype(kvp.Key.Name, this);
                (fragment as FragmentArchetype).LoadStatsFromExportData(kvp.Key, kvp.Value, scene);
                Archetypes.Add(fragment);
            }

            // Process stated anims
            foreach (KeyValuePair<string, List<TargetObjectDef>> animPair in categorisedObjects.AnimGroups)
            {
                StatedAnimArchetype anim = new StatedAnimArchetype(animPair.Key + "_anim", this);
                anim.LoadStatsFromExportData(animPair.Value, scene);
                Archetypes.Add(anim);
            }

            // Process interiors
            foreach (TargetObjectDef sceneObject in categorisedObjects.Interiors)
            {
                InteriorArchetype interior = new InteriorArchetype(sceneObject.Name, this);
                interior.LoadStatsFromExportData(sceneObject, scene);
                Archetypes.Add(interior);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scene"></param>
        /// <param name="section"></param>
        /// <param name="instances"></param>
        /// <returns></returns>
        private void ProcessEntities(Scene scene, ContentNodeMap contentNode, List<TargetObjectDef> instances)
        {
            ChildEntities = new ObservableCollection<IEntity>();

            foreach (TargetObjectDef objectDef in instances)
            {
                this.ProcessEntity(objectDef);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scene"></param>
        private void ProcessCarGens(Scene scene)
        {
            CarGens = new ObservableCollection<ICarGen>();

            foreach (TargetObjectDef objectDef in scene.Objects.Where(item => IsSceneObjectValidToShow(item)))
            {
                if (objectDef.IsCarGen())
                {
                    CarGen carGen = new CarGen(objectDef.Name, this);
                    carGen.LoadStatsFromExportData(objectDef);
                    CarGens.Add(carGen);
                }
                if (objectDef.IsContainer())
                {
                    foreach (TargetObjectDef childDef in objectDef.Children.Where(item => IsSceneObjectValidToShow(item)))
                    {
                        if (childDef.IsCarGen())
                        {
                            CarGen carGen = new CarGen(childDef.Name, this);
                            carGen.LoadStatsFromExportData(childDef);
                            CarGens.Add(carGen);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sceneObject"></param>
        /// <returns></returns>
        private CollisionType GetCollisionFlagsForObject(ObjectDef sceneObject)
        {
            CollisionType flags = 0;

            // If the river collision flag is set, then the default for the rest of the flags is false otherwise its true
            bool isRiver = false;
            bool defaultValue = true;
            string defaultValueString = "true";
            Boolean.TryParse(sceneObject.GetUserProperty(CollisionType.River.GetUserPropertyName(), "false"), out isRiver);

            if (isRiver)
            {
                flags |= CollisionType.River;
                defaultValue = false;
                defaultValueString = "false";
            }

            // Process the rest of the collision types
            foreach (CollisionType type in Enum.GetValues(typeof(CollisionType)))
            {
                if (type != CollisionType.River)
                {
                    bool isFlagSet = false;
                    if (!Boolean.TryParse(sceneObject.GetUserProperty(type.GetUserPropertyName(), defaultValueString), out isFlagSet))
                    {
                        isFlagSet = defaultValue;
                    }

                    if (isFlagSet)
                    {
                        flags |= type;
                    }
                }
            }

            return flags;
        }
        #endregion // SceneXml Parsing

        #region Platform Stats
        /// <summary>
        /// 
        /// </summary>
        /// <param name="gv"></param>
        /// <returns></returns>
        private IDictionary<RSG.Platform.Platform, Packfile> OpenPerPlatformPackfiles(IConfig config)
        {
            // Load up the pack files for the various platforms.
            IDictionary<RSG.Platform.Platform, Packfile> packfiles = new Dictionary<RSG.Platform.Platform, Packfile>();
            foreach (KeyValuePair<RSG.Platform.Platform, ITarget> targetPair in config.Project.DefaultBranch.Targets)
            {
                RSG.Platform.Platform platform = targetPair.Key; ;
                ITarget target = targetPair.Value;

                // Convert the platform independent path to a per platform build path
                String platformFilePath = RSG.Pipeline.Services.Platform.PlatformPathConversion.ConvertAndRemapFilenameToPlatform(target, ExportZipFilepath);

                // Load the pack file
                packfiles[platform] = new Packfile();
                packfiles[platform].Load(platformFilePath);
            }

            return packfiles;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="packfiles"></param>
        private void ClosePerPlaformPackfiles(IDictionary<RSG.Platform.Platform, Packfile> packfiles)
        {
            foreach (KeyValuePair<RSG.Platform.Platform, Packfile> pair in packfiles)
            {
                pair.Value.Close();
            }
        }
        #endregion // Platform Stats

        #region Collision Stats
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private ContentNodeMapProcessedZip GetProcesedZipNode(ConfigGameView gv)
        {
            ContentNodeMapProcessedZip processedNode = null;

            // Get the content node for this section
            ContentNodeMap contentNode = (ContentNodeMap)gv.Content.Root.FindAll(Name, "map").FirstOrDefault();

            if (contentNode != null)
            {
                // Get the export node for the section
                ContentNodeMapZip exportNode = contentNode.Outputs.FirstOrDefault() as ContentNodeMapZip;
                if (exportNode != null)
                {
                    processedNode = exportNode.Outputs.FirstOrDefault() as ContentNodeMapProcessedZip;
                }
            }

            return processedNode;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ibnZipStream"></param>
        private void ExtractCollisionDataFromIbnZipFile(Stream ibnZipStream)
        {
            using (ZipFile zipFile = ZipFile.Read(ibnZipStream))
            {
                IEnumerable<ZipEntry> boundEntries = zipFile.Where(item => item.FileName.EndsWith(".bnd"));
                foreach (ZipEntry entry in boundEntries)
                {
                    using (MemoryStream bndStream = new MemoryStream())
                    {
                        entry.Extract(bndStream);
                        bndStream.Position = 0;
                        BNDFile bndFile = BNDFile.Load(entry.FileName, bndStream);
                        ExtractCollisionDataFromBndFile(bndFile);
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        private void ExtractCollisionDataFromBndFile(BNDFile file)
        {
            Debug.Assert(file.BoundObject is Composite, "Root object in bounds file isn't a composite object.");
            Composite rootObject = (Composite)file.BoundObject;

            foreach (CompositeChild child in rootObject.Children)
            {
                Debug.Assert(child.BoundObject is BVH, "Child bound object isn't a bvh");
                BVH bvh = (BVH)child.BoundObject;

                AddCollisionCount(child.CollisionType, CollisionPrimitiveType.Triangle, (uint)bvh.Primitives.OfType<BVHTriangle>().Count());
                AddCollisionCount(child.CollisionType, CollisionPrimitiveType.Box, (uint)bvh.Primitives.OfType<BVHBox>().Count());
                AddCollisionCount(child.CollisionType, CollisionPrimitiveType.Sphere, (uint)bvh.Primitives.OfType<BVHSphere>().Count());
                AddCollisionCount(child.CollisionType, CollisionPrimitiveType.Cylinder, (uint)bvh.Primitives.OfType<BVHCylinder>().Count());
                AddCollisionCount(child.CollisionType, CollisionPrimitiveType.Capsule, (uint)bvh.Primitives.OfType<BVHCapsule>().Count());
                AddCollisionCount(child.CollisionType, CollisionPrimitiveType.Quad, (uint)bvh.Primitives.OfType<BVHQuad>().Count());
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="collisionType"></param>
        /// <param name="primitiveType"></param>
        /// <param name="count"></param>
        private void AddCollisionCount(BNDFile.CollisionType collisionFlags, CollisionPrimitiveType primitiveType, uint count)
        {
            // Ignore counts of 0
            if (count == 0)
            {
                return;
            }

            // Attempt to retrieve an existing entry for this collision/primitive type.
            CollisionInfo info = CollisionPolygonCountBreakdown.FirstOrDefault(item => item.CollisionFlags == collisionFlags && item.PrimitiveType == primitiveType);

            if (info == null)
            {
                // No entry exists, so create a new one.
                info = new CollisionInfo(collisionFlags, primitiveType);
                CollisionPolygonCountBreakdown.Add(info);
            }

            // Increment the counts.
            info.Count += count;
            CollisionPolygonCount += count;
        }
        #endregion // Collision Stats

        #region Database Stats
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="stat"></param>
        private void ProcessComplexMapSectionStat(MapSectionStatDto dto, Guid stat)
        {
            if (stat == StreamableMapSectionStat.Archetypes)
            {
                ProcessArchetypeStats(dto.Archetypes);
            }
            else if (stat == StreamableMapSectionStat.Entities)
            {
                ProcessEntityStats(dto.Entities);
            }
            else if (stat == StreamableMapSectionStat.CarGens)
            {
                ProcessCarGenStats(dto.CarGens);
            }
            else if (stat == StreamableMapSectionStat.ContainerAttributes)
            {
                ProcessContainerAttributeStats(dto.ContainerAttributes);
            }
            else if (stat == StreamableMapSectionStat.CollisionPolygonCountBreakdown)
            {
                ProcessCollisionPolyCountStats(dto.CollisionPolygonCountBreakdown);
            }
            else if (stat == StreamableMapSectionStat.PlatformStats)
            {
                ProcessPlatformStats(dto.PlatformStats);
            }
            else if (stat == StreamableMapSectionStat.AggregatedStats)
            {
                ProcessAggregateStats(dto.AggregatedStats);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="archetypeDtos"></param>
        private void ProcessArchetypeStats(List<BasicArchetypeStatDto> archetypeDtos)
        {
            Archetypes = new ObservableCollection<IMapArchetype>();
            Archetypes.BeginUpdate();
            foreach (BasicDrawableArchetypeStatDto drawableDto in archetypeDtos.OfType<BasicDrawableArchetypeStatDto>())
            {
                Archetypes.Add(new DrawableArchetype(drawableDto.Name, this));
            }
            foreach (BasicFragmentArchetypeStatDto drawableDto in archetypeDtos.OfType<BasicFragmentArchetypeStatDto>())
            {
                Archetypes.Add(new FragmentArchetype(drawableDto.Name, this));
            }
            foreach (BasicInteriorArchetypeStatDto drawableDto in archetypeDtos.OfType<BasicInteriorArchetypeStatDto>())
            {
                Archetypes.Add(new InteriorArchetype(drawableDto.Name, this));
            }
            foreach (BasicStatedAnimArchetypeStatDto drawableDto in archetypeDtos.OfType<BasicStatedAnimArchetypeStatDto>())
            {
                Archetypes.Add(new StatedAnimArchetype(drawableDto.Name, this));
            }
            Archetypes.EndUpdate();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entityDtos"></param>
        private void ProcessEntityStats(List<BasicEntityStatDto> entityDtos)
        {
            ChildEntities = new ObservableCollection<IEntity>();
            ChildEntities.BeginUpdate();

            lock (MapHierarchy.ArchetypeLookup)
            {
                foreach (BasicEntityStatDto entityDto in entityDtos)
                {
                    if (MapHierarchy.ArchetypeLookup.ContainsKey(entityDto.ArchetypeName.ToLower()))
                    {
                        IArchetype archetype = MapHierarchy.ArchetypeLookup[entityDto.ArchetypeName.ToLower()];
                        IEntity entity = new Entity(entityDto.Name, archetype, this);
                    }
                    else
                    {
                        Log.Log__Warning("Archetype '{0}' wasn't found in the archetype look up for entity '{1}'.", entityDto.ArchetypeName, entityDto.Name);
                    }
                }
            }

            ChildEntities.EndUpdate();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="carGenDtos"></param>
        private void ProcessCarGenStats(List<CarGenStatDto> carGenDtos)
        {
            CarGens = new ObservableCollection<ICarGen>();
            CarGens.BeginUpdate();

            foreach (CarGenStatDto dto in carGenDtos)
            {
                CarGen carGen = new CarGen(dto.Name, this);
                carGen.LoadStatsFromDatabase(dto);
                CarGens.Add(carGen);
            }

            CarGens.EndUpdate();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="attributeDtos"></param>
        private void ProcessContainerAttributeStats(List<MapSectionAttributeStatDto> attributeDtos)
        {
            ContainerAttributes = new Dictionary<String, object>();

            foreach (MapSectionAttributeStatDto attributeDto in attributeDtos)
            {
                object value = null;

                try
                {
                    // Create and parse the string type/value.
                    switch (attributeDto.Type.ToLower())
                    {
                        case "string":
                            value = attributeDto.Value;
                            break;

                        case "boolean":
                            value = Boolean.Parse(attributeDto.Value);
                            break;

                        case "int32":
                            value = Int32.Parse(attributeDto.Value);
                            break;

                        case "uint32":
                            value = UInt32.Parse(attributeDto.Value);
                            break;

                        case "int64":
                            value = Int64.Parse(attributeDto.Value);
                            break;

                        case "uint64":
                            value = UInt64.Parse(attributeDto.Value);
                            break;

                        case "float":
                            value = Single.Parse(attributeDto.Value);
                            break;

                        case "double":
                            value = Double.Parse(attributeDto.Value);
                            break;

                        default:
                            Log.Log__Warning("Unable to determine the type of container attribute for '{0}' with type '{1}'.", attributeDto.Name, attributeDto.Type);
                            break;
                    }
                }
                catch
                {
                    Log.Log__Warning("Unable to parse the '{0}' string value into a '{1}' object.", attributeDto.Value, attributeDto.Type);
                    value = null;
                }

                // Add the value to our list (if we were able to determine it's type).
                if (value != null)
                {
                    ContainerAttributes.Add(attributeDto.Name, attributeDto.Value);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="collisionDtos"></param>
        private void ProcessCollisionPolyCountStats(List<CollisionPolyStatDto> collisionDtos)
        {
            CollisionPolygonCountBreakdown = new List<CollisionInfo>();

            foreach (CollisionPolyStatDto collisionDto in collisionDtos)
            {
                CollisionInfo info = new CollisionInfo((BNDFile.CollisionType)collisionDto.CollisionFlags, collisionDto.PrimitiveType);
                info.Count = collisionDto.PolygonCount;
                CollisionPolygonCountBreakdown.Add(info);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="platformDtos"></param>
        private void ProcessPlatformStats(List<MapSectionPlatformStatDto> platformDtos)
        {
            PlatformStats = new Dictionary<RSG.Platform.Platform, IMapSectionPlatformStat>();

            foreach (MapSectionPlatformStatDto platformDto in platformDtos)
            {
                MapSectionPlatformStat platformStat = new MapSectionPlatformStat(platformDto.Platform.Value);

                foreach (MapSectionMemoryStatDto fileTypeDto in platformDto.FileTypeSizes)
                {
                    platformStat.FileTypeSizes[fileTypeDto.FileType.Value] =
                        new MemoryStat(fileTypeDto.MemorySat.PhysicalSize, fileTypeDto.MemorySat.VirtualSize);
                }

                PlatformStats.Add(platformDto.Platform.Value, platformStat);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="aggregateDtos"></param>
        private void ProcessAggregateStats(List<MapSectionAggregateStatDto> aggregateDtos)
        {
            AggregatedStats = new Dictionary<StatKey, IMapSectionStatistic>();

            foreach (MapSectionAggregateStatDto aggregateDto in aggregateDtos)
            {
                StatKey key = new StatKey(aggregateDto.Group, aggregateDto.LodLevel, aggregateDto.EntityType, aggregateDto.IncludesPropGroup);
                MapSectionStatistic stat = new MapSectionStatistic();
                stat.LoadStatsFromDatabase(aggregateDto);
                AggregatedStats.Add(key, stat);
            }
        }
        #endregion // Database Stats

        #region Neighbours/Area Computations
        /// <summary>
        /// 
        /// </summary>
        private void GenerateNeighboursInternal()
        {
            // Initialise the nieghbours "data structure"
            Neighbours = new List<IMapSection>[Enum.GetValues(typeof(NeighbourMode)).Length];
            for (int i = 0; i < Enum.GetValues(typeof(NeighbourMode)).Length; ++i)
            {
                Neighbours[i] = new List<IMapSection>();
            }

            // Calculate the neighbours
            if (VectorMapPoints != null && VectorMapPoints.Any())
            {
                // Expand the bounding box slightly as sections don't touch each other perfectly.
                BoundingBox2f bbox = new BoundingBox2f(BoundingBox);
                if (bbox.Width > bbox.Height)
                {
                    bbox.Min.X -= 20.0f;
                    bbox.Max.X += 20.0f;
                    bbox.Min.Y -= (bbox.Width - bbox.Height) * 0.3f;
                    bbox.Max.Y += (bbox.Width - bbox.Height) * 0.3f;
                }
                else if (bbox.Height > bbox.Width)
                {
                    bbox.Min.Y -= 20.0f;
                    bbox.Max.Y += 20.0f;
                    bbox.Min.X -= (bbox.Height - bbox.Width) * 0.3f;
                    bbox.Max.X += (bbox.Height - bbox.Width) * 0.3f;
                }
                else
                {
                    bbox.Min.X -= 20.0f;
                    bbox.Min.Y -= 20.0f;
                    bbox.Max.X += 20.0f;
                    bbox.Max.Y += 20.0f;
                }

                // Create a bounding circle from the expanded bounding box.
                BoundingCirclef bcircle = new BoundingCirclef(bbox);

                // Create the list of segments that make up this sections vector map (for the distance neighbour mode).
                List<KeyValuePair<Vector2f, Vector2f>> segments = new List<KeyValuePair<Vector2f, Vector2f>>();
                for (int i = 0; i < VectorMapPoints.Count; i++)
                {
                    Vector2f nextPoint = i == VectorMapPoints.Count - 1 ? VectorMapPoints[0] : VectorMapPoints[i + 1];
                    segments.Add(new KeyValuePair<Vector2f, Vector2f>(VectorMapPoints[i], nextPoint));
                }

                // Iterate over all other sections.
                foreach (MapSection sec in MapHierarchy.AllSections.Where(sec => (sec != this && sec != PropGroup && sec.VectorMapPoints != null && sec.VectorMapPoints.Count > 0)))
                {
                    // Circle circle check first.
                    BoundingCirclef otherBoundingCircle = new BoundingCirclef(sec.BoundingBox);

                    if (bcircle.Intersects(otherBoundingCircle))
                    {
                        Neighbours[(int)NeighbourMode.Radius].Add(sec);

                        // Check bounding box next.
                        if (Intersect(sec.VectorMapPoints, bbox.Min, bbox.Max))
                        {
                            Neighbours[(int)NeighbourMode.Box].Add(sec);

                            // Finally check each point.
                            foreach (KeyValuePair<Vector2f, Vector2f> segment in segments)
                            {
                                float distance = DistanceSquared(sec.VectorMapPoints, segment);
                                if (distance < 100.0f * 100.0f)
                                {
                                    Neighbours[(int)NeighbourMode.Distance].Add(sec);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private float CalculateSectionArea()
        {
            float newArea = 0.0f;

            if (VectorMapPoints != null)
            {
                // Find center
                Vector2f center = new Vector2f();
                foreach (Vector2f point in VectorMapPoints)
                {
                    center.X += point.X;
                    center.Y += point.Y;
                }
                center = new Vector2f(center.X / VectorMapPoints.Count, center.Y / VectorMapPoints.Count);

                List<Vector2f> areaPoints = new List<Vector2f>(VectorMapPoints.Count);
                foreach (Vector2f point in VectorMapPoints)
                {
                    areaPoints.Add(new Vector2f(point.X - center.X, point.Y - center.Y));
                }

                for (int i = 0; i < areaPoints.Count; i++)
                {
                    Vector2f currentPoint = areaPoints[i];
                    Vector2f nextPoint = (i == areaPoints.Count - 1) ? areaPoints[0] : areaPoints[i + 1];

                    float diffence = ((currentPoint.X * nextPoint.Y) - (nextPoint.X * currentPoint.Y));
                    newArea += diffence;
                }
                newArea *= 0.5f;
                if (newArea < 0)
                {
                    newArea = -newArea;
                }
            }

            return newArea;
        }

        #region Intersect/DistanceSquared Methods
        /// <summary>
        /// Returns true if the polygon made of the given points intersects with the circle given
        /// by its center and radius
        /// </summary>
        private bool Intersect(IList<Vector2f> points, Vector2f center, float radiusSquared)
        {
            // Get the segments for the polygon
            List<KeyValuePair<Vector2f, Vector2f>> segments = new List<KeyValuePair<Vector2f, Vector2f>>();
            for (int i = 0; i < points.Count; i++)
            {
                Vector2f nextPoint = i == points.Count - 1 ? points[0] : points[i + 1];
                segments.Add(new KeyValuePair<Vector2f, Vector2f>(points[i], nextPoint));
            }

            // Check the segment distance from the circles center and compare to the radius
            foreach (KeyValuePair<Vector2f, Vector2f> segment in segments)
            {
                if (DistanceSquared(center, segment) <= radiusSquared)
                    return true;
            }

            return false;
        }


        /// <summary>
        /// Returns true if the polygon made of the given points intersects with the aabb given
        /// by its min and max
        /// </summary>
        private bool Intersect(IList<Vector2f> points, Vector2f min, Vector2f max)
        {
            // Get the segments for the polygon
            List<KeyValuePair<Vector2f, Vector2f>> segments = new List<KeyValuePair<Vector2f, Vector2f>>();
            for (int i = 0; i < points.Count; i++)
            {
                Vector2f nextPoint = i == points.Count - 1 ? points[0] : points[i + 1];
                segments.Add(new KeyValuePair<Vector2f, Vector2f>(points[i], nextPoint));
            }

            foreach (KeyValuePair<Vector2f, Vector2f> segment in segments)
            {
                if (Intersect(segment, min, max))
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Returns true if the segment made of the given two points intersects with the aabb given
        /// by its min and max
        /// </summary>
        private bool Intersect(KeyValuePair<Vector2f, Vector2f> s, Vector2f min, Vector2f max)
        {
            // Find min and max X for the segment
            double minX = s.Key.X;
            double maxX = s.Value.X;
            if (s.Key.X > s.Value.X)
            {
                minX = s.Value.X;
                maxX = s.Key.X;
            }
            // Find the intersection of the segment's and rectangle's x-projections
            if (maxX > max.X)
            {
                maxX = max.X;
            }
            if (minX < min.X)
            {
                minX = min.X;
            }
            if (minX > maxX) // If their projections do not intersect return false
            {
                return false;
            }

            // Find corresponding min and max Y for min and max X we found before
            double minY = s.Key.Y;
            double maxY = s.Value.Y;
            double dx = s.Value.X - s.Key.X;

            if (System.Math.Abs(dx) > 0.0000001)
            {
                double a = (s.Value.Y - s.Key.Y) / dx;
                double b = s.Key.Y - a * s.Key.X;
                minY = a * minX + b;
                maxY = a * maxX + b;
            }
            if (minY > maxY)
            {
                double tmp = maxY;
                maxY = minY;
                minY = tmp;
            }

            // Find the intersection of the segment's and rectangle's y-projections
            if (maxY > max.Y)
            {
                maxY = max.Y;
            }
            if (minY < min.Y)
            {
                minY = min.Y;
            }
            if (minY > maxY) // If Y-projections do not intersect return false
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Gets the minimum squared distance between a line segment and a set of points
        /// </summary>
        private float DistanceSquared(IList<Vector2f> points, KeyValuePair<Vector2f, Vector2f> segment)
        {
            List<KeyValuePair<Vector2f, Vector2f>> segments = new List<KeyValuePair<Vector2f, Vector2f>>();
            for (int i = 0; i < points.Count; i++)
            {
                Vector2f nextPoint = i == points.Count - 1 ? points[0] : points[i + 1];
                segments.Add(new KeyValuePair<Vector2f, Vector2f>(points[i], nextPoint));
            }

            float minimumDistance = float.MaxValue;
            foreach (var pointSegment in segments)
            {
                minimumDistance = Math.Min(DistanceSquared(pointSegment, segment), minimumDistance);
            }
            return minimumDistance;
        }

        /// <summary>
        /// Gets the minimum squared distance between two line segments
        /// </summary>
        private float DistanceSquared(KeyValuePair<Vector2f, Vector2f> seg1, KeyValuePair<Vector2f, Vector2f> seg2)
        {
            Vector2f u = new Vector2f(seg1.Value.X - seg1.Key.X, seg1.Value.Y - seg1.Key.Y);
            Vector2f v = new Vector2f(seg2.Value.X - seg2.Key.X, seg2.Value.Y - seg2.Key.Y);
            Vector2f w = new Vector2f(seg1.Key.X - seg2.Key.X, seg1.Key.Y - seg2.Key.Y);

            float a = ((u.X * u.X) + (u.Y * u.Y));
            float b = ((u.X * v.X) + (u.Y * v.Y));
            float c = ((v.X * v.X) + (v.Y * v.Y));
            float d = ((u.X * w.X) + (u.Y * w.Y));
            float e = ((v.X * w.X) + (v.Y * w.Y));
            float D = a * c - b * b;
            float sc, sN, sD = D;
            float tc, tN, tD = D;

            // compute the line parameters of the two closest points
            if (D < 0.0f)
            {
                // the lines are parallel
                sN = 0.0f;
                sD = 1.0f;
                tN = e;
                tD = c;
            }
            else
            {
                // get the closest points on the infinite lines
                sN = (b * e - c * d);
                tN = (a * e - b * d);
                if (sN < 0.0)
                {
                    // sc < 0 => the s=0 edge is visible
                    sN = 0.0f;
                    tN = e;
                    tD = c;
                }
                else if (sN > sD)
                {
                    // sc > 1 => the s=1 edge is visible
                    sN = sD;
                    tN = e + b;
                    tD = c;
                }
            }

            if (tN < 0.0)
            {
                // tc < 0 => the t=0 edge is visible
                tN = 0.0f;

                // recompute sc for this edge
                if (-d < 0.0f)
                    sN = 0.0f;
                else if (-d > a)
                    sN = sD;
                else
                {
                    sN = -d;
                    sD = a;
                }
            }
            else if (tN > tD)
            {
                // tc > 1 => the t=1 edge is visible
                tN = tD;

                // recompute sc for this edge
                if ((-d + b) < 0.0)
                    sN = 0;
                else if ((-d + b) > a)
                    sN = sD;
                else
                {
                    sN = (-d + b);
                    sD = a;
                }
            }

            sc = (Math.Abs(sN) < 0.0000001 ? 0.0f : sN / sD);
            tc = (Math.Abs(tN) < 0.0000001 ? 0.0f : tN / tD);

            Vector2f tcv = new Vector2f(tc * v.X, tc * v.Y);
            Vector2f scu = new Vector2f(sc * u.X, sc * u.Y);
            Vector2f dP = new Vector2f(w.X + scu.X - tcv.X, w.Y + scu.Y - tcv.Y);

            return ((dP.X * dP.X) + (dP.Y * dP.Y));
        }

        /// <summary>
        /// Gets the squared distance between a line segment and a point
        /// </summary>
        private float DistanceSquared(Vector2f p, KeyValuePair<Vector2f, Vector2f> l)
        {
            float l2 = DistanceSquared(l.Key, l.Value);
            if (l2 == 0.0)
                return DistanceSquared(p, l.Key);

            // Consider the line extending the segment, parameterized as v + t (w - v).
            // We find projection of point p onto the line. 
            // It falls where t = [(p-l.key) . (l.value-l.key)] / |w-v|^2
            Vector2f keyDifferent = new Vector2f(p.X - l.Key.X, p.Y - l.Key.Y);
            Vector2f segmentDifferent = new Vector2f(l.Value.X - l.Key.X, l.Value.Y - l.Key.Y);

            float t = ((keyDifferent.X * segmentDifferent.X) + (keyDifferent.Y * segmentDifferent.Y)) / l2;

            if (t < 0.0f)
            {
                return DistanceSquared(p, l.Key);
            }
            else
            {
                if (t > 1.0)
                    return DistanceSquared(p, l.Value);
            }


            Vector2f projection = new Vector2f(l.Key.X + (t * segmentDifferent.X), l.Key.Y + (t * segmentDifferent.Y));
            return DistanceSquared(p, projection);
        }

        /// <summary>
        /// Gets the squared distance between two points
        /// </summary>
        private float DistanceSquared(Vector2f p1, Vector2f p2)
        {
            return ((p1.X - p2.X) * (p1.X - p2.X)) + ((p1.Y - p2.Y) * (p1.Y - p2.Y));
        }
        #endregion // Intersect/DistanceSquared Methods
        #endregion // Neighbours/Area Computations
        #endregion // Private Methods

        #region Object Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return (String.Format("Map Section: [{0}]{1}", this.Name, this.Hash));
        }
        #endregion // Object Overrides
    } // MapSection
}
