﻿using System;
using System.Collections.Generic;

namespace RSG.Model.Common
{
    /// <summary>
    /// Interface for vehicle assets
    /// </summary>
    public interface IVehicle : IAsset, IHasAssetChildren, IComparable<IVehicle>, IEquatable<IVehicle>, IStreamableObject
    {
        #region Properties
        /// <summary>
        /// The collection this character is a part of
        /// </summary>
        IVehicleCollection ParentCollection { get; }

        /// <summary>
        /// Friendly name for the weapon (extracted from a localisation file).
        /// </summary>
        String FriendlyName { get; }

        /// <summary>
        /// Name of the vehicle as it appears in game.
        /// </summary>
        String GameName { get; }

        /// <summary>
        /// Vehicle category classification.
        /// </summary>
        VehicleCategory Category { get; }

        /// <summary>
        /// Array of textures the vehicle uses
        /// </summary>
        ITexture[] Textures { get; }

        /// <summary>
        /// Array of vehicle shaders.
        /// </summary>
        IShader[] Shaders { get; }
        
        /// <summary>
        /// 
        /// </summary>
        bool HasLod1 { get; }

        /// <summary>
        /// 
        /// </summary>
        bool HasLod2 { get; }

        /// <summary>
        /// 
        /// </summary>
        bool HasLod3 { get; }
        
        /// <summary>
        /// 
        /// </summary>
        int PolyCount { get; }

        /// <summary>
        /// 
        /// </summary>
        int CollisionCount { get; }
        
        /// <summary>
        /// 
        /// </summary>
        int WheelCount { get; }

        /// <summary>
        /// 
        /// </summary>
        int SeatCount { get; }

        /// <summary>
        /// 
        /// </summary>
        int DoorCount { get; }

        /// <summary>
        /// 
        /// </summary>
        int LightCount { get; }

        /// <summary>
        /// 
        /// </summary>
        int ExtraCount { get; }

        /// <summary>
        /// 
        /// </summary>
        int RudderCount { get; }

        /// <summary>
        /// 
        /// </summary>
        int RotorCount { get; }

        /// <summary>
        /// 
        /// </summary>
        int PropellerCount { get; }

        /// <summary>
        /// 
        /// </summary>
        int ElevatorCount { get; }

        /// <summary>
        /// 
        /// </summary>
        int HandlebarCount { get; }

        /// <summary>
        /// 
        /// </summary>
        int BoneCount { get; }

        /// <summary>
        /// 
        /// </summary>
        IDictionary<RSG.Platform.Platform, IVehiclePlatformStat> PlatformStats { get; }
        #endregion // Properties
    } // IVehicle

    /// <summary>
    /// Interface for vehicle based platform stats
    /// </summary>
    public interface IVehiclePlatformStat
    {
        /// <summary>
        /// Platform for this stat
        /// </summary>
        RSG.Platform.Platform Platform { get; }

        /// <summary>
        /// Physical size of the vehicle
        /// </summary>
        uint PhysicalSize { get; }

        /// <summary>
        /// Virtual size of the vehicle
        /// </summary>
        uint VirtualSize { get; }

        /// <summary>
        /// Hi-poly virtual size
        /// </summary>
        uint HiPhysicalSize { get; }

        /// <summary>
        /// Hi-poly physical size
        /// </summary>
        uint HiVirtualSize { get; }
    } // ICharacterPlatformStat
}
