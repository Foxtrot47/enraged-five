﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.UI;
using RSG.Base.ConfigParser;
using RSG.Base.Logging;
using RSG.Model.Common;
using System.Linq;
using RSG.Base.Tasks;
using RSG.Model.Common.Map;

namespace RSG.Model.Report.Reports.Map
{
    /// <summary>
    /// Report that processes the prop replacement list and looks for any instances of those
    /// props that are still present in the game
    /// </summary>
    public class PropReplacementReport : HTMLReport, IDynamicLevelReport
    {
        #region Constants
        private const String c_name = "Prop Replacements Report";
        private const String c_description = "Generate and display a report of where props in the prop replacement file are still in use.";
        private const String c_propReplacementFile = @"x:\gta5\assets\maps\PropSwap.txt";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (m_task == null)
                {
                    CompositeTask task = new CompositeTask("Generate Report");
                    task.AddSubTask(new ActionTask("Loading data", (context, progress) => EnsureDataLoaded((DynamicLevelReportContext)context, progress)), 50);
                    task.AddSubTask(new ActionTask("Process prop replacement file", (context, progress) => ProcessPropReplacementFile((DynamicLevelReportContext)context, progress)), 1);
                    task.AddSubTask(new ActionTask("Process level", (context, progress) => LookForPropsInLevel((DynamicLevelReportContext)context, progress)), 5);
                    task.AddSubTask(new ActionTask("Generating report", (context, progress) => GenerateReport((DynamicLevelReportContext)context, progress)));
                    m_task = task;
                }
                return m_task;
            }
        }
        private ITask m_task;

        /// <summary>
        /// List of stats that are required to generate this report.
        /// </summary>
        public IEnumerable<StreamableStat> RequiredStats
        {
            get { return null; }
        }

        /// <summary>
        /// List of prop names that we are looking for
        /// </summary>
        private ISet<string> PropNames
        {
            get;
            set;
        }

        /// <summary>
        /// List of props that were found in the game
        /// </summary>
        private ISet<IArchetype> PropsEncountered
        {
            get;
            set;
        }
        #endregion
        
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public PropReplacementReport()
            : base(c_name, c_description)
        {
            PropNames = new HashSet<string>();
            PropsEncountered = new HashSet<IArchetype>();
        }
        #endregion // Constructor(s)

        #region Task Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void EnsureDataLoaded(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            IMapHierarchy hierarchy = context.Level.MapHierarchy;

            if (hierarchy != null)
            {
                float increment = 1.0f / hierarchy.AllSections.Count();

                // Load all the map section data
                foreach (IMapSection section in hierarchy.AllSections)
                {
                    context.Token.ThrowIfCancellationRequested();

                    // Update the progress information
                    string message = String.Format("Loading {0}", section.Name);
                    progress.Report(new TaskProgress(increment, true, message));

                    section.RequestAllStatistics(false);
                }
            }
        }

        /// <summary>
        /// Processes the prop replacement file extracting the list of prop names that should be replaced
        /// </summary>
        private void ProcessPropReplacementFile(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            PropNames.Clear();

            // Read through the file line by line
            using (StreamReader reader = new StreamReader(c_propReplacementFile))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    line = line.Trim();

                    if (line.Length > 0 && line[0] != '#')
                    {
                        int colonIdx = line.IndexOf(':');
                        if (colonIdx != -1)
                        {
                            string propName = line.Substring(0, colonIdx);
                            PropNames.Add(propName);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Iterates over the level's map sections looking for instances of props in the prop name list
        /// </summary>
        /// <param name="level"></param>
        private void LookForPropsInLevel(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            PropsEncountered.Clear();

            IMapHierarchy hierarchy = context.Level.MapHierarchy;

            if (hierarchy != null)
            {
                float increment = 1.0f / hierarchy.AllSections.Count();

                foreach (IMapSection section in hierarchy.AllSections)
                {
                    context.Token.ThrowIfCancellationRequested();

                    // Update the progress information
                    string message = String.Format("Processing {0}", section.Name);
                    progress.Report(new TaskProgress(increment, true, message));

                    foreach (IArchetype archetype in section.Archetypes)
                    {
                        if (archetype.Entities.Any() && PropNames.Contains(archetype.Name))
                        {
                            PropsEncountered.Add(archetype);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void GenerateReport(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            Stream = WriteReport();
        }
        #endregion // Task Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private MemoryStream WriteReport()
        {
            // Generate the stream for the report
            MemoryStream stream = new MemoryStream();
            StreamWriter reportFile = new StreamWriter(stream);

            List<IArchetype> sortedProps = new List<IArchetype>(PropsEncountered.Count);
            sortedProps.AddRange(PropsEncountered);
            sortedProps.Sort();

            // Start writing out the report
            HtmlTextWriter writer = new HtmlTextWriter(reportFile);
            writer.RenderBeginTag(HtmlTextWriterTag.Html);
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Head);
                writer.RenderEndTag();

                writer.RenderBeginTag(HtmlTextWriterTag.Body);
                {
                    WriteHeader(writer, "Prop Replacement Report");
                    
                    WriteSubHeader(writer, "Summary");
                    OutputSummary(writer, sortedProps);

                    if (sortedProps.Count > 0)
                    {
                        WriteSubHeader(writer, "Detailed Information");

                        foreach (IArchetype propDef in PropsEncountered)
                        {
                            OutputPropInformation(writer, propDef);
                        }
                    }
                }
                writer.RenderEndTag();

            }
            writer.RenderEndTag();
            writer.Flush();

            // Reset the stream before returning it
            stream.Position = 0;
            return stream;
        }

        /// <summary>
        /// Outputs a summary of the information we found
        /// </summary>
        /// <param name="writer"></param>
        private void OutputSummary(HtmlTextWriter writer, List<IArchetype> props)
        {
            // Determine how many instances there are of all the props
            int instanceCount = 0;
            props.ForEach(item => instanceCount += item.Entities.Count);

            // Generate the summary text
            if (instanceCount == 0)
            {
                WriteParagraph(writer, "Congratulations, none of the props in the prop replacement file where found in the map data.");
            }
            else
            {
                string summaryText = String.Format("{0} of the props in the prop replacement file are still in use.  They are being used a total of {1} times in the map data.  " +
                                                   "Click on one of the links below to see more information about where it is in use.",
                                                   props.Count, instanceCount);
                WriteParagraph(writer, summaryText);

                // Output each of the props we found and and link to the detailed information
                foreach (IArchetype prop in props)
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Href, String.Format("#{0}", prop.Name));
                    writer.RenderBeginTag(HtmlTextWriterTag.A);
                    writer.Write(String.Format("{0} ({1} entities)", prop.Name, prop.Entities.Count));
                    writer.RenderEndTag();

                    writer.WriteBreak();
                }
            }

        }

        /// <summary>
        /// Outputs information regarding a single prop's occurrences
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="propDef"></param>
        private void OutputPropInformation(HtmlTextWriter writer, IArchetype prop)
        {
            // <a name="propName"><b>Prop Name</b></a>
            writer.AddAttribute(HtmlTextWriterAttribute.Name, prop.Name);
            writer.RenderBeginTag(HtmlTextWriterTag.A);
            writer.RenderBeginTag(HtmlTextWriterTag.B);
            writer.Write(prop.Name);
            writer.RenderEndTag();
            writer.RenderEndTag();

            // <table>
            //   <tr><td>Section Name</td><td>Position: (X, Y)</td></tr>
            //   ...
            // </table>
            writer.RenderBeginTag(HtmlTextWriterTag.Table);

            foreach (IEntity entity in prop.Entities)
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write(entity.Parent.Name);
                writer.RenderEndTag();
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write(String.Format("Position: {0}", entity.Position));
                writer.RenderEndTag();
                writer.RenderEndTag();
            }

            writer.RenderEndTag();
            writer.WriteBreak();
        }
        #endregion // Private Methods
    } // PropReplacementReport
}
