﻿// --------------------------------------------------------------------------------------------
// <copyright file="StringMember.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions.Members
{
    using System;
    using System.Xml;
    using RSG.Base;
    using RSG.Base.Extensions;
    using RSG.Base.Logging;
    using RSG.Editor;
    using RSG.Editor.Model;
    using RSG.Metadata.Model.Resources;
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// Represents the &lt;string&gt; member node in the parCodeGen system that can be
    /// instanced in a metadata file.
    /// </summary>
    public class StringMember : MemberBase, IEquatable<StringMember>, IHasInitialValue<string>
    {
        #region Fields
        /// <summary>
        /// The string representation of the <see cref="StringType.AtString"/> constant.
        /// </summary>
        private const string StringTypeAtString = "atString";

        /// <summary>
        /// The string representation of the <see cref="StringType.Const"/> constant.
        /// </summary>
        private const string StringTypeConst = "ConstString";

        /// <summary>
        /// The string representation of the <see cref="StringType.FinalHash"/> constant.
        /// </summary>
        private const string StringTypeFinalHashString = "atFinalHashString";

        /// <summary>
        /// The string representation of the <see cref="StringType.HashBank"/> constant.
        /// </summary>
        private const string StringTypeHashBank = "atHashWithStringBank";

        /// <summary>
        /// The string representation of the <see cref="StringType.HashDev"/> constant.
        /// </summary>
        private const string StringTypeHashDev = "atHashWithStringDev";

        /// <summary>
        /// The string representation of the <see cref="StringType.HashNotFinal"/> constant.
        /// </summary>
        private const string StringTypeHashNotFinal = "atHashWithStringNotFinal";

        /// <summary>
        /// The string representation of the <see cref="StringType.Hash"/> constant.
        /// </summary>
        private const string StringTypeHashString = "atHashString";

        /// <summary>
        /// The string representation of the <see cref="StringType.HashValue"/> constant.
        /// </summary>
        private const string StringTypeHashValue = "atHashValue";

        /// <summary>
        /// The string representation of the <see cref="StringType.Member"/> constant.
        /// </summary>
        private const string StringTypeMember = "member";

        /// <summary>
        /// The string representation of the <see cref="StringType.Pointer"/> constant.
        /// </summary>
        private const string StringTypePointer = "pointer";

        /// <summary>
        /// The string representation of the <see cref="StringType.WideMember"/> constant.
        /// </summary>
        private const string StringTypeWideMember = "wide_member";

        /// <summary>
        /// The string representation of the <see cref="StringType.WidePointer"/> constant.
        /// </summary>
        private const string StringTypeWidePointer = "wide_pointer";

        /// <summary>
        /// The string representation of the <see cref="StringType.WideString"/> constant.
        /// </summary>
        private const string StringTypeWideString = "atWideString";

        /// <summary>
        /// The name of the xml attribute containing the data for the
        /// <see cref="InitialValue"/> property.
        /// </summary>
        private const string XmlInitialAttr = "init";

        /// <summary>
        /// The name of the xml attribute containing the data for the <see cref="Size"/>
        /// property.
        /// </summary>
        private const string XmlSizeAttr = "size";

        /// <summary>
        /// The name of the xml attribute containing the data for the <see cref="Type"/>
        /// property.
        /// </summary>
        private const string XmlTypeAttr = "type";

        /// <summary>
        /// The private field used for the <see cref="InitialValue"/> property.
        /// </summary>
        private string _initialValue;

        /// <summary>
        /// The private field used for the <see cref="Size"/> property.
        /// </summary>
        private string _size;

        /// <summary>
        /// The private field used for the <see cref="Type"/> property.
        /// </summary>
        private string _type;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="StringMember"/> class to be one of the
        /// members of the specified structure.
        /// </summary>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        public StringMember(IStructure structure)
            : base(structure)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="StringMember"/> class as a copy of the
        /// specified instance.
        /// </summary>
        /// <param name="other">
        /// The instance to copy.
        /// </param>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        public StringMember(StringMember other, IStructure structure)
            : base(other, structure)
        {
            this._initialValue = other._initialValue;
            this._size = other._size;
            this._type = other._type;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="StringMember"/> class.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        public StringMember(XmlReader reader, IStructure structure)
            : base(reader, structure)
        {
            this.Deserialise(reader);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a value indicating whether a string instance representing this member is
        /// limited to a fixed length or not.
        /// </summary>
        public bool HasLimitedLength
        {
            get
            {
                if (this.Type == StringType.Member || this.Type == StringType.WideMember)
                {
                    return true;
                }

                if (this.Size != 0)
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// Gets or sets the initial value that any instance of this member is set to.
        /// </summary>
        public string InitialValue
        {
            get { return this._initialValue; }
            set { this.SetProperty(ref this._initialValue, value); }
        }

        /// <summary>
        /// Gets or sets the maximum length this string can have. A value of 0 for no limit.
        /// </summary>
        public ushort Size
        {
            get { return this.Dictionary.To<ushort>(this._size, 0); }
            set { this.SetProperty(ref this._size, value.ToString()); }
        }

        /// <summary>
        /// Gets or sets the type of string this string member represents in the c++ code.
        /// </summary>
        public StringType Type
        {
            get { return this.GetTypeFromString(this._type); }
            set { this.SetProperty(ref this._type, this.GetStringFromType(value)); }
        }

        /// <summary>
        /// Gets the type name for this member.
        /// </summary>
        public override string TypeName
        {
            get { return "string"; }
        }

        /// <summary>
        /// Gets a value indicating whether a instance of this string member should use
        /// wide characters  (i.e utf16 encoding).
        /// </summary>
        public bool UsesWideCharacters
        {
            get
            {
                if (this.Type == StringType.WidePointer)
                {
                    return true;
                }
                else if (this.Type == StringType.WideMember)
                {
                    return true;
                }
                else if (this.Type == StringType.WideString)
                {
                    return true;
                }

                return false;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new <see cref="StringMember"/> that is a copy of this instance.
        /// </summary>
        /// <returns>
        /// A new <see cref="StringMember"/> that is a copy of this instance.
        /// </returns>
        public new StringMember Clone()
        {
            return new StringMember(this, this.Structure);
        }

        /// <summary>
        /// Creates a <see cref="ITunable"/> object that represents a instance of this member
        /// with it's default value set.
        /// </summary>
        /// <param name="parent">
        /// The tunable parent the new tunable will belong to.
        /// </param>
        /// <returns>
        /// A parCodeGen tunable that represents a instance of this member.
        /// </returns>
        public override ITunable CreateTunable(ITunableParent parent)
        {
            return new StringTunable(this, parent);
        }

        /// <summary>
        /// Creates a <see cref="ITunable"/> object that represents a instance of this member
        /// using the specified Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The Xml.XmlReader that provides the data used to initialise the new tunable.
        /// </param>
        /// <param name="parent">
        /// The tunable parent the new tunable will belong to.
        /// </param>
        /// <param name="log">
        /// A object that is used to log any warnings or errors produced during the parsing of
        /// the specified reader.
        /// </param>
        /// <returns>
        /// A parCodeGen tunable that represents a instance of this member.
        /// </returns>
        public override ITunable CreateTunable(
            XmlReader reader, ITunableParent parent, ILog log)
        {
            return new StringTunable(reader, this, parent, log);
        }

        /// <summary>
        /// Creates a deep copy of the current instance.
        /// </summary>
        /// <returns>
        /// A deep copy of the current System.Object.
        /// </returns>
        public override object DeepClone()
        {
            return this.Clone();
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified
        /// <see cref="StringMember"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public bool Equals(StringMember other)
        {
            if (other == null)
            {
                return false;
            }

            return object.ReferenceEquals(other, this);
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="IMember"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public override bool Equals(IMember other)
        {
            return this.Equals(other as StringMember);
        }

        /// <summary>
        /// Writes the xml representation of this instance onto the specified
        /// System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that the xml representation of this instance should be
        /// written to.
        /// </param>
        public override void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                throw new SmartArgumentNullException(() => writer);
            }

            this.SerialiseBaseAttributes(writer);
            if (this._type != null)
            {
                writer.WriteAttributeString(XmlTypeAttr, this._type);
            }

            if (this._initialValue != null)
            {
                writer.WriteAttributeString(XmlInitialAttr, this._initialValue);
            }

            if (this._size != null)
            {
                writer.WriteAttributeString(XmlSizeAttr, this._size);
            }
        }

        /// <summary>
        /// Validates this entity and creates a validation result object containing the errors
        /// and warnings.
        /// </summary>
        /// <param name="recursive">
        /// A value indicating whether any child models to this one should also be validated.
        /// </param>
        /// <returns>
        /// A validation result object containing all of the errors and warnings associated
        /// with this validation pass.
        /// </returns>
        public override ValidationResult Validate(bool recursive)
        {
            ValidationResult result = new ValidationResult();
            this.ValidateBaseProperties(result);

            if (this._type == null)
            {
                string msg = StringTable.MissingRequiredAttributeError;
                msg = msg.FormatCurrent("type", "string");
                result.AddError(msg, this.Location);
            }
            else if (String.IsNullOrWhiteSpace(this._type))
            {
                string msg = StringTable.EmptyRequiredAttributeError;
                msg = msg.FormatCurrent("type", "string");
                result.AddError(msg, this.Location);
            }

            if (this.Type == StringType.Unrecognised)
            {
                string msg = StringTable.UnrecognisedRequiredAttributeError;
                msg = msg.FormatCurrent("type", "string", this._type);
                result.AddError(msg, this.Location);
            }

            TryResult<ushort> sizeTry = this.Dictionary.TryTo<ushort>(this._size, 0);
            if (this._size != null && !sizeTry.Success)
            {
                string msg = StringTable.FormatAttributeWarning;
                msg = msg.FormatCurrent(XmlSizeAttr, this._size, "u16");
                result.AddWarning(msg, this.Location);
            }

            if (this.Type == StringType.Member || this.Type == StringType.WideMember)
            {
                if (this._size == null || !sizeTry.Success)
                {
                    string msg = StringTable.InvalidStringSizeError;
                    result.AddError(msg, this.Location);
                }
            }

            return result;
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            this._initialValue = reader.GetAttribute(XmlInitialAttr);
            this._size = reader.GetAttribute(XmlSizeAttr);
            this._type = reader.GetAttribute(XmlTypeAttr);
            reader.Skip();
        }

        /// <summary>
        /// Gets the string value that is equivalent to the specified type.
        /// </summary>
        /// <param name="type">
        /// The type that the returned string will be equivalent to.
        /// </param>
        /// <returns>
        /// The string that is equivalent to the specified type.
        /// </returns>
        private string GetStringFromType(StringType type)
        {
            switch (type)
            {
                case StringType.Pointer:
                    return StringTypePointer;
                case StringType.Member:
                    return StringTypeMember;
                case StringType.Const:
                    return StringTypeConst;
                case StringType.AtString:
                    return StringTypeAtString;
                case StringType.WidePointer:
                    return StringTypeWidePointer;
                case StringType.WideMember:
                    return StringTypeWideMember;
                case StringType.WideString:
                    return StringTypeWideString;
                case StringType.Hash:
                    return StringTypeHashString;
                case StringType.FinalHash:
                    return StringTypeFinalHashString;
                case StringType.HashValue:
                    return StringTypeHashValue;
                case StringType.HashDev:
                    return StringTypeHashDev;
                case StringType.HashBank:
                    return StringTypeHashBank;
                case StringType.HashNotFinal:
                    return StringTypeHashNotFinal;
                case StringType.Unknown:
                case StringType.Unrecognised:
                default:
                    return null;
            }
        }

        /// <summary>
        /// Gets the string type that is equivalent to the specified String.
        /// </summary>
        /// <param name="type">
        /// The string to determine the type to return.
        /// </param>
        /// <returns>
        /// The type that is equivalent to the specified string.
        /// </returns>
        private StringType GetTypeFromString(string type)
        {
            if (type == null)
            {
                return StringType.Unknown;
            }
            else if (String.Equals(type, StringTypePointer))
            {
                return StringType.Pointer;
            }
            else if (String.Equals(type, StringTypeMember))
            {
                return StringType.Member;
            }
            else if (String.Equals(type, StringTypeConst))
            {
                return StringType.Const;
            }
            else if (String.Equals(type, StringTypeAtString))
            {
                return StringType.AtString;
            }
            else if (String.Equals(type, StringTypeWidePointer))
            {
                return StringType.WidePointer;
            }
            else if (String.Equals(type, StringTypeWideMember))
            {
                return StringType.WideMember;
            }
            else if (String.Equals(type, StringTypeWideString))
            {
                return StringType.WideString;
            }
            else if (String.Equals(type, StringTypeHashString))
            {
                return StringType.Hash;
            }
            else if (String.Equals(type, StringTypeFinalHashString))
            {
                return StringType.FinalHash;
            }
            else if (String.Equals(type, StringTypeHashValue))
            {
                return StringType.HashValue;
            }
            else if (String.Equals(type, StringTypeHashDev))
            {
                return StringType.HashDev;
            }
            else if (String.Equals(type, StringTypeHashBank))
            {
                return StringType.HashBank;
            }
            else if (String.Equals(type, StringTypeHashNotFinal))
            {
                return StringType.HashNotFinal;
            }
            else
            {
                return StringType.Unrecognised;
            }
        }
        #endregion Methods
    } // RSG.Metadata.Model.Definitions.Members.StringMember {Class}
} // RSG.Metadata.Model.Definitions.Members {Namespace}
