﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.ConfigParser;
using RSG.Model.Common;
using System.IO;
using RSG.ManagedRage;
using RSG.Platform;
using RSG.Statistics.Client.ServiceClients;
using RSG.Statistics.Common.Dto.GameAssets;
using System.Diagnostics;
using RSG.Base.Logging;
using RSG.Base.Configuration;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Model.Asset.GameText;
using RSG.Model.Common.GameText;
using RSG.Model.Asset.Util;

namespace RSG.Model.Vehicle
{
    /// <summary>
    /// 
    /// </summary>
    public class VehicleFactory : IVehicleFactory
    {
        #region Constants
        /// <summary>
        /// Location of the vehicles.meta file from which to source the local data vehicle collection list.
        /// </summary>
        private const String _vehiclesMetaFile = @"$(common)\data\levels\gta5\vehicles.meta";
        #endregion // Constants

        #region Member Data
        /// <summary>
        /// Branch we are working off of.
        /// </summary>
        private IBranch _branch;

        /// <summary>
        /// Vehicle factory log.
        /// </summary>
        private static ILog _log = LogFactory.CreateUniversalLog("VehicleFactory");
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        [Obsolete("Use the IBranch version of the constructor.")]
        public VehicleFactory()
            : this(ConfigFactory.CreateConfig())
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="config"></param>
        [Obsolete("Use the IBranch version of the constructor.")]
        public VehicleFactory(IConfig config)
            : this(config.CoreProject.DefaultBranch)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branch"></param>
        [Obsolete("Use the static controller method instead.")]
        public VehicleFactory(IBranch branch)
        {
            _branch = branch;
        }
        #endregion // Constructor(s)

        #region IVehicleFactory Implementation
        /// <summary>
        /// Creates a new vehicle collection from the specified source.
        /// </summary>
        /// <param name="source">Source to use for creating the collection.</param>
        /// <param name="buildIdentifier">Optional build identifier for when the source is Database.</param>
        /// <returns></returns>
        [Obsolete("Use the static method instead.")]
        public IVehicleCollection CreateCollection(DataSource source, string buildIdentifier = null)
        {
            IVehicleCollection collection = null;

            if (source == DataSource.SceneXml)
            {
                _log.Profile("VehicleFactory:CreateCollection:SceneXml");
                collection = CreateCollectionFromExportData(_branch);
                _log.ProfileEnd();
            }
            else if (source == DataSource.Database)
            {
                _log.Profile("VehicleFactory:CreateCollection:Database.");
                collection = CreateCollectionFromDatabase(buildIdentifier);
                _log.ProfileEnd();
            }
            else
            {
                Debug.Assert(false, String.Format("Unknown data source '{0}' specified while creating a vehicle collection.", source.ToString()));
                throw new ArgumentException(String.Format("Unknown data source '{0}' specified while creating a vehicle collection.", source.ToString()));
            }

            return collection;
        }
        #endregion // IVehicleFactory Implementation

        #region Static Controller Methods
        /// <summary>
        /// Creates a new vehicle collection from the specified source.
        /// </summary>
        /// <param name="source">Source to use for creating the collection.</param>
        /// <param name="buildIdentifier">Optional build identifier for when the source is Database.</param>
        /// <returns></returns>
        public static IVehicleCollection CreateCollection(DataSource source, IBranch branch = null, string buildIdentifier = null)
        {
            IVehicleCollection collection = null;

            if (source == DataSource.SceneXml)
            {
                Debug.Assert(branch != null, "Branch must be supplied when creating a weapon collection from SceneXml.");
                if (branch == null)
                {
                    throw new ArgumentNullException("branch", "Branch must be supplied when creating a weapon collection from SceneXml.");
                }

                _log.Profile("VehicleFactory:CreateCollection:SceneXml");
                collection = CreateCollectionFromExportData(branch);
                _log.ProfileEnd();
            }
            else if (source == DataSource.Database)
            {
                Debug.Assert(String.IsNullOrEmpty(buildIdentifier), "Build identifier must be supplied when creating a weapon collection from SceneXml.");
                if (branch == null)
                {
                    throw new ArgumentNullException("buildIdentifier", "Build identifier must be supplied when creating a weapon collection from SceneXml.");
                }

                _log.Profile("VehicleFactory:CreateCollection:Database.");
                collection = CreateCollectionFromDatabase(buildIdentifier);
                _log.ProfileEnd();
            }
            else
            {
                Debug.Assert(false, String.Format("Unknown data source '{0}' specified while creating a vehicle collection.", source.ToString()));
                throw new ArgumentException(String.Format("Unknown data source '{0}' specified while creating a vehicle collection.", source.ToString()));
            }

            return collection;
        }
        #endregion // Static Controller Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private static IVehicleCollection CreateCollectionFromExportData(IBranch branch)
        {
            String exportPath = Path.Combine(branch.Assets, "vehicles");
            LocalVehicleCollection vehicles = new LocalVehicleCollection(exportPath);

            GameTextCollection gameText = new GameTextCollection(branch);
            gameText.LoadLanguage(Language.American);

            
            // Loop over the list of files that contain weapon information.
            foreach (String vehiclesMetaFile in GetVehcileMetaFilenames(branch))
            {
                if (File.Exists(vehiclesMetaFile))
                {
                    try
                    {
                        // Parse the file
                        XDocument xmlDoc = XDocument.Load(vehiclesMetaFile);
                        foreach (XElement itemElem in xmlDoc.XPathSelectElements("//InitDatas/Item"))
                        {
                            // Try and get the vehicle name from the localisation files if it is present otherwise from the game name.
                            VehicleMetadataInfo metadataInfo = LocalVehicle.ExtractDataFromMetadataElement(itemElem);

                            if (metadataInfo.GameName != null)
                            {
                                // Check to see whether we can find a friendly name.
                                String key = metadataInfo.GameName.ToLower();

                                String friendlyName = null;
                                gameText.TryGetText(key, Language.American, RSG.Platform.Platform.PS3, out friendlyName);

                                // Convert the category to one we know about.
                                VehicleCategory vehicleCategory = VehicleCategory.Unknown;
                                VehicleCategoryUtils.TryParseFromRuntimeString(metadataInfo.VehicleType, out vehicleCategory);

                                // Create a new vehicle and add it to the collection.
                                vehicles.Add(new LocalVehicle(metadataInfo.ModelName, friendlyName, metadataInfo.GameName, vehicleCategory, vehicles));
                            }
                            else
                            {
                                _log.Warning("Game name element is null for vehicle.");
                            }
                        }
                    }
                    catch (System.Exception ex)
                    {
                        _log.ToolException(ex, "Unhandled exception occurred while creating the vehicles collection for branch {0}.", branch.Name);
                    }
                }
                else
                {
                    _log.Warning("{0} file doesn't exist.", vehiclesMetaFile);
                }
            }

            return vehicles;
        }

        /// <summary>
        /// Get the list of files we need to parse that contain weapon information.
        /// </summary>
        /// <param name="branch"></param>
        /// <returns></returns>
        public static IEnumerable<String> GetVehcileMetaFilenames(IBranch branch)
        {
            // Check what kind of project we are in to determine the list of files to parse.
            if (branch.Project.IsDLC)
            {
                foreach (String weaponInfoFile in DLCContentUtil.GetFilenames(branch, "VEHICLE_METADATA_FILE"))
                {
                    yield return weaponInfoFile;
                }
            }
            else
            {
                yield return branch.Environment.Subst(_vehiclesMetaFile);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="build"></param>
        /// <returns></returns>
        private static IVehicleCollection CreateCollectionFromDatabase(String buildIdentifier)
        {
            IVehicleCollection vehicles = null;

            try
            {
                using (BuildClient client = new BuildClient())
                {
                    // If no build identifier was supplied, use the latest build.
                    if (buildIdentifier == null)
                    {
                        BuildDto dto = client.GetLatest();
                        if (dto != null)
                        {
                            buildIdentifier = dto.Identifier;
                        }
                    }

                    // Check that the identifier is set.
                    if (buildIdentifier != null)
                    {
                        vehicles = new SingleBuildVehicleCollection(buildIdentifier);

                        foreach (VehicleDto dto in client.GetAllVehiclesForBuild(buildIdentifier).Items)
                        {
                            // Create a new vehicle and add it to the collection.
                            vehicles.Add(new DbVehicle(dto, vehicles));
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.ToolException(ex, "Unhandled exception occurred while creating a vehicle collection from the database.");
            }

            return vehicles;
        }
        #endregion // Private Methods
    } // VehicleFactory
}
