﻿// --------------------------------------------------------------------------------------------
// <copyright file="BaseVectorMember.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions.Members
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using System.Xml;
    using RSG.Base.Extensions;
    using RSG.Editor;
    using RSG.Metadata.Model.Resources;
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// Provides a base class to a vector member node in the parCodeGen system that can be
    /// instanced in a metadata file.
    /// </summary>
    public abstract class BaseVectorMember :
        MemberBase,
        IHasInitialValue<float[]>,
        IHasRange<float>,
        IHasStep<float[]>,
        IHasPrecision,
        IHasDecimalPlace,
        IScalarMember
    {
        #region Fields
        /// <summary>
        /// The string representation of the <see cref="VectorType.Colour"/> constant.
        /// </summary>
        private const string VectorTypeColour = "color";

        /// <summary>
        /// The name of the xml attribute containing the data for the
        /// <see cref="DecimalPlaces"/> property.
        /// </summary>
        private const string XmlDecimalPlacesAttr = "ui_decimalPlaces";

        /// <summary>
        /// The name of the xml attribute containing the data for the
        /// <see cref="HighPrecision"/> property.
        /// </summary>
        private const string XmlHighPrecisionAttr = "highPrecision";

        /// <summary>
        /// The name of the xml attribute containing the data for the
        /// <see cref="InitialValue"/> property.
        /// </summary>
        private const string XmlInitialAttr = "init";

        /// <summary>
        /// The name of the xml attribute containing the data for the <see cref="Maximum"/>
        /// property.
        /// </summary>
        private const string XmlMaxAttr = "max";

        /// <summary>
        /// The name of the xml attribute containing the data for the <see cref="Minimum"/>
        /// property.
        /// </summary>
        private const string XmlMinAttr = "min";

        /// <summary>
        /// The name of the xml attribute containing the data for the <see cref="Step"/>
        /// property.
        /// </summary>
        private const string XmlStepAttr = "step";

        /// <summary>
        /// The name of the xml attribute containing the data for the <see cref="Type"/>
        /// property.
        /// </summary>
        private const string XmlTypeAttr = "type";

        /// <summary>
        /// The private field used for the <see cref="DecimalPlaces"/> property.
        /// </summary>
        private string _decimalPlaces;

        /// <summary>
        /// The private field used for the <see cref="HighPrecision"/> property.
        /// </summary>
        private string _highPrecision;

        /// <summary>
        /// The private field used for the <see cref="InitialValue"/> property.
        /// </summary>
        private string _initialValue;

        /// <summary>
        /// The private field used for the <see cref="Maximum"/> property.
        /// </summary>
        private string _maximum;

        /// <summary>
        /// The private field used for the <see cref="Minimum"/> property.
        /// </summary>
        private string _minimum;

        /// <summary>
        /// The private field used for the <see cref="Step"/> property.
        /// </summary>
        private string _step;

        /// <summary>
        /// The private field used for the <see cref="Type"/> property.
        /// </summary>
        private string _type;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="BaseVectorMember"/> class to be one of
        /// the members of the specified structure.
        /// </summary>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        protected BaseVectorMember(IStructure structure)
            : base(structure)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="BaseVectorMember"/> class as a copy of
        /// the specified instance.
        /// </summary>
        /// <param name="other">
        /// The instance to copy.
        /// </param>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        protected BaseVectorMember(BaseVectorMember other, IStructure structure)
            : base(other, structure)
        {
            this._initialValue = other._initialValue;
            this._minimum = other._minimum;
            this._maximum = other._maximum;
            this._step = other._step;
            this._decimalPlaces = other._decimalPlaces;
            this._highPrecision = other._highPrecision;
            this._type = other._type;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="BaseVectorMember"/> class.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        protected BaseVectorMember(XmlReader reader, IStructure structure)
            : base(reader, structure)
        {
            this.Deserialise(reader);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the decimal place count the value of an instance to this member has.
        /// </summary>
        public short DecimalPlaces
        {
            get { return this.Dictionary.To<short>(this._decimalPlaces, -1); }
            set { this.SetProperty(ref this._decimalPlaces, value.ToString()); }
        }

        /// <summary>
        /// Gets the dimension of the vector member.
        /// </summary>
        public abstract int Dimension { get; }

        /// <summary>
        /// Gets or sets a value indicating whether this member has high precision on or off.
        /// </summary>
        public bool HighPrecision
        {
            get { return this.Dictionary.To<bool>(this._highPrecision, false); }
            set { this.SetProperty(ref this._highPrecision, value.ToString()); }
        }

        /// <summary>
        /// Gets or sets the initial value that any instance of this member is set to.
        /// </summary>
        public float[] InitialValue
        {
            get
            {
                return this.Dictionary.To<float>(this._initialValue, this.Dimension, 0.0f);
            }

            set
            {
                if (value == null)
                {
                    this.SetProperty(ref this._initialValue, null);
                }
                else
                {
                    string newValue = String.Join(", ", value);
                    this.SetProperty(ref this._initialValue, newValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the maximum value that a instance of this member can be set to.
        /// </summary>
        public float Maximum
        {
            get { return this.Dictionary.To<float>(this._maximum, float.MaxValue * 0.001f); }
            set { this.SetProperty(ref this._maximum, value.ToString()); }
        }

        /// <summary>
        /// Gets or sets the minimum value that a instance of this member can be set to.
        /// </summary>
        public float Minimum
        {
            get { return this.Dictionary.To<float>(this._minimum, float.MaxValue * -0.001f); }
            set { this.SetProperty(ref this._minimum, value.ToString()); }
        }

        /// <summary>
        /// Gets a value indicating whether this integer member represents a colour value.
        /// </summary>
        public virtual bool RepresentsColourValue
        {
            get { return false; }
        }

        /// <summary>
        /// Gets the number of string components separated by whitespaces the scalar value
        /// contains.
        /// </summary>
        public int ScalarComponentCount
        {
            get { return this.Dimension; }
        }

        /// <summary>
        /// Gets a string that represents the content type of this scalar member.
        /// </summary>
        public string ScalarContent
        {
            get { return "vector" + this.Dimension.ToStringInvariant(); }
        }

        /// <summary>
        /// Gets or sets the increase and decrease amount for a single step.
        /// </summary>
        public float[] Step
        {
            get
            {
                return this.Dictionary.To<float>(this._step, this.Dimension, 0.1f);
            }

            set
            {
                if (value == null)
                {
                    this.SetProperty(ref this._step, null);
                }
                else
                {
                    string newValue = string.Join(", ", value);
                    this.SetProperty(ref this._step, newValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the type of integer this member represents in the c++ code.
        /// </summary>
        public VectorType Type
        {
            get { return this.GetTypeFromString(this._type); }
            set { this.SetProperty(ref this._type, this.GetStringFromType(value)); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a <see cref="ITunable"/> object that represents a instance of this member
        /// using the specified string components to initialise it.
        /// </summary>
        /// <param name="components">
        /// The string components that provides the data used to initialise the new tunable.
        /// </param>
        /// <param name="parent">
        /// The tunable parent the new tunable will belong to.
        /// </param>
        /// <returns>
        /// A parCodeGen tunable that represents a instance of this member.
        /// </returns>
        public virtual ITunable CreateTunable(List<string> components, ITunableParent parent)
        {
            return null;
        }

        /// <summary>
        /// Writes the xml representation of this instance onto the specified
        /// System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that the xml representation of this instance should be
        /// written to.
        /// </param>
        public override void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                throw new SmartArgumentNullException(() => writer);
            }

            this.SerialiseBaseAttributes(writer);
            if (this._initialValue != null)
            {
                writer.WriteAttributeString(XmlInitialAttr, this._initialValue);
            }

            if (this._minimum != null)
            {
                writer.WriteAttributeString(XmlMinAttr, this._minimum);
            }

            if (this._maximum != null)
            {
                writer.WriteAttributeString(XmlMaxAttr, this._maximum);
            }

            if (this._step != null)
            {
                writer.WriteAttributeString(XmlStepAttr, this._step);
            }

            if (this._decimalPlaces != null)
            {
                writer.WriteAttributeString(XmlDecimalPlacesAttr, this._decimalPlaces);
            }

            if (this._highPrecision != null)
            {
                writer.WriteAttributeString(XmlHighPrecisionAttr, this._highPrecision);
            }

            if (this._type != null)
            {
                writer.WriteAttributeString(XmlTypeAttr, this._type);
            }
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            this._initialValue = reader.GetAttribute(XmlInitialAttr);
            this._minimum = reader.GetAttribute(XmlMinAttr);
            this._maximum = reader.GetAttribute(XmlMaxAttr);
            this._step = reader.GetAttribute(XmlStepAttr);
            this._decimalPlaces = reader.GetAttribute(XmlDecimalPlacesAttr);
            this._highPrecision = reader.GetAttribute(XmlHighPrecisionAttr);
            this._type = reader.GetAttribute(XmlTypeAttr);
            reader.Skip();
        }

        /// <summary>
        /// Gets the string value that is equivalent to the specified type.
        /// </summary>
        /// <param name="type">
        /// The type that the returned string will be equivalent to.
        /// </param>
        /// <returns>
        /// The string that is equivalent to the specified type.
        /// </returns>
        private string GetStringFromType(VectorType type)
        {
            switch (type)
            {
                case VectorType.Colour:
                    return VectorTypeColour;
                case VectorType.Standard:
                case VectorType.Unrecognised:
                default:
                    return null;
            }
        }

        /// <summary>
        /// Gets the vector type that is equivalent to the specified string.
        /// </summary>
        /// <param name="type">
        /// The string to determine the type to return.
        /// </param>
        /// <returns>
        /// The type that is equivalent to the specified string.
        /// </returns>
        private VectorType GetTypeFromString(string type)
        {
            if (type == null)
            {
                return VectorType.Standard;
            }
            else if (String.Equals(type, VectorTypeColour))
            {
                return VectorType.Colour;
            }
            else
            {
                return VectorType.Unrecognised;
            }
        }
        #endregion Methods
    } // RSG.Metadata.Model.Definitions.Members.BaseVectorMember {Class}
} // RSG.Metadata.Model.Definitions.Members {Namespace}
