﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Runtime.Serialization;

namespace RSG.Model.Statistics.Captures
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class GpuResult
    {
        #region Properties
        /// <summary>
        /// Name of this gpu result.
        /// </summary>
        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// Time spent for this particular gpu reading.
        /// </summary>
        [DataMember]
        public float Time { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public GpuResult()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public GpuResult(XElement element)
        {
            // Extract the various stats
            XElement nameElement = element.Element("name");
            if (nameElement != null)
            {
                Name = nameElement.Value;
            }

            XElement timeElement = element.Element("time");
            if (timeElement != null && timeElement.Attribute("value") != null)
            {
                Time = Single.Parse(timeElement.Attribute("value").Value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="average"></param>
        public GpuResult(string name, float time)
        {
            Name = name;
            Time = time;
        }
        #endregion // Constructor(s)
    } // GpuResult
}
