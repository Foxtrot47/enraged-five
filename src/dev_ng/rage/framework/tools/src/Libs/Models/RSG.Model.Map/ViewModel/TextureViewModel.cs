﻿using System;
using System.ComponentModel;
using System.Windows.Media.Imaging;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor;

namespace RSG.Model.Map.ViewModel
{
    public class TextureViewModel
        : UserDataViewModelBase
    {

        private static String[] ValidExtentions = new String[3] { ".png", ".jpg", ".bmp" };

        #region Properties

        /// <summary>
        /// The model reference that this view model represents.
        /// </summary>
        public Texture Model
        {
            get { return m_model; }
            set { m_model = value; }
        }
        private Texture m_model;

        /// <summary>
        /// A small get property that returns the smallest possible
        /// thumbnail for this texture
        /// </summary>
        public BitmapImage ThumbnailBig
        {
            get
            {
                if (System.IO.File.Exists(this.Model.Filename))
                {
                    Uri uriSource = new Uri(this.Model.Filename, UriKind.RelativeOrAbsolute);

                    BitmapImage big = new BitmapImage();
                    big.BeginInit();
                    big.CreateOptions = BitmapCreateOptions.None;
                    big.CacheOption = BitmapCacheOption.None;
                    big.UriSource = uriSource;
                    big.EndInit();

                    big.Freeze();

                    return big;
                }
                return null;
            }
        }

        /// <summary>
        /// A small get property that returns the smallest possible
        /// thumbnail for this texture
        /// </summary>
        public BitmapImage ThumbnailSmall
        {
            get
            {
                if (System.IO.File.Exists(this.Model.Filename))
                {
                    Uri uriSource = new Uri(this.Model.Filename, UriKind.RelativeOrAbsolute);

                    BitmapImage small = new BitmapImage();
                    small.BeginInit();
                    small.DecodePixelWidth = 1;
                    small.CreateOptions = BitmapCreateOptions.None;
                    small.CacheOption = BitmapCacheOption.None;
                    small.UriSource = uriSource;
                    small.EndInit();

                    small.Freeze();

                    return small;
                }
                return null;
            }
        }

        /// <summary>
        /// The thumbnail that for this texture, and small 16x16
        /// scaled image of the texture.
        /// This has to be bound to using IsAsync or it'll be too long a delay to show
        /// a full texture dictionary
        /// </summary>
        public BitmapImage Thumbnail
        {
            get
            {
                return ThumbnailManager.GetThumbnail(this.Model.Filename);
            }
            set
            {
                m_thumbnail = value;
            }
        }
        private BitmapImage m_thumbnail = null;

        /// <summary>
        /// A small get property that returns the smallest possible
        /// thumbnail for this texture
        /// </summary>
        public BitmapImage ThumbnailFast
        {
            get
            {
                return null;
            }
        }

        public Boolean ValidTexture
        {
            get;
            set;
        }

        #endregion // Properties

        #region Constructor

        /// <summary>
        /// Default Constructor
        /// </summary>
        public TextureViewModel()
        {
        }

        /// <summary>
        /// Default Constructor
        /// </summary>
        public TextureViewModel(IViewModel parent, Texture model)
        {
            this.Parent = parent;
            this.Model = model;
            this.Thumbnail = null;
            if (System.IO.File.Exists(this.Model.Filename))
            {
                if (ValidExtentions.Contains(System.IO.Path.GetExtension(this.Model.Filename)))
                    ValidTexture = true;
                else
                    ValidTexture = false;
            }
            else
            {
                ValidTexture = false;
            }
        }

        #endregion // Constructor

        public void OnThumbnailCreated()
        {
            OnPropertyChanged("Thumbnail");
        }
    }
}
