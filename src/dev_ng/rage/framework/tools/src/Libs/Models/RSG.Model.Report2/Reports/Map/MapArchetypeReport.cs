﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using RSG.Base.Tasks;
using System.Web.UI;
using RSG.Model.Common.Map;
using System.Xml;
using RSG.Model.Common;

namespace RSG.Model.Report.Reports.Map
{
    /// <summary>
    /// Map archetype report. 
    /// Create report for verifying map archetypes
    /// - Drawables, fragments that do not have an archetype
    /// - Archetypes that don't have a drawable/fragment
    /// - Texture dictionaries missing or redundant (see also Parent TXD XML)
    /// </summary>
    public class MapArchetypeReport : Report, IReportStreamProvider, IDynamicLevelReport
    {
        #region Texture dictionary file helper class

        class TextureDictionary
        {
            #region Constants
            
            /// <summary>
            /// Xml element name for global dictionary entries.
            /// </summary>
            private static readonly string GLOBAL_DICTIONARY = "globalDictionary";

            /// <summary>
            /// Xml element name for source dictionary entries.
            /// </summary>
            private static readonly string SOURCE_DICTIONARY = "sourceDictionary";

            #endregion

            #region Enumeration

            /// <summary>
            /// Type of texture dictionary.
            /// </summary>
            public enum DictionaryType
            {
                /// <summary>
                /// Source dictionary.
                /// </summary>
                Source,

                /// <summary>
                /// Global dictionary.
                /// </summary>
                Global
            }

            #endregion

            #region Private member fields

            private XmlDocument m_doc;

            #endregion

            #region Public properties

            /// <summary>
            /// Global dictionary names.
            /// </summary>
            public List<string> Globals { get; private set; }

            /// <summary>
            /// Source dictionary names.
            /// </summary>
            public List<string> Source { get; private set; }

            /// <summary>
            /// Dictionary names not found.
            /// </summary>
            public List<string> NotFound { get; private set; }

            #endregion

            #region Constructor

            /// <summary>
            /// Constructor.
            /// </summary>
            /// <param name="doc">Xml document.</param>
            public TextureDictionary(XmlDocument doc)
            {
                m_doc = doc;
                Globals = new List<string>();
                Source = new List<string>();
                NotFound = new List<string>();
            }

            #endregion

            #region Public methods

            /// <summary>
            /// Clear the data.
            /// </summary>
            public void Clear()
            {
                Globals.Clear();
                Source.Clear();
                NotFound.Clear();
            }

            /// <summary>
            /// Checks the TXDs file and returns true if the dictionary is found.
            /// </summary>
            /// <param name="dictionary">Dictionary name.</param>
            /// <param name="dictionaryType">Dictionary type.</param>
            /// <returns>True if the dictionary is found.</returns>
            public bool ContainsDictionary(string dictionary, DictionaryType dictionaryType)
            {
                string criteria = dictionaryType == DictionaryType.Global ? GLOBAL_DICTIONARY : SOURCE_DICTIONARY;
                List<string> list = dictionaryType == DictionaryType.Global ? Globals : Source;

                foreach (XmlNode node in m_doc.SelectNodes("//" + criteria))
                {
                    string nodeName = node.Attributes["name"].Value;
                    if (nodeName == dictionary)
                    {
                        if (!list.Contains(nodeName))
                        {
                            list.Add(nodeName);
                        }

                        return true;
                    }
                }

                if (!NotFound.Contains(dictionary))
                {
                    NotFound.Add(dictionary);
                }

                return false;
            }

            /// <summary>
            /// Get information about a texture dictionary.
            /// </summary>
            /// <param name="dictionary">Dictionary name.</param>
            /// <param name="section">The section name.</param>
            /// <returns>The number of textures.</returns>
            public int GetDictionarySection(string dictionary, ref string section)
            {
                int textureCount = 0;

                foreach (XmlNode node in m_doc.SelectNodes("//" + SOURCE_DICTIONARY))
                {
                    string nodeName = node.Attributes["name"].Value;
                    if (nodeName == dictionary)
                    {
                        section = node.Attributes["section"] != null ? node.Attributes["section"].Value : "";
                        textureCount = node.Attributes["textureCount"] != null ? int.Parse(node.Attributes["textureCount"].Value) : 0;
                    }
                }

                return textureCount;
            }

            /// <summary>
            /// Get an enumerable list of unused TXDs.
            /// </summary>
            /// <param name="dictionaryType">Dictionary type.</param>
            /// <returns>An enumerable list of names.</returns>
            public IEnumerable<string> GetUnusedTXDs(DictionaryType dictionaryType)
            {
                string criteria = dictionaryType == DictionaryType.Global ? GLOBAL_DICTIONARY : SOURCE_DICTIONARY;
                List<string> list = dictionaryType == DictionaryType.Global ? Globals : Source;

                foreach (XmlNode node in m_doc.SelectNodes("//" + criteria))
                {
                    string nodeName = node.Attributes["name"].Value;
                    if (!list.Contains(nodeName))
                    {
                        yield return nodeName;
                    }
                }
            }

            #endregion
        }

        #endregion

        #region Private member fields

        private ITask m_task;
        private Dictionary<IMapArchetype, int> m_archeTypeCounter;
        private Dictionary<IMapSection, List<KeyValuePair<IEntity, IArchetype>>> m_missingArchetypes;
        private List<IMapSection> m_sectionNoArcheTypes;
        private Dictionary<IMapSection, List<IEntity>> m_entityNoArchetype;
        private TextureDictionary m_txds;

        #endregion

        #region Constants

        private const String c_name = "Map Archetype Report";
        private const String c_description = "Generate and display a map archetype report.";

        #endregion

        #region Public properties

        /// <summary>
        /// Stream that holds the report.
        /// </summary>
        public Stream Stream { get; private set; }

        /// <summary>
        /// Task instance used to create the report.
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (m_task == null)
                {
                    CompositeTask task = new CompositeTask("Generate Report");
                    task.AddSubTask(new ActionTask("Loading data", (context, progress) => EnsureDataLoaded((DynamicLevelReportContext)context, progress)), 10);
                    task.AddSubTask(new ActionTask("Generating report", (context, progress) => GenerateReport((DynamicLevelReportContext)context, progress)));
                    m_task = task;
                }
                return m_task;
            }
        }

        /// <summary>
        /// List of stats that are required to generate this report.
        /// </summary>
        public IEnumerable<StreamableStat> RequiredStats
        {
            get { return null; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        public MapArchetypeReport()
            : base(c_name, c_description)
        {
            m_archeTypeCounter = new Dictionary<IMapArchetype, int>();
            m_missingArchetypes = new Dictionary<IMapSection, List<KeyValuePair<IEntity, IArchetype>>>();
            m_sectionNoArcheTypes = new List<IMapSection>();
            m_entityNoArchetype = new Dictionary<IMapSection,List<IEntity>>();
        }

        #endregion

        #region Protected methods

        /// <summary>
        /// Generate the report.
        /// </summary>
        /// <param name="context">Context.</param>
        /// <param name="progress">Progress.</param>
        protected virtual void GenerateReport(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            MemoryStream ms = new MemoryStream();
            TextWriter reportFile = new StreamWriter(ms);
            HtmlTextWriter writer = new HtmlTextWriter(reportFile);
            writer.RenderBeginTag(HtmlTextWriterTag.Html);
            writer.RenderBeginTag(HtmlTextWriterTag.Head);
            writer.RenderEndTag();
            writer.RenderBeginTag(HtmlTextWriterTag.Body);
            WriteHeader(writer, HtmlTextWriterTag.H1, "Map Archetypes Report");

            WriteHeader(writer, HtmlTextWriterTag.H2, "Drawables / Fragments with no Archetypes");
            WriteDrawables(context, writer);

            WriteHeader(writer, HtmlTextWriterTag.H2, "Drawables / Fragments with Spurious Archetypes");
            WriteBadArchetypeReferences(context, writer);

            WriteHeader(writer, HtmlTextWriterTag.H2, "Archetypes with no Drawables / Fragments");
            WriteArchetypes(context, writer);

            WriteHeader(writer, HtmlTextWriterTag.H2, "Missing or Redundant Texture Dictionaries");
            WriteTextureDictionaries(context, writer);

            writer.RenderEndTag();
            writer.RenderEndTag();

            writer.Flush();
            ms.Seek(0, SeekOrigin.Begin);
            Stream = ms;
        }

        #endregion

        #region Report output methods

        /// <summary>
        /// Write out an Html header.
        /// </summary>
        /// <param name="writer">Html writer.</param>
        /// <param name="header">Header type.</param>
        /// <param name="heading">The heading text.</param>
        private void WriteHeader(HtmlTextWriter writer, HtmlTextWriterTag header, string heading)
        {
            writer.RenderBeginTag(header);
            writer.Write(heading);
            writer.RenderEndTag();
        }

        /// <summary>
        /// Write out the bad archetype references.
        /// </summary>
        /// <param name="context">Context.</param>
        /// <param name="writer">Html writer.</param>
        private void WriteBadArchetypeReferences(DynamicLevelReportContext context, HtmlTextWriter writer)
        {
            WriteHeader(writer, HtmlTextWriterTag.H3, String.Format("{0} drawables / Fragments with spurious archetypes.", m_missingArchetypes.Count));

            foreach (var kvp in m_missingArchetypes.OrderBy(kvp => kvp.Key.Name))
            {
                WriteHeader(writer, HtmlTextWriterTag.H3, kvp.Key.Name);
                foreach (var item in kvp.Value)
                {
                    writer.WriteLine(String.Format("{0} using {1}", kvp.Key, kvp.Value));
                }
            }
        }
        
        /// <summary>
        /// Write out the missing texture dictionaries.
        /// </summary>
        /// <param name="context">Context.</param>
        /// <param name="writer">Html writer.</param>
        private void WriteTextureDictionaries(DynamicLevelReportContext context, HtmlTextWriter writer)
        {
            WriteHeader(writer, HtmlTextWriterTag.H3, String.Format("Missing TXDs"));

            foreach (var s in GetSortedList<string>(m_txds.NotFound))
            {
                writer.WriteLine(String.Format("{0}<br />", s));
            }

            WriteHeader(writer, HtmlTextWriterTag.H3, String.Format("Redundant TXDs"));
            foreach (var s in GetSortedList<string>(m_txds.GetUnusedTXDs(TextureDictionary.DictionaryType.Source)))
            {
                writer.WriteLine(String.Format("{0}<br />", s));
            }
        }

        /// <summary>
        /// Write out a list of archetypes that have no drawable/fragment instances.
        /// </summary>
        /// <param name="context">Context.</param>
        /// <param name="writer">Html writer.</param>
        private void WriteArchetypes(DynamicLevelReportContext context, HtmlTextWriter writer)
        {
            int count = m_archeTypeCounter.Where(kvp => kvp.Value == 0).Count();
            WriteHeader(writer, HtmlTextWriterTag.H3, String.Format("{0} archetype(s) with no instances found.", count));

            foreach (var kv in m_archeTypeCounter.Where(kvp => kvp.Value == 0).OrderBy(kvp => kvp.Key.Name))
            {
                writer.WriteLine(String.Format("{0} in {1}<br />", kv.Key.Name, kv.Key.ParentSection == null ? "N/A" : kv.Key.ParentSection.DCCSourceFilename));
            }
        }

        /// <summary>
        /// Write out a list of drawable(s) / fragment(s) without an archetype.
        /// </summary>
        /// <param name="context">Context.</param>
        /// <param name="writer">Html writer.</param>
        private void WriteDrawables(DynamicLevelReportContext context, HtmlTextWriter writer)
        {
            WriteHeader(writer, HtmlTextWriterTag.H3, String.Format("{0} drawable(s)/fragment(s) with no archetype assigned.", m_entityNoArchetype.Count));

            foreach (var section in GetSortedList<IMapSection>(m_entityNoArchetype.Keys))
            {
                WriteHeader(writer, HtmlTextWriterTag.H3, section.Name);

                List<IEntity> sortable = new List<IEntity>(m_entityNoArchetype[section]);
                sortable.Sort();
                foreach (var entity in sortable)
                {
                    writer.WriteLine(entity.Name + "<br />");
                }
            }
        }

        #endregion

        #region Private helper methods

        /// <summary>
        /// Clear the data from the previous run.
        /// </summary>
        private void ClearPreviousRun()
        {
            m_archeTypeCounter.Clear();
            m_entityNoArchetype.Clear();
            m_missingArchetypes.Clear();
            m_sectionNoArcheTypes.Clear();
            if (m_txds != null)
            {
                m_txds.Clear();
            }
        }

        /// <summary>
        /// Ensure that the data required by the report is loaded.
        /// </summary>
        /// <param name="context">Context.</param>
        /// <param name="progress">Progress.</param>
        private void EnsureDataLoaded(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            ClearPreviousRun();

            IMapHierarchy hierarchy = context.Level.MapHierarchy;

            if (hierarchy != null)
            {
                LoadXmlFile(context);
                CreateArchetypeCounter(hierarchy);

                float increment = 1.0f / hierarchy.AllSections.Count();

                foreach (IMapSection section in hierarchy.AllSections)
                {
                    context.Token.ThrowIfCancellationRequested();

                    // Update the progress information
                    string message = String.Format("Loading {0}", section.Name);
                    progress.Report(new TaskProgress(increment, true, message));

                    ParseSection(section);
                }
            }
        }

        /// <summary>
        /// Load the XML file containing the parent texture dictionaries.
        /// </summary>
        /// <param name="context">Map report context.</param>
        private void LoadXmlFile(DynamicLevelReportContext context)
        {
            string txdsFile = Path.Combine(context.GameView.AssetsDir, @"maps\ParentTxds.xml");

            if (!File.Exists(txdsFile)) // User doesn't have the file? Grab latest.
            {
                P4API.P4Connection p4 = new P4API.P4Connection();
                p4.Connect();
                p4.Run("sync", txdsFile);
                p4.Disconnect();
            }

            XmlDocument doc = new XmlDocument();
            doc.Load(txdsFile);
            m_txds = new TextureDictionary(doc);
        }

        /// <summary>
        /// Create the entries in the archetype counter dictionary.
        /// </summary>
        /// <param name="hierarchy">Map hierarchy.</param>
        private void CreateArchetypeCounter(IMapHierarchy hierarchy)
        {
            IList<IMapArchetype> archetypes = GetArchetypes(hierarchy);
            foreach (var archetype in archetypes)
            {
                m_archeTypeCounter.Add(archetype, 0);
            }
        }

        /// <summary>
        /// Parse the map section and grab the data from it.
        /// </summary>
        /// <param name="section">Section.</param>
        private void ParseSection(IMapSection section)
        {
            section.LoadAllStats();

            if (section.ChildEntities == null || section.ChildEntities.Count == 0)
            {
                return;
            }
            
            foreach (IEntity entity in section.ChildEntities)
            {
                if ((entity.ReferencedArchetype is IDrawableArchetype || entity.ReferencedArchetype is IFragmentArchetype))
                {
                    if (entity.ReferencedArchetype == null)
                    {
                        if (!m_entityNoArchetype.ContainsKey(section))
                        {
                            m_entityNoArchetype.Add(section, new List<IEntity>());
                        }

                        m_entityNoArchetype[section].Add(entity);
                    }
                    else if (entity.ReferencedArchetype is IMapArchetype)
                    {
                        if (!m_archeTypeCounter.ContainsKey((IMapArchetype)entity.ReferencedArchetype))
                        {
                            m_archeTypeCounter.Add((IMapArchetype)entity.ReferencedArchetype, 0);
                        }

                        m_archeTypeCounter[(IMapArchetype)entity.ReferencedArchetype]++;
                        DoTextureWork((IMapArchetype)entity.ReferencedArchetype);
                    }
                }
            }
        }

        /// <summary>
        /// Get the information about the textures from the texture dictionary file.
        /// </summary>
        /// <param name="archeType">Archetype.</param>
        private void DoTextureWork(IMapArchetype archeType)
        {
            if (archeType.TxdExportSizes == null)
            {
                return;
            }

            foreach (var kvp in archeType.TxdExportSizes)
            {
                m_txds.ContainsDictionary(kvp.Key, TextureDictionary.DictionaryType.Source);
            }
        }

        /// <summary>
        /// Get the archetypes from the map hierarchy.
        /// </summary>
        /// <param name="hierarchy">Hierarchy.</param>
        /// <returns>The list of archetypes.</returns>
        private IList<IMapArchetype> GetArchetypes(IMapHierarchy hierarchy)
        {
            HashSet<IMapArchetype> archetypes = new HashSet<IMapArchetype>();

            foreach (IMapSection section in hierarchy.AllSections)
            {
                if (section.Archetypes == null)
                {
                    m_sectionNoArcheTypes.Add(section);
                    continue;
                }

                foreach (var archetype in section.Archetypes)
                {
                    archetypes.Add(archetype);
                }
            }

            List<IMapArchetype> culledList = new List<IMapArchetype>();
            foreach (var archetype in archetypes)
            {
                culledList.Add(archetype);
            }

            return culledList.AsReadOnly();
        }

        /// <summary>
        /// Create a sorted list from the given list.
        /// </summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="originalList">Original unsorted list.</param>
        /// <returns>Enumerate sorted list.</returns>
        private IEnumerable<T> GetSortedList<T>(IEnumerable<T> originalList)
        {
            SortedList<T, object> sortedList = new SortedList<T, object>();
            foreach (T item in originalList)
            {
                sortedList.Add(item, null);
            }

            foreach (T item in sortedList.Keys)
            {
                yield return item;
            }
        }

        #endregion
    }
}
