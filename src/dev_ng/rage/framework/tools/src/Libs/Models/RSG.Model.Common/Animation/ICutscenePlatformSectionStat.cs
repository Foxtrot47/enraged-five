﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common.Animation
{
    /// <summary>
    /// Physical/virtual sizes for a section of a cutscene.
    /// </summary>
    public interface ICutscenePlatformSectionStat
    {
        /// <summary>
        /// Index of this item.
        /// </summary>
        uint Index { get; }

        /// <summary>
        /// Total physical size.
        /// </summary>
        ulong PhysicalSize { get; }

        /// <summary>
        /// Total virtual size.
        /// </summary>
        ulong VirtualSize { get; }
    } // ICutscenePlatformSectionStat
}
