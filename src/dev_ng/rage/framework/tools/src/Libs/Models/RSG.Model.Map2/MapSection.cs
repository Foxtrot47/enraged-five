﻿using System;
using System.Data;
using System.IO;
using System.Xml;
using System.Collections;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.Base.ConfigParser;
using RSG.Model.Map.Util;
using RSG.Model.Map.Statistics;
using RSG.Model.Asset;

namespace RSG.Model.Map
{
    /// <summary>
    /// The different types of map containers that are available
    /// </summary>
    public enum SectionType
    {
        /// <summary>
        /// Type is unknown; happens when you cann't tell the
        /// difference between a prop container and a interior
        /// container.
        /// </summary>
        UNKNOWN = 0,
        /// <summary>
        /// Section represents a map section that has both ide and ipl data in it.
        /// </summary>
        CONTAINER = 1, 
        /// <summary>
        /// Section represents a prop group - a max file that contains xrefs for a map container or a map container that contains rsrefs
        /// </summary>
        CONTAINER_PROPS = 2,
        /// <summary>
        /// Section represents multiple props that can be referenced inside a map contaier
        /// </summary>
        PROPS = 3,
        /// <summary>
        /// Section represents a single interior that can be referenced inside a map contaier
        /// </summary>
        INTERIORS = 4,
        /// <summary>
        /// Section represents a map section with section lods in it.
        /// </summary>
        CONTAINER_LOD = 5,
        /// <summary>
        /// Section represents a map section with section occuludes in it.
        /// </summary>
        CONTAINER_OCCL = 6,
    }

    public enum NeighbourAlgorithm
    {
        /// <summary>
        /// Neighbours are defined as any section within a certain radius of the
        /// sections center point.
        /// </summary>
        RADIUS = 0,

        /// <summary>
        /// Neighbours are defined as any section intersecting the AABB around the
        /// section
        /// </summary>
        BOX = 1,

        /// <summary>
        /// Neighbours are defined as any section intersecting the radius around the
        /// center of the section and within a certain distance of the sections outline
        /// </summary>
        DISTANCE = 2,
    }

    /// <summary>
    /// Represents a single map section, defined using the configuration parser
    /// </summary>
    public class MapSection : FileAssetContainerBase, IMapContainer, IEnumerable<IMapComponent>
    {
        #region Constants
        private readonly string IDE_EXTENSION = "ide";
        private readonly string IPL_EXTENSION = "ipl";
        private readonly string XML_EXTENSION = "xml";
        private readonly string ZIP_EXTENSION = "zip";
        #endregion // Constants

        #region Fields
        private ILevel m_level;
        private ContentNodeMap m_mapNode;
        private bool m_creatingAssetChildren;
        private bool m_assetChildrenCreated;
        private RawSectionStatistics m_rawSectionStatistics;
        private List<List<MapSection>> m_neighbours;
        private object m_syncRoot = new object();
        #endregion // Fields

        #region Properties
        /// <summary>
        /// Reference to the owner Level object.
        /// </summary>
        public ILevel Level
        {
            get { return m_level; }
            set { m_level = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public ContentNodeMapZip SectionNode
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public ContentNodeMap MapNode
        {
            get { return m_mapNode; }
            set
            {
                if (m_mapNode == value)
                    return;
                ContentNodeMap oldValue = this.m_mapNode;
                m_mapNode = value;

                OnMapNodeChanged(oldValue, value);
            }
        }

        /// <summary>
        /// The immediate parent map container, or
        /// null if non exist
        /// </summary>
        public IMapContainer Container
        {
            get;
            set;
        }
        
        /// <summary>
        /// 
        /// </summary>
        public ContentNodeMap.MapSubType SectionType
        {
            get { return this.MapNode == null ? ContentNodeMap.MapSubType.Unknown : this.MapNode.MapType; }
        }

        /// <summary>
        /// Map section exports definitions?
        /// </summary>
        public bool ExportDefinitions
        {
            get;
            set;
        }

        /// <summary>
        /// Map section exports instances?
        /// </summary>
        public bool ExportInstances
        {
            get;
            set;
        }

        /// <summary>
        /// Map section exports RPF data?
        /// </summary>
        public bool ExportData
        {
            get;
            set;
        }

        /// <summary>
        /// The filepath to the scene xml for this map section
        /// </summary>
        public string SceneXmlFilename
        {
            get;
            set;
        }

        /// <summary>
        /// The filepath to the scene xml for this map section that
        /// is in an optimised format.
        /// </summary>
        public string OptimisedSceneFilename
        {
            get;
            set;
        }

        /// <summary>
        /// Represents whether the optimised file
        /// version of the scene xml for this section can
        /// be used when creating the components.
        /// </summary>
        public bool CanUseOptimisedFile
        {
            get;
            set;
        }

        /// <summary>
        /// The filename of the zip file node
        /// </summary>]
        public string ZipFilename
        {
            get;
            set;
        }
        
        /// <summary>
        /// 
        /// </summary>
        public string IdeFilename
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public string IplFilename
        {
            get;
            set;
        }

        /// <summary>
        /// Map section export data source path.
        /// </summary>
        public string ExportPath
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public string MapNodePath
        {
            get;
            private set;
        }

        /// <summary>
        /// The prop group to this map section if one exists and
        /// is applicable
        /// </summary>
        public MapSection PropGroup
        {
            get;
            set;
        }

        /// <summary>
        /// Indiecates whether the asset children are
        /// currently created and in memory.
        /// </summary>
        public bool CreatingAssetChildren
        {
            get { return m_creatingAssetChildren; }
            set
            {
                if (m_creatingAssetChildren == value)
                    return;

                SetPropertyValue(value, () => CreatingAssetChildren,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_creatingAssetChildren = (bool)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// Indiecates whether the asset children are
        /// currently created and in memory.
        /// </summary>
        public bool AssetChildrenCreated
        {
            get { return m_assetChildrenCreated; }
            set
            {
                if (m_assetChildrenCreated == value)
                    return;

                SetPropertyValue(value, () => AssetChildrenCreated,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_assetChildrenCreated = (bool)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// The raw section statistics for this section
        /// </summary>
        public RawSectionStatistics RawSectionStatistics
        {
            get { return m_rawSectionStatistics; }
            set
            {
                SetPropertyValue(value, m_rawSectionStatistics, () => RawSectionStatistics,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_rawSectionStatistics = (RawSectionStatistics)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// The points for this section gotten from the levels vector
        /// map
        /// </summary>
        public List<RSG.Base.Math.Vector2f> VectorMapPoints
        {
            get
            {
                if (Level.VectorMap.ContainsKey(this.Name.ToLower()))
                {
                    return Level.VectorMap[this.Name.ToLower()];
                }
                return null;
            }
        }

        /// <summary>
        /// The neighbours for this map section, first indexed by the alogithim
        /// used to obtain them
        /// </summary>
        public List<List<MapSection>> Neighbours
        {
            get { return m_neighbours; }
            set
            {
                if (m_neighbours == value)
                    return;

                SetPropertyValue(value, () => Neighbours,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_neighbours = (List<List<MapSection>>)newValue;
                        }
                ));
            }
        }
       
        /// <summary>
        /// The area of the section based on the current vector
        /// map points
        /// </summary>
        public float Area
        {
            get;
            set;
        }

        /// <summary>
        /// A sync object that can be used to lock
        /// this map section class for thread synchronize.
        /// </summary>
        public object SyncRoot
        {
            get { return m_syncRoot; }
        }

        /// <summary>
        /// 
        /// </summary>
        public ContainerAttributes ContainerAttributes
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public List<CarGen> CarGens
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public MapSection(ILevel level, ContentNodeMap mapNode, ContentNodeMapZip sectionNode, IMapContainer container)
        {
            this.Level = level;
            this.MapNode = mapNode;
            this.Container = container;
            this.SectionNode = sectionNode;
            this.RawSectionStatistics = new RawSectionStatistics();
            this.CreateExportPaths();

            if (!string.IsNullOrWhiteSpace(this.SceneXmlFilename) && File.Exists(this.SceneXmlFilename))
            {
                this.ContainerAttributes = new ContainerAttributes(this.SceneXmlFilename, this);
            }

            OnVectorMapPointsChanged(this.VectorMapPoints);
        }
        #endregion // Constructors
        
        #region Property Changed Callbacks
        /// <summary>
        /// Called when the main map node gets changed
        /// </summary>
        private void OnMapNodeChanged(ContentNodeMap oldValue, ContentNodeMap newValue)
        {
            if (newValue != null)
            {
                this.Name = newValue.Name;
                this.ExportDefinitions = newValue.ExportDefinitions;
                this.ExportInstances = newValue.ExportInstances;
                this.ExportData = newValue.ExportData;
            }
            else
            {
                this.Name = "Unknown Map Section";
                this.ExportDefinitions = false;
                this.ExportInstances = false;
                this.ExportData = false;
            }
        }

        /// <summary>
        /// Gets called whenever the vector map points for this section changes.
        /// </summary>
        public void OnVectorMapPointsChanged(List<RSG.Base.Math.Vector2f> newPoints)
        {
            this.Area = 0.0f;

            if (newPoints != null)
            {
                // Find center
                RSG.Base.Math.Vector2f center = new Base.Math.Vector2f();
                foreach (RSG.Base.Math.Vector2f point in newPoints)
                {
                    center.X += point.X;
                    center.Y += point.Y;
                }
                center = new Base.Math.Vector2f(center.X / newPoints.Count, center.Y / newPoints.Count);

                List<RSG.Base.Math.Vector2f> areaPoints = new List<Base.Math.Vector2f>(newPoints.Count);
                foreach (RSG.Base.Math.Vector2f point in newPoints)
                {
                    areaPoints.Add(new Base.Math.Vector2f(point.X - center.X, point.Y - center.Y));
                }

                for (int i = 0; i < areaPoints.Count; i++)
                {
                    RSG.Base.Math.Vector2f currentPoint = areaPoints[i];
                    RSG.Base.Math.Vector2f nextPoint = (i == areaPoints.Count - 1) ? areaPoints[0] : areaPoints[i + 1];

                    float diffence = ((currentPoint.X * nextPoint.Y) - (nextPoint.X * currentPoint.Y));
                    this.Area += diffence;
                }
                this.Area *= 0.5f;
                if (this.Area < 0)
                    this.Area = -this.Area;
            }
        }
        #endregion // Property Changed Callbacks

        #region IEnumerable<IMapComponent>
        /// <summary>
        /// 
        /// </summary>
        IEnumerator<IMapComponent> IEnumerable<IMapComponent>.GetEnumerator()
        {
            foreach (IMapComponent component in this.AssetChildren.Where(c => c is IMapComponent == true))
            {
                yield return component;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.AssetChildren.GetEnumerator();
        }
        #endregion // IEnumerable<IMapContainer>

        #region Public Functions
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetOptimizedXmlPath()
        {
            if (!string.IsNullOrWhiteSpace(this.SceneXmlFilename))
            {
                string filename = Path.GetFileNameWithoutExtension(this.SceneXmlFilename) + "_workbench.xml";
                return Path.Combine(Path.GetDirectoryName(this.SceneXmlFilename), filename);
            }

            return null;
        }

        /// <summary>
        /// Uses the zip map content to find the export paths for this map section
        /// </summary>
        public void CreateExportPaths()
        {
            if (this.SectionNode != null)
            {
                string path = this.ExportPath = SectionNode.Path;

                foreach (ContentNode node in this.SectionNode.Children)
                {
                    if (!(node is ContentNodeFile))
                        continue;

                    ContentNodeFile fileNode = (node as ContentNodeFile);
                    string extenstion = fileNode.Extension;
                    
                    if (string.Compare(extenstion, IDE_EXTENSION, true) == 0)
                    {
                        this.IdeFilename = System.IO.Path.Combine(path, this.Name + "." + IDE_EXTENSION);
                    }
                    else if (string.Compare(extenstion, IPL_EXTENSION, true) == 0)
                    {
                        this.IplFilename = System.IO.Path.Combine(path, this.Name + "." + IPL_EXTENSION);
                    }
                    else if (string.Compare(extenstion, ZIP_EXTENSION, true) == 0)
                    {
                        this.ZipFilename = System.IO.Path.Combine(path, this.Name + "." + ZIP_EXTENSION);
                    }
                    else if (string.Compare(extenstion, XML_EXTENSION, true) == 0)
                    {
                        this.SceneXmlFilename = System.IO.Path.Combine(path, this.Name + "." + XML_EXTENSION);
                        this.OptimisedSceneFilename = System.IO.Path.Combine(path, this.Name + "_workbench." + XML_EXTENSION);
                        FileInfo optimisedInfo = new FileInfo(this.OptimisedSceneFilename);
                        FileInfo nonOptimisedInfo = new FileInfo(this.SceneXmlFilename);
                        if (optimisedInfo.Exists && nonOptimisedInfo.Exists)
                        {
                            if (optimisedInfo.LastWriteTime > nonOptimisedInfo.LastWriteTime)
                            {
                                CanUseOptimisedFile = true;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> UniqueTextureDictionaryNames()
        {
            List<string> textureDictionaries = new List<string>();
            foreach (var asset in this.AssetChildren)
            {
                if (asset is MapDefinition)
                {
                    string txdName = (asset as MapDefinition).TextureDictionaryName;
                    if (!textureDictionaries.Contains(txdName))
                    {
                        yield return txdName;
                        textureDictionaries.Add(txdName);
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> UniqueDefinitionNames()
        {
            List<string> definitions = new List<string>();
            foreach (var asset in this.AssetChildren)
            {
                if (asset is MapDefinition)
                {
                    if (!definitions.Contains(asset.Name))
                    {
                        yield return asset.Name;
                        definitions.Add(asset.Name);
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public override List<IAsset> Find(System.Text.RegularExpressions.Regex expression)
        {
            var results = new List<IAsset>();

            if (!string.IsNullOrEmpty(this.Name))
            {
                if (expression.IsMatch(this.Name))
                    results.Add(this);
            }
            if (this.AssetChildren != null)
            {
                IEnumerable<MapDefinition> definitions = null;
                if (this.ExportInstances == true)
                {
                    foreach (MapInstance instance in AssetChildren.OfType<MapInstance>())
                    {
                        results.AddRange(instance.Find(expression));
                    }
                    definitions = from i in AssetChildren.OfType<MapInstance>()
                                  where i.ReferencedDefinition != null
                                  select i.ReferencedDefinition;
                }
                else
                {
                    foreach (MapDefinition definiton in AssetChildren.OfType<MapDefinition>())
                    {
                        results.AddRange(definiton.Find(expression));
                    }
                    definitions = from d in AssetChildren.OfType<MapDefinition>()
                                  select d;
                }
                if (definitions != null)
                {
                    foreach (string dictionary in UniqueDefinitionNames())
                    {
                        if (expression.IsMatch(dictionary))
                        {
                            var textures = from d in definitions
                                           where d.TextureDictionaryName == dictionary
                                           from t in d.AssetChildren.OfType<ITexture>()
                                           select t;

                            results.Add(new TextureDictionary(dictionary, string.Empty, textures, this.Name));
                        }
                    }
                }
            }

            return results;
        }

        /// <summary>
        /// Serialises this map section in its current
        /// state into the given file as a optimised xml version
        /// of itself.
        /// </summary>
        /// <param name="filename">
        /// The file path to the output file.
        /// </param>
        public void SerialiseSection(string filename)
        {
            XmlDocument doc = new XmlDocument();
            XmlDeclaration xmlDec = doc.CreateXmlDeclaration("1.0", "utf-8", string.Empty);
            doc.AppendChild(xmlDec);

            XmlElement root = doc.CreateElement("container");
            root.SetAttribute("definition", this.ExportDefinitions.ToString());
            root.SetAttribute("instances", this.ExportInstances.ToString());
            root.SetAttribute("data", this.ExportData.ToString());
            root.SetAttribute("name", this.Name);

            XmlElement attributes = doc.CreateElement("container");
            if (this.ContainerAttributes != null)
            {
                this.ContainerAttributes.Serialise(doc, attributes);
            }
            root.AppendChild(attributes);

            var definitions = from d in this.AssetChildren.OfType<MapDefinition>()
                              select d;
            var instances = from i in this.AssetChildren.OfType<MapInstance>()
                            select i;

            List<IShader> shaders = new List<IShader>();
            Dictionary<string, uint> txds = new Dictionary<string,uint>();
            foreach (var definition in definitions)
            {
                if (!txds.ContainsKey(definition.TextureDictionaryName))
                    txds.Add(definition.TextureDictionaryName, definition.ExportTxdSize);
                foreach (var shader in definition.Shaders)
                {
                    if (!shaders.Contains(shader))
                        shaders.Add(shader);
                }
            }

            XmlElement xmlTxds = doc.CreateElement("txds");
            root.AppendChild(xmlTxds);
            XmlElement xmlShaders = doc.CreateElement("shaders");
            root.AppendChild(xmlShaders);
            XmlElement xmlCollision = doc.CreateElement("collision");
            root.AppendChild(xmlCollision);
            XmlElement xmlMover = doc.CreateElement("mover");
            root.AppendChild(xmlMover);
            XmlElement xmlWeapons = doc.CreateElement("weapons");
            root.AppendChild(xmlWeapons);
            XmlElement xmlCamera = doc.CreateElement("camera");
            root.AppendChild(xmlCamera);
            XmlElement xmlRiver = doc.CreateElement("river");
            root.AppendChild(xmlRiver);
            XmlElement xmlDefinitions = doc.CreateElement("definitions");
            root.AppendChild(xmlDefinitions);
            XmlElement xmlDefinitioninstances = doc.CreateElement("definition_instances");
            root.AppendChild(xmlDefinitioninstances);
            XmlElement xmlInstances = doc.CreateElement("instances");
            root.AppendChild(xmlInstances);
            XmlElement xmlLodHierarchy = doc.CreateElement("lod");
            root.AppendChild(xmlLodHierarchy);

            xmlDec.AppendChild(root);
            doc.Save(filename);
        }
        #endregion // Public Functions
    } // MapSection
} // RSG.Model.Map