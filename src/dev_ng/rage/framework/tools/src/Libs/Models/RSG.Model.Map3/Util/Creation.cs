﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.SceneXml;
using RSG.Base.ConfigParser;
using RSG.Model.Common.Map;
using RSG.Model.Common;
using RSG.Base.Logging;
using System.Diagnostics;

namespace RSG.Model.Map3.Util
{
    /// <summary>
    /// Common functions that are used for generating the map data
    /// </summary>
    public static class Creation
    {
        #region Extension Methods
        /// <summary>
        /// MapSection extension method for processing a single entity
        /// </summary>
        /// <param name="section"></param>
        /// <param name="entityDef"></param>
        public static void ProcessEntity(this MapSection section, TargetObjectDef entityDef)
        {
            foreach (IEntity entity in ProcessEntity(entityDef, section))
            {
            }
        }

        /// <summary>
        /// Room extension method for processing a single entity
        /// </summary>
        /// <param name="room"></param>
        /// <param name="entityDef"></param>
        public static void ProcessEntity(this Room room, TargetObjectDef entityDef)
        {
            foreach (IEntity entity in ProcessEntity(entityDef, room))
            {
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="?"></param>
        /// <param name="entityDef"></param>
        public static void ProcessEntity(this InteriorArchetype interior, TargetObjectDef entityDef)
        {
            foreach (IEntity entity in ProcessEntity(entityDef, interior))
            {
            }
        }
        #endregion // Extension Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="entityDef"></param>
        /// <param name="scene"></param>
        /// <param name="parentSection"></param>
        /// <returns></returns>
        private static IEnumerable<IEntity> ProcessEntity(TargetObjectDef entityDef, IHasEntityChildren parent)
        {
            Dictionary<TargetObjectDef, IEntity> entityPairs = new Dictionary<TargetObjectDef, IEntity>();
            List<TargetObjectDef> processedObjects = new List<TargetObjectDef>();

            foreach (TargetObjectDef hierarchyObject in GetLodHierarchy(entityDef))
            {
                if (hierarchyObject.DontExportIPL() == false && !processedObjects.Contains(hierarchyObject))
                {
                    processedObjects.Add(hierarchyObject);

                    IEntity entity = null;
                    if (hierarchyObject.IsXRef() || hierarchyObject.IsRefObject())
                    {
                        entity = ProcessExternalEntity(hierarchyObject, parent);
                    }
                    else
                    {
                        entity = ProcessInternalEntity(hierarchyObject, parent);
                    }

                    if (entity != null)
                    {
                        (entity as Entity).LoadStatsFromExportData(hierarchyObject);
                        entityPairs.Add(hierarchyObject, entity);
                    }
                }
            }

            // If we have more than 1 item, attempt to patch up the lod hierarchy
            if (entityPairs.Count > 1)
            {
                foreach (KeyValuePair<TargetObjectDef, IEntity> entityPair in entityPairs.Where(item => item.Key.LOD != null))
                {
                    if (entityPair.Key.LOD.Parent != null)
                    {
                        if (entityPairs.ContainsKey(entityPair.Key.LOD.Parent))
                        {
                            if (entityPair.Value != null)
                            {
                                entityPair.Value.LodParent = entityPairs[entityPair.Key.LOD.Parent];
                            }
                        }
                        else if (entityPair.Key.LOD.Parent.IsContainerLODRefObject())
                        {
                            entityPair.Value.HasSLOD2Link = true;
                        }
                    }
                    if (entityPair.Key.LOD.Children != null)
                    {
                        foreach (TargetObjectDef child in entityPair.Key.LOD.Children)
                        {
                            if (entityPairs.ContainsKey(child))
                            {
                                if (entityPair.Value != null)
                                {
                                    entityPair.Value.LodChildren.Add(entityPairs[child]);
                                }
                            }
                        }
                    }
                }
            }
            
            // Finally load all the stats and return the individual entities
            foreach (KeyValuePair<TargetObjectDef, IEntity> entityPair in entityPairs)
            {
                yield return entityPair.Value;
            }
        }

        /// <summary>
        /// Gets all the objects that belong below the given objects lod hierarchy
        /// </summary>
        /// <param name="root"></param>
        /// <returns></returns>
        private static IEnumerable<TargetObjectDef> GetLodHierarchy(TargetObjectDef root)
        {
            yield return root;

            if (root.LOD != null)
            {
                foreach (TargetObjectDef child in root.LOD.Children)
                {
                    foreach (TargetObjectDef grandChild in GetLodHierarchy(child))
                    {
                        yield return grandChild;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sceneObject"></param>
        /// <param name="parentSection"></param>
        /// <param name="level"></param>
        /// <returns></returns>
        private static IEntity ProcessExternalEntity(TargetObjectDef entityDef, IHasEntityChildren parent)
        {
            IEntity entity = null;
            string refName = entityDef.RefName;

            if (!String.IsNullOrEmpty(refName))
            {
                refName = refName.ToLower();
                if (refName.EndsWith("_milo_"))
                {
                    refName = System.Text.RegularExpressions.Regex.Replace(refName, "_milo_$", "");
                }
                /*
                if (refName.EndsWith("_anim"))
                {
                    refName = System.Text.RegularExpressions.Regex.Replace(refName, "_anim$", "");
                }
                */

                IMapHierarchy hierarchy = GetMapHierarchyFromEntityParent(parent);
                lock (hierarchy.ArchetypeLookup)
                {
                    IMapArchetype archetype = null;
                    if (!hierarchy.ArchetypeLookup.TryGetValue(refName.ToLower(), out archetype))
                    {
                        String message = String.Format("Unable to find archetype {0} in file {1} for the object {2}", refName, entityDef.RefFile, entityDef.Name);
                        if (parent != null)
                        {
                            message += String.Format(" to be used in the container {0}", ((IAsset)parent).Name);
                        }
                        Log.Log__Warning(message);
                    }
                    entity = new Entity(entityDef.Name, archetype, parent);
                }
                
            }
            else
            {
                // Not resolved...
                string message = String.Format("Ref Object is null for object {0}", entityDef.Name);
                if (parent != null)
                {
                    message += String.Format(" in container {0}", ((IAsset)parent).Name);
                }
                Log.Log__Warning(message);
            }

            return entity;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sceneObject"></param>
        /// <param name="parentSection"></param>
        /// <param name="level"></param>
        /// <returns></returns>
        private static IEntity ProcessInternalEntity(TargetObjectDef entityDef, IHasEntityChildren parent)
        {
            IEntity entity = null;

            String assetName = entityDef.Name;
            if (entityDef.IsInternalRef() || entityDef.IsRefInternalObject())
            {
                assetName = entityDef.RefName;
            }

            IMapHierarchy hierarchy = GetMapHierarchyFromEntityParent(parent);
            lock (hierarchy.ArchetypeLookup)
            {
                IMapArchetype archetype = null;
                if (!hierarchy.ArchetypeLookup.TryGetValue(assetName.ToLower(), out archetype))
                {
                    Log.Log__Warning("Unable to find definition for instance {0} even though it should be in the same file {1}", assetName, entityDef.MyScene.Filename);
                }
                entity = new Entity(entityDef.Name, archetype, parent);
            }

            return entity;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parent"></param>
        /// <returns></returns>
        private static IMapHierarchy GetMapHierarchyFromEntityParent(IHasEntityChildren parent)
        {
            if (parent is IMapSection)
            {
                return ((IMapSection)parent).MapHierarchy;
            }
            else if (parent is IInteriorArchetype)
            {
                return ((IInteriorArchetype)parent).ParentSection.MapHierarchy;
            }
            else if (parent is IRoom)
            {
                return ((IRoom)parent).ParentArchetype.ParentSection.MapHierarchy;
            }
            else
            {
                Debug.Assert(false, "Unknown parent type.");
                throw new ArgumentException("Unknown parent type.");
            }
        }
        #endregion // Private Methods
    } // Creation
}
