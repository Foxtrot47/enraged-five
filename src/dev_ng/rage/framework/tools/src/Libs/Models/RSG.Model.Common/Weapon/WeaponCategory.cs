﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using RSG.Base.Attributes;

namespace RSG.Model.Common.Weapon
{
    /// <summary>
    /// Weapon type enumeration.
    /// </summary>
    public enum WeaponCategory
    {
        Other,

        [RuntimeName("GROUP_UNARMED")]
        Unarmed,

        [RuntimeName("GROUP_MELEE")]
        Melee,

        [RuntimeName("GROUP_PISTOL")]
        Pistol,

        [RuntimeName("GROUP_SMG")]
        SMG,

        [RuntimeName("GROUP_RIFLE")]
        Rifle,

        [RuntimeName("GROUP_MG")]
        MG,

        [RuntimeName("GROUP_SHOTGUN")]
        Shotgun,

        [RuntimeName("GROUP_SNIPER")]
        Sniper,

        [RuntimeName("GROUP_HEAVY")]
        Heavy,

        [RuntimeName("GROUP_THROWN")]
        Thrown
    } // WeaponCategory


    /// <summary>
    /// Weapon category enumeration utility and extension methods.
    /// </summary>
    public static class WeaponCategoryUtils
    {
        /// <summary>
        /// Return runtime name string for a WeaponCategory enum value.
        /// </summary>
        /// <param name="metric"></param>
        /// <returns></returns>
        public static string GetRuntimeName(this WeaponCategory category)
        {
            Type type = category.GetType();

            FieldInfo fi = type.GetField(category.ToString());
            RuntimeNameAttribute[] attributes =
                fi.GetCustomAttributes(typeof(RuntimeNameAttribute), false) as RuntimeNameAttribute[];

            return (attributes.Length > 0 ? attributes[0].Name : null);
        }

        /// <summary>
        /// Return WeaponCategory value from String representation.
        /// </summary>
        /// <param name="category">Category string.</param>
        /// <returns></returns>
        public static WeaponCategory ParseFromRuntimeString(String category)
        {
            WeaponCategory result;
            if (TryParseFromRuntimeString(category, out result))
            {
                return result;
            }
            else
            {
                throw new ArgumentException(String.Format("{0} is an unrecognised category,", category));
            }
        }

        /// <summary>
        /// Tries to parse a runtime name into a WeaponCategory value, returning whether the operation was successful.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="category"></param>
        /// <returns></returns>
        public static bool TryParseFromRuntimeString(String name, out WeaponCategory category)
        {
            foreach (WeaponCategory m in Enum.GetValues(typeof(WeaponCategory)))
            {
                if (0 == String.Compare(name, m.GetRuntimeName(), true))
                {
                    category = m;
                    return true;
                }
            }

            category = WeaponCategory.Other;
            return false;
        }
    } // WeaponCategoryUtils
}
