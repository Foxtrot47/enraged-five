﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Collections;

namespace RSG.Model.Map
{

    /// <summary>
    /// Map component interface.
    /// </summary>
    public interface IMapComponent
    {
        #region Events
        /// <summary>
        /// Event raised when attached user-data object is changed.
        /// </summary>
        event EventHandler UserDataChanged;
        #endregion // Events

        #region Properties
        /// <summary>
        /// Map container name.
        /// </summary>
        String Name { get; }
        
        /// <summary>
        /// Reference to user-defined data (application-specific data).
        /// </summary>
        Object UserData { get; set; }

        /// <summary>
        /// The model user data for this map section
        /// </summary>
        UserData ModelUserData { get; set; }
        
        #endregion // Properties
    }
    
} // RSG.Model.Map namespace
