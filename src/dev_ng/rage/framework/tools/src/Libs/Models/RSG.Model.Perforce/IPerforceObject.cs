﻿using System;
using RSG.SourceControl.Perforce;

namespace RSG.Model.Perforce
{

    /// <summary>
    /// 
    /// </summary>
    public interface IPerforceObject
    {
        P4 Connection { get; }

        void Refresh();
    }

} // RSG.Model.Perforce
