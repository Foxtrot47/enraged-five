﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Xml;
using System.Xml.XPath;
using RSG.Base.ConfigParser;
using RSG.Base.Math;
using RSG.Model.Common;
using RSG.Model.Map.Filters;
using RSG.SceneXml;
using RSG.SceneXml.Material;
using RSG.SceneXml.Statistics;
using System.Diagnostics;
using RSG.Model.Common.Map;
using RSG.Base.Logging;

namespace RSG.Model.Map.Util
{
    using AnimGroups = Dictionary<String, List<ObjectDef>>;
    using ObjList = List<ObjectDef>;
    using VectorList = List<Vector2f>;
    using VectorMap = Dictionary<String, List<RSG.Base.Math.Vector2f>>;

    /// <summary>
    /// 
    /// </summary>
    public static class Creation
    {
        #region Constants

        private static readonly XPathExpression XPATH_VECTOR_SECTION = XPathExpression.Compile("/vector_map/section");
        public static String AssetsDir = String.Empty;

        #endregion // Constants

        #region Creation
        /// <summary>
        /// Add the map model hierarchy onto the given levels as asset children
        /// </summary>
        public static void AddMapComponentsToLevels(List<ILevel> levels, ConfigGameView gv, FilterMapAreaDelegate areaFilter = null, FilterMapSectionDelegate mapFilter = null)
        {
            Creation.AssetsDir = gv.AssetsDir;
            foreach (ILevel level in levels)
            {
                AddMapComponentsToLevel(level, areaFilter, mapFilter, gv);

                List<MapSection> propGroups = new List<MapSection>();
                foreach (MapSection section in Util.Enumerations.GetAllMapSections(level, null, true))
                {
                    if (section.ExportInstances == true)
                    {
                        if (!section.Name.EndsWith("_props"))
                        {
                            Creation.FindNeighboursForSection(section);
                            if (section.PropGroup != null)
                            {
                                if (section.PropGroup.Neighbours == null)
                                {
                                    section.PropGroup.Neighbours = section.PropGroup.Neighbours;
                                }
                            }
                        }
                        else
                        {
                            propGroups.Add(section);
                        }
                    }
                }
                foreach (MapSection section in propGroups)
                {
                    Creation.FindNeighboursForSection(section);
                }
            }
        }

        /// <summary>
        /// Uses the already created area and deserialises the level node in it
        /// to add all the relative map components to the level as asset children
        /// </summary>
        private static void AddMapComponentsToLevel(ILevel level, FilterMapAreaDelegate areaFilter, FilterMapSectionDelegate mapFilter, ConfigGameView gv)
        {
            foreach (IAsset asset in InternalCreation.CreateMapComponentsForLevel(level, areaFilter, mapFilter, gv))
            {
            }
            level.Initialised = true;
        }

        /// <summary>
        /// Uses the already created section and it's export paths to create the scene xml file and
        /// deserialise it adding all the relative map components to the section as asset children.
        /// Returns whether or not the scene was able to be created
        /// </summary>
        public static Boolean AddMapComponentsToSection(MapSection section)
        {
            // Make sure we have created the sceneXml filename
            if (String.IsNullOrEmpty(section.SceneXmlFilename))
                section.CreateExportPaths();

            Scene scene = new Scene(section.SceneXmlFilename, section.MapNode, LoadOptions.All, true);
            if (scene != null)
            {
                section.AssetChildren.BeginUpdate();
                section.AssetChildren.Clear();
                foreach (IMapComponent component in InternalCreation.CreateMapComponentsForSection(scene, section))
                {
                    if (System.Windows.Application.Current != null)
                    {
                        System.Windows.Application.Current.Dispatcher.Invoke(
                        new Action
                        (
                            delegate()
                            {
                                section.AssetChildren.Add(component);
                            }
                        ),
                        System.Windows.Threading.DispatcherPriority.Send);
                    }
                    else
                    {
                        section.AssetChildren.Add(component);
                    }
                }
                if (System.Windows.Application.Current != null)
                {
                    System.Windows.Application.Current.Dispatcher.Invoke(
                    new Action
                    (
                        delegate()
                        {
                            InternalCreation.CreateMapStatisticsForSection(scene, section);
                        }
                    ),
                    System.Windows.Threading.DispatcherPriority.Send);
                }
                else
                {
                    InternalCreation.CreateMapStatisticsForSection(scene, section);
                }

                section.AssetChildren.EndUpdate();
                section.AssetChildrenCreated = true;
                return true;
            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        public static void ProcessMapInstance(ObjectDef sceneObject, IMapContainer container, Scene scene)
        {
            Dictionary<ObjectDef, MapInstance> instancePairs = new Dictionary<ObjectDef, MapInstance>();
            List<ObjectDef> processedObjects = new List<ObjectDef>();
            foreach (ObjectDef hierarchyObject in GetLodHierarchy(sceneObject))
            {
                if (hierarchyObject.DontExportIPL() == false && !processedObjects.Contains(hierarchyObject))
                {
                    processedObjects.Add(hierarchyObject);
                    MapInstance instance = null;
                    if (hierarchyObject.IsXRef() || hierarchyObject.IsRefObject())
                    {
                        instance = ProcessExternalInstance(hierarchyObject, container);
                        instancePairs.Add(hierarchyObject, instance);
                    }
                    else // An internal ref or a definition/instance
                    {
                        int assetCode = hierarchyObject.Name.ToLower().GetHashCode();
                        String assetName = hierarchyObject.Name;
                        if (hierarchyObject.IsInternalRef() || hierarchyObject.IsRefInternalObject())
                        {
                            assetCode = hierarchyObject.RefName.ToLower().GetHashCode();
                            assetName = hierarchyObject.RefName;
                        }
                        if (container.Level.GenericDefinitions.ContainsKey(assetCode))
                        {
                            MapDefinition definition = container.Level.GenericDefinitions[assetCode] as MapDefinition;
                            if (definition != null)
                            {
                                instance = new MapInstance(hierarchyObject, container, definition);
                                instancePairs.Add(hierarchyObject, instance);
                            }
                        }
                        else
                        {
                            //RSG.Base.Logging.Log.Log__Warning("Unable to find definition for instance {0} even though it should be in the same file {1}", assetName, scene.Filename);
                        }
                    }
                    if (instance != null)
                    {
                        container.AssetChildren.Add(instance);
                    }
                    else
                    {
                    }
                }
            }
            if (instancePairs.Count > 1)
            {
                foreach (KeyValuePair<ObjectDef, MapInstance> instancePair in instancePairs)
                {
                    if (instancePair.Key.LOD != null)
                    {
                        if (instancePair.Key.LOD.Parent != null)
                        {
                            if (instancePairs.ContainsKey(instancePair.Key.LOD.Parent))
                            {
                                if (instancePair.Value != null)
                                    instancePair.Value.LodParent = instancePairs[instancePair.Key.LOD.Parent];
                            }
                        }
                        if (instancePair.Key.LOD.Children != null)
                        {
                            foreach (ObjectDef child in instancePair.Key.LOD.Children)
                            {
                                if (instancePairs.ContainsKey(child))
                                {
                                    if (instancePair.Value != null)
                                        instancePair.Value.LodChildren.Add(instancePairs[child]);
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets all the objects that belong below the given objects lod hierarchy
        /// </summary>
        public static IEnumerable<ObjectDef> GetLodHierarchy(ObjectDef root)
        {
            yield return root;
            if (root.LOD != null)
            {
                foreach (ObjectDef child in root.LOD.Children)
                {
                    foreach (ObjectDef grandChild in GetLodHierarchy(child))
                    {
                        yield return grandChild;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        internal static MapInstance ProcessExternalInstance(ObjectDef sceneObject, IMapContainer container)
        {
            MapInstance instance = null;
            if (!String.IsNullOrEmpty(sceneObject.RefName))
            {
                int definitionCode = sceneObject.RefName.ToLower().GetHashCode();
                if (container.Level.GenericDefinitions.ContainsKey(definitionCode))
                {
                    MapDefinition definition = container.Level.GenericDefinitions[definitionCode] as MapDefinition;
                    instance = new MapInstance(sceneObject, container, definition);
                }

                if (instance == null)
                {
                    String message = String.Format("Unable to find definition {0} in file {1} ", sceneObject.RefName, sceneObject.RefFile);
                    message += String.Format("to be used in the container {0} for the object {1}", container.Name, sceneObject.Name);
                    RSG.Base.Logging.Log.Log__Warning(message);
                }
            }
            else
            {
                // Not resolved...
                String message = String.Format("Ref Object is null for object {0} in container {1}", sceneObject.Name, container.Name);
                RSG.Base.Logging.Log.Log__Warning(message);
            }

            return instance;
        }

        /// <summary>
        /// Uses the already created section and it's export paths to create the scene xml file and
        /// deserialise it adding all the car gens to its approiate list.
        /// </summary>
        public static Boolean AddCarGensToSection(MapSection section)
        {
            // Make sure we have created the sceneXml filename
            if (String.IsNullOrEmpty(section.SceneXmlFilename))
                section.CreateExportPaths();

            Scene scene = new Scene(section.SceneXmlFilename, section.MapNode, LoadOptions.All, true);
            if (scene != null)
            {
                section.CarGens = new List<CarGen>();
                foreach (CarGen cargen in InternalCreation.CreateCarGensForSection(scene, section))
                {
                    if (System.Windows.Application.Current != null)
                    {
                        System.Windows.Application.Current.Dispatcher.Invoke(
                        new Action
                        (
                            delegate()
                            {
                                section.CarGens.Add(cargen);
                            }
                        ),
                        System.Windows.Threading.DispatcherPriority.Send);
                    }
                    else
                    {
                        section.CarGens.Add(cargen);
                    }
                }

                return true;
            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        public static bool AddSpawnPointsToLevel(ILevel level, ConfigGameView gv)
        {
            try
            {
                ContentNodeLevel levelNode = (ContentNodeLevel)gv.Content.Root.FindAll(level.Name, "level").FirstOrDefault();
                if (levelNode != null)
                {
                    string metafileDirectory = Path.Combine(levelNode.AbsolutePath, "scenario");
                    foreach (string metafile in Directory.GetFiles(metafileDirectory))
                    {
                        System.Xml.Linq.XDocument xmlDoc = System.Xml.Linq.XDocument.Load(metafile);
                        foreach (System.Xml.Linq.XElement elem in xmlDoc.XPathSelectElements("//ScenarioPoints/Item"))
                        {
                            level.SpawnPoints.Add(new SpawnPoint(elem, metafile));
                        }
                    }

                    return true;
                }
            }
            catch (System.Exception ex)
            {
                Log.Log__Exception(ex, "Exception occurred while attempting to load spawn points from scenario metafiles.");
            }

            return false;
        }
        #endregion // Creation

        #region Vector Map

        /// <summary>
        /// Parses the vectors for a specific section using the given xpath navigator.
        /// </summary>
        internal static void GetMapSectionPoints(XPathNavigator navigator, ref VectorList points)
        {
            XPathNodeIterator pointIter = navigator.Select(XPathExpression.Compile("spline/point"));
            while (pointIter.MoveNext())
            {
                XPathNavigator nav = pointIter.Current;
                String xCoord = nav.GetAttribute("x", "");
                String yCoord = nav.GetAttribute("y", "");
                RSG.Base.Math.Vector2f v = new RSG.Base.Math.Vector2f(float.Parse(xCoord), float.Parse(yCoord));
                points.Add(v);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="vectorMap"></param>
        /// <returns></returns>
        internal static VectorList GetVectorMapPointsForSection(MapSection section, VectorMap vectorMap)
        {
            String sectionName = section.Name.ToLower();
            if (section.Name.EndsWith("_props"))
            {
                sectionName = sectionName.Replace("_props", "");
            }
            if (vectorMap.ContainsKey(sectionName))
                return vectorMap[sectionName];

            return null;
        }

        #endregion // Vector Map

        #region Neighbour Map
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        internal static Dictionary<MapSection, List<List<MapSection>>> GetNeighbourMap(ILevel level, List<MapSection> sections)
        {
            var neighbourMap = new Dictionary<MapSection, List<List<MapSection>>>();
            var boundingBoxes = new Dictionary<MapSection, BoundingBox2f>();
            foreach (var section in sections)
            {
                neighbourMap.Add(section, new List<List<MapSection>>());
                neighbourMap[section].Add(new List<MapSection>());
                neighbourMap[section].Add(new List<MapSection>());
                neighbourMap[section].Add(new List<MapSection>());
                boundingBoxes.Add(section, null);
                if (section.VectorMapPoints == null)
                    continue;
                boundingBoxes[section] = new BoundingBox2f();
                foreach (Vector2f point in section.VectorMapPoints)
                {
                    boundingBoxes[section].Expand(point);
                }
            }

            for (int i = 0; i < sections.Count - 1; i++)
            {
                if (sections[i].VectorMapPoints == null)
                    continue;

                BoundingBox2f sectionBBox = boundingBoxes[sections[i]];
                float radiusSquared = (sectionBBox.Height * sectionBBox.Height + sectionBBox.Width * sectionBBox.Width) * 0.3025f;
                Vector2f center = sectionBBox.Centre();
                Vector2f min = sectionBBox.Min;
                Vector2f max = sectionBBox.Max;
                if (sectionBBox.Width > sectionBBox.Height)
                {
                    min.X -= 20.0f;
                    max.X += 20.0f;
                    min.Y -= (sectionBBox.Width - sectionBBox.Height) * 0.3f;
                    max.Y += (sectionBBox.Width - sectionBBox.Height) * 0.3f;
                }
                else if (sectionBBox.Height > sectionBBox.Width)
                {
                    min.Y -= 20.0f;
                    max.Y += 20.0f;
                    min.X -= (sectionBBox.Height - sectionBBox.Width) * 0.3f;
                    max.X += (sectionBBox.Height - sectionBBox.Width) * 0.3f;
                }
                else
                {
                    min.X -= 20.0f;
                    min.Y -= 20.0f;
                    max.X += 20.0f;
                    max.Y += 20.0f;
                }

                for (int j = i + 1; j < sections.Count; j++)
                {
                    if (sections[j].VectorMapPoints == null)
                        continue;

                    if (Intersect(sections[j].VectorMapPoints, center, radiusSquared))
                    {
                        neighbourMap[sections[i]][0].Add(sections[j]);
                        neighbourMap[sections[j]][0].Add(sections[i]);

                        if (Intersect(sections[j].VectorMapPoints, min, max))
                        {
                            neighbourMap[sections[i]][1].Add(sections[j]);
                            neighbourMap[sections[j]][1].Add(sections[i]);
                        }

                        List<KeyValuePair<Vector2f, Vector2f>> segments = new List<KeyValuePair<Vector2f, Vector2f>>();
                        for (int p = 0; p < sections[i].VectorMapPoints.Count; p++)
                        {
                            Vector2f nextPoint = p == sections[i].VectorMapPoints.Count - 1 ? sections[i].VectorMapPoints[0] : sections[i].VectorMapPoints[p + 1];
                            segments.Add(new KeyValuePair<Vector2f, Vector2f>(sections[i].VectorMapPoints[p], nextPoint));
                        }
                        foreach (KeyValuePair<Vector2f, Vector2f> segment in segments)
                        {
                            float distance = RSG.Model.Map.Util.Creation.DistanceSquared(sections[j].VectorMapPoints, segment);
                            if (distance < 100.0f * 100.0f)
                            {
                                neighbourMap[sections[i]][2].Add(sections[j]);
                                neighbourMap[sections[j]][2].Add(sections[i]);
                                break;
                            }
                        }
                    }
                }
            }

            return neighbourMap;
        }

        /// <summary>
        /// /
        /// </summary>
        /// <param name="section"></param>
        public static void FindNeighboursForSection(MapSection section)
        {
            if (section.Neighbours == null)
            {
                section.Neighbours = new List<List<MapSection>>(3);
                section.Neighbours.Add(new List<MapSection>());
                section.Neighbours.Add(new List<MapSection>());
                section.Neighbours.Add(new List<MapSection>());

                if (section.VectorMapPoints != null && section.VectorMapPoints.Count > 0)
                {
                    BoundingBox2f bbox = new BoundingBox2f();
                    foreach (Vector2f point in section.VectorMapPoints)
                    {
                        bbox.Expand(point);
                    }
                    float radius = (float)System.Math.Sqrt(bbox.Height * bbox.Height + bbox.Width * bbox.Width) * 0.55f;
                    Vector2f center = new Vector2f(bbox.Centre().X, bbox.Centre().Y);
                    if (bbox.Width > bbox.Height)
                    {
                        bbox.Min.X -= 20.0f;
                        bbox.Max.X += 20.0f;
                        bbox.Min.Y -= (bbox.Width - bbox.Height) * 0.3f;
                        bbox.Max.Y += (bbox.Width - bbox.Height) * 0.3f;
                    }
                    else if (bbox.Height > bbox.Width)
                    {
                        bbox.Min.Y -= 20.0f;
                        bbox.Max.Y += 20.0f;
                        bbox.Min.X -= (bbox.Height - bbox.Width) * 0.3f;
                        bbox.Max.X += (bbox.Height - bbox.Width) * 0.3f;
                    }
                    else
                    {
                        bbox.Min.X -= 20.0f;
                        bbox.Min.Y -= 20.0f;
                        bbox.Max.X += 20.0f;
                        bbox.Max.Y += 20.0f;
                    }
                    List<KeyValuePair<Vector2f, Vector2f>> segments = new List<KeyValuePair<Vector2f, Vector2f>>();
                    for (int i = 0; i < section.VectorMapPoints.Count; i++)
                    {
                        Vector2f nextPoint = i == section.VectorMapPoints.Count - 1 ? section.VectorMapPoints[0] : section.VectorMapPoints[i + 1];
                        segments.Add(new KeyValuePair<Vector2f, Vector2f>(section.VectorMapPoints[i], nextPoint));
                    }

                    foreach (MapSection sec in RSG.Model.Map.Util.Enumerations.GetAllMapSections(section.Level, null, true))
                    {
                        if (sec != section && sec != section.PropGroup && sec.VectorMapPoints != null && sec.VectorMapPoints.Count > 0)
                        {
                            if (Intersect(sec.VectorMapPoints, center, radius))
                            {
                                section.Neighbours[(int)NeighbourAlgorithm.RADIUS].Add(sec);
                                foreach (KeyValuePair<Vector2f, Vector2f> segment in segments)
                                {
                                    float distance = RSG.Model.Map.Util.Creation.DistanceSquared(sec.VectorMapPoints, segment);
                                    if (distance < 100.0f * 100.0f)
                                    {
                                        section.Neighbours[(int)NeighbourAlgorithm.DISTANCE].Add(sec);
                                        break;
                                    }
                                }
                            }
                            if (Intersect(sec.VectorMapPoints, bbox.Min, bbox.Max))
                            {
                                section.Neighbours[(int)NeighbourAlgorithm.BOX].Add(sec);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        public static List<MapSection> FindNeighboursForSectionUsingCircle(MapSection section)
        {
            List<MapSection> sections = new List<MapSection>();
            
            if (section.VectorMapPoints != null && section.VectorMapPoints.Count > 0)
            {
                BoundingBox2f bbox = new BoundingBox2f();
                foreach (Vector2f point in section.VectorMapPoints)
                {
                    bbox.Expand(point);
                }
                float radiusSquared = (bbox.Height * bbox.Height + bbox.Width * bbox.Width) * 0.3025f;
                Vector2f center = new Vector2f(bbox.Centre().X, bbox.Centre().Y);

                foreach (MapSection sec in RSG.Model.Map.Util.Enumerations.GetAllMapSections(section.Level, null, true))
                {
                    if (sec != section && sec.VectorMapPoints != null && sec.VectorMapPoints.Count > 0)
                    {
                        if (Intersect(sec.VectorMapPoints, center, radiusSquared))
                            sections.Add(sec);
                    }
                }
            }
            return sections;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        public static List<MapSection> FindNeighboursForSectionUsingBox(MapSection section)
        {
            List<MapSection> sections = new List<MapSection>();
            if (section.VectorMapPoints != null && section.VectorMapPoints.Count > 0)
            {
                BoundingBox2f bbox = new BoundingBox2f();
                foreach (Vector2f point in section.VectorMapPoints)
                {
                    bbox.Expand(point);
                }
                if (bbox.Width > bbox.Height)
                {
                    bbox.Min.X -= 20.0f;
                    bbox.Max.X += 20.0f;
                    bbox.Min.Y -= (bbox.Width - bbox.Height) * 0.3f;
                    bbox.Max.Y += (bbox.Width - bbox.Height) * 0.3f;
                }
                else if (bbox.Height > bbox.Width)
                {
                    bbox.Min.Y -= 20.0f;
                    bbox.Max.Y += 20.0f;
                    bbox.Min.X -= (bbox.Height - bbox.Width) * 0.3f;
                    bbox.Max.X += (bbox.Height - bbox.Width) * 0.3f;
                }
                else
                {
                    bbox.Min.X -= 20.0f;
                    bbox.Min.Y -= 20.0f;
                    bbox.Max.X += 20.0f;
                    bbox.Max.Y += 20.0f;
                }

                foreach (MapSection sec in RSG.Model.Map.Util.Enumerations.GetAllMapSections(section.Level, null, true))
                {
                    if (sec != section && sec.VectorMapPoints != null && sec.VectorMapPoints.Count > 0)
                    {
                        if (Intersect(sec.VectorMapPoints, bbox.Min, bbox.Max))
                            sections.Add(sec);
                    }
                }
            }
            return sections;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        public static List<MapSection> FindNeighboursForSectionUsingDistance(MapSection section)
        {
            List<MapSection> sections = new List<MapSection>();
            if (section.VectorMapPoints != null && section.VectorMapPoints.Count > 0)
            {
                BoundingBox2f bbox = new BoundingBox2f();
                foreach (Vector2f point in section.VectorMapPoints)
                {
                    bbox.Expand(point);
                }
                float radius = (bbox.Height * bbox.Height + bbox.Width * bbox.Width) * 0.3025f;
                Vector2f center = new Vector2f(bbox.Centre().X, bbox.Centre().Y);

                List<KeyValuePair<Vector2f, Vector2f>> segments = new List<KeyValuePair<Vector2f, Vector2f>>();
                for (int i = 0; i < section.VectorMapPoints.Count; i++)
                {
                    Vector2f nextPoint = i == section.VectorMapPoints.Count - 1 ? section.VectorMapPoints[0] : section.VectorMapPoints[i + 1];
                    segments.Add(new KeyValuePair<Vector2f, Vector2f>(section.VectorMapPoints[i], nextPoint));
                }

                foreach (MapSection sec in RSG.Model.Map.Util.Enumerations.GetAllMapSections(section.Level, null, true))
                {
                    if (sec != section && sec.VectorMapPoints != null && sec.VectorMapPoints.Count > 0)
                    {
                        if (Intersect(sec.VectorMapPoints, center, radius))
                        {
                            foreach (KeyValuePair<Vector2f, Vector2f> segment in segments)
                            {
                                float distance = RSG.Model.Map.Util.Creation.DistanceSquared(sec.VectorMapPoints, segment);
                                if (distance < 100.0f * 100.0f)
                                {
                                    sections.Add(sec);
                                }
                            }
                        }
                    }
                }
            }
            return sections;
        }

        /// <summary>
        /// Gets the squared distance between two points
        /// </summary>
        internal static float DistanceSquared(Vector2f p1, Vector2f p2)
        {
            return ((p1.X - p2.X) * (p1.X - p2.X)) + ((p1.Y - p2.Y) * (p1.Y - p2.Y));
        }

        /// <summary>
        /// Gets the squared distance between a line segment and a point
        /// </summary>
        internal static float DistanceSquared(Vector2f p, KeyValuePair<Vector2f, Vector2f> l)
        {
            float l2 = DistanceSquared(l.Key, l.Value);
            if (l2 == 0.0)
                return DistanceSquared(p, l.Key);

            // Consider the line extending the segment, parameterized as v + t (w - v).
            // We find projection of point p onto the line. 
            // It falls where t = [(p-l.key) . (l.value-l.key)] / |w-v|^2
            Vector2f keyDifferent = new Vector2f(p.X - l.Key.X, p.Y - l.Key.Y);
            Vector2f segmentDifferent = new Vector2f(l.Value.X - l.Key.X, l.Value.Y - l.Key.Y);

            float t = ((keyDifferent.X * segmentDifferent.X) + (keyDifferent.Y * segmentDifferent.Y)) / l2;

            if (t < 0.0f)
            {
                return DistanceSquared(p, l.Key);
            }
            else
            {
                if (t > 1.0)
                    return DistanceSquared(p, l.Value);
            }


            Vector2f projection = new Vector2f(l.Key.X + (t * segmentDifferent.X), l.Key.Y + (t * segmentDifferent.Y));
            return DistanceSquared(p, projection);
        }

        /// <summary>
        /// Gets the minimum squared distance between a line segment and a set of points
        /// </summary>
        internal static float DistanceSquared(List<Vector2f> points, KeyValuePair<Vector2f, Vector2f> segment)
        {
            List<KeyValuePair<Vector2f, Vector2f>> segments = new List<KeyValuePair<Vector2f, Vector2f>>();
            for (int i = 0; i < points.Count; i++)
            {
                Vector2f nextPoint = i == points.Count - 1 ? points[0] : points[i + 1];
                segments.Add(new KeyValuePair<Vector2f, Vector2f>(points[i], nextPoint));
            }

            float minimumDistance = float.MaxValue;
            foreach (var pointSegment in segments)
            {
                minimumDistance = Math.Min(DistanceSquared(pointSegment, segment), minimumDistance);
            }
            return minimumDistance;
        }

        /// <summary>
        /// Gets the minimum squared distance between two line segments
        /// </summary>
        internal static float DistanceSquared(KeyValuePair<Vector2f, Vector2f> seg1, KeyValuePair<Vector2f, Vector2f> seg2)
        {
            Vector2f u = new Vector2f(seg1.Value.X - seg1.Key.X, seg1.Value.Y - seg1.Key.Y);
            Vector2f v = new Vector2f(seg2.Value.X - seg2.Key.X, seg2.Value.Y - seg2.Key.Y);
            Vector2f w = new Vector2f(seg1.Key.X - seg2.Key.X, seg1.Key.Y - seg2.Key.Y);

            float a = ((u.X * u.X) + (u.Y * u.Y));
            float b = ((u.X * v.X) + (u.Y * v.Y));
            float c = ((v.X * v.X) + (v.Y * v.Y));
            float d = ((u.X * w.X) + (u.Y * w.Y));
            float e = ((v.X * w.X) + (v.Y * w.Y));
            float D = a * c - b * b;
            float sc, sN, sD = D;
            float tc, tN, tD = D;

            // compute the line parameters of the two closest points
            if (D < 0.0f)
            { 
                // the lines are parallel
                sN = 0.0f;
                sD = 1.0f;
                tN = e;
                tD = c;
            }
            else
            {                
                // get the closest points on the infinite lines
                sN = (b * e - c * d);
                tN = (a * e - b * d);
                if (sN < 0.0)
                {       
                    // sc < 0 => the s=0 edge is visible
                    sN = 0.0f;
                    tN = e;
                    tD = c;
                }
                else if (sN > sD)
                {  
                    // sc > 1 => the s=1 edge is visible
                    sN = sD;
                    tN = e + b;
                    tD = c;
                }
            }

            if (tN < 0.0)
            {           
                // tc < 0 => the t=0 edge is visible
                tN = 0.0f;

                // recompute sc for this edge
                if (-d < 0.0f)
                    sN = 0.0f;
                else if (-d > a)
                    sN = sD;
                else
                {
                    sN = -d;
                    sD = a;
                }
            }
            else if (tN > tD)
            {      
                // tc > 1 => the t=1 edge is visible
                tN = tD;

                // recompute sc for this edge
                if ((-d + b) < 0.0)
                    sN = 0;
                else if ((-d + b) > a)
                    sN = sD;
                else
                {
                    sN = (-d + b);
                    sD = a;
                }
            }
            
            sc = (Math.Abs(sN) < 0.0000001 ? 0.0f : sN / sD);
            tc = (Math.Abs(tN) < 0.0000001 ? 0.0f : tN / tD);

            Vector2f tcv = new Vector2f(tc * v.X, tc * v.Y);
            Vector2f scu = new Vector2f(sc * u.X, sc * u.Y);
            Vector2f dP = new Vector2f(w.X + scu.X - tcv.X, w.Y + scu.Y - tcv.Y);

            return ((dP.X * dP.X) + (dP.Y * dP.Y));
        }

        /// <summary>
        /// Returns true if the polygon made of the given points intersects with the circle given
        /// by its center and radius
        /// </summary>
        internal static Boolean Intersect(List<Vector2f> points, Vector2f center, float radiusSquared)
        {
            // Get the segments for the polygon
            List<KeyValuePair<Vector2f, Vector2f>> segments = new List<KeyValuePair<Vector2f, Vector2f>>();
            for (int i = 0; i < points.Count; i++)
            {
                Vector2f nextPoint = i == points.Count - 1 ? points[0] : points[i + 1];
                segments.Add(new KeyValuePair<Vector2f, Vector2f>(points[i], nextPoint));
            }

            // Check the segment distance from the circles center and compare to the radius
            foreach (KeyValuePair<Vector2f, Vector2f> segment in segments)
            {
                if (DistanceSquared(center, segment) <= radiusSquared)
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Returns true if the polygon made of the given points intersects with the aabb given
        /// by its min and max
        /// </summary>
        internal static Boolean Intersect(List<Vector2f> points, Vector2f min, Vector2f max)
        {
            // Get the segments for the polygon
            List<KeyValuePair<Vector2f, Vector2f>> segments = new List<KeyValuePair<Vector2f, Vector2f>>();
            for (int i = 0; i < points.Count; i++)
            {
                Vector2f nextPoint = i == points.Count - 1 ? points[0] : points[i + 1];
                segments.Add(new KeyValuePair<Vector2f, Vector2f>(points[i], nextPoint));
            }

            foreach (KeyValuePair<Vector2f, Vector2f> segment in segments)
            {
                if (Intersect(segment, min, max))
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Returns true if the segment made of the given two points intersects with the aabb given
        /// by its min and max
        /// </summary>
        internal static Boolean Intersect(KeyValuePair<Vector2f, Vector2f> s, Vector2f min, Vector2f max)
        {
            // Find min and max X for the segment
            double minX = s.Key.X;
            double maxX = s.Value.X;
            if (s.Key.X > s.Value.X)
            {
                minX = s.Value.X;
                maxX = s.Key.X;
            }
            // Find the intersection of the segment's and rectangle's x-projections
            if (maxX > max.X)
            {
                maxX = max.X;
            }
            if (minX < min.X)
            {
                minX = min.X;
            }
            if (minX > maxX) // If their projections do not intersect return false
            {
                return false;
            }

            // Find corresponding min and max Y for min and max X we found before
            double minY = s.Key.Y;
            double maxY = s.Value.Y;
            double dx = s.Value.X - s.Key.X;

            if(System.Math.Abs(dx) > 0.0000001)
            {
                double a = (s.Value.Y - s.Key.Y) / dx;
                double b = s.Key.Y - a * s.Key.X;
                minY = a * minX + b;
                maxY = a * maxX + b;
            }
            if(minY > maxY)
            {
              double tmp = maxY;
              maxY = minY;
              minY = tmp;
            }

            // Find the intersection of the segment's and rectangle's y-projections
            if (maxY > max.Y)
            {
                maxY = max.Y;
            }
            if (minY < min.Y)
            {
                minY = min.Y;
            }
            if(minY > maxY) // If Y-projections do not intersect return false
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Returns true if the segments intersects each other
        /// </summary>
        internal static Boolean Intersect(KeyValuePair<Vector2f, Vector2f> l1, KeyValuePair<Vector2f, Vector2f> l2)
        {
            float x1 = l1.Key.X;
            float y1 = l1.Key.Y;
            float x2 = l1.Value.X;
            float y2 = l1.Value.Y;
            float x3 = l2.Key.X;
            float y3 = l2.Key.Y;
            float x4 = l2.Value.X;
            float y4 = l2.Value.Y;

            float d = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);
            
            if (d == 0.0f) 
                return false;

            float xi = ((x3 - x4) * (x1 * y2 - y1 * x2) - (x1 - x2) * (x3 * y4 - y3 * x4)) / d;
            float yi = ((y3 - y4) * (x1 * y2 - y1 * x2) - (y1 - y2) * (x3 * y4 - y3 * x4)) / d;

            if (xi < Math.Min(x1,x2) || xi > Math.Max(x1,x2)) 
                return false;

            if (xi < Math.Min(x3, x4) || xi > Math.Max(x3, x4))
                return false;

            return true;
        }

        #endregion // Neighbour Map

        #region Specialised Creation
        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        /// <param name="gv"></param>
        public static void InitialiseLevelStatistics(ILevel level, ConfigGameView gv)
        {
            if (level.Initialised == false)
            {
                Creation.AddMapComponentsToLevel(level, null, null, gv);
            }

            // We have got a fully initialised level object so start to create the statistics
            foreach (MapSection section in Util.Enumerations.GetAllMapSections(level, null, true))
            {
                if (section.AssetChildrenCreated == false)
                {
                    AddMapComponentsToSection(section);
                }
            }

            return;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        /// <param name="gv"></param>
        /// <returns></returns>
        public static IEnumerable<MapSection> InitialiseLevel(ILevel level, ConfigGameView gv)
        {
            if (level.Initialised == false)
            {
                Creation.AddMapComponentsToLevel(level, null, null, gv);
            }

            List<MapSection> sections = new List<MapSection>();
            foreach (MapSection section in Enumerations.GetAllMapSections(level, null, true))
            {
                if (section.AssetChildrenCreated == false)
                {
                    if (section.ExportInstances == true)
                    {
                        sections.Add(section);
                    }
                    else
                    {
                        sections.Insert(0, section);
                    }
                }
            }

            // We have got a fully initialised level object so start to create the statistics
            foreach (MapSection section in sections)
            {
                if (section.AssetChildrenCreated == false)
                {
                    AddMapComponentsToSection(section);
                    yield return section;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        /// <param name="gv"></param>
        /// <returns></returns>
        public static IEnumerable<MapSection> InitialiseLevelCarGens(ILevel level, ConfigGameView gv)
        {
            if (level.Initialised == false)
            {
                Creation.AddMapComponentsToLevel(level, null, null, gv);
            }

            List<MapSection> sections = new List<MapSection>();
            foreach (MapSection section in Enumerations.GetAllMapSections(level, null, true))
            {
                if (section.CarGens == null)
                {
                    sections.Add(section);
                }
            }

            // We have got a fully initialised level object so start to create the statistics
            foreach (MapSection section in sections)
            {
                AddCarGensToSection(section);
                yield return section;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        /// <param name="gv"></param>
        /// <returns></returns>
        public static IEnumerable<MapSection> InitialiseLevelSpawnPoints(ILevel level, ConfigGameView gv)
        {
            foreach (MapSection section in InitialiseLevel(level, gv))
            {
                yield return section;
            }

            AddSpawnPointsToLevel(level, gv);
        }
        #endregion // Specialised Creation
    } // Creation

    /// <summary>
    /// Same as the creation class however all the functions take thread parameters and the cancel
    /// flag is checked for each iteration and if cancelled returns false straight away
    /// </summary>
    public static class ThreadedCreation
    {
        /// <summary>
        /// Add the map model hierarchy onto the given levels as asset children
        /// </summary>
        public static Boolean AddMapComponentsToLevels(BackgroundWorker bwAsync, List<ILevel> levels, ConfigGameView gv, FilterMapAreaDelegate areaFilter = null, FilterMapSectionDelegate mapFilter = null)
        {
            Stopwatch sw = Stopwatch.StartNew();
            Creation.AssetsDir = gv.AssetsDir;
            foreach (ILevel level in levels)
            {
                DateTime start = DateTime.Now;

                if (!AddMapComponentsToLevel(bwAsync, level, areaFilter, mapFilter, gv))
                    return false;

                var containers = from s in Util.Enumerations.GetAllMapSections(level, null, true).AsParallel()
                                 where s.SectionType == ContentNodeMap.MapSubType.Container
                                 select s;

                var propContainers = (from s in Util.Enumerations.GetAllMapSections(level, null, true).AsParallel()
                                      where s.SectionType == ContentNodeMap.MapSubType.ContainerProps
                                      select s).ToDictionary<MapSection, string>(s => s.Name.ToLower().Replace("_props", ""));

                var neighboutMap = Creation.GetNeighbourMap(level, containers.ToList<MapSection>());
                foreach (var container in containers)
                {
                    container.Neighbours = neighboutMap[container];
                    MapSection propContainer = null;
                    if (propContainers.TryGetValue(container.Name.ToLower(), out propContainer))
                    {
                        container.PropGroup = propContainers[container.Name.ToLower()];
                        container.PropGroup.Neighbours = container.Neighbours;
                    }
                    if (bwAsync.CancellationPending)
                        return false;
                }
                if (bwAsync.CancellationPending)
                    return false;

                if (level.ParentLevel.Name == "gta5")
                    RSG.Base.Logging.Log.Log__Debug("Old loading took (gta5): {0}", (DateTime.Now - start).TotalMilliseconds);
            }

            sw.Stop();
            return true;
        }

        /// <summary>
        /// Uses the already created area and deserialises the level node in it
        /// to add all the relative map components to the level as asset children
        /// </summary>
        private static Boolean AddMapComponentsToLevel(BackgroundWorker bwAsync, ILevel level, FilterMapAreaDelegate areaFilter, FilterMapSectionDelegate mapFilter, ConfigGameView gv)
        {
            foreach (IAsset asset in InternalCreation.CreateMapComponentsForLevel(level, areaFilter, mapFilter, gv))
            {
                if (bwAsync.CancellationPending)
                    return false;
            }
            level.Initialised = true;
            return true;
        }
    } // ThreadedCreation

    /// <summary>
    /// Contains the main functions for creating the map data either on a background thread or
    /// the main UI thread.
    /// </summary>
    internal static class InternalCreation
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        /// <param name="areaFilter"></param>
        /// <param name="mapFilter"></param>
        /// <param name="gv"></param>
        /// <returns></returns>
        internal static IEnumerable<IMapComponent> CreateMapComponentsForLevel(ILevel level, FilterMapAreaDelegate areaFilter, FilterMapSectionDelegate mapFilter, ConfigGameView gv)
        {
            // Get the content node for this level
            ContentNodeLevel levelNode = (ContentNodeLevel)gv.Content.Root.FindAll(level.ParentLevel.Name, "level").FirstOrDefault();
            if (levelNode != null)
            {
                // Clear the previous asset children
                level.AssetChildren.Clear();
                List<KeyValuePair<ContentNodeMap, ContentNodeMapZip>> interiors = new List<KeyValuePair<ContentNodeMap, ContentNodeMapZip>>();
                Dictionary<KeyValuePair<ContentNodeMap, ContentNodeMapZip>, MapArea> areaInteriors = new Dictionary<KeyValuePair<ContentNodeMap, ContentNodeMapZip>, MapArea>();
                List<KeyValuePair<ContentNodeMap, ContentNodeMapZip>> otherSections = new List<KeyValuePair<ContentNodeMap, ContentNodeMapZip>>();
                Dictionary<KeyValuePair<ContentNodeMap, ContentNodeMapZip>, MapArea> areaOthers = new Dictionary<KeyValuePair<ContentNodeMap, ContentNodeMapZip>, MapArea>();
                foreach (ContentNode node in levelNode.Children)
                {
                    // Map Section
                    if (node is ContentNodeMapZip)
                    {
                        ContentNodeMapZip mapzip = (node as ContentNodeMapZip);
                        if (node.Inputs.Length == 0)
                            continue;

                        ContentNodeMap mapSection = (node.Inputs[0] as ContentNodeMap);

                        Boolean loadMap = true;
                        if (mapFilter != null)
                            loadMap = mapFilter(mapSection, null);

                        if (loadMap)
                        {
                            if (mapSection.MapType == ContentNodeMap.MapSubType.Props)
                            {
                                MapSection newSection = new MapSection(level, mapSection, mapzip, null);
                                level.AssetChildren.Add(newSection);
                                Creation.AddMapComponentsToSection(newSection);
                                yield return newSection;
                            }
                            else if (mapSection.MapType == ContentNodeMap.MapSubType.Interior)
                            {
                                interiors.Add(new KeyValuePair<ContentNodeMap, ContentNodeMapZip>(mapSection, mapzip));
                            }
                            else
                            {
                                otherSections.Add(new KeyValuePair<ContentNodeMap, ContentNodeMapZip>(mapSection, mapzip));
                            }
                        }
                    }
                    // Map Area
                    else if (node is ContentNodeGroup)
                    {
                        ContentNodeGroup areaNode = (node as ContentNodeGroup);
                        if (String.IsNullOrEmpty(areaNode.RawRelativePath))
                            continue;

                        Boolean loadArea = true;
                        if (areaFilter != null)
                            loadArea = areaFilter(areaNode, null);

                        if (loadArea)
                        {
                            MapArea newArea = new MapArea(level, areaNode, null);
                            level.AssetChildren.Add(newArea);
                            yield return newArea;
                            foreach (KeyValuePair<KeyValuePair<ContentNodeMap, ContentNodeMapZip>, MapArea> component in CreateMapComponentsForArea(level, newArea, areaFilter, mapFilter))
                            {
                                if (component.Key.Key.MapType == ContentNodeMap.MapSubType.Props)
                                {
                                    MapSection newSection = new MapSection(level, component.Key.Key, component.Key.Value, component.Value);
                                    component.Value.AssetChildren.Add(newSection);
                                    Creation.AddMapComponentsToSection(newSection);
                                    yield return newSection;
                                }
                                else if (component.Key.Key.MapType == ContentNodeMap.MapSubType.Interior)
                                {
                                    areaInteriors.Add(new KeyValuePair<ContentNodeMap, ContentNodeMapZip>(component.Key.Key, component.Key.Value), component.Value);
                                }
                                else
                                {
                                    areaOthers.Add(new KeyValuePair<ContentNodeMap, ContentNodeMapZip>(component.Key.Key, component.Key.Value), component.Value);
                                }
                            }
                        }
                    }
                }

                foreach (var section in interiors)
                {
                    MapSection newSection = new MapSection(level, section.Key, section.Value, null);
                    level.AssetChildren.Add(newSection);
                    Creation.AddMapComponentsToSection(newSection);
                    yield return newSection;
                }
                foreach (var section in areaInteriors)
                {
                    MapSection newSection = new MapSection(level, section.Key.Key, section.Key.Value, section.Value);
                    section.Value.AssetChildren.Add(newSection);
                    Creation.AddMapComponentsToSection(newSection);
                    yield return newSection;
                }

                foreach (var section in otherSections)
                {
                    MapSection newSection = new MapSection(level, section.Key, section.Value, null);
                    level.AssetChildren.Add(newSection);
                    yield return newSection;
                }
                foreach (var section in areaOthers)
                {
                    MapSection newSection = new MapSection(level, section.Key.Key, section.Key.Value, section.Value);
                    section.Value.AssetChildren.Add(newSection);
                    yield return newSection;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        /// <param name="areaFilter"></param>
        /// <param name="mapFilter"></param>
        /// <param name="gv"></param>
        /// <returns></returns>
        internal static IEnumerable<KeyValuePair<KeyValuePair<ContentNodeMap, ContentNodeMapZip>, MapArea>> CreateMapComponentsForArea(ILevel level, MapArea area, FilterMapAreaDelegate areaFilter, FilterMapSectionDelegate mapFilter)
        {
            ContentNodeGroup areaNode = area.AreaNode;
            if (areaNode != null)
            {
                // Clear the previous asset children
                area.AssetChildren.Clear();
                foreach (ContentNode node in areaNode.Children)
                {
                    if (node is ContentNodeMapZip)
                    {
                        ContentNodeMapZip mapzip = (node as ContentNodeMapZip);
                        if (node.Inputs.Length == 0)
                            continue;

                        ContentNodeMap mapSection = (node.Inputs[0] as ContentNodeMap);

                        Boolean loadMap = true;
                        if (mapFilter != null)
                            loadMap = mapFilter(mapSection, null);

                        if (loadMap)
                        {
                            yield return new KeyValuePair<KeyValuePair<ContentNodeMap, ContentNodeMapZip>, MapArea>(new KeyValuePair<ContentNodeMap, ContentNodeMapZip>(mapSection, mapzip), area);
                        }
                    }
                    else if (node is ContentNodeGroup)
                    {
                        ContentNodeGroup childAreaNode = (node as ContentNodeGroup);
                        if (String.IsNullOrEmpty(childAreaNode.RawRelativePath))
                            continue;

                        Boolean loadArea = true;
                        if (null != areaFilter)
                            loadArea = areaFilter(childAreaNode, null);

                        if (loadArea)
                        {
                            MapArea newArea = new MapArea(level, childAreaNode, area);
                            area.AssetChildren.Add(newArea);
                            foreach (var component in CreateMapComponentsForArea(level, newArea, areaFilter, mapFilter))
                            {
                                yield return component;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        /// <param name="areaFilter"></param>
        /// <param name="mapFilter"></param>
        /// <param name="gv"></param>
        /// <returns></returns>
        internal static IEnumerable<IMapComponent> CreateMapComponentsForSection(Scene scene, MapSection section)
        {
            if (scene == null)
                yield break;

            CategorisedObjects categorisedObjects = new CategorisedObjects();
            CategoriseScene(scene, section, ref categorisedObjects);

            foreach (MapDefinition definition in ProcessDefinitions(scene, section, categorisedObjects))
            {
                int assetCode = definition.Name.ToLower().GetHashCode();
                if (!section.Level.GenericDefinitions.ContainsKey(assetCode))
                {
                    section.Level.GenericDefinitions.Add(assetCode, definition);
                }
                if (definition.Name.EndsWith("_frag_"))
                {
                    assetCode = definition.Name.Replace("_frag_", "").ToLower().GetHashCode();
                    if (!section.Level.GenericDefinitions.ContainsKey(assetCode))
                    {
                        section.Level.GenericDefinitions.Add(assetCode, definition);
                    }
                }
                yield return definition;
            }

            foreach (MapInstance instance in ProcessInstances(scene, section, categorisedObjects.instances))
            {
                yield return instance;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scene"></param>
        /// <param name="section"></param>
        internal static IEnumerable<CarGen> CreateCarGensForSection(Scene scene, MapSection section)
        {
            if (scene != null)
            {
                foreach (ObjectDef sceneObject in scene.Objects)
                {
                    if (!IsSceneObjectValidToShow(sceneObject))
                        continue;

                    if (sceneObject.IsCarGen())
                    {
                        yield return new CarGen(sceneObject, section);
                    }
                    if (sceneObject.IsContainer())
                    {
                        foreach (ObjectDef childObject in sceneObject.Children)
                        {
                            if (!IsSceneObjectValidToShow(childObject))
                                continue;

                            if (childObject.IsCarGen())
                            {
                                yield return new CarGen(childObject, section);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scene"></param>
        /// <param name="section"></param>
        internal static void CreateMapStatisticsForSection(Scene scene, MapSection section)
        {
            section.RawSectionStatistics = new Statistics.RawSectionStatistics(section, scene);
            if (section.Neighbours == null)
            {
                Creation.FindNeighboursForSection(section);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scene"></param>
        /// <param name="section"></param>
        /// <param name="definitions"></param>
        /// <param name="instances"></param>
        /// <param name="milos"></param>
        /// <param name="fragments"></param>
        /// <param name="animationGroups"></param>
        private static void CategoriseScene(Scene scene, MapSection section, ref CategorisedObjects categorisedObjects)
        {
            foreach (ObjectDef sceneObject in scene.Objects)
            {
                if (!IsSceneObjectValidToShow(sceneObject))
                    continue;

                CategoriseSceneObject(sceneObject, section, ref categorisedObjects);
                if (sceneObject.IsContainer())
                {
                    foreach (ObjectDef childObject in sceneObject.Children)
                    {
                        if (!IsSceneObjectValidToShow(childObject))
                            continue;

                        CategoriseSceneObject(childObject, section, ref categorisedObjects);
                    }
                }
            }
        }

        /// <summary>
        /// For the given scene object, determines if it is an instance, definition, fragdef, animdef, milodef, or a combination
        /// </summary>
        private static void CategoriseSceneObject(ObjectDef sceneObject, MapSection section, ref CategorisedObjects categorisedObjects)
        {
            if (section.ExportDefinitions == true)
            {
                if (sceneObject.IsCloth()) // Cloth
                {
                    if (sceneObject.DrawableLOD != null && (sceneObject.DrawableLOD.Children != null && sceneObject.DrawableLOD.Children.Count() != 0))
                        return;

                    categorisedObjects.definitions.Add(sceneObject);
                }
                else if (sceneObject.IsFragment()) // A fragment
                {
                    if (sceneObject.DrawableLOD != null && (sceneObject.DrawableLOD.Children != null && sceneObject.DrawableLOD.Children.Count() != 0))
                        return;

                    categorisedObjects.fragments.Add(sceneObject);
                }
                else if (sceneObject.IsMilo()) // A milo
                {
                    categorisedObjects.milos.Add(sceneObject);
                }
                else if (sceneObject.IsStatedAnim()) // A animation state
                {
                    if (sceneObject.Attributes.ContainsKey(AttrNames.OBJ_GROUP_NAME))
                    {
                        String animGroup = sceneObject.Attributes[AttrNames.OBJ_GROUP_NAME] as String;
                        if (animGroup != null)
                        {
                            if (!categorisedObjects.animGroups.ContainsKey(animGroup.ToLower()))
                                categorisedObjects.animGroups.Add(animGroup.ToLower(), new List<ObjectDef>());

                            categorisedObjects.animGroups[animGroup.ToLower()].Add(sceneObject);
                        }
                    }
                }
                else if (sceneObject.IsObject() && !sceneObject.IsXRef() && !sceneObject.IsRefObject() && !sceneObject.IsInternalRef() && !sceneObject.IsRefInternalObject()) // A definition and posible instance
                {
                    if (sceneObject.DrawableLOD != null && (sceneObject.DrawableLOD.Children != null && sceneObject.DrawableLOD.Children.Count() != 0))
                        return;

                    categorisedObjects.definitions.Add(sceneObject);
                }
            }
            if (section.ExportInstances == true)
            {
                if (sceneObject.DontExportIPL() == false)
                {
                    if (sceneObject.IsXRef() || sceneObject.IsRefObject() || sceneObject.IsInternalRef() || sceneObject.IsRefInternalObject()) // A instance
                    {
                        if (sceneObject.LOD != null && (sceneObject.LOD.Parent != null && !sceneObject.LOD.Parent.IsContainerLODRefObject()))
                            return;

                        categorisedObjects.instances.Add(sceneObject);
                    }
                    else if (sceneObject.IsObject() && section.ExportDefinitions == true) // A instance and possible definition
                    {
                        if (sceneObject.LOD != null && (sceneObject.LOD.Parent != null && !sceneObject.LOD.Parent.IsContainerLODRefObject()))
                            return;

                        categorisedObjects.instances.Add(sceneObject);
                    }
                }
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="scene"></param>
        /// <param name="section"></param>
        /// <param name="definitions"></param>
        /// <param name="milos"></param>
        /// <param name="fragments"></param>
        /// <param name="animGroups"></param>
        /// <returns></returns>
        private static IEnumerable<MapDefinition> ProcessDefinitions(Scene scene, MapSection section, CategorisedObjects categorisedObjects)
        {
            foreach (ObjectDef sceneObject in categorisedObjects.milos)
            {
                yield return MapDefinition.CreateDefinitionForMilo(sceneObject, section, scene);
            }

            List<ObjectDef> polycountZero = new List<ObjectDef>();
            foreach (ObjectDef sceneObject in categorisedObjects.definitions)
            {
                if (sceneObject.PolyCount == 0)
                {
                    polycountZero.Add(sceneObject);
                }
                yield return MapDefinition.CreateDefinitionForObject(sceneObject, section, scene);
            }
            if (polycountZero.Count > 0)
            {
                String message = String.Format("The container {0} has the following objects in it that appear to be made up of zero polygons: ", section.Name);
                foreach (ObjectDef obj in polycountZero)
                {
                    message += obj.Name + " ";
                }
                message += ". Re-exporting this container will most likely fix this issue";
                RSG.Base.Logging.Log.Log__Error(message);
            }

            foreach (ObjectDef sceneObject in categorisedObjects.fragments)
            {
                yield return MapDefinition.CreateDefinitionForFragment(sceneObject, section, scene);
            }

            foreach (List<ObjectDef> animationGroup in categorisedObjects.animGroups.Values)
            {
                yield return MapDefinition.CreateDefinitionForAnimObject(animationGroup, section, scene);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private static Boolean IsSceneObjectValidToShow(ObjectDef sceneObject)
        {
            if (sceneObject.DontExport())
                return false;
            if (sceneObject.DontExportIDE())
                return false;
            if (sceneObject.IsDummy())
                return false;

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scene"></param>
        /// <param name="section"></param>
        /// <param name="instances"></param>
        /// <returns></returns>
        private static IEnumerable<MapInstance> ProcessInstances(Scene scene, MapSection section, ObjList instances)
        {
            foreach (ObjectDef sceneObject in instances)
            {
                foreach (MapInstance instance in ProcessMapInstance(sceneObject, section, scene))
                {
                    yield return instance;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sceneObject"></param>
        /// <param name="container"></param>
        /// <param name="scene"></param>
        private static IEnumerable<MapInstance> ProcessMapInstance(ObjectDef sceneObject, MapSection section, Scene scene)
        {
            Dictionary<ObjectDef, MapInstance> instancePairs = new Dictionary<ObjectDef, MapInstance>();
            List<ObjectDef> processedObjects = new List<ObjectDef>();
            foreach (ObjectDef hierarchyObject in GetLodHierarchy(sceneObject))
            {
                if (hierarchyObject.DontExportIPL() == false && !processedObjects.Contains(hierarchyObject))
                {
                    processedObjects.Add(hierarchyObject);
                    MapInstance instance = null;
                    if (hierarchyObject.IsXRef() || hierarchyObject.IsRefObject())
                    {
                        instance = ProcessExternalInstance(hierarchyObject, section);
                        instancePairs.Add(hierarchyObject, instance);
                    }
                    else // An internal ref or a definition/instance
                    {
                        int assetCode = hierarchyObject.Name.ToLower().GetHashCode();
                        String assetName = hierarchyObject.Name;
                        if (hierarchyObject.IsInternalRef() || hierarchyObject.IsRefInternalObject())
                        {
                            assetCode = hierarchyObject.RefName.ToLower().GetHashCode();
                            assetName = hierarchyObject.RefName;
                        }
                        if (section.Level.GenericDefinitions.ContainsKey(assetCode))
                        {
                            MapDefinition definition = section.Level.GenericDefinitions[assetCode] as MapDefinition;
                            if (definition != null)
                            {
                                instance = new MapInstance(hierarchyObject, section, definition);
                                instancePairs.Add(hierarchyObject, instance);
                            }
                        }
                        else
                        {
                            //RSG.Base.Logging.Log.Log__Warning("Unable to find definition for instance {0} even though it should be in the same file {1}", assetName, scene.Filename);
                        }
                    }
                    if (instance != null)
                    {

                        if (instance.ReferencedDefinition != null)
                        {
                            instance.ReferencedDefinition.Instances.Add(instance);
                        }
                        yield return instance;
                    }
                    else
                    {
                    }
                }
            }
            if (instancePairs.Count > 1)
            {
                foreach (KeyValuePair<ObjectDef, MapInstance> instancePair in instancePairs)
                {
                    if (instancePair.Key.LOD != null)
                    {
                        if (instancePair.Key.LOD.Parent != null)
                        {
                            if (instancePairs.ContainsKey(instancePair.Key.LOD.Parent))
                            {
                                if (instancePair.Value != null)
                                    instancePair.Value.LodParent = instancePairs[instancePair.Key.LOD.Parent];
                            }
                        }
                        if (instancePair.Key.LOD.Children != null)
                        {
                            foreach (ObjectDef child in instancePair.Key.LOD.Children)
                            {
                                if (instancePairs.ContainsKey(child))
                                {
                                    if (instancePair.Value != null)
                                        instancePair.Value.LodChildren.Add(instancePairs[child]);
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets all the objects that belong below the given objects lod hierarchy
        /// </summary>
        /// <param name="root"></param>
        /// <returns></returns>
        private static IEnumerable<ObjectDef> GetLodHierarchy(ObjectDef root)
        {
            yield return root;
            if (root.LOD != null)
            {
                foreach (ObjectDef child in root.LOD.Children)
                {
                    foreach (ObjectDef grandChild in GetLodHierarchy(child))
                    {
                        yield return grandChild;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sceneObject"></param>
        /// <param name="section"></param>
        /// <returns></returns>
        private static MapInstance ProcessExternalInstance(ObjectDef sceneObject, MapSection section)
        {
            MapInstance instance = null;
            if (!String.IsNullOrEmpty(sceneObject.RefName))
            {
                int definitionCode = sceneObject.RefName.ToLower().GetHashCode();
                if (section.Level.GenericDefinitions.ContainsKey(definitionCode))
                {
                    MapDefinition definition = section.Level.GenericDefinitions[definitionCode] as MapDefinition;
                    instance = new MapInstance(sceneObject, section, definition);
                }

                if (instance == null)
                {
                    String message = String.Format("Unable to find definition {0} in file {1} ", sceneObject.RefName, sceneObject.RefFile);
                    message += String.Format("to be used in the container {0} for the object {1}", section.Name, sceneObject.Name);
                    RSG.Base.Logging.Log.Log__Warning(message);
                }
            }
            else
            {
                // Not resolved...
                String message = String.Format("Ref Object is null for object {0} in container {1}", sceneObject.Name, section.Name);
                RSG.Base.Logging.Log.Log__Warning(message);
            }

            return instance;
        }
    } // InternalCreation

    /// <summary>
    /// 
    /// </summary>
    internal class CategorisedObjects
    {
        internal ObjList definitions = new ObjList();
        internal ObjList instances = new ObjList();
        internal ObjList milos = new ObjList();
        internal ObjList fragments = new ObjList();
        internal AnimGroups animGroups = new AnimGroups();

        internal CategorisedObjects()
        {

        }
    } // CategorisedObjects

    /// <summary>
    /// 
    /// </summary>
    public static class TestCreation
    {
        public static bool Test(MapSection section, ref long oldLoadingTime, ref long newLoadingTime)
        {
            if (String.IsNullOrEmpty(section.SceneXmlFilename))
                section.CreateExportPaths();
            if (!System.IO.File.Exists(section.SceneXmlFilename))
                return false;

            System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
            Scene scene = new Scene(section.SceneXmlFilename);
            sw.Stop();
            oldLoadingTime = sw.ElapsedMilliseconds;
            System.Diagnostics.Stopwatch sw1 = System.Diagnostics.Stopwatch.StartNew();
            Scene scene1 = new Scene(section.SceneXmlFilename, section.MapNode, LoadOptions.All, true);
            sw1.Stop();
            newLoadingTime = sw1.ElapsedMilliseconds;

            // Make sure both scene objects are the same.
            //if (!AreScenesEqual(scene, scene1))
            //{
//
            //}

            return true;
        }

        private static bool AreScenesEqual(Scene s1, Scene s2)
        {
            if (s1.TableLookup.Count != s2.TableLookup.Count)
                return false;
            if (s1.MaterialLookup.Count != s2.MaterialLookup.Count)
                return false;
            if (s1.GeometryStatsLookup.Count != s2.GeometryStatsLookup.Count)
                return false;
            if (s1.TxdStatsLookup.Count != s2.TxdStatsLookup.Count)
                return false;

            var s1Objects = s1.TableLookup.Values.ToArray();
            var s2Objects = s2.TableLookup.Values.ToArray();
            for (int i = 0; i < s1.TableLookup.Count; i++)
            {
                if (!AreObjectsEqual(s1Objects[i], s2Objects[i]))
                    return false;
            }

            var s1Materials = s1.MaterialLookup.Values.ToArray();
            var s2Materials = s2.MaterialLookup.Values.ToArray();
            for (int i = 0; i < s1.MaterialLookup.Count; i++)
            {
                if (!AreMaterialsEqual(s1Materials[i], s2Materials[i]))
                    return false;
            }

            var s1geoStats = s1.GeometryStatsLookup.Values.ToArray();
            var s2geoStats = s2.GeometryStatsLookup.Values.ToArray();
            for (int i = 0; i < s1.GeometryStatsLookup.Count; i++)
            {
                if (!AreGeometryStatsEqual(s1geoStats[i], s2geoStats[i]))
                    return false;
            }

            var s1txdStats = s1.TxdStatsLookup.Values.ToArray();
            var s2txdStats = s2.TxdStatsLookup.Values.ToArray();
            for (int i = 0; i < s1.TxdStatsLookup.Count; i++)
            {
                if (!AreTxdStatsEqual(s1txdStats[i], s2txdStats[i]))
                    return false;
            }

            return true;
        }

        private static bool AreObjectsEqual(ObjectDef o1, ObjectDef o2)
        {
            List<RSG.SceneXml.sKeyFrame> activeKeys1;
            List<RSG.SceneXml.sKeyFrame> activeKeys2;
            o1.KeyedProperties.TryGetValue("active", out activeKeys1);
            o2.KeyedProperties.TryGetValue("active", out activeKeys2);

            if (!AreKeyFramesEqual(activeKeys1, activeKeys2))
                return false;
            if (o1.AnimLength != o2.AnimLength)
                return false;
            if (o1.AttributeClass != o2.AttributeClass)
                return false;
            if (o1.AttributeGuid != o2.AttributeGuid)
                return false;
            if (!AreAttributesEqual(o1.Attributes, o2.Attributes))
                return false;
            if (!AreChildrenEqual(o1.Children, o2.Children))
                return false;
            if (o1.Class != o2.Class)
                return false;
            if (!AreAttributesEqual(o1.CustomAttributes, o2.CustomAttributes))
                return false;
            if (!AreLodHierarchiesEqual(o1.DrawableLOD, o2.DrawableLOD))
                return false;
            if (!AreGuidListsEqual(o1.GenericReferenceGuids, o2.GenericReferenceGuids))
                return false;
            if (!AreObjectListsEqual(o1.GenericReferences, o2.GenericReferences))
                return false;
            if (o1.Guid != o2.Guid)
                return false;
            if (!AreObjectGuidEqual(o1.InternalRef, o2.InternalRef))
                return false;
            if (o1.InternalRefGuid != o2.InternalRefGuid)
                return false;
            if (!AreBoundingBoxesEqual(o1.LocalBoundingBox, o2.LocalBoundingBox))
                return false;
            if (!AreLodHierarchiesEqual(o1.LOD, o2.LOD))
                return false;
            if (o1.LODLevel != o2.LODLevel)
                return false;
            if (o1.Material != o2.Material)
                return false;
            if (o1.Name != o2.Name)
                return false;
            if (!AreVectorsEqual(o1.NodeScale, o2.NodeScale))
                return false;
            if (!AreMatricesEqual(o1.NodeTransform, o2.NodeTransform))
                return false;
            if (!AreVectorsEqual(o1.ObjectScale, o2.ObjectScale))
                return false;
            if (!AreMatricesEqual(o1.ObjectTransform, o2.ObjectTransform))
                return false;
            if (!AreParamBlocksEqual(o1.ParamBlocks, o2.ParamBlocks))
                return false;
            if (!AreObjectGuidEqual(o1.Parent, o2.Parent))
                return false;
            if (o1.PolyCount != o2.PolyCount)
                return false;
            if (!AreAttributesEqual(o1.Properties, o2.Properties))
                return false;
            if (o1.RefFile != o2.RefFile)
                return false;
            if (o1.RefName != o2.RefName)
                return false;
            if (!AreObjectGuidEqual(o1.RefObject, o2.RefObject))
                return false;
            if (!AreObjectGuidEqual(o1.SkeletonRoot, o2.SkeletonRoot))
                return false;
            if (o1.SkeletonRootGuid != o2.SkeletonRootGuid)
                return false;
            if (!AreObjectGuidEqual(o1.SkinObj, o2.SkinObj))
                return false;
            if (!AreObjectListsEqual(o1.SubObjects, o2.SubObjects))
                return false;
            if (o1.Superclass != o2.Superclass)
                return false;
            if (!AreAttributesEqual(o1.UserProperties, o2.UserProperties))
                return false;
            if (!AreBoundingBoxesEqual(o1.WorldBoundingBox, o2.WorldBoundingBox))
                return false;

            return true;
        }

        private static bool AreKeyFramesEqual(List<RSG.SceneXml.sKeyFrame> k1, List<RSG.SceneXml.sKeyFrame> k2)
        {
            if (k1 == null)
            {
                if (k2 != null)
                    return false;
            }
            else if (k2 == null)
            {
                return false;
            }
            else
            {
                if (k1.Count != k2.Count)
                    return false;
                for (int i = 0; i < k1.Count; i++)
                {
                    if (k1[i].m_nState != k2[i].m_nState)
                        return false;
                    if (k1[i].m_nTime != k2[i].m_nTime)
                        return false;
                }
            }
            return true;
        }

        private static bool AreAttributesEqual(Dictionary<string, object> a1, Dictionary<string, object> a2)
        {
            if (a1 == null)
            {
                if (a2 != null)
                    return false;
            }
            else if (a2 == null)
            {
                return false;
            }
            else
            {
                if (a1.Count != a2.Count)
                    return false;
                var a1Keys = a1.Keys.ToArray();
                var a2Keys = a2.Keys.ToArray();
                var a1Values = a1.Keys.ToArray();
                var a2Values = a2.Keys.ToArray();
                for (int i = 0; i < a1.Count; i++)
                {
                    if (a1Keys[i] != a2Keys[i])
                        return false;
                    if (a1Values[i] != a2Values[i])
                        return false;
                }
            }
            return true;
        }

        private static bool AreChildrenEqual(ObjectDef[] c1, ObjectDef[] c2)
        {
            if (c1 == null)
            {
                if (c2 != null)
                    return false;
            }
            else if (c2 == null)
            {
                return false;
            }
            else
            {
                if (c1.Length != c2.Length)
                    return false;

                for (int i = 0; i < c1.Length; i++)
                {
                    if (c1[i].Guid != c2[i].Guid)
                        return false;
                }
            }
            return true;
        }

        private static bool AreLodHierarchiesEqual(LODHierarchyDef l1, LODHierarchyDef l2)
        {
            if (l1 == null)
            {
                if (l2 != null)
                    return false;
            }
            else if (l2 == null)
            {
                return false;
            }
            else
            {
                if (!AreObjectListsEqual(l1.Children, l2.Children))
                    return false;
                if (!AreObjectListsEqual(l1.SecondaryChildren, l2.SecondaryChildren))
                    return false;
                if (!AreObjectGuidEqual(l1.Parent, l2.Parent))
                    return false;
                if (!AreObjectGuidEqual(l1.SecondaryParent, l2.SecondaryParent))
                    return false;
            }
            return true;
        }

        private static bool AreGuidListsEqual(Guid[] s1, Guid[] s2)
        {
            if (s1 == null)
            {
                if (s2 != null)
                    return false;
            }
            else if (s2 == null)
            {
                return false;
            }
            else
            {
                if (s1.Length != s2.Length)
                    return false;
                for (int i = 0; i < s1.Length; i++)
                {
                    if (s1[i] != s2[i])
                        return false;
                }
            }
            return true;
        }

        private static bool AreObjectListsEqual(ObjectDef[] s1, ObjectDef[] s2)
        {
            if (s1 == null)
            {
                if (s2 != null)
                    return false;
            }
            else if (s2 == null)
            {
                return false;
            }
            else
            {
                if (s1.Length != s2.Length)
                    return false;
                for (int i = 0; i < s1.Length; i++)
                {
                    if (!AreObjectGuidEqual(s1[i], s2[i]))
                        return false;
                }
            }
            return true;
        }

        private static bool AreObjectGuidEqual(ObjectDef o1, ObjectDef o2)
        {
            if (o1 == null)
            {
                if (o2 != null)
                    return false;
            }
            else if (o2 == null)
            {
                return false;
            }
            else
            {
                if (o1.Guid != o2.Guid)
                    return false;
            }
            return true;
        }

        private static bool AreParamBlocksEqual(Dictionary<string, object>[] b1, Dictionary<string, object>[] b2)
        {
            if (b1 == null)
            {
                if (b2 != null)
                    return false;
            }
            else if (b2 == null)
            {
                return false;
            }
            else
            {
                if (b1.Length != b2.Length)
                    return false;
                for (int i = 0; i < b1.Length; i++)
                {
                    if (!AreAttributesEqual(b1[i], b2[i]))
                        return false;
                }              
            }
            return true;
        }

        private static bool AreMatricesEqual(Matrix34f m1, Matrix34f m2)
        {
            if (m1 == null)
            {
                if (m2 != null)
                    return false;
            }
            else if (m2 == null)
            {
                return false;
            }
            else
            {
                if (!AreVectorsEqual(m1.A, m2.A))
                    return false;
                if (!AreVectorsEqual(m1.B, m2.B))
                    return false;
                if (!AreVectorsEqual(m1.C, m2.C))
                    return false;
                if (!AreVectorsEqual(m1.D, m2.D))
                    return false;
            }
            return true;
        }

        private static bool AreBoundingBoxesEqual(BoundingBox3f b1, BoundingBox3f b2)
        {
            if (b1 == null)
            {
                if (b2 != null)
                    return false;
            }
            else if (b2 == null)
            {
                return false;
            }
            else
            {
                if (!AreVectorsEqual(b1.Max, b2.Max))
                    return false;
                if (!AreVectorsEqual(b1.Min, b2.Min))
                    return false;
            }
            return true;
        }

        private static bool AreVectorsEqual(Vector3f v1, Vector3f v2)
        {
            if (v1 == null)
            {
                if (v2 != null)
                    return false;
            }
            else if (v2 == null)
            {
                return false;
            }
            else
            {
                if (v1.X != v2.X)
                    return false;
                if (v1.Y != v2.Y)
                    return false;
                if (v1.Z != v2.Z)
                    return false;
            }
            return true;
        }



        private static bool AreMaterialsEqual(MaterialDef m1, MaterialDef m2)
        {
            if (m1.Guid != m2.Guid)
                return false;
            if (m1.HasParent != m2.HasParent)
                return false;
            if (m1.HasPresetString != m2.HasPresetString)
                return false;
            if (m1.HasSubMaterials != m2.HasSubMaterials)
                return false;
            if (m1.HasTextures != m2.HasTextures)
                return false;
            if (m1.MaterialType != m2.MaterialType)
                return false;
            if (m1.Name != m2.Name)
                return false;
            if (m1.Parent == null)
            {
                if (m2.Parent != null)
                    return false;
            }
            else 
            {
                if (m1.Parent.Guid != m2.Parent.Guid)
                    return false;
            }
            if (m1.Preset != m2.Preset)
                return false;
            if (m1.SubMaterialsCount != m2.SubMaterialsCount)
                return false;
            if (m1.TextureCount != m2.TextureCount)
                return false;

            if (m1.HasSubMaterials)
            {
                for (int i = 0; i < m1.SubMaterials.Length; i++)
                {
                    if (!AreMaterialsEqual(m1.SubMaterials[i], m2.SubMaterials[i]))
                        return false;
                }     
            }

            if (m1.HasTextures)
            {
                for (int i = 0; i < m1.Textures.Length; i++)
                {
                    if (!AreTexturesEqual(m1.Textures[i], m2.Textures[i]))
                        return false;
                }    
            }

            return true;
        }

        private static bool AreTexturesEqual(TextureDef t1, TextureDef t2)
        {
            if (t1.FilePath != t2.FilePath)
                return false;

            return true;
        }

        private static bool AreGeometryStatsEqual(DrawableStats s1, DrawableStats s2)
        {
            if (s1.Guid != s2.Guid)
                return false;
            if (s1.Name != s2.Name)
                return false;
            if (s1.PolyCount != s2.PolyCount)
                return false;
            if (s1.PolyDensity != s2.PolyDensity)
                return false;
            if (s1.ShaderCount != s2.ShaderCount)
                return false;
            if (s1.Size != s2.Size)
                return false;
            if (s1.Type != s2.Type)
                return false;

            return true;
        }

        private static bool AreTxdStatsEqual(TxdStats s1, TxdStats s2)
        {
            if (s1.Guid != s2.Guid)
                return false;
            if (s1.Name != s2.Name)
                return false;
            if (s1.Size != s2.Size)
                return false;
            if (s1.Usage != s2.Usage)
                return false;

            return true;
        }

    } // TestCreation
} // RSG.Model.Map.Util
