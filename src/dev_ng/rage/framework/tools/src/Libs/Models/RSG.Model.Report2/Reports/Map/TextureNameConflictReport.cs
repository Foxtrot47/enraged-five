﻿namespace RSG.Model.Report.Reports.Map
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using RSG.Base.Tasks;
    using RSG.Model.Common;
    using RSG.Model.Common.Map;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class TextureNameConflictReport : CSVReport, IDynamicLevelReport, IReportFileProvider
    {
        #region Constants
        private const String c_name = "Texture Conflict Report";
        private const String c_description = "Exports a texture conflict table into a .csv file";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (m_task == null)
                {
                    CompositeTask task = new CompositeTask("Generate Report");
                    task.AddSubTask(new ActionTask("Loading data", (context, progress) => EnsureDataLoaded((DynamicLevelReportContext)context, progress)));
                    task.AddSubTask(new ActionTask("Generating report", (context, progress) => GenerateReport((DynamicLevelReportContext)context, progress)));
                    m_task = task;
                }
                return m_task;
            }
        }
        private ITask m_task;

        /// <summary>
        /// List of stats that are required to generate this report.
        /// </summary>
        public IEnumerable<StreamableStat> RequiredStats
        {
            get { return null; }
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public TextureNameConflictReport()
            : base(c_name, c_description)
        {
        }
        #endregion // Constructor(s)

        #region Task Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void EnsureDataLoaded(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            IMapHierarchy hierarchy = context.Level.MapHierarchy;

            if (hierarchy != null)
            {
                float increment = 1.0f / hierarchy.AllSections.Count();

                // Load all the map section data
                foreach (IMapSection section in hierarchy.AllSections)
                {
                    context.Token.ThrowIfCancellationRequested();

                    // Update the progress information
                    string message = String.Format("Loading {0}", section.Name);
                    progress.Report(new TaskProgress(increment, true, message));

                    section.RequestAllStatistics(false);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void GenerateReport(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            using (StreamWriter writer = new StreamWriter(Filename))
            {
                string header = "Texture Name,Object,Section,Parent Area,Grandparent Area, Texture Directory";
                writer.WriteLine(header);

                IMapHierarchy hierarchy = context.Level.MapHierarchy;
                if (hierarchy != null)
                {
                    Dictionary<string, Collision> textureNames = new Dictionary<string, Collision>();
                    Dictionary<string, Dictionary<string, Collision>> collisions = new Dictionary<string, Dictionary<string, Collision>>();
                    foreach (IMapSection section in hierarchy.AllSections)
                    {
                        string parentArea = section.ParentArea != null ? section.ParentArea.Name : "n/a";
                        string grandParentArea = section.ParentArea != null ? 
                            section.ParentArea.ParentArea != null ? section.ParentArea.ParentArea.Name : "n/a"
                            : "n/a";

                        foreach (IMapArchetype archetype in section.Archetypes)
                        {
                            foreach (ITexture texture in archetype.Textures)
                            {
                                foreach (string source in texture.SourceFilenames)
                                {
                                    string name = Path.GetFileNameWithoutExtension(source).ToLower();
                                    string directory = Path.GetDirectoryName(source).ToLower();
                                    if (!textureNames.ContainsKey(name))
                                    {
                                        Collision firstInstanceCollision = new Collision();
                                        firstInstanceCollision.ArchetypeName = archetype.Name;
                                        firstInstanceCollision.AreaName = parentArea;
                                        firstInstanceCollision.Directory = directory;
                                        firstInstanceCollision.Name = name;
                                        firstInstanceCollision.ParentAreaName = grandParentArea;
                                        firstInstanceCollision.SectionName = section.Name;

                                        textureNames.Add(name, firstInstanceCollision);
                                        continue;
                                    }

                                    if (textureNames[name].Directory != directory)
                                    {
                                        if (!collisions.ContainsKey(name))
                                        {
                                            collisions.Add(name, new Dictionary<string, Collision>());
                                        }

                                        if (!collisions[name].ContainsKey(textureNames[name].Directory))
                                        {
                                            collisions[name].Add(textureNames[name].Directory, textureNames[name]);
                                        }

                                        if (!collisions[name].ContainsKey(directory))
                                        {
                                            Collision collision = new Collision();
                                            collision.ArchetypeName = archetype.Name;
                                            collision.AreaName = parentArea;
                                            collision.Directory = directory;
                                            collision.Name = name;
                                            collision.ParentAreaName = grandParentArea;
                                            collision.SectionName = section.Name;

                                            collisions[name].Add(directory, collision);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    foreach (var collisionMap in collisions.Values)
                    {
                        foreach (Collision collision in collisionMap.Values)
                        {
                            string record = String.Format("{0},", collision.Name);
                            record += String.Format("{0},{1},{2},{3},", collision.ArchetypeName, collision.SectionName, collision.AreaName, collision.ParentAreaName);
                            record += String.Format("{0}", collision.Directory);
                            writer.WriteLine(record);
                        }
                    }
                }
            }
        }

        private void GenerateReport(Dictionary<string, List<string>> collisions)
        {

        }
        #endregion // Task Methods

        #region Classes
        private class Collision
        {
            public string Name { get; set; }
            public string Directory { get; set; }
            public string ArchetypeName { get; set; }
            public string SectionName { get; set; }
            public string AreaName { get; set; }
            public string ParentAreaName { get; set; }
        }
        #endregion
    } // TextureComparisonReport
} // RSG.Model.Report.Reports.Map

