﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common.Map
{
    /// <summary>
    /// 
    /// </summary>
    public interface IMapSectionStatistic
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        uint TotalCount { get; }

        /// <summary>
        /// 
        /// </summary>
        uint UniqueCount { get; }

        /// <summary>
        /// 
        /// </summary>
        uint DrawableCost { get; }

        /// <summary>
        /// 
        /// </summary>
        uint TXDCost { get; }

        /// <summary>
        /// 
        /// </summary>
        uint TXDCount { get; }

        /// <summary>
        /// 
        /// </summary>
        uint ShaderCount { get; }

        /// <summary>
        /// 
        /// </summary>
        uint TextureCount { get; }

        /// <summary>
        /// 
        /// </summary>
        uint PolygonCount { get; }

        /// <summary>
        /// 
        /// </summary>
        uint CollisionCount { get; }

        /// <summary>
        /// 
        /// </summary>
        IDictionary<CollisionType, uint> CollisionTypePolygonCounts { get; }

        /// <summary>
        /// 
        /// </summary>
        float LodDistance { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        uint GetPolyCountForCollisionType(CollisionType type);
        #endregion // Methods
    } // IMapSectionStatistic
}
