﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using RSG.Base.Extensions;
using RSG.Model.Report.Reports.GameStatsReports.Model;
using System.IO;
using System.Windows.Controls.DataVisualization.Charting;
using System.Windows.Media;
using System.Windows.Data;
using System.Windows;
using System.Windows.Media.Imaging;

namespace RSG.Model.Report.Reports.GameStatsReports.Tests
{
    /// <summary>
    /// 
    /// </summary>
    public class MemorySmokeTest : SmokeTestBase
    {
        #region Constants
        private const string NAME = "Memory";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Test name -> list of memory results for the latest build
        /// </summary>
        private IDictionary<string, IList<MemoryResult>> LatestStats
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public MemorySmokeTest()
            : base(NAME)
        {
        }
        #endregion // Constructor(s)

        #region SmokeTestBase Methods
        /// <summary>
        /// Executes the particular test
        /// </summary>
        /// <param name="testSessions">List of test sessions</param>
        public override void RunTest(IList<TestSession> testSessions)
        {
            base.RunTest(testSessions);

            LatestStats = GatherLatestBuildStats(testSessions.FirstOrDefault());
        }

        /// <summary>
        /// Generates test specific graphs for this smoke test
        /// </summary>
        /// <param name="test"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns>Null if the smoke test doesn't require a graph</returns>
        public override IList<GraphImage> GenerateGraphs(DateTime start, DateTime end)
        {
            IList<GraphImage> allGraphs = new List<GraphImage>();

            // Generate a couple of graphs for each  test
            foreach (KeyValuePair<string, IList<MemoryResult>> pair in LatestStats)
            {
                string testName = pair.Key;

                // Create the pie graph
                Stream imageStream = GenerateColumnGraph(testName, pair.Value);
                string graphName = String.Format("memory_column_{0}", testName.Replace("(", "").Replace(")", "").Replace(" ", ""));
                GraphImage image = new GraphImage(graphName, imageStream);

                if (!PerTestGraphs.ContainsKey(testName))
                {
                    PerTestGraphs.Add(testName, new List<GraphImage>());
                }

                PerTestGraphs[testName].Add(image);
                allGraphs.Add(image);
            }

            return allGraphs;
        }

        /// <summary>
        /// Write this test's portion of the report
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="test">The name of the test for which we wish to write the report</param>
        /// <param name="includeHeader">Whether we should output a header for this report</param>
        /// <param name="tableResults"></param>
        public override void WriteReport(HtmlTextWriter writer, string test, bool includeHeader, uint tableResults, bool email)
        {
            base.WriteReport(writer, test, includeHeader, tableResults, email);
        }
        #endregion // SmokeTestBase Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        private IDictionary<string, IList<MemoryResult>> GatherLatestBuildStats(TestSession session)
        {
            IDictionary<string, IList<MemoryResult>> latestStats = new Dictionary<string, IList<MemoryResult>>();

            if (session != null)
            {
                foreach (TestResults test in session.TestResults)
                {
                    if (!latestStats.ContainsKey(test.Name))
                    {
                        latestStats.Add(test.Name, new List<MemoryResult>());
                        latestStats[test.Name].AddRange(test.MemoryResults);
                    }
                }
            }

            return latestStats;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="testName"></param>
        /// <param name="setName"></param>
        /// <param name="stats"></param>
        /// <returns></returns>
        private Stream GenerateColumnGraph(string testName, IList<MemoryResult> stats)
        {
            Chart chart = new Chart();
            chart.BeginInit();
            chart.Width = 1280;
            chart.Height = 600;
            chart.Title = String.Format("Memory Stats Breakdown for '{0}'", testName);
            chart.LegendTitle = "Individual Metrics";
            chart.EndInit();

            RenderTargetBitmap renderTarget = new RenderTargetBitmap((int)chart.Width, (int)chart.Height, 96d, 96d, PixelFormats.Default);

            ColumnSeries gvSeries = new ColumnSeries
            {
                Title = "Game Virtual",
                ItemsSource = stats,
                IndependentValueBinding = new Binding("Name"),
                DependentValueBinding = new Binding("GameVirtual.Average")
            };
            chart.Series.Add(gvSeries);

            ColumnSeries gpSeries = new ColumnSeries
            {
                Title = "Game Physical",
                ItemsSource = stats,
                IndependentValueBinding = new Binding("Name"),
                DependentValueBinding = new Binding("GamePhysical.Average")
            };
            chart.Series.Add(gpSeries);

            ColumnSeries rvSeries = new ColumnSeries
            {
                Title = "Resource Virtual",
                ItemsSource = stats,
                IndependentValueBinding = new Binding("Name"),
                DependentValueBinding = new Binding("ResourceVirtual.Average")
            };
            chart.Series.Add(rvSeries);

            ColumnSeries rpSeries = new ColumnSeries
            {
                Title = "Resource Physical",
                ItemsSource = stats,
                IndependentValueBinding = new Binding("Name"),
                DependentValueBinding = new Binding("ResourcePhysical.Average")
            };
            chart.Series.Add(rpSeries);
            
            CategoryAxis xAxis = new CategoryAxis
            {
                Orientation = AxisOrientation.X,
                Title = "Bucket Name"
            };
            chart.Axes.Add(xAxis);

            LinearAxis yAxis = new LinearAxis
            {
                Orientation = AxisOrientation.Y,
                Title = "Kilobytes",
                ShowGridLines = true
            };
            chart.Axes.Add(yAxis);
            
            chart.Measure(new Size(chart.Width, chart.Height));
            chart.Arrange(new Rect(new Size(chart.Width, chart.Height)));
            chart.UpdateLayout();

            renderTarget.Render(chart);

            // Save out the chart to a png file
            PngBitmapEncoder png = new PngBitmapEncoder();
            png.Frames.Add(BitmapFrame.Create(renderTarget));
            MemoryStream stream = new MemoryStream();
            png.Save(stream);
            stream.Position = 0;
            return stream;
        }
        #endregion // Private Methods
    } // MemorySmokeTest
}
