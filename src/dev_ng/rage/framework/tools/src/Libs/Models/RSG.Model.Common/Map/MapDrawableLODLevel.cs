﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common.Map
{
    #region Enums
    /// <summary>
    /// Drawable LOD levels below the HD object
    /// </summary>
    public enum MapDrawableLODLevel
    {
        Medium,
        Low,
        VeryLow
    }
    #endregion
}
