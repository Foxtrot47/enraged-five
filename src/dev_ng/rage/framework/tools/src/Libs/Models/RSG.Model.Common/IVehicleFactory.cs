﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.ConfigParser;

namespace RSG.Model.Common
{
    /// <summary>
    /// Publicly accessible interface for creating IVehicle and IVehicleCollection's
    /// </summary>
    public interface IVehicleFactory
    {
        #region Methods
        /// <summary>
        /// Creates a new vehicle collection from the specified source.
        /// </summary>
        /// <param name="source">Source to use for creating the collection.</param>
        /// <param name="buildIdentifier">Optional build identifier for when the source is Database.</param>
        /// <returns></returns>
        IVehicleCollection CreateCollection(DataSource source, string buildIdentifier = null);
        #endregion // Methods
    } // IVehicleFactory
} // RSG.Model.Common
