﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common.Map;
using RSG.Model.Common;
using System.ComponentModel;

namespace RSG.Model.Map3
{
    /// <summary>
    /// 
    /// </summary>
    public class MapNode : AssetBase, IMapNode
    {
        #region Properties
        /// <summary>
        /// Returns the root of the map tree hierarchy
        /// </summary>
        [Browsable(false)]
        public IMapHierarchy MapHierarchy
        {
            get;
            protected set;
        }

        /// <summary>
        /// Level this node is a part of
        /// </summary>
        public ILevel Level
        {
            get
            {
                return MapHierarchy.OwningLevel;
            }
        }

        /// <summary>
        /// Returns this map node's parent.  Only the root node can have a null parent.
        /// </summary>
        [Browsable(false)]
        public IMapArea ParentArea
        {
            get;
            protected set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public MapNode(string name, IMapHierarchy hierarchy)
            : base(name)
        {
            ParentArea = null;
            MapHierarchy = hierarchy;
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public MapNode(string name, IMapArea parent)
            : base(name)
        {
            ParentArea = parent;
            MapHierarchy = parent.MapHierarchy;
        }
        #endregion // Constructor(s)
    } // MapNode
}
