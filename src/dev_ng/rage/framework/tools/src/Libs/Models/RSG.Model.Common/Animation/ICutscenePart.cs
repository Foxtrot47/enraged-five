﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common.Animation
{
    /// <summary>
    /// A single part that makes up an entire cutscene.
    /// </summary>
    public interface ICutscenePart : IAsset
    {
        /// <summary>
        /// Filename containing the cutscene information.
        /// </summary>
        String CutFile { get; }

        /// <summary>
        /// How long this part is (in seconds).
        /// </summary>
        float Duration { get; }
    } // ICutscenePart
}
