﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common.GameText
{
    /// <summary>
    /// A section of game text.
    /// </summary>
    public interface IGameTextSection
    {
        /// <summary>
        /// Name of the section.
        /// </summary>
        String Name { get; }

        /// <summary>
        /// Retrieves the text associated with a particular text label for the specified platform.
        /// </summary>
        /// <param name="label"></param>
        /// <param name="platform"></param>
        /// <returns></returns>
        String GetText(String label, RSG.Platform.Platform platform);

        /// <summary>
        /// Add's a new piece of text to this section.
        /// </summary>
        /// <param name="label"></param>
        /// <param name="platform"></param>
        /// <param name="text"></param>
        void AddText(String label, RSG.Platform.Platform? platform, String text);
    } // IGameTextSection
}
