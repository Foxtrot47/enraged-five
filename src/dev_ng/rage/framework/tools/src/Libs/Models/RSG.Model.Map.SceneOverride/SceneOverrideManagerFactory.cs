﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Configuration;
using RSG.Pipeline.Content;

namespace RSG.Model.Map.SceneOverride
{
    /// <summary>
    /// This static factory class is the entry point for creating a LOD override manager
    /// </summary>
    public static class SceneOverrideManagerFactory
    {
        /// <summary>
        /// Create a LOD override manager that uses XML based local file data.
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="treeHelper"></param>
        /// <param name="dataDirectory">Local file directory.</param>
        /// <param name="writable">Writable support</param>
        /// <returns>Scene override manager object.</returns>
        public static ISceneOverrideManager CreateXmlSceneOverrideManager(IBranch branch, 
            ContentTreeHelper treeHelper, string dataDirectory, bool writable)
        {
            ISceneOverrideRepository lodOverrideRepository;
            if (writable)
                lodOverrideRepository = new XmlSceneOverrideRepository(dataDirectory);
            else
                lodOverrideRepository = new XmlSceneOverrideReadOnlyRepository(dataDirectory);

            return (new SceneOverrideManager(branch, treeHelper, lodOverrideRepository));
        }

        /// <summary>
        /// Creates a LOD override manager that uses centralised MySql based data.
        /// </summary>
        /// <returns>An interface to a Scene override manager</returns>
        public static ISceneOverrideManager CreateMySqlSceneOverrideManager()
        {
            throw new NotImplementedException();
        }
    }

} // RSG.Model.Map.SceneOverride namespace
