﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;

namespace RSG.Model.Report
{
    /// <summary>
    /// Common interface from which both reports and report categories inherit.
    /// </summary>
    public interface IReportItem : IAsset
    {
        /// <summary>
        /// A description of the report.
        /// </summary>
        String Description { get; }
    } // IReportItem
}
