﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Map.Utilities;

namespace RSG.Model.Map
{

    /// <summary>
    /// Map container interface; map component which has a set of child
    /// map components.
    /// </summary>
    public interface IMapContainer : 
        IMapComponent, ISearchable,
        IEnumerable<IMapComponent>
    {
        #region Properties
        /// <summary>
        /// Dictionary of MapComponent objects for this container.
        /// </summary>
        Dictionary<String, IMapComponent> MapComponents { get; }

        IEnumerable<IMapComponent> ComponentChildren { get; }

        IEnumerable<IMapContainer> ContainerChildren { get; }

        #endregion // Properties

        #region Methods
        /// <summary>
        /// Return true iff level contains a MapComponent with specified name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        bool ContainsMapComponent(String name);
        #endregion // Methods
    }

} // RSG.Model.Map namespace
