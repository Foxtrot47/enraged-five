﻿using System;
using RSG.Base.Collections;

namespace RSG.Model.Common
{

    /// <summary>
    /// Interface declaring object has an attachable user-data collection.
    /// </summary>
    public interface IHasUserData
    {
        #region Properties
        /// <summary>
        /// User-data collection.
        /// </summary>
        UserData UserData { get; set; }
        #endregion // Properties
    }

} // RSG.Model.Common namespace
