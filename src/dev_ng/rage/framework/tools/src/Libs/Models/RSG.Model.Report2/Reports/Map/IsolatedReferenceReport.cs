﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using RSG.Base.ConfigParser;
using RSG.Model.Common;
using RSG.Base.Tasks;
using RSG.Model.Common.Map;
using RSG.Base.Math;
using RSG.Base.Extensions;
using RSG.Platform;
using RSG.ManagedRage;
using System.Diagnostics;

namespace RSG.Model.Report.Reports.Map
{
    /// <summary>
    /// 
    /// </summary>
    public class IsolatedReferenceReport : CSVReport, IDynamicLevelReport
    {
        #region Constants
        private const String c_name = "Isolated Reference Report";
        private const String c_description = "Generate and display a isolated reference report.";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (m_task == null)
                {
                    CompositeTask task = new CompositeTask("Generate Report");
                    task.AddSubTask(new ActionTask("Loading data", (context, progress) => EnsureDataLoaded((DynamicLevelReportContext)context, progress)), 50);
                    task.AddSubTask(new ActionTask("Generating report", (context, progress) => GenerateReport((DynamicLevelReportContext)context, progress)));
                    m_task = task;
                }
                return m_task;
            }
        }
        private ITask m_task;

        /// <summary>
        /// List of stats that are required to generate this report.
        /// </summary>
        public IEnumerable<StreamableStat> RequiredStats
        {
            get { return null; }
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public IsolatedReferenceReport()
            : base(c_name, c_description)
        {
        }
        #endregion // Constructor(s)

        #region Task Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void EnsureDataLoaded(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            IMapHierarchy hierarchy = context.Level.MapHierarchy;

            if (hierarchy != null)
            {
                float increment = 1.0f / hierarchy.AllSections.Count();

                // Load all the map section data
                foreach (IMapSection section in hierarchy.AllSections)
                {
                    context.Token.ThrowIfCancellationRequested();

                    // Update the progress information
                    string message = String.Format("Loading {0}", section.Name);
                    progress.Report(new TaskProgress(increment, true, message));

                    section.LoadStats(new List<StreamableStat>() { StreamableMapSectionStat.Archetypes, StreamableMapSectionStat.Entities });
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void GenerateReport(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            GenerateIsolatedReferenceReport(context.Level, context.GameView);
        }
        #endregion // Task Methods
        
        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        /// <param name="gv"></param>
        public void GenerateIsolatedReferenceReport(ILevel level, ConfigGameView gv)
        {
            IMapHierarchy hierarchy = level.MapHierarchy;
            Debug.Assert(hierarchy != null, "Unable to generate report");
            if (hierarchy == null || hierarchy.AllSections == null)
            {
                return;
            }
            
            float fadeDistance = 20.0f;

            // Keep track of the entities that are isolated
            Dictionary<IMapArchetype, List<IEntity>> validEntities = new Dictionary<IMapArchetype, List<IEntity>>();

            try
            {
                List<IMapSection> mapSections = hierarchy.AllSections.Where(item => item.ExportEntities == false).ToList();
                foreach (IMapSection section in mapSections)
                {
                    if (section == null || section.NonInteriorArchetypes == null)
                    {
                        continue;
                    }


                    foreach (IMapArchetype archetype in section.NonInteriorArchetypes)
                    {
                        if (archetype == null || archetype.ExteriorEntities == null)
                        {
                            continue;
                        }

                        ISet<IEntity> notIsolated = new HashSet<IEntity>();
                        IList<IEntity> exteriorEntities = archetype.ExteriorEntities.ToList();
                        for (int i = 0; i < exteriorEntities.Count; ++i)
                        {
                            IEntity entityA = exteriorEntities[i];
                            float entityARadius = (float)entityA.LodDistance + fadeDistance;
                            Vector3f entityAPosition = entityA.Position;

                            if (!notIsolated.Contains(entityA))
                            {
                                for (int j = i + 1; j < exteriorEntities.Count; ++j)
                                {
                                    IEntity entityB = exteriorEntities[j];
                                    float entityABRadius = (float)entityB.LodDistance + fadeDistance;
                                    Vector3f entityBPosition = entityB.Position;

                                    // Sphere - Sphere intersection test
                                    float testDistance = (entityARadius * entityARadius) + (2 * entityARadius * entityABRadius) + (entityABRadius * entityABRadius);
                                    float dx = entityBPosition.X - entityAPosition.X;
                                    float dy = entityBPosition.Y - entityAPosition.Y;
                                    float dz = entityBPosition.Z - entityAPosition.Z;
                                    float squaredDistance = (dx * dx) + (dy * dy) + (dz * dz);
                                    if (squaredDistance <= testDistance)
                                    {
                                        notIsolated.Add(entityA);
                                        notIsolated.Add(entityB);
                                    }
                                }
                            }
                        }

                        // Get the list of isolated entities
                        List<IEntity> entities = null;
                        if (!validEntities.TryGetValue(archetype, out entities))
                        {
                            entities = new List<IEntity>();
                            validEntities.Add(archetype, entities);
                        }

                        entities.AddRange(archetype.ExteriorEntities.Except(notIsolated));
                    }
                }
            }
            catch
            {
                Trace.Write("fdsfadsafd");
            }

            this.WriteCsvFile(validEntities, gv, level.Name);
        }

        /// <summary>
        /// Wtites out the CSV file using the data gathered at the start of the
        /// <see cref="GenerateReport"/> method.
        /// </summary>
        /// <param name="validEntities">
        /// The list containing the valid entities to output to the csv file.
        /// </param>
        private void WriteCsvFile(Dictionary<IMapArchetype, List<IEntity>> validEntities, ConfigGameView gv, string levelName)
        {
            using (StreamWriter writer = new StreamWriter(this.Filename))
            {
                writer.WriteLine(
                    "Entity Name," +
                    "Section," +
                    "Parent Area," +
                    "Grandparent Area," +
                    "X,Y,Z," +
                    "Archetype Name," +
                    "Virtual PS3 Geomerty Size," +
                    "Virtual PS3 txd Size," +
                    "Total Virtual PS3 Size," +
                    "Physical PS3 Geomerty Size," +
                    "Physical PS3 txd Size," +
                    "Total Physical PS3 Size," +
                    "Priority" +
                    "Lod Distance," +
                    "Is Orphaned HD");

                WriteCsvData(writer, validEntities, gv, levelName);
            }
        }

        /// <summary>
        /// Writes the data out to the specified stream.
        /// </summary>
        /// <param name="writer">
        /// The stream to write the comma separated data into.
        /// </param>
        /// <param name="validEntities">
        /// The list containing the valid entities to output to the csv file.
        /// </param>
        private void WriteCsvData(StreamWriter writer, Dictionary<IMapArchetype, List<IEntity>> validEntities, ConfigGameView gv, string levelName)
        {
            Dictionary<IMapSection, List<IMapArchetype>> sections = new Dictionary<IMapSection, List<IMapArchetype>>();
            foreach (KeyValuePair<IMapArchetype, List<IEntity>> archetype in validEntities)
            {
                List<IMapArchetype> archetypes = null;
                if (!sections.TryGetValue(archetype.Key.ParentSection, out archetypes))
                {
                    archetypes = new List<IMapArchetype>();
                    sections.Add(archetype.Key.ParentSection, archetypes);
                }

                archetypes.Add(archetype.Key);
            }

            Dictionary<string, ArchetypeSizes> archetypeSizes = new Dictionary<string, ArchetypeSizes>();
            char ragebuilderCharacter = RSG.Platform.Platform.PS3.PlatformRagebuilderCharacter();
            string drawableExtension = FileType.Drawable.GetPlatformExtension().Replace('?', ragebuilderCharacter);
            string fragmentExtension = FileType.Fragment.GetPlatformExtension().Replace('?', ragebuilderCharacter);
            string textureExtension = FileType.TextureDictionary.GetPlatformExtension().Replace('?', ragebuilderCharacter);

            foreach (KeyValuePair<IMapSection, List<IMapArchetype>> section in sections)
            {
                string sectionName = section.Key.Name;
                string processedRpfPath = this.GetProcesedRpfPath(gv, sectionName);
                if (processedRpfPath != null && File.Exists(processedRpfPath))
                {
                    Packfile packFile = new Packfile();
                    packFile.Load(processedRpfPath);
                    foreach (IMapArchetype archetype in section.Value)
                    {
                        if (archetypeSizes.ContainsKey(archetype.Name))
                        {
                            continue;
                        }

                        ArchetypeSizes sizes = new ArchetypeSizes();
                        string geometryName = archetype.Name + ".";
                        if (archetype is IFragmentArchetype)
                        {
                            geometryName += fragmentExtension;
                        }
                        else
                        {
                            geometryName += drawableExtension;
                        }

                        PackEntry geometryEntry = packFile.FindEntry(geometryName, true);
                        if (geometryEntry != null)
                        {
                            sizes.PhysicalGeometry = geometryEntry.PhysicalSize;
                            sizes.VirtualGeometry = geometryEntry.VirtualSize;
                        }

                        foreach (string txd in archetype.TxdExportSizes.Keys)
                        {
                            string txdName = txd + "." + textureExtension;
                            PackEntry textureEntry = packFile.FindEntry(txdName, true);
                            if (textureEntry != null)
                            {
                                sizes.PhysicalTexture += textureEntry.PhysicalSize;
                                sizes.VirtualTexture += textureEntry.VirtualSize;
                            }
                        }

                        archetypeSizes.Add(archetype.Name, sizes);
                    }
                }
            }

            foreach (KeyValuePair<IMapArchetype, List<IEntity>> archetype in validEntities)
            {
                if (archetype.Key == null || archetype.Value == null)
                {
                    continue;
                }

                ArchetypeSizes sizes;
                archetypeSizes.TryGetValue(archetype.Key.Name, out sizes);

                foreach (IEntity entity in archetype.Value)
                {
                    IMapSection section = entity.Parent as IMapSection;
                    if (section == null)
                    {
                        IRoom room = entity.Parent as IRoom;
                        if (room != null)
                        {
                            IInteriorArchetype interiorArchetype = room.ParentArchetype;
                            if (room.ParentArchetype != null)
                            {
                                section = room.ParentArchetype.ParentSection;
                            }
                        }
                    }

                    IMapArea parentArea = (section != null ? section.ParentArea : null);
                    IMapArea grandParentArea = (parentArea != null ? parentArea.ParentArea : null);

                    string csvRecord = String.Format("{0},{1},{2},{3},", entity.Name, section != null ? section.Name : "n/a", parentArea != null ? parentArea.Name : "n/a", grandParentArea != null ? grandParentArea.Name : "n/a");
                    csvRecord += String.Format("{0},{1},{2},", entity.Position.X, entity.Position.Y, entity.Position.Z);
                    csvRecord += String.Format("{0},", archetype.Key.Name);
                    csvRecord += String.Format("{0},{1},{2},", sizes.VirtualGeometry, sizes.VirtualTexture, sizes.VirtualGeometry + sizes.VirtualTexture);
                    csvRecord += String.Format("{0},{1},{2},", sizes.PhysicalGeometry, sizes.PhysicalTexture, sizes.PhysicalGeometry + sizes.PhysicalTexture);
                    csvRecord += String.Format("{0},{1},{2}", entity.PriorityLevel, entity.LodDistance, entity.LodLevel == LodLevel.OrphanHd ? "True" : "False");


                    writer.WriteLine(csvRecord);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private string GetProcesedRpfPath(ConfigGameView gv, string name)
        {
            // Get the content node for this section
            ContentNodeMap contentNode = (ContentNodeMap)gv.Content.Root.FindAll(name, "map").FirstOrDefault();

            if (contentNode == null)
            {
                return null;
            }

            // Get the export node for the section
            ContentNodeMapZip exportNode = contentNode.Outputs.FirstOrDefault() as ContentNodeMapZip;
            if (exportNode == null)
            {
                return null;
            }

            ContentNodeMapProcessedZip processedZip = exportNode.Outputs.FirstOrDefault() as ContentNodeMapProcessedZip;
            if (processedZip == null)
            {
                return null;
            }

            string path = processedZip.Filename;
            if (path == null)
            {
                return null;
            }

            path = Path.ChangeExtension(path, ".rpf");
            path = Path.GetFullPath(path);
            return path.Replace(gv.ProcessedDir, Path.Combine(gv.BuildDir, "ps3"));
        }
        #endregion // Private Methods

        #region Structures
        private struct ArchetypeSizes
        {
            public ulong VirtualGeometry { get; set; }

            public ulong VirtualTexture { get; set; }

            public ulong PhysicalGeometry { get; set; }

            public ulong PhysicalTexture { get; set; }
        }
        #endregion Structures
    } // IsolatedReferenceReport
}
