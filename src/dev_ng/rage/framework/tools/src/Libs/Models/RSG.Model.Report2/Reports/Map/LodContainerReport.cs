﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.ConfigParser;
using RSG.Model.Common;
using System.Web.UI;
using RSG.Base.Extensions;
using RSG.Base.Tasks;
using RSG.Model.Common.Map;

namespace RSG.Model.Report.Reports.Map
{
    /// <summary>
    /// 
    /// </summary>
    internal class LodBranch
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public IEntity RootEntity
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public IList<IEntity> ChildEntities
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public LodBranch(IEntity root, IList<IEntity> children)
        {
            RootEntity = root;
            ChildEntities = children;

        }
        #endregion // Constructor(s)
    } // LodBranch

    /// <summary>
    /// 
    /// </summary>
    internal class LodHierarchyAcrossSections
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public IMapSection LodSection
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public IList<IMapSection> ContainerSections
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public IList<LodBranch> SLOD3Branches
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public IList<LodBranch>SLOD2Branches
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="slod3"></param>
        public LodHierarchyAcrossSections()
        {
            SLOD3Branches = new List<LodBranch>();
            SLOD2Branches = new List<LodBranch>();
        }
        #endregion // Constructor(s)

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="children"></param>
        public void AddBranch(IEntity root, IList<IEntity> children)
        {
            if (root.LodLevel == LodLevel.SLod3)
            {
                SLOD3Branches.Add(new LodBranch(root, children));
            }
            else if (root.LodLevel == LodLevel.SLod2)
            {
                SLOD2Branches.Add(new LodBranch(root, children));
            }
        }
        #endregion
    } // LodHierarchyAcrossSections

    /// <summary>
    /// 
    /// </summary>
    internal interface ISingleLodHierarchyTest
    {
        void TestHierarchy(LodHierarchyAcrossSections hierarchy, ref IList<ILodHierarchyFailure> failures);
    } // ISingleLodHierarchyTest

    /// <summary>
    /// 
    /// </summary>
    internal interface IMultipleLodHierarchyTest
    {
        void TestHierarchy(IList<LodHierarchyAcrossSections> hierarchies, ref IList<ILodHierarchyFailure> failures);
    } // IMultipleLodHierarchyTest

    /// <summary>
    /// 
    /// </summary>
    internal interface ILodHierarchyFailure
    {
        string ErrorMessage { get; }

        IMapSection Section { get; }

        IEntity Entity { get; }
    } // ILodHierarchyFailure

    /// <summary>
    /// 
    /// </summary>
    internal class LodHierarchyDistanceTest : ISingleLodHierarchyTest
    {
        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="hierarchy"></param>
        public void TestHierarchy(LodHierarchyAcrossSections hierarchy, ref IList<ILodHierarchyFailure> failures)
        {
            // Tests to make sure that the lod distance is greater on the SLOD2
            // object than any of the parentied SLOD1 objects.
            foreach (LodBranch branch in hierarchy.SLOD2Branches)
            {
                float slod2Distance = (float)branch.RootEntity.LodDistance;
                foreach (IEntity childEntity in branch.ChildEntities)
                {
                    if (slod2Distance < childEntity.LodDistance)
                    {
                        failures.Add(new LodHierarchyFailure(hierarchy.LodSection, branch.RootEntity, String.Format("Lod distance for {0}({1}m) is smaller than its child {2}({3}m)", branch.RootEntity.Name, slod2Distance, childEntity.Name, childEntity.LodDistance)));
                        failures.Add(new LodHierarchyFailure(childEntity.Parent as IMapSection, childEntity, String.Format("Lod distance for {0}({1}m) is greater than its parent {2}({3}m)", childEntity.Name, childEntity.LodDistance, branch.RootEntity.Name, slod2Distance)));
                    }
                }
            }
        }
        #endregion
    } // LodHierarchyDistanceTest

    /// <summary>
    /// 
    /// </summary>
    internal class LodHierarchyFailure : ILodHierarchyFailure
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public string ErrorMessage
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public IMapSection Section
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public IEntity Entity
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="entity"></param>
        /// <param name="msg"></param>
        public LodHierarchyFailure(IMapSection section, IEntity entity, string msg)
        {
            this.Section = section;
            this.Entity = entity;
            this.ErrorMessage = msg;
        }
        #endregion // Constructor(s)
    } // LodHierarchyFailure

    /// <summary>
    /// 
    /// </summary>
    public class LodContainerReport : Report, IDynamicLevelReport, IReportFileProvider
    {
        #region Constants
        private const String c_name = "Lod Container Validation Report";
        private const String c_description = "Exports a html report that notes all lod errors between the lod levels SLOD2 and SLOD1.";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (m_task == null)
                {
                    CompositeTask task = new CompositeTask("Generate Report");
                    task.AddSubTask(new ActionTask("Loading data", (context, progress) => EnsureDataLoaded((DynamicLevelReportContext)context, progress)), 50);
                    task.AddSubTask(new ActionTask("Generating report", (context, progress) => GenerateReport((DynamicLevelReportContext)context, progress)));
                    m_task = task;
                }
                return m_task;
            }
        }
        private ITask m_task;

        /// <summary>
        /// List of stats that are required to generate this report.
        /// </summary>
        public IEnumerable<StreamableStat> RequiredStats
        {
            get { return null; }
        }

        /// <summary>
        /// Filter used for the save dialog
        /// </summary>
        public string Filter
        {
            get
            {
                return "html|*.html";
            }
        }

        /// <summary>
        /// Extensions that this report support
        /// </summary>
        public string SupportedExtension
        {
            get
            {
                return ".html";
            }
        }

        /// <summary>
        /// Filename specifying where a report should be output
        /// </summary>
        public string Filename
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public LodContainerReport()
            : base(c_name, c_description)
        {
            
        }
        #endregion // Constructor(s)

        #region Task Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void EnsureDataLoaded(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            IMapHierarchy hierarchy = context.Level.MapHierarchy;

            if (hierarchy != null)
            {
                float increment = 1.0f / hierarchy.AllSections.Count();

                // Load all the map section data
                foreach (IMapSection section in hierarchy.AllSections)
                {
                    context.Token.ThrowIfCancellationRequested();

                    // Update the progress information
                    string message = String.Format("Loading {0}", section.Name);
                    progress.Report(new TaskProgress(increment, true, message));

                    section.RequestAllStatistics(false);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void GenerateReport(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            IMapHierarchy mapHierarchy = context.Level.MapHierarchy;

            if (mapHierarchy != null)
            {
                // Gather the lod hierarchies that span sections and then run the tests on them checking for any failures.
                IList<LodHierarchyAcrossSections> hierarchies = GetHierarchies(mapHierarchy);
                IList<ILodHierarchyFailure> failures = new List<ILodHierarchyFailure>();

                foreach (LodHierarchyAcrossSections hierarchy in hierarchies)
                {
                    foreach (ISingleLodHierarchyTest singleTest in GetSingleTests())
                    {
                        singleTest.TestHierarchy(hierarchy, ref failures);
                    }
                }
                foreach (IMultipleLodHierarchyTest mulitpleTest in GetMultipleTests())
                {
                    mulitpleTest.TestHierarchy(hierarchies, ref failures);
                }

                CreateReport(Filename, failures);

                /*
                using (StreamWriter sw = new StreamWriter(Filename))
                {
                    WriteCsvHeader(sw);

                    foreach (IMapSection section in hierarchy.AllSections.Where(item => item.CarGens != null))
                    {
                        foreach (ICarGen carGen in section.CarGens)
                        {
                            WriteCsvCarGen(carGen, sw);
                        }
                    }
                }
                */
            }
        }
        #endregion // Task Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private IEnumerable<ISingleLodHierarchyTest> GetSingleTests()
        {
            yield return new LodHierarchyDistanceTest();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private IEnumerable<IMultipleLodHierarchyTest> GetMultipleTests()
        {
            return Enumerable.Empty<IMultipleLodHierarchyTest>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mapHierarchy"></param>
        /// <returns></returns>
        private IList<LodHierarchyAcrossSections> GetHierarchies(IMapHierarchy mapHierarchy)
        {
            IList<LodHierarchyAcrossSections> lodHierarchies = new List<LodHierarchyAcrossSections>();
            IList<IMapSection> lodSections = new List<IMapSection>();

            // Keep track of container sections for later lookup (mapping of section name -> section).
            IDictionary<string, IMapSection> allContainerSections = new Dictionary<string, IMapSection>();

            // Iterate over all sections that export entities
            foreach (IMapSection section in mapHierarchy.AllSections.Where(item => item.ExportEntities == true))
            {
                if (section.ChildEntities.Where(item => item.LodLevel == LodLevel.SLod2).Any())
                {
                    lodSections.Add(section);
                }
                else
                {
                    allContainerSections.Add(section.Name.ToLower(), section);
                }
            }

            // Iterate over all the lod sections that we found
            foreach (IMapSection lodSection in lodSections)
            {
                LodHierarchyAcrossSections lodHierarchy = new LodHierarchyAcrossSections();
                lodHierarchy.LodSection = lodSection;

                foreach (IEntity entity in lodSection.ChildEntities.Where(item => item.LodLevel == LodLevel.SLod3))
                {
                    IList<IEntity> branchChildren = new List<IEntity>(entity.LodChildren);
                    lodHierarchy.AddBranch(entity, branchChildren);
                }

                lodHierarchy.ContainerSections = new List<IMapSection>();
                lodHierarchies.Add(lodHierarchy);
            }

            return lodHierarchies;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="failures"></param>
        private void CreateReport(string filename, IList<ILodHierarchyFailure> failures)
        {
            // Sort the errors by map section and in the order of worst first.
            IDictionary<IMapSection, IList<ILodHierarchyFailure>> sortedSections = new Dictionary<IMapSection, IList<ILodHierarchyFailure>>();
            foreach (ILodHierarchyFailure failure in failures)
            {
                if (!sortedSections.ContainsKey(failure.Section))
                {
                    sortedSections[failure.Section] = new List<ILodHierarchyFailure>();
                }
                sortedSections[failure.Section].Add(failure);
            }

            // Write out the report next
            using (StreamWriter reportFile = new StreamWriter(filename))
            {
                HtmlTextWriter writer = new HtmlTextWriter(reportFile);

                writer.RenderBeginTag(HtmlTextWriterTag.H1);
                writer.Write("Lod Distance Hierarchy Check");
                writer.RenderEndTag();

                foreach (KeyValuePair<IMapSection, IList<ILodHierarchyFailure>> pair in sortedSections)
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.H2);
                    writer.Write(pair.Key.Name);
                    writer.RenderEndTag();

                    writer.RenderBeginTag(HtmlTextWriterTag.Table);
                    writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                    writer.RenderBeginTag(HtmlTextWriterTag.Th);
                    writer.Write("Lod Distance Warnings");
                    writer.RenderEndTag();
                    writer.RenderEndTag();

                    foreach (ILodHierarchyFailure failure in pair.Value)
                    {
                        writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                        writer.RenderBeginTag(HtmlTextWriterTag.Td);
                        writer.Write(failure.ErrorMessage);
                        writer.RenderEndTag();
                        writer.RenderEndTag();
                    }

                    writer.RenderEndTag();
                }

                writer.Flush();
                writer.Dispose();
                writer.Close();
            }
        }
        #endregion // Private Methods
    } // LodContainerReport
}
