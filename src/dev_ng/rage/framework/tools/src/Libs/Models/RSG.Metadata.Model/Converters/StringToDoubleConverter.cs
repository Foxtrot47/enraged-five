﻿//---------------------------------------------------------------------------------------------
// <copyright file="StringToDoubleConverter.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Converters
{
    using System.Globalization;

    /// <summary>
    /// Represents a converter that can convert a string instance into a single or an array of
    /// signed byte values. This class cannot be inherited.
    /// </summary>
    public sealed class StringToDoubleConverter : NumericalConverter<double>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="StringToDoubleConverter"/> class.
        /// </summary>
        public StringToDoubleConverter()
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="StringToDoubleConverter"/> class.
        /// </summary>
        /// <param name="constants">
        /// The numerical constant class that this converter can use.
        /// </param>
        public StringToDoubleConverter(NumericalConstants constants)
            : base(constants)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Converts the double representation of the numerical number to its specified type
        /// equivalent.
        /// </summary>
        /// <param name="d">
        /// A double that contains the number to convert.
        /// </param>
        /// <param name="result">
        /// When this method returns, contains the numerical value equivalent to the number
        /// contained in d, if the conversion succeeded, or zero if the conversion failed.
        /// </param>
        /// <returns>
        /// True if d was converted successfully; otherwise, false.
        /// </returns>
        protected override bool FromDouble(double d, out double result)
        {
            result = d;
            return true;
        }

        /// <summary>
        /// Tries to convert the string representation of a number to its numerical equivalent,
        /// and returns a value that indicates whether the conversion succeeded.
        /// </summary>
        /// <param name="s">
        /// A string that contains a number to convert.
        /// </param>
        /// <param name="result">
        /// When this method returns, contains the numerical value equivalent to the number
        /// contained in s, if the conversion succeeded, or zero if the conversion failed.
        /// </param>
        /// <returns>
        /// True if s was converted successfully; otherwise, false.
        /// </returns>
        protected override bool TryParse(string s, out double result)
        {
            return double.TryParse(s, out result);
        }

        /// <summary>
        /// Tries to convert the string representation of a number to its numerical equivalent,
        /// and returns a value that indicates whether the conversion succeeded.
        /// </summary>
        /// <param name="s">
        /// A string that contains a number to convert.
        /// </param>
        /// <param name="style">
        /// A bitwise combination of enumeration values that indicates the permitted format of
        /// s.
        /// </param>
        /// <param name="result">
        /// When this method returns, contains the numerical value equivalent to the number
        /// contained in s, if the conversion succeeded, or zero if the conversion failed.
        /// </param>
        /// <returns>
        /// True if s was converted successfully; otherwise, false.
        /// </returns>
        protected override bool TryParse(string s, NumberStyles style, out double result)
        {
            return double.TryParse(s, style, CultureInfo.CurrentCulture, out result);
        }
        #endregion Methods
    } // RSG.Metadata.Model.Converters.StringToDoubleConverter {Class}
} // RSG.Metadata.Model.Converters {Namespace}
