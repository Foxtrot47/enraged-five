﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.Base.ConfigParser;
using RSG.Model.Map;
using System.IO;

namespace RSG.Model.Report.Reports
{
    /// <summary>
    /// 
    /// </summary>
    public class LodObjectReport : CSVReport, IDynamicMapReport
    {
        #region Constants
        private const String NAME = "Lod Object CSV Report";
        private const String DESCRIPTION = "Exports the selected levels object into a .csv file";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Determines whether the map data should be loaded
        /// </summary>
        public bool LoadMapData
        {
            get { return true; }
        }

        /// <summary>
        /// Determines whether the car generators should be loaded
        /// 
        /// </summary>
        public bool LoadCarGenarators
        {
            get { return false; }
        }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public LodObjectReport()
            : base(NAME, DESCRIPTION)
        {
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Generate the report dynamically.
        /// </summary>
        /// <param name="level"></param>
        /// <param name="gv"></param>
        public void Generate(ILevel level, ConfigGameView gv, IPlatformCollection platforms)
        {
            RSG.Model.Map.Util.Creation.InitialiseLevelStatistics(level, gv);

            // Write out the report
            using (StreamWriter writer = new StreamWriter(Filename))
            {
                WriteCsvHeader(writer);

                foreach (MapInstance instance in GatherLodInstances(level))
                {
                    WriteCsvInstance(writer, instance);
                }
            }
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        private IEnumerable<MapInstance> GatherLodInstances(ILevel level)
        {
            ISet<MapInstance> encounteredInstances = new HashSet<MapInstance>();

            // Loop over all sections
            foreach (MapSection section in RSG.Model.Map.Util.Enumerations.GetAllMapSections(level, null, true))
            {
                // Loop over all hd instances in the section
                foreach (MapInstance instance in section.AssetChildren.OfType<MapInstance>().Where(inst => inst.LODLevel == SceneXml.ObjectDef.LodLevel.HD))
                {
                    // Is this a prop (i.e. the definition is referenced in an external file)
                    if (instance.Container != instance.ReferencedDefinition.Container)
                    {
                        MapInstance parent = instance.LodParent;

                        while (parent != null)
                        {
                            if (!encounteredInstances.Contains(parent))
                            {
                                yield return parent;
                                encounteredInstances.Add(parent);
                            }
                            parent = parent.LodParent;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sw"></param>
        private void WriteCsvHeader(StreamWriter writer)
        {
            writer.WriteLine("Instance,Section,Parent Area,Grandparent Area,Lod Level,Lod Parent,Lod Children Count,Lod Distance,Poly Count,Shader Count,Texture Count");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="instance"></param>
        private void WriteCsvInstance(StreamWriter writer, MapInstance instance)
        {
            IMapContainer section = instance.Container;
            IMapContainer parent = section.Container;
            IMapContainer grandParent = (parent is MapArea ? (parent as MapArea).Container : null);

            MapDefinition definition = instance.ReferencedDefinition;

            string csvRecord = String.Format("{0},{1},{2},{3},", instance.Name, section.Name, parent != null ? parent.Name : "n/a", grandParent != null ? grandParent.Name : "n/a");
            csvRecord += String.Format("{0},{1},{2},{3},", instance.LODLevel, instance.LodParent != null ? instance.LodParent.Name : "n/a", instance.LodChildren.Count, instance.LodDistance);
            csvRecord += String.Format("{0},{1},{2}", definition.PolygonCount, definition.Shaders.Count, definition.Textures.Count());
            writer.WriteLine(csvRecord);
        }
        #endregion // Private Methods
    } // LodObjectReport
}
