﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.ConfigParser;
using RSG.Model.Common;
using RSG.Model.Map;

namespace RSG.Model.Report.Reports
{
    /// <summary>
    /// 
    /// </summary>
    public class ObjectCSVReport : CSVReport, IDynamicMapReport
    {
        #region Constants
        private const String NAME = "Export Object CSV Report";
        private const String DESCRIPTION = "Exports the selected levels object into a .csv file"; 
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Determines whether the map data should be loaded
        /// </summary>
        public bool LoadMapData
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Determines whether the car generators should be loaded
        /// 
        /// </summary>
        public bool LoadCarGenarators
        {
            get { return false; }
        }
        #endregion // Properties

        #region Construtcor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public ObjectCSVReport()
            : base(NAME, DESCRIPTION)
        {
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Generate the report dynamically.
        /// </summary>
        /// <param name="level"></param>
        /// <param name="gv"></param>
        public void Generate(ILevel level, ConfigGameView gv, IPlatformCollection platforms)
        {
            string directory = System.IO.Path.GetDirectoryName(Filename);
            string filename = System.IO.Path.GetFileNameWithoutExtension(Filename);
            string objectFilename = System.IO.Path.Combine(directory, filename + ".csv");
            string interiorFilename = System.IO.Path.Combine(directory, filename + "_interiors.csv");
            ExportObjectCSVReport(objectFilename, interiorFilename, level, gv);
        }

        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="objectFilename"></param>
        /// <param name="interiorFilename"></param>
        /// <param name="level"></param>
        /// <param name="gv"></param>
        private static void ExportObjectCSVReport(String objectFilename, String interiorFilename, ILevel level, ConfigGameView gv)
        {
            RSG.Model.Map.Util.Creation.InitialiseLevelStatistics(level, gv);

            using (StreamWriter objectSw = new StreamWriter(objectFilename))
            {
                WriteCsvObjectHeader(objectSw, false);
                using (StreamWriter interiorSw = new StreamWriter(interiorFilename))
                {
                    WriteCsvObjectHeader(interiorSw, true);
                    foreach (MapSection section in RSG.Model.Map.Util.Enumerations.GetAllMapSections(level, null, true))
                    {
                        foreach (IAsset asset in section.AssetChildren)
                        {
                            if (asset is MapDefinition)
                            {
                                MapDefinition def = asset as MapDefinition;
                                WriteCsvDefinition(objectSw, interiorSw, def, section, def.IsMilo);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sw"></param>
        /// <param name="milo"></param>
        private static void WriteCsvObjectHeader(StreamWriter sw, Boolean milo)
        {
            if (milo)
            {
                string header = string.Format("Name,Section,Parent Area,Grandparent Area,Tri Count,Instance Count,Texture Count,Shader Count,Mover Collision,Weapons Collision,Camera Collision,River Collision");
                sw.WriteLine(header);
            }
            else
            {
                string header = string.Format("Name,Section,Parent Area,Grandparent Area,Tri Count,Bounding Box Volume (m^3),Tri Density (tris/m^3),Instance Count,Texture Count,Shader Count,Lod Distance,TXD Name,Mover Collision,Weapons Collision,Camera Collision,River Collision");
                sw.WriteLine(header);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objectSw"></param>
        /// <param name="interiorSw"></param>
        /// <param name="def"></param>
        /// <param name="section"></param>
        /// <param name="milo"></param>
        private static void WriteCsvDefinition(StreamWriter objectSw, StreamWriter interiorSw, MapDefinition def, MapSection section, Boolean milo)
        {
            int textureCount = 0;
            foreach (IAsset child in def.AssetChildren)
            {
                if (child is ITexture)
                {
                    textureCount++;
                }
            }

            IMapContainer parent = section.Container;
            IMapContainer grandParent = (parent is MapArea ? (parent as MapArea).Container : null);

            if (milo)
            {
                string scvRecord = string.Format("{0},{1},{2},{3},", def.Name, section.Name, parent != null ? parent.Name : "n/a", grandParent != null ? grandParent.Name : "n/a");
                scvRecord += string.Format("{0},{1},{2},{3},", def.PolygonCount, def.Instances.Count, textureCount, def.Shaders.Count);
                scvRecord += string.Format("{0},{1},{2},{3}", def.MoverPolygonCount, def.WeaponsPolygonCount, def.CameraPolygonCount, def.RiverPolygonCount);
                interiorSw.WriteLine(scvRecord);
            }
            else
            {
                float triDensity = 0;
                if (def.BoundingBox.Volume() != 0)
                {
                    triDensity = def.PolygonCount / def.BoundingBox.Volume();
                }

                string scvRecord = string.Format("{0},{1},{2},{3},", def.Name, section.Name, parent != null ? parent.Name : "n/a", grandParent != null ? grandParent.Name : "n/a");
                scvRecord += string.Format("{0},{1},{2},{3},{4},{5},", def.PolygonCount, def.BoundingBox.Volume(), triDensity, def.Instances.Count, textureCount, def.Shaders.Count);
                scvRecord += string.Format("{0},{1},", def.LodDistance, def.TextureDictionaryName);
                scvRecord += string.Format("{0},{1},{2},{3}", def.MoverPolygonCount, def.WeaponsPolygonCount, def.CameraPolygonCount, def.RiverPolygonCount);
                objectSw.WriteLine(scvRecord);
            }
        }
        #endregion // Private Methods
    } // ObjectCSVReport
} // RSG.Model.Report.Reports
