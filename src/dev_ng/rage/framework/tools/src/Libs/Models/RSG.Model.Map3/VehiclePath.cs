﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.SceneXml;
using RSG.Model.Common.Map;

namespace RSG.Model.Map3
{
    /// <summary>
    /// Collection of nodes and links that forms a path.
    /// </summary>
    public class VehiclePath : AssetBase
    {
        #region Private member fields

        Dictionary<Guid, VehicleNode> nodes;
        List<VehicleNodeLink> links;

        #endregion
        #region Public properties

        /// <summary>
        /// Vehicle node collection.
        /// </summary>
        public Dictionary<Guid, VehicleNode> Nodes
        {
            get { return nodes; }
        }

        /// <summary>
        /// Vehicle node link collection.
        /// </summary>
        public List<VehicleNodeLink> Links 
        { 
            get { return links; } 
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="scene">Scene.</param>
        /// <param name="dontUseForNavigation">If true, links marked as don't use for navigation will not be included.</param>
        public VehiclePath(Scene scene, bool dontUseForNavigation)
        {
            nodes = new Dictionary<Guid, VehicleNode>();
            links = new List<VehicleNodeLink>();

            foreach (TargetObjectDef node in scene.Objects.Where(obj => obj.IsVehicleNode()))
            {
                VehicleNode vehNode = new VehicleNode();
                vehNode.Position = node.NodeTransform.Translation;
                vehNode.StreetName = GetAttribute(node, AttrNames.VEHNOD_STREETNAME, AttrDefaults.VEHNOD_STREETNAME);
                vehNode.Density = GetNumeric(node, AttrNames.VEHNOD_DENSITY, AttrDefaults.VEHNOD_DENSITY);
                vehNode.LanesIn = GetNumeric(node, AttrNames.VEHLNK_LANES_IN, AttrDefaults.VEHLNK_LANES_IN);
                vehNode.LanesOut = GetNumeric(node, AttrNames.VEHLNK_LANES_OUT, AttrDefaults.VEHLNK_LANES_OUT);
                vehNode.SpecialUse = (VehicleNodeSpecialUse)GetNumeric(node, AttrNames.VEHNOD_SPECIAL, AttrDefaults.VEHNOD_SPECIAL);
                vehNode.IsDisabled = GetBoolean(node, AttrNames.VEHNOD_DISABLED, AttrDefaults.VEHNOD_DISABLED);
                vehNode.Guid = node.Guid;
                nodes.Add(node.Guid, vehNode);
            }

            foreach (var link in scene.Objects.Where(obj => obj.IsVehicleLink()))
            {
                VehicleNodeLink vehLink = new VehicleNodeLink();
                vehLink.Node1 = link.GenericReferenceGuids[0];
                vehLink.Node2 = link.GenericReferenceGuids[1];
                vehLink.IsShortCut = GetBoolean(link, AttrNames.VEHLNK_SHORTCUT, AttrDefaults.VEHLNK_SHORTCUT);

                if (dontUseForNavigation)
                {
                    bool dontNavi = GetBoolean(link, AttrNames.VEHLNK_NONAVIGATION, AttrDefaults.VEHLNK_NONAVIGATION);
                    if (dontNavi)
                    {
                        continue;
                    }
                }

                links.Add(vehLink);
            }

            MarryLinks();
        }

        #endregion

        #region Private helper methods

        /// <summary>
        /// Marry the links to their nodes.
        /// </summary>
        private void MarryLinks()
        {
            for (int i = 0; i < links.Count; i++)
            {
                links[i].FirstNode = nodes[links[i].Node1];
                links[i].SecondNode = nodes[links[i].Node2];
            }
        }

        /// <summary>
        /// Get a string attribute.
        /// </summary>
        /// <param name="objectDef">Object definition.</param>
        /// <param name="attrName">Attribute name.</param>
        /// <param name="defaultValue">Default value.</param>
        /// <returns>Value.</returns>
        private string GetAttribute(TargetObjectDef objectDef, string attrName, string defaultValue)
        {
            return objectDef.Attributes.ContainsKey(attrName) ? objectDef.Attributes[attrName].ToString() : defaultValue;
        }

        /// <summary>
        /// Get a numeric attribute.
        /// </summary>
        /// <param name="objectDef">Object definition.</param>
        /// <param name="attrName">Attribute name.</param>
        /// <param name="defaultValue">Default value.</param>
        /// <returns>Value.</returns>
        private int GetNumeric(TargetObjectDef objectDef, string attrName, int defaultValue)
        {
            return objectDef.Attributes.ContainsKey(attrName) ? int.Parse(objectDef.Attributes[attrName].ToString()) : defaultValue;
        }

        /// <summary>
        /// Get a Boolean attribute.
        /// </summary>
        /// <param name="objectDef">Object definition.</param>
        /// <param name="attrName">Attribute name.</param>
        /// <param name="defaultValue">Default value.</param>
        /// <returns></returns>
        private bool GetBoolean(TargetObjectDef objectDef, string attrName, bool defaultValue)
        {
            return objectDef.Attributes.ContainsKey(attrName) ? bool.Parse(objectDef.Attributes[attrName].ToString()) : defaultValue;
        }
        
        #endregion
    }
}
