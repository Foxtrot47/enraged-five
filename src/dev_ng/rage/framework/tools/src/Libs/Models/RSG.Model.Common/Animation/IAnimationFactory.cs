﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common.Animation
{
    /// <summary>
    /// 
    /// </summary>
    public interface IAnimationFactory
    {
        #region Methods
        /// <summary>
        /// Creates a new cutscene collection from the specified source.
        /// </summary>
        /// <param name="source">Source to use for creating the collection.</param>
        /// <param name="buildIdentifier">Optional build identifier for when the source is Database.</param>
        ICutsceneCollection CreateCutsceneCollection(DataSource source, String buildIdentifier = null);

        /// <summary>
        /// Creates a collection of clip dictionaries from the specified source.
        /// </summary>
        /// <param name="source">Source to use for creating the collection.</param>
        /// <param name="buildIdentifier">Optional build identifier for when the source is Database.</param>
        IClipDictionaryCollection CreateClipDictionaryCollection(DataSource source, String buildIdentifier = null);
        
        /// <summary>
        /// 
        /// </summary>
        IEnumerable<IClipDictionaryCollection> CreateInGameClipDictionaryCollectionsFromExportData();

        /// <summary>
        /// 
        /// </summary>
        IEnumerable<IClipDictionaryCollection> CreateCutsceneClipDictionaryCollectionsFromExportData();
        #endregion // Methods
    } // IAnimationFactory
}
