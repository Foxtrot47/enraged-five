﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using RSG.Model.Common.Map;
using RSG.Model.Common;
using RSG.SceneXml;
using RSG.SceneXml.Material;
using RSG.Model.Asset;
using RSG.SceneXml.Statistics;
using RSG.Base.Math;
using System.Collections.ObjectModel;
using RSG.Base.ConfigParser;
using RSG.Statistics.Common.Dto.GameAssetStats;
using RSG.Statistics.Common.Dto.GameAssetStats.Bundles;

namespace RSG.Model.Map3
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class SimpleMapArchetypeBase : MapArchetypeBase, ISimpleMapArchetype
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        [StreamableStatAttribute(StreamableSimpleMapArchetypeStat.IsDynamicString)]
        public bool IsDynamic
        {
            get
            {
                CheckStatLoaded(StreamableSimpleMapArchetypeStat.IsDynamic);
                return m_isDynamic;
            }
            protected set
            {
                SetPropertyValue(ref m_isDynamic, value, "IsDynamic");
                SetStatLodaded(StreamableSimpleMapArchetypeStat.IsDynamic, true);
            }
        }
        private bool m_isDynamic;

        /// <summary>
        /// 
        /// </summary>
        [StreamableStatAttribute(StreamableSimpleMapArchetypeStat.IsFixedString)]
        public bool IsFixed
        {
            get
            {
                CheckStatLoaded(StreamableSimpleMapArchetypeStat.IsFixed);
                return m_isFixed;
            }
            protected set
            {
                SetPropertyValue(ref m_isFixed, value, "IsFixed");
                SetStatLodaded(StreamableSimpleMapArchetypeStat.IsFixed, true);
            }
        }
        private bool m_isFixed;

        /// <summary>
        /// Obtained from the AttrNames.OBJ_COLLISION_GROUP attribute.
        /// </summary>
        [StreamableStatAttribute(StreamableSimpleMapArchetypeStat.CollisionGroupString)]
        public String CollisionGroup
        {
            get
            {
                CheckStatLoaded(StreamableSimpleMapArchetypeStat.CollisionGroup);
                return m_collisionGroup;
            }
            protected set
            {
                SetPropertyValue(ref m_collisionGroup, value, "CollisionGroup", "HasDefaultCollisionGroup");
                SetStatLodaded(StreamableSimpleMapArchetypeStat.CollisionGroup, true);
            }
        }
        private String m_collisionGroup;

        /// <summary>
        /// Whether the collision group this archetype is in is the default group. (Non-streamable, request the collision group stat for this property).
        /// </summary>
        public bool HasDefaultCollisionGroup
        {
            get
            {
                CheckStatLoaded(StreamableSimpleMapArchetypeStat.CollisionGroup);
                return (CollisionGroup == AttrDefaults.OBJ_COLLISION_GROUP);
            }
        }

        /// <summary>
        /// Obtained from the AttrNames.OBJ_FORCE_BAKE_COLLISION attribute.
        /// </summary>
        [StreamableStatAttribute(StreamableSimpleMapArchetypeStat.ForceBakeCollisionString)]
        public bool ForceBakeCollision
        {
            get
            {
                CheckStatLoaded(StreamableSimpleMapArchetypeStat.ForceBakeCollision);
                return m_forceBakeCollision;
            }
            protected set
            {
                SetPropertyValue(ref m_forceBakeCollision, value, "ForceBakeCollision");
                SetStatLodaded(StreamableSimpleMapArchetypeStat.ForceBakeCollision, true);
            }
        }
        private bool m_forceBakeCollision;

        /// <summary>
        /// Obtained from the AttrNames.OBJ_NEVER_BAKE_COLLISION attribute.
        /// </summary>
        [StreamableStatAttribute(StreamableSimpleMapArchetypeStat.NeverBakeCollisionString)]
        public bool NeverBakeCollision
        {
            get
            {
                CheckStatLoaded(StreamableSimpleMapArchetypeStat.NeverBakeCollision);
                return m_neverBakeCollision;
            }
            protected set
            {
                SetPropertyValue(ref m_neverBakeCollision, value, "NeverBakeCollision");
                SetStatLodaded(StreamableSimpleMapArchetypeStat.NeverBakeCollision, true);
            }
        }
        private bool m_neverBakeCollision;

        /// <summary>
        /// Drawable LODs this archetype has
        /// </summary>
        [Browsable(false)]
        [StreamableStatAttribute(StreamableSimpleMapArchetypeStat.DrawableLODsString)]
        public IDictionary<MapDrawableLODLevel, IMapDrawableLOD> DrawableLODs
        {
            get
            {
                CheckStatLoaded(StreamableSimpleMapArchetypeStat.DrawableLODs);
                return m_drawableLods;
            }
            protected set
            {
                SetPropertyValue(ref m_drawableLods, value, "DrawableLODs");
                SetStatLodaded(StreamableSimpleMapArchetypeStat.DrawableLODs, true);
            }
        }
        private IDictionary<MapDrawableLODLevel, IMapDrawableLOD> m_drawableLods;

        /// <summary>
        /// List of spawn points associated with this archetype.
        /// </summary>
        [StreamableStatAttribute(StreamableSimpleMapArchetypeStat.SpawnPointsString)]
        public ICollection<ISpawnPoint> SpawnPoints
        {
            get
            {
                CheckStatLoaded(StreamableSimpleMapArchetypeStat.SpawnPoints);
                return m_spawnPoints;
            }
            protected set
            {
                SetPropertyValue(ref m_spawnPoints, value, "SpawnPoints");
                SetStatLodaded(StreamableSimpleMapArchetypeStat.SpawnPoints, true);
            }
        }
        private ICollection<ISpawnPoint> m_spawnPoints;
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="name"></param>
        /// <param name="parent"></param>
        public SimpleMapArchetypeBase(string name, IMapSection parent)
            : base(name, parent)
        {
        }
        #endregion // Constructor(s)

        #region MapArchetypeBase Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="drawabeObject"></param>
        /// <param name="scene"></param>
        protected override void LoadStatsFromDrawableObject(TargetObjectDef drawableSceneObject, Scene scene)
        {
            base.LoadStatsFromDrawableObject(drawableSceneObject, scene);

            // Finally determine some other stats
            CollisionGroup = GetCollisionGroup(drawableSceneObject);
            ForceBakeCollision = drawableSceneObject.HasForceBakeCollisionFlag();
            NeverBakeCollision = drawableSceneObject.HasNeverBakeCollisionFlag();

            SpawnPoints = RetrieveChildSpawnPoints(drawableSceneObject);

            // Go down the Drawable LOD tree
            DrawableLODs = new Dictionary<MapDrawableLODLevel, IMapDrawableLOD>();
            GetDrawableLODs(drawableSceneObject);

            // Read in whether this is a dynamic drawable or not
            IsDynamic = drawableSceneObject.GetAttribute(AttrNames.OBJ_IS_DYNAMIC, AttrDefaults.OBJ_IS_DYNAMIC);
            IsFixed = drawableSceneObject.GetAttribute(AttrNames.OBJ_IS_FIXED, AttrDefaults.OBJ_IS_FIXED);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="stat"></param>
        protected override void ProcessComplexArchetypeStat(ArchetypeStatDto dto, StreamableStat stat)
        {
            SimpleMapArchetypeStatDto mapDto = dto as SimpleMapArchetypeStatDto;
            Debug.Assert(mapDto != null, "Incorrect dto type encountered.");

            if (stat == StreamableSimpleMapArchetypeStat.SpawnPoints)
            {
                ProcessSpawnPointStats(mapDto.SpawnPoints);
            }
            else if (stat == StreamableSimpleMapArchetypeStat.DrawableLODs)
            {
                ProcessDrawableLodStats(mapDto.DrawableLods);
            }
            else
            {
                base.ProcessComplexArchetypeStat(dto, stat);
            }
        }
        #endregion // MapArchetypeBase Overrides

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sceneObject"></param>
        /// <returns></returns>
        protected String GetCollisionGroup(TargetObjectDef sceneObject)
        {
            string group = AttrDefaults.OBJ_COLLISION_GROUP;

            if (ParentSection.ExportEntities == false)
            {
                group = sceneObject.GetAttribute<string>(AttrNames.OBJ_COLLISION_GROUP, AttrDefaults.OBJ_COLLISION_GROUP);
                if (group == AttrDefaults.OBJ_COLLISION_GROUP)
                {
                    group = ParentSection.Name;
                }
            }

            return group;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sceneObject"></param>
        protected ICollection<ISpawnPoint> RetrieveChildSpawnPoints(TargetObjectDef sceneObject)
        {
            List<ISpawnPoint> spawnPoints = new List<ISpawnPoint>();
            foreach (TargetObjectDef child in sceneObject.Children)
            {
                if (child.Is2dfxSpawnPoint())
                {
                    spawnPoints.Add(new SpawnPoint(child, this));
                }

                spawnPoints.AddRange(this.RetrieveChildSpawnPoints(child));
            }

            return spawnPoints;
        }

        /// <summary>
        /// Walks down the Drawable LOD tree from the HD object and creates a MapDrawableLOD for each
        /// </summary>
        /// <param name="drawableSceneObject"></param>
        private void GetDrawableLODs(TargetObjectDef drawableSceneObject)
        {
            // Test for drawable LOD.
            if (drawableSceneObject.DrawableLOD != null && drawableSceneObject.DrawableLOD.Parent != null)
            {
                WalkDrawableLODs(drawableSceneObject.DrawableLOD.Parent, MapDrawableLODLevel.Medium);
            }
        }

        /// <summary>
        /// Walks down the Drawable LOD tree and creates a MapDrawableLOD for each
        /// </summary>
        /// <param name="drawableLodObject"></param>
        /// <param name="lodType"></param>
        private void WalkDrawableLODs(TargetObjectDef drawableLodObject, MapDrawableLODLevel lodType)
        {
            Debug.Assert(lodType >= 0 && (int)lodType < Enum.GetValues(typeof(MapDrawableLODLevel)).Length,
                "Drawable LOD type is out of range.  Stopping population.");
            if (!(lodType >= 0 && (int)lodType < Enum.GetValues(typeof(MapDrawableLODLevel)).Length))
                return;

            this.DrawableLODs.Add(lodType, new MapDrawableLOD(drawableLodObject, lodType));

            if (drawableLodObject.DrawableLOD != null && drawableLodObject.DrawableLOD.Parent != null)
            {
                TargetObjectDef drawableLODChild = drawableLodObject.DrawableLOD.Parent;
                WalkDrawableLODs(drawableLODChild, ++lodType);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dtos"></param>
        private void ProcessSpawnPointStats(List<SpawnPointStatDto> dtos)
        {
            SpawnPoints = new List<ISpawnPoint>();

            foreach (SpawnPointStatDto dto in dtos)
            {
                SpawnPoint spawnPoint = new SpawnPoint(dto.Name, this);
                spawnPoint.LoadStatsFromDatabase(dto);
                SpawnPoints.Add(spawnPoint);
            }
        }

        private void ProcessDrawableLodStats(List<DrawableLodStatDto> dtos)
        {
            DrawableLODs = new Dictionary<MapDrawableLODLevel, IMapDrawableLOD>();

            foreach (DrawableLodStatDto dto in dtos)
            {
                DrawableLODs[dto.LodLevel] = new MapDrawableLOD(dto);
            }
        }
        #endregion // Private Methods
    } // SimpleMapArchetypeBase
}
