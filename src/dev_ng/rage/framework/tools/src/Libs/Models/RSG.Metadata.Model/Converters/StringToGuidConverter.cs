﻿//---------------------------------------------------------------------------------------------
// <copyright file="StringToGuidConverter.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Converters
{
    using System;
    using RSG.Base;

    /// <summary>
    /// Represents a converter that can convert a string instance into a single or an array of
    /// globally unique identifier values. This class cannot be inherited.
    /// </summary>
    public class StringToGuidConverter : ConverterBase<Guid>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="StringToGuidConverter"/> class.
        /// </summary>
        public StringToGuidConverter()
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Implements the logic of the conversion between a single string instance and a
        /// globally unique identifier value.
        /// </summary>
        /// <param name="value">
        /// The string instance to convert.
        /// </param>
        /// <param name="fallback">
        /// Specifies the value that is returned if the conversion fails.
        /// </param>
        /// <param name="methods">
        /// The conversion methods that can be used during this conversion.
        /// </param>
        /// <returns>
        /// A structure containing the result of the conversion and a value indicating whether
        /// the conversion was successful.
        /// </returns>
        protected override TryResult<Guid> Try(string value, Guid fallback, Methods methods)
        {
            if (String.IsNullOrWhiteSpace(value))
            {
                return new TryResult<Guid>(fallback, false);
            }

            value = value.Trim();
            Guid result;
            if (Guid.TryParse(value, out result))
            {
                return new TryResult<Guid>(result, true);
            }

            return new TryResult<Guid>(fallback, false);
        }
        #endregion Methods
    } // RSG.Metadata.Model.Converters.StringToGuidConverter {Class}
} // RSG.Metadata.Model.Converters {Namespace}
