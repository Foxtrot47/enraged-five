﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Runtime.Serialization;

namespace RSG.Model.Statistics.Captures
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class ThreadResult
    {
        #region Properties
        /// <summary>
        /// Name of the thread these results are for.
        /// </summary>
        [DataMember]
        public String Name { get; set; }

        /// <summary>
        /// Minimum value recorded for this result.
        /// </summary>
        [DataMember]
        public float Min { get; set; }

        /// <summary>
        /// Maximum value recorded for this result.
        /// </summary>
        [DataMember]
        public float Max { get; set; }

        /// <summary>
        /// Average value recorded for this result.
        /// </summary>
        [DataMember]
        public float Average { get; set; }

        /// <summary>
        /// Standard deviation for this result.
        /// </summary>
        [DataMember]
        public float StandardDeviation { get; set; }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ThreadResult()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public ThreadResult(XElement element)
        {
            // Extract the various stats
            XElement nameElement = element.Element("name");
            if (nameElement != null)
            {
                Name = nameElement.Value;
            }
            XElement minElement = element.Element("min");
            if (minElement != null && minElement.Attribute("value") != null)
            {
                Min = Single.Parse(minElement.Attribute("value").Value);
            }

            XElement maxElement = element.Element("max");
            if (maxElement != null && maxElement.Attribute("value") != null)
            {
                Max = Single.Parse(maxElement.Attribute("value").Value);
            }

            XElement avgElement = element.Element("average");
            if (avgElement != null && avgElement.Attribute("value") != null)
            {
                Average = Single.Parse(avgElement.Attribute("value").Value);
            }

            XElement stdElement = element.Element("std");
            if (stdElement != null && stdElement.Attribute("value") != null)
            {
                StandardDeviation = Single.Parse(stdElement.Attribute("value").Value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="average"></param>
        public ThreadResult(String name, float min, float max, float average, float std)
        {
            Name = name;
            Min = min;
            Max = max;
            Average = average;
            StandardDeviation = std;
        }
        #endregion // Constructor(s)
    } // ThreadResult
}
