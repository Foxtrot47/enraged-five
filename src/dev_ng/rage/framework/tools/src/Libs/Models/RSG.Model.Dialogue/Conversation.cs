﻿using System;
using System.IO;
using System.Windows;
using System.Xml;
using System.Xml.XPath;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.ComponentModel;
using RSG.Model.Dialogue.UndoManager;
using RSG.Model.Dialogue.UndoInterface;
using System.Windows.Data;
using System.Text.RegularExpressions;


namespace RSG.Model.Dialogue
{
    public enum Hardware
    {
        Generation7,
        Generation8,
    }

    public class CharacterValue : UndoRedoBase
    {
        public KeyValuePair<Guid, string> Value { get; private set; }

        public string Name { get { return this.Value.Value; } }

        public Guid Id { get { return this.Value.Key; } }

        public bool IsUsed
        {
            get { return this.isUsed; }
            set { this.isUsed = value; OnPropertyChanged("IsUsed"); }
        }
        private bool isUsed;

        public CharacterValue(KeyValuePair<Guid, string> value, bool isUsed)
        {
            this.Value = value;
            this.IsUsed = isUsed;
        }
    }

    /// <summary>
    /// Represents a single conversation in the mission dialogue
    /// </summary>
    public class Conversation : UndoRedoBase
    {
        #region Constants
        private static readonly String PlaceHolderAttribute = "placeholder";
        private static readonly String RootAttribute = "root";
        private static readonly String DescriptionAttribute = "description";
        private static readonly String RandomAttribute = "random";
        private static readonly String CategoryAttribute = "category";
        private static readonly String LockedAttribute = "locked";
        private static readonly String RoleAttribute = "role";
        private static readonly String ModelNameAttribute = "model";
        private static readonly String VoiceAttribute = "voice";
        private static readonly String UseVoiceAttribute = "usevoice";
        private static readonly String InterruptibleAttribute = "interruptible";
        private static readonly String IsAnimTriggeredAttribute = "anim";
        private static readonly String CutsceneSubtitlesAttribute = "cutscene";

        private static readonly XPathExpression XmlLinePath = XPathExpression.Compile("Lines/Line");
        private static readonly XPathExpression XmlAttributes = XPathExpression.Compile("@*");

        #endregion // Constants
        
        #region Properties and associated member data

        /// <summary>
        /// Set to true if this conversation is just a placeholder written by the designer of the mission.
        /// </summary>
        [Undoable(), DisplayNameAttribute("Place Holder")]
        public Boolean PlaceHolder
        {
            get { return m_placeholder; }
            set
            {
                SetPropertyValue(value, () => this.PlaceHolder,
                    new UndoSetterDelegate(delegate(Object newValue) { m_placeholder = (Boolean)newValue; }));
            }
        }
        private Boolean m_placeholder;

        /// <summary>
        /// </summary>
        [Undoable(), DisplayNameAttribute("Conversation Interruptible")]
        public Boolean Interruptible
        {
            get { return m_interruptible; }
            set
            {
                SetPropertyValue(value, () => this.Interruptible,
                    new UndoSetterDelegate(delegate(Object newValue) { m_interruptible = (Boolean)newValue; }));
            }
        }
        private Boolean m_interruptible;

        /// <summary>
        /// </summary>
        [Undoable(), DisplayNameAttribute("Conversation AnimTriggered")]
        public Boolean IsAnimTriggered
        {
            get { return m_isAnimTriggered; }
            set
            {
                SetPropertyValue(value, () => this.IsAnimTriggered,
                    new UndoSetterDelegate(delegate(Object newValue) { m_isAnimTriggered = (Boolean)newValue; }));
            }
        }
        private Boolean m_isAnimTriggered;

        /// <summary>
        /// </summary>
        [Undoable(), DisplayNameAttribute("Conversation CutsceneSubtitles")]
        public Boolean IsCutsceneSubtitles
        {
            get { return m_isCutsceneSubtitles; }
            set
            {
                SetPropertyValue(value, () => this.IsCutsceneSubtitles,
                    new UndoSetterDelegate(delegate(Object newValue) { m_isCutsceneSubtitles = (Boolean)newValue; }));
            }
        }
        private Boolean m_isCutsceneSubtitles;

        /// <summary>
        /// String id name for this conversation
        /// </summary>
        [Undoable(), Collapsable(), DisplayNameAttribute("Conversation Root")]
        public String Root
        {
            get { return m_root; }
            set 
            {
                String newStringValue = MissionDialogue.GetStringInRightFormat(value as String);

                SetPropertyValue(newStringValue, () => this.Root,
                    new UndoSetterDelegate(delegate(Object newValue) { m_root = (String)value; }));
            }
        }
        private String m_root;

        /// <summary>
        /// Description for this conversation
        /// </summary>
        [Undoable(), Collapsable(), DisplayNameAttribute("Conversation Description")]
        public String Description
        {
            get { return m_description; }
            set 
            {
                String newStringValue = MissionDialogue.GetStringInRightFormat(value as String);

                SetPropertyValue(newStringValue, () => this.Description,
                    new UndoSetterDelegate(delegate(Object newValue) { m_description = (String)newValue; }));
            }
        }
        private String m_description;

        /// <summary>
        /// Set to true if this conversation is a random conversation
        /// </summary>
        [Undoable(), DisplayNameAttribute("Random Conversation")]
        public Boolean Random
        {
            get { return m_random; }
            set 
            {
                SetPropertyValue(value, () => this.Random,
                    new UndoSetterDelegate(delegate(Object newValue)
                        { 
                            m_random = (Boolean)newValue;

                            this.FilenameErrors = false;
                            foreach (Line line in this.Lines)
                            {
                                if (!line.IsFilenameValid())
                                {
                                    this.FilenameErrors = true;
                                    break;
                                }
                            }

                            if (this.Lines.Count > 1)
                            {
                                KeyValuePair<Guid, String> character = this.Lines[0].Character;
                                for (int i = 1; i < this.Lines.Count; i++)
                                {
                                    this.Lines[i].Character = character;
                                }
                            }
                        
                        }));
            }
        }
        private Boolean m_random;

        /// <summary>
        /// Returns true if this conversation represents a mocap conversation
        /// uses the m_category property.
        /// </summary>
        [NonSearchable()]
        public Boolean MocapConversation
        {
            get { return m_mocapConversation; }
            set 
            {
                m_mocapConversation = value;
                OnPropertyChanged("MocapConversation");
                if (value == true)
                {
                    this.FilenameErrors = false;
                }
                else
                {
                    this.FilenameErrors = false;
                    foreach (var line in this.Lines)
                    {
                        if (!line.IsFilenameValid())
                        {
                            this.FilenameErrors = true;
                            break;
                        }
                    }
                }
            }
        }
        private Boolean m_mocapConversation;

        /// <summary>
        /// The category for this conversation
        /// (i.e Mocap, cellphone). Makes sure that the property changed event for MocapConversation is fired since 
        /// it uses this property.
        /// </summary>
        [Undoable(), DisplayNameAttribute("Conversation Category")]
        public String Category
        {
            get { return m_category; }
            set 
            {
                SetPropertyValue(value, () => this.Category,
                    new UndoSetterDelegate(delegate(Object newValue) { m_category = (String)newValue; this.MocapConversation = (m_category == "MoCap"); }));
            }
        }
        private String m_category;

        /// <summary>
        /// Represents whether this conversation is locked (i.e has had automatic
        /// filenames generated for it)
        /// </summary>
        [Undoable(), NonSearchable()]
        public Boolean Locked
        {
            get { return m_locked; }
            set 
            {
                SetPropertyValue(value, () => this.Locked,
                    new UndoSetterDelegate(delegate(Object newValue) { m_locked = (Boolean)newValue; this.Mission.DetermineLockedConversations(); }));
                //m_locked = value;
                //OnPropertyChanged("Locked");
            }
        }
        private Boolean m_locked;
        
        /// <summary>
        /// Used only for random event missions
        /// </summary>
        [Undoable(), Collapsable(), DisplayNameAttribute("Role")]
        public String Role
        {
            get { return m_role; }
            set
            {
                String newStringValue = MissionDialogue.GetStringInRightFormat(value as String);

                SetPropertyValue(newStringValue, () => this.Role,
                    new UndoSetterDelegate(delegate(Object newValue) { m_role = (String)newValue; }));
            }
        }
        private String m_role;

        /// <summary>
        /// Used only for random event missions
        /// </summary>
        [Undoable(), Collapsable(), DisplayNameAttribute("Model Name")]
        public String ModelName
        {
            get { return m_modelName; }
            set
            {
                String newStringValue = MissionDialogue.GetStringInRightFormat(value as String);

                SetPropertyValue(newStringValue, () => this.ModelName,
                    new UndoSetterDelegate(delegate(Object newValue) { m_modelName = (String)newValue; }));
            }
        }
        private String m_modelName;

        /// <summary>
        /// Used only for random event missions
        /// </summary>
        [Undoable(), Collapsable(), DisplayNameAttribute("Voice")]
        public String Voice
        {
            get { return m_voice; }
            set
            {
                String newStringValue = MissionDialogue.GetStringInRightFormat(value as String);

                SetPropertyValue(newStringValue, () => this.Voice,
                    new UndoSetterDelegate(delegate(Object newValue) { m_voice = (String)newValue; }));
            }
        }
        private String m_voice;

        /// <summary>
        /// Used only for random event missions
        /// </summary>
        [Undoable(), DisplayNameAttribute("Use Voice")]
        public Boolean UseVoice
        {
            get { return m_useVoice; }
            set
            {
                SetPropertyValue(value, () => this.UseVoice,
                    new UndoSetterDelegate(delegate(Object newValue) { m_useVoice = (Boolean)newValue; }));
            }
        }
        private Boolean m_useVoice;

        /// <summary>
        /// The collection of lines that belong to this conversation
        /// </summary>
        [Undoable()]
        public ObservableCollection<Line> Lines
        {
            get { return m_lines; }
            set 
            {
                SetPropertyValue(value, () => this.Lines,
                    new UndoSetterDelegate(delegate(Object newValue)
                        {
                            foreach (Line line in m_lines)
                            {
                                this.Mission.DirtyManager.UnRegisterViewModel(line);
                                if (this.Random)
                                {
                                    continue;
                                }

                                if (!string.IsNullOrEmpty(line.Filename))
                                {
                                    if (!this.Mission.DeletedFilenames.Contains(line.Filename))
                                    {
                                        this.Mission.DeletedFilenames.Add(line.Filename);
                                    }
                                }
                            }

                            m_lines = (ObservableCollection<Line>)newValue;

                            foreach (Line line in m_lines)
                            {
                                this.Mission.DirtyManager.RegisterViewModel(line);
                                if (!string.IsNullOrEmpty(line.Filename))
                                {
                                    if (this.Mission.DeletedFilenames.Contains(line.Filename))
                                    {
                                        this.Mission.DeletedFilenames.Remove(line.Filename);
                                    }
                                }
                            }

                            this.LinesModified();
                        }));
            }
        }
        private ObservableCollection<Line> m_lines;

        /// <summary>
        /// The mission dialogue object that this conversation belongs to.
        /// </summary>
        [NonSearchable()]
        public MissionDialogue Mission
        {
            get { return m_mission; }
            set 
            { 
                m_mission = value;
            }
        }
        private MissionDialogue m_mission;

        /// <summary>
        /// The collection of lines that belong to this conversation
        /// </summary>
        [NonSearchable()]
        public ObservableCollection<string> UsedCharacters
        {
            get { return m_usedCharacters; }
            set
            {
                SetPropertyValue(value, () => this.UsedCharacters,
                    new UndoSetterDelegate(delegate(Object newValue) { m_usedCharacters = (ObservableCollection<string>)newValue; }));
            }
        }
        private ObservableCollection<string> m_usedCharacters;

        /// <summary>
        /// Get a value indicating whether this conversation has lines that
        /// have missing character ids.
        /// </summary>
        [NonSearchable()]
        public Boolean MissingCharacters
        {
            get 
            {
                foreach (Line line in this.Lines)
                {
                    if (String.IsNullOrEmpty(line.CharacterName) || line.CharacterGuid == Guid.Empty)
                    {
                        return true;
                    }
                }
                return false;
            }
            set
            {
                OnPropertyChanged("MissingCharacters");
            }
        }

        /// <summary>
        /// Set to true if this conversation has a error in it to do with the filenames.
        /// Random filename syntax on non random conversation
        /// Non random filename syntax on random conversation
        /// Contains a filename that is not unique to the parent mission
        /// Empty filenames
        /// </summary>
        public Boolean FilenameErrors
        {
            get { return m_filenameErrors; }
            set
            {
                SetPropertyValue(value, () => this.FilenameErrors,
                    new UndoSetterDelegate(delegate(Object newValue) { m_filenameErrors = (Boolean)newValue; }));
            }
        }
        private Boolean m_filenameErrors;

        [NonSearchable()]
        public Boolean IsSelected
        {
            get { return m_selected; }
            set
            {
                SetPropertyValue(value, () => this.IsSelected,
                    new UndoSetterDelegate(delegate(Object newValue) { m_selected = (Boolean)newValue; }));
            }
        }
        private Boolean m_selected;
        #endregion // Properties and associated member data

        #region Constructor(s)

        /// <summary>
        /// The basic constructor that only takes a mission that it belongs to. You cannnot create a conversation
        /// without giving a mission.
        /// It adds a single empty line and leaves the other properties empty or to their default value.
        /// </summary>
        /// <param name="mission">The mission this conversation belongs to.</param>
        public Conversation(MissionDialogue mission)
        {
            if (mission == null)
                throw new ArgumentNullException("mission", "Cannot create a conversation without giving a valid mission that it belongs to");

            this.m_mission = mission;
            this.m_usedCharacters = new ObservableCollection<string>();
            this.m_placeholder = false;
            this.m_root = String.Empty;
            this.m_description = String.Empty;
            this.m_random = false;
            this.m_mocapConversation = false;
            this.m_category = mission.Configurations.ConversationCategories.First(); // this.Mission.Configurations.ConversationCategories.First();
            this.m_lines = new ObservableCollection<Line>();
            this.m_locked = false;
            this.m_role = String.Empty;
            this.m_modelName = String.Empty;
            this.m_voice = String.Empty;
            this.m_useVoice = false;
            this.m_interruptible = true;
            this.m_isAnimTriggered = false;
            this.m_isCutsceneSubtitles = false;

            mission.Configurations.CharactersChanged += this.OnCharactersChanged;
        }

        /// <summary>
        /// Loads a conversation from a xml document
        /// </summary>
        /// <param name="navigator">The navigator for this object in the xml file</param>
        /// <param name="mission">The mission this conversation belongs to</param>
        public Conversation(XPathNavigator navigator, MissionDialogue mission)
        {
            if (mission == null)
                throw new ArgumentNullException("mission", "Cannot create a conversation without giving a valid mission that it belongs to");

            if (navigator == null)
                throw new ArgumentNullException("navigator", "Cannot load a conversation without giving a valid navigator");

            this.m_mission = mission;
            this.m_usedCharacters = new ObservableCollection<string>();

            this.m_placeholder = false;
            this.m_root = String.Empty;
            this.m_description = String.Empty;
            this.m_random = false;
            this.m_mocapConversation = false;
            this.m_category = mission.Configurations.ConversationCategories.First(); //  this.Mission.Configurations.ConversationCategories.First();
            this.m_lines = new ObservableCollection<Line>();
            this.m_locked = false;
            this.m_role = String.Empty;
            this.m_modelName = String.Empty;
            this.m_voice = String.Empty;
            this.m_useVoice = false;
            this.m_interruptible = true;
            this.m_isAnimTriggered = false;
            this.m_isCutsceneSubtitles = false;

            this.Deserialise(navigator);

            this.FilenameErrors = false;
            foreach (Line line in this.Lines)
            {
                if (!line.IsFilenameValid())
                {
                    this.FilenameErrors = true;
                    break;
                }
            }

            mission.Configurations.CharactersChanged += this.OnCharactersChanged;
            foreach (Line line in this.Lines)
            {
                if (!this.UsedCharacters.Contains(line.Character.Value))
                {
                    this.UsedCharacters.Add(line.Character.Value);
                }
            }
        }
        #endregion // Constructor(s)

        #region Public View Function(s)
        /// <summary>
        /// Saves a conversation out to the given parentNode.
        /// </summary>
        /// <param name="xmlDoc">The xml doc the conversation is being saved to</param>
        /// <param name="parentNode">The parent node the conversation should be added to</param>
        public void SaveConversation(XmlDocument xmlDoc, XmlElement parentNode)
        {
            this.SerialiseConversation(xmlDoc, parentNode);
        }

        /// <summary>
        /// Adds a new line to this conversation.
        /// </summary>
        /// <param name="newIndex"></param>
        /// <returns>The new line that has been addded</returns>
        public Line AddNewLine(int newIndex)
        {
            Line newLine = new Line(this);
            if (this.Random == true && this.Lines.Count > 0)
                newLine.Character = this.Lines[0].Character;

            ObservableCollection<Line> newLines = new ObservableCollection<Line>(this.Lines);

            if (newIndex >= 0)
            {
                newLines.Insert(newIndex, newLine);
            }
            else
            {
                newLines.Add(newLine);
            }

            this.Lines = newLines;

            this.Mission.DirtyManager.RegisterViewModel(newLine);
            return newLine;
        }

        /// <summary>
        /// Adds a new line to this conversation but uses the private member so that
        /// the set property function doesn't get called.
        /// </summary>
        /// <param name="newIndex"></param>
        /// <returns>The new line that has been addded</returns>
        public Line AddNewLinePrivately(int newIndex)
        {
            Line newLine = new Line(this);

            if (newIndex >= 0)
            {
                this.m_lines.Insert(newIndex + 1, newLine);
            }
            else
            {
                this.m_lines.Add(newLine);
            }

            this.Mission.DirtyManager.RegisterViewModel(newLine);
            return newLine;
        }

        /// <summary>
        /// Removes the indexed line
        /// </summary>
        /// <param name="conversationIndex">The index of the line to remove</param>
        public void RemoveLine(int lineIndex)
        {
            if (lineIndex < 0 || lineIndex > this.m_lines.Count) return;

            Line removedLine = this.m_lines[lineIndex];
            this.Mission.DirtyManager.UnRegisterViewModel(removedLine);

            ObservableCollection<Line> newLines = new ObservableCollection<Line>(this.Lines);

            newLines.RemoveAt(lineIndex);

            this.Lines = newLines;
        }

        /// <summary>
        /// Adds the given line into the conversation list at the given index
        /// </summary>
        /// <param name="newIndex"></param>
        /// <returns></returns>
        public Line AddLine(Line newLine, int newIndex)
        {
            ObservableCollection<Line> newLines = new ObservableCollection<Line>(this.Lines);

            if (newIndex >= 0)
            {
                newLines.Insert(newIndex, newLine);
            }
            else
            {
                newLines.Add(newLine);
            }

            this.Lines = newLines;

            this.Mission.DirtyManager.RegisterViewModel(newLine);
            return newLine;
        }

        /// <summary>
        /// Moves the given line to the new given index
        /// </summary>
        /// <param name="conversation"></param>
        /// <param name="newIndex"></param>
        public void MoveLine(Line line, int newIndex)
        {
            int oldIndex = this.Lines.IndexOf(line);
            if (oldIndex < 0)
                return;

            this.RemoveLine(oldIndex);
            this.AddLine(line, newIndex);
        }

        /// <summary>
        /// Creates the filenames for all of the lines belonging to this conversation
        /// </summary>
        public void AutoGenerateFilenames(Collection<Line> linesToGenerate)
        {
            if (linesToGenerate.Count == 0)
            {
                return;
            }

            this.GenerateFilenames(linesToGenerate);
        }

        /// <summary>
        /// This gets the first two characters for the filename code and is related to the conversation index,
        /// and need to be found if there are already automatically generated filenames in the conversation.
        /// </summary>
        /// <returns></returns>
        public String GetConversationPrefix()
        {
            String conversationPrefix = String.Empty;

            foreach (Line line in this.Lines)
            {
                // Can return after finding the first one as all filenames will have the same index
                if (line.ManualFilename == false && !String.IsNullOrEmpty(line.Filename))
                {
                    String filename = line.Filename;
                    if (filename.Contains(this.Mission.MissionId))
                    {
                        filename = line.Filename.Remove(0, this.Mission.MissionId.Length);
                    }

                    // Test to see if this filename has the right pattern to do this.
                    if (System.Text.RegularExpressions.Regex.IsMatch(filename, ".*_....$"))
                    {
                        conversationPrefix = filename[filename.LastIndexOf('_') + 1].ToString() + filename[filename.LastIndexOf('_') + 2].ToString();
                    }
                    else if (System.Text.RegularExpressions.Regex.IsMatch(filename, ".*_...._..$"))
                    {
                        if (filename.Length >= 7)
                        {
                            conversationPrefix = filename[filename.Length - 7].ToString() + filename[filename.Length - 6].ToString();
                        }
                    }
                }
            }

            return conversationPrefix;
        }

        public String GetConversationPrefixIncludingManual()
        {
            string conversationPrefix = String.Empty;
            string maximumPrefix = String.Empty;

            foreach (Line line in this.Lines)
            {
                // Can return after finding the first one as all filenames will have the same index
                if (!String.IsNullOrEmpty(line.Filename))
                {
                    String filename = line.Filename;
                    if (filename.Contains(this.Mission.MissionId))
                    {
                        filename = line.Filename.Remove(0, this.Mission.MissionId.Length);
                    }

                    // Test to see if this filename has the right pattern to do this.
                    if (System.Text.RegularExpressions.Regex.IsMatch(filename, ".*_....$"))
                    {
                        conversationPrefix = filename[filename.LastIndexOf('_') + 1].ToString() + filename[filename.LastIndexOf('_') + 2].ToString();
                        if (MissionDialogue.GetIndexFromString(conversationPrefix) >= MissionDialogue.GetIndexFromString(maximumPrefix))
                        {
                            maximumPrefix = conversationPrefix;
                        }
                    }
                    else if (System.Text.RegularExpressions.Regex.IsMatch(filename, ".*_...._..$"))
                    {
                        if (filename.Length >= 7)
                        {
                            conversationPrefix = filename[filename.Length - 7].ToString() + filename[filename.Length - 6].ToString();
                            if (MissionDialogue.GetIndexFromString(conversationPrefix) >= MissionDialogue.GetIndexFromString(maximumPrefix))
                            {
                                maximumPrefix = conversationPrefix;
                            }
                        }
                    }
                }
            }

            return maximumPrefix;
        }
        

        public void GetNextLinePostfixes(out String linePostfix, out String randomLinePostfix)
        {
            linePostfix = "AA";
            randomLinePostfix = "01";

            foreach (Line line in this.Lines)
            {
                // Can return after finding the first one as all filenames will have the same index
                if (line.ManualFilename == false && !String.IsNullOrEmpty(line.Filename))
                {
                    String filename = line.Filename;
                    if (filename.Contains(this.Mission.MissionId))
                    {
                        filename = line.Filename.Remove(0, this.Mission.MissionId.Length);
                    }

                    // Test to see if this filename has the right pattern to do this.
                    if (System.Text.RegularExpressions.Regex.IsMatch(filename, ".*_....$"))
                    {
                        String postfix = filename.Substring(filename.Length - 2);
                        if (MissionDialogue.GetIndexFromString(postfix) >= MissionDialogue.GetIndexFromString(linePostfix))
                        {
                            linePostfix = MissionDialogue.IncrementString(postfix);
                        }
                    }
                    else if (System.Text.RegularExpressions.Regex.IsMatch(filename, ".*_.._..$"))
                    {
                        String postfix = filename.Substring(filename.Length - 2);
                        if (int.Parse(postfix) >= int.Parse(randomLinePostfix))
                        {
                            randomLinePostfix = (int.Parse(postfix) + 1).ToString();
                            if (randomLinePostfix.Length == 1)
                            {
                                randomLinePostfix = "0" + randomLinePostfix[0];
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Exports this particular conversation into the given text files
        /// </summary>
        /// <param name="dialogueFile">The text writer to write the non mocap conversations into</param>
        /// <param name="mocapFile">The text writer to write the mocap conversations into</param>
        /// <param name="subtitleFile">The text writer to write the subtitle data to</param>
        public bool Export(TextWriter dialogueFile, TextWriter mocapFile, TextWriter subtitleFile, Dictionary<String, int> characterIndices, Hardware hardwareGeneration)
        {
            Boolean randomEvent = this.Mission.RandomEventMission;

            bool missingCharacterId = false;
            int lineIndex = 0; 
            if (this.MocapConversation == false)
            {
                string SLLine = String.Empty;
                string LFLine = String.Empty;
                lineIndex = 0;
                foreach (Line line in this.Lines)
                {
                    lineIndex++;

                    if (this.Random == true)
                    {
                        if (randomEvent == true && characterIndices.ContainsKey(this.ModelName))
                        {
                            String exportString = "[";
                            if (!this.Root.StartsWith(this.Mission.MissionId))
                            {
                                exportString += (this.Mission.MissionId + "_");
                            }
                            exportString += this.Root;
                            exportString += characterIndices[this.ModelName].ToString();

                            exportString += ("A:" + this.Mission.MissionSubtitleId + "]");
                            subtitleFile.WriteLine(exportString);
                        }
                        else
                        {
                            subtitleFile.WriteLine("[" + this.Root + "A" + ":" + this.Mission.MissionSubtitleId + "]");
                        }

                        subtitleFile.WriteLine(line.Filename.Replace("_01", ""));

                        String speakerString = String.IsNullOrEmpty(line.Speaker.ToString()) ? "z" : line.Speaker.ToString();
                        String listenerString = String.IsNullOrEmpty(line.Listener.ToString()) ? "z" : line.Listener.ToString();
                        speakerString = ValidateSpeakerListener(speakerString);
                        listenerString = ValidateSpeakerListener(listenerString);

                        SLLine += speakerString + listenerString + Mission.Configurations.GetAudioTypeIndexFromString(line.AudioType).ToString();
                        if (line.Interruptible)
                        {
                            LFLine += "1";
                        }
                        else
                        {
                            LFLine += "0";
                        }

                        if (line.DucksRadio)
                        {
                            LFLine += "1";
                        }
                        else
                        {
                            LFLine += "0";
                        }

                        if (line.DucksScore)
                        {
                            LFLine += "1";
                        }
                        else
                        {
                            LFLine += "0";
                        }

                        LFLine += line.Audibility.ToString();

                        if (line.HeadsetSubmix || line.Special == "Headset")
                        {
                            LFLine += "1";
                        }
                        else
                        {
                            LFLine += "0";
                        }

                        if (line.DontInterruptForSpecialAbility)
                        {
                            LFLine += "1";
                        }
                        else
                        {
                            LFLine += "0";
                        }

                        if (hardwareGeneration == Hardware.Generation8)
                        {
                            if (line.ControllerPadSpeaker || line.Special == "Pad_Speaker")
                            {
                                LFLine += "1";
                            }
                            else
                            {
                                LFLine += "0";
                            }
                        }

                        break;
                    }
                    else
                    {
                        if (randomEvent == true && characterIndices.ContainsKey(this.ModelName))
                        {
                            String exportString = "[";
                            if (!this.Root.StartsWith(this.Mission.MissionId))
                            {
                                exportString += (this.Mission.MissionId + "_");
                            }
                            exportString += this.Root;
                            exportString += characterIndices[this.ModelName].ToString();

                            exportString += ("_" + lineIndex + "A:" + this.Mission.MissionSubtitleId + "]");

                            subtitleFile.WriteLine(exportString);
                        }
                        else
                        {
                            subtitleFile.WriteLine("[" + this.Root + "_" + lineIndex + "A:" + this.Mission.MissionSubtitleId + "]");
                        }
                        subtitleFile.WriteLine(line.Filename);

                        String speakerString = String.IsNullOrEmpty(line.Speaker.ToString()) ? "z" : line.Speaker.ToString();
                        String listenerString = String.IsNullOrEmpty(line.Listener.ToString()) ? "z" : line.Listener.ToString();
                        speakerString = ValidateSpeakerListener(speakerString);
                        listenerString = ValidateSpeakerListener(listenerString);

                        SLLine += speakerString + listenerString + Mission.Configurations.GetAudioTypeIndexFromString(line.AudioType).ToString();
                        if (line.Interruptible)
                        {
                            LFLine += "1";
                        }
                        else
                        {
                            LFLine += "0";
                        }

                        if (line.DucksRadio)
                        {
                            LFLine += "1";
                        }
                        else
                        {
                            LFLine += "0";
                        }

                        if (line.DucksScore)
                        {
                            LFLine += "1";
                        }
                        else
                        {
                            LFLine += "0";
                        }

                        LFLine += line.Audibility.ToString();

                        if (line.HeadsetSubmix || line.Special == "Headset")
                        {
                            LFLine += "1";
                        }
                        else
                        {
                            LFLine += "0";
                        }

                        if (line.DontInterruptForSpecialAbility)
                        {
                            LFLine += "1";
                        }
                        else
                        {
                            LFLine += "0";
                        }

                        if (hardwareGeneration == Hardware.Generation8)
                        {
                            if (line.ControllerPadSpeaker || line.Special == "Pad_Speaker")
                            {
                                LFLine += "1";
                            }
                            else
                            {
                                LFLine += "0";
                            }
                        }
                    }
                }

                if (randomEvent == true && characterIndices.ContainsKey(this.ModelName))
                {
                    String exportString = "[";
                    if (!this.Root.StartsWith(this.Mission.MissionId))
                    {
                        exportString += (this.Mission.MissionId + "_");
                    }
                    exportString += this.Root;
                    exportString += characterIndices[this.ModelName].ToString();

                    exportString += ("SL:" + this.Mission.MissionSubtitleId + "]");
                    subtitleFile.WriteLine(exportString);
                }
                else
                {
                    subtitleFile.WriteLine("[" + this.Root + "SL" + ":" + this.Mission.MissionSubtitleId + "]");
                }
                subtitleFile.WriteLine(SLLine);

                if (randomEvent == true && characterIndices.ContainsKey(this.ModelName))
                {
                    String exportString = "[";
                    if (!this.Root.StartsWith(this.Mission.MissionId))
                    {
                        exportString += (this.Mission.MissionId + "_");
                    }
                    exportString += this.Root;
                    exportString += characterIndices[this.ModelName].ToString();

                    exportString += ("IF:" + this.Mission.MissionSubtitleId + "]");
                    subtitleFile.WriteLine(exportString);
                }
                else
                {
                    subtitleFile.WriteLine("[" + this.Root + "IF" + ":" + this.Mission.MissionSubtitleId + "]");
                }
                string IFLine = string.Empty;
                if (this.Interruptible)
                {
                    IFLine += "1";
                }
                else
                {
                    IFLine += "0";
                }
                if (this.IsAnimTriggered)
                {
                    IFLine += "1";
                }
                else
                {
                    IFLine += "0";
                }
                if (hardwareGeneration == Hardware.Generation8)
                {
                    if (this.PlaceHolder)
                    {
                        IFLine += "1";
                    }
                    else
                    {
                        IFLine += "0";
                    }
                }

                subtitleFile.WriteLine(IFLine);

                if (randomEvent == true && characterIndices.ContainsKey(this.ModelName))
                {
                    String exportString = "[";
                    if (!this.Root.StartsWith(this.Mission.MissionId))
                    {
                        exportString += (this.Mission.MissionId + "_");
                    }
                    exportString += this.Root;
                    exportString += characterIndices[this.ModelName].ToString();

                    exportString += ("LF:" + this.Mission.MissionSubtitleId + "]");
                    subtitleFile.WriteLine(exportString);
                }
                else
                {
                    subtitleFile.WriteLine("[" + this.Root + "LF" + ":" + this.Mission.MissionSubtitleId + "]");
                }

                subtitleFile.WriteLine(LFLine);
                subtitleFile.WriteLine("");
            }

            lineIndex = 0;
            foreach (Line line in this.Lines)
            {
                lineIndex++;

                if (string.Equals(line.CharacterName, "SFX", StringComparison.CurrentCultureIgnoreCase)) continue;

                if (this.MocapConversation == false)
                {
                    if (String.IsNullOrEmpty(line.Filename))
                    {
                        MessageBox.Show("Filename is blank.\nLine: " + lineIndex + "\nConversation: " + this.Description + "\nFile: " + this.Mission.Filename, "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                    if (String.IsNullOrEmpty(line.CharacterName) || line.CharacterGuid == Guid.Empty)
                    {
                        missingCharacterId = true;
                    }

                    if (this.Random)
                    {
                        if (randomEvent == true && characterIndices.ContainsKey(this.ModelName))
                        {
                            String exportString = "[";
                            if (!this.Root.StartsWith(this.Mission.MissionId))
                            {
                                exportString += (this.Mission.MissionId + "_");
                            }
                            exportString += this.Root;
                            exportString += characterIndices[this.ModelName].ToString();

                            if (lineIndex <= 9)
                            {
                                exportString += ("_0" + lineIndex + ":" + this.Mission.MissionSubtitleId + "]");
                            }
                            else
                            {
                                exportString += ("_" + lineIndex + ":" + this.Mission.MissionSubtitleId + "]");
                            }
                            dialogueFile.WriteLine(exportString);
                        }
                        else
                        {
                            if (lineIndex <= 9)
                            {
                                dialogueFile.WriteLine("[" + this.Root + "_0" + lineIndex + ":" + this.Mission.MissionSubtitleId + "]");
                            }
                            else
                            {
                                dialogueFile.WriteLine("[" + this.Root + "_" + lineIndex + ":" + this.Mission.MissionSubtitleId + "]");
                            }
                        }
                    }
                    else
                    {
                        if (randomEvent == true && characterIndices.ContainsKey(this.ModelName))
                        {
                            String exportString = "[";
                            if (!this.Root.StartsWith(this.Mission.MissionId))
                            {
                                exportString += (this.Mission.MissionId + "_");
                            }
                            exportString += this.Root;
                            exportString += characterIndices[this.ModelName].ToString();

                            exportString += ("_" + lineIndex + ":" + this.Mission.MissionSubtitleId + "]");
                            dialogueFile.WriteLine(exportString);
                        }
                        else
                        {
                            dialogueFile.WriteLine("[" + this.Root + "_" + lineIndex + ":" + this.Mission.MissionSubtitleId + "]");
                        }
                    }

                    if (line.AlwaysSubtitled == false)
                        dialogueFile.WriteLine("~z~" + line.LineDialogue);
                    else
                        dialogueFile.WriteLine("~t~" + line.LineDialogue);

                }
                else
                {
                    if (String.IsNullOrEmpty(line.CharacterName) || line.CharacterGuid == Guid.Empty)
                    {
                        missingCharacterId = true;
                    }

                    if (this.Random)
                    {
                        if (randomEvent == true && characterIndices.ContainsKey(this.ModelName))
                        {
                            mocapFile.WriteLine("[" + this.Mission.MissionId + "_" + this.Root + characterIndices[this.ModelName] + "_0" + lineIndex + ":" + this.Mission.MissionSubtitleId + "]");
                        }
                        else
                        {
                            mocapFile.WriteLine("[" + this.Root + "_0" + lineIndex + ":" + this.Mission.MissionSubtitleId + "]");
                        }
                    }
                    else
                    {
                        if (randomEvent == true && characterIndices.ContainsKey(this.ModelName))
                        {
                            mocapFile.WriteLine("[" + this.Mission.MissionId + "_" + this.Root + characterIndices[this.ModelName] + "_" + lineIndex + ":" + this.Mission.MissionSubtitleId + "]");
                        }
                        else
                        {
                            mocapFile.WriteLine("[" + this.Root + "_" + lineIndex + ":" + this.Mission.MissionSubtitleId + "]");
                        }
                    }
                    
                    if (line.AlwaysSubtitled == false)
                        mocapFile.WriteLine("~z~" + line.LineDialogue);
                    else
                        mocapFile.WriteLine("~t~" + line.LineDialogue);
                }
            }

            if (this.MocapConversation == false)
            {
                dialogueFile.WriteLine("");
            }
            else
            {
                mocapFile.WriteLine("");
            }

            return missingCharacterId;
        }

        public void ExportForTranslation(TextWriter dialogueFile, Dictionary<String, int> characterIndices)
        {
            Boolean randomEvent = this.Mission.RandomEventMission;

            int lineIndex = 0;
            foreach (Line line in this.Lines)
            {
                lineIndex++;

                if (string.Equals(line.CharacterName, "SFX", StringComparison.CurrentCultureIgnoreCase))
                {
                    continue;
                }

                if (this.MocapConversation == true)
                {
                    continue;
                }
                
                if (this.Random)
                {
                    if (randomEvent == true && characterIndices.ContainsKey(this.ModelName))
                    {
                        String exportString = "[";
                        if (!this.Root.StartsWith(this.Mission.MissionId))
                        {
                            exportString += (this.Mission.MissionId + "_");
                        }
                        exportString += this.Root;
                        exportString += characterIndices[this.ModelName].ToString();

                        if (lineIndex <= 9)
                        {
                            exportString += ("_0" + lineIndex + ":" + this.Mission.MissionSubtitleId + "]");
                        }
                        else
                        {
                            exportString += ("_" + lineIndex + ":" + this.Mission.MissionSubtitleId + "]");
                        }
                        dialogueFile.WriteLine(exportString);
                    }
                    else
                    {
                        if (lineIndex <= 9)
                        {
                            dialogueFile.WriteLine("[" + this.Root + "_0" + lineIndex + ":" + this.Mission.MissionSubtitleId + "]");
                        }
                        else
                        {
                            dialogueFile.WriteLine("[" + this.Root + "_" + lineIndex + ":" + this.Mission.MissionSubtitleId + "]");
                        }
                    }
                }
                else
                {
                    if (randomEvent == true && characterIndices.ContainsKey(this.ModelName))
                    {
                        String exportString = "[";
                        if (!this.Root.StartsWith(this.Mission.MissionId))
                        {
                            exportString += (this.Mission.MissionId + "_");
                        }
                        exportString += this.Root;
                        exportString += characterIndices[this.ModelName].ToString();

                        exportString += ("_" + lineIndex + ":" + this.Mission.MissionSubtitleId + "]");
                        dialogueFile.WriteLine(exportString);
                    }
                    else
                    {
                        dialogueFile.WriteLine("[" + this.Root + "_" + lineIndex + ":" + this.Mission.MissionSubtitleId + "]");
                    }
                }

                if (line.AlwaysSubtitled == false)
                {
                    dialogueFile.WriteLine("~z~" + line.LineDialogue);
                }
                else
                {
                    dialogueFile.WriteLine("~t~" + line.LineDialogue);
                }

                dialogueFile.WriteLine("");
            }
        }

        public void Import(String filename)
        {
            GetTextFromDocument(filename);
            IDataObject data = Clipboard.GetDataObject();
            if (data.GetDataPresent(DataFormats.Text))
            {
                String text = data.GetData(DataFormats.Text) as String;

                String[] lines = text.Split("\r\n".ToArray());
                List<String> correctLines = new List<String>(lines);
                Boolean previousSpace = false;
                int index = 0;
                foreach (String line in lines)
                {
                    if (String.IsNullOrEmpty(line))
                    {
                        if (previousSpace == true)
                        {
                        }
                        else
                        {
                            correctLines.RemoveAt(index);
                            previousSpace = true;
                        }
                        index++;
                        continue;
                    }
                    previousSpace = false;
                }

                List<String[]> paragraphs = new List<String[]>();
                List<String> currentParagraph = new List<String>();
                Boolean startedParagraph = false;
                foreach (String line in correctLines)
                {
                    if (String.IsNullOrEmpty(line))
                    {
                        if (startedParagraph == true)
                        {
                            if (currentParagraph.Count > 1)
                            {
                                paragraphs.Add(currentParagraph.ToArray());
                            }
                            startedParagraph = false;
                        }
                        else
                        {
                            currentParagraph.Clear();
                        }
                        continue;
                    }
                    if (line.StartsWith("[") && line.EndsWith("]"))
                        continue;

                    startedParagraph = true;
                    currentParagraph.Add(line.Trim());
                }

                if (paragraphs.Count > 0)
                {
                    this.Lines = new ObservableCollection<Line>();
                }
                int missingCount = 0;
                List<String> missing = new List<String>();
                foreach (String[] paragraph in paragraphs)
                {
                    Boolean characterFound = false;
                    foreach (KeyValuePair<Guid, String> character in Mission.Configurations.Characters)
                    {
                        if (String.Compare(character.Value, paragraph[0], true) == 0)
                        {
                            Line line = new Line(this);
                            line.Character = new KeyValuePair<Guid, String>(character.Key, character.Value);
                            String dialogue = String.Empty;
                            for (int i = 1; i < paragraph.Length; i++)
                            {
                                dialogue += paragraph[i];
                            }
                            line.LineDialogue = dialogue;
                            this.Lines.Add(line);
                            characterFound = true;
                        }
                    }
                    if (characterFound == false)
                    {
                        missingCount++;
                        if (!missing.Contains(paragraph[0]))
                            missing.Add(paragraph[0]);
                    }
                }
                if (missingCount > 0)
                {
                    String message = String.Format("{0} lines are missing due to the fact that the following" +
                                                    " characters in the mocap document couldn't be found:", missingCount);
                    foreach (String miss in missing)
                    {
                        message += ("\n" + miss);
                    }
                    MessageBox.Show(message, "Import Error", MessageBoxButton.OK, MessageBoxImage.Error);                                
                }
            }           
        }

        private void GetTextFromDocument(String filename)
        {
            Object missing = System.Reflection.Missing.Value;
            Object fileName = filename;
            Object readOnly = true;
            Object falseValue = false;

            Microsoft.Office.Interop.Word.Application WordApp = null;
            
            Microsoft.Office.Interop.Word.Document aDoc = null;
            try
            {
                WordApp = new Microsoft.Office.Interop.Word.Application();
                WordApp.Visible = false;
                aDoc = WordApp.Documents.Open(ref fileName, ref missing, ref readOnly, ref missing, ref missing,
                    ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref falseValue);

                Microsoft.Office.Interop.Word.Range fullRange = aDoc.Range();
                String fullText = fullRange.Text;
                Object format = Microsoft.Office.Interop.Word.WdSaveFormat.wdFormatRTF;
                String rtfPath = Path.Combine(Path.GetDirectoryName(filename), "Dialoguestar_ImportTemp.rtf");
                aDoc.SaveAs(rtfPath, format);

                Clipboard.Clear();
                aDoc.Content.Select();
                aDoc.Content.Copy();

                aDoc.Close(falseValue, missing, missing);
                WordApp.Quit(falseValue, missing, missing);
            }
            catch
            {
                if (aDoc != null)
                    aDoc.Close(falseValue, missing, missing);
                if (WordApp != null)
                    WordApp.Quit(falseValue, missing, missing);
            }
        }
        #endregion

        #region Private Function(s)

        /// <summary>
        /// Saves a conversation out to the given parentNode.
        /// </summary>
        /// <param name="xmlDoc">The xml doc the conversation is being saved to</param>
        /// <param name="parentNode">The parent node the conversation should be added to</param>
        private void SerialiseConversation(XmlDocument xmlDoc, XmlElement parentNode)
        {
            // Create the element node
            XmlElement conversationNode = xmlDoc.CreateElement("Conversation");

            // Add the attributes to it.
            conversationNode.SetAttribute(PlaceHolderAttribute, this.PlaceHolder.ToString());
            conversationNode.SetAttribute(RootAttribute, this.Root);
            conversationNode.SetAttribute(DescriptionAttribute, this.Description);
            conversationNode.SetAttribute(RandomAttribute, this.Random.ToString());
            conversationNode.SetAttribute(CategoryAttribute, this.Category);
            conversationNode.SetAttribute(LockedAttribute, this.Locked.ToString());
            conversationNode.SetAttribute(RoleAttribute, this.Role);
            conversationNode.SetAttribute(ModelNameAttribute, this.ModelName);
            conversationNode.SetAttribute(VoiceAttribute, this.Voice);
            conversationNode.SetAttribute(UseVoiceAttribute, this.UseVoice.ToString());
            conversationNode.SetAttribute(InterruptibleAttribute, this.Interruptible.ToString());
            conversationNode.SetAttribute(IsAnimTriggeredAttribute, this.IsAnimTriggered.ToString());
            conversationNode.SetAttribute(CutsceneSubtitlesAttribute, this.IsCutsceneSubtitles.ToString());
            
            // Add the lines to it as child nodes
            XmlElement linesRootNode = xmlDoc.CreateElement("Lines");
            foreach (Line line in this.Lines)
            {
                line.SaveLine(xmlDoc, linesRootNode);
            }
            conversationNode.AppendChild(linesRootNode);

            // Add it to the root node
            parentNode.AppendChild(conversationNode);
        }

        /// <summary>
        /// Deserialise the conversation from the given navigator
        /// property
        /// </summary>
        private void Deserialise(XPathNavigator navigator)
        {
            XPathNodeIterator converationAttrIt = navigator.Select(XmlAttributes);
            while (converationAttrIt.MoveNext())
            {
                if (typeof(String) != converationAttrIt.Current.ValueType)
                    continue;
                String value = (converationAttrIt.Current.TypedValue as String);

                if (converationAttrIt.Current.Name == PlaceHolderAttribute)
                {
                    this.m_placeholder = (value == "True" ? true : false);
                }
                else if (converationAttrIt.Current.Name == RootAttribute)
                {
                    this.m_root = value;
                }
                else if (converationAttrIt.Current.Name == DescriptionAttribute)
                {
                    this.m_description = value;
                }
                else if (converationAttrIt.Current.Name == RandomAttribute)
                {
                    this.m_random = (value == "True" ? true : false);
                }
                else if (converationAttrIt.Current.Name == CategoryAttribute)
                {
                    this.Category = value;
                }
                else if (converationAttrIt.Current.Name == LockedAttribute)
                {
                    this.m_locked = (value == "True" ? true : false);
                }
                else if (converationAttrIt.Current.Name == RoleAttribute)
                {
                    this.m_role = value;
                }
                else if (converationAttrIt.Current.Name == ModelNameAttribute)
                {
                    this.m_modelName = value;
                }
                else if (converationAttrIt.Current.Name == VoiceAttribute)
                {
                    this.m_voice = value;
                }
                else if (converationAttrIt.Current.Name == UseVoiceAttribute)
                {
                    this.m_useVoice = (value == "True" ? true : false);
                }
                else if (converationAttrIt.Current.Name == InterruptibleAttribute)
                {
                    this.m_interruptible = (value == "True" ? true : false);
                }
                else if (converationAttrIt.Current.Name == IsAnimTriggeredAttribute)
                {
                    this.m_isAnimTriggered = (value == "True" ? true : false);
                }
                else if (converationAttrIt.Current.Name == CutsceneSubtitlesAttribute)
                {
                    this.m_isCutsceneSubtitles = (value == "True" ? true : false);
                }
            }

            // Load all of the lines in this converation
            XPathNodeIterator lineIterator = navigator.Select(XmlLinePath);
            while (lineIterator.MoveNext())
            {
                XPathNavigator lineNavigator = lineIterator.Current;
                Line newLine = new Line(lineNavigator, this);
                this.Lines.Add(newLine);
                this.Mission.DirtyManager.RegisterViewModel(newLine);
            }

            if (this.Random)
            {
                foreach (Line line in this.Lines)
                {
                    if (line.Filename[line.Filename.Length - 3] == '_')
                    {
                        line.Filename = line.Filename.Substring(0, line.Filename.Length - 3);
                    } 
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="linesToGenerate">
        /// 
        /// </param>
        private void GenerateNormalFilenames(Collection<Line> linesToGenerate)
        {
            HashSet<string> allFilenames = new HashSet<string>(this.Mission.DeletedFilenames);
            foreach (Conversation conversation in this.Mission.Conversations)
            {
                foreach (Line line in conversation.Lines)
                {
                    if (!string.IsNullOrWhiteSpace(line.Filename))
                    {
                        allFilenames.Add(line.Filename);
                    }
                }
            }

            String conversationPrefix = this.GetConversationPrefix();
            if (conversationPrefix == String.Empty)
            {
                // this is the first time this conversation has been automated so determine what prefix to use
                conversationPrefix = this.Mission.GetNextConversationPrefix();
                if (conversationPrefix == String.Empty)
                {
                    conversationPrefix = "AA";
                }
            }

            String linePostfix = String.Empty;
            String randomLinePostfix = String.Empty;
            GetNextLinePostfixes(out linePostfix, out randomLinePostfix);

            foreach (Line line in linesToGenerate)
            {
                string newFilename = String.Empty;
                string actualNewFilename = String.Empty;
                newFilename = this.Mission.MissionId + "_" + conversationPrefix + linePostfix;
                actualNewFilename = newFilename + "_01";

                while (allFilenames.Contains(newFilename) || allFilenames.Contains(actualNewFilename))
                {
                    linePostfix = MissionDialogue.IncrementString(linePostfix);
                    newFilename = this.Mission.MissionId + "_" + conversationPrefix + linePostfix;
                    actualNewFilename = newFilename + "_01";
                }

                line.Filename = newFilename;
                line.ManualFilename = false;

                linePostfix = MissionDialogue.IncrementString(linePostfix);
                randomLinePostfix = (int.Parse(randomLinePostfix) + 1).ToString();
                if (randomLinePostfix.Length == 1)
                {
                    randomLinePostfix = "0" + randomLinePostfix[0];
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="linesToGenerate">
        /// 
        /// </param>
        private void GenerateRandomFilenames(Collection<Line> linesToGenerate)
        {
            HashSet<string> allFilenames = new HashSet<string>(this.Mission.DeletedFilenames);
            foreach (Conversation conversation in this.Mission.Conversations)
            {
                foreach (Line line in conversation.Lines)
                {
                    if (!string.IsNullOrWhiteSpace(line.Filename))
                    {
                        allFilenames.Add(line.Filename);
                    }
                }
            }

            foreach (Line line in this.Lines)
            {
                if (!string.IsNullOrEmpty(line.Filename))
                {
                    foreach (Line lineToGenerate in linesToGenerate)
                    {
                        lineToGenerate.Filename = line.Filename;
                    }

                    return;
                }
            }

            string conversationPrefix = this.Mission.GetNextConversationPrefix();
            if (conversationPrefix == String.Empty)
            {
                conversationPrefix = "AA";
            }

            string linePostfix = "AA";
            string newFilename = String.Empty;
            string actualNewFilename = String.Empty;
            newFilename = this.Mission.MissionId + "_" + conversationPrefix + linePostfix;
            actualNewFilename = newFilename + "_01";

            while (allFilenames.Contains(newFilename) || allFilenames.Contains(actualNewFilename))
            {
                linePostfix = MissionDialogue.IncrementString(linePostfix);
                newFilename = this.Mission.MissionId + "_" + conversationPrefix + linePostfix;
                actualNewFilename = newFilename + "_01";
            }

            foreach (Line line in linesToGenerate)
            {
                line.Filename = newFilename;
                line.ManualFilename = false;
            }
        }

        /// <summary>
        /// Creates the filenames for all of the lines belonging to this conversation
        /// </summary>
        private void GenerateFilenames(Collection<Line> linesToGenerate)
        {
            if (this.Random)
            {
                this.GenerateRandomFilenames(linesToGenerate);
            }
            else
            {
                this.GenerateNormalFilenames(linesToGenerate);
            }

            this.Locked = true;
        }

        /// <summary>
        /// Returns a collection of lines that belong to this conversation that
        /// need to have their filenames auto generated.
        /// </summary>
        /// <returns></returns>
        public int GetLinesThatNeedGenerating(out Collection<Line> generateLines, bool overwriteUnlocked)
        {
            generateLines = new Collection<Line>();

            foreach (Line line in this.Lines)
            {
                if (line.CharacterGuid == this.Mission.Configurations.SfxIdentifier)
                {
                    continue;
                }

                if (!string.IsNullOrEmpty(line.Filename))
                {
                    continue;
                }

                generateLines.Add(line);
            }

            return generateLines.Count;
        }

        /// <summary>
        /// Make sure that the given speaker listener value is valid (i.e is in the range [0-9,A-Z])
        /// if it's not valid return the character z
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private String ValidateSpeakerListener(String value)
        {
            String result = "z";
            if (value != null && value.Length == 1)
            {
                int integerValue = (int)value[0];
                if (integerValue <= (int)'9' && integerValue >= (int)'0')
                {
                    result = value;
                }
                else if (integerValue <= (int)'Z' && integerValue >= (int)'A')
                {
                    result = value;
                }
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        void LinesModified()
        {
            this.FilenameErrors = false;
            foreach (Line line in this.Lines)
            {
                if (!line.IsFilenameValid())
                {
                    this.FilenameErrors = true;
                    break;
                }

                if (this.Random)
                {
                    line.OnPropertyChanged("FilenameWithLineIndex");
                }
            }

            OnLineCharacterChanged();
        }

        internal void OnLineCharacterChanged()
        {
            List<Guid> currentUsedCharacters = new List<Guid>();
            foreach (Line line in this.Lines)
            {
                if (!currentUsedCharacters.Contains(line.CharacterGuid))
                    currentUsedCharacters.Add(line.CharacterGuid);
            }

            foreach (Line line in this.Lines)
            {
                line.OnLineCharacterChanged(currentUsedCharacters);
            }
        }

        private void OnCharactersChanged(object sender, EventArgs e)
        {
            List<Guid> currentUsedCharacters = new List<Guid>();
            foreach (Line line in this.Lines)
            {
                if (!currentUsedCharacters.Contains(line.CharacterGuid))
                    currentUsedCharacters.Add(line.CharacterGuid);
            }


            this.m_usedCharacters.Clear();
            foreach (Line line in this.Lines)
            {
                if (!this.m_usedCharacters.Contains(line.Character.Value))
                {
                    this.m_usedCharacters.Add(line.Character.Value);
                }

                if (line.availableCharacters == null)
                    continue;

                line.AvailableCharacters.Clear();
                foreach (KeyValuePair<Guid, string> character in this.Mission.Configurations.Characters)
                {
                    if (currentUsedCharacters.Contains(character.Key))
                    {
                        line.AvailableCharacters.Add(new CharacterValue(new KeyValuePair<Guid, string>(character.Key, character.Value), true));
                    }
                    else
                    {
                        line.AvailableCharacters.Add(new CharacterValue(new KeyValuePair<Guid, string>(character.Key, character.Value), false));
                    }
                }

                line.AvailableCharactersCollection.Refresh();
            }
        }
        #endregion // Private Function(s)

    } // Conversation
} // RSG.Model.Dialogue
