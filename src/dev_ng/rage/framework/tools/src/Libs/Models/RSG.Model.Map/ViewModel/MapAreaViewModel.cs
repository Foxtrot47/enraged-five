﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using RSG.Base.Collections;

namespace RSG.Model.Map.ViewModel
{
    /// <summary>
    /// The view model for the map area object used by the view
    /// </summary>
    public class MapAreaViewModel :
        RSG.Base.Editor.HierarchicalViewModelBase, IMapViewModelComponent, IMapViewModelContainer
    {
        #region Properties

        /// <summary>
        /// The model reference that this view model represents.
        /// </summary>
        public MapArea Model
        {
            get { return m_model; }
            set { m_model = value; }
        }
        private MapArea m_model;

        /// <summary>
        /// A string that represents its root in the map content tree
        /// </summary>
        public String TreeString
        {
            get { return m_treeString; }
            set { m_treeString = value; }
        }
        private String m_treeString;

        /// <summary>
        /// Returns a IEnumerable of just map conponents that aren't also containers.
        /// </summary>
        public IEnumerable<IMapViewModelComponent> ComponentChildren
        {
            get
            {
                foreach (IMapViewModelComponent component in this.Children.Where(c => c is IMapViewModelComponent == true && c is IMapViewModelContainer == false))
                {
                    yield return component;
                }
            }
        }

        /// <summary>
        /// Returns a IEnumerable of just map containers
        /// </summary>
        public IEnumerable<IMapViewModelContainer> ContainerChildren
        {
            get
            {
                foreach (IMapViewModelContainer container in this.Children.Where(c => c is IMapViewModelContainer == true))
                {
                    yield return container;
                }
            }
        }

        /// <summary>
        /// Returns a enumerable around the child of this level that are map sections
        /// </summary>
        public IEnumerable<IMapSectionViewModel> SectionChildren
        {
            get
            {
                foreach (IMapSectionViewModel section in this.Children.Where(c => c is IMapSectionViewModel == true))
                {
                    yield return section;
                }
            }
        }

        /// <summary>
        /// Returns the lower case name for this object
        /// </summary>
        public String Name
        {
            get { return Model.Name.ToLower(); }
        }

        /// <summary>
        /// The user data that can be attached to this view model
        /// </summary>
        public UserData ViewModelUserData
        {
            get { return m_viewModelUserData; }
            set { m_viewModelUserData = value; }
        }
        private UserData m_viewModelUserData = new UserData();

        public Boolean InitialisedChildren { get; set; }

        public Boolean InitialisingChildren
        {
            get { return m_initialisingChildren; }
            set
            {
                SetPropertyValue(value, () => this.InitialisingChildren,
                    new PropertySetDelegate(delegate(Object newValue) { m_initialisingChildren = (Boolean)newValue; }));
            }
        }
        private Boolean m_initialisingChildren = false;

        #endregion // Properties

        #region Constructor

        /// <summary>
        /// Default Constructor
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="model"></param>
        public MapAreaViewModel(IViewModel parent, MapArea model)
        {
            this.Parent = parent;
            this.Model = model;

            if (model != null)
            {
                foreach (IMapComponent mapComponent in model.MapComponents.Values)
                {
                    if (mapComponent is MapArea)
                    {
                        MapAreaViewModel newArea = new MapAreaViewModel(this, mapComponent as MapArea);
                        this.Children.Add(newArea);
                    }
                    if (mapComponent is MapSection)
                    {
                        IMapSectionViewModel newSection = MapSectionViewModelFactory.Create(this, mapComponent as MapSection);// new MapSectionViewModel(this, mapComponent as MapSection);
                        this.Children.Add(newSection);
                    }
                }

                this.TreeString = model.Name;
                IViewModel viewParent = this.Parent;
                LevelViewModel levelParent = viewParent as LevelViewModel;
                if (levelParent != null)
                {
                    this.TreeString += " - All";
                }
                while (viewParent != null && levelParent == null)
                {
                    if (viewParent is MapAreaViewModel)
                    {
                        this.TreeString = this.TreeString.Insert(0, (viewParent as MapAreaViewModel).Model.Name + " / ");
                    }
                    viewParent = (viewParent as HierarchicalViewModelBase).Parent;
                    levelParent = viewParent as LevelViewModel;
                }
            }
            else
            {
                this.TreeString += "None";
            }
        }

        #endregion // Constructor

        #region Functions

        /// <summary>
        /// Returns the map section with the given name (or null if it doesn't
        /// exist).
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public MapSectionViewModel GetMapSectionByName(String name, Boolean recursive)
        {
            foreach (MapSectionViewModel section in this.Children.Where(c => c is MapSectionViewModel == true))
            {
                if (String.Compare(section.Model.Name, name, true) == 0)
                {
                    return section;
                }
            }

            if (recursive == true)
            {
                foreach (MapAreaViewModel area in this.Children.Where(c => c is MapAreaViewModel == true))
                {
                    MapSectionViewModel section = area.GetMapSectionByName(name, recursive);
                    if (section != null)
                    {
                        return section;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Returns true if this container contains map sections that are non generic and
        /// have definitions
        /// </summary>
        /// <returns></returns>
        public Boolean ContainsNonGenericWithDefinitionChildren(Boolean recusive)
        {
            foreach (IMapSectionViewModel section in this.SectionChildren)
            {
                if (section.Model.ExportInstances == true && section.Model.ExportDefinitions == true)
                {
                    return true;
                }
            }

            if (recusive == true)
            {
                foreach (IMapViewModelContainer containerChild in this.ContainerChildren)
                {
                    if (containerChild.ContainsNonGenericWithDefinitionChildren(true))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public override IViewModel GetChildWithString(String name)
        {
            foreach (IMapViewModelComponent child in this.Children)
            {
                if (String.Compare(child.Name, name, true) == 0)
                {
                    return child as IViewModel;
                }
            }
            return null;
        }

        #endregion // Functions

    }
}
