﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Collections;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using RSG.Model.Map;

namespace RSG.Model.Map.ViewModel
{
    /// <summary>
    /// 
    /// </summary>
    public class InteriorSectionViewModel :
        RSG.Base.Editor.HierarchicalViewModelBase, IMapViewModelComponent, IMapSectionViewModel
    {
        #region Properties

        /// <summary>
        /// The name of the map section
        /// </summary>
        public String Name
        {
            get { return Model.Name.ToLower(); }
        }

        /// <summary>
        /// The model reference that this view model represents.
        /// </summary>
        public MapSection Model
        {
            get { return m_model; }
            set { m_model = value; }
        }
        private MapSection m_model;

        /// <summary>
        /// The level that this section belongs to.
        /// </summary>
        public LevelViewModel Level
        {
            get;
            private set;
        }

        /// <summary>
        /// Returns true iff this map section represents a non generic map section
        /// that also exports definitions. (i.e exports a ipl file and a ide file).
        /// </summary>
        public Boolean NonGenericWithDefinitions
        {
            get;
            private set;
        }

        /// <summary>
        /// The user data that can be attached to this view model
        /// </summary>
        public UserData ViewModelUserData
        {
            get { return m_viewModelUserData; }
            set { m_viewModelUserData = value; }
        }
        private UserData m_viewModelUserData = new UserData();
        
        public ObservableCollection<MapMiloViewModel> Definitions
        {
            get { return m_definitions; }
            set
            {
                SetPropertyValue(value, () => this.Definitions,
                    new PropertySetDelegate(delegate(Object newValue) { m_definitions = (ObservableCollection<MapMiloViewModel>)newValue; }));
            }
        }
        private ObservableCollection<MapMiloViewModel> m_definitions;

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Default Constructor
        /// </summary>
        public InteriorSectionViewModel(IViewModel parent, MapSection model)
        {
            this.Parent = parent;
            this.Model = model;
            this.Definitions = new ObservableCollection<MapMiloViewModel>();
            this.Definitions.Add(new MapMiloViewModel());
        }

        #endregion // Constructors

        #region Override Functions

        protected override void OnFirstExpanded()
        {
            if (this.Model.Definitions != null)
            {
                ObservableCollection<MapMiloViewModel> definitions = new ObservableCollection<MapMiloViewModel>();
                foreach (IMapDefinition definition in this.Model.Definitions)
                {
                    if (definition is MapMilo)
                    {
                        definitions.Add(new MapMiloViewModel(this, this.Model.Definitions[0] as MapMilo));
                    }
                }
                this.Definitions = definitions;
            }
        }

        #endregion // Override Functions
    } // InteriorSectionViewModel
} // Workbench.AddIn.MapBrowser.MapViewModel
