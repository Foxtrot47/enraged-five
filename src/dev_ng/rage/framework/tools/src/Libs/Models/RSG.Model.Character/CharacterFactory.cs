﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.Base.ConfigParser;
using System.IO;
using RSG.Base.Extensions;
using RSG.Statistics.Client.ServiceClients;
using RSG.Statistics.Common.Dto.GameAssets;
using System.Diagnostics;
using RSG.Base.Logging;
using RSG.Base.Configuration;

namespace RSG.Model.Character
{
    /// <summary>
    /// 
    /// </summary>
    public class CharacterFactory : ICharacterFactory
    {
        #region Member Data
        /// <summary>
        /// Branch we are working off of.
        /// </summary>
        private IBranch _branch;

        /// <summary>
        /// Weapon factory log.
        /// </summary>
        private static ILog _log = LogFactory.CreateUniversalLog("CharacterFactory");
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        [Obsolete("Use the IBranch version of the constructor.")]
        public CharacterFactory()
            : this(ConfigFactory.CreateConfig())
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="config"></param>
        [Obsolete("Use the IBranch version of the constructor.")]
        public CharacterFactory(IConfig config)
            : this(config.CoreProject.DefaultBranch)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branch"></param>
        [Obsolete("Use the static controller method instead.")]
        public CharacterFactory(IBranch branch)
        {
            _branch = branch;
        }
        #endregion // Constructor(s)

        #region ICharacterFactory Implementation
        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="buildIdentifier"></param>
        /// <returns></returns>
        [Obsolete("Use the static method instead.")]
        public ICharacterCollection CreateCollection(DataSource source, String buildIdentifier = null)
        {
            ICharacterCollection collection = null;

            if (source == DataSource.SceneXml)
            {
                _log.Profile("Character collection creation - export data.");
                collection = CreateCollectionFromExportData(_branch);
                _log.ProfileEnd();
            }
            else if (source == DataSource.Database)
            {
                _log.Profile("Character collection creation - database.");
                collection = CreateCollectionFromDatabase(buildIdentifier);
                _log.ProfileEnd();
            }
            else
            {
                Debug.Assert(false, String.Format("Unknown data source '{0}' specified while creating a character collection.", source.ToString()));
                throw new ArgumentException(String.Format("Unknown data source '{0}' specified while creating a character collection.", source.ToString()));
            }

            return collection;
        }
        #endregion // ICharacterFactory Implementation

        #region Static Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="buildIdentifier"></param>
        /// <returns></returns>
        public static ICharacterCollection CreateCollection(DataSource source, IBranch branch = null, String buildIdentifier = null)
        {
            ICharacterCollection collection = null;

            if (source == DataSource.SceneXml)
            {
                Debug.Assert(branch != null, "Branch must be supplied when creating a weapon collection from SceneXml.");
                if (branch == null)
                {
                    throw new ArgumentNullException("branch", "Branch must be supplied when creating a weapon collection from SceneXml.");
                }

                _log.Profile("Character collection creation - export data.");
                collection = CreateCollectionFromExportData(branch);
                _log.ProfileEnd();
            }
            else if (source == DataSource.Database)
            {
                Debug.Assert(String.IsNullOrEmpty(buildIdentifier), "Build identifier must be supplied when creating a weapon collection from SceneXml.");
                if (branch == null)
                {
                    throw new ArgumentNullException("buildIdentifier", "Build identifier must be supplied when creating a weapon collection from SceneXml.");
                }

                _log.Profile("Character collection creation - database.");
                collection = CreateCollectionFromDatabase(buildIdentifier);
                _log.ProfileEnd();
            }
            else
            {
                Debug.Assert(false, String.Format("Unknown data source '{0}' specified while creating a character collection.", source.ToString()));
                throw new ArgumentException(String.Format("Unknown data source '{0}' specified while creating a character collection.", source.ToString()));
            }

            return collection;
        }
        #endregion // Static Controller Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private static ICharacterCollection CreateCollectionFromExportData(IBranch branch)
        {
            String rootPath = Path.Combine(branch.Assets, "characters");
            LocalCharacterCollection characters = new LocalCharacterCollection(rootPath);

            if (Directory.Exists(rootPath))
            {
                try
                {
                    // Create a mapping of directory names to CharacterCategory and another to category collections
                    IDictionary<string, CharacterCategory> categoryMap = new Dictionary<string, CharacterCategory>();
                    IDictionary<CharacterCategory, ICharacterCategoryCollection> collectionMap = new Dictionary<CharacterCategory, ICharacterCategoryCollection>();

                    foreach (CharacterCategory category in Enum.GetValues(typeof(CharacterCategory)))
                    {
                        categoryMap.Add(category.GetDescriptionValue(), category);
                        collectionMap.Add(category, new CharacterCategoryCollection(category));
                    }

                    // Loop over all the directories
                    String[] directories = Directory.GetDirectories(rootPath);
                    foreach (String dir in directories)
                    {
                        CharacterCategory category = CharacterCategory.Unknown;
                        if (categoryMap.ContainsKey(Path.GetFileName(dir)))
                        {
                            category = categoryMap[Path.GetFileName(dir)];
                        }
                        ICharacterCategoryCollection categoryCollection = collectionMap[category];

                        String[] xmlFiles = Directory.GetFiles(dir, "*.xml");
                        foreach (String xmlFile in xmlFiles)
                        {
                            categoryCollection.Add(new LocalCharacter(Path.GetFileNameWithoutExtension(xmlFile), category, characters));
                        }
                    }

                    characters.AddRange(collectionMap.Values.Where(item => item.Count > 0));
                }
                catch (System.Exception ex)
                {
                    _log.ToolException(ex, "Unhandled exception occurred while creating the characters collection for branch {0}.", branch.Name);
                }
            }
            else
            {
                _log.Warning("Character's asset directory {0} doesn't exist.", rootPath);
            }
            
            return characters;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <returns></returns>
        private static ICharacterCollection CreateCollectionFromDatabase(string buildIdentifier)
        {
            ICharacterCollection characters = null;

            try
            {
                using (BuildClient client = new BuildClient())
                {
                    // If no build identifier was supplied, use the latest build.
                    if (buildIdentifier == null)
                    {
                        BuildDto dto = client.GetLatest();
                        if (dto != null)
                        {
                            buildIdentifier = dto.Identifier;
                        }
                    }

                    // Check that the identifier is set.
                    if (buildIdentifier != null)
                    {
                        characters = new SingleBuildCharacterCollection(buildIdentifier);

                        // Create a mapping of character category or collections
                        IDictionary<CharacterCategory, ICharacterCategoryCollection> collectionMap = new Dictionary<CharacterCategory, ICharacterCategoryCollection>();
                        foreach (CharacterCategory category in Enum.GetValues(typeof(CharacterCategory)))
                        {
                            collectionMap.Add(category, new CharacterCategoryCollection(category));
                        }

                        foreach (CharacterDto dto in client.GetAllCharactersForBuild(buildIdentifier).Items)
                        {
                            CharacterCategory category;
                            if (!CharacterCategory.TryParse(dto.Category, out category))
                            {
                                category = CharacterCategory.Unknown;
                            }

                            ICharacterCategoryCollection categoryCollection = collectionMap[category];
                            categoryCollection.Add(new DbCharacter(dto.Name, category, characters));
                        }

                        characters.AddRange(collectionMap.Values.Where(item => item.Count > 0));
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.ToolException(ex, "Unhandled exception occurred while creating a character collection from the database.");
            }

            return characters;
        }
        #endregion // Private Methods
    } // CharacterFactory
}
