﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Collections;

namespace RSG.Model.Common.Map
{
    /// <summary>
    /// A map hierarchy node that contains other nodes
    /// </summary>
    public interface IMapArea : IMapNode
    {
        /// <summary>
        /// Collection of child map nodes
        /// </summary>
        ObservableCollection<IMapNode> ChildNodes { get; }

        /// <summary>
        /// Returns all sections in this map hierarchy
        /// </summary>
        IEnumerable<IMapSection> AllDescendentSections { get; }

        /// <summary>
        /// Map area export data path.
        /// </summary>
        String ExportDataPath { get; }
        
        /// <summary>
        /// Map area processed data path.
        /// </summary>
        String ProcessedDataPath { get; }
    } // IMapArea
}
