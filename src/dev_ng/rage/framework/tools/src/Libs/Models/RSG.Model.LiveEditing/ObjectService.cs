﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;

namespace RSG.Model.LiveEditing
{
    /// <summary>
    /// Corresponds to a parsable object as exposed by the game's REST service
    /// </summary>
    public class ObjectService : AssetBase, IObjectService
    {
        #region Properties
        /// <summary>
        /// The Uri to get information about this item
        /// </summary>
        public Uri Url
        {
            get;
            set;
        }

        /// <summary>
        /// The file path for this particular service
        /// </summary>
        public string AssociatedFilePath
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="url"></param>
        public ObjectService(string name, Uri url, string filePath)
            : base(name)
        {
            Url = url;
            AssociatedFilePath = filePath;
        }
        #endregion // Constructor(s)

        #region Object Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return String.Format("{0}@{1}", Name, Url);
        }
        #endregion // Object Overrides
    } // ObjectService
}
