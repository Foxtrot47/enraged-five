﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using RSG.Model.Report.Reports.GameStatsReports.Model;
using System.IO;
using System.Windows.Controls.DataVisualization.Charting;
using System.Windows.Media;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using System.Windows;

namespace RSG.Model.Report.Reports.GameStatsReports.Tests
{
    /// <summary>
    /// 
    /// </summary>
    public class GpuSmokeTest : SmokeTestBase
    {
        #region Constants
        private const string c_name = "Gpu";
        private const int c_lineGraphWidth = 1000;
        private const int c_lineGraphHeight = 500;
        private const int c_pieGraphWidth = 400;
        private const int c_pieGraphHeight = 300;
        #endregion // Constants

        #region Private Classes
        /// <summary>
        /// Contains information about a single cpu stat
        /// </summary>
        private class GpuInfo : IComparable<GpuInfo>
        {
            public GpuResult Result { get; private set; }
            public DateTime Timestamp { get; private set; }

            public GpuInfo(GpuResult result, DateTime time)
            {
                Result = result;
                Timestamp = time;
            }

            #region IComparable<CpuInfo> Interface
            /// <summary>
            /// Compare this ModificationInfo to another.
            /// </summary>
            /// <param name="other"></param>
            /// <returns></returns>
            public int CompareTo(GpuInfo other)
            {
                if (other == null)
                {
                    return 1;
                }

                return (Result.Time.CompareTo(other.Result.Time));
            }
            #endregion // IComparable<IVehicle> Interface
        }

        /// <summary>
        /// 
        /// </summary>
        private class GpuMetricKey : IEquatable<GpuMetricKey>
        {
            public string TestName { get; private set; }
            public string MetricName { get; private set; }

            public GpuMetricKey(string testName, string metricName)
            {
                TestName = testName;
                MetricName = metricName;
            }

            public override string ToString()
            {
                return String.Format("{0} {1}", TestName, MetricName);
            }

            public override bool Equals(object obj)
            {
                if (obj == null)
                {
                    return base.Equals(obj);
                }

                return ((obj is GpuMetricKey) && Equals(obj as GpuMetricKey));
            }

            public override int GetHashCode()
            {
                return (TestName.GetHashCode() ^ MetricName.GetHashCode());
            }

            public bool Equals(GpuMetricKey other)
            {
                if (other == null)
                {
                    return false;
                }

                return (TestName == other.TestName && MetricName == other.MetricName);
            }
        }
        #endregion // Private Methods

        #region Properties
        /// <summary>
        /// Mapping of test name -> [gpu info list] for the latest build
        /// </summary>
        private IDictionary<string, IList<GpuInfo>> LatestBuildStats
        {
            get;
            set;
        }

        /// <summary>
        /// 9 Highest offenders in the latest stats + other group (if needed)
        /// </summary>
        private IDictionary<string, IList<GpuInfo>> GroupedLatestBuildStats
        {
            get;
            set;
        }

        /// <summary>
        /// Fps info grouped by test name
        /// </summary>
        private IDictionary<GpuMetricKey, IList<GpuInfo>> HistoricalStats
        {
            get;
            set;
        }

        /// <summary>
        /// Fps info grouped by test name
        /// </summary>
        private IDictionary<GpuMetricKey, IList<GpuInfo>> GroupedHistoricalStats
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public GpuSmokeTest()
            : base(c_name)
        {
        }
        #endregion // Constructor(s)

        #region SmokeTestBase Methods
        /// <summary>
        /// Executes the particular test
        /// </summary>
        /// <param name="testSessions">List of test sessions</param>
        public override void RunTest(IList<TestSession> testSessions)
        {
            base.RunTest(testSessions);

            // Gather the stats we are interested in
            LatestBuildStats = GatherLatestBuildStats(testSessions.FirstOrDefault());
            HistoricalStats = GatherHistoricalStats(testSessions);

            // Create the grouped stats that we'll use for the graphs
            GroupedLatestBuildStats = GroupLatestsStats();
            GroupedHistoricalStats = GroupHistoricalStats(testSessions);

            // Run the tests on the historical data
            foreach (KeyValuePair<GpuMetricKey, IList<GpuInfo>> pair in HistoricalStats)
            {
                string testName = pair.Key.TestName;
                string metricName = pair.Key.MetricName;

                GpuInfo first = pair.Value.FirstOrDefault();
                GpuInfo next = null;
                if (pair.Value.Count > 1)
                {
                    next = pair.Value[1];
                }

                if (first != null && next != null && first.Result.Time != 0.0f && next.Result.Time != 0.0f)
                {
                    float averageGrowth = next.Result.Time / first.Result.Time;
                    if (averageGrowth < 0.95f)
                    {
                        Errors.Add(String.Format("{0} {1} {2} metric is slower by {3:0.##}%", testName, metricName, SmokeTestName.ToLower(), -(averageGrowth - 1.0) * 100));
                    }

                    if (averageGrowth > 1.05f)
                    {
                        Messages.Add(String.Format("{0} {1} {2} metric is faster by {3:0.##}%", testName, metricName, SmokeTestName.ToLower(), (averageGrowth - 1.0) * 100));
                    }
                }
            }
        }

        /// <summary>
        /// Generates test specific graphs for this smoke test
        /// </summary>
        /// <param name="test"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns>Null if the smoke test doesn't require a graph</returns>
        public override IList<GraphImage> GenerateGraphs(DateTime start, DateTime end)
        {
            IList<GraphImage> allGraphs = new List<GraphImage>();

            // Generate a couple of graphs for each  test
            foreach (KeyValuePair<string, IList<GpuInfo>> pair in GroupedLatestBuildStats)
            {
                string testName = pair.Key;

                // Create the pie graph
                Stream imageStream = GeneratePieGraph(testName, pair.Value);
                string graphName = String.Format("gpu_pie_{0}", testName.Replace("(", "").Replace(")", "").Replace(" ", ""));
                GraphImage image = new GraphImage(graphName, imageStream);

                if (!PerTestGraphs.ContainsKey(testName))
                {
                    PerTestGraphs.Add(testName, new List<GraphImage>());
                }

                PerTestGraphs[testName].Add(image);
                allGraphs.Add(image);
            }

            // Create the historical line graph
            foreach (var pair in GroupedHistoricalStats.GroupBy(pair => pair.Key.TestName))
            {
                string testName = pair.Key;

                // Create the line graph
                Stream imageStream = GenerateLineGraph(testName, pair);
                string graphName = String.Format("gpu_line_{0}", testName.Replace("(", "").Replace(")", "").Replace(" ", ""));
                GraphImage image = new GraphImage(graphName, imageStream);

                if (!PerTestGraphs.ContainsKey(testName))
                {
                    PerTestGraphs.Add(testName, new List<GraphImage>());
                }

                PerTestGraphs[testName].Add(image);
                allGraphs.Add(image);
            }

            return allGraphs;
        }

        /// <summary>
        /// Write this test's portion of the report
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="test">The name of the test for which we wish to write the report</param>
        /// <param name="includeHeader">Whether we should output a header for this report</param>
        /// <param name="tableResults"></param>
        public override void WriteReport(HtmlTextWriter writer, string test, bool includeHeader, uint tableResults, bool email)
        {
            base.WriteReport(writer, test, includeHeader, tableResults, email);
        }
        #endregion // SmokeTestBase Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        private IDictionary<string, IList<GpuInfo>> GatherLatestBuildStats(TestSession session)
        {
            IDictionary<string, IList<GpuInfo>> latestStats = new Dictionary<string, IList<GpuInfo>>();

            if (session != null)
            {
                ISet<string> encounteredTests = new HashSet<string>();

                foreach (TestResults test in session.TestResults)
                {
                    if (encounteredTests.Contains(test.Name))
                    {
                        continue;
                    }
                    encounteredTests.Add(test.Name);

                    foreach (GpuResult result in test.GpuResults)
                    {
                        if (!latestStats.ContainsKey(test.Name))
                        {
                            latestStats.Add(test.Name, new List<GpuInfo>());
                        }

                        latestStats[test.Name].Add(new GpuInfo(result, session.Timestamp));
                        ((List<GpuInfo>)latestStats[test.Name]).Sort();
                        ((List<GpuInfo>)latestStats[test.Name]).Reverse();
                    }
                }
            }

            return latestStats;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private IDictionary<string, IList<GpuInfo>> GroupLatestsStats()
        {
            IDictionary<string, IList<GpuInfo>> groupedStats = new Dictionary<string, IList<GpuInfo>>(LatestBuildStats);

            // Take the 9 worst offenders and add an "other" category for the rest
            IDictionary<string, List<GpuInfo>> keysToUpdate = new Dictionary<string, List<GpuInfo>>();

            foreach (KeyValuePair<string, IList<GpuInfo>> pair in groupedStats)
            {
                if (pair.Value.Count > 10)
                {
                    // Add other category if we have more than 10 items
                    List<GpuInfo> newList = pair.Value.Take(9).ToList();
                    IEnumerable<GpuInfo> otherEnumerable = pair.Value.Skip(9);

                    if (otherEnumerable.Count() > 0)
                    {
                        float otherTime = otherEnumerable.Sum(item => item.Result.Time);
                        newList.Add(new GpuInfo(new GpuResult("Other", otherTime), DateTime.Now));
                    }

                    keysToUpdate.Add(pair.Key, newList);
                }
            }

            foreach (KeyValuePair<string, List<GpuInfo>> pair in keysToUpdate)
            {
                groupedStats[pair.Key] = pair.Value;
            }

            return groupedStats;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="testSessions"></param>
        /// <returns></returns>
        private IDictionary<GpuMetricKey, IList<GpuInfo>> GatherHistoricalStats(IList<TestSession> testSessions)
        {
            IDictionary<GpuMetricKey, IList<GpuInfo>> historicalStats = new Dictionary<GpuMetricKey, IList<GpuInfo>>();

            foreach (TestSession session in testSessions)
            {
                ISet<string> encounteredTests = new HashSet<string>();

                foreach (TestResults test in session.TestResults)
                {
                    if (encounteredTests.Contains(test.Name))
                    {
                        continue;
                    }
                    encounteredTests.Add(test.Name);

                    foreach (GpuResult result in test.GpuResults)
                    {
                        // Ignore stats that aren't present in the latest build stats
                        if (LatestBuildStats.ContainsKey(test.Name))
                        {
                            if (LatestBuildStats[test.Name].Where(info => info.Result.Name == result.Name).Any())
                            {
                                GpuMetricKey key = new GpuMetricKey(test.Name, result.Name);
                                if (!historicalStats.ContainsKey(key))
                                {
                                    historicalStats.Add(key, new List<GpuInfo>());
                                }
                                historicalStats[key].Add(new GpuInfo(result, session.Timestamp));
                            }
                        }
                    }
                }
            }

            return historicalStats;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="testSessions"></param>
        /// <returns></returns>
        private IDictionary<GpuMetricKey, IList<GpuInfo>> GroupHistoricalStats(IList<TestSession> testSessions)
        {
            IDictionary<GpuMetricKey, IList<GpuInfo>> groupedStats = new Dictionary<GpuMetricKey, IList<GpuInfo>>();

            foreach (TestSession session in testSessions)
            {
                foreach (TestResults test in session.TestResults)
                {
                    foreach (GpuResult result in test.GpuResults)
                    {
                        if (GroupedLatestBuildStats.ContainsKey(test.Name))
                        {
                            if (GroupedLatestBuildStats[test.Name].Where(info => info.Result.Name == result.Name).Any())
                            {
                                GpuMetricKey key = new GpuMetricKey(test.Name, result.Name);
                                if (!groupedStats.ContainsKey(key))
                                {
                                    groupedStats.Add(key, new List<GpuInfo>());
                                }
                                groupedStats[key].Add(new GpuInfo(result, session.Timestamp));
                            }
                            else
                            {
                                GpuMetricKey key = new GpuMetricKey(test.Name, "Other");
                                if (!groupedStats.ContainsKey(key))
                                {
                                    groupedStats.Add(key, new List<GpuInfo>());
                                }

                                if (!groupedStats[key].Where(info => info.Timestamp == session.Timestamp).Any())
                                {
                                    groupedStats[key].Add(new GpuInfo(new GpuResult("Other", 0.0f), session.Timestamp));
                                }

                                GpuInfo otherInfo = groupedStats[key].Where(info => info.Timestamp == session.Timestamp).First();
                                otherInfo.Result.Time += result.Time;
                            }
                        }
                        else
                        {
                            GpuMetricKey key = new GpuMetricKey(test.Name, result.Name);
                            if (!groupedStats.ContainsKey(key))
                            {
                                groupedStats.Add(key, new List<GpuInfo>());
                            }
                            groupedStats[key].Add(new GpuInfo(result, session.Timestamp));
                        }
                    }
                }
            }

            return groupedStats;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="testName"></param>
        /// <param name="stats"></param>
        /// <returns></returns>
        private Stream GeneratePieGraph(string testName, IList<GpuInfo> stats)
        {
            Chart chart = new Chart();
            chart.BeginInit();
            chart.Width = c_pieGraphWidth;
            chart.Height = c_pieGraphHeight;
            chart.Title = String.Format("Latest Gpu Stats Breakdown for '{0}'", testName);
            chart.LegendTitle = "Individual Metrics";
            chart.EndInit();

            RenderTargetBitmap renderTarget = new RenderTargetBitmap((int)chart.Width, (int)chart.Height, 96d, 96d, PixelFormats.Default);

            PieSeries pieSeries = new PieSeries
            {
                ItemsSource = stats,
                IndependentValueBinding = new Binding("Result.Name"),
                DependentValueBinding = new Binding("Result.Time")
            };
            chart.Series.Add(pieSeries);

            chart.Measure(new Size(chart.Width, chart.Height));
            chart.Arrange(new Rect(new Size(chart.Width, chart.Height)));
            chart.UpdateLayout();

            renderTarget.Render(chart);

            // Save out the chart to a png file
            PngBitmapEncoder png = new PngBitmapEncoder();
            png.Frames.Add(BitmapFrame.Create(renderTarget));
            MemoryStream stream = new MemoryStream();
            png.Save(stream);
            stream.Position = 0;
            return stream;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="testName"></param>
        /// <param name="setName"></param>
        /// <param name="stats"></param>
        /// <returns></returns>
        private Stream GenerateLineGraph(string testName, IEnumerable<KeyValuePair<GpuMetricKey, IList<GpuInfo>>> stats)
        {
            Chart chart = new Chart();
            chart.BeginInit();
            chart.Width = c_lineGraphWidth;
            chart.Height = c_lineGraphHeight;
            chart.Title = String.Format("Historical Gpu Stats for '{0}'", testName);
            chart.LegendTitle = "Individual Metrics";
            chart.EndInit();

            RenderTargetBitmap renderTarget = new RenderTargetBitmap((int)chart.Width, (int)chart.Height, 96d, 96d, PixelFormats.Default);

            foreach (KeyValuePair<GpuMetricKey, IList<GpuInfo>> pair in stats)
            {
                LineSeries lineSeries = new LineSeries
                {
                    Title = pair.Key.MetricName,
                    ItemsSource = pair.Value,
                    IndependentValueBinding = new Binding("Timestamp"),
                    DependentValueBinding = new Binding("Result.Time")
                };
                chart.Series.Add(lineSeries);
            }

            DateTimeAxis xAxis = new DateTimeAxis
            {
                Orientation = AxisOrientation.X,
                Interval = 1,
                IntervalType = DateTimeIntervalType.Days,
                Title = "Date"
            };
            chart.Axes.Add(xAxis);

            LinearAxis yAxis = new LinearAxis
            {
                Orientation = AxisOrientation.Y,
                Title = "Milliseconds",
                ShowGridLines = true
            };
            chart.Axes.Add(yAxis);

            chart.Measure(new Size(chart.Width, chart.Height));
            chart.Arrange(new Rect(new Size(chart.Width, chart.Height)));
            chart.UpdateLayout();

            renderTarget.Render(chart);

            // Save out the chart to a png file
            PngBitmapEncoder png = new PngBitmapEncoder();
            png.Frames.Add(BitmapFrame.Create(renderTarget));
            MemoryStream stream = new MemoryStream();
            png.Save(stream);
            stream.Position = 0;
            return stream;
        }
        #endregion // Private Methods
    } // GpuSmokeTest
}
