﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Map;
using RSG.Base.Editor;

namespace RSG.Model.Map.ViewModel
{
    /// <summary>
    /// 
    /// </summary>
    public static class MapSectionViewModelFactory
    {
        /// <summary>
        /// 
        /// </summary>
        public static IMapSectionViewModel Create(IViewModel parent, MapSection section)
        {
            if (section.SectionType == SectionTypes.PROPS)
            {
                return (new PropSectionViewModel(parent, section));
            }
            else if (section.SectionType == SectionTypes.INTERIORS)
            {
                return (new InteriorSectionViewModel(parent, section));
            }
            else if ((section.SectionType == SectionTypes.PROPGROUP))
            {
                return (new PropGroupSectionViewModel(parent, section));
            }
            else
            {
                return (new MapSectionViewModel(parent, section));
            }
        }
    }
}
