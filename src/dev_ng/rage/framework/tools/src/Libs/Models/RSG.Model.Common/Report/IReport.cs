﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common.Report
{
    /// <summary>
    /// Report that can be invoked to generate results.
    /// </summary>
    public interface IReport
    {
        #region Properties
        /// <summary>
        /// Report name.
        /// </summary>
        String Name { get; }
        #endregion // Properties

        #region Events
        /// <summary>
        /// Event that can be fired to notify of report generation progress.
        /// </summary>
        event ReportProgressEventHandler ProgressChanged;
        #endregion // Events

        #region Methods
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        /// <returns></returns>
        object Generate(IReportContext context);
        #endregion // Methods
    } // IReportTemplate


    /// <summary>
    /// Delegate that is used to set a property to a new value.
    /// </summary>
    /// <param name="value"></param>
    public delegate void ReportProgressEventHandler(object sender, ReportProgressEventArgs e);


    /// <summary>
    /// Report progress arguments.
    /// </summary>
    public class ReportProgressEventArgs : EventArgs
    {
        /// <summary>
        /// Results of the task.
        /// </summary>
        public float Progress { get; private set; }

        /// <summary>
        /// Return value from the task (optional).
        /// </summary>
        public String Message { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public ReportProgressEventArgs(float progress, String message)
            : base()
        {
            Progress = progress;
            Message = message;
        }
    } // TaskCompletionArgs
}
