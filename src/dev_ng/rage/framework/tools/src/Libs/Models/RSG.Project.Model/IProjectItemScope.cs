﻿//---------------------------------------------------------------------------------------------
// <copyright file="IProjectItemScope.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.Model
{
    /// <summary>
    /// When implemented defines a scope root for project items where the containing items have
    /// to have a unique include value. Also metadata for the items are resolved at this level.
    /// </summary>
    public interface IProjectItemScope
    {
        #region Properties
        /// <summary>
        /// Gets the root directory for this scope. The root directory is never null; in-memory
        /// loads use the current directory at the time of the load.
        /// </summary>
        string DirectoryPath { get; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Adds a new project item to this scope with the specified type and include value.
        /// </summary>
        /// <param name="type">
        /// The type of the new item.
        /// </param>
        /// <param name="include">
        /// The include value for the new item.
        /// </param>
        /// <returns>
        /// The new instance that has been created and added to this scope.
        /// </returns>
        ProjectItem AddNewProjectItem(string type, string include);

        /// <summary>
        /// Adds the specified project item to this scope.
        /// </summary>
        /// <param name="item">
        /// The item to add to this scope.
        /// </param>
        void AddProjectItem(ProjectItem item);

        /// <summary>
        /// Attempts to find the project item contained within this scope that has the
        /// specified include value.
        /// </summary>
        /// <param name="include">
        /// The include value to search for.
        /// </param>
        /// <returns>
        /// The project item contained within this scope that has the specified include value
        /// if found; otherwise, null.
        /// </returns>
        ProjectItem Find(string include);

        /// <summary>
        /// Attempts to find the project item contained within this scope that has the
        /// specified filename value.
        /// </summary>
        /// <param name="filename">
        /// The filename value to search for.
        /// </param>
        /// <returns>
        /// The project item contained within this scope that has the specified filename value
        /// if found; otherwise, null.
        /// </returns>
        ProjectItem FindFilename(string filename);

        /// <summary>
        /// Retrieves a unique version of the specified string based on the current contents
        /// of this scope. The string will become unique by adding a numerical value to the
        /// end and incrementing it until uniqueness is found.
        /// </summary>
        /// <param name="baseFilename">
        /// The base filename that needs to be unique.
        /// </param>
        /// <returns>
        /// A unique version of the specified filename.
        /// </returns>
        string GetUniqueFilename(string baseFilename);

        /// <summary>
        /// Retrieves a unique version of the specified string based on the current contents
        /// of this scope. The string will become unique by adding a numerical value to the
        /// end and incrementing it until uniqueness is found.
        /// </summary>
        /// <param name="baseInclude">
        /// The base include path that needs to be unique.
        /// </param>
        /// <returns>
        /// A unique version of the specified string include.
        /// </returns>
        string GetUniqueInclude(string baseInclude);

        /// <summary>
        /// Removes the first occurrence of the specified item from this scope.
        /// </summary>
        /// <param name="item">
        /// The item to remove.
        /// </param>
        void RemoveProjectItem(ProjectItem item);
        #endregion Methods
    } // RSG.Project.Model.IProjectItemScope {Interface}
} // RSG.Project.Model {Namespace}
