﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Map.SceneOverride
{
    /// <summary>
    /// A class representing an attribute override item as serialised by the game
    /// </summary>
    public class RuntimeAttributeOverride
    {
        public RuntimeAttributeOverride(uint hash, uint guid, 
                                        float posX, float posY, float posZ,
                                        String imapName, String modelName,
                                        bool dontCastShadows, bool dontRenderInShadows,
                                        bool dontRenderInReflections, bool onlyRenderInReflections,
                                        bool dontRenderInWaterReflections, bool onlyRenderInWaterReflections,
                                        bool dontRenderInMirrorReflections, bool onlyRenderInMirrorReflections, 
                                        bool streamingPriorityLow, int priority)
        {
            Guid = guid;
            Hash = hash;
            PosX = posX;
            PosY = posY;
            PosZ = posZ;
            IMAPName = imapName;
            ModelName = modelName;
            DontCastShadows = dontCastShadows;
            DontRenderInShadows = dontRenderInShadows;
            DontRenderInReflections = dontRenderInReflections;
            OnlyRenderInReflections = onlyRenderInReflections;
            DontRenderInWaterReflections = dontRenderInWaterReflections;
            OnlyRenderInWaterReflections = onlyRenderInWaterReflections;
            DontRenderInMirrorReflections = dontRenderInMirrorReflections;
            OnlyRenderInMirrorReflections = onlyRenderInMirrorReflections;
            StreamingPriorityLow = streamingPriorityLow;
            Priority = priority;
        }

        public uint Guid { get; private set; }
        public uint Hash { get; private set; }
        public float PosX { get; private set; }
        public float PosY { get; private set; }
        public float PosZ { get; private set; }
        public String IMAPName { get; private set; }
        public String ModelName { get; private set; }
        public bool DontCastShadows { get; private set; }
        public bool DontRenderInShadows { get; private set; }
        public bool DontRenderInReflections { get; private set; }
        public bool OnlyRenderInReflections { get; private set; }
        public bool DontRenderInWaterReflections { get; private set; }
        public bool OnlyRenderInWaterReflections { get; private set; }
        public bool DontRenderInMirrorReflections { get; private set; }
        public bool OnlyRenderInMirrorReflections { get; private set; }
        public bool StreamingPriorityLow { get; private set; }
        public int Priority { get; private set; }
    }
}
