﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.SceneXml;

namespace RSG.Model.Common.Map
{
    /// <summary>
    /// Interface for all archetypes
    /// </summary>
    public interface IArchetype : IAsset, IStreamableObject
    {
        #region Properties
        /// <summary>
        /// List of entities that reference this archetype
        /// </summary>
        ICollection<IEntity> Entities { get; }

        /// <summary>
        /// 
        /// </summary>
        float LodDistance { get; }
        #endregion // Properties
    } // IArchetype
}
