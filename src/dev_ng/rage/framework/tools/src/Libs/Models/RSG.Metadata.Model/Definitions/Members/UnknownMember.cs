﻿// --------------------------------------------------------------------------------------------
// <copyright file="UnknownMember.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions.Members
{
    using System;
    using System.IO;
    using System.Xml;
    using RSG.Editor;

    /// <summary>
    /// Represents a parCodeGen member whose type is unknown to the metadata library.
    /// </summary>
    public class UnknownMember : MemberBase
    {
        #region Fields
        /// <summary>
        /// The private field used to store the attribute names on the node used to create
        /// this member.
        /// </summary>
        private string[] _attributeNames;

        /// <summary>
        /// The private field used to store the attribute values on the node used to create
        /// this member.
        /// </summary>
        private string[] _attributeValues;

        /// <summary>
        /// The private field used for the xml inner text that was used to create this member.
        /// </summary>
        private string _innerXml;

        /// <summary>
        /// The private field used for the <see cref="XmlNodeName"/> property.
        /// </summary>
        private string _xmlName;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="UnknownMember"/> class to be one of
        /// the members of the specified structure.
        /// </summary>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        public UnknownMember(IStructure structure)
            : base(structure)
        {
            this._attributeNames = new string[0];
            this._attributeValues = new string[0];
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="UnknownMember"/> class as a copy of
        /// the specified instance.
        /// </summary>
        /// <param name="other">
        /// The instance to copy.
        /// </param>
        /// <param name="structure">
        /// The structure that this member is associated with.
        /// </param>
        public UnknownMember(UnknownMember other, IStructure structure)
            : base(other, structure)
        {
            this._innerXml = other._innerXml;
            this._xmlName = other._xmlName;
            this._attributeNames = other._attributeNames.Clone() as string[];
            this._attributeValues = other._attributeValues.Clone() as string[];
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="UnknownMember"/> class.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <param name="structure">
        /// The structure that this member is associated with.
        /// </param>
        public UnknownMember(XmlReader reader, IStructure structure)
            : base(reader, structure)
        {
            this._xmlName = reader.Name;
            if (reader.HasAttributes)
            {
                int count = reader.AttributeCount;
                this._attributeNames = new string[count];
                this._attributeValues = new string[count];
                bool foundAttribute = reader.MoveToFirstAttribute();
                int index = 0;
                while (foundAttribute)
                {
                    this._attributeNames[index] = reader.Name;
                    this._attributeValues[index++] = reader.Value;
                    foundAttribute = reader.MoveToNextAttribute();
                }
            }
            else
            {
                this._attributeNames = new string[0];
                this._attributeValues = new string[0];
            }

            reader.MoveToElement();
            if (reader.IsEmptyElement)
            {
                reader.Skip();
            }
            else
            {
                this._innerXml = this.GetInnerXml(reader);
            }
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the type name for this member.
        /// </summary>
        public override string TypeName
        {
            get { return "Unknown"; }
        }

        /// <summary>
        /// Gets the xml node name used to create this member.
        /// </summary>
        public string XmlNodeName
        {
            get { return this._xmlName; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new <see cref="UnknownMember"/> that is a copy of this instance.
        /// </summary>
        /// <returns>
        /// A new <see cref="UnknownMember"/> that is a copy of this instance.
        /// </returns>
        public new UnknownMember Clone()
        {
            return new UnknownMember(this, this.Structure);
        }

        /// <summary>
        /// Creates a deep copy of the current instance.
        /// </summary>
        /// <returns>
        /// A deep copy of the current System.Object.
        /// </returns>
        public override object DeepClone()
        {
            return this.Clone();
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified
        /// <see cref="UnknownMember"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public bool Equals(UnknownMember other)
        {
            if (other == null)
            {
                return false;
            }

            return object.ReferenceEquals(other, this);
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="IMember"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public override bool Equals(IMember other)
        {
            return this.Equals(other as UnknownMember);
        }

        /// <summary>
        /// Writes the xml representation of this instance onto the specified
        /// System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that the xml representation of this instance should be
        /// written to.
        /// </param>
        public override void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                throw new SmartArgumentNullException(() => writer);
            }

            for (int i = 0; i < this._attributeValues.Length; i++)
            {
                writer.WriteAttributeString(this._attributeNames[i], this._attributeValues[i]);
            }

            if (this._innerXml != null)
            {
                using (StringReader reader = new StringReader(this._innerXml))
                {
                    using (XmlReader xmlReader = XmlReader.Create(reader))
                    {
                        writer.WriteNode(xmlReader, false);
                    }
                }
            }
        }

        /// <summary>
        /// Gets the inner xml for this unknown member from the specified System.Xml.XmlReader.
        /// </summary>
        /// <param name="reader">
        /// Provides the data used to get the inner xml string.
        /// </param>
        /// <returns>
        /// The inner xml as defined on the specified System.Xml.XmlReader.
        /// </returns>
        private string GetInnerXml(XmlReader reader)
        {
            string innerXml = reader.ReadInnerXml();
            if (innerXml == null)
            {
                return null;
            }

            bool remove = true;
            string finalInnerXml = String.Empty;
            for (int i = 0; i < innerXml.Length; i++)
            {
                if (remove && innerXml[i] == '<')
                {
                    remove = false;
                }

                if (!remove)
                {
                    finalInnerXml += innerXml[i];
                }

                if (!remove && innerXml[i] == '>')
                {
                    remove = true;
                }
            }

            return finalInnerXml;
        }
        #endregion Methods
    } // RSG.Metadata.Model.Definitions.Members.UnknownMember {Class}
} // RSG.Metadata.Model.Definitions.Members {Namespace}
