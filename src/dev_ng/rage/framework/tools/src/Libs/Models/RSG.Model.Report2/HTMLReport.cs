﻿using System;
using System.IO;
using System.Web.UI;

namespace RSG.Model.Report
{
    /// <summary>
    /// HTMLReport - A report that generates a HTML file for display in the web browser
    /// </summary>
    public abstract class HTMLReport : XMLReport
    {
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public HTMLReport()
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="desc"></param>
        public HTMLReport(string name, string desc)
            : base(name, desc)
        {
        }
        #endregion // Constructor(s)

        #region Helper Functions
        /// <summary>
        /// Outputs a header to the supplied html writer
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="text"></param>
        protected void WriteHeader(HtmlTextWriter writer, string text)
        {
            writer.RenderBeginTag(HtmlTextWriterTag.H1);
            writer.Write(text);
            writer.RenderEndTag();
        }

        /// <summary>
        /// Outputs a sub-header to the supplied html writer
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="text"></param>
        protected void WriteSubHeader(HtmlTextWriter writer, string text)
        {
            writer.RenderBeginTag(HtmlTextWriterTag.H2);
            writer.Write(text);
            writer.RenderEndTag();
        }

        /// <summary>
        /// Outputs a paragraph of text
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="text"></param>
        protected void WriteParagraph(HtmlTextWriter writer, string text)
        {
            writer.RenderBeginTag(HtmlTextWriterTag.P);
            writer.Write(text);
            writer.RenderEndTag();
        }
        #endregion
    }
} // RSG.Model.Report namespace
