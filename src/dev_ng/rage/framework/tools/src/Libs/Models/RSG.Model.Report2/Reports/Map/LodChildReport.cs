﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.ConfigParser;
using RSG.Model.Common;
using RSG.Model.Common.Map;
using RSG.Base.Extensions;
using RSG.Base.Tasks;

namespace RSG.Model.Report.Reports.Map
{
    public class LodChildReport : CSVReport, IDynamicLevelReport
    {
        #region Constants
        private const String c_name = "Lod Children";
        private const String c_description = "Exports the lod objects that have more than one unique object underneath them in the selected level into a .csv file";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (m_task == null)
                {
                    CompositeTask task = new CompositeTask("Generate Report");
                    task.AddSubTask(new ActionTask("Loading data", (context, progress) => EnsureDataLoaded((DynamicLevelReportContext)context, progress)), 50);
                    task.AddSubTask(new ActionTask("Generating report", (context, progress) => GenerateReport((DynamicLevelReportContext)context, progress)));
                    m_task = task;
                }
                return m_task;
            }
        }
        private ITask m_task;

        /// <summary>
        /// List of stats that are required to generate this report.
        /// </summary>
        public IEnumerable<StreamableStat> RequiredStats
        {
            get { return null; }
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public LodChildReport()
            : base(c_name, c_description)
        {
        }
        #endregion // Constructor(s)

        #region Task Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void EnsureDataLoaded(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            IMapHierarchy hierarchy = context.Level.MapHierarchy;

            if (hierarchy != null)
            {
                float increment = 1.0f / hierarchy.AllSections.Count();

                // Load all the map section data
                foreach (IMapSection section in hierarchy.AllSections)
                {
                    context.Token.ThrowIfCancellationRequested();

                    // Update the progress information
                    string message = String.Format("Loading {0}", section.Name);
                    progress.Report(new TaskProgress(increment, true, message));

                    section.LoadStats(new StreamableStat[] { StreamableMapSectionStat.Entities });
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void GenerateReport(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            ExportCSVReport(Filename, context.Level, context.GameView);
        }
        #endregion // Task Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="objectFilename"></param>
        /// <param name="interiorFilename"></param>
        /// <param name="level"></param>
        /// <param name="gv"></param>
        private void ExportCSVReport(String filename, ILevel level, ConfigGameView gv)
        {
            // Attempt the get the map hierarchy from the level
            IMapHierarchy hierarchy = level.MapHierarchy;

            if (hierarchy != null)
            {
                // Load all the map section data
                foreach (IMapSection section in hierarchy.AllSections)
                {
                    section.RequestAllStatistics(false);
                }

                // Get all the archetypes
                ISet<Tuple<IMapSection, IEntity, ISet<string>>> errorEntities =
                    new HashSet<Tuple<IMapSection, IEntity, ISet<string>>>();

                // Determine which collision bit flag combinations are in use
                foreach (IMapSection section in hierarchy.AllSections)
                {
                    IEnumerable<IEntity> lodEntities = section.ChildEntities.Where(entity => entity.LodLevel == LodLevel.Lod);
                    foreach (IEntity lodEntity in lodEntities)
                    {
                        ISet<string> archetypes = new HashSet<string>();
                        foreach (IEntity lodChild in lodEntity.LodChildren)
                        {
                            if (lodChild.LodLevel != LodLevel.Hd || lodChild.ReferencedArchetype == null)
                            {
                                continue;
                            }

                            if (!archetypes.Contains(lodChild.ReferencedArchetype.Name))
                            {
                                archetypes.Add(lodChild.ReferencedArchetype.Name);
                            }
                        }

                        if (archetypes.Count > 1)
                        {
                            errorEntities.Add(new Tuple<IMapSection,IEntity,ISet<string>>(section, lodEntity, archetypes));
                        }
                    }
                }
                
                // Generate the report
                using (StreamWriter sw = new StreamWriter(filename))
                {
                    WriteCsvHeader(sw);
                    foreach (Tuple<IMapSection, IEntity, ISet<string>> entity in errorEntities)
                    {
                        WriteCsvEntity(sw, entity);
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sw"></param>
        /// <param name="milo"></param>
        private void WriteCsvHeader(StreamWriter sw)
        {
            string header = "Name,Section,Parent Area,Grandparent Area,Unique Lod Child Count";
            sw.WriteLine(header);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objectSw"></param>
        /// <param name="interiorSw"></param>
        /// <param name="archetype"></param>
        /// <param name="collisionFlags"></param>
        /// <param name="geometryStatsCollection"></param>
        private void WriteCsvEntity(StreamWriter sw, Tuple<IMapSection, IEntity, ISet<string>> entity)
        {
            IMapSection section = entity.Item2.Parent as IMapSection;
            IMapArea parentArea = section.ParentArea;
            IMapArea grandParentArea = (parentArea != null ? parentArea.ParentArea : null);

            string scvRecord = String.Format("{0},{1},{2},{3}", entity.Item2.Name, section.Name, parentArea != null ? parentArea.Name : "n/a", grandParentArea != null ? grandParentArea.Name : "n/a");
            scvRecord += string.Format(",{0}", entity.Item3.Count);

            foreach (string archetypeName in entity.Item3)
            {
                scvRecord += string.Format(",{0}", archetypeName);
            }

            sw.WriteLine(scvRecord);
        }
        #endregion // Private Methods
    }
}
