﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using RSG.Model.Statistics.Captures;
using RSG.SourceControl.Perforce;
using RSG.Model.Report.Reports.GameStatsReports.Tests;

namespace RSG.Model.Report.Reports.GameStatsReports
{
    /// <summary>
    /// 
    /// </summary>
    public class AutomatedPerBuildReport : AutomatedGameStatsReportBase
    {
        #region Constants
        private const String NAME = "Automated Report (Per Build)";
        private const String DESC = "Automated report that shows information regarding various stats on a per build basis.";
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="desc"></param>
        public AutomatedPerBuildReport()
            : base(NAME, DESC)
        {
            Mode = GameStatReportMode.PerBuild;

            // Add the list of smoke tests
            SmokeTests.Add(new FpsSmokeTest());
            SmokeTests.Add(new CpuSmokeTest());
            SmokeTests.Add(new GpuSmokeTest());
        }
        #endregion // Constructor(s)

        #region Methods
        protected override P4 GetPerforceConnection()
        {
            return null;
        }
        #endregion
    } // AutomatedPerBuildReport
}
