﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Map.SceneOverride
{
    /// <summary>
    /// A class representing a LOD override item as serialised by the game
    /// </summary>
    public class RuntimeLODOverride
    {
        public RuntimeLODOverride(uint hash, uint guid, float posX, float posY, float posZ, String imapName, String modelName)
        {
            Guid = guid;
            Hash = hash;
            PosX = posX;
            PosY = posY;
            PosZ = posZ;
            IMAPName = imapName;
            ModelName = modelName;
            Distance = invalidDistance_;
            ChildDistance = invalidDistance_;
        }

        public uint Guid { get; private set; }
        public uint Hash { get; private set; }
        public float PosX { get; private set; }
        public float PosY { get; private set; }
        public float PosZ { get; private set; }
        public String IMAPName { get; private set; }
        public String ModelName { get; private set; }
        public float Distance { get; set; }
        public float ChildDistance { get; set; }

        public bool HasDistance { get { return Distance != invalidDistance_; } }
        public bool HasChildDistance { get { return ChildDistance != invalidDistance_; } }

        private static float invalidDistance_ = -1.0f;
    }
}
