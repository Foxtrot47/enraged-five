﻿// --------------------------------------------------------------------------------------------
// <copyright file="BaseMatrixMember.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions.Members
{
    using System;
    using System.Xml;
    using RSG.Editor;

    /// <summary>
    /// Provides a base class to a matrix member node in the parCodeGen system that can be
    /// instanced in a metadata file.
    /// </summary>
    public abstract class BaseMatrixMember :
        MemberBase,
        IHasInitialValue<float[]>,
        IHasRange<float>,
        IHasStep<float[]>,
        IHasPrecision,
        IHasDecimalPlace
    {
        #region Fields
        /// <summary>
        /// The name of the xml attribute containing the data for the
        /// <see cref="DecimalPlaces"/> property.
        /// </summary>
        private const string XmlDecimalPlacesAttr = "ui_decimalPlaces";

        /// <summary>
        /// The name of the xml attribute containing the data for the
        /// <see cref="HighPrecision"/> property.
        /// </summary>
        private const string XmlHighPrecisionAttr = "highPrecision";

        /// <summary>
        /// The name of the xml attribute containing the data for the
        /// <see cref="InitialValue"/> property.
        /// </summary>
        private const string XmlInitialAttr = "init";

        /// <summary>
        /// The name of the xml attribute containing the data for the <see cref="Maximum"/>
        /// property.
        /// </summary>
        private const string XmlMaxAttr = "max";

        /// <summary>
        /// The name of the xml attribute containing the data for the <see cref="Minimum"/>
        /// property.
        /// </summary>
        private const string XmlMinAttr = "min";

        /// <summary>
        /// The name of the xml attribute containing the data for the <see cref="Step"/>
        /// property.
        /// </summary>
        private const string XmlStepAttr = "step";

        /// <summary>
        /// The private field used for the <see cref="DecimalPlaces"/> property.
        /// </summary>
        private string _decimalPlaces;

        /// <summary>
        /// The private field used for the <see cref="HighPrecision"/> property.
        /// </summary>
        private string _highPrecision;

        /// <summary>
        /// The private field used for the <see cref="InitialValue"/> property.
        /// </summary>
        private string _initialValue;

        /// <summary>
        /// The private field used for the <see cref="Maximum"/> property.
        /// </summary>
        private string _maximum;

        /// <summary>
        /// The private field used for the <see cref="Minimum"/> property.
        /// </summary>
        private string _minimum;

        /// <summary>
        /// The private field used for the <see cref="Step"/> property.
        /// </summary>
        private string _step;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="BaseMatrixMember"/> class to be one of
        /// the members of the specified structure.
        /// </summary>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        protected BaseMatrixMember(IStructure structure)
            : base(structure)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="BaseMatrixMember"/> class as a copy of
        /// the specified instance.
        /// </summary>
        /// <param name="other">
        /// The instance to copy.
        /// </param>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        protected BaseMatrixMember(BaseMatrixMember other, IStructure structure)
            : base(other, structure)
        {
            this._initialValue = other._initialValue;
            this._minimum = other._minimum;
            this._maximum = other._maximum;
            this._step = other._step;
            this._decimalPlaces = other._decimalPlaces;
            this._highPrecision = other._highPrecision;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="BaseMatrixMember"/> class.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        protected BaseMatrixMember(XmlReader reader, IStructure structure)
            : base(reader, structure)
        {
            this.Deserialise(reader);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the decimal place count the value of an instance to this member has.
        /// </summary>
        public short DecimalPlaces
        {
            get { return this.Dictionary.To<short>(this._decimalPlaces, -1); }
            set { this.SetProperty(ref this._decimalPlaces, value.ToString()); }
        }

        /// <summary>
        /// Gets the height dimension of the matrix member.
        /// </summary>
        public abstract int DimensionHeight { get; }

        /// <summary>
        /// Gets the width dimension of the matrix member.
        /// </summary>
        public abstract int DimensionWidth { get; }

        /// <summary>
        /// Gets or sets a value indicating whether this member has high precision on or off.
        /// </summary>
        public bool HighPrecision
        {
            get { return this.Dictionary.To<bool>(this._highPrecision, false); }
            set { this.SetProperty(ref this._highPrecision, value.ToString()); }
        }

        /// <summary>
        /// Gets or sets the initial value that any instance of this member is set to.
        /// </summary>
        public float[] InitialValue
        {
            get
            {
                return this.Dictionary.To<float>(this._initialValue, this.Dimension, 0.0f);
            }

            set
            {
                if (value == null)
                {
                    this.SetProperty(ref this._initialValue, null);
                }
                else
                {
                    string newValue = String.Join(", ", value);
                    this.SetProperty(ref this._initialValue, newValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the maximum value that a instance of this member can be set to.
        /// </summary>
        public float Maximum
        {
            get { return this.Dictionary.To<float>(this._maximum, float.MaxValue * 0.001f); }
            set { this.SetProperty(ref this._maximum, value.ToString()); }
        }

        /// <summary>
        /// Gets or sets the minimum value that a instance of this member can be set to.
        /// </summary>
        public float Minimum
        {
            get { return this.Dictionary.To<float>(this._minimum, float.MaxValue * -0.001f); }
            set { this.SetProperty(ref this._minimum, value.ToString()); }
        }

        /// <summary>
        /// Gets or sets the increase and decrease amount for a single step.
        /// </summary>
        public float[] Step
        {
            get
            {
                return this.Dictionary.To<float>(this._step, this.Dimension, 0.1f);
            }

            set
            {
                if (value == null)
                {
                    this.SetProperty(ref this._step, null);
                }
                else
                {
                    string newValue = String.Join(", ", value);
                    this.SetProperty(ref this._step, newValue);
                }
            }
        }

        /// <summary>
        /// Gets the dimension of the matrix member.
        /// </summary>
        private int Dimension
        {
            get { return this.DimensionHeight * this.DimensionWidth; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Writes the xml representation of this instance onto the specified
        /// System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that the xml representation of this instance should be
        /// written to.
        /// </param>
        public override void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                throw new SmartArgumentNullException(() => writer);
            }

            this.SerialiseBaseAttributes(writer);
            if (this._initialValue != null)
            {
                writer.WriteAttributeString(XmlInitialAttr, this._initialValue);
            }

            if (this._minimum != null)
            {
                writer.WriteAttributeString(XmlMinAttr, this._minimum);
            }

            if (this._maximum != null)
            {
                writer.WriteAttributeString(XmlMaxAttr, this._maximum);
            }

            if (this._step != null)
            {
                writer.WriteAttributeString(XmlStepAttr, this._step);
            }

            if (this._decimalPlaces != null)
            {
                writer.WriteAttributeString(XmlDecimalPlacesAttr, this._decimalPlaces);
            }

            if (this._highPrecision != null)
            {
                writer.WriteAttributeString(XmlHighPrecisionAttr, this._highPrecision);
            }
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            this._initialValue = reader.GetAttribute(XmlInitialAttr);
            this._minimum = reader.GetAttribute(XmlMinAttr);
            this._maximum = reader.GetAttribute(XmlMaxAttr);
            this._step = reader.GetAttribute(XmlStepAttr);
            this._decimalPlaces = reader.GetAttribute(XmlDecimalPlacesAttr);
            this._highPrecision = reader.GetAttribute(XmlHighPrecisionAttr);
            reader.Skip();
        }
        #endregion Methods
    } // RSG.Metadata.Model.Definitions.Members.BaseMatrixMember {Class}
} // RSG.Metadata.Model.Definitions.Members {Namespace}
