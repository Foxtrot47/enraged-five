﻿//---------------------------------------------------------------------------------------------
// <copyright file="MapTunable.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Tunables
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Linq;
    using System.Xml;
    using RSG.Base.Extensions;
    using RSG.Base.Logging;
    using RSG.Editor;
    using RSG.Editor.Model;
    using RSG.Metadata.Model.Definitions.Members;
    using RSG.Metadata.Model.Resources;

    /// <summary>
    /// Represents a tunable used by the parCodeGen system that is instancing a
    /// <see cref="MapMember"/> object.
    /// </summary>
    public class MapTunable : TunableBase, ITunableParent, IDynamicTunableParent
    {
        #region Fields
        /// <summary>
        /// The private dictionary that contains the mapping between the map item tunable and
        /// its map key.
        /// </summary>
        private ModelCollection<ITunable> _keyIndex;

        /// <summary>
        /// The private collection that contains the map keys.
        /// </summary>
        private ModelCollection<KeyLookupItem> _keys;

        /// <summary>
        /// The private field used for the <see cref="Tunables"/> property.
        /// </summary>
        private ReadOnlyModelCollection<ITunable> _readOnlyTunables;

        /// <summary>
        /// The private field for the <see cref="Tunables"/> property.
        /// </summary>
        private ModelCollection<ITunable> _tunables;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MapTunable"/> class to be as
        /// instance of the specified member.
        /// </summary>
        /// <param name="member">
        /// The member that this tunable will be instancing.
        /// </param>
        /// <param name="parent">
        /// The tunable parent this tunable will belong to.
        /// </param>
        public MapTunable(MapMember member, ITunableParent parent)
            : base(member, parent)
        {
            this._keys = new ModelCollection<KeyLookupItem>(this);
            this._keyIndex = new ModelCollection<ITunable>(this);
            this._tunables = new ModelCollection<ITunable>(this);
            this._tunables.CollectionChanged += this.OnCollectionChanged;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="MapTunable"/> class as a copy of the
        /// specified instance.
        /// </summary>
        /// <param name="other">
        /// The instance to copy.
        /// </param>
        /// <param name="parent">
        /// The tunable parent this tunable will belong to.
        /// </param>
        public MapTunable(MapTunable other, ITunableParent parent)
            : base(other, parent)
        {
            this._keyIndex = new ModelCollection<ITunable>(this);
            List<ITunable> tunables = new List<ITunable>();
            List<KeyLookupItem> keys = new List<KeyLookupItem>();
            foreach (ITunable tunable in other.Tunables)
            {
                ITunable cloned = tunable.Clone();
                tunables.Add(cloned);
                KeyLookupItem key = other.GetTunableKeyMappingModel(tunable);
                KeyLookupItem newKey = new KeyLookupItem(cloned, key.Value, this);
                keys.Add(newKey);
                this._keyIndex.Add(cloned);
            }

            this._keys = new ModelCollection<KeyLookupItem>(this, keys);
            this._tunables = new ModelCollection<ITunable>(this, tunables);
            this._tunables.CollectionChanged += this.OnCollectionChanged;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="MapTunable"/> class using the
        /// specified System.Xml.XmlReader as a initialising data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <param name="member">
        /// The member that this tunable will be instancing.
        /// </param>
        /// <param name="parent">
        /// The tunable parent this tunable will belong to.
        /// </param>
        /// <param name="log">
        /// A object that is used to log any warnings or errors produced during the parsing of
        /// the specified reader.
        /// </param>
        public MapTunable(XmlReader reader, MapMember member, ITunableParent parent, ILog log)
            : base(reader, member, parent)
        {
            this._keys = new ModelCollection<KeyLookupItem>(this);
            this._keyIndex = new ModelCollection<ITunable>(this);
            this._tunables = new ModelCollection<ITunable>(this);
            this.Deserialise(reader, log);
            this._tunables.CollectionChanged += this.OnCollectionChanged;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the element type that this maps children are instancing.
        /// </summary>
        public IMember ElementType
        {
            get { return this.MapMember.ElementType; }
        }

        /// <summary>
        /// Gets the number of items currently contained inside this tunables child collection.
        /// </summary>
        public int ItemCount
        {
            get { return this._tunables.Count; }
        }

        /// <summary>
        /// Gets the keys for all the tunables this map contains.
        /// </summary>
        public IEnumerable<string> Keys
        {
            get { return this._keys.Select(item => item.Value).ToList(); }
        }

        /// <summary>
        /// Gets the array member that this tunable is instancing.
        /// </summary>
        public MapMember MapMember
        {
            get
            {
                MapMember member = this.Member as MapMember;
                if (member != null)
                {
                    return member;
                }

                return new MapMember(this.TunableStructure.Definition);
            }
        }

        /// <summary>
        /// Gets a collection of the tunables that are defined for this array.
        /// </summary>
        public ReadOnlyModelCollection<ITunable> Tunables
        {
            get
            {
                if (this._readOnlyTunables == null)
                {
                    this._readOnlyTunables = this._tunables.ToReadOnly();
                }

                return this._readOnlyTunables;
            }
        }

        /// <summary>
        /// Gets or sets the key for the specified tunable.
        /// </summary>
        /// <param name="tunable">
        /// The tunable whose key will be set or retrieved.
        /// </param>
        /// <returns>
        /// The string key for the specified tunable if the tunable belongs to this map;
        /// otherwise, null.
        /// </returns>
        public string this[ITunable tunable]
        {
            get
            {
                if (tunable == null)
                {
                    throw new SmartArgumentNullException(() => tunable);
                }

                foreach (KeyLookupItem key in this._keys)
                {
                    if (Object.ReferenceEquals(tunable, key.Tunable))
                    {
                        return key.Value;
                    }
                }

                return null;
            }

            set
            {
                if (tunable == null)
                {
                    throw new SmartArgumentNullException(() => tunable);
                }

                if (!this._tunables.Contains(tunable))
                {
                    throw new InvalidOperationException(
                        "Unable to set the key on a tunable that isn't apart of this map.");
                }

                foreach (KeyLookupItem key in this._keys)
                {
                    if (Object.ReferenceEquals(tunable, key.Tunable))
                    {
                        key.Value = value;
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Retrieves the tunable that has the specified key within this map tunable if found;
        /// otherwise, null.
        /// </summary>
        /// <param name="key">
        /// The key whose mapped tunable should be retrieved.
        /// </param>
        /// <returns>
        /// The tunable that has the specified key within this map tunable; otherwise, null.
        /// </returns>
        public ITunable this[string key]
        {
            get
            {
                foreach (KeyLookupItem keyLookupItem in this._keys)
                {
                    if (String.Equals(keyLookupItem.Value, key))
                    {
                        return keyLookupItem.Tunable;
                    }
                }

                return null;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Adds a new item to this map and returns it. The item added is a default instance
        /// of the element type for this map.
        /// </summary>
        /// <returns>
        /// The item that has been added.
        /// </returns>
        public ITunable AddNewItem()
        {
            ITunable newTunable = this.MapMember.ElementType.CreateTunable(this);
            KeyLookupItem newKey = new KeyLookupItem(newTunable, this.CreateKey(), this);
            this._keys.Add(newKey);
            this._keyIndex.Add(newTunable);
            this._tunables.Add(newTunable);
            return newTunable;
        }

        /// <summary>
        /// Creates a new <see cref="MapTunable"/> that is a copy of this instance.
        /// </summary>
        /// <returns>
        /// A new <see cref="MapTunable"/> that is a copy of this instance.
        /// </returns>
        public new MapTunable Clone()
        {
            return new MapTunable(this, this.TunableStructure);
        }

        /// <summary>
        /// Creates a deep copy of the current instance.
        /// </summary>
        /// <returns>
        /// A deep copy of the current System.Object.
        /// </returns>
        public override object DeepClone()
        {
            return this.Clone();
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="ITunable"/> by
        /// looking at their values.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance by their values;
        /// otherwise, false.
        /// </returns>
        public override bool EqualByValue(ITunable other)
        {
            return this.EqualByValue(other as MapTunable);
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="ITunable"/> by
        /// looking at their values.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance by their values;
        /// otherwise, false.
        /// </returns>
        public bool EqualByValue(MapTunable other)
        {
            if (other == null)
            {
                return false;
            }

            if (this.Tunables.Count != other.Tunables.Count)
            {
                return false;
            }

            for (int i = 0; i < this.Tunables.Count; i++)
            {
                string keyValue = this[this.Tunables[i]];
                ITunable otherTunable = other[keyValue];
                if (otherTunable == null)
                {
                    return false;
                }

                if (!this.Tunables[i].EqualByValue(otherTunable))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified
        /// <see cref="MapTunable"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public bool Equals(MapTunable other)
        {
            if (other == null)
            {
                return false;
            }

            return Object.ReferenceEquals(other, this);
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="ITunable"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public override bool Equals(ITunable other)
        {
            return this.Equals(other as EnumTunable);
        }

        /// <summary>
        /// Gets the model that represents the mapping between the specified tunable and its
        /// map key.
        /// </summary>
        /// <param name="tunable">
        /// The tunable whose key mapping model should be returned.
        /// </param>
        /// <returns>
        /// The model that represents the mapping between the specified tunable and its map
        /// key.
        /// </returns>
        public KeyLookupItem GetTunableKeyMappingModel(ITunable tunable)
        {
            int index = this._keyIndex.IndexOf(tunable);
            if (index == -1)
            {
                //// Possible during undo/redo
                return null;
            }

            return this._keys[index];
        }

        /// <summary>
        /// Gets the zero-based index for the specified tunable within this tunables items
        /// collection.
        /// </summary>
        /// <param name="tunable">
        /// The tunable whose index will be retrieved.
        /// </param>
        /// <returns>
        /// The index of the specified tunable within this tunables items collection if found;
        /// otherwise, -1.
        /// </returns>
        public int IndexOf(ITunable tunable)
        {
            return this.Tunables.IndexOf(tunable);
        }

        /// <summary>
        /// Adds a new item to this array and returns it. The item added is a default instance
        /// of the element type for this array.
        /// </summary>
        /// <param name="index">
        /// The zero-based index at which the item should be inserted.
        /// </param>
        /// <returns>
        /// The item that has been added.
        /// </returns>
        public ITunable InsertNewItem(int index)
        {
            ITunable newTunable = this.MapMember.ElementType.CreateTunable(this);
            KeyLookupItem newKey = new KeyLookupItem(newTunable, this.CreateKey(), this);
            this._keys.Add(newKey);
            this._keyIndex.Add(newTunable);
            this._tunables.Insert(index, newTunable);
            return newTunable;
        }

        /// <summary>
        /// Adds a new item to this tunable and returns it. The item added is initialised with
        /// the data in the specified xml reader.
        /// </summary>
        /// <param name="index">
        /// The zero-based index at which the item should be inserted.
        /// </param>
        /// <param name="reader">
        /// The reader that contains the data to initialise the new item.
        /// </param>
        /// <param name="log">
        /// A object that is used to log any warnings or errors produced during the parsing of
        /// the specified reader.
        /// </param>
        /// <returns>
        /// The item that has been added.
        /// </returns>
        public ITunable InsertNewItem(int index, XmlReader reader, ILog log)
        {
            ITunable newTunable = this.MapMember.ElementType.CreateTunable(reader, this, log);
            KeyLookupItem newKey = new KeyLookupItem(newTunable, this.CreateKey(), this);
            this._keys.Add(newKey);
            this._keyIndex.Add(newTunable);
            this._tunables.Insert(index, newTunable);
            return newTunable;
        }

        /// <summary>
        /// Moves the item at the specified index to a new location in the collection.
        /// </summary>
        /// <param name="oldIndex">
        /// The zero-based index specifying the location of the item to be moved.
        /// </param>
        /// <param name="newIndex">
        /// The zero-based index specifying the new location of the item.
        /// </param>
        public void MoveItem(int oldIndex, int newIndex)
        {
            this._tunables.Move(oldIndex, newIndex);
        }

        /// <summary>
        /// Removes the first occurrence of the specified object from this arrays child
        /// collection.
        /// </summary>
        /// <param name="item">
        /// The tunable to remove from this arrays child collection.
        /// </param>
        public void RemoveItem(ITunable item)
        {
            this._tunables.Remove(item);
            foreach (KeyLookupItem key in this._keys)
            {
                if (Object.ReferenceEquals(item, key.Tunable))
                {
                    this._keys.Remove(key);
                    break;
                }
            }

            this._keyIndex.Remove(item);
        }

        /// <summary>
        /// Removes the child tunable at the specified index.
        /// </summary>
        /// <param name="index">
        /// The zero-based index of the tunable to remove.
        /// </param>
        public void RemoveItemAt(int index)
        {
            this._tunables.RemoveAt(index);
            ITunable item = this._tunables[index];
            foreach (KeyLookupItem key in this._keys)
            {
                if (Object.ReferenceEquals(item, key.Tunable))
                {
                    this._keys.Remove(key);
                    break;
                }
            }

            this._keyIndex.Remove(item);
        }

        /// <summary>
        /// Resets this tunable to have its default value. (i.e no source data with its initial
        /// values).
        /// </summary>
        public override void ResetValueToDefaultCore()
        {
            this._tunables.Clear();
            this._keys.Clear();
            this._keyIndex.Clear();
        }

        /// <summary>
        /// Writes the xml representation of this instance onto the specified
        /// System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that the xml representation of this instance should be
        /// written to.
        /// </param>
        /// <param name="serialiseDefaultTunables">
        /// A value indicating whether the tunables that have a default value should be written
        /// to the specified writer or not.
        /// </param>
        public override void Serialise(XmlWriter writer, bool serialiseDefaultTunables)
        {
            if (writer == null)
            {
                throw new SmartArgumentNullException(() => writer);
            }

            foreach (ITunable tunable in this._tunables)
            {
                writer.WriteStartElement("Item");
                string key = this[tunable];
                if (key != null)
                {
                    writer.WriteAttributeString("key", key);
                }

                tunable.Serialise(writer, serialiseDefaultTunables);
                writer.WriteEndElement();
            }
        }

        /// <summary>
        /// Updates this tunables HasDefaultValue property.
        /// </summary>
        public void UpdateHasDefaultValue()
        {
            bool hasDefaultValue = true;
            foreach (ITunable tunable in this._tunables)
            {
                if (!tunable.HasDefaultValue)
                {
                    hasDefaultValue = false;
                    break;
                }
            }

            this.HasDefaultValue = hasDefaultValue;
        }

        /// <summary>
        /// Creates the next unique key that is valid for this map tunable based on the current
        /// keys.
        /// </summary>
        /// <returns>
        /// The next unique key.
        /// </returns>
        private string CreateKey()
        {
            switch (this.MapMember.Key)
            {
                case KeyType.AtHashString:
                case KeyType.AtHashValue:
                    return this.CreateKeyForStringType();

                case KeyType.Signed32:
                    return this.CreateKeyForNumericalType(Int32.MinValue, Int32.MaxValue);

                case KeyType.Signed16:
                    return this.CreateKeyForNumericalType(Int16.MinValue, Int16.MaxValue);

                case KeyType.Signed8:
                    return this.CreateKeyForNumericalType(SByte.MinValue, SByte.MaxValue);

                case KeyType.Unsigned32:
                    return this.CreateKeyForNumericalType(UInt32.MinValue, UInt32.MaxValue);

                case KeyType.Unsigned16:
                    return this.CreateKeyForNumericalType(UInt16.MinValue, UInt16.MaxValue);

                case KeyType.Unsigned8:
                    return this.CreateKeyForNumericalType(Byte.MinValue, Byte.MaxValue);

                case KeyType.Enum:
                    return this.CreateKeyForEnumType();
            }

            return null;
        }

        /// <summary>
        /// Creates the next unique key that is valid for this map tunable based on the current
        /// keys.
        /// </summary>
        /// <returns>
        /// The next unique key.
        /// </returns>
        private string CreateKeyForEnumType()
        {
            Definitions.IEnumeration enumeration = this.MapMember.EnumKey;
            if (enumeration == null)
            {
                return null;
            }

            foreach (Definitions.IEnumConstant constant in enumeration.EnumConstants)
            {
                bool unqiue = true;
                foreach (KeyLookupItem key in this._keys)
                {
                    if (constant.Name == key.Value)
                    {
                        unqiue = false;
                        break;
                    }
                }

                if (unqiue)
                {
                    return constant.Name;
                }
            }

            return null;
        }

        /// <summary>
        /// Creates the next unique key that is valid for this map tunable based on the current
        /// keys.
        /// </summary>
        /// <param name="min">
        /// The minimum value the key can represent.
        /// </param>
        /// <param name="max">
        /// The maximum value the key can represent.
        /// </param>
        /// <returns>
        /// The next unique key.
        /// </returns>
        private string CreateKeyForNumericalType(long min, long max)
        {
            for (long value = 0; value <= max; value++)
            {
                bool unqiue = true;
                foreach (KeyLookupItem key in this._keys)
                {
                    long itemValue = 0;
                    if (!long.TryParse(key.Value, out itemValue))
                    {
                        itemValue = 0;
                    }

                    if (value == itemValue)
                    {
                        unqiue = false;
                        break;
                    }
                }

                if (unqiue)
                {
                    return value.ToString();
                }
            }

            for (long value = 0; value >= min; value--)
            {
                bool unqiue = true;
                foreach (KeyLookupItem key in this._keys)
                {
                    long itemValue = 0;
                    if (!long.TryParse(key.Value, out itemValue))
                    {
                        itemValue = 0;
                    }

                    if (value == itemValue)
                    {
                        unqiue = false;
                        break;
                    }
                }

                if (unqiue)
                {
                    return value.ToString();
                }
            }

            return null;
        }

        /// <summary>
        /// Creates the next unique key that is valid for this map tunable based on the current
        /// keys.
        /// </summary>
        /// <returns>
        /// The next unique key.
        /// </returns>
        private string CreateKeyForStringType()
        {
            for (long index = 0; index <= long.MaxValue; index++)
            {
                bool unqiue = true;
                string value = "MapItem" + index.ToString();
                foreach (KeyLookupItem key in this._keys)
                {
                    if (String.Equals(value, key.Value))
                    {
                        unqiue = false;
                        break;
                    }
                }

                if (unqiue)
                {
                    return value;
                }
            }

            return null;
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        /// <param name="log">
        /// A object that is used to log any warnings or errors produced during the parsing of
        /// the specified reader.
        /// </param>
        private void Deserialise(XmlReader reader, ILog log)
        {
            if (reader == null)
            {
                return;
            }

            IXmlLineInfo lineInfo = reader as IXmlLineInfo;
            string line = lineInfo.LineNumber.ToStringInvariant();
            string pos = lineInfo.LinePosition.ToStringInvariant();

            try
            {
                this.DeserialiseInternal(reader, line, pos, log);
            }
            catch (MetadataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                string msg = StringTable.ArrayTunableDeserialiseError;
                msg = msg.FormatInvariant(line, pos, ex.Message);
                throw new MetadataException(msg);
            }

            reader.Skip();
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        /// <param name="line">
        /// The reader line number to add to any thrown exceptions.
        /// </param>
        /// <param name="pos">
        /// The reader position number to add to any thrown exceptions.
        /// </param>
        /// <param name="log">
        /// A object that is used to log any warnings or errors produced during the parsing of
        /// the specified reader.
        /// </param>
        private void DeserialiseInternal(XmlReader reader, string line, string pos, ILog log)
        {
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            MapMember member = this.MapMember;
            if (member.ElementType == null)
            {
                string msg = StringTable.MissingMapElementError;
                throw new MetadataException(msg, line, pos);
            }

            TunableFactory factory = new TunableFactory(this);
            while (reader.MoveToElementOrComment())
            {
                if (reader.NodeType == XmlNodeType.Comment)
                {
                    reader.Skip();
                    continue;
                }

                if (String.Equals(reader.Name, "Item"))
                {
                    string key = reader.GetAttribute("key");
                    ITunable child = factory.Create(reader, member.ElementType, log);
                    if (child == null)
                    {
                        IXmlLineInfo lineInfo = reader as IXmlLineInfo;
                        string childLine = lineInfo.LineNumber.ToStringInvariant();
                        string childPos = lineInfo.LinePosition.ToStringInvariant();
                        string msg = StringTable.MapChildCreationError;
                        throw new MetadataException(msg, childLine, childPos);
                    }

                    KeyLookupItem newKey = new KeyLookupItem(child, key, this);
                    this._keys.Add(newKey);
                    this._keyIndex.Add(child);
                    this._tunables.Add(child);
                }
                else
                {
                    IXmlLineInfo lineInfo = reader as IXmlLineInfo;
                    string childLine = lineInfo.LineNumber.ToStringInvariant();
                    string childPos = lineInfo.LinePosition.ToStringInvariant();
                    string msg = StringTable.MapItemError;
                    throw new MetadataException(msg, line, pos);
                }
            }

            if (reader.NodeType == XmlNodeType.EndElement)
            {
                reader.ReadEndElement();
            }
        }

        /// <summary>
        /// Called whenever the tunable collection containing this elements child changes so
        /// that we can set the default state.
        /// </summary>
        /// <param name="sender">
        /// The collection that fired the change event.
        /// </param>
        /// <param name="e">
        /// The System.Collections.Specialized.NotifyCollectionChangedEventArgs containing the
        /// event data.
        /// </param>
        private void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            this.HasDefaultValue = false;
        }
        #endregion Methods
    } // RSG.Metadata.Model.Tunables.MapTunable {Class}
} // RSG.Metadata.Model.Tunables {Namespace}
