﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace RSG.Model.Statistics.Captures.Historical
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class HistoricalStreamingMemoryResults
    {
        #region Properties
        /// <summary>
        /// Name of the zone these stats are for.
        /// </summary>
        [DataMember]
        public String ZoneName { get; private set; }

        /// <summary>
        /// Mapping of changelist numbers to a dictionary of cpu result keys to cpu results.
        /// </summary>
        [DataMember]
        public IDictionary<Object, IDictionary<String, StreamingMemoryResult>> Results { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public HistoricalStreamingMemoryResults(String zone)
        {
            ZoneName = zone;
            Results = new SortedDictionary<Object, IDictionary<String, StreamingMemoryResult>>();
        }
        #endregion // Constructor(s)

        #region Public Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="result"></param>
        public void AddResult(Object context, StreamingMemoryResult result)
        {
            if (!Results.ContainsKey(context))
            {
                Results[context] = new SortedDictionary<String, StreamingMemoryResult>();
            }

            Results[context][result.ModuleName] = result;
        }
        #endregion // Public Methods
    } // HistoricalStreamingMemoryResults
}
