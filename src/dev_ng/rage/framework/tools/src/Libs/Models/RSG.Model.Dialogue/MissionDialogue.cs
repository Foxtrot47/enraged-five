﻿using System;
using System.IO;
using System.Windows;
using System.Reflection;
using System.Xml;
using System.Xml.XPath;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.ComponentModel;
using RSG.Model.Dialogue.Config;
using RSG.Model.Dialogue.UndoManager;
using RSG.Model.Dialogue.UndoInterface;
using RSG.Model.Dialogue.DirtyManager;
using RSG.Model.Dialogue.Search;

namespace RSG.Model.Dialogue 
{
    using DialogueLine = KeyValuePair<String, String>;
    using ConversationContainer = List<List<KeyValuePair<String, String>>>;
    using System.Diagnostics;

    public enum ConversationMarkup
    {
        NONE,
        MOCAP,
        RANDOM,
        PHONE,
    }

    public class ParsedConversation
    {
        #region Properties and associated member data

        public String Header
        {
            get;
            set;
        }

        public ConversationMarkup Markup
        {
            get;
            set;
        }

        public List<DialogueLine> DialogueLines
        {
            get;
            set;
        }

        #endregion // Properties and associated member data
    }

    /// <summary>
    /// Represents a set of conversations with the same mission id
    /// this gets saved and loaded from a single file.
    /// </summary>
    public class MissionDialogue : UndoRedoBase
    {
        public static Dictionary<int, String> StringFormatReplacements;

        public static String GetStringInRightFormat(String oldFormat)
        {
            String correctStringFormat = String.Empty;
            int index = 0;
            foreach (char character in oldFormat.ToCharArray())
            {
                int characterValue = (int)character;
                if (StringFormatReplacements.ContainsKey(characterValue))
                {
                    correctStringFormat += StringFormatReplacements[character];
                }
                else
                {
                    if (characterValue < 0 || characterValue > 255)
                    {
                        String message = String.Format("Found a non ASCII character ({0}) " + 
                                                       "and dialogue star doesn't know how to convert it!\n" +
                                                       "Please bug this, making a reference to the character number above " +
                                                       "and what the character should be in ASCII if not clear.", characterValue);

                        MessageBox.Show(message, "Non ASCII Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                    correctStringFormat += character;
                }
                index++;
            }

            return correctStringFormat;
        }

        #region Constants
        private static readonly String ExpectedVersion = "2.0";
        private static readonly String VersionAttribute = "version";
        private static readonly String MissionIdAttribute = "id";
        private static readonly String MissionNameAttribute = "name";
        private static readonly String MissionSubtitleIdAttribute = "subtitle";
        private static readonly String TypeAttribute = "type";
        private static readonly String MultiplayerAttribute = "multiplayer";
        private static readonly String AmbientAttribute = "ambient";

        private static readonly XPathExpression XmlMissionPath = XPathExpression.Compile("/MissionDialogue");
        private static readonly XPathExpression XmlConversationPath = XPathExpression.Compile("Conversations/Conversation");
        private static readonly XPathExpression XmlAttributes = XPathExpression.Compile("@*");

        private static readonly String MocapMarkup = "Mocap";
        private static readonly String RandomMarkup = "Random";
        private static readonly String PhoneMarkup = "Phone";

        #endregion // Constants
        
        #region Properties and associated member data

        /// <summary>
        /// The filename that this mission dialogue gets saved and was loaded from, this
        /// also sets the name of this object
        /// </summary>
        [NonSearchable()]
        public String Filename
        {
            get { return m_filename; }
            set 
            {
                SetPropertyValue(value, () => this.Filename,
                    new UndoSetterDelegate(delegate(Object newValue) { m_filename = (String)newValue; this.Name = Path.GetFileNameWithoutExtension(m_filename); }));
            }
        }
        private String m_filename;

        /// <summary>
        /// The name of this mission dialogue
        /// </summary>
        [NonSearchable()]
        public String Name
        {
            get { return m_name; }
            set 
            {
                SetPropertyValue(value, () => this.Name,
                    new UndoSetterDelegate(delegate(Object newValue) { m_name = (String)newValue; }));
            }
        }
        private String m_name;

        /// <summary>
        /// Represents whether this mission dialogue has any unsaved changes in it.
        /// </summary>
        [NonSearchable()]
        public DirtyStateManager DirtyManager
        {
            get { return m_dirtyManager; }
            set 
            {
                m_dirtyManager = value;
                OnPropertyChanged("Dirty");
            }
        }
        private DirtyStateManager m_dirtyManager;

        /// <summary>
        /// This mission ID text
        /// </summary>
        [Undoable(), Collapsable(), DisplayNameAttribute("Mission ID")]
        public String MissionId
        {
            get { return m_missionId; }
            set
            {
                String newStringValue = MissionDialogue.GetStringInRightFormat(value as String);

                if (!newStringValue.Contains(' '))
                {
                    SetPropertyValue(newStringValue, () => this.MissionId,
                        new UndoSetterDelegate(delegate(Object newValue) { m_missionId = (String)newValue; }));
                }
                else
                {
                    throw new ArgumentException("Mission ID cannot contain spaces");
                }
            }
        }
        private String m_missionId;

        /// <summary>
        /// This mission name text
        /// </summary>
        [Undoable(), Collapsable(), DisplayNameAttribute("Mission Name")]
        public String MissionName
        {
            get { return m_missionName; }
            set 
            {
                String newStringValue = MissionDialogue.GetStringInRightFormat(value as String);

                SetPropertyValue(newStringValue, () => this.MissionName,
                    new UndoSetterDelegate(delegate(Object newValue) { m_missionName = (String)newValue; }));
            }
        }
        private String m_missionName;

        /// <summary>
        /// This mission subtitle ID text
        /// </summary>
        [Undoable(), Collapsable(), DisplayNameAttribute("Subtitle Group ID")]
        public String MissionSubtitleId
        {
            get { return m_missionSubtitleId; }
            set 
            {
                String newStringValue = MissionDialogue.GetStringInRightFormat(value as String);

                SetPropertyValue(newStringValue, () => this.MissionSubtitleId,
                    new UndoSetterDelegate(delegate(Object newValue) { m_missionSubtitleId = (String)newValue; }));
            }
        }
        private String m_missionSubtitleId;

        /// <summary>
        /// Returns true if this mission represents a random event
        /// uses the m_missionType property.
        /// </summary>
        [NonSearchable()]
        public Boolean RandomEventMission
        {
            get { return m_randomEventMission; }
            set
            {
                m_randomEventMission = value;
                if (m_randomEventMission)
                {
                    this.IsMultiplayer = false;
                }
                
                OnPropertyChanged("RandomEventMission");
            }
        }
        private Boolean m_randomEventMission;

        /// <summary>
        /// The mission type for this set of dialogues.
        /// (i.e normal, random etc)
        /// </summary>
        [Undoable(), DisplayNameAttribute("Mission Type")]
        public String MissionType
        {
            get { return m_missionType; }
            set
            {
                SetPropertyValue(value, () => this.MissionType,
                    new UndoSetterDelegate(delegate(Object newValue) { m_missionType = (String)newValue; this.RandomEventMission = (m_missionType == "Random Event"); }));
            }
        }
        private String m_missionType;

        /// <summary>
        /// The list of all the conversations current in this mission dialogue
        /// </summary>
        [Undoable()]
        public ObservableCollection<Conversation> Conversations
        {
            get { return m_conversations; }
            set 
            {
                SetPropertyValue(value, () => this.Conversations,
                    new UndoSetterDelegate(delegate(Object newValue)
                    {
                        foreach (Conversation conversation in m_conversations)
                        {
                            this.m_dirtyManager.UnRegisterViewModel(conversation);
                            foreach (Line line in conversation.Lines)
                            {
                                this.m_dirtyManager.UnRegisterViewModel(line);
                                if (!string.IsNullOrEmpty(line.Filename))
                                {
                                    if (!this.DeletedFilenames.Contains(line.Filename))
                                    {
                                        this.DeletedFilenames.Add(line.Filename);
                                    }
                                }
                            }
                        }
                        m_conversations = (ObservableCollection<Conversation>)newValue;
                        foreach (Conversation conversation in m_conversations)
                        {
                            this.m_dirtyManager.RegisterViewModel(conversation);
                            foreach (Line line in conversation.Lines)
                            {
                                this.m_dirtyManager.RegisterViewModel(line);
                                if (!string.IsNullOrEmpty(line.Filename))
                                {
                                    if (this.DeletedFilenames.Contains(line.Filename))
                                    {
                                        this.DeletedFilenames.Remove(line.Filename);
                                    }
                                }
                            }
                        }
                    }
                    ));
            }
        }
        private ObservableCollection<Conversation> m_conversations;

        /// <summary>
        /// The configurations that this mission dialogue should use
        /// </summary>
        [NonSearchable()]
        public DialogueConfigurations Configurations
        {
            get { return m_configurations; }
            set { m_configurations = value; }
        }
        private DialogueConfigurations m_configurations;

        /// <summary>
        /// Represents whether this mission contains conversations that are
        /// currently locked
        /// </summary>
        [NonSearchable()]
        public Boolean HasLockedConversations
        {
            get { return m_hasLockedConversations; }
            set { m_hasLockedConversations = value; OnPropertyChanged("HasLockedConversations"); }
        }
        private Boolean m_hasLockedConversations;
        public void DetermineLockedConversations()
        {
            Boolean hasLockedConversations = false;
            foreach (Conversation conversation in this.Conversations)
            {
                if (conversation.Locked == true)
                {
                    hasLockedConversations = true;
                    break;
                }
            }
            this.HasLockedConversations = hasLockedConversations;
        }

        [Undoable(), DisplayNameAttribute("Multiplayer")]
        public bool IsMultiplayer
        {
            get { return m_isMultiplayer; }
            set
            {
                SetPropertyValue(value, () => this.IsMultiplayer,
                    new UndoSetterDelegate(delegate(Object newValue) { m_isMultiplayer = (bool)newValue; }));
            }
        }
        private bool m_isMultiplayer;

        [Undoable(), DisplayNameAttribute("Ambient")]
        public bool IsAmbient
        {
            get { return m_isAmbient; }
            set
            {
                SetPropertyValue(value, () => this.IsAmbient,
                    new UndoSetterDelegate(delegate(Object newValue) { m_isAmbient = (bool)newValue; }));
            }
        }
        private bool m_isAmbient;

        /// <summary>
        /// Gets or sets the list of filenames that have been in use in this dialog which have
        /// been deleted but still need to be used in generation logic.
        /// </summary>
        public List<string> DeletedFilenames
        {
            get { return this._deletedFilenames; }
            set { this._deletedFilenames = value; }
        }
        private List<string> _deletedFilenames;
        #endregion // Properties and associated member data

        #region Callbacks

        public delegate void MissingCharacterCallbackHandler(object sender, MissingCharacterCallbackEventArgs e);
        public MissingCharacterCallbackHandler MissingCharacterCallback { get; set; }

        #endregion // Callbacks

        #region Constructor(s)

        static MissionDialogue()
        {
            StringFormatReplacements = new Dictionary<int, String>();

            // Microsoft apostrophe
            StringFormatReplacements.Add(8211, "-");
            StringFormatReplacements.Add(8212, "-");
            StringFormatReplacements.Add(8216, "\'");
            StringFormatReplacements.Add(8217, "\'");
            StringFormatReplacements.Add(8220, "\"");
            StringFormatReplacements.Add(8221, "\"");
            StringFormatReplacements.Add(8230, "...");
        }

        /// <summary>
        /// Default constructor so that it can be created dynamically.
        /// It adds a single empty conversation and leaves the other properties empty.
        /// </summary>
        public MissionDialogue(String name, String missionName, DialogueConfigurations configurations)
        {
            if (configurations == null)
                throw new ArgumentNullException("configurations", "Cannot create a dialogue mission without giving valid configurations that it uses");

            m_configurations = configurations;

            this._deletedFilenames = new List<string>();
            this.m_filename = String.Empty;
            this.m_name = name;
            this.m_missionId = String.Empty;
            this.m_missionName = missionName;
            this.m_missionSubtitleId = String.Empty;
            this.m_missionType = Configurations.MissionTypes.First();
            this.m_conversations = new ObservableCollection<Conversation>();
            this.m_hasLockedConversations = false;
            this.m_isMultiplayer = false;
            this.m_isAmbient = false;

            this.m_dirtyManager = new DirtyStateManager();
            this.m_dirtyManager.RegisterViewModel(this);
        }

        /// <summary>
        /// Creates the mission dialogue based on the filename given.
        /// </summary>
        /// <param name="filename">The filename to create this object from</param>
        /// <param name="configurations">The configurations that will be assigned to this dialogue</param>
        public MissionDialogue(String filename, DialogueConfigurations configurations)
        {
            if (configurations == null)
                throw new ArgumentNullException("configurations", "Cannot create a dialogue mission without giving valid configurations that it uses");

            if (filename == String.Empty || filename == null)
                throw new ArgumentNullException("filename", "Cannot load a dialogue mission without giving a valid filename");

            m_configurations = configurations;

            this._deletedFilenames = new List<string>();
            this.m_filename = filename;
            this.m_name = Path.GetFileNameWithoutExtension(filename);
            this.m_missionId = String.Empty;
            this.m_missionName = String.Empty;
            this.m_missionSubtitleId = String.Empty;
            this.m_missionType = Configurations.MissionTypes.First();
            this.m_conversations = new ObservableCollection<Conversation>();
            this.m_hasLockedConversations = false;
            this.m_isMultiplayer = false;
            this.m_isAmbient = false;

            this.m_dirtyManager = new DirtyStateManager();
            this.m_dirtyManager.RegisterViewModel(this);

            this.Deserialise();
            this.DetermineLockedConversations();
        }

        #endregion // Constructor(s)

        #region Public View Function(s)

        /// <summary>
        /// Saves this file out into the filename property, if this property is empty it
        /// returns without doing anything.
        /// </summary>
        public void SaveMissionDialogue()
        {
            if (this.Filename == String.Empty) return;
            this.Serialise();
        }

        /// <summary>
        /// Saves this file out into the given filename, if this property is empty it
        /// returns without doing anything. After saving it reverts back to what the filename was before
        /// </summary>
        public void SaveMissionDialogueInTemp(String filename)
        {
            if (filename == String.Empty) return;

            String previousFilename = this.m_filename;

            this.m_filename = filename;
            this.Serialise();
            this.m_filename = previousFilename;
        }

        /// <summary>
        /// Exports this particular mission dialogue into the given text files
        /// </summary>
        /// <param name="dialogueFile">The text writer to write the non mocap conversations into</param>
        /// <param name="mocapFile">The text writer to write the mocap conversations into</param>
        /// <param name="subtitleFile">The text writer to write the subtitle data to</param>
        public Boolean Export(TextWriter dialogueFile, TextWriter mocapFile, TextWriter subtitleFile, Dictionary<String, int> characterIndices
            , ref Boolean missingSPFields
            , ref Boolean missingRoots
            , ref Boolean missingLine,
            Hardware hardwareGeneration)
        {
            subtitleFile.WriteLine("{" + this.MissionName + "}");
            subtitleFile.WriteLine("");

            mocapFile.WriteLine("{" + this.MissionName + "}");
            mocapFile.WriteLine("");

            dialogueFile.WriteLine("{" + this.MissionName + "}");
            dialogueFile.WriteLine("");

            Boolean missingCharacterId = false;
            foreach (Conversation conversation in this.Conversations)
            {
                if (String.IsNullOrEmpty(conversation.Root))
                {
                    missingRoots = true;
                }
                foreach (Line line in conversation.Lines)
                {
                    if (String.IsNullOrEmpty(line.Listener) || String.IsNullOrEmpty(line.Speaker))
                    {
                        missingSPFields = true;
                    }
                    if (String.IsNullOrEmpty(line.Filename))
                    {
                        missingLine = true;
                    }
                }
                if (conversation.Export(dialogueFile, mocapFile, subtitleFile, characterIndices, hardwareGeneration))
                {
                    missingCharacterId = true;
                }
            }
            return missingCharacterId;
        }

        public void ExportForTranslation(bool placeHolder, TextWriter dialogueFile, Dictionary<String, int> characterIndices)
        {
            dialogueFile.WriteLine("{" + this.MissionName + "}");
            dialogueFile.WriteLine("");

            foreach (Conversation conversation in this.Conversations)
            {
                if (conversation.PlaceHolder == !placeHolder)
                {
                    continue;
                }

                conversation.ExportForTranslation(dialogueFile, characterIndices);
            }
        }

        /// <summary>
        /// Outputs the subtitle id into the given text file
        /// </summary>
        /// <param name="subtitleFile">The file to write the subtitle id to</param>
        public void ExportSubtitleID(TextWriter subtitleFile)
        {
            subtitleFile.WriteLine(this.MissionSubtitleId);
        }

        /// <summary>
        /// Adds a new conversation to this dialogue.
        /// </summary>
        /// <param name="newIndex"></param>
        /// <returns>The new conversation that has been addded</returns>
        public Conversation AddNewConversation(int newIndex)
        {
            Conversation newConversation = new Conversation(this);

            ObservableCollection<Conversation> newConversations = new ObservableCollection<Conversation>(this.Conversations);

            if (newIndex >= 0)
            {
                newConversations.Insert(newIndex + 1, newConversation);
            }
            else
            {
                newConversations.Add(newConversation);
            }

            this.Conversations = newConversations;

            this.m_dirtyManager.RegisterViewModel(newConversation);
            return newConversation;
        }

        /// <summary>
        /// Adds a new conversation to this dialogue but uses the private member so that
        /// the set property function doesn't get called.
        /// </summary>
        /// <param name="newIndex"></param>
        /// <returns>The new conversation that has been addded</returns>
        public Conversation AddNewConversationPrivately(int newIndex)
        {
            Conversation newConversation = new Conversation(this);

            if (newIndex >= 0)
            {
                this.m_conversations.Insert(newIndex + 1, newConversation);
            }
            else
            {
                this.m_conversations.Add(newConversation);
            }

            this.m_dirtyManager.RegisterViewModel(newConversation);
            return newConversation;
        }

        /// <summary>
        /// Adds the given conversation into the conversation list at the given index
        /// </summary>
        /// <param name="newIndex"></param>
        /// <returns></returns>
        public Conversation AddConversation(Conversation newConversation, int newIndex)
        {
            ObservableCollection<Conversation> newConversations = new ObservableCollection<Conversation>(this.Conversations);

            if (newIndex >= 0)
            {
                newConversations.Insert(newIndex, newConversation);
            }
            else
            {
                newConversations.Add(newConversation);
            }

            this.Conversations = newConversations;

            this.m_dirtyManager.RegisterViewModel(newConversation);
            return newConversation;
        }

        /// <summary>
        /// Moves the given conversation to the new given index
        /// </summary>
        /// <param name="conversation"></param>
        /// <param name="newIndex"></param>
        public void MoveConversation(Conversation conversation, int newIndex)
        {
            int oldIndex = this.Conversations.IndexOf(conversation);
            if (oldIndex < 0)
                return;

            this.RemoveConversation(oldIndex);
            this.AddConversation(conversation, newIndex);
        }

        /// <summary>
        /// Removes the indexed conversation
        /// </summary>
        /// <param name="conversationIndex">The index of the conversation to remove</param>
        public void RemoveConversation(int conversationIndex)
        {
            if (conversationIndex < 0 || conversationIndex > this.m_conversations.Count) return;

            this.m_dirtyManager.UnRegisterViewModel(this.m_conversations[conversationIndex]);
            foreach (Line line in this.m_conversations[conversationIndex].Lines)
            {
                this.m_dirtyManager.UnRegisterViewModel(line);
            }

            ObservableCollection<Conversation> newConversations = new ObservableCollection<Conversation>(this.Conversations);

            newConversations.RemoveAt(conversationIndex);

            this.Conversations = newConversations;
        }

        public String GetNextConversationPrefix()
        {
            String conversationPrefix = "AA";
            foreach (Conversation conversation in this.Conversations)
            {
                String prefix = String.Empty;
                prefix = conversation.GetConversationPrefix();
                if (!String.IsNullOrEmpty(prefix))
                {
                    if (GetIndexFromString(prefix) >= GetIndexFromString(conversationPrefix))
                    {
                        conversationPrefix = IncrementString(prefix);
                    }
                }
            }
            return conversationPrefix;
        }

        public string GetNextConversationPrefixIncludingManual()
        {
            string conversationPrefix = "AA";
            string currentPrefix = string.Empty;
            while (true)
            {
                bool beingUsed = false;
                foreach (Conversation conversation in this.Conversations)
                {
                    foreach (Line line in conversation.Lines)
                    {
                        if (string.IsNullOrWhiteSpace(line.Filename))
                        {
                            continue;
                        }

                        string filename = line.Filename;
                        if (filename.Contains(this.MissionId))
                        {
                            filename = line.Filename.Remove(0, this.MissionId.Length);
                        }

                        currentPrefix = String.Empty;
                        if (Regex.IsMatch(filename, ".*_....$"))
                        {
                            currentPrefix = filename[filename.LastIndexOf('_') + 1].ToString() + filename[filename.LastIndexOf('_') + 2].ToString();
                        }
                        else if (Regex.IsMatch(filename, ".*_...._..$"))
                        {
                            if (filename.Length >= 7)
                            {
                                currentPrefix = filename[filename.Length - 7].ToString() + filename[filename.Length - 6].ToString();
                            }
                        }

                        if (currentPrefix == conversationPrefix)
                        {
                            beingUsed = true;
                            break;
                        }
                    }

                    if (beingUsed)
                    {
                        break;
                    }
                }

                if (!beingUsed)
                {
                    break;
                }

                conversationPrefix = IncrementString(conversationPrefix);
            }

            return conversationPrefix;
        }

        public Boolean DetermineIfRootExists(String root)
        {
            foreach (Conversation converstaion in this.Conversations)
            {
                if (String.Compare(root, converstaion.Root, true) == 0)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Returns a value representation of a string so that you can compare prefixes/postfixes
        /// </summary>
        /// <param name="String"></param>
        /// <returns></returns>
        public static int GetIndexFromString(String String)
        {
            if (String.Length == 2)
            {
                int firstValue = (((int)String[0] - 65)) * 26;
                int secondValue = (int)String[1] - 65;
                return firstValue + secondValue;
            }
            return 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="String"></param>
        /// <returns></returns>
        public static String IncrementString(String String)
        {
            if (String.Length == 2)
            {
                int firstValue = (int)String[0] - 65;
                int secondValue = (int)String[1] - 65;
                if (secondValue == 25)
                {
                    return ((char)(firstValue + 66)).ToString() + "A";
                }
                else
                {
                    return ((char)(firstValue + 65)).ToString() + ((char)(secondValue + 66)).ToString();
                }
            }
            return String;
        }

        private static readonly String[] LineBreaks = new String[2] { "\r", "\n" };

        /// <summary>
        /// Imports the given script filename by using the marksup
        /// and creates all of the conversations
        /// </summary>
        public void ImportScriptFile(String scriptFilename)
        {
            String text = this.GetTextFromDocument(scriptFilename);
            if (!String.IsNullOrEmpty(text))
            {
                List<List<String>> conversationBlocks = GetConversationBlocksFromText(text);
                List<String> missingCharacters = new List<String>();
                List<ParsedConversation> parsedConversations = GetParsedConversationsFromBlocks(conversationBlocks, ref missingCharacters);

                foreach (ParsedConversation conversation in parsedConversations)
                {
                    Conversation newConversation = new Conversation(this);
                    newConversation.Description = conversation.Header;
                    if (conversation.Markup == ConversationMarkup.MOCAP)
                        newConversation.Category = "MoCap";
                    else if (conversation.Markup == ConversationMarkup.PHONE)
                        newConversation.Category = "Cellphone";

                    if (conversation.Markup == ConversationMarkup.RANDOM)
                        newConversation.Random = true;

                    foreach (DialogueLine line in conversation.DialogueLines)
                    {
                        Boolean characterFound = false;
                        foreach (KeyValuePair<Guid, String> character in Configurations.Characters)
                        {
                            if (String.Compare(character.Value, line.Key, true) == 0)
                            {
                                Line newLine = new Line(newConversation);
                                newLine.Character = new KeyValuePair<Guid, String>(character.Key, character.Value);
                                newLine.LineDialogue = line.Value;
                                newLine.Listener = "9";
                                newConversation.Lines.Add(newLine);
                                this.m_dirtyManager.RegisterViewModel(newLine);
                                characterFound = true;
                                break;
                            }
                        }
                        if (characterFound == false)
                        {
                            Line newLine = new Line(newConversation);
                            newLine.Character = new KeyValuePair<Guid, String>(Guid.Empty, "Unknown");
                            newLine.LineDialogue = line.Value;
                            newLine.Listener = "9";
                            newConversation.Lines.Add(newLine);
                            this.m_dirtyManager.RegisterViewModel(newLine);
                            newConversation.MissingCharacters = true;
                        }
                    }
                    // Set up speaker values for the lines
                    Dictionary<String, String> speakers = new Dictionary<String, String>();
                    foreach (Line line in newConversation.Lines)
                    {
                        if (!String.IsNullOrEmpty(line.CharacterName))
                        {
                            if (!speakers.ContainsKey(line.CharacterName))
                            {
                                int speakerIndex = speakers.Count;
                                if (speakers.Count < 9)
                                {
                                    speakers.Add(line.CharacterName, speakers.Count.ToString());
                                }
                                else
                                {
                                    int speakerLetterIndex = (speakers.Count - 9) + 65;
                                    speakers.Add(line.CharacterName, ((char)speakerLetterIndex).ToString());
                                }                                
                            }

                            if (speakers.ContainsKey(line.CharacterName))
                            {
                                line.Speaker = speakers[line.CharacterName];
                            }
                        }
                    }

                    this.m_dirtyManager.RegisterViewModel(newConversation);
                    this.Conversations.Add(newConversation);
                }
                if (missingCharacters.Count > 0)
                {
                    String message = "Some lines weren't imported incorrectly due to the fact that the following characters could not be found:";
                    foreach (String missingCharacter in missingCharacters)
                    {
                        message += "\n" + missingCharacter;
                    }
                    MessageBox.Show(message, "Import Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        /// <summary>
        /// Determines where this dialogue is valid for both saving and exporting
        /// </summary>
        public Boolean IsMissionValid(ref List<String> errorMessages)
        {
            String nonRandomPattern = "^(\\S{1,5})_([A-Z]{4})$";
            String randomPattern = "^(\\S{1,5})_([A-Z]{4})$";

            Regex nonRandomRegex = new Regex(nonRandomPattern);
            Regex randomRegex = new Regex(randomPattern);

            Boolean hasError = false;
            if (!ValidateMissionID(ref errorMessages))
                hasError = true;
            if (!ValidateSubtitleGroupID(ref errorMessages))
                hasError = true;

            List<String> filenames = new List<String>();
            List<String> wavFilenames = new List<string>();
            for (int i = 0; i < this.Conversations.Count; i++)
            {
                Conversation conversation = this.Conversations[i];
                if (this.RandomEventMission == true && !String.IsNullOrEmpty(this.MissionId) && !String.IsNullOrWhiteSpace(this.MissionId))
                {
                    // Check that the conversation roots start with the Mission ID
                    if (!conversation.Root.StartsWith(this.MissionId, StringComparison.Ordinal))
                    {
                        errorMessages.Add(String.Format("Invalid dialogue file as the root parameter of conversation (index:{0}) doesn't start with the Mission ID {1}"
                            , i, this.MissionId));
                        hasError = true;
                    }
                }

                if (conversation.Category != "MoCap")
                {
                    Guid conversationCharacter = Guid.Empty;
                    Boolean conversationCharacterError = false;
                    List<int> sortedList = new List<int>();
                    for (int j = 0; j < conversation.Lines.Count; j++)
                    {
                        Line line = conversation.Lines[j];
                        if (!ValidateEmptyFilename(line.Filename, i, j, ref errorMessages))
                            hasError = true;

                        // SFX filenames can be unique and need to be removed from validation.
                        if (line.Filename.StartsWith("SFX"))
                            continue;

                        if (conversation.Random == true)
                        {
                            string actualFilename = line.Filename + "_" + String.Format("{0}", (j + 1).ToString("D2"));
                            if (!String.IsNullOrEmpty(line.Filename) && !String.IsNullOrWhiteSpace(line.Filename))
                            {
                                if (!filenames.Contains(actualFilename))
                                {
                                    filenames.Add(actualFilename);

                                    // Check filename with a _01 appended to it.
                                    string wavFilename = actualFilename;
                                    if (!wavFilenames.Contains(wavFilename))
                                    {
                                        wavFilenames.Add(wavFilename);
                                    }
                                    else
                                    {
                                        errorMessages.Add(String.Format("Invalid dialogue file as the filename ({2}) in, conversation (index:{0}), line (index:{1}) will result in the wav filename not being unique.",
                                            i, j, actualFilename));
                                        hasError = true;
                                    }
                                }
                                else
                                {
                                    errorMessages.Add(String.Format("Invalid dialogue file as the filename ({2}) in, conversation (index:{0}), line (index:{1}) is not unique to the file.",
                                        i, j, actualFilename));
                                    hasError = true;
                                }
                            }

                            // Check character
                            if (conversationCharacter == Guid.Empty)
                                conversationCharacter = line.CharacterGuid;
                            else
                            {
                                if (conversationCharacterError == false)
                                {
                                    if (conversationCharacter != line.CharacterGuid)
                                    {
                                        errorMessages.Add(String.Format("Invalid dialogue file as the conversation (index:{0}) is marked as a random conversation but the lines are said by mulitple characters.",
                                        i));
                                        hasError = true;
                                        conversationCharacterError = true;
                                    }
                                }
                            }

                            // If a conversation is random the file name needs to be using the syntax ^MissionID_([A-Z]{4})$
                            // with the digits at the end being continuous and complete
                            if (!randomRegex.IsMatch(line.Filename))
                            {
                                errorMessages.Add(String.Format("Invalid dialogue file as the filename in, conversation (index:{0}), line (index:{1}) doesn't have the correct syntax required for a random conversation, which is {2}",
                                    i, j, "^MissionID_([A-Z]{4})$"));
                                hasError = true;
                            }
                        }
                        else
                        {

                            if (!String.IsNullOrEmpty(line.Filename) && !String.IsNullOrWhiteSpace(line.Filename))
                            {
                                if (!filenames.Contains(line.Filename))
                                {
                                    filenames.Add(line.Filename);

                                    // Check filename with a _01 appended to it.
                                    string wavFilename = line.Filename;
                                    if (conversation.Random == false)
                                        wavFilename += "_01";

                                    if (!wavFilenames.Contains(wavFilename))
                                    {
                                        wavFilenames.Add(wavFilename);
                                    }
                                    else
                                    {
                                        errorMessages.Add(String.Format("Invalid dialogue file as the filename ({2}) in, conversation (index:{0}), line (index:{1}) will result in the wav filename not being unique.",
                                            i, j, line.Filename));
                                        hasError = true;
                                    }
                                }
                                else
                                {
                                    errorMessages.Add(String.Format("Invalid dialogue file as the filename ({2}) in, conversation (index:{0}), line (index:{1}) is not unique to the file.",
                                        i, j, line.Filename));
                                    hasError = true;
                                }
                            }
                        }
                    }
                }
            }

            return !hasError;
        }

        #endregion // Public View Function(s)

        #region Private Function(s)

        /// <summary>
        /// Gets the uscii text from a word document using the
        /// COM system.
        /// </summary>
        private String GetTextFromDocument(String filename)
        {
            String fullText = String.Empty;
            Object missing = System.Reflection.Missing.Value;
            Object fileName = filename;
            Object readOnly = true;
            Object falseValue = false;

            Microsoft.Office.Interop.Word.Application WordApp = null;

            Microsoft.Office.Interop.Word.Document aDoc = null;
            try
            {
                WordApp = new Microsoft.Office.Interop.Word.Application();
                WordApp.Visible = false;
                aDoc = WordApp.Documents.Open(ref fileName, ref missing, ref readOnly, ref missing, ref missing,
                    ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref falseValue);

                Microsoft.Office.Interop.Word.Range fullRange = aDoc.Range();

                Clipboard.Clear();
                aDoc.Content.Select();
                aDoc.Content.Copy();

                IDataObject data = Clipboard.GetDataObject();
                if (data.GetDataPresent(DataFormats.UnicodeText))
                {
                    fullText = data.GetData(DataFormats.UnicodeText) as String;
                }

                aDoc.Close(falseValue, missing, missing);
                WordApp.Quit(falseValue, missing, missing);
            }
            catch
            {
                if (aDoc != null)
                    aDoc.Close(falseValue, missing, missing);
                if (WordApp != null)
                    WordApp.Quit(falseValue, missing, missing);
            }

            return fullText;
        }

        /// <summary>
        /// Creates an array where each element is a conversation in the text
        /// without anything else done with it.
        /// </summary>
        private List<List<String>> GetConversationBlocksFromText(String text)
        {
            String[] lines = text.Split(MissionDialogue.LineBreaks, StringSplitOptions.None);
            List<List<String>> conversationBlocks = new List<List<String>>();
            int conversationBlockIndex = -1;
            List<String> currentConversation = new List<String>();
            foreach (String line in lines)
            {
                if (String.IsNullOrWhiteSpace(line))
                {
                    if (conversationBlockIndex == -1)
                        continue;

                    conversationBlocks[conversationBlockIndex].Add(line);
                }
                else
                {
                    if (this.StartOfConversation(line))
                    {
                        ++conversationBlockIndex;
                        conversationBlocks.Add(new List<String>());
                        conversationBlocks[conversationBlockIndex].Add(line);
                    }
                    else
                    {
                        if (conversationBlockIndex == -1)
                            continue;

                        conversationBlocks[conversationBlockIndex].Add(line);
                    }
                }
            }
            return conversationBlocks;
        }

        /// <summary>
        /// Creates the well defined parsed conversations from conversation blocks
        /// </summary>
        private List<ParsedConversation> GetParsedConversationsFromBlocks(List<List<String>> conversationBlocks, ref List<String> missingCharacters)
        {
            List<ParsedConversation> conversations = new List<ParsedConversation>();
            foreach (List<String> block in conversationBlocks)
            {
                ParsedConversation parsedConversation = new ParsedConversation();
                parsedConversation.DialogueLines = new List<DialogueLine>();
                if (block.Count > 0)
                {
                    // Get conversation header
                    parsedConversation.Header = GetConversationHeaderFromLine(block[0], "([{");
                    block.RemoveAt(0);

                    // Get conversation markup
                    parsedConversation.Markup = GetMarkupFromBlock(block);

                    // Removes invalid lines from the block making it much easier to parse
                    RemoveInvalidLinesFromBlock(block);

                    // Line dialogue should be made up of two lines, the first the character name
                    // and the second the actual dialogue
                    int totalLines = block.Count;
                    int linesChecked = 0;
                    List<String> initalBlock = new List<String>(block);

                    List<List<String>> dialogueLines = new List<List<String>>();
                    List<String> currentDialogue = new List<String>();
                    while (linesChecked < totalLines)
                    {
                        String line = String.Copy(initalBlock[linesChecked]);
                        if (!String.IsNullOrWhiteSpace(line))
                        {
                            if (!Regex.IsMatch(line, "^[(].*[)]$"))
                                currentDialogue.Add(line);
                        }
                        else
                        {
                            if (currentDialogue.Count > 1)
                            {
                                // Just take the first two lines the character name and the dialogue
                                if (currentDialogue.Count > 2)
                                {
                                    while (currentDialogue.Count > 2)
                                    {
                                        currentDialogue.RemoveAt(currentDialogue.Count - 1);
                                    }
                                }

                                dialogueLines.Add(new List<String>(currentDialogue));
                                currentDialogue.Clear();
                            }
                            else
                            {
                                currentDialogue.Clear();
                            }
                        }
                        linesChecked++;
                    }

                    AddLinesToConversation(dialogueLines, parsedConversation, ref missingCharacters);
                    conversations.Add(parsedConversation);
                }
            }
            return conversations;
        }

        /// <summary>
        /// Gets the conversation header form the given line
        /// </summary>
        private String GetConversationHeaderFromLine(String line, String exception)
        {
            if (line.StartsWith("W "))
            {
                return TrimNonAlphaNumericCharacters(line.Substring(2), exception);
            }

            return TrimNonAlphaNumericCharacters(line, exception);
        }

        /// <summary>
        /// Finds all the lines that contain [*] text and uses the first as
        /// the markup. It then removes all of them from the given block.
        /// </summary>
        private ConversationMarkup GetMarkupFromBlock(List<String> block)
        {
            ConversationMarkup markup = ConversationMarkup.NONE;

            Boolean foundMarkup = false;
            List<String> linesToRemove = new List<String>();
            int index = 0;
            foreach (String line in block)
            {
                if (line.StartsWith("[") && line.EndsWith("]"))
                {
                    if (foundMarkup == false)
                    {
                        String markUpLine = line.Substring(1, line.Length - 2);
                        if (String.Compare(markUpLine, MocapMarkup, true) == 0)
                        {
                            markup = ConversationMarkup.MOCAP;
                            foundMarkup = true;
                        }
                        else if (String.Compare(markUpLine, RandomMarkup, true) == 0)
                        {
                            markup = ConversationMarkup.RANDOM;
                            foundMarkup = true;
                        }
                        else if (String.Compare(markUpLine, PhoneMarkup, true) == 0)
                        {
                            markup = ConversationMarkup.PHONE;
                            foundMarkup = true;
                        }
                    }
                    linesToRemove.Add(line);
                }
                index++;
            }
            foreach (String lineToRemove in linesToRemove)
            {
                block.Remove(lineToRemove);
            }

            return markup;
        }

        /// <summary>
        /// Removes all the empty lines that are inbetween non empty lines and
        /// removes all non empty lines that are inbetween empty lines
        /// </summary>
        /// <param name="block"></param>
        private void RemoveInvalidLinesFromBlock(List<String> block)
        {
            int totalLines = block.Count;
            int linesChecked = 0;
            int linesRemoved = 0;
            String previousLine = String.Empty;
            List<String> initalBlock = new List<String>(block);

            // Remove all valid empty lines
            while (linesChecked < totalLines)
            {
                String line = String.Copy(initalBlock[linesChecked]);
                String nextLine = String.Empty;
                if (initalBlock.Count > linesChecked + 1)
                    nextLine = initalBlock[linesChecked + 1];

                if (!String.IsNullOrWhiteSpace(nextLine) && !String.IsNullOrWhiteSpace(previousLine) && String.IsNullOrWhiteSpace(line))
                {
                    block.RemoveAt(linesChecked - linesRemoved);
                    linesRemoved++;
                }

                previousLine = String.Copy(line);
                linesChecked++;
            }

            // Remove all valid non empty lines
            totalLines = block.Count;
            linesChecked = 0;
            linesRemoved = 0;
            previousLine = String.Empty;
            initalBlock = new List<String>(block);
            while (linesChecked < totalLines)
            {
                String line = String.Copy(initalBlock[linesChecked]);
                String nextLine = String.Empty;
                if (initalBlock.Count > linesChecked + 1)
                    nextLine = initalBlock[linesChecked + 1];

                if (String.IsNullOrWhiteSpace(nextLine) && String.IsNullOrWhiteSpace(previousLine) && !String.IsNullOrWhiteSpace(line))
                {
                    block.RemoveAt(linesChecked - linesRemoved);
                    linesRemoved++;
                }

                previousLine = String.Copy(line);
                linesChecked++;
            }

            // Finally remove all empty lines that are bunched together and
            // just have one
            totalLines = block.Count;
            linesChecked = 0;
            linesRemoved = 0;
            previousLine = String.Empty;
            initalBlock = new List<String>(block);
            Boolean firstNonEmptyLineFound = false;
            while (linesChecked < totalLines)
            {
                String line = String.Copy(initalBlock[linesChecked]);

                if (!String.IsNullOrWhiteSpace(line))
                    firstNonEmptyLineFound = true;

                if (String.IsNullOrWhiteSpace(line) && firstNonEmptyLineFound == false)
                {
                    block.RemoveAt(linesChecked - linesRemoved);
                    linesRemoved++;
                }
                else if (String.IsNullOrWhiteSpace(previousLine) && String.IsNullOrWhiteSpace(line))
                {
                    block.RemoveAt(linesChecked - linesRemoved);
                    linesRemoved++;
                }

                previousLine = String.Copy(line);
                linesChecked++;
            }
        }

        private void AddLinesToConversation(List<List<String>> dialogueLines, ParsedConversation conversation, ref List<String> missingCharacters)
        {
            foreach (List<String> dialogueLine in dialogueLines)
            {
                if (dialogueLine.Count > 1)
                {
                    String character = "Error";
                    if (LineIsCharacter(dialogueLine[0]))
                    {
                        character = Regex.Replace(dialogueLine[0], @"[\W]", "");
                    }
                    else
                    {
                        String characterName = Regex.Replace(dialogueLine[0], @"[\W]", "");
                        if (MissingCharacterCallback != null)
                        {
                            MissingCharacterCallbackEventArgs e = new MissingCharacterCallbackEventArgs(characterName);
                            MissingCharacterCallback(this, e);

                            if (LineIsCharacter(dialogueLine[0]))
                            {
                                character = characterName;
                            }
                            else
                            {
                                if (!missingCharacters.Contains(dialogueLine[0]))
                                    missingCharacters.Add(dialogueLine[0]);
                            }
                        }
                        else
                        {
                            if (!missingCharacters.Contains(dialogueLine[0]))
                                missingCharacters.Add(dialogueLine[0]);
                        }
                    }

                    String dialogue = String.Empty;
                    for (int j = 1; j < dialogueLine.Count; j++)
                        dialogue += dialogueLine[j];

                    int maxLineLength = 110;
                    if (conversation.Markup == ConversationMarkup.MOCAP)
                        maxLineLength = 60;
                    if (dialogue.Length > maxLineLength)
                    {
                        String orginalDialogue = String.Copy(dialogue);
                        int processedCharacterCount = 0;
                        while (processedCharacterCount < orginalDialogue.Length)
                        {
                            int previousLength = dialogue.Length;
                            dialogue = dialogue.Trim();
                            processedCharacterCount += previousLength - dialogue.Length;

                            int splitIndex = GetLineSplitPosition(dialogue, true, conversation);
                            processedCharacterCount += splitIndex;
                            String lineDialogue = dialogue.Substring(0, splitIndex);
                            lineDialogue = lineDialogue.TrimEnd('^');
                            lineDialogue = lineDialogue.Trim();
                            conversation.DialogueLines.Add(new DialogueLine(character, lineDialogue));
                            dialogue = dialogue.Substring(splitIndex);
                            continue;
                        }
                    }
                    else
                    {
                        conversation.DialogueLines.Add(new DialogueLine(character, dialogue));
                    }
                }
            }
        }
        
        /// <summary>
        /// Determines if a set of text starts with a alpha numeric character
        /// </summary>
        private Boolean StartsWithAlhpaNumeric(String line)
        {
            Boolean IsAlphaNumeric = Regex.IsMatch(line[0].ToString(), "^[a-zA-Z0-9]*$");
            if (IsAlphaNumeric)
            {
                return true;
            }
            return false;
        }

        private String TrimNonAlphaNumericCharacters(String line, String exception)
        {
            int trimCount = 0;
            foreach (char character in line)
            {
                Boolean IsAlphaNumeric = Regex.IsMatch(character.ToString(), "^[a-zA-Z0-9]*$");
                if (!IsAlphaNumeric)
                {
                    if (exception.Contains(character))
                    {
                        break;
                    }
                    else
                    {
                        trimCount++;
                        continue;
                    }
                }
                break;
            }
            return line.Substring(trimCount);
        }

        /// <summary>
        /// Returns true if the given line is the start of a new conversation
        /// </summary>
        private Boolean StartOfConversation(String line)
        {
            if (line.Length == 0)
                return false;

            if (!StartsWithAlhpaNumeric(line))
            {
                if (line[0] != '[' && line[0] != '{' && line[0] != '‘' && line[0] != '\'' && line[0] != '"')
                {
                    if (line.StartsWith("(") && line.EndsWith(")"))
                        return false;

                    return true;
                }
            }
            else if (line.StartsWith("W "))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Checks to see if the given line starts with a valid character name
        /// </summary>
        private Boolean LineIsCharacter(String line)
        {
            Boolean characterFound = false;
            String characterName = Regex.Replace(line, @"[\W]", "");
            foreach (KeyValuePair<Guid, String> character in Configurations.Characters)
            {
                if (String.Compare(character.Value, characterName, true) == 0)
                {
                    characterFound = true;
                }
            }

            return characterFound;
        }

        /// <summary>
        /// Takes the given line and returns a int that represents where the line needs to be
        /// split based on a set of rules.
        /// </summary>
        private int GetLineSplitPosition(String line, Boolean nextLine, ParsedConversation conversation)
        {
            int maxLineLength = 110;
            if (conversation.Markup == ConversationMarkup.MOCAP)
                maxLineLength = 60;

            if (line.Length < maxLineLength)
            {
                return line.Length;
            }
            else
            {
                String splitLine = line.Substring(0, maxLineLength);
                if (splitLine.Length < maxLineLength)
                {
                    return splitLine.Length;
                }
                else
                {
                    Boolean fullstopFound = false;
                    Boolean specialMarkerFound = false;
                    int punMarkIndex = splitLine.IndexOf('^');
                    if (punMarkIndex == -1)
                    {
                        punMarkIndex = splitLine.LastIndexOf('.');
                        if (punMarkIndex == -1)
                        {
                            punMarkIndex = splitLine.LastIndexOf("…");
                            if (punMarkIndex == -1)
                            {
                                punMarkIndex = splitLine.LastIndexOf('"');
                                if (punMarkIndex == -1)
                                {
                                    punMarkIndex = splitLine.LastIndexOf(',');
                                    if (punMarkIndex == -1)
                                    {
                                        if (line[maxLineLength] == ' ')
                                            punMarkIndex = maxLineLength;
                                        else if (line[maxLineLength] == '^')
                                        {
                                            punMarkIndex = maxLineLength;
                                            specialMarkerFound = true;
                                        }
                                        else
                                            punMarkIndex = splitLine.LastIndexOf(' ');
                                    }
                                }
                            }
                        }
                        else
                        {
                            fullstopFound = true;
                        }
                    }
                    else
                    {
                        specialMarkerFound = true;
                    }

                    if (punMarkIndex != -1)
                    {
                        if (nextLine == true && fullstopFound == false && specialMarkerFound == false)
                        {
                            int nextLineSplit = GetLineSplitPosition(line.Substring(punMarkIndex + 1), false, conversation);
                            if (nextLineSplit < 20)
                            {
                                return punMarkIndex + 1 + nextLineSplit;
                            }
                        }
                        return punMarkIndex + 1;
                    }
                    else
                    {
                        return line.Length;
                    }
                }
            }
        }

        /// <summary>
        /// Makes sure that the line contains a even number of quotation marks or none.
        /// </summary>
        private String ValidateDialogueLine(String line)
        {
            String result = String.Copy(line);
            int quatationCount = line.Count(c => c == '"');
            if (quatationCount != 0)
            {
                if (quatationCount % 2 != 0)
                {
                    // We have a odd number of quatation marks add one to either the start or end.
                    if (line[0] == '"')
                        result += '"';
                    else if (line[line.Length - 1] == '"')
                        result.Insert(0, "\"");
                    else
                    {

                    }
                }
            }
            return line;
        }

        /// <summary>
        /// Serialises the mission dialogue out to the filename
        /// property as a xml making sure that it has the current version
        /// attached to it.
        /// </summary>
        private void Serialise()
        {
            // Create the xml file and add a header to it.
            XmlDocument xmlDoc = new XmlDocument();
            XmlDeclaration xmlDecl = xmlDoc.CreateXmlDeclaration("1.0", "utf-8", String.Empty);
            xmlDoc.AppendChild(xmlDecl);

            // Add the root node with the current version in it.
            XmlElement rootNode = xmlDoc.CreateElement("MissionDialogue");
            rootNode.SetAttribute(VersionAttribute, ExpectedVersion);
            rootNode.SetAttribute(MissionIdAttribute, this.MissionId);
            rootNode.SetAttribute(MissionNameAttribute, this.MissionName);
            rootNode.SetAttribute(MissionSubtitleIdAttribute, this.MissionSubtitleId);
            rootNode.SetAttribute(TypeAttribute, this.MissionType);
            rootNode.SetAttribute(MultiplayerAttribute, this.IsMultiplayer.ToString());
            rootNode.SetAttribute(AmbientAttribute, this.IsAmbient.ToString());
            xmlDoc.InsertBefore(xmlDecl, xmlDoc.DocumentElement);

            // Serialise all of the conversations that are in this mission dialogue
            XmlElement conversationsRootNode = xmlDoc.CreateElement("Conversations");
            foreach (Conversation conversation in this.Conversations)
            {
                conversation.SaveConversation(xmlDoc, conversationsRootNode);
            }
            rootNode.AppendChild(conversationsRootNode);

            // Serialise all the deleted filenames that theis dialogue mission has
            XmlElement deletedRootNode = xmlDoc.CreateElement("DeletedFilenames");
            foreach (string deletedFilename in this.DeletedFilenames)
            {
                XmlNode deletedNode = xmlDoc.CreateElement("Filename");
                XmlText text = xmlDoc.CreateTextNode(deletedFilename);
                deletedNode.AppendChild(text);
                deletedRootNode.AppendChild(deletedNode);
            }
            rootNode.AppendChild(deletedRootNode);


            xmlDoc.AppendChild(rootNode);

            // Just before saving just make sure the file sn't read only (if it exists) it should have been checked out, but you never know
            FileInfo fileinfo = new FileInfo(this.Filename);
            if (fileinfo != null && fileinfo.IsReadOnly && File.Exists(this.Filename))
            {
                File.SetAttributes(this.Filename, FileAttributes.Normal);
            }
            xmlDoc.Save(this.Filename);
        }

        /// <summary>
        /// Deserialise the mission dialogue from the filename
        /// property
        /// </summary>
        /// <returns>This returns true if it has either failed or succeed in loading a new version dstar file
        /// it returns false if it has tried to load a old version dstar file.</returns>
        private Boolean Deserialise()
        {
            if (!File.Exists(this.Filename))
            {
                MessageBox.Show("Unable to find the dstar file located at " + this.Filename,
                    "File Loading Error", MessageBoxButton.OK, MessageBoxImage.Error);
                RSG.Base.Logging.Log.Log__Warning("Unable to open the dstar file located at " + this.Filename);
                return true;
            }

            XmlDocument document = new XmlDocument();
            try
            {
                document.Load(this.Filename);
            }
            catch (XmlException ex)
            {
                RSG.Base.Logging.Log.Log__Exception(ex, "Unable to open the dstar file located at " + this.Filename);
                return true;
            }

            XPathNavigator navigator = document.CreateNavigator();
            XPathNodeIterator missionIterator = navigator.Select(XmlMissionPath);

            if (missionIterator.Count == 0)
            {
                // This probably means that we are trying to load a old format dstar file.
                return false;
            }

            XPathNavigator missionNavigator = missionIterator.Current;
            XPathNodeIterator missionAttrIterator = missionNavigator.Select(XPathExpression.Compile("/MissionDialogue/@*"));

            // Collect the attributes for the mission
            while (missionAttrIterator.MoveNext())
            {
                if (typeof(String) != missionAttrIterator.Current.ValueType)
                    continue;
                String value = (missionAttrIterator.Current.TypedValue as String);

                if (missionAttrIterator.Current.Name == MissionIdAttribute)
                {
                    this.m_missionId = value;
                }
                else if (missionAttrIterator.Current.Name == MissionNameAttribute)
                {
                    this.m_missionName = value;
                }
                else if (missionAttrIterator.Current.Name == MissionSubtitleIdAttribute)
                {
                    this.m_missionSubtitleId = value;
                }
                else if (missionAttrIterator.Current.Name == TypeAttribute)
                {
                    this.m_missionType = value;
                    this.m_randomEventMission = (this.m_missionType == "Random Event");
                }
                else if (missionAttrIterator.Current.Name == MultiplayerAttribute)
                {
                    this.m_isMultiplayer = bool.Parse(value);
                }
                else if (missionAttrIterator.Current.Name == AmbientAttribute)
                {
                    this.m_isAmbient = bool.Parse(value);
                }
            }

            // Load all of the conversations
            XPathNodeIterator conversationIterator = missionNavigator.Select(XPathExpression.Compile("MissionDialogue/Conversations/Conversation"));
            while (conversationIterator.MoveNext())
            {
                XPathNavigator conversationNavigator = conversationIterator.Current;
                Conversation newConversation = new Conversation(conversationNavigator, this);
                this.m_dirtyManager.RegisterViewModel(newConversation);
                this.Conversations.Add(newConversation);
            }

            // Load all of the deleted filenames
            XPathNodeIterator deleteFilenameIterator = missionNavigator.Select(XPathExpression.Compile("MissionDialogue/DeletedFilenames/Filename"));
            while (deleteFilenameIterator.MoveNext())
            {
                XPathNavigator currentNavigator = deleteFilenameIterator.Current;
                this._deletedFilenames.Add(currentNavigator.Value);
            }

            return true;
        }
        #endregion // Private Function(s)

        #region Private Validation Functions

        private Boolean ValidateMissionID(ref List<String> errorMessages)
        {
            Boolean hasError = false;
            if (String.IsNullOrEmpty(this.MissionId))
            {
                errorMessages.Add("Invalid dialogue file as no Mission ID is set.");
                hasError = true;
            }
            else if (String.IsNullOrWhiteSpace(this.MissionId))
            {
                errorMessages.Add("Invalid dialogue file as Mission ID is set to a blank value.");
                hasError = true;
            }

            return !hasError;
        }

        private Boolean ValidateSubtitleGroupID(ref List<String> errorMessages)
        {
            Boolean hasError = false;
            if (String.IsNullOrEmpty(this.MissionSubtitleId))
            {
                errorMessages.Add("Invalid dialogue file as no Subtitle Group ID is set.");
                hasError = true;
            }
            else if (String.IsNullOrWhiteSpace(this.MissionSubtitleId))
            {
                errorMessages.Add("Invalid dialogue file as Subtitle Group ID is set to a blank value.");
                hasError = true;
            }

            return !hasError;
        }

        private Boolean ValidateEmptyFilename(String filename, int convIndex, int lineIndex, ref List<String> errorMessages)
        {
            Boolean hasError = false;
            if (String.IsNullOrEmpty(filename))
            {
                errorMessages.Add(String.Format("Invalid dialogue file as, conversation (index:{0}), line (index:{1}) has a empty filename.", convIndex, lineIndex));
                hasError = true;
            }
            else if (String.IsNullOrWhiteSpace(filename))
            {
                errorMessages.Add(String.Format("Invalid dialogue file as, conversation (index:{0}), line (index:{1}) has a empty filename.", convIndex, lineIndex));
                hasError = true;
            }

            return !hasError;
        }

        #endregion // Private Validation Functions
    } // MissionDialog
} // RSG.Model.Dialogue
