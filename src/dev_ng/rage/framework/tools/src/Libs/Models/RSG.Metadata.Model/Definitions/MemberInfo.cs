﻿//---------------------------------------------------------------------------------------------
// <copyright file="MemberInfo.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions
{
    using System.Collections.Generic;

    /// <summary>
    /// Contains information for a collection of members for a single structure definition,
    /// including the members order indices.
    /// </summary>
    public class MemberInfo : Dictionary<string, MemberOrderInfo>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MemberInfo"/> class.
        /// </summary>
        public MemberInfo()
        {
        }
        #endregion Constructors
    } // RSG.Metadata.Model.Definitions.MemberInfo {Structure}
} // RSG.Metadata.Model.Definitions {Namespace}
