﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Map.Statistics
{
    /// <summary>
    /// This represents a statistic class that has got references in it. So either a map container or prop group.
    /// </summary>
    public interface IReferences
    {
        #region Properties

        /// <summary>
        /// Contains the reference statistics, indexed first by source filename, then by reference ref name, and contains
        /// both the instance count and the prop statistics
        /// </summary>
        SectionReferences References { get; set; }

        /// <summary>
        /// The maximum draw distance for all the references
        /// </summary>
        float MaxReferenceDistance { get; set; }

        #endregion // Properties

        #region Methods

        /// <summary>
        /// Goes through all the references and adds statistics for them
        /// </summary>
        /// <param name="allReferences"></param>
        void ResolveReferences(Dictionary<String, Dictionary<String, SingleReference>> allReferences);

        #endregion // Methods

    }

}
