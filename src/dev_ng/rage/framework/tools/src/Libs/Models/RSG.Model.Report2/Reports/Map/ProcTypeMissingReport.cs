﻿using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Tasks;
using RSG.Bounds;
using RSG.Model.Common;
using RSG.Model.Common.Extensions;
using RSG.Model.Common.Map;
using RSG.Base.ConfigParser;
using Ionic.Zip;
using System.Web.UI;
using System;
using RSG.Base.Logging;
using System.Diagnostics;
using RSG.SceneXml;

namespace RSG.Model.Report.Reports.Map
{
    /// <summary>
    /// List all the proctypes and the sections they are/are not used in.
    /// </summary>
    public class ProcTypeMissingReport : HTMLReport, IDynamicLevelReport
    {        
        #region Constants

        private const string c_name = "ProcType Missing Report";
        private const string c_description = "List all the proctypes and the sections they are/are not used in.";
        private const string c_metaFile = "common:/data/materials/procedural.meta";
        private const string c_boundZipExtension = ".ibn.zip";
        private const string c_boundFileExtension = ".bnd";

        #endregion

        #region Private member fields

        private IDictionary<string, HashSet<IMapSection>> m_typeMap;
        private SortedDictionary<string, MetadataType> m_metadataTypes;
        private ITask m_task;

        #endregion

        #region Properties

        /// <summary>
        /// Task.
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (m_task == null)
                {
                    CompositeTask task = new CompositeTask("Generate Report");
                    task.AddSubTask(new ActionTask("Loading data", (context, progress) => EnsureDataLoaded((DynamicLevelReportContext)context, progress)), 50);
                    task.AddSubTask(new ActionTask("Generating report", (context, progress) => GenerateReport((DynamicLevelReportContext)context, progress)));
                    m_task = task;
                }
                return m_task;
            }
        }

        /// <summary>
        /// List of stats that are required to generate this report.
        /// </summary>
        public IEnumerable<StreamableStat> RequiredStats
        {
            get { return null; }
        }

        #endregion 

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        public ProcTypeMissingReport()
            : base(c_name, c_description)
        {

        }

        #endregion

        #region Methods
        /// <summary>
        /// Ensure that the data has been loaded.
        /// </summary>
        /// <param name="context">Context.</param>
        /// <param name="progress">Progress.</param>
        private void EnsureDataLoaded(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            bool metaResult = GetTypesFromMeta(context, c_metaFile);
            if (!metaResult)
            {
                Debug.Assert(metaResult == true, "Unable to generate report with no metadata data.");
                return;
            }

            IMapHierarchy hierarchy = context.Level.GetMapHierarchy();
            if (hierarchy == null)
            {
                Debug.Assert(hierarchy != null, "Unable to generate report with a invalid map hierarchy");
                return;
            }

            this.GatherProceduralTypeInformation(context, progress, hierarchy);
        }

        /// <summary>
        /// Generate the HTML report.
        /// </summary>
        /// <param name="context">Context.</param>
        /// <param name="progress">Progress.</param>
        private void GenerateReport(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            List<string> unused = new List<string>();
            foreach (MetadataType type in this.m_metadataTypes.Values)
            {
                bool beingUsed = false;
                foreach (string additionalName in type.AdditionalLookupNames)
                {
                    if (m_typeMap.ContainsKey(additionalName))
                    {
                        beingUsed = true;
                        continue;
                    }
                }

                if (beingUsed == false && !m_typeMap.ContainsKey(type.Name))
                {
                    unused.Add(type.Name);
                }
            }

            List<string> missing = new List<string>();
            foreach (KeyValuePair<string, HashSet<IMapSection>> kvp in this.m_typeMap)
            {
                bool found = false;
                foreach (MetadataType type in this.m_metadataTypes.Values)
                {
                    if (type.Name == kvp.Key)
                    {
                        found = true;
                        break;
                    }

                    foreach (string additionalName in type.AdditionalLookupNames)
                    {
                        if (additionalName == kvp.Key)
                        {
                            found = true;
                            break;
                        }
                    }

                    if (found)
                    {
                        break;
                    }
                }

                if (!found)
                {
                    string msg = kvp.Key + " - ";
                    foreach (IMapSection section in kvp.Value)
                    {
                        msg += section.Name + ",";
                    }

                    missing.Add(msg);
                }
            }

            double unusedPercentage = ((double)unused.Count / (double)this.m_metadataTypes.Count) * 100.0;

            MemoryStream stream = new MemoryStream();
            StreamWriter streamWriter = new StreamWriter(stream);
            HtmlTextWriter writer = new HtmlTextWriter(streamWriter);

            writer.RenderBeginTag(HtmlTextWriterTag.Html);
            writer.RenderBeginTag(HtmlTextWriterTag.Head);
            writer.RenderEndTag();
            writer.RenderBeginTag(HtmlTextWriterTag.Body);

            writer.RenderBeginTag("h1");
            writer.Write("Summary");
            writer.RenderEndTag();

            writer.RenderBeginTag("h3");
            writer.Write("Number of Defined Procedual Types: {0}", this.m_metadataTypes.Count);
            writer.Write("<br />");
            writer.Write("Number of Unused Procedual Types: {0}, {1:0.00}%", unused.Count, unusedPercentage);
            writer.Write("<br />");
            writer.Write("Number of Missing Procedual Types: {0}", missing.Count);
            writer.RenderEndTag();

            writer.RenderBeginTag("h1");
            writer.Write("Unused ProcTypes");
            writer.RenderEndTag();
            writer.RenderBeginTag("h3");
            writer.Write("These are the procedual types defined in the metadata but are not being used in any of the map data.");
            writer.RenderEndTag();

            foreach (string key in unused)
            {
                writer.Write(key);
                writer.Write("<br />");
            }

            writer.RenderBeginTag("h1");
            writer.Write("Missing ProcTypes");
            writer.RenderEndTag();
            writer.RenderBeginTag("h3");
            writer.Write("These are the procedual types being used in the map data but are not defined in the metadata.");
            writer.RenderEndTag();

            foreach (string key in missing)
            {
                writer.Write(key);
                writer.Write("<br />");
            }

            writer.RenderBeginTag("h1");
            writer.Write("ProcType Usage");
            writer.RenderEndTag();

            writer.AddAttribute(HtmlTextWriterAttribute.Width, "80%");
            writer.AddAttribute(HtmlTextWriterAttribute.Border, "1");
            writer.RenderBeginTag(HtmlTextWriterTag.Table);

            writer.RenderBeginTag(HtmlTextWriterTag.Thead);
            writer.RenderBeginTag(HtmlTextWriterTag.Tr);

            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.RenderBeginTag(HtmlTextWriterTag.B);
            writer.Write("ProcType");
            writer.RenderEndTag();
            writer.RenderEndTag();

            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.RenderBeginTag(HtmlTextWriterTag.B);
            writer.Write("Number of Sections");
            writer.RenderEndTag();
            writer.RenderEndTag();

            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.RenderBeginTag(HtmlTextWriterTag.B);
            writer.Write("Sections");
            writer.RenderEndTag();
            writer.RenderEndTag();

            writer.RenderEndTag();
            writer.RenderEndTag();

            foreach (var key in this.m_metadataTypes.Keys)
            {
                if (m_typeMap.ContainsKey(key))
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.Tr);

                    writer.AddAttribute(HtmlTextWriterAttribute.Valign, "top");
                    writer.RenderBeginTag(HtmlTextWriterTag.Td);
                    writer.Write(key);
                    writer.RenderEndTag();

                    writer.AddAttribute(HtmlTextWriterAttribute.Valign, "top");
                    writer.RenderBeginTag(HtmlTextWriterTag.Td);
                    writer.Write(m_typeMap[key].Count);
                    writer.RenderEndTag();

                    writer.AddAttribute(HtmlTextWriterAttribute.Valign, "top");
                    writer.RenderBeginTag(HtmlTextWriterTag.Td);

                    SortedList<string, string> sections = new SortedList<string, string>();
                    foreach (IMapSection section in m_typeMap[key])
                    {
                        sections.Add(section.Name, section.Name);
                    }

                    foreach (string s in sections.Keys)
                    {
                        writer.Write(s);
                        writer.Write("<br />");
                    }

                    writer.RenderEndTag();
                    writer.RenderEndTag();
                }
            }

            writer.RenderEndTag();

            writer.RenderEndTag(); // Body
            writer.Flush();

            stream.Position = 0;
            Stream = stream;
        }

        /// <summary>
        /// Gather up the procedural type information.
        /// </summary>
        /// <param name="context">Context.</param>
        /// <param name="progress">Progress.</param>
        /// <param name="sections">Sections.</param>
        private void GatherProceduralTypeInformation(DynamicLevelReportContext context, IProgress<TaskProgress> progress, IMapHierarchy hierarchy)
        {
            IDictionary<string, HashSet<IMapSection>> proceduralTypeMap = new Dictionary<string, HashSet<IMapSection>>();

            int total = hierarchy.AllSections.Count();
            int current = 0;

            foreach (IMapSection section in hierarchy.AllSections)
            {
                ContentNodeMapZip zipNode = context.GameView.Content.Root.FindAll(section.Name, "mapzip").FirstOrDefault() as ContentNodeMapZip;
                string filename = Path.GetFullPath(zipNode.Filename);
                if (zipNode == null || !File.Exists(filename))
                {
                    continue;
                }

                Scene scene = new Scene(Path.ChangeExtension(filename, ".xml"), SceneXml.LoadOptions.Objects, true);
                foreach (TargetObjectDef objectDef in scene.Walk(Scene.WalkMode.BreadthFirst))
                {
                    if (!objectDef.Is2dfxProcObject())
                    {
                        continue;
                    }

                    string proceduralType = objectDef.GetAttribute<string>(AttrNames.TWODFX_PROC_OBJECT, null);
                    if (proceduralType != null)
                    {
                        HashSet<IMapSection> sections;
                        if (!proceduralTypeMap.TryGetValue(proceduralType, out sections))
                        {
                            sections = new HashSet<IMapSection>();
                            proceduralTypeMap.Add(proceduralType, sections);
                        }

                        sections.Add(section);
                    }
                }

                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.CreateNoWindow = true;
                startInfo.UseShellExecute = false;
                startInfo.FileName = Path.Combine(context.GameView.ToolsBinDir, "ReportGenerator", "BNDZipHelper.exe");
                startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                startInfo.Arguments = string.Format("--input \"{0}\"", filename);
                startInfo.RedirectStandardOutput = true;

                try
                {
                    // Start the process with the info we specified.
                    // Call WaitForExit and then the using statement will close.
                    using (Process p = Process.Start(startInfo))
                    {
                        p.Start();
                        string output = p.StandardOutput.ReadToEnd();
                        if (!string.IsNullOrEmpty(output))
                        {
                            string[] types = output.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                            foreach (string proceduralType in types)
                            {
                                HashSet<IMapSection> sections;
                                if (!proceduralTypeMap.TryGetValue(proceduralType, out sections))
                                {
                                    sections = new HashSet<IMapSection>();
                                    proceduralTypeMap.Add(proceduralType, sections);
                                }

                                sections.Add(section);
                            }
                        }

                        p.WaitForExit();
                    }
                }
                catch (Exception ex)
                {
                    Log.Log__Exception(
                        ex, "Unable to load the {0} processed zip file.", zipNode.Filename);
                }

                ++current;
                progress.Report(new TaskProgress((float)current / (float)total, "Processing " + section.Name));
            }

            m_typeMap = proceduralTypeMap;
        }

        /// <summary>
        /// Get the procedural types from the meta data.
        /// </summary>
        /// <param name="context">Context.</param>
        /// <param name="metaFile">Meta file.</param>
        /// <returns>True if successful.</returns>
        private bool GetTypesFromMeta(DynamicLevelReportContext context, string metaFile)
        {
            this.m_metadataTypes = new SortedDictionary<string,MetadataType>();
            string filepath = c_metaFile.Replace("common:", context.GameView.CommonDir);
            if (!File.Exists(filepath))
            {
                return false;
            }

            XDocument xmlDoc = XDocument.Load(filepath);
            foreach (XElement itemElem in xmlDoc.XPathSelectElements("/CProceduralInfo/procObjInfos/Item"))
            {
                XElement tag = itemElem.Element("Tag");
                XElement modelName = itemElem.Element("ModelName");
                if (tag == null)
                {
                    continue;
                }

                string tagValue = tag.Value.ToLower();
                if (this.m_metadataTypes.ContainsKey(tagValue))
                {
                    continue;
                }

                this.m_metadataTypes.Add(tagValue, new MetadataType() { Name = tagValue });
            }

            foreach (XElement itemElem in xmlDoc.XPathSelectElements("/CProceduralInfo/plantInfos/Item"))
            {
                XElement tag = itemElem.Element("Tag");
                XElement modelName = itemElem.Element("ModelName");
                if (tag == null)
                {
                    continue;
                }

                string tagValue = tag.Value.ToLower();
                if (this.m_metadataTypes.ContainsKey(tagValue))
                {
                    continue;
                }

                this.m_metadataTypes.Add(tagValue, new MetadataType() { Name = tagValue });
            }

            foreach (XElement itemElem in xmlDoc.XPathSelectElements("/CProceduralInfo/procTagTable/Item"))
            {
                XElement name = itemElem.Element("name");
                if (name == null)
                {
                    continue;
                }

                string nameValue = name.Value.ToLower();
                if (nameValue == "null")
                {
                    continue;
                }

                XElement procObjTag = itemElem.Element("procObjTag");
                if (procObjTag != null)
                {
                    string procObjTagValue = procObjTag.Value.ToLower();
                    if (!string.IsNullOrEmpty(procObjTagValue))
                    {
                        MetadataType type;
                        if (this.m_metadataTypes.TryGetValue(procObjTagValue, out type))
                        {
                            if (type.Name != nameValue)
                            {
                                type.AdditionalLookupNames.Add(nameValue);
                            }
                        }
                    }
                }

                XElement plantTag = itemElem.Element("plantTag");
                if (plantTag != null)
                {
                    string plantTagValue = plantTag.Value.ToLower();
                    if (!string.IsNullOrEmpty(plantTagValue))
                    {
                        MetadataType type;
                        if (this.m_metadataTypes.TryGetValue(plantTagValue, out type))
                        {
                            if (type.Name != nameValue)
                            {
                                type.AdditionalLookupNames.Add(nameValue);
                            }
                        }
                    }
                }
            }

            return true;
        }
        #endregion Mathods

        private class MetadataType
        {
            private List<string> _additionalLookupNames;

            public string Name
            {
                get;
                set;
            }

            public List<string> AdditionalLookupNames
            {
                get { return this._additionalLookupNames; }
            }

            public MetadataType()
            {
                _additionalLookupNames = new List<string>();
            }
        }
    }
}
