﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Diagnostics;

namespace RSG.Model.Statistics.Captures.Historical
{
    /// <summary>
    /// Wrapper for historical cpu based results.
    /// </summary>
    [DataContract]
    [Serializable]
    public class HistoricalCpuResults
    {
        #region Properties
        /// <summary>
        /// Mapping of set names to set results.
        /// </summary>
        [DataMember]
        public IDictionary<String, HistoricalCpuSetResult> PerSetResults { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public HistoricalCpuResults()
        {
            PerSetResults = new SortedDictionary<String, HistoricalCpuSetResult>();
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        ///
        /// </summary>
        /// <param name="context"></param>
        /// <param name="result"></param>
        public void AddResult(Object context, CpuResult result)
        {
            if (!PerSetResults.ContainsKey(result.Set))
            {
                PerSetResults[result.Set] = new HistoricalCpuSetResult(result.Set);
            }

            PerSetResults[result.Set].AddResult(context, result);
        }
        #endregion // Public Methods
    } // HistoricalCpuResults

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    public class HistoricalCpuSetResult
    {
        #region Properties
        /// <summary>
        /// Name of the set.
        /// </summary>
        [DataMember]
        public String SetName { get; set; }

        /// <summary>
        /// Mapping of metric names to historical results.
        /// </summary>
        [DataMember]
        public IDictionary<String, HistoricalResults> PerMetricResults { get; private set; }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public HistoricalCpuSetResult(String setName)
        {
            SetName = setName;
            PerMetricResults = new SortedDictionary<String, HistoricalResults>();
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="result"></param>
        public void AddResult(Object context, CpuResult result)
        {
            Debug.Assert(result.Set == SetName, "Set names don't match.");

            if (!PerMetricResults.ContainsKey(result.Name))
            {
                PerMetricResults[result.Name] = new HistoricalResults();
            }

            PerMetricResults[result.Name].AddResult(context, result.Average, result.StandardDeviation);
        }
        #endregion // Public Methods
    } // HistoricalCpuSetResult
}
