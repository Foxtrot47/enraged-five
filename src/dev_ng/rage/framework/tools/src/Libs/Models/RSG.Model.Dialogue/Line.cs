﻿using System;
using System.IO;
using System.Windows;
using System.Xml;
using System.Xml.XPath;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using RSG.Model.Dialogue.UndoManager;
using RSG.Model.Dialogue.UndoInterface;
using System.Globalization;
using System.Windows.Data;
using System.Collections.ObjectModel;

namespace RSG.Model.Dialogue
{
    public class Line : UndoRedoBase
    {
        #region Constants
        private static readonly String CharacterGuidAttribute = "guid";
        private static readonly String DialogueAttribute = "dialogue";
        private static readonly String VolumeAttribute = "volume";
        private static readonly String SpeakerAttribute = "speaker";
        private static readonly String ListenerAttribute = "listener";
        private static readonly String AudioAttribute = "audio";
        private static readonly String FilenameAttribute = "filename";
        private static readonly String ManualFilenameAttribute = "manual";
        private static readonly String SpecialAttribute = "special";
        private static readonly String RecordedAttribute = "recorded";
        private static readonly String SentToRecordedAttribute = "sent";
        private static readonly String TimestampAttribute = "timestamp";
        private static readonly String SubtitledAttribute = "subtitled";
        private static readonly String InterruptibleAttribute = "interruptible";
        private static readonly String DontInterruptForSpecialAbilityAttribute = "dontInterruptForSpecialAbility";
        private static readonly String DucksScoreAttribute = "ducks_score";
        private static readonly String DucksRadioAttribute = "ducks_radio";
        private static readonly String AudibilityAttribute = "audibility";
        private static readonly String HeadsetSubmixAttribute = "headset_submix";
        private static readonly String ControllerPadSpeakerAttribute = "controller_pad_speaker";

        private static readonly XPathExpression XmlAttributes = XPathExpression.Compile("@*");

        private static readonly Dictionary<string, int> AudibilityValues = new Dictionary<string, int>()
        {
            { "Normal", 0 },
            { "Clear", 1 },
            { "Critical", 2 },
            { "LeadIn", 3 },
            { "Ambient", 4 }
        };
        #endregion // Constants

        #region Properties and associated member data

        /// <summary>
        /// Character that says the line, the options for this variable is in the
        /// configurations. Setting this will automatically set the character guid/name as well.
        /// </summary>
        [Undoable(), NonSearchable()]
        public KeyValuePair<Guid, String> Character
        {
            get { return m_character; }
            set
            {
                DateTime previoustime = m_timestamp;
                if (m_character.Key == Guid.Empty)
                {
                    m_character = value;
                    this.CharacterName = m_character.Value;
                    this.CharacterGuid = m_character.Key;
                    OnPropertyChanged("Character");
                    if (this.Conversation.Random == true)
                    {
                        foreach (Line line in this.Conversation.Lines)
                        {
                            if (line != this)
                            {
                                line.m_character = value;
                                line.OnPropertyChanged(new PropertyChangedEventArgs("Character"));
                            }
                        }
                    }
                }
                else
                {
                    SetPropertyValue(value, () => this.Character,
                        new UndoSetterDelegate(delegate(Object newValue)
                            { 
                                m_character = (KeyValuePair<Guid, String>)newValue; 
                                this.CharacterName = m_character.Value;
                                this.CharacterGuid = m_character.Key; 
                                this.Conversation.MissingCharacters = true; 
                                this.Timestamp = previoustime;

                                bool checkSpeaker = true;
                                foreach (Line line in this.Conversation.Lines)
                                {
                                    if (line != this)
                                    {
                                        if (this.Conversation.Random == true)
                                        {
                                            line.m_character = (KeyValuePair<Guid, String>)newValue;
                                            line.m_characterGuid = ((KeyValuePair<Guid, String>)newValue).Key;
                                            line.m_characterName = ((KeyValuePair<Guid, String>)newValue).Value;
                                            line.OnPropertyChanged(new PropertyChangedEventArgs("Character"));
                                        }

                                        if (checkSpeaker && (line.m_characterGuid == this.m_characterGuid))
                                        {
                                            this.Speaker = line.m_speaker;
                                            checkSpeaker = false;
                                        }
                                    }
                                }
                                this.Conversation.OnLineCharacterChanged();
                                if (((KeyValuePair<Guid, String>)newValue).Key == this.Conversation.Mission.Configurations.SfxIdentifier)
                                {
                                    this.ManualFilename = true;
                                }
                                else
                                {
                                    this.ManualFilename = false;
                                }
                            }));

                    this.Timestamp = DateTime.Now;
                }


            }
        }
        private KeyValuePair<Guid, String> m_character;

        /// <summary>
        /// The character name as it appears in the config file
        /// </summary>
        [DisplayNameAttribute("Character Name")]
        public String CharacterName
        {
            get { return m_characterName; }
            set
            {
                DateTime previoustime = m_timestamp;
                SetPropertyValue(value, () => this.CharacterName,
                    new UndoSetterDelegate(delegate(Object newValue) { m_characterName = (String)newValue; //this.Timestamp = previoustime;
                        this.Conversation.MissingCharacters = true; }));

                //this.Timestamp = DateTime.Now;
            }
        }
        private String m_characterName;

        /// <summary>
        /// The guid that cooresponds to the character name
        /// this is so that the character name can be renamed without having to replace all the strings.
        /// </summary>
        [NonSearchable()]
        public Guid CharacterGuid
        {
            get { return m_characterGuid; }
            set
            {
                DateTime previoustime = m_timestamp;
                SetPropertyValue(value, () => this.CharacterGuid,
                    new UndoSetterDelegate(delegate(Object newValue) { m_characterGuid = (Guid)newValue; //this.Timestamp = previoustime;
                        this.Conversation.MissingCharacters = true; }));

                //this.Timestamp = DateTime.Now;
            }
        }
        private Guid m_characterGuid;

        /// <summary>
        /// The actual dialogue said by the character for this line
        /// </summary>
        [Undoable(), Collapsable(), DisplayNameAttribute("Line Dialogue")]
        public String LineDialogue
        {
            get { return m_lineDialogue; }
            set 
            {
                String newStringValue = MissionDialogue.GetStringInRightFormat(value as String);

                DateTime previoustime = m_timestamp;
                SetPropertyValue(newStringValue, () => this.LineDialogue,
                    new UndoSetterDelegate(delegate(Object newValue) { m_lineDialogue = (String)newValue; this.Timestamp = previoustime; }));

                this.Timestamp = DateTime.Now;
            }
        }
        private String m_lineDialogue;

        /// <summary>
        /// The volume setting of this line, the options fof this variable is in the
        /// configurations.
        /// </summary>
        [Undoable()]
        public String Volume
        {
            get { return m_volume; }
            set
            {
                DateTime previoustime = m_timestamp;
                SetPropertyValue(value, () => this.Volume,
                    new UndoSetterDelegate(delegate(Object newValue) { m_volume = (String)newValue; this.Timestamp = previoustime; }));

                this.Timestamp = DateTime.Now;
            }
        }
        private String m_volume;

        /// <summary>
        /// Represents the index of the speaker for the line can, be in the range [0-9]
        /// </summary>
        [Undoable()]
        public String Speaker
        {
            get { return m_speaker; }
            set
            {
                value = (value as String).ToUpper();
                DateTime previoustime = m_timestamp;
                SetPropertyValue(value, () => this.Speaker,
                    new UndoSetterDelegate(delegate(Object newValue) { m_speaker = (String)newValue; this.Timestamp = previoustime; }));

                this.Timestamp = DateTime.Now;
            }
        }
        private String m_speaker;

        /// <summary>
        /// Represents the index of the listener for the line, can be in the range [0-9]
        /// </summary>
        [Undoable()]
        public String Listener
        {
            get { return m_listener; }
            set
            {
                value = (value as String).ToUpper();
                DateTime previoustime = m_timestamp;
                SetPropertyValue(value, () => this.Listener,
                    new UndoSetterDelegate(delegate(Object newValue) { m_listener = (String)newValue; this.Timestamp = previoustime; }));

                this.Timestamp = DateTime.Now;
            }
        }
        private String m_listener;

        /// <summary>
        /// Represents the auto type for the line
        /// </summary>
        [Undoable(), DisplayNameAttribute("Audio Type")]
        public String AudioType
        {
            get { return m_audioType; }
            set
            {
                DateTime previoustime = m_timestamp;
                SetPropertyValue(value, () => this.AudioType,
                    new UndoSetterDelegate(delegate(Object newValue) { m_audioType = (String)newValue; this.Timestamp = previoustime; }));

                this.Timestamp = DateTime.Now;
            }
        }
        private String m_audioType;

        /// <summary>
        /// Represents the filename for this line. This is the filename that
        /// the recordind is actually in, this has a lot of rules surrounding it
        /// and can be either manually entered or automatically generated.
        /// </summary>
        [Undoable(), Collapsable()]
        public String Filename
        {
            get { return m_filename; }
            set
            {
                String newStringValue = MissionDialogue.GetStringInRightFormat(value as String);

                DateTime previoustime = m_timestamp;
                SetPropertyValue(newStringValue, () => this.Filename,
                    new UndoSetterDelegate(delegate(Object newValue)
                        {
                            m_filename = (String)newValue;
                            this.OnPropertyChanged("FilenameWithLineIndex"); 
                            this.Timestamp = previoustime;
                        }));

                this.Timestamp = DateTime.Now;
                this.OnFilenameChanged();
            }
        }
        private String m_filename;
        public void ResetFilename()
        {
            this.m_filename = "";
        }

        /// <summary>
        /// Represents the filename for this line. This is the filename that
        /// the recordind is actually in, this has a lot of rules surrounding it
        /// and can be either manually entered or automatically generated.
        /// </summary>
        [NonSearchable()]
        public String FilenameWithLineIndex
        {
            get { return m_filename + "_" + (this.Conversation.Lines.IndexOf(this) + 1).ToString("D2"); }
        }
        private string m_filenameWithLineIndex;

        /// <summary>
        /// This represents whether the line has a manually entered filename
        /// or not. As default it doesn't
        /// </summary>
        [DisplayNameAttribute("Manual Filename")]
        public Boolean ManualFilename
        {
            get { return m_manualFilename; }
            set
            {
                DateTime previoustime = m_timestamp;
                SetPropertyValue(value, () => this.ManualFilename,
                    new UndoSetterDelegate(delegate(Object newValue) { m_manualFilename = (Boolean)newValue; this.Timestamp = previoustime; }));

                this.Timestamp = DateTime.Now;
            }
        }
        private Boolean m_manualFilename;

        /// <summary>
        /// Represents the specialvaraible for the line, this is a way to set whether
        /// this line has a special effect on it and can represent that situation the 
        /// line is played in.
        /// </summary>
        [Undoable()]
        public String Special
        {
            get { return m_special; }
            set
            {
                DateTime previoustime = m_timestamp;
                SetPropertyValue(value, () => this.Special,
                    new UndoSetterDelegate(delegate(Object newValue) { m_special = (String)newValue; this.Timestamp = previoustime; }));

                this.Timestamp = DateTime.Now;
            }
        }
        private String m_special;

        /// <summary>
        /// 
        /// </summary>
        [Undoable()]
        public Boolean SentToBeRecorded
        {
            get { return m_sentToBeRecorded; }
            set
            {
                if (m_sentToBeRecorded == value)
                    return;

                if (String.IsNullOrEmpty(this.Filename))
                {
                    MessageBox.Show("Unable to set this flag as the line needs a filename before doing so!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                if (m_sentToBeRecorded == false && value == true)
                {
                    MessageBoxResult result = MessageBox.Show("Setting this flag will lock down the filename, with this set you will not be able to either manually " +
                        "enter a filename or reset it using the auto generation.\nAre you sure you wish to continue?", "Warning", 
                        MessageBoxButton.OKCancel, MessageBoxImage.Warning);

                    if (result == MessageBoxResult.OK)
                    {
                        DateTime previoustime = m_timestamp;
                        SetPropertyValue(value, () => this.SentToBeRecorded,
                            new UndoSetterDelegate(delegate(Object newValue) { m_sentToBeRecorded = (Boolean)newValue; this.Timestamp = previoustime; }));

                        this.Timestamp = DateTime.Now;
                    }
                }
                else if (m_sentToBeRecorded == true && value == false)
                {
                    MessageBoxResult result = MessageBox.Show("Un setting this flag will mean that the filename will be allowed to change by either manually " +
                        "entering a filename or resetting it using the auto generation.\nAre you sure you wish to continue?", "Warning", 
                        MessageBoxButton.OKCancel, MessageBoxImage.Warning);

                    if (result == MessageBoxResult.OK)
                    {
                        DateTime previoustime = m_timestamp;
                        SetPropertyValue(value, () => this.SentToBeRecorded,
                            new UndoSetterDelegate(delegate(Object newValue) { m_sentToBeRecorded = (Boolean)newValue; this.Timestamp = previoustime; }));

                        this.Timestamp = DateTime.Now;
                    }
                }
            }
        }
        private Boolean m_sentToBeRecorded;

        /// <summary>
        /// Represents whether this line has been recorded or not.
        /// </summary>
        [Undoable()]
        public Boolean Recorded
        {
            get { return m_recorded; }
            set
            {
                DateTime previoustime = m_timestamp;
                SetPropertyValue(value, () => this.Recorded,
                    new UndoSetterDelegate(delegate(Object newValue) { m_recorded = (Boolean)newValue; this.Timestamp = previoustime; }));

                this.Timestamp = DateTime.Now;
            }
        }
        private Boolean m_recorded;

        /// <summary>
        /// Represents whether this line is to be always subtitled
        /// (i.e whether the ~z~ string gets outputted on export)
        /// </summary>
        [Undoable()]
        public Boolean AlwaysSubtitled
        {
            get { return m_alwaysSubtitled; }
            set
            {
                DateTime previoustime = m_timestamp;
                SetPropertyValue(value, () => this.AlwaysSubtitled,
                    new UndoSetterDelegate(delegate(Object newValue) { m_alwaysSubtitled = (Boolean)newValue; this.Timestamp = previoustime; }));

                this.Timestamp = DateTime.Now;
            }
        }
        private Boolean m_alwaysSubtitled;

        /// <summary>
        /// </summary>
        [Undoable(), DisplayNameAttribute("Line Interruptible")]
        public Boolean Interruptible
        {
            get { return m_interruptible; }
            set
            {
                SetPropertyValue(value, () => this.Interruptible,
                    new UndoSetterDelegate(delegate(Object newValue) { m_interruptible = (Boolean)newValue; }));
            }
        }
        private Boolean m_interruptible;

        /// <summary>
        /// </summary>
        [Undoable(), DisplayNameAttribute("Dont Interrupted For Special Ability")]
        public Boolean DontInterruptForSpecialAbility
        {
            get { return m_dontInterruptForSpecialAbility; }
            set
            {
                SetPropertyValue(value, () => this.DontInterruptForSpecialAbility,
                    new UndoSetterDelegate(delegate(Object newValue) { m_dontInterruptForSpecialAbility = (Boolean)newValue; }));
            }
        }
        private Boolean m_dontInterruptForSpecialAbility;

        /// <summary>
        /// </summary>
        [Undoable(), DisplayNameAttribute("Ducks Score")]
        public Boolean DucksScore
        {
            get { return m_ducksScore; }
            set
            {
                SetPropertyValue(value, () => this.DucksScore,
                    new UndoSetterDelegate(delegate(Object newValue) { m_ducksScore = (Boolean)newValue; }));
            }
        }
        private Boolean m_ducksScore;

        /// <summary>
        /// </summary>
        [Undoable(), DisplayNameAttribute("Ducks Radio")]
        public Boolean DucksRadio
        {
            get { return m_ducksRadio; }
            set
            {
                SetPropertyValue(value, () => this.DucksRadio,
                    new UndoSetterDelegate(delegate(Object newValue) { m_ducksRadio = (Boolean)newValue; }));
            }
        }
        private Boolean m_ducksRadio;

        /// <summary>
        /// The conversation object that this line belongs to.
        /// </summary>
        [NonSearchable()]
        public Conversation Conversation
        {
            get { return m_conversation; }
            set { m_conversation = value; OnPropertyChanged("Conversation"); }
        }
        private Conversation m_conversation;

        /// <summary>
        /// A timestamp that displays the last time the line was modified.
        /// </summary>
        [NonSearchable()]
        public DateTime Timestamp
        {
            get { return m_timestamp; }
            set 
            {
                SetPropertyValue(value, () => this.Timestamp,
                    new UndoSetterDelegate(delegate(Object newValue) { m_timestamp = (DateTime)newValue; }));
            }
        }
        private DateTime m_timestamp;

        /// <summary>
        /// This represents whether the line has a manually entered filename
        /// or not. As default it doesn't
        /// </summary>
        [Undoable(), DisplayNameAttribute("Has Error")]
        public Boolean HasFilenameError
        {
            get { return m_hasError; }
            set
            {
                DateTime previoustime = m_timestamp;
                SetPropertyValue(value, () => this.HasFilenameError,
                    new UndoSetterDelegate(delegate(Object newValue) { m_hasError = (Boolean)newValue; this.Timestamp = previoustime; }));

                this.Timestamp = DateTime.Now;
            }
        }
        private Boolean m_hasError;

        /// <summary>
        /// </summary>
        [Undoable(), DisplayNameAttribute("Audibility")]
        public int Audibility
        {
            get { return m_audibility; }
            set
            {
                DateTime previoustime = m_timestamp;
                SetPropertyValue(value, () => this.Audibility,
                    new UndoSetterDelegate(delegate(Object newValue) { m_audibility = (int)newValue; this.Timestamp = previoustime; }));

                this.Timestamp = DateTime.Now;
            }
        }
        private int m_audibility;

        /// <summary>
        /// </summary>
        [Undoable(), DisplayNameAttribute("Headset Submix")]
        public bool HeadsetSubmix
        {
            get { return m_headsetSubmix; }
            set
            {
                DateTime previoustime = m_timestamp;
                SetPropertyValue(value, () => this.HeadsetSubmix,
                    new UndoSetterDelegate(delegate(Object newValue) { m_headsetSubmix = (bool)newValue; this.Timestamp = previoustime; }));

                this.Timestamp = DateTime.Now;
            }
        }
        private bool m_headsetSubmix;

        /// <summary>
        /// </summary>
        [Undoable(), DisplayNameAttribute("Controller Pad Speaker")]
        public bool ControllerPadSpeaker
        {
            get { return m_controllerPadSpeaker; }
            set
            {
                DateTime previoustime = m_timestamp;
                SetPropertyValue(value, () => this.ControllerPadSpeaker,
                    new UndoSetterDelegate(delegate(Object newValue) { m_controllerPadSpeaker = (bool)newValue; this.Timestamp = previoustime; }));

                this.Timestamp = DateTime.Now;
            }
        }
        private bool m_controllerPadSpeaker;

        /// <summary>
        /// The list of all the characters that can be used in this mission dialogue making
        /// sure the already used characters are at the top.
        /// </summary>
        [NonSearchable()]
        public ObservableCollection<CharacterValue> AvailableCharacters
        {
            get
            {
                if (availableCharacters == null)
                {
                    availableCharacters = new ObservableCollection<CharacterValue>();
                    List<Guid> currentUsedCharacters = new List<Guid>();
                    foreach (Line line in this.Conversation.Lines)
                    {
                        if (!currentUsedCharacters.Contains(line.CharacterGuid))
                            currentUsedCharacters.Add(line.CharacterGuid);
                    }
                    foreach (KeyValuePair<Guid, string> character in this.Conversation.Mission.Configurations.Characters)
                    {
                        if (currentUsedCharacters.Contains(character.Key))
                        {
                            availableCharacters.Add(new CharacterValue(new KeyValuePair<Guid, string>(character.Key, character.Value), true));
                        }
                        else
                        {
                            availableCharacters.Add(new CharacterValue(new KeyValuePair<Guid, string>(character.Key, character.Value), false));
                        }
                    }
                }
                return availableCharacters;
            }
        }
        internal ObservableCollection<CharacterValue> availableCharacters;

        [NonSearchable()]
        public ICollectionView AvailableCharactersCollection
        {
            get
            {
                if (availableCharactersCollection == null)
                {
                    availableCharactersCollection = CollectionViewSource.GetDefaultView(AvailableCharacters);
                    availableCharactersCollection.SortDescriptions.Add(new SortDescription("IsUsed", ListSortDirection.Descending));
                    availableCharactersCollection.SortDescriptions.Add(new SortDescription("Name", ListSortDirection.Ascending));
                    availableCharactersCollection.GroupDescriptions.Add(new PropertyGroupDescription("IsUsed"));
                }
                return availableCharactersCollection;
            }
        }
        private ICollectionView availableCharactersCollection;

        [NonSearchable()]
        public Boolean IsSelected
        {
            get { return m_selected; }
            set
            {
                SetPropertyValue(value, () => this.IsSelected,
                    new UndoSetterDelegate(delegate(Object newValue) { m_selected = (Boolean)newValue; }));
            }
        }
        private Boolean m_selected;
        #endregion // Properties and associated member data

        #region Constructor(s)

        /// <summary>
        /// The basic constructor that only takes a conversation that it belongs to. You cannnot create a line
        /// without giving a parent conversation.
        /// </summary>
        /// <param name="conversation">The conversation this line belongs to.</param>
        public Line(Conversation conversation)
        {
            if (conversation == null)
                throw new ArgumentNullException("conversation", "Cannot create a line without giving a valid conversation that it belongs to");

            this.m_conversation = conversation;

            this.m_character = conversation.Mission.Configurations.Characters.First(); //this.Conversation.Mission.Configurations.Characters.First();
            this.m_characterGuid = conversation.Mission.Configurations.Characters.First().Key; //this.Conversation.Mission.Configurations.Characters.First().Key;
            this.m_characterName = conversation.Mission.Configurations.Characters.First().Value; //this.Conversation.Mission.Configurations.Characters.First().Value;
            this.m_lineDialogue = String.Empty;
            this.m_volume = conversation.Mission.Configurations.VolumeSettings[0]; //this.Conversation.Mission.Configurations.VolumeSettings[0];
            this.m_speaker = "0";
            this.m_listener = "0";
            this.m_audioType = conversation.Mission.Configurations.AudioTypes[0]; //this.Conversation.Mission.Configurations.AudioTypes[0];
            this.m_filename = String.Empty;
            this.m_manualFilename = false;
            this.m_special = conversation.Mission.Configurations.Specials[0]; //this.Conversation.Mission.Configurations.Specials[0];
            this.m_recorded = false;
            this.m_sentToBeRecorded = false;
            this.m_alwaysSubtitled = false;
            this.m_timestamp = DateTime.Now;
            this.m_interruptible = true;
            this.m_dontInterruptForSpecialAbility = false;
            this.m_ducksScore = false;
            this.m_ducksRadio = true;
            this.m_audibility = 0;
            this.m_headsetSubmix = false;
            this.m_controllerPadSpeaker = false;

            foreach (Line line in conversation.Lines)
            {
                if (line.m_characterGuid == this.m_characterGuid)
                {
                    this.m_speaker = line.m_speaker;
                }
            }

            // But a callback into the character configuration changing
            //this.Conversation.Mission.Configurations.CharactersChanged += new EventHandler(Configurations_CharactersChanged);

            conversation.Mission.Configurations.CharactersChanged += new EventHandler(Configurations_CharactersChanged);
        }

        /// <summary>
        /// The basic constructor that only takes a conversation that it belongs to. You cannnot create a line
        /// without giving a parent conversation.
        /// </summary>
        /// <param name="conversation">The conversation this line belongs to.</param>
        public Line(Conversation conversation, Line other)
        {
            if (conversation == null)
                throw new ArgumentNullException("conversation", "Cannot create a line without giving a valid conversation that it belongs to");

            this.m_conversation = conversation;

            this.m_character = other.m_character;
            this.m_characterGuid = other.m_characterGuid;
            this.m_characterName = other.m_characterName;
            this.m_lineDialogue = other.m_lineDialogue;
            this.m_volume = other.m_volume;
            this.m_speaker = other.m_speaker;
            this.m_listener = other.m_listener;
            this.m_audioType = other.m_audioType;
            this.m_filename = String.Empty;
            this.m_manualFilename = false;
            this.m_special = other.m_special;
            this.m_recorded = other.m_recorded;
            this.m_sentToBeRecorded = other.m_sentToBeRecorded;
            this.m_alwaysSubtitled = other.m_alwaysSubtitled;
            this.m_timestamp = DateTime.Now;
            this.m_interruptible = other.m_interruptible;
            this.m_dontInterruptForSpecialAbility = other.m_dontInterruptForSpecialAbility;
            this.m_ducksScore = other.m_ducksScore;
            this.m_ducksRadio = other.m_ducksRadio;
            this.m_audibility = other.m_audibility;
            this.m_headsetSubmix = other.m_headsetSubmix;
            this.m_controllerPadSpeaker = other.m_controllerPadSpeaker;

            conversation.Mission.Configurations.CharactersChanged += new EventHandler(Configurations_CharactersChanged);
        }

        /// <summary>
        /// Loads a line from a xml document
        /// </summary>
        /// <param name="navigator">The navigator for this object in the xml file</param>
        /// <param name="mission">The conversation this line belongs to</param>
        public Line(XPathNavigator navigator, Conversation conversation)
        {
            if (conversation == null)
                throw new ArgumentNullException("conversation", "Cannot create a line without giving a valid conversation that it belongs to");
            
            if (navigator == null)
                throw new ArgumentNullException("navigator", "Cannot load a line without giving a valid navigator");

            this.m_conversation = conversation;

            this.m_character = conversation.Mission.Configurations.Characters.First(); //this.Conversation.Mission.Configurations.Characters.First();
            this.m_characterGuid = conversation.Mission.Configurations.Characters.First().Key; //this.Conversation.Mission.Configurations.Characters.First().Key;
            this.m_characterName = conversation.Mission.Configurations.Characters.First().Value; //this.Conversation.Mission.Configurations.Characters.First().Value;
            this.m_lineDialogue = String.Empty;
            this.m_volume = conversation.Mission.Configurations.VolumeSettings[0]; //this.Conversation.Mission.Configurations.VolumeSettings[0];
            this.m_speaker = "0";
            this.m_listener = "0";
            this.m_audioType = conversation.Mission.Configurations.AudioTypes[0]; //this.Conversation.Mission.Configurations.AudioTypes[0];
            this.m_filename = String.Empty;
            this.m_manualFilename = false;
            this.m_special = conversation.Mission.Configurations.Specials[0]; //this.Conversation.Mission.Configurations.Specials[0];
            this.m_recorded = false;
            this.m_sentToBeRecorded = false;
            this.m_alwaysSubtitled = false;
            this.m_timestamp = DateTime.Now;
            this.m_interruptible = true;
            this.m_dontInterruptForSpecialAbility = false;
            this.m_ducksScore = false;
            this.m_ducksRadio = true;
            this.m_audibility = 0;
            this.m_headsetSubmix = false;
            this.m_controllerPadSpeaker = false;

            // But a callback into the character configuration changing
            //this.Conversation.Mission.Configurations.CharactersChanged += new EventHandler(Configurations_CharactersChanged);

            conversation.Mission.Configurations.CharactersChanged += new EventHandler(Configurations_CharactersChanged);

            this.Deserialise(navigator);
        }
        #endregion // Constructor(s)

        #region Public Function(s)

        /// <summary>
        /// Saves a line out to the given parentNode.
        /// </summary>
        /// <param name="xmlDoc">The xml doc the conversation is being saved to</param>
        /// <param name="parentNode">The parent node the conversation should be added to</param>
        public void SaveLine(XmlDocument xmlDoc, XmlElement parentNode)
        {
            this.SerialiseLine(xmlDoc, parentNode);
        }

        #endregion // Public Function(s)

        #region Private Function(s)

        /// <summary>
        /// Called whenever the character data changes this is because depending on what has just happen
        /// the character might not be present any more, might be called something different, might have merged etc
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Configurations_CharactersChanged(Object sender, EventArgs e)
        {
            foreach (KeyValuePair<Guid, String> character in Conversation.Mission.Configurations.Characters) //this.Conversation.Mission.Configurations.Characters)
            {
                if (character.Key == this.CharacterGuid)
                {
                    // the guid still exists so it's probably just been renamed
                    // You should not be allowed to undo this change
                    this.m_character = character;
                    this.m_characterGuid = character.Key;
                    this.m_characterName = character.Value;
                    return;
                }
            }

            // The guid doesn't exist anymore so just set the name to empty
            // This shouldn't be allowed to be undone but the dirty state needs setting if this is changing
            if (this.m_characterGuid != Guid.Empty)
            {
                this.Conversation.Mission.DirtyManager.Dirty = true;
            }
            this.m_characterGuid = Guid.Empty;
            this.m_characterName = String.Empty;
            this.m_character = new KeyValuePair<Guid, String>(m_characterGuid, m_characterName);

        }

        /// <summary>
        /// Saves a line out to the given parentNode.
        /// </summary>
        /// <param name="xmlDoc">The xml doc the conversation is being saved to</param>
        /// <param name="parentNode">The parent node the conversation should be added to</param>
        private void SerialiseLine(XmlDocument xmlDoc, XmlElement parentNode)
        {
            // Create the element node
            XmlElement lineNode = xmlDoc.CreateElement("Line");

            // Add the attributes to it.
            lineNode.SetAttribute(CharacterGuidAttribute, this.Character.Key.ToString());
            lineNode.SetAttribute(DialogueAttribute, this.LineDialogue);
            lineNode.SetAttribute(VolumeAttribute, this.Volume);
            lineNode.SetAttribute(SpeakerAttribute, this.Speaker);
            lineNode.SetAttribute(ListenerAttribute, this.Listener);
            lineNode.SetAttribute(AudioAttribute, this.AudioType);
            lineNode.SetAttribute(FilenameAttribute, this.Filename);
            lineNode.SetAttribute(ManualFilenameAttribute, this.ManualFilename.ToString());
            lineNode.SetAttribute(SpecialAttribute, this.Special);
            lineNode.SetAttribute(RecordedAttribute, this.Recorded.ToString());
            lineNode.SetAttribute(SentToRecordedAttribute, this.SentToBeRecorded.ToString());
            lineNode.SetAttribute(SubtitledAttribute, this.AlwaysSubtitled.ToString());
            lineNode.SetAttribute(TimestampAttribute, this.Timestamp.ToString(CultureInfo.CreateSpecificCulture("en-GB")));
            lineNode.SetAttribute(InterruptibleAttribute, this.Interruptible.ToString());
            lineNode.SetAttribute(DontInterruptForSpecialAbilityAttribute, this.DontInterruptForSpecialAbility.ToString());
            lineNode.SetAttribute(DucksScoreAttribute, this.DucksScore.ToString());
            lineNode.SetAttribute(DucksRadioAttribute, this.DucksRadio.ToString());
            lineNode.SetAttribute(AudibilityAttribute, this.Audibility.ToString());
            lineNode.SetAttribute(HeadsetSubmixAttribute, this.HeadsetSubmix.ToString());
            lineNode.SetAttribute(ControllerPadSpeakerAttribute, this.ControllerPadSpeaker.ToString());
            
            // Add it to the root node
            parentNode.AppendChild(lineNode);
        }

        /// <summary>
        /// Deserialise the line from the given navigator
        /// property
        /// </summary>
        private void Deserialise(XPathNavigator navigator)
        {
            XPathNodeIterator lineAttrIt = navigator.Select(XmlAttributes);
            while (lineAttrIt.MoveNext())
            {
                if (typeof(String) != lineAttrIt.Current.ValueType)
                    continue;
                String value = (lineAttrIt.Current.TypedValue as String);

                if (lineAttrIt.Current.Name == CharacterGuidAttribute)
                {
                    Guid guid = Guid.Empty;
                    Guid.TryParse(value, out guid);

                    this.m_characterGuid = guid;
                    this.m_characterName = Conversation.Mission.Configurations.GetCharacterFromGuid(m_characterGuid);

                    this.m_character = new KeyValuePair<Guid, String>(this.m_characterGuid, this.m_characterName);
                }
                else if (lineAttrIt.Current.Name == DialogueAttribute)
                {
                    this.m_lineDialogue = MissionDialogue.GetStringInRightFormat(value);
                }
                else if (lineAttrIt.Current.Name == VolumeAttribute)
                {
                    this.m_volume = value;
                }
                else if (lineAttrIt.Current.Name == SpeakerAttribute)
                {
                    this.m_speaker = value;
                }
                else if (lineAttrIt.Current.Name == ListenerAttribute)
                {
                    this.m_listener = value;
                }
                else if (lineAttrIt.Current.Name == AudioAttribute)
                {
                    this.m_audioType = value;
                }
                else if (lineAttrIt.Current.Name == FilenameAttribute)
                {
                    this.m_filename = value;
                }
                else if (lineAttrIt.Current.Name == ManualFilenameAttribute)
                {
                    this.m_manualFilename = (value == "True" ? true : false);
                }
                else if (lineAttrIt.Current.Name == SpecialAttribute)
                {
                    this.m_special = value;
                }
                else if (lineAttrIt.Current.Name == RecordedAttribute)
                {
                    this.m_recorded = (value == "True" ? true : false);
                }
                else if (lineAttrIt.Current.Name == SentToRecordedAttribute)
                {
                    this.m_sentToBeRecorded = (value == "True" ? true : false);
                }
                else if (lineAttrIt.Current.Name == SubtitledAttribute)
                {
                    this.m_alwaysSubtitled = (value == "True" ? true : false);
                }
                else if (lineAttrIt.Current.Name == InterruptibleAttribute)
                {
                    this.m_interruptible = (value == "True" ? true : false);
                }
                else if (lineAttrIt.Current.Name == DontInterruptForSpecialAbilityAttribute)
                {
                    this.m_dontInterruptForSpecialAbility = (value == "True" ? true : false);
                }
                else if (lineAttrIt.Current.Name == DucksScoreAttribute)
                {
                    this.m_ducksScore = (value == "True" ? true : false);
                }
                else if (lineAttrIt.Current.Name == DucksRadioAttribute)
                {
                    this.m_ducksRadio = (value == "True" ? true : false);
                }
                else if (lineAttrIt.Current.Name == AudibilityAttribute)
                {
                    this.m_audibility = int.Parse(value);
                }
                else if (lineAttrIt.Current.Name == HeadsetSubmixAttribute)
                {
                    this.m_headsetSubmix = bool.Parse(value);
                }
                else if (lineAttrIt.Current.Name == ControllerPadSpeakerAttribute)
                {
                    this.m_controllerPadSpeaker = bool.Parse(value);
                }
                else if (lineAttrIt.Current.Name == TimestampAttribute)
                {
                    this.m_timestamp = DateTime.Now;
                    DateTime time = DateTime.Now;
                    if (DateTime.TryParse(value, CultureInfo.CreateSpecificCulture("en-GB"), DateTimeStyles.None, out time))
                    {
                        this.m_timestamp = time;
                    }
                    else
                    {
                        if (DateTime.TryParse(value, CultureInfo.CreateSpecificCulture("en-US"), DateTimeStyles.None, out time))
                        {
                            this.m_timestamp = time;
                        }
                    }
                }

                if (this.CharacterGuid != this.Conversation.Mission.Configurations.SfxIdentifier)
                {
                    this.m_manualFilename = false;
                }
                else
                {
                    this.m_manualFilename = true;
                }
            }
        }

        /// <summary>
        /// Gets called when the filename changes
        /// </summary>
        private void OnFilenameChanged()
        {
            this.Conversation.FilenameErrors = false;
            foreach (Line line in this.Conversation.Lines)
            {
                line.HasFilenameError = false;
            }

            foreach (Line line in this.Conversation.Lines)
            {
                if (!line.IsFilenameValid())
                {
                    this.Conversation.FilenameErrors = true;
                    line.HasFilenameError = true;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Boolean IsFilenameValid()
        {
            if (this.Conversation.Category == "MoCap")
            {
                return true;
            }

            if (String.IsNullOrEmpty(this.Filename) || String.IsNullOrWhiteSpace(this.Filename))
                return false;

            if (this.ManualFilename == false)
            {
                // Check syntax
                String pattern = "^(\\S{1,5})_([A-Z]{4})$";
                if (!System.Text.RegularExpressions.Regex.IsMatch(this.Filename, pattern))
                {
                    return false;
                }

                // Check uniqueness
                MissionDialogue mission = this.Conversation.Mission;
                int count = 0;
                foreach (Conversation conversation in mission.Conversations)
                {
                    if (this.Conversation.Random && conversation == this.Conversation)
                    {
                        continue;
                    }

                    foreach (Line line in conversation.Lines)
                    {
                        if (String.Compare(line.Filename, this.Filename, false) == 0)
                        {
                            count++;
                        }
                    }
                }

                if (count > 1)
                {
                    return false;
                }
            }
            else
            {
                // Check uniqueness
                MissionDialogue mission = this.Conversation.Mission;
                int count = 0;
                if (!this.Filename.StartsWith("SFX"))
                {
                    foreach (Conversation conversation in mission.Conversations)
                    {
                        foreach (Line line in conversation.Lines)
                        {
                            if (String.Compare(line.Filename, this.Filename, false) == 0)
                            {
                                count++;
                            }
                        }
                    }
                }

                if (count > 1)
                    return false;
            }

            return true;
        }

        internal void OnLineCharacterChanged(List<Guid> currentUsedCharacters)
        {
            foreach (CharacterValue character in this.AvailableCharacters)
            {
                character.IsUsed = false;
                if (currentUsedCharacters.Contains(character.Id))
                {
                    character.IsUsed = true;
                }
            }
            
            AvailableCharactersCollection.Refresh();

            this.Conversation.UsedCharacters.Clear();
            foreach (Line line in this.Conversation.Lines)
            {
                if (!this.Conversation.UsedCharacters.Contains(line.Character.Value))
                {
                    this.Conversation.UsedCharacters.Add(line.Character.Value);
                }
            }
        }
        #endregion // Private Function(s)

    } // Line
} // RSG.Model.Dialogue
