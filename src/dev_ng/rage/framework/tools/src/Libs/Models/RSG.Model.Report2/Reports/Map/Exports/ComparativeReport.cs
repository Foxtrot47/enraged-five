﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Report;
using RSG.Base.ConfigParser;
using RSG.Statistics.Common.Dto.ExportStats;
using RSG.Statistics.Common.Config;
using System.ServiceModel;
using RSG.Statistics.Client.ServiceClients;
using System.ServiceModel.Description;
using RSG.Base.Logging;
using System.IO;
using RSG.Base.Tasks;

namespace RSG.Model.Report.Reports.Map.Exports
{
    /// <summary>
    /// 
    /// </summary>
    public class ComparativeReport : CSVReport, IDynamicReport
    {
        #region Constants
        /// <summary>
        /// Report name/description.
        /// </summary>
        private const String c_name = "Comparative Export Stat Report";
        private const String c_description = "Report showing comparisons of section export statistics on a per user basis.";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Task to run for generating this report.
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (m_task == null)
                {
                    m_task = new ActionTask("Generating report", (context, progress) => GenerateReport((DynamicReportContext)context, progress));
                }
                return m_task;
            }
        }
        private ITask m_task;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="desc"></param>
        public ComparativeReport()
            : base(c_name, c_description)
        {
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// Generate the report dynamically.
        /// </summary>
        private void GenerateReport(DynamicReportContext context, IProgress<TaskProgress> progress)
        {
            ComparativeExportStatDtos comparativeExportStatsDto = null;

            try
            {
                IStatisticsConfig config = new StatisticsConfig();

                EndpointAddress address = new EndpointAddress(String.Format("http://{0}:{1}/MapExportStats", config.DefaultServer.Address, config.DefaultServer.HttpPort));
                WebHttpBinding binding = new WebHttpBinding(WebHttpSecurityMode.None);
                binding.MaxReceivedMessageSize = 4194304;
                MapExportStatClient statClient = new MapExportStatClient(binding, address);
                statClient.Endpoint.Behaviors.Add(new WebHttpBehavior());

                // Get the data required to build the report from the stats server.
                comparativeExportStatsDto = statClient.GetComparitiveExportStats();

                statClient.Close();
            }
            catch (Exception e)
            {
                Log.Log__Exception(e, "Exception occurred while attempting to retrieve the daily map export stats from the statistics server.");
            }

            // Did we managed to retrieve the data?
            if (comparativeExportStatsDto != null)
            {
                // Generate the csv data streams
                Stream comparativeCsvStream = GenerateComparativeCsv(comparativeExportStatsDto);

                // Save the users csv data to disk
                if (Filename != null)
                {
                    // Save the exports csv data to disk
                    string comparativeFilename = Path.Combine(Path.GetDirectoryName(Filename), Path.GetFileNameWithoutExtension(Filename) + "_comparative" + Path.GetExtension(Filename));
                    using (Stream fileStream = new FileStream(Filename, FileMode.Create))
                    {
                        comparativeCsvStream.Position = 0;
                        comparativeCsvStream.CopyTo(fileStream);
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="comparativeStats"></param>
        /// <returns></returns>
        private Stream GenerateComparativeCsv(ComparativeExportStatDtos comparativeStats)
        {
            Stream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);

            // Group the results
            if (comparativeStats != null)
            {
                IOrderedEnumerable<ComparativeExportStatDto> orderedStats = comparativeStats.Items.OrderBy(item => item.SectionName).ThenBy(item => item.Username);

                foreach (IGrouping<string, ComparativeExportStatDto> group in orderedStats.GroupBy(item => item.SectionName))
                {
                    string nameRow = String.Format("{0} - Name,{1}", group.Key, String.Join(",", group.Select(item => item.Username)));
                    string totalTimeRow = String.Format("{0} - Total Time,{1}", group.Key, String.Join(",", group.Select(item => item.TotalTime != null ? TruncateTimeSpan(item.TotalTime.Value).ToString() : "")));
                    string exportTimeRow = String.Format("{0} - Export Time,{1}", group.Key, String.Join(",", group.Select(item => item.ExportTime != null ? TruncateTimeSpan(item.ExportTime.Value).ToString() : "")));
                    string buildTimeRow = String.Format("{0} - Build Time,{1}", group.Key, String.Join(",", group.Select(item => item.BuildTime != null ? TruncateTimeSpan(item.BuildTime.Value).ToString() : "")));
                    string dateRow = String.Format("{0} - Date,{1}", group.Key, String.Join(",", group.Select(item => item.ExportDate)));

                    writer.WriteLine(nameRow);
                    writer.WriteLine(totalTimeRow);
                    writer.WriteLine(exportTimeRow);
                    writer.WriteLine(buildTimeRow);
                    writer.WriteLine(dateRow);
                }

                writer.Flush();
                stream.Position = 0;
            }

            return stream;
        }
        #endregion // Private Methods
    } // ComparativeReport
}
