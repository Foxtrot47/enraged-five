﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.ConfigParser;

namespace RSG.Model.Common
{
    /// <summary>
    /// Publically accessible interface for creating IWeapon and IWeaponCollection's
    /// </summary>
    public interface IWeaponFactory
    {
        #region Methods
        /// <summary>
        /// Creates a new weapon collection from the specified source.
        /// </summary>
        /// <param name="source">Source to use for creating the collection.</param>
        /// <param name="buildIdentifier">Optional build identifier for when the source is Database.</param>
        /// <returns></returns>
        IWeaponCollection CreateCollection(DataSource source, string buildIdentifier = null);
        #endregion // Methods
    } // IWeaponFactory
}
