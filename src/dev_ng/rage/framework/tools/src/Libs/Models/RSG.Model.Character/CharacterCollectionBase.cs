﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.Base.Collections;
using System.Collections;
using System.ComponentModel;

namespace RSG.Model.Character
{
    /// <summary>
    /// Base class for the various character collections
    /// </summary>
    public abstract class CharacterCollectionBase : AssetBase, ICharacterCollection
    {
        #region Properties and associated member data
        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public ObservableCollection<ICharacterCategoryCollection> CharacterCategories
        {
            get;
            protected set;
        }

        /// <summary>
        /// Returns all sections in this map hierarchy
        /// </summary>
        [Browsable(false)]
        public IEnumerable<ICharacter> AllCharacters
        {
            get
            {
                foreach (ICharacterCategoryCollection collection in CharacterCategories)
                {
                    foreach (ICharacter character in collection)
                    {
                        yield return character;
                    }
                }
            }
        }
        #endregion // Properties and associated member data
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public CharacterCollectionBase()
            : base("Characters")
        {
            CharacterCategories = new ObservableCollection<ICharacterCategoryCollection>();
        }
        #endregion // Constructor(s)

        #region IWeaponCollection Methods
        /// <summary>
        /// 
        /// </summary>
        public void LoadAllStats()
        {
            LoadStats(StreamableStatUtils.GetValuesAssignedToProperties(typeof(Character)));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="statsToLoad"></param>
        public abstract void LoadStats(IEnumerable<StreamableStat> statsToLoad);
        #endregion // IWeaponCollection Methods

        #region IEnumerable<ICharacter> Implementation
        /// <summary>
        /// Loops through the collections and returns each one in turn
        /// </summary>
        IEnumerator<ICharacterCategoryCollection> IEnumerable<ICharacterCategoryCollection>.GetEnumerator()
        {
            foreach (ICharacterCategoryCollection category in this.CharacterCategories)
            {
                yield return category;
            }
        }

        /// <summary>
        /// Returns the Enumerator for this object
        /// </summary>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerator)this);
        }
        #endregion // IEnumerable<ICharacter> Implementation

        #region ICollection<ICharacterCategoryCollection> Implementation
        #region ICollection<ICharacterCategoryCollection> Properties
        /// <summary>
        /// Returns whether the collection is read-only
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Return number of items in the set.
        /// </summary>
        public int Count
        {
            get
            {
                return CharacterCategories.Count;
            }
        }
        #endregion // ICollection<ICharacter> Properties

        #region ICollection<ICharacter> Methods
        /// <summary>
        /// Add a character to the collection.
        /// </summary>
        /// <param name="item"></param>
        public void Add(ICharacterCategoryCollection item)
        {
            CharacterCategories.Add(item);
        }

        /// <summary>
        /// Removes all characters from the collection.
        /// </summary>
        public void Clear()
        {
            CharacterCategories.Clear();
        }

        /// <summary>
        /// Determine whether the collection contains a specific character
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Contains(ICharacterCategoryCollection item)
        {
            return CharacterCategories.Contains(item);
        }

        /// <summary>
        /// Copy items in the collection to an array, starting at a particular index.
        /// </summary>
        /// <param name="output"></param>
        /// <param name="index"></param>
        public void CopyTo(ICharacterCategoryCollection[] output, int index)
        {
            CharacterCategories.CopyTo(output, index);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        public bool Remove(ICharacterCategoryCollection item)
        {
            return CharacterCategories.Remove(item);
        }
        #endregion // ICollection<ICharacterCategoryCollection> Methods
        #endregion // ICollection<ICharacterCategoryCollection> Implementation

        #region IDisposable Implementation
        /// <summary>
        /// 
        /// </summary>
        public virtual void Dispose()
        {
        }
        #endregion // IDisposable Implementation
    } // CharacterCollectionBase
}
