﻿using System;

namespace RSG.Model.Common
{
    /// <summary>
    /// Publicly accessible interface for creating ILevelCollections
    /// </summary>
    public interface ILevelFactory : IDisposable
    {
        #region Methods
        /// <summary>
        /// Creates a new weapon collection from the specified source.
        /// </summary>
        /// <param name="source">Source to use for creating the collection.</param>
        /// <param name="buildIdentifier">Optional build identifier for when the source is Database.</param>
        /// <returns></returns>
        ILevelCollection CreateCollection(DataSource source, string buildIdentifier = null);
        #endregion // Methods
    } // ILevelFactory
}
