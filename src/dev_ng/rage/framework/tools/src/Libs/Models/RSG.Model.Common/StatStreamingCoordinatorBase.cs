﻿using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using RSG.Base.Logging;
using RSG.Base.Threading;
using RSG.Base.ConfigParser;
using System.Collections.Generic;
using System;
using System.Linq;

namespace RSG.Model.Common
{
    /// <summary>
    /// Base class for stat streaming coordinators
    /// </summary>
    public abstract class StatStreamingCoordinatorBase : IStatStreamingCoordinator
    {
        #region Internal Class
        /// <summary>
        /// Helper class for loading of streamable object stats.
        /// </summary>
        protected class StreamableObjectLoadContext
        {
            public IList<IStreamableObject> Objects { get; set; }
            public IList<StreamableStat> Stats { get; set; }
        } // StreamableObjectLoadContext
        #endregion // Internal Class

        #region Properties
        /// <summary>
        /// Current number of streaming requests in the queue
        /// </summary>
        public int QueuedStreamingRequests
        {
            get
            {
                return TaskQueue.Count;
            }
        }

        /// <summary>
        /// The queue containing the tasks that need processing
        /// </summary>
        protected BlockingCollection<NamedTask> TaskQueue { get; set; }

        /// <summary>
        /// The source for cancelling tasks that are queued up
        /// </summary>
        protected CancellationTokenSource TaskCancellationSouce { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        protected ConfigGameView GameView { get; set; }

        /// <summary>
        /// 
        /// </summary>
        protected abstract string CoordinatorName { get; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        protected StatStreamingCoordinatorBase(ConfigGameView gv)
        {
            GameView = gv;

            TaskCancellationSouce = new CancellationTokenSource();
            TaskQueue = new BlockingCollection<NamedTask>();
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Starts the thread that monitors the task queue for work to do
        /// </summary>
        public void StartProcessingTasks()
        {
            Task.Factory.StartNew(() => ProcessTasks());
        }

        /// <summary>
        /// 
        /// </summary>
        public void ClearQueue()
        {
            // Cancel off any unprocessed tasks
            TaskCancellationSouce.Cancel();

            // Create a new cancellation token source so that new tasks aren't immediately cancelled
            TaskCancellationSouce = new CancellationTokenSource();
        }
        #endregion // Public Methods

        #region Protected Methods
        /// <summary>
        /// 
        /// </summary>
        protected virtual void ProcessTasks()
        {
            Thread.CurrentThread.Name = String.Format("{0} Stat Streaming Coordinator", CoordinatorName);

            foreach (NamedTask task in TaskQueue.GetConsumingEnumerable())
            {
                try
                {
                    task.Start();
                    task.Wait();
                    task.Dispose();
                    Thread.Sleep(30);
                }
                catch (System.Exception ex)
                {
                    Log.Log__Exception(ex, "Unhandled thread exception");
                }
            }
        }

        /// <summary>
        /// Helper function for executing a task either synchronously or asynchronously
        /// </summary>
        /// <param name="theTask"></param>
        /// <param name="async"></param>
        protected void ExecuteTask(NamedTask theTask, bool async)
        {
            if (async)
            {
                TaskQueue.Add(theTask);
            }
            else
            {
                try
                {
                    theTask.RunSynchronously();
                }
                catch (System.Exception ex)
                {
                    Log.Log__Exception(ex, "Unhandled thread exception");
                }
                finally
                {
                    if (theTask.Exception != null)
                    {
                        Log.Log__Exception(theTask.Exception, "Unhandled thread exception");
                    }

                    theTask.Dispose();
                }
            }
        }

        /// <summary>
        /// Helper function that checks whether a particular section has all of the requested stats loaded or not.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="requestedStats"></param>
        /// <returns></returns>
        protected bool AreRequestedStatsLoaded(IStreamableObject obj, IEnumerable<StreamableStat> requestedStats)
        {
            return !StatsRequiredForObject(obj, requestedStats).Any();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="requestedStats"></param>
        /// <returns></returns>
        protected IEnumerable<StreamableStat> StatsRequiredForObject(IStreamableObject obj, IEnumerable<StreamableStat> requestedStats)
        {
            foreach (StreamableStat stat in requestedStats)
            {
                if (!obj.IsStatLoaded(stat))
                {
                    yield return stat;
                }
            }
        }
        #endregion // Protected Methods

        #region IDisposable Implementation
        /// <summary>
        /// Gracefully kill off the task processing queue
        /// </summary>
        public virtual void Dispose()
        {
            // Clear the queue and then let the task queue know we're done
            ClearQueue();
            TaskQueue.CompleteAdding();
        }
        #endregion // IDisposable Implementation
    } // StatStreamingCoordinatorBase
}
