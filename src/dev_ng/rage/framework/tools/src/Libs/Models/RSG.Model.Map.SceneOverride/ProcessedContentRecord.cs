﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Map.SceneOverride
{
    /// <summary>
    /// A processed content table record
    /// </summary>
    public class ProcessedContentRecord
    {
        internal ProcessedContentRecord(int id, string name)
        {
            Id = id;
            Name = name;
        }

        internal int Id { get; set; }
        internal string Name { get; private set; }
    }
}
