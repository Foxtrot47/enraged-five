﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Map.Filters
{

    /// <summary>
    /// Common map section filter methods.
    /// </summary>
    public static class MapFilters
    {
        /// <summary>
        /// Returns true for all map sections so that everything gets loaded
        /// </summary>
        /// <param name="mapNode"></param>
        /// <returns></returns>
        public static Boolean All(RSG.Base.ConfigParser.ContentNodeMap mapNode)
        {
            return true;
        }

        /// <summary>
        /// Returns true if the map node is representing a map file with either definitions or not (i.e has rpf/ipl files and maybe ide)
        /// </summary>
        /// <param name="mapNode"></param>
        /// <returns></returns>
        public static Boolean NonGeneric(RSG.Base.ConfigParser.ContentNodeMap mapNode)
        {
            if (mapNode.ExportInstances) return true;

            return false;
        }

        /// <summary>
        /// Returns true if the map node is representing a map file with definitions (i.e has rpf/ipl/ide files only)
        /// </summary>
        /// <param name="mapNode"></param>
        /// <returns></returns>
        public static Boolean NonGenericWithDefinitions(RSG.Base.ConfigParser.ContentNodeMap mapNode)
        {
            if (mapNode.ExportInstances && mapNode.ExportDefinitions) return true;

            return false;
        }

        /// <summary>
        /// Returns true if the map node is representing a map file without definitions (i.e has rpf/ipl files only)
        /// </summary>
        /// <param name="mapNode"></param>
        /// <returns></returns>
        public static Boolean NonGenericWithoutDefinitions(RSG.Base.ConfigParser.ContentNodeMap mapNode)
        {
            if (mapNode.ExportInstances && !mapNode.ExportDefinitions) return true;

            return false;
        }

        /// <summary>
        /// Returns true if the map node is representing a prop file
        /// </summary>
        /// <param name="mapNode"></param>
        /// <returns></returns>
        public static Boolean Generic(RSG.Base.ConfigParser.ContentNodeMap mapNode)
        {
            if (!mapNode.ExportInstances) return true;

            return false;
        }
    }

} // RSG.Model.Map.Filters namespace
