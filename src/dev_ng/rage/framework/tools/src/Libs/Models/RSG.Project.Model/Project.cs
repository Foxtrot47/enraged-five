﻿//---------------------------------------------------------------------------------------------
// <copyright file="Project.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.Model
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Xml;
    using RSG.Base.Extensions;
    using RSG.Editor;

    /// <summary>
    /// A single node inside the project hierarchy that represents the actual project.
    /// </summary>
    public class Project : ProjectItem, IProjectItemScope
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="ConfigurationSwitches"/> property.
        /// </summary>
        private HashSet<string> _configurationSwitches;

        /// <summary>
        /// The private dictionary containing the current values for the configuration
        /// switches defined for this project.
        /// </summary>
        private Dictionary<string, string> _switchValues;

        /// <summary>
        /// The private field used for the <see cref="DirectoryPath"/> property.
        /// </summary>
        private string _directoryPath;

        /// <summary>
        /// The private field used for the <see cref="DefinitionItem"/> property.
        /// </summary>
        private ProjectItem _definitionItem;

        /// <summary>
        /// The private field used for the <see cref="FullPath"/> property.
        /// </summary>
        private string _fullPath;

        /// <summary>
        /// The private map containing the project items indexed by their type.
        /// </summary>
        private Dictionary<string, HashSet<ProjectItem>> _items;

        /// <summary>
        /// The private collection of defined property groups for this project.
        /// </summary>
        private List<ProjectConfig> _propertyGroups;

        /// <summary>
        /// The private collection of defined property groups for this project that are
        /// currently valid and in the correct override order.
        /// </summary>
        private List<ProjectConfig> _activePropertyGroups;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="Project"/> class.
        /// </summary>
        /// <param name="item">
        /// The project item that was defined and this project is replacing.
        /// </param>
        public Project(ProjectItem item)
            : base(item)
        {
            this._definitionItem = item;
            this._items = new Dictionary<string, HashSet<ProjectItem>>();
            this._propertyGroups = new List<ProjectConfig>();
            this._configurationSwitches = new HashSet<string>();
            this._switchValues = new Dictionary<string, string>();
            this._activePropertyGroups = new List<ProjectConfig>();
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets an iterator around the available configuration switches.
        /// </summary>
        public IEnumerable<string> ConfigurationSwitches
        {
            get
            {
                if (this._configurationSwitches == null)
                {
                    return Enumerable.Empty<string>();
                }

                return this._configurationSwitches.AsEnumerable();
            }
        }

        /// <summary>
        /// Gets the project item that was used to create this project.
        /// </summary>
        public ProjectItem DefinitionItem
        {
            get { return this._definitionItem; }
        }

        /// <summary>
        /// Gets the root directory for this scope. The root directory is never null; in-memory
        /// loads use the current directory at the time of the load.
        /// </summary>
        public string DirectoryPath
        {
            get { return this._directoryPath ?? String.Empty; }
        }

        /// <summary>
        /// Gets or sets the full path to the collections source file. Returns an empty string
        /// if the project was not loaded from disk.
        /// </summary>
        public string FullPath
        {
            get
            {
                return this._fullPath;
            }

            set
            {
                this._fullPath = value;
                this._directoryPath = Path.GetDirectoryName(value);
                this.UpdateIncludeValueFromPathChange(value);
            }
        }

        /// <summary>
        /// Retrieves a iterator around the project items that have the specified type. This
        /// will return a empty enumerable instead of null.
        /// </summary>
        /// <param name="itemType">
        /// The type of items that should be retrieved.
        /// </param>
        /// <returns>
        /// A iterator around the project items defined for this project with the specified
        /// item type.
        /// </returns>
        public IEnumerable<ProjectItem> this[string itemType]
        {
            get
            {
                HashSet<ProjectItem> items;
                if (!this._items.TryGetValue(itemType, out items))
                {
                    return Enumerable.Empty<ProjectItem>();
                }

                return items.AsEnumerable();
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Adds a new project item to this scope with the specified type and include value.
        /// </summary>
        /// <param name="type">
        /// The type of the new item.
        /// </param>
        /// <param name="include">
        /// The include value for the new item.
        /// </param>
        /// <returns>
        /// The new instance that has been created and added to this scope.
        /// </returns>
        public ProjectItem AddNewProjectItem(string type, string include)
        {
            ProjectItem existing = this.Find(include);
            if (existing != null)
            {
                return existing;
            }

            ProjectItem newItem = new ProjectItem(type, include, this);
            HashSet<ProjectItem> items;
            if (!this._items.TryGetValue(type, out items))
            {
                items = new HashSet<ProjectItem>(new ProjectItemComparer());
                this._items.Add(type, items);
            }

            items.Add(newItem);
            return newItem;
        }

        /// <summary>
        /// Adds the specified project item to this scope.
        /// </summary>
        /// <param name="item">
        /// The item to add to this scope.
        /// </param>
        public void AddProjectItem(ProjectItem item)
        {
            HashSet<ProjectItem> items;
            if (!this._items.TryGetValue(item.ItemType, out items))
            {
                items = new HashSet<ProjectItem>(new ProjectItemComparer());
                this._items.Add(item.ItemType, items);
            }

            items.Add(item);
        }

        /// <summary>
        /// Attempts to find the project item contained within this scope that has the
        /// specified include value.
        /// </summary>
        /// <param name="include">
        /// The include value to search for.
        /// </param>
        /// <returns>
        /// The project item contained within this scope that has the specified include value
        /// if found; otherwise, null.
        /// </returns>
        public ProjectItem Find(string include)
        {
            ItemIncludeComparer comparer = new ItemIncludeComparer();
            foreach (HashSet<ProjectItem> set in this._items.Values)
            {
                foreach (ProjectItem item in set)
                {
                    if (comparer.Equals(item, include))
                    {
                        return item;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Attempts to find the project item contained within this scope that has the
        /// specified filename value.
        /// </summary>
        /// <param name="filename">
        /// The filename value to search for.
        /// </param>
        /// <returns>
        /// The project item contained within this scope that has the specified filename value
        /// if found; otherwise, null.
        /// </returns>
        public ProjectItem FindFilename(string filename)
        {
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;
            foreach (KeyValuePair<string, HashSet<ProjectItem>> set in this._items)
            {
                if (String.Equals("Filter", set.Key))
                {
                    continue;
                }

                foreach (ProjectItem item in set.Value)
                {
                    string itemFilename = Path.GetFileName(item.Include);
                    if (comparer.Equals(itemFilename, filename))
                    {
                        return item;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Retrieves the unique ids that make up this projects type.
        /// </summary>
        /// <returns>
        /// A list containing all of the unique ids that define this projects type.
        /// </returns>
        public List<Guid> GetProjectTypeIds()
        {
            List<Guid> ids = new List<Guid>();
            string value = this.GetProperty("ProjectTypeGuids", null);
            if (value == null)
            {
                ids.Add(Guid.NewGuid());
                return ids;
            }

            char[] separator = new char[] { ';' };
            string[] parts = value.Split(separator, StringSplitOptions.RemoveEmptyEntries);
            foreach (string part in parts)
            {
                Guid id;
                if (Guid.TryParseExact(part, "B", out id))
                {
                    ids.Add(id);
                }
            }

            return ids;
        }

        /// <summary>
        /// Retrieves the value of the property with the specified name.
        /// </summary>
        /// <param name="name">
        /// The name of the property to retrieve the value of.
        /// </param>
        /// <param name="defaultValue">
        /// The value to return if the property doesn't exist.
        /// </param>
        /// <returns>
        /// The value of the property with the specified name if it exists; otherwise, the
        /// specified fall-back value.
        /// </returns>
        public string GetProperty(string name, string defaultValue)
        {
            foreach (ProjectConfig propertyGroup in this._activePropertyGroups)
            {
                string groupValue = propertyGroup.GetProperty(name, null);
                if (groupValue != null)
                {
                    return groupValue;
                }
            }

            return defaultValue;
        }

        /// <summary>
        /// Retrieves the value of the property with the specified name.
        /// </summary>
        /// <param name="name">
        /// The name of the property to retrieve the value of.
        /// </param>
        /// <returns>
        /// The value of the property with the specified name if it exists; otherwise, null.
        /// </returns>
        public string GetProperty(string name)
        {
            return this.GetProperty(name, null);
        }

        /// <summary>
        /// Retrieves the value of the property with the specified name as a boolean value.
        /// </summary>
        /// <param name="name">
        /// The name of the property to retrieve the value of.
        /// </param>
        /// <param name="defaultValue">
        /// The value to return if the property doesn't exist.
        /// </param>
        /// <returns>
        /// The value of the property with the specified name as a boolean value if it exists;
        /// otherwise, the specified fall-back value.
        /// </returns>
        public bool GetPropertyAsBool(string name, bool defaultValue)
        {
            string value = this.GetProperty(name, null);
            if (value == null)
            {
                return defaultValue;
            }

            bool result = false;
            if (!Boolean.TryParse(value, out result))
            {
                return defaultValue;
            }

            return result;
        }

        /// <summary>
        /// Retrieves the value of the property with the specified name as a full path value.
        /// </summary>
        /// <param name="name">
        /// The name of the property to retrieve the value of.
        /// </param>
        /// <returns>
        /// The value of the property with the specified name as a full path if the property
        /// exists; otherwise, null.
        /// </returns>
        public string GetPropertyAsFullPath(string name)
        {
            string value = this.GetProperty(name, null);
            if (value == null)
            {
                return null;
            }

            string currentDirectory = Directory.GetCurrentDirectory();
            try
            {
                Directory.SetCurrentDirectory(this._directoryPath);
                return Path.GetFullPath(value);
            }
            catch
            {
                return null;
            }
            finally
            {
                Directory.SetCurrentDirectory(currentDirectory);
            }
        }

        /// <summary>
        /// Retrieves a unique version of the specified string based on the current contents
        /// of this scope. The string will become unique by adding a numerical value to the
        /// end and incrementing it until uniqueness is found.
        /// </summary>
        /// <param name="baseFilename">
        /// The base filename that needs to be unique.
        /// </param>
        /// <returns>
        /// A unique version of the specified filename.
        /// </returns>
        public string GetUniqueFilename(string baseFilename)
        {
            int index = 1;
            string name = Path.GetFileNameWithoutExtension(baseFilename);
            while (name.Contains('.'))
            {
                name = Path.GetFileNameWithoutExtension(name);
            }

            string extension = baseFilename.Replace(name, String.Empty);
            string format = name + "{0}" + extension;
            string filename = format.FormatInvariant(index.ToString());
            while (this.FindFilename(filename) != null)
            {
                index++;
                filename = format.FormatInvariant(index.ToString());
            }

            return filename;
        }

        /// <summary>
        /// Retrieves a unique version of the specified string based on the current contents
        /// of this scope. The string will become unique by adding a numerical value to the
        /// end and incrementing it until uniqueness is found.
        /// </summary>
        /// <param name="baseInclude">
        /// The base include path that needs to be unique.
        /// </param>
        /// <returns>
        /// A unique version of the specified string include.
        /// </returns>
        public string GetUniqueInclude(string baseInclude)
        {
            int index = 1;
            string format = baseInclude + "{0}";
            string include = format.FormatInvariant(index.ToString());

            while (this.Find(include) != null)
            {
                index++;
                include = format.FormatInvariant(index.ToString());
            }

            return include;
        }

        /// <summary>
        /// Loads the data contained within the file located at the specified path.
        /// </summary>
        /// <param name="fullPath">
        /// The path to the file that contains the data to load.
        /// </param>
        public void Load(string fullPath)
        {
            try
            {
                foreach (MetadataLevel level in Enum.GetValues(typeof(MetadataLevel)))
                {
                    string extension = MetadataLevelUtil.GetExtensionAttribute(level);
                    if (extension == null)
                    {
                        continue;
                    }

                    string filename = fullPath + extension;
                    if (File.Exists(filename))
                    {
                        using (XmlReader reader = XmlReader.Create(filename))
                        {
                            reader.MoveToContent();
                            this.Deserialise(reader, level);
                        }
                    }
                }
            }
            catch (XmlException ex)
            {
                throw ProjectLoadException.XmlException(fullPath, ex);
            }
            catch (ProjectLoadException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw ProjectLoadException.GenericException(fullPath, ex);
            }

            this._fullPath = fullPath;
            this._directoryPath = Path.GetDirectoryName(fullPath);
            foreach (string configurationSwitch in this._configurationSwitches)
            {
                this._switchValues.Add(configurationSwitch, String.Empty);
            }

            foreach (ProjectConfig group in this._propertyGroups)
            {
                string condition = group.Condition;
                if (!String.IsNullOrWhiteSpace(condition))
                {
                    continue;
                }

                foreach (string configurationSwitch in this._configurationSwitches)
                {
                    string switchValue = group.GetProperty(configurationSwitch);
                    if (switchValue != null)
                    {
                        this._switchValues[configurationSwitch] = switchValue;
                    }
                }
            }

            this.UpdateActivePropertGroups();
        }

        /// <summary>
        /// Removes the first occurrence of the specified item from this scope.
        /// </summary>
        /// <param name="item">
        /// The item to remove.
        /// </param>
        public void RemoveProjectItem(ProjectItem item)
        {
            foreach (HashSet<ProjectItem> itemSet in this._items.Values)
            {
                itemSet.Remove(item);
                string itemInclude = item.Include;
                List<ProjectItem> itemsToRemove = new List<ProjectItem>();
                foreach (ProjectItem setItem in itemSet)
                {
                    if (setItem.Include.StartsWith(itemInclude + "\\"))
                    {
                        itemsToRemove.Add(setItem);
                    }
                    else
                    {
                        string filter = setItem.GetMetadata("Filter");
                        if (filter != null && String.Equals(filter, itemInclude))
                        {
                            itemsToRemove.Add(setItem);
                        }
                    }
                }

                foreach (ProjectItem itemToRemove in itemsToRemove)
                {
                    itemSet.Remove(itemToRemove);
                }
            }
        }

        /// <summary>
        /// Saves the current state of this instances specified metadata level to the specified
        /// stream.
        /// </summary>
        /// <param name="stream">
        /// The stream to write this instances data to.
        /// </param>
        /// <param name="metadataLevel">
        /// The metadata level that needs to be serialised into the specified stream.
        /// </param>
        /// <returns>
        /// True if the save finished successfully; otherwise, false.
        /// </returns>
        public bool Save(Stream stream, MetadataLevel metadataLevel)
        {
            try
            {
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Indent = true;
                using (XmlWriter writer = XmlWriter.Create(stream, settings))
                {
                    writer.WriteStartDocument();
                    writer.WriteStartElement("Project");

                    if (metadataLevel == MetadataLevel.Core)
                    {
                        this.SerialiseCore(writer);
                    }

                    this.SerialiseItemGroups(writer, metadataLevel);

                    writer.WriteEndElement();
                    writer.WriteEndDocument();
                }
            }
            catch
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Sets a dependent property for this project. Setting a dependent property means that
        /// the value will be saved inside the currently active property group.
        /// </summary>
        /// <param name="name">
        /// The name of the property to set.
        /// </param>
        /// <param name="value">
        /// The value to set the property to.
        /// </param>
        public void SetDependentProperty(string name, string value)
        {
            ProjectConfig config = this._activePropertyGroups.FirstOrDefault();
            if (config != null)
            {
                config.SetProperty(name, value);
            }
        }

        /// <summary>
        /// Sets a independent property for this project. Setting a independent property means
        /// that the value will be saved inside the property group without any conditions.
        /// </summary>
        /// <param name="name">
        /// The name of the property to set.
        /// </param>
        /// <param name="value">
        /// The value to set the property to.
        /// </param>
        public void SetIndependentProperty(string name, string value)
        {
            ProjectConfig config = this._activePropertyGroups.LastOrDefault();
            if (config != null)
            {
                config.SetProperty(name, value);
            }
        }

        /// <summary>
        /// Determines whether any data exists for a specific metadata level on any of the
        /// project items.
        /// </summary>
        /// <param name="level">
        /// The level to test.
        /// </param>
        /// <returns>
        /// True if any of the project items for this collection contain metadata for the
        /// specified level; otherwise, false.
        /// </returns>
        internal override bool ContainsData(MetadataLevel level)
        {
            if (level == MetadataLevel.Core)
            {
                return true;
            }

            foreach (HashSet<ProjectItem> itemGroup in this._items.Values)
            {
                foreach (ProjectItem item in itemGroup)
                {
                    if (item.ContainsData(level))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Initialises this instance using the data contained within the specified xml reader.
        /// </summary>
        /// <param name="reader">
        /// The xml reader that contains the data used to initialise this instance.
        /// </param>
        /// <param name="level">
        /// The metadata level that is currently being deserialised.
        /// </param>
        private void Deserialise(XmlReader reader, MetadataLevel level)
        {
            reader.ValidateStartPosition("Project");
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() == XmlNodeType.Element)
            {
                if (String.Equals("ConfigurationSwitches", reader.Name))
                {
                    this.DeserialiseSwitches(reader);
                }
                else if (String.Equals("PropertyGroup", reader.Name))
                {
                    this._propertyGroups.Add(new ProjectConfig(reader));
                }
                else if (String.Equals("ItemGroup", reader.Name))
                {
                    this.DeserialiseItemGroup(reader, level);
                }
                else
                {
                    reader.Skip();
                }
            }

            if (reader.NodeType == XmlNodeType.EndElement)
            {
                reader.ReadEndElement();
            }
        }

        /// <summary>
        /// Creates a set of project items from a single item group.
        /// </summary>
        /// <param name="reader">
        /// The xml reader that contains the data for the item group.
        /// </param>
        /// <param name="level">
        /// The metadata level that is currently being deserialised.
        /// </param>
        private void DeserialiseItemGroup(XmlReader reader, MetadataLevel level)
        {
            reader.ValidateStartPosition("ItemGroup");
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() == XmlNodeType.Element)
            {
                string type = reader.Name;
                string include = reader.GetAttribute("Include");
                if (String.IsNullOrWhiteSpace(include))
                {
                    reader.Skip();
                    continue;
                }

                ProjectItem newItem = this.AddNewProjectItem(type, include);
                newItem.DeserialiseMetadata(reader, level);
            }

            if (reader.NodeType == XmlNodeType.EndElement)
            {
                reader.ReadEndElement();
            }
        }

        /// <summary>
        /// Creates the configuration switches for the project using the data inside the
        /// specified xml reader.
        /// </summary>
        /// <param name="reader">
        /// The xml reader that contains the data for the configuration switches.
        /// </param>
        private void DeserialiseSwitches(XmlReader reader)
        {
            reader.ValidateStartPosition("ConfigurationSwitches");
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() == XmlNodeType.Element)
            {
                if (String.Equals(reader.Name, "Switch"))
                {
                    string switchName = reader.ReadElementContentAsString();
                    bool exists = this._configurationSwitches.Contains(switchName);
                    if (exists)
                    {
                        Debug.Assert(
                            !exists,
                            "Configuration switch already defined",
                            "Switch Name: {0}",
                            switchName);
                    }

                    this._configurationSwitches.Add(switchName);
                }
                else
                {
                    reader.Skip();
                }
            }

            if (reader.NodeType == XmlNodeType.EndElement)
            {
                reader.ReadEndElement();
            }
        }

        /// <summary>
        /// Determines whether the specified config object has its condition currently meet.
        /// </summary>
        /// <param name="config">
        /// The config to test.
        /// </param>
        /// <returns>
        /// True if the specified configuration condition is currently meet; otherwise, false.
        /// </returns>
        private bool IsConditionMeet(ProjectConfig config)
        {
            string condition = config.Condition;
            if (String.IsNullOrWhiteSpace(condition))
            {
                return true;
            }

            Regex regex = new Regex("'.*' == '.*'");
            if (!regex.IsMatch(condition))
            {
                return false;
            }

            char[] separator = new char[] { '\'' };
            string[] parts = condition.Split(separator, StringSplitOptions.RemoveEmptyEntries);
            if (parts.Length != 3)
            {
                return false;
            }

            string left = parts[0];
            string right = parts[2];
            foreach (string configuationSwitch in this._configurationSwitches)
            {
                string oldValue = "$({0})".FormatInvariant(configuationSwitch);
                string newValue = this._switchValues[configuationSwitch];
                left = left.Replace(oldValue, newValue);
            }

            return String.Equals(left, right, StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Serialises the data contained within this instance onto the specified xml writer.
        /// </summary>
        /// <param name="writer">
        /// The xml writer this instance will be serialised to.
        /// </param>
        private void SerialiseCore(XmlWriter writer)
        {
            if (writer == null)
            {
                throw new SmartArgumentNullException(() => writer);
            }

            writer.WriteStartElement("ConfigurationSwitches");
            foreach (string configurationSwitch in this._configurationSwitches)
            {
                writer.WriteElementString("Switch", configurationSwitch);
            }

            writer.WriteEndElement();

            foreach (ProjectConfig propertyGroup in this._propertyGroups)
            {
                writer.WriteStartElement("PropertyGroup");
                propertyGroup.Serialise(writer);
                writer.WriteEndElement();
            }
        }

        /// <summary>
        /// Serialises all of the item groups to the specified writer.
        /// </summary>
        /// <param name="writer">
        /// The writer that the item groups should be written out to.
        /// </param>
        /// <param name="level">
        /// The metadata level that should be serialised.
        /// </param>
        private void SerialiseItemGroups(XmlWriter writer, MetadataLevel level)
        {
            IEnumerable<string> excluded = MetadataLevelUtil.GetExcludedGroups(level);
            foreach (KeyValuePair<string, HashSet<ProjectItem>> itemGroup in this._items)
            {
                if (excluded.Contains(itemGroup.Key, StringComparer.OrdinalIgnoreCase))
                {
                    continue;
                }

                int validCount = 0;
                foreach (ProjectItem projectItem in itemGroup.Value)
                {
                    if (!projectItem.ContainsData(level) && level != MetadataLevel.Core)
                    {
                        continue;
                    }

                    validCount++;
                }

                if (validCount == 0)
                {
                    continue;
                }

                writer.WriteStartElement("ItemGroup");
                foreach (ProjectItem projectItem in itemGroup.Value)
                {
                    if (!projectItem.ContainsData(level) && level != MetadataLevel.Core)
                    {
                        continue;
                    }

                    writer.WriteStartElement(projectItem.ItemType);
                    projectItem.Serialise(writer, level);
                    writer.WriteEndElement();
                }

                writer.WriteEndElement();
            }
        }

        /// <summary>
        /// Updates the currently active property groups based on the individual conditions.
        /// </summary>
        private void UpdateActivePropertGroups()
        {
            List<ProjectConfig> groups = new List<ProjectConfig>(this._propertyGroups);
            groups.Reverse();
            foreach (ProjectConfig group in groups)
            {
                if (!this.IsConditionMeet(group))
                {
                    continue;
                }

                this._activePropertyGroups.Add(group);
            }

            if (this._activePropertyGroups.Count == 0)
            {
                ProjectConfig projectConfig = new ProjectConfig();
                this._propertyGroups.Add(projectConfig);
                this._activePropertyGroups.Add(projectConfig);
            }
        }
        #endregion Methods
    } // RSG.Project.Model.Project {Class}
} // RSG.Project.Model {Namespace}
