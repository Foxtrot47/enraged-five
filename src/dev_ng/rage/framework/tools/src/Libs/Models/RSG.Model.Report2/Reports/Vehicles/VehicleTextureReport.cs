﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using RSG.Base.Logging;
using System.IO;
using System.Web.UI;
using RSG.Model.Common;
using RSG.Model.Asset;
using RSG.Base.Tasks;

namespace RSG.Model.Report.Reports.Vehicles
{
    /// <summary>
    /// 
    /// </summary>
    public class VehicleTextureReport : VehicleReportBase, IReportStreamProvider, IDisposable
    {
        #region Constants
        private const String c_name = "Vehicle Texture Usage";
        private const String c_description = "Generate and display a vehicle texture report.";
        #endregion // Constants

        #region Types
        /// <summary>
        /// Structure to store per-vehicle texture usage.
        /// </summary>
        private class VehicleTextureUsage
        {
            public List<ITexture> Unique { get; set; }
            public List<ITexture> Shared { get; set; }

            #region Constructor(s)
            /// <summary>
            /// Default constructor.
            /// </summary>
            public VehicleTextureUsage()
            {
                this.Unique = new List<ITexture>();
                this.Shared = new List<ITexture>();
            }
            #endregion // Constructor(s)
        }
        #endregion // Types

        #region Member Data
        /// <summary>
        /// Shared texture usage statistics.
        /// </summary>
        private Dictionary<String, List<IVehicle>> textureUsage;

        /// <summary>
        /// Vehicle texture usage statistics.
        /// </summary>
        private Dictionary<String, VehicleTextureUsage> vehicleUsage;
        #endregion // Member Data

        #region IReportStreamProvider Properties
        /// <summary>
        /// Stream containing the report data
        /// </summary>
        public Stream Stream
        {
            get;
            protected set;
        }
        #endregion // IReportStreamProvider Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public VehicleTextureReport()
            : base(c_name, c_description)
        {
        }
        #endregion // Constructor(s)

        #region VehicleReport Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        protected override void GenerateReport(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            Log.Log__Message("Generating Vehicle Texture Report");
            AnalyseSharedTextures(context.Level);

            MemoryStream stream = new MemoryStream();
            StreamWriter reportFile = new StreamWriter(stream);

            HtmlTextWriter writer = new HtmlTextWriter(reportFile);
            writer.RenderBeginTag(HtmlTextWriterTag.Html);
            writer.RenderBeginTag(HtmlTextWriterTag.Head);
            writer.RenderEndTag();
            writer.RenderBeginTag(HtmlTextWriterTag.Body);
            writer.RenderBeginTag(HtmlTextWriterTag.H1);
            writer.Write("Vehicle Texture Report");
            writer.RenderEndTag();
            writer.RenderBeginTag(HtmlTextWriterTag.H1);
            writer.Write("Summary");
            writer.RenderEndTag();
            writer.RenderBeginTag(HtmlTextWriterTag.P);
            writer.Write("There are {0} vehicles currently available; and {1} textures being tracked.",
                this.vehicleUsage.Count, this.textureUsage.Count);
            writer.RenderEndTag();
            writer.RenderBeginTag(HtmlTextWriterTag.H2);
            writer.Write("Shared Textures");
            writer.RenderEndTag();
            RenderSharedTextureDetails(writer);
            writer.RenderBeginTag(HtmlTextWriterTag.H2);
            writer.Write("Textures by Vehicle");
            RenderVehicleTextureDetails(writer);
            writer.RenderEndTag();
            writer.RenderEndTag();
            writer.RenderEndTag();
            writer.Flush();

            stream.Position = 0;
            Stream = stream;

            sw.Stop();
            Log.Log__Message("Vehicle Texture Report took {0}ms.", sw.ElapsedMilliseconds);
        }
        #endregion // VehicleReport Overrides

        #region Private Methods
        /// <summary>
        /// Analyse our vehicle model gathering info about vehicle texture usage.
        /// </summary>
        private void AnalyseSharedTextures(ILevel level)
        {
            vehicleUsage = new Dictionary<String, VehicleTextureUsage>();
            textureUsage = new Dictionary<String, List<IVehicle>>();

            IVehicleCollection vehicles = level.Vehicles;

            if (vehicles != null)
            {
                foreach (IVehicle vehicle in vehicles)
                {
                    foreach (Texture texture in vehicle.Textures)
                    {
                        if (textureUsage.ContainsKey(texture.Name))
                        {
                            if (!textureUsage[texture.Name].Contains(vehicle))
                                textureUsage[texture.Name].Add(vehicle);
                        }
                        else
                        {
                            textureUsage[texture.Name] = new List<IVehicle>();
                            textureUsage[texture.Name].Add(vehicle);
                        }
                    }
                }

                // Pass at categorising vehicle unique/shared textures.
                foreach (IVehicle vehicle in vehicles)
                {
                    if (!vehicleUsage.ContainsKey(vehicle.Name))
                        vehicleUsage[vehicle.Name] = new VehicleTextureUsage();

                    foreach (Texture texture in vehicle.Textures)
                    {
                        if (1 == textureUsage[texture.Name].Count)
                            vehicleUsage[vehicle.Name].Unique.Add(texture);
                        else
                            vehicleUsage[vehicle.Name].Shared.Add(texture);
                    }
                }
            }
            else
            {
                Log.Log__Error("Failed to find vehicle container in level {0}.", level.Name);
            }
        }

        /// <summary>
        /// Render HTML table to display shared vehicle texture info.
        /// </summary>
        /// <param name="writer"></param>
        private void RenderSharedTextureDetails(HtmlTextWriter writer)
        {
            Dictionary<String, List<IVehicle>> sortedByFrequencyCount =
                (from entry in textureUsage orderby entry.Value.Count descending select entry).
                ToDictionary(pair => pair.Key, pair => pair.Value);

            writer.RenderBeginTag(HtmlTextWriterTag.Table);
            writer.RenderBeginTag(HtmlTextWriterTag.Tr);
            writer.RenderBeginTag(HtmlTextWriterTag.Th);
            writer.Write("Texture");
            writer.RenderEndTag();
            writer.RenderBeginTag(HtmlTextWriterTag.Th);
            writer.Write("Frequency");
            writer.RenderEndTag();
            writer.RenderBeginTag(HtmlTextWriterTag.Th);
            writer.Write("Vehicles Used");
            writer.RenderEndTag();
            writer.RenderEndTag();
            foreach (KeyValuePair<String, List<IVehicle>> kvp in sortedByFrequencyCount)
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write(kvp.Key);
                writer.RenderEndTag();
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write(kvp.Value.Count);
                writer.RenderEndTag();
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                foreach (IVehicle v in kvp.Value)
                {
                    writer.Write("{0}&nbsp;", v.Name);
                }
                writer.RenderEndTag();
                writer.RenderEndTag();
            }
            writer.RenderEndTag();
        }

        /// <summary>
        /// Render HTML table to display per-vehicle texture info.
        /// </summary>
        /// <param name="writer"></param>
        private void RenderVehicleTextureDetails(HtmlTextWriter writer)
        {
            Dictionary<String, VehicleTextureUsage> sortedByName =
                (from entry in vehicleUsage orderby entry.Key ascending select entry).
                ToDictionary(pair => pair.Key, pair => pair.Value);

            writer.RenderBeginTag(HtmlTextWriterTag.Table);
            writer.RenderBeginTag(HtmlTextWriterTag.Tr);
            writer.RenderBeginTag(HtmlTextWriterTag.Th);
            writer.Write("Vehicle");
            writer.RenderEndTag();
            writer.RenderBeginTag(HtmlTextWriterTag.Th);
            writer.Write("Unique Texture Count");
            writer.RenderEndTag();
            writer.RenderBeginTag(HtmlTextWriterTag.Th);
            writer.Write("Shared Texture Count");
            writer.RenderEndTag();
            writer.RenderEndTag();
            foreach (KeyValuePair<String, VehicleTextureUsage> kvp in sortedByName)
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write(kvp.Key);
                writer.RenderEndTag();
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write(kvp.Value.Unique.Count);
                writer.RenderEndTag();
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write(kvp.Value.Shared.Count);
                writer.RenderEndTag();
                writer.RenderEndTag();
            }
            writer.RenderEndTag();
        }
        #endregion // Private Methods

        #region IDisposable Implementation
        /// <summary>
        /// Ensures that the stream is correctly disposed of
        /// </summary>
        public virtual void Dispose()
        {
            if (Stream != null)
            {
                Stream.Dispose();
            }
        }
        #endregion // IDisposable Implementation
    } // VehicleTextureReport
}
