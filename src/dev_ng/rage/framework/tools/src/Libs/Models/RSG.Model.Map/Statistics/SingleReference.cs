﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.SceneXml;
using RSG.SceneXml.Statistics;
using RSG.Base.Math;
using RSG.Base.Collections;

namespace RSG.Model.Map.Statistics
{
    /// <summary>
    /// The base class for a single reference
    /// </summary>
    public class SingleReference
    {
        #region Properties

        /// <summary>
        /// The name that is used to reference this with
        /// </summary>
        public String ReferenceName
        {
            get { return m_referenceName; }
            set { m_referenceName = value; }
        }
        private String m_referenceName;

        /// <summary>
        /// The polygon count for this prop
        /// </summary>
        public uint Polycount
        {
            get { return m_polycount; }
            set { m_polycount = value; }
        }
        private uint m_polycount;

        /// <summary>
        /// The polygon density for this prop
        /// </summary>
        public float Polydensity
        {
            get { return m_polydensity; }
            set { m_polydensity = value; }
        }
        private float m_polydensity;

        /// <summary>
        /// The geometry size for this prop
        /// </summary>
        public uint GeometrySize
        {
            get { return m_geometrySize; }
            set { m_geometrySize = value; }
        }
        private uint m_geometrySize;

        /// <summary>
        /// The polygon count for the collision for this prop
        /// </summary>
        public uint CollisionPolycount
        {
            get { return m_collisionPolycount; }
            set { m_collisionPolycount = value; }
        }
        private uint m_collisionPolycount;

        /// <summary>
        /// The polygon density for the collision for this prop
        /// </summary>
        public float CollisionPolydensity
        {
            get { return m_collisionPolydensity; }
            set { m_collisionPolydensity = value; }
        }
        private float m_collisionPolydensity;

        /// <summary>
        /// A dictionary of all the txds that are in this reference
        /// by name and size
        /// </summary>
        public Dictionary<String, uint> Txds
        {
            get { return m_txds; }
            set { m_txds = value; }
        }
        private Dictionary<String, uint> m_txds;

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// Default constructor
        /// </summary>
        public SingleReference()
        {
            this.ReferenceName = String.Empty;

            this.Polycount = 0;
            this.Polydensity = 0.0f;
            this.GeometrySize = 0;
            this.CollisionPolycount = 0;
            this.CollisionPolydensity = 0.0f;

            this.Txds = new Dictionary<String, uint>();
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="section"></param>
        public SingleReference(String name)
        {
            this.ReferenceName = name;

            this.Polycount = 0;
            this.Polydensity = 0.0f;
            this.GeometrySize = 0;
            this.CollisionPolycount = 0;
            this.CollisionPolydensity = 0.0f;

            this.Txds = new Dictionary<String, uint>();
        }

        #endregion // Constructor(s)
    }

    /// <summary>
    /// Represents the statistics for a single prop
    /// </summary>
    public class SingleProp : SingleReference
    {
        #region Properties

        #endregion // Properties

        #region Constructor(s)
        
        /// <summary>
        /// Default constructor
        /// </summary>
        public SingleProp()
        {
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="section"></param>
        public SingleProp(String name, DrawableStats drawableStats, DrawableStats collisionStats, TxdStats txdStats)
            : base(name)
        {
            if (drawableStats != null)
            {
                this.Polycount = drawableStats.PolyCount;
                this.Polydensity = drawableStats.PolyDensity;
                this.GeometrySize = drawableStats.Size;
            }
            if (collisionStats != null)
            {
                this.CollisionPolycount = collisionStats.PolyCount;
                this.CollisionPolydensity = collisionStats.PolyDensity;
            }
            if (txdStats != null)
            {
                if (!this.Txds.ContainsKey(txdStats.Name))
                {
                    this.Txds.Add(txdStats.Name, txdStats.Size);
                }
            }
        }

        #endregion // Constructor(s)
    }

    /// <summary>
    /// Represents the statistics for a single interior
    /// </summary>
    public class SingleInterior : SingleReference
    {
        #region Properties


        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// Default constructor
        /// </summary>
        public SingleInterior()
        {
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="section"></param>
        public SingleInterior(String name)
            : base(name)
        {
        }

        #endregion // Constructor(s)
    }

    /// <summary>
    /// Represents a wrapper around a keyvaluepair of the reference ref name and the instance
    /// count it appears in a specific file
    /// </summary>
    public class SingleUniqueReference
    {
        #region Properties

        /// <summary>
        /// The name of the object that is being referenced.
        /// </summary>
        public String RefName
        {
            get { return m_refName; }
            set { m_refName = value; }
        }
        private String m_refName;

        /// <summary>
        /// The number of times the referenced object shows up in a given file
        /// </summary>
        public uint Count
        {
            get { return m_count; }
            set { m_count = value; }
        }
        private uint m_count;

        /// <summary>
        /// The statistics for this unique reference
        /// </summary>
        public SingleReference Statistics
        {
            get { return m_statistics; }
            set { m_statistics = value; }
        }
        private SingleReference m_statistics;

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// Default constructor
        /// </summary>
        public SingleUniqueReference()
        {
            this.RefName = String.Empty;
            this.Count = 0;
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="section"></param>
        public SingleUniqueReference(String refName, uint count)
        {
            this.RefName = refName;
            this.Count = count;
        }

        #endregion // Constructor(s)



    }

    /// <summary>
    /// A warpper around a dictionary of unique references that appear in a
    /// set file
    /// </summary>
    public class UniqueReferenceDictionary
    {
        #region Properties

        /// <summary>
        /// The name of the object that is being referenced.
        /// </summary>
        private Dictionary<String, SingleUniqueReference> UniqueReferences
        {
            get { return m_uniqueReferences; }
            set { m_uniqueReferences = value; }
        }
        private Dictionary<String, SingleUniqueReference> m_uniqueReferences;

        /// <summary>
        /// The ref file that all these unique references belong to
        /// </summary>
        public String RefFile
        {
            get { return m_refFile; }
            set { m_refFile = value; }
        }
        private String m_refFile;

        #endregion // Properties

        #region IEnumerable

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<SingleUniqueReference> GetReferences()
        {
            foreach (SingleUniqueReference uniqueReference in this.UniqueReferences.Values)
            {
                yield return uniqueReference;
            }
        }

        #endregion // IEnumerable

        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public UniqueReferenceDictionary(String refFile)
        {
            this.UniqueReferences = new Dictionary<String, SingleUniqueReference>();
            this.RefFile = refFile;
        }

        #endregion // Constructor(s)

        public void AddReference(String refName)
        {
            if (!this.UniqueReferences.ContainsKey(refName))
            {
                this.UniqueReferences.Add(refName, new SingleUniqueReference(refName, 1));
            }
            else
            {
                this.UniqueReferences[refName].Count++;
            }
        }

        public void AddReference(SingleUniqueReference reference)
        {
            if (reference != null)
            {
                if (!this.UniqueReferences.ContainsKey(reference.RefName))
                {
                    this.UniqueReferences.Add(reference.RefName, reference);
                }
                else
                {
                    this.UniqueReferences[reference.RefName].Count++;
                    this.UniqueReferences[reference.RefName].Statistics = reference.Statistics;
                }
            }
        }

        public SingleUniqueReference GetReference(String refName)
        {
            SingleUniqueReference reference = null;
            this.UniqueReferences.TryGetValue(refName, out reference);
            return reference;
        }
    }

    /// <summary>
    /// A warpper around a dictionary of unique reference dictionary, indexed by
    /// source file path
    /// </summary>
    public class SectionReferences
    {
        #region Properties

        /// <summary>
        /// The name of the object that is being referenced.
        /// </summary>
        private Dictionary<String, UniqueReferenceDictionary> References
        {
            get { return m_references; }
            set { m_references = value; }
        }
        private Dictionary<String, UniqueReferenceDictionary> m_references;

        /// <summary>
        /// Get the number of unique refFiles we have in this dictionary
        /// </summary>
        public int Count
        {
            get
            {
                return this.References.Count;
            }
        }

        /// <summary>
        /// Get the number of unique references we have in this section references object
        /// </summary>
        public int UniqueReferenceCount
        {
            get
            {
                int count = 0;
                foreach (UniqueReferenceDictionary dictionary in this.GetReferenceDictionaries())
                {
                    foreach (SingleUniqueReference reference in dictionary.GetReferences())
                    {
                        count++;
                    }
                }
                return count;
            }
        }

        #endregion // Properties

        #region IEnumerable

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<UniqueReferenceDictionary> GetReferenceDictionaries()
        {
            foreach (UniqueReferenceDictionary referenceDictionary in this.References.Values)
            {
                yield return referenceDictionary;
            }
        }

        /// <summary>
        /// Gets a loop around the single unique references inside this section references
        /// </summary>
        /// <returns></returns>
        public IEnumerable<SingleUniqueReference> GetReferences()
        {
            foreach (UniqueReferenceDictionary referenceDictionary in this.References.Values)
            {
                foreach (SingleUniqueReference reference in referenceDictionary.GetReferences())
                {
                    yield return reference;
                }
            }
        }

        #endregion // IEnumerable

        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public SectionReferences()
        {
            this.References = new Dictionary<String, UniqueReferenceDictionary>();
        }

        #endregion // Constructor(s)

        #region Dictionary Methods

        public void AddReference(String refFile, String refName)
        {
            if (!this.References.ContainsKey(refFile))
            {
                this.References.Add(refFile, new UniqueReferenceDictionary(refFile));
            }

            this.References[refFile].AddReference(refName);
        }

        public void AddReference(TargetObjectDef referenceObject)
        {
            String refName = String.Empty;
            String refFile = String.Empty;

            if (referenceObject.IsRefObject())
            {
                foreach (Dictionary<String, Object> container in referenceObject.ParamBlocks)
                {
                    foreach (KeyValuePair<String, Object> attribute in container)
                    {
                        if (attribute.Key == "objectname")
                        {
                            refName = (String)attribute.Value;
                        }
                        if (attribute.Key == "filename")
                        {
                            refFile = new Uri((String)attribute.Value).AbsolutePath;
                        }
                    }
                }
            }
            else if (referenceObject.IsXRef())
            {
                refName = referenceObject.RefName;
                refFile = new Uri(referenceObject.RefFile).AbsolutePath;
            }
            else if (referenceObject.IsInternalRef())
            {
                RSG.Base.Logging.Log.Log__Warning("Internal refs aren't supported yet in the statistic generation");
            }

            if (!String.IsNullOrEmpty(refName) && !String.IsNullOrEmpty(refFile))
            {
                this.AddReference(refFile, refName);
            }
        }

        public void AddReference(String refFile, SingleUniqueReference reference)
        {
            if (!this.References.ContainsKey(refFile))
            {
                this.References.Add(refFile, new UniqueReferenceDictionary(refFile));
            }

            this.References[refFile].AddReference(reference);
        }

        public void AppendReferences(SectionReferences references)
        {
            foreach (UniqueReferenceDictionary referenceDictionary in references.GetReferenceDictionaries())
            {
                foreach (SingleUniqueReference reference in referenceDictionary.GetReferences())
                {
                    this.AddReference(referenceDictionary.RefFile, reference);
                }
            }
        }

        public SingleUniqueReference GetReference(String refFilename, String refName)
        {
            UniqueReferenceDictionary dictionary = null;
            this.References.TryGetValue(refFilename, out dictionary);
            if (dictionary != null)
            {
                return dictionary.GetReference(refName);
            }
            return null;
        }

        #endregion // Dictionary Methods

    }




    public class LevelReferenceStatistics
    {
        #region Properties

        /// <summary>
        /// The polygon count for this prop
        /// </summary>
        public uint Polycount
        {
            get { return m_polycount; }
            set { m_polycount = value; }
        }
        private uint m_polycount;

        /// <summary>
        /// The polygon density for this prop
        /// </summary>
        public float Polydensity
        {
            get { return m_polydensity; }
            set { m_polydensity = value; }
        }
        private float m_polydensity;

        /// <summary>
        /// The geometry size for this prop
        /// </summary>
        public uint GeometrySize
        {
            get { return m_geometrySize; }
            set { m_geometrySize = value; }
        }
        private uint m_geometrySize;

        /// <summary>
        /// The polygon count for the collision for this prop
        /// </summary>
        public uint CollisionPolycount
        {
            get { return m_collisionPolycount; }
            set { m_collisionPolycount = value; }
        }
        private uint m_collisionPolycount;

        /// <summary>
        /// The polygon density for the collision for this prop
        /// </summary>
        public float CollisionPolydensity
        {
            get { return m_collisionPolydensity; }
            set { m_collisionPolydensity = value; }
        }
        private float m_collisionPolydensity;

        /// <summary>
        /// A dictionary of all the txds that are in this reference
        /// by name and size
        /// </summary>
        public Dictionary<String, uint> Txds
        {
            get { return m_txds; }
            set { m_txds = value; }
        }
        private Dictionary<String, uint> m_txds;

        #endregion // Properties

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public LevelReferenceStatistics(SingleReference stats)
        {
            this.Txds = new Dictionary<String, uint>();
            this.Polycount = 0;
            this.Polydensity = 0.0f;
            this.GeometrySize = 0;
            this.CollisionPolycount = 0;
            this.CollisionPolydensity = 0.0f;

            if (stats != null)
            {
                this.Polycount = stats.Polycount;
                this.Polydensity = stats.Polydensity;
                this.GeometrySize = stats.GeometrySize;
                this.CollisionPolycount = stats.CollisionPolycount;
                this.CollisionPolydensity = stats.CollisionPolydensity;

                foreach (var txd in stats.Txds)
                {
                    this.Txds.Add(txd.Key, txd.Value);
                }
            }
        }

        #endregion // Constructor
    }

    public class UniqueLevelReference
    {
        #region Properties

        public String Name
        { 
            get;
            set;
        }

        public LevelReferenceStatistics Statistics
        {
            get;
            set;
        }

        public int AppearenceCount
        {
            get;
            set;
        }

        public List<Vector3f> AppearenceLocations
        {
            get;
            set;
        }

        #endregion // Properties

        #region Constructor

        public UniqueLevelReference(String name, SingleReference stats)
        {
            this.Name = name;
            AppearenceLocations = new List<Vector3f>();
            AppearenceCount = 0;
            Statistics = new LevelReferenceStatistics(stats);
        }

        #endregion // Constructor
    }

    public class LevelReferences
    {
        #region Properties

        public Dictionary<String, Dictionary<String, UniqueLevelReference>> ReferenceFiles
        {
            get;
            set;
        }

        #endregion // Properties

        #region Constructor

        public LevelReferences()
        {
            ReferenceFiles = new Dictionary<String, Dictionary<String, UniqueLevelReference>>();
        }

        #endregion // Constructor

        #region Methods

        public void AddReference(String refFile, String refName, SingleReference stats)
        {
            if (!this.ReferenceFiles.ContainsKey(refFile))
            {
                this.ReferenceFiles.Add(refFile, new Dictionary<String, UniqueLevelReference>());
            }
            if (!this.ReferenceFiles[refFile].ContainsKey(refName))
            {
                this.ReferenceFiles[refFile].Add(refName, new UniqueLevelReference(refName, stats));
            }
        }

        public LevelReferenceStatistics GetReferenceStatistics(String refFile, String refName)
        {
            if (this.ReferenceFiles.ContainsKey(refFile))
            {
                if (this.ReferenceFiles[refFile].ContainsKey(refName))
                {
                    return this.ReferenceFiles[refFile][refName].Statistics;
                }
            }
            return null;
        }

        #endregion Methods
    }
}
