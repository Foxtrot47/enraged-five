﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Base.Collections;

namespace RSG.Model.Common.RadioStation
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [KnownType(typeof(RadioStation))]
    public class RadioStationCollection : AssetBase, IRadioStationCollection
    {
        #region Properties
        /// <summary>
        /// Radio stations that this collection contains.
        /// </summary>
        [DataMember]
        public ObservableCollection<IRadioStation> RadioStations { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public RadioStationCollection()
            : base("Radio Stations")
        {
            RadioStations = new ObservableCollection<IRadioStation>();
        }
        #endregion // Constructor(s)

        #region IEnumerable<IRadioStation> Implementation
        /// <summary>
        /// Loops through the radio stations and returns each one in turn
        /// </summary>
        IEnumerator<IRadioStation> IEnumerable<IRadioStation>.GetEnumerator()
        {
            foreach (IRadioStation radioStation in this.RadioStations)
            {
                yield return radioStation;
            }
        }

        /// <summary>
        /// Returns the enumerator for this object
        /// </summary>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerator)this);
        }
        #endregion // IEnumerable<IRadioStation> Implementation

        #region ICollection<IRadioStation> Implementation
        #region ICollection<IRadioStation> Properties
        /// <summary>
        /// Returns whether the collection is read-only
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Return number of items in the set.
        /// </summary>
        public int Count
        {
            get
            {
                return RadioStations.Count;
            }
        }
        #endregion // ICollection<IRadioStation> Properties

        #region ICollection<IRadioStation> Methods
        /// <summary>
        /// Add a radio station to the collection.
        /// </summary>
        /// <param name="item"></param>
        public void Add(IRadioStation item)
        {
            RadioStations.Add(item);
        }

        /// <summary>
        /// Removes all radio stations from the collection.
        /// </summary>
        public void Clear()
        {
            RadioStations.Clear();
        }

        /// <summary>
        /// Determine whether the collection contains a specific radio station
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Contains(IRadioStation item)
        {
            return RadioStations.Contains(item);
        }

        /// <summary>
        /// Copy items in the collection to an array, starting at a particular index.
        /// </summary>
        /// <param name="output"></param>
        /// <param name="index"></param>
        public void CopyTo(IRadioStation[] output, int index)
        {
            RadioStations.CopyTo(output, index);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        public bool Remove(IRadioStation item)
        {
            return RadioStations.Remove(item);
        }
        #endregion // ICollection<IRadioStation> Methods
        #endregion // ICollection<IRadioStation> Implementation
    } // RadioStationCollection
}
