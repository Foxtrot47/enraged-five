﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Map;

namespace RSG.Model.Report
{
    /// <summary>
    /// A report that generates it's content on the fly based on section data
    /// </summary>
    public interface IDynamicSectionReport
    {
        /// <summary>
        /// Generate the report dynamically.
        /// </summary>
        /// <param name="selectedSections">Sections to be used by the report</param>
        void Generate(List<MapSection> sections);
    } // IDynamicSectionReport
} // RSG.Model.Report 
