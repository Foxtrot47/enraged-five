﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using RSG.ManagedRage;
using RSG.Model.Asset;
using RSG.Model.Common;
using RSG.Model.Common.Weapon;
using RSG.Platform;
using RSG.SceneXml;
using RSG.SceneXml.Material;
using RSG.Statistics.Common.Dto.GameAssets.Bundles;

namespace RSG.Model.Weapon
{
    /// <summary>
    /// Weapon asset.
    /// </summary>
    public abstract class Weapon : StreamableAssetContainerBase, IWeapon
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Friendly name for the weapon (extracted from a localisation file).
        /// </summary>
        public String FriendlyName { get; protected set; }

        /// <summary>
        /// Name of the model associated with this weapon.
        /// </summary>
        public String ModelName { get; protected set; }

        /// <summary>
        /// Name of the stat associated with this weapon.
        /// </summary>
        public String StatName { get; protected set; }

        /// <summary>
        /// Vehicle category classification.
        /// </summary>
        [StreamableStat(StreamableWeaponStat.CategoryString)]
        public WeaponCategory Category
        {
            get
            {
                CheckStatLoaded(StreamableWeaponStat.Category);
                return m_category;
            }
            protected set
            {
                SetPropertyValue(ref m_category, value, "Category");
                SetStatLodaded(StreamableWeaponStat.Category, true);
            }
        }
        private WeaponCategory m_category;

        /// <summary>
        /// The collection this weapon is a part of
        /// </summary>
        [Browsable(false)]
        public IWeaponCollection ParentCollection { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public ITexture[] Textures
        {
            get
            {
                CheckStatLoaded(StreamableWeaponStat.Shaders);
                return m_textures;
            }
            private set
            {
                SetPropertyValue(ref m_textures, value, "Textures", "TextureCount");
            }
        }
        private ITexture[] m_textures;

        /// <summary>
        /// Texture count (exposed for the property grid)
        /// </summary>
        [Description("Number of unique textures this weapon uses.")]
        public int TextureCount
        {
            get
            {
                return (Textures != null ? Textures.Count() : 0);
            }
        }

        /// <summary>
        /// Array of weapon shaders.
        /// </summary>
        [Browsable(false)]
        [StreamableStat(StreamableWeaponStat.ShadersString)]
        public IShader[] Shaders
        {
            get
            {
                CheckStatLoaded(StreamableWeaponStat.Shaders);
                return m_shaders;
            }
            protected set
            {
                if (SetPropertyValue(ref m_shaders, value, "Shaders", "ShaderCount"))
                {
                    if (m_shaders != null)
                        Textures = m_shaders.SelectMany(shader => shader.Textures).Distinct().OrderBy(item => item.Name).ToArray();
                    else
                        Textures = null;
                }
                SetStatLodaded(StreamableWeaponStat.Shaders, true);
            }
        }
        private IShader[] m_shaders;

        /// <summary>
        /// Shader count (exposed for the property grid)
        /// </summary>
        [Description("Number of shaders this weapon uses.")]
        public int ShaderCount
        {
            get
            {
                return (Shaders != null ? Shaders.Count() : 0);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Description("Number of polygons this weapon consists of.")]
        [StreamableStat(StreamableWeaponStat.PolyCountString)]
        public int PolyCount
        {
            get
            {
                CheckStatLoaded(StreamableWeaponStat.PolyCount);
                return m_polyCount;
            }
            protected set
            {
                SetPropertyValue(ref m_polyCount, value, "PolyCount");
                SetStatLodaded(StreamableWeaponStat.PolyCount, true);
            }
        }
        private int m_polyCount;

        /// <summary>
        /// 
        /// </summary>
        [StreamableStat(StreamableWeaponStat.CollisionCountString)]
        public int CollisionCount
        {
            get
            {
                CheckStatLoaded(StreamableWeaponStat.CollisionCount);
                return m_collisionCount;
            }
            protected set
            {
                SetPropertyValue(ref m_collisionCount, value, "CollisionCount");
                SetStatLodaded(StreamableWeaponStat.CollisionCount, true);
            }
        }
        private int m_collisionCount;

        /// <summary>
        /// 
        /// </summary>
        [StreamableStat(StreamableWeaponStat.BoneCountString)]
        public int BoneCount
        {
            get
            {
                CheckStatLoaded(StreamableWeaponStat.BoneCount);
                return m_boneCount;
            }
            protected set
            {
                SetPropertyValue(ref m_boneCount, value, "BoneCount");
                SetStatLodaded(StreamableWeaponStat.BoneCount, true);
            }
        }
        private int m_boneCount;

        /// <summary>
        /// 
        /// </summary>
        [StreamableStat(StreamableWeaponStat.HasLodString)]
        public bool HasLod
        {
            get
            {
                CheckStatLoaded(StreamableWeaponStat.HasLod);
                return m_hasLod;
            }
            protected set
            {
                SetPropertyValue(ref m_hasLod, value, "HasLod");
                SetStatLodaded(StreamableWeaponStat.HasLod, true);
            }
        }
        private bool m_hasLod;

        /// <summary>
        /// 
        /// </summary>
        [StreamableStat(StreamableWeaponStat.LodPolyCountString)]
        public int LodPolyCount
        {
            get
            {
                CheckStatLoaded(StreamableWeaponStat.LodPolyCount);
                return m_LodPolyCount;
            }
            protected set
            {
                SetPropertyValue(ref m_LodPolyCount, value, "LodPolyCount");
                SetStatLodaded(StreamableWeaponStat.LodPolyCount, true);
            }
        }
        private int m_LodPolyCount;

        /// <summary>
        /// 
        /// </summary>
        [StreamableStat(StreamableWeaponStat.GripCountString)]
        public int GripCount
        {
            get
            {
                CheckStatLoaded(StreamableWeaponStat.GripCount);
                return m_gripCount;
            }
            protected set
            {
                SetPropertyValue(ref m_gripCount, value, "GripCount");
                SetStatLodaded(StreamableWeaponStat.GripCount, true);
            }
        }
        private int m_gripCount;

        /// <summary>
        /// 
        /// </summary>
        [StreamableStat(StreamableWeaponStat.MagazineCountString)]
        public int MagazineCount
        {
            get
            {
                CheckStatLoaded(StreamableWeaponStat.MagazineCount);
                return m_magazineCount;
            }
            protected set
            {
                SetPropertyValue(ref m_magazineCount, value, "MagazineCount");
                SetStatLodaded(StreamableWeaponStat.MagazineCount, true);
            }
        }
        private int m_magazineCount;

        /// <summary>
        /// 
        /// </summary>
        [StreamableStat(StreamableWeaponStat.AttachmentCountString)]
        public int AttachmentCount
        {
            get
            {
                CheckStatLoaded(StreamableWeaponStat.AttachmentCount);
                return m_attachmentCount;
            }
            protected set
            {
                SetPropertyValue(ref m_attachmentCount, value, "AttachmentCount");
                SetStatLodaded(StreamableWeaponStat.AttachmentCount, true);
            }
        }
        private int m_attachmentCount;

        /// <summary>
        /// Dictionary mapping platforms to platform stats.
        /// </summary>
        [Browsable(false)]
        [StreamableStat(StreamableWeaponStat.PlatformStatsString)]
        public IDictionary<RSG.Platform.Platform, IWeaponPlatformStat> PlatformStats
        {
            get
            {
                CheckStatLoaded(StreamableWeaponStat.PlatformStats);
                return m_platformStats;
            }
            protected set
            {
                SetPropertyValue(ref m_platformStats, value, "PlatformStats");
                SetStatLodaded(StreamableWeaponStat.PlatformStats, true);
            }
        }
        private IDictionary<RSG.Platform.Platform, IWeaponPlatformStat> m_platformStats;
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public Weapon(String name, String friendlyName, String modelName, String statName, WeaponCategory category, IWeaponCollection parentCollection)
            : base(name)
        {
            FriendlyName = friendlyName;
            ModelName = modelName;
            StatName = statName;
            Category = category;
            ParentCollection = parentCollection;
        }
        #endregion // Constructor(s)

        #region IWeapon Implementation
        /// <summary>
        /// Returns a platform stat for a particular platform
        /// </summary>
        /// <param name="platform"></param>
        /// <returns></returns>
        public IWeaponPlatformStat GetPlatformStat(RSG.Platform.Platform platform)
        {
            IWeaponPlatformStat platformStat = null;
            if (PlatformStats.ContainsKey(platform))
            {
                platformStat = PlatformStats[platform];
            }
            return platformStat;
        }
        #endregion // IWeapon Implementation

        #region IComparable<IWeapon> Interface
        /// <summary>
        /// Compare this weapon to another.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(IWeapon other)
        {
            if (other == null)
            {
                return 1;
            }

            return (Name.CompareTo(other.Name));
        }
        #endregion // IComparable<IWeapon> Interface

        #region IEquatable<IWeapon> Interface
        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>true if the current object is equal to the other parameter; otherwise, false.</returns>
        public bool Equals(IWeapon other)
        {
            if (other == null)
            {
                return false;
            }

            return (Name == other.Name);
        }
        #endregion // IEquatable<IWeapon> Interface

        #region Object Overrides
        /// <summary>
        /// Compare
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(Object obj)
        {
            if (obj == null)
            {
                return base.Equals(obj);
            }

            return ((obj is IWeapon) && Equals(obj as IWeapon));
        }

        /// <summary>
        /// Return String representation of the weapon.
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return Name;
        }

        /// <summary>
        /// Return hash code of weapon (based on Name).
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return (this.Name.GetHashCode());
        }
        #endregion // Object Overrides
    } // Weapon
} // RSG.Model.Weapon
