﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using Ionic.Zip;
using RSG.Base.Extensions;
using RSG.Base.Tasks;
using RSG.Model.Common;
using RSG.Model.Common.Map;
using RSG.Model.Report;

namespace RSG.Model.Report2.Reports.Map
{
    /// <summary>
    /// 
    /// </summary>
    public class ExportedAssetLocatorReport : HTMLReport, IDynamicLevelReport
    {
        #region Constants
        private const String c_name = "Exported Asset Locator";
        private const String c_description = "Searches through the export data for a list of files.";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (m_task == null)
                {
                    CompositeTask task = new CompositeTask("Generate Report");
                    task.AddSubTask(new ActionTask("Loading data", (context, progress) => EnsureDataLoaded((DynamicLevelReportContext)context, progress)), 50);
                    task.AddSubTask(new ActionTask("Generating report", (context, progress) => GenerateReport((DynamicLevelReportContext)context, progress)));
                    m_task = task;
                }
                return m_task;
            }
        }
        private ITask m_task;

        /// <summary>
        /// List of stats that are required to generate this report.
        /// </summary>
        public IEnumerable<StreamableStat> RequiredStats
        {
            get { return null; }
        }

        /// <summary>
        /// The file that contains the list of assets to search for.
        /// </summary>
        public String InputFile { get; set; }

        /// <summary>
        /// String for the regex to use to match files to search in.
        /// </summary>
        public String FileFilterRegex
        {
            set
            {
                FilterRegex = new Regex(value);
            }
        }

        private Regex FilterRegex { get; set; }
        #endregion // Properties
        
        #region Private Classes
        /// <summary>
        /// Class to store information about a single occurrence of an asset we are searching for.
        /// </summary>
        private class AssetOccurrence
        {
            public String AssetName { get; set; }
            public String ExportZipFilename { get; set; }
            public String AssetZipFilename { get; set; }
            
        } // AssetOccurrence
        #endregion // Private Classes

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ExportedAssetLocatorReport()
            : base(c_name, c_description)
        {
        }
        #endregion // Constructor(s)

        #region Task Methods
        /// <summary>
        /// Task for making sure that all the data required by this report is loaded.
        /// NOTE: eventually this will be handled at a higher level.
        /// </summary>
        private void EnsureDataLoaded(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            IMapHierarchy hierarchy = context.Level.MapHierarchy;

            StreamableStat[] statsToLoad = new StreamableStat[] {
                StreamableMapSectionStat.ExportZipFilepath };
            ITask dataLoadTask = hierarchy.CreateStatLoadTask(hierarchy.AllSections, statsToLoad);

            if (dataLoadTask != null)
            {
                (dataLoadTask as CompositeTask).ExecuteInParallel = true;
                dataLoadTask.Execute(context);
            }
        }

        /// <summary>
        /// Task for parsing the texture input file, searching for those textures in the level data and
        /// generating the resulting HTML report stream.
        /// </summary>
        private void GenerateReport(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            IList<String> textureNames = ParseInputFile(Path.GetFullPath(InputFile));
            IList<AssetOccurrence> occurrences = SearchForAssets(context.Level.MapHierarchy, textureNames);
            Stream = WriteReport(occurrences, textureNames);
        }
        #endregion // Task Methods

        #region Private Methods
        /// <summary>
        /// Parses the file containing the list of textures to search for.
        /// </summary>
        private IList<String> ParseInputFile(String filename)
        {
            Debug.Assert(File.Exists(filename), String.Format("Asset locator input file doesn't exist at {0}", filename));

            IList<String> assetNames = new List<String>();
            if (File.Exists(filename))
            {
                using (StreamReader reader = new StreamReader(filename))
                {
                    String line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        if (!line.StartsWith("#"))
                        {
                            assetNames.Add(line);
                        }
                    }
                }
            }
            return assetNames;
        }

        /// <summary>
        /// Uses the map hierarchy to locate the assets in the exported zip files..
        /// </summary>
        private IList<AssetOccurrence> SearchForAssets(IMapHierarchy hierarchy, IList<String> assetNames)
        {
            IList<AssetOccurrence> occurrences = new List<AssetOccurrence>();
            
            // Iterate over all the sections.
            IEnumerable<IMapSection> sections = hierarchy.AllSections;
            sections.AsParallel().ForAll(section =>
            {
                Debug.Assert(File.Exists(section.ExportZipFilepath),
                    String.Format("Export zip file for {0} doesn't exist at {1}", section.Name, section.ExportZipFilepath));

                if (File.Exists(section.ExportZipFilepath))
                {
                    using (ZipFile file = ZipFile.Read(section.ExportZipFilepath))
                    {
                        foreach (ZipEntry entry in file.Entries.Where(item => ShouldExtractZip(item.FileName)))
                        {
                            using (MemoryStream zipStream = new MemoryStream())
                            {
                                entry.Extract(zipStream);
                                zipStream.Position = 0;

                                // Look for the assets in the sub zip.
                                IList<AssetOccurrence> subOccurrences = FindOccurrencesInSubFile(zipStream, assetNames);
                                foreach (AssetOccurrence occurrence in subOccurrences)
                                {
                                    occurrence.ExportZipFilename = section.ExportZipFilepath;
                                    occurrence.AssetZipFilename = entry.FileName;
                                }

                                // Add them to our running total.
                                lock (occurrences)
                                {
                                    occurrences.AddRange(subOccurrences);
                                }
                            }
                        }
                    }
                }
            });

            return occurrences;
        }

        /// <summary>
        /// Checks whether we should look into a particular zip file.
        /// </summary>
        private bool ShouldExtractZip(String filename)
        {
            return FilterRegex.Match(filename).Success;
        }

        /// <summary>
        /// Returns occurrences of the assets in a zip file stream.
        /// </summary>
        private IList<AssetOccurrence> FindOccurrencesInSubFile(Stream stream, IList<String> assetNames)
        {
            IList<AssetOccurrence> occurrences = new List<AssetOccurrence>();
            using (ZipFile file = ZipFile.Read(stream))
            {
                foreach (String entryName in file.Entries.Select(item => item.FileName).Intersect(assetNames))
                {
                    occurrences.Add(new AssetOccurrence { AssetName = entryName });
                }
            }
            return occurrences;
        }

        /// <summary>
        /// Creates the HTML stream containing the report data.
        /// </summary>
        private Stream WriteReport(IList<AssetOccurrence> occurrences, IList<String> assetNames)
        {
            // Generate the stream for the report
            MemoryStream stream = new MemoryStream();
            StreamWriter streamWriter = new StreamWriter(stream);

            // Start writing out the report
            HtmlTextWriter writer = new HtmlTextWriter(streamWriter);
            writer.RenderBeginTag(HtmlTextWriterTag.Html);
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Head);
                writer.RenderEndTag();

                writer.RenderBeginTag(HtmlTextWriterTag.Body);
                {
                    WriteHeader(writer, "Asset Use Report");
                    OutputSummary(writer, occurrences, assetNames);

                    if (occurrences.Any())
                    {
                        OutputOccurrencesBySection(writer, occurrences);
                        OutputOccurrencesByTexture(writer, occurrences);
                    }
                }
                writer.RenderEndTag();

            }
            writer.RenderEndTag();
            writer.Flush();

            // Reset the stream before returning it
            stream.Position = 0;
            return stream;
        }

        /// <summary>
        /// Outputs a summary of the information we found
        /// </summary>
        /// <param name="writer"></param>
        private void OutputSummary(HtmlTextWriter writer, IList<AssetOccurrence> occurrences, IList<String> assetNames)
        {
            // Generate the summary text
            if (!occurrences.Any())
            {
                WriteParagraph(writer, "None of the assets in the input file where found in the export data.");
            }
            else
            {
                String summaryText = String.Format("{0} of the assets in the input file are being used by {1} sections.",
                                                   assetNames.Count,
                                                   occurrences.GroupBy(item => item.ExportZipFilename).Count());
                WriteParagraph(writer, summaryText);

                // Output the table of contents.
                writer.RenderBeginTag(HtmlTextWriterTag.Div);
                writer.AddAttribute(HtmlTextWriterAttribute.Href, "#BySection");
                writer.RenderBeginTag(HtmlTextWriterTag.A);
                writer.Write("Results by Section");
                writer.RenderEndTag();
                writer.RenderEndTag();

                writer.AddAttribute(HtmlTextWriterAttribute.Style, "margin-left: 50px;");
                writer.RenderBeginTag(HtmlTextWriterTag.Div);

                IEnumerable<AssetOccurrence> sortedOccurrences =
                    occurrences.OrderBy(item => item.ExportZipFilename);
                foreach (IGrouping<String, AssetOccurrence> sectionGroup in sortedOccurrences.GroupBy(item => item.ExportZipFilename))
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.Div);
                    writer.AddAttribute(HtmlTextWriterAttribute.Href, String.Format("#{0}", sectionGroup.Key));
                    writer.RenderBeginTag(HtmlTextWriterTag.A);
                    writer.Write(sectionGroup.Key);
                    writer.RenderEndTag();
                    writer.RenderEndTag();
                }
                writer.RenderEndTag();

                writer.RenderBeginTag(HtmlTextWriterTag.Div);
                writer.AddAttribute(HtmlTextWriterAttribute.Href, "#ByAsset");
                writer.RenderBeginTag(HtmlTextWriterTag.A);
                writer.Write("Results by Asset");
                writer.RenderEndTag();
                writer.RenderEndTag();
                /*
                writer.AddAttribute(HtmlTextWriterAttribute.Style, "margin-left: 50px;");
                writer.RenderBeginTag(HtmlTextWriterTag.Div);

                sortedOccurrences =
                    occurrences.OrderBy(item => item.TextureName);
                foreach (IGrouping<String, TextureOccurrence> textureGroup in sortedOccurrences.GroupBy(item => item.Section))
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.Div);
                    writer.AddAttribute(HtmlTextWriterAttribute.Href, String.Format("#{0}", textureGroup.Key));
                    writer.RenderBeginTag(HtmlTextWriterTag.A);
                    writer.Write(textureGroup.Key);
                    writer.RenderEndTag();
                    writer.RenderEndTag();
                }
                writer.RenderEndTag();
                */
            }
        }

        /// <summary>
        /// Outputs the texture information grouped by section then archetype
        /// </summary>
        private void OutputOccurrencesBySection(HtmlTextWriter writer, IList<AssetOccurrence> occurrences)
        {
            // <h2><a name="BySection">By Section</a></h2>
            writer.RenderBeginTag(HtmlTextWriterTag.H2);
            writer.AddAttribute(HtmlTextWriterAttribute.Name, "BySection");
            writer.RenderBeginTag(HtmlTextWriterTag.A);
            writer.Write("Results by Section");
            writer.RenderEndTag();
            writer.RenderEndTag();

            // Group by section
            writer.AddAttribute(HtmlTextWriterAttribute.Style, "margin: 0 auto;");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            IEnumerable<AssetOccurrence> sortedOccurrences =
                occurrences.OrderBy(item => item.ExportZipFilename)
                           .ThenBy(item => item.AssetZipFilename)
                           .ThenBy(item => item.AssetName);

            foreach (IGrouping<String, AssetOccurrence> sectionGroup in sortedOccurrences.GroupBy(item => item.ExportZipFilename))
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Name, sectionGroup.Key);
                writer.RenderBeginTag(HtmlTextWriterTag.A);
                writer.RenderBeginTag(HtmlTextWriterTag.B);
                writer.Write(sectionGroup.Key);
                writer.RenderEndTag();
                writer.RenderEndTag();

                writer.AddAttribute(HtmlTextWriterAttribute.Style, "margin-left: 50px;");
                writer.RenderBeginTag(HtmlTextWriterTag.Div);

                // Group by archetype
                foreach (IGrouping<String, AssetOccurrence> assetZipGroup in sectionGroup.GroupBy(item => item.AssetZipFilename))
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.I);
                    writer.Write(assetZipGroup.Key);
                    writer.RenderEndTag();

                    writer.AddAttribute(HtmlTextWriterAttribute.Style, "margin-left: 50px;margin-bottom:10px;");
                    writer.RenderBeginTag(HtmlTextWriterTag.Div);

                    foreach (IGrouping<String, AssetOccurrence> assetGroup in assetZipGroup.GroupBy(item => item.AssetName))
                    {
                        writer.RenderBeginTag(HtmlTextWriterTag.Div);
                        writer.Write(assetGroup.Key);
                        writer.RenderEndTag();
                    }
                    writer.RenderEndTag();
                }
                writer.RenderEndTag();
            }
            writer.RenderEndTag();
        }

        private void OutputOccurrencesByTexture(HtmlTextWriter writer, IList<AssetOccurrence> occurrences)
        {
            // <h2><a name="ByAsset">By Asset</a></h2>
            writer.RenderBeginTag(HtmlTextWriterTag.H2);
            writer.AddAttribute(HtmlTextWriterAttribute.Name, "ByAsset");
            writer.RenderBeginTag(HtmlTextWriterTag.A);
            writer.Write("Results by Asset");
            writer.RenderEndTag();
            writer.RenderEndTag();

            // Group by section
            writer.AddAttribute(HtmlTextWriterAttribute.Style, "margin: 0 auto;");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            IEnumerable<AssetOccurrence> sortedOccurrences =
                occurrences.OrderBy(item => item.AssetName)
                           .ThenBy(item => item.ExportZipFilename)
                           .ThenBy(item => item.AssetZipFilename);

            foreach (IGrouping<String, AssetOccurrence> assetGroup in sortedOccurrences.GroupBy(item => item.AssetName))
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Name, assetGroup.Key);
                writer.RenderBeginTag(HtmlTextWriterTag.A);
                writer.RenderBeginTag(HtmlTextWriterTag.B);
                writer.Write(assetGroup.Key);
                writer.RenderEndTag();
                writer.RenderEndTag();

                writer.AddAttribute(HtmlTextWriterAttribute.Style, "margin-left: 50px;");
                writer.RenderBeginTag(HtmlTextWriterTag.Div);

                // Group by archetype
                foreach (IGrouping<String, AssetOccurrence> sectionGroup in assetGroup.GroupBy(item => item.ExportZipFilename))
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.I);
                    writer.Write(sectionGroup.Key);
                    writer.RenderEndTag();

                    writer.AddAttribute(HtmlTextWriterAttribute.Style, "margin-left: 50px;margin-bottom:10px;");
                    writer.RenderBeginTag(HtmlTextWriterTag.Div);

                    foreach (IGrouping<String, AssetOccurrence> assetZipGroup in sectionGroup.GroupBy(item => item.AssetZipFilename))
                    {
                        writer.RenderBeginTag(HtmlTextWriterTag.Div);
                        writer.Write(assetZipGroup.Key);
                        writer.RenderEndTag();
                    }
                    writer.RenderEndTag();
                }
                writer.RenderEndTag();
            }
            writer.RenderEndTag();
        }
        #endregion // Private Methods
    } // ExportedAssetLocatorReport
}
