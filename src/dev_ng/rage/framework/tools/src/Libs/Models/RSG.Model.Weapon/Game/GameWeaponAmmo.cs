﻿namespace RSG.Model.Weapon.Game
{
    using System.Collections.Generic;
    using System.Diagnostics;

    /// <summary>
    /// Represents a single Weapon that is being mounted by the game through the waepons.meta
    /// data file.
    /// </summary>
    [DebuggerDisplay("Name = {Name}, Model = {Model}")]
    public class GameWeaponAmmo
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Name"/> property.
        /// </summary>
        private string _name;

        /// <summary>
        /// The private field used for the <see cref="Model"/> property.
        /// </summary>
        private string _model;

        /// <summary>
        /// The private field used for the <see cref="Effects"/> property.
        /// </summary>
        private readonly List<string> _effects;

        /// <summary>
        /// The private field used for the <see cref="Archetype"/> property.
        /// </summary>
        private GameWeaponArchetype _archetype;

        /// <summary>
        /// The private field used for the <see cref="ReferenceCount"/> property.
        /// </summary>
        private int _referenceCount;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="GameWeaponAmmo"/> class.
        /// </summary>
        public GameWeaponAmmo()
        {
            this._effects = new List<string>();
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the weapon archetype used for this weapon ammo object.
        /// </summary>
        public GameWeaponArchetype Archetype
        {
            get { return this._archetype; }
            set { this._archetype = value; }
        }

        /// <summary>
        /// Gets or sets the name used for this weapon.
        /// </summary>
        public string Name
        {
            get { return this._name; }
            set { this._name = value; }
        }

        /// <summary>
        /// Gets or sets the name of the model used for this weapon.
        /// </summary>
        public string Model
        {
            get { return this._model; }
            set { this._model = value; }
        }

        /// <summary>
        /// Gets or sets the name of the texture dictionary containing the textures used on
        /// this weapon ammo object.
        /// </summary>
        public string TextureDictionaryName
        {
            get
            {
                if (this._archetype == null)
                {
                    return null;
                }

                return this._archetype.TextureDictionaryName;
            }
        }

        /// <summary>
        /// Gets or sets the name of fx asset dictionary that this weapon ammo object uses.
        /// </summary>
        public string EffectsDictionaryName
        {
            get
            {
                if (this._archetype == null)
                {
                    return null;
                }

                return this._archetype.EffectsDictionaryName;
            }
        }

        /// <summary>
        /// Gets or sets a iterator around the individual effects used for this weapon.
        /// </summary>
        public IEnumerable<string> Effects
        {
            get
            {
                foreach (string effect in this._effects)
                {
                    yield return effect;
                }
            }
        }

        /// <summary>
        /// Gets or sets the number of times this archetype has been referenced by either a
        /// weapon, component, or ammo object.
        /// </summary>
        public int ReferenceCount
        {
            get { return this._referenceCount; }
            set { this._referenceCount = value; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Adds the specified effect name into this weapons effects list.
        /// </summary>
        /// <param name="effectName">
        /// The name of the effect to add.
        /// </param>
        public void AddEffect(string effectName)
        {
            this._effects.Add(effectName);
        }
        #endregion Methods
    } // RSG.Model.Weapon.Game.GameWeaponAmmo {Class}
} // RSG.Model.Weapon.Game {Namespace}
