﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using RSG.Model.Common;
using RSG.Model.Common.Map;
using RSG.Base.Math;
using RSG.SceneXml;

namespace RSG.Model.Asset
{
    public class ChainingGraphNode : IChainingGraphNode
    {
        #region Constants
        private const string c_positionElement = "Position";
        private const string c_attachedToEntityTypeElement = "AttachedToEntityType";
        private const string c_scenarioTypeElement = "ScenarioType";
        private const string c_hasIncomingEdgesElement = "HasIncomingEdges";
        private const string c_hasOutgoingEdgesElement = "HasOutgoingEdges";

        private const string c_xAttribute = "x";
        private const string c_yAttribute = "y";
        private const string c_zAttribute = "z";

        #endregion // Constants

        #region Properties
        /// <summary>
        /// Position in world-space.
        /// </summary>
        public Vector3f Position
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public string AttachedEntity
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public string ScenarioType
        {
            get;
            private set;
        }

        /// <summary>
        /// Model set override for the scenario point.
        /// </summary>
        public bool HasIncomingEdges
        {
            get;
            private set;
        }

        /// <summary>
        /// Game modes this spawn point is enabled in.
        /// </summary>
        public bool HasOutgoingEdges
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlElem"></param>
        /// <param name="sourceFile"></param>
        public ChainingGraphNode(XElement xmlElem)
        {
            //Position Element
            XElement posElem = xmlElem.Element(c_positionElement);
            if (posElem != null)
            {
                XAttribute xAtt = posElem.Attribute(c_xAttribute);
                XAttribute yAtt = posElem.Attribute(c_yAttribute);
                XAttribute zAtt = posElem.Attribute(c_zAttribute);

                if (xAtt != null && yAtt != null && zAtt != null)
                {
                    Position = new Vector3f((float)Double.Parse(xAtt.Value), (float)Double.Parse(yAtt.Value), (float)Double.Parse(zAtt.Value));
                }
            }

            //Attached Entity Element
            XElement attachedEntityElem = xmlElem.Element(c_attachedToEntityTypeElement);
            if (attachedEntityElem != null)
            {
                AttachedEntity = attachedEntityElem.Value;
            }

            //Scenario Type Element
            XElement scenarioTypeElement = xmlElem.Element(c_scenarioTypeElement);
            if (scenarioTypeElement != null)
            {
                ScenarioType = scenarioTypeElement.Value;
            }

            //Has Incoming Edges Element
            XElement incomingEdgeElements = xmlElem.Element(c_hasIncomingEdgesElement);            
            if (incomingEdgeElements != null )
            {
                bool incomingEdges = false;
                XAttribute valueAttribute = incomingEdgeElements.Attribute("value");
                if (valueAttribute != null && bool.TryParse(valueAttribute.Value, out incomingEdges))
                {
                    HasIncomingEdges = incomingEdges;
                }
            }

            //Has Incoming Edges Element
            XElement outgoingEdgeElements = xmlElem.Element(c_hasOutgoingEdgesElement);
            if (outgoingEdgeElements != null)
            {
                bool outgoingEdges = false;
                XAttribute valueAttribute = incomingEdgeElements.Attribute("value");
                if (valueAttribute != null && bool.TryParse(valueAttribute.Value, out outgoingEdges))
                {
                    HasOutgoingEdges = outgoingEdges;
                }                
            }
        }
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    public class ChainingGraphEdge : IChainingGraphEdge
    {
        #region Constants
        private const string c_fromElement = "NodeIndexFrom";
        private const string c_toElement = "NodeIndexTo";
        private const string c_navModeElement = "NavMode";
        private const string c_navSpeedElement = "NavSpeed";
        #endregion

        #region Properties
        public int NodeIndexFrom
        {
            get;
            private set;
        }

        public int NodeIndexTo
        {
            get;
            private set;
        }

        public string NavMode
        {
            get;
            private set;
        }

        public string NavSpeed
        {
            get;
            private set;
        }
        #endregion

        public ChainingGraphEdge(XElement xmlElem)
        {
            //Node Index From Element
            NodeIndexFrom = -1;
            XElement nodeIndexFromElement = xmlElem.Element(c_fromElement);
            if (nodeIndexFromElement != null)
            {
                int fromIndex = -1;
                XAttribute valueAttribute = nodeIndexFromElement.Attribute("value");
                if (valueAttribute != null && Int32.TryParse(valueAttribute.Value, out fromIndex))
                {
                    NodeIndexFrom = fromIndex;
                }
            }

            //Node Index To Element
            NodeIndexTo = -1;
            XElement nodeIndexToElement = xmlElem.Element(c_toElement);
            if (nodeIndexFromElement != null)
            {
                int toIndex = -1;
                XAttribute valueAttribute = nodeIndexToElement.Attribute("value");
                if (valueAttribute != null && Int32.TryParse(valueAttribute.Value, out toIndex))
                {
                    NodeIndexTo = toIndex;
                }
            }

            //Nav Mode Element
            XElement navModeElement = xmlElem.Element(c_navModeElement);
            if (navModeElement != null)
            {
                NavMode = navModeElement.Value;
            }

            //Nav Speed Element
            XElement navSpeedElement = xmlElem.Element(c_navSpeedElement);
            if (navSpeedElement != null)
            {
                NavSpeed = navSpeedElement.Value;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ChainingGraph : IChainingGraph
    {
        #region Properties
        public string SourceFile
        {
            get;
            private set;
        }

        public List<IChainingGraphNode> ChainedNodes
        {
            get;
            private set;
        }
    
        public List<IChainingGraphEdge> ChainedEdges
        {
            get;
            private set;
        }

        public List<LinkedList<IChainingGraphNode>> ChainedPaths
        {
            get;
            private set;
        }
        #endregion

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlElem"></param>
        /// <param name="sourceFile"></param>
        public ChainingGraph(XElement xmlElem, string sourceFile)
        {
            SourceFile = sourceFile;
            ChainedNodes = new List<IChainingGraphNode>();
            ChainedEdges = new List<IChainingGraphEdge>();
            ChainedPaths = new List<LinkedList<IChainingGraphNode>>();
            
            foreach(XElement element in xmlElem.Elements())
            {
                if ( String.Compare(element.Name.LocalName, "Nodes", true) == 0)
                {
                    foreach (XElement itemElement in element.Elements())
                    {
                        ChainedNodes.Add(new ChainingGraphNode(itemElement));
                    }
                }
            }

            foreach (XElement element in xmlElem.Elements())
            {
                if (String.Compare(element.Name.LocalName, "Edges", true) == 0)
                {
                    foreach (XElement itemElement in element.Elements())
                    {
                        ChainedEdges.Add(new ChainingGraphEdge(itemElement));
                    }
                }
            }

            if (ChainedEdges.Count > 0)
            {
                List<bool> processedEdges = new List<bool>(new bool[ChainedEdges.Count]);
                List<bool> processedNodes = new List<bool>(new bool[ChainedNodes.Count]);

                int currentEdgeIndex = 0;
                IChainingGraphEdge currentEdge = ChainedEdges[0];

                while (true)
                {
                    int processedEdgeCount = 0;
                    foreach (bool isProcessed in processedEdges.Where(item => item == true))
                    {
                        processedEdgeCount++;
                    }

                    if (processedEdgeCount >= processedEdges.Count)
                    {
                        break;
                    }

                    IChainingGraphNode fromNode = ChainedNodes[currentEdge.NodeIndexFrom];
                    IChainingGraphNode toNode = ChainedNodes[currentEdge.NodeIndexTo];

                    //Find if this node is already in a list somewhere.
                    LinkedList<IChainingGraphNode> currentList = null;
                    LinkedListNode<IChainingGraphNode> foundNode = null;
                    foreach (LinkedList<IChainingGraphNode> linkedList in ChainedPaths)
                    {
                        if (linkedList.Contains(fromNode))
                        {
                            currentList = linkedList;
                            foundNode = currentList.Find(fromNode);
                            break;
                        }
                    }

                    if (currentList == null)
                    {
                        currentList = new LinkedList<IChainingGraphNode>();
                        LinkedListNode<IChainingGraphNode> newNode = currentList.AddFirst(fromNode);
                        currentList.AddAfter(newNode, toNode);
                        ChainedPaths.Add(currentList);
                    }
                    else
                    {
                        currentList.AddAfter(foundNode, toNode);
                    }

                    processedEdges[currentEdgeIndex] = true;
                    processedNodes[currentEdge.NodeIndexFrom] = true;

                    //If we have already processed the 'to' edge, then we have completed a path
                    //and need to move onto the next unprocessed edge.
                    bool found = false;
                    if ( processedNodes[currentEdge.NodeIndexTo] == false )
                    {
                        //Move to the next link in the path.  Need to find the next edge.
                        for (int edgeIndex = 0; edgeIndex < ChainedEdges.Count;  ++edgeIndex)
                        {
                            if ( ChainedEdges[edgeIndex].NodeIndexFrom == currentEdge.NodeIndexTo )
                            {
                                currentEdge = ChainedEdges[edgeIndex];
                                currentEdgeIndex = edgeIndex;
                                found = true;
                                break;
                            }
                        }
                    }
                    
                    if (found == false)
                    {
                        for (int processedIndex = 0; processedIndex < processedEdges.Count; ++processedIndex)
                        {
                            if (processedEdges[processedIndex] == false)
                            {
                                currentEdge = ChainedEdges[processedIndex];
                                currentEdgeIndex = processedIndex;
                                break;
                            }
                        }
                    }
                }
            }

        }
        #endregion // Constructor
    } // ChainingGraph




}
