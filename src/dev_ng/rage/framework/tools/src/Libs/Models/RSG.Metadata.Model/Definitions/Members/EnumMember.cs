﻿// --------------------------------------------------------------------------------------------
// <copyright file="EnumMember.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions.Members
{
    using System;
    using System.Xml;
    using RSG.Base;
    using RSG.Base.Extensions;
    using RSG.Base.Logging;
    using RSG.Editor;
    using RSG.Editor.Model;
    using RSG.Metadata.Model.Resources;
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// Represents the &lt;enum&gt; member node in the parCodeGen system that can be instanced
    /// in a metadata file.
    /// </summary>
    public class EnumMember :
        MemberBase, IHasInitialValue<IEnumConstant>, IEquatable<EnumMember>
    {
        #region Fields
        /// <summary>
        /// The name of the xml attribute containing the data for the
        /// <see cref="InitialValue"/> property.
        /// </summary>
        private const string XmlInitialAttr = "init";

        /// <summary>
        /// The name of the xml attribute containing the data for the
        /// <see cref="Size"/> property.
        /// </summary>
        private const string XmlSizeAttr = "size";

        /// <summary>
        /// The name of the xml attribute containing the data for the
        /// <see cref="SourceType"/> property.
        /// </summary>
        private const string XmlTypeAttr = "type";

        /// <summary>
        /// The private field used for the <see cref="InitialValue"/> property.
        /// </summary>
        private string _initialValue;

        /// <summary>
        /// The private field used for the <see cref="Size"/> property.
        /// </summary>
        private string _size;

        /// <summary>
        /// The private field used for the <see cref="SourceType"/> property.
        /// </summary>
        private string _sourceType;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="EnumMember"/>
        /// class to be one of the members of the specified structure.
        /// </summary>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        public EnumMember(IStructure structure)
            : base(structure)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="EnumMember"/>
        /// class as a copy of the specified instance.
        /// </summary>
        /// <param name="other">
        /// The instance to copy.
        /// </param>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        public EnumMember(EnumMember other, IStructure structure)
            : base(other, structure)
        {
            this._initialValue = other._initialValue;
            this._size = other._size;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="EnumMember"/>
        /// class.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        public EnumMember(XmlReader reader, IStructure structure)
            : base(reader, structure)
        {
            this.Deserialise(reader);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the initial value that any instance of this member is set to.
        /// </summary>
        public IEnumConstant InitialValue
        {
            get
            {
                IEnumeration source = this.Source;
                if (source == null)
                {
                    return null;
                }

                TryResult<long> tryValue = this.Dictionary.TryTo<long>(this._initialValue, 0);
                if (tryValue.Success)
                {
                    return this.Source[tryValue.Value];
                }
                else
                {
                    return this.Source[this._initialValue];
                }
            }

            set
            {
                this.SetProperty(ref this._initialValue, value.ToString());
            }
        }

        /// <summary>
        /// Gets or sets the size in bits of the enum field. Can be 8,16, or 32.
        /// </summary>
        public byte Size
        {
            get { return this.Dictionary.To<byte>(this._size, 32); }
            set { this.SetProperty(ref this._size, value.ToString()); }
        }

        /// <summary>
        /// Gets the enumeration used by this enum member as a source for the possible values.
        /// </summary>
        public IEnumeration Source
        {
            get { return this.GetEnumerationSource(this.Structure.Dictionary); }
        }

        /// <summary>
        /// Gets or sets the data type for the enumeration that is used as the source.
        /// </summary>
        public string SourceType
        {
            get
            {
                return this._sourceType;
            }

            set
            {
                string newValue = value.ToString();
                this.SetProperty(ref this._sourceType, newValue, "SourceType", "Source");
            }
        }

        /// <summary>
        /// Gets the type name for this member.
        /// </summary>
        public override string TypeName
        {
            get { return "enum"; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new <see cref="EnumMember"/> that is a copy of this instance.
        /// </summary>
        /// <returns>
        /// A new <see cref="EnumMember"/> that is a copy of this instance.
        /// </returns>
        public new EnumMember Clone()
        {
            return new EnumMember(this, this.Structure);
        }

        /// <summary>
        /// Creates a <see cref="ITunable"/> object that represents a instance of this member
        /// with it's default value set.
        /// </summary>
        /// <param name="parent">
        /// The tunable parent the new tunable will belong to.
        /// </param>
        /// <returns>
        /// A parCodeGen tunable that represents a instance of this member.
        /// </returns>
        public override ITunable CreateTunable(ITunableParent parent)
        {
            return new EnumTunable(this, parent);
        }

        /// <summary>
        /// Creates a <see cref="ITunable"/> object that represents a instance of this member
        /// using the specified Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The Xml.XmlReader that provides the data used to initialise the new tunable.
        /// </param>
        /// <param name="parent">
        /// The tunable parent the new tunable will belong to.
        /// </param>
        /// <param name="log">
        /// A object that is used to log any warnings or errors produced during the parsing of
        /// the specified reader.
        /// </param>
        /// <returns>
        /// A parCodeGen tunable that represents a instance of this member.
        /// </returns>
        public override ITunable CreateTunable(
            XmlReader reader, ITunableParent parent, ILog log)
        {
            return new EnumTunable(reader, this, parent, log);
        }

        /// <summary>
        /// Creates a deep copy of the current instance.
        /// </summary>
        /// <returns>
        /// A deep copy of the current System.Object.
        /// </returns>
        public override object DeepClone()
        {
            return this.Clone();
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="EnumMember"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public bool Equals(EnumMember other)
        {
            if (other == null)
            {
                return false;
            }

            return object.ReferenceEquals(other, this);
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="IMember"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public override bool Equals(IMember other)
        {
            return this.Equals(other as EnumMember);
        }

        /// <summary>
        /// Writes the xml representation of this instance onto the specified
        /// System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that the xml representation of this instance should be
        /// written to.
        /// </param>
        public override void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                throw new SmartArgumentNullException(() => writer);
            }

            this.SerialiseBaseAttributes(writer);
            if (this._sourceType != null)
            {
                writer.WriteAttributeString(XmlTypeAttr, this._sourceType);
            }

            if (this._initialValue != null)
            {
                writer.WriteAttributeString(XmlInitialAttr, this._initialValue);
            }

            if (this._size != null)
            {
                writer.WriteAttributeString(XmlSizeAttr, this._size);
            }
        }

        /// <summary>
        /// Validates this entity and creates a validation result object containing the errors
        /// and warnings.
        /// </summary>
        /// <param name="recursive">
        /// A value indicating whether any child models to this one should also be validated.
        /// </param>
        /// <returns>
        /// A validation result object containing all of the errors and warnings associated
        /// with this validation pass.
        /// </returns>
        public override ValidationResult Validate(bool recursive)
        {
            ValidationResult result = new ValidationResult();
            this.ValidateBaseProperties(result);

            if (this._sourceType == null)
            {
                string msg = StringTable.MissingRequiredAttributeError;
                msg = msg.FormatCurrent("type", "enum");
                result.AddError(msg, this.Location);
            }
            else if (String.IsNullOrWhiteSpace(this._sourceType))
            {
                string msg = StringTable.EmptyRequiredAttributeError;
                msg = msg.FormatCurrent("type", "enum");
                result.AddError(msg, this.Location);
            }

            TryResult<byte> sizeTry = this.Dictionary.TryTo<byte>(this._size, 32);
            if (this._size != null && !sizeTry.Success)
            {
                string msg = StringTable.FormatAttributeWarning;
                msg = msg.FormatCurrent(XmlSizeAttr, this._size, "byte");
                result.AddWarning(msg, this.Location);
            }

            byte size = sizeTry.Value;
            if (size != 8 && size != 16 && size != 32)
            {
                string msg = StringTable.IncorrectEnumSizeWarning;
                msg = msg.FormatCurrent(size.ToStringInvariant());
                result.AddWarning(msg, this.Location);
            }

            if (this.Source == null)
            {
                string msg = StringTable.UnresolvedEnumerationError;
                msg = msg.FormatCurrent(this._sourceType, "Enum");
                result.AddError(msg, this.Location);
            }
            else
            {
                if (this._initialValue != null && this.InitialValue == null)
                {
                    string msg = StringTable.UnresolvedEnumConstantWarning;
                    msg = msg.FormatCurrent(this._initialValue, this._sourceType);
                    result.AddWarning(msg, this.Location);
                }
            }

            return result;
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            this._initialValue = reader.GetAttribute(XmlInitialAttr);
            this._size = reader.GetAttribute(XmlSizeAttr);
            this._sourceType = reader.GetAttribute(XmlTypeAttr);
            reader.Skip();
        }

        /// <summary>
        /// Looks through the specified scope and returns the enumeration that this enum
        /// member uses as a source.
        /// </summary>
        /// <param name="scope">
        /// A definition dictionary containing all the other types in the search scope.
        /// </param>
        /// <returns>
        /// This enum members enumeration source.
        /// </returns>
        private IEnumeration GetEnumerationSource(IDefinitionDictionary scope)
        {
            if (this.SourceType == null || scope == null)
            {
                return null;
            }

            return scope.GetEnumeration(this._sourceType);
        }
        #endregion Methods
    } // RSG.Metadata.Model.Definitions.Members.EnumMember {Class}
} // RSG.Metadata.Model.Definitions.Members {Namespace}
