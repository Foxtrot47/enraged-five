﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;

namespace RSG.Model.Dialogue.Export
{
    /// <summary>
    /// Extracts the conversation keys from the .dstar files.
    /// </summary>
    public class KeyExtractor
    {
        #region Private member fields

        private string m_keyFile;
        private string m_dstarFolder;

        #endregion

        #region Public events

        /// <summary>
        /// Key found event.
        /// </summary>
        public event KeyExtractorEventHandler KeyFound;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="keyFile">Key file.</param>
        /// <param name="dstarFolder">Folder containing .dstar files.</param>
        public KeyExtractor(string keyFile, string dstarFolder)
        {
            m_keyFile = keyFile;
            m_dstarFolder = dstarFolder;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Runs the extraction.
        /// </summary>
        public void Run()
        {
            string[] files = Directory.GetFiles(m_dstarFolder, "*.dstar");

            if (File.Exists(m_keyFile))
            {
                File.Delete(m_keyFile);
            }

            using (Stream s = File.Open(m_keyFile, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Read))
            {
                using (TextWriter writer = new StreamWriter(s))
                {
                    foreach (string file in files)
                    {
                        ParseFile(file, writer);
                    }
                }
            }
        }

        #endregion

        #region Private helper methods

        /// <summary>
        /// Parse a file and extract the conversation root keys.
        /// </summary>
        /// <param name="file">File.</param>
        /// <param name="writer">Text writer.</param>
        private void ParseFile(string file, TextWriter writer)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(file);

            XmlNodeList nodes = doc.SelectNodes("//Conversation");
            foreach (XmlNode node in nodes)
            {
                string root = node.Attributes["root"] == null ? String.Empty : node.Attributes["root"].Value;
                if (root != null)
                {
                    writer.WriteLine(root);

                    if (KeyFound != null)
                    {
                        KeyFound(this, new KeyExtractorEventArgs(root));
                    }      
                }
            }
        }

        #endregion
    }
}
