﻿using System;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.Base.ConfigParser;
using RSG.SceneXml;
using RSG.Base.Math;
using RSG.SceneXml.Material;
using RSG.SceneXml.Statistics;
using RSG.Model.Map.Statistics;

namespace RSG.Model.Map
{
    /// <summary>
    /// Defines a room within a milo.
    /// </summary>
    public class MiloRoom : FileAssetContainerBase, IMapObject, IMapContainer
    {
        #region Members

        private ILevel m_level;
        private IMapContainer m_container;
        private BoundingBox3f m_boundingBox;
        private Dictionary<int, Object> m_attributes;
        private int m_exportGeometrySize = 0;
        private uint m_exportTxdSize = 0;
        private int m_polygonCount = 0;
        private int m_collisionPolygonCount = 0;
        private int m_moverPolygonCount = 0;
        private int m_weaponsPolygonCount = 0;
        private int m_cameraPolygonCount = 0;
        private int m_riverPolygonCount = 0;
        private Dictionary<String, uint> m_textureDictionaries;
        private List<IShader> m_shaders = new List<IShader>();

        #endregion // Members

        #region Properties

        /// <summary>
        /// Reference to the owner Level object.
        /// </summary>
        public ILevel Level
        {
            get { return m_level; }
            set
            {
                if (m_level == value)
                    return;

                SetPropertyValue(value, () => Level,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_level = (ILevel)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// The immediate parent map container, or
        /// null if non exist
        /// </summary>
        public IMapContainer Container
        {
            get { return m_container; }
            set
            {
                if (m_container == value)
                    return;

                SetPropertyValue(value, () => Container,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_container = (IMapContainer)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// The tightest box surrounding the map
        /// object, used for density statistics
        /// </summary>
        public BoundingBox3f BoundingBox
        {
            get { return m_boundingBox; }
            set
            {
                if (m_boundingBox == value)
                    return;

                SetPropertyValue(value, () => BoundingBox,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_boundingBox = (BoundingBox3f)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// The dictionary of attributes on this definition
        /// </summary>
        private Dictionary<int, Object> Attributes
        {
            get { return m_attributes; }
            set
            {
                if (m_attributes == value)
                    return;

                SetPropertyValue(value, () => Attributes,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_attributes = (Dictionary<int, Object>)newValue;
                        }
                ));
            }
        }
        
        /// <summary>
        /// The size in bytes of the definitions idr.zip file
        /// that is produce during the map export
        /// </summary>
        public int ExportGeometrySize
        {
            get { return m_exportGeometrySize; }
            set
            {
                if (m_exportGeometrySize == value)
                    return;

                SetPropertyValue(value, () => ExportGeometrySize,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_exportGeometrySize = (int)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// The size in bytes of the definitions txd itd.zip file
        /// that is produce during the map export
        /// </summary>
        public uint ExportTxdSize
        {
            get { return m_exportTxdSize; }
            set
            {
                if (m_exportTxdSize == value)
                    return;

                SetPropertyValue(value, () => ExportTxdSize,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_exportTxdSize = (uint)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// The number of polygons this definition is
        /// made up of
        /// </summary>
        public int PolygonCount
        {
            get { return m_polygonCount; }
            set
            {
                if (m_polygonCount == value)
                    return;

                SetPropertyValue(value, () => PolygonCount,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_polygonCount = (int)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// The number of polygons this definitions collision
        /// is made up of.
        /// </summary>
        public int CollisionPolygonCount
        {
            get { return m_collisionPolygonCount; }
            set
            {
                if (m_collisionPolygonCount == value)
                    return;

                SetPropertyValue(value, () => CollisionPolygonCount,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_collisionPolygonCount = (int)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// The number of polygons this definitions collision
        /// is made up of that is set as mover collision.
        /// </summary>
        public int MoverPolygonCount
        {
            get { return m_moverPolygonCount; }
            set
            {
                if (m_moverPolygonCount == value)
                    return;

                SetPropertyValue(value, () => MoverPolygonCount,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_moverPolygonCount = (int)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// The number of polygons this definitions collision
        /// is made up of that is set as mover collision.
        /// </summary>
        public int WeaponsPolygonCount
        {
            get { return m_weaponsPolygonCount; }
            set
            {
                if (m_weaponsPolygonCount == value)
                    return;

                SetPropertyValue(value, () => WeaponsPolygonCount,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_weaponsPolygonCount = (int)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// The number of polygons this definitions collision
        /// is made up of that is set as camera collision.
        /// </summary>
        public int CameraPolygonCount
        {
            get { return m_cameraPolygonCount; }
            set
            {
                if (m_cameraPolygonCount == value)
                    return;

                SetPropertyValue(value, () => CameraPolygonCount,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_cameraPolygonCount = (int)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// The number of polygons this definitions collision
        /// is made up of that is set as river collision.
        /// </summary>
        public int RiverPolygonCount
        {
            get { return m_riverPolygonCount; }
            set
            {
                if (m_riverPolygonCount == value)
                    return;

                SetPropertyValue(value, () => RiverPolygonCount,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_riverPolygonCount = (int)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// The number of polygons this definitions collision
        /// is made up of.
        /// </summary>
        public Dictionary<String, uint> TextureDictionaries
        {
            get { return m_textureDictionaries; }
            set
            {
                if (m_textureDictionaries == value)
                    return;

                SetPropertyValue(value, () => TextureDictionaries,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_textureDictionaries = (Dictionary<String, uint>)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public List<IShader> Shaders
        {
            get { return m_shaders; }
            set
            {
                if (m_shaders == value)
                    return;

                SetPropertyValue(value, () => Shaders,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_shaders = (List<IShader>)newValue;
                        }
                ));
            }
        }

        #endregion // Properties
        
        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public MiloRoom(ObjectDef sceneObject, IMapContainer container, Scene scene)
        {
            this.Container = container;
            this.Level = container.Level;
            this.m_textureDictionaries = new Dictionary<String, uint>();
            this.m_shaders = new List<IShader>();
            GetNameFromObject(sceneObject, this);
            GetAttributesFromObject(sceneObject, this);

            Debug.Assert(sceneObject.IsMloRoom() == true, "Scene object needs to be a milo room to create a mlo room definition");
            if (sceneObject.IsMloRoom() == false)
                return;

            List<ObjectDef> definitions = new List<ObjectDef>();
            List<ObjectDef> instances = new List<ObjectDef>();
            List<ObjectDef> fragments = new List<ObjectDef>();
            Dictionary<String, List<ObjectDef>> animationGroups = new Dictionary<String, List<ObjectDef>>();
            foreach (ObjectDef child in sceneObject.Children)
            {
                if (!IsSceneObjectValidForRoom(child))
                    continue;

                CategoriseSceneObject(child, ref definitions, ref instances, ref fragments, ref animationGroups);
            }

            foreach (ObjectDef definition in definitions)
            {
                this.AssetChildren.Add(MapDefinition.CreateDefinitionForObject(definition, this, scene));
            }
            foreach (ObjectDef fragment in fragments)
            {
                this.AssetChildren.Add(MapDefinition.CreateDefinitionForFragment(fragment, this, scene));
            }
            foreach (List<ObjectDef> animationGroup in animationGroups.Values)
            {
                this.AssetChildren.Add(MapDefinition.CreateDefinitionForAnimObject(animationGroup, this, scene));
            }

            foreach (IAsset asset in this.AssetChildren)
            {
                if (asset is MapDefinition)
                {
                    int assetCode = asset.Name.ToLower().GetHashCode();
                    if (!this.Level.GenericDefinitions.ContainsKey(assetCode))
                    {
                        this.Level.GenericDefinitions.Add(assetCode, asset);
                    }
                }
            }

            foreach (ObjectDef instance in instances)
            {
                RSG.Model.Map.Util.Creation.ProcessMapInstance(instance, this, scene);
            }
            foreach (IAsset asset in this.AssetChildren)
            {
                if (asset is MapInstance)
                {
                    if ((asset as MapInstance).BoundingBox != null)
                    {
                        if (this.m_boundingBox != null)
                            this.m_boundingBox.Expand((asset as MapInstance).BoundingBox);
                        else
                            this.m_boundingBox = new BoundingBox3f((asset as MapInstance).BoundingBox);
                    }
                }
            }
            if (this.m_boundingBox != null)
            {
                float halfWidth = (this.m_boundingBox.Max.X - this.m_boundingBox.Min.X) * 0.5f;
                float halfDepth = (this.m_boundingBox.Max.Y - this.m_boundingBox.Min.Y) * 0.5f;
                float halfHeight = (this.m_boundingBox.Max.Z - this.m_boundingBox.Min.Z) * 0.5f;
                this.m_boundingBox = new BoundingBox3f(new Vector3f(-halfWidth, -halfDepth, -halfHeight), new Vector3f(halfWidth, halfDepth, halfHeight));
            }

            this.GetStatisticsForRoom();
        }

        #endregion // Constructor

        #region Object Overrides

        /// <summary>
        /// Make sure the name is returned on ToString
        /// </summary>
        public override String ToString()
        {
            return this.Name;
        }

        /// <summary>
        /// Gets the hash code for this level
        /// using the name property
        /// </summary>
        public override int GetHashCode()
        {
            return this.Name.GetHashCode();
        }

        #endregion // Object Overrides

        #region Private Functions

        /// <summary>
        /// Gets the name from the given object
        /// </summary>
        private static void GetNameFromObject(ObjectDef sceneObject, MiloRoom room)
        {
            room.m_name = String.Copy(sceneObject.Name);
        }

        /// <summary>
        /// Gets the attributes from the given object
        /// </summary>
        private static void GetAttributesFromObject(ObjectDef sceneObject, MiloRoom room)
        {
            room.m_attributes = new Dictionary<int, Object>();
            foreach (var attribute in sceneObject.Attributes)
            {
                room.m_attributes.Add(attribute.Key.ToLower().GetHashCode(), attribute.Value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private static Boolean IsSceneObjectValidForRoom(ObjectDef sceneObject)
        {
            if (sceneObject.DontExport())
                return false;
            if (sceneObject.DontExportIDE())
                return false;
            if (sceneObject.DontExportIPL())
                return false;
            if (sceneObject.IsDummy())
                return false;

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        private static void CategoriseSceneObject(ObjectDef sceneObject,
           ref List<ObjectDef> definitions, ref List<ObjectDef> instances, ref List<ObjectDef> fragments,
           ref Dictionary<String, List<ObjectDef>> animationGroups)
        {
            if (sceneObject.IsFragment()) // A fragment
            {
                if (sceneObject.DrawableLOD != null && (sceneObject.DrawableLOD.Children != null && sceneObject.DrawableLOD.Children.Count() != 0))
                    return;

                fragments.Add(sceneObject);
            }
            else if (sceneObject.IsStatedAnim()) // A animation state
            {
                if (sceneObject.Attributes.ContainsKey(AttrNames.OBJ_GROUP_NAME))
                {
                    String animGroup = sceneObject.Attributes[AttrNames.OBJ_GROUP_NAME] as String;
                    if (animGroup != null)
                    {
                        if (!animationGroups.ContainsKey(animGroup.ToLower()))
                            animationGroups.Add(animGroup.ToLower(), new List<ObjectDef>());

                        animationGroups[animGroup.ToLower()].Add(sceneObject);
                    }
                }
            }
            else if (sceneObject.IsObject()) // A definition or instance
            {
                if (sceneObject.IsXRef() || sceneObject.IsRefObject() || sceneObject.IsInternalRef() || sceneObject.IsRefInternalObject()) // A instance
                {
                    if (sceneObject.LOD != null && sceneObject.LOD.Parent != null)
                        return;

                    instances.Add(sceneObject);
                }
                else // A definition
                {
                    if (sceneObject.DrawableLOD != null && sceneObject.DrawableLOD.Parent != null)
                        return;

                    definitions.Add(sceneObject);
                    instances.Add(sceneObject);
                }
            }
        }

        /// <summary>
        /// Creates the statistics for this specific milo room
        /// </summary>
        private void GetStatisticsForRoom()
        {
            List<String> definitions = new List<String>();
            List<IAsset> polycountZero = new List<IAsset>();
            foreach (IAsset child in this.AssetChildren)
            {
                if (child is MapInstance && (child as MapInstance).ReferencedDefinition != null)
                {
                    MapDefinition definition = (child as MapInstance).ReferencedDefinition;
                    MapDefinition stats = definition;
                    if (stats != null)
                    {
                        this.m_polygonCount += stats.PolygonCount;
                        if (stats.PolygonCount == 0)
                        {
                            polycountZero.Add(child);
                        }
                        this.m_collisionPolygonCount += stats.CollisionPolygonCount;
                        this.m_moverPolygonCount += stats.MoverPolygonCount;
                        this.m_weaponsPolygonCount += stats.WeaponsPolygonCount;
                        this.m_cameraPolygonCount += stats.CameraPolygonCount;
                        this.m_riverPolygonCount += stats.RiverPolygonCount;
                        if (!definitions.Contains(definition.Name))
                        {
                            definitions.Add(definition.Name);
                            this.m_exportGeometrySize += stats.ExportGeometrySize;
                        }
                        if (!m_textureDictionaries.ContainsKey(definition.TextureDictionaryName))
                        {
                            m_textureDictionaries.Add(definition.TextureDictionaryName, stats.ExportTxdSize);
                            this.m_exportTxdSize += stats.ExportTxdSize;
                        }
                        foreach (IShader shader in stats.Shaders)
                        {
                            if (!m_shaders.Contains(shader))
                                this.m_shaders.Add(shader);
                        }
                    }
                }
            }

            if (polycountZero.Count != 0)
            {
                String message = String.Format("The interior {0} has the following objects in it that appear to be made up of zero polygons: ", this.Container.Name);
                foreach (IAsset obj in polycountZero)
                {
                    message += obj.Name + " ";
                }
                message += ". Re-exporting this container will most likely fix this issue";
                RSG.Base.Logging.Log.Log__Error(message);
            }
        }

        #endregion // Private Functions

        #region IEnumerable<IMapComponent>

        /// <summary>
        /// 
        /// </summary>
        IEnumerator<IMapComponent> IEnumerable<IMapComponent>.GetEnumerator()
        {
            foreach (IMapComponent component in this.AssetChildren.Where(c => c is IMapComponent == true))
            {
                yield return component;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerator)this);
        }

        #endregion // IEnumerable<IMapContainer>
    } // MiloRoom
} // RSG.Model.Map
