﻿namespace RSG.Model.Report.Reports.Map
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml;
    using System.Xml.Linq;
    using System.Xml.XPath;
    using RSG.Base.Tasks;
    using RSG.Base.Configuration;
    using RSG.Model.Common;
    using RSG.Model.Common.Map;
    using RSG.SourceControl.Perforce;
    using P4API;

    /// <summary>
    /// This is a report that parses all SceneXML files for a list of all missing textures.
    /// </summary>
    public class MissingTextureReport : CSVReport, IDynamicReport, IReportFileProvider
    {
        #region Constants
        private const String c_name = "Missing Texture Report";
        private const String c_description = "Exports a .csv file include all textures missing locally.";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (m_task == null)
                {
                    CompositeTask task = new CompositeTask("Generate Report");
                    task.AddSubTask(new ActionTask("Syncing data", (context, progress) => SynchronizeData((DynamicReportContext)context, progress)));
                    task.AddSubTask(new ActionTask("Loading data", (context, progress) => EnsureDataLoaded((DynamicReportContext)context, progress)));
                    task.AddSubTask(new ActionTask("Generating report", (context, progress) => GenerateReport((DynamicReportContext)context, progress)));
                    m_task = task;
                }
                return m_task;
            }
        }
        private ITask m_task;

        #endregion // Properties

        #region Member Data
        /// <summary>
        /// The perforce connection object to use for p4 queries
        /// </summary>
        private P4 PerforceConnection
        {
            get
            {
                if (m_perforceConnection == null)
                {
                    m_perforceConnection = new P4();
                    m_perforceConnection.Connect();
                }
                return m_perforceConnection;
            }
            set
            {
                m_perforceConnection = value;
            }
        }
        private P4 m_perforceConnection;

        /// <summary>
        /// Caches all relevant data to write out the report.
        /// </summary>
        private Dictionary<String, List<String>> MissingTextures;

        /// <summary>
        /// Project configuration.
        /// </summary>
        private IConfig Config;
        #endregion


        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public MissingTextureReport()
            : base(c_name, c_description)
        {
            Config = ConfigFactory.CreateConfig();
            MissingTextures = new Dictionary<String, List<String>>();
        }
        #endregion // Constructor(s)

        #region Task Methods

        /// <summary>
        /// Syncs all data necessary for generating this report.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="progress"></param>
        private void SynchronizeData(DynamicReportContext context, IProgress<TaskProgress> progress)
        {
            List<String> syncArguments = new List<String>();
            syncArguments.Add(Path.Combine(Config.Project.DefaultBranch.Art, "textures", "..."));
            syncArguments.Add(Path.Combine(Config.Project.DefaultBranch.Export, "....xml"));

            string message = String.Format("Syncing textures and assets...");
            progress.Report(new TaskProgress(0.0f, true, message));

            PerforceConnection.Run("sync", syncArguments.ToArray());
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void EnsureDataLoaded(DynamicReportContext context, IProgress<TaskProgress> progress)
        {
            MissingTextures.Clear();

            string[] sceneXmls = Directory.GetFiles(Config.Project.DefaultBranch.Export, "*.xml", SearchOption.AllDirectories);

            foreach (string sceneXml in sceneXmls)
            {
                MissingTextures.Add(sceneXml, new List<String>());

                context.Token.ThrowIfCancellationRequested();

                float increment = 1.0f / sceneXmls.Length;
                string message = String.Format("Loading {0}", Path.GetFileName(sceneXml));
                progress.Report(new TaskProgress(increment, true, message));

                try
                {
                    XDocument document = XDocument.Load(sceneXml);

                    XPathNavigator navigator = document.CreateNavigator();
                    XPathNodeIterator iterator = navigator.Select("//material/textures/texture");
                    while (iterator.MoveNext())
                    {
                        string filename = iterator.Current.GetAttribute("filename", "");

                        if (File.Exists(filename) == false)
                        {
                            if (MissingTextures[sceneXml].Contains(filename) == false)
                                MissingTextures[sceneXml].Add(filename);
                        }
                    }
                }
                catch (System.Xml.XmlException ex) { }  //Some Xml files can be malformed.
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void GenerateReport(DynamicReportContext context, IProgress<TaskProgress> progress)
        {
            using (StreamWriter writer = new StreamWriter(Filename))
            {
                string header = "Section Name,Missing Texture";
                writer.WriteLine(header);

                foreach (KeyValuePair<String, List<String>> keyValuePair in MissingTextures)
                {
                    if (keyValuePair.Value.Count == 0) continue;

                    writer.WriteLine(Path.GetFileNameWithoutExtension(keyValuePair.Key) + ",");

                    foreach (String missingTexture in keyValuePair.Value)
                    {
                        writer.WriteLine("," + missingTexture);
                    }
                }
            }
        }
        #endregion // Task Methods

    } // TextureComparisonReport
} // RSG.Model.Report.Reports.Map

