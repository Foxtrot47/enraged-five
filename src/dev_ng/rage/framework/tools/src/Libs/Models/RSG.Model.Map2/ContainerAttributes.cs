﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace RSG.Model.Map
{
    /// <summary>
    /// 
    /// </summary>
    public class ContainerAttributes
    {
        #region Classes
        /// <summary>
        /// 
        /// </summary>
        public class ContainerAttribute
        {
            #region Properties
            /// <summary>
            /// The type of this attribute
            /// </summary>
            public string Type
            {
                get;
                set;
            }

            /// <summary>
            /// The value of this attribute
            /// </summary>
            public string Value
            {
                get;
                set;
            }
            #endregion

            #region Constructor
            /// <summary>
            /// 
            /// </summary>
            /// <param name="type"></param>
            /// <param name="value"></param>
            internal ContainerAttribute(string type, string value)
            {
                this.Type = type;
                this.Value = value;
            }
            #endregion
        }
        #endregion

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public MapSection Section
        {
            get;
            set;
        }

        /// <summary>
        /// The username of the person that last exported
        /// it.
        /// </summary>
        public String ExportUser
        {
            get;
            set;
        }

        /// <summary>
        /// The time that this section was
        /// last exported.
        /// </summary>
        public DateTime ExportTime
        {
            get;
            set;
        }

        /// <summary>
        /// A dictionary of all attributes attached to this container.
        /// </summary>
        public Dictionary<string, ContainerAttribute> Attributes
        {
            get;
            set;
        }

        public bool this[string name]
        {
            get
            {
                if (!Attributes.ContainsKey(name))
                    return false;

                string value = Attributes[name].Value;
                bool result;
                bool.TryParse(value, out result);
                return result;
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        public ContainerAttributes(string filename, MapSection section)
        {
            this.Attributes = new Dictionary<string, ContainerAttribute>();
            this.Section = section;
            using (XmlTextReader reader = new XmlTextReader(filename))
            {
                while (reader.MoveToContent() != XmlNodeType.None)
                {
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        if (reader.Name == "attributes")
                        {
                            if (reader.GetAttribute(0) == "RS Container")
                            {
                                bool empty = reader.IsEmptyElement;
                                reader.ReadStartElement();
                                if (!empty)
                                {
                                    while (reader.MoveToContent() == XmlNodeType.Element)
                                    {
                                        if (reader.Name == "attribute")
                                        {
                                            string name = reader.GetAttribute("name");
                                            string type = reader.GetAttribute("type");
                                            string value = reader.GetAttribute("value");
                                            if (!this.Attributes.ContainsKey(name))
                                                this.Attributes.Add(name, new ContainerAttribute(type, value));

                                            reader.ReadStartElement();
                                        }
                                    }
                                }
                            }

                            break;
                        }
                        else if (reader.Name == "scene")
                        {
                            this.ExportUser = reader.GetAttribute("user");
                            DateTime dateTime = new DateTime();
                            string time = reader.GetAttribute("timestamp");
                            if (!string.IsNullOrWhiteSpace(time) && DateTime.TryParse(time, out dateTime))
                            {
                                this.ExportTime = dateTime;
                            }
                            reader.ReadStartElement();
                        }
                        else
                        {
                            reader.ReadStartElement();
                        }
                    }
                    else
                    {
                        if (reader.NodeType == XmlNodeType.Text)
                            reader.ReadString();
                        if (reader.NodeType == XmlNodeType.EndElement)
                            reader.ReadEndElement();
                    }
                }
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Serialises this class into the given xml root node
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="attributesRoot"></param>
        internal void Serialise(XmlDocument doc, XmlElement attributesRoot)
        {
            XmlElement xmlUser = doc.CreateElement("user");
            xmlUser.SetAttribute("value", this.ExportUser.ToString());
            attributesRoot.AppendChild(xmlUser);

            XmlElement xmlTimestamp = doc.CreateElement("timestamp");
            xmlUser.SetAttribute("value", this.ExportTime.ToString());
            attributesRoot.AppendChild(xmlTimestamp);

            foreach (var attribute in this.Attributes)
            {
                XmlElement xmlAttribute = doc.CreateElement(attribute.Key);
                xmlUser.SetAttribute("value", attribute.Value.Value);
                attributesRoot.AppendChild(xmlAttribute);
            }
        }
        #endregion
    } // ContainerAttributes
} // RSG.Model.Map
