﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Linq;
using RSG.Base.Logging;
using RSG.Platform;

namespace RSG.Model.Statistics.Platform
{
    /// <summary>
    /// Resource bucket statistic.
    /// - this is created from ragebuilder output
    /// - serialises out upon resource conversions ( asset builder or otherwise )
    /// </summary>
    [DataContract]
    public class ResourceBucketStat
    {
        #region Properties
        /// <summary>
        /// Resource bucket platform.
        /// </summary>
        [DataMember]
        public RSG.Platform.Platform Platform 
        { 
            get; 
            set; 
        }

        /// <summary>
        /// Resource bucket filetype.
        /// </summary>
        [DataMember]
        public RSG.Platform.FileType FileType 
        { 
            get; 
            set; 
        }

        /// <summary>
        /// Resource name.
        /// </summary>
        [DataMember]
        public String ResourceName 
        { 
            get; 
            set; 
        }

        /// <summary>
        /// Resource source filename (for Ragebuilder).
        /// </summary>
        [DataMember]
        public String SourceFilename
        {
            get;
            set;
        }

        /// <summary>
        /// Resource destination filename (for Ragebuilder).
        /// </summary>
        [DataMember]
        public String DestinationFilename
        {
            get;
            set;
        }

        /// <summary>
        /// Bucket identifier.
        /// </summary>
        [DataMember]
        public UInt32 ID  
        { 
            get;  
            set; 
        }

        /// <summary>
        /// Bucket type.
        /// </summary>
        [DataMember]
        public ResourceBucketType Type 
        { 
            get; 
            set; 
        }
        
        /// <summary>
        /// Bucket size in bytes.
        /// </summary>
        [DataMember]
        public UInt32 Size  
        { 
            get; 
            set; 
        }

        /// <summary>
        /// Bucket capacity in bytes.
        /// </summary>
        [DataMember]
        public UInt32 Capacity  
        { 
            get; 
            set; 
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="platform"></param>
        /// <param name="filetype"></param>
        /// <param name="basename"></param>
        /// <param name="id"></param>
        /// <param name="type"></param>
        /// <param name="size"></param>
        /// <param name="capacity"></param>
        public ResourceBucketStat(RSG.Platform.Platform platform, FileType filetype, 
            String basename, String sourceFilename, String destinationFilename, 
            UInt32 id, ResourceBucketType type, UInt32 size, UInt32 capacity)
        {
            this.Platform = platform;
            this.FileType = filetype;
            this.ResourceName = basename;
            this.SourceFilename = sourceFilename;
            this.DestinationFilename = destinationFilename;
            this.ID = id;
            this.Type = type;
            this.Size = size;
            this.Capacity = capacity;
        }

        /// <summary>
        /// Deserialise constructor
        /// </summary>
        /// <param name="xmlElem"></param>
        public ResourceBucketStat(XElement xmlElem)
        {
            if (String.Compare(xmlElem.Name.LocalName, "Bucket", true) != 0)
            {
                throw new ArgumentException("ResourceBucketStat can't contruct form element of name {0}", xmlElem.Name.LocalName);
            }

            XElement xmlPlatformElem = xmlElem.Element("Platform");
            if (null != xmlPlatformElem)
                this.Platform = (RSG.Platform.Platform)Enum.Parse(typeof(RSG.Platform.Platform),xmlPlatformElem.Value, true);

            XElement xmlFileTypeElem = xmlElem.Element("FileType");
            if (null != xmlFileTypeElem)
                this.FileType = (FileType)Enum.Parse(typeof(FileType), xmlFileTypeElem.Value, true);

            XElement xmlResourceNameElem = xmlElem.Element("ResourceName");
            if (null != xmlResourceNameElem)
                this.ResourceName = xmlResourceNameElem.Value;

            XElement xmlSourceFilenameElem = xmlElem.Element("SourceFilename");
            if (null != xmlSourceFilenameElem)
                this.SourceFilename = xmlSourceFilenameElem.Value;

            XElement xmlDestinationFilenameElem = xmlElem.Element("DestinationFilename");
            if (null != xmlDestinationFilenameElem)
                this.DestinationFilename = xmlDestinationFilenameElem.Value;

            XElement xmlIdElem = xmlElem.Element("ID");
            if (null != xmlIdElem)
                this.ID = uint.Parse(xmlIdElem.Value);

            XElement xmlTypeElem = xmlElem.Element("Type");
            if (null != xmlTypeElem)
                this.Type = (ResourceBucketType)Enum.Parse(typeof(ResourceBucketType), xmlTypeElem.Value, true); ;

            XElement xmlSizeElem = xmlElem.Element("Size");
            if (null != xmlSizeElem)
                this.Size = uint.Parse(xmlSizeElem.Value);

            XElement xmlCapacityElem = xmlElem.Element("Capacity");
            if (null != xmlCapacityElem)
                this.Capacity = uint.Parse(xmlCapacityElem.Value);
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Return true if the XML element submitted is valid for XML parameterised constructor.
        /// </summary>
        /// <param name="xmlElem"></param>
        /// <returns></returns>
        public static bool CanBeConstructed(XElement xmlElem)
        {
            if (xmlElem.Element("Platform") != null && 
                xmlElem.Element("FileType") != null &&
                xmlElem.Element("ResourceName") != null &&
                xmlElem.Element("SourceFilename") != null &&
                xmlElem.Element("DestinationFilename") != null &&
                xmlElem.Element("Type") != null &&
                xmlElem.Element("ID") != null &&
                xmlElem.Element("Capacity") != null &&
                xmlElem.Element("Size") != null)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Serialise resource bucket stat to XML element.
        /// </summary>
        /// <returns></returns>
        public XElement Serialise()
        {
            XElement xmlBucket = new XElement("Bucket",
                new XElement("Platform", this.Platform),
                new XElement("FileType", this.FileType),
                new XElement("ResourceName", this.ResourceName),
                new XElement("SourceFilename", this.SourceFilename),
                new XElement("DestinationFilename", this.DestinationFilename),
                new XElement("ID", this.ID),
                new XElement("Type", this.Type),
                new XElement("Size", this.Size),
                new XElement("Capacity", this.Capacity));
            return (xmlBucket);
        }
        #endregion // Controller Methods
    }

} // RSG.Model.Statistics.Platform namespace
