﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Logging;
using RSG.Model.Common;
using RSG.Statistics.Client.ServiceClients;
using RSG.Statistics.Common.Dto.GameAssets;
using RSG.Statistics.Common.Dto.GameAssets.Bundles;

namespace RSG.Model.Vehicle
{
    /// <summary>
    /// Collection of vehicles that come from a single build
    /// </summary>
    public class SingleBuildVehicleCollection : VehicleCollectionBase
    {
        #region Properties
        /// <summary>
        /// The build this vehicle collection is for.
        /// </summary>
        public string BuildIdentifier { get; protected set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public SingleBuildVehicleCollection(string buildIdentifier)
            : base()
        {
            BuildIdentifier = buildIdentifier;
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="statsToLoad"></param>
        public override void LoadStats(IEnumerable<StreamableStat> statsToLoad)
        {
            try
            {
                using (BuildClient client = new BuildClient())
                {
                    foreach (IVehicle vehicle in Vehicles)
                    {
                        // Check whether the stats are already loaded
                        if (!vehicle.AreStatsLoaded(statsToLoad))
                        {
                            VehicleStatBundleDto bundle = client.GetVehicleStat(BuildIdentifier, vehicle.Hash.ToString());
                            (vehicle as DbVehicle).LoadStatsFromDatabase(bundle);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Log.Log__Exception(e, "Unhandled exception while retrieving stats from the database.");
            }
        }
        #endregion // Overrides
    } // SingleBuildVehicleCollection
}
