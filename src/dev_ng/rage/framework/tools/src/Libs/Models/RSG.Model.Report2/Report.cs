﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using RSG.Model.Common;
using RSG.Base.Configuration.Reports;

namespace RSG.Model.Report
{
    /// <summary>
    /// ReportUri - A report that has a Uri - file or URL to display.
    /// </summary>
    public abstract class Report : AssetBase, IReport
    {
        #region Properties
        /// <summary>
        /// Report description string.
        /// </summary>
        public String Description
        {
            get { return m_sDescription; }
            set
            {
                SetPropertyValue(value, () => this.Description,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_sDescription = (String)newValue;
                        }
                ));
            }
        }
        private String m_sDescription;

        /// <summary>
        /// Which data source modes this report can be used in.
        /// </summary>
        public DataSourceCollection DataSourceModes
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        public Report()
        {
            this.DataSourceModes = new DataSourceCollection();
            this.DataSourceModes[DataSource.SceneXml] = true;
            this.DataSourceModes[DataSource.Database] = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="desc"></param>
        public Report(string name, string desc)
            : base(name)
        {
            Description = desc;

            this.DataSourceModes = new DataSourceCollection();
            this.DataSourceModes[DataSource.SceneXml] = true;
            this.DataSourceModes[DataSource.Database] = false;
        }
        #endregion // Constructor(s)
    } // Report
} // RSG.Model.Report
