﻿// --------------------------------------------------------------------------------------------
// <copyright file="IDefinitionDictionary.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using RSG.Base;
    using RSG.Base.Logging;
    using RSG.Editor.Model;

    /// <summary>
    /// When implemented represents a dictionary of parCodeGen definitions that can be used
    /// to open and manipulate metadata files.
    /// </summary>
    public interface IDefinitionDictionary
    {
        #region Properties
        /// <summary>
        /// Gets the read-only collection of loaded constants for this dictionary.
        /// </summary>
        ReadOnlyModelCollection<IConstant> Constants { get; }

        /// <summary>
        /// Gets the collection of loaded enumerations for this dictionary.
        /// </summary>
        ReadOnlyModelCollection<IEnumeration> Enumerations { get; }

        /// <summary>
        /// Gets the collection of loaded structures for this dictionary.
        /// </summary>
        ReadOnlyModelCollection<IStructure> Structures { get; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Removes all structures and enumerations from this dictionary.
        /// </summary>
        void Clear();

        /// <summary>
        /// Determines whether this dictionary contains a constant definition with the
        /// specified name.
        /// </summary>
        /// <param name="name">
        /// The name of the constant to locate in the dictionary.
        /// </param>
        /// <returns>
        /// True if this dictionary contains a constant with the specified name; otherwise,
        /// false.
        /// </returns>
        bool ContainsConstant(string name);

        /// <summary>
        /// Determines whether this dictionary contains a enumeration definition with the
        /// specified data type.
        /// </summary>
        /// <param name="type">
        /// The data type of the enumeration to locate in the dictionary.
        /// </param>
        /// <returns>
        /// True if this dictionary contains an enumeration with the specified data type;
        /// otherwise, false.
        /// </returns>
        bool ContainsEnumeration(string type);

        /// <summary>
        /// Determines whether this dictionary contains a structure definition with the
        /// specified data type.
        /// </summary>
        /// <param name="name">
        /// The data type of the structure to locate in the dictionary.
        /// </param>
        /// <returns>
        /// True if this dictionary contains an structure with the specified data type;
        /// otherwise, false.
        /// </returns>
        bool ContainsStructure(string name);

        /// <summary>
        /// Create a new constant and adds it to this dictionary before returning a reference
        /// to it.
        /// </summary>
        /// <param name="name">
        /// The name of the new constant.
        /// </param>
        /// <param name="value">
        /// The value of the new constant.
        /// </param>
        /// <returns>
        /// A reference to the newly created constant.
        /// </returns>
        IConstant CreateAndAddConstant(string name, string value);

        /// <summary>
        /// Create a new enumeration and adds it to this dictionary before returning a
        /// reference to it.
        /// </summary>
        /// <param name="dataType">
        /// The full data type of the new enumeration.
        /// </param>
        /// <returns>
        /// A reference to the newly created enumeration.
        /// </returns>
        IEnumeration CreateAndAddEnumeration(string dataType);

        /// <summary>
        /// Create a new structure and adds it to this dictionary before returning a reference
        /// to it.
        /// </summary>
        /// <param name="dataType">
        /// The full data type of the new structure.
        /// </param>
        /// <returns>
        /// A reference to the newly created structure.
        /// </returns>
        IStructure CreateAndAddStructure(string dataType);

        /// <summary>
        /// Gets the first occurrence of a loaded constant with the specified name if found;
        /// otherwise null.
        /// </summary>
        /// <param name="name">
        /// The name of the constant to locate.
        /// </param>
        /// <returns>
        /// The first occurrence of a loaded constant with the specified name if found;
        /// otherwise null.
        /// </returns>
        IConstant GetConstant(string name);

        /// <summary>
        /// Attempts to get the constant with the specified name and returns a value indicating
        /// whether or not it was successful.
        /// </summary>
        /// <param name="name">
        /// The name of the constant to get.
        /// </param>
        /// <param name="constant">
        /// When this method returns, contains the constant with the specified name, if the
        /// constant is found; otherwise, null.
        /// </param>
        /// <returns>
        /// True if the dictionary contains a constant with the specified name; otherwise,
        /// false.
        /// </returns>
        bool GetConstant(string name, out IConstant constant);

        /// <summary>
        /// Gets all of the constants that have been loaded from the file with the specified
        /// full path.
        /// </summary>
        /// <param name="fullPath">
        /// The full path of the file whose constants should be retrieved.
        /// </param>
        /// <returns>
        /// A read-only list of constants that have been loaded from the file with the
        /// specified full path.
        /// </returns>
        IReadOnlyList<IConstant> GetConstantsFromFile(string fullPath);

        /// <summary>
        /// Gets the first occurrence of a loaded enumeration with the specified data type if
        /// found; otherwise null.
        /// </summary>
        /// <param name="type">
        /// The data type of the enumeration to locate.
        /// </param>
        /// <returns>
        /// The first occurrence of a loaded enumeration with the specified data type if found;
        /// otherwise, null.
        /// </returns>
        IEnumeration GetEnumeration(string type);

        /// <summary>
        /// Attempts to get the enumeration with the specified data type and returns a value
        /// indicating whether or not it was successful.
        /// </summary>
        /// <param name="type">
        /// The data type of the enumeration to get.
        /// </param>
        /// <param name="enumeration">
        /// When this method returns, contains the enumeration with the specified data type, if
        /// the enumeration is found; otherwise, null.
        /// </param>
        /// <returns>
        /// True if the dictionary contains a enumeration with the specified data type;
        /// otherwise, false.
        /// </returns>
        bool GetEnumeration(string type, out IEnumeration enumeration);

        /// <summary>
        /// Gets all of the enumerations that have been loaded from the file with the specified
        /// full path.
        /// </summary>
        /// <param name="fullPath">
        /// The full path of the file whose enumerations should be retrieved.
        /// </param>
        /// <returns>
        /// A read-only list of enumerations that have been loaded from the file with the
        /// specified full path.
        /// </returns>
        IReadOnlyList<IEnumeration> GetEnumerationsFromFile(string fullPath);

        /// <summary>
        /// Gets all of the enumerations that have been loaded that are located in the
        /// specified scope.
        /// </summary>
        /// <param name="scope">
        /// The scope whose enumerations should be retrieved.
        /// </param>
        /// <returns>
        /// A read-only list of enumerations that have been loaded that are located in the
        /// specified scope.
        /// </returns>
        IReadOnlyList<IEnumeration> GetEnumerationsFromScope(string scope);

        /// <summary>
        /// Gets all of the valid namespaces for the loaded definitions.
        /// </summary>
        /// <returns>
        /// A read-only list of namespaces for the loaded definitions.
        /// </returns>
        IReadOnlyList<string> GetNamespaces();

        /// <summary>
        /// Gets the first occurrence of a loaded structure with the specified name if found;
        /// otherwise, null.
        /// </summary>
        /// <param name="name">
        /// The name of the structure to locate.
        /// </param>
        /// <returns>
        /// The first occurrence of a loaded structure with the specified name if found;
        /// otherwise, null.
        /// </returns>
        IStructure GetStructure(string name);

        /// <summary>
        /// Attempts to get the structure with the specified data type and returns a value
        /// indicating whether or not it was successful.
        /// </summary>
        /// <param name="name">
        /// The data type of the structure to get.
        /// </param>
        /// <param name="structure">
        /// When this method returns, contains the structure with the specified data type, if
        /// the structure is found; otherwise, null.
        /// </param>
        /// <returns>
        /// True if the dictionary contains a structure with the specified data type;
        /// otherwise, false.
        /// </returns>
        bool GetStructure(string name, out IStructure structure);

        /// <summary>
        /// Gets all of the structures that have been loaded from the file with the specified
        /// full path.
        /// </summary>
        /// <param name="fullPath">
        /// The full path of the file whose structures should be retrieved.
        /// </param>
        /// <returns>
        /// A read-only list of structures that have been loaded from the file with the
        /// specified full path.
        /// </returns>
        IReadOnlyList<IStructure> GetStructuresFromFile(string fullPath);

        /// <summary>
        /// Gets all of the structures that have been loaded that are located in the specified
        /// scope.
        /// </summary>
        /// <param name="scope">
        /// The scope whose structures should be retrieved.
        /// </param>
        /// <returns>
        /// A read-only list of structures that have been loaded that are located in the
        /// specified scope.
        /// </returns>
        IReadOnlyList<IStructure> GetStructuresFromScope(string scope);

        /// <summary>
        /// Determines whether the specified structure can be instanced inside a metadata file
        /// without any errors being produced.
        /// </summary>
        /// <param name="structure">
        /// The structure to test.
        /// </param>
        /// <param name="log">
        /// The log object that will receives any errors or warning found while validating.
        /// </param>
        /// <returns>
        /// True if the specified structure can be instanced; otherwise false. If false the
        /// specified log object will have received the reasons why not.
        /// </returns>
        bool IsStructureValidForInstancing(IStructure structure, ILog log);

        /// <summary>
        /// Determines whether the specified data type can be instanced inside a metadata file
        /// without any errors being produced.
        /// </summary>
        /// <param name="structureType">
        /// The data type of the structure to test.
        /// </param>
        /// <param name="log">
        /// The log object that will receives any errors or warning found while validating.
        /// </param>
        /// <returns>
        /// True if the specified data type can be instanced; otherwise false. If false the
        /// specified log object will have received the reasons why not.
        /// </returns>
        bool IsStructureValidForInstancing(string structureType, ILog log);

        /// <summary>
        /// Loads the given file and adds its contents to this dictionary.
        /// </summary>
        /// <param name="filename">
        /// The path to the file to load.
        /// </param>
        void LoadFile(string filename);

        /// <summary>
        /// Gets a task that represents the work needing to be done to load the specified
        /// file into this dictionary.
        /// </summary>
        /// <param name="filename">
        /// The path to the file to load.
        /// </param>
        /// <returns>
        /// A task that represents the work needing to be done to load the specified file.
        /// </returns>
        Task LoadFileAsync(string filename);

        /// <summary>
        /// Removes the first occurrence of the specified constant from this dictionary.
        /// </summary>
        /// <param name="constant">
        /// The constant to remove.
        /// </param>
        void RemoveConstant(IConstant constant);

        /// <summary>
        /// Removes the first occurrence of the specified enumeration from this dictionary.
        /// </summary>
        /// <param name="enumeration">
        /// The enumeration to remove.
        /// </param>
        void RemoveEnumeration(IEnumeration enumeration);

        /// <summary>
        /// Removes the first occurrence of the specified structure from this dictionary.
        /// </summary>
        /// <param name="structure">
        /// The structure to remove.
        /// </param>
        void RemoveStructure(IStructure structure);

        /// <summary>
        /// Converts the specified <paramref name="s"/> parameter to an instance of the type
        /// specified by the type parameter.
        /// </summary>
        /// <typeparam name="T">
        /// The type that the specified string object will be converted to.
        /// </typeparam>
        /// <param name="s">
        /// The string instance to convert.
        /// </param>
        /// <param name="fallback">
        /// The value that will be returned if the conversion fails.
        /// </param>
        /// <returns>
        /// The converted value if the conversion is successful; otherwise, the specified
        /// <paramref name="fallback"/> value.
        /// </returns>
        T To<T>(string s, T fallback);

        /// <summary>
        /// Converts the specified <paramref name="s"/> parameter to an array of values of the
        /// specified type.
        /// </summary>
        /// <typeparam name="T">
        /// The type that the specified string object will be converted to.
        /// </typeparam>
        /// <param name="s">
        /// The string instance to convert.
        /// </param>
        /// <param name="count">
        /// The number of values the <paramref name="s"/> parameter should be converted to.
        /// </param>
        /// <param name="fallback">
        /// The value that will be returned if the conversion fails.
        /// </param>
        /// <returns>
        /// The converted value if the conversion is successful; otherwise, the specified
        /// <paramref name="fallback"/> value.
        /// </returns>
        T[] To<T>(string s, int count, T fallback);

        /// <summary>
        /// Attempts to convert the specified <paramref name="s"/> parameter to an instance of
        /// the type specified by the type parameter.
        /// </summary>
        /// <typeparam name="T">
        /// The type that the specified string object will be converted to.
        /// </typeparam>
        /// <param name="s">
        /// The string instance to convert.
        /// </param>
        /// <param name="fallback">
        /// The value that will be returned if the conversion fails.
        /// </param>
        /// <returns>
        /// A structure containing the converted value and a value indicating whether the
        /// conversion was successful.
        /// </returns>
        TryResult<T> TryTo<T>(string s, T fallback);

        /// <summary>
        /// Attempts to convert the specified <paramref name="s"/> parameter to an array of
        /// values of the specified type.
        /// </summary>
        /// <typeparam name="T">
        /// The type that the specified string object will be converted to.
        /// </typeparam>
        /// <param name="s">
        /// The string instance to convert.
        /// </param>
        /// <param name="count">
        /// The number of values the <paramref name="s"/> parameter should be converted to.
        /// </param>
        /// <param name="fallback">
        /// The value that will be returned if the conversion fails.
        /// </param>
        /// <returns>
        /// A structure containing the converted value and a value indicating whether the
        /// conversion was successful.
        /// </returns>
        TryResult<T[]> TryTo<T>(string s, int count, T fallback);

        /// <summary>
        /// Validates this entity and creates a validation result object containing the errors
        /// and warnings.
        /// </summary>
        /// <param name="recursive">
        /// A value indicating whether any child models to this one should also be validated.
        /// </param>
        /// <returns>
        /// A validation result object containing all of the errors and warnings associated
        /// with this validation pass.
        /// </returns>
        ValidationResult Validate(bool recursive);

        /// <summary>
        /// Validates this entity and adds the errors and warnings to the specified log object.
        /// </summary>
        /// <param name="log">
        /// The log object the errors and warnings for this entity should be added.
        /// </param>
        /// <param name="recursive">
        /// A value indicating whether any child models to this one should also be validated.
        /// </param>
        void Validate(ILog log, bool recursive);

        /// <summary>
        /// Determines whether the specified string object can be successfully converted to a
        /// instance of the specified type.
        /// </summary>
        /// <typeparam name="T">
        /// The type that this string will be tested against.
        /// </typeparam>
        /// <param name="s">
        /// The string instance to test.
        /// </param>
        /// <param name="treatNullAsValid">
        /// A value indicating whether a null value is handled as a success.
        /// </param>
        /// <returns>
        /// True if the specified string can be successfully converted to the specified type;
        /// otherwise, false.
        /// </returns>
        bool Validate<T>(string s, bool treatNullAsValid);

        /// <summary>
        /// Determines whether this string object can be successfully converted to an array of
        /// of the specified type of the specified count.
        /// </summary>
        /// <typeparam name="T">
        /// The type that this string will be tested against.
        /// </typeparam>
        /// <param name="s">
        /// The string instance to test.
        /// </param>
        /// <param name="count">
        /// The number of instances this string should represent.
        /// </param>
        /// <param name="treatNullAsValid">
        /// A value indicating whether a null value is handled as a success.
        /// </param>
        /// <returns>
        /// True if this string can be successfully converted to the specified number of the
        /// specified type; otherwise, false.
        /// </returns>
        bool Validate<T>(string s, int count, bool treatNullAsValid);
        #endregion Methods
    } // RSG.Metadata.Model.Definitions.IDefinitionDictionary {Interface}
} // RSG.Metadata.Model.Definitions {Namespace}
