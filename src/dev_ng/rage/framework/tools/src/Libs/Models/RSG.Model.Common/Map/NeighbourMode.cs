﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common.Map
{
    /// <summary>
    /// Enumeration of the different ways a section's neighbours can be calculated
    /// </summary>
    public enum NeighbourMode
    {
        /// <summary>
        /// Neighbours are defined as any section within a certain radius of the
        /// sections center point.
        /// </summary>
        Radius,

        /// <summary>
        /// Neighbours are defined as any section intersecting the AABB around the
        /// section
        /// </summary>
        Box,

        /// <summary>
        /// Neighbours are defined as any section intersecting the radius around the
        /// center of the section and within a certain distance of the sections outline
        /// </summary>
        Distance
    } // NeighbourMode
}
