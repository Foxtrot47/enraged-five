﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.ConfigParser;

namespace RSG.Model.Common
{
    /// <summary>
    /// Publicly accessible interface for creating ICharacterCollections
    /// </summary>
    public interface ICharacterFactory
    {
        #region Methods
        /// <summary>
        /// Creates a new character collection from the specified source.
        /// </summary>
        /// <param name="source">Source to use for creating the collection.</param>
        /// <param name="buildIdentifier">Optional build identifier for when the source is Database.</param>
        /// <returns></returns>
        ICharacterCollection CreateCollection(DataSource source, string buildIdentifier = null);
        #endregion // Methods
    } // ICharacterFactory
}
