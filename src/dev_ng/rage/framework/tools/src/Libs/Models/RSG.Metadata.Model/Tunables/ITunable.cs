﻿//---------------------------------------------------------------------------------------------
// <copyright file="ITunable.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Tunables
{
    using System;
    using System.Xml;
    using RSG.Editor.Model;
    using RSG.Metadata.Model.Definitions.Members;

    /// <summary>
    /// When implemented represents a instance of a specific parCodeGen member object.
    /// </summary>
    public interface ITunable : IModel, IEquatable<ITunable>
    {
        #region Properties
        /// <summary>
        /// Gets a value indicating whether or not the value for this tunable can be inherited.
        /// </summary>
        bool CanInheritValue { get; }

        /// <summary>
        /// Gets a value indicating whether this tunable currently has it's default value. This
        /// is the equivalent of having no source data present in the metadata file.
        /// </summary>
        bool HasDefaultValue { get; set; }

        /// <summary>
        /// Gets the depth of the inheritance hierarchy this tunable is currently inside.
        /// </summary>
        int InheritanceHierarchyDepth { get; }

        /// <summary>
        /// Gets the inheritance level this tunable current resides in. This level represents
        /// the number of inheritance levels above this tunable that currently exist.
        /// </summary>
        int InheritanceLevel { get; }

        /// <summary>
        /// Gets the parent tunable that has been set as the value source of this tunables
        /// value when the value is being inherited.
        /// </summary>
        ITunable InheritanceParent { get; }

        /// <summary>
        /// Gets or sets a value indicating whether this tunable is currently locked from being
        /// edited.
        /// </summary>
        bool IsLocked { get; set; }

        /// <summary>
        /// Gets the member that this tunable is currently instancing.
        /// </summary>
        IMember Member { get; }

        /// <summary>
        /// Gets the tunable structure this instance belongs to.
        /// </summary>
        TunableStructure TunableStructure { get; }

        /// <summary>
        /// Gets the inheritance level the value for this tunable is currently coming from.
        /// This value goes from the <see cref="InheritanceLevel"/> (no inheritance) to n (the
        /// maximum level). If the value is currently coming from the psc file this property
        /// returns -1.
        /// </summary>
        int ValueSourceInheritanceLevel { get; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new <see cref="ITunable"/> that is a copy of this instance.
        /// </summary>
        /// <returns>
        /// A new <see cref="ITunable"/> that is a copy of this instance.
        /// </returns>
        new ITunable Clone();

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="ITunable"/> by
        /// looking at their values.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance by their values;
        /// otherwise, false.
        /// </returns>
        bool EqualByValue(ITunable other);

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="ITunable"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        new bool Equals(ITunable other);

        /// <summary>
        /// Resets this tunable to have its default value. (i.e no source data).
        /// </summary>
        void ResetValueToDefault();

        /// <summary>
        /// Writes the xml representation of this instance onto the specified
        /// System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that the xml representation of this instance should be
        /// written to.
        /// </param>
        /// <param name="serialiseDefaultTunables">
        /// A value indicating whether the tunables that have a default value should be written
        /// to the specified writer or not.
        /// </param>
        void Serialise(XmlWriter writer, bool serialiseDefaultTunables);
        #endregion Methods
    } // RSG.Metadata.Model.Tunables.ITunable {Interface}
} // RSG.Metadata.Model.Tunables {Namespace}
