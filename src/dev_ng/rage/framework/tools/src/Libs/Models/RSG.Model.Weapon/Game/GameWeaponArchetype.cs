﻿namespace RSG.Model.Weapon.Game
{
    using System.Collections.Generic;
    using System.Diagnostics;

    /// <summary>
    /// </summary>
    [DebuggerDisplay("Model = {Model}")]
    public class GameWeaponArchetype
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Model"/> property.
        /// </summary>
        private string _model;

        /// <summary>
        /// The private field used for the <see cref="TextureDictionaryName"/> property.
        /// </summary>
        private string _textureDictionaryName;

        /// <summary>
        /// The private field used for the <see cref="EffectsDictionaryName"/> property.
        /// </summary>
        private string _effectsDictionaryName;

        /// <summary>
        /// The private field used for the <see cref="ReferenceCount"/> property.
        /// </summary>
        private int _referenceCount;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="GameWeaponArchetype"/> class.
        /// </summary>
        public GameWeaponArchetype()
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the name of the model used for this weapon.
        /// </summary>
        public string Model
        {
            get { return this._model; }
            set { this._model = value; }
        }

        /// <summary>
        /// Gets or sets the name of the texture dictionary containing the textures used on
        /// this weapon.
        /// </summary>
        public string TextureDictionaryName
        {
            get { return this._textureDictionaryName; }
            set { this._textureDictionaryName = value; }
        }

        /// <summary>
        /// Gets or sets the name of fx asset dictionary that this weapon uses.
        /// </summary>
        public string EffectsDictionaryName
        {
            get { return this._effectsDictionaryName; }
            set { this._effectsDictionaryName = value; }
        }

        /// <summary>
        /// Gets or sets the number of times this archetype has been referenced by either a
        /// weapon, component, or ammo object.
        /// </summary>
        public int ReferenceCount
        {
            get { return this._referenceCount; }
            set { this._referenceCount = value; }
        }
        #endregion Properties
    } // RSG.Model.Weapon.Game.GameWeaponArchetype {Class}
} // RSG.Model.Weapon.Game {Namespace}
