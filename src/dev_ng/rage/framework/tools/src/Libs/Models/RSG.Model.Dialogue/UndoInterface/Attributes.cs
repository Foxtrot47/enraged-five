﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Dialogue.UndoInterface
{
    /// <summary>
    /// Attribute that means that a property has undo redo
    /// actions created for it.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class Undoable : System.Attribute
    {
        #region Constructor(s)

        /// <summary>
        /// Default constructor, no properties as presence along is enough
        /// to say that a property needs to create undo redo actions
        /// </summary>
        public Undoable()
        {
        }

        #endregion // Constructor(s)

    } // Undoable

    /// <summary>
    /// Attribute that when set means that any undo redo actions created for it can be collasped in it one if
    /// next to each other on the stack.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class Collapsable : System.Attribute
    {
        #region Constructor(s)

        /// <summary>
        /// Default constructor, no properties as presence alone is enough
        /// to say that a property undo can be collapsed
        /// </summary>
        public Collapsable()
        {
        }

        #endregion // Constructor(s)

    } // Collapsable

    /// <summary>
    /// Attribute that when set means that this property cannot be searched for by the user and a search attribute isn't created for this.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class NonSearchable : System.Attribute
    {
        #region Constructor(s)

        /// <summary>
        /// Default constructor, no properties as presence alone is enough
        /// to say that a property isn't searchable
        /// </summary>
        public NonSearchable()
        {
        }

        #endregion // Constructor(s)

    } // NonSearchable
} // RSG.Model.Dialogue.UndoInterface
