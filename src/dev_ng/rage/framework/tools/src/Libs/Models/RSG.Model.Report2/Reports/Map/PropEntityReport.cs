﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.Base.ConfigParser;
using System.IO;
using RSG.Base.Tasks;
using RSG.Model.Common.Map;
using RSG.Base.Extensions;

namespace RSG.Model.Report.Reports.Map
{
    /// <summary>
    /// 
    /// </summary>
    public class PropEntityReport : CSVReport, IDynamicLevelReport
    {
        #region Constants
        private const String c_name = "Prop Entity Report";
        private const String c_description = "Exports the prop instance information to a CSV file.";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (m_task == null)
                {
                    CompositeTask task = new CompositeTask("Generate Report");
                    task.AddSubTask(new ActionTask("Loading data", (context, progress) => EnsureDataLoaded((DynamicLevelReportContext)context, progress)), 50);
                    task.AddSubTask(new ActionTask("Generating report", (context, progress) => GenerateReport((DynamicLevelReportContext)context, progress)));
                    m_task = task;
                }
                return m_task;
            }
        }
        private ITask m_task;

        /// <summary>
        /// List of stats that are required to generate this report.
        /// </summary>
        public IEnumerable<StreamableStat> RequiredStats
        {
            get { return null; }
        }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public PropEntityReport()
            : base(c_name, c_description)
        {
        }
        #endregion // Constructor(s)

        #region Task Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void EnsureDataLoaded(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            IMapHierarchy hierarchy = context.Level.MapHierarchy;

            if (hierarchy != null)
            {
                float increment = 1.0f / hierarchy.AllSections.Count();

                // Load all the map section data
                foreach (IMapSection section in hierarchy.AllSections)
                {
                    context.Token.ThrowIfCancellationRequested();

                    // Update the progress information
                    string message = String.Format("Loading {0}", section.Name);
                    progress.Report(new TaskProgress(increment, true, message));

                    section.LoadStats(new StreamableStat[] { StreamableMapSectionStat.Archetypes, StreamableMapSectionStat.Entities });
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void GenerateReport(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            IMapHierarchy hierarchy = context.Level.MapHierarchy;

            if (hierarchy != null)
            {
                // Write out the report
                using (StreamWriter writer = new StreamWriter(Filename))
                {
                    // Get the list of entities to process
                    IEnumerable<IEntity> entities = GatherLodEntities(hierarchy);

                    // Determine which collision bit flag combinations are in use
                    ISet<CollisionType> collisionFlags = new HashSet<CollisionType>();
                    foreach (IMapArchetype archetype in entities.Where(item => item.ReferencedArchetype != null).Select(item => item.ReferencedArchetype))
                    {
                        collisionFlags.AddRange(archetype.CollisionTypePolygonCounts.Keys);
                    }

                    // Sort the collision type flags so they are always in the same order in the CSV
                    List<CollisionType> sortedCollisionFlags = new List<CollisionType>();
                    sortedCollisionFlags.AddRange(collisionFlags);
                    sortedCollisionFlags.Sort();

                    //
                    WriteCsvHeader(writer, sortedCollisionFlags);

                    // Output all the entities to the CSV report
                    foreach (IEntity entity in entities)
                    {
                        WriteCsvEntity(writer, entity, sortedCollisionFlags);
                    }
                }
            }
        }
        #endregion // Task Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        private IEnumerable<IEntity> GatherLodEntities(IMapHierarchy hierarchy)
        {
            ISet<IEntity> encounteredInstances = new HashSet<IEntity>();

            // Loop over all sections
            foreach (IMapSection section in hierarchy.AllSections)
            {
                // Loop over all entities in the section
                foreach (IEntity entity in section.ChildEntities.Where(item => item.ReferencedArchetype != null))
                {
                    // Is this a prop (i.e. the definition is referenced in an external file)
                    if (entity.Parent != (entity.ReferencedArchetype as IMapArchetype).ParentSection)
                    {
                        yield return entity;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sw"></param>
        private void WriteCsvHeader(StreamWriter writer, IEnumerable<CollisionType> collisionFlags)
        {
            string header = "Entity Name,Section,Parent Area,Grandparent Area,X,Y,Z,Archetype Name,Tri Count,Bounding Box Volume (m^3),Tri Density (tris/m^3),";
            header += "Shader Count,Texture Count,TXD Name(s),Lod Distance";

            foreach (CollisionType flag in collisionFlags)
            {
                header += String.Format(",{0} Collision", String.Join(" + ", flag.GetDisplayNames().ToArray()));
            }
            writer.WriteLine(header);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="instance"></param>
        private void WriteCsvEntity(StreamWriter writer, IEntity entity, IEnumerable<CollisionType> collisionFlags)
        {
            if (entity.IsInteriorReference == true)
            {
                return;
            }

            IMapSection section = entity.Parent as IMapSection;
            IMapArea parentArea = section.ParentArea;
            IMapArea grandParentArea = (parentArea != null ? parentArea.ParentArea : null);

            IMapArchetype archetype = entity.ReferencedArchetype as IMapArchetype;

            float triDensity = 0;
            if (archetype != null && archetype.BoundingBox.Volume() != 0)
            {
                triDensity = (float)archetype.PolygonCount / archetype.BoundingBox.Volume();
            }

            // Generate the csv line
            string csvRecord = String.Format("{0},{1},{2},{3},", entity.Name, section.Name, parentArea != null ? parentArea.Name : "n/a", grandParentArea != null ? grandParentArea.Name : "n/a");
            csvRecord += String.Format("{0},{1},{2},", entity.Position.X, entity.Position.Y, entity.Position.Z);

            if (archetype != null)
            {
                csvRecord += String.Format("{0},{1},{2},{3},", archetype.Name, archetype.PolygonCount, archetype.BoundingBox.Volume(), triDensity);
                csvRecord += String.Format("{0},{1},", archetype.Shaders.Count(), archetype.Textures.Count());
                if (!(archetype is IInteriorArchetype))
                {
                    csvRecord += string.Format("{0},", String.Join(";", archetype.TxdExportSizes.Keys));
                }
                else
                {
                    csvRecord += ",";
                }
            }
            else
            {
                csvRecord += ",,,,,,,";
            }
            csvRecord += String.Format("{0},", entity.LodDistance);

            foreach (CollisionType flag in collisionFlags)
            {
                uint collisionPolyCount = 0;
                if (archetype != null && archetype.CollisionTypePolygonCounts.ContainsKey(flag))
                {
                    collisionPolyCount = archetype.CollisionTypePolygonCounts[flag];
                }
                csvRecord += String.Format(",{0}", collisionPolyCount);
            }

            writer.WriteLine(csvRecord);
        }
        #endregion // Private Methods
    } // PropEntityReport
}
