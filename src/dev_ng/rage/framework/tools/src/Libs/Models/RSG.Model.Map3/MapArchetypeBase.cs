﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.Model.Common.Map;
using RSG.SceneXml;
using RSG.Base.ConfigParser;
using RSG.Base.Math;
using RSG.SceneXml.Statistics;
using RSG.SceneXml.Material;
using RSG.Model.Asset;
using System.Collections.ObjectModel;
using System.ComponentModel;
using RSG.Statistics.Common.Dto.GameAssetStats;
using RSG.Statistics.Common.Dto.GameAssetStats.Bundles;
using System.Diagnostics;
using RSG.Bounds;
using RSG.ObjectLinks;

namespace RSG.Model.Map3
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class MapArchetypeBase : ArchetypeBase, IMapArchetype
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="IsInDrawableLodHierarchy"/> property.
        /// </summary>
        private bool _isInDrawableLodHierarchy;

        /// <summary>
        /// The private field used for the <see cref="HasShadowProxies"/> property.
        /// </summary>
        private bool _hasShadowProxies;
        #endregion Fields

        #region Properties
        /// <summary>
        /// The parent section of this archetype
        /// </summary>
        [Browsable(false)]
        public IMapSection ParentSection
        {
            get;
            protected set;
        }

        /// <summary>
        /// 
        /// </summary>
        [StreamableStatAttribute(StreamableMapArchetypeStat.BoundingBoxString)]
        public BoundingBox3f BoundingBox
        {
            get
            {
                CheckStatLoaded(StreamableMapArchetypeStat.BoundingBox);
                return m_boundingBox;
            }
            protected set
            {
                SetPropertyValue(ref m_boundingBox, value, "BoundingBox");
                SetStatLodaded(StreamableMapArchetypeStat.BoundingBox, true);
            }
        }
        private BoundingBox3f m_boundingBox;

        /// <summary>
        /// Array of vehicle textures.
        /// </summary>
        [Browsable(false)]
        public ITexture[] Textures
        {
            get
            {
                CheckStatLoaded(StreamableMapArchetypeStat.Shaders);
                return m_textures;
            }
            private set
            {
                SetPropertyValue(ref m_textures, value, "Textures", "TextureCount");
            }
        }
        private ITexture[] m_textures;

        /// <summary>
        /// Texture count (exposed for the property grid)
        /// </summary>
        public int TextureCount
        {
            get
            {
                CheckStatLoaded(StreamableMapArchetypeStat.Shaders);
                return (Textures != null ? Textures.Count() : 0);
            }
        }

        /// <summary>
        /// Array of vehicle shaders.
        /// </summary>
        [Browsable(false)]
        [StreamableStatAttribute(StreamableMapArchetypeStat.ShadersString)]
        public IShader[] Shaders
        {
            get
            {
                CheckStatLoaded(StreamableMapArchetypeStat.Shaders);
                return m_shaders;
            }
            protected set
            {
                if (SetPropertyValue(ref m_shaders, value, "Shaders", "ShaderCount"))
                {
                    if (m_shaders != null)
                        Textures = m_shaders.SelectMany(shader => shader.Textures).Distinct().OrderBy(item => item.Name).ToArray();
                    else
                        Textures = null;
                }
                SetStatLodaded(StreamableMapArchetypeStat.Shaders, true);
            }
        }
        private IShader[] m_shaders;

        /// <summary>
        /// Shader count (exposed for the property grid)
        /// </summary>
        public int ShaderCount
        {
            get
            {
                CheckStatLoaded(StreamableMapArchetypeStat.Shaders);
                return (Shaders != null ? Shaders.Count() : 0);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [StreamableStatAttribute(StreamableMapArchetypeStat.ExportGeometrySizeString)]
        public uint ExportGeometrySize
        {
            get
            {
                CheckStatLoaded(StreamableMapArchetypeStat.ExportGeometrySize);
                return m_exportGeometrySize;
            }
            protected set
            {
                SetPropertyValue(ref m_exportGeometrySize, value, "ExportGeometrySize");
                SetStatLodaded(StreamableMapArchetypeStat.ExportGeometrySize, true);
            }
        }
        private uint m_exportGeometrySize;

        /// <summary>
        /// 
        /// </summary>
        [StreamableStatAttribute(StreamableMapArchetypeStat.TxdExportSizesString)]
        public IDictionary<String, uint> TxdExportSizes
        {
            get
            {
                CheckStatLoaded(StreamableMapArchetypeStat.TxdExportSizes);
                return m_txdExportSizes;
            }
            protected set
            {
                SetPropertyValue(ref m_txdExportSizes, value, "TxdExportSizes");
                SetStatLodaded(StreamableMapArchetypeStat.TxdExportSizes, true);
            }
        }
        private IDictionary<String, uint> m_txdExportSizes;
        
        /// <summary>
        /// 
        /// </summary>
        [StreamableStatAttribute(StreamableMapArchetypeStat.PolygonCountString)]
        public uint PolygonCount
        {
            get
            {
                CheckStatLoaded(StreamableMapArchetypeStat.PolygonCount);
                return m_polygonCount;
            }
            protected set
            {
                SetPropertyValue(ref m_polygonCount, value, "PolygonCount");
                SetStatLodaded(StreamableMapArchetypeStat.PolygonCount, true);
            }
        }
        private uint m_polygonCount;

        /// <summary>
        /// 
        /// </summary>
        [StreamableStatAttribute(StreamableMapArchetypeStat.CollisionPolygonCountString)]
        public uint CollisionPolygonCount
        {
            get
            {
                CheckStatLoaded(StreamableMapArchetypeStat.CollisionPolygonCount);
                return m_collisionPolygonCount;
            }
            protected set
            {
                SetPropertyValue(ref m_collisionPolygonCount, value, "CollisionPolygonCount");
                SetStatLodaded(StreamableMapArchetypeStat.CollisionPolygonCount, true);
            }
        }
        private uint m_collisionPolygonCount;

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        [StreamableStatAttribute(StreamableMapArchetypeStat.CollisionTypePolygonCountsString)]
        public IDictionary<CollisionType, uint> CollisionTypePolygonCounts
        {
            get
            {
                CheckStatLoaded(StreamableMapArchetypeStat.CollisionTypePolygonCounts);
                return m_collisionTypePolygonCounts;
            }
            protected set
            {
                SetPropertyValue(ref m_collisionTypePolygonCounts, value, "CollisionTypePolygonCounts");
                SetStatLodaded(StreamableMapArchetypeStat.CollisionTypePolygonCounts, true);
            }
        }
        private IDictionary<CollisionType, uint> m_collisionTypePolygonCounts;

        /// <summary>
        /// 
        /// </summary>
        [StreamableStatAttribute(StreamableMapArchetypeStat.HasAttachedLightString)]
        public bool HasAttachedLight
        {
            get
            {
                CheckStatLoaded(StreamableMapArchetypeStat.HasAttachedLight);
                return m_hasAttachedLight;
            }
            protected set
            {
                SetPropertyValue(ref m_hasAttachedLight, value, "HasAttachedLight");
                SetStatLodaded(StreamableMapArchetypeStat.HasAttachedLight, true);
            }
        }
        private bool m_hasAttachedLight;

        /// <summary>
        /// 
        /// </summary>
        [StreamableStatAttribute(StreamableMapArchetypeStat.HasExplosiveEffectString)]
        public bool HasExplosiveEffect
        {
            get
            {
                CheckStatLoaded(StreamableMapArchetypeStat.HasExplosiveEffect);
                return m_hasExplosiveEffect;
            }
            protected set
            {
                SetPropertyValue(ref m_hasExplosiveEffect, value, "HasExplosiveEffect");
                SetStatLodaded(StreamableMapArchetypeStat.HasExplosiveEffect, true);
            }
        }
        private bool m_hasExplosiveEffect;

        /// <summary>
        /// Filters the list of entities to return only the exterior entities.
        /// </summary>
        [Browsable(false)]
        public IEnumerable<IEntity> ExteriorEntities
        {
            get
            {
                return Entities.Where(item => item.Parent is IMapSection);
            }
        }

        /// <summary>
        /// Filters the list of entities to return only the interior entities.
        /// </summary>
        [Browsable(false)]
        public IEnumerable<IEntity> InteriorEntities
        {
            get
            {
                return Entities.Where(item => !(item.Parent is IMapSection));
            }
        }

        /// <summary>
        /// Gets a value indicating whether this archetype represents the highest detailed
        /// object inside a drawable lod hierarchy.
        /// </summary>
        public bool IsInDrawableLodHierarchy
        {
            get { return this._isInDrawableLodHierarchy; }
        }

        /// <summary>
        /// Gets a value indicating whether this archetype is linked to a shadow mesh.
        /// </summary>
        public bool HasShadowProxies
        {
            get { return this._hasShadowProxies;  }
        }

        /// <summary>
        /// 
        /// </summary>
        IDictionary<string, LODHierarchyDef> IMapArchetype.SceneLinks 
        {
            get { return this.m_SceneLinks; }
        }

        /// <summary>
        /// Gets a value indicating whether this archetype gets added to the ipl data or not.
        /// </summary>
        [StreamableStatAttribute(StreamableMapArchetypeStat.DontAddToIplString)]
        public bool DontAddToIpl
        {
            get
            {
                CheckStatLoaded(StreamableMapArchetypeStat.DontAddToIpl);
                return this._dontAddToIpl;
            }
            protected set
            {
                SetPropertyValue(ref this._dontAddToIpl, value, "DontAddToIpl");
                SetStatLodaded(StreamableMapArchetypeStat.DontAddToIpl, true);
            }
        }
        private bool _dontAddToIpl;

        /// <summary>
        /// Gets a value indicating whether this archetype is a permanent archetype and will
        /// always be in memory.
        /// </summary>
        [StreamableStatAttribute(StreamableMapArchetypeStat.IsPermanentArchetypeString)]
        public bool IsPermanentArchetype
        {
            get
            {
                CheckStatLoaded(StreamableMapArchetypeStat.IsPermanentArchetype);
                return this._isPermanentArchetype;
            }
            protected set
            {
                SetPropertyValue(ref this._isPermanentArchetype, value, "IsPermanentArchetype");
                SetStatLodaded(StreamableMapArchetypeStat.IsPermanentArchetype, true);
            }
        }
        private bool _isPermanentArchetype;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="name"></param>
        /// <param name="parent"></param>
        public MapArchetypeBase(string name, IMapSection parent)
            : base(name)
        {
            ParentSection = parent;

            // Optimisation - Keep track of this archetype in our lookup
            if (ParentSection != null)
            {
                ParentSection.MapHierarchy.AddArchetypeToLookup(this);
            }
        }
        #endregion // Constructor(s)

        #region IMapArchetype Implementation
        /// <summary>
        /// Load basic stats from the scene xml object def
        /// </summary>
        /// <param name="sceneObject"></param>
        public override void LoadStatsFromExportData(TargetObjectDef sceneObject, Scene scene)
        {
            base.LoadStatsFromExportData(sceneObject, scene);

            if (sceneObject.SceneLinks.Count > 0)
            {
                LODHierarchyDef lodHierachy = null;
                if (sceneObject.SceneLinks.TryGetValue(LinkChannel.LinkChannelShadowMesh, out lodHierachy))
                {
                    if (lodHierachy.Parent != null)
                    {
                        this._hasShadowProxies = true;
                    }
                }
            }

            // Determine which object we should use to extract the information
            TargetObjectDef drawableSceneObject = sceneObject;
            if (sceneObject.DrawableLOD != null)
            {
                // Get the highest definition object in the hierarchy by walking the children
                this._isInDrawableLodHierarchy = true;
                drawableSceneObject = GetHighestDefinition(sceneObject);
            }

            LoadStatsFromDrawableObject(drawableSceneObject, scene);
        }

        /// <summary>
        /// Returns the combined polygon count for a particular collision type.
        /// E.g. If Mover is passed in this would return things like Mover, Mover+Weapon, Mover+etc...
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public uint GetPolyCountForCollisionType(CollisionType type)
        {
            uint value = 0;
            foreach (KeyValuePair<CollisionType, uint> pair in CollisionTypePolygonCounts)
            {
                if ((pair.Key & type) != 0)
                {
                    value += pair.Value;
                }
            }
            return value;
        }
        #endregion // IMapArchetype Implementation

        #region Protected Methods
        /// <summary>
        /// Loads statistics from the drawable object.
        /// </summary>
        /// <param name="drawableSceneObject"></param>
        /// <param name="scene"></param>
        protected virtual void LoadStatsFromDrawableObject(TargetObjectDef drawableSceneObject, Scene scene)
        {
            // Compute the bounding box by walking all the children
            BoundingBox = ComputeBoundingBoxForObject(drawableSceneObject);

            // Extract the various statistical information
            ExportGeometrySize = 0;
            PolygonCount = 0;
            CollisionPolygonCount = 0;
            CollisionTypePolygonCounts = new Dictionary<CollisionType, uint>();

            ComputeStatisticsForObject(drawableSceneObject, scene);

            // Get the TXD name for this drawable (needs to be done before calculating txd size)
            string textureDictionaryName = ExtractTextureDictionaryName(drawableSceneObject);
            uint exportTxdSize = ComputeTxdSizeForObject(drawableSceneObject, scene, textureDictionaryName);

            TxdExportSizes = new Dictionary<string, uint>();
            TxdExportSizes.Add(textureDictionaryName, exportTxdSize);

            // Get the shader/texture list
            List<IShader> shaders = new List<IShader>();
            GetShadersForObject(drawableSceneObject, scene, ref shaders);
            Shaders = shaders.ToArray();

            // Finally determine some other stats
            HasAttachedLight = HasLightAttached(drawableSceneObject);
            HasExplosiveEffect = IsExplosive(drawableSceneObject);
            this.DontAddToIpl = drawableSceneObject.GetAttribute<bool>(AttrNames.OBJ_DONT_ADD_TO_IPL, false);
            this.IsPermanentArchetype = drawableSceneObject.GetAttribute<bool>(AttrNames.OBJ_PERMANENT_ARCHTYPE, false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="stat"></param>
        protected override void ProcessComplexArchetypeStat(ArchetypeStatDto dto, StreamableStat stat)
        {
            MapArchetypeStatDto mapDto = dto as MapArchetypeStatDto;
            Debug.Assert(mapDto != null, "Incorrect dto type encountered.");

            if (stat == StreamableMapArchetypeStat.Shaders)
            {
                ProcessShaderStats(mapDto.Shaders);
            }
            else if (stat == StreamableMapArchetypeStat.TxdExportSizes)
            {
                ProcessTxdExportSizeStats(mapDto.TxdExportSizes);
            }
            else if (stat == StreamableMapArchetypeStat.CollisionTypePolygonCounts)
            {
                ProcessCollisionPolyCountStats(mapDto.CollisionTypePolygonCounts);
            }
            else
            {
                base.ProcessComplexArchetypeStat(dto, stat);
            }
        }
        #endregion // Protected Methods

        #region Private Methods
        #region SceneXml Parsing
        /// <summary>
        /// Returns the highest definition in a scene object using the scene objects drawable hierarchy
        /// </summary>
        private TargetObjectDef GetHighestDefinition(TargetObjectDef sceneObject)
        {
            if (sceneObject.DrawableLOD != null)
            {
                if (sceneObject.DrawableLOD.Children == null || sceneObject.DrawableLOD.Children.Count() == 0)
                {
                    return sceneObject;
                }
                foreach (TargetObjectDef child in sceneObject.DrawableLOD.Children)
                {
                    return GetHighestDefinition(child);
                }
            }

            return sceneObject;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scenenObject"></param>
        /// <returns></returns>
        private BoundingBox3f ComputeBoundingBoxForObject(TargetObjectDef sceneObject)
        {
            BoundingBox3f bbox = new BoundingBox3f();

            if (sceneObject.Children != null && sceneObject.Children.Count() > 0)
            {
                bbox.Expand(sceneObject.WorldBoundingBox);

                foreach (TargetObjectDef child in GetChildren(sceneObject).Where(item => !item.IsCollision()))
                {
                    bbox.Expand(child.WorldBoundingBox);
                }

                // Convert the bounding box from world to local space
                // We should really being transforming the existing b-box by the inverse of it's center...
                if (!bbox.IsEmpty)
                {
                    float halfWidth = (bbox.Max.X - bbox.Min.X) * 0.5f;
                    float halfDepth = (bbox.Max.Y - bbox.Min.Y) * 0.5f;
                    float halfHeight = (bbox.Max.Z - bbox.Min.Z) * 0.5f;
                    bbox = new BoundingBox3f(new Vector3f(-halfWidth, -halfDepth, -halfHeight), new Vector3f(halfWidth, halfDepth, halfHeight));
                }
            }
            else
            {
                bbox.Expand(sceneObject.LocalBoundingBox);
            }

            return bbox;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="root"></param>
        /// <returns></returns>
        private IEnumerable<TargetObjectDef> GetChildren(TargetObjectDef root)
        {
            foreach (TargetObjectDef child in root.Children)
            {
                yield return child;

                foreach (TargetObjectDef grandChild in GetChildren(child))
                {
                    yield return grandChild;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sceneObject"></param>
        /// <param name="scene"></param>
        private void ComputeStatisticsForObject(TargetObjectDef sceneObject, Scene scene)
        {
            // Geometry size
            DrawableStats geometryStat = null;
            scene.GeometryStatsLookup.TryGetValue(sceneObject.Guid, out geometryStat);
            if (geometryStat != null)
            {
                ExportGeometrySize += geometryStat.Size;
            }

            // Poly count
            PolygonCount += (uint)sceneObject.PolyCount;

            // Process the children next
            foreach (TargetObjectDef childObject in sceneObject.Children.Where(item => !(item.DontExport() || item.IsDummy())))
            {
                if (childObject.IsCollision() == true)
                {
                    if (childObject.PolyCount > 0)
                    {
                        CollisionPolygonCount += (uint)childObject.PolyCount;

                        // Get the bit flags for this collision object
                        CollisionType collisionType = GetCollisionFlagsForObject(childObject);

                        if (!CollisionTypePolygonCounts.ContainsKey(collisionType))
                        {
                            CollisionTypePolygonCounts.Add(collisionType, 0);
                        }
                        CollisionTypePolygonCounts[collisionType] += (uint)childObject.PolyCount;
                    }
                }
                else if (childObject.IsObject())
                {
                    ComputeStatisticsForObject(childObject, scene);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sceneObject"></param>
        /// <returns></returns>
        private CollisionType GetCollisionFlagsForObject(TargetObjectDef sceneObject)
        {
            CollisionType flags = 0;

            // Process the rest of the collision types
            foreach (CollisionType type in Enum.GetValues(typeof(CollisionType)))
            {
                bool defaultValue = type.IsSetByDefault();

                bool isFlagSet = false;
                if (!Boolean.TryParse(sceneObject.GetUserProperty(type.GetUserPropertyName(), defaultValue.ToString()), out isFlagSet))
                {
                    isFlagSet = defaultValue;
                }

                if (isFlagSet)
                {
                    flags |= type;
                }
            }

            return flags;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sceneObject"></param>
        /// <returns></returns>
        private string ExtractTextureDictionaryName(TargetObjectDef sceneObject)
        {
            if (sceneObject.Attributes.ContainsKey(AttrNames.OBJ_TXD))
            {
                return (sceneObject.Attributes[AttrNames.OBJ_TXD] as string).ToLower();
            }
            return "";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sceneObject"></param>
        /// <param name="scene"></param>
        /// <returns></returns>
        private uint ComputeTxdSizeForObject(TargetObjectDef sceneObject, Scene scene, string textureDictionaryName)
        {
            TxdStats txdStat = null;
            scene.TxdStatsLookup.TryGetValue(textureDictionaryName, out txdStat);
            if (txdStat != null)
            {
                return txdStat.Size;
            }

            return 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sceneObject"></param>
        /// <param name="scene"></param>
        /// <param name="shaders"></param>
        private void GetShadersForObject(TargetObjectDef sceneObject, Scene scene, ref List<IShader> shaders)
        {
            MaterialDef material = null;
            if (scene.MaterialLookup.TryGetValue(sceneObject.Material, out material))
            {
                GetShadersForMaterial(material, ref shaders);
            }

            // Recurse over all the children
            foreach (TargetObjectDef child in GetChildren(sceneObject))
            {
                if (!child.IsCollision() && !child.DontExport() && !child.DontExportIDE())
                {
                    if (scene.MaterialLookup.TryGetValue(child.Material, out material))
                    {
                        GetShadersForMaterial(material, ref shaders);
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="material"></param>
        /// <param name="shaders"></param>
        private void GetShadersForMaterial(MaterialDef material, ref List<IShader> shaders)
        {
            switch (material.MaterialType)
            {
                case MaterialTypes.Rage:
                    GetShadersForRageMaterial(material, ref shaders);
                    break;

                case MaterialTypes.MultiMaterial:
                    GetShadersForMultiMaterial(material, ref shaders);
                    break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="material"></param>
        /// <param name="shaders"></param>
        private void GetShadersForRageMaterial(MaterialDef material, ref List<IShader> shaders)
        {
            if (!string.IsNullOrEmpty(material.Preset))
            {
                Shader shader = new Shader(material.Preset);

                // Add the textures to the shader combining the alpha and non-alpha textures
                for (int i = 0; i < material.TextureCount; i++)
                {
                    TextureDef currentTexture = material.Textures[i];
                    TextureDef nextTexture = ((i + 1) < material.TextureCount ? material.Textures[i + 1] : null);

                    // Check whether we should be combining the two textures
                    ITexture texture = null;

                    TextureTypes currentType = currentTexture.Type;

                    if (nextTexture != null)
                    {
                        TextureTypes nextType = nextTexture.Type;

                        if (currentType == TextureTypes.DiffuseMap && nextType == TextureTypes.DiffuseAlpha ||
                            currentType == TextureTypes.SpecularMap && nextType == TextureTypes.SpecularAlpha ||
                            currentType == TextureTypes.BumpMap && nextType == TextureTypes.BumpAlpha)
                        {
                            texture = new Texture(currentTexture.FilePath, nextTexture.FilePath, currentTexture.Type, this);
                            ++i;
                        }

                        if (currentType == TextureTypes.DiffuseAlpha && nextType == TextureTypes.DiffuseMap ||
                            currentType == TextureTypes.SpecularAlpha && nextType == TextureTypes.SpecularMap ||
                            currentType == TextureTypes.BumpAlpha && nextType == TextureTypes.BumpMap)
                        {
                            texture = new Texture(nextTexture.FilePath, currentTexture.FilePath, nextTexture.Type, this);
                            ++i;
                        }
                        else if (currentType == TextureTypes.DiffuseAlpha || currentType == TextureTypes.SpecularAlpha || currentType == TextureTypes.BumpAlpha)
                        {
                            // Ignore alpha textures without the corresponding non-alpha texture
                            continue;
                        }
                    }
                    else if (currentType == TextureTypes.DiffuseAlpha || currentType == TextureTypes.SpecularAlpha || currentType == TextureTypes.BumpAlpha)
                    {
                        // Ignore alpha textures without the corresponding non-alpha texture
                        continue;
                    }

                    // If texture isn't null then we created a combined one.  Otherwise we just need to create a "standard" texture.
                    if (texture == null)
                    {
                        texture = new Texture(currentTexture.FilePath, null, currentTexture.Type, this);
                    }

                    if (!shader.Textures.Contains(texture))
                    {
                        shader.Textures.Add(texture);
                    }
                }

                if (!shaders.Contains(shader))
                {
                    shaders.Add(shader);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="material"></param>
        /// <param name="shaders"></param>
        private void GetShadersForMultiMaterial(MaterialDef material, ref List<IShader> shaders)
        {
            if (material.HasSubMaterials)
            {
                foreach (MaterialDef child in material.SubMaterials)
                {
                    GetShadersForMaterial(child, ref shaders);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="root"></param>
        /// <returns></returns>
        private bool HasLightAttached(TargetObjectDef sceneObject)
        {
            foreach (TargetObjectDef childObject in GetChildren(sceneObject))
            {
                if (childObject.IsMaxSuperclass("light"))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sceneObject"></param>
        /// <returns></returns>
        private bool IsExplosive(TargetObjectDef sceneObject)
        {
            foreach (TargetObjectDef childObject in GetChildren(sceneObject))
            {
                if (childObject.Is2dfxExplosionEffect())
                {
                    return true;
                }
            }

            return false;
        }
        #endregion // SceneXml Parsing

        #region Database Stats
        /// <summary>
        /// 
        /// </summary>
        /// <param name="shaderDtos"></param>
        private void ProcessShaderStats(List<ShaderStatDto> shaderDtos)
        {
            List<IShader> shaders = new List<IShader>();

            foreach (ShaderStatDto shaderDto in shaderDtos)
            {
                Shader shader = new Shader(shaderDto.Name);

                foreach (TextureStatDto textureDto in shaderDto.TextureStats)
                {
                    shader.Textures.Add(new Texture(textureDto));
                }
                shaders.Add(shader);
            }

            Shaders = shaders.ToArray();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="txdDtos"></param>
        private void ProcessTxdExportSizeStats(List<TxdExportStatDto> txdDtos)
        {
            TxdExportSizes = new Dictionary<String, uint>();

            foreach (TxdExportStatDto txdDto in txdDtos)
            {
                TxdExportSizes.Add(txdDto.Name, txdDto.Size);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dtos"></param>
        private void ProcessCollisionPolyCountStats(List<CollisionPolyStatDto> collisionDtos)
        {
            CollisionTypePolygonCounts = new Dictionary<CollisionType, uint>();

            foreach (CollisionPolyStatDto collisionDto in collisionDtos)
            {
                CollisionTypePolygonCounts.Add((CollisionType)collisionDto.CollisionFlags, collisionDto.PolygonCount);
            }
        }
        #endregion // Database Stats
        #endregion // Private Methods

        #region IHasTextureChildren Implementation
        /// <summary>
        /// 
        /// </summary>
        /// <param name="texture"></param>
        /// <param name="gv"></param>
        /// <returns></returns>
        public String GetTcsFilenameForTexture(ITexture texture, ConfigGameView gv)
        {
            String tcsPath = String.Empty;

            if (ParentSection != null)
            {
                tcsPath = Util.Metadata.GetTcsFilename(texture.Name,
                                                       ParentSection.ExportZipFilepath,
                                                       Name,
                                                       TxdExportSizes.First().Key,
                                                       gv);
            }
            return tcsPath;
        }
        #endregion // IHasTextureChildren Interface
    } // ArchetypeBase
}
