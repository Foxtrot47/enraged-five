﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.Base.ConfigParser;
using RSG.Platform;
using RSG.Statistics.Client.ServiceClients;
using RSG.Statistics.Common.Dto.Enums;
using RSG.Base.Logging;
using System.Diagnostics;
using RSG.Base.Configuration;

namespace RSG.Model.Asset.Platform
{
    /// <summary>
    /// 
    /// </summary>
    public class PlatformFactory : IPlatformFactory
    {
        #region IPlatformFactory Implementation
        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public IPlatformCollection CreateCollection(DataSource source, IConfig config)
        {
            IPlatformCollection collection = null;

            if (source == DataSource.SceneXml)
            {
                Log.Log__Profile("Platform collection creation - config data.");
                collection = CreateCollectionFromExportData(config);
                Log.Log__ProfileEnd();
            }
            else if (source == DataSource.Database)
            {
                Log.Log__Profile("Platform collection creation - database.");
                collection = CreateCollectionFromDatabase();
                Log.Log__ProfileEnd();
            }
            else
            {
                Debug.Assert(false, String.Format("Unknown data source '{0}' specified while creating a platform collection.", source.ToString()));
                throw new ArgumentException(String.Format("Unknown data source '{0}' specified while creating a platform collection.", source.ToString()));
            }

            return collection;
        }
        #endregion // IPlatformFactory Implementation

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="gv"></param>
        /// <returns></returns>
        private IPlatformCollection CreateCollectionFromExportData(IConfig config)
        {
            PlatformCollection platforms = new PlatformCollection();

            foreach (KeyValuePair<RSG.Platform.Platform, ITarget> pair in config.Project.DefaultBranch.Targets)
            {
                if (pair.Value.Enabled)
                {
                    platforms.Add(pair.Key);
                }
            }

            platforms.Sort();
            return platforms;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gv"></param>
        /// <returns></returns>
        private IPlatformCollection CreateCollectionFromDatabase()
        {
            // Get the list of platforms for this project
            PlatformCollection platforms = new PlatformCollection();

            using (EnumClient client = new EnumClient())
            {
                foreach (PlatformDto dto in client.GetPlatforms())
                {
                    platforms.Add(dto.Value);
                }
            }

            platforms.Sort();
            return platforms;
        }
        #endregion // Private Methods
    } // PlatformFactory
}
