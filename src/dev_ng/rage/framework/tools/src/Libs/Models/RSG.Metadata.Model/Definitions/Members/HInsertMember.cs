﻿// --------------------------------------------------------------------------------------------
// <copyright file="HInsertMember.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions.Members
{
    using System;
    using System.Xml;
    using RSG.Editor;

    /// <summary>
    /// Represents the &lt;hinsert&gt; member node in the parCodeGen system that can be
    /// instanced in a parCodeGen metadata file.
    /// </summary>
    public class HInsertMember : MemberBase, IEquatable<HInsertMember>
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Data"/> property.
        /// </summary>
        private string _data;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="HInsertMember"/> class to be one of
        /// the members of the specified structure.
        /// </summary>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        public HInsertMember(IStructure structure)
            : base(structure)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="HInsertMember"/> class as a copy of the
        /// specified instance.
        /// </summary>
        /// <param name="other">
        /// The instance to copy.
        /// </param>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        public HInsertMember(HInsertMember other, IStructure structure)
            : base(other, structure)
        {
            this._data = other._data;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="HInsertMember"/> class.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        public HInsertMember(XmlReader reader, IStructure structure)
            : base(reader, structure)
        {
            this.Deserialise(reader);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the number of bytes that this member is representing as padding.
        /// </summary>
        public string Data
        {
            get { return this._data; }
            set { this.SetProperty(ref this._data, value); }
        }

        /// <summary>
        /// Gets a value indicating whether this member can have instances of it defined in
        /// metadata, i.e whether or not a tunable can be created for it.
        /// </summary>
        public override bool CanBeInstanced
        {
            get { return false; }
        }

        /// <summary>
        /// Gets the type name for this member.
        /// </summary>
        public override string TypeName
        {
            get { return "hinsert"; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new <see cref="HInsertMember"/> that is a copy of this instance.
        /// </summary>
        /// <returns>
        /// A new <see cref="HInsertMember"/> that is a copy of this instance.
        /// </returns>
        public new HInsertMember Clone()
        {
            return new HInsertMember(this, this.Structure);
        }

        /// <summary>
        /// Creates a deep copy of the current instance.
        /// </summary>
        /// <returns>
        /// A deep copy of the current System.Object.
        /// </returns>
        public override object DeepClone()
        {
            return this.Clone();
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified
        /// <see cref="HInsertMember"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public bool Equals(HInsertMember other)
        {
            if (other == null)
            {
                return false;
            }

            return object.ReferenceEquals(other, this);
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="IMember"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public override bool Equals(IMember other)
        {
            return this.Equals(other as HInsertMember);
        }

        /// <summary>
        /// Writes the xml representation of this instance onto the specified
        /// System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that the xml representation of this instance should be
        /// written to.
        /// </param>
        public override void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                throw new SmartArgumentNullException(() => writer);
            }

            this.SerialiseBaseAttributes(writer);
            if (this._data != null)
            {
                writer.WriteString(this._data);
            }
        }
        
        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            if (reader.IsEmptyElement)
            {
                reader.Skip();
            }
            else
            {
                this._data = reader.ReadInnerXml();
            }

            reader.Skip();
        }
        #endregion Methods
    } // RSG.Metadata.Model.Definitions.Members.PadMember {Class}
} // RSG.Metadata.Model.Definitions.Members {Namespace}
