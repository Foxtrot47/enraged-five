﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common.Map
{
    public enum SpawnPointAvailableModes
    {
        kUnknown,
        kBoth,
        kSingleplayer,
        kMultiplayer
    }

    /// <summary>
    /// 
    /// </summary>
    public interface ISpawnPointCluster
    {
        /// <summary>
        /// 
        /// </summary>
        IList<ISpawnPoint> SpawnPoints { get; }
    }

    /// <summary>
    /// 
    /// </summary>
    public interface ISpawnPoint : IMapObjectWithTransform
    {
        /// <summary>
        /// 
        /// </summary>
        string SourceFile { get; }

        /// <summary>
        /// 
        /// </summary>
        string SpawnType { get; }

        /// <summary>
        /// Group spawn point this in.
        /// </summary>
        string SpawnGroup { get; }

        /// <summary>
        /// Model set associated with the spawn type.
        /// </summary>
        string ModelSet { get; }

        /// <summary>
        /// Game modes this spawn point is enabled in.
        /// </summary>
        SpawnPointAvailableModes? AvailabilityMode { get; }

        /// <summary>
        /// Start time active.
        /// </summary>
        int? StartTime { get; }

        /// <summary>
        /// End time active.
        /// </summary>
        int? EndTime { get; }

        /// <summary>
        /// Parent entity, if this spawn point is attached to an object.
        /// </summary>
        ISimpleMapArchetype Archetype { get; }

    } // ISpawnPoint
}
