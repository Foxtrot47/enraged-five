﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.Model.Common.Animation;
using System.ComponentModel;
using RSG.Base.ConfigParser;
using System.IO;
using System.Xml.Linq;
using RSG.Base.Logging;
using System.Xml.XPath;
using RSG.Base.Collections;
using System.Collections;
using System.Runtime.Serialization;

namespace RSG.Model.Common.Animation
{
    [DataContract]
    [KnownType(typeof(ClipDictionary))]
    public class ClipDictionaryCollection : AssetBase, IClipDictionaryCollection
    {
        #region Properties
        /// <summary>
        /// ClipDictionaries that this collection contains.
        /// </summary>
        [DataMember]       
        public ICollection<IClipDictionary> ClipDictionaries { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Paramaterised constructor
        /// </summary>
        public ClipDictionaryCollection(string name)
            : base(name)
        {
            ClipDictionaries = new List<IClipDictionary>();
        }
        #endregion // Constructor(s)

        #region IEnumerable<IClipDictionary> Implementation
        /// <summary>
        /// Loops through the ClipDictionaries and returns each one in turn
        /// </summary>
        IEnumerator<IClipDictionary> IEnumerable<IClipDictionary>.GetEnumerator()
        {
            foreach (IClipDictionary clipDictionary in this.ClipDictionaries)
            {
                yield return clipDictionary;
            }
        }

        /// <summary>
        /// Returns the enumerator for this object
        /// </summary>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerator)this);
        }
        #endregion // IEnumerable<IClipDictionary> Implementation

        #region ICollection<IClipDictionary> Implementation
        #region ICollection<IClipDictionary> Properties
        /// <summary>
        /// Returns whether the collection is read-only
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Return number of items in the set.
        /// </summary>
        public int Count
        {
            get
            {
                return ClipDictionaries.Count;
            }
        }
        #endregion // ICollection<IClipDictionary> Properties

        #region ICollection<IClipDictionary> Methods
        /// <summary>
        /// Add a clipdictionary to the collection.
        /// </summary>
        /// <param name="item"></param>
        public void Add(IClipDictionary item)
        {
            ClipDictionaries.Add(item);
        }

        /// <summary>
        /// Removes all ClipDictionarys from the collection.
        /// </summary>
        public void Clear()
        {
            ClipDictionaries.Clear();
        }

        /// <summary>
        /// Determine whether the collection contains a specific ClipDictionary
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Contains(IClipDictionary item)
        {
            return ClipDictionaries.Contains(item);
        }

        /// <summary>
        /// Copy items in the collection to an array, starting at a particular index.
        /// </summary>
        /// <param name="output"></param>
        /// <param name="index"></param>
        public void CopyTo(IClipDictionary[] output, int index)
        {
            ClipDictionaries.CopyTo(output, index);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        public bool Remove(IClipDictionary item)
        {
            return ClipDictionaries.Remove(item);
        }
        #endregion // ICollection<IClipDictionary> Methods
        #endregion // ICollection<IClipDictionary> Implementation
    } // class ClipDictionaryCollection
} // namespace RSG.Model.Animation
