﻿//---------------------------------------------------------------------------------------------
// <copyright file="BitsetItem.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Tunables
{
    using System.ComponentModel;
    using RSG.Editor.Model;

    /// <summary>
    /// Represents a item contained by a bitset tunable. This item either represents a enum
    /// value for the tunable or a hexidecimal integer value.
    /// </summary>
    public class BitsetItem : ModelBase
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="AssociatedWithEnumValue"/> property.
        /// </summary>
        private bool _associatedWithEnumValue;

        /// <summary>
        /// The default state of the is checked value for this bitset item.
        /// </summary>
        private bool _defaultState;

        /// <summary>
        /// The default state of the is checked value for this bitset item.
        /// </summary>
        private BitsetItem _inheritanceItem;

        /// <summary>
        /// The private field used for the <see cref="IsChecked"/> property.
        /// </summary>
        private bool _isChecked;

        /// <summary>
        /// The private field used for the <see cref="Name"/> property.
        /// </summary>
        private string _name;

        /// <summary>
        /// The private field used for the <see cref="Value"/> property.
        /// </summary>
        private long _value;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="BitsetItem"/> class.
        /// </summary>
        /// <param name="name">
        /// The text to display as content for this item.
        /// </param>
        /// <param name="isChecked">
        /// A value indicating this item is initially checked.
        /// </param>
        /// <param name="defaultState">
        /// The default state of the is checked value for this bitset item.
        /// </param>
        /// <param name="parent">
        /// A reference to the bitset tunable that created this instance.
        /// </param>
        public BitsetItem(string name, bool isChecked, bool defaultState, BitsetTunable parent)
            : base(parent)
        {
            this._name = name;
            this._isChecked = isChecked;
            this._defaultState = defaultState;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="BitsetItem"/> class as a copy of the
        /// specified instance.
        /// </summary>
        /// <param name="other">
        /// The instance to copy.
        /// </param>
        /// <param name="parent">
        /// A reference to the bitset tunable that created this instance.
        /// </param>
        public BitsetItem(BitsetItem other, BitsetTunable parent)
            : base(parent)
        {
            this._name = other._name;
            this._isChecked = other._isChecked;
            this._defaultState = other._defaultState;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether this bit item is associated with a enum
        /// constant value or not.
        /// </summary>
        public bool AssociatedWithEnumValue
        {
            get { return this._associatedWithEnumValue; }
            set { this.SetProperty(ref this._associatedWithEnumValue, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this bit is currently 'turned on' or
        /// not.
        /// </summary>
        public bool IsChecked
        {
            get
            {
                return this._isChecked;
            }

            set
            {
                using (new UndoRedoBatch(this.UndoEngine))
                {
                    ((BitsetTunable)this.Parent).HasDefaultValue = false;
                    this.SetProperty(ref this._isChecked, value);
                }
            }
        }

        /// <summary>
        /// Gets the text that is displayed for this bitset item.
        /// </summary>
        public string Name
        {
            get { return this._name; }
        }

        /// <summary>
        /// Gets or sets the value of the item based on the associated enum constant or index.
        /// </summary>
        public long Value
        {
            get { return this._value; }
            set { this.SetProperty(ref this._value, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Sets this items is checked value to its default value.
        /// </summary>
        public void SetToDefaultState()
        {
            if (this._inheritanceItem != null)
            {
                this.IsChecked = this._inheritanceItem.IsChecked;
            }
            else
            {
                this.IsChecked = this._defaultState;
            }
        }

        /// <summary>
        /// Called whenever the bitset item whose value will be used when this bitset item is
        /// being inherited changes.
        /// </summary>
        internal void InheritanceParentChanged(BitsetItem newValue)
        {
            string name = "IsChecked";
            if (this._inheritanceItem != null)
            {
                PropertyChangedEventManager.RemoveHandler(
                    this._inheritanceItem, this.OnInheritedValueChanged, name);
            }

            this._inheritanceItem = newValue;
            if (this._inheritanceItem != null)
            {
                PropertyChangedEventManager.AddHandler(
                    this._inheritanceItem, this.OnInheritedValueChanged, name);
            }
        }

        /// <summary>
        /// Called whenever the value of the inheritance parent changes so that the values can
        /// be kept in sync.
        /// </summary>
        /// <param name="sender">
        /// The inheritance parent.
        /// </param>
        /// <param name="e">
        /// The event data.
        /// </param>
        private void OnInheritedValueChanged(object sender, PropertyChangedEventArgs e)
        {
            if (!((BitsetTunable)this.Parent).HasDefaultValue)
            {
                return;
            }

            if (this._inheritanceItem != null)
            {
                this._isChecked = this._inheritanceItem.IsChecked;
                this.NotifyPropertyChanged("IsChecked");
            }
        }
        #endregion Methods
    } // RSG.Metadata.Model.Tunables.BitsetItem {Class}
} // RSG.Metadata.Model.Tunables {Namespace}
