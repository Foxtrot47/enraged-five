﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.Model.Common.Animation;
using RSG.Base.Collections;
using System.Collections;
using System.Runtime.Serialization;

namespace RSG.Model.Animation
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    public abstract class CutsceneCollectionBase : AssetBase, ICutsceneCollection
    {
        #region Properties
        /// <summary>
        /// The collection of cutscenes that are currently loaded.
        /// </summary>
        [DataMember]
        public ICollection<ICutscene> Cutscenes { get; protected set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public CutsceneCollectionBase()
            : base("Cutscenes")
        {
            Cutscenes = new List<ICutscene>();
        }
        #endregion // Constructor(s)

        #region IEnumerable<ICutscene> Implementation
        /// <summary>
        /// Loops through the cutscenes and returns each one in turn
        /// </summary>
        IEnumerator<ICutscene> IEnumerable<ICutscene>.GetEnumerator()
        {
            foreach (ICutscene cutscene in this.Cutscenes)
            {
                yield return cutscene;
            }
        }

        /// <summary>
        /// Returns the Enumerator for this object
        /// </summary>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerator)this);
        }
        #endregion // IEnumerable<ICutscene> Implementation

        #region ICollection<ICutscene> Implementation
        #region ICollection<ICutscene> Properties
        /// <summary>
        /// Returns whether the collection is read-only
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Return number of items in the set.
        /// </summary>
        public int Count
        {
            get
            {
                return Cutscenes.Count;
            }
        }
        #endregion // ICollection<ICutscene> Properties

        #region ICollection<ICutscene> Methods
        /// <summary>
        /// Add a cutscene to the collection.
        /// </summary>
        /// <param name="item"></param>
        public void Add(ICutscene item)
        {
            Cutscenes.Add(item);
        }

        /// <summary>
        /// Removes all cutscenes from the collection.
        /// </summary>
        public void Clear()
        {
            Cutscenes.Clear();
        }

        /// <summary>
        /// Determine whether the collection contains a specific cutscene
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Contains(ICutscene item)
        {
            return Cutscenes.Contains(item);
        }

        /// <summary>
        /// Copy items in the collection to an array, starting at a particular index.
        /// </summary>
        /// <param name="output"></param>
        /// <param name="index"></param>
        public void CopyTo(ICutscene[] output, int index)
        {
            Cutscenes.CopyTo(output, index);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        public bool Remove(ICutscene item)
        {
            return Cutscenes.Remove(item);
        }
        #endregion // ICollection<ICutscene> Methods
        #endregion // ICollection<ICutscene> Implementation
    } // CutsceneCollectionBase
}
