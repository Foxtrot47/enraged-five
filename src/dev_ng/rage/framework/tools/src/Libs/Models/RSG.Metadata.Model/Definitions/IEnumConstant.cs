﻿// --------------------------------------------------------------------------------------------
// <copyright file="IEnumConstant.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions
{
    using System;
    using System.Xml;
    using RSG.Editor.Model;

    /// <summary>
    /// When implemented represents a constant member for a enumeration definition.
    /// </summary>
    public interface IEnumConstant : IModel, IEquatable<IEnumConstant>
    {
        #region Properties
        /// <summary>
        /// Gets or sets the description for this constant.
        /// </summary>
        string Description { get; set; }

        /// <summary>
        /// Gets or sets a reference to the enumeration that owns this constant.
        /// </summary>
        IEnumeration Enumeration { get; set; }

        /// <summary>
        /// Gets or sets the filename this instance is defined in or null if not applicable.
        /// </summary>
        string Filename { get; set; }

        /// <summary>
        /// Gets or sets the systems that this value will be hidden from.
        /// </summary>
        string[] HideFrom { get; set; }

        /// <summary>
        /// Gets or sets the line number this instance is defined on or -1 if not available.
        /// </summary>
        int LineNumber { get; set; }

        /// <summary>
        /// Gets or sets the line position this instance is defined at or -1 if not available.
        /// </summary>
        int LinePosition { get; set; }

        /// <summary>
        /// Gets the location of this definition inside the parCodeGen definition data sat on
        /// the local disk.
        /// </summary>
        FileLocation Location { get; }

        /// <summary>
        /// Gets or sets the name of the constant.
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Gets the raw string representation of the value this constant represents.
        /// </summary>
        string RawValue { get; }

        /// <summary>
        /// Gets or sets the value this constant represents.
        /// </summary>
        long Value { get; set; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new <see cref="IEnumConstant"/> that is a copy of this instance.
        /// </summary>
        /// <returns>
        /// A new <see cref="IEnumConstant"/> that is a copy of this instance.
        /// </returns>
        new IEnumConstant Clone();

        /// <summary>
        /// Writes the xml representation of this instance onto the specified
        /// System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that the xml representation of this instance should be
        /// written to.
        /// </param>
        void Serialise(XmlWriter writer);
        #endregion Methods
    } // RSG.Metadata.Model.Definitions.IEnumConstant {Interface}
} // RSG.Metadata.Model.Definitions {Namespace}
