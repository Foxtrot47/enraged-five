﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Collections;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using RSG.Model.Map;

namespace RSG.Model.Map.ViewModel
{
    public class PropSectionViewModel :
        RSG.Base.Editor.HierarchicalViewModelBase, IMapViewModelComponent, IMapSectionViewModel
    {
        #region Properties

        /// <summary>
        /// The name of the map section
        /// </summary>
        public String Name
        {
            get { return Model.Name.ToLower(); }
        }

        /// <summary>
        /// The model reference that this view model represents.
        /// </summary>
        public MapSection Model
        {
            get { return m_model; }
            set { m_model = value; }
        }
        private MapSection m_model;

        /// <summary>
        /// The level that this section belongs to.
        /// </summary>
        public LevelViewModel Level
        {
            get;
            private set;
        }

        /// <summary>
        /// Returns true iff this map section represents a non generic map section
        /// that also exports definitions. (i.e exports a ipl file and a ide file).
        /// </summary>
        public Boolean NonGenericWithDefinitions
        {
            get;
            private set;
        }

        /// <summary>
        /// The user data that can be attached to this view model
        /// </summary>
        public UserData ViewModelUserData
        {
            get { return m_viewModelUserData; }
            set { m_viewModelUserData = value; }
        }
        private UserData m_viewModelUserData = new UserData();

        public ObservableCollection<MapDefinitionViewModel> Definitions
        {
            get { return m_definitions; }
            set
            {
                SetPropertyValue(value, () => this.Definitions,
                    new PropertySetDelegate(delegate(Object newValue) { m_definitions = (ObservableCollection<MapDefinitionViewModel>)newValue; }));
            }
        }
        private ObservableCollection<MapDefinitionViewModel> m_definitions;

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Default Constructor
        /// </summary>
        public PropSectionViewModel(IViewModel parent, MapSection model)
        {
            this.Parent = parent;
            this.Model = model;
            this.Definitions = new ObservableCollection<MapDefinitionViewModel>();
            this.Definitions.Add(new MapDefinitionViewModel());
        }

        #endregion // Constructors

        #region Override Functions

        protected override void OnFirstExpanded()
        {
            if (this.Model.Definitions != null)
            {
                ObservableCollection<MapDefinitionViewModel> definitions = new ObservableCollection<MapDefinitionViewModel>();
                foreach (MapDefinition definition in this.Model.Definitions)
                {
                    definitions.Add(new MapDefinitionViewModel(this, definition));
                }
                this.Definitions = definitions;
            }
        }

        public override IViewModel GetChildWithString(String name)
        {
            foreach (MapDefinitionViewModel child in this.Definitions)
            {
                if (String.Compare(child.Name, name, true) == 0)
                {
                    return child as IViewModel;
                }
            }
            return null;
        }

        #endregion // Override Functions
    } // PropSectionViewModel
} // Workbench.AddIn.MapBrowser.MapViewModel
