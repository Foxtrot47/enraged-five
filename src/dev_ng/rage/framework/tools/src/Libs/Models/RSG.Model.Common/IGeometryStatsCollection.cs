﻿using System;

namespace RSG.Model.Common
{
    public interface IGeometryStatsCollection
    {
        bool HasStatsForArchetype(string archetypeName);
        IGeometryStats GetStatsForArchetype(string archetypeName);
    }
}
