﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Configuration;

namespace RSG.Model.Common
{
    /// <summary>
    /// 
    /// </summary>
    public interface IPlatformFactory
    {
        #region Methods
        /// <summary>
        /// Creates a new platform collection from the specified source.
        /// </summary>
        /// <param name="source">Source to use for creating the collection.</param>
        /// <param name="buildIdentifier">Optional build identifier for when the source is Database.</param>
        /// <returns></returns>
        IPlatformCollection CreateCollection(DataSource source, IConfig config);
        #endregion // Methods
    } // IPlatformFactory
}
