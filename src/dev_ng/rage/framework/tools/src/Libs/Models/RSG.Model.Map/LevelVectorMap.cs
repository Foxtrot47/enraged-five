﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Xml;
using System.Xml.XPath;
using RSG.Base.Logging;
using RSG.Base.Math;
using RSG.Model.Map.Utilities;

namespace RSG.Model.Map
{
    
    /// <summary>
    /// The LevelVectorMap class is designed as being a potential Level object's
    /// user data representing each map section's boundary as a polygon outline.
    /// </summary>
    public class LevelVectorMap
    {
        #region Constants
        private static readonly XPathExpression XPATH_SECTION =
            XPathExpression.Compile("/vector_map/section");
        private static readonly XPathExpression XPATH_SECTION_NAME =
            XPathExpression.Compile("@name");
        private static readonly XPathExpression XPATH_SPLINE_POINT =
            XPathExpression.Compile("spline/point");
        private static readonly XPathExpression XPATH_SPLINE_COLOUR =
            XPathExpression.Compile("colour");
        #endregion // Constants

        /// <summary>
        /// 
        /// </summary>
        public struct Polygon
        {
            public System.Drawing.Color colour;
            public Vector2f[] points;
        }

        #region Properties and Associated Member Data
        /// <summary>
        /// Associated Level object.
        /// </summary>
        public Level Level
        {
            get { return m_Level; }
            private set { m_Level = value; }
        }
        private Level m_Level;

        /// <summary>
        /// Vector map XML filename currently loaded.
        /// </summary>
        public String Filename
        {
            get { return m_sFilename; }
            set { m_sFilename = value; }
        }
        private String m_sFilename;

        /// <summary>
        /// Polygon data, indexed by MapSection.
        /// </summary>
        public Dictionary<MapSection, Polygon> Polygons
        {
            get { return m_Polygons; }
            set { m_Polygons = value; }
        }
        private Dictionary<MapSection, Polygon> m_Polygons;
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public LevelVectorMap(Level level, String filename)
        {
            this.Level = level;
            this.Filename = filename;
            this.Polygons = new Dictionary<MapSection, Polygon>();
            Parse();
        }
        #endregion // Constructor(s)


        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private void Parse()
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(this.Filename);

                XPathNavigator navigator = xmlDoc.CreateNavigator();
                XPathNodeIterator splineIter = navigator.Select(XPATH_SECTION);
                while (splineIter.MoveNext())
                {
                    // Parse section name
                    String mapSectionName = String.Empty;
                    XPathNodeIterator navNameIter = splineIter.Current.Select(XPATH_SECTION_NAME);
                    if (navNameIter.MoveNext())
                        mapSectionName = (navNameIter.Current.Value as String);
                    Debug.Assert(!String.IsNullOrEmpty(mapSectionName),
                        "Map Section name empty; invalid vector map XML.");
                    //Debug.Assert(StaticMapHelperFunctions.ContainsMapSection(this.Level, mapSectionName, true),
                    //    String.Format("Map Section {0} not found in current level.", mapSectionName));
                    if (!StaticMapHelperFunctions.ContainsMapSection(this.Level, mapSectionName, true))//  this.Level.ContainsMapSection(mapSectionName))
                    {
                        Log.Log__Error("Map Section {0} not found in current level.", mapSectionName);
                        continue;
                    }

                    // Parse points
                    Polygon poly = ParseSpline(splineIter.Current);

                    IMapComponent mapComponent = StaticMapHelperFunctions.GetMapSection(this.Level, mapSectionName, true);
                    if (mapComponent is MapSection)
                    {
                        this.Polygons[mapComponent as MapSection] = poly;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Log__Exception(ex, "Unhandled exception in vector map parse");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="navigator"></param>
        private Polygon ParseSpline(XPathNavigator navigator)
        {
            System.Drawing.Color colour = System.Drawing.Color.Black;
            List<Vector2f> points = new List<Vector2f>();

            XPathNodeIterator colourIter = navigator.Select(XPATH_SPLINE_COLOUR);
            while (colourIter.MoveNext())
            {
                XPathNavigator nav = colourIter.Current;
                colour = System.Drawing.Color.FromArgb(
                    (int)(float.Parse(nav.GetAttribute("r", "")) * 255),
                    (int)(float.Parse(nav.GetAttribute("g", "")) * 255),
                    (int)(float.Parse(nav.GetAttribute("b", "")) * 255));
            }

            XPathNodeIterator pointIter = navigator.Select(XPATH_SPLINE_POINT);
            while (pointIter.MoveNext())
            {
                XPathNavigator nav = pointIter.Current;
                String xCoord = nav.GetAttribute("x", "");
                String yCoord = nav.GetAttribute("y", "");
                Vector2f v = new Vector2f(float.Parse(xCoord), float.Parse(yCoord));
                points.Add(v);
            }

            Polygon poly = new Polygon();
            poly.points = points.ToArray();
            poly.colour = colour;
            return (poly);
        }
        #endregion // Private Methods
    }

} // RSG.Model.Map namespace
