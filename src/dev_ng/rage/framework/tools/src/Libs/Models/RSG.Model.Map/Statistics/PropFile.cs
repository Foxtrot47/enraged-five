﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.SceneXml;
using RSG.SceneXml.Statistics;
using RSG.Base.Math;

namespace RSG.Model.Map.Statistics
{
    /// <summary>
    /// Sorts the statistics for a map section that represent a single/multiple props,
    /// each prop should have a name, polycount, polydensity, and collision stats if available
    /// </summary>
    public class PropFile :
        Base
    {
        #region Properties

        /// <summary>
        /// The collection of single prop statistics indexed by
        /// the prop name
        /// </summary>
        public Dictionary<String, SingleProp> Props
        {
            get { return m_props; }
            set { m_props = value; }
        }
        private Dictionary<String, SingleProp> m_props;

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="section"></param>
        public PropFile(MapSection section)
        {
            this.Props = new Dictionary<String, SingleProp>();

            Scene scene = SceneManager.GetScene(section.SceneXmlFilename);
            if (scene != null)
            {
                Stats sceneStatistics = scene.Statistics;
                if (sceneStatistics != null)
                {
                    if (sceneStatistics.GeometryStats.Count > 0)
                    {
                        IDictionary<Guid, DrawableStats> statistics = scene.Statistics.GeometryStats;
                        Boolean findCollision = sceneStatistics.CollisionStats.Count > 0 ? true : false;
                        Boolean findTxd = sceneStatistics.TxdStats.Count > 0 ? true : false;

                        DrawableStats collisionStats = null;
                        TxdStats txdStats = null;
                        foreach (DrawableStats drawableStat in statistics.Values)
                        {
                            TargetObjectDef drawable = scene.FindObject(drawableStat.Guid);
                            if (drawable.DontExport() == false && drawable.DontExportIDE() == false && drawable.DontExportIPL() == false)
                            {
                                if (findCollision)
                                {
                                    foreach (TargetObjectDef child in drawable.Children)
                                    {
                                        if (child.IsCollision())
                                        {
                                            sceneStatistics.CollisionStats.TryGetValue(child.Guid, out collisionStats);
                                        }
                                    }
                                }
                                if (findTxd)
                                {
                                    String txdName = drawable.GetAttribute(AttrNames.OBJ_TXD, AttrDefaults.OBJ_TXD);
                                    sceneStatistics.TxdStats.TryGetValue(txdName, out txdStats);
                                }

                                if (!this.Props.ContainsKey(drawable.Name))
                                {
                                    SingleProp newStat = new SingleProp(drawable.Name, drawableStat, collisionStats, txdStats);
                                    this.Props.Add(drawable.Name, newStat);
                                }
                            }
                        }
                    }
                }
            }
        }

        #endregion // Constructor(s)

        #region Override Functions

        #region Total Cost Statistics

        public override int GetHighTotalCost()
        {
            return -1;
        }

        public override int GetLodTotalCost()
        {
            return -1;
        }

        public override int GetSLodTotalCost()
        {
            return -1;
        }

        #endregion // Total Cost Statistics

        #region Geometry Cost Statistics

        public override int GetHighGeometryCost()
        {
            return -1;
        }

        public override int GetLodGeometryCost()
        {
            return -1;
        }

        public override int GetSLodGeometryCost()
        {
            return -1;
        }

        #endregion // Geometry Cost Statistics

        #region TXD Cost Statistics

        public override int GetHighTXDCost()
        {
            return -1;
        }

        public override int GetLodTXDCost()
        {
            return -1;
        }

        public override int GetSLodTXDCost()
        {
            return -1;
        }

        #endregion // TXD Cost Statistics

        #region Geometry Count Statistics

        public override int GetHighGeometryCount()
        {
            return -1;
        }

        public override int GetLodGeometryCount()
        {
            return -1;
        }

        public override int GetSLodGeometryCount()
        {
            return -1;
        }

        #endregion // Geometry Count Statistics

        #region TXD Count Statistics

        public override int GetHighTXDCount()
        {
            return -1;
        }

        public override int GetLodTXDCount()
        {
            return -1;
        }

        public override int GetSLodTXDCount()
        {
            return -1;
        }

        #endregion // TXD Count Statistics

        #region Polygon Count Statistics

        public override int GetHighPolygonCount()
        {
            return -1;
        }

        public override int GetLodPolygonCount()
        {
            return -1;
        }

        public override int GetSLodPolygonCount()
        {
            return -1;
        }

        #endregion // Polygon Count Statistics

        #region Polygon Density Statistics

        public override float GetHighPolygonDensity()
        {
            return -1;
        }

        public override float GetLodPolygonDensity()
        {
            return -1;
        }

        public override float GetSLodPolygonDensity()
        {
            return -1;
        }

        #endregion // Polygon Density Statistics

        #region Collision Statistics

        public override int GetCollisionPolygonCount()
        {
            return -1;
        }

        public override float GetCollisionPolygonDensity()
        {
            return -1;
        }

        public override float GetCollisionPolygonRatio()
        {
            return -1;
        }

        #endregion // Collision Statistics

        #region LOD Distance Statistics

        public override int GetHighLodDistance()
        {
            return -1;
        }

        public override int GetLodLodDistance()
        {
            return -1;
        }

        public override int GetSLodLodDistance()
        {
            return -1;
        }

        #endregion // LOD Distance Statistics

        #region Reference Total Cost Statistics

        public override int GetAllRefTotalCost()
        {
            return -1;
        }

        public override int GetUniqueRefTotalCost()
        {
            return -1;
        }

        #endregion // Reference Cost Statistics

        #region Reference Geometry Cost Statistics

        public override int GetAllRefGeometryCost()
        {
            return -1;
        }

        public override int GetUniqueRefGeometryCost()
        {
            return -1;
        }

        #endregion // Reference Geometry Cost Statistics

        #region Reference TXD Statistics

        public override int GetAllRefTXDCost()
        {
            return -1;
        }

        public override int GetAllRefTXDCount()
        {
            return -1;
        }

        #endregion // Reference TXD Statistics

        #region Reference Count Statistics

        public override int GetAllRefCount()
        {
            return -1;
        }

        public override int GetUniqueRefCount()
        {
            return -1;
        }

        #endregion // Reference Count Statistics

        #region Reference Polygon Count Statistics

        public override int GetReferencePolygonCountAll()
        {
            return -1;
        }

        public override int GetReferencePolygonCountUnique()
        {
            return -1;
        }

        #endregion // Reference Polygon Count Statistics

        #region Reference Polygon Density Statistics

        public override float GetReferencePolygonDensityAll()
        {
            return -1;
        }

        public override float GetReferencePolygonDensityUnique()
        {
            return -1;
        }

        #endregion // Reference Polygon Density Statistics

        #region Reference Collision Count Statistics

        public override int GetReferenceCollisionPolygonCountAll()
        {
            return -1;
        }

        public override int GetReferenceCollisionPolygonCountUnique()
        {
            return -1;
        }

        #endregion // Reference Collision Count Statistics

        #region Reference Collision Density Statistics

        public override float GetReferenceCollisionPolygonDensityAll()
        {
            return -1;
        }

        public override float GetReferenceCollisionPolygonDensityUnique()
        {
            return -1;
        }

        #endregion // Reference Collision Density Statistics

        #region Reference Collision Ratio Statistics

        public override float GetReferenceCollisionPolygonRatioAll()
        {
            return -1;
        }

        public override float GetReferenceCollisionPolygonRatioUnique()
        {
            return -1;
        }

        #endregion // Reference Collision Ratio Statistics

        #region Reference LOD Distance Statistics

        public override float GetReferenceLodDistance()
        {
            return -1;
        }

        #endregion Reference // LOD Distance Statistics

        #endregion // Override Functions
    }
}
