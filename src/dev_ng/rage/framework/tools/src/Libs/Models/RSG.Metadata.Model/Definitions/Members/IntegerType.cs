﻿// --------------------------------------------------------------------------------------------
// <copyright file="IntegerType.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions.Members
{
    /// <summary>
    /// Specifies the different types of integer members the parCodeGen system supports.
    /// </summary>
    public enum IntegerType
    {
        /// <summary>
        /// The integer type is not specified.
        /// </summary>
        Standard,

        /// <summary>
        /// The integer type is not recognised as valid, though it is defined.
        /// </summary>
        Unrecognised,

        /// <summary>
        /// The integer member is representing a colour value.
        /// </summary>
        Colour,
    } // RSG.Metadata.Model.Definitions.Members.IntegerType {Enum}
} // RSG.Metadata.Model.Definitions.Members {Namespace}
