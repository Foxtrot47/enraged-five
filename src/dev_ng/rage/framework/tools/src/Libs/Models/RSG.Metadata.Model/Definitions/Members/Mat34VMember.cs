﻿// --------------------------------------------------------------------------------------------
// <copyright file="Mat34VMember.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions.Members
{
    using System;
    using System.Xml;
    using RSG.Base.Logging;
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// Represents the &lt;Mat34V&gt; member node in the parCodeGen system that can be
    /// instanced in a metadata file.
    /// </summary>
    public class Mat34VMember : BaseMatrixMember, IEquatable<Mat34VMember>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="Mat34VMember"/> class to be one of the
        /// members of the specified structure.
        /// </summary>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        public Mat34VMember(IStructure structure)
            : base(structure)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Mat34VMember"/> class as a copy of the
        /// specified instance.
        /// </summary>
        /// <param name="other">
        /// The instance to copy.
        /// </param>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        public Mat34VMember(Mat34VMember other, IStructure structure)
            : base(other, structure)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Mat34VMember"/> class.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        public Mat34VMember(XmlReader reader, IStructure structure)
            : base(reader, structure)
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the height dimension of the matrix member.
        /// </summary>
        public override int DimensionHeight
        {
            get { return 3; }
        }

        /// <summary>
        /// Gets the width dimension of the matrix member.
        /// </summary>
        public override int DimensionWidth
        {
            get { return 4; }
        }

        /// <summary>
        /// Gets the type name for this member.
        /// </summary>
        public override string TypeName
        {
            get { return "Mat34V"; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new <see cref="Mat34VMember"/> that is a copy of this instance.
        /// </summary>
        /// <returns>
        /// A new <see cref="Mat34VMember"/> that is a copy of this instance.
        /// </returns>
        public new Mat34VMember Clone()
        {
            return new Mat34VMember(this, this.Structure);
        }

        /// <summary>
        /// Creates a <see cref="ITunable"/> object that represents a instance of this member
        /// with it's default value set.
        /// </summary>
        /// <param name="parent">
        /// The tunable parent the new tunable will belong to.
        /// </param>
        /// <returns>
        /// A parCodeGen tunable that represents a instance of this member.
        /// </returns>
        public override ITunable CreateTunable(ITunableParent parent)
        {
            return new Mat34VTunable(this, parent);
        }

        /// <summary>
        /// Creates a <see cref="ITunable"/> object that represents a instance of this member
        /// using the specified Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The Xml.XmlReader that provides the data used to initialise the new tunable.
        /// </param>
        /// <param name="parent">
        /// The tunable parent the new tunable will belong to.
        /// </param>
        /// <param name="log">
        /// A object that is used to log any warnings or errors produced during the parsing of
        /// the specified reader.
        /// </param>
        /// <returns>
        /// A parCodeGen tunable that represents a instance of this member.
        /// </returns>
        public override ITunable CreateTunable(
            XmlReader reader, ITunableParent parent, ILog log)
        {
            return new Mat34VTunable(reader, this, parent, log);
        }

        /// <summary>
        /// Creates a deep copy of the current instance.
        /// </summary>
        /// <returns>
        /// A deep copy of the current System.Object.
        /// </returns>
        public override object DeepClone()
        {
            return this.Clone();
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified
        /// <see cref="Mat34VMember"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public bool Equals(Mat34VMember other)
        {
            if (other == null)
            {
                return false;
            }

            return object.ReferenceEquals(other, this);
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="IMember"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public override bool Equals(IMember other)
        {
            return this.Equals(other as Mat34VMember);
        }
        #endregion Methods
    } // RSG.Metadata.Model.Definitions.Members.Mat34VMember {Class}
} // RSG.Metadata.Model.Definitions.Members {Namespace}
