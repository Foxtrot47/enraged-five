﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.Base.ConfigParser;
using RSG.Model.Common.Map;
using RSG.Base.Threading;
using System.Threading;
using RSG.Base.Logging;
using System.Diagnostics;
using RSG.Statistics.Client.ServiceClients;
using RSG.Statistics.Common.Dto.GameAssetStats;
using RSG.Base.Extensions;
using RSG.Base.Tasks;

namespace RSG.Model.Map3
{
    /// <summary>
    /// 
    /// </summary>
    public class MapStatStreamingCoordinator : StatStreamingCoordinatorBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        protected override string CoordinatorName
        {
            get
            {
                return "Map";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private BuildClient BuildClient
        {
            get
            {
                if (m_buildClient == null)
                {
                    m_buildClient = new BuildClient();
                }
                return m_buildClient;
            }
        }
        private BuildClient m_buildClient;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public MapStatStreamingCoordinator(ConfigGameView gv)
            : base(gv)
        {
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="root"></param>
        /// <param name="statsToLoad"></param>
        /// <param name="async"></param>
        public void LoadStats(IEnumerable<IMapSection> sections, IEnumerable<StreamableStat> requestedStats, bool async)
        {
            // Validate the arguments.
            if (!sections.Any() || !requestedStats.Any())
            {
                return;
            }
            StreamableStatUtils.ValidateStatsAreOfType(requestedStats, sections.First().GetType());

            // Determine where we'll need to get the data from.
            IMapHierarchy ownerHierarchy = sections.First().MapHierarchy;

            if (ownerHierarchy is ExportDataMapHierarchy)
            {
                LoadExportStats(sections, requestedStats, async);
            }
            else if (ownerHierarchy is SingleBuildMapHierarchy)
            {
                LoadDatabaseStats(sections, requestedStats, async);
            }
            else
            {
                Debug.Assert(false, "Unknown map hierarchy type encountered.");
                throw new NotSupportedException("Unknown map hierarchy type encountered.");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="archetypes"></param>
        /// <param name="statsToLoad"></param>
        /// <param name="async"></param>
        public void LoadStats(IEnumerable<IMapArchetype> archetypes, IEnumerable<StreamableStat> requestedStats, bool async)
        {
            // Validate the arguments.
            if (!archetypes.Any() || !requestedStats.Any())
            {
                return;
            }
            StreamableStatUtils.ValidateStatsAreOfType(requestedStats, archetypes.First().GetType());

            // Determine where we'll need to get the data from.
            IMapHierarchy ownerHierarchy = archetypes.First().ParentSection.MapHierarchy;

            if (ownerHierarchy is ExportDataMapHierarchy)
            {
                LoadExportStats(archetypes, requestedStats, async);
            }
            else if (ownerHierarchy is SingleBuildMapHierarchy)
            {
                LoadDatabaseStats(archetypes, requestedStats, async);
            }
            else
            {
                Debug.Assert(false, "Unknown map hierarchy type encountered.");
                throw new NotSupportedException("Unknown map hierarchy type encountered.");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="drawables"></param>
        /// <param name="statsToLoad"></param>
        /// <param name="async"></param>
        public void LoadStats(IEnumerable<IDrawableArchetype> drawables, IEnumerable<StreamableStat> requestedStats, bool async)
        {
            // Validate the arguments.
            if (!drawables.Any() || !requestedStats.Any())
            {
                return;
            }
            StreamableStatUtils.ValidateStatsAreOfType(requestedStats, drawables.First().GetType());

            // Determine where we'll need to get the data from.
            IMapHierarchy ownerHierarchy = drawables.First().ParentSection.MapHierarchy;

            if (ownerHierarchy is ExportDataMapHierarchy)
            {
                LoadExportStats(drawables, requestedStats, async);
            }
            else if (ownerHierarchy is SingleBuildMapHierarchy)
            {
                LoadDatabaseStats(drawables, requestedStats, async);
            }
            else
            {
                Debug.Assert(false, "Unknown map hierarchy type encountered.");
                throw new NotSupportedException("Unknown map hierarchy type encountered.");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fragment"></param>
        /// <param name="statsToLoad"></param>
        /// <param name="async"></param>
        public void LoadStats(IEnumerable<IFragmentArchetype> fragments, IEnumerable<StreamableStat> requestedStats, bool async)
        {
            // Validate the arguments.
            if (!fragments.Any() || !requestedStats.Any())
            {
                return;
            }
            StreamableStatUtils.ValidateStatsAreOfType(requestedStats, fragments.First().GetType());

            // Determine where we'll need to get the data from.
            IMapHierarchy ownerHierarchy = fragments.First().ParentSection.MapHierarchy;
            if (ownerHierarchy is ExportDataMapHierarchy)
            {
                LoadExportStats(fragments, requestedStats, async);
            }
            else if (ownerHierarchy is SingleBuildMapHierarchy)
            {
                LoadDatabaseStats(fragments, requestedStats, async);
            }
            else
            {
                throw new NotSupportedException("Unknown weapon collection type encountered.");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="drawables"></param>
        /// <param name="statsToLoad"></param>
        /// <param name="async"></param>
        public void LoadStats(IEnumerable<IInteriorArchetype> interiors, IEnumerable<StreamableStat> requestedStats, bool async)
        {
            // Validate the arguments.
            if (!interiors.Any() || !requestedStats.Any())
            {
                return;
            }
            StreamableStatUtils.ValidateStatsAreOfType(requestedStats, interiors.First().GetType());

            // Determine where we'll need to get the data from.
            IMapHierarchy ownerHierarchy = interiors.First().ParentSection.MapHierarchy;

            if (ownerHierarchy is ExportDataMapHierarchy)
            {
                LoadExportStats(interiors, requestedStats, async);
            }
            else if (ownerHierarchy is SingleBuildMapHierarchy)
            {
                LoadDatabaseStats(interiors, requestedStats, async);
            }
            else
            {
                Debug.Assert(false, "Unknown map hierarchy type encountered.");
                throw new NotSupportedException("Unknown map hierarchy type encountered.");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="drawables"></param>
        /// <param name="statsToLoad"></param>
        /// <param name="async"></param>
        public void LoadStats(IEnumerable<IStatedAnimArchetype> statedAnims, IEnumerable<StreamableStat> requestedStats, bool async)
        {
            // Validate the arguments.
            if (!statedAnims.Any() || !requestedStats.Any())
            {
                return;
            }
            StreamableStatUtils.ValidateStatsAreOfType(requestedStats, statedAnims.First().GetType());

            // Determine where we'll need to get the data from.
            IMapHierarchy ownerHierarchy = statedAnims.First().ParentSection.MapHierarchy;

            if (ownerHierarchy is ExportDataMapHierarchy)
            {
                LoadExportStats(statedAnims, requestedStats, async);
            }
            else if (ownerHierarchy is SingleBuildMapHierarchy)
            {
                LoadDatabaseStats(statedAnims, requestedStats, async);
            }
            else
            {
                Debug.Assert(false, "Unknown map hierarchy type encountered.");
                throw new NotSupportedException("Unknown map hierarchy type encountered.");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="drawables"></param>
        /// <param name="statsToLoad"></param>
        /// <param name="async"></param>
        public void LoadStats(IEnumerable<IRoom> rooms, IEnumerable<StreamableStat> requestedStats, bool async)
        {
            // Validate the arguments.
            if (!rooms.Any() || !requestedStats.Any())
            {
                return;
            }
            StreamableStatUtils.ValidateStatsAreOfType(requestedStats, rooms.First().GetType());

            // Determine where we'll need to get the data from.
            IMapHierarchy ownerHierarchy = rooms.First().ParentArchetype.ParentSection.MapHierarchy;

            if (ownerHierarchy is ExportDataMapHierarchy)
            {
                LoadExportStats(rooms, requestedStats, async);
            }
            else if (ownerHierarchy is SingleBuildMapHierarchy)
            {
                LoadDatabaseStats(rooms, requestedStats, async);
            }
            else
            {
                Debug.Assert(false, "Unknown map hierarchy type encountered.");
                throw new NotSupportedException("Unknown map hierarchy type encountered.");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="drawables"></param>
        /// <param name="statsToLoad"></param>
        /// <param name="async"></param>
        public void LoadStats(IEnumerable<IEntity> entities, IEnumerable<StreamableStat> requestedStats, bool async)
        {
            // Validate the arguments.
            if (!entities.Any() || !requestedStats.Any())
            {
                return;
            }
            StreamableStatUtils.ValidateStatsAreOfType(requestedStats, entities.First().GetType());

            // Determine where we'll need to get the data from.
            IMapHierarchy ownerHierarchy = entities.First().ContainingSection.MapHierarchy;

            if (ownerHierarchy is ExportDataMapHierarchy)
            {
                LoadExportStats(entities, requestedStats, async);
            }
            else if (ownerHierarchy is SingleBuildMapHierarchy)
            {
                LoadDatabaseStats(entities, requestedStats, async);
            }
            else
            {
                Debug.Assert(false, "Unknown map hierarchy type encountered.");
                throw new NotSupportedException("Unknown map hierarchy type encountered.");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sections"></param>
        /// <param name="requestedStats"></param>
        /// <returns></returns>
        public ITask CreateLoadTask(IEnumerable<IMapSection> sections, IEnumerable<StreamableStat> requestedStats)
        {
            // Validate the arguments.
            if (!sections.Any() || !requestedStats.Any())
            {
                return null;
            }

            // Determine where we'll need to get the data from.
            IMapHierarchy ownerHierarchy = sections.First().MapHierarchy;

            if (ownerHierarchy is ExportDataMapHierarchy)
            {
                // Only request section stats.
                //TODO: Need to take the stat dependency system into account here.
                IEnumerable<StreamableStat> sectionStats = StreamableStatUtils.GetValuesAssignedToProperties(typeof(MapSection));
                return CreateExportStatTask(sections, requestedStats.Intersect(sectionStats));
            }
            else if (ownerHierarchy is SingleBuildMapHierarchy)
            {
                return CreateDatabaseTask(sections, requestedStats);
            }
            else
            {
                Debug.Assert(false, "Unknown map hierarchy type encountered.");
                throw new NotSupportedException("Unknown map hierarchy type encountered.");
            }
        }
        #endregion // Public Methods

        #region StatStreamingCoordinatorBase Overrides
        /// <summary>
        /// 
        /// </summary>
        protected override void ProcessTasks()
        {
            Thread.CurrentThread.Name = String.Format("{0} Stat Streaming Coordinator", CoordinatorName);

            foreach (MapTask task in TaskQueue.GetConsumingEnumerable())
            {
                try
                {
                    // If the task can't run stick it back to the end of the queue.
                    if (!task.CanRunTask)
                    {
                        TaskQueue.Add(task);
                    }
                    else
                    {
                        task.Start();
                        task.Wait();
                        task.Dispose();
                    }

                    Thread.Sleep(30);
                }
                catch (System.Exception ex)
                {
                    Log.Log__Exception(ex, "Unhandled thread exception");
                }
            }
        }
        #endregion // StatStreamingCoordinatorBase Overrides

        #region Private Methods
        #region Export Data
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sections"></param>
        /// <param name="statsToLoad"></param>
        /// <param name="async"></param>
        private void LoadExportStats(IEnumerable<IMapSection> sections, IEnumerable<StreamableStat> requestedStats, bool async)
        {
            // If we've requested aggregate stats, make sure that the prop group sections have their data loaded first.
            if (requestedStats.Contains(StreamableMapSectionStat.AggregatedStats))
            {
                IEnumerable<IMapSection> propSections = sections.Where(item => item.PropGroup != null).Select(item => item.PropGroup);
                foreach (IMapSection section in propSections)
                {
                    StreamableStat[] propStatsToLoad =
                        new StreamableStat[]
                        {
                            StreamableMapSectionStat.ExportArchetypes,
                            StreamableMapSectionStat.ExportEntities,
                            StreamableMapSectionStat.Archetypes,
                            StreamableMapSectionStat.Entities
                        };

                    LoadExportStats(section, propStatsToLoad, async);
                }
            }

            foreach (IMapSection section in sections)
            {
                LoadExportStats(section, requestedStats, async);
            }
        }

        /// <summary>
        /// Load statistics from the export data for a single map section
        /// </summary>
        /// <param name="section"></param>
        /// <param name="statsToLoad"></param>
        /// <param name="async"></param>
        private void LoadExportStats(IMapSection section, IEnumerable<StreamableStat> requestedStats, bool async)
        {
            // Check whether the stats are already loaded
            if (!AreRequestedStatsLoaded(section, requestedStats))
            {
                IList<StreamableStat> statsToLoad = StatsRequiredForObject(section, requestedStats).ToList();

                // Create a task for reading in the export data
                string taskName = String.Format("Load export data map section stats for {0}", section.Name);

                // Check what sort of stats we should be loading.
                StreamableStat[] basicStats = new StreamableStat[] { StreamableMapSectionStat.ExportArchetypes,
                                                                     StreamableMapSectionStat.ExportEntities,
                                                                     StreamableMapSectionStat.DCCSourceFilename,
                                                                     StreamableMapSectionStat.ExportZipFilepath };
                StreamableStat platformStat = StreamableMapSectionStat.PlatformStats;
                StreamableStat[] collisionStats = new StreamableStat[] { StreamableMapSectionStat.CollisionPolygonCount, StreamableMapSectionStat.CollisionPolygonCountBreakdown };
                StreamableStat aggregatedStats = StreamableMapSectionStat.AggregatedStats;

                // Platform stats require basic stats.
                if (statsToLoad.Intersect(basicStats).Any() || statsToLoad.Contains(platformStat) || statsToLoad.Contains(aggregatedStats))
                {
                    MapTask loadStatsTask = new MapTask(section.MapHierarchy.OwningLevel, String.Format("{0} - Basic", taskName), (obj) => { LoadBasicStatsFromExportData((IMapSection)obj); }, section, TaskCancellationSouce.Token);
                    ExecuteTask(loadStatsTask, async);
                }
                if (statsToLoad.Intersect(collisionStats).Any())
                {
                    MapTask loadStatsTask = new MapTask(section.MapHierarchy.OwningLevel, String.Format("{0} - Processed", taskName), (obj) => { LoadCollisionStatsFromExportData((IMapSection)obj); }, section, TaskCancellationSouce.Token);
                    ExecuteTask(loadStatsTask, async);
                }
                if (statsToLoad.Contains(platformStat))
                {
                    MapTask loadStatsTask = new MapTask(section.MapHierarchy.OwningLevel, String.Format("{0} - Platform", taskName), (obj) => { LoadPlatformStatsFromExportData((IMapSection)obj); }, section, TaskCancellationSouce.Token);
                    ExecuteTask(loadStatsTask, async);
                }

                // Check if anything else needs to be loaded.
                IList<StreamableStat> specialStats = new List<StreamableStat>();
                specialStats.AddRange(basicStats);
                specialStats.AddRange(collisionStats);
                specialStats.Add(platformStat);
                specialStats.Add(aggregatedStats);

                IEnumerable<StreamableStat> allMapSectionStats = StreamableStatUtils.GetValuesAssignedToProperties(typeof(MapSection));

                if (statsToLoad.Except(specialStats).Any() ||
                    (statsToLoad.Contains(aggregatedStats) && !AreRequestedStatsLoaded(section, allMapSectionStats.Except(specialStats))))
                {
                    MapTask loadStatsTask = new MapTask(section.MapHierarchy.OwningLevel, String.Format("{0} - ExportData", taskName), (obj) => { LoadStatsFromExportData((IMapSection)obj); }, section, TaskCancellationSouce.Token);
                    ExecuteTask(loadStatsTask, async);
                }

                if (statsToLoad.Contains(aggregatedStats))
                {
                    MapTask loadStatsTask = new MapTask(section.MapHierarchy.OwningLevel, String.Format("{0} - Aggregated", taskName), (obj) => { LoadAggregatedStatsFromExportData((IMapSection)obj); }, section, TaskCancellationSouce.Token);
                    ExecuteTask(loadStatsTask, async);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        private void LoadBasicStatsFromExportData(IMapSection section)
        {
            ExportDataMapHierarchy root = (ExportDataMapHierarchy)section.MapHierarchy;
            (section as MapSection).LoadBasicStatsFromExportData(root.GameView);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        private void LoadStatsFromExportData(IMapSection section)
        {
            ExportDataMapHierarchy root = (ExportDataMapHierarchy)section.MapHierarchy;
            (section as MapSection).LoadStatsFromExportData(root.GameView);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        private void LoadPlatformStatsFromExportData(IMapSection section)
        {
            ExportDataMapHierarchy root = (ExportDataMapHierarchy)section.MapHierarchy;
            (section as MapSection).LoadPlatformStatsFromExportData(root.Config);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        private void LoadCollisionStatsFromExportData(IMapSection section)
        {
            ExportDataMapHierarchy root = (ExportDataMapHierarchy)section.MapHierarchy;
            (section as MapSection).LoadCollisionStatsFromProcessedData(root.GameView);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        private void LoadAggregatedStatsFromExportData(IMapSection section)
        {
            (section as MapSection).GatherAggregatedStatistics();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fragment"></param>
        /// <param name="statsToLoad"></param>
        /// <param name="async"></param>
        private void LoadExportStats(IEnumerable<IMapArchetype> archetypes, IEnumerable<StreamableStat> requestedStats, bool async)
        {
            // Nothing to do (for now).
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fragment"></param>
        /// <param name="statsToLoad"></param>
        /// <param name="async"></param>
        private void LoadExportStats(IEnumerable<IRoom> rooms, IEnumerable<StreamableStat> requestedStats, bool async)
        {
            // Nothing to do (for now).
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fragment"></param>
        /// <param name="statsToLoad"></param>
        /// <param name="async"></param>
        private void LoadExportStats(IEnumerable<IEntity> entities, IEnumerable<StreamableStat> requestedStats, bool async)
        {
            // Nothing to do (for now).
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sections"></param>
        /// <param name="requestedStats"></param>
        /// <returns></returns>
        private ITask CreateExportStatTask(IEnumerable<IMapSection> sections, IEnumerable<StreamableStat> requestedStats)
        {
            CompositeTask loadTask = new CompositeTask("Load Data");

            // If we've requested aggregate stats, make sure that the prop group sections have their data loaded first.
            IEnumerable<IMapSection> propSections = sections.Where(item => item.PropGroup != null).Select(item => item.PropGroup);

            if (requestedStats.Contains(StreamableMapSectionStat.AggregatedStats))
            {
                foreach (IMapSection section in propSections)
                {
                    StreamableStat[] propStatsToLoad =
                        new StreamableStat[]
                        {
                            StreamableMapSectionStat.ExportArchetypes,
                            StreamableMapSectionStat.ExportEntities,
                            StreamableMapSectionStat.Archetypes,
                            StreamableMapSectionStat.Entities
                        };

                    // Only add a sub task if the returned composite task needs to do anything.
                    CompositeTask compTask = CreateExportStatTask(section, propStatsToLoad);
                    if (compTask.SubTasks.Count > 0)
                    {
                        loadTask.AddSubTask(CreateExportStatTask(section, propStatsToLoad));
                    }
                }
            }

            // Crete a sub task for each of the sections.
            foreach (IMapSection section in sections)
            {
                // Only add a sub task if the returned composite task needs to do anything.
                CompositeTask compTask = CreateExportStatTask(section, requestedStats);
                if (compTask.SubTasks.Count > 0)
                {
                    loadTask.AddSubTask(compTask);
                }
            }

            // Only return a valid task if there is data to load.
            if (loadTask.SubTasks.Count > 0)
            {
                return loadTask;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Load statistics from the export data for a single map section.
        /// </summary>
        /// <param name="section"></param>
        /// <param name="statsToLoad"></param>
        /// <param name="async"></param>
        private CompositeTask CreateExportStatTask(IMapSection section, IEnumerable<StreamableStat> requestedStats)
        {
            CompositeTask sectionLoadTask = new CompositeTask(String.Format("Loading {0} Data", section.Name));

            // Check whether the stats are already loaded
            if (!AreRequestedStatsLoaded(section, requestedStats))
            {
                IList<StreamableStat> statsToLoad = StatsRequiredForObject(section, requestedStats).ToList();

                // Create a task for reading in the export data
                string taskName = String.Format("Load export data map section stats for {0}", section.Name);

                // Check what sort of stats we should be loading.
                StreamableStat[] basicStats = new StreamableStat[] { StreamableMapSectionStat.ExportArchetypes,
                                                                     StreamableMapSectionStat.ExportEntities,
                                                                     StreamableMapSectionStat.DCCSourceFilename,
                                                                     StreamableMapSectionStat.ExportZipFilepath };
                StreamableStat platformStat = StreamableMapSectionStat.PlatformStats;
                StreamableStat[] collisionStats = new StreamableStat[] { StreamableMapSectionStat.CollisionPolygonCount, StreamableMapSectionStat.CollisionPolygonCountBreakdown };
                StreamableStat aggregatedStats = StreamableMapSectionStat.AggregatedStats;

                // Platform stats require basic stats.
                if (statsToLoad.Intersect(basicStats).Any() || statsToLoad.Contains(platformStat) || statsToLoad.Contains(aggregatedStats))
                {
                    ActionTask subTask = new ActionTask("Basic Data", (ctx, prg) => LoadBasicStatsFromExportData(ctx, prg, section));
                    sectionLoadTask.AddSubTask(subTask);
                }
                if (statsToLoad.Intersect(collisionStats).Any())
                {
                    ActionTask subTask = new ActionTask("Processed Data", (ctx, prg) => LoadCollisionStatsFromExportData(ctx, prg, section));
                    sectionLoadTask.AddSubTask(subTask);
                }
                if (statsToLoad.Contains(platformStat))
                {
                    ActionTask subTask = new ActionTask("Platform Data", (ctx, prg) => LoadPlatformStatsFromExportData(ctx, prg, section));
                    sectionLoadTask.AddSubTask(subTask);
                }

                // Check if anything else needs to be loaded.
                IList<StreamableStat> specialStats = new List<StreamableStat>();
                specialStats.AddRange(basicStats);
                specialStats.AddRange(collisionStats);
                specialStats.Add(platformStat);
                specialStats.Add(aggregatedStats);

                IEnumerable<StreamableStat> allMapSectionStats = StreamableStatUtils.GetValuesAssignedToProperties(typeof(MapSection));

                if (statsToLoad.Except(specialStats).Any() ||
                    (statsToLoad.Contains(aggregatedStats) && !AreRequestedStatsLoaded(section, allMapSectionStats.Except(specialStats))))
                {
                    ActionTask subTask = new ActionTask("Export Data", (ctx, prg) => LoadStatsFromExportData(ctx, prg, section));
                    sectionLoadTask.AddSubTask(subTask);
                }

                if (statsToLoad.Contains(aggregatedStats))
                {
                    ActionTask subTask = new ActionTask("Aggregated Data", (ctx, prg) => LoadAggregatedStatsFromExportData(ctx, prg, section));
                    sectionLoadTask.AddSubTask(subTask);
                }
            }

            return sectionLoadTask;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        private void LoadBasicStatsFromExportData(ITaskContext context, IProgress<TaskProgress> progress, IMapSection section)
        {
            StreamableStat[] basicStats = new StreamableStat[] { StreamableMapSectionStat.ExportArchetypes,
                                                                     StreamableMapSectionStat.ExportEntities,
                                                                     StreamableMapSectionStat.DCCSourceFilename,
                                                                     StreamableMapSectionStat.ExportZipFilepath };

            if (!AreRequestedStatsLoaded(section, basicStats))
            {
                ExportDataMapHierarchy root = (ExportDataMapHierarchy)section.MapHierarchy;
                (section as MapSection).LoadBasicStatsFromExportData(root.GameView);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        private void LoadStatsFromExportData(ITaskContext context, IProgress<TaskProgress> progress, IMapSection section)
        {
            StreamableStat[] basicStats = new StreamableStat[] { StreamableMapSectionStat.ExportArchetypes,
                                                                     StreamableMapSectionStat.ExportEntities,
                                                                     StreamableMapSectionStat.DCCSourceFilename,
                                                                     StreamableMapSectionStat.ExportZipFilepath };
            StreamableStat platformStat = StreamableMapSectionStat.PlatformStats;
            StreamableStat[] collisionStats = new StreamableStat[] { StreamableMapSectionStat.CollisionPolygonCount, StreamableMapSectionStat.CollisionPolygonCountBreakdown };
            StreamableStat aggregatedStats = StreamableMapSectionStat.AggregatedStats;

            IList<StreamableStat> specialStats = new List<StreamableStat>();
            specialStats.AddRange(basicStats);
            specialStats.AddRange(collisionStats);
            specialStats.Add(platformStat);
            specialStats.Add(aggregatedStats);

            IEnumerable<StreamableStat> allMapSectionStats = StreamableStatUtils.GetValuesAssignedToProperties(typeof(MapSection));

            if (!AreRequestedStatsLoaded(section, allMapSectionStats.Except(specialStats)))
            {
                ExportDataMapHierarchy root = (ExportDataMapHierarchy)section.MapHierarchy;
                (section as MapSection).LoadStatsFromExportData(root.GameView);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        private void LoadPlatformStatsFromExportData(ITaskContext context, IProgress<TaskProgress> progress, IMapSection section)
        {
            if (!AreRequestedStatsLoaded(section, new StreamableStat[] { StreamableMapSectionStat.PlatformStats }))
            {
                ExportDataMapHierarchy root = (ExportDataMapHierarchy)section.MapHierarchy;
                (section as MapSection).LoadPlatformStatsFromExportData(root.Config);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        private void LoadCollisionStatsFromExportData(ITaskContext context, IProgress<TaskProgress> progress, IMapSection section)
        {
            StreamableStat[] collisionStats = new StreamableStat[] { StreamableMapSectionStat.CollisionPolygonCount, StreamableMapSectionStat.CollisionPolygonCountBreakdown };

            if (!AreRequestedStatsLoaded(section, collisionStats))
            {
                ExportDataMapHierarchy root = (ExportDataMapHierarchy)section.MapHierarchy;
                (section as MapSection).LoadCollisionStatsFromProcessedData(root.GameView);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        private void LoadAggregatedStatsFromExportData(ITaskContext context, IProgress<TaskProgress> progress, IMapSection section)
        {
            if (!AreRequestedStatsLoaded(section, new StreamableStat[] { StreamableMapSectionStat.AggregatedStats }))
            {
                (section as MapSection).GatherAggregatedStatistics();
            }
        }
        #endregion // Export Data

        #region Database Data
        /// <summary>
        /// Load specific stats for a list of map section.
        /// </summary>
        /// <param name="sections"></param>
        /// <param name="statsToLoad"></param>
        /// <param name="async"></param>
        private void LoadDatabaseStats(IEnumerable<IMapSection> sections, IEnumerable<StreamableStat> requestedStats, bool async)
        {
            // Get the list of sections that actually need to load the data.
            IList<IStreamableObject> sectionsNeedingData = new List<IStreamableObject>();
            foreach (IMapSection section in sections)
            {
                if (!AreRequestedStatsLoaded(section, requestedStats))
                {
                    sectionsNeedingData.Add(section);
                }
            }

            // Check if we need to load any data.
            if (sectionsNeedingData.Any())
            {
                string taskName = String.Format("Load database data map section stats for {0} sections.", sectionsNeedingData.Count);

                StreamableObjectLoadContext context = new StreamableObjectLoadContext { Objects = sectionsNeedingData, Stats = requestedStats.ToList() };
                MapTask loadStatsTask = new MapTask(sections.First().MapHierarchy.OwningLevel, String.Format("{0} - DatabaseData", taskName), (obj) => { LoadSectionStatsFromDatabase((StreamableObjectLoadContext)obj); }, context, TaskCancellationSouce.Token);
                ExecuteTask(loadStatsTask, async);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sections"></param>
        private void LoadSectionStatsFromDatabase(StreamableObjectLoadContext context)
        {
            // Make sure what we have requested is valid.
            IList<IMapSection> sections = context.Objects.Cast<IMapSection>().ToList();
            IList<StreamableStat> requestedStats = context.Stats;
            Debug.Assert(sections.Any() && requestedStats.Any(), "Either sections or stats don't contain any items.");

            // Get the root of the hierarchy (for the build/level)
            SingleBuildMapHierarchy root = (SingleBuildMapHierarchy)sections.First().MapHierarchy;

            // Construct the request object and query the database for the data.
            StreamableStatFetchDto fetchDto = new StreamableStatFetchDto(sections.Select(item => item.Hash).ToList(), requestedStats.ToList());
            IList<MapSectionStatDto> sectionDtos = BuildClient.GetMultipleMapSectionStats(root.BuildIdentifier, root.OwningLevel.Hash.ToString(), fetchDto);

            // Match up the retrieved data with the sections that were requested to allow them to read in the relevant data.
            IDictionary<uint, IMapSection> requestedSectionLookup = sections.ToDictionary(item => item.Hash);

            foreach (MapSectionStatDto sectionDto in sectionDtos)
            {
                if (requestedSectionLookup.ContainsKey(sectionDto.Identifier))
                {
                    IMapSection section = requestedSectionLookup[sectionDto.Identifier];
                    (section as MapSection).LoadStatsFromDatabase(sectionDto, requestedStats);
                }
                else
                {
                    Debug.Assert(false, "Received section stat information for a section that wasn't requested.");
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fragment"></param>
        /// <param name="statsToLoad"></param>
        /// <param name="async"></param>
        private void LoadDatabaseStats(IEnumerable<IMapArchetype> archetypes, IEnumerable<StreamableStat> requestedStats, bool async)
        {
            // Get the list of sections that actually need to load the data.
            IList<IStreamableObject> archetypesNeedingData = new List<IStreamableObject>();
            foreach (IMapArchetype archetype in archetypes)
            {
                if (!AreRequestedStatsLoaded(archetype, requestedStats))
                {
                    archetypesNeedingData.Add(archetype);
                }
            }

            // Check if we need to load any data.
            if (archetypesNeedingData.Any())
            {
                string taskName = String.Format("Load database data map archetype stats for {0} archetypes.", archetypesNeedingData.Count);

                StreamableObjectLoadContext context = new StreamableObjectLoadContext { Objects = archetypesNeedingData, Stats = requestedStats.ToList() };
                MapTask loadStatsTask = new MapTask(archetypes.First().ParentSection.MapHierarchy.OwningLevel, String.Format("{0} - DatabaseData", taskName), (obj) => { LoadMapArchetypeStatsFromDatabase((StreamableObjectLoadContext)obj); }, context, TaskCancellationSouce.Token);
                ExecuteTask(loadStatsTask, async);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void LoadMapArchetypeStatsFromDatabase(StreamableObjectLoadContext context)
        {
            // Make sure what we have requested is valid.
            IList<IMapArchetype> archetypes = context.Objects.Cast<IMapArchetype>().ToList();
            IList<StreamableStat> requestedStats = context.Stats;
            Debug.Assert(archetypes.Any() && requestedStats.Any(), "Either archetypes or stats don't contain any items.");

            // Get the root of the hierarchy (for the build/level)
            SingleBuildMapHierarchy root = (SingleBuildMapHierarchy)archetypes.First().ParentSection.MapHierarchy;

            // Construct the request object and query the database for the data.
            StreamableStatFetchDto fetchDto = new StreamableStatFetchDto(archetypes.Select(item => item.Hash).ToList(), requestedStats.ToList());
            IList<ArchetypeStatDto> archetypeDtos = BuildClient.GetMultipleArchetypeStats(root.BuildIdentifier, root.OwningLevel.Hash.ToString(), fetchDto);

            // Match up the retrieved data with the sections that were requested to allow them to read in the relevant data.
            IDictionary<uint, IMapArchetype> requestedArchetypeLookup = archetypes.ToDictionary(item => item.Hash);

            foreach (ArchetypeStatDto archetypeDto in archetypeDtos)
            {
                if (requestedArchetypeLookup.ContainsKey(archetypeDto.Identifier))
                {
                    IMapArchetype archetype = requestedArchetypeLookup[archetypeDto.Identifier];
                    (archetype as MapArchetypeBase).LoadStatsFromDatabase(archetypeDto, requestedStats);
                }
                else
                {
                    Debug.Assert(false, "Received archetype stat information for an archetype that wasn't requested.");
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fragment"></param>
        /// <param name="statsToLoad"></param>
        /// <param name="async"></param>
        private void LoadDatabaseStats(IEnumerable<IRoom> rooms, IEnumerable<StreamableStat> requestedStats, bool async)
        {
            // Get the list of sections that actually need to load the data.
            IList<IStreamableObject> roomsNeedingData = new List<IStreamableObject>();
            foreach (IRoom room in rooms)
            {
                if (!AreRequestedStatsLoaded(room, requestedStats))
                {
                    roomsNeedingData.Add(room);
                }
            }

            // Check if we need to load any data.
            if (roomsNeedingData.Any())
            {
                string taskName = String.Format("Load database data room stats for {0} rooms.", roomsNeedingData.Count);

                StreamableObjectLoadContext context = new StreamableObjectLoadContext { Objects = roomsNeedingData, Stats = requestedStats.ToList() };
                MapTask loadStatsTask = new MapTask(rooms.First().ParentArchetype.ParentSection.MapHierarchy.OwningLevel, String.Format("{0} - DatabaseData", taskName), (obj) => { LoadRoomStatsFromDatabase((StreamableObjectLoadContext)obj); }, context, TaskCancellationSouce.Token);
                ExecuteTask(loadStatsTask, async);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sections"></param>
        private void LoadRoomStatsFromDatabase(StreamableObjectLoadContext context)
        {
            // Make sure what we have requested is valid.
            IList<IRoom> rooms = context.Objects.Cast<IRoom>().ToList();
            IList<StreamableStat> requestedStats = context.Stats;
            Debug.Assert(rooms.Any() && requestedStats.Any(), "Either rooms or stats don't contain any items.");

            // Get the root of the hierarchy (for the build/level)
            SingleBuildMapHierarchy root = (SingleBuildMapHierarchy)rooms.First().ParentArchetype.ParentSection.MapHierarchy;

            // Construct the request object and query the database for the data.
            StreamableStatFetchDto fetchDto = new StreamableStatFetchDto(rooms.Select(item => item.Hash).ToList(), requestedStats.ToList());
            IList<RoomStatDto> roomDtos = BuildClient.GetMultipleRoomStats(root.BuildIdentifier, root.OwningLevel.Hash.ToString(), fetchDto);

            // Match up the retrieved data with the sections that were requested to allow them to read in the relevant data.
            IDictionary<uint, IRoom> requestedRoomLookup = rooms.ToDictionary(item => item.Hash);

            foreach (RoomStatDto roomDto in roomDtos)
            {
                if (requestedRoomLookup.ContainsKey(roomDto.Identifier))
                {
                    IRoom room = requestedRoomLookup[roomDto.Identifier];
                    (room as Room).LoadStatsFromDatabase(roomDto, requestedStats);
                }
                else
                {
                    Debug.Assert(false, "Received room stat information for a room that wasn't requested.");
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fragment"></param>
        /// <param name="statsToLoad"></param>
        /// <param name="async"></param>
        private void LoadDatabaseStats(IEnumerable<IEntity> entities, IEnumerable<StreamableStat> requestedStats, bool async)
        {
            // Get the list of sections that actually need to load the data.
            IList<IStreamableObject> entitiesNeedingData = new List<IStreamableObject>();
            foreach (IEntity entity in entities)
            {
                if (!AreRequestedStatsLoaded(entity, requestedStats))
                {
                    entitiesNeedingData.Add(entity);
                }
            }

            // Check if we need to load any data.
            if (entitiesNeedingData.Any())
            {
                string taskName = String.Format("Load database data entity stats for {0} entities.", entitiesNeedingData.Count);

                StreamableObjectLoadContext context = new StreamableObjectLoadContext { Objects = entitiesNeedingData, Stats = requestedStats.ToList() };
                MapTask loadStatsTask = new MapTask(entities.First().ContainingSection.MapHierarchy.OwningLevel, String.Format("{0} - DatabaseData", taskName), (obj) => { LoadEntityStatsFromDatabase((StreamableObjectLoadContext)obj); }, context, TaskCancellationSouce.Token);
                ExecuteTask(loadStatsTask, async);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sections"></param>
        private void LoadEntityStatsFromDatabase(StreamableObjectLoadContext context)
        {
            // Make sure what we have requested is valid.
            IList<IEntity> entities = context.Objects.Cast<IEntity>().ToList();
            IList<StreamableStat> requestedStats = context.Stats;
            Debug.Assert(entities.Any() && requestedStats.Any(), "Either entities or stats don't contain any items.");

            // Get the root of the hierarchy (for the build/level)
            SingleBuildMapHierarchy root = (SingleBuildMapHierarchy)entities.First().ContainingSection.MapHierarchy;

            // Construct the request object and query the database for the data.
            StreamableStatFetchDto fetchDto = new StreamableStatFetchDto(entities.Select(item => item.Hash).ToList(), requestedStats.ToList());
            IList<EntityStatDto> entityDtos = BuildClient.GetMultipleEntityStats(root.BuildIdentifier, root.OwningLevel.Hash.ToString(), fetchDto);

            // Match up the retrieved data with the sections that were requested to allow them to read in the relevant data.
            IDictionary<uint, IEntity> requestedEntityLookup = entities.ToDictionary(item => item.Hash);

            foreach (EntityStatDto entityDto in entityDtos)
            {
                if (requestedEntityLookup.ContainsKey(entityDto.Identifier))
                {
                    IEntity entity = requestedEntityLookup[entityDto.Identifier];
                    (entity as Entity).LoadStatsFromDatabase(entityDto, requestedStats);
                }
                else
                {
                    Debug.Assert(false, "Received entity stat information for a entity that wasn't requested.");
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sections"></param>
        /// <param name="requestedStats"></param>
        /// <returns></returns>
        private ITask CreateDatabaseTask(IEnumerable<IMapSection> sections, IEnumerable<StreamableStat> requestedStats)
        {
            StreamableObjectLoadContext context = new StreamableObjectLoadContext { Objects = sections.Cast<IStreamableObject>().ToList(), Stats = requestedStats.ToList() };

            ActionTask loadTask = new ActionTask(String.Format("Loading data for {0} sections.", sections.Count()));
            loadTask.ExecuteAction = (ctx, prg) => LoadSectionDatabaseStats(context);
            return loadTask;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void LoadSectionDatabaseStats(StreamableObjectLoadContext context)
        {
            IList<IMapSection> sections = context.Objects.Cast<IMapSection>().ToList();
            IList<StreamableStat> requestedStats = context.Stats;
            Debug.Assert(sections.Any() && requestedStats.Any(), "Either sections or stats don't contain any items.");
         
            // Have we been asked for section stats?   
            IEnumerable<StreamableStat> sectionStats = StreamableStatUtils.GetValuesAssignedToProperties(typeof(MapSection));
            IEnumerable<StreamableStat> requestedSectionStats = requestedStats.Intersect(sectionStats);
            if (requestedSectionStats.Any())
            {
                IList<IStreamableObject> sectionsNeedingData = new List<IStreamableObject>();
                foreach (IMapSection section in sections)
                {
                    if (!AreRequestedStatsLoaded(section, requestedSectionStats))
                    {
                        sectionsNeedingData.Add(section);
                    }
                }
                LoadSectionStatsFromDatabase(new StreamableObjectLoadContext { Objects = sectionsNeedingData, Stats = requestedSectionStats.ToList() });
            }

            // Have any archetype stats been requested?
            IEnumerable<StreamableStat> drawableStats = StreamableStatUtils.GetValuesAssignedToProperties(typeof(DrawableArchetype));
            IEnumerable<StreamableStat> requestedDrawableStats = requestedStats.Intersect(drawableStats);
            if (requestedDrawableStats.Any())
            {
                IList<IStreamableObject> drawablesNeedingData = new List<IStreamableObject>();
                foreach (IMapSection section in sections)
                {
                    foreach (IDrawableArchetype drawable in section.Archetypes.OfType<IDrawableArchetype>())
                    {
                        if (!AreRequestedStatsLoaded(drawable, requestedDrawableStats))
                        {
                            drawablesNeedingData.Add(drawable);
                        }
                    }
                }
                LoadMapArchetypeStatsFromDatabase(new StreamableObjectLoadContext { Objects = drawablesNeedingData, Stats = requestedDrawableStats.ToList() });
            }

            IEnumerable<StreamableStat> fragmentStats = StreamableStatUtils.GetValuesAssignedToProperties(typeof(FragmentArchetype));
            IEnumerable<StreamableStat> requestedFragmentStats = requestedStats.Intersect(fragmentStats);
            if (requestedFragmentStats.Any())
            {
                IList<IStreamableObject> fragmentsNeedingData = new List<IStreamableObject>();
                foreach (IMapSection section in sections)
                {
                    foreach (IFragmentArchetype fragment in section.Archetypes.OfType<IFragmentArchetype>())
                    {
                        if (!AreRequestedStatsLoaded(fragment, requestedFragmentStats))
                        {
                            fragmentsNeedingData.Add(fragment);
                        }
                    }
                }
                LoadMapArchetypeStatsFromDatabase(new StreamableObjectLoadContext { Objects = fragmentsNeedingData, Stats = requestedFragmentStats.ToList() });
            }

            IEnumerable<StreamableStat> interiorStats = StreamableStatUtils.GetValuesAssignedToProperties(typeof(InteriorArchetype));
            IEnumerable<StreamableStat> requestedInteriorStats = requestedStats.Intersect(interiorStats);
            if (requestedInteriorStats.Any())
            {
                IList<IStreamableObject> interiorsNeedingData = new List<IStreamableObject>();
                foreach (IMapSection section in sections)
                {
                    foreach (IInteriorArchetype interior in section.Archetypes.OfType<IInteriorArchetype>())
                    {
                        if (!AreRequestedStatsLoaded(interior, requestedInteriorStats))
                        {
                            interiorsNeedingData.Add(interior);
                        }
                    }
                }
                LoadMapArchetypeStatsFromDatabase(new StreamableObjectLoadContext { Objects = interiorsNeedingData, Stats = requestedInteriorStats.ToList() });
            }

            IEnumerable<StreamableStat> statedAnimStats = StreamableStatUtils.GetValuesAssignedToProperties(typeof(StatedAnimArchetype));
            IEnumerable<StreamableStat> requestedStatedAnimStats = requestedStats.Intersect(statedAnimStats);
            if (requestedStatedAnimStats.Any())
            {
                IList<IStreamableObject> statedAnimsNeedingData = new List<IStreamableObject>();
                foreach (IMapSection section in sections)
                {
                    foreach (IStatedAnimArchetype statedAnim in section.Archetypes.OfType<IStatedAnimArchetype>())
                    {
                        if (!AreRequestedStatsLoaded(statedAnim, requestedStatedAnimStats))
                        {
                            statedAnimsNeedingData.Add(statedAnim);
                        }
                    }
                }
                LoadMapArchetypeStatsFromDatabase(new StreamableObjectLoadContext { Objects = statedAnimsNeedingData, Stats = requestedStatedAnimStats.ToList() });
            }

            // Have any entity stats been requested?
            IEnumerable<StreamableStat> entityStats = StreamableStatUtils.GetValuesAssignedToProperties(typeof(Entity));
            IEnumerable<StreamableStat> requestedEntityStats = requestedStats.Intersect(entityStats);
            if (requestedEntityStats.Any())
            {
                IList<IStreamableObject> entitiesNeedingData = new List<IStreamableObject>();
                foreach (IMapSection section in sections)
                {
                    foreach (IEntity entity in section.ChildEntities)
                    {
                        if (!AreRequestedStatsLoaded(entity, requestedEntityStats))
                        {
                            entitiesNeedingData.Add(entity);
                        }
                    }
                }
                LoadEntityStatsFromDatabase(new StreamableObjectLoadContext { Objects = entitiesNeedingData, Stats = requestedEntityStats.ToList() });
            }

            // Have any room stats been requested?
            IEnumerable<StreamableStat> roomStats = StreamableStatUtils.GetValuesAssignedToProperties(typeof(Room));
            IEnumerable<StreamableStat> requestedRoomStats = requestedStats.Intersect(roomStats);
            if (requestedRoomStats.Any())
            {
                IList<IStreamableObject> roomsNeedingData = new List<IStreamableObject>();
                foreach (IMapSection section in sections)
                {
                    foreach (IInteriorArchetype interior in section.InteriorArchetypes)
                    {
                        foreach (IRoom room in interior.Rooms)
                        {
                            if (!AreRequestedStatsLoaded(room, requestedRoomStats))
                            {
                                roomsNeedingData.Add(room);
                            }
                        }
                    }
                }
                LoadRoomStatsFromDatabase(new StreamableObjectLoadContext { Objects = roomsNeedingData, Stats = requestedRoomStats.ToList() });
            }
        }
        #endregion // Database Data
        #endregion // Private Methods

        #region StatStreamingCoordinatorBase Overrides
        /// <summary>
        /// 
        /// </summary>
        public override void Dispose()
        {
            base.Dispose();
            BuildClient.Close();
        }
        #endregion // StatStreamingCoordinatorBase Overrides
    } // MapStatStreamingCoordinator
}
