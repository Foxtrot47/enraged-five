﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common.GameText
{
    /// <summary>
    /// A single entry in a game text file.
    /// </summary>
    public interface IGameTextEntry
    {
        /// <summary>
        /// Label associated with this piece of game text.
        /// </summary>
        String Label { get; }

        /// <summary>
        /// Textual string associated with this label.
        /// </summary>
        String Text { get; }

        /// <summary>
        /// Optional platform that this piece of text is for. 
        /// If null it indicates that it is for all platforms (provided
        /// there isn't a platform specific version for it already).
        /// </summary>
        RSG.Platform.Platform? Platform { get; }
    } // IGameTextEntry
}
