﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.Base.Math;
using RSG.Model.Common.Map;

namespace RSG.Model.Map3
{
    public enum VehicleNodeSpecialUse
    {
        None,
        ParallelParking,
        PerpendicularParking,
        DropoffGoods,
        DriveThrough,
        DriveThroughWindow,
        DropoffGoodsUnload,
        HidingNode,
        SmallworkVehicles,
        PetrolStation,
        PedCrossing,
        DropoffPassengers,
        DropOffPassengersUnload,
        OpenSpace,
        PedAssistedMovement,
        TrafficLight,
        GiveWay,
        ForceJunction
    }

    /// <summary>
    /// Vehicle node.
    /// </summary>
    public class VehicleNode : AssetBase
    {
        /// <summary>
        /// Unique node identifier.
        /// </summary>
        public Guid Guid
        {
            get;
            internal set;
        }

        /// <summary>
        /// Position.
        /// </summary>
        public Vector3f Position
        {
            get;
            set;
        }

        /// <summary>
        /// Street name.
        /// </summary>
        public string StreetName
        {
            get;
            set;
        }

        /// <summary>
        /// Traffic density.
        /// </summary>
        public int Density
        {
            get;
            set;
        }

        /// <summary>
        /// Number of lanes attached to this node.
        /// </summary>
        public int LanesIn
        {
            get;
            set;
        }

        /// <summary>
        /// Number of lanes existing this node.
        /// </summary>
        public int LanesOut
        {
            get;
            set;
        }

        /// <summary>
        /// Vehicle node special use flag.
        /// </summary>
        public VehicleNodeSpecialUse SpecialUse
        {
            get;
            set;
        }

        /// <summary>
        /// True if the node is disabled.
        /// </summary>
        public bool IsDisabled
        {
            get;
            set;
        }
    }
}
