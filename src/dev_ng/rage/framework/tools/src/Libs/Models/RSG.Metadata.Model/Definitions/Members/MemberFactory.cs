﻿// --------------------------------------------------------------------------------------------
// <copyright file="MemberFactory.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions.Members
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using System.Reflection.Emit;
    using System.Xml;

    /// <summary>
    /// Provides methods to create parCodeGen members based on a specified data provider.
    /// </summary>
    public static class MemberFactory
    {
        #region Fields
        /// <summary>
        /// The xml node name that contains data to initialise a <see cref="ArrayMember"/>
        /// object.
        /// </summary>
        private const string ArrayMemberNodeName = "array";

        /// <summary>
        /// The xml node name that contains data to initialise a <see cref="BitsetMember"/>
        /// object.
        /// </summary>
        private const string BitsetMemberNodeName = "bitset";

        /// <summary>
        /// The xml node name that contains data to initialise a <see cref="BoolMember"/>
        /// object.
        /// </summary>
        private const string BoolMemberNodeName = "bool";

        /// <summary>
        /// The xml node name that contains data to initialise a <see cref="BoolVectorMember"/>
        /// object.
        /// </summary>
        private const string BoolVectorMemberNodeName = "VecBoolV";

        /// <summary>
        /// The xml node name that contains data to initialise a <see cref="BoolVMember"/>
        /// object.
        /// </summary>
        private const string BoolVNodeName = "BoolV";

        /// <summary>
        /// The xml node name that contains data to initialise a <see cref="CharMember"/>
        /// object.
        /// </summary>
        private const string CharMemberNodeName = "char";

        /// <summary>
        /// The xml node name that contains data to initialise a <see cref="ColourMember"/>
        /// object.
        /// </summary>
        private const string ColourMemberNodeName = "Color32";

        /// <summary>
        /// The xml node name that contains data to initialise a <see cref="DoubleMember"/>
        /// object.
        /// </summary>
        private const string DoubleNodeName = "double";

        /// <summary>
        /// The xml node name that contains data to initialise a <see cref="EnumMember"/>
        /// object.
        /// </summary>
        private const string EnumMemberNodeName = "enum";

        /// <summary>
        /// The xml node name that contains data to initialise a <see cref="Float16Member"/>
        /// object.
        /// </summary>
        private const string Float16MemberNodeName = "Float16";

        /// <summary>
        /// The xml node name that contains data to initialise a <see cref="FloatMember"/>
        /// object.
        /// </summary>
        private const string FloatMemberNodeName = "float";

        /// <summary>
        /// The xml node name that contains data to initialise a <see cref="HInsertMember"/>
        /// object.
        /// </summary>
        private const string HInsertMemberNodeName = "hinsert";

        /// <summary>
        /// The xml node name that contains data to initialise a <see cref="IntMember"/>
        /// object.
        /// </summary>
        private const string IntMemberNodeName = "int";

        /// <summary>
        /// The xml node name that contains data to initialise a <see cref="MapMember"/>
        /// object.
        /// </summary>
        private const string MapMemberNodeName = "map";

        /// <summary>
        /// The xml node name that contains data to initialise a <see cref="Mat34VMember"/>
        /// object.
        /// </summary>
        private const string Mat33VMemberNodeName = "Mat33V";

        /// <summary>
        /// The xml node name that contains data to initialise a <see cref="Mat34VMember"/>
        /// object.
        /// </summary>
        private const string Mat34VMemberNodeName = "Mat34V";

        /// <summary>
        /// The xml node name that contains data to initialise a <see cref="Mat44VMember"/>
        /// object.
        /// </summary>
        private const string Mat44VMemberNodeName = "Mat44V";

        /// <summary>
        /// The xml node name that contains data to initialise a <see cref="Matrix34Member"/>
        /// object.
        /// </summary>
        private const string Matrix34MemberNodeName = "Matrix34";

        /// <summary>
        /// The xml node name that contains data to initialise a <see cref="Matrix44Member"/>
        /// object.
        /// </summary>
        private const string Matrix44MemberNodeName = "Matrix44";

        /// <summary>
        /// The xml node name that contains data to initialise a <see cref="PadMember"/>
        /// object.
        /// </summary>
        private const string PadMemberNodeName = "pad";

        /// <summary>
        /// The xml node name that contains data to initialise a <see cref="PointerMember"/>
        /// object.
        /// </summary>
        private const string PointerMemberNodeName = "pointer";

        /// <summary>
        /// The xml node name that contains data to initialise a <see cref="PtrDiffMember"/>
        /// object.
        /// </summary>
        private const string PtrDiffNodeName = "ptrdiff_t";

        /// <summary>
        /// The xml node name that contains data to initialise a <see cref="ScalarVMember"/>
        /// object.
        /// </summary>
        private const string ScalarVNodeName = "ScalarV";

        /// <summary>
        /// The xml node name that contains data to initialise a <see cref="ShortMember"/>
        /// object.
        /// </summary>
        private const string ShortMemberNodeName = "short";

        /// <summary>
        /// The xml node name that contains data to initialise a <see cref="Signed16Member"/>
        /// object.
        /// </summary>
        private const string Signed16MemberNodeName = "s16";

        /// <summary>
        /// The xml node name that contains data to initialise a <see cref="Signed32Member"/>
        /// object.
        /// </summary>
        private const string Signed32MemberNodeName = "s32";

        /// <summary>
        /// The xml node name that contains data to initialise a <see cref="Signed64Member"/>
        /// object.
        /// </summary>
        private const string Signed64MemberNodeName = "s64";

        /// <summary>
        /// The xml node name that contains data to initialise a <see cref="Signed8Member"/>
        /// object.
        /// </summary>
        private const string Signed8MemberNodeName = "s8";

        /// <summary>
        /// The xml node name that contains data to initialise a <see cref="SizeTMember"/>
        /// object.
        /// </summary>
        private const string SizeTNodeName = "size_t";

        /// <summary>
        /// The xml node name that contains data to initialise a <see cref="StringMember"/>
        /// object.
        /// </summary>
        private const string StringMemberNodeName = "string";

        /// <summary>
        /// The xml node name that contains data to initialise a <see cref="StructMember"/>
        /// object.
        /// </summary>
        private const string StructMemberNodeName = "struct";

        /// <summary>
        /// The xml node name that contains data to initialise a <see cref="Unsigned16Member"/>
        /// object.
        /// </summary>
        private const string Unsigned16MemberNodeName = "u16";

        /// <summary>
        /// The xml node name that contains data to initialise a <see cref="Unsigned32Member"/>
        /// object.
        /// </summary>
        private const string Unsigned32MemberNodeName = "u32";

        /// <summary>
        /// The xml node name that contains data to initialise a <see cref="Unsigned64Member"/>
        /// object.
        /// </summary>
        private const string Unsigned64MemberNodeName = "u64";

        /// <summary>
        /// The xml node name that contains data to initialise a <see cref="Unsigned8Member"/>
        /// object.
        /// </summary>
        private const string Unsigned8MemberNodeName = "u8";

        /// <summary>
        /// The xml node name that contains data to initialise a <see cref="Vec2VMember"/>
        /// object.
        /// </summary>
        private const string Vec2VMemberNodeName = "Vec2V";

        /// <summary>
        /// The xml node name that contains data to initialise a <see cref="Vec3VMember"/>
        /// object.
        /// </summary>
        private const string Vec3VMemberNodeName = "Vec3V";

        /// <summary>
        /// The xml node name that contains data to initialise a <see cref="Vec4VMember"/>
        /// object.
        /// </summary>
        private const string Vec4VMemberNodeName = "Vec4V";

        /// <summary>
        /// The xml node name that contains data to initialise a <see cref="Vector2Member"/>
        /// object.
        /// </summary>
        private const string Vector2MemberNodeName = "Vector2";

        /// <summary>
        /// The xml node name that contains data to initialise a <see cref="Vector3Member"/>
        /// object.
        /// </summary>
        private const string Vector3MemberNodeName = "Vector3";

        /// <summary>
        /// The xml node name that contains data to initialise a <see cref="Vector4Member"/>
        /// object.
        /// </summary>
        private const string Vector4MemberNodeName = "Vector4";

        /// <summary>
        /// A private dictionary containing the delegate methods to use to create the different
        /// types of members.
        /// </summary>
        private static Dictionary<string, CreateMember> _creationDelegates;

        /// <summary>
        /// A private dictionary containing the mapping between all of the known member types
        /// and their xml node name.
        /// </summary>
        private static Dictionary<Type, string> _factoryMapper;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="MemberFactory"/> class.
        /// </summary>
        static MemberFactory()
        {
            _factoryMapper = new Dictionary<Type, string>();
            _factoryMapper.Add(typeof(FloatMember), FloatMemberNodeName);
            _factoryMapper.Add(typeof(StringMember), StringMemberNodeName);
            _factoryMapper.Add(typeof(ArrayMember), ArrayMemberNodeName);
            _factoryMapper.Add(typeof(BoolMember), BoolMemberNodeName);
            _factoryMapper.Add(typeof(StructMember), StructMemberNodeName);
            _factoryMapper.Add(typeof(Float16Member), Float16MemberNodeName);
            _factoryMapper.Add(typeof(Unsigned32Member), Unsigned32MemberNodeName);
            _factoryMapper.Add(typeof(Unsigned16Member), Unsigned16MemberNodeName);
            _factoryMapper.Add(typeof(Unsigned8Member), Unsigned8MemberNodeName);
            _factoryMapper.Add(typeof(Signed32Member), Signed32MemberNodeName);
            _factoryMapper.Add(typeof(Signed16Member), Signed16MemberNodeName);
            _factoryMapper.Add(typeof(Signed8Member), Signed8MemberNodeName);
            _factoryMapper.Add(typeof(IntMember), IntMemberNodeName);
            _factoryMapper.Add(typeof(ShortMember), ShortMemberNodeName);
            _factoryMapper.Add(typeof(CharMember), CharMemberNodeName);
            _factoryMapper.Add(typeof(PointerMember), PointerMemberNodeName);
            _factoryMapper.Add(typeof(MapMember), MapMemberNodeName);
            _factoryMapper.Add(typeof(Vector4Member), Vector4MemberNodeName);
            _factoryMapper.Add(typeof(Vec4VMember), Vec4VMemberNodeName);
            _factoryMapper.Add(typeof(Vector3Member), Vector3MemberNodeName);
            _factoryMapper.Add(typeof(Vec3VMember), Vec3VMemberNodeName);
            _factoryMapper.Add(typeof(Vector2Member), Vector2MemberNodeName);
            _factoryMapper.Add(typeof(Vec2VMember), Vec2VMemberNodeName);
            _factoryMapper.Add(typeof(BitsetMember), BitsetMemberNodeName);
            _factoryMapper.Add(typeof(ColourMember), ColourMemberNodeName);
            _factoryMapper.Add(typeof(EnumMember), EnumMemberNodeName);
            _factoryMapper.Add(typeof(BoolVectorMember), BoolVectorMemberNodeName);
            _factoryMapper.Add(typeof(Matrix44Member), Matrix44MemberNodeName);
            _factoryMapper.Add(typeof(Matrix34Member), Matrix34MemberNodeName);
            _factoryMapper.Add(typeof(Mat44VMember), Mat44VMemberNodeName);
            _factoryMapper.Add(typeof(Mat34VMember), Mat34VMemberNodeName);
            _factoryMapper.Add(typeof(Mat33VMember), Mat33VMemberNodeName);
            _factoryMapper.Add(typeof(PadMember), PadMemberNodeName);
            _factoryMapper.Add(typeof(ScalarVMember), ScalarVNodeName);
            _factoryMapper.Add(typeof(BoolVMember), BoolVNodeName);
            _factoryMapper.Add(typeof(SizeTMember), SizeTNodeName);
            _factoryMapper.Add(typeof(PtrDiffMember), PtrDiffNodeName);
            _factoryMapper.Add(typeof(DoubleMember), DoubleNodeName);
            _factoryMapper.Add(typeof(Signed64Member), Signed64MemberNodeName);
            _factoryMapper.Add(typeof(Unsigned64Member), Unsigned64MemberNodeName);
            _factoryMapper.Add(typeof(HInsertMember), HInsertMemberNodeName);

            MemberFactory.InitialiseCreationDelegates(_factoryMapper);
        }
        #endregion Constructors

        #region Delegates
        /// <summary>
        /// Defines the delegate method to use to create a new instance of a member using a
        /// System.Xml.XmlReader and a parent structure.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise the new member.
        /// </param>
        /// <param name="structure">
        /// The structure definition that the new member will be apart of.
        /// </param>
        /// <returns>
        /// A parCodeGen member that's type is determined by the specified data provider.
        /// </returns>
        private delegate IMember CreateMember(XmlReader reader, IStructure structure);
        #endregion Delegates

        #region Methods
        /// <summary>
        /// Creates a <see cref="IMember"/> object using the specified System.Xml.XmlReader as
        /// a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise the new member.
        /// </param>
        /// <param name="structure">
        /// The structure definition that the new member will be apart of.
        /// </param>
        /// <returns>
        /// A parCodeGen member that's type is determined by the specified data provider.
        /// </returns>
        public static IMember Create(XmlReader reader, IStructure structure)
        {
            CreateMember creationMethod = null;
            if (!_creationDelegates.TryGetValue(reader.Name, out creationMethod))
            {
                return new UnknownMember(reader, structure);
            }

            return creationMethod(reader, structure);
        }

        /// <summary>
        /// Gets the name of the xml node that contains the data for the specified member.
        /// </summary>
        /// <param name="member">
        /// The member whose xml node name should be returned.
        /// </param>
        /// <returns>
        /// The name of the xml node that contains the data for the specified member.
        /// </returns>
        public static string GetNodeNameForMember(IMember member)
        {
            string nodeName = null;
            if (_factoryMapper.TryGetValue(member.GetType(), out nodeName))
            {
                return nodeName;
            }

            UnknownMember unknownMember = member as UnknownMember;
            if (unknownMember != null)
            {
                return unknownMember.XmlNodeName;
            }

            return "InvalidXmlNodeName";
        }

        /// <summary>
        /// Initialises the creation methods that are used to create the different types of
        /// members from a System.Xml.XmlReader and a parent structure.
        /// </summary>
        /// <param name="mapper">
        /// A mapper dictionary that contains the mapping between the member types and their
        /// corresponding xml node names.
        /// </param>
        private static void InitialiseCreationDelegates(Dictionary<Type, string> mapper)
        {
            _creationDelegates = new Dictionary<string, CreateMember>();
            Type[] parameters = new Type[] { typeof(XmlReader), typeof(IStructure) };
            Type delegateType = typeof(CreateMember);
            foreach (KeyValuePair<Type, string> edge in mapper)
            {
                Type creationType = edge.Key;
                ConstructorInfo ctor = creationType.GetConstructor(parameters);
                DynamicMethod method = new DynamicMethod("Create", creationType, parameters);
                ILGenerator gen = method.GetILGenerator();
                gen.Emit(OpCodes.Ldarg_0);
                gen.Emit(OpCodes.Ldarg_1);
                gen.Emit(OpCodes.Newobj, ctor);
                gen.Emit(OpCodes.Ret);
                Delegate creationDelegate = method.CreateDelegate(delegateType);
                _creationDelegates.Add(edge.Value, creationDelegate as CreateMember);
            }
        }
        #endregion Methods
    } // RSG.Metadata.Model.Definitions.Members.MemberFactory {Class}
} // RSG.Metadata.Model.Definitions.Members {Namespace}
