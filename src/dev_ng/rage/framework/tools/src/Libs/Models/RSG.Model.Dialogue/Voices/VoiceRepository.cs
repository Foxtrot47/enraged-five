﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Speech.Synthesis;

namespace RSG.Model.Dialogue.Voices
{
    public class VoiceRepository
    {
        #region Fields

        String m_defaultVoiceName;
        ObservableCollection<String> m_voices;

        #endregion // Fields

        #region Properties

        /// <summary>
        /// The name of the default voice, the voice 
        /// that gets serialised out if no voice is
        /// selected for a character.
        /// </summary>
        public String DefaultVoiceName
        {
            get { return m_defaultVoiceName; }
            private set
            {
                if (m_defaultVoiceName == value)
                    return;

                m_defaultVoiceName = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<String> Voices
        {
            get { return m_voices; }
            private set
            {
                if (m_voices == value)
                    return;

                m_voices = value;
            }
        }

        #endregion // Properties

        #region Constructors

        /// <summary>
        /// Default Constructor
        /// </summary>
        public VoiceRepository()
        {
            this.m_voices = new ObservableCollection<String>();
        }

        #endregion // Constructors

        #region Public Functions

        /// <summary>
        /// Determines if the given voice exists as an installed
        /// voice and can be selected
        /// </summary>
        public Boolean Exists(String voiceName)
        {
            if (string.IsNullOrEmpty(voiceName))
            {
                return false;
            }
            return m_voices.Contains(voiceName);
        }

        #endregion // Public Functions

        #region Private Functions

        /// <summary>
        /// Finds all the installed voices using a
        /// SpeechSynthesizer object
        /// </summary>
        private void FindInstalledVoices()
        {
            SpeechSynthesizer synthesizer = new SpeechSynthesizer();
            ReadOnlyCollection<InstalledVoice> voices = synthesizer.GetInstalledVoices();
            foreach (InstalledVoice voice in voices)
            {
                if (IsVoiceUsable(synthesizer, voice))
                {
                    String voiceName = voice.VoiceInfo.Name;
                    if (DefaultVoiceName == null)
                    {
                        DefaultVoiceName = voiceName;
                    }
                    m_voices.Add(voiceName);
                }
            }
        }

        /// <summary>
        /// Determines if a voice is usable by trying to select it.
        /// </summary>
        private static Boolean IsVoiceUsable(SpeechSynthesizer synthesizer, InstalledVoice voice)
        {
            try
            {
                synthesizer.SelectVoice(voice.VoiceInfo.Name);
            }
            catch
            {
                return false;
            }
            return true;
        }

        #endregion // Private Functions
    } // VoiceRepository
} // RSG.Model.Dialogue.Voices
