﻿// --------------------------------------------------------------------------------------------
// <copyright file="PointerPolicy.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions.Members
{
    /// <summary>
    /// Specifies the different types of policies a pointer member in the parCodeGen system
    /// supports.
    /// </summary>
    public enum PointerPolicy
    {
        /// <summary>
        /// The policy is unknown.
        /// </summary>
        Unknown,

        /// <summary>
        /// The policy is not recognised as valid, though it is defined as something.
        /// </summary>
        Unrecognised,

        /// <summary>
        /// The pointer member has a policy of owner.
        /// </summary>
        Owner,

        /// <summary>
        /// The pointer member has a policy of simple owner.
        /// </summary>
        SimpleOwner,

        /// <summary>
        /// The pointer member has a policy of external.
        /// </summary>
        External,

        /// <summary>
        /// The pointer member has a policy of link.
        /// </summary>
        Link,

        /// <summary>
        /// The pointer member has a policy of lazy link.
        /// </summary>
        LazyLink,
    } // RSG.Metadata.Model.Definitions.Members.PointerPolicy {Enum}
} // RSG.Metadata.Model.Definitions.Members {Namespace}
