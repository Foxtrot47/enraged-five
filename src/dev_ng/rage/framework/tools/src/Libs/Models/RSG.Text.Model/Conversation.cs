﻿//---------------------------------------------------------------------------------------------
// <copyright file="Conversation.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.Model
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Text.RegularExpressions;
    using System.Xml;
    using RSG.Editor;
    using RSG.Editor.Model;
    using RSG.Text.Model.Resources;

    /// <summary>
    /// Represents a single conversation that contains a collection of lines.
    /// </summary>
    public class Conversation : ModelBase
    {
        #region Fields
        /// <summary>
        /// The category name a new conversation is given.
        /// </summary>
        private const string DefaultConversationCategory = "Default";

        /// <summary>
        /// The index that represents the conversation index part of the filenames for the
        /// lines contained within this conversation.
        /// </summary>
        private int _audioFilepathIndex;

        /// <summary>
        /// The private field used for the <see cref="Category"/> property.
        /// </summary>
        private string _category;

        /// <summary>
        /// The private field used for the <see cref="CutsceneSubtitles"/> property.
        /// </summary>
        private bool _cutsceneSubtitles;

        /// <summary>
        /// The private field used for the <see cref="Description"/> property.
        /// </summary>
        private string _description;

        /// <summary>
        /// The private field used for the <see cref="Interruptible"/> property.
        /// </summary>
        private bool _interruptible;

        /// <summary>
        /// The private field used for the <see cref="IsPlaceholder"/> property.
        /// </summary>
        private bool _isPlaceholder;

        /// <summary>
        /// The private field used for the <see cref="IsRandom"/> property.
        /// </summary>
        private bool _isRandom;

        /// <summary>
        /// The private field used for the <see cref="IsTriggeredByAnimation"/> property.
        /// </summary>
        private bool _isTriggeredByAnimation;

        /// <summary>
        /// The private field used for the <see cref="Lines"/> property.
        /// </summary>
        private ModelCollection<ILine> _lines;

        /// <summary>
        /// The private field used for the <see cref="LockedFilenames"/> property.
        /// </summary>
        private bool _lockedFilenames;

        /// <summary>
        /// The private field used for the <see cref="Lines"/> property.
        /// </summary>
        private ReadOnlyModelCollection<ILine> _readOnlyLines;

        /// <summary>
        /// The private field used for the <see cref="Root"/> property.
        /// </summary>
        private string _root;

        /// <summary>
        /// The private field used for the <see cref="SubtitleType"/> property.
        /// </summary>
        private Guid _subtitleType;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="Conversation"/> class.
        /// </summary>
        /// <param name="dialogue">
        /// The dialogue that the conversation will belong to.
        /// </param>
        internal Conversation(Dialogue dialogue)
            : base(dialogue)
        {
            this._lines = new ModelCollection<ILine>(this);
            this._lockedFilenames = false;
            this._readOnlyLines = new ReadOnlyModelCollection<ILine>(this._lines);
            this._audioFilepathIndex = -1;
            this._category = DefaultConversationCategory;
            this._cutsceneSubtitles = false;
            this._description = String.Empty;
            this._interruptible = true;
            this._isPlaceholder = true;
            this._isRandom = false;
            this._isTriggeredByAnimation = false;
            this._root = String.Empty;
            this._subtitleType = this.Configurations.DefaultSubtitleTypeId;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Conversation"/> class using the
        /// specified xml reader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The xml reader that contains the data used to initialise this instance.
        /// </param>
        /// <param name="dialogue">
        /// The dialogue that the conversation will belong to.
        /// </param>
        internal Conversation(XmlReader reader, Dialogue dialogue)
            : base(dialogue)
        {
            if (dialogue == null)
            {
                throw new SmartArgumentNullException(() => dialogue);
            }

            this._lines = new ModelCollection<ILine>();
            this._lockedFilenames = false;
            this._readOnlyLines = new ReadOnlyModelCollection<ILine>(this._lines);
            this._audioFilepathIndex = -1;
            this._category = DefaultConversationCategory;
            this._cutsceneSubtitles = false;
            this._description = String.Empty;
            this._interruptible = true;
            this._isPlaceholder = true;
            this._isRandom = false;
            this._isTriggeredByAnimation = false;
            this._root = String.Empty;
            this._subtitleType = this.Configurations.DefaultSubtitleTypeId;

            using (new SuspendUndoEngine(this.UndoEngine, SuspendUndoEngineMode.UndoEvents))
            {
                this.Deserialise(reader);
                if (this._subtitleType == Guid.Empty)
                {
                    this._subtitleType = this.Configurations.DefaultSubtitleTypeId;
                }
            }

            this._lines.SetUndoEngineProvider(this);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Conversation"/> class as a deep copy
        /// of the specified instance.
        /// </summary>
        /// <param name="other">
        /// The instance to copy.
        /// </param>
        /// <param name="dialogue">
        /// The dialogue that the conversation will belong to.
        /// </param>
        private Conversation(Conversation other, Dialogue dialogue)
            : base(dialogue)
        {
            if (dialogue == null)
            {
                throw new SmartArgumentNullException(() => dialogue);
            }

            if (other == null)
            {
                throw new SmartArgumentNullException(() => other);
            }

            this._category = other._category;
            this._lockedFilenames = other._lockedFilenames;
            this._cutsceneSubtitles = other._cutsceneSubtitles;
            this._description = other._description;
            this._interruptible = other._interruptible;
            this._isPlaceholder = other._isPlaceholder;
            this._isRandom = other._isRandom;
            this._isTriggeredByAnimation = other._isTriggeredByAnimation;
            this._root = other._root;
            this._subtitleType = other._subtitleType;

            List<ILine> lines = new List<ILine>();
            foreach (ILine line in other.Lines)
            {
                lines.Add(line.DeepClone(this));
            }

            this._lines = new ModelCollection<ILine>(this, lines);
            this._readOnlyLines = new ReadOnlyModelCollection<ILine>(this._lines);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the audio file path index that this conversation uses to generate
        /// file paths for the audio lines.
        /// </summary>
        public int AudioFilepathIndex
        {
            get { return this._audioFilepathIndex; }
            set { this.SetProperty(ref this._audioFilepathIndex, value); }
        }

        /// <summary>
        /// Gets or sets the category that this conversation uses.
        /// </summary>
        public string Category
        {
            get { return this._category; }
            set { this.SetProperty(ref this._category, value); }
        }

        /// <summary>
        /// Gets the dialogue configurations that have been associated with this conversation.
        /// </summary>
        public DialogueConfigurations Configurations
        {
            get
            {
                if (this.Dialogue == null)
                {
                    return new DialogueConfigurations();
                }

                return this.Dialogue.Configurations;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this conversation contains subtitles used
        /// during a cut-scene.
        /// </summary>
        public bool CutsceneSubtitles
        {
            get { return this._cutsceneSubtitles; }
            set { this.SetProperty(ref this._cutsceneSubtitles, value); }
        }

        /// <summary>
        /// Gets or sets the description for this conversation.
        /// </summary>
        public string Description
        {
            get { return this._description; }
            set { this.SetProperty(ref this._description, value ?? String.Empty); }
        }

        /// <summary>
        /// Gets the dialogue object this conversation belongs to.
        /// </summary>
        public Dialogue Dialogue
        {
            get { return this.Parent as Dialogue; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this conversation can be interrupted by
        /// other game audio.
        /// </summary>
        public bool Interruptible
        {
            get { return this._interruptible; }
            set { this.SetProperty(ref this._interruptible, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this conversation is currently placeholder
        /// created by the designer.
        /// </summary>
        public bool IsPlaceholder
        {
            get { return this._isPlaceholder; }
            set { this.SetProperty(ref this._isPlaceholder, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this conversation contains random lines of
        /// dialogue.
        /// </summary>
        public bool IsRandom
        {
            get { return this._isRandom; }
            set { this.SetProperty(ref this._isRandom, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this conversation is triggered off by a
        /// specified animation and not called from script.
        /// </summary>
        public bool IsTriggeredByAnimation
        {
            get { return this._isTriggeredByAnimation; }
            set { this.SetProperty(ref this._isTriggeredByAnimation, value); }
        }

        /// <summary>
        /// Gets the collection of lines belonging to this conversation.
        /// </summary>
        public ReadOnlyModelCollection<ILine> Lines
        {
            get { return this._readOnlyLines; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the lines contained within this
        /// conversation have their filenames locked or are free to have them modified without
        /// first unlocking.
        /// </summary>
        public bool LockedFilenames
        {
            get { return this._lockedFilenames; }
            set { this.SetProperty(ref this._lockedFilenames, value); }
        }

        /// <summary>
        /// Gets or sets the root identifier for this conversation.
        /// </summary>
        public string Root
        {
            get { return this._root; }
            set { this.SetProperty(ref this._root, value ?? String.Empty); }
        }

        /// <summary>
        /// Gets or sets the identifier of the type of subtitle this conversation uses.
        /// </summary>
        public Guid SubtitleType
        {
            get { return this._subtitleType; }
            set { this.SetProperty(ref this._subtitleType, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Adds a cloned instance of the specified line to this conversation.
        /// </summary>
        /// <param name="line">
        /// The line to clone and add to the collection.
        /// </param>
        public void AddCloneLine(ILine line)
        {
            this._lines.Add(line.DeepClone(this));
        }

        /// <summary>
        /// Inserts the specified line in this conversation at the specified index.
        /// </summary>
        /// <param name="line">
        /// The line to insert into the collection.
        /// </param>
        /// <param name="index">
        /// The zero-based index at which the line should be inserted.
        /// </param>
        public void AddCloneLineAt(ILine line, int index)
        {
            this._lines.Insert(index, line.DeepClone(this));
        }

        /// <summary>
        /// Creates a new line and adds it to this conversation before returning the newly
        /// created line.
        /// </summary>
        /// <param name="reader">
        /// The reader that contains the data used to create the new line.
        /// </param>
        /// <returns>
        /// The newly created line.
        /// </returns>
        public ILine AddDeserialisedLine(XmlReader reader)
        {
            ILine line = new Line(reader, this);
            this._lines.Add(line);
            return line;
        }

        /// <summary>
        /// Creates a new line and adds it to this conversation at the specified index before
        /// returning the newly created line.
        /// </summary>
        /// <param name="reader">
        /// The reader that contains the data used to create the new line.
        /// </param>
        /// <param name="index">
        /// The zero-based index at which the line should be inserted.
        /// </param>
        /// <returns>
        /// The newly created line.
        /// </returns>
        public ILine AddDeserialisedLineAt(XmlReader reader, int index)
        {
            ILine line = new Line(reader, this);
            this._lines.Insert(index, line);
            return line;
        }

        /// <summary>
        /// Creates a new line and adds it to this conversation before returning the newly
        /// created line.
        /// </summary>
        /// <returns>
        /// The newly created line.
        /// </returns>
        public ILine AddNewLine()
        {
            Line line = new Line(this);
            this._lines.Add(line);
            return line;
        }

        /// <summary>
        /// Creates a new line and adds it to this conversation at the specified index before
        /// returning the newly created line.
        /// </summary>
        /// <param name="index">
        /// The zero-based index at which the line should be inserted.
        /// </param>
        /// <returns>
        /// The newly created line.
        /// </returns>
        public ILine AddNewLineAt(int index)
        {
            Line line = new Line(this);
            this._lines.Insert(index, line);
            return line;
        }

        /// <summary>
        /// Creates a deep copy of the current instance.
        /// </summary>
        /// <returns>
        /// A deep copy of the current System.Object.
        /// </returns>
        public override object DeepClone()
        {
            return new Conversation(this, this.Dialogue);
        }

        /// <summary>
        /// Clones this conversation object and sets up its parent to be the specified
        /// dialogue.
        /// </summary>
        /// <param name="dialogue">
        /// The dialogue that is to be the cloned objects parent.
        /// </param>
        /// <returns>
        /// The newly cloned conversation object.
        /// </returns>
        public Conversation DeepClone(Dialogue dialogue)
        {
            return new Conversation(this, dialogue);
        }

        /// <summary>
        /// Generates the filenames automatically for the lines underneath this conversation.
        /// </summary>
        public void GenerateFilenames()
        {
            List<ILine> linesToGenerate = new List<ILine>();
            DialogueConfigurations config = this.Configurations;
            List<string> currentFilenames = new List<string>();
            foreach (ILine line in this._lines)
            {
                DialogueCharacter character = config.GetCharacter(line.CharacterId);
                if (character != null && character.UsesManualFilenames)
                {
                    continue;
                }

                if (!String.IsNullOrWhiteSpace(line.AudioFilepath))
                {
                    currentFilenames.Add(line.AudioFilepath);
                    continue;
                }

                linesToGenerate.Add(line);
            }

            if (linesToGenerate.Count == 0)
            {
                return;
            }

            if (this.IsRandom)
            {
                this.GenerateFilenamesForRandom(linesToGenerate);
            }
            else
            {
                this.GenerateFilenamesForNonRandom(linesToGenerate);
            }
        }

        /// <summary>
        /// Moves the first occurrence of the specified line to the specified index.
        /// </summary>
        /// <param name="line">
        /// The line that should be moved.
        /// </param>
        /// <param name="newIndex">
        /// The zero-based index specifying the new location of the line.
        /// </param>
        public void MoveLine(ILine line, int newIndex)
        {
            using (new UndoRedoBatch(this.UndoEngine))
            {
                this._lines.Remove(line);
                this._lines.Insert(newIndex, line);
            }
        }

        /// <summary>
        /// Moves the line at the specified index to a new location in the collection.
        /// </summary>
        /// <param name="oldIndex">
        /// The zero-based index specifying the location of the line to be moved.
        /// </param>
        /// <param name="newIndex">
        /// The zero-based index specifying the new location of the line.
        /// </param>
        public void MoveLine(int oldIndex, int newIndex)
        {
            ILine item = this._lines[oldIndex];
            using (new UndoRedoBatch(this.UndoEngine))
            {
                this._lines.RemoveAt(oldIndex);
                this._lines.Insert(newIndex, item);
            }
        }

        /// <summary>
        /// Removes the first occurrence of the specified line from this conversation.
        /// </summary>
        /// <param name="line">
        /// The line to remove from this conversation.
        /// </param>
        public void RemoveLine(ILine line)
        {
            int index = this._lines.IndexOf(line);
            if (index == -1)
            {
                return;
            }

            this._lines.RemoveAt(index);
            string filename = line.AudioFilepath;
            if (!String.IsNullOrWhiteSpace(filename))
            {
                this.Dialogue.DeletedFilenames.Add(filename);
            }
        }

        /// <summary>
        /// Removes the line at the specified index.
        /// </summary>
        /// <param name="index">
        /// The zero-based index of the line to remove.
        /// </param>
        public void RemoveLineAt(int index)
        {
            if (index > this._lines.Count - 1 || index < 0)
            {
                Debug.Assert(false, "Incorrect index specified, returning as not to crash");
                return;
            }

            ILine line = this._lines[index];
            this._lines.RemoveAt(index);

            string filename = line.AudioFilepath;
            if (!String.IsNullOrWhiteSpace(filename))
            {
                this.Dialogue.DeletedFilenames.Add(filename);
            }
        }

        /// <summary>
        /// Serialises the data contained within this instance onto the specified xml writer.
        /// </summary>
        /// <param name="writer">
        /// The xml writer this instance will be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                Debug.Assert(writer != null, "Unable to serialise into a null writer.");
                return;
            }

            writer.WriteStartElement("AudioFilepathIndex");
            writer.WriteAttributeString("value", this._audioFilepathIndex.ToString());
            writer.WriteEndElement();

            writer.WriteElementString("Category", this._category);

            writer.WriteStartElement("CutsceneSubtitles");
            writer.WriteAttributeString("value", this._cutsceneSubtitles.ToString());
            writer.WriteEndElement();

            writer.WriteElementString("Description", this._description);
            writer.WriteStartElement("Interruptible");
            writer.WriteAttributeString("value", this._interruptible.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("IsPlaceholder");
            writer.WriteAttributeString("value", this._isPlaceholder.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("IsRandom");
            writer.WriteAttributeString("value", this._isRandom.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("IsTriggeredByAnimation");
            writer.WriteAttributeString("value", this._isTriggeredByAnimation.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("LockedFilenames");
            writer.WriteAttributeString("value", this._lockedFilenames.ToString());
            writer.WriteEndElement();

            writer.WriteElementString("Root", this._root);
            writer.WriteElementString("SubtitleType", this._subtitleType.ToString("D"));

            writer.WriteStartElement("Lines");
            foreach (ILine line in this._lines)
            {
                writer.WriteStartElement("Item");
                line.Serialise(writer);
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }

        /// <summary>
        /// Validates this entity and creates a validation result object containing the errors
        /// and warnings.
        /// </summary>
        /// <param name="recursive">
        /// A value indicating whether any child models to this one should also be validated.
        /// </param>
        /// <returns>
        /// A validation result object containing all of the errors and warnings associated
        /// with this validation pass.
        /// </returns>
        public override ValidationResult Validate(bool recursive)
        {
            ValidationResult result = new ValidationResult();
            if (String.IsNullOrEmpty(this.Root))
            {
                result.AddError("Root", StringTable.RootFieldMissing);
            }
            else
            {
                if (this.Root.Length > 15)
                {
                    result.AddError("Root", StringTable.RootFieldTooLong);
                }

                for (int i = 0; i < this.Root.Length; i++)
                {
                    if (char.IsWhiteSpace(this.Root[i]))
                    {
                        result.AddError("Root", StringTable.RootFieldContainsSpaces);
                        break;
                    }
                }
            }

            if (this.Dialogue != null)
            {
                StringComparison comparisonType = StringComparison.OrdinalIgnoreCase;
                foreach (Conversation conversation in this.Dialogue.Conversations)
                {
                    if (Object.ReferenceEquals(conversation, this))
                    {
                        continue;
                    }

                    if (String.Equals(conversation.Root, this.Root, comparisonType))
                    {
                        result.AddError("Root", StringTable.RootFieldNotUnique);
                        break;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Indicates whether the current object is equal to the specified object. When this
        /// method is called it has already been determined that the specified object is not
        /// equal to the current object or null through reference equality.
        /// </summary>
        /// <param name="other">
        /// An object to compare with this object.
        /// </param>
        /// <returns>
        /// True if the current object is equal to the other parameter; otherwise, false.
        /// </returns>
        protected override bool EqualsCore(IModel other)
        {
            return false;
        }

        /// <summary>
        /// Initialises this instance using the data contained within the specified xml reader.
        /// </summary>
        /// <param name="reader">
        /// The xml reader that contains the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() == XmlNodeType.Element)
            {
                if (String.Equals("AudioFilepathIndex", reader.Name))
                {
                    string value = reader.GetAttribute("value");
                    if (!int.TryParse(value, out this._audioFilepathIndex))
                    {
                        this._audioFilepathIndex = -1;
                    }
                }
                else if (String.Equals("Category", reader.Name))
                {
                    this._category = reader.ReadElementContentAsString();
                }
                else if (String.Equals("CutsceneSubtitles", reader.Name))
                {
                    string value = reader.GetAttribute("value");
                    if (!bool.TryParse(value, out this._cutsceneSubtitles))
                    {
                        this._cutsceneSubtitles = false;
                    }
                }
                else if (String.Equals("Description", reader.Name))
                {
                    this._description = reader.ReadElementContentAsString();
                }
                else if (String.Equals("Interruptible", reader.Name))
                {
                    string value = reader.GetAttribute("value");
                    if (!bool.TryParse(value, out this._interruptible))
                    {
                        this._interruptible = true;
                    }
                }
                else if (String.Equals("IsPlaceholder", reader.Name))
                {
                    string value = reader.GetAttribute("value");
                    if (!bool.TryParse(value, out this._isPlaceholder))
                    {
                        this._isPlaceholder = true;
                    }
                }
                else if (String.Equals("IsRandom", reader.Name))
                {
                    string value = reader.GetAttribute("value");
                    if (!bool.TryParse(value, out this._isRandom))
                    {
                        this._isRandom = false;
                    }
                }
                else if (String.Equals("IsTriggeredByAnimation", reader.Name))
                {
                    string value = reader.GetAttribute("value");
                    if (!bool.TryParse(value, out this._isTriggeredByAnimation))
                    {
                        this._isTriggeredByAnimation = false;
                    }
                }
                else if (String.Equals("LockedFilenames", reader.Name))
                {
                    string value = reader.GetAttribute("value");
                    if (!bool.TryParse(value, out this._lockedFilenames))
                    {
                        this._lockedFilenames = false;
                    }
                }
                else if (String.Equals("Root", reader.Name))
                {
                    this._root = reader.ReadElementContentAsString();
                }
                else if (String.Equals("SubtitleType", reader.Name))
                {
                    string input = reader.ReadElementContentAsString();
                    if (!Guid.TryParseExact(input, "D", out this._subtitleType))
                    {
                        this._subtitleType = this.Configurations.DefaultSubtitleTypeId;
                    }
                }
                else if (String.Equals("Lines", reader.Name))
                {
                    this.DeserialiseLines(reader);
                }

                reader.Skip();
            }

            if (reader.NodeType == XmlNodeType.EndElement)
            {
                reader.ReadEndElement();
            }
        }

        /// <summary>
        /// Creates the lines for this conversation using the data contained with the
        /// specified xml reader.
        /// </summary>
        /// <param name="reader">
        /// The xml reader that contains the data used to initialise the lines for this
        /// conversation.
        /// </param>
        private void DeserialiseLines(XmlReader reader)
        {
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() == XmlNodeType.Element)
            {
                if (String.Equals("Item", reader.Name))
                {
                    this._lines.Add(new Line(reader, this));
                }
                else
                {
                    reader.Skip();
                }
            }

            if (reader.NodeType == XmlNodeType.EndElement)
            {
                reader.ReadEndElement();
            }
        }

        /// <summary>
        /// Ensures that this conversation has a valid audio file path index to use while
        /// constructing the auto-generated filenames.
        /// </summary>
        private void EnsureValidAudioFilepathIndex()
        {
            if (this.AudioFilepathIndex != -1)
            {
                return;
            }

            List<int> currentIndices = new List<int>();
            foreach (Conversation conversation in this.Dialogue.Conversations)
            {
                currentIndices.Add(conversation.AudioFilepathIndex);
            }

            Regex regex = new Regex("\\A[a-zA-Z0-9]+_[a-zA-Z]{4}");
            foreach (string deletedFilename in this.Dialogue.DeletedFilenames)
            {
                string substring = null;
                if (regex.IsMatch(deletedFilename))
                {
                    int startIndex = deletedFilename.IndexOf('_') + 1;
                    substring = deletedFilename.Substring(startIndex, 2);
                }
                else
                {
                    continue;
                }

                int mod = (int)substring[0];
                int remainder = (int)substring[1];

                mod -= 65;
                remainder -= 65;
                int index = (26 * mod) + remainder;
                if (!currentIndices.Contains(index))
                {
                    currentIndices.Add(index);
                }
            }

            this.AudioFilepathIndex = 0;
            while (currentIndices.Contains(this.AudioFilepathIndex))
            {
                this.AudioFilepathIndex++;
            }
        }

        /// <summary>
        /// Generates the audio file path for the given lines assuming that conversation is
        /// non-random.
        /// </summary>
        /// <param name="linesToGenerate">
        /// The lines that have been determined as invalid and need generating.
        /// </param>
        private void GenerateFilenamesForNonRandom(List<ILine> linesToGenerate)
        {
            if (this.IsRandom)
            {
                return;
            }

            string conversationIndex = this.GetConversationStringIndex();
            string baseAudioFilepath = this.Dialogue.Id + "_" + conversationIndex;
            HashSet<string> allAudioFilepaths = this.Dialogue.AllAudioFilepaths;
            foreach (ILine line in linesToGenerate)
            {
                int lineIndex = 0;
                string lineComponent = this.GetAudiopathStringFromIndex(lineIndex);
                string audioFilepath = baseAudioFilepath + lineComponent + "_01";
                while (allAudioFilepaths.Contains(audioFilepath))
                {
                    lineIndex++;
                    lineComponent = this.GetAudiopathStringFromIndex(lineIndex);
                    audioFilepath = baseAudioFilepath + lineComponent + "_01";
                }

                line.AudioFilepath = audioFilepath;
                allAudioFilepaths.Add(audioFilepath);
            }
        }

        /// <summary>
        /// Generates the audio file path for the given lines assuming that conversation is
        /// random.
        /// </summary>
        /// <param name="linesToGenerate">
        /// The lines that have been determined as invalid and need generating.
        /// </param>
        private void GenerateFilenamesForRandom(List<ILine> linesToGenerate)
        {
            if (!this.IsRandom)
            {
                return;
            }

            Regex regex = new Regex("\\A[a-zA-Z0-9]+_[a-zA-Z]{4}");
            string conversationIndex = this.GetConversationStringIndex();

            string baseAudioFilepath = null;
            foreach (ILine line in this._lines)
            {
                if (!line.ManualFilename)
                {
                    if (regex.IsMatch(line.AudioFilepath))
                    {
                        int length = line.AudioFilepath.IndexOf('_') + 5;
                        baseAudioFilepath = line.AudioFilepath.Substring(0, length);
                        break;
                    }
                }
            }

            HashSet<string> allAudioFilepaths = this.Dialogue.AllAudioFilepaths;
            if (baseAudioFilepath == null)
            {
                int lineIndex = 0;
                string line = this.GetAudiopathStringFromIndex(lineIndex);
                baseAudioFilepath = this.Dialogue.Id + "_" + conversationIndex + line;
                while (allAudioFilepaths.Contains(baseAudioFilepath))
                {
                    lineIndex++;
                    line = this.GetAudiopathStringFromIndex(lineIndex);
                    baseAudioFilepath = this.Dialogue.Id + "_" + conversationIndex + line;
                }
            }

            int index = 1;
            foreach (ILine line in linesToGenerate)
            {
                string audioFilepath = baseAudioFilepath + "_" + index.ToString("D2");
                while (allAudioFilepaths.Contains(audioFilepath))
                {
                    index++;
                    audioFilepath = baseAudioFilepath + "_" + index.ToString("D2");
                }

                line.AudioFilepath = audioFilepath;
                allAudioFilepaths.Add(audioFilepath);
            }
        }

        /// <summary>
        /// Gets the audio path filename string equivalent for the specified index. This starts
        /// with 0 - AA and ends with 675 - ZZ.
        /// </summary>
        /// <param name="index">
        /// The index to convert.
        /// </param>
        /// <returns>
        /// The audio path filename string equivalent for the specified index.
        /// </returns>
        private string GetAudiopathStringFromIndex(int index)
        {
            int remainder = index;
            int division = 0;
            while (remainder > 25)
            {
                division++;
                remainder -= 26;
            }

            string result = String.Empty;
            result += (char)(65 + division);
            result += (char)(65 + remainder);
            return result;
        }

        /// <summary>
        /// Gets the index for this conversation that should be used to construct the
        /// auto-generated filenames.
        /// </summary>
        /// <returns>
        /// The index for this conversation that should be used to construct the auto-generated
        /// filenames.
        /// </returns>
        private string GetConversationStringIndex()
        {
            this.EnsureValidAudioFilepathIndex();
            return this.GetAudiopathStringFromIndex(this.AudioFilepathIndex);
        }
        #endregion Methods
    } // RSG.Text.Model.Dialogue {Class}
} // RSG.Text.Model {Namespace}
