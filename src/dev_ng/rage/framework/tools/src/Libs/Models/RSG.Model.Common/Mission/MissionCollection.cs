﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Base.Collections;

namespace RSG.Model.Common.Mission
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [KnownType(typeof(Mission))]
    public class MissionCollection : AssetBase, IMissionCollection
    {
        #region Properties
        /// <summary>
        /// Missions that this collection contains.
        /// </summary>
        [DataMember]
        public ObservableCollection<IMission> Missions { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public MissionCollection()
            : base("Missions")
        {
            Missions = new ObservableCollection<IMission>();
        }
        #endregion // Constructor(s)

        #region IEnumerable<IMission> Implementation
        /// <summary>
        /// Loops through the weapons and returns each one in turn
        /// </summary>
        IEnumerator<IMission> IEnumerable<IMission>.GetEnumerator()
        {
            foreach (IMission weapon in this.Missions)
            {
                yield return weapon;
            }
        }

        /// <summary>
        /// Returns the enumerator for this object
        /// </summary>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerator)this);
        }
        #endregion // IEnumerable<IMission> Implementation

        #region ICollection<IMission> Implementation
        #region ICollection<IMission> Properties
        /// <summary>
        /// Returns whether the collection is read-only
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Return number of items in the set.
        /// </summary>
        public int Count
        {
            get
            {
                return Missions.Count;
            }
        }
        #endregion // ICollection<IMission> Properties

        #region ICollection<IMission> Methods
        /// <summary>
        /// Add a mission to the collection.
        /// </summary>
        /// <param name="item"></param>
        public void Add(IMission item)
        {
            Missions.Add(item);
        }

        /// <summary>
        /// Removes all Missions from the collection.
        /// </summary>
        public void Clear()
        {
            Missions.Clear();
        }

        /// <summary>
        /// Determine whether the collection contains a specific weapon
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Contains(IMission item)
        {
            return Missions.Contains(item);
        }

        /// <summary>
        /// Copy items in the collection to an array, starting at a particular index.
        /// </summary>
        /// <param name="output"></param>
        /// <param name="index"></param>
        public void CopyTo(IMission[] output, int index)
        {
            Missions.CopyTo(output, index);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        public bool Remove(IMission item)
        {
            return Missions.Remove(item);
        }
        #endregion // ICollection<IMission> Methods
        #endregion // ICollection<IMission> Implementation
    } // MissionCollection
}
