﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common.Map;
using System.ComponentModel;
using RSG.Base.Math;
using RSG.Model.Common;
using RSG.SceneXml;
using RSG.SceneXml.Statistics;
using RSG.SceneXml.Material;
using RSG.Model.Asset;
using RSG.Base.ConfigParser;
using System.Diagnostics;

namespace RSG.Model.Map3
{
    /// <summary>
    /// 
    /// </summary>
    public class DrawableArchetype : SimpleMapArchetypeBase, IDrawableArchetype
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public DrawableArchetype(string name, IMapSection parent)
            : base(name, parent)
        {
        }
        #endregion // Constructor(s)

        #region SimpleMapArchetypeBase Overrides
        /// <summary>
        /// Load specific statistics for this map section
        /// </summary>
        /// <param name="statsToLoad"></param>
        /// <param name="async"></param>
        public override void RequestStatistics(IEnumerable<StreamableStat> statsToLoad, bool async)
        {
            ParentSection.MapHierarchy.RequestStatisticsForDrawable(this, statsToLoad, async);
        }
        #endregion // SimpleMapArchetypeBase Overrides

        #region Object Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return (String.Format("Drawable Archetype: [{0}]{1}", this.Name, this.Hash));
        }
        #endregion // Object Overrides
    } // DrawableArchetype
}
