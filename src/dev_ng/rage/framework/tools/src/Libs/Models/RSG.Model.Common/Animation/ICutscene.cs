﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common.Animation
{
    /// <summary>
    /// Game cutscenes.
    /// </summary>
    public interface ICutscene : IAsset, IComparable<ICutscene>, IEquatable<ICutscene>, IStreamableObject
    {
        /// <summary>
        /// Friendly name for the cutscene (extracted from the cutscene data file).
        /// </summary>
        String FriendlyName { get; }

        /// <summary>
        /// Mission this cutscene is associated with (extracted from the cutscene data file).
        /// </summary>
        String MissionId { get; }

        /// <summary>
        /// Name of the exported zip file (for concats this will be null).
        /// </summary>
        String ExportZipFilepath { get; }

        /// <summary>
        /// Flag indicating whether this is a cutscene concat.
        /// </summary>
        bool IsConcat { get; }

        /// <summary>
        /// Flag indicating whether this cutscene contains branches.
        /// </summary>
        bool HasBranch { get; }

        /// <summary>
        /// The sub parts that make up this cutscene.
        /// </summary>
        IList<ICutscenePart> Parts { get; }

        /// <summary>
        /// How long this entire cutscene is (in seconds).
        /// </summary>
        float TotalDuration { get; }

        /// <summary>
        /// Platform size statistics.
        /// </summary>
        IDictionary<RSG.Platform.Platform, ICutscenePlatformStat> PlatformStats { get; }
    } // ICutscene
}
