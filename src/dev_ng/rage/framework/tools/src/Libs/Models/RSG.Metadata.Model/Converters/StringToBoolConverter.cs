﻿//---------------------------------------------------------------------------------------------
// <copyright file="StringToBoolConverter.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Converters
{
    using System;
    using RSG.Base;

    /// <summary>
    /// Represents a converter that can convert a string instance into a single or an array of
    /// boolean values. This class cannot be inherited.
    /// </summary>
    public sealed class StringToBoolConverter : ConverterBase<bool>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="StringToBoolConverter"/> class.
        /// </summary>
        public StringToBoolConverter()
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Implements the logic of the conversion between a single string instance and a
        /// boolean value.
        /// </summary>
        /// <param name="value">
        /// The string instance to convert.
        /// </param>
        /// <param name="fallback">
        /// Specifies the value that is returned if the conversion fails.
        /// </param>
        /// <param name="methods">
        /// The conversion methods that can be used during this conversion.
        /// </param>
        /// <returns>
        /// A structure containing the result of the conversion and a value indicating whether
        /// the conversion was successful.
        /// </returns>
        protected override TryResult<bool> Try(string value, bool fallback, Methods methods)
        {
            if (String.IsNullOrWhiteSpace(value))
            {
                return new TryResult<bool>(fallback, false);
            }

            value = value.Trim();
            bool result;
            if (bool.TryParse(value, out result))
            {
                return new TryResult<bool>(result, true);
            }

            if (value.Length > 1)
            {
                return new TryResult<bool>(fallback, false);
            }

            char firstCharacter = value[0];
            if (char.Equals(firstCharacter, '1'))
            {
                return new TryResult<bool>(true, true);
            }

            if (char.Equals(firstCharacter, '0'))
            {
                return new TryResult<bool>(false, true);
            }

            return new TryResult<bool>(fallback, false);
        }
        #endregion Methods
    } // RSG.Metadata.Model.Converters.StringToBoolConverter {Class}
} // RSG.Metadata.Model.Converters {Namespace}
