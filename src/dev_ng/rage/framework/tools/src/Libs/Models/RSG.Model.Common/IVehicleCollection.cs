﻿using System.Collections.Generic;
using RSG.Base.Collections;
using System;

namespace RSG.Model.Common
{
    /// <summary>
    /// 
    /// </summary>
    public interface IVehicleCollection :
        IAsset,
        IEnumerable<IVehicle>,
        ICollection<IVehicle>,
        IDisposable
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        ObservableCollection<IVehicle> Vehicles { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        void LoadAllStats();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="statsToLoad"></param>
        void LoadStats(IEnumerable<StreamableStat> statsToLoad);
        #endregion // Methods
    } // IVehicleCollection
} // RSG.Model.Common
