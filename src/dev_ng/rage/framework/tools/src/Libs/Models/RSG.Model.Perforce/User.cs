﻿using System;
using System.Collections.Generic;
using RSG.SourceControl.Perforce;
using RSG.Model.Common;
using RSG.Base.Editor.Command;

namespace RSG.Model.Perforce
{

    /// <summary>
    /// Perforce user object.
    /// </summary>
    public class User : 
        AssetBase,
        IPerforceObject
    {
        #region Properties
        /// <summary>
        /// User's email address.
        /// </summary>
        public String Email
        {
            get;
            protected set;
        }

        /// <summary>
        /// User's review specification.
        /// </summary>
        public String[] Reviews
        {
            get;
            protected set;
        }

        /// <summary>
        /// User's workspaces.
        /// </summary>
        public Workspace[] Workspaces
        {
            get;
            protected set;
        }
        #endregion // Properties

        #region IPerforceObject Interface
        /// <summary>
        /// Perforce Connection object.
        /// </summary>
        public P4 Connection
        {
            get { return (m_Connection); }
            set
            {
                SetPropertyValue(ref m_Connection, value, "Connection");
            }
        }
        private P4 m_Connection;

        /// <summary>
        /// 
        /// </summary>
        public void Refresh()
        {

        }
        #endregion // IPerforace Object Interface

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p4"></param>
        public User(P4 p4)
        {
            Init(p4);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="username"></param>
        /// <param name="p4"></param>
        public User(String username, P4 p4)
        {
            Init(p4);
            this.Name = username;
        }
        #endregion // Constructor(s)

        #region Static Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p4"></param>
        /// <returns></returns>
        public static User[] GetUsers(P4 p4)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static User GetUser(String name)
        {
            throw new NotImplementedException();
        }
        #endregion // Static Controller Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p4"></param>
        private void Init(P4 p4)
        {
            this.Connection = p4;
            this.Name = p4.User;
        }
        #endregion // Private Methods
    }

} // RSG.Model.Perforce namespace
