﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.ManagedRage;
using RSG.Model.Common;
using RSG.Model.Common.Animation;
using RSG.Pipeline.Content;
using RSG.Pipeline.Core;

namespace RSG.Model.Animation
{
    /// <summary>
    /// Local cutscene model.
    /// </summary>
    [DataContract]
    public class LocalCutscene : Cutscene
    {
        #region Constants
        private const String c_missionOverloadFile = @"$(toolsconfig)/config/cutscene/dev/missionoverload.xml";
        #endregion // Constants

        #region Member Data
        private IBranch m_branch;
        private IContentTree m_contentTree;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public LocalCutscene(String name, String friendlyName, String missionId, bool isConcat, bool hasBranch, String exportPath, IBranch branch, IContentTree contentTree)
            : base(name, friendlyName, missionId, isConcat, hasBranch, exportPath)
        {
            m_branch = branch;
            m_contentTree = contentTree;
        }
        #endregion // Constructor(s)

        #region StreamableAssetContainerBase Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="statsToLoad"></param>
        protected override void LoadStatsInternal(IEnumerable<StreamableStat> statsToLoad)
        {
            if (statsToLoad.Contains(StreamableCutsceneStat.Duration) && !IsStatLoaded(StreamableCutsceneStat.Duration))
            {
                foreach (CutscenePart part in Parts.Cast<CutscenePart>())
                {
                    part.LoadStatsFromAssetData();
                }
                SetStatLodaded(StreamableCutsceneStat.Duration, true);
            }
            if (statsToLoad.Contains(StreamableCutsceneStat.PlatformStats) && !IsStatLoaded(StreamableCutsceneStat.PlatformStats))
            {
                LoadPlatformStats();
            }
        }
        #endregion // StreamableAssetContainerBase Overrides

        #region Static Controller Methods
        /// <summary>
        /// Creates a new cutscene off of a single cutscene part.
        /// </summary>
        public static LocalCutscene CreateFromCutscenePart(ICutscenePart part, String friendlyName, String missionId, bool hasBranches, String exportPath, IBranch branch, IContentTree contentTree)
        {
            LocalCutscene cutscene = new LocalCutscene(part.Name, friendlyName, missionId, false, hasBranches, exportPath, branch, contentTree);
            cutscene.Parts.Add(part);
            return cutscene;
        }

        /// <summary>
        /// Creates a new cutscene based off of a concats file.
        /// </summary>
        public static LocalCutscene CreateFromConcatsFile(String filename, String friendlyName, String missionId, bool hasBranches, IBranch branch, IContentTree contentTree)
        {
            Debug.Assert(System.IO.File.Exists(filename), "Concats file doesn't exist.");

            String cutsceneName = Path.GetFileNameWithoutExtension(filename).ToLower();
            LocalCutscene cutscene = null;

            // Load up the file and extract the parts from it.
            using (FileStream stream = System.IO.File.OpenRead(filename))
            {
                try
                {
                    XDocument doc = XDocument.Load(stream);
                    XElement pathElem = doc.XPathSelectElement("/RexRageCutscenePartFile/path");
                    if (pathElem != null)
                    {
                        cutscene = new LocalCutscene(Path.GetFileName(pathElem.Value), friendlyName, missionId, true, hasBranches, null, branch, contentTree);

                        foreach (XElement itemElem in doc.XPathSelectElements("//parts/Item"))
                        {
                            XElement nameElem = itemElem.Element("name");
                            XElement filenameElem = itemElem.Element("filename");

                            if (nameElem != null && filenameElem != null)
                            {
                                String partName = Path.GetFileName(Path.GetDirectoryName(filenameElem.Value.ToLower()));
                                String cutFile = Path.Combine(Path.GetDirectoryName(filenameElem.Value.ToLower()),
                                    Path.GetFileNameWithoutExtension(filenameElem.Value.ToLower()),
                                    "data_stream.cutxml");

                                CutscenePart part = new CutscenePart(partName, cutFile);
                                cutscene.Parts.Add(part);
                            }
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    Log.Log__Exception(ex, "Unexpected exception while parsing the {0} concat file.", filename);
                }
            }

            return cutscene;
        }
        #endregion // Static Controller Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private void LoadPlatformStats()
        {
            IDictionary<RSG.Platform.Platform, ICutscenePlatformStat> platformStats =
                new Dictionary<RSG.Platform.Platform, ICutscenePlatformStat>();
            
            Regex sectionNameRegex = new Regex(String.Format(@"^{0}\-(?'index'\d+)\.", Name));

            foreach (Tuple<RSG.Platform.Platform, String> platformTuple in GetPlatformFilenames())
            {
                ICutscenePlatformStat platformStat = new CutscenePlatformStat(platformTuple.Item2);

                Packfile packFile = new Packfile();
                try
                {
                    if (System.IO.File.Exists(platformTuple.Item2) && packFile.Load(platformTuple.Item2))
                    {
                        foreach (PackEntry entry in packFile.Entries)
                        {
                            Match match = sectionNameRegex.Match(entry.Name);
                            if (match.Success)
                            {
                                Group indexGroup = match.Groups["index"];
                                uint index = UInt32.Parse(indexGroup.Value);

                                platformStat.Sections.Add(new CutscenePlatformSectionStat(index, entry.PhysicalSize, entry.VirtualSize));
                            }
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    Log.Log__Exception(ex, "Unable to process the '{0}' pack file.", platformTuple.Item2);
                }
                finally
                {
                    packFile.Close();
                }

                platformStats.Add(platformTuple.Item1, platformStat);
            }

            PlatformStats = platformStats;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private IEnumerable<Tuple<RSG.Platform.Platform, String>> GetPlatformFilenames()
        {
            // Find the one that this cutscene falls under.
            String cutsceneDirName;
            if (IsConcat)
            {
                // Use the parts to determine the directory name.
                String partName = Path.GetFileName(Path.GetDirectoryName(Parts.First().CutFile));
                String cutsceneExportPath = Path.Combine(m_branch.Export, "anim", "cutscene");

                String exportZipFilepath = System.IO.Directory.GetFiles(cutsceneExportPath,
                    String.Format("{0}.icd.zip", partName), SearchOption.AllDirectories).FirstOrDefault();

                cutsceneDirName = Path.GetDirectoryName(exportZipFilepath);
            }
            else
            {
                cutsceneDirName = Path.GetDirectoryName(ExportZipFilepath);
            }
            
            // Get the cutscene process that this cutscene will be useing..
            IProcess process = null;

            if (cutsceneDirName != null)
            {
                IEnumerable<IProcess> cutsceneProcesses =
                        m_contentTree.Processes.Where(item => item.ProcessorClassName == "RSG.Pipeline.Processor.Animation.Cutscene.PreProcess");
                process = cutsceneProcesses.FirstOrDefault(item =>
                    Path.GetFullPath((item.Inputs.First() as IFilesystemNode).AbsolutePath) == Path.GetFullPath(cutsceneDirName));
            }

            if (process != null)
            {
                IFilesystemNode output = process.Outputs.FirstOrDefault() as IFilesystemNode;

#warning We can't use the output path as it isn't valid for cutscenes.  Construct the output path manually instead.
                String outputFilename = Path.Combine(Path.GetDirectoryName(output.AbsolutePath), GetDestinationRPFName(cutsceneDirName));

                foreach (KeyValuePair<RSG.Platform.Platform, ITarget> targetPair in m_branch.Targets.Where(item => item.Value.Enabled))
                {
                    yield return Tuple.Create(targetPair.Key, targetPair.Value.Environment.Subst(outputFilename));
                }
            }
            else
            {
                Log.Log__Warning("Unable to find process for cutscene {0} based on directory {1}.", Name, cutsceneDirName);
            }
        }

        /// <summary>
        /// Checks the mission overload file to determine whether we should override the output filename for this cutscene.
        /// </summary>
        /// <param name="cutsceneDirName"></param>
        /// <returns></returns>
        private String GetDestinationRPFName(String cutsceneDirName)
        {
            String rpfFileName = String.Format("cuts_{0}.rpf", Path.GetFileName(cutsceneDirName));

            try
            {
                // Is this cutscene present in the missionoverload xml file?
                String overloadFile = m_branch.Environment.Subst(c_missionOverloadFile);
                XDocument doc = XDocument.Load(overloadFile);
                XElement elem = doc.XPathSelectElement(String.Format("/MissionOverloads/RPFOverload[@name='{0}']", Name));
                if (elem != null)
                {
                    XAttribute missionAtt = elem.Attribute("mission");
                    if (missionAtt != null)
                    {
                        rpfFileName = String.Format("cuts_{0}.rpf", missionAtt.Value);
                    }
                }
            }
            catch (System.Exception ex)
            {
                Log.Log__Exception(ex, "Failed to parse the missionoverload.xml file.");
            }

            return rpfFileName;
        }
        #endregion // Private Methods
    } // LocalCutscene
}
