﻿//---------------------------------------------------------------------------------------------
// <copyright file="BitsetTunable.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Tunables
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Xml;
    using RSG.Base;
    using RSG.Base.Extensions;
    using RSG.Base.Logging;
    using RSG.Editor;
    using RSG.Editor.Model;
    using RSG.Metadata.Model.Definitions;
    using RSG.Metadata.Model.Definitions.Members;

    /// <summary>
    /// Represents a tunable used by the parCodeGen system that is instancing a
    /// <see cref="BitsetMember"/> object.
    /// </summary>
    public class BitsetTunable : TunableBase
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Value"/> property.
        /// </summary>
        private ulong _value;

        /// <summary>
        /// The private field used for the <see cref="Items"/> property.
        /// </summary>
        private List<BitsetItem> _items;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="BitsetTunable"/> class to be as
        /// instance of the specified member.
        /// </summary>
        /// <param name="member">
        /// The member that this tunable will be instancing.
        /// </param>
        /// <param name="parent">
        /// The tunable parent this tunable will belong to.
        /// </param>
        public BitsetTunable(BitsetMember member, ITunableParent parent)
            : base(member, parent)
        {
            this._items = new List<BitsetItem>();
            IEnumeration values = member.Values;
            string[] parts = member.InitialValue.SafeSplit(new char[] { '|', ' ' }, true);
            if (member.NumBits > 0)
            {
                HashSet<string> initialLookup = new HashSet<string>();
                foreach (string part in parts)
                {
                    int colonIndex = part.LastIndexOf(':');
                    if (colonIndex == -1)
                    {
                        initialLookup.Add(part);
                    }
                    else
                    {
                        initialLookup.Add(part.Substring(colonIndex + 1));
                    }
                }

                long value = -1;
                for (int i = 0; i < member.NumBits; i++)
                {
                    value = value + 1;
                    string name = null;
                    bool associatedWithEnumValue = false;
                    if (values != null && values.EnumConstants.Count > i)
                    {
                        IEnumConstant constant = values.EnumConstants[i];
                        if (constant != null)
                        {
                            name = constant.Name;
                            associatedWithEnumValue = true;
                        }
                    }

                    if (name == null)
                    {
                        name = "0x" + i.ToStringInvariant("X4");
                    }

                    bool isChecked = false;
                    if (initialLookup.Contains(name))
                    {
                        isChecked = true;
                    }

                    this._items.Add(
                        new BitsetItem(name, isChecked, isChecked, this)
                        {
                            AssociatedWithEnumValue = associatedWithEnumValue,
                            Value = value
                        });
                }
            }
            else
            {
                ulong value = 0;
                foreach (string part in parts)
                {
                    ulong bit = 0;
                    bool resolved = false;
                    if (values != null)
                    {
                        int index = values.GetIndexOf(part);
                        if (index != -1)
                        {
                            resolved = true;
                            bit = (ulong)(1 << index);
                        }
                    }

                    if (!resolved)
                    {
                        TryResult<ulong> result = this.Dictionary.TryTo<ulong>(part, 0);
                        if (result.Success)
                        {
                            bit = result.Value;
                        }
                    }

                    value |= bit;
                }

                this._value = value;
            }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="BitsetTunable"/> class as a copy of
        /// the specified instance.
        /// </summary>
        /// <param name="other">
        /// The instance to copy.
        /// </param>
        /// <param name="parent">
        /// The tunable parent this tunable will belong to.
        /// </param>
        public BitsetTunable(BitsetTunable other, ITunableParent parent)
            : base(other, parent)
        {
            this._value = other._value;
            this._items = new List<BitsetItem>();
            foreach (BitsetItem bitsetItem in other.Items)
            {
                this._items.Add(new BitsetItem(bitsetItem, this));
            }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="BitsetTunable"/> class using the
        /// specified System.Xml.XmlReader as a initialising data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <param name="member">
        /// The member that this tunable will be instancing.
        /// </param>
        /// <param name="parent">
        /// The tunable parent this tunable will belong to.
        /// </param>
        /// <param name="log">
        /// A object that is used to log any warnings or errors produced during the parsing of
        /// the specified reader.
        /// </param>
        public BitsetTunable(
            XmlReader reader, BitsetMember member, ITunableParent parent, ILog log)
            : base(reader, member, parent)
        {
            this._items = new List<BitsetItem>();
            IEnumeration values = member.Values;
            string[] parts = member.InitialValue.SafeSplit(new char[] { '|', ' ' }, true);
            int numBits = member.NumBits;
            if (numBits > 0)
            {
                HashSet<string> initialLookup = new HashSet<string>();
                foreach (string part in parts)
                {
                    int colonIndex = part.LastIndexOf(':');
                    if (colonIndex == -1)
                    {
                        initialLookup.Add(part);
                    }
                    else
                    {
                        initialLookup.Add(part.Substring(colonIndex + 1));
                    }
                }

                long value = -1;
                for (int i = 0; i < numBits; i++)
                {
                    value = value + 1;
                    string name = null;
                    bool associatedWithEnumValue = false;
                    if (values != null && values.EnumConstants.Count > i)
                    {
                        IEnumConstant constant = values.EnumConstants[i];
                        if (constant != null)
                        {
                            name = constant.Name;
                            value = constant.Value;
                            associatedWithEnumValue = true;
                        }
                    }

                    if (name == null)
                    {
                        name = "0x" + i.ToStringInvariant("X4");
                    }

                    bool defaultState = false;
                    if (initialLookup.Contains(name))
                    {
                        defaultState = true;
                    }

                    this._items.Add(
                        new BitsetItem(name, false, defaultState, this)
                        {
                            AssociatedWithEnumValue = associatedWithEnumValue,
                            Value = value
                        });
                }
            }

            this.Deserialise(reader);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the bool member that this tunable is instancing.
        /// </summary>
        public BitsetMember BitsetMember
        {
            get
            {
                BitsetMember member = this.Member as BitsetMember;
                if (member != null)
                {
                    return member;
                }

                return new BitsetMember(this.TunableStructure.Definition);
            }
        }

        /// <summary>
        /// Gets the bitset items that have been defined for this tunable.
        /// </summary>
        public List<BitsetItem> Items
        {
            get { return this._items; }
        }

        /// <summary>
        /// Gets or sets the value assigned to this bitset tunable when the bitset is of
        /// indeterminate size.
        /// </summary>
        public ulong Value
        {
            get { return this._value; }
            set { this.SetProperty(ref this._value, value); }
        }

        /// <summary>
        /// Gets the bitset item used on this tunable for the bit with the specified name.
        /// </summary>
        /// <param name="name">
        /// The name of the bit whose item will be returned.
        /// </param>
        /// <returns>
        /// The bitset item used on this tunable for the bit with the specified name.
        /// </returns>
        public BitsetItem this[string name]
        {
            get
            {
                foreach (BitsetItem bitsetItem in this._items)
                {
                    if (String.Equals(bitsetItem.Name, name))
                    {
                        return bitsetItem;
                    }
                }

                return null;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new <see cref="BitsetTunable"/> that is a copy of this instance.
        /// </summary>
        /// <returns>
        /// A new <see cref="BitsetTunable"/> that is a copy of this instance.
        /// </returns>
        public new BitsetTunable Clone()
        {
            return new BitsetTunable(this, this.TunableStructure);
        }

        /// <summary>
        /// Creates a deep copy of the current instance.
        /// </summary>
        /// <returns>
        /// A deep copy of the current System.Object.
        /// </returns>
        public override object DeepClone()
        {
            return this.Clone();
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="ITunable"/> by
        /// looking at their values.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance by their values;
        /// otherwise, false.
        /// </returns>
        public override bool EqualByValue(ITunable other)
        {
            return this.EqualByValue(other as BitsetTunable);
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="ITunable"/> by
        /// looking at their values.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance by their values;
        /// otherwise, false.
        /// </returns>
        public bool EqualByValue(BitsetTunable other)
        {
            if (other == null)
            {
                return false;
            }

            if (this.Value != other.Value)
            {
                return false;
            }

            if (this.Items.Count != other.Items.Count)
            {
                return false;
            }

            foreach (BitsetItem bitsetItem in this.Items)
            {
                BitsetItem otherBitsetItem = other[bitsetItem.Name];
                if (otherBitsetItem == null)
                {
                    return false;
                }

                if (otherBitsetItem.IsChecked != bitsetItem.IsChecked)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified
        /// <see cref="BitsetTunable"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public bool Equals(BitsetTunable other)
        {
            if (other == null)
            {
                return false;
            }

            return Object.ReferenceEquals(other, this);
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="ITunable"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public override bool Equals(ITunable other)
        {
            return this.Equals(other as BitsetTunable);
        }

        /// <summary>
        /// Resets this tunable to have its default value. (i.e no source data with its initial
        /// values).
        /// </summary>
        public override void ResetValueToDefaultCore()
        {
            BitsetTunable source = this.InheritanceParent as BitsetTunable;
            if (this.BitsetMember.NumBits > 0)
            {
                foreach (BitsetItem item in this._items)
                {
                    item.SetToDefaultState();
                }
            }
            else
            {
                if (source != null)
                {
                    this.Value = source.Value;
                }
                else
                {
                    IEnumeration values = this.BitsetMember.Values;
                    string initialValue = this.BitsetMember.InitialValue;
                    string[] parts = initialValue.SafeSplit(new char[] { '|', ' ' }, true);
                    ulong value = 0;
                    foreach (string part in parts)
                    {
                        ulong bit = 0;
                        bool resolved = false;
                        if (values != null)
                        {
                            int index = values.GetIndexOf(part);
                            if (index != -1)
                            {
                                resolved = true;
                                bit = (ulong)(1 << index);
                            }
                        }

                        if (!resolved)
                        {
                            TryResult<ulong> result = this.Dictionary.TryTo<ulong>(part, 0);
                            if (result.Success)
                            {
                                bit = result.Value;
                            }
                        }

                        value |= bit;
                    }

                    this.Value = value;
                }
            }
        }

        /// <summary>
        /// Writes the xml representation of this instance onto the specified
        /// System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that the xml representation of this instance should be
        /// written to.
        /// </param>
        /// <param name="serialiseDefaultTunables">
        /// A value indicating whether the tunables that have a default value should be written
        /// to the specified writer or not.
        /// </param>
        public override void Serialise(XmlWriter writer, bool serialiseDefaultTunables)
        {
            if (writer == null)
            {
                throw new SmartArgumentNullException(() => writer);
            }

            if (this.BitsetMember.NumBits <= 0)
            {
                string rawData = "0x" + this._value.ToStringInvariant("X");
                string data = rawData;
                if (data == null)
                {
                    data = "0x0";
                }

                int bitCount = (rawData.Length - 2) * 4;

                writer.WriteAttributeString("bits", bitCount.ToStringInvariant());
                writer.WriteAttributeString("content", "int_array");
                writer.WriteString(data);
            }
            else
            {
                List<string> values = new List<string>();
                foreach (BitsetItem bitsetItem in this._items)
                {
                    if (bitsetItem.IsChecked)
                    {
                        values.Add(bitsetItem.Name);
                    }
                }

                string data = String.Join(" ", values);
                if (!String.IsNullOrEmpty(data))
                {
                    writer.WriteString(data);
                }
            }
        }

        /// <summary>
        /// Called whenever the tunable whose value will be used when this tunable is being
        /// inherited changes.
        /// </summary>
        protected override void InheritanceParentChanged(ITunable oldValue, ITunable newValue)
        {
            string name = "Value";
            if (oldValue != null)
            {
                PropertyChangedEventManager.RemoveHandler(
                    oldValue, this.OnInheritedValueChanged, name);
            }

            BitsetTunable source = this.InheritanceParent as BitsetTunable;
            if (source == null)
            {
                throw new NotSupportedException(
                    "Only the same type can be an inheritance parent.");
            }

            PropertyChangedEventManager.AddHandler(source, this.OnInheritedValueChanged, name);
            if (this.HasDefaultValue)
            {
                foreach (BitsetItem item in this._items)
                {
                    BitsetItem parent = source[item.Name];
                    if (parent == null)
                    {
                        Debug.Fail("Inheritance parent for bitset item is null.");
                        continue;
                    }

                    item.InheritanceParentChanged(parent);
                }

                this._value = source.Value;
                this.NotifyPropertyChanged("Value");
            }
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            if (reader == null)
            {
                return;
            }

            if (this.BitsetMember.NumBits <= 0)
            {
                string value = reader.ReadElementContentAsString();
                this._value = this.Dictionary.To<ulong>(value, 0);
            }
            else
            {
                string value = reader.GetAttribute("value");
                if (value != null)
                {
                    switch (this.BitsetMember.Type)
                    {
                        case BitsetType.Fixed:
                        case BitsetType.Fixed16:
                        case BitsetType.Fixed32:
                        case BitsetType.Fixed8:
                            break;

                        default:
                            throw new MetadataException(
                                "Bitsets only support the value attribute on fixed types.");
                    }
                }
                else
                {
                    string text = reader.ReadElementContentAsString();
                    if (text != null)
                    {
                        using (new SuspendUndoEngine(this.UndoEngine))
                        {
                            string[] parts = text.SafeSplit(' ', true);
                            foreach (string part in parts)
                            {
                                foreach (BitsetItem item in this._items)
                                {
                                    if (String.Equals(item.Name, part))
                                    {
                                        item.IsChecked = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            reader.Skip();
        }

        /// <summary>
        /// Called whenever the value of the inheritance parent changes so that the values can
        /// be kept in sync.
        /// </summary>
        /// <param name="sender">
        /// The inheritance parent.
        /// </param>
        /// <param name="e">
        /// The event data.
        /// </param>
        private void OnInheritedValueChanged(object sender, PropertyChangedEventArgs e)
        {
            if (!this.HasDefaultValue)
            {
                return;
            }

            BitsetTunable source = this.InheritanceParent as BitsetTunable;
            if (source != null)
            {
                this._value = source.Value;
                this.NotifyPropertyChanged("Value");
            }
        }
        #endregion Methods
    } // RSG.Metadata.Model.Tunables.BitsetTunable {Class}
} // RSG.Metadata.Model.Tunables {Namespace}
