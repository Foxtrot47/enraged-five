﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.IO;
using RSG.Base.ConfigParser;
using RSG.Statistics.Common.Dto.ExportStats;
using RSG.Statistics.Client.ServiceClients;
using RSG.Base.Logging;
using RSG.Base.Attributes;
using System.Reflection;
using System.Diagnostics;
using System.Web.UI;
using System.ServiceModel;
using RSG.Statistics.Common.Config;
using System.ServiceModel.Description;
using RSG.Statistics.Common;
using RSG.Base.Tasks;
using RSG.Statistics.Common.Dto.Reports;
using RSG.Statistics.Common.Model.MapExport;
using RSG.Platform;

namespace RSG.Model.Report.Reports.Map.Exports
{
    /// <summary>
    /// 
    /// </summary>
    public class NightlyExportStatReport : CSVReport, IDynamicReport, IReportMailProvider
    {
        #region Constants
        /// <summary>
        /// Report name/description.
        /// </summary>
        private const String c_name = "Nightly Export Stat Report";
        private const String c_description = "Report showing user/map section export statistics.  Running this in the workbench will show yesterday's data.";

        /// <summary>
        /// Default sender of the automated stats report mail.
        /// </summary>
        private const String c_defaultEmailSender = "report_generator@rockstarnorth.com";

        /// <summary>
        /// Default colors for the emailed report.
        /// </summary>
        private const String c_defaultBackgroundColour = "#999";
        private const String c_defaultForegroundColour = "#FFF";
        #endregion // Constants

        #region Private Classes
        /// <summary>
        /// Helper class that contains information for a single row for the users summary csv file.
        /// </summary>
        private class UserCsvData
        {
            [FriendlyName("Username")]
            public String Username { get; set; }

            [FriendlyName("Total Export Count")]
            public uint ExportCount { get; set; }

            [FriendlyName("Successful Export Count")]
            public uint SuccesfulCount { get; set; }

            [FriendlyName("Failed Export Count")]
            public uint FailedCount { get; set; }

            [FriendlyName("Success Ratio")]
            public double SuccessRatio
            {
                get
                {
                    return (SuccesfulCount / (SuccesfulCount + FailedCount));
                }
            }

            [FriendlyName("Total Export Time")]
            public TimeSpan TotalTime { get; set; }

            [FriendlyName("Average Export Time")]
            public TimeSpan AverageTime { get; set; }

            [FriendlyName("Maximum Export Time")]
            public TimeSpan MaxTime { get; set; }
        } // UserData

        /// <summary>
        /// Helper class that contains information for a single row for the sections summary csv file.
        /// </summary>
        private class SectionCsvData
        {
            [FriendlyName("Section Name")]
            public String SectionName { get; set; }

            [FriendlyName("Total Export Count")]
            public uint ExportCount { get; set; }

            [FriendlyName("Successful Export Count")]
            public uint ExportSuccesfulCount { get; set; }

            [FriendlyName("Export Success Ratio")]
            public double ExportSuccessRatio { get; set; }

            [FriendlyName("Total Time")]
            public TimeSpan TotalTime { get; set; }

            [FriendlyName("Average Total Time")]
            public TimeSpan AverageTotalTime { get; set; }

            [FriendlyName("Maximum Total Time")]
            public TimeSpan MaxTotalTime { get; set; }

            [FriendlyName("Section Export Count")]
            public uint SectionExportCount { get; set; }

            [FriendlyName("Section Export Success Ratio")]
            public double SectionExportSuccessRatio { get; set; }

            [FriendlyName("Average Export Time")]
            public TimeSpan AverageExportTime { get; set; }

            [FriendlyName("Maximum Export Time")]
            public TimeSpan MaxExportTime { get; set; }

            [FriendlyName("Total Build Count")]
            public uint BuildCount { get; set; }

            [FriendlyName("Build Success Ratio")]
            public double BuildSuccesRatio { get; set; }

            [FriendlyName("Average Build Time")]
            public TimeSpan AverageBuildTime { get; set; }

            [FriendlyName("Maximum Build Time")]
            public TimeSpan MaxBuildTime { get; set; }
        }

        /// <summary>
        /// Helper class that contains information for a single row in the all exports csv file.
        /// </summary>
        private class ExportCsvData
        {
            [FriendlyName("Username")]
            public String Username { get; set; }

            [FriendlyName("Machine Name")]
            public String MachineName { get; set; }

            [FriendlyName("Export Type")]
            public String ExportType { get; set; }
            
            [FriendlyName("Num Sections Exported")]
            public uint NumSectionsExported { get; set; }

            [FriendlyName("Sections Exported")]
            public String SectionsExported { get; set; }

            [FriendlyName("Platforms Enabled")]
            public String PlatformsEnabled { get; set; }

            [FriendlyName("Success")]
            public bool Success { get; set; }

            [FriendlyName("Tools Version")]
            public float? ToolsVersion { get; set; }

            [FriendlyName("XGE Enabled")]
            public bool? XGEEnabled { get; set; }

            [FriendlyName("XGE Standalone")]
            public bool? XGEStandalone { get; set; }

            [FriendlyName("XGE Force CPU Count")]
            public int? XGEForceCPUCount { get; set; }

            [FriendlyName("Export Number")]
            public uint ExportNumber { get; set; }

            [FriendlyName("Total Time")]
            public TimeSpan TotalTime { get; set; }

            [FriendlyName("Map Check Time")]
            public TimeSpan MapCheckTime { get; set; }

            [FriendlyName("Map Check Successful")]
            public bool MapCheckSuccessful { get; set; }

            [FriendlyName("Total Section Export Time")]
            public TimeSpan TotalSectionsExportTime { get; set; }

            [FriendlyName("All Section Exports Successful")]
            public bool AllSectionExportsSuccessful { get; set; }

            [FriendlyName("Build Time")]
            public TimeSpan BuildTime { get; set; }

            [FriendlyName("Build Successful")]
            public bool BuildSuccessful { get; set; }
        }
        #endregion // Private Classes

        #region Properties
        /// <summary>
        /// Mail message to send when the report is invoked
        /// </summary>
        public MailMessage MailMessage { get; protected set; }

        /// <summary>
        /// Task to run for generating this report.
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (m_task == null)
                {
                    m_task = new ActionTask("Generating report", (context, progress) => GenerateReport((DynamicReportContext)context, progress));
                }
                return m_task;
            }
        }
        private ITask m_task;

        /// <summary>
        /// Email address for the sender of the email.
        /// </summary>
        public String EmailSender { get; set; }

        /// <summary>
        /// Comma separated list of the default people to email.
        /// </summary>
        public String DefaultEmailList { get; set; }

        /// <summary>
        /// The number of users to show inline in the emailed report.
        /// </summary>
        public int NumInlineUsers { get; set; }

        /// <summary>
        /// The number of sections to show inline in the emailed report.
        /// </summary>
        public int NumInlineSections { get; set; }

        /// <summary>
        /// The number of individual exports to show inline in the emailed report.
        /// </summary>
        public int NumInlineExports { get; set; }

        /// <summary>
        /// Number of hours to offset the report by.
        /// </summary>
        public int HoursOffset { get; set; }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="desc"></param>
        public NightlyExportStatReport()
            : base(c_name, c_description)
        {
            EmailSender = c_defaultEmailSender;
            NumInlineUsers = 20;
            NumInlineSections = 20;
            NumInlineExports = 20;
            HoursOffset = 0;
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// Generate the report dynamically.
        /// </summary>
        protected virtual void GenerateReport(DynamicReportContext context, IProgress<TaskProgress> progress)
        {
            DateTime yesterday = DateTime.UtcNow.AddDays(-1);
            DailySummary dailyData = null;

            try
            {
                IStatisticsConfig config = new StatisticsConfig();

                EndpointAddress address = new EndpointAddress(String.Format("http://{0}:{1}/StatisticsService/Reports", config.DefaultServer.Address, config.DefaultServer.HttpPort));
                WebHttpBinding binding = new WebHttpBinding(WebHttpSecurityMode.None);
                binding.MaxReceivedMessageSize = 4194304;
                using (ReportClient client = new ReportClient(binding, address))
                {
                    client.Endpoint.Behaviors.Add(new WebHttpBehavior());
                    
                    // Get the parameters required for this report and set their values.
                    List<ReportParameter> parameters = client.GetReportParameters("MapExportsDailySummary");
                    if (SetParameters(ref parameters, yesterday))
                    {
                        dailyData = (DailySummary)client.RunReport("MapExportsDailySummary", true, parameters);
                    }
                }
            }
            catch (Exception e)
            {
                Log.Log__Exception(e, "Exception occurred while attempting to retrieve the daily map export stats from the statistics server.");
            }

            // Did we managed to retrieve the data?
            if (dailyData != null)
            {
                // Generate the csv data streams
                Stream usersCsvStream = GenerateUsersCsv(dailyData.UserStats);
                Stream sectionsCsvStream = GenerateSectionsCsv(dailyData.SectionStats);
                Stream exportsCsvStream = GenerateExportsCsv(dailyData.ExportStats);

                // Save the users csv data to disk
                if (Filename != null)
                {
                    string userFilename = Path.Combine(Path.GetDirectoryName(Filename), Path.GetFileNameWithoutExtension(Filename) + "_users" + Path.GetExtension(Filename));
                    SaveStreamToFile(usersCsvStream, userFilename);

                    // Save the sections csv data to disk
                    string sectionsFilename = Path.Combine(Path.GetDirectoryName(Filename), Path.GetFileNameWithoutExtension(Filename) + "_sections" + Path.GetExtension(Filename));
                    SaveStreamToFile(sectionsCsvStream, sectionsFilename);

                    // Save the exports csv data to disk
                    string exportsFilename = Path.Combine(Path.GetDirectoryName(Filename), Path.GetFileNameWithoutExtension(Filename) + "_breakdown" + Path.GetExtension(Filename));
                    SaveStreamToFile(exportsCsvStream, exportsFilename);
                }

                // Create the email message
                MailMessage = GenerateEmail(dailyData, usersCsvStream, sectionsCsvStream, exportsCsvStream, yesterday, context.GameView.ProjectName);
            }
        }

        /// <summary>
        /// Sets the report parameters for sending back to the server.
        /// </summary>
        /// <returns>True if the required parameters were found, otherwise false.</returns>
        private bool SetParameters(ref List<ReportParameter> parameters, DateTime yesterday)
        {
            ReportParameter dateParam = parameters.FirstOrDefault(item => item.Name == "Date");
            ReportParameter offsetParam = parameters.FirstOrDefault(item => item.Name == "HourOffset");

            if (dateParam != null && offsetParam != null)
            {
                dateParam.Value = yesterday.ToString("u");
                offsetParam.Value = HoursOffset.ToString();
                return true;
            }
            return false;
        }

        /// <summary>
        /// Converts the user summary information into a CSV stream.
        /// </summary>
        private Stream GenerateUsersCsv(List<UserSummary> userStats)
        {
            Stream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);

            // Convert the user stats to csv data object for writing out to csv.
            IList<UserCsvData> csvData = new List<UserCsvData>();

            foreach (UserSummary userStat in userStats.OrderBy(item => item.Username))
            {
                csvData.Add(
                    new UserCsvData
                    {
                        Username = userStat.Username,
                        ExportCount = userStat.TotalCount,
                        SuccesfulCount = userStat.SuccessfulCount,
                        FailedCount = (userStat.TotalCount - userStat.SuccessfulCount),
                        TotalTime = TimeSpan.FromSeconds(userStat.TotalTime),
                        AverageTime = TimeSpan.FromSeconds(userStat.TotalTime / userStat.TotalCount),
                        MaxTime = TimeSpan.FromSeconds(userStat.MaxTime)
                    });
            }

            return GenerateCSVStream(csvData);
        }

        /// <summary>
        /// Converts the section summary information into a CSV stream.
        /// </summary>
        private Stream GenerateSectionsCsv(List<SectionSummary> sectionStats)
        {
            Stream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);

            // Convert the section stats to csv data objects for writing out to csv.
            IList<SectionCsvData> csvData = new List<SectionCsvData>();

            if (sectionStats != null)
            {
                foreach (SectionSummary sectionStat in sectionStats.OrderBy(item => item.SectionName))
                {
                    csvData.Add(
                        new SectionCsvData
                        {
                            SectionName = sectionStat.SectionName,
                            ExportCount = (uint)sectionStat.TotalCount,
                            ExportSuccesfulCount = (uint)sectionStat.SuccessCount,
                            ExportSuccessRatio = sectionStat.SuccessCount / sectionStat.TotalCount,
                            TotalTime = TimeSpan.FromSeconds(sectionStat.AvgTotalTime * sectionStat.TotalCount),
                            AverageTotalTime = TimeSpan.FromSeconds(sectionStat.AvgTotalTime),
                            MaxTotalTime = TimeSpan.FromSeconds(sectionStat.MaxTotalTime),
                            SectionExportCount = (uint)sectionStat.SectionExportCount,
                            SectionExportSuccessRatio = sectionStat.SectionExportSuccessCount / sectionStat.SectionExportCount,
                            AverageExportTime = TimeSpan.FromSeconds(sectionStat.AvgSectionExportTime),
                            MaxExportTime = TimeSpan.FromSeconds(sectionStat.MaxSectionExportTime),
                            BuildCount = (uint)sectionStat.BuildCount,
                            BuildSuccesRatio = sectionStat.BuildSuccessCount / sectionStat.BuildCount,
                            AverageBuildTime = TimeSpan.FromSeconds(sectionStat.AvgBuildTime.HasValue ? sectionStat.AvgBuildTime.Value : 0.0),
                            MaxBuildTime = TimeSpan.FromSeconds(sectionStat.MaxBuildTime.HasValue ? sectionStat.MaxBuildTime.Value : 0.0)
                        });
                }
            }

            return GenerateCSVStream(csvData);
        }

        /// <summary>
        /// Converts all the export details into a CSV stream.
        /// </summary>
        private Stream GenerateExportsCsv(List<ExportDetails> exportStats)
        {
            Stream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);

            // Convert the export stats to csv data objects for writing out to csv.
            // Convert the user stats to the UserData object we will be outputting to the csv file.
            IList<ExportCsvData> csvData = new List<ExportCsvData>();

            if (exportStats != null)
            {
                IOrderedEnumerable<ExportDetails> sortedStats =
                    exportStats.OrderBy(item => item.Username).ThenBy(item => item.MachineName).ThenBy(item => item.End - item.Start);

                foreach (ExportDetails exportStat in sortedStats)
                {
                    try
                    {
                        ExportCsvData data =
                            new ExportCsvData
                            {
                                Username = exportStat.Username,
                                MachineName = exportStat.MachineName,
                                ExportType = exportStat.ExportType,
                                NumSectionsExported = (uint)exportStat.SectionsExported.Count,
                                SectionsExported = String.Join(";", exportStat.SectionsExported),
                                PlatformsEnabled = String.Join(";", exportStat.PlatformsEnabled.Select(item => item.PlatformToFriendlyName())),
                                Success = (exportStat.Success.HasValue ? exportStat.Success.Value : false),
                                ToolsVersion = exportStat.ToolsVersion,
                                XGEEnabled = exportStat.XGEEnabled,
                                XGEStandalone = exportStat.XGEStandalone,
                                XGEForceCPUCount = exportStat.XGEForceCPUCount,
                                ExportNumber = exportStat.ExportNumber,
                                TotalTime = TimeSpan.FromSeconds(exportStat.TotalTime)
                            };

                        ExportDetailsSubStep mapCheckSubStep = exportStat.SubSteps.FirstOrDefault(item => item.Name == "Map Check");
                        if (mapCheckSubStep != null)
                        {
                            data.MapCheckTime = TimeSpan.FromSeconds(mapCheckSubStep.Time);
                            data.MapCheckSuccessful = mapCheckSubStep.Success;
                        }

                        IEnumerable<ExportDetailsSubStep> sectionExportSubSteps =
                            exportStat.SubSteps.Where(item => item.Name.EndsWith("section export"));
                        if (sectionExportSubSteps.Any())
                        {
                            data.TotalSectionsExportTime = TimeSpan.FromSeconds(sectionExportSubSteps.Sum(item => (double)item.Time));
                            data.AllSectionExportsSuccessful = !sectionExportSubSteps.Any(item => item.Success == false);
                        }

                        ExportDetailsSubStep buildSubStep = exportStat.SubSteps.FirstOrDefault(item => item.Name == "Image Build");
                        if (buildSubStep != null)
                        {
                            data.BuildTime = TimeSpan.FromSeconds(buildSubStep.Time);
                            data.BuildSuccessful = buildSubStep.Success;
                        }

                        csvData.Add(data);
                    }
                    catch (System.Exception ex)
                    {
                        Log.Log__Exception(ex, "Bad export stat data while generating exports csv file!");
                    }
                }
            }

            return GenerateCSVStream(csvData);
        }

        /// <summary>
        /// Saves a stream to a file.
        /// </summary>
        private void SaveStreamToFile(Stream stream, String filename)
        {
            using (Stream fileStream = new FileStream(filename, FileMode.Create))
            {
                stream.Position = 0;
                stream.CopyTo(fileStream);
            }
        }

        /// <summary>
        /// Generates the emailed message from the daily summary information.
        /// </summary>
        private MailMessage GenerateEmail(DailySummary dailyData, Stream usersStream, Stream sectionsStream, Stream exportsStream, DateTime date, String projectName)
        {
            MailMessage mailMessage = new MailMessage();

            // Add basic information.
            mailMessage.From = new MailAddress(EmailSender);
            mailMessage.Subject = Name;

            if (DefaultEmailList != null)
            {
                foreach (string email in DefaultEmailList.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries).Distinct())
                {
                    mailMessage.To.Add(email);
                }
            }

            // Create the HTML view of the message and add it to the mail
            AlternateView htmlView = AlternateView.CreateAlternateViewFromString(GenerateEmailContent(dailyData, date, projectName), null, "text/html");
            mailMessage.AlternateViews.Add(htmlView);

            // Add the csv's as attachments
            usersStream.Position = 0;
            mailMessage.Attachments.Add(new Attachment(usersStream, String.Format("map_exports_users_{0}-{1:00}-{2:00}.csv", date.Year, date.Month, date.Day)));
            sectionsStream.Position = 0;
            mailMessage.Attachments.Add(new Attachment(sectionsStream, String.Format("map_exports_sections_{0}-{1:00}-{2:00}.csv", date.Year, date.Month, date.Day)));
            exportsStream.Position = 0;
            mailMessage.Attachments.Add(new Attachment(exportsStream, String.Format("map_exports_breakdown_{0}-{1:00}-{2:00}.csv", date.Year, date.Month, date.Day)));
            
            return mailMessage;
        }

        /// <summary>
        /// Generates the contents of the email.
        /// </summary>
        private string GenerateEmailContent(DailySummary dailyData, DateTime date, string projectName)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter sw = new StreamWriter(stream);
            HtmlTextWriter writer = new HtmlTextWriter(sw);

            // Generate the html
            writer.RenderBeginTag(HtmlTextWriterTag.Html);
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Body);
                {
                    WriteReportHeader(writer, dailyData.ExportStats, date, projectName);

                    if (dailyData.UserStats.Any())
                    {
                        writer.RenderBeginTag(HtmlTextWriterTag.Br);
                        writer.RenderEndTag();

                        WriteUserStats(writer, dailyData.UserStats);
                    }

                    if (dailyData.SectionStats.Any())
                    {
                        writer.RenderBeginTag(HtmlTextWriterTag.Br);
                        writer.RenderEndTag();

                        WriteSectionStats(writer, dailyData.SectionStats);
                    }

                    if (dailyData.ExportStats.Any())
                    {
                        writer.RenderBeginTag(HtmlTextWriterTag.Br);
                        writer.RenderEndTag();

                        WriteExportStats(writer, dailyData.ExportStats);
                    }
                }
                writer.RenderEndTag();
            }
            writer.RenderEndTag();
            writer.Flush();

            // Convert the stream of data into a string and return it
            string mailContent = null;
            stream.Position = 0;
            using (StreamReader reader = new StreamReader(stream))
            {
                mailContent = reader.ReadToEnd();
            }
            return mailContent;
        }

        /// <summary>
        /// Outputs some summary information.
        /// </summary>
        private void WriteReportHeader(HtmlTextWriter writer, List<ExportDetails> exportDetails, DateTime date, string projectName)
        {
            string defaultStyle = String.Format("background-color: {0}; color: {1}", c_defaultBackgroundColour, c_defaultForegroundColour);

            writer.AddAttribute(HtmlTextWriterAttribute.Border, "1");
            writer.AddAttribute(HtmlTextWriterAttribute.Width, "100%");
            writer.RenderBeginTag(HtmlTextWriterTag.Table);
            {
                // Header row
                writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                writer.AddAttribute(HtmlTextWriterAttribute.Colspan, "2");
                writer.AddAttribute(HtmlTextWriterAttribute.Align, "center");
                writer.AddAttribute(HtmlTextWriterAttribute.Style, String.Format("{0}; font-size: 18pt", defaultStyle));
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write(String.Format("Map Export Stats for {0:00}/{1:00}/{2}", date.Day, date.Month, date.Year));
                writer.RenderEndTag();
                writer.RenderEndTag();

                // Info rows
                long totalCount = exportDetails.Count;
                TimeSpan totalTime = TimeSpan.FromSeconds(exportDetails.Sum(item => (double)item.TotalTime));
                double successRatio = 0.0;
                TimeSpan averageTime = new TimeSpan();

                if (totalCount != 0)
                {
                    successRatio = (double)exportDetails.Count(item => item.Success == true) / (double)totalCount;
                    averageTime = new TimeSpan(totalTime.Ticks / totalCount);
                }

                WriteReportHeaderRow(writer, "Project", projectName, defaultStyle);
                WriteReportHeaderRow(writer, "Total Number of Exports", totalCount.ToString(), defaultStyle);
                WriteReportHeaderRow(writer, "Overall Success Ratio", String.Format("{0:0.##}", successRatio), defaultStyle);
                WriteReportHeaderRow(writer, "Total Time Spent Exporting", GenerateTimeSpanText(totalTime), defaultStyle);
                WriteReportHeaderRow(writer, "Average Export Time", GenerateTimeSpanText(averageTime), defaultStyle);
                WriteReportHeaderRow(writer, "Unique Users Count", exportDetails.Select(item => item.Username).Distinct().Count().ToString(), defaultStyle);
            }
            writer.RenderEndTag();
        }

        /// <summary>
        /// Converts a timespan to a nicer string format.
        /// </summary>
        private String GenerateTimeSpanText(TimeSpan time)
        {
            string format = @"{0:hh\:mm\:ss}";
            if (time.Days > 0)
            {
                format = @"{0:d\:hh\:mm\:ss}";
            }

            return String.Format(format, time);
        }

        /// <summary>
        /// 
        /// </summary>
        private void WriteReportHeaderRow(HtmlTextWriter writer, string header, string content, string style)
        {
            writer.RenderBeginTag(HtmlTextWriterTag.Tr);
            writer.AddAttribute(HtmlTextWriterAttribute.Style, style);
            writer.AddAttribute(HtmlTextWriterAttribute.Width, "50%");
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.Write(header);
            writer.RenderEndTag();
            writer.AddAttribute(HtmlTextWriterAttribute.Width, "50%");
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.Write(content);
            writer.RenderEndTag();
            writer.RenderEndTag();
        }

        /// <summary>
        /// 
        /// </summary>
        private void WriteUserStats(HtmlTextWriter writer, IList<UserSummary> userSummaries)
        {
            // Order the users by total time and take the 10 worst.
            IEnumerable<UserSummary> worstUsers = userSummaries.OrderByDescending(item => item.TotalTime).Take(NumInlineUsers);

            String defaultStyle = String.Format("background-color: {0}; color: {1}", c_defaultBackgroundColour, c_defaultForegroundColour);

            writer.AddAttribute(HtmlTextWriterAttribute.Border, "1");
            writer.AddAttribute(HtmlTextWriterAttribute.Width, "100%");
            writer.RenderBeginTag(HtmlTextWriterTag.Table);
            {
                // Section Header
                writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                writer.AddAttribute(HtmlTextWriterAttribute.Colspan, "6");
                writer.AddAttribute(HtmlTextWriterAttribute.Align, "center");
                writer.AddAttribute(HtmlTextWriterAttribute.Style, String.Format("{0}; font-size: 18pt", defaultStyle));
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write(String.Format("Top {0} Users Based on Total Time", worstUsers.Count()));
                writer.RenderEndTag();
                writer.RenderEndTag();

                // Header row
                writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                {
                    WriteReportTableColumnHeader(writer, "Username", defaultStyle);
                    WriteReportTableColumnHeader(writer, "Total Time", defaultStyle);
                    WriteReportTableColumnHeader(writer, "Export Count", defaultStyle);
                    WriteReportTableColumnHeader(writer, "Success Ratio", defaultStyle);
                    WriteReportTableColumnHeader(writer, "Average Total Time", defaultStyle);
                    WriteReportTableColumnHeader(writer, "Max Total Time", defaultStyle);
                }
                writer.RenderEndTag();

                // Output the worst users
                foreach (UserSummary userSummary in worstUsers)
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                    {
                        WriteReportTableColumn(writer, userSummary.Username);
                        WriteReportTableColumn(writer, GenerateTimeSpanText(TimeSpan.FromSeconds(userSummary.TotalTime)));
                        WriteReportTableColumn(writer, userSummary.TotalCount.ToString());
                        WriteReportTableColumn(writer, String.Format("{0:0.##}", userSummary.SuccessRatio));
                        WriteReportTableColumn(writer, GenerateTimeSpanText(TimeSpan.FromSeconds(userSummary.AverageTime)));
                        WriteReportTableColumn(writer, GenerateTimeSpanText(TimeSpan.FromSeconds(userSummary.MaxTime)));
                    }
                    writer.RenderEndTag();
                }
            }
            writer.RenderEndTag();
        }

        /// <summary>
        /// 
        /// </summary>
        private void WriteReportTableColumnHeader(HtmlTextWriter writer, string content, string style)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Style, style);
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.Write(content);
            writer.RenderEndTag();
        }

        /// <summary>
        /// 
        /// </summary>
        private void WriteReportTableColumn(HtmlTextWriter writer, string content)
        {
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.Write(content);
            writer.RenderEndTag();
        }

        /// <summary>
        /// 
        /// </summary>
        private void WriteSectionStats(HtmlTextWriter writer, IList<SectionSummary> sectionSummaries)
        {
            // Order the sections by total time and take the 10 worst.
            IEnumerable<SectionSummary> worstSections = sectionSummaries.OrderByDescending(item => item.AvgTotalTime).Take(NumInlineSections);

            String defaultStyle = String.Format("background-color: {0}; color: {1}", c_defaultBackgroundColour, c_defaultForegroundColour);

            writer.AddAttribute(HtmlTextWriterAttribute.Border, "1");
            writer.AddAttribute(HtmlTextWriterAttribute.Width, "100%");
            writer.RenderBeginTag(HtmlTextWriterTag.Table);
            {
                // Section header
                writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                writer.AddAttribute(HtmlTextWriterAttribute.Colspan, "11");
                writer.AddAttribute(HtmlTextWriterAttribute.Align, "center");
                writer.AddAttribute(HtmlTextWriterAttribute.Style, String.Format("{0}; font-size: 18pt", defaultStyle));
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write(String.Format("Worst {0} Sections Based on Average Total Time", worstSections.Count()));
                writer.RenderEndTag();
                writer.RenderEndTag();

                // Header rows
                writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                {
                    WriteReportTableColumnHeader(writer, "Section", defaultStyle);
                    WriteReportTableColumnHeader(writer, "Times Exported", defaultStyle);
                    WriteReportTableColumnHeader(writer, "Success Ratio", defaultStyle);
                    WriteReportTableColumnHeader(writer, "Average Total Time", defaultStyle);
                    WriteReportTableColumnHeader(writer, "Max Total Time", defaultStyle);

                    WriteReportTableColumnHeader(writer, "Export Success Ratio", defaultStyle);
                    WriteReportTableColumnHeader(writer, "Average Export Time", defaultStyle);
                    WriteReportTableColumnHeader(writer, "Max Export Time", defaultStyle);

                    WriteReportTableColumnHeader(writer, "Build Success Ratio", defaultStyle);
                    WriteReportTableColumnHeader(writer, "Average Build Time", defaultStyle);
                    WriteReportTableColumnHeader(writer, "Max Build Time", defaultStyle);
                }
                writer.RenderEndTag();

                // Output each of the users
                foreach (SectionSummary sectionSummary in worstSections)
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                    {
                        WriteReportTableColumn(writer, sectionSummary.SectionName);
                        WriteReportTableColumn(writer, sectionSummary.TotalCount.ToString());
                        WriteReportTableColumn(writer, String.Format("{0:0.##}", sectionSummary.SuccessRatio));
                        WriteReportTableColumn(writer, GenerateTimeSpanText(TimeSpan.FromSeconds(sectionSummary.AvgTotalTime)));
                        WriteReportTableColumn(writer, GenerateTimeSpanText(TimeSpan.FromSeconds(sectionSummary.MaxTotalTime)));

                        WriteReportTableColumn(writer, String.Format("{0:0.##}", sectionSummary.SectionExportSuccessRatio));
                        WriteReportTableColumn(writer, GenerateTimeSpanText(TimeSpan.FromSeconds(sectionSummary.AvgSectionExportTime)));
                        WriteReportTableColumn(writer, GenerateTimeSpanText(TimeSpan.FromSeconds(sectionSummary.MaxSectionExportTime)));
                        
                        WriteReportTableColumn(writer, String.Format("{0:0.##}", sectionSummary.BuildSuccessRatio));
                        WriteReportTableColumn(writer, GenerateTimeSpanText(TimeSpan.FromSeconds(sectionSummary.AvgBuildTime.HasValue ? sectionSummary.AvgBuildTime.Value : 0)));
                        WriteReportTableColumn(writer, GenerateTimeSpanText(TimeSpan.FromSeconds(sectionSummary.MaxBuildTime.HasValue ? sectionSummary.MaxBuildTime.Value : 0)));
                    }
                    writer.RenderEndTag();
                }
            }
            writer.RenderEndTag();
        }

        /// <summary>
        /// 
        /// </summary>
        private void WriteExportStats(HtmlTextWriter writer, IList<ExportDetails> exportDetails)
        {
            // Order the sections by total time and take the 10 worst.
            IEnumerable<ExportDetails> worstExports = exportDetails.OrderByDescending(item => item.TotalTime).Take(NumInlineExports);

            String defaultStyle = String.Format("background-color: {0}; color: {1}", c_defaultBackgroundColour, c_defaultForegroundColour);

            writer.AddAttribute(HtmlTextWriterAttribute.Border, "1");
            writer.AddAttribute(HtmlTextWriterAttribute.Width, "100%");
            writer.RenderBeginTag(HtmlTextWriterTag.Table);
            {
                // Section header
                writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                writer.AddAttribute(HtmlTextWriterAttribute.Colspan, "10");
                writer.AddAttribute(HtmlTextWriterAttribute.Align, "center");
                writer.AddAttribute(HtmlTextWriterAttribute.Style, String.Format("{0}; font-size: 18pt", defaultStyle));
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write(String.Format("Worst {0} Exports of the Day", worstExports.Count()));
                writer.RenderEndTag();
                writer.RenderEndTag();

                // Header rows
                writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                {
                    WriteReportTableColumnHeader(writer, "User", defaultStyle);
                    WriteReportTableColumnHeader(writer, "Total Time", defaultStyle);
                    WriteReportTableColumnHeader(writer, "Export Type", defaultStyle);
                    WriteReportTableColumnHeader(writer, "Success", defaultStyle);
                    WriteReportTableColumnHeader(writer, "Tools Version", defaultStyle);

                    WriteReportTableColumnHeader(writer, "Sections Exported", defaultStyle);
                    WriteReportTableColumnHeader(writer, "Export Time", defaultStyle);
                    WriteReportTableColumnHeader(writer, "Build Time", defaultStyle);

                }
                writer.RenderEndTag();

                // Output each of the users
                foreach (ExportDetails export in worstExports)
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                    {
                        WriteReportTableColumn(writer, export.Username);
                        WriteReportTableColumn(writer, GenerateTimeSpanText(TimeSpan.FromSeconds(export.TotalTime)));
                        WriteReportTableColumn(writer, export.ExportType);
                        WriteReportTableColumn(writer, (export.Success.HasValue ? export.Success.ToString() : "False"));
                        WriteReportTableColumn(writer, (export.ToolsVersion.HasValue ? export.ToolsVersion.ToString() : ""));

                        String sectionsText;
                        if (export.SectionsExported.Count == 1)
                        {
                            sectionsText = export.SectionsExported.First();
                        }
                        else if (export.SectionsExported.Count > 1)
                        {
                            sectionsText = String.Format("multiple ({0})", export.SectionsExported.Count);
                        }
                        else
                        {
                            sectionsText = "none";
                        }
                        WriteReportTableColumn(writer, sectionsText);

                        IEnumerable<ExportDetailsSubStep> sectionExportSubSteps =
                            export.SubSteps.Where(item => item.Name.EndsWith("section export"));
                        
                        TimeSpan exportTime = TimeSpan.FromSeconds(sectionExportSubSteps.Sum(item => (double)item.Time));
                        WriteReportTableColumn(writer, GenerateTimeSpanText(exportTime));

                        ExportDetailsSubStep buildSubStep = export.SubSteps.FirstOrDefault(item => item.Name == "Image Build");
                        if (buildSubStep != null)
                        {
                            WriteReportTableColumn(writer, GenerateTimeSpanText(TimeSpan.FromSeconds(buildSubStep.Time)));
                        }
                        else
                        {
                            WriteReportTableColumn(writer, "");
                        }
                    }
                    writer.RenderEndTag();
                }
            }
            writer.RenderEndTag();
        }
        #endregion // Private Methods
    } // NightlyExportStatReport
}
