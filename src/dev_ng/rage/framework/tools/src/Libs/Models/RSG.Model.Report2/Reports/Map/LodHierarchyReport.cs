﻿using RSG.Model.Report;
using System.Linq;
using RSG.Base.Tasks;
using RSG.Model.Common.Map;
using System.IO;
using System.Collections.Generic;
using RSG.Base.Math;
using RSG.Model.Common;
using System;

namespace RSG.Model.Report.Reports.Map
{
    /// <summary>
    /// 
    /// </summary>
    public class LodHierarchyReport : CSVReport, IDynamicLevelReport
    {
        #region Fields
        private const string c_name = "Lod Hierarchy Report";

        private const string c_description = "Exports the selected levels lod hierarchies where 1 or more levels are too close too another into a .csv file";

        private ITask m_task;
        #endregion
        
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public LodHierarchyReport()
            : base(c_name, c_description)
        {
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (m_task == null)
                {
                    CompositeTask task = new CompositeTask("Generate Report");
                    task.AddSubTask(new ActionTask("Loading data", (context, progress) => EnsureDataLoaded((DynamicLevelReportContext)context, progress)), 50);
                    task.AddSubTask(new ActionTask("Generating report", (context, progress) => GenerateReport((DynamicLevelReportContext)context, progress)));
                    m_task = task;
                }
                return m_task;
            }
        }

        /// <summary>
        /// List of stats that are required to generate this report.
        /// </summary>
        public IEnumerable<StreamableStat> RequiredStats
        {
            get { return null; }
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual int MinimumDistance
        {
            get { return 50; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void EnsureDataLoaded(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            IMapHierarchy hierarchy = context.Level.MapHierarchy;

            if (hierarchy != null)
            {
                float increment = 1.0f / hierarchy.AllSections.Count();

                // Load all the map section data
                foreach (IMapSection section in hierarchy.AllSections)
                {
                    context.Token.ThrowIfCancellationRequested();

                    // Update the progress information
                    string message = string.Format("Loading {0}", section.Name);
                    progress.Report(new TaskProgress(increment, true, message));

                    section.RequestAllStatistics(false);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void GenerateReport(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            IMapHierarchy hierarchy = context.Level.MapHierarchy;

            if (hierarchy != null)
            {
                // Write out the report
                using (StreamWriter writer = new StreamWriter(Filename))
                {
                    WriteCsvHeader(writer);

                    foreach (KeyValuePair<IEntity, float> entity in GatherLodEntities(hierarchy))
                    {
                        WriteCsvEntity(writer, entity.Key, entity.Value);
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hierarchy"></param>
        /// <returns></returns>
        private IEnumerable<KeyValuePair<IEntity, float>> GatherLodEntities(IMapHierarchy hierarchy)
        {
            ISet<IEntity> encounteredEntities = new HashSet<IEntity>();
            foreach (IMapSection section in hierarchy.AllSections)
            {
                // Loop over all lod instances in the map that have children and test them.
                foreach (IEntity entity in section.ChildEntities.Where(
                    item => item.LodLevel != LodLevel.Hd &&
                        item.LodLevel != LodLevel.OrphanHd &&
                        item.LodLevel != LodLevel.Unknown &&
                        item.LodChildren.Count != 0))
                {
                    IEntity lodParent = entity.LodParent;

                    if (lodParent == null)
                    {
                        // When the lod parent is null the pivots converge and the visible
                        // range of this item is just the different in the lod distance between
                        // itself and any child.
                        float lodDistance = entity.LodDistance;
                        float childLodDistance = entity.LodChildren.First().LodDistance;
                        float difference = lodDistance - childLodDistance;
                        if (difference < this.MinimumDistance)
                        {
                            yield return new KeyValuePair<IEntity, float>(entity, difference);
                        }
                    }
                    else
                    {
                        System.Windows.Point pivot1 = new System.Windows.Point(lodParent.Position.X, lodParent.Position.Y);
                        System.Windows.Point pivot2 = new System.Windows.Point(entity.Position.X, entity.Position.Y);
                        double a = pivot1.X - pivot2.X;
                        double b = pivot1.Y - pivot2.Y;
                        double displacement = Math.Sqrt(a * a + b * b);
                        double x = entity.LodDistance - displacement - entity.LodChildren.First().LodDistance;
                        if (x < this.MinimumDistance)
                        {
                            yield return new KeyValuePair<IEntity, float>(entity, (float)x);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sw"></param>
        private void WriteCsvHeader(StreamWriter writer)
        {
            writer.WriteLine("Entity,Section,Parent Area,Grandparent Area, Lod Distance Difference to Parent , Lod Level,Lod Parent,Lod Children Count,Lod Distance");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="instance"></param>
        private void WriteCsvEntity(StreamWriter writer, IEntity entity, float difference)
        {
            IMapSection section = entity.Parent as IMapSection;
            IMapArea parentArea = section.ParentArea;
            IMapArea grandParentArea = (parentArea != null ? parentArea.ParentArea : null);

            string csvRecord = string.Format("{0},{1},{2},{3},", entity.Name, section.Name, parentArea != null ? parentArea.Name : "n/a", grandParentArea != null ? grandParentArea.Name : "n/a");
            csvRecord += string.Format("{0}m,", difference);
            csvRecord += string.Format("{0},{1},{2},{3},", entity.LodLevel, entity.LodParent != null ? entity.LodParent.Name : "n/a", entity.LodChildren.Count, entity.LodDistance);
            writer.WriteLine(csvRecord);
        }
        #endregion
    }
}
