﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.SceneXml;
using RSG.Base.Math;

namespace RSG.Model.Map
{
    /// <summary>
    /// 
    /// </summary>
    public class CarGen : AssetBase
    {
        #region Fields
        private ILevel m_level;
        private IMapContainer m_container;
        private Dictionary<int, Object> m_attributes;
        #endregion // Fields

        #region Properties
        /// <summary>
        /// Reference to the owner Level object.
        /// </summary>
        public ILevel Level
        {
            get { return m_level; }
            set { m_level = value; }
        }

        /// <summary>
        /// The immediate parent map container, or
        /// null if non exist
        /// </summary>
        public IMapContainer Container
        {
            get { return m_container; }
            set { m_container = value; }
        }

        /// <summary>
        /// The dictionary of attributes on this definition
        /// </summary>
        private Dictionary<int, Object> Attributes
        {
            get { return m_attributes; }
            set { m_attributes = value; }
        }

        /// <summary>
        /// Instance's position in world-space.
        /// </summary>
        public Vector3f Position
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsPolice
        {
            get { return GetAttributeValue(AttrNames.CARGEN_POLICE, false); }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsFire
        {
            get { return GetAttributeValue(AttrNames.CARGEN_FIRETRUCK, false); }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsAmbulance
        {
            get { return GetAttributeValue(AttrNames.CARGEN_AMBULANCE, false); }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsSpecificModel
        {
            get
            {
                string model = GetAttributeValue(AttrNames.CARGEN_MODEL, "");
                if (string.IsNullOrWhiteSpace(model))
                    return false;
                
                return true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string ModelName
        {
            get
            {
                return GetAttributeValue(AttrNames.CARGEN_MODEL, "");
            }
        }
        #endregion // Properties

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public CarGen(ObjectDef sceneObject, IMapContainer container)
        {
            this.m_container = container;
            this.m_level = container.Level;
            this.Position = sceneObject.NodeTransform.Translation;
            GetAttributesFromObject(sceneObject);
            GetNameFromObject(sceneObject);
        }
        #endregion // Constructor

        #region Object Overrides
        /// <summary>
        /// Make sure the name is returned on ToString
        /// </summary>
        public override String ToString()
        {
            return this.Name;
        }

        /// <summary>
        /// Gets the hash code for this level
        /// using the name property
        /// </summary>
        public override int GetHashCode()
        {
            return this.Name.GetHashCode();
        }
        #endregion // Object Overrides

        #region Methods
        /// <summary>
        /// Returns the attribute with the given identifier or the given
        /// default value if the attribute doesn't exist
        /// </summary>
        public T GetAttributeValue<T>(String attributeName, T defaultValue)
        {
            if (this.Attributes != null)
            {
                int hashCode = attributeName.ToLower().GetHashCode();
                if (this.Attributes.ContainsKey(hashCode))
                {
                    return (T)this.Attributes[hashCode];
                }
            }
            return defaultValue;
        }

        /// <summary>
        /// Returns true if and only if the attributes for this definition contains
        /// the given attribute name
        /// </summary>
        public Boolean HasAttribute(String attributeName)
        {
            if (this.Attributes != null)
            {
                int hashCode = attributeName.ToLower().GetHashCode();
                if (this.Attributes.ContainsKey(hashCode))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Gets the attributes from the given object
        /// </summary>
        private void GetAttributesFromObject(ObjectDef sceneObject)
        {
            m_attributes = new Dictionary<int, Object>(sceneObject.Attributes.Count);
            foreach (var attribute in sceneObject.Attributes)
            {
                m_attributes.Add(attribute.Key.ToLower().GetHashCode(), attribute.Value);
            }
        }

        /// <summary>
        /// Gets the name from the given object
        /// </summary>
        private void GetNameFromObject(ObjectDef sceneObject)
        {
            m_name = sceneObject.Name;
        }
        #endregion // Mathods
    } // CarGen
} // RSG.Model.Map
