﻿//---------------------------------------------------------------------------------------------
// <copyright file="DialogueVoice.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.Model
{
    using System;
    using System.Diagnostics;
    using System.Xml;
    using RSG.Editor;
    using RSG.Editor.Model;

    /// <summary>
    /// Represents a single voice that can be assigned to a dialogue character.
    /// </summary>
    [DebuggerDisplay("{_name}")]
    public sealed class DialogueVoice : ModelBase
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Name"/> property.
        /// </summary>
        private string _name;

        /// <summary>
        /// The private field used for the <see cref="Id"/> property.
        /// </summary>
        private Guid _id;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DialogueVoice"/> class.
        /// </summary>
        /// <param name="id">
        /// The static id for this voice that cannot be changed.
        /// </param>
        /// <param name="name">
        /// The name for this voice.
        /// </param>
        public DialogueVoice(Guid id, string name)
        {
            this._id = id;
            this._name = name;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="DialogueVoice"/> class.
        /// </summary>
        /// <param name="reader">
        /// Contains data used to initialise this instance.
        /// </param>
        public DialogueVoice(XmlReader reader)
        {
            if (reader == null)
            {
                throw new SmartArgumentNullException(() => reader);
            }

            this.Deserialise(reader);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the name for this voice.
        /// </summary>
        public string Name
        {
            get { return this._name; }
            set { this.SetProperty(ref this._name, value); }
        }

        /// <summary>
        /// Gets the id used by this voice. This cannot be changed once setup and is how the
        /// voice can be renamed without any data being lost or manipulated.
        /// </summary>
        public Guid Id
        {
            get { return this._id; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a deep copy of the current instance.
        /// </summary>
        /// <returns>
        /// A deep copy of the current System.Object.
        /// </returns>
        public override object DeepClone()
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Serialises the data contained within this instance onto the specified xml writer.
        /// </summary>
        /// <param name="writer">
        /// The xml writer this instance will be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                Debug.Assert(writer != null, "Unable to serialise into a null writer.");
                return;
            }

            writer.WriteElementString("Name", this._name);
            writer.WriteElementString("Id", this._id.ToString("D"));
        }

        /// <summary>
        /// Initialises this instance using the data contained within the specified xml reader.
        /// </summary>
        /// <param name="reader">
        /// The xml reader that contains the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() == XmlNodeType.Element)
            {
                if (String.Equals("Name", reader.Name))
                {
                    this._name = reader.ReadElementContentAsString();
                }
                else if (String.Equals("Id", reader.Name))
                {
                    string value = reader.ReadElementContentAsString();
                    if (!Guid.TryParseExact(value, "D", out this._id))
                    {
                        this._id = Guid.Empty;
                    }
                }

                reader.Skip();
            }

            if (reader.NodeType == XmlNodeType.EndElement)
            {
                reader.ReadEndElement();
            }
        }
        #endregion Methods
    } // RSG.Text.Model.DialogueVoice {Class}
} // RSG.Text.Model {Namespace}
