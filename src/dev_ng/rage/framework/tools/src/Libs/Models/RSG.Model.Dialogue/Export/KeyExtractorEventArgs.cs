﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Dialogue.Export
{
    /// <summary>
    /// Key extractor event arguments.
    /// </summary>
    public class KeyExtractorEventArgs : EventArgs
    {
        #region Public properties

        /// <summary>
        /// The conversation key.
        /// </summary>
        public string ConversationKey { get; private set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="conversationKey">Conversation key.</param>
        public KeyExtractorEventArgs(string conversationKey)
        {
            ConversationKey = conversationKey;
        }

        #endregion
    }
}
