﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.ConfigParser;
using RSG.Base.Configuration;
using RSG.ManagedRage;
using RSG.Model.Asset.Util;
using RSG.Model.Common;
using RSG.Model.Common.Util;

namespace RSG.Model.Vehicle
{
    /// <summary>
    /// Collection of vehicles that come from the local export data
    /// </summary>
    public class LocalVehicleCollection : VehicleCollectionBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public ConfigGameView GameView
        {
            get
            {
                if (m_gv == null)
                {
                    m_gv = new ConfigGameView();
                }
                return m_gv;
            }
        }
        private ConfigGameView m_gv;

        /// <summary>
        /// 
        /// </summary>
        public IConfig Config
        {
            get
            {
                if (m_config == null)
                {
                    m_config = ConfigFactory.CreateConfig();
                }
                return m_config;
            }
        }
        private IConfig m_config;

        /// <summary>
        /// The source folder for the vehicle data
        /// </summary>
        public string ExportDataPath
        {
            get;
            protected set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="exportDataPath"></param>
        public LocalVehicleCollection(string exportDataPath)
            : base()
        {
            ExportDataPath = exportDataPath;
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="statsToLoad"></param>
        public override void LoadStats(IEnumerable<StreamableStat> statsToLoad)
        {
            // Two different load paths.
            // 1) Loading "basic" stat from scenexml data.
            // 2) Loading platform stats data from rpf's.

            // Basic stats
            IEnumerable<StreamableStat> nonPlatformStats = statsToLoad.Where(item => item != StreamableVehicleStat.PlatformStats);

            // Load each weapon individually.
            foreach (IVehicle vehicle in Vehicles)
            {
                if (!vehicle.AreStatsLoaded(nonPlatformStats))
                {
                    vehicle.LoadStats(nonPlatformStats);
                }
            }

            // Platform stats.
            if (statsToLoad.Contains(StreamableVehicleStat.PlatformStats) && !ArePlatformStatsLoaded())
            {
                // Get the path to the independent rpf file
                List<ContentNode> contentNodes = GameView.Content.Root.FindAll("vehiclesdirectory");

                String independentPath = null;
                if (contentNodes.Count > 0)
                {
                    independentPath = LocalLoadUtil.GeneratePathForNode(contentNodes[0]);
                }

                // Open up the pack files
                IDictionary<RSG.Platform.Platform, Packfile> packFiles = LocalLoadUtil.OpenPackFiles(independentPath, GameView, Config);

                // Go over all the weapons telling them to load the platform data
                foreach (IVehicle vehicle in Vehicles)
                {
                    (vehicle as LocalVehicle).LoadPlatformStatsFromExportData(packFiles);
                }

                // Close all the pack files we opened
                LocalLoadUtil.ClosePackFiles(packFiles);
            }
        }
        #endregion // Overrides

        #region Private Methods
        /// <summary>
        /// Helper function that checks whether all the vehicles in a collection have their platform stats loaded
        /// </summary>
        /// <param name="vehicles"></param>
        /// <returns></returns>
        private bool ArePlatformStatsLoaded()
        {
            bool allLoaded = true;

            foreach (IVehicle vehicle in Vehicles)
            {
                if (!vehicle.IsStatLoaded(StreamableVehicleStat.PlatformStats))
                {
                    allLoaded = false;
                    break;
                }
            }

            return allLoaded;
        }
        #endregion // Private Methods

        #region IDisposable Methods
        /// <summary>
        /// 
        /// </summary>
        public override void Dispose()
        {
            base.Dispose();

            if (m_gv != null)
            {
                m_gv.Dispose();
            }
        }
        #endregion // IDisposable Methods
    } // ExportDataVehicleCollection
}
