﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common.RadioStation
{
    /// <summary>
    /// Interface for radio station assets.
    /// </summary>
    public interface IRadioStation : IAsset
    {
        /// <summary>
        /// Friendly name for this radio station.
        /// </summary>
        String FriendlyName { get; set; }
    } // IRadioStation
}
