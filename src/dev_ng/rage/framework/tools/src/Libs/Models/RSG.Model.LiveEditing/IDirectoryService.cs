﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;

namespace RSG.Model.LiveEditing
{
    /// <summary>
    /// 
    /// </summary>
    public interface IDirectoryService : IHasAssetChildren
    {
        /// <summary>
        /// The Uri to get information about this item
        /// </summary>
        Uri Url { get; }
    } // IDirectoryService
}
