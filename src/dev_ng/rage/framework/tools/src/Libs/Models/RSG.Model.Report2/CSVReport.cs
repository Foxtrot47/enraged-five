﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using System.ComponentModel;
using System.IO;
using RSG.Base.Attributes;
using System.Reflection;

namespace RSG.Model.Report
{
    /// <summary>
    /// CSVReport - A report that generates a CSV file
    /// </summary>
    public abstract class CSVReport : Report, IReportFileProvider
    {
        #region Properties
        /// <summary>
        /// Filter used for the save dialog
        /// </summary>
        public string Filter
        {
            get
            {
                return "CSV (Comma delimited)|*.csv";
            }
        }

        /// <summary>
        /// Extensions that this report support
        /// </summary>
        public string SupportedExtension
        {
            get
            {
                return ".csv";
            }
        }

        /// <summary>
        /// Filename specifying where a report should be output
        /// </summary>
        public string Filename
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="desc"></param>
        public CSVReport()
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="desc"></param>
        public CSVReport(string name, string desc)
            : base(name, desc)
        {
        }
        #endregion // Constructor(s)

        #region Helper Methods
        /// <summary>
        /// Creates a stream of CSV data from a list of items, optionally outputting a header.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <param name="outputHeader"></param>
        /// <returns></returns>
        protected Stream GenerateCSVStream<T>(IEnumerable<T> items, bool includeHeader = true) where T : class
        {
            Stream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);

            // Check if we should output a header
            if (includeHeader)
            {
                OutputCsvHeader<T>(writer);
            }

            // Output each line
            foreach (T item in items)
            {
                OutputCsvLine<T>(writer, item);
            }

            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        /// <summary>
        /// Outputs a header line that would be included in a CSV file by iterating over all properties in the passed in type
        /// and using either the properties associated friendly name attribute or the properties name itself.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="writer"></param>
        private void OutputCsvHeader<T>(StreamWriter writer) where T : class
        {
            // Get the type
            Type type = typeof(T);
            StringBuilder sb = new StringBuilder();

            // Loop over all of the class's properties.
            foreach (PropertyInfo info in type.GetProperties())
            {
                if (sb.Length > 0)
                {
                    sb.Append(",");
                }

                // Use the friendly name if one exists, otherwise use the property's name
                FriendlyNameAttribute[] attributes = info.GetCustomAttributes(typeof(FriendlyNameAttribute), false) as FriendlyNameAttribute[];
                if (attributes.Length > 0)
                {
                    sb.Append(attributes[0].FriendlyName);
                }
                else
                {
                    sb.Append(info.Name);
                }
            }

            writer.WriteLine(sb.ToString());
        }

        /// <summary>
        /// Outputs a line of csv for the passed in items.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="writer"></param>
        /// <param name="values"></param>
        private void OutputCsvLine<T>(StreamWriter writer, T item) where T : class
        {
            // Get the type
            Type type = typeof(T);

            // Construct the line's data from the values passed in
            StringBuilder sb = new StringBuilder();

            // Loop over all of the class's properties.
            foreach (PropertyInfo info in type.GetProperties())
            {
                object value = info.GetValue(item, null);
                if (value != null)
                {
                    // Fucking timespans don't display in excel correctly :/
                    if (value is TimeSpan)
                    {
                        sb.Append(TruncateTimeSpan((TimeSpan)value));
                    }
                    else
                    {
                        sb.Append(value);
                    }
                }
                sb.Append(",");
            }

            writer.WriteLine(sb.ToString().TrimEnd(new char[] { '.' }));
        }

        /// <summary>
        /// Gets rid of any milliseconds that the time span has so that excel can correctly display those values.
        /// </summary>
        /// <param name="ts"></param>
        /// <returns></returns>
        protected TimeSpan TruncateTimeSpan(TimeSpan ts)
        {
            if (ts.Milliseconds > 0)
            {
                return new TimeSpan(ts.Days, ts.Hours, ts.Minutes, ts.Seconds, 0);
            }
            else
            {
                return ts;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="filename"></param>
        protected void SaveStreamToFile(Stream stream, string filename)
        {
            using (Stream fileStream = new FileStream(filename, FileMode.Create))
            {
                stream.Position = 0;
                stream.CopyTo(fileStream);
            }
        }

        #endregion // Helper Methods
    } // CSVReport
} // RSG.Model.Report namespace
