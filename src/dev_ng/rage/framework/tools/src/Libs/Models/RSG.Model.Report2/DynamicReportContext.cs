﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Tasks;
using RSG.Base.ConfigParser;
using System.Threading;
using RSG.Base.Configuration;

namespace RSG.Model.Report
{
    /// <summary>
    /// Context to use when generating a dynamic report.
    /// </summary>
    public class DynamicReportContext : TaskContext
    {
        #region Properties
        /// <summary>
        /// Config object to use when generating the report.
        /// </summary>
        //TODO: Change this to an RSG.Base.Configuration object.  Only DynamicLevelReportContext needs the pipeline's content tree.
        public ConfigGameView GameView { get; private set; }

        /// <summary>
        /// Branch to use for generating the report.
        /// </summary>
        public IBranch Branch { get; private set; }

        /// <summary>
        /// Core branch for the project.
        /// </summary>
        public IBranch CoreBranch { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public DynamicReportContext(ConfigGameView gv, IConfig config)
            : this(CancellationToken.None, gv, config)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="token"></param>
        public DynamicReportContext(CancellationToken token, ConfigGameView gv, IConfig config)
            : base(token)
        {
            GameView = gv;
            Branch = config.Project.DefaultBranch;
            CoreBranch = config.CoreProject.DefaultBranch;
        }
        #endregion // Constructor(s)
    } // DynamicReportContext
}
