﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.ConfigParser;
using RSG.Model.Common;
using RSG.Base.Logging;
using RSG.Base.Collections;
using RSG.Base.Tasks;
using RSG.Model.Common.Map;
using RSG.Base.Extensions;

namespace RSG.Model.Report.Reports.Map
{
    /// <summary>
    /// 
    /// </summary>
    public class ShaderReport : CSVReport, IDynamicLevelReport
    {
        #region Constants
        private const String c_name = "Shader Report";
        private const String c_description = "Exports the selected levels shader usage into a .csv file";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (m_task == null)
                {
                    CompositeTask task = new CompositeTask("Generate Report");
                    task.AddSubTask(new ActionTask("Loading data", (context, progress) => EnsureDataLoaded((DynamicLevelReportContext)context, progress)), 50);
                    task.AddSubTask(new ActionTask("Generating report", (context, progress) => GenerateReport((DynamicLevelReportContext)context, progress)));
                    m_task = task;
                }
                return m_task;
            }
        }
        private ITask m_task;

        /// <summary>
        /// List of stats that are required to generate this report.
        /// </summary>
        public IEnumerable<StreamableStat> RequiredStats
        {
            get { return null; }
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public ShaderReport()
            : base(c_name, c_description)
        {
        }
        #endregion // Constructor(s)

        #region Task Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void EnsureDataLoaded(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            IMapHierarchy hierarchy = context.Level.MapHierarchy;

            if (hierarchy != null)
            {
                float increment = 1.0f / hierarchy.AllSections.Count();

                // Load all the map section data
                foreach (IMapSection section in hierarchy.AllSections)
                {
                    context.Token.ThrowIfCancellationRequested();

                    // Update the progress information
                    string message = String.Format("Loading {0}", section.Name);
                    progress.Report(new TaskProgress(increment, true, message));

                    section.RequestAllStatistics(false);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void GenerateReport(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            IMapHierarchy hierarchy = context.Level.MapHierarchy;

            if (hierarchy != null)
            {
                string directory = Path.GetDirectoryName(Filename);
                string filename = Path.GetFileNameWithoutExtension(Filename);

                string exteriorFilename = Path.Combine(directory, filename + ".csv");
                string interiorFilename = Path.Combine(directory, filename + "_interiors.csv");

                progress.Report(new TaskProgress(0.0, "Processing exterior entities."));
                ExportExteriorShaderCSVReport(exteriorFilename, hierarchy, context.GameView);
                progress.Report(new TaskProgress(0.5, "Processing interior entities."));
                ExportInteriorShaderCSVReport(interiorFilename, hierarchy, context.GameView);
                progress.Report(new TaskProgress(1.0, "Complete."));
            }
        }
        #endregion // Task Methods

        #region Private Methods
        /// <summary>
        /// Exports all exterior shaders
        /// </summary>
        /// <param name="filepath"></param>
        /// <param name="level"></param>
        /// <param name="gv"></param>
        private void ExportExteriorShaderCSVReport(string filepath, IMapHierarchy hierarchy, ConfigGameView gv)
        {
            // Gather the data we are interested in.
            // (Three separate maps of shaders to where those shaders are being used.)
            IDictionary<string, ISet<IEntity>> entityMap = new Dictionary<string, ISet<IEntity>>();
            IDictionary<string, ISet<IMapArchetype>> archetypeMap = new Dictionary<string, ISet<IMapArchetype>>();
            IDictionary<string, ISet<IMapSection>> sectionsMap = new Dictionary<string, ISet<IMapSection>>();

            // Process the hierarchy to get the shader information
            foreach (IMapSection section in hierarchy.AllSections)
            {
                foreach (IMapArchetype archetype in section.NonInteriorArchetypes)
                {
                    foreach (IEntity entity in archetype.Entities)
                    {
                        ProcessEntity(entity, section, ref entityMap, ref archetypeMap, ref sectionsMap);
                    }
                }
            }

            // Now write the retrieved data to a csv file
            WriteCsvFile(filepath, entityMap, archetypeMap, sectionsMap);
        }

        /// <summary>
        /// Exports all interior shaders
        /// </summary>
        /// <param name="filepath"></param>
        /// <param name="level"></param>
        /// <param name="gv"></param>
        private void ExportInteriorShaderCSVReport(string filepath, IMapHierarchy hierarchy, ConfigGameView gv)
        {
            // Gather the data we are interested in.
            // (Three separate maps of shaders to where those shaders are being used.)
            IDictionary<string, ISet<IEntity>> entityMap = new Dictionary<string, ISet<IEntity>>();
            IDictionary<string, ISet<IMapArchetype>> archetypeMap = new Dictionary<string, ISet<IMapArchetype>>();
            IDictionary<string, ISet<IMapSection>> sectionsMap = new Dictionary<string, ISet<IMapSection>>();

            // Process the hierarchy to get the shader information
            foreach (IMapSection section in hierarchy.AllSections)
            {
                foreach (IInteriorArchetype archetype in section.InteriorArchetypes)
                {
                    foreach (IEntity interiorEntity in archetype.ChildEntities)
                    {
                        ProcessEntity(interiorEntity, section, ref entityMap, ref archetypeMap, ref sectionsMap);
                    }

                    foreach (IRoom room in archetype.Rooms)
                    {
                        foreach (IEntity roomEntity in room.ChildEntities)
                        {
                            ProcessEntity(roomEntity, section, ref entityMap, ref archetypeMap, ref sectionsMap);
                        }
                    }
                }
            }

            // Now write the retrieved data to a csv file
            WriteCsvFile(filepath, entityMap, archetypeMap, sectionsMap);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="section"></param>
        /// <param name="archetypeMap"></param>
        /// <param name="sectionsMap"></param>
        private void ProcessEntity(IEntity entity, IMapSection section, ref IDictionary<string, ISet<IEntity>> entityMap, ref IDictionary<string, ISet<IMapArchetype>> archetypeMap, ref IDictionary<string, ISet<IMapSection>> sectionsMap)
        {
            IMapArchetype archetype = entity.ReferencedArchetype as IMapArchetype;
            if (archetype != null)
            {
                // Iterate over the archetype's shaders adding entries into both maps
                foreach (IShader shader in archetype.Shaders)
                {
                    string shaderName = shader.Name.ToLower();

                    if (!entityMap.ContainsKey(shaderName))
                    {
                        entityMap[shaderName] = new HashSet<IEntity>();
                    }
                    entityMap[shaderName].Add(entity);

                    if (!archetypeMap.ContainsKey(shaderName))
                    {
                        archetypeMap[shaderName] = new HashSet<IMapArchetype>();
                    }
                    archetypeMap[shaderName].Add(archetype);

                    if (!sectionsMap.ContainsKey(shaderName))
                    {
                        sectionsMap[shaderName] = new HashSet<IMapSection>();
                    }
                    sectionsMap[shaderName].Add(section);
                }
            }
        }

        /// <summary>
        /// Writes out a CSV file based on the based in shader information
        /// </summary>
        /// <param name="filepath"></param>
        /// <param name="definitionsMap"></param>
        /// <param name="sectionsMap"></param>
        private void WriteCsvFile(string filepath, IDictionary<string, ISet<IEntity>> entityMap, IDictionary<string, ISet<IMapArchetype>> archetypeMap, IDictionary<string, ISet<IMapSection>> sectionsMap)
        {
            using (StreamWriter sw = new StreamWriter(filepath))
            {
                WriteCsvHeader(sw);

                foreach (string shader in archetypeMap.OrderBy(item => item.Key).Select(item => item.Key))
                {
                    int archetypeCount = archetypeMap[shader].Count;
                    int entityCount = entityMap[shader].Count;

                    ISet<string> sections = new HashSet<string>();
                    ISet<string> users = new HashSet<string>();

                    if (sectionsMap.ContainsKey(shader))
                    {
                        sections.AddRange(sectionsMap[shader].Select(item => item.Name.ToLower()));
                        users.AddRange(sectionsMap[shader].Select(item => item.LastExportUser.ToLower()));
                    }

                    List<string> sortedSections = sections.ToList();
                    List<string> sortedUsers = users.ToList();
                    sortedSections.Sort();
                    sortedUsers.Sort();

                    string csvRecord = string.Format("{0},{1},{2},{3},{4},{5},{6}",
                        shader, archetypeCount, entityCount,
                        sortedSections.Count,
                        (sortedSections.Any() ? String.Join(";", sortedSections) : "n/a"),
                        sortedUsers.Count,
                        (sortedUsers.Any() ? String.Join(";", sortedUsers) : "n/a")
                        );
                    sw.WriteLine(csvRecord);
                }
            }
        }

        /// <summary>
        /// Writes out the header for the csv file
        /// </summary>
        /// <param name="sw"></param>
        /// <param name="milo"></param>
        private void WriteCsvHeader(StreamWriter sw)
        {
            sw.WriteLine("Shader Name,Archetype Usage Count,Entity Usage Count,Section Count,Sections,User Count,Users");
        }
        #endregion // Private Methods
    } // ObjectCSVReport
} // RSG.Model.Report.Reports
