﻿// --------------------------------------------------------------------------------------------
// <copyright file="EnumerationSortMethod.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions
{
    /// <summary>
    /// Specifies the different types of ways the enumeration items can be sorted.
    /// </summary>
    public enum EnumerationSortMethod
    {
        /// <summary>
        /// No sorting is added so the items are viewed as they are in the PSC definition.
        /// </summary>
        None,

        /// <summary>
        /// The enumeration items are sorted alphabetically.
        /// </summary>
        Alphabetical,

        /// <summary>
        /// The enumeration items are sorted based on their constant value.
        /// </summary>
        Value,

        /// <summary>
        /// The default way the items are sorted.
        /// </summary>
        Default = None,
    } // RSG.Metadata.Model.Definitions.EnumerationSortMethod {Enum}
} // RSG.Metadata.Model.Definitions {Namespace}
