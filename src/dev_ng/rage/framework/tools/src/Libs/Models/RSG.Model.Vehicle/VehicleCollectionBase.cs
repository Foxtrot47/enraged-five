﻿using System;
using System.Collections;
using System.Collections.Generic;
using RSG.Base.Collections;
using RSG.Model.Common;

namespace RSG.Model.Vehicle
{
    /// <summary>
    /// Collection of vehicles
    /// </summary>
    public abstract class VehicleCollectionBase : AssetBase, IVehicleCollection
    {
        #region Properties and associated member data
        /// <summary>
        /// The collection of levels that are currently
        /// loaded in this dictionary
        /// </summary>
        public ObservableCollection<IVehicle> Vehicles
        {
            get;
            protected set;
        }
        #endregion // Properties and associated member data
    
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public VehicleCollectionBase()
            : base("Vehicles")
        {
            Vehicles = new ObservableCollection<IVehicle>();
        }
        #endregion // Constructor(s)

        #region IVehicleCollection Implementation
        /// <summary>
        /// 
        /// </summary>
        public void LoadAllStats()
        {
            LoadStats(StreamableStatUtils.GetValuesAssignedToProperties(typeof(Vehicle)));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="statsToLoad"></param>
        public abstract void LoadStats(IEnumerable<StreamableStat> statsToLoad);
        #endregion // IVehicleCollection Partial Implementation
    
        #region IEnumerable<IVehicle> Implementation
        /// <summary>
        /// Loops through the levels and returns each one in turn
        /// </summary>
        IEnumerator<IVehicle> IEnumerable<IVehicle>.GetEnumerator()
        {
            foreach (IVehicle vehicle in this.Vehicles)
            {
                yield return vehicle;
            }
        }
    
        /// <summary>
        /// Returns the Enumerator for this object
        /// </summary>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerator)this);
        }
        #endregion // IEnumerable<IVehicle> Implementation
    
        #region ICollection<IVehicle> Implementation
        #region ICollection<IVehicle> Properties
        /// <summary>
        /// Returns whether the collection is read-only
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }
    
        /// <summary>
        /// Return number of items in the set.
        /// </summary>
        public int Count
        {
            get
            {
                return Vehicles.Count;
            }
        }
        #endregion // ICollection<IVehicle> Properties
    
        #region ICollection<IVehicle> Methods
        /// <summary>
        /// Add a vehicle to the collection.
        /// </summary>
        /// <param name="item"></param>
        public void Add(IVehicle item)
        {
            Vehicles.Add(item);
        }
    
        /// <summary>
        /// Removes all vehicles from the collection.
        /// </summary>
        public void Clear()
        {
            Vehicles.Clear();
        }
    
        /// <summary>
        /// Determine whether the collection contains a specific vehicle
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Contains(IVehicle item)
        {
            return Vehicles.Contains(item);
        }
    
        /// <summary>
        /// Copy items in the collection to an array, starting at a particular index.
        /// </summary>
        /// <param name="output"></param>
        /// <param name="index"></param>
        public void CopyTo(IVehicle[] output, int index)
        {
            Vehicles.CopyTo(output, index);
        }
    
        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        public bool Remove(IVehicle item)
        {
            return Vehicles.Remove(item);
        }
        #endregion // ICollection<IVehicle> Methods
        #endregion // ICollection<IVehicle> Implementation

        #region IDisposable Implementation
        /// <summary>
        /// 
        /// </summary>
        public virtual void Dispose()
        {
        }
        #endregion // IDisposable Implementation
    } // VehicleCollectionBase
}
