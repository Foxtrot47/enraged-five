﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Web.UI;
using RSG.Model.Statistics.Captures;
using System.Windows.Media.Imaging;
using System.Windows;
using System.Windows.Media;
using System.Windows.Controls.DataVisualization.Charting;
using System.Windows.Data;

namespace RSG.Model.Report.Reports.GameStatsReports.Tests
{
    /// <summary>
    /// 
    /// </summary>
    public class CpuSmokeTest : SmokeTestBase
    {
        #region Constants
        private const string c_name = "Cpu";
        private const int c_lineGraphWidth = 1000;
        private const int c_lineGraphHeight = 500;
        private const int c_pieGraphWidth = 400;
        private const int c_pieGraphHeight = 300;
        #endregion // Constants

        #region Private Classes
        /// <summary>
        /// Contains information about a single cpu stat
        /// </summary>
        public class CpuInfo : IComparable<CpuInfo>
        {
            public CpuResult Result { get; private set; }
            public DateTime Timestamp { get; private set; }

            public CpuInfo(CpuResult result, DateTime time)
            {
                Result = result;
                Timestamp = time;
            }

            #region IComparable<CpuInfo> Interface
            /// <summary>
            /// Compare this ModificationInfo to another.
            /// </summary>
            /// <param name="other"></param>
            /// <returns></returns>
            public int CompareTo(CpuInfo other)
            {
                if (other == null)
                {
                    return 1;
                }

                return (Result.Average.CompareTo(other.Result.Average));
            }
            #endregion // IComparable<IVehicle> Interface
        }

        /// <summary>
        /// 
        /// </summary>
        public class CpuSetKey : IEquatable<CpuSetKey>
        {
            public string TestName { get; private set; }
            public string SetName { get; private set; }

            public CpuSetKey(string testName, string setName)
            {
                TestName = testName;
                SetName = setName;
            }

            public override string ToString()
            {
                return String.Format("{0} {1}", TestName, SetName);
            }

            public override bool Equals(object obj)
            {
                if (obj == null)
                {
                    return base.Equals(obj);
                }

                return ((obj is CpuSetKey) && Equals(obj as CpuSetKey));
            }

            public override int GetHashCode()
            {
                return (TestName.GetHashCode() + SetName.GetHashCode());
            }

            public bool Equals(CpuSetKey other)
            {
                if (other == null)
                {
                    return false;
                }

                return (TestName == other.TestName && SetName == other.SetName);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public class CpuMetricKey : CpuSetKey, IEquatable<CpuMetricKey>
        {
            public string MetricName { get; private set; }

            public CpuMetricKey(string testName, string setName, string metricName)
                : base(testName, setName)
            {
                MetricName = metricName;
            }

            public override string ToString()
            {
                return String.Format("{0} {1} {2}", TestName, SetName, MetricName);
            }

            public override bool Equals(object obj)
            {
                if (obj == null)
                {
                    return base.Equals(obj);
                }

                return ((obj is CpuMetricKey) && Equals(obj as CpuMetricKey));
            }

            public override int GetHashCode()
            {
                return (TestName.GetHashCode() + SetName.GetHashCode() + MetricName.GetHashCode());
            }

            public bool Equals(CpuMetricKey other)
            {
                if (other == null)
                {
                    return false;
                }

                return (TestName == other.TestName && SetName == other.SetName && MetricName == other.MetricName);
            }
        }
        #endregion // Private Classes

        #region Properties
        /// <summary>
        /// Fps info grouped by test name
        /// </summary>
        public IDictionary<CpuMetricKey, IList<CpuInfo>> HistoricalCpuStats
        {
            get;
            private set;
        }

        /// <summary>
        /// Mapping of [test names, set names] -> [cpu info list] for the latest build
        /// </summary>
        public IDictionary<CpuSetKey, IList<CpuInfo>> LatestBuildCpuStats
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public CpuSmokeTest()
            : base(c_name)
        {
        }
        #endregion // Constructor(s)

        #region SmokeTestBase Methods
        /// <summary>
        /// Executes the particular test
        /// </summary>
        /// <param name="testSessions">List of test sessions</param>
        public override void RunTest(IList<TestSession> testSessions)
        {
            base.RunTest(testSessions);

            // Gather the stats we are interested in
            LatestBuildCpuStats = GatherLatestBuildStats(testSessions.FirstOrDefault());
            HistoricalCpuStats = GatherGroupedStats(testSessions);
            
            // Check all the individual stats to see if there were any major changes
            foreach (KeyValuePair<CpuMetricKey, IList<CpuInfo>> pair in HistoricalCpuStats)
            {
                string testName = pair.Key.TestName;
                string setName = pair.Key.SetName;
                string metricName = pair.Key.MetricName;

                CpuInfo first = pair.Value.FirstOrDefault();
                CpuInfo next = null;
                if (pair.Value.Count > 1)
                {
                    next = pair.Value[1];
                }

                if (first != null && next != null && first.Result.Average != 0.0f && next.Result.Average != 0.0f)
                {
                    float averageGrowth = next.Result.Average / first.Result.Average;
                    if (averageGrowth < 0.95f)
                    {
                        Errors.Add(String.Format("{0} {1} {2} metric is slower by {3:0.##}%", testName, metricName, SmokeTestName.ToLower(), -(averageGrowth - 1.0) * 100));
                    }

                    if (averageGrowth > 1.05f)
                    {
                        Messages.Add(String.Format("{0} {1} {2} metric is faster by {3:0.##}%", testName, metricName, SmokeTestName.ToLower(), (averageGrowth - 1.0) * 100));
                    }
                }
            }
        }

        /// <summary>
        /// Generates test specific graphs for this smoke test
        /// </summary>
        /// <param name="test"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns>Null if the smoke test doesn't require a graph</returns>
        public override IList<GraphImage> GenerateGraphs(DateTime start, DateTime end)
        {
            IList<GraphImage> allGraphs = new List<GraphImage>();

            // Generate a couple of graphs for each  test
            foreach (KeyValuePair<CpuSetKey, IList<CpuInfo>> pair in LatestBuildCpuStats)
            {
                string testName = pair.Key.TestName;
                string setName = pair.Key.SetName;

                // Create the pie graph
                Stream imageStream = GeneratePieGraph(testName, setName, pair.Value);
                string graphName = String.Format("cpu_pie_{0}_{1}", testName.Replace("(", "").Replace(")", "").Replace(" ", ""), setName);
                GraphImage image = new GraphImage(graphName, imageStream);

                if (!PerTestGraphs.ContainsKey(testName))
                {
                    PerTestGraphs.Add(testName, new List<GraphImage>());
                }

                PerTestGraphs[testName].Add(image);
                allGraphs.Add(image);
            }

            // Create the historical line graph
            foreach (var pair in HistoricalCpuStats.GroupBy(pair => new CpuSetKey(pair.Key.TestName, pair.Key.SetName)))
            {
                string testName = pair.Key.TestName;
                string setName = pair.Key.SetName;

                // Create the line graph
                Stream imageStream = GenerateLineGraph(testName, setName, pair);
                string graphName = String.Format("cpu_line_{0}_{1}", testName.Replace("(", "").Replace(")", "").Replace(" ", ""), setName);
                GraphImage image = new GraphImage(graphName, imageStream);

                if (!PerTestGraphs.ContainsKey(testName))
                {
                    PerTestGraphs.Add(testName, new List<GraphImage>());
                }

                PerTestGraphs[testName].Add(image);
                allGraphs.Add(image);
            }

            return allGraphs;
        }

        /// <summary>
        /// Write this test's portion of the report
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="test">The name of the test for which we wish to write the report</param>
        /// <param name="includeHeader">Whether we should output a header for this report</param>
        /// <param name="tableResults"></param>
        public override void WriteReport(HtmlTextWriter writer, string test, bool includeHeader, uint tableResults, bool email)
        {
            base.WriteReport(writer, test, includeHeader, tableResults, email);
        }
        #endregion // SmokeTestBase Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        private IDictionary<CpuSetKey, IList<CpuInfo>> GatherLatestBuildStats(TestSession session)
        {
            IDictionary<CpuSetKey, IList<CpuInfo>> latestStats = new Dictionary<CpuSetKey, IList<CpuInfo>>();

            if (session != null)
            {
                ISet<string> encounteredTests = new HashSet<string>();

                foreach (TestResults test in session.TestResults)
                {
                    if (encounteredTests.Contains(test.Name))
                    {
                        continue;
                    }
                    encounteredTests.Add(test.Name);
                
                    foreach (CpuResult result in test.CpuResults)
                    {
                        CpuSetKey key = new CpuSetKey(test.Name, result.Set);

                        if (!latestStats.ContainsKey(key))
                        {
                            latestStats.Add(key, new List<CpuInfo>());
                        }

                        latestStats[key].Add(new CpuInfo(result, session.Timestamp));
                        ((List<CpuInfo>)latestStats[key]).Sort();
                        ((List<CpuInfo>)latestStats[key]).Reverse();
                    }
                }

                // Take the 9 worst offenders and add an "other" category for the rest
                IDictionary<CpuSetKey, List<CpuInfo>> keysToUpdate = new Dictionary<CpuSetKey, List<CpuInfo>>();
                ISet<CpuSetKey> keysToRemove = new HashSet<CpuSetKey>();

                foreach (KeyValuePair<CpuSetKey, IList<CpuInfo>> pair in latestStats)
                {
                    if (pair.Value.Count > 10)
                    {
                        // Add other category if we have more than 10 items
                        List<CpuInfo> newList = pair.Value.Take(9).ToList();
                        IEnumerable<CpuInfo> otherEnumerable = pair.Value.Skip(9);

                        if (otherEnumerable.Count() > 0)
                        {
                            float otherAverage = otherEnumerable.Sum(item => item.Result.Average);
                            newList.Add(new CpuInfo(new CpuResult("Other", otherAverage), DateTime.Now));
                        }

                        keysToUpdate.Add(pair.Key, newList);
                    }
                    else if (pair.Value.Count < 2)
                    {
                        // Remove this
                        keysToRemove.Add(pair.Key);
                    }
                }

                foreach (KeyValuePair<CpuSetKey, List<CpuInfo>> pair in keysToUpdate)
                {
                    latestStats[pair.Key] = pair.Value;
                }

                foreach (CpuSetKey key in keysToRemove)
                {
                    latestStats.Remove(key);
                }
            }

            return latestStats;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="testSessions"></param>
        /// <returns></returns>
        private IDictionary<CpuMetricKey, IList<CpuInfo>> GatherGroupedStats(IList<TestSession> testSessions)
        {
            IDictionary<CpuMetricKey, IList<CpuInfo>> groupedStats = new Dictionary<CpuMetricKey, IList<CpuInfo>>();

            foreach (TestSession session in testSessions)
            {
                ISet<string> encounteredTests = new HashSet<string>();
            
                foreach (TestResults test in session.TestResults)
                {
                    if (encounteredTests.Contains(test.Name))
                    {
                        continue;
                    }
                    encounteredTests.Add(test.Name);

                    foreach (CpuResult result in test.CpuResults)
                    {
                        CpuSetKey setKey = new CpuSetKey(test.Name, result.Set);

                        if (LatestBuildCpuStats.ContainsKey(setKey))
                        {
                            if (LatestBuildCpuStats[setKey].Where(info => info.Result.Name == result.Name).Any())
                            {
                                CpuMetricKey metricKey = new CpuMetricKey(test.Name, result.Set, result.Name);
                                if (!groupedStats.ContainsKey(metricKey))
                                {
                                    groupedStats.Add(metricKey, new List<CpuInfo>());
                                }
                                groupedStats[metricKey].Add(new CpuInfo(result, session.Timestamp));
                            }
                            else
                            {
                                CpuMetricKey metricKey = new CpuMetricKey(test.Name, result.Set, "Other");
                                if (!groupedStats.ContainsKey(metricKey))
                                {
                                    groupedStats.Add(metricKey, new List<CpuInfo>());
                                }

                                if (!groupedStats[metricKey].Where(info => info.Timestamp == session.Timestamp).Any())
                                {
                                    groupedStats[metricKey].Add(new CpuInfo(new CpuResult("Other", 0.0f), session.Timestamp));
                                }

                                CpuInfo otherInfo = groupedStats[metricKey].Where(info => info.Timestamp == session.Timestamp).First();
                                otherInfo.Result.Average += result.Average;
                            }
                        }
                        else
                        {
                            CpuMetricKey metricKey = new CpuMetricKey(test.Name, result.Set, result.Name);
                            if (!groupedStats.ContainsKey(metricKey))
                            {
                                groupedStats.Add(metricKey, new List<CpuInfo>());
                            }
                            groupedStats[metricKey].Add(new CpuInfo(result, session.Timestamp));
                        }
                    }
                }
            }

            return groupedStats;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="testName"></param>
        /// <param name="setName"></param>
        /// <param name="stats"></param>
        /// <returns></returns>
        private Stream GeneratePieGraph(string testName, string setName, IList<CpuInfo> stats)
        {
            Chart chart = new Chart();
            chart.BeginInit();
            chart.Width = c_pieGraphWidth;
            chart.Height = c_pieGraphHeight;
            chart.Title = String.Format("Latest Cpu Stats Breakdown for '{0}' ({1} Set)", testName, setName);
            chart.LegendTitle = "Individual Metrics";
            chart.EndInit();

            RenderTargetBitmap renderTarget = new RenderTargetBitmap((int)chart.Width, (int)chart.Height, 96d, 96d, PixelFormats.Default);

            PieSeries pieSeries = new PieSeries
            {
                ItemsSource = stats,
                IndependentValueBinding = new Binding("Result.Name"),
                DependentValueBinding = new Binding("Result.Average")
            };
            chart.Series.Add(pieSeries);

            chart.Measure(new Size(chart.Width, chart.Height));
            chart.Arrange(new Rect(new Size(chart.Width, chart.Height)));
            chart.UpdateLayout();

            renderTarget.Render(chart);

            // Save out the chart to a png file
            PngBitmapEncoder png = new PngBitmapEncoder();
            png.Frames.Add(BitmapFrame.Create(renderTarget));
            MemoryStream stream = new MemoryStream();
            png.Save(stream);
            stream.Position = 0;
            return stream;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="testName"></param>
        /// <param name="setName"></param>
        /// <param name="stats"></param>
        /// <returns></returns>
        private Stream GenerateLineGraph(string testName, string setName, IEnumerable<KeyValuePair<CpuMetricKey, IList<CpuInfo>>> stats)
        {
            Chart chart = new Chart();
            chart.BeginInit();
            chart.Width = c_lineGraphWidth;
            chart.Height = c_lineGraphHeight;
            chart.Title = String.Format("Historical Cpu Stats for '{0}' ({1} Set)", testName, setName);
            chart.LegendTitle = "Individual Metrics";
            chart.EndInit();

            RenderTargetBitmap renderTarget = new RenderTargetBitmap((int)chart.Width, (int)chart.Height, 96d, 96d, PixelFormats.Default);

            foreach (KeyValuePair<CpuMetricKey, IList<CpuInfo>> pair in stats)
            {
                LineSeries lineSeries = new LineSeries
                {
                    Title = pair.Key.MetricName,
                    ItemsSource = pair.Value,
                    IndependentValueBinding = new Binding("Timestamp"),
                    DependentValueBinding = new Binding("Result.Average")
                };
                chart.Series.Add(lineSeries);
            }

            DateTimeAxis xAxis = new DateTimeAxis
            {
                Orientation = AxisOrientation.X,
                Interval = 1,
                IntervalType = DateTimeIntervalType.Days,
                Title = "Date"
            };
            chart.Axes.Add(xAxis);

            LinearAxis yAxis = new LinearAxis
            {
                Orientation = AxisOrientation.Y,
                Title = "Milliseconds",
                ShowGridLines = true
            };
            chart.Axes.Add(yAxis);

            chart.Measure(new Size(chart.Width, chart.Height));
            chart.Arrange(new Rect(new Size(chart.Width, chart.Height)));
            chart.UpdateLayout();

            renderTarget.Render(chart);

            // Save out the chart to a png file
            PngBitmapEncoder png = new PngBitmapEncoder();
            png.Frames.Add(BitmapFrame.Create(renderTarget));
            MemoryStream stream = new MemoryStream();
            png.Save(stream);
            stream.Position = 0;
            return stream;
        }
        #endregion // Private Methods
    } // CpuSmokeTest
}
