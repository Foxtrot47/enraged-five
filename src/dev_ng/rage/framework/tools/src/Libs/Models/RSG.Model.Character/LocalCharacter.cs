﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using RSG.Base.ConfigParser;
using RSG.ManagedRage;
using RSG.Model.Asset;
using RSG.Model.Common;
using RSG.Model.Common.Util;
using RSG.SceneXml;
using RSG.SceneXml.Material;
using RSG.Platform;
using RSG.Base.Extensions;
using RSG.Model.Asset.Util;

namespace RSG.Model.Character
{
    /// <summary>
    /// 
    /// </summary>
    public class LocalCharacter : Character
    {
        #region Constants
        /// <summary>
        /// 
        /// </summary>
        private static readonly string SkeletonName = "SKEL_ROOT";
        #endregion // Constants
        
        #region Constructor(s)
        /// <summary>
        /// Constructor from a SceneXml file.
        /// </summary>
        public LocalCharacter(String name, CharacterCategory category, ICharacterCollection parentCollection)
            : base(name, category, parentCollection)
        {
        }
        #endregion // Constructor(s)

        #region StreamableAssetContainerBase Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="statsToLoad"></param>
        protected override void LoadStatsInternal(IEnumerable<StreamableStat> statsToLoad)
        {
            Debug.Assert(ParentCollection is LocalCharacterCollection, "Parent collection isn't an LocalCharacterCollection.");
            LocalCharacterCollection localCollection = (LocalCharacterCollection)ParentCollection;

            if (!AreStatsLoaded(statsToLoad.Where(item => item != StreamableCharacterStat.PlatformStats)))
            {
                LoadStatsFromExportData(localCollection.ExportDataPath);
            }

            if (statsToLoad.Contains(StreamableCharacterStat.PlatformStats) && !IsStatLoaded(StreamableCharacterStat.PlatformStats))
            {
                // Get the streamed peds pack files
                List<ContentNode> streamedNodes = localCollection.GameView.Content.Root.FindAll("streamedpeds", "directory");
                string streamedIndependentPath = LocalLoadUtil.GeneratePathForNodes(streamedNodes);

                IDictionary<RSG.Platform.Platform, Packfile> streamedPackFiles;
                if (!String.IsNullOrEmpty(streamedIndependentPath))
                {
                    streamedPackFiles = LocalLoadUtil.OpenPackFiles(streamedIndependentPath, localCollection.GameView, localCollection.Config);
                }
                else
                {
                    streamedPackFiles = new Dictionary<RSG.Platform.Platform, Packfile>();
                }

                // Get the component ped pack files
                List<ContentNode> componentNodes = localCollection.GameView.Content.Root.FindAll("componentpeds", "directory");
                string componentIndependentPath = LocalLoadUtil.GeneratePathForNodes(componentNodes);

                IDictionary<RSG.Platform.Platform, Packfile> componentPackFiles;
                if (!String.IsNullOrEmpty(componentIndependentPath))
                {
                    componentPackFiles = LocalLoadUtil.OpenPackFiles(componentIndependentPath, localCollection.GameView, localCollection.Config);
                }
                else
                {
                    componentPackFiles = new Dictionary<RSG.Platform.Platform, Packfile>();
                }

                // Get the cutscene pack files
                List<ContentNode> cutsceneNodes = localCollection.GameView.Content.Root.FindAll("cutspeds", "directory");
                string cutsceneIndependentPath = LocalLoadUtil.GeneratePathForNodes(cutsceneNodes);

                IDictionary<RSG.Platform.Platform, Packfile> cutscenePackFiles;
                if (!String.IsNullOrEmpty(cutsceneIndependentPath))
                {
                    cutscenePackFiles = LocalLoadUtil.OpenPackFiles(cutsceneIndependentPath, localCollection.GameView, localCollection.Config);
                }
                else
                {
                    cutscenePackFiles = new Dictionary<RSG.Platform.Platform, Packfile>();
                }

                LoadPlatformStatsFromExportData(componentPackFiles, cutscenePackFiles, streamedPackFiles);

                // Close all the pack files we opened
                LocalLoadUtil.ClosePackFiles(streamedPackFiles);
                LocalLoadUtil.ClosePackFiles(componentPackFiles);
                LocalLoadUtil.ClosePackFiles(cutscenePackFiles);
            }
        }
        #endregion // StreamableAssetContainerBase Overrides

        #region Internal Methods
        /// <summary>
        /// Load basic stats from the export data scene xml file
        /// </summary>
        /// <param name="source"></param>
        internal void LoadStatsFromExportData(string exportDataPath)
        {
            string sceneXmlFileName = Path.Combine(exportDataPath, Category.GetDescriptionValue(), Name) + ".xml";
            ParseInternal(sceneXmlFileName);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="componentPackFiles"></param>
        /// <param name="cutscenePackFiles"></param>
        /// <param name="streamedPackFiles"></param>
        internal void LoadPlatformStatsFromExportData(IDictionary<RSG.Platform.Platform, Packfile> componentPackFiles, IDictionary<RSG.Platform.Platform, Packfile> cutscenePackFiles, IDictionary<RSG.Platform.Platform, Packfile> streamedPackFiles)
        {
            // Next load in the platform stats
            PlatformStats = new Dictionary<RSG.Platform.Platform, ICharacterPlatformStat>();

            if (Category == CharacterCategory.Component)
            {
                LoadComponentPlatformStats(componentPackFiles);
            }
            else if (Category == CharacterCategory.Cutscene)
            {
                LoadCutsceneOrStreamedPlatformStats(cutscenePackFiles);
            }
            else if (Category == CharacterCategory.Streamed)
            {
                LoadCutsceneOrStreamedPlatformStats(streamedPackFiles);
            }
        }
        #endregion // Public Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="packFiles"></param>
        private void LoadComponentPlatformStats(IDictionary<RSG.Platform.Platform, Packfile> packFiles)
        {
            foreach (KeyValuePair<RSG.Platform.Platform, Packfile> pair in packFiles)
            {
                if (pair.Value.Entries != null)
                {
                    uint physicalSize = 0;
                    uint virtualSize = 0;

                    // Process all of the extensions we were provided
                    foreach (string extension in GetComponentExtensionsForPlatform(pair.Key))
                    {
                        PackEntry entry = pair.Value.FindEntry(Name + "." + extension, true);
                        if (entry != null)
                        {
                            physicalSize += (uint)entry.PhysicalSize;
                            virtualSize += (uint)entry.VirtualSize;
                        }
                    }

                    if (physicalSize != 0 && virtualSize != 0)
                    {
                        ICharacterPlatformStat platformStat = new CharacterPlatformStat(pair.Key, physicalSize, virtualSize);
                        PlatformStats.Add(pair.Key, platformStat);
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="packFiles"></param>
        private void LoadCutsceneOrStreamedPlatformStats(IDictionary<RSG.Platform.Platform, Packfile> packFiles)
        {
            foreach (KeyValuePair<RSG.Platform.Platform, Packfile> pair in packFiles)
            {
                if (pair.Value.Entries != null)
                {
                    uint physicalSize = 0;
                    uint virtualSize = 0;

                    // Process all of the extensions we were provided
                    PackEntry defaultEntry = pair.Value.FindEntry(Name + "." + GetFragmentExtensionsForPlatform(pair.Key), true);
                    if (defaultEntry != null)
                    {
                        physicalSize += (uint)defaultEntry.PhysicalSize;
                        virtualSize += (uint)defaultEntry.VirtualSize;
                    }

                    foreach (PackEntry entry in pair.Value.Entries)
                    {
                        if (entry.Path.ToLower() == Name.ToLower())
                        {
                            physicalSize += (uint)entry.PhysicalSize;
                            virtualSize += (uint)entry.VirtualSize;
                        }
                    }

                    if (physicalSize != 0 && virtualSize != 0)
                    {
                        ICharacterPlatformStat platformStat = new CharacterPlatformStat(pair.Key, physicalSize, virtualSize);
                        PlatformStats.Add(pair.Key, platformStat);
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="platform"></param>
        /// <returns></returns>
        private string[] GetComponentExtensionsForPlatform(RSG.Platform.Platform platform)
        {
            List<string> extensions = new List<string>();

            foreach (string ext in new string[] { "ft", "td", "dd" })
            {
                extensions.Add(platform.PlatformRagebuilderCharacter() + ext);
            }

            return extensions.ToArray();
        }

        /// 
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        private string GetFragmentExtensionsForPlatform(RSG.Platform.Platform platform)
        {
            return platform.PlatformRagebuilderCharacter() + "ft";
        }

        /// <summary>
        /// Parse character SceneXml (internal)
        /// </summary>
        /// <param name="sceneXmlFilename"></param>
        private void ParseInternal(String sceneXmlFilename)
        {
            // Initialise the stats
            Shaders = new IShader[0];
            PolyCount = 0;
            CollisionCount = 0;
            BoneCount = 0;
            ClothCount = 0;
            ClothPolyCount = 0;

            if (File.Exists(sceneXmlFilename))
            {
                Scene scene = new Scene(sceneXmlFilename);
                ParseObjects(scene);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="material"></param>
        /// <param name="textures"></param>
        /// <param name="shaders"></param>
        private void GetTexturesFromMaterial(MaterialDef material, ref List<IShader> shaders)
        {
            if (material.TextureCount > 0)
            {
                Shader shader = null;
                if (!string.IsNullOrEmpty(material.Preset))
                {
                    shader = new Shader(material.Preset);

                    foreach (TextureDef texture in material.Textures)
                    {
                        Texture newTexture = new Texture(texture.FilePath);

                        if (shader != null)
                        {
                            shader.Textures.Add(newTexture);
                        }
                    }
                    if (!shaders.Contains(shader))
                        shaders.Add(shader);
                }
            }

            if (material.HasSubMaterials)
            {
                foreach (MaterialDef submat in material.SubMaterials)
                {
                    GetTexturesFromMaterial(submat, ref shaders);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scene"></param>
        private void ParseObjects(Scene scene)
        {
            TargetObjectDef skeleton = null;
            TargetObjectDef mesh = null;
            foreach (TargetObjectDef obj in scene.Objects)
            {
                if (obj.Name == this.Name)
                {
                    mesh = obj;
                }
            }
            foreach (TargetObjectDef obj in scene.Objects)
            {
                TargetObjectDef skel = FindSkeletion(obj);
                if (skel != null)
                {
                    skeleton = skel;
                    break;
                }
            }

            List<IShader> shaders = new List<IShader>();
            ParseMesh(scene, mesh, ref shaders);

            shaders.Sort();
            this.Shaders = shaders.ToArray();

            this.AssetChildren.Clear();
            this.AssetChildren.AddRange(Textures);

            ParseSkeleton(skeleton);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="root"></param>
        /// <returns></returns>
        private TargetObjectDef FindSkeletion(TargetObjectDef root)
        {
            if (root.Name == SkeletonName)
            {
                return root;
            }
            else
            {
                foreach (TargetObjectDef child in root.Children)
                {
                    TargetObjectDef skel = FindSkeletion(child);
                    if (skel != null)
                    {
                        return skel;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="skeleton"></param>
        private void ParseSkeleton(TargetObjectDef root)
        {
            if (root == null)
                return;
            if (root.IsBone())
                this.BoneCount++;
            if (root.IsCollision())
                this.CollisionCount += root.PolyCount;

            foreach (var child in root.Children)
            {
                ParseSkeleton(child);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="skeleton"></param>
        private void ParseMesh(Scene scene, TargetObjectDef root, ref List<IShader> shaders)
        {
            if (root == null)
                return;
            if (root.DontExport())
                return;

            if (root.IsCloth())
            {
                this.ClothCount++;
                this.ClothPolyCount += root.PolyCount;
            }
            else if (root.IsObject())
            {
                this.PolyCount += root.PolyCount;
            }
            MaterialDef material = null;
            if (scene.MaterialLookup.TryGetValue(root.Material, out material))
            {
                GetTexturesFromMaterial(material, ref shaders);
            }
            foreach (var child in root.Children)
            {
                ParseMesh(scene, child, ref shaders);
            }
        }
        #endregion // Private Methods
    } // LocalCharacter
}
