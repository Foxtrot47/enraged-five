﻿//---------------------------------------------------------------------------------------------
// <copyright file="ConverterBase{T}.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Converters
{
    using System;
    using RSG.Base;

    /// <summary>
    /// Provides a abstract base class implementing the <see cref="IConverter{T}"/> interface.
    /// </summary>
    /// <typeparam name="T">
    /// The type that this converter can convert a string instance to.
    /// </typeparam>
    public abstract class ConverterBase<T> : IConverter<T>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ConverterBase{T}"/> class.
        /// </summary>
        protected ConverterBase()
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Converts the specified <paramref name="value"/> parameter to an instance of the
        /// type specified by the type parameter.
        /// </summary>
        /// <param name="value">
        /// The string instance to convert.
        /// </param>
        /// <param name="fallback">
        /// The value to return if an error occurs during the conversion or if the conversion
        /// isn't possible.
        /// </param>
        /// <param name="methods">
        /// The conversion methods that can be used during this conversion.
        /// </param>
        /// <returns>
        /// The converted value if the conversion is successful; otherwise, the specified
        /// <paramref name="fallback"/> value.
        /// </returns>
        public T Convert(string value, T fallback, Methods methods)
        {
            return this.Try(value, fallback, methods).Value;
        }

        /// <summary>
        /// Converts the specified <paramref name="value"/> parameter to an array of instances
        /// of the type specified by the type parameter.
        /// </summary>
        /// <param name="value">
        /// The string instance to convert.
        /// </param>
        /// <param name="count">
        /// The number of values the specified <paramref name="value"/> should be converted
        /// into.
        /// </param>
        /// <param name="fallback">
        /// The value that should be inserted into the returned array if the conversion fails.
        /// </param>
        /// <param name="methods">
        /// The conversion methods that can be used during this conversion.
        /// </param>
        /// <returns>
        /// The converted array of values if the conversation is successful; otherwise, an
        /// array of the specified count with the specified <paramref name="fallback"/> value
        /// in place of any unconverted values.
        /// </returns>
        public T[] Convert(string value, int count, T fallback, Methods methods)
        {
            return this.Try(value, count, fallback, methods).Value;
        }

        /// <summary>
        /// Attempts to convert the specified <paramref name="value"/> parameter to an instance
        /// of the type specified by the type parameter.
        /// </summary>
        /// <param name="value">
        /// The string instance to convert.
        /// </param>
        /// <param name="fallback">
        /// The value to return if an error occurs during the conversion or if the conversion
        /// isn't possible.
        /// </param>
        /// <param name="methods">
        /// The conversion methods that can be used during this conversion.
        /// </param>
        /// <returns>
        /// A structure containing the converted value and a boolean value indicating whether
        /// the conversion was successful or not.
        /// </returns>
        public TryResult<T> TryConvert(string value, T fallback, Methods methods)
        {
            return this.Try(value, fallback, methods);
        }

        /// <summary>
        /// Attempts to convert the specified <paramref name="value"/> parameter to an array
        /// of instance of the type specified by the type parameter.
        /// </summary>
        /// <param name="value">
        /// The string instance to convert.
        /// </param>
        /// <param name="count">
        /// The number of values the specified <paramref name="value"/> should be converted
        /// into.
        /// </param>
        /// <param name="fallback">
        /// The value that should be inserted into the returned array if the conversion fails.
        /// </param>
        /// <param name="methods">
        /// The conversion methods that can be used during this conversion.
        /// </param>
        /// <returns>
        /// A structure containing the converted value and a boolean value indicating whether
        /// the conversion was successful or not.
        /// </returns>
        public TryResult<T[]> TryConvert(string value, int count, T fallback, Methods methods)
        {
            return this.Try(value, count, fallback, methods);
        }

        /// <summary>
        /// Override to implement the conversion logic between a single string instance and a
        /// instance of the type specified by the type parameter.
        /// </summary>
        /// <param name="value">
        /// The string instance to convert.
        /// </param>
        /// <param name="fallback">
        /// Specifies the value that is returned if the conversion fails.
        /// </param>
        /// <param name="methods">
        /// The conversion methods that can be used during this conversion.
        /// </param>
        /// <returns>
        /// A structure containing the result of the conversion and a value indicating whether
        /// the conversion was successful.
        /// </returns>
        protected abstract TryResult<T> Try(string value, T fallback, Methods methods);

        /// <summary>
        /// Safely splits the specified string into substring delimited by the tuple separator
        /// character.
        /// </summary>
        /// <param name="value">
        /// The string to split. if this is null a empty array is returned.
        /// </param>
        /// <returns>
        /// An array whose elements contain the substrings in this string that are delimited
        /// by the tuple separator character.
        /// </returns>
        private string[] SafeSplit(string value)
        {
            if (value == null)
            {
                return new string[0];
            }

            return value.Split(new char[] { ',' }, StringSplitOptions.None);
        }

        /// <summary>
        /// Attempts to convert the specified <paramref name="value"/> parameter to an array
        /// of instances of the type specified by the type parameter.
        /// </summary>
        /// <param name="value">
        /// The string instance to convert.
        /// </param>
        /// <param name="count">
        /// The number of values the specified <paramref name="value"/> should be converted
        /// into.
        /// </param>
        /// <param name="fallback">
        /// The value that should be inserted into the returned array if the conversion fails.
        /// </param>
        /// <param name="methods">
        /// The conversion methods that can be used during this conversion.
        /// </param>
        /// <returns>
        /// A structure containing the converted value and a boolean value indicating whether
        /// the conversion was successful or not.
        /// </returns>
        private TryResult<T[]> Try(string value, int count, T fallback, Methods methods)
        {
            string[] parts = this.SafeSplit(value);
            bool success = parts.Length == count || parts.Length == 1;
            T[] values = new T[count];
            if (success == true)
            {
                if (parts.Length == 1)
                {
                    TryResult<T> result = this.Try(parts[0], fallback, methods);
                    if (result.Success)
                    {
                        for (int i = 0; i < values.Length; i++)
                        {
                            values[i] = result.Value;
                        }
                    }

                    success = result.Success;
                }
                else
                {
                    for (int i = 0; i < count; i++)
                    {
                        if (success && parts.Length > i)
                        {
                            TryResult<T> result = this.Try(parts[i], fallback, methods);
                            if (result.Success)
                            {
                                values[i] = result.Value;
                                continue;
                            }
                        }

                        success = false;
                        break;
                    }
                }
            }

            if (success == false)
            {
                for (int i = 0; i < count; i++)
                {
                    values[i] = fallback;
                }
            }

            return new TryResult<T[]>(values, success);
        }
        #endregion Methods
    } // RSG.Metadata.Model.Converters.ConverterBase{T} {Interface}
} // RSG.Metadata.Model.Converters {Namespace}
