﻿using System.Collections.Generic;
using System.Xml.Linq;
using System.Xml.XPath;

namespace RSG.Model.Asset
{
    /// <summary>
    /// Contains lists of strings that represent lookup tables so that a spawn point can be
    /// initialised using data provided by a xml element.
    /// </summary>
    public class SpawnPointLookupTables
    {
        #region Fields
        /// <summary>
        /// Defines the value returned from a request to get the type name with a index that
        /// doesn't exist.
        /// </summary>
        private const string TypeNameDefaultValue = "UNKNOWN";

        /// <summary>
        /// Defines the value returned from a request to get the ped model set with a index
        /// that doesn't exist.
        /// </summary>
        private const string PedModelSetNameDefaultValue = "NONE";

        /// <summary>
        /// Defines the value returned from a request to get the vehicle set name with a index
        /// that doesn't exist.
        /// </summary>
        private const string VehicleSetNameDefaultValue = "NONE";

        /// <summary>
        /// Defines the value returned from a request to get the group name with a index that
        /// doesn't exist.
        /// </summary>
        private const string GroupNameDefaultValue = "NONE";

        /// <summary>
        /// Defines the value returned from a request to get the interior name with a index
        /// that doesn't exist.
        /// </summary>
        private const string InteriorNameDefaultValue = "NONE";

        /// <summary>
        /// Defines the x-path string used to iterator over the type names defined in the
        /// source document.
        /// </summary>
        private const string XmlTypeNamePath = "//LookUps/TypeNames/Item";

        /// <summary>
        /// Defines the x-path string used to iterator over the type names defined in the
        /// source document.
        /// </summary>
        private const string XmlPedNamePath = "//LookUps/PedModelSetNames/Item";

        /// <summary>
        /// Defines the x-path string used to iterator over the vehicle names defined in the
        /// source document.
        /// </summary>
        private const string XmlVehicleNamePath = "//LookUps/VehicleModelSetNames/Item";

        /// <summary>
        /// Defines the x-path string used to iterator over the group names defined in the
        /// source document.
        /// </summary>
        private const string XmlGroupNamePath = "//LookUps/GroupNames/Item";

        /// <summary>
        /// Defines the x-path string used to iterator over the interior names defined in the
        /// source document.
        /// </summary>
        private const string XmlInteriorNamePath = "//LookUps/InteriorNames/Item";

        /// <summary>
        /// Contains the valid type names in the correct order for indexing.
        /// </summary>
        private List<string> typeNames;

        /// <summary>
        /// Contains the valid ped model set names in the correct order for indexing.
        /// </summary>
        private List<string> pedModelSetNames;

        /// <summary>
        /// Contains the valid vehicle model set names in the correct order for indexing.
        /// </summary>
        private List<string> vehicleModelSetNames;

        /// <summary>
        /// Contains the valid group names in the correct order for indexing.
        /// </summary>
        private List<string> groupNames;

        /// <summary>
        /// Contains the valid interior names in the correct order for indexing.
        /// </summary>
        private List<string> interiorNames;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="SpawnPointLookupTables"/> class.
        /// </summary>
        /// <param name="document">
        /// The System.Xml.Linq.XDocument object that provides the data used to create the
        /// lookup tables.
        /// </param>
        public SpawnPointLookupTables(XDocument document)
        {
            this.typeNames = new List<string>();
            this.pedModelSetNames = new List<string>();
            this.vehicleModelSetNames = new List<string>();
            this.groupNames = new List<string>();
            this.interiorNames = new List<string>();

            foreach (XElement element in document.XPathSelectElements(XmlTypeNamePath))
            {
                if (element == null)
                {
                    continue;
                }

                this.typeNames.Add(element.Value);
            }

            foreach (XElement element in document.XPathSelectElements(XmlPedNamePath))
            {
                if (element == null)
                {
                    continue;
                }

                this.pedModelSetNames.Add(element.Value);
            }

            foreach (XElement element in document.XPathSelectElements(XmlVehicleNamePath))
            {
                if (element == null)
                {
                    continue;
                }

                this.vehicleModelSetNames.Add(element.Value);
            }

            foreach (XElement element in document.XPathSelectElements(XmlGroupNamePath))
            {
                if (element == null)
                {
                    continue;
                }

                this.groupNames.Add(element.Value);
            }

            foreach (XElement element in document.XPathSelectElements(XmlInteriorNamePath))
            {
                if (element == null)
                {
                    continue;
                }

                this.interiorNames.Add(element.Value);
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Gets the type name for the specified index.
        /// </summary>
        /// <param name="index">
        /// The zero-based index for the type name to retrieve.
        /// </param>
        /// <returns>
        /// The type name that has the specified index if valid; otherwise the default value.
        /// </returns>
        public string GetTypeName(int index)
        {
            if (index > this.typeNames.Count - 1 || index < 0)
            {
                return TypeNameDefaultValue;
            }

            return this.typeNames[index];
        }

        /// <summary>
        /// Gets the ped model set name for the specified index.
        /// </summary>
        /// <param name="index">
        /// The zero-based index for the ped model set name to retrieve.
        /// </param>
        /// <returns>
        /// The ped model set name that has the specified index if valid; otherwise the default
        /// value.
        /// </returns>
        public string GetPedModelSetName(int index)
        {
            if (index > this.pedModelSetNames.Count - 1 || index < 0)
            {
                return PedModelSetNameDefaultValue;
            }

            return this.pedModelSetNames[index];
        }

        /// <summary>
        /// Gets the vehicle model set name for the specified index.
        /// </summary>
        /// <param name="index">
        /// The zero-based index for the vehicle model set name to retrieve.
        /// </param>
        /// <returns>
        /// The vehicle model set name that has the specified index if valid; otherwise the
        /// default value.
        /// </returns>
        public string GetVehicleModelSetName(int index)
        {
            if (index > this.vehicleModelSetNames.Count - 1 || index < 0)
            {
                return VehicleSetNameDefaultValue;
            }

            return this.vehicleModelSetNames[index];
        }

        /// <summary>
        /// Gets the group name for the specified index.
        /// </summary>
        /// <param name="index">
        /// The zero-based index for the group name to retrieve.
        /// </param>
        /// <returns>
        /// The group name that has the specified index if valid; otherwise the default value.
        /// </returns>
        public string GetGroupName(int index)
        {
            if (index > this.groupNames.Count - 1 || index < 0)
            {
                return GroupNameDefaultValue;
            }

            return this.groupNames[index];
        }

        /// <summary>
        /// Gets the interior name for the specified index.
        /// </summary>
        /// <param name="index">
        /// The zero-based index for the interior name to retrieve.
        /// </param>
        /// <returns>
        /// The interior name that has the specified index if valid; otherwise the default
        /// value.
        /// </returns>
        public string GetInteriorName(int index)
        {
            if (index > this.interiorNames.Count - 1 || index < 0)
            {
                return InteriorNameDefaultValue;
            }

            return this.interiorNames[index];
        }
        #endregion
    } // RSG.Model.Asset.SpawnPointLookupTables {Class}
} // RSG.Model.Asset {Namespace}
