﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.SceneXml;

namespace RSG.Model.Common.Map
{
    /// <summary>
    /// 
    /// </summary>
    public interface ICarGen : IMapObjectWithTransform
    {
        #region Properties
        /// <summary>
        /// The parent section of this archetype
        /// </summary>
        IMapSection ParentSection { get; }

        /// <summary>
        /// 
        /// </summary>
        bool IsPolice { get; }

        /// <summary>
        /// 
        /// </summary>
        bool IsFire { get; }

        /// <summary>
        /// 
        /// </summary>
        bool IsAmbulance { get; }

        /// <summary>
        /// 
        /// </summary>
        bool IsSpecificModel { get; }

        /// <summary>
        /// 
        /// </summary>
        string ModelName { get; }

        /// <summary>
        /// 
        /// </summary>
        bool IsHighPriority { get; }
        #endregion // Properties
    } // ICarGen
}
