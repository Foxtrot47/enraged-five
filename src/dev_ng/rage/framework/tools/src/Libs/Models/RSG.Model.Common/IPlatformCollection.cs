﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common
{
    /// <summary>
    /// 
    /// </summary>
    public interface IPlatformCollection : IEnumerable<RSG.Platform.Platform>, ICollection<RSG.Platform.Platform>
    {
    } // IPlatformCollection
}
