﻿//---------------------------------------------------------------------------------------------
// <copyright file="MetadataLevelUtil.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    /// <summary>
    /// Provides utility methods for the metadata set level enum type.
    /// </summary>
    public static class MetadataLevelUtil
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Count"/> property.
        /// </summary>
        private static int? _count;

        /// <summary>
        /// The private field containing the extensions used on the individual metadata levels.
        /// </summary>
        private static Dictionary<MetadataLevel, HashSet<string>> _exclusions;

        /// <summary>
        /// The private field containing the extensions used on the individual metadata levels.
        /// </summary>
        private static Dictionary<MetadataLevel, string> _extensions;
        #endregion Fields

        #region Properties
        /// <summary>
        /// Gets the number of metadata sets defined on the Metadata Set Level enum type.
        /// </summary>
        public static int Count
        {
            get
            {
                if (_count == null)
                {
                    _count = Enum.GetValues(typeof(MetadataLevel)).Length;
                }

                return _count.Value;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Gets the MetadataLevelExcludedAttribute attribute on the specified enum field.
        /// </summary>
        /// <param name="field">
        /// The enum field whose excluded groups will be returned.
        /// </param>
        /// <returns>
        /// The excluded groups that have been set on the specified enum field.
        /// </returns>
        public static HashSet<string> GetExcludedGroups(MetadataLevel field)
        {
            if (_exclusions == null)
            {
                _exclusions = new Dictionary<MetadataLevel, HashSet<string>>();
                foreach (MetadataLevel level in Enum.GetValues(typeof(MetadataLevel)))
                {
                    MemberInfo[] members = level.GetType().GetMember(level.ToString());
                    if (members.Length <= 0)
                    {
                        _extensions.Add(level, null);
                        continue;
                    }

                    Type type = typeof(MetadataLevelExcludedAttribute);
                    MemberInfo member = members[0];
                    HashSet<string> values = new HashSet<string>();
                    foreach (Attribute attribute in member.GetCustomAttributes(type))
                    {
                        MetadataLevelExcludedAttribute exclusionAttribute =
                            attribute as MetadataLevelExcludedAttribute;
                        if (exclusionAttribute == null)
                        {
                            continue;
                        }

                        values.Add(exclusionAttribute.GroupName);
                    }

                    _exclusions.Add(level, values);
                }
            }

            HashSet<string> exclusions = null;
            if (!_exclusions.TryGetValue(field, out exclusions))
            {
                return null;
            }

            return exclusions;
        }

        /// <summary>
        /// Gets the MetadataLevelExtensionAttribute attribute on the specified enum field.
        /// </summary>
        /// <param name="field">
        /// The enum field whose extension will be returned.
        /// </param>
        /// <returns>
        /// The extension that exists on the enum field.
        /// </returns>
        public static string GetExtensionAttribute(MetadataLevel field)
        {
            if (_extensions == null)
            {
                _extensions = new Dictionary<MetadataLevel, string>();
                foreach (MetadataLevel level in Enum.GetValues(typeof(MetadataLevel)))
                {
                    MemberInfo[] members = level.GetType().GetMember(level.ToString());
                    if (members.Length <= 0)
                    {
                        _extensions.Add(level, null);
                        continue;
                    }

                    Type type = typeof(MetadataLevelExtensionAttribute);
                    MemberInfo member = members[0];
                    Attribute attribute = member.GetCustomAttributes(type).FirstOrDefault();
                    MetadataLevelExtensionAttribute extensionAttribute =
                        attribute as MetadataLevelExtensionAttribute;
                    if (extensionAttribute == null)
                    {
                        _extensions.Add(level, null);
                        continue;
                    }

                    _extensions.Add(level, extensionAttribute.Extension);
                }
            }

            string extension = null;
            if (!_extensions.TryGetValue(field, out extension))
            {
                return null;
            }

            return extension;
        }
        #endregion Methods
    } // RSG.Project.Model.MetadataLevelUtil {Class}
} // RSG.Project.Model {Namespace}
