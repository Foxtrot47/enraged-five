﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.SceneXml;

namespace RSG.Model.Map3.Util
{
    /// <summary>
    /// Helper class used when parsing map section scene xml
    /// </summary>
    public class CategorisedObjects
    {
        public List<TargetObjectDef> Drawables { get; protected set; }
        public List<TargetObjectDef> Interiors { get; protected set; }
        public List<TargetObjectDef> Fragments { get; protected set; }
        public List<TargetObjectDef> FragmentRoots { get; protected set; }
        public Dictionary<TargetObjectDef, TargetObjectDef> FragmentsWithRoots { get; protected set; }
        public Dictionary<String, List<TargetObjectDef>> AnimGroups { get; protected set; }
        public List<TargetObjectDef> Entities { get; protected set; }

        public CategorisedObjects()
        {
            Drawables = new List<TargetObjectDef>();
            Interiors = new List<TargetObjectDef>();
            Fragments = new List<TargetObjectDef>();
            AnimGroups = new Dictionary<String, List<TargetObjectDef>>();
            Entities = new List<TargetObjectDef>();
            FragmentRoots = new List<TargetObjectDef>();
            FragmentsWithRoots = new Dictionary<TargetObjectDef, TargetObjectDef>();
        }
    } // CategorisedObjects
}
