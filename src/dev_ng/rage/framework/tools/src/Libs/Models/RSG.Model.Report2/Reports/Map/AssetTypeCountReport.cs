﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Report;
using RSG.Base.Tasks;
using RSG.Model.Common.Map;
using RSG.Base.ConfigParser;
using RSG.Model.Common.Extensions;
using System.IO;
using RSG.Model.Common;
using RSG.ManagedRage;
using RSG.Platform;


namespace RSG.Model.Report.Reports.Map
{
    #region Helper classes

    class PlatformData
    {
        public IDictionary<FileType, int> Counters { get; private set; }

        public PlatformData()
        {
            Counters = new Dictionary<FileType, int>();
        }
    }
    
    class FileData
    {
        public PlatformData Data { get; private set; }

        public FileData(Packfile packFile)
        {
            Data = new PlatformData();
            ExtractEntries(packFile);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="entryMap"></param>
        private void ExtractEntries(Packfile pack)
        {
            if (pack == null || pack.Entries == null)
            {
                return;
            }

            foreach (PackEntry entry in pack.Entries)
            {
                // Ignore non-resource entries
                if (entry.IsResource)
                {
                    // Generate the name to use for this entry
                    string extension = Path.GetExtension(entry.Name);
                    FileType fileType = FileTypeUtils.ConvertExtensionToFileType(extension);
                    switch (fileType)
                    {
                        case FileType.Drawable:
                        case FileType.DrawableDictionary:
                        case FileType.TextureDictionary:
                        case FileType.BoundsFile:
                        case FileType.Fragment:
                        case FileType.ClipDictionary:
                            if (!Data.Counters.ContainsKey(fileType)) { Data.Counters.Add(fileType, 0); }
                            Data.Counters[fileType]++;
                            break;
                    }
                }
            }
        }
    }

    class ReportData
    {
        public IDictionary<string, FileData> Files { get; private set; }

        public ReportData()
        {
            Files = new Dictionary<string, FileData>();
        }
    }

    #endregion

    /// <summary>
    /// Counts each of the asset types per .rpf file.
    /// </summary>
    public class AssetTypeCountReport : CSVReport, IDynamicLevelReport
    {
        #region Constants

        public static readonly string c_name = "Asset Type Count Report";
        public static readonly string c_desc = "Counts each of the asset types per .rpf file.";

        #endregion

        #region Private member fields

        private ITask m_task;
        private ReportData m_data;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        public AssetTypeCountReport()
            : base(c_name, c_desc)
        {
            m_data = new ReportData();
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (m_task == null)
                {
                    CompositeTask task = new CompositeTask("Generate Report");
                    task.AddSubTask(new ActionTask("Generating report", (context, progress) => GenerateReport((DynamicLevelReportContext)context, progress)));
                    m_task = task;
                }
                return m_task;
            }
        }

        /// <summary>
        /// List of stats that are required to generate this report.
        /// </summary>
        public IEnumerable<StreamableStat> RequiredStats
        {
            get { return null; }
        }
        
        #endregion

        #region Task methods

        /// <summary>
        /// Generate the report.
        /// </summary>
        /// <param name="context">Context.</param>
        /// <param name="progress">Progress.</param>
        private void GenerateReport(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            IMapHierarchy hierarchy = context.Level.MapHierarchy;
            if (hierarchy == null)
            {
                return;
            }

            foreach (IMapSection section in hierarchy.AllSections)
            {
                ContentNodeMap node = section.GetConfigMapNode(context.GameView);
                foreach (ContentNode outputNode in node.Outputs)
                {
                    if (outputNode.Outputs.Count() == 0)
                    {
                        ContentNodeMapZip nodeZip = outputNode as ContentNodeMapZip;
                        if (nodeZip != null)
                        {
                            string independentFilePath = Path.GetFullPath(nodeZip.Filename);
                            ProcessIndependentFilePath(independentFilePath, section, context.GameView, context.Platforms);
                        }
                    }
                    else
                    {
                        foreach (ContentNode processedNode in outputNode.Outputs)
                        {
                            ContentNodeMapProcessedZip processedZip = processedNode as ContentNodeMapProcessedZip;
                            if (processedZip != null)
                            {
                                string independentFilePath = Path.GetFullPath(processedZip.Filename);
                                ProcessIndependentFilePath(independentFilePath, section, context.GameView, context.Platforms);
                            }
                        }
                    }
                }
            }

            WriteCSV();
        }

        /// <summary>
        /// Write the CSV file.
        /// </summary>
        private void WriteCSV()
        {
            using (StreamWriter writer = new StreamWriter(Filename))
            {
                WriteHeader(writer);
                WriteData(writer);
            }
        }

        /// <summary>
        /// Write the data out to the CSV file.
        /// </summary>
        /// <param name="writer">Stream writer.</param>
        private void WriteData(StreamWriter writer)
        {
            FileType[] columns = new FileType[] { FileType.Drawable, FileType.DrawableDictionary, FileType.TextureDictionary, FileType.BoundsFile, FileType.ClipDictionary, FileType.Fragment };

            foreach (var rpfFile in m_data.Files)
            {
                string line = rpfFile.Key;
                foreach (var fileType in columns)
                {
                    line = line + "," + (rpfFile.Value.Data.Counters.ContainsKey(fileType) ? rpfFile.Value.Data.Counters[fileType].ToString() : "0");
                }
                writer.WriteLine(line);
            }
        }

        /// <summary>
        /// Write the header out to the CSV file.
        /// </summary>
        /// <param name="writer">Stream writer.</param>
        private void WriteHeader(StreamWriter writer)
        {
            writer.WriteLine("Rpf File,Drawables,Drawable Dictionaries,Texture Dictionaries,Bounds,Clips,Fragments");
        }

        private void ProcessIndependentFilePath(string filepath, IMapSection section, ConfigGameView gv, Common.IPlatformCollection platforms)
        {
            // Open up the packfiles we wish to process
            IDictionary<RSG.Platform.Platform, Packfile> packFiles = OpenPackFiles(filepath, gv, platforms);

            // Close all the pack files we opened
            foreach (KeyValuePair<RSG.Platform.Platform, Packfile> pair in packFiles)
            {
                pair.Value.Close();
            }
        }

        /// <summary>
        /// Uses the config's target info to convert the independent file path to per platform files and loads the resulting pack files.
        /// </summary>
        /// <param name="independentFilePath"></param>
        /// <param name="gv"></param>
        /// <returns>Dictionary mapping platform -> packfile containing all the loaded files</returns>
        private IDictionary<RSG.Platform.Platform, Packfile> OpenPackFiles(string independentFilePath, ConfigGameView gv, IPlatformCollection platforms)
        {
            IDictionary<RSG.Platform.Platform, Packfile> packFiles = new Dictionary<RSG.Platform.Platform, Packfile>();

            foreach (RSG.Platform.Platform platform in platforms)
            {
                string platformFilepath = ConvertIndependentPathToPlatformPath(independentFilePath, gv, platform);

                if (platformFilepath != null)
                {
                    // Load the pack file
                    packFiles[platform] = new Packfile();
                    packFiles[platform].Load(platformFilepath);
                    m_data.Files[platformFilepath] = new FileData(packFiles[platform]);
                }
            }

            return packFiles;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filepath"></param>
        /// <param name="gv"></param>
        /// <returns></returns>
        private string ConvertIndependentPathToPlatformPath(string filepath, ConfigGameView gv, RSG.Platform.Platform platform)
        {
            var config = RSG.Base.Configuration.ConfigFactory.CreateConfig();
            var target = config.Project.DefaultBranch.Targets[platform];

            string result = RSG.Pipeline.Services.Platform.PlatformPathConversion.ConvertAndRemapFilenameToPlatform(target, filepath);
            return result;
        }

        #endregion
    }
}

