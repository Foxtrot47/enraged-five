﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace RSG.Model.Statistics.Captures
{
    /// <summary>
    /// Model object representing a single submission to p4.
    /// </summary>
    public class Modification : IComparable<Modification>
    {
        #region Properties
        /// <summary>
        /// Changelist for this submission.
        /// </summary>
        public uint Changelist { get; private set; }

        /// <summary>
        /// Username that submitted this changelist
        /// </summary>
        public String Username { get; private set; }

        /// <summary>
        /// Comment the user added when submitting this changelist.
        /// </summary>
        public String Comment { get; private set; }

        /// <summary>
        /// Time of this submission.
        /// </summary>
        public DateTime Time { get; private set; }

        /// <summary>
        /// Url to view information about this changelist.
        /// </summary>
        public String Url { get; private set; }

        /// <summary>
        /// List of files that were submitted as part of this modification.
        /// </summary>
        public IList<String> Files { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="infos"></param>
        public Modification(IEnumerable<ModificationInfo> infos)
        {
            Debug.Assert(infos.Any(), "No modification infos supplied when attempting to create a new modification object.");
            if (!infos.Any())
            {
                throw new ArgumentException("No modification infos supplied when attempting to create a new modification object.");
            }

            Changelist = infos.First().Changelist;
            Username = infos.First().Username;
            Comment = infos.First().Comment;
            Time = infos.First().Time;
            Url = infos.First().Url;
            Files = new List<String>();

            foreach (ModificationInfo info in infos)
            {
                Files.Add(info.DepotPath);
            }
        }
        #endregion // Constructor(s)

        #region IComparable<Modification> Interface
        /// <summary>
        /// Compare this Modification to another.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(Modification other)
        {
            if (other == null)
            {
                return 1;
            }

            return (Changelist.CompareTo(other.Changelist));
        }
        #endregion // IComparable<Modification> Interface
    } // Modification
}
