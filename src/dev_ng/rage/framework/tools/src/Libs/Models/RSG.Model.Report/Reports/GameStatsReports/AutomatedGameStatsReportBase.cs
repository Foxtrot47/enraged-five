﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Report.Reports.GameStatsReports.Tests;
using RSG.SourceControl.Perforce;
using RSG.Model.Report.Reports.GameStatsReports.Model;
using System.IO;
using System.Web.UI;
using RSG.Base.Configuration.Reports;
using System.Net.Mail;
using System.Xml.Linq;
using System.Xml.XPath;

namespace RSG.Model.Report.Reports.GameStatsReports
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class AutomatedGameStatsReportBase : GameStatReportBase, IReportMailProvider, IReportStreamProvider
    {
        #region Constants
        /// <summary>
        /// Default sender of the automated stats report mail.
        /// </summary>
        private const string c_defaultEmailSender = "workbench@rockstarnorth.com";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Mail message to send when the report is invoked
        /// </summary>
        public MailMessage MailMessage
        {
            get;
            protected set;
        }

        /// <summary>
        /// Stream containing the html report data
        /// </summary>
        public Stream Stream
        {
            get;
            private set;
        }

        /// <summary>
        /// List of smoke tests that will be run against the data
        /// </summary>
        protected IList<ISmokeTest> SmokeTests
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        protected IList<GraphImage> AllImages
        {
            get;
            set;
        }

        /// <summary>
        /// Location of the file containing the submitted modifications
        /// </summary>
        protected string ModificationsFile
        {
            get
            {
                if (Mode == GameStatReportMode.PerChangelist)
                {
                    return ReportsConfig.AutomatedReports.PerChangelistModificationsFile;
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Email address for the sender of the email.
        /// </summary>
        public string EmailSender
        {
            get;
            set;
        }

        /// <summary>
        /// Comma separated list of the default people to email
        /// </summary>
        public string DefaultEmailList
        {
            get;
            set;
        }

        /// <summary>
        /// Whether the people named in the modifications list should be emailed
        /// </summary>
        public bool EmailModifiers
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="desc"></param>
        public AutomatedGameStatsReportBase(string name, string desc)
            : base(name, desc)
        {
            SmokeTests = new List<ISmokeTest>();
            EmailSender = c_defaultEmailSender;
        }
        #endregion // Constructor(s)

        #region GameStatReportBase Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="testSessions"></param>
        /// <param name="uniqueTestNames"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="cacheDir"></param>
        protected override void GenerateReport(IList<TestSession> testSessions, IEnumerable<string> uniqueTestNames, DateTime start, DateTime end, string cacheDir)
        {
            // Extract the list of modifications
            IList<ModificationInfo> modifications = ExtractModificationInfo();

            // Run the smoke test first
            RunSmokeTests(testSessions);

            // Generate any graphs that will be part of the report and save them out to the cache directory
            AllImages = GenerateGraphs(start, end);

            // Generate the html stream
            SaveImagesToCache(AllImages, cacheDir);
            Stream = GenerateContentStream(modifications, AllImages, uniqueTestNames, false);

            // Generate the email message
            Stream emailStream = GenerateContentStream(modifications, AllImages, uniqueTestNames, true);
            MailMessage = GenerateEmail(emailStream, AllImages, GetEmailAddresses(modifications), SmokeTests.SelectMany(test => test.Errors).Any());
        }
        #endregion // GameStatReportBase Overrides

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected List<ModificationInfo> ExtractModificationInfo()
        {
            List<ModificationInfo> modifications = new List<ModificationInfo>();

            if (!String.IsNullOrEmpty(ModificationsFile))
            {
                // Get the changes from the modifications file
                P4API.P4RecordSet recordSet = PerforceConnection.Run("print", String.Format("{0}#head", ModificationsFile));

                using (Stream stream = new MemoryStream())
                {
                    TextWriter writer = new StreamWriter(stream);
                    foreach (string message in recordSet.Messages)
                    {
                        writer.WriteLine(message);
                    }
                    writer.Flush();

                    // Load up the document as xml and extract the info we are after
                    stream.Position = 0;
                    XDocument doc = XDocument.Load(stream);

                    foreach (XElement elem in doc.XPathSelectElements("//ArrayOfModification/Modification"))
                    {
                        ModificationInfo modification = new ModificationInfo(elem);

                        // Ignore changes made by builder north and duplicate entries
                        if (modification.Username != "buildernorth" && !modifications.Contains(modification))
                        {
                            modifications.Add(modification);
                        }
                    }
                }

                // Update the email addresses for all the modifications
                foreach (ModificationInfo modification in modifications)
                {
                    P4API.P4RecordSet userRecordSet = PerforceConnection.Run("users", modification.Username);

                    if (userRecordSet.Records.Length > 0)
                    {
                        P4API.P4Record userRecord = userRecordSet.Records[0];
                        if (userRecord.Fields.ContainsKey("Email"))
                        {
                            modification.Email = userRecord["Email"];
                        }
                    }
                }
            }

            // Sort the changelists into ascending order before returning them
            modifications.Sort();
            modifications.Reverse();
            return modifications;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="testSessions"></param>
        protected void RunSmokeTests(IList<TestSession> testSessions)
        {
            foreach (ISmokeTest test in SmokeTests)
            {
                test.RunTest(testSessions);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="groupedFpsResults"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        protected IList<GraphImage> GenerateGraphs(DateTime start, DateTime end)
        {
            IList<GraphImage> images = new List<GraphImage>();

            foreach (ISmokeTest smokeTest in SmokeTests)
            {
                foreach (GraphImage image in smokeTest.GenerateGraphs(start, end))
                {
                    images.Add(image);
                }
            }

            return images;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="modifications"></param>
        /// <param name="images"></param>
        /// <param name="uniqueTestNames"></param>
        /// <returns></returns>
        protected Stream GenerateContentStream(IList<ModificationInfo> modifications, IList<GraphImage> images, IEnumerable<string> uniqueTestNames, bool email)
        {
            // Finally generate the report based on the smoke test's results
            MemoryStream stream = new MemoryStream();
            StreamWriter sw = new StreamWriter(stream);
            HtmlTextWriter writer = new HtmlTextWriter(sw);

            writer.RenderBeginTag(HtmlTextWriterTag.Html);
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Body);
                {
                    IEnumerable<GraphImage> commonGraphs = SmokeTests.SelectMany(test => test.CommonGraphs);

                    // Write the generic header
                    WriteReportHeader(writer, modifications, SmokeTests.SelectMany(test => test.Errors), SmokeTests.SelectMany(test => test.Warnings), SmokeTests.SelectMany(test => test.Messages));

                    // Write a table of contents if required
                    WriteReportTableOfContents(writer, commonGraphs, uniqueTestNames);

                    if (commonGraphs.Any())
                    {
                        WriteReportCommonGraphs(writer, commonGraphs, true, email);
                    }

                    WriteReportTestResults(writer, uniqueTestNames, email);
                }
                writer.RenderEndTag();
            }
            writer.RenderEndTag();
            writer.Flush();

            stream.Position = 0;
            return stream;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="uniqueTestNames"></param>
        protected void WriteReportTestResults(HtmlTextWriter writer, IEnumerable<string> uniqueTestNames, bool email)
        {
            // Output a header for the section
            writer.RenderBeginTag(HtmlTextWriterTag.H1);
            writer.AddAttribute(HtmlTextWriterAttribute.Name, "Individual Tests");
            writer.RenderBeginTag(HtmlTextWriterTag.A);
            writer.Write("Individual Tests");
            writer.RenderEndTag();
            writer.RenderEndTag();

            // Go over all the tests that ran
            foreach (string testName in uniqueTestNames)
            {
                // Output a header for this test
                writer.RenderBeginTag(HtmlTextWriterTag.H2);
                writer.AddAttribute(HtmlTextWriterAttribute.Name, testName);
                writer.RenderBeginTag(HtmlTextWriterTag.A);
                writer.Write(testName);
                writer.RenderEndTag();
                writer.RenderEndTag();
                
                // Output the html for each smoke test
                foreach (ISmokeTest test in SmokeTests)
                {
                    test.WriteReport(writer, testName, true, MaxTableResults, email);
                }
            }
        }

        /// <summary>
        /// Writes a table of contents to the report
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="imageStreams"></param>
        private void WriteReportTableOfContents(HtmlTextWriter writer, IEnumerable<GraphImage> commonImages, IEnumerable<string> uniqueTestNames)
        {
            // Header
            writer.RenderBeginTag(HtmlTextWriterTag.H1);
            writer.AddAttribute(HtmlTextWriterAttribute.Name, "Table of Contents");
            writer.RenderBeginTag(HtmlTextWriterTag.A);
            writer.Write("Table of Contents");
            writer.RenderEndTag();
            writer.RenderEndTag();

            // ToC
            // Commmon Graphs
            writer.AddAttribute(HtmlTextWriterAttribute.Href, String.Format("#{0}", "Common Graphs"));
            writer.RenderBeginTag(HtmlTextWriterTag.A);
            writer.Write("Common Graphs");
            writer.RenderEndTag();
            writer.WriteBreak();

            writer.RenderBeginTag(HtmlTextWriterTag.Ul);
            foreach (GraphImage image in commonImages)
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Li);
                writer.AddAttribute(HtmlTextWriterAttribute.Href, String.Format("#{0}", image.Name));
                writer.RenderBeginTag(HtmlTextWriterTag.A);
                writer.Write(image.Name);
                writer.RenderEndTag();
                writer.RenderEndTag();
            }
            writer.RenderEndTag();

            // Individual Tests
            writer.AddAttribute(HtmlTextWriterAttribute.Href, String.Format("#{0}", "Individual Tests"));
            writer.RenderBeginTag(HtmlTextWriterTag.A);
            writer.Write("Individual Tests");
            writer.RenderEndTag();
            writer.WriteBreak();

            // Go over all the tests that ran
            writer.RenderBeginTag(HtmlTextWriterTag.Ul);
            foreach (string testName in uniqueTestNames)
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Li);
                writer.AddAttribute(HtmlTextWriterAttribute.Href, String.Format("#{0}", testName));
                writer.RenderBeginTag(HtmlTextWriterTag.A);
                writer.Write(testName);
                writer.RenderEndTag();
                writer.RenderEndTag();
            }
            writer.RenderEndTag();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="modifications"></param>
        private void WriteReportModifications(HtmlTextWriter writer, List<ModificationInfo> modifications)
        {
            string defaultStyle = String.Format("background-color: {0}; color: {1}", BGCOL_DEFAULT, FGCOL_DEFAULT);

            writer.AddAttribute(HtmlTextWriterAttribute.Border, "1");
            writer.AddAttribute(HtmlTextWriterAttribute.Width, "100%");
            writer.RenderBeginTag(HtmlTextWriterTag.Table);
            {
                // Header row
                writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                writer.AddAttribute(HtmlTextWriterAttribute.Style, defaultStyle);
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write("changelist");
                writer.RenderEndTag();
                writer.AddAttribute(HtmlTextWriterAttribute.Style, defaultStyle);
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write("user");
                writer.RenderEndTag();
                writer.AddAttribute(HtmlTextWriterAttribute.Style, defaultStyle);
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write("summary");
                writer.RenderEndTag();
                writer.RenderEndTag();

                foreach (ModificationInfo modification in modifications)
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                    writer.RenderBeginTag(HtmlTextWriterTag.Td);
                    writer.AddAttribute(HtmlTextWriterAttribute.Href, modification.Url);
                    writer.RenderBeginTag(HtmlTextWriterTag.A);
                    writer.Write(modification.Changelist);
                    writer.RenderEndTag();
                    writer.RenderEndTag();
                    writer.RenderBeginTag(HtmlTextWriterTag.Td);
                    writer.AddAttribute(HtmlTextWriterAttribute.Href, String.Format("mailto:{0}", modification.Email));
                    writer.RenderBeginTag(HtmlTextWriterTag.A);
                    writer.Write(modification.Username);
                    writer.RenderEndTag();
                    writer.RenderEndTag();
                    writer.RenderBeginTag(HtmlTextWriterTag.Td);
                    writer.Write(modification.Comment);
                    writer.RenderEndTag();
                    writer.RenderEndTag();
                }
            }
            writer.RenderEndTag();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="modifications"></param>
        /// <returns></returns>
        private IEnumerable<string> GetEmailAddresses(IEnumerable<ModificationInfo> modifications)
        {
            if (DefaultEmailList != null)
            {
                foreach (string email in DefaultEmailList.Split(new char[] { ';' }))
                {
                    yield return email;
                }
            }

            if (Mode == GameStatReportMode.PerChangelist && EmailModifiers)
            {
                foreach (ModificationInfo modification in modifications)
                {
                    yield return modification.Email;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="contentStream"></param>
        /// <param name="imageStreams"></param>
        /// <param name="emailList"></param>
        /// <returns></returns>
        private MailMessage GenerateEmail(Stream contentStream, IEnumerable<GraphImage> images, IEnumerable<string> emailList, bool testsGeneratedError)
        {
            MailMessage mailMessage = new MailMessage();

            // Add basic information.
            mailMessage.From = new MailAddress(EmailSender);
            mailMessage.Subject = Name;

            foreach (string email in emailList.Distinct())
            {
                mailMessage.To.Add(email);
            }

            // Create the HTML view of the message and add it to the mail
            AlternateView htmlView = null;

            contentStream.Position = 0;
            using (StreamReader reader = new StreamReader(contentStream))
            {
                string mailContent = reader.ReadToEnd();
                htmlView = AlternateView.CreateAlternateViewFromString(mailContent, null, "text/html");
                mailMessage.AlternateViews.Add(htmlView);

                // Add images to HTML version
                foreach (GraphImage image in images)
                {
                    image.DataStream.Position = 0;
                    LinkedResource imageResource = new LinkedResource(image.DataStream);
                    imageResource.ContentId = image.Name;
                    htmlView.LinkedResources.Add(imageResource);
                }
            }

            return mailMessage;
        }

        /// <summary>
        /// Saves all the images to the cache dir
        /// </summary>
        /// <param name="images"></param>
        /// <param name="cacheDir"></param>
        private void SaveImagesToCache(IList<GraphImage> images, string cacheDir)
        {
            foreach (GraphImage image in images)
            {
                image.Filepath = Path.Combine(cacheDir, String.Format("{0}.png", image.Name));

                using (Stream fileStream = File.Create(image.Filepath))
                {
                    image.DataStream.CopyTo(fileStream);
                    fileStream.Flush();
                }
            }
        }
        #endregion // Private Methods

        #region IDisposable Override
        /// <summary>
        /// Perform cleanup
        /// </summary>
        public override void Dispose()
        {
            base.Dispose();

            // Dispose of the mail message if one exists
            if (MailMessage != null)
            {
                MailMessage.Dispose();
            }

            if (Stream != null)
            {
                Stream.Dispose();
            }

            if (AllImages != null)
            {
                foreach (GraphImage image in AllImages)
                {
                    image.Dispose();
                }
            }
        }
        #endregion // IDisposable Override
    } // CompositeAutomatedReportBase
}
