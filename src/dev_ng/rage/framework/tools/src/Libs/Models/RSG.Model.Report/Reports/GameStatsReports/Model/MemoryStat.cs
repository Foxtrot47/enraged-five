﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Report.Reports.GameStatsReports.Model
{
    public class MemoryStat
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public uint Min
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public uint Max
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public float Average
        {
            get;
            private set;
        }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <param name="average"></param>
        public MemoryStat(uint min, uint max, float average)
        {
            Min = min;
            Max = max;
            Average = average;
        }
        #endregion // Constructor(s)
    } // MemoryStat
}
