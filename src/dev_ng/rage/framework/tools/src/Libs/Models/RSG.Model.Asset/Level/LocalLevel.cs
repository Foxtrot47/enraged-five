﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;
using System.Xml;
using RSG.Base.Math;
using RSG.Model.Common;
using System.Xml.XPath;
using RSG.Base.Configuration;
using RSG.Base.Configuration.Bugstar;
using RSG.Base.ConfigParser;
using RSG.Base.Logging;
using RSG.Interop.Bugstar;
using RSG.Interop.Bugstar.Organisation;
using RSG.Interop.Bugstar.Game;
using RSG.Model.Common.Map;
using RSG.Model.Common.Extensions;
using RSG.Base.Configuration.Bugstar;
using RSG.Base.Configuration;

namespace RSG.Model.Asset.Level
{
    /// <summary>
    /// 
    /// </summary>
    public class LocalLevel : Level
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public LocalLevel(String name, ILevelCollection parentCollection)
            : base(name, parentCollection)
        {
        }
        #endregion // Constructor(s)

        #region StreamableAssetContainerBase Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="statsToLoad"></param>
        protected override void LoadStatsInternal(IEnumerable<StreamableStat> statsToLoad)
        {
            Debug.Assert(ParentCollection is LocalLevelCollection, "Parent collection isn't an LocalLevelCollection.");
            LocalLevelCollection localCollection = (LocalLevelCollection)ParentCollection;

            // Check what sort of stats we should be loading.
            StreamableStat[] basicStats = new StreamableStat[] { StreamableLevelStat.ExportDataPath, StreamableLevelStat.ProcessedDataPath };
            StreamableStat[] bugstarStats = new StreamableStat[] { StreamableLevelStat.BackgroundImage, StreamableLevelStat.ImageBounds };
            StreamableStat[] spawnPointStats = new StreamableStat[] { StreamableLevelStat.SpawnPoints, StreamableLevelStat.SpawnPointClusters, StreamableLevelStat.SpawnPointClusters };

            if (statsToLoad.Intersect(basicStats).Any())
            {
                LoadBasicExportStats(localCollection.GameView);
            }
            if (statsToLoad.Intersect(bugstarStats).Any())
            {
                LoadStatsFromBugstar(localCollection.BugstarConfig, localCollection.Config);
            }
            if (statsToLoad.Intersect(spawnPointStats).Any())
            {
                LoadSpawnPointStats(localCollection.GameView);
            }
        }
        #endregion // StreamableAssetContainerBase Overrides

        #region Internal Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="gv"></param>
        internal void LoadBasicExportStats(ConfigGameView gv)
        {
            ContentNodeLevel levelNode = this.GetConfigLevelNode(gv);
            if (null != levelNode)
            {
                String exportDir = Path.GetFullPath(gv.ExportDir);
                String processedDir = Path.GetFullPath(gv.ProcessedDir);
                this.ExportDataPath = Path.GetFullPath(levelNode.AbsolutePath);
                this.ProcessedDataPath = Path.GetFullPath(this.ExportDataPath.Replace(exportDir, processedDir));
            }
            else
            {
                SetStatLodaded(StreamableLevelStat.ExportDataPath, true);
                SetStatLodaded(StreamableLevelStat.ProcessedDataPath, true);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gv"></param>
        internal void LoadStatsFromBugstar(IBugstarConfig bugstarConfig, IConfig config)
        {
            this.ImageBounds = new BoundingBox2f();

            // Check if an entry exists for this level in the game config
            uint levelId;
            if (bugstarConfig.TryGetLevelId(this.Name, out levelId))
            {
                // Query bugstar for the background image/bounds
                bool dataRetrieved = false;

                BugstarConnection connection = new BugstarConnection(bugstarConfig.RESTService, bugstarConfig.AttachmentService);
                connection.Login(bugstarConfig.ReadOnlyUsername, bugstarConfig.ReadOnlyPassword, "");
                if (connection != null)
                {
                    try
                    {
                        Project project = Project.GetProjectById(connection, bugstarConfig.ProjectId);
                        if (project != null)
                        {
                            Map bugstarMap = Map.GetMapById(project, levelId);
                            if (bugstarMap != null)
                            {
                                this.Image = bugstarMap.Image;
                                this.Image.Freeze();
                                this.ImageBounds.Min = bugstarMap.LowerLeft;
                                this.ImageBounds.Max = bugstarMap.UpperRight;

                                // Save the information out to the local cache
                                SaveDataToCache(config.CoreProject.Cache);
                                dataRetrieved = true;
                            }
                        }
                    }
                    catch (System.Exception ex)
                    {
                        Log.Log__Exception(ex, "Unhandled exception during Bugstar connection.");
                    }
                }

                // Check if we managed to retrieve the image
                if (!dataRetrieved)
                {
                    // Bugstar appears to be down (or there was some other problem).  Attempt to get the information from the cache instead.
                    LoadDataFromCache(config.CoreProject.Cache);
                }
            }

            // Make sure the data is set as loaded (to prevent multiple requests for the same data).
            if (Image == null)
            {
                SetStatLodaded(StreamableLevelStat.BackgroundImage, true);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gv"></param>
        internal void LoadSpawnPointStats(ConfigGameView gv)
        {
            IList<ISpawnPoint> spawnPoints = new List<ISpawnPoint>();
            IList<ISpawnPointCluster> spawnPointClusters = new List<ISpawnPointCluster>();
            IList<IChainingGraph> chainingGraphs = new List<IChainingGraph>();

            ContentNodeLevel levelNode = (ContentNodeLevel)gv.Content.Root.FindAll(Name, "level").FirstOrDefault();
            if (levelNode != null)
            {
                this.ExportDataPath = Path.GetFullPath(levelNode.AbsolutePath);
                this.ProcessedDataPath = Path.GetFullPath(
                    levelNode.AbsolutePath.Replace(gv.ExportDir, gv.ProcessedDir));

                string metafileDirectory = Path.GetFullPath(Path.Combine(levelNode.AbsolutePath, "scenario"));

                if (Directory.Exists(metafileDirectory))
                {
                    foreach (string metafile in Directory.GetFiles(metafileDirectory))
                    {
                        try
                        {
                            System.Xml.Linq.XDocument xmlDoc = System.Xml.Linq.XDocument.Load(metafile);
                            SpawnPointLookupTables tables = new SpawnPointLookupTables(xmlDoc);

                            foreach (System.Xml.Linq.XElement elem in xmlDoc.XPathSelectElements("//Points/MyPoints/Item"))
                            {
                                spawnPoints.Add(new SpawnPoint(elem, metafile, tables));
                            }

                            foreach (System.Xml.Linq.XElement elem in xmlDoc.XPathSelectElements("//Clusters/Item"))
                            {
                                spawnPointClusters.Add(new SpawnPointCluster(elem, metafile, tables));
                            }

                            foreach (System.Xml.Linq.XElement elem in xmlDoc.XPathSelectElements("//ChainingGraph"))
                            {
                                chainingGraphs.Add(new ChainingGraph(elem, metafile));
                            }
                        }
                        catch (Exception)
                        {
                            //Protect against syntax errors in the XML.  
                            //For example, a shortcut item within this directory would cause a DecoderFallbackException.
                        }
                    }
                }
            }

            SpawnPoints = spawnPoints;
            SpawnPointClusters = spawnPointClusters;
            ChainingGraphs = chainingGraphs;
        }
        #endregion // Internal Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private void SaveDataToCache(string cacheDir)
        {
            // Save the background image to file (if its not null)
            if (this.Image != null)
            {
                string imagePath = GetCachedImageFilePath(cacheDir);
                string imageDir = Path.GetDirectoryName(imagePath);
                if (!Directory.Exists(imageDir))
                {
                    Directory.CreateDirectory(imageDir);
                }

                FileStream stream = new FileStream(GetCachedImageFilePath(cacheDir), FileMode.Create);
                JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                encoder.QualityLevel = 100;
                encoder.Frames.Add(BitmapFrame.Create(this.Image));
                encoder.Save(stream);
                stream.Close();
            }

            // Save out the bounds
            string boundsPath = GetCachedBoundsFilePath(cacheDir);
            string boundsDir = Path.GetDirectoryName(boundsPath);
            if (!Directory.Exists(boundsDir))
            {
                Directory.CreateDirectory(boundsDir);
            }

            XmlWriter writer = XmlWriter.Create(boundsPath);
            writer.WriteStartElement("bounds");
            writer.WriteStartElement("lowerleft");
            writer.WriteElementString("x", this.ImageBounds.Min.X.ToString());
            writer.WriteElementString("y", this.ImageBounds.Min.Y.ToString());
            writer.WriteEndElement();
            writer.WriteStartElement("upperright");
            writer.WriteElementString("x", this.ImageBounds.Max.X.ToString());
            writer.WriteElementString("y", this.ImageBounds.Max.Y.ToString());
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.Close();
        }

        /// <summary>
        /// 
        /// </summary>
        private void LoadDataFromCache(string cacheDir)
        {
            // Load in the background image from the cache
            string imagePath = GetCachedImageFilePath(cacheDir);

            if (File.Exists(imagePath))
            {
                Stream imageStreamSource = new FileStream(imagePath, FileMode.Open, FileAccess.Read, FileShare.Read);

                BitmapImage bImg = new BitmapImage();
                bImg.BeginInit();
                bImg.StreamSource = imageStreamSource;
                bImg.EndInit();

                this.Image = bImg;
                this.Image.Freeze();
            }

            // Load in the bounds from the cache
            string boundsPath = GetCachedBoundsFilePath(cacheDir);

            if (File.Exists(boundsPath))
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(boundsPath);

                this.ImageBounds = new BoundingBox2f();

                // Bounds
                XmlNode lowerLeftXNode = doc.SelectSingleNode("/bounds/lowerleft/x");
                XmlNode lowerLeftYNode = doc.SelectSingleNode("/bounds/lowerleft/y");
                this.ImageBounds.Min = new Vector2f(Single.Parse(lowerLeftXNode.InnerText), Single.Parse(lowerLeftYNode.InnerText));

                XmlNode upperRightXNode = doc.SelectSingleNode("/bounds/upperright/x");
                XmlNode upperRightYNode = doc.SelectSingleNode("/bounds/upperright/y");
                this.ImageBounds.Max = new Vector2f(Single.Parse(upperRightXNode.InnerText), Single.Parse(upperRightYNode.InnerText));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cacheDir"></param>
        /// <returns></returns>
        private string GetCachedImageFilePath(string cacheDir)
        {
            return Path.Combine(cacheDir, "Bugstar", "Map", this.Name, "image.jpg");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cacheDir"></param>
        /// <returns></returns>
        private string GetCachedBoundsFilePath(string cacheDir)
        {
            return Path.Combine(cacheDir, "Bugstar", "Map", this.Name, "bounds.xml");
        }
        #endregion // Private Methods
    } // LocalLevel
}
