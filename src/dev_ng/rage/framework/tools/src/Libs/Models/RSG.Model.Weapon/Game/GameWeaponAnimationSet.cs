﻿namespace RSG.Model.Weapon.Game
{
    using System.Collections.Generic;
    using System.Diagnostics;

    /// <summary>
    /// Represents a single Weapon that is being mounted by the game through the waepons.meta
    /// data file.
    /// </summary>
    [DebuggerDisplay("Name = {Name}")]
    public class GameWeaponAnimationSet
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Name"/> property.
        /// </summary>
        private string _name;

        /// <summary>
        /// The private field used for the <see cref="ClipSets"/> property.
        /// </summary>
        private readonly List<string> _clipSets;

        /// <summary>
        /// The private field used for the <see cref="ReferenceCount"/> property.
        /// </summary>
        private int _referenceCount;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="GameWeaponAnimationSet"/> class.
        /// </summary>
        public GameWeaponAnimationSet()
        {
            this._clipSets = new List<string>();
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the name used for this animation set.
        /// </summary>
        public string Name
        {
            get { return this._name; }
            set { this._name = value; }
        }

        /// <summary>
        /// Gets or sets a iterator around the individual clip sets inside this animation set.
        /// </summary>
        public IEnumerable<string> ClipSets
        {
            get
            {
                foreach (string clipSet in this._clipSets)
                {
                    yield return clipSet;
                }
            }
        }

        /// <summary>
        /// Gets or sets the number of times this archetype has been referenced by either a
        /// weapon, component, or ammo object.
        /// </summary>
        public int ReferenceCount
        {
            get { return this._referenceCount; }
            set { this._referenceCount = value; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Adds the specified clip set name into this sets list.
        /// </summary>
        /// <param name="clipSet">
        /// The name of the clip set to add.
        /// </param>
        public void AddClipSet(string clipSet)
        {
            this._clipSets.Add(clipSet);
        }
        #endregion Methods
    } // RSG.Model.Weapon.Game.GameWeaponAnimationSet {Class}
} // RSG.Model.Weapon.Game {Namespace}
