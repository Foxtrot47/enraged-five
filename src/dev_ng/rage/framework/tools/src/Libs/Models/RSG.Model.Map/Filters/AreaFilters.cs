﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Map.Filters
{

    /// <summary>
    /// Common map area filter methods.
    /// </summary>
    public static class AreaFilters
    {
        /// <summary>
        /// Returns true for all areas so that everything gets loaded
        /// </summary>
        /// <param name="mapNode"></param>
        /// <returns></returns>
        public static bool All(RSG.Base.ConfigParser.ContentNodeGroup groupNode)
        {
            return (true);
        }
    }

} // RSG.Model.Map.Filters namespace
