﻿namespace RSG.Model.Report2.Reports.Map
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using RSG.Base.ConfigParser;
    using RSG.Base.Tasks;
    using RSG.ManagedRage;
    using RSG.Model.Common;
    using RSG.Model.Common.Map;
    using RSG.Model.Report;
    using RSG.Platform;
    using RSG.SceneXml;

    /// <summary>
    /// A report that lists all of the rpfs with a break down of the file types inside them
    /// with their counts.
    /// </summary>
    public class RpfSizesReport : CSVReport, IDynamicLevelReport
    {
        #region Fields
        /// <summary>
        /// The name of this report.
        /// </summary>
        private const string Name = "Rpf Sizes";

        /// <summary>
        /// The description for this report.
        /// </summary>
        private const string Description =
            "Exports a csv report that lists all of the rpfs with a break down of the file" +
            " types inside them with their counts";

        /// <summary>
        /// The private field used for the <see cref="GenerationTask"/> property.
        /// </summary>
        private ITask _generationTask;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="RpfSizesReport"/> class.
        /// </summary>
        public RpfSizesReport()
            : base(Name, Description)
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the task that represents the work needing to be done to generate this report.
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (this._generationTask == null)
                {
                    CompositeTask task = new CompositeTask("Generate Report");
                    task.AddSubTask(new ActionTask("Generating report", this.GenerateReport));
                    this._generationTask = task;
                }

                return this._generationTask;
            }
        }

        /// <summary>
        /// Gets a iterator around the stats that are required to generate this report. Always
        /// returns null.
        /// </summary>
        public IEnumerable<StreamableStat> RequiredStats
        {
            get { return null; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Generates the report and saves the file associated with it.
        /// </summary>
        /// <param name="context">
        /// The task context for this action.
        /// </param>
        /// <param name="progress">
        /// The object used to notify a listener of the progress.
        /// </param>
        private void GenerateReport(ITaskContext context, IProgress<TaskProgress> progress)
        {
            DynamicLevelReportContext reportContext = context as DynamicLevelReportContext;
            Debug.Assert(reportContext != null, "Unable to generate report");
            if (reportContext == null)
            {
                return;
            }

            IMapHierarchy hierarchy = reportContext.Level.MapHierarchy;
            Debug.Assert(hierarchy != null, "Unable to generate report");
            if (hierarchy == null || hierarchy.AllSections == null)
            {
                return;
            }

            Dictionary<string, Dictionary<string, int>> previousData = this.PreviousData();

            Dictionary<string, Dictionary<string, int>> data = new Dictionary<string, Dictionary<string, int>>();

            string path = Path.Combine(reportContext.GameView.BuildDir, "ps3", "levels", reportContext.Level.Name);
            string[] rpfs = Directory.GetFiles(path, "*.rpf", SearchOption.AllDirectories);
            foreach (string rpf in rpfs)
            {
                Dictionary<string, int> counts = new Dictionary<string, int>();
                counts.Add("Resource Count", 0);
                counts.Add("File Count", 0);
                data.Add(rpf, counts);
            }

            foreach (string fileType in Enum.GetNames(typeof(RSG.Platform.FileType)))
            {
                foreach (Dictionary<string, int> counts in data.Values)
                {
                    counts.Add(fileType, 0);
                }
            }

            Dictionary<string, string> extensionMap = new Dictionary<string, string>();
            foreach (FileType fileType in Enum.GetValues(typeof(RSG.Platform.FileType)))
            {
                string exportExtension;
                string platformExtension;
                fileType.GetExtensions(out exportExtension, out platformExtension);
                if (platformExtension == null)
                {
                    continue;
                }

                platformExtension = "." + platformExtension.Replace('?', 'c');
                if (extensionMap.ContainsKey(platformExtension))
                {
                    continue;
                }

                extensionMap.Add(platformExtension, fileType.ToString());
            }

            foreach (string rpf in rpfs)
            {
                using (Packfile packFile = new Packfile())
                {
                    packFile.Load(rpf);
                    if (packFile.Entries != null)
                    {
                        Dictionary<string, int> counts = data[rpf];
                        foreach (PackEntry entity in packFile.Entries)
                        {
                            if (entity.IsResource)
                            {
                                counts["Resource Count"]++;
                            }
                            else if (entity.IsFile)
                            {
                                counts["File Count"]++;
                            }

                            string extension = Path.GetExtension(entity.Name);
                            string type;
                            if (!extensionMap.TryGetValue(extension, out type))
                            {
                                type = FileType.Unknown.ToString();
                            }

                            counts[type]++;
                        }
                    }

                    packFile.Close();
                }
            }

            this.WriteCsvFile(data);
            this.WriteCsvComparisonFile(data, previousData);
        }

        /// <summary>
        /// Wtites out the CSV file using the data gathered at the start of the
        /// <see cref="GenerateReport"/> method.
        /// </summary>
        /// <param name="validEntities">
        /// The list containing the valid entities to output to the csv file.
        /// </param>
        private void WriteCsvFile(Dictionary<string, Dictionary<string, int>> data)
        {
            using (StreamWriter writer = new StreamWriter(this.Filename))
            {
                string header = "Filename,Resource Count,File Count";
                foreach (string fileType in Enum.GetNames(typeof(RSG.Platform.FileType)))
                {
                    header += "," + fileType;
                }

                writer.WriteLine(header);
                WriteCsvData(writer, data);
            }
        }

        /// <summary>
        /// Wtites out the CSV file using the data gathered at the start of the
        /// <see cref="GenerateReport"/> method.
        /// </summary>
        /// <param name="validEntities">
        /// The list containing the valid entities to output to the csv file.
        /// </param>
        private void WriteCsvComparisonFile(Dictionary<string, Dictionary<string, int>> data, Dictionary<string, Dictionary<string, int>> previousdata)
        {
            Dictionary<string, Dictionary<string, int>> comparison = new Dictionary<string, Dictionary<string, int>>();
            foreach (KeyValuePair<string, Dictionary<string, int>> record in data)
            {
                comparison.Add(record.Key, new Dictionary<string, int>());
                Dictionary<string, int> previous;
                if (!previousdata.TryGetValue(record.Key, out previous))
                {
                    foreach (KeyValuePair<string, int> entry in record.Value)
                    {
                        comparison[record.Key].Add(entry.Key, entry.Value);
                    }
                }
                else
                {
                    foreach (KeyValuePair<string, int> entry in record.Value)
                    {
                        int previousValue;
                        if (!previous.TryGetValue(entry.Key, out previousValue))
                        {
                            comparison[record.Key].Add(entry.Key, entry.Value);
                        }
                        else
                        {
                            comparison[record.Key].Add(entry.Key, entry.Value - previousValue);
                        }
                    }
                }
            }

            foreach (KeyValuePair<string, Dictionary<string, int>> record in previousdata)
            {
                if (!comparison.ContainsKey(record.Key))
                {
                    comparison.Add(record.Key, new Dictionary<string, int>());
                    foreach (KeyValuePair<string, int> entry in record.Value)
                    {
                        comparison[record.Key].Add(entry.Key, -entry.Value);
                    }
                }
            }

            string filename = Path.Combine(Path.GetDirectoryName(this.Filename), Path.GetFileNameWithoutExtension(this.Filename) + "_delta" + Path.GetExtension(this.Filename));
            using (StreamWriter writer = new StreamWriter(filename))
            {
                string header = "Filename,Resource Count,File Count";
                foreach (string fileType in Enum.GetNames(typeof(RSG.Platform.FileType)))
                {
                    header += "," + fileType;
                }

                writer.WriteLine(header);
                WriteCsvData(writer, comparison);
            }
        }

        /// <summary>
        /// Writes the data out to the specified stream.
        /// </summary>
        /// <param name="writer">
        /// The stream to write the comma separated data into.
        /// </param>
        /// <param name="validEntities">
        /// The list containing the valid entities to output to the csv file.
        /// </param>
        private void WriteCsvData(StreamWriter writer, Dictionary<string, Dictionary<string, int>> data)
        {
            foreach (KeyValuePair<string, Dictionary<string, int>> record in data)
            {
                string csv = record.Key;
                foreach (int counts in record.Value.Values)
                {
                    csv += "," + counts.ToString();
                }

                writer.WriteLine(csv);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private Dictionary<string, Dictionary<string, int>> PreviousData()
        {
            Dictionary<string, Dictionary<string, int>> data = new Dictionary<string, Dictionary<string, int>>();
            if (!File.Exists(this.Filename))
            {
                return data;
            }

            Dictionary<int, string> headerMap = new Dictionary<int, string>();
            int lineNumber = 0;
            using (StreamReader file = new StreamReader(this.Filename))
            {
                string line;
                while ((line = file.ReadLine()) != null)
                {
                    if (lineNumber == 0)
                    {
                        string[] headers = line.Split(',');
                        for (int i = 0; i < headers.Length; i++)
                        {
                            headerMap.Add(i, headers[i]);
                        }
                    }
                    else
                    {
                        string[] values = line.Split(',');
                        string rpf = null;
                        for (int i = 0; i < values.Length; i++)
                        {
                            string header = headerMap[i];
                            if (header == "Filename")
                            {
                                rpf = values[i];
                                data.Add(values[i], new Dictionary<string, int>());
                                break;
                            }
                        }

                        if (rpf != null)
                        {
                            for (int i = 0; i < values.Length; i++)
                            {
                                string header = headerMap[i];
                                if (header != "Filename")
                                {
                                    data[rpf].Add(header, int.Parse(values[i]));
                                }
                            }
                        }
                    }

                    lineNumber++;
                }
            }

            return data;
        }
        #endregion Methods
    } // RSG.Model.Report2.Reports.Map.GhostPrologueReport {Class}
} // RSG.Model.Report2.Reports.Map {Namespace}
