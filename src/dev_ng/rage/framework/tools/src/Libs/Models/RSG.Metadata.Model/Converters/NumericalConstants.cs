﻿//---------------------------------------------------------------------------------------------
// <copyright file="NumericalConstants.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Converters
{
    using System;
    using System.Collections.Generic;
    using RSG.Base;
    using RSG.Metadata.Model.Helpers;

    /// <summary>
    /// Provides a look up of numerical constants by their string constant representation. This
    /// class cannot be inherited.
    /// </summary>
    public sealed class NumericalConstants
    {
        #region Fields
        /// <summary>
        /// The private dictionary that contains numerical types indexed by the object type.
        /// </summary>
        private static Dictionary<Type, NumericalConstantType> _numericalTypeMap;

        /// <summary>
        /// The private dictionary containing the numerical constants grouped by their type.
        /// </summary>
        private Dictionary<NumericalConstantType, ConstantCollection> _constants;

        /// <summary>
        /// The private collection of converters to use to get the numerical values for the
        /// constants.
        /// </summary>
        private ConverterCollection _converters;

        /// <summary>
        /// A private collection containing the constants that are being parsed. This is to
        /// stop circular dependencies.
        /// </summary>
        private HashSet<string> _retrievingConstant;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="NumericalConstants"/> class.
        /// </summary>
        static NumericalConstants()
        {
            _numericalTypeMap = new Dictionary<Type, NumericalConstantType>();
            _numericalTypeMap.Add(typeof(long), NumericalConstantType.Long);
            _numericalTypeMap.Add(typeof(int), NumericalConstantType.Int);
            _numericalTypeMap.Add(typeof(short), NumericalConstantType.Short);
            _numericalTypeMap.Add(typeof(sbyte), NumericalConstantType.SignedByte);
            _numericalTypeMap.Add(typeof(ulong), NumericalConstantType.UnsignedLong);
            _numericalTypeMap.Add(typeof(uint), NumericalConstantType.UnsignedInt);
            _numericalTypeMap.Add(typeof(ushort), NumericalConstantType.UnsignedShort);
            _numericalTypeMap.Add(typeof(byte), NumericalConstantType.Byte);
            _numericalTypeMap.Add(typeof(float), NumericalConstantType.Float);
            _numericalTypeMap.Add(typeof(double), NumericalConstantType.Double);
            _numericalTypeMap.Add(typeof(decimal), NumericalConstantType.Decimal);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="NumericalConstants"/> class.
        /// </summary>
        /// <param name="converters">
        /// The collection containing the converter objects to use.
        /// </param>
        public NumericalConstants(ConverterCollection converters)
        {
            this._converters = converters;
            this._constants = new Dictionary<NumericalConstantType, ConstantCollection>();
            this._retrievingConstant = new HashSet<string>();
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Adds a new constant to this class with the specified parameters.
        /// </summary>
        /// <param name="name">
        /// The lookup index name for the new constant.
        /// </param>
        /// <param name="value">
        /// The value of the new constant as a string.
        /// </param>
        /// <param name="type">
        /// The type of the new constant.
        /// </param>
        public void Add(string name, string value, NumericalConstantType type)
        {
            Constant newConstant = new Constant(name, value);
            ConstantCollection collection = null;
            lock (this._constants)
            {
                if (!this._constants.TryGetValue(type, out collection))
                {
                    collection = new ConstantCollection();
                    this._constants.Add(type, collection);
                }
            }

            collection.Add(newConstant);
        }

        /// <summary>
        /// Removes all of the currently loaded constant leaving just the base constants.
        /// </summary>
        public void Clear()
        {
            this.Initialise();
        }

        /// <summary>
        /// Attempts to get the corresponding numerical value of the specified constant name
        /// and returns a structure containing the corresponding value and a value indicating
        /// whether the specified name is a known constant.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the corresponding numerical value of the specified constant name.
        /// </typeparam>
        /// <param name="name">
        /// The name of the constant whose associated value should be returned in the
        /// specified type parameter.
        /// </param>
        /// <returns>
        /// A structure containing the corresponding numerical value of the specified constant
        /// name and a value indicating whether the specified name is a known constant.
        /// </returns>
        public TryResult<T> GetConstant<T>(string name)
        {
            NumericalConstantType type = NumericalConstantType.Unknown;
            if (!_numericalTypeMap.TryGetValue(typeof(T), out type))
            {
                throw new KeyNotFoundException();
            }

            ConstantCollection collection = null;
            if (!this._constants.TryGetValue(type, out collection))
            {
                return new TryResult<T>(default(T), false);
            }

            Constant constant;
            if (!collection.TryGetValue(name, out constant) || constant == null)
            {
                return new TryResult<T>(default(T), false);
            }

            string rawValue = constant.ConvertedValue as string;
            if (rawValue != null)
            {
                if (this._retrievingConstant.Contains(constant.Name))
                {
                    return new TryResult<T>(default(T), false);
                }

                this._retrievingConstant.Add(constant.Name);
                TryResult<T> convertResult = rawValue.TryTo<T>(default(T), this._converters);
                this._retrievingConstant.Remove(constant.Name);
                if (!convertResult.Success)
                {
                    return new TryResult<T>(default(T), false);
                }

                constant.ConvertedValue = convertResult.Value;
                return new TryResult<T>(convertResult.Value, true);
            }

            return new TryResult<T>((T)constant.ConvertedValue, true);
        }

        /// <summary>
        /// Removes all occurrences of the constant with the specified name and type.
        /// </summary>
        /// <param name="name">
        /// The name of the constant to remove.
        /// </param>
        /// <param name="type">
        /// The type of the constant to remove.
        /// </param>
        public void Remove(string name, NumericalConstantType type)
        {
            ConstantCollection collection = null;
            if (!this._constants.TryGetValue(type, out collection))
            {
                return;
            }

            collection.Remove(name);
        }

        /// <summary>
        /// Initialises the constant dictionary will default values.
        /// </summary>
        private void Initialise()
        {
            this._constants.Clear();
        }
        #endregion Methods

        #region Classes
        /// <summary>
        /// Represents a single constant that can be referenced by a single index name.
        /// </summary>
        private class Constant
        {
            #region Fields
            /// <summary>
            /// The private field used for the <see cref="ConvertedValue"/> property.
            /// </summary>
            private object _convertedValue;

            /// <summary>
            /// The private field used for the <see cref="Name"/> value.
            /// </summary>
            private string _name;

            /// <summary>
            /// The private field used for the <see cref="RawValue"/> value.
            /// </summary>
            private string _rawValue;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="Constant"/> class.
            /// </summary>
            /// <param name="name">
            /// The lookup index name for this constant.
            /// </param>
            /// <param name="value">
            /// The value of the constant as a string.
            /// </param>
            public Constant(string name, string value)
            {
                this._name = name;
                this._rawValue = value;
                this._convertedValue = value;
            }
            #endregion Constructors

            #region Properties
            /// <summary>
            /// Gets or sets the value of this constant.
            /// </summary>
            public object ConvertedValue
            {
                get { return this._convertedValue; }
                set { this._convertedValue = value; }
            }

            /// <summary>
            /// Gets or sets the lookup index name for this constant.
            /// </summary>
            public string Name
            {
                get { return this._name; }
                set { this._name = value; }
            }

            /// <summary>
            /// Gets the raw value for this constant before it has been converted to the
            /// appropriate type.
            /// </summary>
            public string RawValue
            {
                get { return this._rawValue; }
            }
            #endregion Properties
        } // NumericalConstants.Constant {Class}

        /// <summary>
        /// Defines a container for all the constants of a particular type and manages
        /// constants that have the same name.
        /// </summary>
        private class ConstantCollection
        {
            #region Fields
            /// <summary>
            /// A private dictionary containing the constants that are apart of this collection
            /// that have unique names or were the first to have a name that has been
            /// duplicated.
            /// </summary>
            private Dictionary<string, Constant> _constants;

            /// <summary>
            /// A private list containing all of the constants that are apart of this
            /// collection but don't have unique names.
            /// </summary>
            private List<Constant> _duplicates;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="ConstantCollection"/> class.
            /// </summary>
            public ConstantCollection()
            {
                this._duplicates = new List<Constant>();
                this._constants = new Dictionary<string, Constant>();
            }
            #endregion Constructors

            #region Properties
            /// <summary>
            /// Gets a iterator around all of the constants defined in this container starting
            /// with the unique ones and then any that are duplicates.
            /// </summary>
            public IEnumerable<Constant> AllConstants
            {
                get
                {
                    foreach (Constant constant in this._constants.Values)
                    {
                        yield return constant;
                    }

                    foreach (Constant constant in this._duplicates)
                    {
                        yield return constant;
                    }
                }
            }

            /// <summary>
            /// Gets the value associated with the specified name.
            /// </summary>
            /// <param name="name">
            /// The name of the value to get.
            /// </param>
            /// <returns>
            /// The value associated with the specified name. If the specified name is not
            /// found, a System.Collections.Generic.KeyNotFoundException is thrown.
            /// </returns>
            public Constant this[string name]
            {
                get { return this._constants[name]; }
            }
            #endregion Properties

            #region Methods
            /// <summary>
            /// Adds the specified constant to this container.
            /// </summary>
            /// <param name="constant">
            /// The constant to add to this container.
            /// </param>
            public void Add(Constant constant)
            {
                if (constant == null)
                {
                    return;
                }

                lock (this._constants)
                {
                    if (this._constants.ContainsKey(constant.Name))
                    {
                        if (!this._duplicates.Contains(constant))
                        {
                            this._duplicates.Add(constant);
                        }
                    }
                    else
                    {
                        this._constants.Add(constant.Name, constant);
                    }
                }
            }

            /// <summary>
            /// Removes all constants in this container with the specified name.
            /// </summary>
            /// <param name="name">
            /// The name to the constants to remove.
            /// </param>
            public void Remove(string name)
            {
                this._constants.Remove(name);
                this._duplicates.RemoveAll(constant => constant.Name == name);
            }

            /// <summary>
            /// Gets the value associated with the specified name.
            /// </summary>
            /// <param name="name">
            /// The name of the value to get.
            /// </param>
            /// <param name="value">
            /// When this method returns, contains the value associated with the specified
            /// name, if the name is found; otherwise, the default value for the type of the
            /// value parameter. This parameter is passed uninitialized.
            /// </param>
            /// <returns>
            /// True if this.container contains a constant with the specified name;
            /// otherwise, false.
            /// </returns>
            public bool TryGetValue(string name, out Constant value)
            {
                value = null;
                if (name == null)
                {
                    return false;
                }

                return this._constants.TryGetValue(name, out value);
            }
            #endregion Methods
        } // NumericalConstants.ConstantCollection {Class}
        #endregion Classes
    } // RSG.Metadata.Model.Converters.NumericalConstants {Class}
} // RSG.Metadata.Model.Converters {Namespace}
