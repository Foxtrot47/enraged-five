﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common
{
    /// <summary>
    /// 
    /// </summary>
    public interface IStatStreamingCoordinator : IDisposable
    {
        /// <summary>
        /// Current number of streaming requests in the queue
        /// </summary>
        int QueuedStreamingRequests { get; }

        /// <summary>
        /// Clear out any queued requests
        /// </summary>
        void ClearQueue();
    } // IStatStreamingCoordinator
} // RSG.Model.Common
