﻿// --------------------------------------------------------------------------------------------
// <copyright file="BitsetType.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions.Members
{
    /// <summary>
    /// Specifies the different types of bitset members the parCodeGen system supports.
    /// </summary>
    public enum BitsetType
    {
        /// <summary>
        /// The bitset type is unknown.
        /// </summary>
        Unknown,

        /// <summary>
        /// The bitset type is not recognised as valid, though it is defined as something.
        /// </summary>
        Unrecognised,

        /// <summary>
        /// The bitset member has a type with a default fixed length of 8 bits.
        /// </summary>
        Fixed8,

        /// <summary>
        /// The bitset member has a type with a default fixed length of 16 bits.
        /// </summary>
        Fixed16,

        /// <summary>
        /// The bitset member has a type with a default fixed length of 32 bits.
        /// </summary>
        Fixed32,

        /// <summary>
        /// The bitset member has a type with a default fixed length of 32 bits.
        /// </summary>
        Fixed,

        /// <summary>
        /// The bitset member has a type with no fixed length.
        /// </summary>
        AtBitSet,

        /// <summary>
        /// The bitset member has a type with a fixed length equal to the number of values in
        /// the associated enumeration.
        /// </summary>
        Generated,
    } // RSG.Metadata.Model.Definitions.Members.BitsetType {Enum}
} // RSG.Metadata.Model.Definitions.Members {Namespace}
