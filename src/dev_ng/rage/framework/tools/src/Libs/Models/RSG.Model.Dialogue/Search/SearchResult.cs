﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Dialogue.Search
{
    /// <summary>
    /// A search result object that is created when the ISearchable functions finds a matching property,
    /// so far this only has a reference to the model that was found to have a matching property in it.
    /// </summary>
    public class SearchResult
    {
        #region Propertie(s)

        public Object ModelObject { get; set; }

        #endregion // Propertie(s)

        #region Constructor(s)

        /// <summary>
        /// Default constructor, just sets the properties to their default value
        /// </summary>
        public SearchResult()
        {
            ModelObject = null;
        }

        /// <summary>
        /// Constructor that takes a model that is used to set the object reference 
        /// with the matching property in it.
        /// </summary>
        /// <param name="model"></param>
        public SearchResult(Object model)
        {
            ModelObject = model;
        }

        #endregion // Constructor(s)

    } // SearchResult
} // RSG.Model.Dialogue.Search
