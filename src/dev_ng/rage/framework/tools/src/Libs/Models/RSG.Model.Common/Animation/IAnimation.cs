﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common.Animation
{
    /// <summary>
    /// IAnimation interface
    /// </summary>
    public interface IAnimation : IAsset, IComparable<IAnimation>, IEquatable<IAnimation>
    {
    } // interface IAnimation
} // namespace RSG.Model.Common.Animation
