﻿using System;
using System.IO;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Collections;
using System.Xml.XPath;
using System.Windows.Media.Imaging;
using System.Xml;
using RSG.SceneXml.Material;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace RSG.Model.Map
{
    public class UserTXD : INotifyPropertyChanged
    {
        #region Events

        /// <summary>
        /// Property changed event fired when the name changes so that the 
        /// binding can be dynamic.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Create the OnPropertyChanged method to raise the event
        /// </summary>
        /// <param name="name"></param>
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        #endregion

        #region Constants
        #region XPath Compiled Expressions
        private static readonly XPathExpression sC_s_xpath_XPathTxdAttrs = XPathExpression.Compile("@*");
            private static readonly XPathExpression sC_s_xpath_XPathUserChildren = XPathExpression.Compile("UserChildren/UserDictionary");
            private static readonly XPathExpression sC_s_xpath_XPathMapChildren = XPathExpression.Compile("MapChildren/MapDictionary"); 
            private static readonly XPathExpression sC_s_xpath_XPathTextures = XPathExpression.Compile("Textures/Texture");
        #endregion // XPath Compiled Expressions
        #region UserTXD Attribute Names
            private static readonly String sC_s_s_TxdName = "Name";
        #endregion // UserTXD Attribute Names
        #endregion

        #region Properties & Member Data

        public String Name
        {
            get { return m_sName; }
            set { m_sName = value; OnPropertyChanged("Name"); }
        }
        private String m_sName;

        public UserTXD ParentUserTXD
        {
            get { return m_parentUserTXD; }
            set { m_parentUserTXD = value; }
        }
        public Boolean IsRoot
        {
            get { return m_parentUserTXD == null; }
        }
        public Boolean IsFirstLevel
        {
            get { return m_parentUserTXD != null ? m_parentUserTXD.ParentUserTXD == null : false; }
        }
        public Boolean IsSecondLevel
        {
            get { return m_parentUserTXD != null ? m_parentUserTXD.ParentUserTXD != null : false; }
        }
        private UserTXD m_parentUserTXD;

        public TXDHierarchy.TXDHierarchy ParentHierarchy
        {
            get { return m_parentHierarchy; }
            set { m_parentHierarchy = value; }
        }
        private TXDHierarchy.TXDHierarchy m_parentHierarchy;

        public int UserTXDChildrenCount
        {
            get { return m_ChildrenUserTXDs != null ? m_ChildrenUserTXDs.Count : 0; }
        }
        public Boolean CanExceptMapTXD
        {
            get { return UserTXDChildrenCount == 0; }
        }
        public ObservableDictionary<String, UserTXD> ChildrenUserTXD
        {
            get { return m_ChildrenUserTXDs; }
        }
        private ObservableDictionary<String, UserTXD> m_ChildrenUserTXDs;   // <name, Object>

        public int MapTXDChildrenCount
        {
            get { return m_ChildrenMapTXDs != null ? m_ChildrenMapTXDs.Count : 0; }
        }
        public Boolean CanExceptUserTXD
        {
            get { return MapTXDChildrenCount == 0; }
        }
        public ObservableDictionary<String, MapTXD> ChildrenMapTXD
        {
            get { return m_ChildrenMapTXDs; }
        }
        private ObservableDictionary<String, MapTXD> m_ChildrenMapTXDs;  // <name, Object>

        public int TextureCount
        {
            get { return m_ChildrenTextures != null ? m_ChildrenTextures.Count : 0; }
        }
        public ObservableDictionary<String, Texture> ChildrenTextures
        {
            get { return m_ChildrenTextures; }
        }
        private ObservableDictionary<String, Texture> m_ChildrenTextures;   // <streamName, Object>

        public RSG.Base.Collections.ObservableCollection<Object> BindingObjects
        {
            get { return m_BindingObjects; }
        }
        private RSG.Base.Collections.ObservableCollection<Object> m_BindingObjects;

        public static int ExportIndex = 0;

        #endregion

        #region Constructor(s)

        /// <summary>
        /// Create a empty user TXD dynamically on the users request.
        /// </summary>
        public UserTXD(String name, UserTXD parent, TXDHierarchy.TXDHierarchy parentHierarchy)
        {
            m_sName = name;
            m_parentUserTXD = parent;
            m_parentHierarchy = parentHierarchy;

            m_ChildrenUserTXDs = new ObservableDictionary<String, UserTXD>();
            m_ChildrenMapTXDs = new ObservableDictionary<String, MapTXD>();
            m_ChildrenTextures = new ObservableDictionary<String, Texture>();
            m_BindingObjects = new RSG.Base.Collections.ObservableCollection<Object>();
        }

        /// <summary>
        /// Create a user TXD from parsing the xml file starting at the given navigator
        /// </summary>
        public UserTXD(XPathNavigator navigator, UserTXD parent, TXDHierarchy.TXDHierarchy parentHierarchy)
        {
            m_parentUserTXD = parent;
            m_parentHierarchy = parentHierarchy;

            m_ChildrenUserTXDs = new ObservableDictionary<String, UserTXD>();
            m_ChildrenMapTXDs = new ObservableDictionary<String, MapTXD>();
            m_ChildrenTextures = new ObservableDictionary<String, Texture>();
            m_BindingObjects = new RSG.Base.Collections.ObservableCollection<Object>();

            Deserialise(navigator);
        }

        #endregion

        #region Public Function(s)

        /// <summary>
        /// Renames this dictionary
        /// </summary>
        /// <param name="newName"></param>
        /// <returns></returns>
        public bool RenameTXD(String newName)
        {
            if (newName == Name) return false;

            // Got to change the key in the parents children
            if (ParentUserTXD != null)
            {
                if (ParentUserTXD.ChildrenUserTXD.ContainsKey(Name))
                {
                    ParentUserTXD.ChildrenUserTXD.Remove(Name);
                    ParentUserTXD.ChildrenUserTXD.Add(newName, this);
                }
            }

            this.Name = newName;
            return true;
        }

        public bool CreateAndAddNewUserTXDAsChild(String name)
        {
            // Make usre that this object can except children of the type UserTXD
            if (!this.CanExceptUserTXD) return false;

            // Make sure the m_ChildrenUserTXDs object has been initialised
            if (m_ChildrenUserTXDs == null) m_ChildrenUserTXDs = new ObservableDictionary<String, UserTXD>();
            // Check whether this TXD is already a child of this dictionary
            if (m_ChildrenUserTXDs.ContainsKey(name)) return false;

            UserTXD newUserTXD = new UserTXD(name, this, m_parentHierarchy);
            m_ChildrenUserTXDs.Add(name, newUserTXD);

            //make sure it is added to the bottom of the dictionaries and not the textures.
            int index = 0;
            foreach (Object obj in m_BindingObjects)
            {
                if (obj is Texture)
                {
                    index = m_BindingObjects.IndexOf(obj);
                    break;
                }
                if (obj is UserTXD)
                {
                    index = m_BindingObjects.IndexOf(obj) + 1;
                }
            }
            // Make sure the m_BindingObjects object has been initialised
            if (m_BindingObjects == null) m_BindingObjects = new RSG.Base.Collections.ObservableCollection<Object>();
            m_BindingObjects.Insert(index, newUserTXD);
            return true;
        }

        public bool AddNewUserTXDChild(UserTXD userTXD)
        {
            // Make usre that this object can except children of the type UserTXD
            if (!this.CanExceptUserTXD) return false;

            // Make sure the m_ChildrenUserTXDs object has been initialised
            if (m_ChildrenUserTXDs == null) m_ChildrenUserTXDs = new ObservableDictionary<String, UserTXD>();
            // Check whether this TXD is already a child of this dictionary
            if (m_ChildrenUserTXDs.ContainsKey(userTXD.Name)) return false;

            m_ChildrenUserTXDs.Add(userTXD.Name, userTXD);

            //make sure it is added to the bottom of the dictionaries and not the textures.
            int index = 0;
            foreach (Object obj in m_BindingObjects)
            {
                if (obj is Texture)
                {
                    index = m_BindingObjects.IndexOf(obj);
                    break;
                }
                if (obj is UserTXD)
                {
                    index = m_BindingObjects.IndexOf(obj) + 1;
                }
            }
            // Make sure the m_BindingObjects object has been initialised
            if (m_BindingObjects == null) m_BindingObjects = new RSG.Base.Collections.ObservableCollection<Object>();
            m_BindingObjects.Insert(index, userTXD);
            return true;
        }

        public bool MoveUserTXDIntoChildren(UserTXD userTXD)
        {
            // Make usre that this object can except children of the type UserTXD
            if (!this.CanExceptUserTXD) return false;

            // Make sure the m_ChildrenUserTXDs object has been initialised
            if (m_ChildrenUserTXDs == null) m_ChildrenUserTXDs = new ObservableDictionary<String, UserTXD>();
            // Check whether this TXD is already a child of this dictionary
            if (m_ChildrenUserTXDs.ContainsKey(userTXD.Name)) return false;

            UserTXD originalParent = userTXD.ParentUserTXD;

            userTXD.ParentUserTXD = this;
            userTXD.ParentHierarchy = m_parentHierarchy;
            m_ChildrenUserTXDs.Add(userTXD.Name, userTXD);

            //make sure it is added to the bottom of the dictionaries and not the textures.
            int index = 0;
            foreach (Object obj in m_BindingObjects)
            {
                if (obj is Texture)
                {
                    index = m_BindingObjects.IndexOf(obj);
                    break;
                }
                if (obj is UserTXD)
                {
                    index = m_BindingObjects.IndexOf(obj) + 1;
                }
            }
            // Make sure the m_BindingObjects object has been initialised
            if (m_BindingObjects == null) m_BindingObjects = new RSG.Base.Collections.ObservableCollection<Object>();
            m_BindingObjects.Insert(index, userTXD);

            // Now that it is added to this parent remove it from the original parent
            originalParent.RemoveUserTXDFromChildren(userTXD);

            return true;
        }

        public bool RemoveUserTXDFromChildren(UserTXD userTXD)
        {
            // Make sure the m_ChildrenUserTXDs object is initialised
            if (m_ChildrenUserTXDs == null) return false;
            // Check whether this TXD is a child of this dictionary
            if (!m_ChildrenUserTXDs.ContainsKey(userTXD.Name)) return false;

            m_ChildrenUserTXDs.Remove(userTXD.Name);
            m_BindingObjects.Remove(userTXD);
            return true;
        }

        public bool AddNewMapTXDChild(MapTXD mapTXD)
        {
            // Make usre that this object can except children of the type UserTXD
            if (!this.CanExceptMapTXD) return false;

            // Make sure the m_ChildrenUserTXDs object has been initialised
            if (m_ChildrenMapTXDs == null) m_ChildrenUserTXDs = new ObservableDictionary<String, UserTXD>();
            // Check whether this TXD is already a child of this dictionary
            if (m_ChildrenMapTXDs.ContainsKey(mapTXD.Name)) return false;

            mapTXD.ParentUserTXD = this;
            mapTXD.ParentHierarchy = m_parentHierarchy;
            m_ChildrenMapTXDs.Add(mapTXD.Name, mapTXD);
            m_BindingObjects.Add(mapTXD);
            OnAddMapTXD(mapTXD);
            return true;
        }

        public bool CopyTextureIntoChildren(Texture texture)
        {
            // Make sure the m_ChildrenUserTXDs object is initialised
            //if (m_ChildrenTextures == null) return false;
            //// Check whether this TXD is a child of this dictionary already
            //if (m_ChildrenTextures.ContainsKey(texture.StreamName)) return false;

            //// See if this texture is already contained inside the automatically promoted textures.
            //if (!m_AutoPromotedTextures.ContainsKey(texture.StreamName)) return false;

            //Texture newTexture = m_AutoPromotedTextures[texture.StreamName];
            //newTexture.UserParent = this;
            //newTexture.MapParent = null;
            //m_ChildrenTextures.Add(newTexture.StreamName, newTexture);
            //m_BindingObjects.Add(newTexture.StreamName, newTexture);
            return true;
        }

        public bool RecurseContainsDictionaryName(String dictionaryName)
        {
            if (this.Name == dictionaryName)
            {
                return true;
            }
            foreach (MapTXD child in m_ChildrenMapTXDs.Values)
            {
                if (child.Name == dictionaryName)
                {
                    return true;
                }
            }

            foreach (UserTXD child in m_ChildrenUserTXDs.Values)
            {
                if (child.Name == dictionaryName)
                {
                    return true;
                }
                if (child.RecurseContainsDictionaryName(dictionaryName))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Goes through all the children and returns the first texture it comes across with the given streamName,
        /// this goes through all of the child user dictionaries as well as the map dictionaries
        /// </summary>
        /// <param name="streamName"></param>
        /// <returns></returns>
        public Texture RecurseGetTexture(Texture texture)
        {
            if (m_ChildrenTextures.ContainsKey(texture.StreamName)) return m_ChildrenTextures[texture.StreamName];

            foreach (MapTXD mapTXD in m_ChildrenMapTXDs.Values)
            {
                if (mapTXD.ContainsTexture(texture.StreamName)) return mapTXD.Textures[texture.StreamName];
            }

            foreach (UserTXD child in m_ChildrenUserTXDs.Values)
            {
                return child.RecurseGetTexture(texture);
            }

            return null;
        }

        /// <summary>
        /// Moves a mapTXD from one UserTXD to another, this is used in the drag and drop code,
        /// this code takes care of adding the map to this dictionary and removing it from the previous one.
        /// It also calls all the validation functions needed.
        /// </summary>
        /// <param name="mapTXD"></param>
        /// <returns></returns>
        public bool MoveMapTXDIntoChildren(MapTXD mapTXD)
        {
            // Make usre that this object can except children of the type UserTXD
            if (!this.CanExceptMapTXD) return false;

            // Make sure the m_ChildrenUserTXDs object has been initialised
            if (m_ChildrenMapTXDs == null) m_ChildrenMapTXDs = new ObservableDictionary<String, MapTXD>();
            // Check whether this TXD is already a child of this dictionary
            if (m_ChildrenMapTXDs.ContainsKey(mapTXD.Name)) return false;

            // Get last user parent if one was set
            UserTXD originalParent = mapTXD.ParentUserTXD;

            mapTXD.ParentUserTXD = this;
            mapTXD.ParentHierarchy = m_parentHierarchy;

            // Reset the binding objects on the mapTXD so it can be treated like a new mapTXD from the scene tree
            mapTXD.ResetBindingObjects();

            m_ChildrenMapTXDs.Add(mapTXD.Name, mapTXD);

            //make sure it is added to the bottom of the dictionaries and not the textures.
            int index = 0;
            foreach (Object obj in m_BindingObjects)
            {
                if (obj is Texture)
                {
                    index = m_BindingObjects.IndexOf(obj);
                    break;
                }
                if (obj is MapTXD)
                {
                    index = m_BindingObjects.IndexOf(obj) + 1;
                }
            }
            m_BindingObjects.Insert(index, mapTXD);
            OnAddMapTXD(mapTXD);

            if (originalParent != null) originalParent.RemoveMapTXDFromChildren(mapTXD);

            return true;
        }

        /// <summary>
        /// Copies a mapTXD from the level data into the global treeview. This happens when the user drags a
        /// map dictionaries from one treeview to the other.
        /// </summary>
        /// <param name="mapTXD"></param>
        /// <returns></returns>
        public bool CopyMapTXDIntoChildren(MapTXD mapTXD)
        {
            // Make sure that this object can except children of the type UserTXD
            if (!this.CanExceptMapTXD) return false;

            // Make sure the m_ChildrenUserTXDs object has been initialised
            if (m_ChildrenMapTXDs == null) m_ChildrenMapTXDs = new ObservableDictionary<String, MapTXD>();
            // Check whether this TXD is already a child of this dictionary
            if (m_ChildrenMapTXDs.ContainsKey(mapTXD.Name)) return false;

            MapTXD newMapTXD = new MapTXD(mapTXD, this);

            newMapTXD.ParentUserTXD = this;
            newMapTXD.ParentHierarchy = m_parentHierarchy;
            m_ChildrenMapTXDs.Add(newMapTXD.Name, newMapTXD);

            //make sure it is added to the bottom of the dictionaries and not the textures.
            int index = 0;
            foreach (Object obj in m_BindingObjects)
            {
                if (obj is Texture)
                {
                    index = m_BindingObjects.IndexOf(obj);
                    break;
                }
                if (obj is MapTXD)
                {
                    index = m_BindingObjects.IndexOf(obj) + 1;
                }
            }
            m_BindingObjects.Insert(index, newMapTXD);

            OnAddMapTXD(newMapTXD);
            return true;
        }

        /// <summary>
        /// Removes a map section from this dictionary and performs all the validation
        /// code making sure that everything stays valid.
        /// </summary>
        /// <param name="mapTXD"></param>
        /// <returns></returns>
        public bool RemoveMapTXDFromChildren(MapTXD mapTXD)
        {
            // Make sure the m_ChildrenUserTXDs object is initialised
            if (m_ChildrenMapTXDs == null) return false;
            // Check whether this TXD is a child of this dictionary
            if (!m_ChildrenMapTXDs.ContainsKey(mapTXD.Name)) return false;

            m_ChildrenMapTXDs.Remove(mapTXD.Name);
            m_BindingObjects.Remove(mapTXD);
            OnMapTXDRemoved();

            return true;
        }

        /// <summary>
        /// Starting at this one determines if there is a mapTXD in one of the children that contains the given texture
        /// </summary>
        /// <param name="texture"></param>
        /// <returns></returns>
        public Boolean RecurseContainsMapTexture(Texture texture)
        {
            foreach (MapTXD mapChild in m_ChildrenMapTXDs.Values)
            {
                if (mapChild.ContainsTexture(texture)) return true;
            }

            foreach (UserTXD child in m_ChildrenUserTXDs.Values)
            {
                if (child.RecurseContainsMapTexture(texture)) return true;
            }

            return false;
        }

        /// <summary>
        /// Starting at this one determines if there is a mapTXD in one of the children that contains the given texture
        /// </summary>
        /// <param name="texture"></param>
        /// <returns></returns>
        public Boolean RecurseContainsMapTexture(String streamName)
        {
            foreach (MapTXD mapChild in m_ChildrenMapTXDs.Values)
            {
                if (mapChild.ContainsTexture(streamName)) return true;
            }

            foreach (UserTXD child in m_ChildrenUserTXDs.Values)
            {
                if (child.RecurseContainsMapTexture(streamName)) return true;
            }

            return false;
        }


        /// <summary>
        /// Searches through all the child map sections and returns the first instance of the
        /// given texture stream name
        /// </summary>
        /// <param name="texture"></param>
        public Texture GetTextureFromChildMaps(Texture texture)
        {
            List<MapTXD> mapList = new List<MapTXD>();
            this.GetAllMapTXDChildren(mapList);

            foreach (MapTXD mapTXD in mapList)
            {
                if (mapTXD.ContainsTexture(texture))
                {
                    return mapTXD.Textures[texture.StreamName];
                }
            }
            mapList.Clear();
            return null;
        }

        /// <summary>
        /// Searches through all the child map sections and returns the first instance of the
        /// given stream name texture
        /// </summary>
        /// <param name="texture"></param>
        public Texture GetTextureFromChildMaps(String streamName)
        {
            List<MapTXD> mapList = new List<MapTXD>();
            this.GetAllMapTXDChildren(mapList);

            foreach (MapTXD mapTXD in mapList)
            {
                if (mapTXD.ContainsTexture(streamName))
                {
                    return mapTXD.Textures[streamName];
                }
            }
            mapList.Clear();
            return null;
        }

        /// <summary>
        /// Moves a texture from either a mapTXD or another userTXD into this dictionary. It then
        /// goes through the current branch and and makes sure that this texture doesn't appear anywhere else.
        /// </summary>
        /// <param name="texture"></param>
        /// <returns></returns>
        public Boolean MoveTextureIntoChildren(Texture texture)
        {
            // Make sure the m_ChildrenUserTXDs object is initialised
            if (m_ChildrenTextures == null) return false;
            // Check whether this TXD is a child of this dictionary already
            if (m_ChildrenTextures.ContainsKey(texture.StreamName)) return false;

            // Get last user parent if one was set
            UserTXD originalParent = texture.UserParent;

            // Get the original texture from the child map dictionary
            Texture newTexture = this.GetTextureFromChildMaps(texture);

            newTexture.UserParent = this;
            newTexture.MapParent = null;
            m_ChildrenTextures.Add(newTexture.StreamName, newTexture);
            m_BindingObjects.Add(newTexture);

            ValidateBranchForTexture(texture);
            if (originalParent != null) originalParent.RecurseValidateTexture(texture);
            // You need to update the presence count of the texture since it could have been moved
            // into a higher/lower level that has different map dictionaries.
            newTexture.UpdatePresenceCount();

            return true;
        }

        /// <summary>
        /// Starting with this dictionary goes through and determines is the given dictionary
        /// is a child of it.
        /// </summary>
        /// <param name="userTXD"></param>
        /// <returns></returns>
        public Boolean RecurseContainsChildDictionary(UserTXD userTXD)
        {
            if (m_ChildrenUserTXDs.ContainsValue(userTXD)) return true;

            foreach (UserTXD child in m_ChildrenUserTXDs.Values)
            {
                if (child.RecurseContainsChildDictionary(userTXD)) return true;
            }

            return false;
        }

        /// <summary>
        /// Determines if this user map has a child map dictionary with the given name,
        /// if it does it returns itself. This is a method to find the parent of a map txd.
        /// </summary>
        /// <param name="mapTXDName"></param>
        /// <returns></returns>
        public UserTXD ResurseContainsMapTXD(String mapTXDName)
        {
            if (m_ChildrenMapTXDs.ContainsKey(mapTXDName)) return this;

            foreach (UserTXD child in m_ChildrenUserTXDs.Values)
            {
                UserTXD result = child.ResurseContainsMapTXD(mapTXDName);
                if (result != null)
                {
                    return result;
                }
            }

            return null;
        }

        /// <summary>
        /// Determines whether this dictionary contains a binding texture with the
        /// same stream name as the given texture
        /// </summary>
        /// <param name="texture"></param>
        /// <returns></returns>
        public Boolean ContainsBindingTexture(Texture texture)
        {
            foreach (Object obj in m_BindingObjects)
            {
                if (obj is Texture)
                {
                    if (((Texture)obj).StreamName == texture.StreamName) return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Removes the texture in the binding objects with the same stream name
        /// as the given texture if present
        /// </summary>
        /// <param name="texture"></param>
        /// <returns></returns>
        public Boolean RemoveBindingTextureIfPresent(Texture texture)
        {
            foreach (Object obj in m_BindingObjects)
            {
                if (obj is Texture)
                {
                    if (((Texture)obj).StreamName == texture.StreamName)
                    {
                        m_BindingObjects.Remove(obj);
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Determines whether this dictionary contains a texture with the
        /// same stream name as the given texture
        /// </summary>
        /// <param name="texture"></param>
        /// <returns></returns>
        public Boolean ContainsTexture(Texture texture)
        {
            return m_ChildrenTextures.ContainsKey(texture.StreamName);
        }

        /// <summary>
        /// Determines whether this dictionary contains a texture with the
        /// same stream name as the stream name given
        /// </summary>
        /// <param name="texture"></param>
        /// <returns></returns>
        public Boolean ContainsTexture(String StreamName)
        {
            return m_ChildrenTextures.ContainsKey(StreamName);
        }

        /// <summary>
        /// Removes the texture in the texture children with the same stream name
        /// as the given texture if present
        /// </summary>
        /// <param name="texture"></param>
        /// <returns></returns>
        public Boolean RemoveTextureIfpresent(Texture texture)
        {
            if (!this.ContainsTexture(texture)) return false;

            this.ChildrenTextures.Remove(texture.StreamName);
            return true;
        }

        /// <summary>
        /// Determines if the given userTXD is either a child or a parent of this dictionary
        /// </summary>
        /// <param name="userTXD"></param>
        /// <returns></returns>
        public Boolean IsARelative(UserTXD userTXD)
        {
            if (RecurseContainsChildDictionary(userTXD)) return true;

            UserTXD parent = this.ParentUserTXD;
            while (parent != null)
            {
                if (parent == userTXD)
                {
                    return true;
                }
                parent = parent.ParentUserTXD;
            }
            return false;
        }

        /// <summary>
        /// Starting at this one goes through the children textures, binding objects, mapTXD children and removes it if found.
        /// </summary>
        /// <param name="texture"></param>
        public void RecurseRemoveTextureFromChildren(Texture texture)
        {
            foreach (MapTXD mapChild in m_ChildrenMapTXDs.Values)
            {
                mapChild.RemoveBindingTextureIfPresent(texture.StreamName);
            }

            this.RemoveTextureIfpresent(texture);
            this.RemoveBindingTextureIfPresent(texture);

            foreach (UserTXD child in m_ChildrenUserTXDs.Values)
            {
                child.RecurseRemoveTextureFromChildren(texture);
            }
        }

        /// <summary>
        /// Makes sure that any children or parents are currently contain the given texture remove it so that there
        /// is only one of this texture per branch. This also validates the children mapTXDs as well.
        /// </summary>
        /// <param name="texture"></param>
        public void ValidateBranchForTexture(Texture texture)
        {
            foreach (UserTXD child in m_ChildrenUserTXDs.Values)
            {
                child.RecurseRemoveTextureFromChildren(texture);
            }

            foreach (MapTXD mapChild in m_ChildrenMapTXDs.Values)
            {
                mapChild.RemoveBindingTextureIfPresent(texture.StreamName);
            }

            UserTXD parent = this.ParentUserTXD;
            while (parent != null)
            {
                if (parent.ChildrenTextures.ContainsKey(texture.StreamName)) parent.ChildrenTextures.Remove(texture.StreamName);
                if (parent.BindingObjects.Contains(texture)) parent.BindingObjects.Remove(texture);

                parent = parent.ParentUserTXD;
            }
        }

        /// <summary>
        /// Starting from this goes through all the children and parents and makes sure that it can find the given texture.
        /// If it cannot find the texture it gets re-added to any mapTXDs that it came from
        /// </summary>
        /// <param name="texture"></param>
        /// <returns></returns>
        public void RecurseValidateTexture(Texture texture)
        {
            if (m_ChildrenTextures.ContainsKey(texture.StreamName)) return;
            UserTXD parent = this.ParentUserTXD;
            while (parent != null)
            {
                if (parent.ContainsTexture(texture)) return;
                parent = parent.ParentUserTXD;
            }

            foreach (MapTXD mapChild in m_ChildrenMapTXDs.Values)
            {
                // test to see if this map originally had this texture in it.
                if (mapChild.ContainsTexture(texture))
                {
                    mapChild.ReAddTexture(texture);
                }
            }

            foreach (UserTXD child in m_ChildrenUserTXDs.Values)
            {
                child.RecurseValidateTexture(texture);
            }
        }

        /// <summary>
        /// Removes a promoted texture from the texture collection and binding objects. It also goes through
        /// all of the children and makes sure that it is present in all of the leaves it originally came from.
        /// </summary>
        /// <param name="texture"></param>
        public bool RemovePromotedTexture(Texture texture)
        {
            Boolean result = false;
            // Remove the texture
            if (this.RemoveTextureIfpresent(texture)) result = true;
            if (this.RemoveBindingTextureIfPresent(texture)) result = true;

            // Go through all children and make sure that it is present in the leaf nodes.
            RecurseValidateTexture(texture);
            return result;
        }

        /// <summary>
        /// Returns all the map dictionaries that are connected with this one in the given list.
        /// </summary>
        /// <returns></returns>
        public void GetAllMapTXDChildren(List<MapTXD> list)
        {
            foreach (MapTXD map in m_ChildrenMapTXDs.Values)
            {
                list.Add(map);
            }

            foreach (UserTXD child in m_ChildrenUserTXDs.Values)
            {
                child.GetAllMapTXDChildren(list);
            }
        }

        /// <summary>
        /// Serialises this user dictionary out to the given xml document and appends the
        /// data onto the given parent node from the document
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="parentNode"></param>
        public void Serialise(XmlDocument xmlDoc, XmlElement parentNode)
        {
            XmlElement dictionaryNode = xmlDoc.CreateElement("UserDictionary");

            // set attributes
            dictionaryNode.SetAttribute("Name", this.Name);
            dictionaryNode.SetAttribute("UserChildCount", this.UserTXDChildrenCount.ToString());
            dictionaryNode.SetAttribute("MapChildCount", this.MapTXDChildrenCount.ToString());
            dictionaryNode.SetAttribute("TextureCount", this.ChildrenTextures.Count.ToString());

            // Create the user children root node and add the child in for this dictionary
            XmlElement UserRootNode = xmlDoc.CreateElement("UserChildren");
            foreach (UserTXD usertxd in this.ChildrenUserTXD.Values)
            {
                usertxd.Serialise(xmlDoc, UserRootNode);
            }
            dictionaryNode.AppendChild(UserRootNode);

            // Create the map children root node and add the child in for this dictionary
            XmlElement MapRootNode = xmlDoc.CreateElement("MapChildren");
            foreach (MapTXD maptxd in this.ChildrenMapTXD.Values)
            {
                maptxd.Serialise(xmlDoc, MapRootNode);
            }
            dictionaryNode.AppendChild(MapRootNode);

            // Create the texture root node and add child for each texture in this dictionary
            XmlElement textureRootNode = xmlDoc.CreateElement("Textures");
            foreach (Texture texture in this.ChildrenTextures.Values)
            {
                texture.Serialise(xmlDoc, textureRootNode);
            }
            dictionaryNode.AppendChild(textureRootNode);

            parentNode.AppendChild(dictionaryNode);
        }

        /// <summary>
        /// Writes out the text that is needed for the ruby script to export this dictionary,
        /// it also takes a list of missing texture such that if any are found they can be printed
        /// out at the end.
        /// </summary>
        /// <param name="textWriter"></param>
        /// <param name="missingTextures"></param>
        public void Export(TextWriter textWriter, List<String> missingTextures)
        {
            // Write out the hash for this userTXD
            int texturesFound = 0;
            String TextureArray = "TextureArray" + (Char)(65 + ExportIndex);

            foreach (Texture texture in m_ChildrenTextures.Values)
            {
                // find the texture inside the texture stream
                // Two possible locations K:\texturesGTA5 & K:\texturesGTA5lod
                Boolean fileFound = false;
                String completePath = String.Empty;
                String location = System.IO.Path.Combine("K:\\", "texturesGTA5", texture.StreamName);
                if (System.IO.File.Exists(location + ".dds"))
                {
                    fileFound = true;
                    completePath = location + ".dds";
                }
                else
                {
                    location = System.IO.Path.Combine("K:\\", "texturesGTA5lod", texture.StreamName);
                    if (System.IO.File.Exists(location + ".dds"))
                    {
                        fileFound = true;
                        completePath = location + ".dds";
                    }
                }

                if (fileFound)
                {
                    texturesFound++;
                    if (texturesFound == 1)
                    {
                        textWriter.Write("\n\t\t" + TextureArray + " = [");
                        ExportIndex++;
                    }
                    else
                    {
                        textWriter.Write(", ");
                    }

                    textWriter.Write("\n\t\t\t'" + completePath + "' ");
                }
                else
                {
                    // add the texture into the missing list so that it can be printed out after.
                    missingTextures.Add(texture.Name);
                }
            }
            if (texturesFound > 0)
            {
                textWriter.Write("]");
                textWriter.WriteLine("\n\t\tTextureDictionaries[ '" + this.Name + "' ] = " + TextureArray);
            }

            // Write out the hash for each of this UserTXDs children
            foreach (UserTXD child in m_ChildrenUserTXDs.Values)
            {
                child.Export(textWriter, missingTextures);
            }
        }

        /// <summary>
        /// Finds the first occurance to the given obj name
        /// </summary>
        /// <param name="objName"></param>
        /// <param name="parentsToObject">Lists the parents it has to go through to get to that object</param>
        /// <returns>The object it found</returns>
        public Object FindFirst(String objName, List<Object> parentsToObject, Boolean matchWhole, Boolean caseSensitive)
        {
            Object found = null;
            if (matchWhole == true)
            {
                if (String.Compare(this.Name, objName, !caseSensitive) == 0)
                {
                    found = this;
                }
            }
            else
            {
                if (this.Name.StartsWith(objName, !caseSensitive, null))
                {
                    found = this;
                }
            }
            if (found != null)
            {
                UserTXD parent = this.ParentUserTXD;
                while (parent != null)
                {
                    parentsToObject.Add(parent);
                    parent = parent.ParentUserTXD;
                }
                parentsToObject.Reverse();
                return found;
            }


            found = DoesObjectBelongToBindingObjects(objName, matchWhole, caseSensitive);
            if (found != null)
            {
                parentsToObject.Add(this);
                UserTXD parent = this.ParentUserTXD;
                while (parent != null)
                {
                    parentsToObject.Add(parent);
                    parent = parent.ParentUserTXD;
                }
                parentsToObject.Reverse();
                return found;
            }

            foreach (UserTXD userChild in m_ChildrenUserTXDs.Values)
            {
                Object result = userChild.FindFirst(objName, parentsToObject, matchWhole, caseSensitive);
                if (result != null)
                {
                    return result;
                }
            }

            foreach (MapTXD mapTXD in m_ChildrenMapTXDs.Values)
            {
                Object result = mapTXD.FindFirst(objName, parentsToObject, matchWhole, caseSensitive);
                if (result != null)
                {
                    return result;
                }
            }

            return null;
        }

        /// <summary>
        /// Finds the last occurance to the given obj name
        /// </summary>
        /// <param name="objName"></param>
        /// <param name="parentsToObject">Lists the parents it has to go through to get to that object</param>
        /// <returns>The object it found</returns>
        public Object FindLast(String objName, List<Object> parentsToObject, Boolean matchWhole, Boolean caseSensitive)
        {
            foreach (UserTXD userChild in m_ChildrenUserTXDs.Values.Reverse<UserTXD>())
            {
                Object result = userChild.FindLast(objName, parentsToObject, matchWhole, caseSensitive);
                if (result != null)
                {
                    return result;
                }
            }

            foreach (MapTXD mapTXD in m_ChildrenMapTXDs.Values.Reverse<MapTXD>())
            {
                Object result = mapTXD.FindLast(objName, parentsToObject, matchWhole, caseSensitive);
                if (result != null)
                {
                    return result;
                }
            }

            Object found = null;
            if (matchWhole == true)
            {
                if (String.Compare(this.Name, objName, !caseSensitive) == 0)
                {
                    found = this;
                }
            }
            else
            {
                if (this.Name.StartsWith(objName, !caseSensitive, null))
                {
                    found = this;
                }
            }
            if (found != null)
            {
                UserTXD parent = this.ParentUserTXD;
                while (parent != null)
                {
                    parentsToObject.Add(parent);
                    parent = parent.ParentUserTXD;
                }
                parentsToObject.Reverse();
                return found;
            }

            return null;
        }

        public void FindAll(String objName, Collection<List<Object>> parentsToObjects, Collection<Object> results, Boolean matchWhole, Boolean caseSensitive)
        {
            // Detemine if this map is valid for the search
            Object found = null;
            if (matchWhole == true)
            {
                if (String.Compare(this.Name, objName, !caseSensitive) == 0)
                {
                    found = this;
                }
            }
            else
            {
                if (this.Name.StartsWith(objName, !caseSensitive, null))
                {
                    found = this;
                }
            }
            if (found != null)
            {
                List<Object> newParentList = new List<Object>();
                UserTXD parent = this.ParentUserTXD;
                while (parent != null)
                {
                    newParentList.Add(parent);
                    parent = parent.ParentUserTXD;
                }
                newParentList.Reverse();
                parentsToObjects.Add(newParentList);
                results.Add(this);
            }

            // Go through all the textures and determine if any of them are valid for the search
            foreach (Texture childTexture in ChildrenTextures.Values)
            {
                Texture foundTexture = null;
                if (matchWhole == true)
                {
                    if (String.Compare(childTexture.Name, objName, !caseSensitive) == 0)
                    {
                        foundTexture = childTexture;
                    }
                }
                else
                {
                    if (childTexture.Name.StartsWith(objName, !caseSensitive, null))
                    {
                        foundTexture = childTexture;
                    }
                }
                if (foundTexture != null)
                {
                    List<Object> newParentList = new List<Object>();
                    newParentList.Add(this);
                    UserTXD parent = this.ParentUserTXD;
                    while (parent != null)
                    {
                        newParentList.Add(parent);
                        parent = parent.ParentUserTXD;
                    }
                    newParentList.Reverse();
                    parentsToObjects.Add(newParentList);
                    results.Add(foundTexture);
                }
            }

            // Go through all of the map children and determine if there are valid for the search or any of their textures are.
            foreach (MapTXD childMap in ChildrenMapTXD.Values)
            {
                childMap.FindAll(objName, parentsToObjects, results, matchWhole, caseSensitive);
            }

            // Go through all of the user children and do the same thing as above
            foreach (UserTXD childUser in ChildrenUserTXD.Values)
            {
                childUser.FindAll(objName, parentsToObjects, results, matchWhole, caseSensitive);
            }
        }

        public void ResurseCollectParents(Dictionary<String, String> parents)
        {
            if (parents != null && this.ParentUserTXD != null) parents.Add(this.Name, this.ParentUserTXD.Name);
            foreach (UserTXD child in this.ChildrenUserTXD.Values)
            {
                child.ResurseCollectParents(parents);
            }
        }

        /// <summary>
        /// Determines whether this dictionary or any of the child dictionaries are empty (i.e don;t contain any textures)
        /// </summary>
        /// <returns></returns>
        public Boolean ContainsEmptyDictionaries()
        {
            if (this.ChildrenTextures.Count == 0) return true;

            foreach (UserTXD child in this.ChildrenUserTXD.Values)
            {
                if (child.ContainsEmptyDictionaries()) return true;
            }

            return false;
        }

        #endregion

        #region Private Function(s)

        /// <summary>
        /// This gets called whenever a map dictionary gets added to this user dictionary. It
        /// makes sure that previous promoted textures are not shown and that the presence count is
        /// updated for all child map dictionaries including the new one.
        /// </summary>
        /// <param name="newMapTXD"></param>
        private void OnAddMapTXD(MapTXD newMapTXD)
        {
            // First thing to do is remove the textures that have already been promoted by any other map dictionary
            // Also update the presence count for any promoted texture
            UserTXD parent = this;
            while (parent != null)
            {
                foreach (Texture promotedTexture in parent.ChildrenTextures.Values)
                {
                    newMapTXD.RemoveBindingTextureIfPresent(promotedTexture.StreamName);
                    promotedTexture.UpdatePresenceCount();
                }
                parent = parent.ParentUserTXD;
            }

            // Next update the presence count for each of the map dictionaries.
            foreach (MapTXD mapChild in ChildrenMapTXD.Values) mapChild.UpdatePresenceCount();
        }

        /// <summary>
        /// This gets called whenever a map dictionary gets removed from this user dictionary, either by deleting 
        /// or dragging. It goes up the tree starting at this one and determines if any of the promoted textures 
        /// are are invlaid and removes them if so.
        /// </summary>
        private void OnMapTXDRemoved()
        {
            // make sure that all the parents to the removed mapTXD including this one still has valid promoted textures
            UserTXD parent = this;
            while (parent != null)
            {
                parent.ValidatePromotedTextures();
                parent = parent.ParentUserTXD;
            }

            // The next thing to do is recaluate the presence count for the remaining map dictionaries.
            foreach (MapTXD mapChild in ChildrenMapTXD.Values) mapChild.UpdatePresenceCount();
        }

        /// <summary>
        /// Goes through all of the promoted textures and determines whether they are still valid, i.e. whether there
        /// exists a map dictionary with this texture in it that is a child of this.
        /// </summary>
        private void ValidatePromotedTextures()
        {
            List<MapTXD> mapList = new List<MapTXD>();
            GetAllMapTXDChildren(mapList);

            foreach (Texture promotedTexture in m_ChildrenTextures.Values)
            {
                Boolean textureFound = false;

                foreach (MapTXD mapTXD in mapList)
                {
                    if (mapTXD.ContainsTexture(promotedTexture))
                    {
                        textureFound = true;
                        promotedTexture.UpdatePresenceCount();
                        break;
                    }
                }

                if (textureFound == false)
                {
                    this.RemovePromotedTexture(promotedTexture);
                }
            }

            mapList.Clear();
        }
        
        /// <summary>
        /// Deserialises the object from a xml file navigator
        /// </summary>
        /// <param name="navigator"></param>
        private void Deserialise(XPathNavigator navigator)
        {
            XPathNodeIterator dictionaryAttrIt = navigator.Select(sC_s_xpath_XPathTxdAttrs);
            while (dictionaryAttrIt.MoveNext())
            {
                if (typeof(String) != dictionaryAttrIt.Current.ValueType)
                    continue;
                String value = (dictionaryAttrIt.Current.TypedValue as String);

                if (dictionaryAttrIt.Current.Name == sC_s_s_TxdName)
                    this.m_sName = value;
            }

            // Load any map TXDs this UserTXD has
            XPathNodeIterator mapIt = navigator.Select(sC_s_xpath_XPathMapChildren);
            while (mapIt.MoveNext())
            {
                XPathNavigator mapNavigator = mapIt.Current;
                MapTXD newMapTXD = new MapTXD(mapNavigator, this);

                this.AddNewMapTXDChild(newMapTXD);
            }

            // Load any child userTXD this UserTXD has
            XPathNodeIterator userIt = navigator.Select(sC_s_xpath_XPathUserChildren);
            while (userIt.MoveNext())
            {
                XPathNavigator userNavigator = userIt.Current;
                UserTXD newUserTXD = new UserTXD(userNavigator, this, this.ParentHierarchy);

                this.AddNewUserTXDChild(newUserTXD);
            }

            // Finally load any textures that this userTXD has in it. This will automatically update any child automatic textures.
            // Plus this is left to last as the textures are loaded with the maps and this just gets a reference to it.
            XPathNodeIterator textureIt = navigator.Select(sC_s_xpath_XPathTextures);
            while (textureIt.MoveNext())
            {
                XPathNavigator textureNavigator = textureIt.Current;
                XPathNodeIterator textureAttrIt = textureNavigator.Select(sC_s_xpath_XPathTxdAttrs);

                // Textures have to come from the leaf map nodes if the texture isn't there then there is a problem.
                // Parse the string here and fetch the texture from the information we get here.
                String filename = String.Empty;
                String alphaFilename = String.Empty;
                String streamName = String.Empty;
                while (textureAttrIt.MoveNext())
                {
                    if (typeof(String) != textureAttrIt.Current.ValueType)
                        continue;
                    String value = (textureAttrIt.Current.TypedValue as String);

                    if (textureAttrIt.Current.Name == "filename")
                    {
                        filename = value;
                        streamName = System.IO.Path.GetFileNameWithoutExtension(filename);
                    }
                    if (textureAttrIt.Current.Name == "alpha")
                    {
                        alphaFilename = value;
                        if (alphaFilename != null && alphaFilename != String.Empty)
                        {
                            // we have a alpha texture and therefore the stream name is a combination of the two paths
                            streamName += System.IO.Path.GetFileNameWithoutExtension(alphaFilename);
                        }
                    }
                }

                // Now that we have this information find the texture in any child TXDs (user and map) and add it to this one
                // using the common drag_drop methods.
                if (streamName != String.Empty && streamName != null)
                {
                    Texture newTexture = this.GetTextureFromChildMaps(streamName);
                    if (newTexture != null)
                    {
                        this.MoveTextureIntoChildren(newTexture);
                    }
                }
            }
        }

        /// <summary>
        /// Determines whether the given obj name has a object in the binding list and if so returns the
        /// object it found.
        /// </summary>
        /// <param name="objName"></param>
        /// <param name="matchWhole"></param>
        /// <param name="caseSensitive"></param>
        /// <returns></returns>
        private Object DoesObjectBelongToBindingObjects(String objName, Boolean matchWhole, Boolean caseSensitive)
        {
            foreach (Object obj in m_BindingObjects)
            {
                if (obj is UserTXD)
                {
                    if (matchWhole == true)
                    {
                        if (String.Compare(((UserTXD)obj).Name, objName, !caseSensitive) == 0)
                        {
                            return obj;
                        }
                    }
                    else
                    {
                        if (((UserTXD)obj).Name.StartsWith(objName, !caseSensitive, null))
                        {
                            return obj;
                        }
                    }
                }
                else if (obj is MapTXD)
                {
                    if (matchWhole == true)
                    {
                        if (String.Compare(((MapTXD)obj).Name, objName, !caseSensitive) == 0)
                        {
                            return obj;
                        }
                    }
                    else
                    {
                        if (((MapTXD)obj).Name.StartsWith(objName, !caseSensitive, null))
                        {
                            return obj;
                        }
                    }
                }
                else if (obj is Texture)
                {
                    if (matchWhole == true)
                    {
                        if (String.Compare(((Texture)obj).Name, objName, !caseSensitive) == 0)
                        {
                            return obj;
                        }
                    }
                    else
                    {
                        if (((Texture)obj).Name.StartsWith(objName, !caseSensitive, null))
                        {
                            return obj;
                        }
                    }
                }
            }

            return null;
        }

        #endregion

    }
}
