﻿//---------------------------------------------------------------------------------------------
// <copyright file="IProjectBuilder.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.Model
{
    using System.Collections.Generic;
    using RSG.Base.Logging;

    /// <summary>
    /// When implemented represents a builder class that can build a list of project objects.
    /// </summary>
    public interface IProjectBuilder
    {
        #region Properties
        /// <summary>
        /// Gets a list containing the absolute path to all of the files that were written to
        /// during the last build process.
        /// </summary>
        IList<string> FilesWrittenToDuringBuild { get; }

        /// <summary>
        /// Gets the full path to all of the files that will be read from during the build.
        /// </summary>
        IList<string> SourceFiles { get; }

        /// <summary>
        /// Gets the full path to all of the files that will be written to on build.
        /// </summary>
        IList<string> TargetFiles { get; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Builds the projects that have been associated with this object through the
        /// <see cref="SetProjects"/> method.
        /// </summary>
        /// <param name="log">
        /// The log object that build messages, warnings, and errors get outputted to.
        /// </param>
        void Build(ILog log);

        /// <summary>
        /// Sets the projects that this builder will build on calling its execute method.
        /// </summary>
        /// <param name="projects">
        /// A collection of projects that will be built using this builder.
        /// </param>
        void SetProjects(IEnumerable<Project> projects);
        #endregion Methods
    } // RSG.Project.Model.IProjectBuilder {Interface}
} // RSG.Project.Model {Namespace}
