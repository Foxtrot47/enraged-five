﻿//---------------------------------------------------------------------------------------------
// <copyright file="DialogueSubtitleType.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.Model
{
    using System;
    using System.Diagnostics;
    using System.Xml;
    using RSG.Editor;
    using RSG.Editor.Model;

    /// <summary>
    /// Represents a single subtitle type value that can be assigned to a audio conversation.
    /// </summary>
    [DebuggerDisplay("{_name}")]
    public sealed class DialogueSubtitleType : ModelBase
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="ExportString"/> property.
        /// </summary>
        private string _exportString;

        /// <summary>
        /// The private field used for the <see cref="Name"/> property.
        /// </summary>
        private string _name;

        /// <summary>
        /// The private field used for the <see cref="Id"/> property.
        /// </summary>
        private Guid _id;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DialogueSubtitleType"/> class.
        /// </summary>
        /// <param name="id">
        /// The static id for this subtitle type that cannot be changed.
        /// </param>
        /// <param name="name">
        /// The name for this subtitle type.
        /// </param>
        /// <param name="exportString">
        /// The string that is used for this value during the export process.
        /// </param>
        public DialogueSubtitleType(Guid id, string name, string exportString)
        {
            this._id = id;
            this._name = name;
            this._exportString = exportString;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="DialogueSubtitleType"/> class.
        /// </summary>
        /// <param name="reader">
        /// Contains data used to initialise this instance.
        /// </param>
        public DialogueSubtitleType(XmlReader reader)
        {
            if (reader == null)
            {
                throw new SmartArgumentNullException(() => reader);
            }

            this.Deserialise(reader);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the string that this subtitle type is exported with. This string is in
        /// line with the naConvSubtitleType list in /audio/speech/conversationmetadata.psc.
        /// </summary>
        public string ExportString
        {
            get { return this._exportString; }
            set { this.SetProperty(ref this._exportString, value); }
        }

        /// <summary>
        /// Gets or sets the name for this subtitle type.
        /// </summary>
        public string Name
        {
            get { return this._name; }
            set { this.SetProperty(ref this._name, value); }
        }

        /// <summary>
        /// Gets the id used by this subtitle type. This cannot be changed once setup and is
        /// how the subtitle type can be renamed without any data being lost or manipulated.
        /// </summary>
        public Guid Id
        {
            get { return this._id; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a deep copy of the current instance.
        /// </summary>
        /// <returns>
        /// A deep copy of the current System.Object.
        /// </returns>
        public override object DeepClone()
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Serialises the data contained within this instance onto the specified xml writer.
        /// </summary>
        /// <param name="writer">
        /// The xml writer this instance will be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                Debug.Assert(writer != null, "Unable to serialise into a null writer.");
                return;
            }

            writer.WriteElementString("Name", this._name);
            writer.WriteElementString("Id", this._id.ToString("D"));
            writer.WriteElementString("ExportString", this._exportString);
        }

        /// <summary>
        /// Initialises this instance using the data contained within the specified xml reader.
        /// </summary>
        /// <param name="reader">
        /// The xml reader that contains the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() == XmlNodeType.Element)
            {
                if (String.Equals("Name", reader.Name))
                {
                    this._name = reader.ReadElementContentAsString();
                }
                else if (String.Equals("Id", reader.Name))
                {
                    string value = reader.ReadElementContentAsString();
                    if (!Guid.TryParseExact(value, "D", out this._id))
                    {
                        this._id = Guid.Empty;
                    }
                }
                else if (String.Equals("ExportString", reader.Name))
                {
                    this._exportString = reader.ReadElementContentAsString();
                }

                reader.Skip();
            }

            if (reader.NodeType == XmlNodeType.EndElement)
            {
                reader.ReadEndElement();
            }
        }
        #endregion Methods
    } // RSG.Text.Model.DialogueSubtitleType {Class}
} // RSG.Text.Model {Namespace}
