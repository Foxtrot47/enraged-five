﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.Model.Map.SceneOverride
{
    /// <summary>
    /// An XML based implementation of ISceneOverrideRepository
    /// </summary>
    internal class XmlSceneOverrideReadOnlyRepository : ISceneOverrideRepository
    {
        internal XmlSceneOverrideReadOnlyRepository(string dataDirectory)
        {
            if (!Directory.Exists(dataDirectory))
                Directory.CreateDirectory(dataDirectory);

            processedContentPathname_ = Path.Combine(dataDirectory, "processed_content.xml");
            processedContentExists_ = File.Exists(processedContentPathname_);
            lodOverridesPathname_ = Path.Combine(dataDirectory, "lod_overrides.xml");
            lodOverridesExists_ = File.Exists(lodOverridesPathname_);
            entityDeletesPathname_ = Path.Combine(dataDirectory, "orphan_hd_delete_overrides.xml");
            entityDeletesExists_ = File.Exists(entityDeletesPathname_);
            attributeOverridesPathname_ = Path.Combine(dataDirectory, "attribute_overrides.xml");
            attributeOverridesExists_ = File.Exists(attributeOverridesPathname_);
            usersPathname_ = Path.Combine(dataDirectory, "users.xml");
            usersExists_ = File.Exists(usersPathname_);

            processedContentRecords_ = LoadProcessedContentRecords();
            lodOverrideRecords_ = LoadLODOverrideRecords();
            entityDeleteRecords_ = LoadEntityDeleteRecords();
            attributeOverrideRecords_ = LoadAttributeOverrideRecords();
            userRecords_ = LoadUserRecords();
        }

        // Get a record (or NULL) given a key
        public ProcessedContentRecord GetProcessedContent(int processedContentId)
        {
            if (processedContentRecords_.ContainsKey(processedContentId))
                return processedContentRecords_[processedContentId];

            return null;
        }

        public LODOverrideRecord GetLODOverride(EntityKey lodOverrideKey)
        {
            if (lodOverrideRecords_.ContainsKey(lodOverrideKey))
                return lodOverrideRecords_[lodOverrideKey];

            return null;
        }

        public EntityDeleteRecord GetEntityDelete(EntityKey entityKey)
        {
            Dictionary<EntityKey, EntityDeleteRecord> records = LoadEntityDeleteRecords();
            if (records.ContainsKey(entityKey))
                return records[entityKey];

            return null;
        }

        public AttributeOverrideRecord GetAttributeOverride(EntityKey key)
        {
            if (attributeOverrideRecords_.ContainsKey(key))
                return attributeOverrideRecords_[key];

            return null;
        }

        public UserRecord GetUser(int userId)
        {
            if (userRecords_.ContainsKey(userId))
                return userRecords_[userId];

            return null;
        }

        // Create a record, yielding a key
        public int CreateProcessedContent(ProcessedContentRecord processedContent)
        {
            throw new InvalidOperationException("Read-only repository cannot create content");
        }

        public EntityKey CreateOrUpdateLODOverride(LODOverrideRecord record)
        {
            throw new InvalidOperationException("Read-only repository cannot create content");
        }

        public IEnumerable<EntityKey> CreateOrUpdateLODOverrides(IEnumerable<LODOverrideRecord> recordsToUpdate)
        {
            throw new InvalidOperationException("Read-only repository cannot create content");
        }

        public EntityKey CreateOrUpdateEntityDelete(EntityDeleteRecord record)
        {
            throw new InvalidOperationException("Read-only repository cannot create content");
        }

        public IEnumerable<EntityKey> CreateOrUpdateEntityDeletes(IEnumerable<EntityDeleteRecord> recordsToUpdate)
        {
            throw new InvalidOperationException("Read-only repository cannot create content");
        }

        public EntityKey CreateOrUpdateAttributeOverride(AttributeOverrideRecord record)
        {
            throw new InvalidOperationException("Read-only repository cannot create content");
        }

        public IEnumerable<EntityKey> CreateOrUpdateAttributeOverrides(IEnumerable<AttributeOverrideRecord> recordsToUpdate)
        {
            throw new InvalidOperationException("Read-only repository cannot create content");
        }

        public int CreateUser(UserRecord user)
        {
            throw new InvalidOperationException("Read-only repository cannot create content");
        }

        // Helper methods
        public int FindProcessedContentKey(string processedContentName)
        {
            if (processedContentRecords_.Where(kvp => kvp.Value.Name == processedContentName).Count() == 0)
                return -1;

            return processedContentRecords_.First(kvp => kvp.Value.Name == processedContentName).Value.Id;
        }

        public int FindUserKey(string userName)
        {
            if (userRecords_.Where(kvp => kvp.Value.Name == userName).Count() == 0)
                return -1;

            return userRecords_.First(kvp => kvp.Value.Name == userName).Value.Id;
        }

        public LODOverrideRecord[] GetLODOverridesForProcessedContent(int processedContentId)
        {
            var query = from recordPair in lodOverrideRecords_
                        where recordPair.Key.ContentId == processedContentId
                        select recordPair.Value;

            return query.ToArray();
        }

        public EntityDeleteRecord[] GetEntityDeletesForProcessedContent(int processedContentId)
        {
            var query = from recordPair in entityDeleteRecords_
                        where recordPair.Key.ContentId == processedContentId
                        select recordPair.Value;

            return query.ToArray();
        }

        public AttributeOverrideRecord[] GetAttributeOverridesForProcessedContent(int processedContentId)
        {
            var query = from recordPair in attributeOverrideRecords_
                        where recordPair.Key.ContentId == processedContentId
                        select recordPair.Value;

            return query.ToArray();
        }

        #region IO
        private Dictionary<int, ProcessedContentRecord> LoadProcessedContentRecords()
        {
            Dictionary<int, ProcessedContentRecord> processedContent = new Dictionary<int, ProcessedContentRecord>();
            if (!processedContentExists_)
                return (processedContent);

            try
            {
                XDocument document = XDocument.Load(processedContentPathname_);
                foreach (XElement recordElement in document.Root.Elements("ProcessedContent"))
                {
                    processedContent.Add(
                        Int32.Parse(recordElement.Attribute("Id").Value),
                        new ProcessedContentRecord(Int32.Parse(recordElement.Attribute("Id").Value), recordElement.Attribute("Name").Value));
                }
            }
            catch (System.IO.FileNotFoundException) { }

            return processedContent;
        }

        private Dictionary<EntityKey, LODOverrideRecord> LoadLODOverrideRecords()
        {
            Dictionary<EntityKey, LODOverrideRecord> overrides = new Dictionary<EntityKey, LODOverrideRecord>(new EntityKey.EntityKeyComparer());
            if (!lodOverridesExists_)
                return (overrides);

            try
            {
                XDocument document = XDocument.Load(lodOverridesPathname_);
                foreach (XElement recordElement in document.Root.Elements("LODOverride"))
                {
                    String modelName = "????";
                    if (recordElement.Attribute("ModelName") != null)
                        modelName = recordElement.Attribute("ModelName").Value;

                    overrides.Add(
                        new EntityKey(
                            Int32.Parse(recordElement.Attribute("ContentId").Value),
                            UInt32.Parse(recordElement.Attribute("Guid").Value),
                            UInt32.Parse(recordElement.Attribute("Hash").Value)),
                        new LODOverrideRecord(
                            Int32.Parse(recordElement.Attribute("ContentId").Value),
                            UInt32.Parse(recordElement.Attribute("Hash").Value),
                            UInt32.Parse(recordElement.Attribute("Guid").Value),
                            modelName,
                            Single.Parse(recordElement.Attribute("PosX").Value),
                            Single.Parse(recordElement.Attribute("PosY").Value),
                            Single.Parse(recordElement.Attribute("PosZ").Value),
                            Int32.Parse(recordElement.Attribute("UserId").Value),
                            Single.Parse(recordElement.Attribute("Distance").Value),
                            Single.Parse(recordElement.Attribute("ChildDistance").Value),
                            Int64.Parse(recordElement.Attribute("DateTimeSubmitted").Value)));
                }
            }
            catch (System.IO.FileNotFoundException) { }

            return overrides;
        }

        private Dictionary<EntityKey, EntityDeleteRecord> LoadEntityDeleteRecords()
        {
            Dictionary<EntityKey, EntityDeleteRecord> overrides = new Dictionary<EntityKey, EntityDeleteRecord>(new EntityKey.EntityKeyComparer());
            if (!entityDeletesExists_)
                return (overrides);

            try
            {
                XDocument document = XDocument.Load(entityDeletesPathname_);
                foreach (XElement recordElement in document.Root.Elements("EntityDelete"))
                {
                    String modelName = "????";
                    if (recordElement.Attribute("ModelName") != null)
                        modelName = recordElement.Attribute("ModelName").Value;

                    overrides.Add(
                        new EntityKey(
                            Int32.Parse(recordElement.Attribute("ContentId").Value),
                            UInt32.Parse(recordElement.Attribute("Guid").Value),
                            UInt32.Parse(recordElement.Attribute("Hash").Value)),
                        new EntityDeleteRecord(
                            Int32.Parse(recordElement.Attribute("ContentId").Value),
                            UInt32.Parse(recordElement.Attribute("Hash").Value),
                            UInt32.Parse(recordElement.Attribute("Guid").Value),
                            modelName,
                            Single.Parse(recordElement.Attribute("PosX").Value),
                            Single.Parse(recordElement.Attribute("PosY").Value),
                            Single.Parse(recordElement.Attribute("PosZ").Value),
                            Int32.Parse(recordElement.Attribute("UserId").Value),
                            Int64.Parse(recordElement.Attribute("DateTimeSubmitted").Value)));
                }
            }
            catch (System.IO.FileNotFoundException) { }

            return overrides;
        }

        private Dictionary<EntityKey, AttributeOverrideRecord> LoadAttributeOverrideRecords()
        {
            Dictionary<EntityKey, AttributeOverrideRecord> overrides = new Dictionary<EntityKey, AttributeOverrideRecord>(new EntityKey.EntityKeyComparer());
            if (!attributeOverridesExists_)
                return (overrides);

            try
            {
                XDocument document = XDocument.Load(attributeOverridesPathname_);
                foreach (XElement recordElement in document.Root.Elements("AttributeOverride"))
                {
                    String modelName = "????";
                    if (recordElement.Attribute("ModelName") != null)
                        modelName = recordElement.Attribute("ModelName").Value;

                    bool dontRenderInReflections = false;
                    if (recordElement.Attribute("DontRenderInReflections") != null)
                        dontRenderInReflections = Boolean.Parse(recordElement.Attribute("DontRenderInReflections").Value);
                    bool onlyRenderInReflections = false;
                    if (recordElement.Attribute("OnlyRenderInReflections") != null)
                        onlyRenderInReflections = Boolean.Parse(recordElement.Attribute("OnlyRenderInReflections").Value);
                    bool dontRenderInWaterReflections = false;
                    if (recordElement.Attribute("DontRenderInWaterReflections") != null)
                        dontRenderInWaterReflections = Boolean.Parse(recordElement.Attribute("DontRenderInWaterReflections").Value);
                    bool onlyRenderInWaterReflections = false;
                    if (recordElement.Attribute("OnlyRenderInWaterReflections") != null)
                        onlyRenderInWaterReflections = Boolean.Parse(recordElement.Attribute("OnlyRenderInWaterReflections").Value);
                    bool dontRenderInMirrorReflections = false;
                    if (recordElement.Attribute("DontRenderInMirrorReflections") != null)
                        dontRenderInMirrorReflections = Boolean.Parse(recordElement.Attribute("DontRenderInMirrorReflections").Value);
                    bool onlyRenderInMirrorReflections = false;
                    if (recordElement.Attribute("OnlyRenderInMirrorReflections") != null)
                        onlyRenderInMirrorReflections = Boolean.Parse(recordElement.Attribute("OnlyRenderInMirrorReflections").Value);

                    overrides.Add(
                        new EntityKey(
                            Int32.Parse(recordElement.Attribute("ContentId").Value),
                            UInt32.Parse(recordElement.Attribute("Guid").Value),
                            UInt32.Parse(recordElement.Attribute("Hash").Value)),
                        new AttributeOverrideRecord(
                            Int32.Parse(recordElement.Attribute("ContentId").Value),
                            UInt32.Parse(recordElement.Attribute("Hash").Value),
                            UInt32.Parse(recordElement.Attribute("Guid").Value),
                            modelName,
                            Single.Parse(recordElement.Attribute("PosX").Value),
                            Single.Parse(recordElement.Attribute("PosY").Value),
                            Single.Parse(recordElement.Attribute("PosZ").Value),
                            Int32.Parse(recordElement.Attribute("UserId").Value),
                            Boolean.Parse(recordElement.Attribute("DontCastShadows").Value),
                            Boolean.Parse(recordElement.Attribute("DontRenderInShadows").Value),
                            dontRenderInReflections,
                            onlyRenderInReflections,
                            dontRenderInWaterReflections,
                            onlyRenderInWaterReflections,
                            dontRenderInMirrorReflections,
                            onlyRenderInMirrorReflections,
                            Boolean.Parse(recordElement.Attribute("StreamingPriorityLow").Value),
                            Int32.Parse(recordElement.Attribute("Priority").Value),
                            Int64.Parse(recordElement.Attribute("DateTimeSubmitted").Value)));
                }
            }
            catch (System.IO.FileNotFoundException) { }

            return overrides;
        }

        private Dictionary<int, UserRecord> LoadUserRecords()
        {
            Dictionary<int, UserRecord> users = new Dictionary<int, UserRecord>();
            if (!usersExists_)
                return (users);

            try
            {
                XDocument document = XDocument.Load(usersPathname_);
                foreach (XElement recordElement in document.Root.Elements("User"))
                {
                    users.Add(
                        Int32.Parse(recordElement.Attribute("Id").Value),
                        new UserRecord(
                            Int32.Parse(recordElement.Attribute("Id").Value),
                            recordElement.Attribute("Name").Value));
                }
            }
            catch (System.IO.FileNotFoundException) { }

            return users;
        }
        #endregion IO

        // Paired filename to bools; ensures we don't continually check for files existing.
        // This previously relied on exceptions per-entity which is killing our performance.
        private string processedContentPathname_;
        private bool processedContentExists_;
        private string lodOverridesPathname_;
        private bool lodOverridesExists_;
        private string entityDeletesPathname_;
        private bool entityDeletesExists_;
        private string attributeOverridesPathname_;
        private bool attributeOverridesExists_;
        private string usersPathname_;
        private bool usersExists_;

        Dictionary<int, ProcessedContentRecord> processedContentRecords_;
        Dictionary<EntityKey, LODOverrideRecord> lodOverrideRecords_;
        Dictionary<EntityKey, EntityDeleteRecord> entityDeleteRecords_;
        Dictionary<EntityKey, AttributeOverrideRecord> attributeOverrideRecords_;
        Dictionary<int, UserRecord> userRecords_;
    }
}
