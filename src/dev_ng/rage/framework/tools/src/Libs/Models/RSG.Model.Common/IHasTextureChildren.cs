﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common
{
    /// <summary>
    /// Implemented when a asset has texture children
    /// </summary>
    public interface IHasTextureChildren
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        ITexture[] Textures { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="texture"></param>
        /// <param name="gv"></param>
        /// <returns></returns>
        String GetTcsFilenameForTexture(ITexture texture, RSG.Base.ConfigParser.ConfigGameView gv);
        #endregion // Methods
    } // IHasTextureChildren
} // RSG.Model.Common
