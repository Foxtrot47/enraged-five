﻿// --------------------------------------------------------------------------------------------
// <copyright file="IScalarMember.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions.Members
{
    using System.Collections.Generic;
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// When implemented represents a member that when serialised in an array gets serialised
    /// as a continuous string on one or more lines instead of individual item nodes.
    /// </summary>
    public interface IScalarMember : IMember
    {
        #region Properties
        /// <summary>
        /// Gets the number of string components separated by whitespaces the scalar value
        /// contains.
        /// </summary>
        int ScalarComponentCount { get; }

        /// <summary>
        /// Gets a string that represents the content type of this scalar member.
        /// </summary>
        string ScalarContent { get; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a <see cref="ITunable"/> object that represents a instance of this member
        /// using the specified string components to initialise it.
        /// </summary>
        /// <param name="components">
        /// The string components that provides the data used to initialise the new tunable.
        /// </param>
        /// <param name="tunableParent">
        /// The tunable parent the new tunable will belong to.
        /// </param>
        /// <returns>
        /// A parCodeGen tunable that represents a instance of this member.
        /// </returns>
        ITunable CreateTunable(List<string> components, ITunableParent tunableParent);
        #endregion Methods
    } // RSG.Metadata.Model.Definitions.Members.IScalarMember {Interface}
} // RSG.Metadata.Model.Definitions.Members {Namespace}
