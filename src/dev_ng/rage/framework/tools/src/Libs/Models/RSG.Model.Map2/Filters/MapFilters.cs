﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.ConfigParser;

namespace RSG.Model.Map.Filters
{
    /// <summary>
    /// Used to filter map sections so as to load only map sections that pass this function
    /// </summary>
    /// <param name="mapNode">The map node to test and see if it should get loaded or not</param>
    /// <returns type=Boolean>If this function returns false the map section will not get loaded else it will get loaded</returns>
    public delegate Boolean FilterMapSectionDelegate(ContentNodeMap mapNode, Object parameter);

    /// <summary>
    /// Common map section filter methods.
    /// </summary>
    public static class MapFilters
    {
        /// <summary>
        /// Returns true for all map sections so that everything gets loaded
        /// </summary>
        public static Boolean All(RSG.Base.ConfigParser.ContentNodeMap mapNode, Object parameter)
        {
            return true;
        }
        
        /// <summary>
        /// Returns false for all map sections so that nothing gets loaded
        /// </summary>
        public static Boolean None(RSG.Base.ConfigParser.ContentNodeMap mapNode, Object parameter)
        {
            return false;
        }

        /// <summary>
        /// Returns true if the map node is representing a map file with either 
        /// definitions or not (i.e has rpf/ipl files and maybe ide)
        /// </summary>
        public static Boolean NonGeneric(RSG.Base.ConfigParser.ContentNodeMap mapNode, Object parameter)
        {
            if (mapNode.ExportInstances) return true;

            return false;
        }

        /// <summary>
        /// Returns true if the map node is representing a map file 
        /// with definitions (i.e has rpf/ipl/ide files only)
        /// </summary>
        public static Boolean NonGenericWithDefinitions(RSG.Base.ConfigParser.ContentNodeMap mapNode, Object parameter)
        {
            if (mapNode.ExportInstances && mapNode.ExportDefinitions) return true;

            return false;
        }

        /// <summary>
        /// Returns true if the map node is representing a map 
        /// file without definitions (i.e has rpf/ipl files only)
        /// </summary>
        public static Boolean NonGenericWithoutDefinitions(RSG.Base.ConfigParser.ContentNodeMap mapNode, Object parameter)
        {
            if (mapNode.ExportInstances && !mapNode.ExportDefinitions) return true;

            return false;
        }

        /// <summary>
        /// Returns true if the map node is  representing a prop file
        /// </summary>
        public static Boolean Generic(RSG.Base.ConfigParser.ContentNodeMap mapNode, Object parameter)
        {
            if (!mapNode.ExportInstances) return true;

            return false;
        }
    } // MapFilters
} // RSG.Model.Common.Filters
