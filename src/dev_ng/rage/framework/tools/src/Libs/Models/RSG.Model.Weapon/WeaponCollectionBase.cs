﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using System.Collections;
using RSG.Base.Collections;

namespace RSG.Model.Weapon
{
    /// <summary>
    /// Base class for the various weapon collections
    /// </summary>
    public abstract class WeaponCollectionBase : AssetBase, IWeaponCollection
    {
        #region Properties and associated member data
        /// <summary>
        /// The collection of levels that are currently
        /// loaded in this dictionary
        /// </summary>
        public ObservableCollection<IWeapon> Weapons
        {
            get;
            protected set;
        }
        #endregion // Properties and associated member data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public WeaponCollectionBase()
            : base("Weapons")
        {
            Weapons = new ObservableCollection<IWeapon>();
        }
        #endregion // Constructor(s)

        #region IWeaponCollection Methods
        /// <summary>
        /// 
        /// </summary>
        public void LoadAllStats()
        {
            LoadStats(StreamableStatUtils.GetValuesAssignedToProperties(typeof(Weapon)));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="statsToLoad"></param>
        public abstract void LoadStats(IEnumerable<StreamableStat> statsToLoad);
        #endregion // IWeaponCollection Methods

        #region IEnumerable<IWeapon> Implementation
        /// <summary>
        /// Loops through the levels and returns each one in turn
        /// </summary>
        IEnumerator<IWeapon> IEnumerable<IWeapon>.GetEnumerator()
        {
            foreach (IWeapon weapon in this.Weapons)
            {
                yield return weapon;
            }
        }

        /// <summary>
        /// Returns the Enumerator for this object
        /// </summary>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerator)this);
        }
        #endregion // IEnumerable<IWeapon> Implementation

        #region ICollection<IWeapon> Implementation
        #region ICollection<IWeapon> Properties
        /// <summary>
        /// Returns whether the collection is read-only
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Return number of items in the set.
        /// </summary>
        public int Count
        {
            get
            {
                return Weapons.Count;
            }
        }
        #endregion // ICollection<IWeapon> Properties

        #region ICollection<IWeapon> Methods
        /// <summary>
        /// Add a weapon to the collection.
        /// </summary>
        /// <param name="item"></param>
        public void Add(IWeapon item)
        {
            Weapons.Add(item);
        }

        /// <summary>
        /// Removes all weapons from the collection.
        /// </summary>
        public void Clear()
        {
            Weapons.Clear();
        }

        /// <summary>
        /// Determine whether the collection contains a specific weapon
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Contains(IWeapon item)
        {
            return Weapons.Contains(item);
        }

        /// <summary>
        /// Copy items in the collection to an array, starting at a particular index.
        /// </summary>
        /// <param name="output"></param>
        /// <param name="index"></param>
        public void CopyTo(IWeapon[] output, int index)
        {
            Weapons.CopyTo(output, index);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        public bool Remove(IWeapon item)
        {
            return Weapons.Remove(item);
        }
        #endregion // ICollection<IWeapon> Methods
        #endregion // ICollection<IWeapon> Implementation

        #region IDisposable Implementation
        /// <summary>
        /// 
        /// </summary>
        public virtual void Dispose()
        {
        }
        #endregion // IDisposable Implemenation
    } // WeaponCollectionBase
} // RSG.Model.Weapon
