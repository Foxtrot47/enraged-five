﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;

namespace RSG.Model.Vehicle
{
    /// <summary>
    /// 
    /// </summary>
    public class VehiclePlatformStat : IVehiclePlatformStat
    {
        #region Properties
        /// <summary>
        /// Platform for this stat
        /// </summary>
        public RSG.Platform.Platform Platform
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public uint PhysicalSize
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public uint VirtualSize
        {
            get;
            set;
        }
        
        /// <summary>
        /// 
        /// </summary>
        public uint HiPhysicalSize
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public uint HiVirtualSize
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default Constructor
        /// </summary>
        public VehiclePlatformStat()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="physicalSize"></param>
        /// <param name="virtualSize"></param>
        public VehiclePlatformStat(RSG.Platform.Platform platform, uint physicalSize, uint virtualSize, uint hiPhysicalSize, uint hiVirtualSize)
        {
            Platform = platform;
            PhysicalSize = physicalSize;
            VirtualSize = virtualSize;
            HiPhysicalSize = hiPhysicalSize;
            HiVirtualSize = hiVirtualSize;
        }
        #endregion // Constructor(s)
    } // VehiclePlatformStat
}
