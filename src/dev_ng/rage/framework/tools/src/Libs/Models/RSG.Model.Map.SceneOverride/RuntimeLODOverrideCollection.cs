﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.Model.Map.SceneOverride
{
    /// <summary>
    /// A thin wrapper around a List of RuntimeLODOverride objects.  This was developed to make Max interop a little nicer.
    /// </summary>
    public class RuntimeLODOverrideCollection
    {
        public RuntimeLODOverrideCollection()
        {
            items_ = new List<RuntimeLODOverride>();
        }

        public static RuntimeLODOverrideCollection Load(string pathname)
        {
            try
            {
                Dictionary<uint, RuntimeLODOverride> guidItemMap = new Dictionary<uint, RuntimeLODOverride>();

                XDocument document = XDocument.Load(pathname);
                XElement distanceOverideElement = document.Root.Element("distanceOverride");
                if (distanceOverideElement != null)
                {
                    foreach (XElement itemElement in distanceOverideElement.Elements("Item"))
                    {
                        try
                        {
                            uint guid = uint.Parse(itemElement.Element("guid").Attribute("value").Value);
                            uint hash = uint.Parse(itemElement.Element("hash").Attribute("value").Value);
                            float posX = float.Parse(itemElement.Element("posn").Attribute("x").Value);
                            float posY = float.Parse(itemElement.Element("posn").Attribute("y").Value);
                            float posZ = float.Parse(itemElement.Element("posn").Attribute("z").Value);
                            String imapName = itemElement.Element("imapName").Value;
                            String modelName = "?????";
                            if (itemElement.Element("modelName") != null)
                                modelName = itemElement.Element("modelName").Value;

                            float distance = float.Parse(itemElement.Element("distance").Attribute("value").Value);

                            RuntimeLODOverride newItem = new RuntimeLODOverride(hash, guid, posX, posY, posZ, imapName, modelName);
                            newItem.Distance = distance;

                            guidItemMap.Add(guid, newItem);
                        }
                        catch (Exception) { }
                    }
                }

                XElement childDistanceOverideElement = document.Root.Element("childDistanceOverride");
                if (childDistanceOverideElement != null)
                {
                    foreach (XElement itemElement in childDistanceOverideElement.Elements("Item"))
                    {
                        try
                        {
                            uint guid = uint.Parse(itemElement.Element("guid").Attribute("value").Value);
                            float distance = float.Parse(itemElement.Element("distance").Attribute("value").Value);

                            if (guidItemMap.ContainsKey(guid))
                            {
                                guidItemMap[guid].ChildDistance = distance;
                            }
                            else
                            {
                                uint hash = uint.Parse(itemElement.Element("hash").Attribute("value").Value);
                                float posX = float.Parse(itemElement.Element("posn").Attribute("x").Value);
                                float posY = float.Parse(itemElement.Element("posn").Attribute("y").Value);
                                float posZ = float.Parse(itemElement.Element("posn").Attribute("z").Value);
                                String imapName = itemElement.Element("imapName").Value;
                                String modelName = "?????";
                                if (itemElement.Element("modelName") != null)
                                    modelName = itemElement.Element("modelName").Value;

                                RuntimeLODOverride newItem = new RuntimeLODOverride(hash, guid, posX, posY, posZ, imapName, modelName);
                                newItem.ChildDistance = distance;

                                guidItemMap.Add(guid, newItem);
                            }
                        }
                        catch (Exception) { }
                    }
                }

                RuntimeLODOverrideCollection collection = new RuntimeLODOverrideCollection();
                foreach (KeyValuePair<uint, RuntimeLODOverride> guidItemPair in guidItemMap)
                {
                    collection.Add(guidItemPair.Value);
                }

                return collection;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public IEnumerable<RuntimeLODOverride> Items
        {
            get
            {
                return items_;
            }
        }

        public RuntimeLODOverride GetItem(int index)
        {
            return items_[index];
        }

        public int Count 
        { 
            get 
            { 
                return items_.Count; 
            } 
        }

        internal void Add(RuntimeLODOverride item)
        {
            items_.Add(item);
        }

        private List<RuntimeLODOverride> items_;
    }
}
