﻿// --------------------------------------------------------------------------------------------
// <copyright file="IHasRange{T}.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions.Members
{
    /// <summary>
    /// When implemented represents a member that has a valid range setup with a minimum and
    /// maximum value.
    /// </summary>
    /// <typeparam name="T">
    /// The type of the members minimum and maximum values.
    /// </typeparam>
    public interface IHasRange<T> : IMember, IHasRange
    {
        #region Properties
        /// <summary>
        /// Gets or sets the maximum value that a instance of this member can be set to.
        /// </summary>
        T Maximum { get; set; }

        /// <summary>
        /// Gets or sets the minimum value that a instance of this member can be set to.
        /// </summary>
        T Minimum { get; set; }
        #endregion Properties
    } // RSG.Metadata.Model.Definitions.Members.IHasRange{T} {Interface}
} // RSG.Metadata.Model.Definitions.Members {Namespace}
