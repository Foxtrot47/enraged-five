﻿//---------------------------------------------------------------------------------------------
// <copyright file="ArrayTunable.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Tunables
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Linq;
    using System.Text;
    using System.Xml;
    using System.Xml.Linq;
    using RSG.Base.Extensions;
    using RSG.Base.Logging;
    using RSG.Editor;
    using RSG.Editor.Model;
    using RSG.Metadata.Model.Definitions;
    using RSG.Metadata.Model.Definitions.Members;
    using RSG.Metadata.Model.Resources;

    /// <summary>
    /// Represents a tunable used by the parCodeGen system that is instancing a
    /// <see cref="ArrayMember"/> object.
    /// </summary>
    public class ArrayTunable : TunableBase, IDynamicTunableParent
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Tunables"/> property.
        /// </summary>
        private ReadOnlyModelCollection<ITunable> _readOnlyTunables;

        /// <summary>
        /// The private field for the <see cref="Tunables"/> property.
        /// </summary>
        private ModelCollection<ITunable> _tunables;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ArrayTunable"/> class to be as
        /// instance of the specified member.
        /// </summary>
        /// <param name="member">
        /// The member that this tunable will be instancing.
        /// </param>
        /// <param name="parent">
        /// The tunable parent this tunable will belong to.
        /// </param>
        public ArrayTunable(ArrayMember member, ITunableParent parent)
            : base(member, parent)
        {
            this._tunables = new ModelCollection<ITunable>(this);
            this._tunables.CollectionChanged += this.OnCollectionChanged;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ArrayTunable"/> class as a copy of the
        /// specified instance.
        /// </summary>
        /// <param name="other">
        /// The instance to copy.
        /// </param>
        /// <param name="parent">
        /// The tunable parent this tunable will belong to.
        /// </param>
        public ArrayTunable(ArrayTunable other, ITunableParent parent)
            : base(other, parent)
        {
            List<ITunable> tunables = new List<ITunable>();
            foreach (ITunable tunable in other.Tunables)
            {
                tunables.Add(tunable.Clone());
            }

            this._tunables = new ModelCollection<ITunable>(this, tunables);
            this._tunables.CollectionChanged += this.OnCollectionChanged;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ArrayTunable"/> class using the
        /// specified System.Xml.XmlReader as a initialising data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <param name="member">
        /// The member that this tunable will be instancing.
        /// </param>
        /// <param name="parent">
        /// The tunable parent this tunable will belong to.
        /// </param>
        /// <param name="log">
        /// A object that is used to log any warnings or errors produced during the parsing of
        /// the specified reader.
        /// </param>
        public ArrayTunable(
            XmlReader reader, ArrayMember member, ITunableParent parent, ILog log)
            : base(reader, member, parent)
        {
            this._tunables = new ModelCollection<ITunable>(this);
            this.Deserialise(reader, log);
            this._tunables.CollectionChanged += this.OnCollectionChanged;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the array member that this tunable is instancing.
        /// </summary>
        public ArrayMember ArrayMember
        {
            get
            {
                ArrayMember member = this.Member as ArrayMember;
                if (member != null)
                {
                    return member;
                }

                return new ArrayMember(this.TunableStructure.Definition);
            }
        }

        /// <summary>
        /// Gets the element type that this arrays children are instancing.
        /// </summary>
        public IMember ElementType
        {
            get { return this.ArrayMember.ElementType; }
        }

        /// <summary>
        /// Gets the number of items currently contained inside this tunables child collection.
        /// </summary>
        public int ItemCount
        {
            get { return this._tunables.Count; }
        }

        /// <summary>
        /// Gets a collection of the tunables that are defined for this array.
        /// </summary>
        public ReadOnlyModelCollection<ITunable> Tunables
        {
            get
            {
                if (this._readOnlyTunables == null)
                {
                    this._readOnlyTunables = this._tunables.ToReadOnly();
                }

                return this._readOnlyTunables;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a XML element node object for a array tunable with the specified array
        /// content.
        /// </summary>
        /// <param name="elemName">
        /// The XML element node name to give this element.
        /// </param>
        /// <param name="content">
        /// The contents of the element created.
        /// </param>
        /// <returns>
        /// A new XML Element node object that represents a array tunable with the specified
        /// content.
        /// </returns>
        public static XElement CreateXElement(string elemName, params object[] content)
        {
            return new XElement(elemName, content);
        }

        /// <summary>
        /// Creates a XML element node object for a item in an array tunable with the specified
        /// content.
        /// </summary>
        /// <param name="type">
        /// A optionally type attribute to add to the new element node if the item is
        /// representing a pointer tunable.
        /// </param>
        /// <param name="content">
        /// The contents of the element created.
        /// </param>
        /// <returns>
        /// A new XML Element node object that represents a item in an array tunable with the
        /// specified content and pointer type.
        /// </returns>
        public static XElement CreateItemXElement(string type, params object[] content)
        {
            XElement element = new XElement("Item");
            if (type != null)
            {
                element.Add(new XAttribute("type", type));
            }

            element.Add(content);
            return element;
        }

        /// <summary>
        /// Adds a new item to this array and returns it. The item added is a default instance
        /// of the element type for this array.
        /// </summary>
        /// <returns>
        /// The item that has been added.
        /// </returns>
        public ITunable AddNewItem()
        {
            ITunable newTunable = this.ArrayMember.ElementType.CreateTunable(this);
            this._tunables.Add(newTunable);
            return newTunable;
        }

        /// <summary>
        /// Creates a new <see cref="ArrayTunable"/> that is a copy of this instance.
        /// </summary>
        /// <returns>
        /// A new <see cref="ArrayTunable"/> that is a copy of this instance.
        /// </returns>
        public new ArrayTunable Clone()
        {
            return new ArrayTunable(this, this.TunableStructure);
        }

        /// <summary>
        /// Creates a deep copy of the current instance.
        /// </summary>
        /// <returns>
        /// A deep copy of the current System.Object.
        /// </returns>
        public override object DeepClone()
        {
            return this.Clone();
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="ITunable"/> by
        /// looking at their values.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance by their values;
        /// otherwise, false.
        /// </returns>
        public override bool EqualByValue(ITunable other)
        {
            return this.EqualByValue(other as ArrayTunable);
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="ITunable"/> by
        /// looking at their values.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance by their values;
        /// otherwise, false.
        /// </returns>
        public bool EqualByValue(ArrayTunable other)
        {
            if (other == null)
            {
                return false;
            }

            if (this.Tunables.Count != other.Tunables.Count)
            {
                return false;
            }

            for (int i = 0; i < this.Tunables.Count; i++)
            {
                if (!this.Tunables[i].EqualByValue(other.Tunables[i]))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified
        /// <see cref="ArrayTunable"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public bool Equals(ArrayTunable other)
        {
            if (other == null)
            {
                return false;
            }

            return Object.ReferenceEquals(other, this);
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="ITunable"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public override bool Equals(ITunable other)
        {
            return this.Equals(other as ArrayTunable);
        }

        /// <summary>
        /// Gets the zero-based index for the specified tunable within this tunables items
        /// collection.
        /// </summary>
        /// <param name="tunable">
        /// The tunable whose index will be retrieved.
        /// </param>
        /// <returns>
        /// The index of the specified tunable within this tunables items collection if found;
        /// otherwise, -1.
        /// </returns>
        public int IndexOf(ITunable tunable)
        {
            return this.Tunables.IndexOf(tunable);
        }

        /// <summary>
        /// Adds a new item to this array and returns it. The item added is a default instance
        /// of the element type for this array.
        /// </summary>
        /// <param name="index">
        /// The zero-based index at which the item should be inserted.
        /// </param>
        /// <returns>
        /// The item that has been added.
        /// </returns>
        public ITunable InsertNewItem(int index)
        {
            ITunable newTunable = this.ArrayMember.ElementType.CreateTunable(this);
            this._tunables.Insert(index, newTunable);
            return newTunable;
        }

        /// <summary>
        /// Adds a new item to this tunable and returns it. The item added is initialised with
        /// the data in the specified xml reader.
        /// </summary>
        /// <param name="index">
        /// The zero-based index at which the item should be inserted.
        /// </param>
        /// <param name="reader">
        /// The reader that contains the data to initialise the new item.
        /// </param>
        /// <param name="log">
        /// A object that is used to log any warnings or errors produced during the parsing of
        /// the specified reader.
        /// </param>
        /// <returns>
        /// The item that has been added.
        /// </returns>
        public ITunable InsertNewItem(int index, XmlReader reader, ILog log)
        {
            ITunable newTunable =
                this.ArrayMember.ElementType.CreateTunable(reader, this, log);
            this._tunables.Insert(index, newTunable);
            return newTunable;
        }

        /// <summary>
        /// Moves the item at the specified index to a new location in the collection.
        /// </summary>
        /// <param name="oldIndex">
        /// The zero-based index specifying the location of the item to be moved.
        /// </param>
        /// <param name="newIndex">
        /// The zero-based index specifying the new location of the item.
        /// </param>
        public void MoveItem(int oldIndex, int newIndex)
        {
            this._tunables.Move(oldIndex, newIndex);
        }

        /// <summary>
        /// Removes the first occurrence of the specified object from this arrays child
        /// collection.
        /// </summary>
        /// <param name="item">
        /// The tunable to remove from this arrays child collection.
        /// </param>
        public void RemoveItem(ITunable item)
        {
            this._tunables.Remove(item);
        }

        /// <summary>
        /// Removes the child tunable at the specified index.
        /// </summary>
        /// <param name="index">
        /// The zero-based index of the tunable to remove.
        /// </param>
        public void RemoveItemAt(int index)
        {
            this._tunables.RemoveAt(index);
        }

        /// <summary>
        /// Resets this tunable to have its default value. (i.e no source data with its initial
        /// values).
        /// </summary>
        public override void ResetValueToDefaultCore()
        {
            this._tunables.Clear();
        }

        /// <summary>
        /// Writes the xml representation of this instance onto the specified
        /// System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that the xml representation of this instance should be
        /// written to.
        /// </param>
        /// <param name="serialiseDefaultTunables">
        /// A value indicating whether the tunables that have a default value should be written
        /// to the specified writer or not.
        /// </param>
        public override void Serialise(XmlWriter writer, bool serialiseDefaultTunables)
        {
            if (writer == null)
            {
                throw new SmartArgumentNullException(() => writer);
            }

            IScalarMember scalarMember = this.ArrayMember.ElementType as IScalarMember;
            if (scalarMember != null)
            {
                string content = "{0}_array".FormatInvariant(scalarMember.ScalarContent);
                writer.WriteAttributeString("content", content);

                int hierarchyLevel = 0;
                IModel parent = this;
                TunableStructure tunableStructure = parent as TunableStructure;
                string separator = writer.Settings.NewLineChars + writer.Settings.IndentChars;
                while (parent != null && tunableStructure == null)
                {
                    separator += writer.Settings.IndentChars;
                    hierarchyLevel++;
                    parent = parent.Parent;
                    tunableStructure = parent as TunableStructure;
                }

                StringBuilder builder = new StringBuilder();
                int perLine = this.ArrayMember.ValuesPerLine;
                for (int i = 0; i < this._tunables.Count;)
                {
                    builder.Append(separator);
                    List<string> scalarValues = new List<string>();
                    for (int j = 0; j < perLine && i < this._tunables.Count; i++, j++)
                    {
                        ITunable tunable = this._tunables[i];
                        scalarValues.Add(tunable.ToString().Trim());
                    }

                    builder.Append(String.Join(" ", scalarValues));
                }

                builder.Append(writer.Settings.NewLineChars);
                builder.Insert(builder.Length, writer.Settings.IndentChars, hierarchyLevel);
                writer.WriteString(builder.ToString());
            }
            else
            {
                foreach (ITunable tunable in this._tunables)
                {
                    writer.WriteStartElement("Item");
                    tunable.Serialise(writer, serialiseDefaultTunables);
                    writer.WriteEndElement();
                }
            }
        }

        /// <summary>
        /// Updates this tunables HasDefaultValue property.
        /// </summary>
        public void UpdateHasDefaultValue()
        {
            bool hasDefaultValue = true;
            foreach (ITunable tunable in this._tunables)
            {
                if (!tunable.HasDefaultValue)
                {
                    hasDefaultValue = false;
                    break;
                }
            }

            this.HasDefaultValue = hasDefaultValue;
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        /// <param name="log">
        /// A object that is used to log any warnings or errors produced during the parsing of
        /// the specified reader.
        /// </param>
        private void Deserialise(XmlReader reader, ILog log)
        {
            if (reader == null)
            {
                return;
            }

            IXmlLineInfo lineInfo = reader as IXmlLineInfo;
            string line = lineInfo.LineNumber.ToStringInvariant();
            string pos = lineInfo.LinePosition.ToStringInvariant();

            try
            {
                this.DeserialiseInternal(reader, line, pos, log);
            }
            catch (MetadataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                string msg = StringTable.ArrayTunableDeserialiseError;
                msg = msg.FormatInvariant(line, pos, ex.Message);
                throw new MetadataException(msg);
            }

            reader.Skip();
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        /// <param name="line">
        /// The reader line number to add to any thrown exceptions.
        /// </param>
        /// <param name="pos">
        /// The reader position number to add to any thrown exceptions.
        /// </param>
        /// <param name="log">
        /// A object that is used to log any warnings or errors produced during the parsing of
        /// the specified reader.
        /// </param>
        private void DeserialiseInternal(XmlReader reader, string line, string pos, ILog log)
        {
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            ArrayMember member = this.ArrayMember;
            if (member.ElementType == null)
            {
                //// We need to know the type of element this array contains.
                string msg = StringTable.MissingArrayElementError;
                throw new MetadataException(msg, line, pos);
            }

            TunableFactory factory = new TunableFactory(this);
            IScalarMember scalarMember = member.ElementType as IScalarMember;
            int maximumSize = member.HasFixedLength ? member.Size : int.MaxValue;
            bool isScalarMember = false;
            reader.ReadStartElement();
            reader.MoveToContent();
            if (scalarMember != null)
            {
                if (reader.NodeType == XmlNodeType.Text)
                {
                    isScalarMember = true;
                }
            }

            if (isScalarMember)
            {
                string value = reader.ReadContentAsString();
                if (reader.NodeType == XmlNodeType.EndElement)
                {
                    reader.ReadEndElement();
                }

                string[] parts = value.SafeSplit(new char[] { ' ', '\t', '\r', '\n' }, true);
                int componentCount = scalarMember.ScalarComponentCount;
                int remainder = parts.Length % componentCount;
                if (remainder != 0)
                {
                    //// If we don't have the right multiple of parts the last item in the
                    //// array will not get loaded.
                    string msg = StringTable.ScalarComponentModError;
                    log.Warning(msg, componentCount, parts.Length, line, pos);
                }

                for (int i = 0; i < parts.Length - remainder;)
                {
                    int startingIndex = i;
                    List<string> tunableComponents = new List<string>();
                    for (int j = 0; j < componentCount && i < parts.Length; j++, i++)
                    {
                        tunableComponents.Add(parts[i]);
                    }

                    ITunable child = null;

                    try
                    {
                        child = factory.Create(tunableComponents, scalarMember);
                        if (child == null)
                        {
                            string msg = StringTable.ArrayChildCreationError;
                            throw new MetadataException(msg, line, pos);
                        }
                    }
                    catch (MetadataException)
                    {
                        throw;
                    }
                    catch (Exception ex)
                    {
                        string msg = StringTable.ArrayScalarChildInitialisationError;
                        msg = msg.FormatInvariant(startingIndex, line, pos, ex.Message);
                        throw new MetadataException(msg);
                    }

                    this._tunables.Add(child);
                }

                foreach (string part in parts)
                {
                    ITunable child = factory.Create(member.ElementType);
                }
            }
            else
            {
                IXmlLineInfo lineInfo = reader as IXmlLineInfo;
                while (reader.MoveToElementOrComment())
                {
                    if (reader.NodeType == XmlNodeType.Comment)
                    {
                        reader.Skip();
                        continue;
                    }

                    string childLine = lineInfo.LineNumber.ToStringInvariant();
                    string childPos = lineInfo.LinePosition.ToStringInvariant();
                    if (String.Equals(reader.Name, "Item"))
                    {
                        ITunable child = null;

                        try
                        {
                            child = factory.Create(reader, member.ElementType, log);
                            if (child == null)
                            {
                                string msg = StringTable.ArrayChildCreationError;
                                throw new MetadataException(msg, childLine, childPos);
                            }
                        }
                        catch (MetadataException)
                        {
                            throw;
                        }
                        catch (Exception ex)
                        {
                            string msg = StringTable.ArrayChildCreationError;
                            msg = msg.FormatInvariant(childLine, childPos);
                            throw new MetadataException(msg, ex);
                        }

                        this._tunables.Add(child);
                    }
                    else
                    {
                        string msg = StringTable.ArrayItemError;
                        log.Warning(msg, line, pos);
                        reader.Skip();
                    }
                }

                if (reader.NodeType == XmlNodeType.EndElement)
                {
                    reader.ReadEndElement();
                }
            }

            if (this._tunables.Count > maximumSize)
            {
                string msg = StringTable.MaximumArraySizeError;
                log.Warning(msg, line, pos);
            }
        }

        /// <summary>
        /// Called whenever the tunable collection containing this elements child changes so
        /// that we can set the default state.
        /// </summary>
        /// <param name="sender">
        /// The collection that fired the change event.
        /// </param>
        /// <param name="e">
        /// The System.Collections.Specialized.NotifyCollectionChangedEventArgs containing the
        /// event data.
        /// </param>
        private void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            this.HasDefaultValue = false;
        }
        #endregion Methods
    } // RSG.Metadata.Model.Tunables.ArrayTunable {Class}
} // RSG.Metadata.Model.Tunables {Namespace}
