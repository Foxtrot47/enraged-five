﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Report.Reports.GameStatsReports.Tests;
using RSG.Model.Statistics.Captures;
using RSG.Model.Report.Reports.GameStatsReports;

namespace RSG.Model.Report.Reports.GameStatsReports.Tests
{
    /// <summary>
    /// 
    /// </summary>
    public class ThreadSmokeTest : SmokeTestBase
    {
        #region Constants
        private const string c_name = "Thread Timing";
        #endregion // Constants

        #region Private Classes
        /// <summary>
        /// 
        /// </summary>
        public class ThreadInfo : IComparable<ThreadInfo>
        {
            public ThreadResult Result { get; private set; }
            public DateTime Timestamp { get; private set; }

            public ThreadInfo(ThreadResult result, DateTime time)
            {
                Result = result;
                Timestamp = time;
            }

            #region IComparable<CpuInfo> Interface
            /// <summary>
            /// Compare this ModificationInfo to another.
            /// </summary>
            /// <param name="other"></param>
            /// <returns></returns>
            public int CompareTo(ThreadInfo other)
            {
                if (other == null)
                {
                    return 1;
                }

                return (Result.Average.CompareTo(other.Result.Average));
            }
            #endregion // IComparable<IVehicle> Interface
        }

        /// <summary>
        /// 
        /// </summary>
        public class ThreadNameKey : IEquatable<ThreadNameKey>
        {
            public string TestName { get; private set; }
            public string ThreadName { get; private set; }

            public ThreadNameKey(string testName, string threadName)
            {
                TestName = testName;
                ThreadName = threadName;
            }

            public override string ToString()
            {
                return String.Format("{0} {1}", TestName, ThreadName);
            }

            public override bool Equals(object obj)
            {
                if (obj == null)
                {
                    return base.Equals(obj);
                }

                return ((obj is ThreadNameKey) && Equals(obj as ThreadNameKey));
            }

            public override int GetHashCode()
            {
                return (TestName.GetHashCode() ^ ThreadName.GetHashCode());
            }

            public bool Equals(ThreadNameKey other)
            {
                if (other == null)
                {
                    return false;
                }

                return (TestName == other.TestName && ThreadName == other.ThreadName);
            }
        }
        #endregion // Private Methods

        #region Properties
        /// <summary>
        /// Mapping of test name -> [gpu info list] for the latest build
        /// </summary>
        public IDictionary<string, IList<ThreadInfo>> LatestBuildStats
        {
            get;
            set;
        }

        /// <summary>
        /// 9 Highest offenders in the latest stats + other group (if needed)
        /// </summary>
        public IDictionary<string, IList<ThreadInfo>> GroupedLatestBuildStats
        {
            get;
            set;
        }

        /// <summary>
        /// Fps info grouped by test name
        /// </summary>
        public IDictionary<ThreadNameKey, IList<ThreadInfo>> HistoricalStats
        {
            get;
            set;
        }

        /// <summary>
        /// Fps info grouped by test name
        /// </summary>
        public IDictionary<ThreadNameKey, IList<ThreadInfo>> GroupedHistoricalStats
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public ThreadSmokeTest()
            : base(c_name)
        {
        }
        #endregion // Constructor(s)

        #region SmokeTestBase Methods
        /// <summary>
        /// Executes the particular test
        /// </summary>
        /// <param name="testSessions">List of test sessions</param>
        public override void RunTest(IList<TestSession> testSessions)
        {
            base.RunTest(testSessions);

            // Gather the stats we are interested in
            LatestBuildStats = GatherLatestBuildStats(testSessions.FirstOrDefault());
            HistoricalStats = GatherHistoricalStats(testSessions);

            // Create the grouped stats that we'll use for the graphs
            GroupedLatestBuildStats = GroupLatestsStats();
            GroupedHistoricalStats = GroupHistoricalStats(testSessions);

            // Run the tests on the historical data
            foreach (KeyValuePair<ThreadNameKey, IList<ThreadInfo>> pair in HistoricalStats)
            {
                string testName = pair.Key.TestName;
                string threadName = pair.Key.ThreadName;

                ThreadInfo first = pair.Value.FirstOrDefault();
                ThreadInfo next = null;
                if (pair.Value.Count > 1)
                {
                    next = pair.Value[1];
                }

                if (first != null && next != null && first.Result.Average != 0.0f && next.Result.Average != 0.0f)
                {
                    float averageGrowth = next.Result.Average / first.Result.Average;
                    if (averageGrowth < 0.95f)
                    {
                        Errors.Add(String.Format("{0} {1} thread timing is slower by {2:0.##}% on average", testName, threadName, -(averageGrowth - 1.0) * 100));
                    }

                    if (averageGrowth > 1.05f)
                    {
                        Messages.Add(String.Format("{0} {1} thread timing is faster by {2:0.##}% on average", testName, threadName, (averageGrowth - 1.0) * 100));
                    }
                }
            }
        }

        /// <summary>
        /// Generates test specific graphs for this smoke test
        /// </summary>
        /// <param name="test"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns>Null if the smoke test doesn't require a graph</returns>
        public override IList<GraphImage> GenerateGraphs(DateTime start, DateTime end)
        {
            return null;
        }
        #endregion // SmokeTestBase Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        private IDictionary<string, IList<ThreadInfo>> GatherLatestBuildStats(TestSession session)
        {
            IDictionary<string, IList<ThreadInfo>> latestStats = new Dictionary<string, IList<ThreadInfo>>();

            if (session != null)
            {
                ISet<string> encounteredTests = new HashSet<string>();

                foreach (TestResults test in session.TestResults)
                {
                    if (encounteredTests.Contains(test.Name))
                    {
                        continue;
                    }
                    encounteredTests.Add(test.Name);

                    foreach (ThreadResult result in test.ThreadResults)
                    {
                        if (!latestStats.ContainsKey(test.Name))
                        {
                            latestStats.Add(test.Name, new List<ThreadInfo>());
                        }

                        latestStats[test.Name].Add(new ThreadInfo(result, session.Timestamp));
                        ((List<ThreadInfo>)latestStats[test.Name]).Sort();
                        ((List<ThreadInfo>)latestStats[test.Name]).Reverse();
                    }
                }
            }

            return latestStats;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private IDictionary<string, IList<ThreadInfo>> GroupLatestsStats()
        {
            IDictionary<string, IList<ThreadInfo>> groupedStats = new Dictionary<string, IList<ThreadInfo>>(LatestBuildStats);

            // Take the 9 worst offenders and add an "other" category for the rest
            IDictionary<string, List<ThreadInfo>> keysToUpdate = new Dictionary<string, List<ThreadInfo>>();

            foreach (KeyValuePair<string, IList<ThreadInfo>> pair in groupedStats)
            {
                if (pair.Value.Count > 10)
                {
                    // Add other category if we have more than 10 items
                    List<ThreadInfo> newList = pair.Value.Take(9).ToList();
                    IEnumerable<ThreadInfo> otherEnumerable = pair.Value.Skip(9);

                    if (otherEnumerable.Count() > 0)
                    {
                        float otherMin = otherEnumerable.Min(item => item.Result.Min);
                        float otherMax = otherEnumerable.Max(item => item.Result.Max);
                        float otherAverage = otherEnumerable.Average(item => item.Result.Average);
                        float otherStd = otherEnumerable.Average(item => item.Result.StandardDeviation);
                        newList.Add(new ThreadInfo(new ThreadResult("Other", otherMin, otherMax, otherAverage, otherStd), DateTime.Now));
                    }

                    keysToUpdate.Add(pair.Key, newList);
                }
            }

            foreach (KeyValuePair<string, List<ThreadInfo>> pair in keysToUpdate)
            {
                groupedStats[pair.Key] = pair.Value;
            }

            return groupedStats;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="testSessions"></param>
        /// <returns></returns>
        private IDictionary<ThreadNameKey, IList<ThreadInfo>> GatherHistoricalStats(IList<TestSession> testSessions)
        {
            IDictionary<ThreadNameKey, IList<ThreadInfo>> historicalStats = new Dictionary<ThreadNameKey, IList<ThreadInfo>>();

            foreach (TestSession session in testSessions)
            {
                ISet<string> encounteredTests = new HashSet<string>();

                foreach (TestResults test in session.TestResults)
                {
                    if (encounteredTests.Contains(test.Name))
                    {
                        continue;
                    }
                    encounteredTests.Add(test.Name);

                    foreach (ThreadResult result in test.ThreadResults)
                    {
                        // Ignore stats that aren't present in the latest build stats
                        if (LatestBuildStats.ContainsKey(test.Name))
                        {
                            if (LatestBuildStats[test.Name].Where(info => info.Result.Name == result.Name).Any())
                            {
                                ThreadNameKey key = new ThreadNameKey(test.Name, result.Name);
                                if (!historicalStats.ContainsKey(key))
                                {
                                    historicalStats.Add(key, new List<ThreadInfo>());
                                }
                                historicalStats[key].Add(new ThreadInfo(result, session.Timestamp));
                            }
                        }
                    }
                }
            }

            return historicalStats;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="testSessions"></param>
        /// <returns></returns>
        private IDictionary<ThreadNameKey, IList<ThreadInfo>> GroupHistoricalStats(IList<TestSession> testSessions)
        {
            IDictionary<ThreadNameKey, IList<ThreadInfo>> groupedStats = new Dictionary<ThreadNameKey, IList<ThreadInfo>>();

            foreach (TestSession session in testSessions)
            {
                foreach (TestResults test in session.TestResults)
                {
                    foreach (ThreadResult result in test.ThreadResults)
                    {
                        if (GroupedLatestBuildStats.ContainsKey(test.Name))
                        {
                            if (GroupedLatestBuildStats[test.Name].Where(info => info.Result.Name == result.Name).Any())
                            {
                                ThreadNameKey key = new ThreadNameKey(test.Name, result.Name);
                                if (!groupedStats.ContainsKey(key))
                                {
                                    groupedStats.Add(key, new List<ThreadInfo>());
                                }
                                groupedStats[key].Add(new ThreadInfo(result, session.Timestamp));
                            }
                            else
                            {
                                ThreadNameKey key = new ThreadNameKey(test.Name, "Other");
                                if (!groupedStats.ContainsKey(key))
                                {
                                    groupedStats.Add(key, new List<ThreadInfo>());
                                }

                                if (!groupedStats[key].Where(info => info.Timestamp == session.Timestamp).Any())
                                {
                                    groupedStats[key].Add(new ThreadInfo(new ThreadResult("Other", 0.0f, 0.0f, 0.0f, 0.0f), session.Timestamp));
                                }

                                ThreadInfo otherInfo = groupedStats[key].Where(info => info.Timestamp == session.Timestamp).First();
                                otherInfo.Result.Average += result.Average;
                                otherInfo.Result.Average /= 2.0f;           //This is wrong...
                                otherInfo.Result.Min = Math.Min(otherInfo.Result.Min, result.Min);
                                otherInfo.Result.Max = Math.Max(otherInfo.Result.Max, result.Max);
                            }
                        }
                        else
                        {
                            ThreadNameKey key = new ThreadNameKey(test.Name, result.Name);
                            if (!groupedStats.ContainsKey(key))
                            {
                                groupedStats.Add(key, new List<ThreadInfo>());
                            }
                            groupedStats[key].Add(new ThreadInfo(result, session.Timestamp));
                        }
                    }
                }
            }

            return groupedStats;
        }
        #endregion // Private Methods
    } // ThreadSmokeTest
}
