﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common.Map
{
    /// <summary>
    /// Enumeration of all statistics that a vehicle can have associated with it
    /// </summary>
    [DependentStat(StreamableLevelStat.MapHierarchyString)]
    public static class StreamableMapSectionStat
    {
        public const String ExportArchetypesString = "60F833CB-08CB-4D46-BBA7-F9F3565241A1";
        public const String ExportEntitiesString = "63597796-C27F-4C06-B207-C24EA0C21314";
        public const String ArchetypesString = "2DE21FD7-58AE-4BD9-A433-69EFD66AB258";
        public const String EntitiesString = "C827401F-DC24-4608-842A-831A264EB35F";
        public const String CarGensString = "BBB7C480-7982-48AB-BB3B-6AFC41256CDA";
        public const String CollisionPolygonCountString = "DABA5F41-06D6-4A43-986A-ED67A204AEBE";
        public const String CollisionPolygonCountBreakdownString = "E349D3AD-FDAE-45FA-B6EE-7AEA5F2954F8";
        public const String LastExportUserString = "892904FA-943A-4D04-A492-E791D34C4C50";
        public const String LastExportTimeString = "350E385B-C990-4C5F-B209-211878F38A96";
        public const String ContainerAttributesString = "7BECF606-D992-4A87-9A6C-DA98C243246B";
        public const String DCCSourceFilenameString = "55F506C2-6482-4641-B096-C1635F49DC65";
        public const String ExportZipFilepathString = "4D33D6B2-63A7-44A9-BCA4-89EA564F2D88";
        public const String PlatformStatsString = "C5B276CC-9FB0-4E1B-9372-9F598B8D3086";
        public const String AggregatedStatsString = "7B03D6E6-FBB4-4A02-B5B7-FE25004B3540";

        public static readonly StreamableStat ExportArchetypes = new StreamableStat(ExportArchetypesString, true);
        public static readonly StreamableStat ExportEntities = new StreamableStat(ExportEntitiesString, true);
        public static readonly StreamableStat Archetypes = new StreamableStat(ArchetypesString, true, true, true);
        public static readonly StreamableStat Entities = new StreamableStat(EntitiesString, true, true, true);
        public static readonly StreamableStat CarGens = new StreamableStat(CarGensString, true, true, true);
        public static readonly StreamableStat CollisionPolygonCount = new StreamableStat(CollisionPolygonCountString, true);
        public static readonly StreamableStat CollisionPolygonCountBreakdown = new StreamableStat(CollisionPolygonCountBreakdownString, true, true, true);
        public static readonly StreamableStat LastExportUser = new StreamableStat(LastExportUserString, true);
        public static readonly StreamableStat LastExportTime = new StreamableStat(LastExportTimeString, true);
        public static readonly StreamableStat ContainerAttributes = new StreamableStat(ContainerAttributesString, true, true, true);
        public static readonly StreamableStat DCCSourceFilename = new StreamableStat(DCCSourceFilenameString, true);
        public static readonly StreamableStat ExportZipFilepath = new StreamableStat(ExportZipFilepathString, true);
        public static readonly StreamableStat PlatformStats = new StreamableStat(PlatformStatsString, true, true, true);

        [DependentStat(StreamableMapSectionStat.EntitiesString, DataSource.SceneXml)]
        [DependentStat(StreamableMapSectionStat.ArchetypesString, DataSource.SceneXml)]
        [DependentStat(StreamableMapSectionStat.CollisionPolygonCountString, DataSource.SceneXml)]
        public static readonly StreamableStat AggregatedStats = new StreamableStat(AggregatedStatsString, true, true, true);
    } // StreamableMapSectionStat
}
