﻿//---------------------------------------------------------------------------------------------
// <copyright file="KeyLookupItem.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Tunables
{
    using System;
    using System.Collections.Generic;
    using RSG.Editor.Model;

    /// <summary>
    /// Represents a mapping between a tunable and its map key.
    /// </summary>
    public class KeyLookupItem : ModelBase
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Value"/> property.
        /// </summary>
        private string _value;

        /// <summary>
        /// The private field used for the <see cref="Tunable"/> property.
        /// </summary>
        private ITunable _tunable;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="KeyLookupItem"/> class.
        /// </summary>
        /// <param name="tunable">
        /// The tunable whose key this object is managing.
        /// </param>
        /// <param name="value">
        /// The map key that is associated with the specified tunable.
        /// </param>
        /// <param name="parent">
        /// The map tunable whose key mappings include this object.
        /// </param>
        public KeyLookupItem(ITunable tunable, string value, MapTunable parent)
            : base(parent)
        {
            this._tunable = tunable;
            this._value = value;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the map key of the associated tunable.
        /// </summary>
        public string Value
        {
            get { return this._value; }
            set { this.SetProperty(ref this._value, value); }
        }

        /// <summary>
        /// Gets the tunable whose key this object is managing.
        /// </summary>
        public ITunable Tunable
        {
            get { return this._tunable; }
            set { this.SetProperty(ref this._tunable, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Validates this entity and creates a validation result object containing the errors
        /// and warnings.
        /// </summary>
        /// <param name="recursive">
        /// A value indicating whether any child models to this one should also be validated.
        /// </param>
        /// <returns>
        /// A validation result object containing all of the errors and warnings associated
        /// with this validation pass.
        /// </returns>
        public override ValidationResult Validate(bool recursive)
        {
            ValidationResult result = new ValidationResult();
            if (String.IsNullOrWhiteSpace(this._value))
            {
                result.AddError("Value", "Map key needs to be valid, cannot be empty.");
            }

            MapTunable mapTunable = this.Parent as MapTunable;
            if (mapTunable == null)
            {
                return ValidationResult.Empty;
            }

            List<ITunable> tunables = new List<ITunable>(mapTunable.Tunables);
            foreach (ITunable tunable in tunables)
            {
                if (Object.ReferenceEquals(tunable, this._tunable))
                {
                    continue;
                }

                string tunableKey = mapTunable[tunable];
                if (String.Equals(this._value, tunableKey))
                {
                    result.AddError("Value", "Map key needs to be unique within the map.");
                }
            }

            return result;
        }
        #endregion Methods
    } // RSG.Metadata.Model.Tunables.KeyLookupItem {Class}
} // RSG.Metadata.Model.Tunables {Namespace}
