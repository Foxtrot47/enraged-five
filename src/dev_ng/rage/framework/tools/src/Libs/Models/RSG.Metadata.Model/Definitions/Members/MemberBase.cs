﻿// --------------------------------------------------------------------------------------------
// <copyright file="MemberBase.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions.Members
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Xml;
    using RSG.Base.Extensions;
    using RSG.Base.Logging;
    using RSG.Editor;
    using RSG.Editor.Model;
    using RSG.Metadata.Model.Resources;
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// Serves as the base class for all the type members supported in the parCodeGen system.
    /// </summary>
    [DebuggerDisplay("{MetadataName} - {TypeName}")]
    public abstract class MemberBase : ModelBase, IMember
    {
        #region Fields
        /// <summary>
        /// The name of the xml attribute containing the data for the
        /// <see cref="Description"/> property.
        /// </summary>
        private const string XmlDescriptionAttr = "description";

        /// <summary>
        /// The name of the xml attribute containing the data for the
        /// <see cref="HideFrom"/> property.
        /// </summary>
        private const string XmlHideFromAttr = "hideFrom";

        /// <summary>
        /// The name of the xml attribute containing the data for the
        /// <see cref="HideWidgets"/> property.
        /// </summary>
        private const string XmlHideWidgetsAttr = "hideWidgets";

        /// <summary>
        /// The name of the xml attribute containing the data for the
        /// <see cref="Name"/> property.
        /// </summary>
        private const string XmlNameAttr = "name";

        /// <summary>
        /// The name of the xml attribute containing the data for the
        /// <see cref="NoInit"/> property.
        /// </summary>
        private const string XmlNoInitAttr = "noInit";

        /// <summary>
        /// The name of the xml attribute containing the data for the
        /// <see cref="MetadataName"/> property.
        /// </summary>
        private const string XmlParNameAttr = "parName";

        /// <summary>
        /// The name of the xml attribute containing the data for the
        /// <see cref="IsStructureKey"/> property.
        /// </summary>
        private const string XmlUiKeyAttr = "ui_key";

        /// <summary>
        /// The name of the xml attribute containing the data for the
        /// <see cref="IsLocked"/> property.
        /// </summary>
        private const string XmlUiLockedAttr = "ui_locked";

        /// <summary>
        /// The name of the xml attribute containing the data for the
        /// <see cref="Priority"/> property.
        /// </summary>
        private const string XmlUiPriorityAttr = "ui_priority";

        /// <summary>
        /// The name of the xml attribute containing the data for the
        /// <see cref="WidgetChangedCallback"/> property.
        /// </summary>
        private const string XmlWidgetChangedAttr = "onWidgetChanged";

        /// <summary>
        /// The private field used for the <see cref="Description"/> property.
        /// </summary>
        private string _description;

        /// <summary>
        /// The private field used for the <see cref="HideFrom"/> property.
        /// </summary>
        private string _hideFrom;

        /// <summary>
        /// The private field used for the <see cref="HideWidgets"/> property.
        /// </summary>
        private string _hideWidgets;

        /// <summary>
        /// The private field used for the <see cref="IsLocked"/> property.
        /// </summary>
        private string _isLocked;

        /// <summary>
        /// The private field used for the <see cref="IsStructureKey"/> property.
        /// </summary>
        private string _isStructureKey;

        /// <summary>
        /// The private field used for the <see cref="IsSubMember"/> property.
        /// </summary>
        private bool _isSubMember;

        /// <summary>
        /// The private field used for the <see cref="LineNumber"/> property.
        /// </summary>
        private int _lineNumber;

        /// <summary>
        /// The private field used for the <see cref="LinePosition"/> property.
        /// </summary>
        private int _linePosition;

        /// <summary>
        /// The private field used for the <see cref="MetadataName"/> property.
        /// </summary>
        private string _metadataName;

        /// <summary>
        /// The private field used for the <see cref="Name"/> property.
        /// </summary>
        private string _name;

        /// <summary>
        /// The private field used for the <see cref="NoInit"/> property.
        /// </summary>
        private string _noInit;

        /// <summary>
        /// The private field used for the <see cref="MetadataName"/> property.
        /// </summary>
        private string _parName;

        /// <summary>
        /// The private field used for the <see cref="Priority"/> property.
        /// </summary>
        private string _priority;

        /// <summary>
        /// The structure that this member is associated with.
        /// </summary>
        private IStructure _structure;

        /// <summary>
        /// The private field used for the <see cref="WidgetChangedCallback"/> property.
        /// </summary>
        private string _widgetChanged;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MemberBase"/> class to be one of the
        /// members of the specified structure.
        /// </summary>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        protected MemberBase(IStructure structure)
        {
            if (structure == null)
            {
                throw new SmartArgumentNullException(() => structure);
            }

            this._structure = structure;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="MemberBase"/> class as a copy of the
        /// specified instance.
        /// </summary>
        /// <param name="other">
        /// The instance to copy.
        /// </param>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        protected MemberBase(MemberBase other, IStructure structure)
        {
            if (structure == null)
            {
                throw new SmartArgumentNullException(() => structure);
            }

            if (other == null)
            {
                throw new SmartArgumentNullException(() => other);
            }

            this._structure = structure;
            this._name = other._name;
            this._parName = other._parName;
            this._metadataName = other._metadataName;
            this._description = other._description;
            this._hideFrom = other._hideFrom;
            this._widgetChanged = other._widgetChanged;
            this._noInit = other._noInit;
            this._hideWidgets = other._hideWidgets;
            this._isStructureKey = other._isStructureKey;
            this._isSubMember = other._isSubMember;
            this._lineNumber = other._lineNumber;
            this._linePosition = other._linePosition;
            this._priority = other._priority;
            this._isLocked = other._isLocked;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="MemberBase"/> class.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        protected MemberBase(XmlReader reader, IStructure structure)
        {
            if (structure == null)
            {
                throw new SmartArgumentNullException(() => structure);
            }

            if (reader == null)
            {
                throw new SmartArgumentNullException(() => reader);
            }

            this._structure = structure;
            IXmlLineInfo info = reader as IXmlLineInfo;
            if (info != null)
            {
                this._lineNumber = info.LineNumber;
                this._linePosition = info.LinePosition;
            }
            else
            {
                this._lineNumber = -1;
                this._linePosition = -1;
            }

            this._name = reader.GetAttribute(XmlNameAttr);
            this._description = reader.GetAttribute(XmlDescriptionAttr);
            this._hideFrom = reader.GetAttribute(XmlHideFromAttr);
            this._widgetChanged = reader.GetAttribute(XmlWidgetChangedAttr);
            this._noInit = reader.GetAttribute(XmlNoInitAttr);
            this._hideWidgets = reader.GetAttribute(XmlHideWidgetsAttr);
            this._isStructureKey = reader.GetAttribute(XmlUiKeyAttr);
            this._parName = reader.GetAttribute(XmlParNameAttr);
            this._priority = reader.GetAttribute(XmlUiPriorityAttr);
            this._isLocked = reader.GetAttribute(XmlUiLockedAttr);

            if (this.IsSubMember || this._name == null)
            {
                this._metadataName = "Item";
            }
            else
            {
                if (!String.IsNullOrWhiteSpace(this._parName))
                {
                    this._metadataName = this._parName;
                }
                else if (this._name.Length > 2)
                {
                    if (this._name.StartsWith("m_"))
                    {
                        this._metadataName = this._name.Substring(2);
                    }
                    else
                    {
                        this._metadataName = this._name;
                    }
                }
                else
                {
                    this._metadataName = this._name;
                }
            }
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a value indicating whether this member can have instances of it defined in
        /// metadata, i.e whether or not a tunable can be created for it.
        /// </summary>
        public virtual bool CanBeInstanced
        {
            get { return true; }
        }

        /// <summary>
        /// Gets or sets the description for this enum constant.
        /// </summary>
        public string Description
        {
            get { return this._description; }
            set { this.SetProperty(ref this._description, value); }
        }

        /// <summary>
        /// Gets the dictionary this member is associated with.
        /// </summary>
        public IDefinitionDictionary Dictionary
        {
            get { return this._structure != null ? this._structure.Dictionary : null; }
        }

        /// <summary>
        /// Gets the base System.Uri for the file this instance is defined in.
        /// </summary>
        public string Filename
        {
            get { return this._structure != null ? this._structure.Filename : null; }
        }

        /// <summary>
        /// Gets or sets an array of runtime-visitors which will not be able to see this
        /// member.
        /// </summary>
        public string[] HideFrom
        {
            get
            {
                if (this._hideFrom == null)
                {
                    return new string[0];
                }

                return this._hideFrom.Split(',');
            }

            set
            {
                if (value == null)
                {
                    this.SetProperty(ref this._hideFrom, null);
                }
                else
                {
                    this.SetProperty(ref this._hideFrom, String.Join(", ", value));
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether widgets are created for this member.
        /// </summary>
        public bool HideWidgets
        {
            get { return this.Dictionary.To<bool>(this._hideWidgets, false); }
            set { this.SetProperty(ref this._hideWidgets, value.ToString()); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether all instances of this member should be
        /// locked from being edited.
        /// </summary>
        public bool IsLocked
        {
            get { return this.Dictionary.To<bool>(this._isLocked, false); }
            set { this.SetProperty(ref this._isLocked, value.ToString()); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this member is a key to its
        /// structure definition.
        /// </summary>
        public bool IsStructureKey
        {
            get { return this.Dictionary.To<bool>(this._isStructureKey, false); }
            set { this.SetProperty(ref this._isStructureKey, value.ToString()); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this member is to be used as a sub item
        /// member to another member that supports children.
        /// </summary>
        public bool IsSubMember
        {
            get { return this._isSubMember; }
            set { this.SetProperty(ref this._isSubMember, value); }
        }

        /// <summary>
        /// Gets or sets the line number this instance is defined on or 0 if none is available.
        /// </summary>
        public int LineNumber
        {
            get { return this._lineNumber; }
            set { this.SetProperty(ref this._lineNumber, value); }
        }

        /// <summary>
        /// Gets or sets the line position this instance is defined at or 0 if none
        /// is available.
        /// </summary>
        public int LinePosition
        {
            get { return this._linePosition; }
            set { this.SetProperty(ref this._linePosition, value); }
        }

        /// <summary>
        /// Gets the location of this member inside the parCodeGen definition data sat on the
        /// local disk.
        /// </summary>
        public FileLocation Location
        {
            get { return new FileLocation(this.LineNumber, this.LinePosition, this.Filename); }
        }

        /// <summary>
        /// Gets the metadata name for this member. This is the name but without the m_ at the
        /// front of it. This is also the name that this member gets serialised with into a
        /// metadata file.
        /// </summary>
        public string MetadataName
        {
            get { return this._metadataName; }
        }

        /// <summary>
        /// Gets or sets the string name to use in save files and in widgets.
        /// </summary>
        public string Name
        {
            get
            {
                return this._name;
            }

            set
            {
                string[] names = new string[] { "Name", "MetadataName" };
                this.SetProperty(ref this._name, value, names);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether  PARSER.InitObject() will modify this
        /// member or not.
        /// </summary>
        public bool NoInit
        {
            get { return this.Dictionary.To<bool>(this._noInit, false); }
            set { this.SetProperty(ref this._noInit, value.ToString()); }
        }

        /// <summary>
        /// Gets or sets the priority this member has been given inside its owning structure.
        /// </summary>
        public int Priority
        {
            get { return this.Dictionary.To<int>(this._priority, int.MaxValue); }
            set { this.SetProperty(ref this._priority, value.ToStringInvariant()); }
        }

        /// <summary>
        /// Gets or sets the structure that this member is a member of.
        /// </summary>
        public IStructure Structure
        {
            get
            {
                return this._structure;
            }

            set
            {
                string[] names = new string[]
                {
                    "Structure",
                    "Filename",
                    "Dictionary"
                };

                this.SetProperty(ref this._structure, value, names);
            }
        }

        /// <summary>
        /// Gets the type name for this member.
        /// </summary>
        public abstract string TypeName { get; }

        /// <summary>
        /// Gets or sets the call back function used when the widget is changed.
        /// </summary>
        public string WidgetChangedCallback
        {
            get { return this._widgetChanged; }
            set { this.SetProperty(ref this._widgetChanged, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new <see cref="IMember"/> that is a copy of this instance.
        /// </summary>
        /// <returns>
        /// A new <see cref="IMember"/> that is a copy of this instance.
        /// </returns>
        public new IMember Clone()
        {
            return base.Clone() as IMember;
        }

        /// <summary>
        /// Creates a <see cref="ITunable"/> object that represents a instance of this member
        /// with it's default value set.
        /// </summary>
        /// <param name="tunableParent">
        /// The tunable parent the new tunable will belong to.
        /// </param>
        /// <returns>
        /// A parCodeGen tunable that represents a instance of this member.
        /// </returns>
        public virtual ITunable CreateTunable(ITunableParent tunableParent)
        {
            return null;
        }

        /// <summary>
        /// Creates a <see cref="ITunable"/> object that represents a instance of this member
        /// using the specified Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The Xml.XmlReader that provides the data used to initialise the new tunable.
        /// </param>
        /// <param name="tunableParent">
        /// The tunable parent the new tunable will belong to.
        /// </param>
        /// <param name="log">
        /// A object that is used to log any warnings or errors produced during the parsing of
        /// the specified reader.
        /// </param>
        /// <returns>
        /// A parCodeGen tunable that represents a instance of this member.
        /// </returns>
        public virtual ITunable CreateTunable(
            XmlReader reader, ITunableParent tunableParent, ILog log)
        {
            return null;
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified object.
        /// </summary>
        /// <param name="obj">
        /// The object to compare with this instance.
        /// </param>
        /// <returns>
        /// True if the specified object is equal to this instance; otherwise, false.
        /// </returns>
        public override bool Equals(object obj)
        {
            return this.Equals(obj as IMember);
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="IMember"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public abstract bool Equals(IMember other);

        /// <summary>
        /// Serves as a hash function for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for the current instance.
        /// </returns>
        public override int GetHashCode()
        {
            string toString = this.ToString();
            if (toString == null)
            {
                return base.GetHashCode();
            }

            return toString.GetHashCode();
        }

        /// <summary>
        /// Determines whether this structure can be instanced inside a metadata file depending
        /// on any critical errors found.
        /// </summary>
        /// <param name="log">
        /// The log object that will receives any errors or warning found while validating.
        /// </param>
        /// <returns>
        /// True if this structure can be instanced inside a metadata file; otherwise false. If
        /// false the specified log object will have received the reasons why not.
        /// </returns>
        public bool IsValidForInstancing(ILog log)
        {
            return true;
        }

        /// <summary>
        /// Writes the xml representation of this instance onto the specified
        /// System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that the xml representation of this instance should be
        /// written to.
        /// </param>
        public abstract void Serialise(XmlWriter writer);

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A string that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return String.Format("{0} - {1}", this.Name, this.TypeName);
        }

        /// <summary>
        /// Writes out all the attributes to the specified System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that the attributes should be written to.
        /// </param>
        protected void SerialiseBaseAttributes(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            if (this._name != null)
            {
                writer.WriteAttributeString(XmlNameAttr, this._name);
            }

            if (this._description != null)
            {
                writer.WriteAttributeString(XmlDescriptionAttr, this._description);
            }

            if (this._hideFrom != null)
            {
                writer.WriteAttributeString(XmlHideFromAttr, this._hideFrom);
            }

            if (this._widgetChanged != null)
            {
                writer.WriteAttributeString(XmlWidgetChangedAttr, this._widgetChanged);
            }

            if (this._noInit != null)
            {
                writer.WriteAttributeString(XmlNoInitAttr, this._noInit);
            }

            if (this._hideWidgets != null)
            {
                writer.WriteAttributeString(XmlHideWidgetsAttr, this._hideWidgets);
            }

            if (this._isStructureKey != null)
            {
                writer.WriteAttributeString(XmlUiKeyAttr, this._isStructureKey);
            }

            if (this._parName != null)
            {
                writer.WriteAttributeString(XmlParNameAttr, this._parName);
            }

            if (this._priority != null)
            {
                writer.WriteAttributeString(XmlUiPriorityAttr, this._priority);
            }

            if (this._isLocked != null)
            {
                writer.WriteAttributeString(XmlUiLockedAttr, this._isLocked);
            }
        }

        /// <summary>
        /// Validates the base properties for this member and adds the errors and warnings into
        /// the specified validation results object.
        /// </summary>
        /// <param name="result">
        /// The validation results object that receives the errors and warnings for the base
        /// properties.
        /// </param>
        protected void ValidateBaseProperties(ValidationResult result)
        {
            if (this.CanBeInstanced && !this.IsSubMember)
            {
                if (this._name == null)
                {
                    string msg = StringTable.MissingMemberNameError;
                    result.AddError(msg, this.Location);
                }
                else if (String.IsNullOrWhiteSpace(this._name))
                {
                    string msg = StringTable.EmptyMemberNameError;
                    result.AddError(msg, this.Location);
                }
            }

            if (!this.Dictionary.Validate<bool>(this._hideWidgets, true))
            {
                string msg = StringTable.FormatAttributeWarning;
                msg = msg.FormatCurrent(XmlHideWidgetsAttr, this._hideWidgets, "boolean");
                result.AddWarning(msg, this.Location);
            }

            if (!this.Dictionary.Validate<bool>(this._isStructureKey, true))
            {
                string msg = StringTable.FormatAttributeWarning;
                msg = msg.FormatCurrent(XmlUiKeyAttr, this._isStructureKey, "boolean");
                result.AddWarning(msg, this.Location);
            }

            if (!this.Dictionary.Validate<bool>(this._noInit, true))
            {
                string msg = StringTable.FormatAttributeWarning;
                msg = msg.FormatCurrent(XmlNoInitAttr, this._noInit, "boolean");
                result.AddWarning(msg, this.Location);
            }
        }
        #endregion Methods
    } // RSG.Metadata.Model.Definitions.Members.MemberBase {Class}
} // RSG.Metadata.Model.Definitions.Members {Namespace}
