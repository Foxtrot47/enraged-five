﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common.Mission
{
    /// <summary>
    /// Enumeration of all possibly outcomes of a mission.
    /// </summary>
    public enum MissionAttemptResult
    {
        /// <summary>
        /// Mission was successfully completed.
        /// </summary>
        Passed,

        /// <summary>
        /// User failed to complete the mission.
        /// </summary>
        Failed,

        /// <summary>
        /// User cancelled the mission.
        /// </summary>
        Cancelled,

        /// <summary>
        /// Special situation for multiplayer missions where neither team completed the goal.
        /// e.g. First team to steal 10 cars wins, but neither team manages to accomplish that.
        /// </summary>
        Over,

        /// <summary>
        /// For when the result of a mission isn't known.
        /// </summary>
        Unknown
    } // MissionAttemptResult
}
