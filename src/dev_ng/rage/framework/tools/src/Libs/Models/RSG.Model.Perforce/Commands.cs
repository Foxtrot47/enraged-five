﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Perforce
{
    internal static class Commands
    {
        internal static String P4_CLIENT = "client";
        internal static String P4_DEPOTS = "depots";
    }

} // RSG.Model.Perforce namespace
