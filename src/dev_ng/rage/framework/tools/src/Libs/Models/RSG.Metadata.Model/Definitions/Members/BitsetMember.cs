﻿// --------------------------------------------------------------------------------------------
// <copyright file="BitsetMember.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions.Members
{
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using RSG.Base;
    using RSG.Base.Extensions;
    using RSG.Base.Logging;
    using RSG.Editor;
    using RSG.Editor.Model;
    using RSG.Metadata.Model.Resources;
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// Represents the &lt;bitset&gt; member node in the parCodeGen system that can be
    /// instanced in a metadata file.
    /// </summary>
    public class BitsetMember : MemberBase, IHasInitialValue<string>, IEquatable<BitsetMember>
    {
        #region Fields
        /// <summary>
        /// The string representation of the <see cref="BitsetType.AtBitSet"/> constant.
        /// </summary>
        private const string BitsetTypeAtBitset = "atBitSet";

        /// <summary>
        /// The string representation of the <see cref="BitsetType.Fixed"/> constant.
        /// </summary>
        private const string BitsetTypeFixed = "fixed";

        /// <summary>
        /// The string representation of the <see cref="BitsetType.Fixed16"/> constant.
        /// </summary>
        private const string BitsetTypeFixed16 = "fixed16";

        /// <summary>
        /// The string representation of the <see cref="BitsetType.Fixed32"/> constant.
        /// </summary>
        private const string BitsetTypeFixed32 = "fixed32";

        /// <summary>
        /// The string representation of the <see cref="BitsetType.Fixed8"/> constant.
        /// </summary>
        private const string BitsetTypeFixed8 = "fixed8";

        /// <summary>
        /// The string representation of the <see cref="BitsetType.Generated"/> constant.
        /// </summary>
        private const string BitsetTypeGenerated = "generated";

        /// <summary>
        /// The name of the xml attribute containing the data for the
        /// <see cref="InitialValue"/> property.
        /// </summary>
        private const string XmlInitialAttr = "init";

        /// <summary>
        /// The name of the xml attribute containing the data for the
        /// <see cref="NumBits"/> property.
        /// </summary>
        private const string XmlNumBitsAttr = "numBits";

        /// <summary>
        /// The name of the xml attribute containing the data for the
        /// <see cref="Type"/> property.
        /// </summary>
        private const string XmlTypeAttr = "type";

        /// <summary>
        /// The name of the xml attribute containing the data for the
        /// <see cref="Values"/> property.
        /// </summary>
        private const string XmlValuesAttr = "values";

        /// <summary>
        /// The private field used for the <see cref="InitialValue"/> property.
        /// </summary>
        private string _initialValue;

        /// <summary>
        /// The private field used for the <see cref="NumBits"/> property.
        /// </summary>
        private string _numBits;

        /// <summary>
        /// The private field used for the <see cref="Type"/> property.
        /// </summary>
        private string _type;

        /// <summary>
        /// The private field used for the <see cref="ValuesType"/> property.
        /// </summary>
        private string _valuesType;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="BitsetMember"/> class to be one of the
        /// members of the specified structure.
        /// </summary>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        public BitsetMember(IStructure structure)
            : base(structure)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="BitsetMember"/> class as a copy of the
        /// specified instance.
        /// </summary>
        /// <param name="other">
        /// The instance to copy.
        /// </param>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        public BitsetMember(BitsetMember other, IStructure structure)
            : base(other, structure)
        {
            this._initialValue = other._initialValue;
            this._numBits = other._numBits;
            this._valuesType = other._valuesType;
            this._type = other._type;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="BitsetMember"/> class.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <param name="structure">
        /// The structure definition that this member will belong to.
        /// </param>
        public BitsetMember(XmlReader reader, IStructure structure)
            : base(reader, structure)
        {
            this.Deserialise(reader);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the initial value that any instance of this member is set to.
        /// </summary>
        public string InitialValue
        {
            get { return this._initialValue; }
            set { this.SetProperty(ref this._initialValue, value); }
        }

        /// <summary>
        /// Gets or sets the number of bits that make up this bitset member.
        /// </summary>
        public int NumBits
        {
            get
            {
                if (this._numBits == "use_values" || this.Type == BitsetType.Generated)
                {
                    IEnumeration values = this.Values;
                    if (values != null)
                    {
                        return values.EnumConstants.Count;
                    }
                }

                TryResult<ushort> result = this.Dictionary.TryTo<ushort>(this._numBits, 0);
                if (result.Success)
                {
                    return result.Value;
                }

                switch (this.Type)
                {
                    case BitsetType.Fixed8:
                        return 8;
                    case BitsetType.Fixed16:
                        return 16;
                    case BitsetType.AtBitSet:
                        return -1;
                    case BitsetType.Fixed32:
                    case BitsetType.Fixed:
                    default:
                        return 32;
                }
            }

            set
            {
                this.SetProperty(ref this._numBits, value.ToString());
            }
        }

        /// <summary>
        /// Gets or sets the type of bitset this bitset member represents in the c++ code.
        /// </summary>
        public BitsetType Type
        {
            get { return this.GetTypeFromString(this._type); }
            set { this.SetProperty(ref this._type, this.GetStringFromType(value)); }
        }

        /// <summary>
        /// Gets the type name for this member.
        /// </summary>
        public override string TypeName
        {
            get { return "bitset"; }
        }

        /// <summary>
        /// Gets the enumeration used by this bitset as a source for the possible values.
        /// </summary>
        public IEnumeration Values
        {
            get { return this.GetEnumerationSource(this.Structure.Dictionary); }
        }

        /// <summary>
        /// Gets or sets the data type for the enumeration that is used as the source.
        /// </summary>
        public string ValuesType
        {
            get
            {
                return this._valuesType;
            }

            set
            {
                string newValue = value.ToString();
                this.SetProperty(ref this._valuesType, newValue, "ValuesType", "Source");
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new <see cref="BitsetMember"/> that is a copy of this instance.
        /// </summary>
        /// <returns>
        /// A new <see cref="BitsetMember"/> that is a copy of this instance.
        /// </returns>
        public new BitsetMember Clone()
        {
            return new BitsetMember(this, this.Structure);
        }

        /// <summary>
        /// Creates a <see cref="ITunable"/> object that represents a instance of this member
        /// with it's default value set.
        /// </summary>
        /// <param name="parent">
        /// The tunable parent the new tunable will belong to.
        /// </param>
        /// <returns>
        /// A parCodeGen tunable that represents a instance of this member.
        /// </returns>
        public override ITunable CreateTunable(ITunableParent parent)
        {
            return new BitsetTunable(this, parent);
        }

        /// <summary>
        /// Creates a <see cref="ITunable"/> object that represents a instance of this member
        /// using the specified Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The Xml.XmlReader that provides the data used to initialise the new tunable.
        /// </param>
        /// <param name="parent">
        /// The tunable parent the new tunable will belong to.
        /// </param>
        /// <param name="log">
        /// A object that is used to log any warnings or errors produced during the parsing of
        /// the specified reader.
        /// </param>
        /// <returns>
        /// A parCodeGen tunable that represents a instance of this member.
        /// </returns>
        public override ITunable CreateTunable(
            XmlReader reader, ITunableParent parent, ILog log)
        {
            return new BitsetTunable(reader, this, parent, log);
        }

        /// <summary>
        /// Creates a deep copy of the current instance.
        /// </summary>
        /// <returns>
        /// A deep copy of the current System.Object.
        /// </returns>
        public override object DeepClone()
        {
            return this.Clone();
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified
        /// <see cref="BitsetMember"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public bool Equals(BitsetMember other)
        {
            if (other == null)
            {
                return false;
            }

            return object.ReferenceEquals(other, this);
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="IMember"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public override bool Equals(IMember other)
        {
            return this.Equals(other as BitsetMember);
        }

        /// <summary>
        /// Writes the xml representation of this instance onto the specified
        /// System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that the xml representation of this instance should be
        /// written to.
        /// </param>
        public override void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                throw new SmartArgumentNullException(() => writer);
            }

            this.SerialiseBaseAttributes(writer);
            if (this._type != null)
            {
                writer.WriteAttributeString(XmlTypeAttr, this._type);
            }

            if (this._valuesType != null)
            {
                writer.WriteAttributeString(XmlValuesAttr, this._valuesType);
            }

            if (this._numBits != null)
            {
                writer.WriteAttributeString(XmlNumBitsAttr, this._numBits);
            }

            if (this._initialValue != null)
            {
                writer.WriteAttributeString(XmlInitialAttr, this._initialValue);
            }
        }

        /// <summary>
        /// Validates this entity and creates a validation result object containing the errors
        /// and warnings.
        /// </summary>
        /// <param name="recursive">
        /// A value indicating whether any child models to this one should also be validated.
        /// </param>
        /// <returns>
        /// A validation result object containing all of the errors and warnings associated
        /// with this validation pass.
        /// </returns>
        public override ValidationResult Validate(bool recursive)
        {
            ValidationResult result = new ValidationResult();
            this.ValidateBaseProperties(result);

            if (this._type == null)
            {
                string msg = StringTable.MissingRequiredAttributeError;
                msg = msg.FormatCurrent("type", "bitset");
                result.AddError(msg, this.Location);
            }
            else if (String.IsNullOrWhiteSpace(this._type))
            {
                string msg = StringTable.EmptyRequiredAttributeError;
                msg = msg.FormatCurrent("type", "bitset");
                result.AddError(msg, this.Location);
            }
            else if (this.Type == BitsetType.Unrecognised)
            {
                string msg = StringTable.UnrecognisedRequiredAttributeError;
                msg = msg.FormatCurrent("type", "bitset", this._type);
                result.AddError(msg, this.Location);
            }

            if (this._valuesType != null && this.Values == null)
            {
                string msg = StringTable.UnresolvedStructureError;
                msg = msg.FormatCurrent(this._valuesType, "bitest");
                result.AddError(msg, this.Location);
            }

            if (this._numBits != "use_values")
            {
                if (!this.Dictionary.Validate<ushort>(this._numBits, true))
                {
                    string msg = StringTable.FormatAttributeWarning;
                    msg = msg.FormatCurrent(XmlNumBitsAttr, this._numBits, "ushort");
                    result.AddWarning(msg, this.Location);
                }
            }

            return result;
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            this._initialValue = reader.GetAttribute(XmlInitialAttr);
            this._numBits = reader.GetAttribute(XmlNumBitsAttr);
            this._valuesType = reader.GetAttribute(XmlValuesAttr);
            this._type = reader.GetAttribute(XmlTypeAttr);
            reader.Skip();
        }

        /// <summary>
        /// Looks through the specified scope and returns the enumeration that this bitset
        /// member uses as a source.
        /// </summary>
        /// <param name="scope">
        /// A definition dictionary containing all the other types in the search scope.
        /// </param>
        /// <returns>
        /// This enum members enumeration source.
        /// </returns>
        private IEnumeration GetEnumerationSource(IDefinitionDictionary scope)
        {
            if (this.ValuesType == null || scope == null)
            {
                return null;
            }

            return scope.GetEnumeration(this._valuesType);
        }

        /// <summary>
        /// Determines the value of the bitset.
        /// </summary>
        /// <returns>
        /// The initial value.
        /// </returns>
        private ulong GetNumericInitialValue()
        {
            if (String.IsNullOrWhiteSpace(this._initialValue))
            {
                return 0;
            }

            TryResult<ulong> result = this.Dictionary.TryTo<ulong>(this._initialValue, 0);
            if (result.Success)
            {
                return result.Value;
            }

            if (this.Values == null)
            {
                return 0;
            }

            string[] parts = this._initialValue.Split('|');
            ulong value = 0;
            foreach (string part in parts)
            {
                int index = this.Values.GetIndexOf(part);
                if (index == -1)
                {
                    continue;
                }

                ulong bit = (ulong)(1 << index);
                value |= bit;
            }

            return value;
        }

        /// <summary>
        /// Gets the string value that is equivalent to the specified type.
        /// </summary>
        /// <param name="type">
        /// The type that the returned string will be equivalent to.
        /// </param>
        /// <returns>
        /// The string that is equivalent to the specified type.
        /// </returns>
        private string GetStringFromType(BitsetType type)
        {
            switch (type)
            {
                case BitsetType.Fixed8:
                    return BitsetTypeFixed8;
                case BitsetType.Fixed16:
                    return BitsetTypeFixed16;
                case BitsetType.Fixed32:
                    return BitsetTypeFixed32;
                case BitsetType.Fixed:
                    return BitsetTypeFixed;
                case BitsetType.AtBitSet:
                    return BitsetTypeAtBitset;
                case BitsetType.Generated:
                    return BitsetTypeGenerated;
                case BitsetType.Unknown:
                case BitsetType.Unrecognised:
                default:
                    return null;
            }
        }

        /// <summary>
        /// Determines the string representation of the numeric value within this bitset.
        /// </summary>
        /// <param name="value">
        /// The numeric value to get the string represent of.
        /// </param>
        /// <returns>
        /// The string representation of the numeric value within this bitset.
        /// </returns>
        private string GetStringInitialValue(ulong value)
        {
            if (this.Values == null)
            {
                return value.ToString();
            }

            List<string> initialValues = new List<string>();
            int index = 0;
            ulong remainder = value;
            foreach (IEnumConstant constant in this.Values.EnumConstants)
            {
                ulong constantValue = (ulong)index;
                if ((constantValue & value) == constantValue)
                {
                    initialValues.Add(constant.Name);
                    remainder -= constantValue;
                }

                index++;
            }

            if (remainder > 0)
            {
                return value.ToString();
            }

            return String.Join(" | ", initialValues);
        }

        /// <summary>
        /// Gets the bitset type that is equivalent to the specified string.
        /// </summary>
        /// <param name="type">
        /// The string to determine the type to return.
        /// </param>
        /// <returns>
        /// The type that is equivalent to the specified string.
        /// </returns>
        private BitsetType GetTypeFromString(string type)
        {
            if (type == null)
            {
                return BitsetType.Unknown;
            }
            else if (String.Equals(type, BitsetTypeFixed8))
            {
                return BitsetType.Fixed8;
            }
            else if (String.Equals(type, BitsetTypeFixed16))
            {
                return BitsetType.Fixed16;
            }
            else if (String.Equals(type, BitsetTypeFixed32))
            {
                return BitsetType.Fixed32;
            }
            else if (String.Equals(type, BitsetTypeFixed))
            {
                return BitsetType.Fixed;
            }
            else if (String.Equals(type, BitsetTypeAtBitset))
            {
                return BitsetType.AtBitSet;
            }
            else if (String.Equals(type, BitsetTypeGenerated))
            {
                return BitsetType.Generated;
            }
            else
            {
                return BitsetType.Unrecognised;
            }
        }
        #endregion Methods
    } // RSG.Metadata.Model.Definitions.Members.BitsetMember {Class}
} // RSG.Metadata.Model.Definitions.Members {Namespace}
