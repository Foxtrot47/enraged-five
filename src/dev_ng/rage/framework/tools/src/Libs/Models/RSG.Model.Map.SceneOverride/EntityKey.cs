﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Map.SceneOverride
{
    /// <summary>
    /// The three-part composite key that differentiates override entries
    /// </summary>
    internal class EntityKey
    {
        internal EntityKey(int contentId, uint guid, uint hash)
        {
            ContentId = contentId;
            Guid = guid;
            Hash = hash;
        }

        internal int ContentId { get; private set; }
        internal uint Guid { get; private set; }
        internal uint Hash { get; private set; }

        internal class EntityKeyComparer : IEqualityComparer<EntityKey>
        {
            #region IEqualityComparer<EntityKey> Members

            public bool Equals(EntityKey lhs, EntityKey rhs)
            {
                return ((lhs.ContentId == rhs.ContentId) &&
                        (lhs.Guid == rhs.Guid) &&
                        (lhs.Hash == rhs.Hash));
            }

            public int GetHashCode(EntityKey obj)
            {
                return obj.ContentId.GetHashCode() ^ obj.Guid.GetHashCode() ^ obj.Hash.GetHashCode();
            }

            #endregion
        }
    }
}
