﻿//---------------------------------------------------------------------------------------------
// <copyright file="MetadataLevelExcludedAttribute.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.Model
{
    using System;

    /// <summary>
    /// Used to give a metadata level a group name to exclude. This can be used to force a
    /// specific group away from the core file and only in the metadata level file needed.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = true)]
    internal class MetadataLevelExcludedAttribute : Attribute
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="GroupName"/> property.
        /// </summary>
        private string _groupName;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MetadataLevelExcludedAttribute"/>
        /// class with the specified group name.
        /// </summary>
        /// <param name="groupName">
        /// The group name set on this instance.
        /// </param>
        public MetadataLevelExcludedAttribute(string groupName)
        {
            this._groupName = groupName;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the group name value that this attribute has set.
        /// </summary>
        public string GroupName
        {
            get { return this._groupName; }
        }
        #endregion Properties
    } // RSG.Project.Model.MetadataLevelExcludedAttribute {Class}
} // RSG.Project.Model {Namespace}
