﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Logging;
using RSG.Base.ConfigParser;
using System.IO;
using RSG.Model.Common;
using RSG.ManagedRage;
using RSG.Platform;

namespace RSG.Model.Report.Reports
{
    /// <summary>
    /// 
    /// </summary>
    public class WeaponCSVReport : CSVReport, IDynamicMapReport
    {
        #region Constants
        private const String NAME = "Export Weapon CSV Report";
        private const String DESCRIPTION = "Exports the selected level vehicles into a .csv file"; 
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Determines whether the map data should be loaded
        /// </summary>
        public bool LoadMapData
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Determines whether the car generators should be loaded
        /// 
        /// </summary>
        public bool LoadCarGenarators
        {
            get { return false; }
        }
        #endregion // Properties

        #region Construtcor(s)

        /// <summary>
        /// Constructor.
        /// </summary>
        public WeaponCSVReport()
            : base(NAME, DESCRIPTION)
        {
        }

        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Generate the report dynamically.
        /// </summary>
        /// <param name="level"></param>
        /// <param name="gv"></param>
        public void Generate(ILevel level, ConfigGameView gv, IPlatformCollection platforms)
        {
            DateTime start = DateTime.Now;
            ExportVehicleCSVReport(level, gv, platforms);
            Log.Log__Message("Vehicle CSV export complete. (report took {0} milliseconds to create.)", (DateTime.Now - start).TotalMilliseconds);
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        /// <param name="gv"></param>
        private void ExportVehicleCSVReport(ILevel level, ConfigGameView gv, IPlatformCollection platforms)
        {
            using (StreamWriter sw = new StreamWriter(Filename))
            {
                WriteCsvHeader(sw, platforms);
                IAsset weaponContainer = null;
                foreach (IAsset asset in level.AssetChildren)
                {
                    if (0 == String.Compare("weapons", asset.Name, true))
                    {
                        weaponContainer = asset;
                        break;
                    }
                }
                if (null != weaponContainer)
                {
                    System.Diagnostics.Debug.Assert(weaponContainer is IHasAssetChildren);
                    foreach (IAsset asset in (weaponContainer as IHasAssetChildren).AssetChildren)
                    {
                        // Skip all non-Weapon assets
                        IWeapon weapon = asset as IWeapon;
                        if (weapon != null)
                        {
                            // Ensure that all the data is loaded
                            weapon.RequestAllStatistics(true, false);

                            // Generate the csv line
                            string scvRecord = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11}",
                                weapon.Name, weapon.PolyCount, weapon.CollisionCount,
                                weapon.Textures.Count(), weapon.Shaders.Count(),
                                weapon.BoneCount,
                                weapon.GripCount, weapon.MagazineCount, weapon.AttachmentCount,
                                weapon.HasLOD,
                                (weapon.HasLOD == true ? weapon.LODPolyCount.ToString() : "n/a"),
                                GenerateWeaponMemoryDetails(weapon, platforms));
                            sw.WriteLine(scvRecord);
                        }
                    }
                }
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sw"></param>
        /// <param name="platforms"></param>
        private void WriteCsvHeader(StreamWriter sw, IPlatformCollection platforms)
        {
            string header = "Name,Poly Count,Collision Count,Texture Count,Shader Count,Bone Count,Grip Count,Magazine Count,Attachment Count,Has LOD,LOD Poly Count";

            foreach (IPlatform platform in platforms)
            {
                header += String.Format(",{0} Physical Size,{0} Virtual Size,{0} Total Size", platform.Name);
            }

            sw.WriteLine(header);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="weapon"></param>
        /// <param name="platforms"></param>
        /// <returns></returns>
        private string GenerateWeaponMemoryDetails(IWeapon weapon, IPlatformCollection platforms)
        {
            StringBuilder details = new StringBuilder("");

            foreach (IPlatform platform in platforms)
            {
                IWeaponPlatformStat stat = weapon.GetPlatformStat(platform.Platform);

                if (stat != null)
                {
                    details.Append(String.Format("{0},{1},{2},", stat.PhysicalSize, stat.VirtualSize, stat.PhysicalSize + stat.VirtualSize));
                }
                else
                {
                    details.Append("n/a,n/a,n/a,");
                }
            }

            return details.ToString().TrimEnd(new char[] {','});
        }
        #endregion // Private Methods
    } // WeaponCSVReport
}
