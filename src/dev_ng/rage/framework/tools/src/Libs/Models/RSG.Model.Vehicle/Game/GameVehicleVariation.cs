﻿namespace RSG.Model.Vehicle.Game
{
    using System.Collections.Generic;
    using System.Diagnostics;

    /// <summary>
    /// Represents a single Vehicle Variation that can be referenced through a game vehicle.
    /// </summary>
    [DebuggerDisplay("Model = {ModelName}, Kit Count = {_kits.Count}")]
    public class GameVehicleVariation
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="ModelName"/> property.
        /// </summary>
        private string _modelName;

        /// <summary>
        /// The private field used for the <see cref="Kits"/> property.
        /// </summary>
        private readonly Dictionary<string, GameVehicleKit> _kits;

        /// <summary>
        /// The private field used for the <see cref="ReferenceCount"/> property.
        /// </summary>
        private int _referenceCount;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="GameVehicleVariation"/> class.
        /// </summary>
        public GameVehicleVariation()
        {
            this._kits = new Dictionary<string, GameVehicleKit>();
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the name used for this vehicle kit.
        /// </summary>
        public string ModelName
        {
            get { return this._modelName; }
            set { this._modelName = value; }
        }

        /// <summary>
        /// Gets or sets a iterator around the individual kits that this vehicle can use.
        /// </summary>
        public IEnumerable<KeyValuePair<string, GameVehicleKit>> Kits
        {
            get
            {
                foreach (var kit in this._kits)
                {
                    yield return kit;
                }
            }
        }

        /// <summary>
        /// Gets or sets the number of times this archetype has been referenced by a vehicle.
        /// </summary>
        public int ReferenceCount
        {
            get { return this._referenceCount; }
            set { this._referenceCount = value; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Adds the specified component into this weapons components list.
        /// </summary>
        /// <param name="name">
        /// The name of the component.
        /// </param>
        /// <param name="kit">
        /// The component to add to this weapon.
        /// </param>
        public void AddVehicleKit(string name, GameVehicleKit kit)
        {
            if (!this._kits.ContainsKey(name))
            {
                this._kits.Add(name, kit);
            }
        }
        #endregion Methods
    } // RSG.Model.Vehicle.Game.GameVehicleKit {Class}
} // RSG.Model.Vehicle.Game {Namespace}
