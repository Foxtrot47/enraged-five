﻿//---------------------------------------------------------------------------------------------
// <copyright file="Vec4VTunable.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Tunables
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Xml;
    using RSG.Base.Extensions;
    using RSG.Base.Logging;
    using RSG.Editor;
    using RSG.Editor.Model;
    using RSG.Metadata.Model.Definitions.Members;
    using RSG.Metadata.Model.Resources;

    /// <summary>
    /// Represents a tunable used by the parCodeGen system that is instancing a
    /// <see cref="Vec4VMember"/> object.
    /// </summary>
    public class Vec4VTunable : TunableBase
    {
        #region Fields
        /// <summary>
        /// The private field used to store all of the vector component values.
        /// </summary>
        private float[] _values;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="Vec4VTunable"/> class to be as
        /// instance of the specified member.
        /// </summary>
        /// <param name="member">
        /// The member that this tunable will be instancing.
        /// </param>
        /// <param name="parent">
        /// The tunable parent this tunable will belong to.
        /// </param>
        public Vec4VTunable(Vec4VMember member, ITunableParent parent)
            : base(member, parent)
        {
            this._values = new float[4];
            float[] initialValue = member.InitialValue;
            for (int i = 0; i < initialValue.Length && i < this._values.Length; i++)
            {
                this._values[i] = initialValue[i];
            }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Vec4VTunable"/> class to be a
        /// instance of the specified member and uses the specified component array for the x,
        /// y, z, and w values.
        /// </summary>
        /// <param name="member">
        /// The member that this tunable will be instancing.
        /// </param>
        /// <param name="parent">
        /// The tunable parent this tunable will belong to.
        /// </param>
        /// <param name="components">
        /// A component array containing the x, y, z, and w values for this vector.
        /// </param>
        public Vec4VTunable(Vec4VMember member, ITunableParent parent, float[] components)
            : base(member, parent)
        {
            if (components.Length != 4)
            {
                throw new ArgumentException(
                    "Need to have 4 components to initialise a 4d-vector", "components");
            }

            this._values = new float[4];
            for (int i = 0; i < 4; i++)
            {
                this._values[i] = components[i];
            }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Vec4VTunable"/> class as a copy of
        /// the specified instance.
        /// </summary>
        /// <param name="other">
        /// The instance to copy.
        /// </param>
        /// <param name="parent">
        /// The tunable parent this tunable will belong to.
        /// </param>
        public Vec4VTunable(Vec4VTunable other, ITunableParent parent)
            : base(other, parent)
        {
            this._values = new float[4];
            for (int i = 0; i < other._values.Length && i < this._values.Length; i++)
            {
                this._values[i] = other._values[i];
            }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Vec4VTunable"/> class using the
        /// specified System.Xml.XmlReader as a initialising data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <param name="member">
        /// The member that this tunable will be instancing.
        /// </param>
        /// <param name="parent">
        /// The tunable parent this tunable will belong to.
        /// </param>
        /// <param name="log">
        /// A object that is used to log any warnings or errors produced during the parsing of
        /// the specified reader.
        /// </param>
        public Vec4VTunable(
            XmlReader reader, Vec4VMember member, ITunableParent parent, ILog log)
            : base(reader, member, parent)
        {
            this._values = new float[4];
            float[] initialValue = member.InitialValue;
            for (int i = 0; i < initialValue.Length && i < this._values.Length; i++)
            {
                this._values[i] = initialValue[i];
            }

            this.Deserialise(reader, log);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the float member that this tunable is instancing.
        /// </summary>
        public Vec4VMember Vec4VMember
        {
            get
            {
                Vec4VMember member = this.Member as Vec4VMember;
                if (member != null)
                {
                    return member;
                }

                return new Vec4VMember(this.TunableStructure.Definition);
            }
        }

        /// <summary>
        /// Gets or sets the value of the w component for this vector.
        /// </summary>
        public float W
        {
            get { return this._values[3]; }
            set { this.SetProperty(ref this._values[3], value); }
        }

        /// <summary>
        /// Gets or sets the value of the x component for this vector.
        /// </summary>
        public float X
        {
            get { return this._values[0]; }
            set { this.SetProperty(ref this._values[0], value); }
        }

        /// <summary>
        /// Gets or sets the value of the y component for this vector.
        /// </summary>
        public float Y
        {
            get { return this._values[1]; }
            set { this.SetProperty(ref this._values[1], value); }
        }

        /// <summary>
        /// Gets or sets the value of the z component for this vector.
        /// </summary>
        public float Z
        {
            get { return this._values[2]; }
            set { this.SetProperty(ref this._values[2], value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new <see cref="Vec4VTunable"/> that is a copy of this instance.
        /// </summary>
        /// <returns>
        /// A new <see cref="Vec4VTunable"/> that is a copy of this instance.
        /// </returns>
        public new Vec4VTunable Clone()
        {
            return new Vec4VTunable(this, this.TunableStructure);
        }

        /// <summary>
        /// Creates a deep copy of the current instance.
        /// </summary>
        /// <returns>
        /// A deep copy of the current System.Object.
        /// </returns>
        public override object DeepClone()
        {
            return this.Clone();
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="ITunable"/> by
        /// looking at their values.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance by their values;
        /// otherwise, false.
        /// </returns>
        public override bool EqualByValue(ITunable other)
        {
            return this.EqualByValue(other as Vec4VTunable);
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="ITunable"/> by
        /// looking at their values.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance by their values;
        /// otherwise, false.
        /// </returns>
        public bool EqualByValue(Vec4VTunable other)
        {
            if (other == null)
            {
                return false;
            }

            if (this.W != other.W)
            {
                return false;
            }

            if (this.X != other.X)
            {
                return false;
            }

            if (this.Y != other.Y)
            {
                return false;
            }

            if (this.Z != other.Z)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified
        /// <see cref="Vec4VTunable"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public bool Equals(Vec4VTunable other)
        {
            if (other == null)
            {
                return false;
            }

            return Object.ReferenceEquals(other, this);
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="ITunable"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public override bool Equals(ITunable other)
        {
            return this.Equals(other as Vec4VTunable);
        }

        /// <summary>
        /// Resets this tunable to have its default value. (i.e no source data with its initial
        /// values).
        /// </summary>
        public override void ResetValueToDefaultCore()
        {
            Vec4VTunable source = this.InheritanceParent as Vec4VTunable;
            if (source != null)
            {
                this.X = source.X;
                this.Y = source.Y;
                this.Z = source.Z;
                this.W = source.W;
            }
            else
            {
                float[] initialValue = this.Vec4VMember.InitialValue;
                this.X = initialValue[0];
                this.Y = initialValue[1];
                this.Z = initialValue[2];
                this.W = initialValue[3];
            }
        }

        /// <summary>
        /// Writes the xml representation of this instance onto the specified
        /// System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that the xml representation of this instance should be
        /// written to.
        /// </param>
        /// <param name="serialiseDefaultTunables">
        /// A value indicating whether the tunables that have a default value should be written
        /// to the specified writer or not.
        /// </param>
        public override void Serialise(XmlWriter writer, bool serialiseDefaultTunables)
        {
            if (writer == null)
            {
                throw new SmartArgumentNullException(() => writer);
            }

            writer.WriteAttributeString("x", this.GetScalarRepresentation(this.X));
            writer.WriteAttributeString("y", this.GetScalarRepresentation(this.Y));
            writer.WriteAttributeString("z", this.GetScalarRepresentation(this.Z));
            writer.WriteAttributeString("w", this.GetScalarRepresentation(this.W));
        }

        /// <summary>
        /// Returns the string scalar representation of this tunable. This string is used
        /// within an array.
        /// </summary>
        /// <returns>
        /// The string scalar representation.
        /// </returns>
        public override string ToString()
        {
            List<string> values = new List<string>();
            values.Add(this.GetScalarRepresentation(this.X));
            values.Add(this.GetScalarRepresentation(this.Y));
            values.Add(this.GetScalarRepresentation(this.Z));
            values.Add(this.GetScalarRepresentation(this.W));
            return String.Join(" ", values);
        }

        /// <summary>
        /// Validates this entity and creates a validation result object containing all of the
        /// errors and warnings.
        /// </summary>
        /// <param name="recursive">
        /// A value indicating whether any child objects should also be validated.
        /// </param>
        /// <returns>
        /// A new validation object containing all of the errors and warnings.
        /// </returns>
        public override ValidationResult Validate(bool recursive)
        {
            ValidationResult result = new ValidationResult();
            string[] propertyNames = new string[]
            {
                "X",
                "Y",
                "Z",
                "W",
            };

            string maximum = this.Vec4VMember.Maximum.ToStringInvariant();
            string minimum = this.Vec4VMember.Minimum.ToStringInvariant();
            float maximumValue = this.Vec4VMember.Maximum;
            float minimumValue = this.Vec4VMember.Minimum;
            string maximumMsg = StringTable.GreaterThanMaximum;
            string minimumMsg = StringTable.LessThanMinimum;
            for (int i = 0; i < this._values.Length; i++)
            {
                if (this._values[i] > maximumValue)
                {
                    string value = this._values[i].ToStringInvariant();
                    string msg = maximumMsg.FormatInvariant(value, maximum);
                    result.AddError(propertyNames[i], msg);
                }

                if (this._values[i] < minimumValue)
                {
                    string value = this._values[i].ToStringInvariant();
                    string msg = minimumMsg.FormatInvariant(value, minimum);
                    result.AddError(propertyNames[i], msg);
                }
            }

            return result;
        }

        /// <summary>
        /// Called whenever the tunable whose value will be used when this tunable is being
        /// inherited changes.
        /// </summary>
        /// <param name="oldValue">
        /// The old inheritance parent.
        /// </param>
        /// <param name="newValue">
        /// The new inheritance parent.
        /// </param>
        protected override void InheritanceParentChanged(ITunable oldValue, ITunable newValue)
        {
            string[] propertyNames = new string[]
            {
                "X",
                "Y",
                "Z",
                "W",
            };

            if (oldValue != null)
            {
                foreach (string propertyName in propertyNames)
                {
                    PropertyChangedEventManager.RemoveHandler(
                        oldValue, this.OnInheritedValueChanged, propertyName);
                }
            }

            Vec4VTunable source = newValue as Vec4VTunable;
            if (source == null)
            {
                throw new NotSupportedException(
                    "Only the smae type can be an inheritance parent.");
            }

            foreach (string propertyName in propertyNames)
            {
                PropertyChangedEventManager.AddHandler(
                    source, this.OnInheritedValueChanged, propertyName);
            }

            if (this.HasDefaultValue)
            {
                this._values[0] = source.X;
                this._values[1] = source.Y;
                this._values[2] = source.Z;
                this._values[3] = source.W;

                foreach (string propertyName in propertyNames)
                {
                    this.NotifyPropertyChanged(propertyName);
                }
            }
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        /// <param name="log">
        /// A object that is used to log any warnings or errors produced during the parsing of
        /// the specified reader.
        /// </param>
        private void Deserialise(XmlReader reader, ILog log)
        {
            if (reader == null)
            {
                return;
            }

            IXmlLineInfo lineInfo = reader as IXmlLineInfo;
            string line = lineInfo.LineNumber.ToStringInvariant();
            string pos = lineInfo.LinePosition.ToStringInvariant();

            try
            {
                int found = 0;
                for (int i = 0; i < reader.AttributeCount; i++)
                {
                    reader.MoveToAttribute(i);
                    string name = reader.LocalName;
                    string value = reader.Value;

                    if (String.Equals(name, "x"))
                    {
                        var result = this.Dictionary.TryTo<float>(value, this._values[0]);
                        if (!result.Success)
                        {
                            string msg = StringTable.VectorAttributeParseError;
                            log.Warning(msg, "x", value, line, pos);
                        }

                        this._values[0] = result.Value;
                        found++;
                    }
                    else if (String.Equals(name, "y"))
                    {
                        var result = this.Dictionary.TryTo<float>(value, this._values[1]);
                        if (!result.Success)
                        {
                            string msg = StringTable.VectorAttributeParseError;
                            log.Warning(msg, "y", value, line, pos);
                        }

                        this._values[1] = result.Value;
                        found++;
                    }
                    else if (String.Equals(name, "z"))
                    {
                        var result = this.Dictionary.TryTo<float>(value, this._values[2]);
                        if (!result.Success)
                        {
                            string msg = StringTable.VectorAttributeParseError;
                            log.Warning(msg, "z", value, line, pos);
                        }

                        this._values[2] = result.Value;
                        found++;
                    }
                    else if (String.Equals(name, "w"))
                    {
                        var result = this.Dictionary.TryTo<float>(value, this._values[3]);
                        if (!result.Success)
                        {
                            string msg = StringTable.VectorAttributeParseError;
                            log.Warning(msg, "w", value, line, pos);
                        }

                        this._values[3] = result.Value;
                        found++;
                    }
                    else
                    {
                        string msg = StringTable.Vector4dUnrecognisedAttributeError;
                        log.Warning(msg, line, pos);
                    }
                }

                if (found != this.Vec4VMember.Dimension)
                {
                    string msg = StringTable.Vector4dTunableDataMissingError;
                    log.Warning(msg, line, pos);
                }
            }
            catch (Exception ex)
            {
                string msg = StringTable.Vector4dTunableDeserialiseError;
                throw new MetadataException(msg, line, pos, ex.Message);
            }

            reader.Skip();
        }

        /// <summary>
        /// Gets the string representation of the specified string value.
        /// </summary>
        /// <param name="value">
        /// The value to convert to a string representation.
        /// </param>
        /// <returns>
        /// A string that represents this tunables value.
        /// </returns>
        private string GetScalarRepresentation(float value)
        {
            if (this.Vec4VMember.HighPrecision)
            {
                return value.ToStringInvariant("F9");
            }
            else
            {
                return value.ToStringInvariant("F6");
            }
        }

        /// <summary>
        /// Called whenever the value of the inheritance parent changes so that the values can
        /// be kept in sync.
        /// </summary>
        /// <param name="sender">
        /// The inheritance parent.
        /// </param>
        /// <param name="e">
        /// The event data.
        /// </param>
        private void OnInheritedValueChanged(object sender, PropertyChangedEventArgs e)
        {
            if (!this.HasDefaultValue)
            {
                return;
            }

            Vec4VTunable source = this.InheritanceParent as Vec4VTunable;
            if (source != null)
            {
                this._values[0] = source.X;
                this.NotifyPropertyChanged("X");

                this._values[1] = source.Y;
                this.NotifyPropertyChanged("Y");

                this._values[2] = source.Z;
                this.NotifyPropertyChanged("Z");

                this._values[3] = source.W;
                this.NotifyPropertyChanged("W");
            }
        }
        #endregion Methods
    } // RSG.Metadata.Model.Tunables.Vec4VTunable {Class}
} // RSG.Metadata.Model.Tunables {Namespace}
