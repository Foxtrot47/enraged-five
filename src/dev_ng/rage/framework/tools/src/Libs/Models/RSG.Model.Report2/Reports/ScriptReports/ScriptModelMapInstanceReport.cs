﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using RSG.Base.ConfigParser;
using RSG.Base.Configuration;
using RSG.Base.Tasks;
using RSG.Model.Common.Extensions;
using RSG.Model.Common.Map;
using System.IO;
using System.Diagnostics;
using RSG.Base.Logging;
using RSG.Model.Common;

namespace RSG.Model.Report.Reports.ScriptReports
{
    public class ScriptModelMapInstanceReport : ScriptReportBase, IDynamicLevelReport
    {
        #region Constants

        private const String NAME = "Script Model Map Instance Report";
        private const String DESC = "Generates a report that shows information about the script model enums including which ones aren't being used in script and as map instances.";

        private const String MODEL_ENUM_FILEPATH = @"script:\core\game\data\model_enums.sch";

        /// <summary>
        /// Regex used to match an enum line
        /// Matches lines like the following:
        /// "NAME=1234,      //"
        /// "NAME=1234           //"
        /// </summary>
        private static readonly Regex LINE_MATCH_REGEX = new Regex(@"^(?<name>\w+)=(?<hash>[-\d]+),?[ \t]*//");

        #endregion

        #region Private member fields

        private DynamicLevelReportContext m_context;
        private IProgress<TaskProgress> m_progress;
        private Dictionary<string, string> m_archetypeInScript;
        private Dictionary<string, List<string>> m_archNoEntities;

        #endregion

        #region Properties

        public override ITask GenerationTask
        {
            get 
            {
                if (m_task == null)
                {
                    CompositeTask task = new CompositeTask("Generate Report");
                    task.AddSubTask(new ActionTask("Generating report", (context, progress) => GenerateReport((DynamicLevelReportContext)context, progress)));
                    m_task = task;
                }
                return m_task;
            }
        }

        /// <summary>
        /// List of stats that are required to generate this report.
        /// </summary>
        public IEnumerable<StreamableStat> RequiredStats
        {
            get { return null; }
        }

        /// <summary>
        /// The file that we wish to search for enum values in
        /// </summary>
        public override string EnumFile
        {
            get
            {
                return MODEL_ENUM_FILEPATH;
            }
        }

        /// <summary>
        /// The regex to use when searching through the enum file
        /// </summary>
        public override Regex EnumValueRegex
        {
            get
            {
                return LINE_MATCH_REGEX;
            }
        }

        #endregion

        #region Constructor(s)

        /// <summary>
        /// Default constructor.
        /// </summary>
        public ScriptModelMapInstanceReport()
            : base(NAME, DESC)
        {
            m_archetypeInScript = new Dictionary<string, string>();
            m_archNoEntities = new Dictionary<string, List<string>>();
        }

        #endregion

        #region Protected methods

        private bool HasArchetype(string file, ref string archetype)
        {
            // Open up the file
            using (StreamReader fileStream = new StreamReader(file))
            {
                string line = String.Empty;
                while ((line = fileStream.ReadLine()) != null)
                {
                    foreach(string key in m_archNoEntities.Keys)
                    {
                        if (line.IndexOf(key, StringComparison.OrdinalIgnoreCase) >= 0)
                        {
                            archetype = key;
                            return true;
                        }
                    }
                }

            }

            return false;
        }

        /// <summary>
        /// On generate the report.
        /// </summary>
        /// <param name="gameView">Game view.</param>
        private void GetNoEntityArcheTypes(ConfigGameView gameView)
        {
            IMapHierarchy hierarchy = m_context.Level.MapHierarchy;
            if (hierarchy == null)
            {
                return;
            }

            double count = (double)hierarchy.AllSections.Count();
            double progressTick = 0.5f / count; // This is the first half of the report.
            double ticker = 0;

            // Load all the map section data
            foreach (IMapSection section in hierarchy.AllSections)
            {
                section.RequestStatistics(new StreamableStat[] { StreamableMapSectionStat.Entities }, false);

                foreach (IMapArchetype arch in section.Archetypes)
                {
                    if (arch.ExteriorEntities.Count() == 0 && arch.InteriorEntities.Count() == 0)
                    {
                        if (!m_archNoEntities.ContainsKey(arch.Name.ToLower()))
                        {
                            m_archNoEntities[arch.Name.ToLower()] = new List<string>();
                        }
                        m_archNoEntities[arch.Name.ToLower()].Add(section.Name);
                    }
                }

                ticker += progressTick;
                m_progress.Report(new TaskProgress(ticker, section.Name));
            }
        }

        protected override Stream WriteReport(SortedDictionary<string, EnumValueInfo> info)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter reportFile = new StreamWriter(stream);

            HtmlTextWriter writer = new HtmlTextWriter(reportFile);

            writer.RenderBeginTag(HtmlTextWriterTag.Html);
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Head);
                writer.RenderEndTag();

                writer.RenderBeginTag(HtmlTextWriterTag.Body);
                {
                    WriteHeader(writer, "Archetype in Script & Instance Report");

                    OnSummary(writer);

                    WriteArchNoScript(writer);
                    WriteScriptInstances(writer);
                }
                writer.RenderEndTag();
            }

            writer.RenderEndTag();
            writer.Flush();
            stream.Position = 0;

            return stream;
        }


        /// <summary>
        /// On generate summary.
        /// </summary>
        /// <param name="writer">Html writer.</param>
        private void OnSummary(HtmlTextWriter writer)
        {
            WriteParagraph(writer, String.Format("A total of {0} archetypes with no entities are referenced in map sections.", m_archNoEntities.Count));
            WriteParagraph(writer, String.Format("There are {0} archetypes referenced in scripts.", m_archetypeInScript.Count));
        }

        /// <summary>
        /// Generate the report.
        /// </summary>
        /// <param name="context">Context.</param>
        /// <param name="progress">Progress.</param>
        protected virtual void GenerateReport(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            m_context = context;
            m_progress = progress;

            Stopwatch sw = new Stopwatch();
            sw.Start();

            GetNoEntityArcheTypes(context.GameView);

            IEnumerable<string> scriptFiles = Directory.EnumerateFiles(context.GameView.ScriptDir, "*.*", SearchOption.AllDirectories).Where(file => SCRIPT_EXTENSIONS.Contains(Path.GetExtension(file)));
            double fileCount = scriptFiles.Count();
            double tickValue = 0.5 / fileCount; // This is the second half of the report.
            double tick = 0.5; // Must start at the half way mark because this is the second half.

            foreach (string s in scriptFiles)
            {
                string archetype = String.Empty;

                if (HasArchetype(s, ref archetype))
                {
                    m_archetypeInScript[archetype] = s;
                }
                tick += tickValue;
                m_progress.Report(new TaskProgress(tick, Path.GetFileName(s)));
            }

            Stream = WriteReport(null);

            sw.Stop();
            Log.Log__Message("{0} report took {1}ms to generate.", this.Name, sw.ElapsedMilliseconds);
        }

        #endregion

        #region Private helper methods

        /// <summary>
        /// Write the archetype report.
        /// </summary>
        /// <param name="writer">Writer.</param>
        private void WriteArchNoScript(HtmlTextWriter writer)
        {
            WriteSubHeader(writer, "Archetypes With No Entities That Don't Appear in Script");
            if (m_archNoEntities.Count == 0)
            {
                WriteParagraph(writer, "None found");
            }
            else
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Table);

                // Header Row
                writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.RenderBeginTag(HtmlTextWriterTag.B);
                writer.Write("Archetype");
                writer.RenderEndTag();
                writer.RenderEndTag();

                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.RenderBeginTag(HtmlTextWriterTag.B);
                writer.Write("Section(s)");
                writer.RenderEndTag();
                writer.RenderEndTag();
                writer.RenderEndTag();

                // Output a line for each of them
                foreach (var pair in m_archNoEntities)
                {

                    if (!m_archetypeInScript.ContainsKey(pair.Key))
                    {
                        writer.RenderBeginTag(HtmlTextWriterTag.Tr);

                        writer.RenderBeginTag(HtmlTextWriterTag.Td);
                        writer.Write(pair.Key);
                        writer.RenderEndTag();

                        writer.RenderBeginTag(HtmlTextWriterTag.Td);

                        int count = m_archNoEntities.Values.Count;
                        int counter = 0;
                        foreach (string s in pair.Value)
                        {
                            writer.Write(s);
                            if (counter < count - 1)
                            {
                                writer.Write(", ");
                            }

                            counter++;
                        }

                        writer.RenderEndTag();
                        writer.RenderEndTag();
                    }
                }

                writer.RenderEndTag();
                writer.WriteBreak();
            }
        }

        /// <summary>
        /// Write the map instances report.
        /// </summary>
        /// <param name="writer">Writer.</param>
        private void WriteScriptInstances(HtmlTextWriter writer)
        {
            WriteSubHeader(writer, "Archetypes Referenced in Script");

            if (m_archetypeInScript.Count == 0)
            {
                WriteParagraph(writer, "None found");
            }
            else
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Table);

                // Header Row
                writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.RenderBeginTag(HtmlTextWriterTag.B);
                writer.Write("Archetype Name");
                writer.RenderEndTag();
                writer.RenderEndTag();

                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.RenderBeginTag(HtmlTextWriterTag.B);
                writer.Write("Example of Where Used");
                writer.RenderEndTag();
                writer.RenderEndTag();
                writer.RenderEndTag();

                // Output a line for each of them
                foreach (var pair in m_archetypeInScript)
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                    writer.RenderBeginTag(HtmlTextWriterTag.Td);
                    writer.Write(pair.Key);
                    writer.RenderEndTag();

                    writer.RenderBeginTag(HtmlTextWriterTag.Td);
                    writer.Write(pair.Value);
                    writer.RenderEndTag();

                    writer.RenderEndTag();
                }

                writer.RenderEndTag();
                writer.WriteBreak();
            }
        }

        #endregion
    }
}
