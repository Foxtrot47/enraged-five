﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using RSG.Base.Logging;
using RSG.Interop.Bugstar.Organisation;
using RSG.Model.Common.Mission;
using RSG.Statistics.Client.ServiceClients;

namespace RSG.Model.Asset.Factories
{
    /// <summary>
    /// 
    /// </summary>
    public class MissionFactory
    {
        #region Public Methods
        /// <summary>
        /// Creates a new mission collection from the specified source.
        /// </summary>
        public IMissionCollection CreateCollection(bool fromBugstar, Project bugstarProject = null)
        {
            IMissionCollection collection = null;

            if (fromBugstar)
            {
                Debug.Assert(bugstarProject != null,
                    "Attempting to create a mission collection from bugstar without providing the bugstar project.");
                if (bugstarProject == null)
                {
                    throw new ArgumentNullException("bugstarProject",
                        "Attempting to create a mission collection from bugstar without providing the bugstar project.");
                }

                Log.Log__Profile("Mission collection creation - bugstar.");
                collection = CreateCollectionFromBugstar(bugstarProject);
                Log.Log__ProfileEnd();
            }
            else
            {
                Log.Log__Profile("Mission collection creation - database.");
                collection = CreateCollectionFromDatabase();
                Log.Log__ProfileEnd();
            }

            return collection;
        }
        #endregion // Public Methods

        #region Private Methods
        /// <summary>
        /// Copy fields from a Bugstar mission object into a IMission object.
        /// </summary>
        /// <param name="bgsMission">Bugstar mission object.</param>
        /// <param name="mission">IMission object (target).</param>
        private void CopyBugstarMissionToMission(RSG.Interop.Bugstar.Game.Mission bgsMission, IMission mission)
        {
            mission.Description = bgsMission.Description;
            mission.MissionId = bgsMission.MissionId;
            mission.ScriptName = bgsMission.ScriptName;
            mission.ProjectedAttempts = bgsMission.ProjectedAttempts;
            mission.ProjectedAttemptsMin = bgsMission.ProjectedAttemptsMin;
            mission.ProjectedAttemptsMax = bgsMission.ProjectedAttemptsMax;
            mission.Singleplayer = bgsMission.Singleplayer;
            mission.Active = bgsMission.Active;
            mission.InBugstar = true;

            // Handle checkpoints next.
            mission.Checkpoints.Clear();
            foreach (RSG.Interop.Bugstar.Game.MissionCheckpoint bgsCheckpoint in bgsMission.Checkpoints)
            {
                IMissionCheckpoint checkpoint = new RSG.Model.Common.Mission.MissionCheckpoint(bgsCheckpoint.Name);
                checkpoint.ProjectedAttempts = bgsCheckpoint.ProjectedAttempts;
                checkpoint.ProjectedAttemptsMin = bgsCheckpoint.ProjectedAttemptsMin;
                checkpoint.ProjectedAttemptsMax = bgsCheckpoint.ProjectedAttemptsMax;
                checkpoint.InBugstar = true;
                checkpoint.Index = bgsCheckpoint.Ordering;
                mission.Checkpoints.Add(checkpoint);
            }
        }

        /// <summary>
        /// Creates a mission collection from Bugstar.
        /// </summary>
        /// <returns></returns>
        private IMissionCollection CreateCollectionFromBugstar(Project bugstarProject)
        {
            IMissionCollection missions = new MissionCollection();

            foreach (RSG.Interop.Bugstar.Game.Mission bgsMission in bugstarProject.Missions)
            {
                IMission mission = new Mission(bgsMission.Name);
                CopyBugstarMissionToMission(bgsMission, mission);
                missions.Add(mission);
            }

            return missions;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private IMissionCollection CreateCollectionFromDatabase()
        {
            IMissionCollection missions;

            try
            {
                using (GameAssetClient client = new GameAssetClient())
                {
                    missions = client.GetMissions();
                }
            }
            catch (Exception ex)
            {
                Log.Log__Exception(ex, "Unhandled exception occurred while retrieving the missions collection from the database.");
                missions = new MissionCollection();
            }

            return missions;
        }
        #endregion // Private Methods
    } // MissionFactory
}
