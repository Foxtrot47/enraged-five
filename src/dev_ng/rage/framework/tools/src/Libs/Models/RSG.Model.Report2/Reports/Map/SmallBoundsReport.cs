﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using RSG.Base.Logging;
using System.IO;
using System.Web.UI;
using RSG.Model.Common;
using RSG.Base.Math;
using RSG.Bounds;
using RSG.Base.ConfigParser;
using Ionic.Zip;
using RSG.SceneXml;
using RSG.Model.Common.Map;
using RSG.Base.Tasks;
using RSG.Model.Common.Extensions;
using RSG.Base.Extensions;

namespace RSG.Model.Report.Reports.Map
{
    /// <summary>
    /// 
    /// </summary>
    public class SmallBoundsReport : HTMLReport, IDynamicLevelReport
    {
        #region Constants
        private const string c_name = "Small Collision Bounds Report";
        private const string c_description = "Generate and display information about any small collision boxes sections may have.";
        private const int    c_defaultMaxBndFilesize = 512;
        private const float  c_defaultMinDimensionSize = 0.0199f;
        #endregion // Constants

        #region Types
        /// <summary>
        /// 
        /// </summary>
        protected class CollisionBoundInfo
        {
            public string CollisionName { get; private set; }
            public CollisionType CollisionFlags { get; private set; }
            public BNDFile BoundsFile { get; private set; }

            public CollisionBoundInfo(string name, CollisionType flags, BNDFile bndFile)
            {
                CollisionName = name;
                CollisionFlags = flags;
                BoundsFile = bndFile;
            }
        }

        /// <summary>
        /// Class to hold information about a single small bound
        /// </summary>
        protected class SmallBoundInfo
        {
            public string ObjectName { get; private set; }
            public CollisionType CollisionFlags { get; private set; }
            public Vector3f BoundDimensions { get; private set; }

            public SmallBoundInfo(string objectName, CollisionType flags, Vector3f dimensions)
            {
                ObjectName = objectName;
                CollisionFlags = flags;
                BoundDimensions = dimensions;
            }
        }
        #endregion // Types

        #region Member Data
        /// <summary>
        /// Map of section to a list of small bounds that it contains
        /// </summary>
        private Dictionary<IMapSection, IList<SmallBoundInfo>> m_smallBoundsMap;
        #endregion // Member Data

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (m_task == null)
                {
                    CompositeTask task = new CompositeTask("Generate Report");
                    task.AddSubTask(new ActionTask("Searching for small bounds", (context, progress) => SearchForSmallBounds((DynamicLevelReportContext)context, progress)), 10);
                    task.AddSubTask(new ActionTask("Generating report", (context, progress) => GenerateReport((DynamicLevelReportContext)context, progress)));
                    m_task = task;
                }
                return m_task;
            }
        }
        private ITask m_task;

        /// <summary>
        /// List of stats that are required to generate this report.
        /// </summary>
        public IEnumerable<StreamableStat> RequiredStats
        {
            get { return null; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int MaxBNDFileSize
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public float MinDimensionSize
        {
            get;
            set;
        }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public SmallBoundsReport()
            : base(c_name, c_description)
        {
            MaxBNDFileSize = c_defaultMaxBndFilesize;
            MinDimensionSize = c_defaultMinDimensionSize;
        }
        #endregion // Constructor(s)

        #region Task Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void SearchForSmallBounds(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            IMapHierarchy hierarchy = context.Level.MapHierarchy;
            m_smallBoundsMap = null;

            if (hierarchy != null)
            {
                m_smallBoundsMap = SearchForSmallBounds(hierarchy, context.GameView);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void GenerateReport(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            if (m_smallBoundsMap != null)
            {
                Stream = WriteReport(m_smallBoundsMap);
            }
        }
        #endregion // Task Methods

        #region Private Methods
        /// <summary>
        /// Processes the entire level looking for bounds that are smaller than a certain size, returning
        /// a map of map section to small bounds list
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        private Dictionary<IMapSection, IList<SmallBoundInfo>> SearchForSmallBounds(IMapHierarchy hierarchy, ConfigGameView gv)
        {
            Dictionary<IMapSection, IList<SmallBoundInfo>> smallBoundsMap = new Dictionary<IMapSection, IList<SmallBoundInfo>>();

            // Check all the sections
            foreach (IMapSection section in hierarchy.AllSections)
            {
                IList<CollisionBoundInfo> boundFiles = new List<CollisionBoundInfo>();

                // Get the node that we actually want to process
                ContentNodeMap mapNode = section.GetConfigMapNode(gv);

                foreach (ContentNode exportNode in mapNode.Outputs)
                {
                    ContentNodeMapZip nodeZip = exportNode as ContentNodeMapZip;
                    if (nodeZip != null)
                    {
                        // Open up the scene xml file (for retrieving collision type flags)
                        string sceneXmlFilename = Path.ChangeExtension(nodeZip.Filename, "xml");

                        Scene scene = null;
                        if (File.Exists(sceneXmlFilename))
                        {
                            scene = new Scene(sceneXmlFilename, mapNode, LoadOptions.All, true);
                        }

                        if (scene != null && File.Exists(nodeZip.Filename))
                        {
                            boundFiles.AddRange(LoadBoundsDataFromMapZipFile(nodeZip.Name, nodeZip.Filename, scene));
                        }
                    }
                }

                // We now have a list of all the bndfiles, so check them out for box primitives with small bounds
                List<SmallBoundInfo> smallBounds = new List<SmallBoundInfo>();

                foreach (CollisionBoundInfo boundInfo in boundFiles)
                {
                    ProcessBoundObject(boundInfo.CollisionName, boundInfo.CollisionFlags, boundInfo.BoundsFile.BoundObject, ref smallBounds);
                }

                if (smallBounds.Count > 0)
                {
                    smallBoundsMap.Add(section, smallBounds);
                }
            }

            return smallBoundsMap;
        }

        /// <summary>
        /// Processes an export zip file to get a list of BND files
        /// </summary>
        /// <param name="sectionName"></param>
        /// <param name="zipFileName"></param>
        /// <returns></returns>
        protected IEnumerable<CollisionBoundInfo> LoadBoundsDataFromMapZipFile(string sectionName, string zipFileName, Scene scene)
        {
            // Get the list of bound files
            using (ZipFile zipFile = ZipFile.Read(zipFileName))
            {
                // Get the item that contains the bounds information
                ZipEntry entry = zipFile.FirstOrDefault(item => (item.FileName.EndsWith(".ibr.zip") || item.FileName.EndsWith(".ibn.zip") || item.FileName.EndsWith("collision.zip")));

                if (entry != null)
                {
                    using (MemoryStream zipStream = new MemoryStream())
                    {
                        entry.Extract(zipStream);
                        zipStream.Position = 0;

                        foreach (CollisionBoundInfo info in LoadBoundsDataFromBoundsZip(zipStream, scene))
                        {
                            yield return info;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Processes a ibn.zip or ibr.zip file to extract a list of bnd files
        /// </summary>
        /// <param name="zipStream"></param>
        /// <returns></returns>
        private IEnumerable<CollisionBoundInfo> LoadBoundsDataFromBoundsZip(Stream zipStream, Scene scene)
        {
            using (ZipFile zipFile = ZipFile.Read(zipStream))
            {
                // Iterate over all bnd files that are less than MAX_BND_FILESIZE.  The filesize limit is to stop
                // us needing to process files that aren't going to contain data we are looking for.
                IEnumerable<ZipEntry> bndEntries = zipFile.Where(item => (item.FileName.EndsWith(".bnd") && item.UncompressedSize < MaxBNDFileSize));
                foreach (ZipEntry entry in bndEntries)
                {
                    // Check whether the corresponding collision object in the scenexml scene for this bound file
                    // has mover or horse collision flags set on it.
                    string name = Path.GetFileNameWithoutExtension(entry.FileName);
                    TargetObjectDef objDef = scene.FindObject(name);

                    if (objDef != null)
                    {
                        CollisionType collisionFlags = GetCollisionFlagsForObject(objDef);

                        if ((collisionFlags & CollisionType.Mover) != 0 ||
                            (collisionFlags & CollisionType.Mover) != 0)
                        {
                            using (MemoryStream fileStream = new MemoryStream())
                            {
                                entry.Extract(fileStream);
                                fileStream.Position = 0;

                                yield return new CollisionBoundInfo(name, collisionFlags, BNDFile.Load(fileStream));
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sceneObject"></param>
        /// <returns></returns>
        private CollisionType GetCollisionFlagsForObject(TargetObjectDef sceneObject)
        {
            CollisionType flags = 0;

            // Process the rest of the collision types
            foreach (CollisionType type in Enum.GetValues(typeof(CollisionType)))
            {
                bool defaultValue = type.IsSetByDefault();

                bool isFlagSet = false;
                if (!Boolean.TryParse(sceneObject.GetUserProperty(type.GetUserPropertyName(), defaultValue.ToString()), out isFlagSet))
                {
                    isFlagSet = defaultValue;
                }

                if (isFlagSet)
                {
                    flags |= type;
                }
            }

            return flags;
        }

        /// <summary>
        /// Processes an individual bound object
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="obj"></param>
        /// <param name="smallBounds"></param>
        protected void ProcessBoundObject(string objectName, CollisionType collisionFlags, BoundObject obj, ref List<SmallBoundInfo> smallBounds)
        {
            if (obj is Composite)
            {
                Composite composite = (Composite)obj;

                foreach (CompositeChild child in composite.Children)
                {
                    ProcessBoundObject(objectName, collisionFlags, child.BoundObject, ref smallBounds);
                }
            }
            else
            {
                // Convert the bound object to a BVH and then process
                BVH bvh = BoundObjectConverter.ToBVH(obj);
                ProcessBVH(objectName, collisionFlags, bvh, ref smallBounds);
            }
        }

        /// <summary>
        /// Processes a BVH, checking all boxes for small bounds
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="bvh"></param>
        /// <param name="smallBounds"></param>
        private void ProcessBVH(string objectName, CollisionType collisionFlags, BVH bvh, ref List<SmallBoundInfo> smallBounds)
        {
            List<BVHMaterial> materials = bvh.Materials.Select(item => new BVHMaterial(item)).ToList();

            foreach (BVHPrimitive prim in bvh.Primitives)
            {
                if (prim is BVHBox)
                {
                    BVHBox box = (BVHBox)prim;

                    // Get the size of the box
                    Vector3f twiceBoxX = bvh.Verts[box.Index0].Position + bvh.Verts[box.Index2].Position - bvh.Verts[box.Index1].Position - bvh.Verts[box.Index3].Position;
                    Vector3f twiceBoxY = bvh.Verts[box.Index0].Position + bvh.Verts[box.Index1].Position - bvh.Verts[box.Index2].Position - bvh.Verts[box.Index3].Position;
                    Vector3f twiceBoxZ = bvh.Verts[box.Index1].Position + bvh.Verts[box.Index2].Position - bvh.Verts[box.Index0].Position - bvh.Verts[box.Index3].Position;
                    Vector3f boxSize = new Vector3f((float)twiceBoxX.Magnitude() * 0.5f, (float)twiceBoxY.Magnitude() * 0.5f, (float)twiceBoxZ.Magnitude() * 0.5f);

                    if (boxSize.X < MinDimensionSize ||
                        boxSize.Y < MinDimensionSize ||
                        boxSize.Z < MinDimensionSize)
                    {
                        smallBounds.Add(new SmallBoundInfo(objectName, collisionFlags, boxSize));
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="smallBoundsMap"></param>
        /// <returns></returns>
        protected Stream WriteReport(Dictionary<IMapSection, IList<SmallBoundInfo>> smallBoundsMap)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter reportFile = new StreamWriter(stream);

            HtmlTextWriter writer = new HtmlTextWriter(reportFile);
            writer.RenderBeginTag(HtmlTextWriterTag.Html);
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Head);
                writer.RenderEndTag();

                writer.RenderBeginTag(HtmlTextWriterTag.Body);
                {
                    WriteHeader(writer, "Small Bounds Report");

                    WriteSubHeader(writer, "Summary");
                    OutputSummary(writer, smallBoundsMap);

                    if (smallBoundsMap.Count > 0)
                    {
                        WriteSubHeader(writer, "Detailed Information");

                        foreach (KeyValuePair<IMapSection, IList<SmallBoundInfo>> pair in smallBoundsMap)
                        {
                            OutputDefinitionInformation(writer, pair.Key, pair.Value);
                        }
                    }
                }
                writer.RenderEndTag();
            }
            writer.RenderEndTag();
            writer.Flush();

            stream.Position = 0;
            return stream;
        }
        
        /// <summary>
        /// Outputs a summary of the information we found
        /// </summary>
        /// <param name="writer"></param>
        private void OutputSummary(HtmlTextWriter writer, Dictionary<IMapSection, IList<SmallBoundInfo>> smallBounds)
        {
            if (smallBounds.Count == 0)
            {
                WriteParagraph(writer, String.Format("Congratulations, none of the map sections have bounds smaller than {0} meters.", MinDimensionSize));
            }
            else
            {
                // Determine the total number of small bounds
                int totalCount = smallBounds.Sum(item => item.Value.Count);

                // Output the summary
                string summaryText = String.Format("{0} sections have a total of {1} collision boxes with one (or more) bound dimension less than {2} meters.",
                                                   smallBounds.Count, totalCount, MinDimensionSize);
                WriteParagraph(writer, summaryText);

                // Output each of the props we found and and link to the detailed information
                foreach (KeyValuePair<IMapSection, IList<SmallBoundInfo>> pair in smallBounds)
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Href, String.Format("#{0}", pair.Key.Name));
                    writer.RenderBeginTag(HtmlTextWriterTag.A);
                    writer.Write(String.Format("{0} ({1} small bounds)", pair.Key.Name, pair.Value.Count));
                    writer.RenderEndTag();
                    writer.WriteBreak();
                }
            }
        }

        /// <summary>
        /// Outputs information regarding a single definition
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="propDef"></param>
        private void OutputDefinitionInformation(HtmlTextWriter writer, IMapSection section, IList<SmallBoundInfo> smallBounds)
        {
            // <a name="sectionName"><b>Section Name</b></a>
            writer.AddAttribute(HtmlTextWriterAttribute.Name, section.Name);
            writer.RenderBeginTag(HtmlTextWriterTag.A);
            writer.RenderBeginTag(HtmlTextWriterTag.B);
            writer.Write(section.Name);
            writer.RenderEndTag();
            writer.RenderEndTag();

            // Output section parent info??
            /*
            IMapContainer parent = section.Container;
            IMapContainer grandParent = null;
            if (parent is MapArea)
            {
                grandParent = (parent as MapArea).Container;
            }
            */

            // <table>
            //   <tr><td><I>Collision Object Name</I></td><td><I>Bounds</I></td></tr>
            //   <tr><td>...</td><td>...</td></tr>
            //   ....
            // </table>
            writer.RenderBeginTag(HtmlTextWriterTag.Table);

            // Header Row
            writer.RenderBeginTag(HtmlTextWriterTag.Tr);
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.RenderBeginTag(HtmlTextWriterTag.I);
            writer.Write("Collision Object Name");
            writer.RenderEndTag();
            writer.RenderEndTag();
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.RenderBeginTag(HtmlTextWriterTag.I);
            writer.Write("Collision Flags");
            writer.RenderEndTag();
            writer.RenderEndTag();
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.RenderBeginTag(HtmlTextWriterTag.I);
            writer.Write("Bounds Dimensions");
            writer.RenderEndTag();
            writer.RenderEndTag();
            writer.RenderEndTag();

            foreach (SmallBoundInfo boundInfo in smallBounds)
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write(boundInfo.ObjectName);
                writer.RenderEndTag();
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write(String.Join(" + ", boundInfo.CollisionFlags.GetDisplayNames().ToArray()));
                writer.RenderEndTag();
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write(boundInfo.BoundDimensions);
                writer.RenderEndTag();
                writer.RenderEndTag();
            }

            writer.RenderEndTag();
            writer.WriteBreak();
        }
        #endregion // Private Methods
    } // SmallBoundsReport
} // RSG.Model.Report.Reports
