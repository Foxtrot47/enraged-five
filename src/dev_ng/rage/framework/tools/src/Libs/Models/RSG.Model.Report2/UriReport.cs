﻿using System;
using RSG.Base.Editor.Command;

namespace RSG.Model.Report
{
    /// <summary>
    /// UriReport - A report that has a Uri - file or URL to display.
    /// </summary>
    public abstract class UriReport : Report, IReportUriProvider
    {
        #region Properties
        /// <summary>
        /// Uri of the report.
        /// </summary>
        public Uri Uri 
        { 
            get { return m_uri; } 
            set
            {
                SetPropertyValue(value, () => this.Uri,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_uri = (Uri)newValue;
                        }
                ));
            }
        }
        private Uri m_uri;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="desc"></param>
        public UriReport(string name, string desc)
            : base(name, desc)
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="name"></param>
        /// <param name="desc"></param>
        public UriReport(Uri uri, string name, string desc)
            : base(name, desc)
        {
            this.Uri = uri;
        }
        #endregion // Constructor(s)
    }
} // RSG.Model.Report namespace
