﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common
{
    /// <summary>
    /// Base class for all asset types
    /// </summary>
    public abstract class FileAssetBase : 
        AssetBase, 
        IFileAsset,
        IComparable,
        IComparable<FileAssetBase>
    {
        #region Members

        protected String m_filename;

        #endregion // Members

        #region Properties

        /// <summary>
        /// The filename (disk location) of the asset
        /// </summary>
        public String Filename
        {
            get { return m_filename; }
            set
            {
                SetPropertyValue(ref m_filename, value, "Filename");
            }
        }

        #endregion // Properties

        #region Constructors

        /// <summary>
        /// Default Constructor
        /// </summary>
        public FileAssetBase()
            : base("Unknown File Asset")
        {
            this.m_filename = "";
        }

        /// <summary>
        /// Default Constructor
        /// </summary>
        public FileAssetBase(String name, String filename)
            : base(name)
        {
            this.m_filename = filename;
        }

        #endregion // Constructors

        #region IComparable Interface
        /// <summary>
        /// Compare to another object.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(Object other)
        {
            if (other is FileAssetBase)
                return (CompareTo(other as FileAssetBase));
            return (-1);
        }

        /// <summary>
        /// Compare's to another FileAssetBase object.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(FileAssetBase other)
        {
            int filenameComparison = this.Filename.CompareTo(other.Filename);
            return (filenameComparison == 0 ? base.CompareTo(other) : filenameComparison);
        }
        #endregion // IComparable Interface
    } // FileAssetBase
} // RSG.Model.Common
