﻿//---------------------------------------------------------------------------------------------
// <copyright file="BoolVectorTunable.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Tunables
{
    using System;
    using System.ComponentModel;
    using System.Xml;
    using RSG.Base.Logging;
    using RSG.Editor;
    using RSG.Metadata.Model.Definitions.Members;
    using RSG.Metadata.Model.Resources;

    /// <summary>
    /// Represents a tunable used by the parCodeGen system that is instancing a
    /// <see cref="BoolVectorMember"/> object.
    /// </summary>
    public class BoolVectorTunable : TunableBase
    {
        #region Fields
        /// <summary>
        /// The private field used to store all of the vector component values.
        /// </summary>
        private bool[] _values;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="BoolVectorTunable"/> class to be as
        /// instance of the specified member.
        /// </summary>
        /// <param name="member">
        /// The member that this tunable will be instancing.
        /// </param>
        /// <param name="parent">
        /// The tunable parent this tunable will belong to.
        /// </param>
        public BoolVectorTunable(BoolVectorMember member, ITunableParent parent)
            : base(member, parent)
        {
            this._values = new bool[4];
            bool[] initialValue = member.InitialValue;
            for (int i = 0; i < initialValue.Length && i < this._values.Length; i++)
            {
                this._values[i] = initialValue[i];
            }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="BoolVectorTunable"/> class as a copy
        /// of the specified instance.
        /// </summary>
        /// <param name="other">
        /// The instance to copy.
        /// </param>
        /// <param name="parent">
        /// The tunable parent this tunable will belong to.
        /// </param>
        public BoolVectorTunable(BoolVectorTunable other, ITunableParent parent)
            : base(other, parent)
        {
            this._values = new bool[4];
            for (int i = 0; i < other._values.Length && i < this._values.Length; i++)
            {
                this._values[i] = other._values[i];
            }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="BoolVectorTunable"/> class using the
        /// specified System.Xml.XmlReader as a initialising data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <param name="member">
        /// The member that this tunable will be instancing.
        /// </param>
        /// <param name="parent">
        /// The tunable parent this tunable will belong to.
        /// </param>
        /// <param name="log">
        /// A object that is used to log any warnings or errors produced during the parsing of
        /// the specified reader.
        /// </param>
        public BoolVectorTunable(
            XmlReader reader, BoolVectorMember member, ITunableParent parent, ILog log)
            : base(reader, member, parent)
        {
            this._values = new bool[4];
            bool[] initialValue = member.InitialValue;
            for (int i = 0; i < initialValue.Length && i < this._values.Length; i++)
            {
                this._values[i] = initialValue[i];
            }

            this.Deserialise(reader, log);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the bool member that this tunable is instancing.
        /// </summary>
        public BoolVectorMember BoolVectorMember
        {
            get
            {
                BoolVectorMember member = this.Member as BoolVectorMember;
                if (member != null)
                {
                    return member;
                }

                return new BoolVectorMember(this.TunableStructure.Definition);
            }
        }

        /// <summary>
        /// Gets or sets the value of the w component for this vector.
        /// </summary>
        public bool W
        {
            get { return this._values[3]; }
            set { this.SetProperty(ref this._values[3], value); }
        }

        /// <summary>
        /// Gets or sets the value of the x component for this vector.
        /// </summary>
        public bool X
        {
            get { return this._values[0]; }
            set { this.SetProperty(ref this._values[0], value); }
        }

        /// <summary>
        /// Gets or sets the value of the y component for this vector.
        /// </summary>
        public bool Y
        {
            get { return this._values[1]; }
            set { this.SetProperty(ref this._values[1], value); }
        }

        /// <summary>
        /// Gets or sets the value of the z component for this vector.
        /// </summary>
        public bool Z
        {
            get { return this._values[2]; }
            set { this.SetProperty(ref this._values[2], value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new <see cref="BoolVectorTunable"/> that is a copy of this instance.
        /// </summary>
        /// <returns>
        /// A new <see cref="BoolVectorTunable"/> that is a copy of this instance.
        /// </returns>
        public new BoolVectorTunable Clone()
        {
            return new BoolVectorTunable(this, this.TunableStructure);
        }

        /// <summary>
        /// Creates a deep copy of the current instance.
        /// </summary>
        /// <returns>
        /// A deep copy of the current System.Object.
        /// </returns>
        public override object DeepClone()
        {
            return this.Clone();
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="ITunable"/> by
        /// looking at their values.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance by their values;
        /// otherwise, false.
        /// </returns>
        public override bool EqualByValue(ITunable other)
        {
            return this.EqualByValue(other as BoolVectorTunable);
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="ITunable"/> by
        /// looking at their values.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance by their values;
        /// otherwise, false.
        /// </returns>
        public bool EqualByValue(BoolVectorTunable other)
        {
            if (other == null)
            {
                return false;
            }

            if (this.W != other.W)
            {
                return false;
            }

            if (this.X != other.X)
            {
                return false;
            }

            if (this.Y != other.Y)
            {
                return false;
            }

            if (this.Z != other.Z)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified
        /// <see cref="BoolVectorTunable"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public bool Equals(BoolVectorTunable other)
        {
            if (other == null)
            {
                return false;
            }

            return Object.ReferenceEquals(other, this);
        }

        /// <summary>
        /// Indicates whether this instance is equal to the specified <see cref="ITunable"/>.
        /// </summary>
        /// <param name="other">
        /// A definition to compare with this instance.
        /// </param>
        /// <returns>
        /// True if this instance is equal to the specified instance; otherwise, false.
        /// </returns>
        public override bool Equals(ITunable other)
        {
            return this.Equals(other as BoolVectorTunable);
        }

        /// <summary>
        /// Resets this tunable to have its default value. (i.e no source data with its initial
        /// values).
        /// </summary>
        public override void ResetValueToDefaultCore()
        {
            BoolVectorTunable source = this.InheritanceParent as BoolVectorTunable;
            if (source != null)
            {
                this.X = source.X;
                this.Y = source.Y;
                this.Z = source.Z;
                this.W = source.W;
            }
            else
            {
                bool[] initialValue = this.BoolVectorMember.InitialValue;
                this.X = initialValue[0];
                this.Y = initialValue[1];
                this.Z = initialValue[2];
                this.W = initialValue[3];
            }
        }

        /// <summary>
        /// Writes the xml representation of this instance onto the specified
        /// System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that the xml representation of this instance should be
        /// written to.
        /// </param>
        /// <param name="serialiseDefaultTunables">
        /// A value indicating whether the tunables that have a default value should be written
        /// to the specified writer or not.
        /// </param>
        public override void Serialise(XmlWriter writer, bool serialiseDefaultTunables)
        {
            if (writer == null)
            {
                throw new SmartArgumentNullException(() => writer);
            }

            writer.WriteAttributeString("x", this.X.ToString());
            writer.WriteAttributeString("y", this.Y.ToString());
            writer.WriteAttributeString("z", this.Z.ToString());
            writer.WriteAttributeString("w", this.W.ToString());
        }

        /// <summary>
        /// Called whenever the tunable whose value will be used when this tunable is being
        /// inherited changes.
        /// </summary>
        /// <param name="oldValue">
        /// The old inheritance parent.
        /// </param>
        /// <param name="newValue">
        /// The new inheritance parent.
        /// </param>
        protected override void InheritanceParentChanged(ITunable oldValue, ITunable newValue)
        {
            string[] propertyNames = new string[]
            {
                "X",
                "Y",
                "Z",
                "W",
            };

            if (oldValue != null)
            {
                foreach (string propertyName in propertyNames)
                {
                    PropertyChangedEventManager.RemoveHandler(
                        oldValue, this.OnInheritedValueChanged, propertyName);
                }
            }

            BoolVectorTunable source = newValue as BoolVectorTunable;
            if (source == null)
            {
                throw new NotSupportedException(
                    "Only the smae type can be an inheritance parent.");
            }

            foreach (string propertyName in propertyNames)
            {
                PropertyChangedEventManager.AddHandler(
                    source, this.OnInheritedValueChanged, propertyName);
            }

            if (this.HasDefaultValue)
            {
                this._values[0] = source.X;
                this._values[1] = source.Y;
                this._values[2] = source.Z;
                this._values[3] = source.W;

                foreach (string propertyName in propertyNames)
                {
                    this.NotifyPropertyChanged(propertyName);
                }
            }
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        /// <param name="log">
        /// A object that is used to log any warnings or errors produced during the parsing of
        /// the specified reader.
        /// </param>
        private void Deserialise(XmlReader reader, ILog log)
        {
            if (reader == null)
            {
                return;
            }

            IXmlLineInfo lineInfo = reader as IXmlLineInfo;
            int line = lineInfo.LineNumber;
            int pos = lineInfo.LinePosition;

            try
            {
                if (!reader.IsEmptyElement)
                {
                    string msg = StringTable.BoolVectorTunableInnerXmlError;
                    log.Warning(msg, line, pos);
                }

                int found = 0;
                for (int i = 0; i < reader.AttributeCount; i++)
                {
                    reader.MoveToAttribute(i);
                    string name = reader.LocalName;
                    string value = reader.Value;

                    if (String.Equals(name, "x"))
                    {
                        var result = this.Dictionary.TryTo<bool>(value, this._values[0]);
                        if (!result.Success)
                        {
                            string msg = StringTable.BoolVectorAttributeParseError;
                            log.Warning(msg, "x", value, line, pos);
                        }

                        this._values[0] = result.Value;
                        found++;
                    }
                    else if (String.Equals(name, "y"))
                    {
                        var result = this.Dictionary.TryTo<bool>(value, this._values[1]);
                        if (!result.Success)
                        {
                            string msg = StringTable.BoolVectorAttributeParseError;
                            log.Warning(msg, "y", value, line, pos);
                        }

                        this._values[1] = result.Value;
                        found++;
                    }
                    else if (String.Equals(name, "z"))
                    {
                        var result = this.Dictionary.TryTo<bool>(value, this._values[2]);
                        if (!result.Success)
                        {
                            string msg = StringTable.BoolVectorAttributeParseError;
                            log.Warning(msg, "z", value, line, pos);
                        }

                        this._values[2] = result.Value;
                        found++;
                    }
                    else if (String.Equals(name, "w"))
                    {
                        var result = this.Dictionary.TryTo<bool>(value, this._values[3]);
                        if (!result.Success)
                        {
                            string msg = StringTable.BoolVectorAttributeParseError;
                            log.Warning(msg, "w", value, line, pos);
                        }

                        this._values[3] = result.Value;
                        found++;
                    }
                    else
                    {
                        string msg = StringTable.BoolVectorUnrecognisedAttributeError;
                        log.Warning(msg, line, pos);
                    }
                }

                if (found != 4)
                {
                    string msg = StringTable.BoolVectorTunableDataMissingError;
                    log.Warning(msg, line, pos);
                }
            }
            catch (Exception ex)
            {
                string msg = StringTable.BoolVectorTunableDeserialiseError;
                throw new MetadataException(msg, line, pos, ex.Message);
            }

            reader.Skip();
        }

        /// <summary>
        /// Called whenever the value of the inheritance parent changes so that the values can
        /// be kept in sync.
        /// </summary>
        /// <param name="sender">
        /// The inheritance parent.
        /// </param>
        /// <param name="e">
        /// The event data.
        /// </param>
        private void OnInheritedValueChanged(object sender, PropertyChangedEventArgs e)
        {
            if (!this.HasDefaultValue)
            {
                return;
            }

            BoolVectorTunable source = this.InheritanceParent as BoolVectorTunable;
            if (source != null)
            {
                this._values[0] = source.X;
                this.NotifyPropertyChanged("X");

                this._values[1] = source.Y;
                this.NotifyPropertyChanged("Y");

                this._values[2] = source.Z;
                this.NotifyPropertyChanged("Z");

                this._values[3] = source.W;
                this.NotifyPropertyChanged("W");
            }
        }
        #endregion Methods
    } // RSG.Metadata.Model.Tunables.BoolVectorTunable {Class}
} // RSG.Metadata.Model.Tunables {Namespace}
