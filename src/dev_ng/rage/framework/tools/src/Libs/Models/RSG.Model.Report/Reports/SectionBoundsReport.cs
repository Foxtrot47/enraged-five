﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using RSG.Base.Logging;
using System.IO;
using System.Web.UI;
using RSG.Model.Map;
using RSG.Model.Common;
using RSG.Base.Math;
using RSG.Bounds;
using RSG.Base.ConfigParser;
using Ionic.Zip;
using RSG.SceneXml;
using RSG.Model.Common.Map;

namespace RSG.Model.Report.Reports
{
    /// <summary>
    /// 
    /// </summary>
    public class SectionBoundsReport : HTMLReport, IDynamicMapReport
    {
        #region Constants
        private const String NAME = "Map Section Bounds Report";
        private const String DESC = "Generate and display information about map section's bounding boxes.";
        private const int    DEFAULT_MAX_BND_FILESIZE = 512;
        private const Single DEFAULT_MINIMUM_DIMENSION_SIZE = 0.0199f;
        #endregion // Constants

        #region Types
        /// <summary>
        /// 
        /// </summary>
        protected class CollisionBoundInfo
        {
            public string CollisionName { get; private set; }
            public CollisionType CollisionFlags { get; private set; }
            public BNDFile BoundsFile { get; private set; }

            public CollisionBoundInfo(string name, CollisionType flags, BNDFile bndFile)
            {
                CollisionName = name;
                CollisionFlags = flags;
                BoundsFile = bndFile;
            }
        }

        /// <summary>
        /// Class to hold information about a single small bound
        /// </summary>
        protected class SmallBoundInfo
        {
            public string ObjectName { get; private set; }
            public CollisionType CollisionFlags { get; private set; }
            public Vector3f BoundDimensions { get; private set; }

            public SmallBoundInfo(string objectName, CollisionType flags, Vector3f dimensions)
            {
                ObjectName = objectName;
                CollisionFlags = flags;
                BoundDimensions = dimensions;
            }
        }
        #endregion // Types

        #region Properties
        /// <summary>
        /// Determines whether the map data should be loaded
        /// </summary>
        public bool LoadMapData
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Determines whether the car generators should be loaded
        /// 
        /// </summary>
        public bool LoadCarGenarators
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int MaxBNDFileSize
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public float MinDimensionSize
        {
            get;
            set;
        }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public SectionBoundsReport()
            : base(NAME, DESC)
        {
            MaxBNDFileSize = DEFAULT_MAX_BND_FILESIZE;
            MinDimensionSize = DEFAULT_MINIMUM_DIMENSION_SIZE;
        }
        #endregion // Constructor(s)
        
        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        /// <param name="gv"></param>
        public virtual void Generate(ILevel level, Base.ConfigParser.ConfigGameView gv, IPlatformCollection platforms)
        {
            DateTime start = DateTime.Now;

            // Data structure to track all small bounds
            Dictionary<MapSection, List<SmallBoundInfo>> smallBoundsMap = SearchForSmallBounds(level);

            // Generate the report based on the information we retrieved
            Stream = WriteReport(smallBoundsMap);

            Log.Log__Message("Map definition bounds report generation complete. (report took {0} milliseconds to create.)", (DateTime.Now - start).TotalMilliseconds);
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Processes the entire level looking for bounds that are smaller than a certain size, returning
        /// a map of map section to small bounds list
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        private Dictionary<MapSection, List<SmallBoundInfo>> SearchForSmallBounds(ILevel level)
        {
            Dictionary<MapSection, List<SmallBoundInfo>> smallBoundsMap = new Dictionary<MapSection, List<SmallBoundInfo>>();

            // Check all the sections
            foreach (MapSection section in RSG.Model.Map.Util.Enumerations.GetAllMapSections(level, null, true))
            {
                List<CollisionBoundInfo> boundFiles = new List<CollisionBoundInfo>();

                // Get the node that we actually want to process
                ContentNodeMap mapNode = section.MapNode;

                foreach (ContentNode exportNode in mapNode.Outputs)
                {
                    ContentNodeMapZip nodeZip = exportNode as ContentNodeMapZip;
                    if (nodeZip != null)
                    {
                        // Open up the scene xml file (for retrieving collision type flags)
                        string sceneXmlFilename = Path.ChangeExtension(nodeZip.Filename, "xml");

                        Scene scene = null;
                        if (File.Exists(sceneXmlFilename))
                        {
                            scene = new Scene(sceneXmlFilename, LoadOptions.All, true);
                        }

                        if (scene != null && File.Exists(nodeZip.Filename))
                        {
                            boundFiles.AddRange(LoadBoundsDataFromMapZipFile(nodeZip.Name, nodeZip.Filename, scene));
                        }
                    }
                }

                // We now have a list of all the bndfiles, so check them out for box primitives with small bounds
                List<SmallBoundInfo> smallBounds = new List<SmallBoundInfo>();

                foreach (CollisionBoundInfo boundInfo in boundFiles)
                {
                    ProcessBoundObject(boundInfo.CollisionName, boundInfo.CollisionFlags, boundInfo.BoundsFile.BoundObject, ref smallBounds);
                }

                if (smallBounds.Count > 0)
                {
                    smallBoundsMap.Add(section, smallBounds);
                }
            }

            return smallBoundsMap;
        }

        /// <summary>
        /// Processes an export zip file to get a list of BND files
        /// </summary>
        /// <param name="sectionName"></param>
        /// <param name="zipFileName"></param>
        /// <returns></returns>
        protected IEnumerable<CollisionBoundInfo> LoadBoundsDataFromMapZipFile(string sectionName, string zipFileName, Scene scene)
        {
            // Get the list of bound files
            using (ZipFile zipFile = ZipFile.Read(zipFileName))
            {
                // Get the item that contains the bounds information
                ZipEntry entry = zipFile.FirstOrDefault(item => (item.FileName.EndsWith(".ibr.zip") || item.FileName.EndsWith(".ibn.zip") || item.FileName.EndsWith("collision.zip")));

                if (entry != null)
                {
                    using (MemoryStream zipStream = new MemoryStream())
                    {
                        entry.Extract(zipStream);
                        zipStream.Position = 0;

                        foreach (CollisionBoundInfo info in LoadBoundsDataFromBoundsZip(zipStream, scene))
                        {
                            yield return info;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Processes a ibn.zip or ibr.zip file to extract a list of bnd files
        /// </summary>
        /// <param name="zipStream"></param>
        /// <returns></returns>
        private IEnumerable<CollisionBoundInfo> LoadBoundsDataFromBoundsZip(Stream zipStream, Scene scene)
        {
            using (ZipFile zipFile = ZipFile.Read(zipStream))
            {
                // Iterate over all bnd files that are less than MAX_BND_FILESIZE.  The filesize limit is to stop
                // us needing to process files that aren't going to contain data we are looking for.
                IEnumerable<ZipEntry> bndEntries = zipFile.Where(item => (item.FileName.EndsWith(".bnd") && item.UncompressedSize < MaxBNDFileSize));
                foreach (ZipEntry entry in bndEntries)
                {
                    // Check whether the corresponding collision object in the scenexml scene for this bound file
                    // has mover or horse collision flags set on it.
                    string name = Path.GetFileNameWithoutExtension(entry.FileName);
                    ObjectDef objDef = scene.FindObject(name);

                    if (objDef != null)
                    {
                        CollisionType collisionFlags = GetCollisionFlagsForObject(objDef);

                        if ((collisionFlags & CollisionType.Mover) != 0 ||
                            (collisionFlags & CollisionType.Mover) != 0)
                        {
                            using (MemoryStream fileStream = new MemoryStream())
                            {
                                entry.Extract(fileStream);
                                fileStream.Position = 0;

                                yield return new CollisionBoundInfo(name, collisionFlags, BNDFile.Load(fileStream));
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sceneObject"></param>
        /// <returns></returns>
        private CollisionType GetCollisionFlagsForObject(ObjectDef sceneObject)
        {
            CollisionType flags = 0;

            // Process the rest of the collision types
            foreach (CollisionType type in Enum.GetValues(typeof(CollisionType)))
            {
                bool defaultValue = type.IsSetByDefault();

                bool isFlagSet = false;
                if (!Boolean.TryParse(sceneObject.GetUserProperty(type.GetUserPropertyName(), defaultValue.ToString()), out isFlagSet))
                {
                    isFlagSet = defaultValue;
                }

                if (isFlagSet)
                {
                    flags |= type;
                }
            }

            return flags;
        }

        /// <summary>
        /// Processes an individual bound object
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="obj"></param>
        /// <param name="smallBounds"></param>
        protected void ProcessBoundObject(string objectName, CollisionType collisionFlags, BoundObject obj, ref List<SmallBoundInfo> smallBounds)
        {
            if (obj is Composite)
            {
                Composite composite = (Composite)obj;

                foreach (CompositeChild child in composite.Children)
                {
                    ProcessBoundObject(objectName, collisionFlags, child.BoundObject, ref smallBounds);
                }
            }
            else
            {
                // Convert the bound object to a BVH and then process
                BVH bvh = BoundObjectConverter.ToBVH(obj);
                ProcessBVH(objectName, collisionFlags, bvh, ref smallBounds);
            }
        }

        /// <summary>
        /// Processes a BVH, checking all boxes for small bounds
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="bvh"></param>
        /// <param name="smallBounds"></param>
        private void ProcessBVH(string objectName, CollisionType collisionFlags, BVH bvh, ref List<SmallBoundInfo> smallBounds)
        {
            List<BVHMaterial> materials = bvh.Materials.Select(item => new BVHMaterial(item)).ToList();

            foreach (BVHPrimitive prim in bvh.Primitives)
            {
                if (prim is BVHBox)
                {
                    BVHBox box = (BVHBox)prim;

                    // Get the size of the box
                    Vector3f twiceBoxX = bvh.Verts[box.Index0] + bvh.Verts[box.Index2] - bvh.Verts[box.Index1] - bvh.Verts[box.Index3];
                    Vector3f twiceBoxY = bvh.Verts[box.Index0] + bvh.Verts[box.Index1] - bvh.Verts[box.Index2] - bvh.Verts[box.Index3];
                    Vector3f twiceBoxZ = bvh.Verts[box.Index1] + bvh.Verts[box.Index2] - bvh.Verts[box.Index0] - bvh.Verts[box.Index3];
                    Vector3f boxSize = new Vector3f((float)twiceBoxX.Magnitude() * 0.5f, (float)twiceBoxY.Magnitude() * 0.5f, (float)twiceBoxZ.Magnitude() * 0.5f);

                    if (boxSize.X < MinDimensionSize ||
                        boxSize.Y < MinDimensionSize ||
                        boxSize.Z < MinDimensionSize)
                    {
                        smallBounds.Add(new SmallBoundInfo(objectName, collisionFlags, boxSize));
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="smallBoundsMap"></param>
        /// <returns></returns>
        protected Stream WriteReport(Dictionary<MapSection, List<SmallBoundInfo>> smallBoundsMap)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter reportFile = new StreamWriter(stream);

            HtmlTextWriter writer = new HtmlTextWriter(reportFile);
            writer.RenderBeginTag(HtmlTextWriterTag.Html);
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Head);
                writer.RenderEndTag();

                writer.RenderBeginTag(HtmlTextWriterTag.Body);
                {
                    WriteHeader(writer, "Small Bounds Report");

                    WriteSubHeader(writer, "Summary");
                    OutputSummary(writer, smallBoundsMap);

                    if (smallBoundsMap.Count > 0)
                    {
                        WriteSubHeader(writer, "Detailed Information");

                        foreach (KeyValuePair<MapSection, List<SmallBoundInfo>> pair in smallBoundsMap)
                        {
                            OutputDefinitionInformation(writer, pair.Key, pair.Value);
                        }
                    }
                }
                writer.RenderEndTag();
            }
            writer.RenderEndTag();
            writer.Flush();

            stream.Position = 0;
            return stream;
        }
        
        /// <summary>
        /// Outputs a summary of the information we found
        /// </summary>
        /// <param name="writer"></param>
        private void OutputSummary(HtmlTextWriter writer, Dictionary<MapSection, List<SmallBoundInfo>> smallBounds)
        {
            if (smallBounds.Count == 0)
            {
                WriteParagraph(writer, String.Format("Congratulations, none of the map sections have bounds smaller than {0} meters.", MinDimensionSize));
            }
            else
            {
                // Determine the total number of small bounds
                int totalCount = 0;
                foreach (KeyValuePair<MapSection, List<SmallBoundInfo>> pair in smallBounds)
                {
                    totalCount += pair.Value.Count();
                }

                // Output the summary
                string summaryText = String.Format("{0} sections have a total of {1} collision boxes with one (or more) bound dimension less than {2} meters.",
                                                   smallBounds.Count, totalCount, MinDimensionSize);
                WriteParagraph(writer, summaryText);

                // Output each of the props we found and and link to the detailed information
                foreach (KeyValuePair<MapSection, List<SmallBoundInfo>> pair in smallBounds)
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Href, String.Format("#{0}", pair.Key.Name));
                    writer.RenderBeginTag(HtmlTextWriterTag.A);
                    writer.Write(String.Format("{0} ({1} small bounds)", pair.Key.Name, pair.Value.Count));
                    writer.RenderEndTag();
                    writer.WriteBreak();
                }
            }
        }

        /// <summary>
        /// Outputs information regarding a single definition
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="propDef"></param>
        private void OutputDefinitionInformation(HtmlTextWriter writer, MapSection section, List<SmallBoundInfo> smallBounds)
        {
            // <a name="sectionName"><b>Section Name</b></a>
            writer.AddAttribute(HtmlTextWriterAttribute.Name, section.Name);
            writer.RenderBeginTag(HtmlTextWriterTag.A);
            writer.RenderBeginTag(HtmlTextWriterTag.B);
            writer.Write(section.Name);
            writer.RenderEndTag();
            writer.RenderEndTag();

            // Output section parent info??
            /*
            IMapContainer parent = section.Container;
            IMapContainer grandParent = null;
            if (parent is MapArea)
            {
                grandParent = (parent as MapArea).Container;
            }
            */

            // <table>
            //   <tr><td><I>Collision Object Name</I></td><td><I>Bounds</I></td></tr>
            //   <tr><td>...</td><td>...</td></tr>
            //   ....
            // </table>
            writer.RenderBeginTag(HtmlTextWriterTag.Table);

            // Header Row
            writer.RenderBeginTag(HtmlTextWriterTag.Tr);
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.RenderBeginTag(HtmlTextWriterTag.I);
            writer.Write("Collision Object Name");
            writer.RenderEndTag();
            writer.RenderEndTag();
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.RenderBeginTag(HtmlTextWriterTag.I);
            writer.Write("Collision Flags");
            writer.RenderEndTag();
            writer.RenderEndTag();
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.RenderBeginTag(HtmlTextWriterTag.I);
            writer.Write("Bounds Dimensions");
            writer.RenderEndTag();
            writer.RenderEndTag();
            writer.RenderEndTag();

            foreach (SmallBoundInfo boundInfo in smallBounds)
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write(boundInfo.ObjectName);
                writer.RenderEndTag();
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write(String.Join(" + ", boundInfo.CollisionFlags.GetDisplayNames().ToArray()));
                writer.RenderEndTag();
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write(boundInfo.BoundDimensions);
                writer.RenderEndTag();
                writer.RenderEndTag();
            }

            writer.RenderEndTag();
            writer.WriteBreak();
        }
        #endregion // Private Methods
    } // MapDefinitionBoundsReport
} // RSG.Model.Report.Reports
