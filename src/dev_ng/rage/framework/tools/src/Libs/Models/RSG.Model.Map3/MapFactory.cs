﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common.Map;
using RSG.Model.Common;
using RSG.Base.ConfigParser;
using RSG.Statistics.Common.Dto.GameAssets;
using RSG.Model.Common.Extensions;
using RSG.Base.Logging;
using RSG.Base.Math;
using System.IO;
using System.Xml;
using System.Diagnostics;
using RSG.Statistics.Client.ServiceClients;
using RSG.Statistics.Common.Dto.GameAssets.Bundles;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Configuration;
using RSG.Base.Configuration.Bugstar;
using RSG.Interop.Bugstar;
using RSG.Interop.Bugstar.Game;
using RSG.Interop.Bugstar.Organisation;

namespace RSG.Model.Map3
{
    /// <summary>
    /// 
    /// </summary>
    public class MapFactory : IMapFactory
    {
        #region Properties
        /// <summary>
        /// Streaming coordinator
        /// </summary>
        public IStatStreamingCoordinator StreamingCoordinator
        {
            get
            {
                return m_streamingCoordinator;
            }
        }
        private MapStatStreamingCoordinator m_streamingCoordinator;

        /// <summary>
        /// Personal game view
        /// </summary>
        private ConfigGameView GameView { get; set; }

        /// <summary>
        /// Configuration object.
        /// </summary>
        private IConfig Config { get; set; }

        /// <summary>
        /// Bugstar configuration object.
        /// </summary>
        private IBugstarConfig BugstarConfig { get; set; }

        /// <summary>
        /// 
        /// </summary>
        private BugstarConnection BugstarConnection { get; set; }
        #endregion
       
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public MapFactory(IConfig config, IBugstarConfig bugstarConfig, BugstarConnection connection)
        {
            GameView = new ConfigGameView();
            Config = config;
            BugstarConfig = bugstarConfig;
            BugstarConnection = connection;
            m_streamingCoordinator = new MapStatStreamingCoordinator(GameView);
            m_streamingCoordinator.StartProcessingTasks();
        }
        #endregion // Constructor(s)

        #region IMapFactory Implementation
        /// <summary>
        /// Creates a new map hierarchy from the specified source and for a particular level.
        /// </summary>
        /// <param name="source">Source to use for creating the collection.</param>
        /// <param name="level">Level to create the hierarchy for.</param>
        /// <param name="buildIdentifier">Optional build identifier for when the source is Database.</param>
        /// <returns></returns>
        public IMapHierarchy CreateHierarchy(DataSource source, ILevel level, String buildIdentifier = null)
        {
            Debug.Assert(level != null, "Level is null.  A level must be provided.");
            IMapHierarchy hierarchy = null;

            if (source == DataSource.SceneXml)
            {
                Log.Log__Profile("{0} create map hierarchy from export data.", level.Name);
                hierarchy = CreateHierarchyFromExportData(level);
                Log.Log__ProfileEnd();
            }
            else if (source == DataSource.Database)
            {
                Log.Log__Profile("{0} create map hierarchy from database.", level.Name);
                hierarchy = CreateHierarchyFromDatabase(level, buildIdentifier);
                Log.Log__ProfileEnd();
            }
            else
            {
                Debug.Assert(false, String.Format("Unknown data source '{0}' specified while creating a map hierarchy.", source.ToString()));
                throw new ArgumentException(String.Format("Unknown data source '{0}' specified while creating a map hierarchy.", source.ToString()));
            }

            return hierarchy;
        }
        #endregion // IMapFactory Implementation

        #region Private Methods
        #region SceneXml Creation
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private IMapHierarchy CreateHierarchyFromExportData(ILevel level)
        {
            IMapHierarchy hierarchy = new ExportDataMapHierarchy(level, m_streamingCoordinator, GameView, Config);

            ContentNodeLevel levelNode = level.GetConfigLevelNode(GameView);
            if (levelNode != null)
            {
                Log.Log__Profile("create hierarchy {0}", level.Name);
                CreateHierarchyFromExportData(hierarchy, levelNode.Children);
                Log.Log__ProfileEnd();

                // Retrieve the bugstar vector map points
                Log.Log__Profile("vector map {0}", level.Name);
                PopulateVectorMapPoints(hierarchy, level);
                Log.Log__ProfileEnd();

                // Compute which sections are neighbours of which
                Log.Log__Profile("generate neighbours {0}", level.Name);
                GenerateSectionNeighbours(hierarchy);
                Log.Log__ProfileEnd();

                // Link up the sections with their prop groups
                Log.Log__Profile("link containers {0}", level.Name);
                LinkContainersWithPropGroups(hierarchy);
                Log.Log__ProfileEnd();
            }

            return hierarchy;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hierarchy"></param>
        /// <param name="nodes"></param>
        private void CreateHierarchyFromExportData(IMapHierarchy hierarchy, ContentNode[] nodes)
        {
            foreach (ContentNode node in nodes)
            {
                if (node is ContentNodeMapZip)
                {
                    // There should be only 1 input which is the ContentNodeMap for this section
                    if (node.Inputs.Length > 0 && node.Inputs[0] is ContentNodeMap)
                    {
                        ContentNodeMapZip mapzip = (ContentNodeMapZip)node;
                        ContentNodeMap map = (ContentNodeMap)node.Inputs[0];

                        MapSection newSection = new MapSection(map.Name, ConvertMapSectionType(map.MapType), hierarchy);
                        hierarchy.ChildNodes.Add(newSection);
                    }
                }
                else if (node is ContentNodeGroup)
                {
                    ContentNodeGroup areaNode = (ContentNodeGroup)node;

                    if (!String.IsNullOrEmpty(areaNode.RawRelativePath))
                    {
                        MapArea newArea = new MapArea(areaNode.RawRelativePath, hierarchy);
                        hierarchy.ChildNodes.Add(newArea);
                        CreateHierarchyFromExportData(newArea, areaNode.Children);
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="nodes"></param>
        private void CreateHierarchyFromExportData(IMapArea parent, ContentNode[] nodes)
        {
            foreach (ContentNode node in nodes)
            {
                if (node is ContentNodeMapZip)
                {
                    // There should be only 1 input which is the ContentNodeMap for this section
                    if (node.Inputs.Length > 0 && node.Inputs[0] is ContentNodeMap)
                    {
                        ContentNodeMapZip mapzip = (ContentNodeMapZip)node;
                        ContentNodeMap map = (ContentNodeMap)node.Inputs[0];

                        MapSection newSection = new MapSection(map.Name, ConvertMapSectionType(map.MapType), parent);
                        parent.ChildNodes.Add(newSection);
                    }
                }
                else if (node is ContentNodeGroup)
                {
                    ContentNodeGroup areaNode = (ContentNodeGroup)node;

                    if (!String.IsNullOrEmpty(areaNode.RawRelativePath))
                    {
                        MapArea newArea = new MapArea(areaNode.RawRelativePath, parent);
                        parent.ChildNodes.Add(newArea);
                        CreateHierarchyFromExportData(newArea, areaNode.Children);
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private SectionType ConvertMapSectionType(ContentNodeMap.MapSubType type)
        {
            switch (type)
            {
                case ContentNodeMap.MapSubType.Container:
                    return SectionType.Container;

                case ContentNodeMap.MapSubType.ContainerProps:
                    return SectionType.ContainerProp;

                case ContentNodeMap.MapSubType.Props:
                    return SectionType.Prop;

                case ContentNodeMap.MapSubType.Interior:
                    return SectionType.Interior;

                case ContentNodeMap.MapSubType.ContainerLod:
                    return SectionType.ContainerLod;

                case ContentNodeMap.MapSubType.ContainerOccl:
                    return SectionType.ContainerOcclusion;

                default:
                    //throw new ArgumentException(String.Format("Unknown ContentNodeMap.MapSubType encountered ('{0}')", type));
                    return SectionType.Unknown;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hierarchy"></param>
        private void PopulateVectorMapPoints(IMapHierarchy hierarchy, ILevel level)
        {
            // Check if an entry exists for this level in the game config
            uint levelId;
            if (BugstarConfig.TryGetLevelId(level.Name, out levelId))
            {
                // Query bugstar for the background image/bounds
                bool dataRetrieved = false;

                try
                {
                    Project project = Project.GetProjectById(BugstarConnection, BugstarConfig.ProjectId);
                    if (project != null)
                    {
                        Map bugstarMap = Map.GetMapById(project, levelId);
                        if (bugstarMap != null)
                        {
                            IDictionary<string, MapGrid> bugstarGrids = bugstarMap.Grids.Where(item => item.Active).ToDictionary(item => item.Name.ToLower());

                            foreach (IMapSection section in hierarchy.AllSections)
                            {
                                if (bugstarGrids.ContainsKey(section.Name.ToLower()))
                                {
                                    section.VectorMapPoints = bugstarGrids[section.Name.ToLower()].Points;
                                }
                                else
                                {
                                    section.VectorMapPoints = new List<Vector2f>();
                                }
                            }

                            // Save the information out to the local cache
                            SaveVectorMapToCache(hierarchy, GameView.ToolsCacheDir);
                            dataRetrieved = true;
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    Log.Log__Exception(ex, "Unhandled exception during Bugstar connection.");
                }

                // Check if we managed to retrieve the image
                if (!dataRetrieved)
                {
                    // Bugstar appears to be down (or there was some other problem).  Attempt to get the information from the cache instead.
                    LoadVectorMapFromCache(hierarchy, Config.Project.Cache);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hierarchy"></param>
        /// <param name="cacheDir"></param>
        private void SaveVectorMapToCache(IMapHierarchy hierarchy, string cacheDir)
        {
            // Save out the vector map
            string vectorMapPath = GetCachedVectorMapFilePath(cacheDir, hierarchy.OwningLevel.Name);
            string vectorMapDir = Path.GetDirectoryName(vectorMapPath);
            if (!Directory.Exists(vectorMapDir))
            {
                Directory.CreateDirectory(vectorMapDir);
            }

            XmlWriter writer = XmlWriter.Create(vectorMapPath);
            writer.WriteStartElement("vectormap");

            foreach (IMapSection section in hierarchy.AllSections.Where(item => item.VectorMapPoints != null))
            {
                writer.WriteStartElement("section");
                writer.WriteAttributeString("name", section.Name);

                foreach (Vector2f point in section.VectorMapPoints)
                {
                    writer.WriteStartElement("point");
                    writer.WriteAttributeString("x", point.X.ToString());
                    writer.WriteAttributeString("y", point.Y.ToString());
                    writer.WriteEndElement();
                }

                writer.WriteEndElement();
            }

            writer.WriteEndElement();
            writer.Close();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hierarchy"></param>
        /// <param name="cacheDir"></param>
        private void LoadVectorMapFromCache(IMapHierarchy hierarchy, string cacheDir)
        {
            // Load in the vector map information from the cache
            string vectorMapPath = GetCachedVectorMapFilePath(cacheDir, hierarchy.OwningLevel.Name);

            if (File.Exists(vectorMapPath))
            {
                // Read in the data
                XmlDocument doc = new XmlDocument();
                doc.Load(vectorMapPath);

                XmlNodeList sectionNodes = doc.SelectNodes("/vectormap/section");

                IDictionary<string, IList<Vector2f>> vectorMap = new Dictionary<string, IList<Vector2f>>();
                foreach (XmlNode sectionNode in sectionNodes)
                {
                    XmlAttribute nameAttribute = sectionNode.Attributes["name"];
                    XmlNodeList pointNodes = sectionNode.SelectNodes("point");
                    IList<Vector2f> points = new List<Vector2f>();

                    foreach (XmlNode pointNode in pointNodes)
                    {
                        XmlAttribute xAttribute = pointNode.Attributes["x"];
                        XmlAttribute yAttribute = pointNode.Attributes["y"];
                        points.Add(new Vector2f(Single.Parse(xAttribute.Value), Single.Parse(yAttribute.Value)));
                    }

                    vectorMap.Add(nameAttribute.InnerText, points);
                }

                // Set up the map section vector map points
                foreach (IMapSection section in hierarchy.AllSections)
                {
                    if (vectorMap.ContainsKey(section.Name.ToLower()))
                    {
                        section.VectorMapPoints = vectorMap[section.Name.ToLower()];
                    }
                    else
                    {
                        section.VectorMapPoints = null;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cacheDir"></param>
        /// <returns></returns>
        private string GetCachedVectorMapFilePath(string cacheDir, string levelName)
        {
            return Path.Combine(cacheDir, "Bugstar", "Map", levelName, "vector_map.xml");
        }
        #endregion // SceneXml Creation

        #region Database Creation
        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        /// <param name="buildIdentifier"></param>
        /// <returns></returns>
        private IMapHierarchy CreateHierarchyFromDatabase(ILevel level, string buildIdentifier)
        {
            IMapHierarchy hierarchy = null;
            BuildClient buildClient = new BuildClient();

            try
            {
                // If no build identifier was supplied, use the latest build.
                if (buildIdentifier == null)
                {
                    BuildDto dto = buildClient.GetLatest();
                    if (dto != null)
                    {
                        buildIdentifier = dto.Identifier;
                    }
                }

                // Check that the identifier is set.
                if (buildIdentifier != null)
                {
                    Log.Log__Profile("retrieve hierarchy {0}", level.Name);
                    MapHierarchyBundleDto hierarchyBundle = buildClient.GetMapHierarchy(buildIdentifier, level.Hash.ToString());
                    Log.Log__ProfileEnd();

                    Log.Log__Profile("create hierarchy {0}", level.Name);
                    // Create and populate the hierarchy.
                    hierarchy = new SingleBuildMapHierarchy(level, m_streamingCoordinator, buildIdentifier);
                    CreateHierarchyFromDatabase(hierarchy, hierarchyBundle.ChildNodes);
                    Log.Log__ProfileEnd();

                    // Compute which sections are neighbours of which
                    Log.Log__Profile("generate neighbours {0}", level.Name);
                    GenerateSectionNeighbours(hierarchy);
                    Log.Log__ProfileEnd();

                    // Link up the sections with their prop groups
                    Log.Log__Profile("link containers {0}", level.Name);
                    LinkContainersWithPropGroups(hierarchy);
                    Log.Log__ProfileEnd();
                    
                }
            }
            catch (System.Exception ex)
            {
                Log.Log__Exception(ex, "Unhandled exception occurred while creating the map hierarchy for level '{0}' from the database.", level.Name);
            }
            finally
            {
                buildClient.Close();
            }

            return hierarchy;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hierarchy"></param>
        /// <param name="bundle"></param>
        private void CreateHierarchyFromDatabase(IMapHierarchy hierarchy, List<BasicMapNodeStatBaseDto> dtos)
        {
            foreach (BasicMapNodeStatBaseDto dto in dtos)
            {
                if (dto is BasicMapSectionStatDto)
                {
                    BasicMapSectionStatDto sectionDto = (BasicMapSectionStatDto)dto;

                    MapSection newSection = new MapSection(dto.Name, ConvertMapSectionType(sectionDto.Type), hierarchy);
                    newSection.VectorMapPoints = sectionDto.VectorMapPoints;
                    hierarchy.ChildNodes.Add(newSection);
                }
                else if (dto is BasicMapAreaStatDto)
                {
                    BasicMapAreaStatDto areaDto = (BasicMapAreaStatDto)dto;
                    
                    MapArea newArea = new MapArea(areaDto.Name, hierarchy);
                    hierarchy.ChildNodes.Add(newArea);
                    CreateHierarchyFromDatabase(newArea, areaDto.ChildNodes);
                }
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="hierarchy"></param>
        /// <param name="bundle"></param>
        private void CreateHierarchyFromDatabase(IMapArea parent, List<BasicMapNodeStatBaseDto> dtos)
        {
            foreach (BasicMapNodeStatBaseDto dto in dtos)
            {
                if (dto is BasicMapSectionStatDto)
                {
                    BasicMapSectionStatDto sectionDto = (BasicMapSectionStatDto)dto;

                    MapSection newSection = new MapSection(dto.Name, ConvertMapSectionType(sectionDto.Type), parent);
                    newSection.VectorMapPoints = sectionDto.VectorMapPoints;
                    parent.ChildNodes.Add(newSection);
                }
                else if (dto is BasicMapAreaStatDto)
                {
                    BasicMapAreaStatDto areaDto = (BasicMapAreaStatDto)dto;
                    
                    MapArea newArea = new MapArea(areaDto.Name, parent);
                    parent.ChildNodes.Add(newArea);
                    CreateHierarchyFromDatabase(newArea, areaDto.ChildNodes);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private SectionType ConvertMapSectionType(string typeString)
        {
            SectionType type;
            if (!Enum.TryParse(typeString, out type))
            {
                type = SectionType.Unknown;
            }
            return type;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vectorMapXml"></param>
        /// <returns></returns>
        private IList<Vector2f> ConvertDBVectorMapPoints(byte[] vectorMapXml)
        {
            IList<Vector2f> vectorMap = new List<Vector2f>();

            // Convert the byte data to a stream for reading via the xdocument below
            MemoryStream stream = new MemoryStream();
            BinaryWriter writer = new BinaryWriter(stream);
            writer.Write(vectorMapXml);
            stream.Position = 0;

            // Extract the data we are after from the stream.
            XDocument doc = XDocument.Load(stream);

            foreach (XElement elem in doc.XPathSelectElements("/vectormap/point"))
            {
                XAttribute xAtt = elem.Attribute("x");
                XAttribute yAtt = elem.Attribute("y");

                if (xAtt != null && yAtt != null)
                {
                    float xVal = Single.Parse(xAtt.Value);
                    float yVal = Single.Parse(yAtt.Value);
                    vectorMap.Add(new Vector2f(xVal, yVal));
                }
            }

            writer.Dispose();
            stream.Dispose();
            return (vectorMap.Any() ? vectorMap : null);
        }
        #endregion // Database Creation

        #region Common
        /// <summary>
        /// Calculates the neighbour lists for all the sections
        /// </summary>
        /// <param name="hierarchy"></param>
        private void GenerateSectionNeighbours(IMapHierarchy hierarchy)
        {
            foreach (IMapSection section in hierarchy.AllSections)
            {
                section.GenerateNeighbours();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hierarchy"></param>
        private void LinkContainersWithPropGroups(IMapHierarchy hierarchy)
        {
            IEnumerable<IMapSection> containers = hierarchy.AllSections.Where(section => section.Type == SectionType.Container);
            Dictionary<string, IMapSection> containerProps = hierarchy.AllSections.Where(section => section.Type == SectionType.ContainerProp)
                                                                               .ToDictionary(s => s.Name.ToLower().Replace("_props", ""));

            foreach (IMapSection container in containers)
            {
                if (containerProps.ContainsKey(container.Name))
                {
                    container.SetPropGroup(containerProps[container.Name]);
                }
            }
        }
        #endregion // Common
        #endregion // Private Methods

        #region IDisposable Implementation
        /// <summary>
        /// Dispose of the weapon streaming coordinator
        /// </summary>
        public void Dispose()
        {
            GameView.Dispose();
            StreamingCoordinator.Dispose();
        }
        #endregion // IDisposable Implementation
    } // MapFactory
}
