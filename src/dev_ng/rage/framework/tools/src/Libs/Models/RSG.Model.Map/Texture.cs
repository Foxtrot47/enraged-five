﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.XPath;
using RSG.SceneXml.Material;
using System.ComponentModel;
using RSG.Base.ConfigParser;
using RSG.Model.Map.Utilities;

namespace RSG.Model.Map
{
    public class Texture : INotifyPropertyChanged, ISearchable
    {
        #region Events

        /// <summary>
        /// Property changed event fired when the name changes so that the 
        /// binding can be dynamic.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Create the OnPropertyChanged method to raise the event
        /// </summary>
        /// <param name="name"></param>
        protected void OnPropertyChanged(String name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        #endregion

        #region Constants
        #region XPath Compiled Expressions
        private static readonly XPathExpression sC_s_xpath_XPathTextureAttrs =
            XPathExpression.Compile("@*");
        #endregion
        #region ParentTXD Attribute Names
        private static readonly String sC_s_s_TextureFileName = "filename";
        private static readonly String sC_s_s_TextureAlphaPath = "alpha";
        private static readonly String sC_s_s_TextureType = "type";
        #endregion // ParentTXD Attribute Names
        #endregion

        #region Properties and Associated Member Data

        public String Name
        {
            get { return m_sName; }
            set { m_sName = value; }
        }
        private String m_sName;

        public String Filename
        {
            get { return m_sFileName; }
            private set { m_sFileName = value; }
        }
        private String m_sFileName;

        public RSG.SceneXml.Material.TextureTypes Type
        {
            get { return m_type; }
        }
        private RSG.SceneXml.Material.TextureTypes m_type;

        public Boolean HasAlphaTexture
        {
            get { return m_sAlphaFilepath != null && m_sAlphaFilepath != String.Empty; }
        }
        public String AlphaFilepath
        {
            get { return m_sAlphaFilepath; }
            set { m_sAlphaFilepath = value; }
        }
        public String AlphaTextureName
        {
            get { return HasAlphaTexture ? System.IO.Path.GetFileNameWithoutExtension(m_sAlphaFilepath) : String.Empty; }
        }
        private String m_sAlphaFilepath;

        public String StreamName
        {
            get { return m_sStreamName; }
            set { m_sStreamName = value; }
        }
        private String m_sStreamName;

        public int StreamSize
        {
            get
            {
                if (m_streamSize == -1)
                {
                    DetermineStreamProperties();
                }
                return m_streamSize;
            }
            private set
            {
                m_streamSize = value;
            }
        }
        private int m_streamSize = -1;

        public String StreamFilename
        {
            get 
            {
                if (m_streamFilename == null)
                {
                    this.DetermineStreamProperties();
                }
                return m_streamFilename;
            }
            private set { m_streamFilename = value; }
        }
        private String m_streamFilename = null;

        /// <summary>
        /// The parent of this searchable object
        /// </summary>
        public ISearchable SearchableParent
        {
            get;
            set;
        }

        #endregion // Properties and Associated Member Data

        #region Constructor(s)

        /// <summary>
        /// Creates a texture with the given filename and parent texture dictionary
        /// </summary>
        public Texture(String filename, TextureTypes type)
        {
            this.m_sFileName = filename;
            this.m_sStreamName = System.IO.Path.GetFileNameWithoutExtension(filename);
            this.m_sName = m_sStreamName;// +" " + type.ToString();
            this.m_type = type;
            this.AlphaFilepath = String.Empty;
        }

        /// <summary>
        /// Creates a new texture with the given filename, alphaname and type.
        /// This constructor should be used if you already know that the texture has a alpha component
        /// and the filename for that component
        /// </summary>
        public Texture(String filename, String alphaname, TextureTypes type)
        {
            this.m_sFileName = filename;
            this.AlphaFilepath = alphaname;
            this.m_sStreamName = System.IO.Path.GetFileNameWithoutExtension(filename) + System.IO.Path.GetFileNameWithoutExtension(alphaname);
            this.m_sName = m_sStreamName;// +" " + type.ToString() + " with alpha " + System.IO.Path.GetFileNameWithoutExtension(alphaname);
            this.m_type = type;
        }

        #endregion // Constructor(s)

        #region Private Function(s)

        /// <summary>
        /// Deserialises the object from a xml file navigator
        /// </summary>
        private void Deserialise(XPathNavigator navigator)
        {
            // Get filename
            XPathNodeIterator dictionaryAttrIt = navigator.Select(sC_s_xpath_XPathTextureAttrs);
            while (dictionaryAttrIt.MoveNext())
            {
                if (typeof(String) != dictionaryAttrIt.Current.ValueType)
                    continue;
                String value = (dictionaryAttrIt.Current.TypedValue as String);

                if (dictionaryAttrIt.Current.Name == sC_s_s_TextureFileName)
                {
                    this.m_sFileName = value;
                    this.m_sStreamName = System.IO.Path.GetFileNameWithoutExtension(m_sFileName);
                    this.m_sName = m_sStreamName;
                }
                if (dictionaryAttrIt.Current.Name == sC_s_s_TextureAlphaPath)
                {
                    this.m_sAlphaFilepath = value;
                    if (this.m_sAlphaFilepath != null && this.m_sAlphaFilepath != String.Empty)
                    {
                        // we have a alpha texture and therefore the stream name is a combination of the two paths
                        this.m_sStreamName += System.IO.Path.GetFileNameWithoutExtension(m_sAlphaFilepath);
                        this.m_sName += (" " + "with alpha " + System.IO.Path.GetFileNameWithoutExtension(m_sAlphaFilepath));
                    }
                }
                if (dictionaryAttrIt.Current.Name == sC_s_s_TextureType)
                {
                    this.m_type = GetTypeFromString(value);
                    this.m_sName += (" " + m_type.ToString());
                }
            }
        }

        private void DetermineStreamProperties()
        {
            this.StreamSize = 0;
            this.StreamFilename = String.Empty;

            Boolean fileFound = false;
            String completePath = String.Empty;
            String location = System.IO.Path.Combine(LevelDictionary.TextureFolder, this.StreamName);
            if (System.IO.File.Exists(location + ".dds"))
            {
                fileFound = true;
                completePath = location + ".dds";
            }

            if (fileFound)
            {
                System.IO.FileInfo fileInfo = new System.IO.FileInfo(completePath);

                this.StreamSize = (int)fileInfo.Length;
                this.StreamFilename = completePath;
            }
            else
            {
                this.StreamFilename = "Not Found In Stream";
            }
        }

        #endregion

        #region Public Function(s)

        /// <summary>
        /// Serialises this texture out to the given xml document and appends the
        /// data onto the given parent node from the document
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="parentNode"></param>
        public void Serialise(XmlDocument xmlDoc, XmlElement parentNode)
        {
            XmlElement textureNode = xmlDoc.CreateElement("Texture");
            textureNode.SetAttribute("filename", this.Filename);
            textureNode.SetAttribute("type", this.Type.ToString());
            textureNode.SetAttribute("alpha", this.AlphaFilepath);
            parentNode.AppendChild(textureNode);
        }

        /// <summary>
        /// Gets a texture type from the type string got from the xml file
        /// </summary>
        /// <param name="typeString"></param>
        /// <returns></returns>
        public static TextureTypes GetTypeFromString(String typeString)
        {
            switch (typeString)
            {
                case "DiffuseMap":
                    return TextureTypes.DiffuseMap;
                case "BumpMap":
                    return TextureTypes.BumpMap;
                case "SpecularMap":
                    return TextureTypes.SpecularMap;
                case "DiffuseAlpha":
                    return TextureTypes.DiffuseAlpha;
                case "BumpAlpha":
                    return TextureTypes.BumpAlpha;
                case "SpecularAlpha":
                    return TextureTypes.SpecularAlpha;
                case "AmbientMap":
                    return TextureTypes.AmbientMap;
                case "SpecularLevelMap":
                    return TextureTypes.SpecularLevelMap;
                case "GlossMap":
                    return TextureTypes.GlossMap;
                case "SelfIlluminationMap":
                    return TextureTypes.SelfIlluminationMap;
                case "OpacityMap":
                    return TextureTypes.OpacityMap;
                case "FilterColourMap":
                    return TextureTypes.FilterColourMap;
                case "ReflectionMap":
                    return TextureTypes.ReflectionMap;
                case "RefractionMap":
                    return TextureTypes.RefractionMap;
                case "DisplacementMap":
                    return TextureTypes.DisplacementMap;
                default:
                    return TextureTypes.None;
            }
        }

        #endregion

        #region Walk Properties

        /// <summary>
        /// Walks through the children of ISearchables, going
        /// depth first
        /// </summary>
        public IEnumerable<ISearchable> WalkSearchDepth
        {
            get
            {
                yield return this;
            }
        }

        #endregion // Walk Properties

    } // Texture
} // RSG.Model.Map
