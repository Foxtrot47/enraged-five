﻿using System;
using System.IO;
using System.Windows;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.XPath;
using RSG.Base.Collections;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using RSG.Base.Logging;
using RSG.Model.Asset;
using RSG.Model.Common;
using System.Diagnostics;

namespace RSG.Model.GlobalTXD
{
    public enum ValidationOptions
    {
        Validate,
        DontValidate
    }

    public enum ValidationDirection
    {
        Up,
        Down,
        Both,
    }

    /// <summary>
    /// A global dictionary root is a dummy root node that the user can add
    /// global texture dictionaries to.
    /// </summary>
    public class GlobalRoot : ModelBase, IEnumerable<GlobalTextureDictionary>, IDictionaryContainer
    {
        #region Constants

        private const String EXPORT_GTXD_SCRIPT = "%RS_TOOLSROOT%\\tmp\\data_mk_global_txd_rpf.rb";
        private const String EXPORT_COMMAND = "%RS_TOOLSROOT%\\bin\\ruby\\bin\\ruby.exe";

        #region XPath Compiled Expressions

        private static readonly XPathExpression sC_s_xpath_XPathRootDictionariesOld = XPathExpression.Compile("/Global_Dictionaries/UserDictionary");
        private static readonly XPathExpression sC_s_xpath_XPathRootDictionaries = XPathExpression.Compile("/globalDictionaries/globalDictionary");

        #endregion // XPath Compiled Expressions

        #endregion

        #region Properties
        /// <summary>
        /// If this is true it means that source texture dictionaries can be added to
        /// this container as children
        /// </summary>
        public Boolean CanAcceptSourceDictionaries
        { 
            get; private set;
        }

        /// <summary>
        /// If this is true it means that global texture dictionaries can be added to
        /// this container as children
        /// </summary>
        public Boolean CanAcceptGlobalDictionaries
        { 
            get; private set;
        }

        /// <summary>
        /// If this flag is set it means that none of the source txds/textures are loaded.
        /// </summary>
        public Boolean LoadSourceTextures
        {
            get; private set;
        }

        /// <summary>
        /// A dictionary of uniquely named texture dictionaries that are children of this global root
        /// The dictionaries are indexed by names and these names need to be unique.
        /// </summary>
        public ObservableDictionary<String, GlobalTextureDictionary> GlobalTextureDictionaries
        {
            get { return m_globalTextureDictionaries; }
            private set
            {
                this.SetPropertyValue(
                    ref this.m_globalTextureDictionaries, value, "GlobalTextureDictionaries");
            }
        }
        private ObservableDictionary<String, GlobalTextureDictionary> m_globalTextureDictionaries;

        /// <summary>
        /// A dictionary of uniquely named source texture dictionaries that are children of this global root
        /// The dictionaries are indexed by names and these names need to be unique.
        /// </summary>
        public ObservableDictionary<String, SourceTextureDictionary> SourceTextureDictionaries
        {
            get { return m_sourceTextureDictionaries; }
            private set
            {
                this.SetPropertyValue(
                    ref this.m_sourceTextureDictionaries, value, "SourceTextureDictionaries");
            }
        }
        private ObservableDictionary<String, SourceTextureDictionary> m_sourceTextureDictionaries;

        /// <summary>
        /// Returns the texture dictionary with the given name if it exists and null otherwise
        /// </summary>
        public GlobalTextureDictionary this[String name]
        {
            get
            {
                GlobalTextureDictionary dictionary = null;
                this.GlobalTextureDictionaries.TryGetValue(name, out dictionary);
                return dictionary;
            }
        }

        /// <summary>
        /// The name of the file that this root is currently associated to
        /// </summary>
        public String Filename
        {
            get;
            set;
        }

        /// <summary>
        /// The name given to this global root
        /// </summary>
        public String Name
        {
            get { return "Root"; }
        }

        /// <summary>
        /// The parent global dictionary if it has one
        /// </summary>
        public IDictionaryContainer Parent
        {
            get { return null; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string TextureLocation
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, List<ITexture>> SourceTXDs
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public GlobalRoot()
            : this(null, null)
        {

        }

        /// <summary>
        /// Deserialisation constructor
        /// </summary>
        public GlobalRoot(String filename, Dictionary<string, List<ITexture>> txds, string textureLocation, ValidationOptions validationOptions = ValidationOptions.Validate)
            : this(txds, textureLocation)
        {
            this.Filename = filename;
            this.Deserialise(filename, validationOptions);
        }

        /// <summary>
        /// Deserialisation constructor
        /// </summary>
        /// <param name="loadSourceTextures">Only set this if you know you don't need to use the source textures.</param>
        public GlobalRoot(String filename, string textureLocation, ValidationOptions validationOptions = ValidationOptions.Validate, bool loadSourceTextures = true)
            : this(null, textureLocation)
        {
            this.Filename = filename;
            this.LoadSourceTextures = loadSourceTextures;
            this.Deserialise(filename, validationOptions);
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public GlobalRoot(Dictionary<string, List<ITexture>> txds, string textureLocation)
        {
            this.GlobalTextureDictionaries = new ObservableDictionary<String, GlobalTextureDictionary>();
            this.CanAcceptSourceDictionaries = false;
            this.CanAcceptGlobalDictionaries = true;
            this.SourceTXDs = txds;
            this.TextureLocation = textureLocation;
            this.Filename = String.Empty;
            this.LoadSourceTextures = true;
        }
        #endregion // Constructors

        #region IDictionaryContainer Implementation
        /// <summary>
        /// Returns true iff this root has a texture dictionary in it with
        /// the given name. Is recursive is true it will recursive through all the texture
        /// dictionaries as well
        /// </summary>
        public Boolean ContainsTextureDictionary(String name, Boolean recursive)
        {
            foreach (GlobalTextureDictionary dictionary in GlobalTextureDictionaries.Values)
            {
                if (String.Compare(name, dictionary.Name, true) == 0)
                {
                    return true;
                }
                if (recursive == true)
                {
                    if (dictionary.ContainsTextureDictionary(name, true))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Returns true iff this root has a texture dictionary in it with
        /// the given name. Is recursive is true it will recursive through all the texture
        /// dictionaries as well
        /// </summary>
        public Boolean ContainsSourceTextureDictionary(String name, Boolean recursive)
        {
            foreach (GlobalTextureDictionary dictionary in GlobalTextureDictionaries.Values)
            {
                if (dictionary.ContainsSourceTextureDictionary(name, recursive))
                {
                    return true;
                }
            }

            return false;
        }


        /// <summary>
        /// Acquires a list of all child texture dictionaries within the global texture dictionary.
        /// </summary>
        /// <param name="root"></param>
        /// <param name="recursive"></param>
        /// <returns></returns>
        public IEnumerable<SourceTextureDictionary> GetAllSourceDictionaries()
        {
            foreach (SourceTextureDictionary dictionary in GetAllSourceDictionaries(this, true))
            {
                yield return dictionary;
            }
        }

        /// <summary>
        /// Acquires a list of all child texture dictionaries within the global texture dictionary.
        /// </summary>
        /// <param name="root"></param>
        /// <param name="recursive"></param>
        /// <returns></returns>
        public static IEnumerable<SourceTextureDictionary> GetAllSourceDictionaries(IDictionaryContainer root, Boolean recursive)
        {
            if (root.SourceTextureDictionaries != null)
            {
                foreach (SourceTextureDictionary dictionary in root.SourceTextureDictionaries.Values)
                {
                    yield return dictionary;
                }
            }

            if (recursive && root.GlobalTextureDictionaries != null)
            {
                foreach (GlobalTextureDictionary dictionary in root.GlobalTextureDictionaries.Values)
                {
                    foreach (SourceTextureDictionary child in GetAllSourceDictionaries(dictionary, true))
                    {
                        yield return child;
                    }
                }
            }
        }

        public Boolean ContainsEmptyTextureDictionary(Boolean recursive)
        {
            foreach (GlobalTextureDictionary dictionary in GlobalTextureDictionaries.Values)
            {
                if (dictionary.Textures.Count == 0)
                {
                    return true;
                }
                if (recursive == true)
                {
                    if (dictionary.ContainsEmptyTextureDictionary(true))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Adds the given dictionary into the children texture dictionaries iff the name
        /// is unique.
        /// </summary>
        public Boolean AddExistingGlobalDictionary(GlobalTextureDictionary dictionary)
        {
            if (this.GlobalTextureDictionaries.ContainsKey(dictionary.Name))
                return false;

            this.GlobalTextureDictionaries.Add(dictionary.Name, dictionary);
            return true;
        }

        /// <summary>
        /// Creates a new dictionary with the given name and adds it to this dictionary 
        /// iff the name is unique
        /// </summary>
        public Boolean AddNewGlobalDictionary(String name)
        {
            if (this.GlobalTextureDictionaries.ContainsKey(name))
                return false;

            this.GlobalTextureDictionaries.Add(name, new GlobalTextureDictionary(name, this));
            return true;
        }

        /// <summary>
        /// Creates a new source dictionary using the given texture dictionary as a 
        /// source guide.
        /// </summary>
        public Boolean AddNewSourceDictionary(TextureDictionary dictionary)
        {
            return false;
        }

        /// <summary>
        /// Removes the texture dictionary with the given name from the list iff
        /// it exists.
        /// </summary>
        public Boolean RemoveGlobalTextureDictionary(String name)
        {
            if (!this.GlobalTextureDictionaries.ContainsKey(name))
                return false;

            this.GlobalTextureDictionaries.Remove(name);
            return true;
        }

        /// <summary>
        /// Removes the given texture dictionary from the list iff
        /// it exists.
        /// </summary>
        public Boolean RemoveGlobalTextureDictionary(GlobalTextureDictionary dictionary)
        {
            if (!this.GlobalTextureDictionaries.ContainsKey(dictionary.Name))
                return false;

            this.GlobalTextureDictionaries.Remove(dictionary.Name);
            GlobalRoot.CheckPromotedTexturesAreValid(this, ValidationDirection.Up);
            GlobalRoot.CheckTexturesPromotionState(this);
            return true;
        }

        /// <summary>
        /// Removes the given texture dictionary from the list iff
        /// it exists.
        /// </summary>
        public Boolean RemoveSourceTextureDictionary(SourceTextureDictionary dictionary)
        {
            return false;
        }
        #endregion // IDictionaryContainer Implementation

        #region IEnumerable<GlobalTextureDictionary>
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerator<GlobalTextureDictionary> IEnumerable<GlobalTextureDictionary>.GetEnumerator()
        {
            foreach (GlobalTextureDictionary t in this.GlobalTextureDictionaries.Values)
            {
                yield return t;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerator)this);
        }
        #endregion // IEnumerable<GlobalTextureDictionary>
        
        #region Serialisation / Deserialisation Methods
        /// <summary>
        /// 
        /// </summary>
        public void Serialise(String filename)
        {
            try
            {
                XmlDocument document = null;
                Serialise(out document);
                String folder = System.IO.Path.GetDirectoryName(filename);
                System.IO.Directory.CreateDirectory(folder);
                document.Save(filename);
                this.Filename = filename;
            }
            catch (Exception ex)
            {
                String message = String.Format("Exception serialising TXDParentiser to file {0}.  See InnerException.", filename);
                Log.Log__Exception(ex, message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void Deserialise(String filename, ValidationOptions validationOptions)
        {
            try
            {
                using (XmlReader reader = XmlReader.Create(filename))
                {
                    this.LoadFrom(reader, validationOptions);
                }
                this.Filename = filename;
            }
            catch (Exception ex)
            {
                String message = String.Format("Exception deserialising TXDParentiser from file {0}.  See InnerException.", filename);
                throw new Exception(message, ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void Serialise(out XmlDocument document)
        {
            try
            {
                SaveTo(out document);
            }
            catch (Exception ex)
            {
                String message = String.Format("Exception serialising TXDParentiser to XML.  See InnerException.");
                throw new Exception(message, ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="document"></param>
        private void SaveTo(out XmlDocument document)
        {
            document = new XmlDocument();

            // XML Declaration
            XmlDeclaration xmlDecl = document.CreateXmlDeclaration("1.0", "utf-8", String.Empty);
            document.AppendChild(xmlDecl);

            // Document Root
            XmlElement rootNode = document.CreateElement("globalDictionaries");
            document.InsertBefore(xmlDecl, document.DocumentElement);

            // Loop through child data
            foreach (GlobalTextureDictionary dictionary in this.GlobalTextureDictionaries.Values)
            {
                dictionary.Serialise(document, rootNode);
            }

            document.AppendChild(rootNode);
        }

        private void LoadFrom(XmlReader reader, ValidationOptions validationOptions)
        {
            this.GlobalTextureDictionaries.Clear();
            System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
            while (reader.MoveToContent() != XmlNodeType.None)
            {
                if (reader.IsStartElement())
                {
                    if (string.Compare(reader.Name, "globalDictionary") == 0)
                    {
                        GlobalTextureDictionary newDictionary = new GlobalTextureDictionary(reader, this);
                        this.GlobalTextureDictionaries.Add(newDictionary.Name, newDictionary);

                        if (validationOptions == ValidationOptions.Validate)
                        {
                            GlobalRoot.CheckPromotedTexturesAreValid(newDictionary, ValidationDirection.Down);
                            GlobalRoot.CheckTexturesPromotionState(newDictionary);
                            GlobalRoot.CheckTextureDictionarySizes(newDictionary, ValidationDirection.Down);
                            GlobalRoot.CheckTextureInstanceCounts(newDictionary, ValidationDirection.Down);
                        }
                        continue;
                    }
                }

                reader.Read();
            }
            sw.Stop();
            RSG.Base.Logging.Log.StaticLog.Message("Loading the parent txd model took {0} milliseconds.", sw.ElapsedMilliseconds);
        }
        #endregion // Serialisation / Deserialisation Methods
        
        #region Static Helper Functions
        #region Public

        public static void CheckPromotedTexturesAreValid(IDictionaryContainer root, ValidationDirection direction)
        {
            IList<string> sourceTextures = new List<string>();
            GetAllSourceTextures(root, ref sourceTextures, true);

            if (root is ITextureContainer)
            {
                IList<ITextureChild> removeList = new List<ITextureChild>();
                foreach (ITextureChild texture in (root as ITextureContainer).Textures.Values)
                {
                    if (!sourceTextures.Contains(texture.StreamName.ToLower()))
                        removeList.Add(texture);
                }
                foreach (ITextureChild texture in removeList)
                {
                    (root as ITextureContainer).Textures.Remove(texture.StreamName);
                    (root as ITextureContainer).OnPromotionChanged(texture, false);
                }
            }
            if (direction == ValidationDirection.Up || direction == ValidationDirection.Both)
            {
                if (root.Parent != null)
                    CheckPromotedTexturesAreValid(root.Parent, ValidationDirection.Up);
            }
            if (direction == ValidationDirection.Down || direction == ValidationDirection.Both)
            {
                foreach (var child in root.GlobalTextureDictionaries.Values)
                {
                    CheckPromotedTexturesAreValid(child, ValidationDirection.Down);
                }
            }
        }

        public static void CheckTexturesPromotionState(IDictionaryContainer root)
        {
            if (root.SourceTextureDictionaries != null)
            {
                foreach (SourceTextureDictionary sd in root.SourceTextureDictionaries.Values)
                {
                    IList<string> promotedTextures = new List<string>();
                    GetAllPromotedTexturesFromBase(root, ref promotedTextures, true);
                    foreach (SourceTexture texture in sd.Textures.Values)
                    {
                        if (!promotedTextures.Contains(texture.StreamName.ToLower()))
                            texture.Promoted = false;
                        else
                            texture.Promoted = true;
                    }
                }
            }

            foreach (var child in root.GlobalTextureDictionaries.Values)
            {
                CheckTexturesPromotionState(child);
            }
        }

        public static void CheckSingleGlobalTexture(IDictionaryContainer root, string streamName, ValidationDirection direction)
        {
            CheckSingleGlobalTexture(root, streamName, direction, false);
        }

        private static void CheckSingleGlobalTexture(IDictionaryContainer root, string streamName, ValidationDirection direction, bool remove)
        {
            if (root is ITextureContainer && remove == true)
            {
                IList<ITextureChild> removeList = new List<ITextureChild>();
                foreach (ITextureChild texture in (root as ITextureContainer).Textures.Values)
                {
                    if (texture.StreamName.ToLower() == streamName.ToLower())
                        removeList.Add(texture);
                }
                foreach (ITextureChild texture in removeList)
                {
                    (root as ITextureContainer).Textures.Remove(texture.StreamName);
                }
            }
            if (direction == ValidationDirection.Up || direction == ValidationDirection.Both)
            {
                if (root.Parent != null)
                    CheckSingleGlobalTexture(root.Parent, streamName, ValidationDirection.Up, true);
            }
            if (direction == ValidationDirection.Down || direction == ValidationDirection.Both)
            {
                foreach (var child in root.GlobalTextureDictionaries.Values)
                {
                    CheckSingleGlobalTexture(child, streamName, ValidationDirection.Down, true);
                }
            }
        }

        public static void CheckTextureDictionarySizes(GlobalTextureDictionary root, ValidationDirection direction)
        {
            root.SetCurrentSize();
            if (direction == ValidationDirection.Up || direction == ValidationDirection.Both)
            {
                if (root.Parent != null && root.Parent is GlobalTextureDictionary)
                    CheckTextureDictionarySizes(root.Parent as GlobalTextureDictionary, ValidationDirection.Up);
            }
            if (direction == ValidationDirection.Down || direction == ValidationDirection.Both)
            {
                foreach (var child in root.GlobalTextureDictionaries.Values)
                {
                    CheckTextureDictionarySizes(child, ValidationDirection.Down);
                }
            }
        }

        public static void CheckTextureInstanceCounts(GlobalTextureDictionary root, ValidationDirection direction)
        {
            CheckAllPromotedTexturesInstanceCount(root);
            if (direction == ValidationDirection.Up || direction == ValidationDirection.Both)
            {
                if (root.Parent != null && root.Parent is GlobalTextureDictionary)
                    CheckTextureInstanceCounts(root.Parent as GlobalTextureDictionary, ValidationDirection.Up);
            }
            if (direction == ValidationDirection.Down || direction == ValidationDirection.Both)
            {
                foreach (var child in root.GlobalTextureDictionaries.Values)
                {
                    CheckTextureInstanceCounts(child, ValidationDirection.Down);
                }
            }
        }

        public static GlobalTextureDictionary GetParent(GlobalTextureDictionary dictionary1, GlobalTextureDictionary dictionary2)
        {
            if (dictionary1 == dictionary2)
                return dictionary1;

            GlobalTextureDictionary parent = dictionary1.Parent as GlobalTextureDictionary;
            while (parent != null)
            {
                if (parent == dictionary2)
                {
                    return dictionary2;
                }
                parent = parent.Parent as GlobalTextureDictionary;
            }
            
            parent = dictionary2.Parent as GlobalTextureDictionary;
            while (parent != null)
            {
                if (parent == dictionary1)
                {
                    return dictionary1;
                }
                parent = parent.Parent as GlobalTextureDictionary;
            }
            return null;
        }

        public static void SetTextureInstanceCount(String streamName, IDictionaryContainer root)
        {
            if (root == null)
                return;

            int count = 0;
            List<ITextureChild> textures = new List<ITextureChild>();
            if (root.SourceTextureDictionaries != null)
            {
                foreach (SourceTextureDictionary sourceDictionary in root.SourceTextureDictionaries.Values)
                {
                    SourceTexture texture = sourceDictionary[streamName];
                    if (texture != null)
                    {
                        textures.Add(texture);
                        count++;
                    }
                }
            }
            if (root.GlobalTextureDictionaries != null)
            {
                foreach (GlobalTextureDictionary globalDictionary in root.GlobalTextureDictionaries.Values)
                {
                    GlobalTexture texture = globalDictionary.GetTextureByName(streamName);
                    if (texture != null)
                    {
                        textures.Add(texture);
                        count++;
                    }
                }
            }

            foreach (ITextureChild texture in textures)
            {
                texture.InstanceCount = count;
                if (texture.Parent is GlobalTextureDictionary)
                {
                    if ((texture.Parent as GlobalTextureDictionary).Parent is GlobalTextureDictionary)
                    {
                        if (((texture.Parent as GlobalTextureDictionary).Parent as GlobalTextureDictionary).Parent is GlobalTextureDictionary)
                        {
                            GlobalTextureDictionary promotedTo = (texture.Parent as GlobalTextureDictionary).Parent as GlobalTextureDictionary;
                            GlobalTextureDictionary parent = promotedTo.Parent as GlobalTextureDictionary;
                            int countIfPromoted = 0;
                            foreach (GlobalTextureDictionary child in parent.GlobalTextureDictionaries.Values)
                            {
                                foreach (GlobalTexture srcTexture in child.Textures.Values)
                                {
                                    if (srcTexture.StreamName == texture.StreamName)
                                    {
                                        countIfPromoted++;
                                    }
                                }
                            }
                            texture.InstanceCountIfPromoted = 1 + countIfPromoted;
                        }
                    }
                }
                else if (texture.Parent is SourceTextureDictionary)
                {
                    if ((texture.Parent as SourceTextureDictionary).Parent is GlobalTextureDictionary)
                    {
                        if (((texture.Parent as SourceTextureDictionary).Parent as GlobalTextureDictionary).Parent is GlobalTextureDictionary)
                        {
                            GlobalTextureDictionary promotedTo = (texture.Parent as SourceTextureDictionary).Parent as GlobalTextureDictionary;
                            GlobalTextureDictionary parent = promotedTo.Parent as GlobalTextureDictionary;
                            int countIfPromoted = 0;
                            foreach (GlobalTextureDictionary child in parent.GlobalTextureDictionaries.Values)
                            {
                                foreach (GlobalTexture srcTexture in child.Textures.Values)
                                {
                                    if (srcTexture.StreamName == texture.StreamName)
                                    {
                                        countIfPromoted++;
                                    }
                                }
                            }
                            if ((texture as SourceTexture).Promoted)
                                texture.InstanceCountIfPromoted = countIfPromoted;
                            else
                                texture.InstanceCountIfPromoted = 1 + countIfPromoted;
                        }
                    }
                        
                }
            }
        }

        public static void CheckAllPromotedTexturesInstanceCount(IDictionaryContainer root)
        {
            if (root != null)
            {
                if (root.GlobalTextureDictionaries != null)
                {
                    foreach (GlobalTextureDictionary globalDictionary in root.GlobalTextureDictionaries.Values)
                    {
                        foreach (GlobalTexture texture in globalDictionary.Textures.Values)
                        {
                            SetTextureInstanceCount(texture.StreamName, root);
                        }
                    }
                }
                if (root.SourceTextureDictionaries != null)
                {
                    foreach (SourceTextureDictionary sourceDictionary in root.SourceTextureDictionaries.Values)
                    {
                        foreach (SourceTexture texture in sourceDictionary.Textures.Values)
                        {
                            SetTextureInstanceCount(texture.StreamName, root);
                        }
                    }
                }
            }
        }

        public static void TexturePromotionPostProcess(String streamName, IDictionaryContainer root)
        {
            IList<ITextureChild> textures = new List<ITextureChild>();
            int iteration = 0;
            GlobalRoot.GetAllTexturesByName(streamName, root, ref textures, ref iteration, true);

            foreach (ITextureChild texture in textures)
            {
                if (texture is SourceTexture)
                    (texture as SourceTexture).Promoted = true;
            }
        }

        public static void TextureFullyUnpromotedPostProcess(String streamName, IDictionaryContainer root)
        {
            List<ITextureChild> textures = new List<ITextureChild>();
            if (root.CanAcceptSourceDictionaries == true)
            {
                foreach (SourceTextureDictionary sourceDictionary in root.SourceTextureDictionaries.Values)
                {
                    SourceTexture texture = sourceDictionary[streamName];
                    if (texture != null)
                    {
                        texture.Promoted = false;
                        textures.Add(texture);
                    }
                }
            }
            if (root.CanAcceptGlobalDictionaries == true)
            {
                foreach (GlobalTextureDictionary globalDictionary in root.GlobalTextureDictionaries.Values)
                {
                    GlobalRoot.TextureFullyUnpromotedPostProcess(streamName, globalDictionary);
                }
            }
            if (root is GlobalTextureDictionary)
            {
                CheckAllPromotedTexturesInstanceCount((root as GlobalTextureDictionary).Parent);
            }
            foreach (ITextureChild texture in textures)
            {
                if (texture.InstanceCountIfPromoted > 1)
                {
                    texture.InstanceCountIfPromoted--;
                }
            }
        }

        public static void MoveTexturePostProcess(String streamName, GlobalTextureDictionary oldParent, GlobalTextureDictionary newParent)
        {
            // When a texture moves (i.e gets promoted/unpromoted by a certain number of levels we need to make sure that
            // the hierarchy is still valid.

            // Determine if we have promoted or unpromted the texture
            if (GlobalRoot.IsParent(oldParent, newParent))
            {
                // PROMOTED
                IList<ITextureChild> textures = new List<ITextureChild>();
                int iteration = 0;
                GlobalRoot.GetAllTexturesByName(streamName, newParent, ref textures, ref iteration, true);

                foreach (SourceTexture texture in textures.Where(t => t is SourceTexture == true))
                {
                    texture.Promoted = true;
                }
                foreach (GlobalTexture texture in textures.Where(t => t is GlobalTexture == true))
                {
                    (texture.Parent as GlobalTextureDictionary).MoveTextureFromHere(texture);
                }
            }
            else if (GlobalRoot.IsParent(newParent, oldParent))
            {
                // UNPROMOTED
                IList<ITextureChild> textures = new List<ITextureChild>();
                int iteration = 0;
                GlobalRoot.GetAllTexturesByName(streamName, newParent, ref textures, ref iteration, false);
                foreach (GlobalTexture texture in textures.Where(t => t is GlobalTexture == true))
                {
                    (texture.Parent as GlobalTextureDictionary).MoveTextureFromHere(texture);
                }
            }
        }

        public static void RemoveSourceDictionaryPostProcess(GlobalTextureDictionary sourceParent)
        {
            // Walk up the tree and make sure that all promoted textures are still valid if not remove them
            IDictionary<ITextureContainer, IList<String>> textures = new Dictionary<ITextureContainer, IList<String>>();
            GetAllPromotedTexturesWithParents(sourceParent as ITextureContainer, ref textures);

            foreach (KeyValuePair<ITextureContainer, IList<String>> parents in textures)
            {
                ITextureContainer parent = parents.Key;
                IList<String> sourceTextures = new List<String>();
                GetAllSourceTextures(parent as IDictionaryContainer, ref sourceTextures);

                IList<String> removeList = new List<String>();
                foreach (String promotedTexture in parents.Value)
                {
                    if (!sourceTextures.Contains(promotedTexture))
                    {
                        removeList.Add(promotedTexture);
                    }
                }
                (parent as GlobalTextureDictionary).MoveTexturesFromHere(removeList);
            }

            foreach (SourceTextureDictionary srcDictionary in sourceParent.SourceTextureDictionaries.Values)
            {
                foreach (SourceTexture texture in srcDictionary)
                {
                    GlobalRoot.SetTextureInstanceCount(texture.StreamName, sourceParent);
                }
            }
        }

        public static Boolean ContainsSourceTexture(String streamName, IDictionaryContainer root)
        {
            if (root.CanAcceptSourceDictionaries == true)
            {
                foreach (SourceTextureDictionary sourceDictionary in root.SourceTextureDictionaries.Values)
                {
                    SourceTexture texture = sourceDictionary[streamName];
                    if (texture != null)
                    {
                        return true;
                    }
                }
            }
            if (root.CanAcceptGlobalDictionaries == true)
            {
                foreach (GlobalTextureDictionary globalDictionary in root.GlobalTextureDictionaries.Values)
                {
                    if (GlobalRoot.ContainsSourceTexture(streamName, globalDictionary))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static Boolean AreDictionariesParentChild(GlobalTextureDictionary dictionary1, GlobalTextureDictionary dictionary2)
        {
            if (dictionary1 == dictionary2)
                return false;

            GlobalTextureDictionary parent = dictionary1.Parent as GlobalTextureDictionary;
            while (parent != null)
            {
                if (parent == dictionary2)
                {
                    return true;
                }
                parent = parent.Parent as GlobalTextureDictionary;
            }

            parent = dictionary2.Parent as GlobalTextureDictionary;
            while (parent != null)
            {
                if (parent == dictionary1)
                {
                    return true;
                }
                parent = parent.Parent as GlobalTextureDictionary;
            }

            return false;
        }

        /// <summary>
        /// Finds the source dictionary with the given name in the tree if it exists and
        /// returns it's parent, this returns null if the source dictionary coulfn't be found
        /// </summary>
        /// <param name="sourceName"></param>
        /// <returns></returns>
        public static GlobalTextureDictionary GetSourceDictionaryParent(String sourceName, String section, IDictionaryContainer root)
        {
            if (root.CanAcceptSourceDictionaries == true && (root is GlobalTextureDictionary))
            {
                foreach (SourceTextureDictionary dictionary in root.SourceTextureDictionaries.Values)
                {
                    if (String.Compare(sourceName, dictionary.Name, true) == 0
                     && String.Compare(section, dictionary.SourceSectionName, true) == 0)
                    {
                        return root as GlobalTextureDictionary;
                    }
                }
            }

            if (root.CanAcceptGlobalDictionaries == true)
            {
                foreach (GlobalTextureDictionary dictionary in root.GlobalTextureDictionaries.Values)
                {
                    GlobalTextureDictionary parent = GlobalRoot.GetSourceDictionaryParent(sourceName, section, dictionary);
                    if (parent != null)
                    {
                        return parent;
                    }
                }
            }

            return null;
        }

        #endregion // Public

        #region Export Functions
        
        private static System.Diagnostics.Process ExportProcess;

        /// <summary>
        /// Exports the given global root into the given xml filename
        /// </summary>
        public static Boolean ExportGlobalRoot(
            GlobalRoot root,
            String exportIdeFilename,
            String exportRpfFilename,
            String projectName,
            String branch,
            String metadataRoot,
            String mapTextureDirName,
            string toolsRoot,
            string sourceTxdPrefix)
        {
            if (root.GlobalTextureDictionaries.Count == 0)
            {
                MessageBox.Show("Unable to export the file " + root.Filename + " due to the fact that it doesn't contain any valid dictionaries to export", "Export Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }

            Dictionary<String, String> parents = new Dictionary<String, String>();
            GlobalRoot.CollectAllParents(root, ref parents, sourceTxdPrefix);

            ExportMetadataFile(parents, exportIdeFilename);

            string errorMessage = null;
            bool result = true;

            String exportTextureItdsScriptPath = GlobalRoot.CreateRubyExportScript(
                root,
                exportIdeFilename,
                exportRpfFilename,
                projectName,
                branch,
                metadataRoot,
                mapTextureDirName);
            if (GlobalRoot.RunRubyExportScript(root, exportTextureItdsScriptPath) == false)
            {
                errorMessage = "An error has occurred exporting the child texture dictionaries.";
                result = false;
            }

            //Delete any remaining child texture dictionaries from the directory.
            foreach (GlobalTextureDictionary dictionary in GetAllGlobalDictionaries(root, true))
            {
                String childItdOutputPath = Path.Combine(Path.GetDirectoryName(exportRpfFilename), dictionary.Name + ".itd.zip");
                if (File.Exists(childItdOutputPath))
                    File.Delete(childItdOutputPath);
            }

            if (File.Exists(exportIdeFilename))
            {
                File.Delete(exportIdeFilename);
            }

            string metaFilename = Path.ChangeExtension(exportIdeFilename, "meta");
            if (File.Exists(metaFilename))
            {
                File.Delete(metaFilename);
            }

            string convertExe = Path.Combine(toolsRoot, "ironlib", "lib", "RSG.Pipeline.Convert.exe");
            string branchArg = "--branch " + branch;
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = convertExe;
            startInfo.Arguments = string.Format("{0} {1}", branchArg, exportRpfFilename);
            startInfo.UseShellExecute = false;
            Process buildProcess = Process.Start(startInfo);
            buildProcess.WaitForExit();

            if (result == true)
            {
                MessageBox.Show("The export process has finished successfully.", "Successful", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else
            {
                MessageBox.Show(errorMessage, "Export Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
           
            return true;
        }

        /// <summary>
        /// Creates the new CMapTypes metadata file that replaces the gtxd.ide file.
        /// This makes sure the TXD hierarchy can be read in-game.
        /// </summary>
        /// <param name="parents"></param>
        /// <param name="exportIdeFilename"></param>
        private static void ExportItypFile(Dictionary<String, String> parents, String exportIdeFilename)
        {
            XmlDocument document = new XmlDocument();

            // XML Declaration
            XmlDeclaration xmlDecl = document.CreateXmlDeclaration(
                "1.0", "UTF-8", null);
            document.AppendChild(xmlDecl);

            // Document Root
            XmlElement xmlRoot = document.CreateElement("CMapTypes");
            document.AppendChild(xmlRoot);

            // Empty members
            xmlRoot.AppendChild(document.CreateElement("name"));
            xmlRoot.AppendChild(document.CreateElement("dependencies"));
            xmlRoot.AppendChild(document.CreateElement("archetypes"));
            xmlRoot.AppendChild(document.CreateElement("extensions"));

            // Relationships
            XmlElement xmlRelationships = document.CreateElement("txdRelationships");
            xmlRoot.AppendChild(xmlRelationships);

            foreach (KeyValuePair<String, String> parent in parents)
            {
                XmlElement xmlItem = document.CreateElement("item");
                XmlElement xmlParent = document.CreateElement("parent");
                xmlParent.AppendChild(document.CreateTextNode(parent.Value));
                XmlElement xmlChild = document.CreateElement("child");
                xmlChild.AppendChild(document.CreateTextNode(parent.Key));

                xmlItem.AppendChild(xmlParent);
                xmlItem.AppendChild(xmlChild);
                xmlRelationships.AppendChild(xmlItem);
            }

            xmlRoot.AppendChild(document.CreateElement("compositeEntityTypes"));
            document.Save(exportIdeFilename);
        }

        /// <summary>
        /// Creates the new CMapParentTxds metadata file that replaces the gtxd.ide file.
        /// This makes sure the TXD hierarchy can be read in-game.
        /// </summary>
        /// <param name="parents"></param>
        /// <param name="exportIdeFilename"></param>
        private static void ExportMetadataFile(Dictionary<String, String> parents, String filename)
        {
            XmlDocument document = new XmlDocument();
            XmlDeclaration xmlDecl = document.CreateXmlDeclaration("1.0", "UTF-8", null);
            document.AppendChild(xmlDecl);

            XmlElement xmlRoot = document.CreateElement("CMapParentTxds");
            document.AppendChild(xmlRoot);

            XmlElement xmlRelationships = document.CreateElement("txdRelationships");
            xmlRoot.AppendChild(xmlRelationships);

            foreach (KeyValuePair<String, String> parent in parents)
            {
                XmlElement xmlItem = document.CreateElement("item");
                XmlElement xmlParent = document.CreateElement("parent");
                xmlParent.AppendChild(document.CreateTextNode(parent.Value));
                XmlElement xmlChild = document.CreateElement("child");
                xmlChild.AppendChild(document.CreateTextNode(parent.Key));

                xmlItem.AppendChild(xmlParent);
                xmlItem.AppendChild(xmlChild);
                xmlRelationships.AppendChild(xmlItem);
            }

            document.Save(filename);
        }

        /// <summary>
        /// Acquires a list of all child texture dictionaries within the global texture dictionary.
        /// </summary>
        /// <param name="root"></param>
        /// <param name="recursive"></param>
        /// <returns></returns>
        public static IEnumerable<GlobalTextureDictionary> GetAllGlobalDictionaries(IDictionaryContainer root, Boolean recursive)
        {
            foreach (GlobalTextureDictionary dictionary in root.GlobalTextureDictionaries.Values)
            {
                if (dictionary.WorkInProgress)
                    continue;

                if (dictionary.Textures.Count > 0)
                    yield return dictionary;

                if (dictionary.CanAcceptGlobalDictionaries && recursive == true)
                {
                    foreach (GlobalTextureDictionary child in GetAllGlobalDictionaries(dictionary, true))
                    {
                        yield return child;
                    }
                }
            }
        }


        /// <summary>
        /// Creates a Ruby script to group all child texture dictionaries, package them in one zip and resource.
        /// </summary>
        /// <param name="root"></param>
        /// <param name="exportIdeFilename"></param>
        /// <param name="exportRpfFilename"></param>
        /// <param name="projectName"></param>
        /// <param name="branch"></param>
        /// <param name="metadataRoot"></param>
        /// <param name="mapTextureDirName"></param>
        /// <returns></returns>
        public static String CreateRubyExportScript(
            GlobalRoot root,
            String exportIdeFilename,
            String exportRpfFilename,
            String projectName,
            String branch,
            String metadataRoot,
            String mapTextureDirName)
        {
            String outputPath = Path.GetDirectoryName(exportRpfFilename);

            string metadataPath = Path.Combine(metadataRoot, "textures", mapTextureDirName, "gtxd");
            string templatePath = Path.Combine(metadataRoot, "textures", "templates", mapTextureDirName, "Diffuse");

            String filename = System.Environment.ExpandEnvironmentVariables(EXPORT_GTXD_SCRIPT);
            TextWriter textWriter = new StreamWriter(filename);

            textWriter.WriteLine("require 'pipeline/config/projects'");
            textWriter.WriteLine("require 'pipeline/config/project'");
            textWriter.WriteLine("require 'pipeline/os/file'");
            textWriter.WriteLine("require 'pipeline/os/getopt'");
            textWriter.WriteLine("require 'pipeline/os/path'");
            textWriter.WriteLine("require 'pipeline/util/environment'");
            textWriter.WriteLine("require 'pipeline/util/rage'");
            textWriter.WriteLine("require 'pipeline/projectutil/data_convert'");
            textWriter.WriteLine("require 'pipeline/gui/exception_dialog'");
            textWriter.WriteLine("require 'pipeline/projectutil/data_extract'");
            textWriter.WriteLine("require 'pipeline/projectutil/data_texture'");
            textWriter.WriteLine("require 'pipeline/scm/perforce'");
            textWriter.WriteLine("include Pipeline");
            textWriter.WriteLine("include Pipeline::ProjectUtil");
            
            textWriter.WriteLine("CL_DESCRIPTION = 'Automatic tcs creation for itd'");

            textWriter.WriteLine("if ( __FILE__ == $0 ) then");
            textWriter.WriteLine("\tbegin");

            textWriter.WriteLine("\t\t#-----------------------------------------------------------------------------");
            textWriter.WriteLine("\t\t# Global varaibles to create the pack files with.");
            textWriter.WriteLine("\t\t#-----------------------------------------------------------------------------");
            textWriter.WriteLine("\t\tg_Config = Pipeline::Config.instance( )");
            textWriter.WriteLine("\t\tg_Project = g_Config.projects[ '" + projectName + "' ]");
            textWriter.WriteLine("\t\tr = RageUtils.new( g_Project, '" + branch + "' )");
            textWriter.WriteLine("\t\tg_perforce = Pipeline::SCM::Perforce.new( )");
            textWriter.WriteLine("\t\tg_perforce.connect( )");
            textWriter.WriteLine("\t\tg_changelist_number = CL_DESCRIPTION");
            textWriter.WriteLine("\t\tg_metadata_path = '" + metadataPath + "'");
            textWriter.WriteLine("\t\tg_template_path = '" + templatePath + "'");

            // Now that we have the indiviual itd files for the texture dictionaries pack them into the main zip file.
            textWriter.WriteLine("\n\n\t\t#-----------------------------------------------------------------------------");
            textWriter.WriteLine("\t\t# Create the child texture dictionaries.");
            textWriter.WriteLine("\t\t#-----------------------------------------------------------------------------");

            foreach (GlobalTextureDictionary dictionary in GetAllGlobalDictionaries(root, true))
            {
                if (dictionary.WorkInProgress)
                    continue;

                String outputFilename = Path.Combine(Path.GetDirectoryName(outputPath), dictionary.Name + ".itd.zip");
                textWriter.WriteLine("\n\n\t\t#-----------------------------------------------------------------------------");
                textWriter.WriteLine("\t\t# Create the " + dictionary.Name + " texture dictionary.");
                textWriter.WriteLine("\t\t#-----------------------------------------------------------------------------");
                textWriter.WriteLine("\t\ttemp_dir = OS::Path::combine(Globals::instance().toolstemp,'metadata', '" + dictionary.Name + "' )");
                textWriter.WriteLine("\t\ttextureFileList = []");

                foreach (GlobalTexture texture in dictionary.Textures.Values)
                {
                    textWriter.WriteLine("\t\ttcsname = OS::Path::get_basename( '" + texture.StreamFilename + "' ) + '.tcs'");
                    textWriter.WriteLine("\t\ttcsname = OS::Path::combine( g_metadata_path, tcsname )");

                    textWriter.WriteLine("\t\ttclname = OS::Path::get_basename( '" + texture.StreamFilename + "' ) + '.tcl'");
                    textWriter.WriteLine("\t\ttclname = OS::Path::combine( temp_dir, tclname )");

                    textWriter.WriteLine("\t\tg_changelist_number = make_sure_tcs_exists_with_asset_base(g_perforce,g_changelist_number,tcsname,g_template_path,false,false)");
                    textWriter.WriteLine("\t\tmake_sure_tcs_exists_with_asset_base(nil,nil,tclname,OS::Path.remove_extension(tcsname))");

                    textWriter.WriteLine("\t\ttextureFileList << tclname");
                }

                textWriter.WriteLine("\t\ttxdOutput = '" + Path.Combine(outputPath, dictionary.Name + ".itd.zip'"));
                textWriter.WriteLine("\t\tFileUtils::rm( txdOutput ) if ( File::exists?( txdOutput ) )");
                textWriter.WriteLine("\t\tProjectUtil::data_zip_create( txdOutput, textureFileList )");
            }

            // Now that we have the indiviual itd files for the texture dictionaries pack them into the main zip file.
            textWriter.WriteLine("\n\n\t\t#-----------------------------------------------------------------------------");
            textWriter.WriteLine("\t\t# Create the main rpf file %RS_BUILDBRANCH%/independent/data/cdimages/gtxd.zip.");
            textWriter.WriteLine("\t\t#-----------------------------------------------------------------------------");
            textWriter.WriteLine("\t\tfileList = []");
            string metaFilename = Path.ChangeExtension(exportIdeFilename, "meta");
            textWriter.WriteLine("\t\tfileList.push '" + metaFilename + "'");

            foreach (GlobalTextureDictionary dictionary in GetAllGlobalDictionaries(root, true))
            {
                if (dictionary.WorkInProgress)
                    continue;

                String gtxdOutputPath = Path.Combine(Path.GetDirectoryName(exportRpfFilename), dictionary.Name + ".itd.zip");
                textWriter.WriteLine("\t\tfileList << '" + gtxdOutputPath + "'");
            }

            textWriter.WriteLine("\t\tProjectUtil::data_zip_create( '" + exportRpfFilename + "', fileList )");
            textWriter.WriteLine("\trescue Exception => ex");
            textWriter.WriteLine("\t\tputs 'Unhandled exception: #{ex.message}'");
            textWriter.WriteLine("\t\tPipeline::GUI::ExceptionDialog.show_dialog( ex )");
            textWriter.WriteLine("\t\texit( 1 )");

            textWriter.WriteLine("\n\tend");
            textWriter.WriteLine("end");
            textWriter.Close();

            return filename;
        }


        /// <summary>
        /// Executes the script to build the global texture dictionary.
        /// </summary>
        /// <param name="root"></param>
        /// <param name="exportScriptPath"></param>
        public static bool RunRubyExportScript(GlobalRoot root, String exportScriptPath)
        {
            if (!String.IsNullOrEmpty(exportScriptPath))
            {
                try
                {
                    ExportProcess = new System.Diagnostics.Process();
                    ExportProcess.StartInfo.FileName = Environment.ExpandEnvironmentVariables(EXPORT_COMMAND);
                    ExportProcess.StartInfo.Arguments = Environment.ExpandEnvironmentVariables(exportScriptPath);
                    ExportProcess.StartInfo.UseShellExecute = false;
                    ExportProcess.StartInfo.CreateNoWindow = false;
                    ExportProcess.EnableRaisingEvents = true;

                    ExportProcess.Start();

                    ExportProcess.WaitForExit();

                    if (ExportProcess.ExitCode != 0)
                    {
                        // process has finished but there has been an error
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Unhandled exception caught in the export process.", "Export Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    RSG.Base.Logging.Log.Log__Exception(ex, "Unhandled exception exporting data");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Event handler for the processing to be canceled.  This propagates the 'cancel' event 
        /// to any child processes running.
        /// </summary>
        public static void OnCancel()
        {
            if (ExportProcess != null && ExportProcess.HasExited == false)
            {
                ExportProcess.Kill();
            }
        }
        #endregion // Export Functions

        #region Private

        /// <summary>
        /// Goes through the whole given global root and adds parents/children to the given dictionary
        /// </summary>
        private static void CollectAllParents(IDictionaryContainer root, ref Dictionary<String, String> parents, string sourceTxdPrefix)
        {
            if ((root is GlobalTextureDictionary) && (root as GlobalTextureDictionary).Textures.Count > 0)
            {
                IDictionaryContainer firstParent = null;
                IDictionaryContainer parent = (root as GlobalTextureDictionary).Parent;
                while (parent != null && parent is GlobalTextureDictionary)
                {
                    if ((parent as GlobalTextureDictionary).WorkInProgress)
                    {
                        firstParent = null;
                        break;
                    }
                    if (firstParent == null && parent is ITextureContainer)
                    {
                        if ((parent as ITextureContainer).Textures.Count != 0)
                        {
                            firstParent = parent;
                        }
                    }
                    parent = (parent as GlobalTextureDictionary).Parent;
                }
                if (firstParent != null)
                    parents.Add(root.Name, firstParent.Name);
            }

            if (root.GlobalTextureDictionaries != null)
            {
                foreach (var gd in root.GlobalTextureDictionaries.Values)
                {
                    CollectAllParents(gd, ref parents, sourceTxdPrefix);
                }
            }

            if (root.SourceTextureDictionaries != null)
            {
                IDictionaryContainer firstParent = null;
                foreach (var sd in root.SourceTextureDictionaries.Values)
                {
                    if (firstParent == null)
                    {
                        IDictionaryContainer parent = sd.Parent;
                        while (parent != null && parent is GlobalTextureDictionary)
                        {
                            if ((parent as GlobalTextureDictionary).WorkInProgress)
                            {
                                firstParent = null;
                                break;
                            }
                            if (firstParent == null && parent is ITextureContainer)
                            {
                                if ((parent as ITextureContainer).Textures.Count != 0)
                                {
                                    firstParent = parent;
                                }
                            }
                            parent = (parent as GlobalTextureDictionary).Parent;
                        }
                    }

                    if (firstParent != null)
                    {
                        if (string.IsNullOrWhiteSpace(sourceTxdPrefix))
                        {
                            parents.Add(sd.Name, firstParent.Name);
                        }
                        else
                        {
                            parents.Add(string.Format("{0}{1}", sourceTxdPrefix, sd.Name), firstParent.Name);
                        }
                    }
                    else
                        break;
                }
            }
        }

        /// <summary>
        /// Returns the first parent of the specified child that isn't empty
        /// </summary>
        /// <param name="child"></param>
        /// <returns></returns>
        private static GlobalTextureDictionary NonEmptyParent(GlobalTextureDictionary child)
        {
            GlobalTextureDictionary parentTxd = child.Parent as GlobalTextureDictionary;
            if (parentTxd == null)
                return null;

            // Need to make sure that the parentTxd isn't empty and if so
            // we need to go up the hierarchy until we find a parent
            // that isn't empty.
            while (parentTxd != null && parentTxd.Textures.Count == 0)
            {
                parentTxd = parentTxd.Parent as GlobalTextureDictionary;
            }
            return parentTxd;
        }

        private static Boolean IsParent(GlobalTextureDictionary root, GlobalTextureDictionary parent)
        {
            if (root == parent)
                return false;

            GlobalTextureDictionary rootParent = root.Parent as GlobalTextureDictionary;
            while (parent != null)
            {
                if (rootParent == parent)
                {
                    return true;
                }
                parent = parent.Parent as GlobalTextureDictionary;
            }

            return false;
        }

        public static void GetAllPromotedTexturesFromBase(ITextureContainer baseDictionary, ref IList<String> promotedTextures)
        {
            foreach (GlobalTexture texture in baseDictionary.Textures.Values)
            {
                if (!promotedTextures.Contains(texture.StreamName))
                {
                    promotedTextures.Add(texture.StreamName);
                }
            }
            if (baseDictionary is IDictionaryChild && (baseDictionary as IDictionaryChild).Parent is ITextureContainer)
            {
                GlobalRoot.GetAllPromotedTexturesFromBase(((baseDictionary as IDictionaryChild).Parent as ITextureContainer), ref promotedTextures);
            }
        }

        public static void GetAllPromotedTexturesFromBase(IDictionaryContainer root, ref IList<String> textures, bool lowerCase = false)
        {
            if (root is GlobalTextureDictionary)
            {
                foreach (GlobalTexture texture in (root as GlobalTextureDictionary).Textures.Values)
                {
                    if (lowerCase)
                    {
                        if (!textures.Contains(texture.StreamName.ToLower()))
                        {
                            textures.Add(texture.StreamName.ToLower());
                        }
                    }
                    else
                    {
                        if (!textures.Contains(texture.StreamName))
                        {
                            textures.Add(texture.StreamName);
                        }
                    }
                }
            }
            if (root.Parent != null)
                GlobalRoot.GetAllPromotedTexturesFromBase(root.Parent, ref textures, lowerCase);
        }

        /// <summary>
        /// Acquires a list of all promoted textures for the specified route.
        /// </summary>
        /// <param name="root"></param>
        /// <param name="textures"></param>
        /// <param name="lowerCase"></param>
        public static void GetAllPromotedTextures(IDictionaryContainer root, ref IList<String> textures, bool lowerCase = false)
        {
            if (root is GlobalTextureDictionary)
            {
                foreach (GlobalTexture texture in (root as GlobalTextureDictionary).Textures.Values)
                {
                    if (lowerCase)
                    {
                        if (!textures.Contains(texture.StreamName.ToLower()))
                        {
                            textures.Add(texture.StreamName.ToLower());
                        }
                    }
                    else
                    {
                        if (!textures.Contains(texture.StreamName))
                        {
                            textures.Add(texture.StreamName);
                        }
                    }
                }
            }
            foreach (GlobalTextureDictionary globalDictionary in root.GlobalTextureDictionaries.Values)
            {
                GlobalRoot.GetAllPromotedTextures(globalDictionary, ref textures, lowerCase);
            }
        }

        private static void GetAllPromotedTexturesWithParents(ITextureContainer baseDictionary, ref IDictionary<ITextureContainer, IList<String>> promotedTextures)
        {
            if (!promotedTextures.ContainsKey(baseDictionary))
            {
                promotedTextures.Add(baseDictionary, new List<String>());
                foreach (GlobalTexture texture in baseDictionary.Textures.Values)
                {
                    if (!promotedTextures[baseDictionary].Contains(texture.StreamName))
                    {
                        promotedTextures[baseDictionary].Add(texture.StreamName);
                    }
                }
            }

            if (baseDictionary is IDictionaryChild && (baseDictionary as IDictionaryChild).Parent is ITextureContainer)
            {
                GlobalRoot.GetAllPromotedTexturesWithParents(((baseDictionary as IDictionaryChild).Parent as ITextureContainer), ref promotedTextures);
            }
        }

        private static void GetAllTexturesByName(String streamName, IDictionaryContainer root, ref IList<ITextureChild> textures, ref int iteration, Boolean down)
        {
            if (down == true)
            {
                if (root is ITextureContainer && iteration != 0)
                {
                    foreach (ITextureChild texture in (root as ITextureContainer).Textures.Values)
                    {
                        if (texture.StreamName == streamName)
                        {
                            textures.Add(texture);
                        }
                    }
                }
                if (root.CanAcceptSourceDictionaries == true)
                {
                    foreach (SourceTextureDictionary sourceDictionary in root.SourceTextureDictionaries.Values)
                    {
                        SourceTexture texture = sourceDictionary[streamName];
                        if (texture != null)
                        {
                            textures.Add(texture);
                        }
                    }
                }
                if (root.CanAcceptGlobalDictionaries == true)
                {
                    foreach (GlobalTextureDictionary globalDictionary in root.GlobalTextureDictionaries.Values)
                    {
                        iteration++;
                        GlobalRoot.GetAllTexturesByName(streamName, globalDictionary, ref textures, ref iteration, true);
                    }
                }
            }
            else
            {
                if (root is ITextureContainer && iteration != 0)
                {
                    foreach (ITextureChild texture in (root as ITextureContainer).Textures.Values)
                    {
                        if (texture.StreamName == streamName)
                        {
                            textures.Add(texture);
                        }
                    }
                }
                GlobalTextureDictionary parent = (root as GlobalTextureDictionary) != null ? (root as GlobalTextureDictionary).Parent as GlobalTextureDictionary : null;

                while (parent != null)
                {
                    foreach (ITextureChild texture in (parent as ITextureContainer).Textures.Values)
                    {
                        if (texture.StreamName == streamName)
                        {
                            textures.Add(texture);
                        }
                    }
                    parent = parent.Parent as GlobalTextureDictionary;
                }
            }
        }

        private static void GetAllTexturesByNameWithParents(String streamName, IDictionaryContainer root, ref IList<ITextureChild> textures, ref IList<ITextureContainer> parents, ref int iteration, Boolean down)
        {
            if (down == true)
            {
                if (root is ITextureContainer && iteration != 0)
                {
                    foreach (ITextureChild texture in (root as ITextureContainer).Textures.Values)
                    {
                        if (texture.StreamName == streamName)
                        {
                            textures.Add(texture);
                            if (!parents.Contains((root as ITextureContainer)))
                            {
                                parents.Add((root as ITextureContainer));
                            }
                        }
                    }
                }
                foreach (SourceTextureDictionary sourceDictionary in root.SourceTextureDictionaries.Values)
                {
                    SourceTexture texture = sourceDictionary[streamName];
                    if (texture != null)
                    {
                        textures.Add(texture);
                        if (!parents.Contains((sourceDictionary as ITextureContainer)))
                        {
                            parents.Add((sourceDictionary as ITextureContainer));
                        }
                    }
                }
                foreach (GlobalTextureDictionary globalDictionary in root.GlobalTextureDictionaries.Values)
                {
                    iteration++;
                    GlobalRoot.GetAllTexturesByNameWithParents(streamName, globalDictionary, ref textures, ref parents, ref iteration, true);
                }
            }
            else
            {
                if (root is ITextureContainer && iteration != 0)
                {
                    foreach (ITextureChild texture in (root as ITextureContainer).Textures.Values)
                    {
                        if (texture.StreamName == streamName)
                        {
                            textures.Add(texture);
                            if (!parents.Contains((root as ITextureContainer)))
                            {
                                parents.Add((root as ITextureContainer));
                            }
                        }
                    }
                }
                GlobalTextureDictionary parent = (root as GlobalTextureDictionary) != null ? (root as GlobalTextureDictionary).Parent as GlobalTextureDictionary : null;

                while (parent != null)
                {
                    foreach (ITextureChild texture in (parent as ITextureContainer).Textures.Values)
                    {
                        if (texture.StreamName == streamName)
                        {
                            textures.Add(texture);
                            if (!parents.Contains((parent as ITextureContainer)))
                            {
                                parents.Add((parent as ITextureContainer));
                            }
                        }
                    }
                    parent = parent.Parent as GlobalTextureDictionary;
                }
            }
        }

        private static void GetAllSourceTexturesByName(String streamName, IDictionaryContainer root, ref IList<SourceTexture> textures)
        {
            foreach (SourceTextureDictionary sourceDictionary in root.SourceTextureDictionaries.Values)
            {
                SourceTexture texture = sourceDictionary[streamName];
                if (texture != null)
                {
                    textures.Add(texture);
                }
            }
            foreach (GlobalTextureDictionary globalDictionary in root.GlobalTextureDictionaries.Values)
            {
                GlobalRoot.GetAllSourceTexturesByName(streamName, globalDictionary, ref textures);
            }
        }

        private static void GetAllSourceTextures(IDictionaryContainer root, ref IList<String> textures, bool lowerCase = false)
        {
            if (root.SourceTextureDictionaries != null)
            {
                foreach (SourceTextureDictionary sourceDictionary in root.SourceTextureDictionaries.Values)
                {
                    foreach (SourceTexture texture in sourceDictionary.Textures.Values)
                    {
                        if (lowerCase)
                        {
                            if (!textures.Contains(texture.StreamName.ToLower()))
                            {
                                textures.Add(texture.StreamName.ToLower());
                            }
                        }
                        else
                        {
                            if (!textures.Contains(texture.StreamName))
                            {
                                textures.Add(texture.StreamName);
                            }
                        }
                    }
                }
            }
            foreach (GlobalTextureDictionary globalDictionary in root.GlobalTextureDictionaries.Values)
            {
                GlobalRoot.GetAllSourceTextures(globalDictionary, ref textures, lowerCase);
            }
        }
        #endregion // Private
        #endregion Static Helper Functions
    } // GlobalRoot
} // RSG.Model.GlobalTXD
