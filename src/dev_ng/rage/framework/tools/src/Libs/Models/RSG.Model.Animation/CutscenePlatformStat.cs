﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Animation;

namespace RSG.Model.Animation
{
    /// <summary>
    /// Cutscene platform statistics.
    /// </summary>
    [DataContract]
    [Serializable]
    [KnownType(typeof(CutscenePlatformSectionStat))]
    public class CutscenePlatformStat : ICutscenePlatformStat
    {
        #region Properties
        /// <summary>
        /// Total physical size.
        /// </summary>
        public ulong PhysicalSize
        {
            get
            {
                return (ulong)Sections.Sum(item => (decimal)item.PhysicalSize);
            }
        }

        /// <summary>
        /// Total virtual size.
        /// </summary>
        public ulong VirtualSize
        {
            get
            {
                return (ulong)Sections.Sum(item => (decimal)item.VirtualSize);
            }
        }

        /// <summary>
        /// Name of the file where this platform data resides.
        /// </summary>
        [DataMember]
        public String PlatformDataPath { get; private set; }

        /// <summary>
        /// Sizes of the various sections for this cutscene.
        /// </summary>
        [DataMember]
        public IList<ICutscenePlatformSectionStat> Sections { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// For serialisation purposes.
        /// </summary>
        private CutscenePlatformStat()
        {
            Sections = new List<ICutscenePlatformSectionStat>();
        }

        /// <summary>
        /// 
        /// </summary>
        public CutscenePlatformStat(String dataPath)
            : this()
        {
            PlatformDataPath = dataPath;
        }
        #endregion // Constructor(s)
    } // CutscenePlatformStat
}
