﻿//---------------------------------------------------------------------------------------------
// <copyright file="ParentStreamResolver.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Inheritance
{
    using System.Xml;

    /// <summary>
    /// A class that is used to create a stream object from a parent string value from tunable
    /// structure used for 
    /// </summary>
    public abstract class ParentStreamResolver
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ParentStreamResolver"/> class.
        /// </summary>
        public ParentStreamResolver()
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Override to convert a string that has been obtained from the parent tunable of a
        /// data inheritance structure to a XML reader object.
        /// </summary>
        /// <param name="value">
        /// The string value to convert.
        /// </param>
        /// <returns>
        /// A new XML reader object that represents the data inside the file the given string
        /// points to.
        /// </returns>
        /// <remarks>
        /// The return stream will be closed after it has been used.
        /// </remarks>
        public abstract XmlReader ResolveString(string value);
        #endregion Methods
    } // RSG.Metadata.Model.Inheritance.ParentStreamResolver {Class}
} // RSG.Metadata.Model.Inheritance {Namespace}
