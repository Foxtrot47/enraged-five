﻿//---------------------------------------------------------------------------------------------
// <copyright file="ConverterCollection.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Converters
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Contains a set of converters for all registered types that use a single numerical
    /// constant object for the conversion operation.
    /// </summary>
    public class ConverterCollection
    {
        #region Fields
        /// <summary>
        /// A reference to the numerical constants to use during any conversions.
        /// </summary>
        private NumericalConstants _constants;

        /// <summary>
        /// The private dictionary that contains string converts indexed by the conversion
        /// type.
        /// </summary>
        private Dictionary<Type, object> _internalConverters;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ConverterCollection"/> class.
        /// </summary>
        public ConverterCollection()
        {
            this._constants = new NumericalConstants(this);
            this._internalConverters = new Dictionary<Type, object>
            {
                { typeof(sbyte), new StringToInt8Converter(this._constants) },
                { typeof(short), new StringToInt16Converter(this._constants) },
                { typeof(int), new StringToInt32Converter(this._constants) },
                { typeof(long), new StringToInt64Converter(this._constants) },
                { typeof(byte), new StringToUnsignedInt8Converter(this._constants) },
                { typeof(ushort), new StringToUnsignedInt16Converter(this._constants) },
                { typeof(uint), new StringToUnsignedInt32Converter(this._constants) },
                { typeof(ulong), new StringToUnsignedInt64Converter(this._constants) },
                { typeof(float), new StringToFloatConverter(this._constants) },
                { typeof(double), new StringToDoubleConverter(this._constants) },
                { typeof(bool), new StringToBoolConverter() },
                { typeof(Guid), new StringToGuidConverter() }
            };
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ConverterCollection"/> class.
        /// </summary>
        /// <param name="constants">
        /// The numerical constant class that the converters can use.
        /// </param>
        public ConverterCollection(NumericalConstants constants)
        {
            this._constants = constants;
            this._internalConverters = new Dictionary<Type, object>
            {
                { typeof(sbyte), new StringToInt8Converter(constants) },
                { typeof(short), new StringToInt16Converter(constants) },
                { typeof(int), new StringToInt32Converter(constants) },
                { typeof(long), new StringToInt64Converter(constants) },
                { typeof(byte), new StringToUnsignedInt8Converter(constants) },
                { typeof(ushort), new StringToUnsignedInt16Converter(constants) },
                { typeof(uint), new StringToUnsignedInt32Converter(constants) },
                { typeof(ulong), new StringToUnsignedInt64Converter(constants) },
                { typeof(float), new StringToFloatConverter(constants) },
                { typeof(double), new StringToDoubleConverter(constants) },
                { typeof(bool), new StringToBoolConverter() },
                { typeof(Guid), new StringToGuidConverter() }
            };
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the numerical constants used by the converters.
        /// </summary>
        public NumericalConstants Constants
        {
            get { return this._constants; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Gets a converter that can convert a string object to a value of the specific type.
        /// </summary>
        /// <typeparam name="T">
        /// The type the returned converter should be converting a string into.
        /// </typeparam>
        /// <returns>
        /// The converter for the specified type.
        /// </returns>
        public IConverter<T> GetConverter<T>()
        {
            object cachedConverter;
            this._internalConverters.TryGetValue(typeof(T), out cachedConverter);
            IConverter<T> converter = cachedConverter as IConverter<T>;
            if (converter == null)
            {
                string typeName = typeof(T).Name;
                throw new KeyNotFoundException();
            }

            return converter;
        }
        #endregion Methods
    } // RSG.Metadata.Model.Converters.ConverterCollection {Class}
} // RSG.Metadata.Model.Converters {Namespace}
