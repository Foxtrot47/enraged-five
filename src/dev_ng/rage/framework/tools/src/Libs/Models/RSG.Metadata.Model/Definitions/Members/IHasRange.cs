﻿// --------------------------------------------------------------------------------------------
// <copyright file="IHasRange.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions.Members
{
    /// <summary>
    /// When implemented represents a member that has a valid range setup with a minimum and
    /// maximum value.
    /// </summary>
    public interface IHasRange : IMember
    {
    } // RSG.Metadata.Model.Definitions.Members.IHasRange {Interface}
} // RSG.Metadata.Model.Definitions.Members {Namespace}
