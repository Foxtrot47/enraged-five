﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.ConfigParser;
using System.IO;
using System.Web.UI;
using RSG.Model.Common;
using RSG.Base.Threading;
using RSG.Base.Tasks;
using RSG.Model.Common.Map;
using RSG.Base.Math;

namespace RSG.Model.Report.Reports.Map
{
    /// <summary>
    /// Generate and display a duplicated item within a specific radius report.
    /// </summary>
    public class DuplicatedItemWithRadiusReport : HTMLReport, IDynamicLevelReport
    {
        #region Helper classes

        /// <summary>
        /// Duplicate items.
        /// </summary>
        class Duplicate
        {
            #region Properties

            /// <summary>
            /// Archetype name.
            /// </summary>
            public string ArchetypeName { get; set; }

            /// <summary>
            /// Entity one.
            /// </summary>
            public IEntity EntityOne { get; set; }

            /// <summary>
            /// Entity two.
            /// </summary>
            public IEntity EntityTwo { get; set; }

            #endregion

            #region Constructor

            /// <summary>
            /// Constructor.
            /// </summary>
            /// <param name="archetypeName">Archetype name.</param>
            /// <param name="entityOne">Entity one.</param>
            /// <param name="entityTwo">Entity two.</param>
            public Duplicate(string archetypeName, IEntity entityOne, IEntity entityTwo)
            {
                ArchetypeName = archetypeName;
                EntityOne = entityOne;
                EntityTwo = entityTwo;
            }

            #endregion

            #region Public methods

            /// <summary>
            /// Compares the given entity against EntityOne and EntityTwo. If the given entity is either of them, returns true.
            /// </summary>
            /// <param name="entity">Entity.</param>
            /// <returns>True if EntityOne or EntityTwo is the given entity.</returns>
            public bool ContainsEntity(IEntity entity)
            {
                return EntityOne == entity || EntityTwo == entity;
            }

            #endregion
        }

        /// <summary>
        /// Duplicates list.
        /// </summary>
        class DuplicatesList : List<Duplicate>
        {
            #region Properties

            /// <summary>
            /// The name of the section.
            /// </summary>
            public string SectionName { get; set; }

            /// <summary>
            /// File name.
            /// </summary>
            public string FileName { get; set; }

            #endregion

            #region Constructor

            /// <summary>
            /// Constructor.
            /// </summary>
            /// <param name="sectionName">The name of the section.</param>
            /// <param name="fileName">File name.</param>
            public DuplicatesList(string sectionName, string fileName)
            {
                SectionName = sectionName;
                FileName = fileName;
            }

            #endregion

            #region Public methods

            /// <summary>
            /// Compares the given entity with each duplicate contained in the list. If the entity appears in the list, the 
            /// method returns true.
            /// </summary>
            /// <param name="entity">Entity.</param>
            /// <returns>Returns true if the entity appears in the list.</returns>
            public bool ContainsEntity(IEntity entity)
            {
                foreach (var item in this)
                {
                    if (item.ContainsEntity(entity))
                    {
                        return true;
                    }
                }

                return false;
            }

            #endregion
        }

        #endregion

        #region Private member fields

        private double m_radius;
        private double m_radiusSquared;

        #endregion

        #region Constants

        private const String c_name = "Duplicated Entity Report";
        private const String c_description = "Generate and display a duplicated item within a specific radius report.";
        private const double c_oneCentimetre = 0.01;

        #endregion

        #region Properties

        /// <summary>
        /// Radius around an object. Any object within this radius and of the same archetype is considered a duplicate.
        /// </summary>
        public double Radius
        {
            get { return m_radius; }
            set
            {
                m_radius = value;
                m_radiusSquared = m_radius * m_radius;
            }
        }
        
        /// <summary>
        /// Task.
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (m_task == null)
                {
                    CompositeTask task = new CompositeTask("Generate Report");
                    task.AddSubTask(new ActionTask("Loading data", (context, progress) => EnsureDataLoaded((DynamicLevelReportContext)context, progress)), 50);
                    task.AddSubTask(new ActionTask("Generating report", (context, progress) => GenerateReport((DynamicLevelReportContext)context, progress)));
                    m_task = task;
                }
                return m_task;
            }
        }
        private ITask m_task;

        /// <summary>
        /// List of stats that are required to generate this report.
        /// </summary>
        public IEnumerable<StreamableStat> RequiredStats
        {
            get { return null; }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor.
        /// </summary>
        public DuplicatedItemWithRadiusReport()
            : base(c_name, c_description)
        {
            Radius = c_oneCentimetre;
        }

        #endregion

        #region Task Methods

        /// <summary>
        /// Ensure that the data from each section is available for the report.
        /// </summary>
        /// <param name="context">Report context.</param>
        private void EnsureDataLoaded(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            IMapHierarchy hierarchy = context.Level.MapHierarchy;

            if (hierarchy != null)
            {
                double increment = 1.0 / hierarchy.AllSections.Count();

                // Load all the map section data
                foreach (IMapSection section in hierarchy.AllSections)
                {
                    context.Token.ThrowIfCancellationRequested();

                    // Update the progress information
                    string message = String.Format("Loading {0}", section.Name);
                    progress.Report(new TaskProgress(increment, true, message));

                    section.RequestAllStatistics(false);
                }
            }

        }

        /// <summary>
        /// Generate the report.
        /// </summary>
        /// <param name="context">Report context.</param>
        private void GenerateReport(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            GenerateDuplicatedItemReport(context.Level, context.GameView);
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Generate the duplicated item report for the specified level.
        /// </summary>
        /// <param name="level">Game level.</param>
        /// <param name="gv">Configuration.</param>
        public void GenerateDuplicatedItemReport(ILevel level, ConfigGameView gv)
        {
            IMapHierarchy hierarchy = level.MapHierarchy;

            if (hierarchy != null)
            {
                List<DuplicatesList> duplicates = new List<DuplicatesList>();

                foreach (IMapSection section in hierarchy.AllSections)
                {
                    DuplicatesList sectionDuplicates = new DuplicatesList(section.Name, section.DCCSourceFilename);
                    duplicates.Add(sectionDuplicates);

                    FindDuplicates(section, sectionDuplicates);
                }

                GenerateReport(duplicates);
            }
        }

        /// <summary>
        /// Render the report in HTML.
        /// </summary>
        /// <param name="duplicates">List of duplicated items.</param>
        private void GenerateReport(List<DuplicatesList> duplicates)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter reportFile = new StreamWriter(stream);

            HtmlTextWriter writer = new HtmlTextWriter(reportFile);

            writer.RenderBeginTag(HtmlTextWriterTag.Html);

            writer.RenderBeginTag(HtmlTextWriterTag.Head);
            writer.RenderBeginTag(HtmlTextWriterTag.Title);
            writer.Write(c_name);
            writer.RenderEndTag();
            writer.RenderEndTag();

            writer.RenderBeginTag(HtmlTextWriterTag.Body);

            writer.RenderBeginTag(HtmlTextWriterTag.H1);
            writer.Write(c_name);
            writer.RenderEndTag();

            writer.RenderBeginTag(HtmlTextWriterTag.Em);
            writer.Write(c_description);
            writer.RenderEndTag();

            int sectionsNoDups = duplicates.Count(dup => dup.Count == 0);
            int sectionsWithDups = duplicates.Count - sectionsNoDups;

            writer.Write("<br />");
            writer.Write("<br />");

            writer.Write("There are " + duplicates.Count + " section(s). " + sectionsNoDups + " sections have no duplicate items, " + sectionsWithDups + " section(s) have duplicate items.");
            writer.Write("<br />");

            WriteReport(writer, duplicates);

            writer.RenderEndTag();

            writer.Flush();
            stream.Flush();
            stream.Position = 0;
            Stream = stream;
        }

        /// <summary>
        /// Write the report.
        /// </summary>
        /// <param name="writer">Html writer.</param>
        /// <param name="duplicates">List of duplicates.</param>
        private void WriteReport(HtmlTextWriter writer, List<DuplicatesList> duplicates)
        {
            foreach(var dup in duplicates)
            {
                WriteSection(writer, dup);
            }
        }

        /// <summary>
        /// Write the section data out to Html.
        /// </summary>
        /// <param name="writer">Html writer.</param>
        /// <param name="duplicatesList">List of duplicates.</param>
        private void WriteSection(HtmlTextWriter writer, DuplicatesList duplicatesList)
        {
            if (duplicatesList.Count == 0)
            {
                return;
            }

            SortedList<string, List<Duplicate>> sortedList = new SortedList<string, List<Duplicate>>();
            foreach (var dup in duplicatesList)
            {
                if (!sortedList.ContainsKey(dup.ArchetypeName))
                {
                    sortedList.Add(dup.ArchetypeName, new List<Duplicate>());
                }

                sortedList[dup.ArchetypeName].Add(dup);
            }

            writer.RenderBeginTag(HtmlTextWriterTag.H2);
            writer.Write(duplicatesList.SectionName);
            writer.RenderEndTag();

            writer.RenderBeginTag(HtmlTextWriterTag.Center);

            writer.AddAttribute(HtmlTextWriterAttribute.Width, "80%");
            writer.AddAttribute(HtmlTextWriterAttribute.Border, "1");
            writer.AddAttribute(HtmlTextWriterAttribute.Cellpadding, "2");
            writer.RenderBeginTag(HtmlTextWriterTag.Table);

            writer.RenderBeginTag(HtmlTextWriterTag.Tr);
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.RenderBeginTag(HtmlTextWriterTag.B);
            writer.Write("Archetype");
            writer.RenderEndTag();
            writer.RenderEndTag();
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.RenderBeginTag(HtmlTextWriterTag.B);
            writer.Write("Entity One");
            writer.RenderEndTag();
            writer.RenderEndTag();
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.RenderBeginTag(HtmlTextWriterTag.B);
            writer.Write("Entity Two");
            writer.RenderEndTag();
            writer.RenderEndTag();
            writer.RenderEndTag();

            foreach (var entry in sortedList)
            {
                foreach (var dup in entry.Value)
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                    writer.RenderBeginTag(HtmlTextWriterTag.Td);
                    writer.Write(dup.ArchetypeName);
                    writer.RenderEndTag();
                    writer.RenderBeginTag(HtmlTextWriterTag.Td);
                    if(dup.EntityOne==null || dup.EntityOne.Position==null)
                    {
                        writer.Write("INVALID");
                    }
                    else
                    {
                        writer.Write(dup.EntityOne.Position);
                    }
                    writer.RenderEndTag();
                    writer.RenderBeginTag(HtmlTextWriterTag.Td);
                    if (dup.EntityTwo == null || dup.EntityTwo.Position == null)
                    {
                        writer.Write("INVALID");
                    }
                    else
                    {
                        writer.Write(dup.EntityTwo.Position);
                    }
                    writer.RenderEndTag();
                    writer.RenderEndTag();
                }
            }

            writer.RenderEndTag();

            writer.RenderEndTag();
        }

        /// <summary>
        /// Find the duplicates in a map section.
        /// </summary>
        /// <param name="mapSection">Map section.</param>
        /// <param name="sectionDuplicates">Duplicates.</param>
        private void FindDuplicates(IMapSection mapSection, DuplicatesList sectionDuplicates)
        {
            foreach (var archetype in mapSection.Archetypes)
            {
                foreach (var entity in archetype.Entities)
                {
                    foreach (var other in archetype.Entities.Where(e => e != entity))
                    {
                        if (entity.Position == null || other.Position == null)
                        {
                            continue;
                        }

                        double diff = entity.Position.Dist2(other.Position);
                        if (diff <= m_radiusSquared)
                        {
                            if (!sectionDuplicates.ContainsEntity(entity))
                            {
                                sectionDuplicates.Add(new Duplicate(archetype.Name, entity, other));
                            }
                        }

                    }
                }
            }
        }

        #endregion
    }
}
