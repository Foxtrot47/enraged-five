﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Map.Utilities
{
    public static class StaticMapHelperFunctions
    {
        /// <summary>
        /// Return true iff the given amp conomponent contains a map section with the specified name.
        /// This goes through all of the map areas to get at the map sections.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        static public Boolean ContainsMapSection(IMapComponent mapComponent, String name, Boolean recursive)
        {
            if (mapComponent is Level)
            {
                if (recursive == true)
                {
                    foreach (IMapComponent component in (mapComponent as Level).MapComponents.Values)
                    {
                        if (ContainsMapSection(component, name, recursive))
                        {
                            return true;
                        }
                    }
                }
                else
                {
                    foreach (IMapComponent component in (mapComponent as Level).MapComponents.Values)
                    {
                        if (component is MapSection && component.Name == name)
                        {
                            return true;
                        }
                    }
                }
            }
            else if (mapComponent is MapArea)
            {
                if (recursive == true)
                {
                    foreach (IMapComponent component in (mapComponent as MapArea).MapComponents.Values)
                    {
                        if (ContainsMapSection(component, name, recursive))
                        {
                            return true;
                        }
                    }
                }
                else
                {
                    foreach (IMapComponent component in (mapComponent as MapArea).MapComponents.Values)
                    {
                        if (component is MapSection && component.Name == name)
                        {
                            return true;
                        }
                    }
                }
            }
            else if (mapComponent is MapSection)
            {
                return String.Compare((mapComponent as MapSection).Name, name, true) == 0;
            }

            return false;
        }

        static public IEnumerable<MapSection> GetMapSections(IMapComponent mapComponent, Boolean recursive)
        {
            if (mapComponent is Level)
            {
                if (recursive == true)
                {
                    foreach (IMapComponent component in (mapComponent as Level).MapComponents.Values)
                    {
                        foreach (MapSection mapSection in GetMapSections(component, recursive))
                        {
                            yield return mapSection;
                        }
                    }
                }
                else
                {
                    foreach (IMapComponent component in (mapComponent as Level).MapComponents.Values)
                    {
                        if (component is MapSection)
                        {
                            yield return component as MapSection;
                        }
                    }
                }
            }
            else if (mapComponent is MapArea)
            {
                if (recursive == true)
                {
                    foreach (IMapComponent component in (mapComponent as MapArea).MapComponents.Values)
                    {
                        foreach (MapSection mapSection in GetMapSections(component, recursive))
                        {
                            yield return mapSection;
                        }
                    }
                }
                else
                {
                    foreach (IMapComponent component in (mapComponent as MapArea).MapComponents.Values)
                    {
                        if (component is MapSection)
                        {
                            yield return component as MapSection;
                        }
                    }
                }
            }
            else if (mapComponent is MapSection)
            {
                yield return mapComponent as MapSection;
            }
        }

        static public MapSection GetMapSection(IMapComponent mapComponent, String name, Boolean recursive)
        {
            if (mapComponent is Level)
            {
                if (recursive == true)
                {
                    foreach (IMapComponent component in (mapComponent as Level).MapComponents.Values)
                    {
                        if (component is MapArea)
                        {
                            MapSection mapSection = GetMapSection(component, name, recursive);
                            if (mapSection != null)
                            {
                                return mapSection;
                            }
                        }
                        else if (component is MapSection && String.Compare((component as MapSection).Name, name, true) == 0)
                        {
                            return component as MapSection;
                        }
                    }
                }
                else
                {
                    foreach (IMapComponent component in (mapComponent as MapArea).MapComponents.Values)
                    {
                        if (component is MapSection && String.Compare((component as MapSection).Name, name, true) == 0)
                        {
                            return component as MapSection;
                        }
                    }
                }
            }
            else if (mapComponent is MapArea)
            {
                if (recursive == true)
                {
                    foreach (IMapComponent component in (mapComponent as MapArea).MapComponents.Values)
                    {
                        if (component is MapArea)
                        {
                            MapSection mapSection = GetMapSection(component, name, recursive);
                            if (mapSection != null)
                            {
                                return mapSection;
                            }
                        }
                        else if (component is MapSection && String.Compare((component as MapSection).Name, name, true) == 0)
                        {
                            return component as MapSection;
                        }
                    }
                }
                else
                {
                    foreach (IMapComponent component in (mapComponent as MapArea).MapComponents.Values)
                    {
                        if (component is MapSection && String.Compare((component as MapSection).Name, name, true) == 0)
                        {
                            return component as MapSection;
                        }
                    }
                }
            }
            else if (mapComponent is MapSection && String.Compare((mapComponent as MapSection).Name, name, true) == 0)
            {
                return mapComponent as MapSection;
            }

            return null;
        }

        static public MapSection GetMapSectionByName(IMapContainer container, String name, Boolean recursive)
        {
            foreach (IMapComponent childComponent in container.ComponentChildren)
            {
                if (String.Compare(childComponent.Name, name, true) == 0)
                {
                    return childComponent as MapSection;
                }
            }

            if (recursive == true)
            {
                foreach (IMapContainer childContainer in container.ContainerChildren)
                {
                    MapSection result = GetMapSectionByName(childContainer, name, true);
                    if (result != null)
                    {
                        return result as MapSection;
                    }
                }
            }

            return null;
        }

        static public IEnumerable<MapSection> GetAllSections(IMapContainer container, Boolean recursive)
        {
            foreach (MapSection section in container.ComponentChildren)
            {
                yield return section;
            }

            if (recursive == true)
            {
                foreach (IMapContainer childContainer in container.ContainerChildren)
                {
                    foreach (MapSection section in GetAllSections(childContainer, true))
                    {
                        yield return section;
                    }
                }
            }
        }

    }
}
