﻿//---------------------------------------------------------------------------------------------
// <copyright file="MetadataInheritanceHierarchy.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Inheritance
{
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using RSG.Base.Logging;
    using RSG.Editor;
    using RSG.Editor.Model;
    using RSG.Metadata.Model.Definitions;
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// Represents a hierarchy of metadata tunable structures that are apart of the same
    /// inheritance hierarchy.
    /// </summary>
    public class MetadataInheritanceHierarchy : ModelBase
    {
        #region Fields
        /// <summary>
        /// The private reference to the stream resolver used on the parent tunables.
        /// </summary>
        private ParentStreamResolver _resolver;

        /// <summary>
        /// The private field used for the <see cref="Layers"/> property.
        /// </summary>
        private ModelCollection<TunableStructure> _layers;

        /// <summary>
        /// The private field used for the <see cref="Layers"/> property.
        /// </summary>
        private ReadOnlyModelCollection<TunableStructure> _readonlyLayers;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MetadataInheritanceHierarchy"/> class.
        /// </summary>
        /// <param name="reader">
        /// The XmlReader object used to parse the leaf metadata tunable structure.
        /// </param>
        /// <param name="resolver">
        /// A resolver class that is used to convert the string value of the parent to a
        /// stream that can be parsed.
        /// </param>
        /// <param name="undoEngine">
        /// The undo engine that will be associated with this model.
        /// </param>
        /// <param name="scope">
        /// A definition dictionary used to resolve structure references.
        /// </param>
        /// <param name="log">
        /// A object that is used to log any warnings or errors produced during the parsing of
        /// the specified reader.
        /// </param>
        public MetadataInheritanceHierarchy(
            XmlReader reader,
            ParentStreamResolver resolver,
            UndoEngine undoEngine,
            IDefinitionDictionary scope,
            ILog log)
        {
            if (reader == null)
            {
                throw new SmartArgumentNullException(() => reader);
            }

            if (resolver == null)
            {
                throw new SmartArgumentNullException(() => resolver);
            }

            if (scope == null)
            {
                throw new SmartArgumentNullException(() => scope);
            }

            if (log == null)
            {
                throw new SmartArgumentNullException(() => log);
            }

            this._layers = new ModelCollection<TunableStructure>();
            this._readonlyLayers = new ReadOnlyModelCollection<TunableStructure>(this._layers);
            this._resolver = resolver;
            this.CreateHierarchy(reader, undoEngine, scope, log, null);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a list containing the individual layers in the inheritance hierarchy.
        /// </summary>
        public ReadOnlyModelCollection<TunableStructure> Layers
        {
            get { return this._readonlyLayers; }
        }

        /// <summary>
        /// Gets the leaf tunable structure. The bottom of the hierarchy.
        /// </summary>
        public TunableStructure Leaf
        {
            get { return this._layers[this._layers.Count - 1]; }
        }
        #endregion Properties

        #region Methods
        /// <param name="reader">
        /// The XmlReader object that represents the data inside the leaf metadata tunable
        /// structure.
        /// </param>
        /// <param name="undoEngine">
        /// The undo engine that will be associated with this model.
        /// </param>
        /// <param name="scope">
        /// A definition dictionary used to resolve structure references.
        /// </param>
        /// <param name="log">
        /// A object that is used to log any warnings or errors produced during the parsing of
        /// the specified reader.
        /// </param>
        /// <param name="child">
        /// 
        /// </param>
        private void CreateHierarchy(
            XmlReader reader,
            UndoEngine undoEngine,
            IDefinitionDictionary scope,
            ILog log,
            TunableStructure child)
        {
            TunableStructure curr = new TunableStructure(reader, undoEngine, scope, log);
            this._layers.Insert(0, curr);
            StringTunable parentTunable = null;
            foreach (ITunable tunable in curr.Tunables)
            {
                string name = tunable.Member.MetadataName;
                if (parentTunable == null)
                {
                    if (String.Equals(name, "Parent", StringComparison.OrdinalIgnoreCase))
                    {
                        StringTunable stringTunable = tunable as StringTunable;
                        if (stringTunable != null)
                        {
                            parentTunable = stringTunable;
                            break;
                        }
                    }
                }
            }

            if (child != null)
            {
                child.SetInheritanceParent(curr);
            }

            if (parentTunable != null && !String.IsNullOrWhiteSpace(parentTunable.Value))
            {
                XmlReader nextReader = this._resolver.ResolveString(parentTunable.Value);
                if (nextReader != null)
                {
                    using (nextReader)
                    {
                        nextReader.MoveToContent();
                        this.CreateHierarchy(nextReader, undoEngine, scope, log, curr);
                    }
                }
            }
        }
        #endregion Methods
    } // RSG.Metadata.Model.Inheritance.MetadataInheritanceHierarchy {Class}
} // RSG.Metadata.Model.Inheritance {Namespace}
