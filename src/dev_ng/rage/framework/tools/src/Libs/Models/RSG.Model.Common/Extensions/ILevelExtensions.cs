﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.ConfigParser;
using RSG.Model.Common;
using RSG.Model.Common.Map;

namespace RSG.Model.Common.Extensions
{
    /// <summary>
    /// ILevel extension methods
    /// </summary>
    public static class ILevelExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        /// <param name="gv"></param>
        /// <returns></returns>
        public static ContentNodeLevel GetConfigLevelNode(this ILevel level, ConfigGameView gv)
        {
            return (ContentNodeLevel)gv.Content.Root.FindAll(level.Name, "level").FirstOrDefault();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        public static IMapHierarchy GetMapHierarchy(this ILevel level)
        {
            return level.MapHierarchy;
        }
    } // ILevelExtensions
}
