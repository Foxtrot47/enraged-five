﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Configuration;

namespace RSG.Model.Asset.Util
{
    /// <summary>
    /// Util methods for determining filepaths for DLC projects.
    /// </summary>
    public static class DLCContentUtil
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="fileType"></param>
        /// <returns></returns>
        public static IEnumerable<String> GetFilenames(IBranch branch, String filetype)
        {
            // Parse the content file looking for the appropriate files.
            String setupFilename = Path.Combine(branch.Build, "setup2.xml");

            String dlcGameName;
            String contentFilename;
            if (GetSetupInformation(setupFilename, out dlcGameName, out contentFilename))
            {
                contentFilename = Path.Combine(branch.Build, contentFilename);
                return GetFilenames(contentFilename, dlcGameName, branch.Build, filetype);
            }

            return Enumerable.Empty<String>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="contentFilename"></param>
        /// <returns></returns>
        private static IEnumerable<String> GetFilenames(String contentFilename, String dlcGameName, String buildDir, String filetype)
        {
            if (File.Exists(contentFilename))
            {
                XDocument doc = XDocument.Load(contentFilename);
                foreach (XElement itemElem in doc.XPathSelectElements("//dataFiles/Item"))
                {
                    // Is this an element that we should process?
                    XElement filenameElem = itemElem.Element("filename");
                    XElement fileTypeElem = itemElem.Element("fileType");
                    if (filenameElem != null && fileTypeElem != null && fileTypeElem.Value == filetype)
                    {
                        yield return filenameElem.Value.Replace(String.Format("{0}:", dlcGameName), buildDir);
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="setupFilename"></param>
        /// <returns></returns>
        private static bool GetSetupInformation(String setupFilename, out String dlcGameName, out String contentFilename)
        {
            if (File.Exists(setupFilename))
            {
                XDocument doc = XDocument.Load(setupFilename);
                XElement nameElem = doc.XPathSelectElement("/SSetupData/deviceName");
                XElement contentElem = doc.XPathSelectElement("/SSetupData/datFile");
                if (nameElem != null && contentElem != null)
                {
                    dlcGameName = nameElem.Value;
                    contentFilename = contentElem.Value;
                    return true;
                }
            }

            // If we fall through to here it means we failed to extract the required information from the setup file.
            contentFilename = null;
            dlcGameName = null;
            return false;
        }
    } // DLCContentUtil
}
