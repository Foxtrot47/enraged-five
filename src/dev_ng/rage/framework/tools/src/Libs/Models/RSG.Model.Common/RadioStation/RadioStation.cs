﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Model.Common.RadioStation
{
    /// <summary>
    /// Concrete implementation of a radio station.
    /// </summary>
    [DataContract]
    public class RadioStation : AssetBase, IRadioStation
    {
        #region Properties
        /// <summary>
        /// Friendly name for this radio station.
        /// </summary>
        [DataMember]
        public String FriendlyName { get; set; }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// For serialisation purposes.
        /// </summary>
        private RadioStation()
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public RadioStation(String name, String friendlyName)
            : base(name)
        {
            FriendlyName = friendlyName;
        }
        #endregion // Constructor(s)
    } // RadioStation
}
