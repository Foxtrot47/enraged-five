﻿using System;
using System.Collections.Generic;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using RSG.Base.Math;
using RSG.Base.ConfigParser;
using RSG.Base.Collections;
using RSG.Model.Common.Map;

namespace RSG.Model.Common
{

    /// <summary>
    /// ILevel interface; defining publically accessible features of a game
    /// level object.
    /// </summary>
    public interface ILevel : 
        IAsset,
        IComparable<ILevel>,
        IEquatable<ILevel>,
        IStreamableObject,
        IDisposable
    {
        #region Properties
        /// <summary>
        /// The collection this level is a part of
        /// </summary>
        ILevelCollection ParentCollection { get; }

        /// <summary>
        /// Represents whether this level has had it's map hierarchy
        /// added to it through the use of the config parser.
        /// </summary>
        bool Initialised { get; set; }

        /// <summary>
        /// Level map image.
        /// </summary>
        BitmapImage Image { get; }

        /// <summary>
        /// Level map image bounding box; basically the game worldspace 
        /// bounding box.
        /// </summary>
        BoundingBox2f ImageBounds { get; }

        /// <summary>
        /// List of spawn points that are gathered from the metadata files.
        /// </summary>
        IList<ISpawnPoint> SpawnPoints { get; }

        /// <summary>
        /// List of spawn points that are gathered from the metadata files.
        /// </summary>
        IList<IChainingGraph> ChainingGraphs { get; }

        /// <summary>
        /// Level export data path.
        /// </summary>
        String ExportDataPath { get; }

        /// <summary>
        /// Level processed data path.
        /// </summary>
        String ProcessedDataPath { get; }
        #endregion // Properties

        #region Level Components
        /// <summary>
        /// Map hierarchy data (i.e. map areas/map sections).
        /// </summary>
        IMapHierarchy MapHierarchy { get; set; }

        /// <summary>
        /// Collection of characters.
        /// </summary>
        ICharacterCollection Characters { get; set; }

        /// <summary>
        /// Collection of vehicles.
        /// </summary>
        IVehicleCollection Vehicles { get; set; }

        /// <summary>
        /// Collection of weapons.
        /// </summary>
        IWeaponCollection Weapons { get; set; }

        /// <summary>
        /// The pathanme of the geometry stats document (or null)
        /// </summary>
        IGeometryStatsCollection GeometryStats { get; set; }
        #endregion // Level Components
    } // ILevel

} // RSG.Model.Common namespace
