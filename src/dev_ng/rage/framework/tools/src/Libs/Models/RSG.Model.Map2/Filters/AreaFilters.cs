﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.ConfigParser;

namespace RSG.Model.Map.Filters
{
    /// <summary>
    /// Used to filter map areas.
    /// </summary>
    /// Only map areas that pass this method are loaded.
    /// <param name="mapGroup"></param>
    /// <returns></returns>
    public delegate Boolean FilterMapAreaDelegate(ContentNodeGroup mapGroup, Object parameter);

    /// <summary>
    /// Common map area filter methods.
    /// </summary>
    public static class AreaFilters
    {
        /// <summary>
        /// Returns true for all areas so that everything gets loaded
        /// </summary>
        public static Boolean All(RSG.Base.ConfigParser.ContentNodeGroup groupNode, Object parameter)
        {
            return true;
        }

        /// <summary>
        /// Returns false for all areas so that nothing gets loaded
        /// </summary>
        public static Boolean None(RSG.Base.ConfigParser.ContentNodeGroup groupNode, Object parameter)
        {
            return false;
        }
    } // AreaFilters
} // RSG.Model.Common.Filters
