﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using RSG.Base.ConfigParser;
using RSG.Base.Logging;
using RSG.ManagedRage;
using RSG.Model.Common;
using RSG.Base.Tasks;
using RSG.Model.Common.Map;

namespace RSG.Model.Report.Reports.Map
{
    /// <summary>
    /// 
    /// </summary>
    public class CarGenReport : CSVReport, IDynamicLevelReport
    {
        #region Constants
        private const String c_name = "Car Generators Report";
        private const String c_description = "Exports all the car gens (forced and not forced) into a .csv file."; 
        #endregion // Constants

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (m_task == null)
                {
                    CompositeTask task = new CompositeTask("Generate Report");
                    task.AddSubTask(new ActionTask("Loading data", (context, progress) => EnsureDataLoaded((DynamicLevelReportContext)context, progress)), 50);
                    task.AddSubTask(new ActionTask("Generating report", (context, progress) => GenerateReport((DynamicLevelReportContext)context, progress)));
                    m_task = task;
                }
                return m_task;
            }
        }
        private ITask m_task;

        /// <summary>
        /// List of stats that are required to generate this report.
        /// </summary>
        public IEnumerable<StreamableStat> RequiredStats
        {
            get { return null; }
        }
        #endregion // Properties

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public CarGenReport()
            : base(c_name, c_description)
        {
        }
        #endregion // Constructor

        #region Task Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void EnsureDataLoaded(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            IMapHierarchy hierarchy = context.Level.MapHierarchy;

            if (hierarchy != null)
            {
                hierarchy.RequestStatisticsForSections(hierarchy.AllSections, new StreamableStat[] { StreamableMapSectionStat.CarGens }, false);
                /*
                float increment = 1.0f / hierarchy.AllSections.Count();

                // Load all the map section data
                foreach (IMapSection section in hierarchy.AllSections)
                {
                    context.Token.ThrowIfCancellationRequested();

                    // Update the progress information
                    string message = String.Format("Loading {0}", section.Name);
                    progress.Report(new TaskProgress(increment, true, message));

                    section.RequestStatistics(new Guid[] { StreamableMapSectionStat.CarGens });
                }
                */
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void GenerateReport(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            IMapHierarchy hierarchy = context.Level.MapHierarchy;

            if (hierarchy != null)
            {
                using (StreamWriter sw = new StreamWriter(Filename))
                {
                    WriteCsvHeader(sw);

                    foreach (IMapSection section in hierarchy.AllSections.Where(item => item.CarGens != null))
                    {
                        foreach (ICarGen carGen in section.CarGens)
                        {
                            WriteCsvCarGen(carGen, sw);
                        }
                    }
                }
            }
        }
        #endregion // Task Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sw"></param>
        /// <param name="milo"></param>
        private void WriteCsvHeader(StreamWriter sw)
        {
            sw.WriteLine("Name,Container,Coords,Vehicle Type,High Priority");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sw"></param>
        /// <param name="milo"></param>
        private void WriteCsvCarGen(ICarGen carGen, StreamWriter sw)
        {
            string coords = String.Format("{0}:{1}:{2}", carGen.Position.X, carGen.Position.Y, carGen.Position.Z);

            string type = null;
            if (carGen.IsSpecificModel == true)
            {
                type = carGen.ModelName;
            }
            else
            {
                if (carGen.IsPolice == true)
                    type = "Generic Police";
                else if (carGen.IsAmbulance == true)
                    type = "Generic Ambulance";
                else if (carGen.IsFire == true)
                    type = "Generic Fire";
                else
                    type = "Generic";
            }

            sw.WriteLine(String.Format("{0},{1},{2},{3},{4}", carGen.Name, carGen.ParentSection.Name, coords, type, carGen.IsHighPriority));
        }
        #endregion // Private Methods
    } // CarGenReport
}
