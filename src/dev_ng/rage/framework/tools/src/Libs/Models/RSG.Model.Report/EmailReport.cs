﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;

namespace RSG.Model.Report
{
    public class EmailReport : Report, IReportMailProvider, IDisposable
    {
        #region Properties
        /// <summary>
        /// Mail message to send when the report is invoked
        /// </summary>
        public MailMessage MailMessage
        {
            get;
            protected set;
        }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="desc"></param>
        public EmailReport(string name, string desc)
            : base(name, desc)
        {
        }
        #endregion // Constructor(s)

        #region IDisposable Implementation
        /// <summary>
        /// Ensures that the stream is correctly disposed of
        /// </summary>
        public void Dispose()
        {
            if (MailMessage != null)
            {
                MailMessage.Dispose();
            }
        }
        #endregion // IDisposable Implementation
    } // EmailReport
}
