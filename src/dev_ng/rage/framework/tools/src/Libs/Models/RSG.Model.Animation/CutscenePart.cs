﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Model.Common;
using RSG.Model.Common.Animation;

namespace RSG.Model.Animation
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    public class CutscenePart : AssetBase, ICutscenePart
    {
        #region Properties
        /// <summary>
        /// Filename containing the cutscene information.
        /// </summary>
        [DataMember]
        public String CutFile
        {
            get { return m_cutFile; }
            private set
            {
                SetPropertyValue(ref m_cutFile, value, "CutFile");
            }
        }
        private String m_cutFile;

        /// <summary>
        /// How long this part is (in seconds).
        /// </summary>
        [DataMember]
        public float Duration
        {
            get { return m_duration; }
            private set
            {
                SetPropertyValue(ref m_duration, value, "Duration");
            }
        }
        private float m_duration;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public CutscenePart(String name, String cutFile)
            : base(name)
        {
            CutFile = cutFile;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public CutscenePart(String name, String cutFile, float duration)
            : this(name, cutFile)
        {
            Duration = duration;
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// Parses the data stream file to extract the part's duration.
        /// </summary>
        public void LoadStatsFromAssetData()
        {
            Debug.Assert(File.Exists(CutFile), "Cut file doesn't exist.");
            if (File.Exists(CutFile))
            {
                try
                {
                    XDocument doc = XDocument.Load(CutFile);
                    XElement durationElem = doc.XPathSelectElement("//fTotalDuration");
                    if (durationElem != null)
                    {
                        XAttribute valueAtt = durationElem.Attribute("value");
                        if (valueAtt != null)
                        {
                            Duration = Single.Parse(valueAtt.Value);
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    Log.Log__Exception(ex, "Unexpected exception while parsing the {0} file.", CutFile);
                }
            }
        }
        #endregion // Public Methods
    } // CutscenePart
}
