﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common.GameText;

namespace RSG.Model.Asset.GameText
{
    /// <summary>
    /// 
    /// </summary>
    public class GameTextEntry : IGameTextEntry
    {
        /// <summary>
        /// 
        /// </summary>
        private String _label;

        /// <summary>
        /// 
        /// </summary>
        private String _text;

        /// <summary>
        /// 
        /// </summary>
        private RSG.Platform.Platform? _platform;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="label"></param>
        /// <param name="text"></param>
        public GameTextEntry(String label, String text)
            : this(label, null, text)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="label"></param>
        /// <param name="platform"></param>
        /// <param name="text"></param>
        public GameTextEntry(String label, RSG.Platform.Platform? platform, String text)
        {
            _label = label;
            _platform = platform;
            _text = text;
        }

        /// <summary>
        /// Label associated with this piece of game text.
        /// </summary>
        public String Label
        {
            get { return _label; }
        }

        /// <summary>
        /// Textual string associated with this label.
        /// </summary>
        public String Text
        {
            get { return _text; }
        }

        /// <summary>
        /// Optional platform that this piece of text is for. 
        /// If null it indicates that it is for all platforms (provided
        /// there isn't a platform specific version for it already).
        /// </summary>
        public RSG.Platform.Platform? Platform
        {
            get { return _platform; }
        }

        public override String ToString()
        {
            return Text;
        }
    } // GameTextEntry
}
