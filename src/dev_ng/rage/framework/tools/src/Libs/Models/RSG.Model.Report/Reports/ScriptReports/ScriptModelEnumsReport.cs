﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.ConfigParser;
using System.Diagnostics;
using RSG.Base.Logging;
using System.IO;
using System.Text.RegularExpressions;
using System.Web.UI;

namespace RSG.Model.Report.Reports.ScriptReports
{
    /// <summary>
    /// Generates a report that shows information about the script model enums including
    /// which ones aren't being used
    /// </summary>
    public class ScriptModelEnumsReport : ScriptReportBase
    {
        #region Constants
        private const String NAME = "Script Model Enums Report";
        private const String DESC = "Generates a report that shows information about the script model enums including which ones aren't being used.";

        private const String MODEL_ENUM_FILEPATH = @"script:\core\game\data\model_enums.sch";

        /// <summary>
        /// Regex used to match an enum line
        /// Matches lines like the following:
        /// "NAME=1234,      //"
        /// "NAME=1234           //"
        /// </summary>
        private static readonly Regex LINE_MATCH_REGEX = new Regex(@"^(?<name>\w+)=(?<hash>[-\d]+),?[ \t]*//");
        #endregion // Constants

        #region Properties
        /// <summary>
        /// The file that we wish to search for enum values in
        /// </summary>
        public override string EnumFile
        {
            get
            {
                return MODEL_ENUM_FILEPATH;
            }
        }

        /// <summary>
        /// The regex to use when searching through the enum file
        /// </summary>
        public override Regex EnumValueRegex
        {
            get
            {
                return LINE_MATCH_REGEX;
            }
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ScriptModelEnumsReport()
            : base(NAME, DESC)
        {
        }
        #endregion // Constructor(s)
    } // ScriptModelEnumsReport
}
